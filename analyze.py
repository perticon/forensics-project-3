import json
import numpy as np
import matplotlib.pyplot as plt

stats_base_path = "stats"
nbins = 20  # Number of bins to divide the LOCs into

# Calculates the Equal Width Bin ranges for a certain list (locs) and
# given a certain number of bins (nbins)
def equi_width_bins(locs, nbins):
    plt.figure(0)

    n, bins, _ = plt.hist(locs, bins=nbins)
    plt.close()
    return bins

# Extracts from the db file the number of LOCs for each function and
# returns the set (only unique values) of these LOCs
def source_locs_set(db):
    locs = set()
    for program in db.keys():
        for function, decompilers in db[program].items():
            if "source_code" in decompilers and "LOC" in decompilers["source_code"]:
                locs.add(decompilers["source_code"]["LOC"])
            else:
                print(f"Function {function} in program {program} has created a problem in source code LOC exraction")
    return locs

# Returns the list of decompilers available in the db.json file
def get_decompilers():
    # Can be obtained by doing an union of all the decompilers that
    # appear in the DB, for now they are hardcoded
    return ["ghidra", "retdec", "snowman", "angr", "r2dec", "reko"]

# Returns the list of metrics available in the db.json file
def get_metrics():
    return ["LOC", "MVG", "COM", "L_C", "M_C", "goto"]

def main():
    db = json.load(open(f"{stats_base_path}/db.json"))
    errors = json.load(open(f"{stats_base_path}/errors.json"))

    locs = source_locs_set(db)      # Extract the number of unique values of LOCs
                                    # of the source code
    sorted_locs=sorted(list(locs))  # Sort the values in ascending order
    loc_bins_ranges = equi_width_bins(sorted_locs, nbins)   # Calculate the bins ranges according to
                                                            # the required number of bins

    #Initialize bins
    bins = []
    for i in range(nbins):
        bins.append({"nfunctions": 0})
        for dec in get_decompilers():
            bins[i][dec] = {}
            for metric in get_metrics():
                bins[i][dec][metric] = []

    print("Computing the metrics average in bins")
    for program,functions in db.items():
        for function, decompilers in functions.items():
            if "source_code" not in decompilers or "LOC" not in decompilers["source_code"]:
                print(f"Can't analyze function {function} from program {program}")
                continue

            #Find the bin the function belogs to
            for bin in range(1, nbins+1):
                if decompilers["source_code"]["LOC"] <= loc_bins_ranges[bin]:
                    break
            bin -= 1

            # Add to bin frequency count
            bins[bin]["nfunctions"] += 1
            # Add metric value to list of values per bin
            for decompiler, metrics in decompilers.items():
                if decompiler == "source_code": # Source code not considered in the calculations
                    continue
                for metric, value in metrics.items():
                    # Add the metric value if the value is there, otherwise put not shown number
                    # on the graphs (0)
                    bins[bin][decompiler][metric].append(value if value != None else 0)
    
    # Pretty output
    #print(json.dumps(bins, sort_keys=True, indent=4))

    print("Preparing to plot")
    plot_y_gen = {}
    for metric in get_metrics():    # For each metric
        plot_y = []         # List of decompilers' metric average per bin
        for decompiler in get_decompilers():    # For each decompiler
            dec_plot = []   # Single decompiler's metric average per bin
            for bin in bins:    # For each bin structure
                vals = bin[decompiler][metric]  # Take the values for that decompiler and metric
                if (len(vals) == 0):    # If the values are empty say that the average is 0
                    dec_plot.append(0)
                    continue
                dec_plot.append(np.average(np.array(vals))) # Calculate the average of the values
            plot_y.append(dec_plot)     # Append result to the decompilers list
        plot_y_gen[metric] = plot_y     # Append result to the output dict for each metric

    # Prepare the X axis variable by taking the beginning value of each bin
    # and creating a list with these values for each decompiler
    plot_x=[list(loc_bins_ranges)[0:-1] for x in range(len(get_decompilers()))]
    

    plt.figure(1)
    plt.hist(loc_bins_ranges[0:-1], weights=[x["nfunctions"] for x in bins])
    plt.xlabel("LOCs of source file (ranges)")
    plt.ylabel("N° Functions in range")
    plt.title("Frequency distribution of functions LOC")
    plt.show()

    # Show the plots
    print("Plotting:")
    for metric in get_metrics():
        print(f"\t- {metric}")
        plt.figure(1)
        plt.hist(plot_x, weights=plot_y_gen[metric], label=get_decompilers())
        plt.legend()
        plt.title(f"Average {metric} for every decompiler in ranges")
        plt.xlabel("LOCs of source file (ranges)")
        plt.ylabel(f"Average {metric}")
        plt.show()

if __name__ == "__main__":
    main()