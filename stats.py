from re import L
import xml.etree.ElementTree as ET
import subprocess
import os
import json
import glob

"""
    Compute the metrics for the decompiled functions and save them in the db dictionary
    If there is an error in the extraction, the log is saved in the errors dictionary
    It uses cccc tools to extract the metrics from the decompiled files
    if cccc fails the metrics are extracted from the function files in the decompiled_base_path
    More metrics are computed by the function extract_more_stats
    extract_more_stats extract also the metrics where cccc failed
"""

decompiled_base_path = "decompiled"
stats_base_path = "stats"
# error values in cccc stats
not_a_value_in_cccc_stats = ["********","------","******"]

                
def parse_cccc_metrics(func):
    obj = {}
    obj["LOC"] = int(func.find("lines_of_code").get("value")) if func.find("lines_of_code").get("value") not in not_a_value_in_cccc_stats else None
    obj["MVG"] = int(func.find("McCabes_cyclomatic_complexity").get("value")) if func.find("McCabes_cyclomatic_complexity").get("value") not in not_a_value_in_cccc_stats else None
    obj["COM"] = int(func.find("lines_of_comment").get("value")) if func.find("lines_of_comment").get("value") not in not_a_value_in_cccc_stats else None
    obj["L_C"] = float(func.find("lines_of_code_per_line_of_comment").get("value")) if func.find("lines_of_code_per_line_of_comment").get("value") not in not_a_value_in_cccc_stats else None
    obj["M_C"] = float(func.find("McCabes_cyclomatic_complexity_per_line_of_comment").get("value")) if func.find("McCabes_cyclomatic_complexity_per_line_of_comment").get("value") not in not_a_value_in_cccc_stats else None
    return obj


"""
Structure of the "database" file:
{progname: {
    function: {
        decompiler: {
            LOC: int,
            MVG: int,
            COM: int,
            L_C: float,
            M_C: float,
            goto: int
        }
    }
}}
"""
"""
all files in folder /cccc/prog_name/decompiler
parse xml file to retrieve functions names and stats
extract only functions with a corresponding name in the source code
stats present:
- LOC: Lines of Code (excluding comments)
- MVG: McCabe's cyclomatic complexity
- COM: Comment lines
- L_C: Lines of Code per lines of comment
- M_C: Cyclomatic complexity per lines of comment
"""

def cccc_info_extraction(db, errors):
    """
    Function metrics extraction by CCCC tool
    if there is some error in the extraction, the log is reported in the errors dictionary
    all the stats extracted are saved in the db dictionary
    :param db: dictionary containing all the stats
    :param errors: dictionary containing the logs of the extraction
    """
    # decompiled/prog_name/decompiler.c

    for prog_name in os.listdir(decompiled_base_path):
        errors[prog_name] = {}
        db[prog_name] = {}
        src_func_names = os.listdir(f"{decompiled_base_path}/{prog_name}/functions")

        for decompiler_file in os.listdir(f"{decompiled_base_path}/{prog_name}"):
            
            # setup and run cccc
            decompiler_name = decompiler_file.split(".")[0]
            
            if os.path.isdir(f"{decompiled_base_path}/{prog_name}/{decompiler_file}"):
                continue
            os.makedirs(f"{stats_base_path}/{prog_name}/{decompiler_name}", exist_ok=True)
            try:
                log = subprocess.getoutput(f'cccc --outdir={stats_base_path}/{prog_name}/{decompiler_name} {decompiled_base_path}/{prog_name}/{decompiler_file}')
            except UnicodeDecodeError as e:
                print(f"UTF-8 decode while running cccc on {decompiled_base_path}/{prog_name}/{decompiler_file}")
            
            # if the anonymous.xml file is not present, cccc has failed to extract the metrics,
            # so the log is saved in the errors dictionary
            if not os.path.isfile(f"{stats_base_path}/{prog_name}/{decompiler_name}/anonymous.xml"):
                errors[prog_name][decompiler_name] = log.split("details.\n")[1].split("\n")
                continue

            # parse anonymous.xml file to retrieve functions names and stats
            for func in ET.parse(f"{stats_base_path}/{prog_name}/{decompiler_name}/anonymous.xml").getroot().findall("procedural_detail/member_function"):
                func_name = func.find("name").text.split("(")[0]
                # the function name has to be the same of the source code, otherwise the stats cannot be compared
                if func_name not in src_func_names:
                    continue
                if not db[prog_name].get(func_name):
                    db[prog_name][func_name] = {}
                # db populated with the stats
                db[prog_name][func_name][decompiler_name] = parse_cccc_metrics(func)


def force_function_extraction(prog_name, decompiler_name, func_name, db, errors):
    """
    Function used when cccc fails to extract the metrics from the decompiled files
    It uses cccc tools function by function to extract the metrics, using the parsed function in the functions folders
    :param prog_name: name of the program
    :param decompiler_name: name of the decompiler
    :param func_name: name of the function
    :param db: dictionary containing all the stats
    :param errors: dictionary containing the error logs of cccc
    """

    os.makedirs(f"{stats_base_path}/{prog_name}/functions/{func_name}/{decompiler_name}", exist_ok=True)
    try:
        log = subprocess.getoutput(f'cccc --outdir={stats_base_path}/{prog_name}/functions/{func_name}/{decompiler_name} {decompiled_base_path}/{prog_name}/functions/{func_name}/{decompiler_name}.c')
    except UnicodeDecodeError as e:
        print(f"UTF-8 decode while running cccc on {decompiled_base_path}/{prog_name}/functions/{func_name}/{decompiler_name}.c")
        
    # if there is no key for this function add it to the database and initialize the entry
    if not db[prog_name].get(func_name):
        db[prog_name][func_name] = {}

    # if cccc failed to extract from decompiler file, the decompiler field for that function should be initialized 
    if not db[prog_name][func_name].get(decompiler_name):
        db[prog_name][func_name][decompiler_name] = {}
    
    # if there is still an error in the extraction, it is reported in the log file
    if not os.path.isfile(f"{stats_base_path}/{prog_name}/functions/{func_name}/{decompiler_name}/anonymous.xml"):
        if not errors[prog_name].get(func_name):
            errors[prog_name][func_name] = {}
        errors[prog_name][func_name][decompiler_name] = log.split("details.\n")[1].split('\n')
        return 

    # db populated with the stats
    for func in ET.parse(f"{stats_base_path}/{prog_name}/functions/{func_name}/{decompiler_name}/anonymous.xml").getroot().findall("procedural_detail/member_function"):
        db[prog_name][func_name][decompiler_name] = parse_cccc_metrics(func)
    
            
def extract_more_stats(db, errors):
    """
    extract more stats from the decompiled files at function level
    extraction at function level by cccc where it failed to do at the whole program level
    implementation of the following metrics:
    - number of goto statements
    #  Here you can add your own metrics
    :param db: dictionary containing all the stats
    :param errors: dictionary containing the error logs of cccc
    """

    for prog_name in os.listdir(decompiled_base_path):
        for func_name in os.listdir(f"{decompiled_base_path}/{prog_name}/functions"):
            # if the func_name is not a directory, the it is the decompiled file 
            # but the program needs only the decompiled code of each function at his point
            if not os.path.isdir(f"{decompiled_base_path}/{prog_name}/functions/{func_name}"):
                continue
            for decompiler_file in os.listdir(f"{decompiled_base_path}/{prog_name}/functions/{func_name}"):            
                decompiler_name = decompiler_file.split(".")[0]
                # Stats form source code are not needed
                #if decompiler_name == "source_code":
                #    continue
                f = open(f"{decompiled_base_path}/{prog_name}/functions/{func_name}/{decompiler_file}", "r")
                lines = f.read()
                
                # if cccc failed to the function from every decompiler file or
                # if the function has not been extracted correctly from the decompilerd 
                # by cccc to compute the metrics, the extraction is forced at function level
                # !!! This is also used to calculate metrics on the source code !!!
                if not db[prog_name].get(func_name) or not db[prog_name][func_name].get(decompiler_name):
                    force_function_extraction(prog_name, decompiler_name, func_name, db, errors)
                    

                # number of goto statements
                db[prog_name][func_name][decompiler_name]["goto"] = lines.count(r"goto")
                        
                # TODO Here you can add more stats
                f.close()
               

def clean_useless_files():
    """
    remove all the .html, .opt, .db files from /stats_base_path/ and subdirectories
    created by cccc
    """
    for filename in glob.iglob(f"{stats_base_path}/**/*.html", recursive=True):
        os.remove(filename)
    for filename in glob.iglob(f"{stats_base_path}/**/*.opt", recursive=True):
        os.remove(filename)
    for filename in glob.iglob(f"{stats_base_path}/**/*.db", recursive=True):
        os.remove(filename)
        

def main():
    db = json.load(open(f"{stats_base_path}/db.json"))
    errors = json.load(open(f"{stats_base_path}/errors.json"))
    # First metrics extraction by cccc
    cccc_info_extraction(db, errors)
    # Second custom metrics and forced extraction where cccc failed at whole code level
    extract_more_stats(db, errors)
    # Third clean useless files generated by cccc
    clean_useless_files()
    # Save the db and errors in the stats_base_path as json files
    json.dump(db, open(f"{stats_base_path}/db.json", "w"))
    json.dump(errors, open(f"{stats_base_path}/errors.json", "w"))




if __name__ == "__main__":
    main()