void main(int param_1,undefined8 *param_2)

{
  bool bVar1;
  size_t sVar2;
  size_t sVar3;
  char *__src;
  char *pcVar4;
  char *pcVar5;
  int *piVar6;
  char **ppcVar7;
  long lVar8;
  ulong uVar9;
  char **ppcVar10;
  undefined8 uVar11;
  char **local_48;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  uVar11 = 0x10263b;
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,&DAT_00107004,"GNU coreutils",Version,1,usage,"David MacKenzie",0,
             uVar11);
  ppcVar7 = (char **)(param_2 + optind);
  local_48 = (char **)(param_2 + param_1);
  if (optind == param_1) {
    *local_48 = "y";
    local_48 = local_48 + 1;
  }
  __src = *ppcVar7;
  bVar1 = true;
  uVar9 = 0;
  ppcVar10 = ppcVar7;
  pcVar5 = __src;
  while( true ) {
    ppcVar10 = ppcVar10 + 1;
    sVar2 = strlen(pcVar5);
    uVar9 = uVar9 + sVar2 + 1;
    if (local_48 <= ppcVar10) break;
    pcVar4 = pcVar5 + sVar2 + 1;
    pcVar5 = *ppcVar10;
    if (pcVar4 != pcVar5) {
      bVar1 = false;
    }
  }
  if (uVar9 < 0x1001) {
    uVar9 = 0x2000;
  }
  else if (bVar1) goto LAB_001026ef;
  bVar1 = false;
  __src = (char *)xmalloc(uVar9);
LAB_001026ef:
  sVar2 = 0;
  do {
    pcVar5 = *ppcVar7;
    sVar3 = strlen(pcVar5);
    if (!bVar1) {
      memcpy(__src + sVar2,pcVar5,sVar3);
    }
    lVar8 = sVar2 + sVar3;
    ppcVar7 = ppcVar7 + 1;
    sVar2 = lVar8 + 1;
    __src[lVar8] = ' ';
  } while (ppcVar7 < local_48);
  __src[lVar8] = '\n';
  lVar8 = uVar9 / sVar2 - 1;
  pcVar5 = __src;
  if (lVar8 != 0) {
    do {
      pcVar5 = (char *)memcpy(pcVar5 + sVar2,__src,sVar2);
      lVar8 = lVar8 + -1;
    } while (lVar8 != 0);
    sVar2 = sVar2 * (uVar9 / sVar2);
  }
  do {
    sVar3 = full_write(1,__src,sVar2);
  } while (sVar2 == sVar3);
  uVar11 = dcgettext(0,"standard output",5);
  piVar6 = __errno_location();
  error(0,*piVar6,uVar11);
                    /* WARNING: Subroutine does not return */
  exit(1);
}