int64_t scan_zeros (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x20)) != 0) {
        goto label_0;
    }
    if (*((rdi + 0x19)) != 0) {
        goto label_0;
    }
    if (*(obj.head) == 0) {
        void (*0x2950)() ();
    }
    rax = zeros;
    *((rax + 0x28)) = rdi;
    *(obj.zeros) = rdi;
label_0:
    eax = 0;
    return rax;
    __asm ("in eax, dx");
}