new_item (char const *str)
{
  /* T1. Initialize (COUNT[k] <- 0 and TOP[k] <- ^).  */
  struct item *k = xzalloc (sizeof *k);
  if (str)
    k->str = xstrdup (str);
  return k;
}