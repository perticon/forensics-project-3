char ** search_item(char **param_1,char *param_2)

{
  char cVar1;
  int iVar2;
  char **ppcVar3;
  char *pcVar4;
  char **ppcVar5;
  char **ppcVar6;
  char **ppcVar7;
  char cVar8;
  uint uVar9;
  char **ppcVar10;
  
  ppcVar3 = (char **)param_1[2];
  if (ppcVar3 == (char **)0x0) {
    ppcVar3 = (char **)xzalloc(0x38);
    if (param_2 != (char *)0x0) {
      pcVar4 = (char *)xstrdup(param_2);
      *ppcVar3 = pcVar4;
    }
    param_1[2] = (char *)ppcVar3;
    return ppcVar3;
  }
  ppcVar10 = ppcVar3;
  if (param_2 != (char *)0x0) {
    while (ppcVar6 = ppcVar3, *ppcVar6 != (char *)0x0) {
      iVar2 = strcmp(param_2,*ppcVar6);
      if (iVar2 == 0) {
        return ppcVar6;
      }
      ppcVar3 = (char **)ppcVar6[2];
      if (iVar2 < 0) {
        ppcVar3 = (char **)ppcVar6[1];
      }
      if (ppcVar3 == (char **)0x0) {
        ppcVar3 = (char **)xzalloc(0x38);
        pcVar4 = (char *)xstrdup(param_2);
        *ppcVar3 = pcVar4;
        if (iVar2 < 0) {
          ppcVar6[1] = (char *)ppcVar3;
        }
        else {
          ppcVar6[2] = (char *)ppcVar3;
        }
        if (*ppcVar10 != (char *)0x0) {
          iVar2 = strcmp(param_2,*ppcVar10);
          if (iVar2 != 0) {
            ppcVar6 = (char **)ppcVar10[2];
            if (iVar2 < 0) {
              ppcVar6 = (char **)ppcVar10[1];
            }
            uVar9 = iVar2 >> 0x1f | 1;
            if (ppcVar6 == ppcVar3) goto LAB_00102f39;
            ppcVar7 = ppcVar6;
            if (ppcVar6 == (char **)0x0) goto LAB_0010308e;
            goto LAB_00102f0e;
          }
        }
                    /* WARNING: Subroutine does not return */
        __assert_fail("str && s && s->str && !STREQ (str, s->str)","src/tsort.c",0xa3,"search_item")
        ;
      }
      if (*(char *)(ppcVar3 + 3) != '\0') {
        param_1 = ppcVar6;
        ppcVar10 = ppcVar3;
      }
    }
  }
                    /* WARNING: Subroutine does not return */
  __assert_fail("str && p && p->str","src/tsort.c",0x8c,"search_item");
LAB_00102f0e:
  do {
    if (*ppcVar7 == (char *)0x0) break;
    iVar2 = strcmp(param_2,*ppcVar7);
    if (iVar2 == 0) break;
    if (iVar2 < 0) {
      *(char *)(ppcVar7 + 3) = -1;
      ppcVar7 = (char **)ppcVar7[1];
    }
    else {
      *(char *)(ppcVar7 + 3) = '\x01';
      ppcVar7 = (char **)ppcVar7[2];
    }
    if (ppcVar7 == ppcVar3) {
LAB_00102f39:
      cVar1 = *(char *)(ppcVar10 + 3);
      cVar8 = (char)uVar9;
      if ((cVar1 == '\0') || ((int)cVar1 == -uVar9)) {
        *(char *)(ppcVar10 + 3) = cVar1 + cVar8;
        return ppcVar3;
      }
      if ((int)*(char *)(ppcVar6 + 3) == uVar9) {
        if (uVar9 == 0xffffffff) {
          ppcVar10[1] = ppcVar6[2];
          ppcVar6[2] = (char *)ppcVar10;
        }
        else {
          ppcVar10[2] = ppcVar6[1];
          ppcVar6[1] = (char *)ppcVar10;
        }
        *(char *)(ppcVar6 + 3) = '\0';
        *(char *)(ppcVar10 + 3) = '\0';
      }
      else {
        ppcVar7 = (char **)ppcVar6[1];
        ppcVar5 = (char **)ppcVar6[2];
        if (uVar9 == 0xffffffff) {
          ppcVar6[2] = ppcVar5[1];
          ppcVar5[1] = (char *)ppcVar6;
          ppcVar10[1] = ppcVar5[2];
          ppcVar5[2] = (char *)ppcVar10;
        }
        else {
          ppcVar6[1] = ppcVar7[2];
          ppcVar7[2] = (char *)ppcVar6;
          ppcVar10[2] = ppcVar7[1];
          ppcVar7[1] = (char *)ppcVar10;
          ppcVar5 = ppcVar7;
        }
        *(char *)(ppcVar10 + 3) = '\0';
        *(char *)(ppcVar6 + 3) = '\0';
        if ((int)*(char *)(ppcVar5 + 3) == uVar9) {
          *(char *)(ppcVar10 + 3) = -cVar8;
        }
        else if (-uVar9 == (int)*(char *)(ppcVar5 + 3)) {
          *(char *)(ppcVar6 + 3) = cVar8;
        }
        *(char *)(ppcVar5 + 3) = '\0';
        ppcVar6 = ppcVar5;
      }
      if (ppcVar10 != (char **)param_1[2]) {
        param_1[1] = (char *)ppcVar6;
        return ppcVar3;
      }
      param_1[2] = (char *)ppcVar6;
      return ppcVar3;
    }
  } while (ppcVar7 != (char **)0x0);
LAB_0010308e:
                    /* WARNING: Subroutine does not return */
  __assert_fail("str && p && p->str && !STREQ (str, p->str)","src/tsort.c",0xb1,"search_item");
}