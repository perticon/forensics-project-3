struct Eq_404 * search_item(Eq_32 rsi, struct Eq_404 * rdi)
{
	struct Eq_404 * rbp_22 = rdi;
	struct Eq_404 * r12_222 = rdi->ptr0010;
	if (r12_222 == null)
	{
		struct Eq_404 * rax_33 = xzalloc((struct <anonymous> *) 0x38);
		r12_222 = rax_33;
		if (rsi != 0x00)
			rax_33->t0000 = (<anonymous>) xstrdup(rsi);
		rdi->ptr0010 = rax_33;
		return r12_222;
	}
	else
	{
		if (rsi != 0x00)
		{
			struct Eq_404 * r13_111 = r12_222;
			while (true)
			{
				Eq_32 rsi_62 = r12_222->t0000;
				if (rsi_62 == 0x00)
					break;
				int32 eax_68 = fn0000000000002530(rsi_62, rsi);
				if (eax_68 == 0x00)
					return r12_222;
				struct Eq_404 * rdx_74 = r12_222->ptr0010;
				if (eax_68 < 0x00)
					rdx_74 = r12_222->ptr0008;
				if (rdx_74 == null)
				{
					struct Eq_404 * rax_83 = xzalloc((struct <anonymous> *) 0x38);
					rax_83->t0000 = (<anonymous>) xstrdup(rsi);
					if (eax_68 >= 0x00)
						r12_222->ptr0010 = rax_83;
					else
						r12_222->ptr0008 = rax_83;
					Eq_32 rsi_113 = r13_111->t0000;
					if (rsi_113 != 0x00)
					{
						int32 eax_119 = fn0000000000002530(rsi_113, rsi);
						if (eax_119 != 0x00)
						{
							struct Eq_404 * rax_125 = r13_111->ptr0010;
							if (eax_119 < 0x00)
								rax_125 = r13_111->ptr0008;
							int32 r12d_201 = eax_119 >> 0x1F | 0x01;
							byte r12b_289 = (byte) r12d_201;
							if (rax_125 != rax_83)
							{
								if (rax_125 != null)
								{
									struct Eq_404 * rbx_147 = rax_125;
									do
									{
										Eq_32 rsi_150 = rbx_147->t0000;
										if (rsi_150 == 0x00)
											break;
										int32 eax_156 = fn0000000000002530(rsi_150, rsi);
										if (eax_156 == 0x00)
											break;
										if (eax_156 < 0x00)
										{
											rbx_147->b0018 = ~0x00;
											rbx_147 = rbx_147->ptr0008;
											if (rbx_147 == rax_83)
												goto l0000000000002F39;
										}
										else
										{
											rbx_147->b0018 = 0x01;
											rbx_147 = rbx_147->ptr0010;
											if (rbx_147 == rax_83)
												goto l0000000000002F39;
										}
									} while (rbx_147 != null);
								}
								fn00000000000024B0("search_item", 177, "src/tsort.c", "str && p && p->str && !STREQ (str, p->str)");
							}
l0000000000002F39:
							uint64 rax_195 = (uint64) r13_111->b0018;
							byte al_196 = (byte) rax_195;
							word32 eax_214 = (word32) rax_195;
							if (al_196 != 0x00)
							{
								int32 edx_210 = -r12d_201;
								if ((int32) al_196 != edx_210)
								{
									struct Eq_404 * rax_244;
									if ((int32) rax_125->b0018 != r12d_201)
									{
										rax_244 = rax_125->ptr0008;
										struct Eq_404 * rcx_249 = rax_125->ptr0010;
										if (r12d_201 != ~0x00)
										{
											rax_125->ptr0008 = rax_244->ptr0010;
											struct Eq_404 * rcx_263 = rax_244->ptr0008;
											rax_244->ptr0010 = rax_125;
											r13_111->ptr0010 = rcx_263;
											rax_244->ptr0008 = r13_111;
										}
										else
										{
											rax_125->ptr0010 = rcx_249->ptr0008;
											struct Eq_404 * rax_255 = rcx_249->ptr0010;
											rcx_249->ptr0008 = rax_125;
											r13_111->ptr0008 = rax_255;
											rcx_249->ptr0010 = r13_111;
											rax_244 = rcx_249;
										}
										r13_111->b0018 = 0x00;
										rax_125->b0018 = 0x00;
										int32 ecx_274 = (int32) rax_244->b0018;
										if (ecx_274 != r12d_201)
										{
											if (edx_210 == ecx_274)
												rax_125->b0018 = r12b_289;
										}
										else
											r13_111->b0018 = (byte) -r12d_201;
										rax_244->b0018 = 0x00;
									}
									else
									{
										if (r12d_201 != ~0x00)
										{
											r13_111->ptr0010 = rax_125->ptr0008;
											rax_125->ptr0008 = r13_111;
										}
										else
										{
											r13_111->ptr0008 = rax_125->ptr0010;
											rax_125->ptr0010 = r13_111;
										}
										rax_125->b0018 = 0x00;
										r13_111->b0018 = 0x00;
										rax_244 = rax_125;
									}
									if (r13_111 != rbp_22->ptr0010)
									{
										rbp_22->ptr0008 = rax_244;
										r12_222 = rax_83;
									}
									else
									{
										rbp_22->ptr0010 = rax_244;
										r12_222 = rax_83;
									}
									return r12_222;
								}
							}
							r13_111->b0018 = (byte) (eax_214 + r12d_201);
							r12_222 = rax_83;
							return r12_222;
						}
					}
					fn00000000000024B0("search_item", 0xA3, "src/tsort.c", "str && s && s->str && !STREQ (str, s->str)");
				}
				if (rdx_74->b0018 != 0x00)
				{
					r13_111 = rdx_74;
					rbp_22 = r12_222;
				}
				r12_222 = rdx_74;
			}
		}
		fn00000000000024B0("search_item", 0x8C, "src/tsort.c", "str && p && p->str");
	}
}