int32_t * search_item(int32_t * root, char * str) {
    int64_t v1 = (int64_t)root;
    int64_t * v2 = (int64_t *)(v1 + 16); // 0x2e14
    int64_t v3 = *v2; // 0x2e14
    if (v3 == 0) {
        char * v4 = xzalloc(56); // 0x2fff
        if (str != NULL) {
            // 0x300c
            *(int64_t *)v4 = (int64_t)xstrdup(str);
        }
        int64_t v5 = (int64_t)v4; // 0x2fff
        *v2 = v5;
        // 0x2e6a
        return (int32_t *)v5;
    }
    if (str == NULL) {
      lab_0x30ad:
        // 0x30ad
        function_24b0();
        // 0x30cc
        return (int32_t *)function_24b0();
    }
    int64_t v6 = v3; // 0x2e56
    int64_t v7 = v1; // 0x2e56
    if (*(int64_t *)v3 == 0) {
        // 0x30ad
        function_24b0();
        // 0x30cc
        return (int32_t *)function_24b0();
    }
    int64_t v8 = v3;
    int32_t v9 = function_2530(); // 0x2e66
    int64_t v10 = v8; // 0x2e68
    int64_t v11; // 0x2e00
    int64_t v12; // 0x2e00
    int64_t v13; // 0x2e85
    while (v9 != 0) {
        // 0x2e30
        v12 = v7;
        v11 = v6;
        int64_t * v14 = (int64_t *)(v8 + 16);
        int64_t * v15 = (int64_t *)(v8 + 8);
        int64_t v16 = v9 < 0 ? *v15 : *v14; // 0x2e35
        if (v16 == 0) {
            char * v17 = xzalloc(56); // 0x2e85
            v13 = (int64_t)v17;
            *(int64_t *)v17 = (int64_t)xstrdup(str);
            if (v9 < 0) {
                // 0x2ff0
                *v15 = v13;
                goto lab_0x2ea5;
            } else {
                // 0x2ea0
                *v14 = v13;
                goto lab_0x2ea5;
            }
        }
        char v18 = *(char *)(v16 + 24); // 0x2e40
        v6 = v18 != 0 ? v16 : v11;
        v7 = v18 != 0 ? v8 : v12;
        if (*(int64_t *)v16 == 0) {
            // 0x30ad
            function_24b0();
            // 0x30cc
            return (int32_t *)function_24b0();
        }
        v8 = v16;
        v9 = function_2530();
        v10 = v8;
    }
  lab_0x2e6a:
    // 0x2e6a
    return (int32_t *)v10;
  lab_0x2ea5:;
    // 0x2ea5
    int64_t v19; // 0x2e00
    int32_t v20; // 0x2ebd
    int64_t * v21; // 0x2ec5
    int64_t * v22; // 0x2ec9
    int64_t v23; // 0x2ec9
    if (*(int64_t *)v11 == 0) {
        return (int32_t *)function_24b0();
    } else {
        // 0x2eb2
        v20 = function_2530();
        if (v20 == 0) {
            return (int32_t *)function_24b0();
        } else {
            // 0x2ec5
            v21 = (int64_t *)(v11 + 16);
            v22 = (int64_t *)(v11 + 8);
            v23 = v20 < 0 ? *v22 : *v21;
            if (v23 == v13) {
                goto lab_0x2f39;
            } else {
                // 0x2ee0
                v19 = v23;
                if (v23 == 0) {
                    // 0x308e
                    function_24b0();
                    goto lab_0x30ad;
                } else {
                    goto lab_0x2f0e;
                }
            }
        }
    }
  lab_0x2f39:;
    int32_t v24 = v20 >> 31 | 1; // 0x2ed2
    char * v25 = (char *)(v11 + 24); // 0x2f39
    char v26 = *v25; // 0x2f39
    int64_t v27; // 0x2e00
    int32_t v28; // 0x2f4c
    char * v29; // 0x2f60
    if (v26 == 0) {
        // 0x2fe0
        *v25 = v26 + (char)v24;
        v10 = v13;
        return (int32_t *)v10;
    } else {
        // 0x2f46
        v28 = -v24;
        if ((int32_t)v26 == v28) {
            // 0x2fe0
            *v25 = v26 + (char)v24;
            v10 = v13;
            return (int32_t *)v10;
        } else {
            // 0x2f56
            v29 = (char *)(v23 + 24);
            if (v24 == (int32_t)*v29) {
                if (v24 == -1) {
                    int64_t * v30 = (int64_t *)(v23 + 16); // 0x304f
                    *v22 = *v30;
                    *v30 = v11;
                    goto lab_0x3033;
                } else {
                    int64_t * v31 = (int64_t *)(v23 + 8); // 0x3027
                    *v21 = *v31;
                    *v31 = v11;
                    goto lab_0x3033;
                }
            } else {
                if (v24 == -1) {
                    int64_t * v32 = (int64_t *)(v23 + 16); // 0x2f71
                    int64_t v33 = *v32; // 0x2f71
                    int64_t * v34 = (int64_t *)(v33 + 8); // 0x3069
                    *v32 = *v34;
                    int64_t * v35 = (int64_t *)(v33 + 16); // 0x3076
                    *v34 = v23;
                    *v22 = *v35;
                    *v35 = v11;
                    v27 = v33;
                    goto lab_0x2f9c;
                } else {
                    int64_t * v36 = (int64_t *)(v23 + 8); // 0x2f6d
                    int64_t v37 = *v36; // 0x2f6d
                    int64_t * v38 = (int64_t *)(v37 + 16); // 0x2f7f
                    *v36 = *v38;
                    int64_t * v39 = (int64_t *)(v37 + 8); // 0x2f8c
                    *v38 = v23;
                    *v21 = *v39;
                    *v39 = v11;
                    v27 = v37;
                    goto lab_0x2f9c;
                }
            }
        }
    }
  lab_0x2f0e:;
    int64_t v40 = v19;
    int64_t v41; // 0x2e00
    if (*(int64_t *)v40 == 0) {
        // 0x308e
        function_24b0();
        goto lab_0x30ad;
    } else {
        int32_t v42 = function_2530(); // 0x2f22
        if (v42 == 0) {
            // 0x308e
            function_24b0();
            goto lab_0x30ad;
        } else {
            char * v43 = (char *)(v40 + 24);
            if (v42 >= 0) {
                // 0x2ef8
                *v43 = 1;
                int64_t v44 = *(int64_t *)(v40 + 16); // 0x2efc
                v41 = v44;
                if (v44 == v13) {
                    goto lab_0x2f39;
                } else {
                    goto lab_0x2f05;
                }
            } else {
                // 0x2f2c
                *v43 = -1;
                int64_t v45 = *(int64_t *)(v40 + 8); // 0x2f30
                v41 = v45;
                if (v45 != v13) {
                    goto lab_0x2f05;
                } else {
                    goto lab_0x2f39;
                }
            }
        }
    }
  lab_0x3033:
    // 0x3033
    *v29 = 0;
    *v25 = 0;
    int64_t v46 = v23; // 0x3041
    goto lab_0x2fc8;
  lab_0x2f9c:
    // 0x2f9c
    *v25 = 0;
    *v29 = 0;
    char * v48 = (char *)(v27 + 24); // 0x2faa
    int32_t v49 = (int32_t)*v48; // 0x2fae
    if (v24 == v49) {
        // 0x305d
        *v25 = -(char)v24;
        // 0x2fc4
        *v48 = 0;
        v46 = v27;
        goto lab_0x2fc8;
    } else {
        if (v28 != v49) {
            // 0x2fc4
            *v48 = 0;
            v46 = v27;
            goto lab_0x2fc8;
        } else {
            // 0x2fbb
            *v29 = (char)v24;
            // 0x2fc4
            *v48 = 0;
            v46 = v27;
            goto lab_0x2fc8;
        }
    }
  lab_0x2f05:
    // 0x2f05
    v19 = v41;
    if (v41 == 0) {
        // 0x308e
        function_24b0();
        goto lab_0x30ad;
    } else {
        goto lab_0x2f0e;
    }
  lab_0x2fc8:;
    int64_t * v47 = (int64_t *)(v12 + 16); // 0x2fc8
    if (v11 == *v47) {
        // 0x3043
        *v47 = v46;
        v10 = v13;
        goto lab_0x2e6a;
    } else {
        // 0x2fce
        *(int64_t *)(v12 + 8) = v46;
        v10 = v13;
        goto lab_0x2e6a;
    }
}