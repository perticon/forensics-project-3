void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002640(fn0000000000002420(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002AFE;
	}
	fn00000000000025D0(fn0000000000002420(0x05, "Usage: %s [OPTION] [FILE]\nWrite totally ordered list consistent with the partial ordering in FILE.\n", null), 0x01);
	fn0000000000002510(stdout, fn0000000000002420(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002510(stdout, fn0000000000002420(0x05, "\n", null));
	fn0000000000002510(stdout, fn0000000000002420(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002510(stdout, fn0000000000002420(0x05, "      --version     output version information and exit\n", null));
	struct Eq_793 * rbx_152 = fp - 0xB8 + 16;
	do
	{
		Eq_32 rsi_154 = rbx_152->qw0000;
		++rbx_152;
	} while (rsi_154 != 0x00 && fn0000000000002530(rsi_154, 0xA004) != 0x00);
	ptr64 r13_167 = rbx_152->qw0008;
	if (r13_167 != 0x00)
	{
		fn00000000000025D0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_254 = fn00000000000025C0(null, 0x05);
		if (rax_254 == 0x00 || fn00000000000023A0(0x03, "en_", rax_254) == 0x00)
			goto l0000000000002CD6;
	}
	else
	{
		fn00000000000025D0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_196 = fn00000000000025C0(null, 0x05);
		if (rax_196 == 0x00 || fn00000000000023A0(0x03, "en_", rax_196) == 0x00)
		{
			fn00000000000025D0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002D13:
			fn00000000000025D0(fn0000000000002420(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002AFE:
			fn0000000000002620(edi);
		}
		r13_167 = 0xA004;
	}
	fn0000000000002510(stdout, fn0000000000002420(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002CD6:
	fn00000000000025D0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002D13;
}