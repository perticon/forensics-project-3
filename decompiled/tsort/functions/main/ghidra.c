void main(int param_1,undefined8 *param_2)

{
  long lVar1;
  undefined8 uVar2;
  undefined *puVar3;
  undefined8 uVar4;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  uVar4 = 0x102710;
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,"tsort","GNU coreutils",Version,1,usage,"Mark Kettenis",0,uVar4);
  lVar1 = (long)optind;
  if (param_1 - optind < 2) {
    puVar3 = &DAT_0010a103;
    if (optind != param_1) {
      puVar3 = (undefined *)param_2[lVar1];
    }
    lVar1 = tsort(puVar3);
  }
  uVar4 = quote(param_2[lVar1 + 1]);
  uVar2 = dcgettext(0,"extra operand %s",5);
  error(0,0,uVar2,uVar4);
                    /* WARNING: Subroutine does not return */
  usage(1);
}