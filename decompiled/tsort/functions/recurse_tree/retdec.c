int64_t recurse_tree(int64_t a1, int64_t a2) {
    int64_t v1 = a1;
    int64_t v2 = *(int64_t *)(v1 + 8); // 0x28bc
    char v3; // 0x28b0
    int64_t v4; // 0x28b0
    int64_t v5; // 0x28b0
    int64_t v6; // 0x28c8
    char v7; // 0x28b0
    if (v2 == 0) {
        // 0x2900
        if (*(int64_t *)(v1 + 16) == 0) {
            // break -> 0x28e3
            break;
        }
        // 0x2900
        v3 = v4;
        v5 = v4;
    } else {
        // 0x28c5
        v6 = recurse_tree(v2, a2);
        v7 = v6;
        v3 = v7;
        v5 = v6;
        if (v7 != 0) {
            // break -> 0x28e3
            break;
        }
    }
    int64_t result = 1; // 0x28d8
    while (v3 == 0) {
        int64_t v8 = *(int64_t *)(v1 + 16); // 0x28da
        v4 = v5;
        result = v5;
        if (v8 == 0) {
            // break -> 0x28e3
            break;
        }
        v1 = v8;
        v2 = *(int64_t *)(v1 + 8);
        if (v2 == 0) {
            // 0x2900
            result = a2;
            if (*(int64_t *)(v1 + 16) == 0) {
                // break -> 0x28e3
                break;
            }
            // 0x2900
            v3 = v4;
            v5 = v4;
        } else {
            // 0x28c5
            v6 = recurse_tree(v2, a2);
            v7 = v6;
            v3 = v7;
            v5 = v6;
            result = 1;
            if (v7 != 0) {
                // break -> 0x28e3
                break;
            }
        }
        // 0x28d1
        result = 1;
    }
    // 0x28e3
    return result;
}