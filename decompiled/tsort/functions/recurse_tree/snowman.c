signed char recurse_tree(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx) {
    struct s0* rbp5;
    struct s0* rbx6;
    struct s0* rdi7;
    signed char al8;
    signed char al9;

    rbp5 = rdi;
    rbx6 = rsi;
    do {
        rdi7 = rbp5->f8;
        if (!rdi7) {
            if (!rbp5->f10) 
                break;
        } else {
            rsi = rbx6;
            al8 = recurse_tree(rdi7, rsi, rdx, rcx);
            if (al8) 
                goto addr_28f0_5;
        }
        al9 = reinterpret_cast<signed char>(rbx6(rbp5, rsi));
        if (al9) 
            goto addr_28f0_5;
        rbp5 = rbp5->f10;
    } while (rbp5);
    goto addr_28e3_8;
    goto rbx6;
    addr_28f0_5:
    return 1;
    addr_28e3_8:
    return al9;
}