uint64_t recurse_tree (int64_t arg_8h, uint32_t arg_10h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    do {
        rbx = rsi;
label_0:
        rdi = *((rbp + 8));
        if (rdi == 0) {
            goto label_2;
        }
        rsi = rbx;
        al = recurse_tree ();
    } while (1);
    if (al != 0) {
        goto label_3;
    }
label_1:
    rdi = rbp;
    al = void (*rbx)() ();
    if (al != 0) {
        goto label_3;
    }
    rbp = *((rbp + 0x10));
    if (rbp != 0) {
        goto label_0;
    }
    return al;
label_3:
    eax = 1;
    return eax;
label_2:
    if (*((rbp + 0x10)) != 0) {
        goto label_1;
    }
    rdi = rbp;
    rax = rbx;
    return void (*rax)() ();
}