undefined8 detect_loop(undefined8 *param_1)

{
  undefined8 *puVar1;
  long *plVar2;
  undefined8 *puVar3;
  long **pplVar4;
  
  if (param_1[4] == 0) {
    return 0;
  }
  if (loop == (undefined8 *)0x0) {
    loop = param_1;
    return 0;
  }
  plVar2 = (long *)param_1[6];
  pplVar4 = (long **)(param_1 + 6);
  if (plVar2 != (long *)0x0) {
    while (loop != (undefined8 *)*plVar2) {
      pplVar4 = (long **)(plVar2 + 1);
      plVar2 = (long *)plVar2[1];
      if (plVar2 == (long *)0x0) {
        return 0;
      }
    }
    if (param_1[5] != 0) {
      do {
        puVar1 = (undefined8 *)loop[5];
        error(0,0,"%s",*loop);
        puVar3 = loop;
        if (param_1 == loop) {
          plVar2 = *pplVar4;
          *(long *)(*plVar2 + 0x20) = *(long *)(*plVar2 + 0x20) + -1;
          *pplVar4 = (long *)plVar2[1];
          free(plVar2);
          do {
            puVar1 = (undefined8 *)puVar3[5];
            puVar3[5] = 0;
            puVar3 = puVar1;
          } while (puVar1 != (undefined8 *)0x0);
          loop = (undefined8 *)0x0;
          return 1;
        }
        loop[5] = 0;
        loop = puVar1;
      } while (puVar1 != (undefined8 *)0x0);
      return 1;
    }
    param_1[5] = loop;
    loop = param_1;
  }
  return 0;
}