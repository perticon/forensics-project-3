int64_t detect_loop (uint32_t arg_28h, uint32_t arg1) {
    rdi = arg1;
    r8d = 0;
    if (*((rdi + 0x20)) == 0) {
        goto label_0;
    }
    rbx = loop;
    if (rbx == 0) {
        goto label_1;
    }
    rax = *((rdi + 0x30));
    r13 = rdi + 0x30;
    if (rax != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        r13 = rax + 8;
        rax = *((rax + 8));
        if (rax == 0) {
            goto label_4;
        }
label_2:
    } while (rbx != *(rax));
    if (*((rbp + 0x28)) == 0) {
        goto label_5;
    }
    r14 = 0x0000a3df;
    while (rbp != r12) {
        *((r12 + 0x28)) = 0;
        *(obj.loop) = rbx;
        if (rbx == 0) {
            goto label_6;
        }
        rax = rbx;
        rcx = *(rax);
        eax = 0;
        rbx = *((rbx + 0x28));
        error (0, 0, r14);
        r12 = loop;
    }
    rax = *(rdi);
    *((rax + 0x20))--;
    rax = *((rdi + 8));
    *(r13) = rax;
    free (*(r13));
    do {
        rax = r12;
        r12 = *((r12 + 0x28));
        *((rax + 0x28)) = 0;
    } while (r12 != 0);
    *(obj.loop) = 0;
label_6:
    r8d = 1;
    do {
label_3:
        eax = r8d;
        return rax;
label_0:
        eax = r8d;
        return rax;
label_1:
        eax = r8d;
        *(obj.loop) = rdi;
        return rax;
label_4:
        r8d = 0;
        eax = r8d;
        return rax;
label_5:
        *((rbp + 0x28)) = rbx;
        r8d = 0;
        *(obj.loop) = rbp;
    } while (1);
}