byte detect_loop(struct Eq_404 * rdi)
{
	word32 r8d_10 = 0x00;
	if (rdi->qw0020 == 0x00)
		return 0x00;
	struct Eq_404 * rbx_105 = loop;
	if (rbx_105 == null)
	{
		loop = rdi;
		return 0x00;
	}
	else
	{
		struct Eq_653 * rax_46 = rdi->ptr0030;
		word64 * r13_116 = &rdi->ptr0030;
		if (rax_46 == null)
			return (byte) r8d_10;
		do
		{
			if (rbx_105 == rax_46->ptr0000)
			{
				if (rdi->ptr0028 != null)
				{
					do
					{
						rbx_105 = rbx_105->ptr0028;
						fn00000000000025E0(41951, 0x00, 0x00);
						struct Eq_404 * r12_106 = loop;
						if (rdi == r12_106)
						{
							struct Eq_17 * rdi_117 = *r13_116;
							Eq_710 rax_118 = rdi_117->t0000;
							--*((word128) rax_118 + 32);
							*r13_116 = rdi_117->ptr0008;
							fn0000000000002370(rdi_117);
							do
							{
								r12_106 = r12_106->ptr0028;
								r12_106->ptr0028 = null;
							} while (r12_106 != null);
							loop = null;
							break;
						}
						r12_106->ptr0028 = null;
						loop = rbx_105;
					} while (rbx_105 != null);
					r8d_10 = 0x01;
				}
				else
				{
					rdi->ptr0028 = rbx_105;
					loop = rdi;
					r8d_10 = 0x00;
				}
				return (byte) r8d_10;
			}
			r13_116 = &rax_46->ptr0008;
			rax_46 = rax_46->ptr0008;
		} while (rax_46 != null);
		return 0x00;
	}
}