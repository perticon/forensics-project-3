void tsort(byte *param_1)

{
  long lVar1;
  long lVar2;
  long lVar3;
  long lVar4;
  long lVar5;
  bool bVar6;
  char **ppcVar7;
  char **ppcVar8;
  char cVar9;
  int iVar10;
  long lVar11;
  long lVar12;
  undefined8 uVar13;
  char *pcVar14;
  char **ppcVar15;
  undefined8 uVar16;
  int *piVar17;
  char **ppcVar18;
  char **ppcVar19;
  char **ppcVar20;
  char *pcVar21;
  char **in_R8;
  char **ppcVar22;
  char **unaff_R14;
  long in_FS_OFFSET;
  byte local_80;
  uint local_64;
  undefined local_58 [8];
  undefined8 local_50;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  local_64 = *param_1 - 0x2d;
  if (local_64 == 0) {
    local_64 = (uint)param_1[1];
  }
  lVar11 = xzalloc(0x38);
  if ((local_64 != 0) && (lVar12 = freopen_safer(param_1,"r",stdin), lVar12 == 0))
  goto LAB_00105cd5;
  ppcVar22 = (char **)&DAT_0010a0cc;
  fadvise(stdin,2);
  init_tokenbuffer(local_58);
  do {
    ppcVar15 = (char **)0x0;
    do {
      ppcVar19 = ppcVar15;
      lVar12 = readtoken(stdin,&DAT_0010a0cc,3,local_58);
      if (lVar12 == -1) {
        if (ppcVar19 != (char **)0x0) {
          ppcVar15 = (char **)quotearg_n_style_colon(0,3,param_1);
          uVar16 = dcgettext(0,"%s: input contains an odd number of tokens",5);
          ppcVar19 = ppcVar15;
          error(1,0,uVar16);
          goto LAB_00104ea7;
        }
        if (*(long *)(lVar11 + 0x10) != 0) {
          recurse_tree();
        }
        local_80 = 1;
        goto LAB_001031f4;
      }
      if (lVar12 == 0) {
                    /* WARNING: Subroutine does not return */
        __assert_fail("len != 0","src/tsort.c",0x1ca,"tsort");
      }
      unaff_R14 = (char **)search_item(lVar11,local_50);
      ppcVar15 = unaff_R14;
    } while (ppcVar19 == (char **)0x0);
    iVar10 = strcmp(*ppcVar19,*unaff_R14);
    if (iVar10 != 0) {
      unaff_R14[4] = unaff_R14[4] + 1;
      ppcVar15 = (char **)xmalloc(0x10);
      pcVar14 = ppcVar19[6];
      *ppcVar15 = (char *)unaff_R14;
      ppcVar15[1] = pcVar14;
      ppcVar19[6] = (char *)ppcVar15;
    }
  } while( true );
LAB_001031f4:
  if (n_strings == 0) {
LAB_001033a9:
    iVar10 = rpl_fclose();
    if (iVar10 == 0) {
                    /* WARNING: Subroutine does not return */
      exit((uint)(local_80 ^ 1));
    }
    if (local_64 != 0) goto LAB_00105d04;
    uVar16 = dcgettext(0,"standard input",5);
    do {
      piVar17 = __errno_location();
      error(1,*piVar17,"%s",uVar16);
LAB_00105cd5:
      uVar16 = quotearg_n_style_colon(0,3,param_1);
      piVar17 = __errno_location();
      error(1,*piVar17,"%s",uVar16);
LAB_00105d04:
      uVar16 = quotearg_n_style_colon(0,3,param_1);
    } while( true );
  }
  ppcVar15 = *(char ***)(lVar11 + 0x10);
  ppcVar22 = zeros;
  ppcVar19 = head;
  if (ppcVar15 == (char **)0x0) {
LAB_001033d7:
    head = ppcVar19;
    zeros = ppcVar22;
    if (head != (char **)0x0) goto LAB_001033f0;
  }
  else {
    unaff_R14 = (char **)ppcVar15[1];
    if (unaff_R14 == (char **)0x0) {
      ppcVar20 = (char **)ppcVar15[2];
      if (ppcVar20 == (char **)0x0) {
        if (((ppcVar15[4] == (char *)0x0) && (*(char *)((long)ppcVar15 + 0x19) == '\0')) &&
           (ppcVar22 = ppcVar15, ppcVar19 = ppcVar15, head != (char **)0x0)) {
          zeros[5] = (char *)ppcVar15;
          ppcVar19 = head;
        }
        goto LAB_001033d7;
      }
      if (ppcVar15[4] == (char *)0x0) goto LAB_00103357;
LAB_0010348e:
      ppcVar22 = (char **)ppcVar20[1];
      if (ppcVar22 == (char **)0x0) {
        ppcVar22 = (char **)ppcVar20[2];
        pcVar14 = ppcVar20[4];
        if (ppcVar22 != (char **)0x0) {
          if (pcVar14 == (char *)0x0) goto LAB_00103af1;
          goto LAB_00103b21;
        }
LAB_00104aaf:
        lVar12 = n_strings;
        ppcVar7 = zeros;
        ppcVar8 = head;
        if (((pcVar14 == (char *)0x0) && (*(char *)((long)ppcVar20 + 0x19) == '\0')) &&
           (ppcVar7 = ppcVar20, ppcVar8 = ppcVar20, head != (char **)0x0)) {
          zeros[5] = (char *)ppcVar20;
          lVar12 = n_strings;
          ppcVar8 = head;
        }
      }
      else {
        ppcVar15 = (char **)ppcVar22[1];
        if (ppcVar15 == (char **)0x0) {
          ppcVar15 = (char **)ppcVar22[2];
          pcVar14 = ppcVar22[4];
          if (ppcVar15 != (char **)0x0) {
            if (pcVar14 == (char *)0x0) goto LAB_001041fd;
            goto LAB_00103db3;
          }
LAB_00104d95:
          ppcVar19 = zeros;
          ppcVar18 = head;
          if (((pcVar14 == (char *)0x0) && (*(char *)((long)ppcVar22 + 0x19) == '\0')) &&
             (ppcVar19 = ppcVar22, ppcVar18 = ppcVar22, head != (char **)0x0)) {
            zeros[5] = (char *)ppcVar22;
            ppcVar18 = head;
          }
        }
        else {
          ppcVar19 = (char **)ppcVar15[1];
          if (ppcVar19 == (char **)0x0) {
            ppcVar19 = (char **)ppcVar15[2];
            if (ppcVar19 != (char **)0x0) {
              if (ppcVar15[4] == (char *)0x0) goto LAB_00104734;
              goto LAB_00104672;
            }
            ppcVar18 = zeros;
            ppcVar8 = head;
            if (((ppcVar15[4] == (char *)0x0) && (*(char *)((long)ppcVar15 + 0x19) == '\0')) &&
               (ppcVar18 = ppcVar15, ppcVar8 = ppcVar15, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar15;
              ppcVar8 = head;
            }
          }
          else {
            ppcVar18 = (char **)ppcVar19[1];
            if (ppcVar18 == (char **)0x0) {
              ppcVar18 = (char **)ppcVar19[2];
              if (ppcVar18 != (char **)0x0) {
                if (ppcVar19[4] == (char *)0x0) goto LAB_0010353d;
                goto LAB_00104174;
              }
              ppcVar7 = zeros;
              ppcVar8 = head;
              if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0')) &&
                 (ppcVar7 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
                zeros[5] = (char *)ppcVar19;
                ppcVar8 = head;
              }
            }
            else {
              if (ppcVar18[1] == (char *)0x0) {
                pcVar14 = ppcVar18[2];
                if (pcVar14 != (char *)0x0) {
                  if (ppcVar18[4] == (char *)0x0) goto LAB_00103501;
                  goto LAB_0010538a;
                }
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (((ppcVar18[4] == (char *)0x0) && (*(char *)((long)ppcVar18 + 0x19) == '\0')) &&
                   (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar18;
                  ppcVar8 = head;
                }
              }
              else {
                cVar9 = recurse_tree();
                lVar12 = n_strings;
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (cVar9 != '\0') goto joined_r0x00103392;
                pcVar14 = ppcVar18[2];
                if (ppcVar18[4] == (char *)0x0) {
LAB_00103501:
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if ((*(char *)((long)ppcVar18 + 0x19) == '\0') &&
                     (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar18;
                    ppcVar8 = head;
                  }
                }
                head = ppcVar8;
                zeros = ppcVar7;
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (pcVar14 != (char *)0x0) {
LAB_0010538a:
                  cVar9 = recurse_tree();
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  lVar12 = n_strings;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                }
              }
              head = ppcVar8;
              zeros = ppcVar7;
              ppcVar18 = (char **)ppcVar19[2];
              ppcVar7 = zeros;
              ppcVar8 = head;
              if (ppcVar19[4] == (char *)0x0) {
LAB_0010353d:
                ppcVar7 = zeros;
                ppcVar8 = head;
                if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                   (ppcVar7 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar19;
                  ppcVar8 = head;
                }
              }
              head = ppcVar8;
              zeros = ppcVar7;
              ppcVar7 = zeros;
              ppcVar8 = head;
              if (ppcVar18 != (char **)0x0) {
LAB_00104174:
                if (ppcVar18[1] == (char *)0x0) {
                  pcVar14 = ppcVar18[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar18[4] == (char *)0x0) goto LAB_001041a9;
                    goto LAB_0010513c;
                  }
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (((ppcVar18[4] == (char *)0x0) && (*(char *)((long)ppcVar18 + 0x19) == '\0'))
                     && (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar18;
                    ppcVar8 = head;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar18[2];
                  ppcVar19 = zeros;
                  ppcVar7 = head;
                  if (ppcVar18[4] == (char *)0x0) {
LAB_001041a9:
                    ppcVar19 = zeros;
                    ppcVar7 = head;
                    if ((*(char *)((long)ppcVar18 + 0x19) == '\0') &&
                       (ppcVar19 = ppcVar18, ppcVar7 = ppcVar18, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar18;
                      ppcVar7 = head;
                    }
                  }
                  head = ppcVar7;
                  zeros = ppcVar19;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_0010513c:
                    cVar9 = recurse_tree();
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    lVar12 = n_strings;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
              }
            }
            head = ppcVar8;
            zeros = ppcVar7;
            ppcVar19 = (char **)ppcVar15[2];
            ppcVar18 = zeros;
            ppcVar7 = head;
            if (ppcVar15[4] == (char *)0x0) {
LAB_00104734:
              ppcVar18 = zeros;
              ppcVar7 = head;
              if ((*(char *)((long)ppcVar15 + 0x19) == '\0') &&
                 (ppcVar18 = ppcVar15, ppcVar7 = ppcVar15, head != (char **)0x0)) {
                zeros[5] = (char *)ppcVar15;
                ppcVar7 = head;
              }
            }
            head = ppcVar7;
            zeros = ppcVar18;
            ppcVar18 = zeros;
            ppcVar8 = head;
            if (ppcVar19 != (char **)0x0) {
LAB_00104672:
              ppcVar15 = (char **)ppcVar19[1];
              if (ppcVar15 == (char **)0x0) {
                pcVar14 = ppcVar19[2];
                if (pcVar14 != (char *)0x0) {
                  if (ppcVar19[4] == (char *)0x0) goto LAB_001046ee;
                  goto LAB_0010471f;
                }
                ppcVar18 = zeros;
                ppcVar8 = head;
                if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0')) &&
                   (ppcVar18 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar19;
                  ppcVar8 = head;
                }
              }
              else {
                if (ppcVar15[1] == (char *)0x0) {
                  pcVar14 = ppcVar15[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar15[4] == (char *)0x0) goto LAB_001046b2;
                    goto LAB_001057b9;
                  }
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (((ppcVar15[4] == (char *)0x0) && (*(char *)((long)ppcVar15 + 0x19) == '\0'))
                     && (ppcVar18 = ppcVar15, ppcVar8 = ppcVar15, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar15;
                    ppcVar8 = head;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar15[2];
                  ppcVar18 = zeros;
                  ppcVar7 = head;
                  if (ppcVar15[4] == (char *)0x0) {
LAB_001046b2:
                    ppcVar18 = zeros;
                    ppcVar7 = head;
                    if ((*(char *)((long)ppcVar15 + 0x19) == '\0') &&
                       (ppcVar18 = ppcVar15, ppcVar7 = ppcVar15, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar15;
                      ppcVar7 = head;
                    }
                  }
                  head = ppcVar7;
                  zeros = ppcVar18;
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_001057b9:
                    cVar9 = recurse_tree();
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
                head = ppcVar8;
                zeros = ppcVar18;
                pcVar14 = ppcVar19[2];
                ppcVar15 = zeros;
                ppcVar18 = head;
                if (ppcVar19[4] == (char *)0x0) {
LAB_001046ee:
                  ppcVar15 = zeros;
                  ppcVar18 = head;
                  if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                     (ppcVar15 = ppcVar19, ppcVar18 = ppcVar19, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar19;
                    ppcVar18 = head;
                  }
                }
                head = ppcVar18;
                zeros = ppcVar15;
                ppcVar18 = zeros;
                ppcVar8 = head;
                if (pcVar14 != (char *)0x0) {
LAB_0010471f:
                  cVar9 = recurse_tree();
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                }
              }
            }
          }
          head = ppcVar8;
          zeros = ppcVar18;
          ppcVar15 = (char **)ppcVar22[2];
          ppcVar19 = zeros;
          ppcVar18 = head;
          if (ppcVar22[4] == (char *)0x0) {
LAB_001041fd:
            ppcVar19 = zeros;
            ppcVar18 = head;
            if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
               (ppcVar19 = ppcVar22, ppcVar18 = ppcVar22, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar22;
              ppcVar18 = head;
            }
          }
          head = ppcVar18;
          zeros = ppcVar19;
          ppcVar19 = zeros;
          ppcVar18 = head;
          if (ppcVar15 != (char **)0x0) {
LAB_00103db3:
            ppcVar22 = (char **)ppcVar15[1];
            if (ppcVar22 == (char **)0x0) {
              ppcVar22 = (char **)ppcVar15[2];
              if (ppcVar22 != (char **)0x0) {
                if (ppcVar15[4] == (char *)0x0) goto LAB_00103e77;
                goto LAB_001049ab;
              }
              ppcVar19 = zeros;
              ppcVar18 = head;
              if (((ppcVar15[4] == (char *)0x0) && (*(char *)((long)ppcVar15 + 0x19) == '\0')) &&
                 (ppcVar19 = ppcVar15, ppcVar18 = ppcVar15, head != (char **)0x0)) {
                zeros[5] = (char *)ppcVar15;
                ppcVar18 = head;
              }
            }
            else {
              ppcVar19 = (char **)ppcVar22[1];
              if (ppcVar19 == (char **)0x0) {
                pcVar14 = ppcVar22[2];
                if (pcVar14 != (char *)0x0) {
                  if (ppcVar22[4] == (char *)0x0) goto LAB_00103e3a;
                  goto LAB_00105c86;
                }
                ppcVar19 = zeros;
                ppcVar18 = head;
                if (((ppcVar22[4] == (char *)0x0) && (*(char *)((long)ppcVar22 + 0x19) == '\0')) &&
                   (ppcVar19 = ppcVar22, ppcVar18 = ppcVar22, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar22;
                  ppcVar18 = head;
                }
              }
              else {
                if (ppcVar19[1] == (char *)0x0) {
                  pcVar14 = ppcVar19[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar19[4] == (char *)0x0) goto LAB_00103e00;
                    goto LAB_00105c57;
                  }
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0'))
                     && (ppcVar18 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar19;
                    ppcVar8 = head;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar19[2];
                  ppcVar18 = zeros;
                  ppcVar7 = head;
                  if (ppcVar19[4] == (char *)0x0) {
LAB_00103e00:
                    ppcVar18 = zeros;
                    ppcVar7 = head;
                    if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                       (ppcVar18 = ppcVar19, ppcVar7 = ppcVar19, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar19;
                      ppcVar7 = head;
                    }
                  }
                  head = ppcVar7;
                  zeros = ppcVar18;
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_00105c57:
                    cVar9 = recurse_tree();
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
                head = ppcVar8;
                zeros = ppcVar18;
                pcVar14 = ppcVar22[2];
                ppcVar19 = zeros;
                ppcVar18 = head;
                if (ppcVar22[4] == (char *)0x0) {
LAB_00103e3a:
                  ppcVar19 = zeros;
                  ppcVar18 = head;
                  if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                     (ppcVar19 = ppcVar22, ppcVar18 = ppcVar22, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar22;
                    ppcVar18 = head;
                  }
                }
                head = ppcVar18;
                zeros = ppcVar19;
                ppcVar19 = zeros;
                ppcVar18 = head;
                if (pcVar14 != (char *)0x0) {
LAB_00105c86:
                  cVar9 = recurse_tree();
                  ppcVar19 = zeros;
                  ppcVar18 = head;
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                }
              }
              head = ppcVar18;
              zeros = ppcVar19;
              ppcVar22 = (char **)ppcVar15[2];
              ppcVar19 = zeros;
              ppcVar18 = head;
              if (ppcVar15[4] == (char *)0x0) {
LAB_00103e77:
                ppcVar19 = zeros;
                ppcVar18 = head;
                if ((*(char *)((long)ppcVar15 + 0x19) == '\0') &&
                   (ppcVar19 = ppcVar15, ppcVar18 = ppcVar15, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar15;
                  ppcVar18 = head;
                }
              }
              head = ppcVar18;
              zeros = ppcVar19;
              ppcVar19 = zeros;
              ppcVar18 = head;
              if (ppcVar22 != (char **)0x0) {
LAB_001049ab:
                if (ppcVar22[1] == (char *)0x0) {
                  pcVar21 = ppcVar22[2];
                  pcVar14 = ppcVar22[4];
                  if (pcVar21 == (char *)0x0) goto LAB_00104d95;
                  if (pcVar14 == (char *)0x0) goto LAB_001049d3;
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar21 = ppcVar22[2];
                  ppcVar15 = zeros;
                  ppcVar19 = head;
                  if (ppcVar22[4] == (char *)0x0) {
LAB_001049d3:
                    ppcVar15 = zeros;
                    ppcVar19 = head;
                    if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                       (ppcVar15 = ppcVar22, ppcVar19 = ppcVar22, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar22;
                      ppcVar19 = head;
                    }
                  }
                  head = ppcVar19;
                  zeros = ppcVar15;
                  ppcVar19 = zeros;
                  ppcVar18 = head;
                  if (pcVar21 == (char *)0x0) goto LAB_0010422d;
                }
                cVar9 = recurse_tree();
                ppcVar19 = zeros;
                ppcVar18 = head;
                lVar12 = n_strings;
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (cVar9 != '\0') goto joined_r0x00103392;
              }
            }
          }
        }
LAB_0010422d:
        head = ppcVar18;
        zeros = ppcVar19;
        ppcVar22 = (char **)ppcVar20[2];
        ppcVar15 = zeros;
        ppcVar19 = head;
        if (ppcVar20[4] == (char *)0x0) {
LAB_00103af1:
          ppcVar15 = zeros;
          ppcVar19 = head;
          if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
             (ppcVar15 = ppcVar20, ppcVar19 = ppcVar20, head != (char **)0x0)) {
            zeros[5] = (char *)ppcVar20;
            ppcVar19 = head;
          }
        }
        head = ppcVar19;
        zeros = ppcVar15;
        lVar12 = n_strings;
        ppcVar7 = zeros;
        ppcVar8 = head;
        if (ppcVar22 != (char **)0x0) {
LAB_00103b21:
          ppcVar15 = (char **)ppcVar22[1];
          if (ppcVar15 == (char **)0x0) {
            ppcVar20 = (char **)ppcVar22[2];
            if (ppcVar20 != (char **)0x0) {
              if (ppcVar22[4] == (char *)0x0) goto LAB_00103f75;
              goto LAB_00103fa6;
            }
            lVar12 = n_strings;
            ppcVar7 = zeros;
            ppcVar8 = head;
            if (((ppcVar22[4] == (char *)0x0) && (*(char *)((long)ppcVar22 + 0x19) == '\0')) &&
               (ppcVar7 = ppcVar22, ppcVar8 = ppcVar22, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar22;
              lVar12 = n_strings;
              ppcVar8 = head;
            }
          }
          else {
            ppcVar19 = (char **)ppcVar15[1];
            if (ppcVar19 == (char **)0x0) {
              ppcVar19 = (char **)ppcVar15[2];
              if (ppcVar19 != (char **)0x0) {
                if (ppcVar15[4] == (char *)0x0) goto LAB_00104c75;
                goto LAB_00103f10;
              }
              ppcVar20 = zeros;
              ppcVar18 = head;
              if (((ppcVar15[4] == (char *)0x0) && (*(char *)((long)ppcVar15 + 0x19) == '\0')) &&
                 (ppcVar20 = ppcVar15, ppcVar18 = ppcVar15, head != (char **)0x0)) {
                zeros[5] = (char *)ppcVar15;
                ppcVar18 = head;
              }
            }
            else {
              ppcVar20 = (char **)ppcVar19[1];
              if (ppcVar20 == (char **)0x0) {
                pcVar14 = ppcVar19[2];
                if (pcVar14 != (char *)0x0) {
                  if (ppcVar19[4] == (char *)0x0) goto LAB_00104c3a;
                  goto LAB_00105288;
                }
                ppcVar20 = zeros;
                ppcVar18 = head;
                if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0')) &&
                   (ppcVar20 = ppcVar19, ppcVar18 = ppcVar19, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar19;
                  ppcVar18 = head;
                }
              }
              else {
                if (ppcVar20[1] == (char *)0x0) {
                  pcVar14 = ppcVar20[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar20[4] == (char *)0x0) goto LAB_00103b7b;
                    goto LAB_00104c1f;
                  }
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (((ppcVar20[4] == (char *)0x0) && (*(char *)((long)ppcVar20 + 0x19) == '\0'))
                     && (ppcVar18 = ppcVar20, ppcVar8 = ppcVar20, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar20;
                    ppcVar8 = head;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar20[2];
                  ppcVar18 = zeros;
                  ppcVar7 = head;
                  if (ppcVar20[4] == (char *)0x0) {
LAB_00103b7b:
                    ppcVar18 = zeros;
                    ppcVar7 = head;
                    if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
                       (ppcVar18 = ppcVar20, ppcVar7 = ppcVar20, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar20;
                      ppcVar7 = head;
                    }
                  }
                  head = ppcVar7;
                  zeros = ppcVar18;
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_00104c1f:
                    cVar9 = recurse_tree();
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
                head = ppcVar8;
                zeros = ppcVar18;
                pcVar14 = ppcVar19[2];
                ppcVar20 = zeros;
                ppcVar18 = head;
                if (ppcVar19[4] == (char *)0x0) {
LAB_00104c3a:
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                     (ppcVar20 = ppcVar19, ppcVar18 = ppcVar19, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar19;
                    ppcVar18 = head;
                  }
                }
                head = ppcVar18;
                zeros = ppcVar20;
                ppcVar20 = zeros;
                ppcVar18 = head;
                if (pcVar14 != (char *)0x0) {
LAB_00105288:
                  cVar9 = recurse_tree();
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                }
              }
              head = ppcVar18;
              zeros = ppcVar20;
              ppcVar19 = (char **)ppcVar15[2];
              ppcVar20 = zeros;
              ppcVar18 = head;
              if (ppcVar15[4] == (char *)0x0) {
LAB_00104c75:
                ppcVar20 = zeros;
                ppcVar18 = head;
                if ((*(char *)((long)ppcVar15 + 0x19) == '\0') &&
                   (ppcVar20 = ppcVar15, ppcVar18 = ppcVar15, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar15;
                  ppcVar18 = head;
                }
              }
              head = ppcVar18;
              zeros = ppcVar20;
              ppcVar20 = zeros;
              ppcVar18 = head;
              if (ppcVar19 != (char **)0x0) {
LAB_00103f10:
                if (ppcVar19[1] == (char *)0x0) {
                  pcVar14 = ppcVar19[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar19[4] == (char *)0x0) goto LAB_00103f38;
                    goto LAB_0010501a;
                  }
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0'))
                     && (ppcVar20 = ppcVar19, ppcVar18 = ppcVar19, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar19;
                    ppcVar18 = head;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar19[2];
                  ppcVar15 = zeros;
                  ppcVar20 = head;
                  if (ppcVar19[4] == (char *)0x0) {
LAB_00103f38:
                    ppcVar15 = zeros;
                    ppcVar20 = head;
                    if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                       (ppcVar15 = ppcVar19, ppcVar20 = ppcVar19, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar19;
                      ppcVar20 = head;
                    }
                  }
                  head = ppcVar20;
                  zeros = ppcVar15;
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_0010501a:
                    cVar9 = recurse_tree();
                    ppcVar20 = zeros;
                    ppcVar18 = head;
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
              }
            }
            head = ppcVar18;
            zeros = ppcVar20;
            ppcVar20 = (char **)ppcVar22[2];
            ppcVar15 = zeros;
            ppcVar19 = head;
            if (ppcVar22[4] == (char *)0x0) {
LAB_00103f75:
              ppcVar15 = zeros;
              ppcVar19 = head;
              if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                 (ppcVar15 = ppcVar22, ppcVar19 = ppcVar22, head != (char **)0x0)) {
                zeros[5] = (char *)ppcVar22;
                ppcVar19 = head;
              }
            }
            head = ppcVar19;
            zeros = ppcVar15;
            lVar12 = n_strings;
            ppcVar7 = zeros;
            ppcVar8 = head;
            if (ppcVar20 != (char **)0x0) {
LAB_00103fa6:
              ppcVar22 = (char **)ppcVar20[1];
              if (ppcVar22 == (char **)0x0) {
                pcVar21 = ppcVar20[2];
                pcVar14 = ppcVar20[4];
                if (pcVar21 == (char *)0x0) goto LAB_00104aaf;
                if (pcVar14 == (char *)0x0) goto LAB_0010401a;
              }
              else {
                if (ppcVar22[1] == (char *)0x0) {
                  pcVar14 = ppcVar22[2];
                  if (pcVar14 == (char *)0x0) {
                    ppcVar15 = zeros;
                    ppcVar19 = head;
                    if (((ppcVar22[4] == (char *)0x0) && (*(char *)((long)ppcVar22 + 0x19) == '\0'))
                       && (ppcVar15 = ppcVar22, ppcVar19 = ppcVar22, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar22;
                      ppcVar19 = head;
                    }
                  }
                  else {
                    if (ppcVar22[4] == (char *)0x0) goto LAB_00103fde;
LAB_00105103:
                    cVar9 = recurse_tree();
                    ppcVar15 = zeros;
                    ppcVar19 = head;
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar22[2];
                  ppcVar15 = zeros;
                  ppcVar19 = head;
                  if (ppcVar22[4] == (char *)0x0) {
LAB_00103fde:
                    ppcVar15 = zeros;
                    ppcVar19 = head;
                    if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                       (ppcVar15 = ppcVar22, ppcVar19 = ppcVar22, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar22;
                      ppcVar19 = head;
                    }
                  }
                  head = ppcVar19;
                  zeros = ppcVar15;
                  ppcVar15 = zeros;
                  ppcVar19 = head;
                  if (pcVar14 != (char *)0x0) goto LAB_00105103;
                }
                head = ppcVar19;
                zeros = ppcVar15;
                pcVar21 = ppcVar20[2];
                ppcVar22 = zeros;
                ppcVar15 = head;
                if (ppcVar20[4] == (char *)0x0) {
LAB_0010401a:
                  ppcVar22 = zeros;
                  ppcVar15 = head;
                  if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
                     (ppcVar22 = ppcVar20, ppcVar15 = ppcVar20, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar20;
                    ppcVar15 = head;
                  }
                }
                head = ppcVar15;
                zeros = ppcVar22;
                lVar12 = n_strings;
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (pcVar21 == (char *)0x0) goto joined_r0x00103392;
              }
              recurse_tree();
              lVar12 = n_strings;
              ppcVar7 = zeros;
              ppcVar8 = head;
            }
          }
        }
      }
    }
    else {
      ppcVar22 = (char **)unaff_R14[1];
      if (ppcVar22 == (char **)0x0) {
        ppcVar22 = (char **)unaff_R14[2];
        pcVar14 = unaff_R14[4];
        if (ppcVar22 == (char **)0x0) {
LAB_00104838:
          ppcVar19 = zeros;
          ppcVar20 = head;
          if (((pcVar14 == (char *)0x0) && (*(char *)((long)unaff_R14 + 0x19) == '\0')) &&
             (ppcVar19 = unaff_R14, ppcVar20 = unaff_R14, head != (char **)0x0)) {
            zeros[5] = (char *)unaff_R14;
            ppcVar19 = unaff_R14;
            ppcVar20 = head;
          }
        }
        else {
          if (pcVar14 == (char *)0x0) goto LAB_0010358e;
LAB_001035be:
          ppcVar19 = (char **)ppcVar22[1];
          if (ppcVar19 == (char **)0x0) {
            unaff_R14 = (char **)ppcVar22[2];
            pcVar14 = ppcVar22[4];
            if (unaff_R14 != (char **)0x0) {
              if (pcVar14 == (char *)0x0) goto LAB_00104125;
              goto LAB_00103bd5;
            }
LAB_00104e26:
            ppcVar19 = zeros;
            ppcVar20 = head;
            if (((pcVar14 == (char *)0x0) && (*(char *)((long)ppcVar22 + 0x19) == '\0')) &&
               (ppcVar19 = ppcVar22, ppcVar20 = ppcVar22, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar22;
              ppcVar20 = head;
            }
          }
          else {
            ppcVar20 = (char **)ppcVar19[1];
            if (ppcVar20 == (char **)0x0) {
              ppcVar20 = (char **)ppcVar19[2];
              if (ppcVar20 != (char **)0x0) {
                if (ppcVar19[4] == (char *)0x0) goto LAB_001047f8;
                goto LAB_00104785;
              }
              ppcVar18 = zeros;
              ppcVar8 = head;
              if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0')) &&
                 (ppcVar18 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
                zeros[5] = (char *)ppcVar19;
                ppcVar8 = head;
              }
            }
            else {
              ppcVar18 = (char **)ppcVar20[1];
              if (ppcVar18 == (char **)0x0) {
                ppcVar18 = (char **)ppcVar20[2];
                if (ppcVar18 != (char **)0x0) {
                  if (ppcVar20[4] == (char *)0x0) goto LAB_0010365c;
                  goto LAB_001040a0;
                }
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (((ppcVar20[4] == (char *)0x0) && (*(char *)((long)ppcVar20 + 0x19) == '\0')) &&
                   (ppcVar7 = ppcVar20, ppcVar8 = ppcVar20, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar20;
                  ppcVar8 = head;
                }
              }
              else {
                if (ppcVar18[1] == (char *)0x0) {
                  pcVar14 = ppcVar18[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar18[4] == (char *)0x0) goto LAB_00103621;
                    goto LAB_0010540e;
                  }
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (((ppcVar18[4] == (char *)0x0) && (*(char *)((long)ppcVar18 + 0x19) == '\0'))
                     && (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar18;
                    ppcVar8 = head;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar18[2];
                  if (ppcVar18[4] == (char *)0x0) {
LAB_00103621:
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if ((*(char *)((long)ppcVar18 + 0x19) == '\0') &&
                       (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar18;
                      ppcVar8 = head;
                    }
                  }
                  head = ppcVar8;
                  zeros = ppcVar7;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_0010540e:
                    cVar9 = recurse_tree();
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    lVar12 = n_strings;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
                head = ppcVar8;
                zeros = ppcVar7;
                ppcVar18 = (char **)ppcVar20[2];
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (ppcVar20[4] == (char *)0x0) {
LAB_0010365c:
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
                     (ppcVar7 = ppcVar20, ppcVar8 = ppcVar20, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar20;
                    ppcVar8 = head;
                  }
                }
                head = ppcVar8;
                zeros = ppcVar7;
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (ppcVar18 != (char **)0x0) {
LAB_001040a0:
                  if (ppcVar18[1] == (char *)0x0) {
                    pcVar14 = ppcVar18[2];
                    if (pcVar14 != (char *)0x0) {
                      if (ppcVar18[4] == (char *)0x0) goto LAB_001040d2;
                      goto LAB_00105072;
                    }
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if (((ppcVar18[4] == (char *)0x0) && (*(char *)((long)ppcVar18 + 0x19) == '\0'))
                       && (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar18;
                      ppcVar8 = head;
                    }
                  }
                  else {
                    cVar9 = recurse_tree();
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                    pcVar14 = ppcVar18[2];
                    ppcVar20 = zeros;
                    ppcVar7 = head;
                    if (ppcVar18[4] == (char *)0x0) {
LAB_001040d2:
                      ppcVar20 = zeros;
                      ppcVar7 = head;
                      if ((*(char *)((long)ppcVar18 + 0x19) == '\0') &&
                         (ppcVar20 = ppcVar18, ppcVar7 = ppcVar18, head != (char **)0x0)) {
                        zeros[5] = (char *)ppcVar18;
                        ppcVar7 = head;
                      }
                    }
                    head = ppcVar7;
                    zeros = ppcVar20;
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if (pcVar14 != (char *)0x0) {
LAB_00105072:
                      cVar9 = recurse_tree();
                      ppcVar7 = zeros;
                      ppcVar8 = head;
                      lVar12 = n_strings;
                      if (cVar9 != '\0') goto joined_r0x00103392;
                    }
                  }
                }
              }
              head = ppcVar8;
              zeros = ppcVar7;
              ppcVar20 = (char **)ppcVar19[2];
              ppcVar18 = zeros;
              ppcVar7 = head;
              if (ppcVar19[4] == (char *)0x0) {
LAB_001047f8:
                ppcVar18 = zeros;
                ppcVar7 = head;
                if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                   (ppcVar18 = ppcVar19, ppcVar7 = ppcVar19, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar19;
                  ppcVar7 = head;
                }
              }
              head = ppcVar7;
              zeros = ppcVar18;
              ppcVar18 = zeros;
              ppcVar8 = head;
              if (ppcVar20 != (char **)0x0) {
LAB_00104785:
                ppcVar19 = (char **)ppcVar20[1];
                if (ppcVar19 == (char **)0x0) {
                  pcVar14 = ppcVar20[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar20[4] == (char *)0x0) goto LAB_0010580b;
                    goto LAB_0010583b;
                  }
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (((ppcVar20[4] == (char *)0x0) && (*(char *)((long)ppcVar20 + 0x19) == '\0'))
                     && (ppcVar18 = ppcVar20, ppcVar8 = ppcVar20, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar20;
                    ppcVar8 = head;
                  }
                }
                else {
                  if (ppcVar19[1] == (char *)0x0) {
                    pcVar14 = ppcVar19[2];
                    if (pcVar14 != (char *)0x0) {
                      if (ppcVar19[4] == (char *)0x0) goto LAB_001047c4;
                      goto LAB_001057f0;
                    }
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0'))
                       && (ppcVar18 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar19;
                      ppcVar8 = head;
                    }
                  }
                  else {
                    cVar9 = recurse_tree();
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                    pcVar14 = ppcVar19[2];
                    ppcVar18 = zeros;
                    ppcVar7 = head;
                    if (ppcVar19[4] == (char *)0x0) {
LAB_001047c4:
                      ppcVar18 = zeros;
                      ppcVar7 = head;
                      if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                         (ppcVar18 = ppcVar19, ppcVar7 = ppcVar19, head != (char **)0x0)) {
                        zeros[5] = (char *)ppcVar19;
                        ppcVar7 = head;
                      }
                    }
                    head = ppcVar7;
                    zeros = ppcVar18;
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    if (pcVar14 != (char *)0x0) {
LAB_001057f0:
                      cVar9 = recurse_tree();
                      ppcVar18 = zeros;
                      ppcVar8 = head;
                      lVar12 = n_strings;
                      ppcVar7 = zeros;
                      if (cVar9 != '\0') goto joined_r0x00103392;
                    }
                  }
                  head = ppcVar8;
                  zeros = ppcVar18;
                  pcVar14 = ppcVar20[2];
                  ppcVar19 = zeros;
                  ppcVar18 = head;
                  if (ppcVar20[4] == (char *)0x0) {
LAB_0010580b:
                    ppcVar19 = zeros;
                    ppcVar18 = head;
                    if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
                       (ppcVar19 = ppcVar20, ppcVar18 = ppcVar20, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar20;
                      ppcVar18 = head;
                    }
                  }
                  head = ppcVar18;
                  zeros = ppcVar19;
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_0010583b:
                    cVar9 = recurse_tree();
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
              }
            }
            head = ppcVar8;
            zeros = ppcVar18;
            unaff_R14 = (char **)ppcVar22[2];
            ppcVar19 = zeros;
            ppcVar20 = head;
            if (ppcVar22[4] == (char *)0x0) {
LAB_00104125:
              ppcVar19 = zeros;
              ppcVar20 = head;
              if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                 (ppcVar19 = ppcVar22, ppcVar20 = ppcVar22, head != (char **)0x0)) {
                zeros[5] = (char *)ppcVar22;
                ppcVar20 = head;
              }
            }
            head = ppcVar20;
            zeros = ppcVar19;
            ppcVar19 = zeros;
            ppcVar20 = head;
            if (unaff_R14 == (char **)0x0) goto LAB_0010334a;
LAB_00103bd5:
            ppcVar22 = (char **)unaff_R14[1];
            if (ppcVar22 == (char **)0x0) {
              ppcVar22 = (char **)unaff_R14[2];
              pcVar14 = unaff_R14[4];
              if (ppcVar22 == (char **)0x0) goto LAB_00104838;
              if (pcVar14 == (char *)0x0) goto LAB_00104d60;
            }
            else {
              ppcVar19 = (char **)ppcVar22[1];
              if (ppcVar19 == (char **)0x0) {
                pcVar14 = ppcVar22[2];
                if (pcVar14 != (char *)0x0) {
                  if (ppcVar22[4] == (char *)0x0) goto LAB_00104d25;
                  goto LAB_00105c10;
                }
                ppcVar19 = zeros;
                ppcVar20 = head;
                if (((ppcVar22[4] == (char *)0x0) && (*(char *)((long)ppcVar22 + 0x19) == '\0')) &&
                   (ppcVar19 = ppcVar22, ppcVar20 = ppcVar22, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar22;
                  ppcVar20 = head;
                }
              }
              else {
                if (ppcVar19[1] == (char *)0x0) {
                  pcVar14 = ppcVar19[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar19[4] == (char *)0x0) goto LAB_00103c21;
                    goto LAB_00104d0a;
                  }
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0'))
                     && (ppcVar20 = ppcVar19, ppcVar18 = ppcVar19, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar19;
                    ppcVar18 = head;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar19[2];
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  if (ppcVar19[4] == (char *)0x0) {
LAB_00103c21:
                    ppcVar20 = zeros;
                    ppcVar18 = head;
                    if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                       (ppcVar20 = ppcVar19, ppcVar18 = ppcVar19, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar19;
                      ppcVar18 = head;
                    }
                  }
                  head = ppcVar18;
                  zeros = ppcVar20;
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_00104d0a:
                    cVar9 = recurse_tree();
                    ppcVar20 = zeros;
                    ppcVar18 = head;
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
                head = ppcVar18;
                zeros = ppcVar20;
                pcVar14 = ppcVar22[2];
                ppcVar19 = zeros;
                ppcVar20 = head;
                if (ppcVar22[4] == (char *)0x0) {
LAB_00104d25:
                  ppcVar19 = zeros;
                  ppcVar20 = head;
                  if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                     (ppcVar19 = ppcVar22, ppcVar20 = ppcVar22, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar22;
                    ppcVar20 = head;
                  }
                }
                head = ppcVar20;
                zeros = ppcVar19;
                ppcVar19 = zeros;
                ppcVar20 = head;
                if (pcVar14 != (char *)0x0) {
LAB_00105c10:
                  cVar9 = recurse_tree();
                  ppcVar19 = zeros;
                  ppcVar20 = head;
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                }
              }
              head = ppcVar20;
              zeros = ppcVar19;
              ppcVar22 = (char **)unaff_R14[2];
              ppcVar19 = zeros;
              ppcVar20 = head;
              if (unaff_R14[4] == (char *)0x0) {
LAB_00104d60:
                ppcVar19 = zeros;
                ppcVar20 = head;
                if ((*(char *)((long)unaff_R14 + 0x19) == '\0') &&
                   (ppcVar19 = unaff_R14, ppcVar20 = unaff_R14, head != (char **)0x0)) {
                  zeros[5] = (char *)unaff_R14;
                  ppcVar19 = unaff_R14;
                  ppcVar20 = head;
                }
              }
              head = ppcVar20;
              zeros = ppcVar19;
              ppcVar19 = zeros;
              ppcVar20 = head;
              if (ppcVar22 == (char **)0x0) goto LAB_0010334a;
            }
            if (ppcVar22[1] == (char *)0x0) {
              pcVar21 = ppcVar22[2];
              pcVar14 = ppcVar22[4];
              if (pcVar21 == (char *)0x0) goto LAB_00104e26;
              if (pcVar14 == (char *)0x0) goto LAB_00104b1b;
            }
            else {
              cVar9 = recurse_tree();
              lVar12 = n_strings;
              ppcVar7 = zeros;
              ppcVar8 = head;
              if (cVar9 != '\0') goto joined_r0x00103392;
              pcVar21 = ppcVar22[2];
              ppcVar19 = zeros;
              ppcVar20 = head;
              if (ppcVar22[4] == (char *)0x0) {
LAB_00104b1b:
                ppcVar19 = zeros;
                ppcVar20 = head;
                if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                   (ppcVar19 = ppcVar22, ppcVar20 = ppcVar22, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar22;
                  ppcVar20 = head;
                }
              }
              head = ppcVar20;
              zeros = ppcVar19;
              ppcVar19 = zeros;
              ppcVar20 = head;
              if (pcVar21 == (char *)0x0) goto LAB_0010334a;
            }
            cVar9 = recurse_tree();
            ppcVar19 = zeros;
            ppcVar20 = head;
            lVar12 = n_strings;
            ppcVar7 = zeros;
            ppcVar8 = head;
            if (cVar9 != '\0') goto joined_r0x00103392;
          }
        }
LAB_0010334a:
        head = ppcVar20;
        zeros = ppcVar19;
        ppcVar20 = (char **)ppcVar15[2];
        ppcVar22 = zeros;
        ppcVar19 = head;
        if (ppcVar15[4] == (char *)0x0) {
LAB_00103357:
          ppcVar22 = zeros;
          ppcVar19 = head;
          if ((*(char *)((long)ppcVar15 + 0x19) == '\0') &&
             (ppcVar22 = ppcVar15, ppcVar19 = ppcVar15, head != (char **)0x0)) {
            zeros[5] = (char *)ppcVar15;
            ppcVar19 = head;
          }
        }
        head = ppcVar19;
        zeros = ppcVar22;
        lVar12 = n_strings;
        ppcVar7 = zeros;
        ppcVar8 = head;
        if (ppcVar20 != (char **)0x0) goto LAB_0010348e;
      }
      else {
        ppcVar20 = (char **)ppcVar22[1];
        if (ppcVar20 == (char **)0x0) {
          ppcVar19 = (char **)ppcVar22[2];
          pcVar14 = ppcVar22[4];
          if (ppcVar19 == (char **)0x0) {
LAB_00104956:
            ppcVar20 = zeros;
            ppcVar18 = head;
            if (((pcVar14 == (char *)0x0) && (*(char *)((long)ppcVar22 + 0x19) == '\0')) &&
               (ppcVar20 = ppcVar22, ppcVar18 = ppcVar22, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar22;
              ppcVar18 = head;
            }
          }
          else {
            if (pcVar14 == (char *)0x0) goto LAB_001039e6;
LAB_00103a16:
            ppcVar22 = (char **)ppcVar19[1];
            if (ppcVar22 == (char **)0x0) {
              ppcVar22 = (char **)ppcVar19[2];
              if (ppcVar22 == (char **)0x0) {
                ppcVar20 = zeros;
                ppcVar18 = head;
                if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0')) &&
                   (ppcVar20 = ppcVar19, ppcVar18 = ppcVar19, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar19;
                  ppcVar18 = head;
                }
                goto LAB_00103332;
              }
              if (ppcVar19[4] == (char *)0x0) goto LAB_00104595;
            }
            else {
              ppcVar20 = (char **)ppcVar22[1];
              if (ppcVar20 == (char **)0x0) {
                ppcVar20 = (char **)ppcVar22[2];
                if (ppcVar20 != (char **)0x0) {
                  if (ppcVar22[4] == (char *)0x0) goto LAB_00103aa6;
                  goto LAB_0010451f;
                }
                ppcVar18 = zeros;
                ppcVar8 = head;
                if (((ppcVar22[4] == (char *)0x0) && (*(char *)((long)ppcVar22 + 0x19) == '\0')) &&
                   (ppcVar18 = ppcVar22, ppcVar8 = ppcVar22, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar22;
                  ppcVar8 = head;
                }
              }
              else {
                if (ppcVar20[1] == (char *)0x0) {
                  pcVar14 = ppcVar20[2];
                  if (pcVar14 != (char *)0x0) {
                    if (ppcVar20[4] == (char *)0x0) goto LAB_00103a6c;
                    goto LAB_0010569d;
                  }
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (((ppcVar20[4] == (char *)0x0) && (*(char *)((long)ppcVar20 + 0x19) == '\0'))
                     && (ppcVar18 = ppcVar20, ppcVar8 = ppcVar20, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar20;
                    ppcVar8 = head;
                  }
                }
                else {
                  cVar9 = recurse_tree();
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                  pcVar14 = ppcVar20[2];
                  ppcVar18 = zeros;
                  ppcVar7 = head;
                  if (ppcVar20[4] == (char *)0x0) {
LAB_00103a6c:
                    ppcVar18 = zeros;
                    ppcVar7 = head;
                    if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
                       (ppcVar18 = ppcVar20, ppcVar7 = ppcVar20, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar20;
                      ppcVar7 = head;
                    }
                  }
                  head = ppcVar7;
                  zeros = ppcVar18;
                  ppcVar18 = zeros;
                  ppcVar8 = head;
                  if (pcVar14 != (char *)0x0) {
LAB_0010569d:
                    cVar9 = recurse_tree();
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                  }
                }
                head = ppcVar8;
                zeros = ppcVar18;
                ppcVar20 = (char **)ppcVar22[2];
                ppcVar18 = zeros;
                ppcVar7 = head;
                if (ppcVar22[4] == (char *)0x0) {
LAB_00103aa6:
                  ppcVar18 = zeros;
                  ppcVar7 = head;
                  if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                     (ppcVar18 = ppcVar22, ppcVar7 = ppcVar22, head != (char **)0x0)) {
                    zeros[5] = (char *)ppcVar22;
                    ppcVar7 = head;
                  }
                }
                head = ppcVar7;
                zeros = ppcVar18;
                ppcVar18 = zeros;
                ppcVar8 = head;
                if (ppcVar20 != (char **)0x0) {
LAB_0010451f:
                  if (ppcVar20[1] == (char *)0x0) {
                    pcVar14 = ppcVar20[2];
                    if (pcVar14 != (char *)0x0) {
                      if (ppcVar20[4] == (char *)0x0) goto LAB_0010455b;
                      goto LAB_00104f1a;
                    }
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    if (((ppcVar20[4] == (char *)0x0) && (*(char *)((long)ppcVar20 + 0x19) == '\0'))
                       && (ppcVar18 = ppcVar20, ppcVar8 = ppcVar20, head != (char **)0x0)) {
                      zeros[5] = (char *)ppcVar20;
                      ppcVar8 = head;
                    }
                  }
                  else {
                    cVar9 = recurse_tree();
                    lVar12 = n_strings;
                    ppcVar7 = zeros;
                    ppcVar8 = head;
                    if (cVar9 != '\0') goto joined_r0x00103392;
                    pcVar14 = ppcVar20[2];
                    ppcVar22 = zeros;
                    ppcVar18 = head;
                    if (ppcVar20[4] == (char *)0x0) {
LAB_0010455b:
                      ppcVar22 = zeros;
                      ppcVar18 = head;
                      if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
                         (ppcVar22 = ppcVar20, ppcVar18 = ppcVar20, head != (char **)0x0)) {
                        zeros[5] = (char *)ppcVar20;
                        ppcVar18 = head;
                      }
                    }
                    head = ppcVar18;
                    zeros = ppcVar22;
                    ppcVar18 = zeros;
                    ppcVar8 = head;
                    if (pcVar14 != (char *)0x0) {
LAB_00104f1a:
                      cVar9 = recurse_tree();
                      ppcVar18 = zeros;
                      ppcVar8 = head;
                      lVar12 = n_strings;
                      ppcVar7 = zeros;
                      if (cVar9 != '\0') goto joined_r0x00103392;
                    }
                  }
                }
              }
              head = ppcVar8;
              zeros = ppcVar18;
              ppcVar22 = (char **)ppcVar19[2];
              ppcVar20 = zeros;
              ppcVar18 = head;
              if (ppcVar19[4] == (char *)0x0) {
LAB_00104595:
                ppcVar20 = zeros;
                ppcVar18 = head;
                if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                   (ppcVar20 = ppcVar19, ppcVar18 = ppcVar19, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar19;
                  ppcVar18 = head;
                }
              }
              head = ppcVar18;
              zeros = ppcVar20;
              ppcVar20 = zeros;
              ppcVar18 = head;
              if (ppcVar22 == (char **)0x0) goto LAB_00103332;
            }
            if (ppcVar22[1] == (char *)0x0) {
              pcVar21 = ppcVar22[2];
              pcVar14 = ppcVar22[4];
              if (pcVar21 == (char *)0x0) goto LAB_00104956;
              if (pcVar14 == (char *)0x0) goto LAB_001044c0;
            }
            else {
              cVar9 = recurse_tree();
              lVar12 = n_strings;
              ppcVar7 = zeros;
              ppcVar8 = head;
              if (cVar9 != '\0') goto joined_r0x00103392;
              pcVar21 = ppcVar22[2];
              ppcVar19 = zeros;
              ppcVar20 = head;
              if (ppcVar22[4] == (char *)0x0) {
LAB_001044c0:
                ppcVar19 = zeros;
                ppcVar20 = head;
                if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
                   (ppcVar19 = ppcVar22, ppcVar20 = ppcVar22, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar22;
                  ppcVar20 = head;
                }
              }
              head = ppcVar20;
              zeros = ppcVar19;
              ppcVar20 = zeros;
              ppcVar18 = head;
              if (pcVar21 == (char *)0x0) goto LAB_00103332;
            }
            cVar9 = recurse_tree();
            ppcVar20 = zeros;
            ppcVar18 = head;
            lVar12 = n_strings;
            ppcVar7 = zeros;
            ppcVar8 = head;
            if (cVar9 != '\0') goto joined_r0x00103392;
          }
LAB_00103332:
          head = ppcVar18;
          zeros = ppcVar20;
          ppcVar22 = (char **)unaff_R14[2];
          ppcVar19 = zeros;
          ppcVar20 = head;
          if (unaff_R14[4] == (char *)0x0) {
LAB_0010358e:
            ppcVar19 = zeros;
            ppcVar20 = head;
            if ((*(char *)((long)unaff_R14 + 0x19) == '\0') &&
               (ppcVar19 = unaff_R14, ppcVar20 = unaff_R14, head != (char **)0x0)) {
              zeros[5] = (char *)unaff_R14;
              ppcVar19 = unaff_R14;
              ppcVar20 = head;
            }
          }
          head = ppcVar20;
          zeros = ppcVar19;
          ppcVar19 = zeros;
          ppcVar20 = head;
          if (ppcVar22 != (char **)0x0) goto LAB_001035be;
          goto LAB_0010334a;
        }
        ppcVar19 = (char **)ppcVar20[1];
        if (ppcVar19 == (char **)0x0) {
          ppcVar19 = (char **)ppcVar20[2];
          pcVar14 = ppcVar20[4];
          if (ppcVar19 == (char **)0x0) {
LAB_00104ded:
            ppcVar18 = zeros;
            ppcVar8 = head;
            if (((pcVar14 == (char *)0x0) && (*(char *)((long)ppcVar20 + 0x19) == '\0')) &&
               (ppcVar18 = ppcVar20, ppcVar8 = ppcVar20, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar20;
              ppcVar8 = head;
            }
          }
          else {
            if (pcVar14 == (char *)0x0) goto LAB_001032ea;
LAB_00103c6f:
            in_R8 = (char **)ppcVar19[1];
            if (in_R8 == (char **)0x0) {
              ppcVar20 = (char **)ppcVar19[2];
              if (ppcVar20 == (char **)0x0) {
                ppcVar18 = zeros;
                ppcVar8 = head;
                if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0')) &&
                   (ppcVar18 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar19;
                  ppcVar8 = head;
                }
                goto LAB_0010331a;
              }
              if (ppcVar19[4] == (char *)0x0) goto LAB_00103cf3;
            }
            else {
              if (in_R8[1] == (char *)0x0) {
LAB_00104ea7:
                pcVar14 = in_R8[2];
                if (pcVar14 != (char *)0x0) {
                  if (in_R8[4] == (char *)0x0) goto LAB_00103cb8;
                  goto LAB_00104ec1;
                }
                ppcVar20 = zeros;
                ppcVar18 = head;
                if (((in_R8[4] == (char *)0x0) && (*(char *)((long)in_R8 + 0x19) == '\0')) &&
                   (ppcVar20 = in_R8, ppcVar18 = in_R8, head != (char **)0x0)) {
                  zeros[5] = (char *)in_R8;
                  ppcVar20 = in_R8;
                  ppcVar18 = head;
                }
              }
              else {
                cVar9 = recurse_tree();
                lVar12 = n_strings;
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (cVar9 != '\0') goto joined_r0x00103392;
                pcVar14 = in_R8[2];
                ppcVar20 = zeros;
                ppcVar18 = head;
                if (in_R8[4] == (char *)0x0) {
LAB_00103cb8:
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  if ((*(char *)((long)in_R8 + 0x19) == '\0') &&
                     (ppcVar20 = in_R8, ppcVar18 = in_R8, head != (char **)0x0)) {
                    zeros[5] = (char *)in_R8;
                    ppcVar20 = in_R8;
                    ppcVar18 = head;
                  }
                }
                head = ppcVar18;
                zeros = ppcVar20;
                ppcVar20 = zeros;
                ppcVar18 = head;
                if (pcVar14 != (char *)0x0) {
LAB_00104ec1:
                  cVar9 = recurse_tree();
                  ppcVar20 = zeros;
                  ppcVar18 = head;
                  lVar12 = n_strings;
                  ppcVar7 = zeros;
                  ppcVar8 = head;
                  if (cVar9 != '\0') goto joined_r0x00103392;
                }
              }
              head = ppcVar18;
              zeros = ppcVar20;
              ppcVar20 = (char **)ppcVar19[2];
              ppcVar18 = zeros;
              ppcVar7 = head;
              if (ppcVar19[4] == (char *)0x0) {
LAB_00103cf3:
                ppcVar18 = zeros;
                ppcVar7 = head;
                if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
                   (ppcVar18 = ppcVar19, ppcVar7 = ppcVar19, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar19;
                  ppcVar7 = head;
                }
              }
              head = ppcVar7;
              zeros = ppcVar18;
              ppcVar18 = zeros;
              ppcVar8 = head;
              if (ppcVar20 == (char **)0x0) goto LAB_0010331a;
            }
            if (ppcVar20[1] == (char *)0x0) {
              pcVar21 = ppcVar20[2];
              pcVar14 = ppcVar20[4];
              if (pcVar21 == (char *)0x0) goto LAB_00104ded;
              if (pcVar14 == (char *)0x0) goto LAB_00103d54;
            }
            else {
              cVar9 = recurse_tree();
              lVar12 = n_strings;
              ppcVar7 = zeros;
              ppcVar8 = head;
              if (cVar9 != '\0') goto joined_r0x00103392;
              pcVar21 = ppcVar20[2];
              ppcVar19 = zeros;
              ppcVar18 = head;
              if (ppcVar20[4] == (char *)0x0) {
LAB_00103d54:
                ppcVar19 = zeros;
                ppcVar18 = head;
                if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
                   (ppcVar19 = ppcVar20, ppcVar18 = ppcVar20, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar20;
                  ppcVar18 = head;
                }
              }
              head = ppcVar18;
              zeros = ppcVar19;
              ppcVar18 = zeros;
              ppcVar8 = head;
              if (pcVar21 == (char *)0x0) goto LAB_0010331a;
            }
            cVar9 = recurse_tree();
            ppcVar18 = zeros;
            ppcVar8 = head;
            lVar12 = n_strings;
            ppcVar7 = zeros;
            if (cVar9 != '\0') goto joined_r0x00103392;
          }
LAB_0010331a:
          head = ppcVar8;
          zeros = ppcVar18;
          ppcVar19 = (char **)ppcVar22[2];
          ppcVar20 = zeros;
          ppcVar18 = head;
          if (ppcVar22[4] == (char *)0x0) {
LAB_001039e6:
            ppcVar20 = zeros;
            ppcVar18 = head;
            if ((*(char *)((long)ppcVar22 + 0x19) == '\0') &&
               (ppcVar20 = ppcVar22, ppcVar18 = ppcVar22, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar22;
              ppcVar18 = head;
            }
          }
          head = ppcVar18;
          zeros = ppcVar20;
          ppcVar20 = zeros;
          ppcVar18 = head;
          if (ppcVar19 != (char **)0x0) goto LAB_00103a16;
          goto LAB_00103332;
        }
        ppcVar18 = (char **)ppcVar19[1];
        if (ppcVar18 == (char **)0x0) {
          ppcVar18 = (char **)ppcVar19[2];
          if (ppcVar18 == (char **)0x0) {
            ppcVar7 = zeros;
            ppcVar8 = head;
            if (((ppcVar19[4] == (char *)0x0) && (*(char *)((long)ppcVar19 + 0x19) == '\0')) &&
               (ppcVar7 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar19;
              ppcVar8 = head;
            }
          }
          else {
            if (ppcVar19[4] == (char *)0x0) goto LAB_001032af;
LAB_001048ac:
            if (ppcVar18[1] == (char *)0x0) {
              pcVar14 = ppcVar18[2];
              if (pcVar14 == (char *)0x0) {
                ppcVar7 = zeros;
                ppcVar8 = head;
                if (((ppcVar18[4] == (char *)0x0) && (*(char *)((long)ppcVar18 + 0x19) == '\0')) &&
                   (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar18;
                  ppcVar8 = head;
                }
              }
              else {
                if (ppcVar18[4] == (char *)0x0) goto LAB_001048e8;
LAB_00104917:
                cVar9 = recurse_tree();
                ppcVar7 = zeros;
                ppcVar8 = head;
                lVar12 = n_strings;
                if (cVar9 != '\0') goto joined_r0x00103392;
              }
            }
            else {
              cVar9 = recurse_tree();
              lVar12 = n_strings;
              ppcVar7 = zeros;
              ppcVar8 = head;
              if (cVar9 != '\0') goto joined_r0x00103392;
              pcVar14 = ppcVar18[2];
              ppcVar19 = zeros;
              ppcVar7 = head;
              if (ppcVar18[4] == (char *)0x0) {
LAB_001048e8:
                ppcVar19 = zeros;
                ppcVar7 = head;
                if ((*(char *)((long)ppcVar18 + 0x19) == '\0') &&
                   (ppcVar19 = ppcVar18, ppcVar7 = ppcVar18, head != (char **)0x0)) {
                  zeros[5] = (char *)ppcVar18;
                  ppcVar7 = head;
                }
              }
              head = ppcVar7;
              zeros = ppcVar19;
              ppcVar7 = zeros;
              ppcVar8 = head;
              if (pcVar14 != (char *)0x0) goto LAB_00104917;
            }
          }
LAB_001032df:
          head = ppcVar8;
          zeros = ppcVar7;
          ppcVar19 = (char **)ppcVar20[2];
          ppcVar18 = zeros;
          ppcVar7 = head;
          if (ppcVar20[4] == (char *)0x0) {
LAB_001032ea:
            ppcVar18 = zeros;
            ppcVar7 = head;
            if ((*(char *)((long)ppcVar20 + 0x19) == '\0') &&
               (ppcVar18 = ppcVar20, ppcVar7 = ppcVar20, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar20;
              ppcVar7 = head;
            }
          }
          head = ppcVar7;
          zeros = ppcVar18;
          ppcVar18 = zeros;
          ppcVar8 = head;
          if (ppcVar19 != (char **)0x0) goto LAB_00103c6f;
          goto LAB_0010331a;
        }
        if (ppcVar18[1] == (char *)0x0) {
          pcVar14 = ppcVar18[2];
          if (pcVar14 == (char *)0x0) {
            ppcVar7 = zeros;
            ppcVar8 = head;
            if (((ppcVar18[4] == (char *)0x0) && (*(char *)((long)ppcVar18 + 0x19) == '\0')) &&
               (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar18;
              ppcVar8 = head;
            }
          }
          else {
            if (ppcVar18[4] == (char *)0x0) goto LAB_00104057;
LAB_00104b7a:
            cVar9 = recurse_tree();
            ppcVar7 = zeros;
            ppcVar8 = head;
            lVar12 = n_strings;
            if (cVar9 != '\0') goto joined_r0x00103392;
          }
LAB_001032a4:
          head = ppcVar8;
          zeros = ppcVar7;
          ppcVar18 = (char **)ppcVar19[2];
          ppcVar7 = zeros;
          ppcVar8 = head;
          if (ppcVar19[4] == (char *)0x0) {
LAB_001032af:
            ppcVar7 = zeros;
            ppcVar8 = head;
            if ((*(char *)((long)ppcVar19 + 0x19) == '\0') &&
               (ppcVar7 = ppcVar19, ppcVar8 = ppcVar19, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar19;
              ppcVar8 = head;
            }
          }
          head = ppcVar8;
          zeros = ppcVar7;
          ppcVar7 = zeros;
          ppcVar8 = head;
          if (ppcVar18 != (char **)0x0) goto LAB_001048ac;
          goto LAB_001032df;
        }
        cVar9 = recurse_tree();
        lVar12 = n_strings;
        ppcVar7 = zeros;
        ppcVar8 = head;
        if (cVar9 == '\0') {
          pcVar14 = ppcVar18[2];
          if (ppcVar18[4] == (char *)0x0) {
LAB_00104057:
            ppcVar7 = zeros;
            ppcVar8 = head;
            if ((*(char *)((long)ppcVar18 + 0x19) == '\0') &&
               (ppcVar7 = ppcVar18, ppcVar8 = ppcVar18, head != (char **)0x0)) {
              zeros[5] = (char *)ppcVar18;
              ppcVar8 = head;
            }
          }
          head = ppcVar8;
          zeros = ppcVar7;
          ppcVar7 = zeros;
          ppcVar8 = head;
          if (pcVar14 != (char *)0x0) goto LAB_00104b7a;
          goto LAB_001032a4;
        }
      }
    }
joined_r0x00103392:
    while (head = ppcVar8, zeros = ppcVar7, head != (char **)0x0) {
LAB_001033f0:
      ppcVar22 = (char **)head[6];
      puts(*head);
      ppcVar15 = zeros;
      bVar6 = false;
      lVar12 = n_strings + -1;
      *(undefined *)((long)head + 0x19) = 1;
      n_strings = lVar12;
      if (ppcVar22 != (char **)0x0) {
        do {
          ppcVar20 = (char **)*ppcVar22;
          ppcVar19 = ppcVar20 + 4;
          *ppcVar19 = *ppcVar19 + -1;
          if (*ppcVar19 == (char *)0x0) {
            ppcVar15[5] = (char *)ppcVar20;
            bVar6 = true;
            ppcVar15 = ppcVar20;
          }
          ppcVar22 = (char **)ppcVar22[1];
        } while (ppcVar22 != (char **)0x0);
        if (bVar6) {
          zeros = ppcVar15;
        }
      }
      ppcVar7 = zeros;
      ppcVar8 = (char **)head[5];
    }
    if (lVar12 == 0) goto LAB_001033a9;
  }
  uVar16 = quotearg_n_style_colon(0,3,param_1);
  uVar13 = dcgettext(0,"%s: input contains a loop:",5);
  error(0,0,uVar13,uVar16);
  do {
    lVar12 = *(long *)(lVar11 + 0x10);
    if (lVar12 == 0) goto LAB_001037d7;
    lVar1 = *(long *)(lVar12 + 8);
    if (lVar1 != 0) {
      lVar2 = *(long *)(lVar1 + 8);
      if (lVar2 != 0) {
        lVar3 = *(long *)(lVar2 + 8);
        if (lVar3 == 0) {
          if (*(long *)(lVar2 + 0x10) == 0) {
LAB_00104ccd:
            cVar9 = detect_loop();
          }
          else {
LAB_00104251:
            cVar9 = detect_loop();
            if (cVar9 != '\0') goto LAB_001037d7;
            lVar2 = *(long *)(lVar2 + 0x10);
            if (lVar2 == 0) goto LAB_00103908;
            lVar3 = *(long *)(lVar2 + 8);
            if (lVar3 == 0) {
              if (*(long *)(lVar2 + 0x10) == 0) goto LAB_00104ccd;
            }
            else {
              if (*(long *)(lVar3 + 8) == 0) {
                if (*(long *)(lVar3 + 0x10) != 0) goto LAB_001042ae;
                cVar9 = detect_loop();
              }
              else {
                cVar9 = recurse_tree();
                if (cVar9 != '\0') goto LAB_001037d7;
LAB_001042ae:
                cVar9 = detect_loop();
                if (cVar9 != '\0') goto LAB_001037d7;
                if (*(long *)(lVar3 + 0x10) == 0) goto LAB_001042e1;
                cVar9 = recurse_tree();
              }
              if (cVar9 != '\0') goto LAB_001037d7;
            }
LAB_001042e1:
            cVar9 = detect_loop();
            if (cVar9 != '\0') goto LAB_001037d7;
            if (*(long *)(lVar2 + 0x10) == 0) goto LAB_00103908;
            cVar9 = recurse_tree();
          }
          if (cVar9 == '\0') goto LAB_00103908;
        }
        else {
          lVar4 = *(long *)(lVar3 + 8);
          if (lVar4 == 0) {
            if (*(long *)(lVar3 + 0x10) != 0) goto LAB_00104a28;
            cVar9 = detect_loop();
joined_r0x00105510:
            if (cVar9 == '\0') goto LAB_00104251;
          }
          else {
            lVar5 = *(long *)(lVar4 + 8);
            if (lVar5 == 0) {
              if (*(long *)(lVar4 + 0x10) != 0) goto LAB_001037a7;
              cVar9 = detect_loop();
            }
            else {
              if (*(long *)(lVar5 + 8) == 0) {
                if (*(long *)(lVar5 + 0x10) != 0) goto LAB_0010377c;
                cVar9 = detect_loop();
              }
              else {
                cVar9 = recurse_tree();
                if (cVar9 != '\0') goto LAB_001037d7;
LAB_0010377c:
                cVar9 = detect_loop();
                if (cVar9 != '\0') goto LAB_001037d7;
                if (*(long *)(lVar5 + 0x10) == 0) goto LAB_001037a7;
                cVar9 = recurse_tree();
              }
              if (cVar9 != '\0') goto LAB_001037d7;
LAB_001037a7:
              cVar9 = detect_loop();
              if (cVar9 != '\0') goto LAB_001037d7;
              if (*(long *)(lVar4 + 0x10) == 0) goto LAB_00104a28;
              cVar9 = recurse_tree();
            }
            if (cVar9 != '\0') goto LAB_001037d7;
LAB_00104a28:
            cVar9 = detect_loop();
            if (cVar9 == '\0') {
              lVar3 = *(long *)(lVar3 + 0x10);
              if (lVar3 == 0) goto LAB_00104251;
              if (*(long *)(lVar3 + 8) == 0) {
                if (*(long *)(lVar3 + 0x10) == 0) {
                  cVar9 = detect_loop();
                  goto joined_r0x00105510;
                }
              }
              else {
                cVar9 = recurse_tree();
                if (cVar9 != '\0') goto LAB_001037d7;
              }
              cVar9 = detect_loop();
              if (cVar9 == '\0') {
                if (*(long *)(lVar3 + 0x10) == 0) goto LAB_00104251;
                cVar9 = recurse_tree();
                goto joined_r0x00105510;
              }
            }
          }
        }
        goto LAB_001037d7;
      }
      if (*(long *)(lVar1 + 0x10) == 0) {
LAB_00104941:
        cVar9 = detect_loop();
        goto joined_r0x0010494b;
      }
LAB_00103908:
      cVar9 = detect_loop();
      if (cVar9 != '\0') goto LAB_001037d7;
      lVar1 = *(long *)(lVar1 + 0x10);
      if (lVar1 == 0) goto LAB_001037fa;
      lVar2 = *(long *)(lVar1 + 8);
      if (lVar2 == 0) {
        if (*(long *)(lVar1 + 0x10) != 0) goto LAB_0010440b;
        cVar9 = detect_loop();
joined_r0x0010494b:
        if (cVar9 == '\0') goto LAB_001037fa;
      }
      else {
        lVar3 = *(long *)(lVar2 + 8);
        if (lVar3 == 0) {
          if (*(long *)(lVar2 + 0x10) != 0) goto LAB_0010399e;
          cVar9 = detect_loop();
        }
        else {
          if (*(long *)(lVar3 + 8) == 0) {
            if (*(long *)(lVar3 + 0x10) != 0) goto LAB_0010396b;
            cVar9 = detect_loop();
          }
          else {
            cVar9 = recurse_tree();
            if (cVar9 != '\0') goto LAB_001037d7;
LAB_0010396b:
            cVar9 = detect_loop();
            if (cVar9 != '\0') goto LAB_001037d7;
            if (*(long *)(lVar3 + 0x10) == 0) goto LAB_0010399e;
            cVar9 = recurse_tree();
          }
          if (cVar9 != '\0') goto LAB_001037d7;
LAB_0010399e:
          cVar9 = detect_loop();
          if (cVar9 != '\0') goto LAB_001037d7;
          if (*(long *)(lVar2 + 0x10) == 0) goto LAB_0010440b;
          cVar9 = recurse_tree();
        }
        if (cVar9 != '\0') goto LAB_001037d7;
LAB_0010440b:
        cVar9 = detect_loop();
        if (cVar9 == '\0') {
          lVar1 = *(long *)(lVar1 + 0x10);
          if (lVar1 == 0) goto LAB_001037fa;
          if (*(long *)(lVar1 + 8) == 0) {
            if (*(long *)(lVar1 + 0x10) == 0) goto LAB_00104941;
          }
          else {
            cVar9 = recurse_tree();
            if (cVar9 != '\0') goto LAB_001037d7;
          }
          cVar9 = detect_loop();
          if (cVar9 == '\0') {
            if (*(long *)(lVar1 + 0x10) == 0) goto LAB_001037fa;
            cVar9 = recurse_tree();
            goto joined_r0x0010494b;
          }
        }
      }
      goto LAB_001037d7;
    }
    if (*(long *)(lVar12 + 0x10) == 0) {
LAB_0010464b:
      detect_loop();
    }
    else {
LAB_001037fa:
      cVar9 = detect_loop();
      if ((cVar9 != '\0') || (lVar12 = *(long *)(lVar12 + 0x10), lVar12 == 0)) goto LAB_001037d7;
      lVar1 = *(long *)(lVar12 + 8);
      if (lVar1 == 0) {
        if (*(long *)(lVar12 + 0x10) == 0) {
          detect_loop();
          goto LAB_001037d7;
        }
LAB_00104325:
        cVar9 = detect_loop();
        if ((cVar9 == '\0') && (lVar12 = *(long *)(lVar12 + 0x10), lVar12 != 0)) {
          lVar1 = *(long *)(lVar12 + 8);
          if (lVar1 == 0) {
            if (*(long *)(lVar12 + 0x10) == 0) goto LAB_0010464b;
          }
          else {
            if (*(long *)(lVar1 + 8) == 0) {
              if (*(long *)(lVar1 + 0x10) != 0) goto LAB_00104373;
              cVar9 = detect_loop();
            }
            else {
              cVar9 = recurse_tree();
              if (cVar9 != '\0') goto LAB_001037d7;
LAB_00104373:
              cVar9 = detect_loop();
              if (cVar9 != '\0') goto LAB_001037d7;
              if (*(long *)(lVar1 + 0x10) == 0) goto LAB_0010439c;
              cVar9 = recurse_tree();
            }
            if (cVar9 != '\0') goto LAB_001037d7;
          }
LAB_0010439c:
          cVar9 = detect_loop();
          if ((cVar9 == '\0') && (lVar12 = *(long *)(lVar12 + 0x10), lVar12 != 0)) {
            if (*(long *)(lVar12 + 8) == 0) {
              if (*(long *)(lVar12 + 0x10) == 0) goto LAB_0010464b;
            }
            else {
              cVar9 = recurse_tree();
              if (cVar9 != '\0') goto LAB_001037d7;
            }
            cVar9 = detect_loop();
            if ((cVar9 == '\0') && (*(long *)(lVar12 + 0x10) != 0)) {
              recurse_tree();
            }
          }
        }
      }
      else {
        lVar2 = *(long *)(lVar1 + 8);
        if (lVar2 == 0) {
          if (*(long *)(lVar1 + 0x10) != 0) goto LAB_001045d4;
LAB_00105aa5:
          cVar9 = detect_loop();
joined_r0x00105aaf:
          if (cVar9 == '\0') goto LAB_00104325;
        }
        else {
          lVar3 = *(long *)(lVar2 + 8);
          if (lVar3 == 0) {
            if (*(long *)(lVar2 + 0x10) != 0) goto LAB_00103895;
            cVar9 = detect_loop();
          }
          else {
            if (*(long *)(lVar3 + 8) == 0) {
              if (*(long *)(lVar3 + 0x10) != 0) goto LAB_00103862;
              cVar9 = detect_loop();
            }
            else {
              cVar9 = recurse_tree();
              if (cVar9 != '\0') goto LAB_001037d7;
LAB_00103862:
              cVar9 = detect_loop();
              if (cVar9 != '\0') goto LAB_001037d7;
              if (*(long *)(lVar3 + 0x10) == 0) goto LAB_00103895;
              cVar9 = recurse_tree();
            }
            if (cVar9 != '\0') goto LAB_001037d7;
LAB_00103895:
            cVar9 = detect_loop();
            if (cVar9 != '\0') goto LAB_001037d7;
            if (*(long *)(lVar2 + 0x10) == 0) goto LAB_001045d4;
            cVar9 = recurse_tree();
          }
          if (cVar9 != '\0') goto LAB_001037d7;
LAB_001045d4:
          cVar9 = detect_loop();
          if (cVar9 == '\0') {
            lVar1 = *(long *)(lVar1 + 0x10);
            if (lVar1 != 0) {
              if (*(long *)(lVar1 + 8) == 0) {
                if (*(long *)(lVar1 + 0x10) == 0) goto LAB_00105aa5;
              }
              else {
                cVar9 = recurse_tree();
                if (cVar9 != '\0') goto LAB_001037d7;
              }
              cVar9 = detect_loop();
              if (cVar9 != '\0') goto LAB_001037d7;
              if (*(long *)(lVar1 + 0x10) != 0) {
                cVar9 = recurse_tree();
                goto joined_r0x00105aaf;
              }
            }
            goto LAB_00104325;
          }
        }
      }
    }
LAB_001037d7:
    local_80 = 0;
  } while (loop != 0);
  goto LAB_001031f4;
}