
struct s0 {
    struct s0* f0;
    unsigned char f1;
    signed char f2;
    signed char[5] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    struct s0* f18;
    signed char f19;
    signed char[6] pad32;
    int64_t f20;
    struct s0* f28;
    signed char[7] pad48;
    struct s0* f30;
};

signed char recurse_tree(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx) {
    struct s0* rbp5;
    struct s0* rbx6;
    struct s0* rdi7;
    signed char al8;
    signed char al9;

    rbp5 = rdi;
    rbx6 = rsi;
    do {
        rdi7 = rbp5->f8;
        if (!rdi7) {
            if (!rbp5->f10) 
                break;
        } else {
            rsi = rbx6;
            al8 = recurse_tree(rdi7, rsi, rdx, rcx);
            if (al8) 
                goto addr_28f0_5;
        }
        al9 = reinterpret_cast<signed char>(rbx6(rbp5, rsi));
        if (al9) 
            goto addr_28f0_5;
        rbp5 = rbp5->f10;
    } while (rbp5);
    goto addr_28e3_8;
    goto rbx6;
    addr_28f0_5:
    return 1;
    addr_28e3_8:
    return al9;
}

struct s0* xzalloc(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* xstrdup(struct s0* rdi);

void fun_24b0(unsigned char* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

uint32_t fun_2530(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

unsigned char ga320 = 0x73;

unsigned char ga321 = 0x74;

struct s0* stdin = reinterpret_cast<struct s0*>(0);

int64_t freopen_safer(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

void fadvise(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

void init_tokenbuffer(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

struct s0* readtoken(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8);

struct s0* xmalloc(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* quotearg_n_style_colon();

struct s0* fun_2420();

void fun_25e0();

int64_t rpl_fclose(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int64_t fun_2620();

signed char detect_loop(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* loop = reinterpret_cast<struct s0*>(0);

int64_t n_strings = 0;

struct s0* head = reinterpret_cast<struct s0*>(0);

struct s0* zeros = reinterpret_cast<struct s0*>(0);

void fun_23d0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t* fun_2390();

struct s0* search_item(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* r15_6;
    struct s0* rbp7;
    void* rsp8;
    struct s0* r12_9;
    struct s0* rax10;
    struct s0* rax11;
    struct s0* r13_12;
    struct s0* rsi13;
    uint32_t eax14;
    int1_t zf15;
    struct s0* rax16;
    struct s0* r14_17;
    struct s0* rax18;
    struct s0* rsi19;
    uint32_t eax20;
    struct s0* rdx21;
    struct s0* rax22;
    struct s0* v23;
    uint32_t eax24;
    uint32_t eax25;
    uint32_t v26;
    uint32_t eax27;
    struct s0* rax28;
    void* rsp29;
    struct s0* rbp30;
    int64_t rax31;
    struct s0* rdi32;
    struct s0* r12_33;
    struct s0* r13_34;
    struct s0* rbx35;
    struct s0* rdi36;
    struct s0* rcx37;
    struct s0* rdx38;
    struct s0* rsi39;
    struct s0* rax40;
    struct s0* v41;
    struct s0* rax42;
    struct s0* rsi43;
    struct s0* rdi44;
    uint32_t eax45;
    struct s0* rax46;
    struct s0* rdx47;
    struct s0* rax48;
    struct s0* rax49;
    struct s0* rdi50;
    uint32_t eax51;
    uint32_t edx52;
    uint32_t eax53;
    struct s0* rax54;
    struct s0* rcx55;
    struct s0* rax56;
    struct s0* rcx57;
    uint32_t ecx58;
    int32_t r12d59;
    struct s0* rbx60;
    struct s0* rsi61;
    uint32_t eax62;
    struct s0* rdi63;
    int64_t rax64;
    int64_t rax65;
    struct s0* rax66;
    struct s0* rax67;
    struct s0* r13_68;
    struct s0* r14_69;
    struct s0* rax70;
    struct s0* v71;
    struct s0* rax72;
    struct s0* rax73;
    signed char al74;
    struct s0* r8_75;
    signed char al76;
    struct s0* rdi77;
    signed char al78;
    signed char al79;
    struct s0* rdi80;
    signed char al81;
    signed char al82;
    signed char al83;
    struct s0* rdi84;
    signed char al85;
    signed char al86;
    struct s0* r8_87;
    signed char al88;
    struct s0* rdi89;
    signed char al90;
    signed char al91;
    signed char al92;
    struct s0* rax93;
    signed char al94;
    struct s0* r8_95;
    struct s0* rdi96;
    signed char al97;
    signed char al98;
    signed char al99;
    struct s0* rdi100;
    signed char al101;
    signed char al102;
    signed char al103;
    struct s0* rax104;
    struct s0* r14_105;
    signed char al106;
    struct s0* r8_107;
    struct s0* rdi108;
    signed char al109;
    signed char al110;
    signed char al111;
    struct s0* rdi112;
    signed char al113;
    signed char al114;
    signed char al115;
    signed char al116;
    signed char al117;
    struct s0* rax118;
    struct s0* rdi119;
    signed char al120;
    signed char al121;
    signed char al122;
    struct s0* r13_123;
    struct s0* r14_124;
    struct s0* r8_125;
    struct s0* rdi126;
    signed char al127;
    signed char al128;
    signed char al129;
    struct s0* rdi130;
    signed char al131;
    signed char al132;
    signed char al133;
    signed char al134;
    signed char al135;
    struct s0* rdi136;
    signed char al137;
    signed char al138;
    signed char al139;
    struct s0* r14_140;
    signed char al141;
    int1_t zf142;
    struct s0* rdi143;
    signed char al144;
    struct s0* rdi145;
    signed char al146;
    struct s0* rdi147;
    signed char al148;
    signed char al149;
    signed char al150;
    struct s0* rdi151;
    signed char al152;
    struct s0* rdi153;
    signed char al154;
    struct s0* rdi155;
    signed char al156;
    struct s0* rdi157;
    signed char al158;
    struct s0* rdi159;
    signed char al160;
    struct s0* rdi161;
    signed char al162;
    struct s0* rdi163;
    signed char al164;
    int1_t zf165;
    struct s0* rax166;
    struct s0* r14_167;
    int1_t zf168;
    struct s0* rax169;
    int1_t zf170;
    struct s0* rax171;
    struct s0* r13_172;
    int64_t rax173;
    int64_t rax174;
    struct s0* r8_175;
    int1_t zf176;
    struct s0* rax177;
    int1_t zf178;
    struct s0* rax179;
    struct s0* rdi180;
    struct s0* rdi181;
    int1_t zf182;
    struct s0* rax183;
    signed char al184;
    struct s0* r13_185;
    int64_t rax186;
    struct s0* r8_187;
    int64_t rax188;
    struct s0* r9_189;
    int64_t rax190;
    int1_t zf191;
    struct s0* rax192;
    struct s0* rdi193;
    struct s0* rdi194;
    int1_t zf195;
    struct s0* rax196;
    signed char al197;
    struct s0* r13_198;
    struct s0* rax199;
    struct s0* r14_200;
    struct s0* r14_201;
    int1_t zf202;
    struct s0* rax203;
    struct s0* rdi204;
    int1_t zf205;
    struct s0* rax206;
    struct s0* rdi207;
    struct s0* rdi208;
    struct s0* rax209;
    struct s0* rdi210;
    struct s0* rdi211;
    int1_t zf212;
    struct s0* rax213;
    signed char al214;
    int1_t zf215;
    struct s0* rax216;
    signed char al217;
    struct s0* rdi218;
    int64_t rax219;
    struct s0* r8_220;
    int64_t rdi221;
    int1_t zf222;
    struct s0* rax223;
    struct s0* rdi224;
    struct s0* rdi225;
    int1_t zf226;
    struct s0* rax227;
    signed char al228;
    int1_t zf229;
    struct s0* rax230;
    signed char al231;
    int1_t zf232;
    struct s0* rax233;
    struct s0* rdi234;
    int1_t zf235;
    struct s0* rax236;
    int1_t zf237;
    struct s0* rax238;
    signed char al239;
    signed char al240;
    int1_t zf241;
    struct s0* rax242;
    int1_t zf243;
    struct s0* rax244;
    signed char al245;
    int1_t zf246;
    struct s0* rax247;
    struct s0* r13_248;
    int1_t zf249;
    struct s0* rax250;
    struct s0* rdi251;
    int1_t zf252;
    struct s0* rax253;
    struct s0* rdi254;
    struct s0* rdi255;
    struct s0* rdi256;
    int1_t zf257;
    struct s0* rax258;
    signed char al259;
    int1_t zf260;
    struct s0* rax261;
    signed char al262;
    struct s0* rdi263;
    int1_t zf264;
    struct s0* rax265;
    int1_t zf266;
    struct s0* rax267;
    signed char al268;
    signed char al269;
    int1_t zf270;
    struct s0* rax271;
    int1_t zf272;
    struct s0* rax273;
    signed char al274;
    struct s0* rdi275;
    int1_t zf276;
    struct s0* rax277;
    struct s0* rdi278;
    struct s0* rdi279;
    int1_t zf280;
    struct s0* rax281;
    signed char al282;
    int1_t zf283;
    struct s0* rax284;
    signed char al285;
    int1_t zf286;
    struct s0* rax287;
    signed char al288;
    int1_t zf289;
    struct s0* rax290;
    struct s0* rdi291;
    struct s0* rdi292;
    int1_t zf293;
    struct s0* rax294;
    signed char al295;
    int1_t zf296;
    struct s0* rax297;
    signed char al298;
    int1_t zf299;
    struct s0* rax300;
    int1_t zf301;
    struct s0* rax302;
    signed char al303;
    int1_t zf304;
    struct s0* rax305;
    int64_t rax306;
    struct s0* r8_307;
    struct s0* r14_308;
    int1_t zf309;
    struct s0* rax310;
    struct s0* r14_311;
    struct s0* r14_312;
    int1_t zf313;
    struct s0* rax314;
    struct s0* rdi315;
    struct s0* rdi316;
    int1_t zf317;
    struct s0* rax318;
    signed char al319;
    struct s0* r13_320;
    int1_t zf321;
    struct s0* rax322;
    struct s0* rdi323;
    struct s0* rdi324;
    int1_t zf325;
    struct s0* rax326;
    signed char al327;
    int1_t zf328;
    struct s0* rax329;
    struct s0* rdi330;
    int1_t zf331;
    struct s0* rax332;
    struct s0* rdi333;
    int1_t zf334;
    struct s0* rax335;
    signed char al336;
    struct s0* rdi337;
    int1_t zf338;
    struct s0* rax339;
    int1_t zf340;
    struct s0* rax341;
    signed char al342;
    signed char al343;
    int1_t zf344;
    struct s0* rax345;
    signed char al346;
    struct s0* rdi347;
    int1_t zf348;
    struct s0* rax349;
    struct s0* rdi350;
    struct s0* rdi351;
    int1_t zf352;
    struct s0* rax353;
    signed char al354;
    int1_t zf355;
    struct s0* rax356;
    int1_t zf357;
    struct s0* rax358;
    signed char al359;
    int1_t zf360;
    struct s0* rax361;
    signed char al362;
    struct s0* rdi363;
    struct s0* rdi364;
    int1_t zf365;
    struct s0* rax366;
    signed char al367;
    int1_t zf368;
    struct s0* rax369;
    signed char al370;
    int1_t zf371;
    struct s0* rax372;
    int1_t zf373;
    struct s0* rax374;
    signed char al375;
    int1_t zf376;
    struct s0* rax377;
    struct s0* r13_378;
    int1_t zf379;
    struct s0* rax380;
    int1_t zf381;
    struct s0* rax382;
    struct s0* rdi383;
    struct s0* rdi384;
    int1_t zf385;
    struct s0* rax386;
    signed char al387;
    struct s0* rdi388;
    struct s0* rdi389;
    int1_t zf390;
    struct s0* rax391;
    signed char al392;
    int1_t zf393;
    struct s0* rax394;
    int1_t zf395;
    struct s0* rax396;
    signed char al397;
    struct s0* rdi398;
    struct s0* rdi399;
    int1_t zf400;
    struct s0* rax401;
    signed char al402;
    int1_t zf403;
    struct s0* rax404;
    signed char al405;
    int1_t zf406;
    struct s0* rax407;
    int1_t zf408;
    struct s0* rax409;
    signed char al410;
    int1_t zf411;
    struct s0* rax412;
    int1_t zf413;
    struct s0* rax414;
    struct s0* rdi415;
    struct s0* rdi416;
    int1_t zf417;
    struct s0* rax418;
    signed char al419;
    struct s0* rdi420;
    struct s0* rdi421;
    int1_t zf422;
    struct s0* rax423;
    signed char al424;
    int1_t zf425;
    struct s0* rax426;
    int1_t zf427;
    struct s0* rax428;
    signed char al429;
    int1_t zf430;
    struct s0* rax431;
    signed char al432;
    int1_t zf433;
    struct s0* rax434;
    struct s0* rdi435;
    struct s0* rdi436;
    int1_t zf437;
    struct s0* rax438;
    signed char al439;
    int1_t zf440;
    struct s0* rax441;
    int1_t zf442;
    struct s0* rax443;
    signed char al444;
    int1_t zf445;
    struct s0* rax446;
    signed char al447;
    int1_t zf448;
    struct s0* rax449;

    r15_6 = rsi;
    rbp7 = rdi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    r12_9 = rdi->f10;
    if (!r12_9) {
        rax10 = xzalloc(56, rsi, rdx, rcx);
        r12_9 = rax10;
        if (r15_6) {
            rax11 = xstrdup(r15_6);
            *reinterpret_cast<struct s0**>(&r12_9->f0) = rax11;
        }
        rbp7->f10 = r12_9;
    } else {
        if (!rsi) {
            addr_30ad_6:
            fun_24b0("str && p && p->str", "src/tsort.c", 0x8c, "search_item");
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            goto addr_30cc_7;
        } else {
            r13_12 = r12_9;
            while (rsi13 = *reinterpret_cast<struct s0**>(&r12_9->f0), !!rsi13) {
                eax14 = fun_2530(r15_6, rsi13, rdx, rcx);
                rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
                if (!eax14) 
                    goto addr_2e6a_11;
                rdx = r12_9->f10;
                if (reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0)) {
                    rdx = r12_9->f8;
                }
                if (!rdx) 
                    goto addr_2e80_15;
                zf15 = rdx->f18 == 0;
                if (!zf15) {
                    r13_12 = rdx;
                }
                if (!zf15) {
                    rbp7 = r12_9;
                }
                r12_9 = rdx;
            }
            goto addr_30ad_6;
        }
    }
    addr_2e6a_11:
    return r12_9;
    addr_2e80_15:
    rax16 = xzalloc(56, rsi13, rdx, rcx);
    r14_17 = rax16;
    rax18 = xstrdup(r15_6);
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 + 8);
    *reinterpret_cast<struct s0**>(&r14_17->f0) = rax18;
    if (reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0)) {
        r12_9->f8 = r14_17;
    } else {
        r12_9->f10 = r14_17;
    }
    rsi19 = *reinterpret_cast<struct s0**>(&r13_12->f0);
    if (!rsi19 || (eax20 = fun_2530(r15_6, rsi19, rdx, rcx), rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8), eax20 == 0)) {
        addr_30cc_7:
        *reinterpret_cast<int32_t*>(&rdx21) = 0xa3;
        *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
        fun_24b0("str && s && s->str && !STREQ (str, s->str)", "src/tsort.c", 0xa3, "search_item");
    } else {
        rax22 = r13_12->f10;
        if (reinterpret_cast<int32_t>(eax20) < reinterpret_cast<int32_t>(0)) {
            rax22 = r13_12->f8;
        }
        *reinterpret_cast<uint32_t*>(&r12_9) = reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(eax20) >> 31) | 1;
        v23 = rax22;
        if (rax22 == r14_17) 
            goto addr_2f39_27; else 
            goto addr_2ee0_28;
    }
    eax24 = ga320;
    eax25 = eax24 - 45;
    v26 = eax25;
    if (!eax25) {
        eax27 = ga321;
        v26 = eax27;
    }
    rax28 = xzalloc(56, "src/tsort.c", 0xa3, "search_item");
    rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8);
    rbp30 = rax28;
    if (!v26) 
        goto addr_3159_32;
    rdx21 = stdin;
    rax31 = freopen_safer("str && s && s->str && !STREQ (str, s->str)", "r", rdx21, "search_item");
    rsp29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp29) - 8 + 8);
    if (!rax31) 
        goto addr_5cd5_34;
    addr_3159_32:
    rdi32 = stdin;
    r12_33 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp29) + 48);
    r13_34 = reinterpret_cast<struct s0*>(" \t\n");
    fadvise(rdi32, 2, rdx21, "search_item");
    init_tokenbuffer(r12_33, 2, rdx21, "search_item");
    while (1) {
        *reinterpret_cast<int32_t*>(&rbx35) = 0;
        *reinterpret_cast<int32_t*>(&rbx35 + 4) = 0;
        while (rdi36 = stdin, rcx37 = r12_33, *reinterpret_cast<int32_t*>(&rdx38) = 3, *reinterpret_cast<int32_t*>(&rdx38 + 4) = 0, rsi39 = reinterpret_cast<struct s0*>(" \t\n"), rax40 = readtoken(rdi36, " \t\n", 3, rcx37, r8), !reinterpret_cast<int1_t>(rax40 == 0xffffffffffffffff)) {
            if (!rax40) 
                goto addr_3691_38;
            rax42 = search_item(rbp30, v41, 3, rcx37, r8);
            r14_17 = rax42;
            if (rbx35) 
                goto addr_38c7_40;
            rbx35 = r14_17;
        }
        break;
        addr_38c7_40:
        rsi43 = *reinterpret_cast<struct s0**>(&rax42->f0);
        rdi44 = *reinterpret_cast<struct s0**>(&rbx35->f0);
        eax45 = fun_2530(rdi44, rsi43, 3, rcx37);
        if (!eax45) 
            continue;
        r14_17->f20 = r14_17->f20 + 1;
        rax46 = xmalloc(16, rsi43, 3, rcx37);
        rdx47 = rbx35->f30;
        *reinterpret_cast<struct s0**>(&rax46->f0) = r14_17;
        rax46->f8 = rdx47;
        rbx35->f30 = rax46;
    }
    if (rbx35) {
        rax48 = quotearg_n_style_colon();
        r12_33 = rax48;
        rax49 = fun_2420();
        rcx37 = r12_33;
        *reinterpret_cast<int32_t*>(&rsi39) = 0;
        *reinterpret_cast<int32_t*>(&rsi39 + 4) = 0;
        rdx38 = rax49;
        fun_25e0();
        goto addr_4ea7_45;
    } else {
        rdi50 = rbp30->f10;
        if (rdi50) {
            rsi39 = reinterpret_cast<struct s0*>(0x28a0);
            recurse_tree(rdi50, 0x28a0, 3, rcx37);
        }
        rbx35 = reinterpret_cast<struct s0*>(0x2920);
        goto addr_31f4_49;
    }
    addr_3691_38:
    fun_24b0("len != 0", "src/tsort.c", 0x1ca, "tsort");
    goto addr_36b0_50;
    addr_2f39_27:
    eax51 = reinterpret_cast<unsigned char>(r13_12->f18);
    if (!*reinterpret_cast<signed char*>(&eax51) || (edx52 = -*reinterpret_cast<uint32_t*>(&r12_9), static_cast<int32_t>(*reinterpret_cast<signed char*>(&eax51)) == edx52)) {
        eax53 = eax51 + *reinterpret_cast<uint32_t*>(&r12_9);
        r12_9 = r14_17;
        r13_12->f18 = *reinterpret_cast<struct s0**>(&eax53);
        goto addr_2e6a_11;
    } else {
        if (static_cast<int32_t>(reinterpret_cast<signed char>(v23->f18)) == *reinterpret_cast<uint32_t*>(&r12_9)) {
            if (*reinterpret_cast<uint32_t*>(&r12_9) == 0xffffffff) {
                r13_12->f8 = v23->f10;
                v23->f10 = r13_12;
            } else {
                r13_12->f10 = v23->f8;
                v23->f8 = r13_12;
            }
            rax54 = v23;
            rax54->f18 = reinterpret_cast<struct s0*>(0);
            r13_12->f18 = reinterpret_cast<struct s0*>(0);
        } else {
            rax54 = v23->f8;
            rcx55 = v23->f10;
            if (*reinterpret_cast<uint32_t*>(&r12_9) == 0xffffffff) {
                v23->f10 = rcx55->f8;
                rax56 = rcx55->f10;
                rcx55->f8 = v23;
                r13_12->f8 = rax56;
                rax54 = rcx55;
                rcx55->f10 = r13_12;
            } else {
                v23->f8 = rax54->f10;
                rcx57 = rax54->f8;
                rax54->f10 = v23;
                r13_12->f10 = rcx57;
                rax54->f8 = r13_12;
            }
            r13_12->f18 = reinterpret_cast<struct s0*>(0);
            v23->f18 = reinterpret_cast<struct s0*>(0);
            ecx58 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rax54->f18)));
            if (ecx58 == *reinterpret_cast<uint32_t*>(&r12_9)) {
                r12d59 = reinterpret_cast<int32_t>(-*reinterpret_cast<uint32_t*>(&r12_9));
                r13_12->f18 = *reinterpret_cast<struct s0**>(&r12d59);
            } else {
                if (edx52 == ecx58) {
                    v23->f18 = r12_9;
                }
            }
            rax54->f18 = reinterpret_cast<struct s0*>(0);
        }
        if (r13_12 == rbp7->f10) {
            rbp7->f10 = rax54;
            r12_9 = r14_17;
            goto addr_2e6a_11;
        } else {
            rbp7->f8 = rax54;
            r12_9 = r14_17;
            goto addr_2e6a_11;
        }
    }
    addr_2ee0_28:
    if (!v23) {
        addr_308e_68:
        fun_24b0("str && p && p->str && !STREQ (str, p->str)", "src/tsort.c", 0xb1, "search_item");
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        goto addr_30ad_6;
    } else {
        rbx60 = v23;
        do {
            rsi61 = *reinterpret_cast<struct s0**>(&rbx60->f0);
            if (!rsi61) 
                goto addr_308e_68;
            eax62 = fun_2530(r15_6, rsi61, rdx, rcx);
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            if (!eax62) 
                goto addr_308e_68;
            if (reinterpret_cast<int32_t>(eax62) >= reinterpret_cast<int32_t>(0)) {
                rbx60->f18 = reinterpret_cast<struct s0*>(1);
                rbx60 = rbx60->f10;
                if (rbx60 == r14_17) 
                    goto addr_2f39_27;
            } else {
                rbx60->f18 = reinterpret_cast<struct s0*>(0xff);
                rbx60 = rbx60->f8;
                if (rbx60 == r14_17) 
                    goto addr_2f39_27;
            }
        } while (rbx60);
        goto addr_308e_68;
    }
    while (rdi63 = stdin, rax64 = rpl_fclose(rdi63, rsi39, rdx38, rcx37), !*reinterpret_cast<int32_t*>(&rax64)) {
        rax65 = fun_2620();
        goto addr_33ce_78;
        while (1) {
            addr_36b0_50:
            rax66 = quotearg_n_style_colon();
            rax67 = fun_2420();
            rcx37 = rax66;
            *reinterpret_cast<int32_t*>(&rsi39) = 0;
            *reinterpret_cast<int32_t*>(&rsi39 + 4) = 0;
            rdx38 = rax67;
            r12_33 = reinterpret_cast<struct s0*>(detect_loop);
            fun_25e0();
            while (1) {
                r13_68 = rbp30->f10;
                if (!r13_68) 
                    goto addr_37d7_80;
                r14_69 = r13_68->f8;
                if (!r14_69) {
                    if (!r13_68->f10) 
                        goto addr_464b_83; else 
                        goto addr_37fa_84;
                }
                rax70 = r14_69->f8;
                v71 = rax70;
                if (!rax70) {
                    if (!r14_69->f10) 
                        goto addr_4941_87; else 
                        goto addr_3908_88;
                }
                rax72 = v71->f8;
                if (!rax72) {
                    if (!v71->f10) 
                        goto addr_4ccd_91; else 
                        goto addr_4251_92;
                }
                rax73 = rax72->f8;
                if (!rax73) {
                    if (!rax72->f10) {
                        al74 = detect_loop(rax72, rsi39, rdx38, rcx37);
                        if (al74) {
                            goto addr_37d7_80;
                        }
                    }
                } else {
                    r8_75 = rax73->f8;
                    if (!r8_75) {
                        if (rax73->f10) {
                            addr_37a7_99:
                            al76 = detect_loop(rax73, rsi39, rdx38, rcx37);
                            if (al76) 
                                goto addr_37d7_80;
                            rdi77 = rax73->f10;
                            if (!rdi77) 
                                goto addr_4a28_101;
                            rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                            al78 = recurse_tree(rdi77, detect_loop, rdx38, rcx37);
                            if (al78) 
                                goto addr_37d7_80;
                        } else {
                            al79 = detect_loop(rax73, rsi39, rdx38, rcx37);
                            if (al79) {
                                goto addr_37d7_80;
                            }
                        }
                    } else {
                        rdi80 = r8_75->f8;
                        if (!rdi80) {
                            if (!r8_75->f10) {
                                al81 = detect_loop(r8_75, rsi39, rdx38, rcx37);
                                if (al81) {
                                    goto addr_37d7_80;
                                }
                            }
                        } else {
                            rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                            al82 = recurse_tree(rdi80, detect_loop, rdx38, rcx37);
                            r8_75 = r8_75;
                            if (al82) 
                                goto addr_37d7_80;
                        }
                        al83 = detect_loop(r8_75, rsi39, rdx38, rcx37);
                        if (al83) 
                            goto addr_37d7_80;
                        rdi84 = r8_75->f10;
                        if (!rdi84) 
                            goto addr_37a7_99;
                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                        al85 = recurse_tree(rdi84, detect_loop, rdx38, rcx37);
                        if (al85) 
                            goto addr_37d7_80; else 
                            goto addr_37a7_99;
                    }
                }
                addr_4a28_101:
                al86 = detect_loop(rax72, rsi39, rdx38, rcx37);
                if (al86) 
                    goto addr_37d7_80;
                r8_87 = rax72->f10;
                if (!r8_87) {
                    addr_4251_92:
                    al88 = detect_loop(v71, rsi39, rdx38, rcx37);
                    if (al88) 
                        goto addr_37d7_80;
                } else {
                    rdi89 = r8_87->f8;
                    if (rdi89) {
                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                        al90 = recurse_tree(rdi89, detect_loop, rdx38, rcx37);
                        r8_87 = r8_87;
                        if (al90) 
                            goto addr_37d7_80; else 
                            goto addr_4a73_116;
                    }
                    if (r8_87->f10) {
                        addr_4a73_116:
                        al91 = detect_loop(r8_87, rsi39, rdx38, rcx37);
                        if (al91) 
                            goto addr_37d7_80; else 
                            goto addr_4a8d_118;
                    } else {
                        al92 = detect_loop(r8_87, rsi39, rdx38, rcx37);
                        if (al92) {
                            goto addr_37d7_80;
                        }
                    }
                }
                rax93 = v71->f10;
                v71 = rax93;
                if (!rax93) {
                    addr_3908_88:
                    al94 = detect_loop(r14_69, rsi39, rdx38, rcx37);
                    if (al94) 
                        goto addr_37d7_80;
                } else {
                    r8_95 = rax93->f8;
                    if (r8_95) {
                        rdi96 = r8_95->f8;
                        if (!rdi96) {
                            if (!r8_95->f10) {
                                al97 = detect_loop(r8_95, rsi39, rdx38, rcx37);
                                if (al97) {
                                    goto addr_37d7_80;
                                }
                            }
                        } else {
                            rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                            al98 = recurse_tree(rdi96, detect_loop, rdx38, rcx37);
                            r8_95 = r8_95;
                            if (al98) 
                                goto addr_37d7_80;
                        }
                        al99 = detect_loop(r8_95, rsi39, rdx38, rcx37);
                        if (al99) 
                            goto addr_37d7_80;
                        rdi100 = r8_95->f10;
                        if (!rdi100) 
                            goto addr_42e1_130;
                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                        al101 = recurse_tree(rdi100, detect_loop, rdx38, rcx37);
                        if (al101) 
                            goto addr_37d7_80; else 
                            goto addr_42e1_130;
                    }
                    if (rax93->f10) {
                        addr_42e1_130:
                        al102 = detect_loop(v71, rsi39, rdx38, rcx37);
                        if (al102) 
                            goto addr_37d7_80; else 
                            goto addr_42f3_133;
                    } else {
                        addr_4ccd_91:
                        al103 = detect_loop(v71, rsi39, rdx38, rcx37);
                        if (al103) {
                            goto addr_37d7_80;
                        }
                    }
                }
                rax104 = r14_69->f10;
                if (!rax104) 
                    goto addr_37fa_84;
                r14_105 = rax104->f8;
                if (!r14_105) {
                    if (!rax104->f10) {
                        al106 = detect_loop(rax104, rsi39, rdx38, rcx37);
                        if (al106) {
                            goto addr_37d7_80;
                        }
                    }
                } else {
                    r8_107 = r14_105->f8;
                    if (r8_107) {
                        rdi108 = r8_107->f8;
                        if (!rdi108) {
                            if (!r8_107->f10) {
                                al109 = detect_loop(r8_107, rsi39, rdx38, rcx37);
                                if (al109) {
                                    goto addr_37d7_80;
                                }
                            }
                        } else {
                            rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                            al110 = recurse_tree(rdi108, detect_loop, rdx38, rcx37);
                            r8_107 = r8_107;
                            if (al110) 
                                goto addr_37d7_80;
                        }
                        al111 = detect_loop(r8_107, rsi39, rdx38, rcx37);
                        if (al111) 
                            goto addr_37d7_80;
                        rdi112 = r8_107->f10;
                        if (!rdi112) 
                            goto addr_399e_148;
                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                        al113 = recurse_tree(rdi112, detect_loop, rdx38, rcx37);
                        if (al113) 
                            goto addr_37d7_80; else 
                            goto addr_399e_148;
                    }
                    if (r14_105->f10) {
                        addr_399e_148:
                        al114 = detect_loop(r14_105, rsi39, rdx38, rcx37);
                        if (al114) 
                            goto addr_37d7_80; else 
                            goto addr_39ae_151;
                    } else {
                        al115 = detect_loop(r14_105, rsi39, rdx38, rcx37);
                        if (al115) {
                            goto addr_37d7_80;
                        }
                    }
                }
                al116 = detect_loop(rax104, rsi39, rdx38, rcx37);
                if (al116) 
                    goto addr_37d7_80;
                r14_69 = rax104->f10;
                if (!r14_69) {
                    addr_37fa_84:
                    al117 = detect_loop(r13_68, rsi39, rdx38, rcx37);
                    if (al117) 
                        goto addr_37d7_80;
                    rax118 = r13_68->f10;
                    if (!rax118) 
                        goto addr_37d7_80;
                } else {
                    rdi119 = r14_69->f8;
                    if (rdi119) {
                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                        al120 = recurse_tree(rdi119, detect_loop, rdx38, rcx37);
                        if (al120) 
                            goto addr_37d7_80; else 
                            goto addr_444c_159;
                    }
                    if (r14_69->f10) {
                        addr_444c_159:
                        al121 = detect_loop(r14_69, rsi39, rdx38, rcx37);
                        if (al121) 
                            goto addr_37d7_80; else 
                            goto addr_445c_161;
                    } else {
                        addr_4941_87:
                        al122 = detect_loop(r14_69, rsi39, rdx38, rcx37);
                        if (al122) {
                            goto addr_37d7_80;
                        }
                    }
                }
                r13_123 = rax118->f8;
                if (!r13_123) {
                    if (!rax118->f10) {
                        detect_loop(rax118, rsi39, rdx38, rcx37);
                        goto addr_37d7_80;
                    }
                }
                r14_124 = r13_123->f8;
                if (!r14_124) {
                    if (!r13_123->f10) 
                        goto addr_5aa5_168;
                } else {
                    r8_125 = r14_124->f8;
                    if (r8_125) {
                        rdi126 = r8_125->f8;
                        if (!rdi126) {
                            if (!r8_125->f10) {
                                al127 = detect_loop(r8_125, rsi39, rdx38, rcx37);
                                if (al127) {
                                    goto addr_37d7_80;
                                }
                            }
                        } else {
                            rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                            al128 = recurse_tree(rdi126, detect_loop, rdx38, rcx37);
                            r8_125 = r8_125;
                            if (al128) 
                                goto addr_37d7_80;
                        }
                        al129 = detect_loop(r8_125, rsi39, rdx38, rcx37);
                        if (al129) 
                            goto addr_37d7_80;
                        rdi130 = r8_125->f10;
                        if (!rdi130) 
                            goto addr_3895_177;
                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                        al131 = recurse_tree(rdi130, detect_loop, rdx38, rcx37);
                        if (al131) 
                            goto addr_37d7_80; else 
                            goto addr_3895_177;
                    }
                    if (r14_124->f10) {
                        addr_3895_177:
                        al132 = detect_loop(r14_124, rsi39, rdx38, rcx37);
                        if (al132) 
                            goto addr_37d7_80; else 
                            goto addr_38a5_180;
                    } else {
                        al133 = detect_loop(r14_124, rsi39, rdx38, rcx37);
                        if (al133) {
                            goto addr_37d7_80;
                        }
                    }
                }
                al134 = detect_loop(r13_123, rsi39, rdx38, rcx37);
                if (al134) 
                    goto addr_37d7_80;
                r13_123 = r13_123->f10;
                if (!r13_123) {
                    al135 = detect_loop(rax118, rsi39, rdx38, rcx37);
                    if (al135) 
                        goto addr_37d7_80;
                    r13_68 = rax118->f10;
                    if (!r13_68) 
                        goto addr_37d7_80;
                } else {
                    rdi136 = r13_123->f8;
                    if (rdi136) {
                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                        al137 = recurse_tree(rdi136, detect_loop, rdx38, rcx37);
                        if (al137) 
                            goto addr_37d7_80; else 
                            goto addr_460e_189;
                    }
                    if (r13_123->f10) {
                        addr_460e_189:
                        al138 = detect_loop(r13_123, rsi39, rdx38, rcx37);
                        if (al138) 
                            goto addr_37d7_80; else 
                            goto addr_461e_191;
                    } else {
                        addr_5aa5_168:
                        al139 = detect_loop(r13_123, rsi39, rdx38, rcx37);
                        if (al139) {
                            goto addr_37d7_80;
                        }
                    }
                }
                r14_140 = r13_68->f8;
                if (!r14_140) {
                    if (r13_68->f10) {
                        addr_439c_195:
                        al141 = detect_loop(r13_68, rsi39, rdx38, rcx37);
                        if (al141 || (r13_68 = r13_68->f10, r13_68 == 0)) {
                            addr_37d7_80:
                            zf142 = loop == 0;
                            if (!zf142) 
                                continue; else 
                                break;
                        } else {
                            rdi143 = r13_68->f8;
                            if (!rdi143) {
                                if (r13_68->f10) {
                                    addr_43d6_198:
                                    al144 = detect_loop(r13_68, rsi39, rdx38, rcx37);
                                    if (!al144 && (rdi145 = r13_68->f10, !!rdi145)) {
                                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                                        recurse_tree(rdi145, detect_loop, rdx38, rcx37);
                                        goto addr_37d7_80;
                                    }
                                } else {
                                    goto addr_464b_83;
                                }
                            } else {
                                rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                                al146 = recurse_tree(rdi143, detect_loop, rdx38, rcx37);
                                if (al146) 
                                    goto addr_37d7_80; else 
                                    goto addr_43d6_198;
                            }
                        }
                    } else {
                        addr_464b_83:
                        detect_loop(r13_68, rsi39, rdx38, rcx37);
                        goto addr_37d7_80;
                    }
                } else {
                    rdi147 = r14_140->f8;
                    if (!rdi147) {
                        if (!r14_140->f10) {
                            al148 = detect_loop(r14_140, rsi39, rdx38, rcx37);
                            if (al148) {
                                goto addr_37d7_80;
                            }
                        }
                    } else {
                        rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                        al149 = recurse_tree(rdi147, detect_loop, rdx38, rcx37);
                        if (al149) 
                            goto addr_37d7_80;
                    }
                    al150 = detect_loop(r14_140, rsi39, rdx38, rcx37);
                    if (al150) 
                        goto addr_37d7_80;
                    rdi151 = r14_140->f10;
                    if (!rdi151) 
                        goto addr_439c_195;
                    rsi39 = reinterpret_cast<struct s0*>(detect_loop);
                    al152 = recurse_tree(rdi151, detect_loop, rdx38, rcx37);
                    if (al152) 
                        goto addr_37d7_80; else 
                        goto addr_439c_195;
                }
                addr_461e_191:
                rdi153 = r13_123->f10;
                if (rdi153 && (rsi39 = reinterpret_cast<struct s0*>(detect_loop), al154 = recurse_tree(rdi153, detect_loop, rdx38, rcx37), !!al154)) {
                    goto addr_37d7_80;
                }
                addr_38a5_180:
                rdi155 = r14_124->f10;
                if (rdi155 && (rsi39 = reinterpret_cast<struct s0*>(detect_loop), al156 = recurse_tree(rdi155, detect_loop, rdx38, rcx37), !!al156)) {
                    goto addr_37d7_80;
                }
                addr_445c_161:
                rdi157 = r14_69->f10;
                if (rdi157 && (rsi39 = reinterpret_cast<struct s0*>(detect_loop), al158 = recurse_tree(rdi157, detect_loop, rdx38, rcx37), !!al158)) {
                    goto addr_37d7_80;
                }
                addr_39ae_151:
                rdi159 = r14_105->f10;
                if (rdi159 && (rsi39 = reinterpret_cast<struct s0*>(detect_loop), al160 = recurse_tree(rdi159, detect_loop, rdx38, rcx37), !!al160)) {
                    goto addr_37d7_80;
                }
                addr_42f3_133:
                rdi161 = v71->f10;
                if (rdi161 && (rsi39 = reinterpret_cast<struct s0*>(detect_loop), al162 = recurse_tree(rdi161, detect_loop, rdx38, rcx37), !!al162)) {
                    goto addr_37d7_80;
                }
                addr_4a8d_118:
                rdi163 = r8_87->f10;
                if (rdi163 && (rsi39 = reinterpret_cast<struct s0*>(detect_loop), al164 = recurse_tree(rdi163, detect_loop, rdx38, rcx37), !!al164)) {
                    goto addr_37d7_80;
                }
            }
            addr_31f4_49:
            zf165 = n_strings == 0;
            if (zf165) 
                break;
            r12_33 = rbp30->f10;
            if (!r12_33) {
                addr_33d7_218:
                rax166 = head;
                if (!rax166) 
                    continue;
            } else {
                r14_17 = r12_33->f8;
                if (!r14_17) {
                    r14_167 = r12_33->f10;
                    rax65 = r12_33->f20;
                    if (!r14_167) {
                        addr_33ce_78:
                        if (!rax65 && !r12_33->f19) {
                            zf168 = head == 0;
                            if (zf168) {
                                head = r12_33;
                            } else {
                                rax169 = zeros;
                                rax169->f28 = r12_33;
                            }
                            zeros = r12_33;
                            goto addr_33d7_218;
                        }
                    } else {
                        if (!rax65) {
                            addr_3357_226:
                            if (!r12_33->f19) {
                                zf170 = head == 0;
                                if (zf170) {
                                    head = r12_33;
                                } else {
                                    rax171 = zeros;
                                    rax171->f28 = r12_33;
                                }
                                zeros = r12_33;
                                goto addr_337f_231;
                            }
                        } else {
                            addr_348e_232:
                            r13_172 = r14_167->f8;
                            if (!r13_172) {
                                r12_33 = r14_167->f10;
                                rax173 = r14_167->f20;
                                if (!r12_33) 
                                    goto addr_4aaf_234;
                                if (rax173) 
                                    goto addr_3b21_236; else 
                                    goto addr_3af1_237;
                            } else {
                                rcx37 = r13_172->f8;
                                if (!rcx37) {
                                    r12_33 = r13_172->f10;
                                    rax174 = r13_172->f20;
                                    if (!r12_33) 
                                        goto addr_4d95_240;
                                    if (!rax174) 
                                        goto addr_41fd_242; else 
                                        goto addr_3db3_243;
                                } else {
                                    r8_175 = rcx37->f8;
                                    if (!r8_175) {
                                        r12_33 = rcx37->f10;
                                        if (!r12_33) {
                                            if (!rcx37->f20 && !rcx37->f19) {
                                                zf176 = head == 0;
                                                if (zf176) {
                                                    head = rcx37;
                                                } else {
                                                    rax177 = zeros;
                                                    rax177->f28 = rcx37;
                                                }
                                                zeros = rcx37;
                                                goto addr_41f2_251;
                                            }
                                        }
                                        if (!rcx37->f20) 
                                            goto addr_4734_253; else 
                                            goto addr_4672_254;
                                    } else {
                                        r12_33 = r8_175->f8;
                                        if (!r12_33) {
                                            r12_33 = r8_175->f10;
                                            if (!r12_33) {
                                                if (!r8_175->f20 && !r8_175->f19) {
                                                    zf178 = head == 0;
                                                    if (zf178) {
                                                        head = r8_175;
                                                    } else {
                                                        rax179 = zeros;
                                                        rax179->f28 = r8_175;
                                                    }
                                                    zeros = r8_175;
                                                    goto addr_41da_262;
                                                }
                                            }
                                            if (!r8_175->f20) 
                                                goto addr_353d_264; else 
                                                goto addr_4174_265;
                                        } else {
                                            rdi180 = r12_33->f8;
                                            if (!rdi180) {
                                                rdi181 = r12_33->f10;
                                                if (!rdi181) {
                                                    if (!r12_33->f20 && !r12_33->f19) {
                                                        zf182 = head == 0;
                                                        if (zf182) {
                                                            head = r12_33;
                                                        } else {
                                                            rax183 = zeros;
                                                            rax183->f28 = r12_33;
                                                        }
                                                        zeros = r12_33;
                                                        goto addr_3532_273;
                                                    }
                                                } else {
                                                    if (!r12_33->f20) 
                                                        goto addr_3501_275; else 
                                                        goto addr_538a_276;
                                                }
                                            } else {
                                                rsi39 = rbx35;
                                                al184 = recurse_tree(rdi180, rsi39, rdx38, rcx37);
                                                if (al184) 
                                                    goto addr_3388_278;
                                                rcx37 = rcx37;
                                                r8_175 = r8_175;
                                                rdi181 = r12_33->f10;
                                                if (r12_33->f20) 
                                                    goto addr_3529_280; else 
                                                    goto addr_3501_275;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    r13_34 = r14_17->f8;
                    if (!r13_34) {
                        r13_185 = r14_17->f10;
                        rax186 = r14_17->f20;
                        if (!r13_185) 
                            goto addr_4838_283;
                        if (rax186) 
                            goto addr_35be_285; else 
                            goto addr_358e_286;
                    } else {
                        r8_187 = r13_34->f8;
                        if (!r8_187) {
                            rcx37 = r13_34->f10;
                            rax188 = r13_34->f20;
                            if (!rcx37) 
                                goto addr_4956_289;
                            if (rax188) 
                                goto addr_3a16_291; else 
                                goto addr_39e6_292;
                        } else {
                            r9_189 = r8_187->f8;
                            if (!r9_189) {
                                rcx37 = r8_187->f10;
                                rax190 = r8_187->f20;
                                if (!rcx37) 
                                    goto addr_4ded_295;
                                if (!rax190) 
                                    goto addr_32ea_297; else 
                                    goto addr_3c6f_298;
                            } else {
                                rcx37 = r9_189->f8;
                                if (!rcx37) {
                                    rcx37 = r9_189->f10;
                                    if (!rcx37) {
                                        if (!r9_189->f20 && !r9_189->f19) {
                                            zf191 = head == 0;
                                            if (zf191) {
                                                head = r9_189;
                                            } else {
                                                rax192 = zeros;
                                                rax192->f28 = r9_189;
                                            }
                                            zeros = r9_189;
                                            goto addr_32df_306;
                                        }
                                    }
                                    if (!r9_189->f20) 
                                        goto addr_32af_308; else 
                                        goto addr_48ac_309;
                                } else {
                                    rdi193 = rcx37->f8;
                                    if (!rdi193) {
                                        rdi194 = rcx37->f10;
                                        if (!rdi194) {
                                            if (!rcx37->f20 && !rcx37->f19) {
                                                zf195 = head == 0;
                                                if (zf195) {
                                                    head = rcx37;
                                                } else {
                                                    rax196 = zeros;
                                                    rax196->f28 = rcx37;
                                                }
                                                zeros = rcx37;
                                                goto addr_32a4_317;
                                            }
                                        } else {
                                            if (!rcx37->f20) 
                                                goto addr_4057_319; else 
                                                goto addr_4b7a_320;
                                        }
                                    } else {
                                        rsi39 = rbx35;
                                        al197 = recurse_tree(rdi193, rsi39, rdx38, rcx37);
                                        if (al197) 
                                            goto addr_3388_278;
                                        rcx37 = rcx37;
                                        r8_187 = r8_187;
                                        r9_189 = r9_189;
                                        rdi194 = rcx37->f10;
                                        if (!rcx37->f20) 
                                            goto addr_4057_319; else 
                                            goto addr_329b_323;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            goto addr_33f0_325;
            addr_337f_231:
            if (r14_167) 
                goto addr_348e_232; else 
                goto addr_3388_278;
            addr_3b21_236:
            r13_198 = r12_33->f8;
            if (!r13_198) {
                r14_167 = r12_33->f10;
                if (!r14_167) {
                    rdx38 = head;
                    if (!r12_33->f20 && !r12_33->f19) {
                        if (!rdx38) {
                            head = r12_33;
                        } else {
                            rax199 = zeros;
                            rax199->f28 = r12_33;
                        }
                        zeros = r12_33;
                        goto addr_3388_278;
                    }
                }
                if (!r12_33->f20) 
                    goto addr_3f75_333;
            } else {
                r14_200 = r13_198->f8;
                if (!r14_200) {
                    r14_201 = r13_198->f10;
                    if (!r14_201) {
                        if (!r13_198->f20 && !r13_198->f19) {
                            zf202 = head == 0;
                            if (zf202) {
                                head = r13_198;
                            } else {
                                rax203 = zeros;
                                rax203->f28 = r13_198;
                            }
                            zeros = r13_198;
                            goto addr_3f68_342;
                        }
                    }
                    if (!r13_198->f20) 
                        goto addr_4c75_344; else 
                        goto addr_3f10_345;
                } else {
                    rcx37 = r14_200->f8;
                    if (!rcx37) {
                        rdi204 = r14_200->f10;
                        if (!rdi204) {
                            if (!r14_200->f20 && !r14_200->f19) {
                                zf205 = head == 0;
                                if (zf205) {
                                    head = r14_200;
                                } else {
                                    rax206 = zeros;
                                    rax206->f28 = r14_200;
                                }
                                zeros = r14_200;
                                goto addr_4c6a_353;
                            }
                        } else {
                            if (!r14_200->f20) 
                                goto addr_4c3a_355; else 
                                goto addr_5288_356;
                        }
                    }
                    rdi207 = rcx37->f8;
                    if (!rdi207) 
                        goto addr_4c05_358; else 
                        goto addr_3b56_359;
                }
            }
            addr_3fa6_360:
            r12_33 = r14_167->f8;
            if (!r12_33) {
                rdi208 = r14_167->f10;
                rax173 = r14_167->f20;
                if (!rdi208) {
                    addr_4aaf_234:
                    rdx38 = head;
                    if (!rax173 && !r14_167->f19) {
                        if (!rdx38) {
                            head = r14_167;
                        } else {
                            rax209 = zeros;
                            rax209->f28 = r14_167;
                        }
                        zeros = r14_167;
                        goto addr_3388_278;
                    }
                } else {
                    if (rax173) {
                        goto addr_404a_368;
                    }
                }
            }
            rdi210 = r12_33->f8;
            if (!rdi210) {
                rdi211 = r12_33->f10;
                if (!rdi211) {
                    if (!r12_33->f20 && !r12_33->f19) {
                        zf212 = head == 0;
                        if (zf212) {
                            head = r12_33;
                        } else {
                            rax213 = zeros;
                            rax213->f28 = r12_33;
                        }
                        zeros = r12_33;
                        goto addr_400f_376;
                    }
                } else {
                    if (r12_33->f20) 
                        goto addr_5103_378;
                }
            } else {
                rsi39 = rbx35;
                al214 = recurse_tree(rdi210, rsi39, rdx38, rcx37);
                if (al214) 
                    goto addr_3388_278;
                rdi211 = r12_33->f10;
                if (r12_33->f20) 
                    goto addr_4006_381;
            }
            if (!r12_33->f19) {
                zf215 = head == 0;
                if (zf215) {
                    head = r12_33;
                } else {
                    rax216 = zeros;
                    rax216->f28 = r12_33;
                }
                zeros = r12_33;
            }
            addr_4006_381:
            if (rdi211) {
                addr_5103_378:
                rsi39 = rbx35;
                al217 = recurse_tree(rdi211, rsi39, rdx38, rcx37);
                if (al217) {
                    goto addr_3388_278;
                }
            } else {
                addr_400f_376:
                rdi208 = r14_167->f10;
                if (r14_167->f20) {
                    addr_4041_388:
                    if (!rdi208) {
                        addr_3388_278:
                        rax166 = head;
                        if (rax166) {
                            while (1) {
                                addr_33f0_325:
                                rdi218 = *reinterpret_cast<struct s0**>(&rax166->f0);
                                r12_33 = rax166->f30;
                                fun_23d0(rdi218, rsi39, rdx38, rcx37);
                                rax219 = n_strings;
                                r8_220 = head;
                                *reinterpret_cast<int32_t*>(&rsi39) = 0;
                                *reinterpret_cast<int32_t*>(&rsi39 + 4) = 0;
                                rcx37 = zeros;
                                rdi221 = rax219 - 1;
                                r8_220->f19 = 1;
                                n_strings = rdi221;
                                if (!r12_33) {
                                    addr_345d_389:
                                    rax166 = r8_220->f28;
                                    head = rax166;
                                    if (rax166) 
                                        continue; else 
                                        break;
                                } else {
                                    do {
                                        rdx38 = *reinterpret_cast<struct s0**>(&r12_33->f0);
                                        rdx38->f20 = rdx38->f20 - 1;
                                        if (!rdx38->f20) {
                                            rcx37->f28 = rdx38;
                                            *reinterpret_cast<int32_t*>(&rsi39) = 1;
                                            *reinterpret_cast<int32_t*>(&rsi39 + 4) = 0;
                                            rcx37 = rdx38;
                                        }
                                        r12_33 = r12_33->f8;
                                    } while (r12_33);
                                    if (!*reinterpret_cast<signed char*>(&rsi39)) 
                                        goto addr_345d_389;
                                }
                                zeros = rcx37;
                                goto addr_345d_389;
                            }
                        } else {
                            rdi221 = n_strings;
                            goto addr_33a0_397;
                        }
                    } else {
                        addr_404a_368:
                        rsi39 = rbx35;
                        recurse_tree(rdi208, rsi39, rdx38, rcx37);
                        goto addr_3388_278;
                    }
                } else {
                    if (!r14_167->f19) {
                        zf222 = head == 0;
                        if (zf222) {
                            head = r14_167;
                        } else {
                            rax223 = zeros;
                            rax223->f28 = r14_167;
                        }
                        zeros = r14_167;
                        goto addr_4041_388;
                    }
                }
            }
            addr_33a0_397:
            if (rdi221) 
                continue; else 
                break;
            addr_3f10_345:
            rdi224 = r14_201->f8;
            if (!rdi224) {
                rdi225 = r14_201->f10;
                if (!rdi225) {
                    if (!r14_201->f20 && !r14_201->f19) {
                        zf226 = head == 0;
                        if (zf226) {
                            head = r14_201;
                        } else {
                            rax227 = zeros;
                            rax227->f28 = r14_201;
                        }
                        zeros = r14_201;
                        goto addr_3f68_342;
                    }
                } else {
                    if (r14_201->f20) 
                        goto addr_501a_411;
                }
            } else {
                rsi39 = rbx35;
                al228 = recurse_tree(rdi224, rsi39, rdx38, rcx37);
                if (al228) 
                    goto addr_3388_278;
                rdi225 = r14_201->f10;
                if (r14_201->f20) 
                    goto addr_3f5f_414;
            }
            if (!r14_201->f19) {
                zf229 = head == 0;
                if (zf229) {
                    head = r14_201;
                } else {
                    rax230 = zeros;
                    rax230->f28 = r14_201;
                }
                zeros = r14_201;
            }
            addr_3f5f_414:
            if (rdi225) {
                addr_501a_411:
                rsi39 = rbx35;
                al231 = recurse_tree(rdi225, rsi39, rdx38, rcx37);
                if (al231) {
                    goto addr_3388_278;
                }
            } else {
                addr_3f68_342:
                r14_167 = r12_33->f10;
                if (r12_33->f20) {
                    addr_3f9d_421:
                    if (!r14_167) 
                        goto addr_3388_278; else 
                        goto addr_3fa6_360;
                } else {
                    addr_3f75_333:
                    if (!r12_33->f19) {
                        zf232 = head == 0;
                        if (zf232) {
                            head = r12_33;
                        } else {
                            rax233 = zeros;
                            rax233->f28 = r12_33;
                        }
                        zeros = r12_33;
                        goto addr_3f9d_421;
                    }
                }
            }
            addr_4c05_358:
            rdi234 = rcx37->f10;
            if (!rdi234) {
                if (!rcx37->f20 && !rcx37->f19) {
                    zf235 = head == 0;
                    if (zf235) {
                        head = rcx37;
                    } else {
                        rax236 = zeros;
                        rax236->f28 = rcx37;
                    }
                    zeros = rcx37;
                    goto addr_4c2f_431;
                }
            }
            if (!rcx37->f20) {
                addr_3b7b_433:
                if (!rcx37->f19) {
                    zf237 = head == 0;
                    if (zf237) {
                        head = rcx37;
                    } else {
                        rax238 = zeros;
                        rax238->f28 = rcx37;
                    }
                    zeros = rcx37;
                }
            } else {
                addr_4c1f_438:
                rsi39 = rbx35;
                al239 = recurse_tree(rdi234, rsi39, rdx38, rcx37);
                if (al239) 
                    goto addr_3388_278; else 
                    goto addr_4c2f_431;
            }
            addr_3ba1_439:
            if (!rdi234) {
                addr_4c2f_431:
                rdi204 = r14_200->f10;
                if (r14_200->f20) {
                    addr_4c61_440:
                    if (rdi204) {
                        addr_5288_356:
                        rsi39 = rbx35;
                        al240 = recurse_tree(rdi204, rsi39, rdx38, rcx37);
                        if (al240) {
                            goto addr_3388_278;
                        }
                    } else {
                        addr_4c6a_353:
                        r14_201 = r13_198->f10;
                        if (r13_198->f20) {
                            addr_4c9c_442:
                            if (!r14_201) 
                                goto addr_3f68_342;
                        } else {
                            addr_4c75_344:
                            if (!r13_198->f19) {
                                zf241 = head == 0;
                                if (zf241) {
                                    head = r13_198;
                                } else {
                                    rax242 = zeros;
                                    rax242->f28 = r13_198;
                                }
                                zeros = r13_198;
                                goto addr_4c9c_442;
                            }
                        }
                    }
                } else {
                    addr_4c3a_355:
                    if (!r14_200->f19) {
                        zf243 = head == 0;
                        if (zf243) {
                            head = r14_200;
                        } else {
                            rax244 = zeros;
                            rax244->f28 = r14_200;
                        }
                        zeros = r14_200;
                        goto addr_4c61_440;
                    }
                }
            } else {
                goto addr_4c1f_438;
            }
            goto addr_3f10_345;
            addr_3b56_359:
            rsi39 = rbx35;
            al245 = recurse_tree(rdi207, rsi39, rdx38, rcx37);
            if (al245) 
                goto addr_3388_278;
            rcx37 = rcx37;
            rdi234 = rcx37->f10;
            if (rcx37->f20) 
                goto addr_3ba1_439; else 
                goto addr_3b7b_433;
            addr_3af1_237:
            if (!r14_167->f19) {
                zf246 = head == 0;
                if (zf246) {
                    head = r14_167;
                } else {
                    rax247 = zeros;
                    rax247->f28 = r14_167;
                }
                zeros = r14_167;
                goto addr_3b18_458;
            }
            addr_3db3_243:
            r13_248 = r12_33->f8;
            if (!r13_248) {
                r13_172 = r12_33->f10;
                if (!r13_172) {
                    if (!r12_33->f20 && !r12_33->f19) {
                        zf249 = head == 0;
                        if (zf249) {
                            head = r12_33;
                        } else {
                            rax250 = zeros;
                            rax250->f28 = r12_33;
                        }
                        zeros = r12_33;
                        goto addr_422d_465;
                    }
                }
                if (!r12_33->f20) 
                    goto addr_3e77_467;
            } else {
                rcx37 = r13_248->f8;
                if (!rcx37) {
                    rdi251 = r13_248->f10;
                    if (!rdi251) {
                        if (!r13_248->f20 && !r13_248->f19) {
                            zf252 = head == 0;
                            if (zf252) {
                                head = r13_248;
                            } else {
                                rax253 = zeros;
                                rax253->f28 = r13_248;
                            }
                            zeros = r13_248;
                            goto addr_3e6a_475;
                        }
                    } else {
                        if (!r13_248->f20) 
                            goto addr_3e3a_477; else 
                            goto addr_5c86_478;
                    }
                }
                rdi254 = rcx37->f8;
                if (!rdi254) 
                    goto addr_5c3d_480; else 
                    goto addr_3ddb_481;
            }
            addr_49ab_482:
            rdi255 = r13_172->f8;
            if (!rdi255) {
                rdi256 = r13_172->f10;
                rax174 = r13_172->f20;
                if (!rdi256) {
                    addr_4d95_240:
                    if (!rax174 && !r13_172->f19) {
                        zf257 = head == 0;
                        if (zf257) {
                            head = r13_172;
                        } else {
                            rax258 = zeros;
                            rax258->f28 = r13_172;
                        }
                        zeros = r13_172;
                        goto addr_422d_465;
                    }
                } else {
                    if (rax174) 
                        goto addr_4a03_489;
                }
            } else {
                rsi39 = rbx35;
                al259 = recurse_tree(rdi255, rsi39, rdx38, rcx37);
                if (al259) 
                    goto addr_3388_278;
                rdi256 = r13_172->f10;
                if (r13_172->f20) 
                    goto addr_49fa_493;
            }
            if (!r13_172->f19) {
                zf260 = head == 0;
                if (zf260) {
                    head = r13_172;
                } else {
                    rax261 = zeros;
                    rax261->f28 = r13_172;
                }
                zeros = r13_172;
            }
            addr_49fa_493:
            if (!rdi256) {
                addr_422d_465:
                r12_33 = r14_167->f10;
                if (r14_167->f20) {
                    addr_3b18_458:
                    if (!r12_33) 
                        goto addr_3388_278; else 
                        goto addr_3b21_236;
                } else {
                    goto addr_3af1_237;
                }
            } else {
                addr_4a03_489:
                rsi39 = rbx35;
                al262 = recurse_tree(rdi256, rsi39, rdx38, rcx37);
                if (al262) {
                    goto addr_3388_278;
                }
            }
            addr_5c3d_480:
            rdi263 = rcx37->f10;
            if (!rdi263) {
                if (!rcx37->f20 && !rcx37->f19) {
                    zf264 = head == 0;
                    if (zf264) {
                        head = rcx37;
                    } else {
                        rax265 = zeros;
                        rax265->f28 = rcx37;
                    }
                    zeros = rcx37;
                    goto addr_3e2f_506;
                }
            } else {
                if (rcx37->f20) 
                    goto addr_5c57_508;
            }
            addr_3e00_509:
            if (!rcx37->f19) {
                zf266 = head == 0;
                if (zf266) {
                    head = rcx37;
                } else {
                    rax267 = zeros;
                    rax267->f28 = rcx37;
                }
                zeros = rcx37;
            }
            addr_3e26_514:
            if (rdi263) {
                addr_5c57_508:
                rsi39 = rbx35;
                al268 = recurse_tree(rdi263, rsi39, rdx38, rcx37);
                if (al268) {
                    goto addr_3388_278;
                }
            } else {
                addr_3e2f_506:
                rdi251 = r13_248->f10;
                if (r13_248->f20) {
                    addr_3e61_516:
                    if (rdi251) {
                        addr_5c86_478:
                        rsi39 = rbx35;
                        al269 = recurse_tree(rdi251, rsi39, rdx38, rcx37);
                        if (al269) {
                            goto addr_3388_278;
                        }
                    } else {
                        addr_3e6a_475:
                        r13_172 = r12_33->f10;
                        if (r12_33->f20) {
                            addr_3e9f_518:
                            if (!r13_172) 
                                goto addr_422d_465;
                        } else {
                            addr_3e77_467:
                            if (!r12_33->f19) {
                                zf270 = head == 0;
                                if (zf270) {
                                    head = r12_33;
                                } else {
                                    rax271 = zeros;
                                    rax271->f28 = r12_33;
                                }
                                zeros = r12_33;
                                goto addr_3e9f_518;
                            }
                        }
                    }
                } else {
                    addr_3e3a_477:
                    if (!r13_248->f19) {
                        zf272 = head == 0;
                        if (zf272) {
                            head = r13_248;
                        } else {
                            rax273 = zeros;
                            rax273->f28 = r13_248;
                        }
                        zeros = r13_248;
                        goto addr_3e61_516;
                    }
                }
            }
            goto addr_49ab_482;
            addr_3ddb_481:
            rsi39 = rbx35;
            al274 = recurse_tree(rdi254, rsi39, rdx38, rcx37);
            if (al274) 
                goto addr_3388_278;
            rcx37 = rcx37;
            rdi263 = rcx37->f10;
            if (rcx37->f20) 
                goto addr_3e26_514; else 
                goto addr_3e00_509;
            addr_4672_254:
            rcx37 = r12_33->f8;
            if (!rcx37) {
                rdi275 = r12_33->f10;
                if (!rdi275) {
                    if (!r12_33->f20 && !r12_33->f19) {
                        zf276 = head == 0;
                        if (zf276) {
                            head = r12_33;
                        } else {
                            rax277 = zeros;
                            rax277->f28 = r12_33;
                        }
                        zeros = r12_33;
                        goto addr_41f2_251;
                    }
                } else {
                    if (r12_33->f20) {
                        goto addr_471f_537;
                    }
                }
            }
            rdi278 = rcx37->f8;
            if (!rdi278) {
                rdi279 = rcx37->f10;
                if (!rdi279) {
                    if (!rcx37->f20 && !rcx37->f19) {
                        zf280 = head == 0;
                        if (zf280) {
                            head = rcx37;
                        } else {
                            rax281 = zeros;
                            rax281->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_46e1_545;
                    }
                } else {
                    if (rcx37->f20) 
                        goto addr_57b9_547;
                }
            } else {
                rsi39 = rbx35;
                al282 = recurse_tree(rdi278, rsi39, rdx38, rcx37);
                if (al282) 
                    goto addr_3388_278;
                rcx37 = rcx37;
                rdi279 = rcx37->f10;
                if (rcx37->f20) 
                    goto addr_46d8_550;
            }
            if (!rcx37->f19) {
                zf283 = head == 0;
                if (zf283) {
                    head = rcx37;
                } else {
                    rax284 = zeros;
                    rax284->f28 = rcx37;
                }
                zeros = rcx37;
            }
            addr_46d8_550:
            if (rdi279) {
                addr_57b9_547:
                rsi39 = rbx35;
                al285 = recurse_tree(rdi279, rsi39, rdx38, rcx37);
                if (al285) {
                    goto addr_3388_278;
                }
            } else {
                addr_46e1_545:
                rdi275 = r12_33->f10;
                if (r12_33->f20) {
                    addr_4716_557:
                    if (!rdi275) {
                        addr_41f2_251:
                        r12_33 = r13_172->f10;
                        if (r13_172->f20) {
                            addr_4224_558:
                            if (r12_33) 
                                goto addr_3db3_243; else 
                                goto addr_422d_465;
                        } else {
                            addr_41fd_242:
                            if (!r13_172->f19) {
                                zf286 = head == 0;
                                if (zf286) {
                                    head = r13_172;
                                } else {
                                    rax287 = zeros;
                                    rax287->f28 = r13_172;
                                }
                                zeros = r13_172;
                                goto addr_4224_558;
                            }
                        }
                    } else {
                        addr_471f_537:
                        rsi39 = rbx35;
                        al288 = recurse_tree(rdi275, rsi39, rdx38, rcx37);
                        if (al288) {
                            goto addr_3388_278;
                        }
                    }
                } else {
                    if (!r12_33->f19) {
                        zf289 = head == 0;
                        if (zf289) {
                            head = r12_33;
                        } else {
                            rax290 = zeros;
                            rax290->f28 = r12_33;
                        }
                        zeros = r12_33;
                        goto addr_4716_557;
                    }
                }
            }
            addr_4174_265:
            rdi291 = r12_33->f8;
            if (!rdi291) {
                rdi292 = r12_33->f10;
                if (!rdi292) {
                    if (!r12_33->f20 && !r12_33->f19) {
                        zf293 = head == 0;
                        if (zf293) {
                            head = r12_33;
                        } else {
                            rax294 = zeros;
                            rax294->f28 = r12_33;
                        }
                        zeros = r12_33;
                        goto addr_41da_262;
                    }
                } else {
                    if (r12_33->f20) 
                        goto addr_513c_576;
                }
            } else {
                rsi39 = rbx35;
                al295 = recurse_tree(rdi291, rsi39, rdx38, rcx37);
                if (al295) 
                    goto addr_3388_278;
                rcx37 = rcx37;
                rdi292 = r12_33->f10;
                if (r12_33->f20) 
                    goto addr_41d1_579;
            }
            if (!r12_33->f19) {
                zf296 = head == 0;
                if (zf296) {
                    head = r12_33;
                } else {
                    rax297 = zeros;
                    rax297->f28 = r12_33;
                }
                zeros = r12_33;
            }
            addr_41d1_579:
            if (rdi292) {
                addr_513c_576:
                rsi39 = rbx35;
                al298 = recurse_tree(rdi292, rsi39, rdx38, rcx37);
                rcx37 = rcx37;
                if (al298) {
                    goto addr_3388_278;
                }
            } else {
                addr_41da_262:
                r12_33 = rcx37->f10;
                if (!rcx37->f20) {
                    addr_4734_253:
                    if (!rcx37->f19) {
                        zf299 = head == 0;
                        if (zf299) {
                            head = rcx37;
                        } else {
                            rax300 = zeros;
                            rax300->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_41e9_590;
                    }
                } else {
                    addr_41e9_590:
                    if (r12_33) 
                        goto addr_4672_254; else 
                        goto addr_41f2_251;
                }
            }
            addr_3501_275:
            if (!r12_33->f19) {
                zf301 = head == 0;
                if (zf301) {
                    head = r12_33;
                } else {
                    rax302 = zeros;
                    rax302->f28 = r12_33;
                }
                zeros = r12_33;
            }
            addr_3529_280:
            if (rdi181) {
                addr_538a_276:
                rsi39 = rbx35;
                al303 = recurse_tree(rdi181, rsi39, rdx38, rcx37);
                rcx37 = rcx37;
                r8_175 = r8_175;
                if (al303) {
                    goto addr_3388_278;
                }
            } else {
                addr_3532_273:
                r12_33 = r8_175->f10;
                if (r8_175->f20) {
                    addr_3564_596:
                    if (!r12_33) 
                        goto addr_41da_262;
                } else {
                    addr_353d_264:
                    if (!r8_175->f19) {
                        zf304 = head == 0;
                        if (zf304) {
                            head = r8_175;
                        } else {
                            rax305 = zeros;
                            rax305->f28 = r8_175;
                        }
                        zeros = r8_175;
                        goto addr_3564_596;
                    }
                }
            }
            goto addr_4174_265;
            addr_35be_285:
            rcx37 = r13_185->f8;
            if (!rcx37) {
                r14_17 = r13_185->f10;
                rax306 = r13_185->f20;
                if (!r14_17) 
                    goto addr_4e26_603;
                if (!rax306) 
                    goto addr_4125_605;
            } else {
                r8_307 = rcx37->f8;
                if (!r8_307) {
                    r14_308 = rcx37->f10;
                    if (!r14_308) {
                        if (!rcx37->f20 && !rcx37->f19) {
                            zf309 = head == 0;
                            if (zf309) {
                                head = rcx37;
                            } else {
                                rax310 = zeros;
                                rax310->f28 = rcx37;
                            }
                            zeros = rcx37;
                            goto addr_411a_613;
                        }
                    }
                    if (!rcx37->f20) 
                        goto addr_47f8_615; else 
                        goto addr_4785_616;
                } else {
                    r14_311 = r8_307->f8;
                    if (!r14_311) {
                        r14_312 = r8_307->f10;
                        if (!r14_312) {
                            if (!r8_307->f20 && !r8_307->f19) {
                                zf313 = head == 0;
                                if (zf313) {
                                    head = r8_307;
                                } else {
                                    rax314 = zeros;
                                    rax314->f28 = r8_307;
                                }
                                zeros = r8_307;
                                goto addr_4102_624;
                            }
                        }
                        if (!r8_307->f20) 
                            goto addr_365c_626; else 
                            goto addr_40a0_627;
                    } else {
                        rdi315 = r14_311->f8;
                        if (!rdi315) {
                            rdi316 = r14_311->f10;
                            if (!rdi316) {
                                if (!r14_311->f20 && !r14_311->f19) {
                                    zf317 = head == 0;
                                    if (zf317) {
                                        head = r14_311;
                                    } else {
                                        rax318 = zeros;
                                        rax318->f28 = r14_311;
                                    }
                                    zeros = r14_311;
                                    goto addr_3651_635;
                                }
                            } else {
                                if (!r14_311->f20) 
                                    goto addr_3621_637; else 
                                    goto addr_540e_638;
                            }
                        } else {
                            rsi39 = rbx35;
                            al319 = recurse_tree(rdi315, rsi39, rdx38, rcx37);
                            if (al319) 
                                goto addr_3388_278;
                            rcx37 = rcx37;
                            r8_307 = r8_307;
                            rdi316 = r14_311->f10;
                            if (r14_311->f20) 
                                goto addr_3648_641; else 
                                goto addr_3621_637;
                        }
                    }
                }
            }
            addr_3bd5_642:
            r13_320 = r14_17->f8;
            if (!r13_320) {
                r13_185 = r14_17->f10;
                rax186 = r14_17->f20;
                if (r13_185) {
                    if (!rax186) {
                        addr_4d60_645:
                        if (!r14_17->f19) {
                            zf321 = head == 0;
                            if (zf321) {
                                head = r14_17;
                            } else {
                                rax322 = zeros;
                                rax322->f28 = r14_17;
                            }
                            zeros = r14_17;
                        }
                    } else {
                        addr_4af3_650:
                        rdi323 = r13_185->f8;
                        if (!rdi323) {
                            rdi324 = r13_185->f10;
                            rax306 = r13_185->f20;
                            if (!rdi324) {
                                addr_4e26_603:
                                if (!rax306 && !r13_185->f19) {
                                    zf325 = head == 0;
                                    if (zf325) {
                                        head = r13_185;
                                    } else {
                                        rax326 = zeros;
                                        rax326->f28 = r13_185;
                                    }
                                    zeros = r13_185;
                                    goto addr_334a_656;
                                }
                            } else {
                                if (rax306) 
                                    goto addr_4b4b_658;
                                goto addr_4b1b_660;
                            }
                        } else {
                            rsi39 = rbx35;
                            al327 = recurse_tree(rdi323, rsi39, rdx38, rcx37);
                            if (al327) 
                                goto addr_3388_278;
                            rdi324 = r13_185->f10;
                            if (r13_185->f20) 
                                goto addr_4b42_663; else 
                                goto addr_4b1b_660;
                        }
                    }
                } else {
                    addr_4838_283:
                    if (!rax186 && !r14_17->f19) {
                        zf328 = head == 0;
                        if (zf328) {
                            head = r14_17;
                        } else {
                            rax329 = zeros;
                            rax329->f28 = r14_17;
                        }
                        zeros = r14_17;
                        goto addr_334a_656;
                    }
                }
            } else {
                rcx37 = r13_320->f8;
                if (!rcx37) {
                    rdi330 = r13_320->f10;
                    if (!rdi330) {
                        if (!r13_320->f20 && !r13_320->f19) {
                            zf331 = head == 0;
                            if (zf331) {
                                head = r13_320;
                            } else {
                                rax332 = zeros;
                                rax332->f28 = r13_320;
                            }
                            zeros = r13_320;
                            goto addr_4d55_675;
                        }
                    } else {
                        if (!r13_320->f20) 
                            goto addr_4d25_677; else 
                            goto addr_5c10_678;
                    }
                }
                rdi333 = rcx37->f8;
                if (!rdi333) 
                    goto addr_4cf0_680; else 
                    goto addr_3bfc_681;
            }
            addr_4d87_682:
            if (!r13_185) 
                goto addr_334a_656;
            goto addr_4af3_650;
            addr_4b1b_660:
            if (!r13_185->f19) {
                zf334 = head == 0;
                if (zf334) {
                    head = r13_185;
                } else {
                    rax335 = zeros;
                    rax335->f28 = r13_185;
                }
                zeros = r13_185;
            }
            addr_4b42_663:
            if (!rdi324) {
                addr_334a_656:
                r14_167 = r12_33->f10;
                if (r12_33->f20) 
                    goto addr_337f_231; else 
                    goto addr_3357_226;
            } else {
                addr_4b4b_658:
                rsi39 = rbx35;
                al336 = recurse_tree(rdi324, rsi39, rdx38, rcx37);
                if (al336) {
                    goto addr_3388_278;
                }
            }
            addr_4cf0_680:
            rdi337 = rcx37->f10;
            if (!rdi337) {
                if (!rcx37->f20 && !rcx37->f19) {
                    zf338 = head == 0;
                    if (zf338) {
                        head = rcx37;
                    } else {
                        rax339 = zeros;
                        rax339->f28 = rcx37;
                    }
                    zeros = rcx37;
                    goto addr_4d1a_694;
                }
            }
            if (!rcx37->f20) {
                addr_3c21_696:
                if (!rcx37->f19) {
                    zf340 = head == 0;
                    if (zf340) {
                        head = rcx37;
                    } else {
                        rax341 = zeros;
                        rax341->f28 = rcx37;
                    }
                    zeros = rcx37;
                }
            } else {
                addr_4d0a_701:
                rsi39 = rbx35;
                al342 = recurse_tree(rdi337, rsi39, rdx38, rcx37);
                if (al342) 
                    goto addr_3388_278; else 
                    goto addr_4d1a_694;
            }
            addr_3c47_702:
            if (!rdi337) {
                addr_4d1a_694:
                rdi330 = r13_320->f10;
                if (r13_320->f20) {
                    addr_4d4c_703:
                    if (rdi330) {
                        addr_5c10_678:
                        rsi39 = rbx35;
                        al343 = recurse_tree(rdi330, rsi39, rdx38, rcx37);
                        if (al343) {
                            goto addr_3388_278;
                        }
                    } else {
                        addr_4d55_675:
                        r13_185 = r14_17->f10;
                        if (r14_17->f20) 
                            goto addr_4d87_682; else 
                            goto addr_4d60_645;
                    }
                } else {
                    addr_4d25_677:
                    if (!r13_320->f19) {
                        zf344 = head == 0;
                        if (zf344) {
                            head = r13_320;
                        } else {
                            rax345 = zeros;
                            rax345->f28 = r13_320;
                        }
                        zeros = r13_320;
                        goto addr_4d4c_703;
                    }
                }
            } else {
                goto addr_4d0a_701;
            }
            addr_3bfc_681:
            rsi39 = rbx35;
            al346 = recurse_tree(rdi333, rsi39, rdx38, rcx37);
            if (al346) 
                goto addr_3388_278;
            rcx37 = rcx37;
            rdi337 = rcx37->f10;
            if (rcx37->f20) 
                goto addr_3c47_702; else 
                goto addr_3c21_696;
            addr_4785_616:
            rcx37 = r14_308->f8;
            if (!rcx37) {
                rdi347 = r14_308->f10;
                if (!rdi347) {
                    if (!r14_308->f20 && !r14_308->f19) {
                        zf348 = head == 0;
                        if (zf348) {
                            head = r14_308;
                        } else {
                            rax349 = zeros;
                            rax349->f28 = r14_308;
                        }
                        zeros = r14_308;
                        goto addr_411a_613;
                    }
                } else {
                    if (r14_308->f20) {
                        goto addr_583b_719;
                    }
                }
            }
            rdi350 = rcx37->f8;
            if (!rdi350) {
                rdi351 = rcx37->f10;
                if (!rdi351) {
                    if (!rcx37->f20 && !rcx37->f19) {
                        zf352 = head == 0;
                        if (zf352) {
                            head = rcx37;
                        } else {
                            rax353 = zeros;
                            rax353->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_5800_727;
                    }
                }
                if (rcx37->f20) 
                    goto addr_57f0_729;
            } else {
                rsi39 = rbx35;
                al354 = recurse_tree(rdi350, rsi39, rdx38, rcx37);
                if (al354) 
                    goto addr_3388_278;
                rcx37 = rcx37;
                rdi351 = rcx37->f10;
                if (rcx37->f20) 
                    goto addr_47ea_732;
            }
            if (!rcx37->f19) {
                zf355 = head == 0;
                if (zf355) {
                    head = rcx37;
                } else {
                    rax356 = zeros;
                    rax356->f28 = rcx37;
                }
                zeros = rcx37;
            }
            addr_47ea_732:
            if (!rdi351) {
                addr_5800_727:
                rdi347 = r14_308->f10;
                if (r14_308->f20) {
                    addr_5832_738:
                    if (!rdi347) {
                        addr_411a_613:
                        r14_17 = r13_185->f10;
                        if (r13_185->f20) {
                            addr_414c_739:
                            if (!r14_17) 
                                goto addr_334a_656;
                        } else {
                            addr_4125_605:
                            if (!r13_185->f19) {
                                zf357 = head == 0;
                                if (zf357) {
                                    head = r13_185;
                                } else {
                                    rax358 = zeros;
                                    rax358->f28 = r13_185;
                                }
                                zeros = r13_185;
                                goto addr_414c_739;
                            }
                        }
                    } else {
                        addr_583b_719:
                        rsi39 = rbx35;
                        al359 = recurse_tree(rdi347, rsi39, rdx38, rcx37);
                        if (al359) {
                            goto addr_3388_278;
                        }
                    }
                } else {
                    if (!r14_308->f19) {
                        zf360 = head == 0;
                        if (zf360) {
                            head = r14_308;
                        } else {
                            rax361 = zeros;
                            rax361->f28 = r14_308;
                        }
                        zeros = r14_308;
                        goto addr_5832_738;
                    }
                }
            } else {
                goto addr_57f0_729;
            }
            goto addr_3bd5_642;
            addr_57f0_729:
            rsi39 = rbx35;
            al362 = recurse_tree(rdi351, rsi39, rdx38, rcx37);
            if (al362) 
                goto addr_3388_278; else 
                goto addr_5800_727;
            addr_40a0_627:
            rdi363 = r14_312->f8;
            if (!rdi363) {
                rdi364 = r14_312->f10;
                if (!rdi364) {
                    if (!r14_312->f20 && !r14_312->f19) {
                        zf365 = head == 0;
                        if (zf365) {
                            head = r14_312;
                        } else {
                            rax366 = zeros;
                            rax366->f28 = r14_312;
                        }
                        zeros = r14_312;
                        goto addr_4102_624;
                    }
                } else {
                    if (r14_312->f20) 
                        goto addr_5072_759;
                }
            } else {
                rsi39 = rbx35;
                al367 = recurse_tree(rdi363, rsi39, rdx38, rcx37);
                if (al367) 
                    goto addr_3388_278;
                rcx37 = rcx37;
                rdi364 = r14_312->f10;
                if (r14_312->f20) 
                    goto addr_40f9_762;
            }
            if (!r14_312->f19) {
                zf368 = head == 0;
                if (zf368) {
                    head = r14_312;
                } else {
                    rax369 = zeros;
                    rax369->f28 = r14_312;
                }
                zeros = r14_312;
            }
            addr_40f9_762:
            if (rdi364) {
                addr_5072_759:
                rsi39 = rbx35;
                al370 = recurse_tree(rdi364, rsi39, rdx38, rcx37);
                rcx37 = rcx37;
                if (al370) {
                    goto addr_3388_278;
                }
            } else {
                addr_4102_624:
                r14_308 = rcx37->f10;
                if (!rcx37->f20) {
                    addr_47f8_615:
                    if (!rcx37->f19) {
                        zf371 = head == 0;
                        if (zf371) {
                            head = rcx37;
                        } else {
                            rax372 = zeros;
                            rax372->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_4111_773;
                    }
                } else {
                    addr_4111_773:
                    if (r14_308) 
                        goto addr_4785_616; else 
                        goto addr_411a_613;
                }
            }
            addr_3621_637:
            if (!r14_311->f19) {
                zf373 = head == 0;
                if (zf373) {
                    head = r14_311;
                } else {
                    rax374 = zeros;
                    rax374->f28 = r14_311;
                }
                zeros = r14_311;
            }
            addr_3648_641:
            if (rdi316) {
                addr_540e_638:
                rsi39 = rbx35;
                al375 = recurse_tree(rdi316, rsi39, rdx38, rcx37);
                rcx37 = rcx37;
                r8_307 = r8_307;
                if (al375) {
                    goto addr_3388_278;
                }
            } else {
                addr_3651_635:
                r14_312 = r8_307->f10;
                if (r8_307->f20) {
                    addr_3683_779:
                    if (!r14_312) 
                        goto addr_4102_624;
                } else {
                    addr_365c_626:
                    if (!r8_307->f19) {
                        zf376 = head == 0;
                        if (zf376) {
                            head = r8_307;
                        } else {
                            rax377 = zeros;
                            rax377->f28 = r8_307;
                        }
                        zeros = r8_307;
                        goto addr_3683_779;
                    }
                }
            }
            goto addr_40a0_627;
            addr_3a16_291:
            r13_378 = rcx37->f8;
            if (!r13_378) {
                r13_34 = rcx37->f10;
                if (!r13_34) {
                    if (!rcx37->f20 && !rcx37->f19) {
                        zf379 = head == 0;
                        if (zf379) {
                            head = rcx37;
                        } else {
                            rax380 = zeros;
                            rax380->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_3332_791;
                    }
                }
                if (!rcx37->f20) 
                    goto addr_4595_793;
            } else {
                rdx38 = r13_378->f8;
                if (!rdx38) {
                    rdx38 = r13_378->f10;
                    if (!rdx38) {
                        if (!r13_378->f20 && !r13_378->f19) {
                            zf381 = head == 0;
                            if (zf381) {
                                head = r13_378;
                            } else {
                                rax382 = zeros;
                                rax382->f28 = r13_378;
                            }
                            zeros = r13_378;
                            goto addr_458a_801;
                        }
                    }
                    if (!r13_378->f20) 
                        goto addr_3aa6_803; else 
                        goto addr_451f_804;
                } else {
                    rdi383 = rdx38->f8;
                    if (!rdi383) {
                        rdi384 = rdx38->f10;
                        if (!rdi384) {
                            if (!rdx38->f20 && !rdx38->f19) {
                                zf385 = head == 0;
                                if (zf385) {
                                    head = rdx38;
                                } else {
                                    rax386 = zeros;
                                    rax386->f28 = rdx38;
                                }
                                zeros = rdx38;
                                goto addr_3a9b_812;
                            }
                        } else {
                            if (!rdx38->f20) 
                                goto addr_3a6c_814; else 
                                goto addr_569d_815;
                        }
                    } else {
                        rsi39 = rbx35;
                        al387 = recurse_tree(rdi383, rsi39, rdx38, rcx37);
                        if (al387) 
                            goto addr_3388_278;
                        rdx38 = rdx38;
                        rcx37 = rcx37;
                        rdi384 = rdx38->f10;
                        if (rdx38->f20) 
                            goto addr_3a92_818; else 
                            goto addr_3a6c_814;
                    }
                }
            }
            addr_4498_819:
            rdi388 = r13_34->f8;
            if (!rdi388) {
                rdi389 = r13_34->f10;
                rax188 = r13_34->f20;
                if (!rdi389) {
                    addr_4956_289:
                    if (!rax188 && !r13_34->f19) {
                        zf390 = head == 0;
                        if (zf390) {
                            head = r13_34;
                        } else {
                            rax391 = zeros;
                            rax391->f28 = r13_34;
                        }
                        zeros = r13_34;
                        goto addr_3332_791;
                    }
                } else {
                    if (rax188) {
                        goto addr_44f0_827;
                    }
                }
            } else {
                rsi39 = rbx35;
                al392 = recurse_tree(rdi388, rsi39, rdx38, rcx37);
                if (al392) 
                    goto addr_3388_278;
                rdi389 = r13_34->f10;
                if (r13_34->f20) 
                    goto addr_44e7_830;
            }
            if (!r13_34->f19) {
                zf393 = head == 0;
                if (zf393) {
                    head = r13_34;
                } else {
                    rax394 = zeros;
                    rax394->f28 = r13_34;
                }
                zeros = r13_34;
            }
            addr_44e7_830:
            if (!rdi389) {
                addr_3332_791:
                r13_185 = r14_17->f10;
                if (!r14_17->f20) {
                    addr_358e_286:
                    if (!r14_17->f19) {
                        zf395 = head == 0;
                        if (zf395) {
                            head = r14_17;
                        } else {
                            rax396 = zeros;
                            rax396->f28 = r14_17;
                        }
                        zeros = r14_17;
                        goto addr_3341_840;
                    }
                } else {
                    addr_3341_840:
                    if (r13_185) 
                        goto addr_35be_285; else 
                        goto addr_334a_656;
                }
            } else {
                addr_44f0_827:
                rsi39 = rbx35;
                al397 = recurse_tree(rdi389, rsi39, rdx38, rcx37);
                if (al397) {
                    goto addr_3388_278;
                }
            }
            addr_451f_804:
            rdi398 = rdx38->f8;
            if (!rdi398) {
                rdi399 = rdx38->f10;
                if (!rdi399) {
                    if (!rdx38->f20 && !rdx38->f19) {
                        zf400 = head == 0;
                        if (zf400) {
                            head = rdx38;
                        } else {
                            rax401 = zeros;
                            rax401->f28 = rdx38;
                        }
                        zeros = rdx38;
                        goto addr_458a_801;
                    }
                } else {
                    if (rdx38->f20) 
                        goto addr_4f1a_849;
                }
            } else {
                rsi39 = rbx35;
                al402 = recurse_tree(rdi398, rsi39, rdx38, rcx37);
                if (al402) 
                    goto addr_3388_278;
                rdx38 = rdx38;
                rcx37 = rcx37;
                rdi399 = rdx38->f10;
                if (rdx38->f20) 
                    goto addr_4581_852;
            }
            if (!rdx38->f19) {
                zf403 = head == 0;
                if (zf403) {
                    head = rdx38;
                } else {
                    rax404 = zeros;
                    rax404->f28 = rdx38;
                }
                zeros = rdx38;
            }
            addr_4581_852:
            if (rdi399) {
                addr_4f1a_849:
                rsi39 = rbx35;
                al405 = recurse_tree(rdi399, rsi39, rdx38, rcx37);
                rcx37 = rcx37;
                if (al405) {
                    goto addr_3388_278;
                }
            } else {
                addr_458a_801:
                r13_34 = rcx37->f10;
                if (rcx37->f20) {
                    addr_45bb_859:
                    if (!r13_34) 
                        goto addr_3332_791;
                } else {
                    addr_4595_793:
                    if (!rcx37->f19) {
                        zf406 = head == 0;
                        if (zf406) {
                            head = rcx37;
                        } else {
                            rax407 = zeros;
                            rax407->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_45bb_859;
                    }
                }
            }
            goto addr_4498_819;
            addr_3a6c_814:
            if (!rdx38->f19) {
                zf408 = head == 0;
                if (zf408) {
                    head = rdx38;
                } else {
                    rax409 = zeros;
                    rax409->f28 = rdx38;
                }
                zeros = rdx38;
            }
            addr_3a92_818:
            if (rdi384) {
                addr_569d_815:
                rsi39 = rbx35;
                al410 = recurse_tree(rdi384, rsi39, rdx38, rcx37);
                rcx37 = rcx37;
                if (al410) {
                    goto addr_3388_278;
                }
            } else {
                addr_3a9b_812:
                rdx38 = r13_378->f10;
                if (r13_378->f20) {
                    addr_3acd_870:
                    if (!rdx38) 
                        goto addr_458a_801;
                } else {
                    addr_3aa6_803:
                    if (!r13_378->f19) {
                        zf411 = head == 0;
                        if (zf411) {
                            head = r13_378;
                        } else {
                            rax412 = zeros;
                            rax412->f28 = r13_378;
                        }
                        zeros = r13_378;
                        goto addr_3acd_870;
                    }
                }
            }
            goto addr_451f_804;
            addr_3c6f_298:
            r8 = rcx37->f8;
            if (!r8) {
                r8_187 = rcx37->f10;
                if (!r8_187) {
                    if (!rcx37->f20 && !rcx37->f19) {
                        zf413 = head == 0;
                        if (zf413) {
                            head = rcx37;
                        } else {
                            rax414 = zeros;
                            rax414->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_331a_882;
                    }
                }
                if (!rcx37->f20) 
                    goto addr_3cf3_884;
            } else {
                rdi415 = r8->f8;
                if (!rdi415) {
                    addr_4ea7_45:
                    rdi416 = r8->f10;
                    if (!rdi416) {
                        if (!r8->f20 && !r8->f19) {
                            zf417 = head == 0;
                            if (zf417) {
                                head = r8;
                            } else {
                                rax418 = zeros;
                                rax418->f28 = r8;
                            }
                            zeros = r8;
                            goto addr_3ce8_892;
                        }
                    } else {
                        if (!r8->f20) 
                            goto addr_3cb8_894; else 
                            goto addr_4ec1_895;
                    }
                } else {
                    rsi39 = rbx35;
                    al419 = recurse_tree(rdi415, rsi39, rdx38, rcx37);
                    if (al419) 
                        goto addr_3388_278;
                    r8 = r8;
                    rcx37 = rcx37;
                    rdi416 = r8->f10;
                    if (r8->f20) 
                        goto addr_3cdf_898; else 
                        goto addr_3cb8_894;
                }
            }
            addr_3d22_899:
            rdi420 = r8_187->f8;
            if (!rdi420) {
                rdi421 = r8_187->f10;
                rax190 = r8_187->f20;
                if (!rdi421) {
                    addr_4ded_295:
                    if (!rax190 && !r8_187->f19) {
                        zf422 = head == 0;
                        if (zf422) {
                            head = r8_187;
                        } else {
                            rax423 = zeros;
                            rax423->f28 = r8_187;
                        }
                        zeros = r8_187;
                        goto addr_331a_882;
                    }
                } else {
                    if (rax190) 
                        goto addr_3d84_906;
                }
            } else {
                rsi39 = rbx35;
                al424 = recurse_tree(rdi420, rsi39, rdx38, rcx37);
                if (al424) 
                    goto addr_3388_278;
                r8_187 = r8_187;
                rdi421 = r8_187->f10;
                if (r8_187->f20) 
                    goto addr_3d7b_910;
            }
            if (!r8_187->f19) {
                zf425 = head == 0;
                if (zf425) {
                    head = r8_187;
                } else {
                    rax426 = zeros;
                    rax426->f28 = r8_187;
                }
                zeros = r8_187;
            }
            addr_3d7b_910:
            if (!rdi421) {
                addr_331a_882:
                rcx37 = r13_34->f10;
                if (!r13_34->f20) {
                    addr_39e6_292:
                    if (!r13_34->f19) {
                        zf427 = head == 0;
                        if (zf427) {
                            head = r13_34;
                        } else {
                            rax428 = zeros;
                            rax428->f28 = r13_34;
                        }
                        zeros = r13_34;
                        goto addr_3329_920;
                    }
                } else {
                    addr_3329_920:
                    if (rcx37) 
                        goto addr_3a16_291; else 
                        goto addr_3332_791;
                }
            } else {
                addr_3d84_906:
                rsi39 = rbx35;
                al429 = recurse_tree(rdi421, rsi39, rdx38, rcx37);
                if (al429) {
                    goto addr_3388_278;
                }
            }
            addr_3cb8_894:
            if (!r8->f19) {
                zf430 = head == 0;
                if (zf430) {
                    head = r8;
                } else {
                    rax431 = zeros;
                    rax431->f28 = r8;
                }
                zeros = r8;
            }
            addr_3cdf_898:
            if (rdi416) {
                addr_4ec1_895:
                rsi39 = rbx35;
                al432 = recurse_tree(rdi416, rsi39, rdx38, rcx37);
                rcx37 = rcx37;
                if (al432) {
                    goto addr_3388_278;
                }
            } else {
                addr_3ce8_892:
                r8_187 = rcx37->f10;
                if (rcx37->f20) {
                    addr_3d19_927:
                    if (!r8_187) 
                        goto addr_331a_882; else 
                        goto addr_3d22_899;
                } else {
                    addr_3cf3_884:
                    if (!rcx37->f19) {
                        zf433 = head == 0;
                        if (zf433) {
                            head = rcx37;
                        } else {
                            rax434 = zeros;
                            rax434->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_3d19_927;
                    }
                }
            }
            addr_48ac_309:
            rdi435 = rcx37->f8;
            if (!rdi435) {
                rdi436 = rcx37->f10;
                if (!rdi436) {
                    if (!rcx37->f20 && !rcx37->f19) {
                        zf437 = head == 0;
                        if (zf437) {
                            head = rcx37;
                        } else {
                            rax438 = zeros;
                            rax438->f28 = rcx37;
                        }
                        zeros = rcx37;
                        goto addr_32df_306;
                    }
                } else {
                    if (rcx37->f20) {
                        goto addr_4917_940;
                    }
                }
            } else {
                rsi39 = rbx35;
                al439 = recurse_tree(rdi435, rsi39, rdx38, rcx37);
                if (al439) 
                    goto addr_3388_278;
                rcx37 = rcx37;
                r8_187 = r8_187;
                rdi436 = rcx37->f10;
                if (rcx37->f20) 
                    goto addr_490e_943;
            }
            if (!rcx37->f19) {
                zf440 = head == 0;
                if (zf440) {
                    head = rcx37;
                } else {
                    rax441 = zeros;
                    rax441->f28 = rcx37;
                }
                zeros = rcx37;
            }
            addr_490e_943:
            if (!rdi436) {
                addr_32df_306:
                rcx37 = r8_187->f10;
                if (r8_187->f20) {
                    addr_3311_949:
                    if (rcx37) 
                        goto addr_3c6f_298; else 
                        goto addr_331a_882;
                } else {
                    addr_32ea_297:
                    if (!r8_187->f19) {
                        zf442 = head == 0;
                        if (zf442) {
                            head = r8_187;
                        } else {
                            rax443 = zeros;
                            rax443->f28 = r8_187;
                        }
                        zeros = r8_187;
                        goto addr_3311_949;
                    }
                }
            } else {
                addr_4917_940:
                rsi39 = rbx35;
                al444 = recurse_tree(rdi436, rsi39, rdx38, rcx37);
                r8_187 = r8_187;
                if (al444) {
                    goto addr_3388_278;
                }
            }
            addr_4057_319:
            if (!rcx37->f19) {
                zf445 = head == 0;
                if (zf445) {
                    head = rcx37;
                } else {
                    rax446 = zeros;
                    rax446->f28 = rcx37;
                }
                zeros = rcx37;
            }
            addr_329b_323:
            if (rdi194) {
                addr_4b7a_320:
                rsi39 = rbx35;
                al447 = recurse_tree(rdi194, rsi39, rdx38, rcx37);
                r8_187 = r8_187;
                r9_189 = r9_189;
                if (al447) {
                    goto addr_3388_278;
                }
            } else {
                addr_32a4_317:
                rcx37 = r9_189->f10;
                if (r9_189->f20) {
                    addr_32d6_960:
                    if (rcx37) 
                        goto addr_48ac_309; else 
                        goto addr_32df_306;
                } else {
                    addr_32af_308:
                    if (!r9_189->f19) {
                        zf448 = head == 0;
                        if (zf448) {
                            head = r9_189;
                        } else {
                            rax449 = zeros;
                            rax449->f28 = r9_189;
                        }
                        zeros = r9_189;
                        goto addr_32d6_960;
                    }
                }
            }
        }
    }
    if (v26) {
        while (1) {
            quotearg_n_style_colon();
            addr_5cb8_967:
            fun_2390();
            fun_25e0();
            addr_5cd5_34:
            quotearg_n_style_colon();
            fun_2390();
            fun_25e0();
        }
    } else {
        fun_2420();
        goto addr_5cb8_967;
    }
}

int64_t fun_2430();

int64_t fun_2380(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2430();
    if (r8d > 10) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xa4c0 + rax11 * 4) + 0xa4c0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* g28;

struct s0* slotvec = reinterpret_cast<struct s0*>(0x70);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_24c0();

struct s2 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_2370(struct s0* rdi, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

void fun_2450();

struct s0* quotearg_n_options(struct s0* rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x7c2f;
    rax8 = fun_2390();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xe070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x62a1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x7cbb;
            fun_24c0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xe120) {
                fun_2370(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x7d4a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2450();
        } else {
            return r14_19;
        }
    }
}

int64_t tsort(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10) {
    uint32_t eax11;
    uint32_t v12;
    struct s0* rax13;
    void* rsp14;
    struct s0* rbp15;
    int64_t rax16;
    struct s0* rdi17;
    struct s0* r12_18;
    struct s0* r13_19;
    struct s0* rbx20;
    struct s0* rdi21;
    struct s0* rcx22;
    struct s0* rdx23;
    struct s0* rsi24;
    struct s0* rax25;
    struct s0* v26;
    struct s0* rax27;
    struct s0* r14_28;
    struct s0* rsi29;
    struct s0* rdi30;
    uint32_t eax31;
    struct s0* rax32;
    struct s0* rdx33;
    struct s0* rax34;
    struct s0* rax35;
    struct s0* rdi36;
    struct s0* rdi37;
    int64_t rax38;
    int64_t rax39;
    struct s0* rax40;
    struct s0* rax41;
    struct s0* r13_42;
    struct s0* r14_43;
    struct s0* rax44;
    struct s0* v45;
    struct s0* rax46;
    struct s0* rax47;
    signed char al48;
    struct s0* r8_49;
    signed char al50;
    struct s0* rdi51;
    signed char al52;
    signed char al53;
    struct s0* rdi54;
    signed char al55;
    signed char al56;
    signed char al57;
    struct s0* rdi58;
    signed char al59;
    signed char al60;
    struct s0* r8_61;
    signed char al62;
    struct s0* rdi63;
    signed char al64;
    signed char al65;
    signed char al66;
    struct s0* rax67;
    signed char al68;
    struct s0* r8_69;
    struct s0* rdi70;
    signed char al71;
    signed char al72;
    signed char al73;
    struct s0* rdi74;
    signed char al75;
    signed char al76;
    signed char al77;
    struct s0* rax78;
    struct s0* r14_79;
    signed char al80;
    struct s0* r8_81;
    struct s0* rdi82;
    signed char al83;
    signed char al84;
    signed char al85;
    struct s0* rdi86;
    signed char al87;
    signed char al88;
    signed char al89;
    signed char al90;
    signed char al91;
    struct s0* rax92;
    struct s0* rdi93;
    signed char al94;
    signed char al95;
    signed char al96;
    struct s0* r13_97;
    struct s0* r14_98;
    struct s0* r8_99;
    struct s0* rdi100;
    signed char al101;
    signed char al102;
    signed char al103;
    struct s0* rdi104;
    signed char al105;
    signed char al106;
    signed char al107;
    signed char al108;
    signed char al109;
    struct s0* rdi110;
    signed char al111;
    signed char al112;
    signed char al113;
    struct s0* r14_114;
    signed char al115;
    int1_t zf116;
    struct s0* rdi117;
    signed char al118;
    struct s0* rdi119;
    signed char al120;
    struct s0* rdi121;
    signed char al122;
    signed char al123;
    signed char al124;
    struct s0* rdi125;
    signed char al126;
    struct s0* rdi127;
    signed char al128;
    struct s0* rdi129;
    signed char al130;
    struct s0* rdi131;
    signed char al132;
    struct s0* rdi133;
    signed char al134;
    struct s0* rdi135;
    signed char al136;
    struct s0* rdi137;
    signed char al138;
    int1_t zf139;
    struct s0* rax140;
    struct s0* r14_141;
    int1_t zf142;
    struct s0* rax143;
    int1_t zf144;
    struct s0* rax145;
    struct s0* r13_146;
    int64_t rax147;
    int64_t rax148;
    struct s0* r8_149;
    int1_t zf150;
    struct s0* rax151;
    int1_t zf152;
    struct s0* rax153;
    struct s0* rdi154;
    struct s0* rdi155;
    int1_t zf156;
    struct s0* rax157;
    signed char al158;
    struct s0* r13_159;
    int64_t rax160;
    struct s0* r8_161;
    int64_t rax162;
    struct s0* r9_163;
    int64_t rax164;
    int1_t zf165;
    struct s0* rax166;
    struct s0* rdi167;
    struct s0* rdi168;
    int1_t zf169;
    struct s0* rax170;
    signed char al171;
    struct s0* r13_172;
    struct s0* rax173;
    struct s0* r14_174;
    struct s0* r14_175;
    int1_t zf176;
    struct s0* rax177;
    struct s0* rdi178;
    int1_t zf179;
    struct s0* rax180;
    struct s0* rdi181;
    struct s0* rdi182;
    struct s0* rax183;
    struct s0* rdi184;
    struct s0* rdi185;
    int1_t zf186;
    struct s0* rax187;
    signed char al188;
    int1_t zf189;
    struct s0* rax190;
    signed char al191;
    struct s0* rdi192;
    int64_t rax193;
    struct s0* r8_194;
    int64_t rdi195;
    int1_t zf196;
    struct s0* rax197;
    struct s0* rdi198;
    struct s0* rdi199;
    int1_t zf200;
    struct s0* rax201;
    signed char al202;
    int1_t zf203;
    struct s0* rax204;
    signed char al205;
    int1_t zf206;
    struct s0* rax207;
    struct s0* rdi208;
    int1_t zf209;
    struct s0* rax210;
    int1_t zf211;
    struct s0* rax212;
    signed char al213;
    signed char al214;
    int1_t zf215;
    struct s0* rax216;
    int1_t zf217;
    struct s0* rax218;
    signed char al219;
    int1_t zf220;
    struct s0* rax221;
    struct s0* r13_222;
    int1_t zf223;
    struct s0* rax224;
    struct s0* rdi225;
    int1_t zf226;
    struct s0* rax227;
    struct s0* rdi228;
    struct s0* rdi229;
    struct s0* rdi230;
    int1_t zf231;
    struct s0* rax232;
    signed char al233;
    int1_t zf234;
    struct s0* rax235;
    signed char al236;
    struct s0* rdi237;
    int1_t zf238;
    struct s0* rax239;
    int1_t zf240;
    struct s0* rax241;
    signed char al242;
    signed char al243;
    int1_t zf244;
    struct s0* rax245;
    int1_t zf246;
    struct s0* rax247;
    signed char al248;
    struct s0* rdi249;
    int1_t zf250;
    struct s0* rax251;
    struct s0* rdi252;
    struct s0* rdi253;
    int1_t zf254;
    struct s0* rax255;
    signed char al256;
    int1_t zf257;
    struct s0* rax258;
    signed char al259;
    int1_t zf260;
    struct s0* rax261;
    signed char al262;
    int1_t zf263;
    struct s0* rax264;
    struct s0* rdi265;
    struct s0* rdi266;
    int1_t zf267;
    struct s0* rax268;
    signed char al269;
    int1_t zf270;
    struct s0* rax271;
    signed char al272;
    int1_t zf273;
    struct s0* rax274;
    int1_t zf275;
    struct s0* rax276;
    signed char al277;
    int1_t zf278;
    struct s0* rax279;
    int64_t rax280;
    struct s0* r8_281;
    struct s0* r14_282;
    int1_t zf283;
    struct s0* rax284;
    struct s0* r14_285;
    struct s0* r14_286;
    int1_t zf287;
    struct s0* rax288;
    struct s0* rdi289;
    struct s0* rdi290;
    int1_t zf291;
    struct s0* rax292;
    signed char al293;
    struct s0* r13_294;
    int1_t zf295;
    struct s0* rax296;
    struct s0* rdi297;
    struct s0* rdi298;
    int1_t zf299;
    struct s0* rax300;
    signed char al301;
    int1_t zf302;
    struct s0* rax303;
    struct s0* rdi304;
    int1_t zf305;
    struct s0* rax306;
    struct s0* rdi307;
    int1_t zf308;
    struct s0* rax309;
    signed char al310;
    struct s0* rdi311;
    int1_t zf312;
    struct s0* rax313;
    int1_t zf314;
    struct s0* rax315;
    signed char al316;
    signed char al317;
    int1_t zf318;
    struct s0* rax319;
    signed char al320;
    struct s0* rdi321;
    int1_t zf322;
    struct s0* rax323;
    struct s0* rdi324;
    struct s0* rdi325;
    int1_t zf326;
    struct s0* rax327;
    signed char al328;
    int1_t zf329;
    struct s0* rax330;
    int1_t zf331;
    struct s0* rax332;
    signed char al333;
    int1_t zf334;
    struct s0* rax335;
    signed char al336;
    struct s0* rdi337;
    struct s0* rdi338;
    int1_t zf339;
    struct s0* rax340;
    signed char al341;
    int1_t zf342;
    struct s0* rax343;
    signed char al344;
    int1_t zf345;
    struct s0* rax346;
    int1_t zf347;
    struct s0* rax348;
    signed char al349;
    int1_t zf350;
    struct s0* rax351;
    struct s0* r13_352;
    int1_t zf353;
    struct s0* rax354;
    int1_t zf355;
    struct s0* rax356;
    struct s0* rdi357;
    struct s0* rdi358;
    int1_t zf359;
    struct s0* rax360;
    signed char al361;
    struct s0* rdi362;
    struct s0* rdi363;
    int1_t zf364;
    struct s0* rax365;
    signed char al366;
    int1_t zf367;
    struct s0* rax368;
    int1_t zf369;
    struct s0* rax370;
    signed char al371;
    struct s0* rdi372;
    struct s0* rdi373;
    int1_t zf374;
    struct s0* rax375;
    signed char al376;
    int1_t zf377;
    struct s0* rax378;
    signed char al379;
    int1_t zf380;
    struct s0* rax381;
    int1_t zf382;
    struct s0* rax383;
    signed char al384;
    int1_t zf385;
    struct s0* rax386;
    int1_t zf387;
    struct s0* rax388;
    struct s0* rdi389;
    struct s0* rdi390;
    int1_t zf391;
    struct s0* rax392;
    signed char al393;
    struct s0* rdi394;
    struct s0* rdi395;
    int1_t zf396;
    struct s0* rax397;
    signed char al398;
    int1_t zf399;
    struct s0* rax400;
    int1_t zf401;
    struct s0* rax402;
    signed char al403;
    int1_t zf404;
    struct s0* rax405;
    signed char al406;
    int1_t zf407;
    struct s0* rax408;
    struct s0* rdi409;
    struct s0* rdi410;
    int1_t zf411;
    struct s0* rax412;
    signed char al413;
    int1_t zf414;
    struct s0* rax415;
    int1_t zf416;
    struct s0* rax417;
    signed char al418;
    int1_t zf419;
    struct s0* rax420;
    signed char al421;
    int1_t zf422;
    struct s0* rax423;

    eax11 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0)) - 45;
    v12 = eax11;
    if (!eax11) {
        v12 = rdi->f1;
    }
    rax13 = xzalloc(56, rsi, rdx, rcx);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8);
    rbp15 = rax13;
    if (!v12) 
        goto addr_3159_4;
    rdx = stdin;
    rax16 = freopen_safer(rdi, "r", rdx, rcx);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    if (!rax16) 
        goto addr_5cd5_6;
    addr_3159_4:
    rdi17 = stdin;
    r12_18 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 48);
    r13_19 = reinterpret_cast<struct s0*>(" \t\n");
    fadvise(rdi17, 2, rdx, rcx);
    init_tokenbuffer(r12_18, 2, rdx, rcx);
    while (1) {
        *reinterpret_cast<int32_t*>(&rbx20) = 0;
        *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
        while (rdi21 = stdin, rcx22 = r12_18, *reinterpret_cast<int32_t*>(&rdx23) = 3, *reinterpret_cast<int32_t*>(&rdx23 + 4) = 0, rsi24 = reinterpret_cast<struct s0*>(" \t\n"), rax25 = readtoken(rdi21, " \t\n", 3, rcx22, r8), !reinterpret_cast<int1_t>(rax25 == 0xffffffffffffffff)) {
            if (!rax25) 
                goto addr_3691_10;
            rax27 = search_item(rbp15, v26, 3, rcx22, r8);
            r14_28 = rax27;
            if (rbx20) 
                goto addr_38c7_12;
            rbx20 = r14_28;
        }
        break;
        addr_38c7_12:
        rsi29 = *reinterpret_cast<struct s0**>(&rax27->f0);
        rdi30 = *reinterpret_cast<struct s0**>(&rbx20->f0);
        eax31 = fun_2530(rdi30, rsi29, 3, rcx22);
        if (!eax31) 
            continue;
        r14_28->f20 = r14_28->f20 + 1;
        rax32 = xmalloc(16, rsi29, 3, rcx22);
        rdx33 = rbx20->f30;
        *reinterpret_cast<struct s0**>(&rax32->f0) = r14_28;
        rax32->f8 = rdx33;
        rbx20->f30 = rax32;
    }
    if (rbx20) {
        rax34 = quotearg_n_style_colon();
        r12_18 = rax34;
        rax35 = fun_2420();
        rcx22 = r12_18;
        *reinterpret_cast<int32_t*>(&rsi24) = 0;
        *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
        rdx23 = rax35;
        fun_25e0();
        goto addr_4ea7_17;
    } else {
        rdi36 = rbp15->f10;
        if (rdi36) {
            rsi24 = reinterpret_cast<struct s0*>(0x28a0);
            recurse_tree(rdi36, 0x28a0, 3, rcx22);
        }
        rbx20 = reinterpret_cast<struct s0*>(0x2920);
        goto addr_31f4_21;
    }
    addr_3691_10:
    fun_24b0("len != 0", "src/tsort.c", 0x1ca, "tsort");
    goto addr_36b0_22;
    while (rdi37 = stdin, rax38 = rpl_fclose(rdi37, rsi24, rdx23, rcx22), !*reinterpret_cast<int32_t*>(&rax38)) {
        rax39 = fun_2620();
        goto addr_33ce_25;
        while (1) {
            addr_36b0_22:
            rax40 = quotearg_n_style_colon();
            rax41 = fun_2420();
            rcx22 = rax40;
            *reinterpret_cast<int32_t*>(&rsi24) = 0;
            *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
            rdx23 = rax41;
            r12_18 = reinterpret_cast<struct s0*>(detect_loop);
            fun_25e0();
            while (1) {
                r13_42 = rbp15->f10;
                if (!r13_42) 
                    goto addr_37d7_27;
                r14_43 = r13_42->f8;
                if (!r14_43) {
                    if (!r13_42->f10) 
                        goto addr_464b_30; else 
                        goto addr_37fa_31;
                }
                rax44 = r14_43->f8;
                v45 = rax44;
                if (!rax44) {
                    if (!r14_43->f10) 
                        goto addr_4941_34; else 
                        goto addr_3908_35;
                }
                rax46 = v45->f8;
                if (!rax46) {
                    if (!v45->f10) 
                        goto addr_4ccd_38; else 
                        goto addr_4251_39;
                }
                rax47 = rax46->f8;
                if (!rax47) {
                    if (!rax46->f10) {
                        al48 = detect_loop(rax46, rsi24, rdx23, rcx22);
                        if (al48) {
                            goto addr_37d7_27;
                        }
                    }
                } else {
                    r8_49 = rax47->f8;
                    if (!r8_49) {
                        if (rax47->f10) {
                            addr_37a7_46:
                            al50 = detect_loop(rax47, rsi24, rdx23, rcx22);
                            if (al50) 
                                goto addr_37d7_27;
                            rdi51 = rax47->f10;
                            if (!rdi51) 
                                goto addr_4a28_48;
                            rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                            al52 = recurse_tree(rdi51, detect_loop, rdx23, rcx22);
                            if (al52) 
                                goto addr_37d7_27;
                        } else {
                            al53 = detect_loop(rax47, rsi24, rdx23, rcx22);
                            if (al53) {
                                goto addr_37d7_27;
                            }
                        }
                    } else {
                        rdi54 = r8_49->f8;
                        if (!rdi54) {
                            if (!r8_49->f10) {
                                al55 = detect_loop(r8_49, rsi24, rdx23, rcx22);
                                if (al55) {
                                    goto addr_37d7_27;
                                }
                            }
                        } else {
                            rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                            al56 = recurse_tree(rdi54, detect_loop, rdx23, rcx22);
                            r8_49 = r8_49;
                            if (al56) 
                                goto addr_37d7_27;
                        }
                        al57 = detect_loop(r8_49, rsi24, rdx23, rcx22);
                        if (al57) 
                            goto addr_37d7_27;
                        rdi58 = r8_49->f10;
                        if (!rdi58) 
                            goto addr_37a7_46;
                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                        al59 = recurse_tree(rdi58, detect_loop, rdx23, rcx22);
                        if (al59) 
                            goto addr_37d7_27; else 
                            goto addr_37a7_46;
                    }
                }
                addr_4a28_48:
                al60 = detect_loop(rax46, rsi24, rdx23, rcx22);
                if (al60) 
                    goto addr_37d7_27;
                r8_61 = rax46->f10;
                if (!r8_61) {
                    addr_4251_39:
                    al62 = detect_loop(v45, rsi24, rdx23, rcx22);
                    if (al62) 
                        goto addr_37d7_27;
                } else {
                    rdi63 = r8_61->f8;
                    if (rdi63) {
                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                        al64 = recurse_tree(rdi63, detect_loop, rdx23, rcx22);
                        r8_61 = r8_61;
                        if (al64) 
                            goto addr_37d7_27; else 
                            goto addr_4a73_63;
                    }
                    if (r8_61->f10) {
                        addr_4a73_63:
                        al65 = detect_loop(r8_61, rsi24, rdx23, rcx22);
                        if (al65) 
                            goto addr_37d7_27; else 
                            goto addr_4a8d_65;
                    } else {
                        al66 = detect_loop(r8_61, rsi24, rdx23, rcx22);
                        if (al66) {
                            goto addr_37d7_27;
                        }
                    }
                }
                rax67 = v45->f10;
                v45 = rax67;
                if (!rax67) {
                    addr_3908_35:
                    al68 = detect_loop(r14_43, rsi24, rdx23, rcx22);
                    if (al68) 
                        goto addr_37d7_27;
                } else {
                    r8_69 = rax67->f8;
                    if (r8_69) {
                        rdi70 = r8_69->f8;
                        if (!rdi70) {
                            if (!r8_69->f10) {
                                al71 = detect_loop(r8_69, rsi24, rdx23, rcx22);
                                if (al71) {
                                    goto addr_37d7_27;
                                }
                            }
                        } else {
                            rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                            al72 = recurse_tree(rdi70, detect_loop, rdx23, rcx22);
                            r8_69 = r8_69;
                            if (al72) 
                                goto addr_37d7_27;
                        }
                        al73 = detect_loop(r8_69, rsi24, rdx23, rcx22);
                        if (al73) 
                            goto addr_37d7_27;
                        rdi74 = r8_69->f10;
                        if (!rdi74) 
                            goto addr_42e1_77;
                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                        al75 = recurse_tree(rdi74, detect_loop, rdx23, rcx22);
                        if (al75) 
                            goto addr_37d7_27; else 
                            goto addr_42e1_77;
                    }
                    if (rax67->f10) {
                        addr_42e1_77:
                        al76 = detect_loop(v45, rsi24, rdx23, rcx22);
                        if (al76) 
                            goto addr_37d7_27; else 
                            goto addr_42f3_80;
                    } else {
                        addr_4ccd_38:
                        al77 = detect_loop(v45, rsi24, rdx23, rcx22);
                        if (al77) {
                            goto addr_37d7_27;
                        }
                    }
                }
                rax78 = r14_43->f10;
                if (!rax78) 
                    goto addr_37fa_31;
                r14_79 = rax78->f8;
                if (!r14_79) {
                    if (!rax78->f10) {
                        al80 = detect_loop(rax78, rsi24, rdx23, rcx22);
                        if (al80) {
                            goto addr_37d7_27;
                        }
                    }
                } else {
                    r8_81 = r14_79->f8;
                    if (r8_81) {
                        rdi82 = r8_81->f8;
                        if (!rdi82) {
                            if (!r8_81->f10) {
                                al83 = detect_loop(r8_81, rsi24, rdx23, rcx22);
                                if (al83) {
                                    goto addr_37d7_27;
                                }
                            }
                        } else {
                            rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                            al84 = recurse_tree(rdi82, detect_loop, rdx23, rcx22);
                            r8_81 = r8_81;
                            if (al84) 
                                goto addr_37d7_27;
                        }
                        al85 = detect_loop(r8_81, rsi24, rdx23, rcx22);
                        if (al85) 
                            goto addr_37d7_27;
                        rdi86 = r8_81->f10;
                        if (!rdi86) 
                            goto addr_399e_95;
                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                        al87 = recurse_tree(rdi86, detect_loop, rdx23, rcx22);
                        if (al87) 
                            goto addr_37d7_27; else 
                            goto addr_399e_95;
                    }
                    if (r14_79->f10) {
                        addr_399e_95:
                        al88 = detect_loop(r14_79, rsi24, rdx23, rcx22);
                        if (al88) 
                            goto addr_37d7_27; else 
                            goto addr_39ae_98;
                    } else {
                        al89 = detect_loop(r14_79, rsi24, rdx23, rcx22);
                        if (al89) {
                            goto addr_37d7_27;
                        }
                    }
                }
                al90 = detect_loop(rax78, rsi24, rdx23, rcx22);
                if (al90) 
                    goto addr_37d7_27;
                r14_43 = rax78->f10;
                if (!r14_43) {
                    addr_37fa_31:
                    al91 = detect_loop(r13_42, rsi24, rdx23, rcx22);
                    if (al91) 
                        goto addr_37d7_27;
                    rax92 = r13_42->f10;
                    if (!rax92) 
                        goto addr_37d7_27;
                } else {
                    rdi93 = r14_43->f8;
                    if (rdi93) {
                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                        al94 = recurse_tree(rdi93, detect_loop, rdx23, rcx22);
                        if (al94) 
                            goto addr_37d7_27; else 
                            goto addr_444c_106;
                    }
                    if (r14_43->f10) {
                        addr_444c_106:
                        al95 = detect_loop(r14_43, rsi24, rdx23, rcx22);
                        if (al95) 
                            goto addr_37d7_27; else 
                            goto addr_445c_108;
                    } else {
                        addr_4941_34:
                        al96 = detect_loop(r14_43, rsi24, rdx23, rcx22);
                        if (al96) {
                            goto addr_37d7_27;
                        }
                    }
                }
                r13_97 = rax92->f8;
                if (!r13_97) {
                    if (!rax92->f10) {
                        detect_loop(rax92, rsi24, rdx23, rcx22);
                        goto addr_37d7_27;
                    }
                }
                r14_98 = r13_97->f8;
                if (!r14_98) {
                    if (!r13_97->f10) 
                        goto addr_5aa5_115;
                } else {
                    r8_99 = r14_98->f8;
                    if (r8_99) {
                        rdi100 = r8_99->f8;
                        if (!rdi100) {
                            if (!r8_99->f10) {
                                al101 = detect_loop(r8_99, rsi24, rdx23, rcx22);
                                if (al101) {
                                    goto addr_37d7_27;
                                }
                            }
                        } else {
                            rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                            al102 = recurse_tree(rdi100, detect_loop, rdx23, rcx22);
                            r8_99 = r8_99;
                            if (al102) 
                                goto addr_37d7_27;
                        }
                        al103 = detect_loop(r8_99, rsi24, rdx23, rcx22);
                        if (al103) 
                            goto addr_37d7_27;
                        rdi104 = r8_99->f10;
                        if (!rdi104) 
                            goto addr_3895_124;
                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                        al105 = recurse_tree(rdi104, detect_loop, rdx23, rcx22);
                        if (al105) 
                            goto addr_37d7_27; else 
                            goto addr_3895_124;
                    }
                    if (r14_98->f10) {
                        addr_3895_124:
                        al106 = detect_loop(r14_98, rsi24, rdx23, rcx22);
                        if (al106) 
                            goto addr_37d7_27; else 
                            goto addr_38a5_127;
                    } else {
                        al107 = detect_loop(r14_98, rsi24, rdx23, rcx22);
                        if (al107) {
                            goto addr_37d7_27;
                        }
                    }
                }
                al108 = detect_loop(r13_97, rsi24, rdx23, rcx22);
                if (al108) 
                    goto addr_37d7_27;
                r13_97 = r13_97->f10;
                if (!r13_97) {
                    al109 = detect_loop(rax92, rsi24, rdx23, rcx22);
                    if (al109) 
                        goto addr_37d7_27;
                    r13_42 = rax92->f10;
                    if (!r13_42) 
                        goto addr_37d7_27;
                } else {
                    rdi110 = r13_97->f8;
                    if (rdi110) {
                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                        al111 = recurse_tree(rdi110, detect_loop, rdx23, rcx22);
                        if (al111) 
                            goto addr_37d7_27; else 
                            goto addr_460e_136;
                    }
                    if (r13_97->f10) {
                        addr_460e_136:
                        al112 = detect_loop(r13_97, rsi24, rdx23, rcx22);
                        if (al112) 
                            goto addr_37d7_27; else 
                            goto addr_461e_138;
                    } else {
                        addr_5aa5_115:
                        al113 = detect_loop(r13_97, rsi24, rdx23, rcx22);
                        if (al113) {
                            goto addr_37d7_27;
                        }
                    }
                }
                r14_114 = r13_42->f8;
                if (!r14_114) {
                    if (r13_42->f10) {
                        addr_439c_142:
                        al115 = detect_loop(r13_42, rsi24, rdx23, rcx22);
                        if (al115 || (r13_42 = r13_42->f10, r13_42 == 0)) {
                            addr_37d7_27:
                            zf116 = loop == 0;
                            if (!zf116) 
                                continue; else 
                                break;
                        } else {
                            rdi117 = r13_42->f8;
                            if (!rdi117) {
                                if (r13_42->f10) {
                                    addr_43d6_145:
                                    al118 = detect_loop(r13_42, rsi24, rdx23, rcx22);
                                    if (!al118 && (rdi119 = r13_42->f10, !!rdi119)) {
                                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                                        recurse_tree(rdi119, detect_loop, rdx23, rcx22);
                                        goto addr_37d7_27;
                                    }
                                } else {
                                    goto addr_464b_30;
                                }
                            } else {
                                rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                                al120 = recurse_tree(rdi117, detect_loop, rdx23, rcx22);
                                if (al120) 
                                    goto addr_37d7_27; else 
                                    goto addr_43d6_145;
                            }
                        }
                    } else {
                        addr_464b_30:
                        detect_loop(r13_42, rsi24, rdx23, rcx22);
                        goto addr_37d7_27;
                    }
                } else {
                    rdi121 = r14_114->f8;
                    if (!rdi121) {
                        if (!r14_114->f10) {
                            al122 = detect_loop(r14_114, rsi24, rdx23, rcx22);
                            if (al122) {
                                goto addr_37d7_27;
                            }
                        }
                    } else {
                        rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                        al123 = recurse_tree(rdi121, detect_loop, rdx23, rcx22);
                        if (al123) 
                            goto addr_37d7_27;
                    }
                    al124 = detect_loop(r14_114, rsi24, rdx23, rcx22);
                    if (al124) 
                        goto addr_37d7_27;
                    rdi125 = r14_114->f10;
                    if (!rdi125) 
                        goto addr_439c_142;
                    rsi24 = reinterpret_cast<struct s0*>(detect_loop);
                    al126 = recurse_tree(rdi125, detect_loop, rdx23, rcx22);
                    if (al126) 
                        goto addr_37d7_27; else 
                        goto addr_439c_142;
                }
                addr_461e_138:
                rdi127 = r13_97->f10;
                if (rdi127 && (rsi24 = reinterpret_cast<struct s0*>(detect_loop), al128 = recurse_tree(rdi127, detect_loop, rdx23, rcx22), !!al128)) {
                    goto addr_37d7_27;
                }
                addr_38a5_127:
                rdi129 = r14_98->f10;
                if (rdi129 && (rsi24 = reinterpret_cast<struct s0*>(detect_loop), al130 = recurse_tree(rdi129, detect_loop, rdx23, rcx22), !!al130)) {
                    goto addr_37d7_27;
                }
                addr_445c_108:
                rdi131 = r14_43->f10;
                if (rdi131 && (rsi24 = reinterpret_cast<struct s0*>(detect_loop), al132 = recurse_tree(rdi131, detect_loop, rdx23, rcx22), !!al132)) {
                    goto addr_37d7_27;
                }
                addr_39ae_98:
                rdi133 = r14_79->f10;
                if (rdi133 && (rsi24 = reinterpret_cast<struct s0*>(detect_loop), al134 = recurse_tree(rdi133, detect_loop, rdx23, rcx22), !!al134)) {
                    goto addr_37d7_27;
                }
                addr_42f3_80:
                rdi135 = v45->f10;
                if (rdi135 && (rsi24 = reinterpret_cast<struct s0*>(detect_loop), al136 = recurse_tree(rdi135, detect_loop, rdx23, rcx22), !!al136)) {
                    goto addr_37d7_27;
                }
                addr_4a8d_65:
                rdi137 = r8_61->f10;
                if (rdi137 && (rsi24 = reinterpret_cast<struct s0*>(detect_loop), al138 = recurse_tree(rdi137, detect_loop, rdx23, rcx22), !!al138)) {
                    goto addr_37d7_27;
                }
            }
            addr_31f4_21:
            zf139 = n_strings == 0;
            if (zf139) 
                break;
            r12_18 = rbp15->f10;
            if (!r12_18) {
                addr_33d7_165:
                rax140 = head;
                if (!rax140) 
                    continue;
            } else {
                r14_28 = r12_18->f8;
                if (!r14_28) {
                    r14_141 = r12_18->f10;
                    rax39 = r12_18->f20;
                    if (!r14_141) {
                        addr_33ce_25:
                        if (!rax39 && !r12_18->f19) {
                            zf142 = head == 0;
                            if (zf142) {
                                head = r12_18;
                            } else {
                                rax143 = zeros;
                                rax143->f28 = r12_18;
                            }
                            zeros = r12_18;
                            goto addr_33d7_165;
                        }
                    } else {
                        if (!rax39) {
                            addr_3357_173:
                            if (!r12_18->f19) {
                                zf144 = head == 0;
                                if (zf144) {
                                    head = r12_18;
                                } else {
                                    rax145 = zeros;
                                    rax145->f28 = r12_18;
                                }
                                zeros = r12_18;
                                goto addr_337f_178;
                            }
                        } else {
                            addr_348e_179:
                            r13_146 = r14_141->f8;
                            if (!r13_146) {
                                r12_18 = r14_141->f10;
                                rax147 = r14_141->f20;
                                if (!r12_18) 
                                    goto addr_4aaf_181;
                                if (rax147) 
                                    goto addr_3b21_183; else 
                                    goto addr_3af1_184;
                            } else {
                                rcx22 = r13_146->f8;
                                if (!rcx22) {
                                    r12_18 = r13_146->f10;
                                    rax148 = r13_146->f20;
                                    if (!r12_18) 
                                        goto addr_4d95_187;
                                    if (!rax148) 
                                        goto addr_41fd_189; else 
                                        goto addr_3db3_190;
                                } else {
                                    r8_149 = rcx22->f8;
                                    if (!r8_149) {
                                        r12_18 = rcx22->f10;
                                        if (!r12_18) {
                                            if (!rcx22->f20 && !rcx22->f19) {
                                                zf150 = head == 0;
                                                if (zf150) {
                                                    head = rcx22;
                                                } else {
                                                    rax151 = zeros;
                                                    rax151->f28 = rcx22;
                                                }
                                                zeros = rcx22;
                                                goto addr_41f2_198;
                                            }
                                        }
                                        if (!rcx22->f20) 
                                            goto addr_4734_200; else 
                                            goto addr_4672_201;
                                    } else {
                                        r12_18 = r8_149->f8;
                                        if (!r12_18) {
                                            r12_18 = r8_149->f10;
                                            if (!r12_18) {
                                                if (!r8_149->f20 && !r8_149->f19) {
                                                    zf152 = head == 0;
                                                    if (zf152) {
                                                        head = r8_149;
                                                    } else {
                                                        rax153 = zeros;
                                                        rax153->f28 = r8_149;
                                                    }
                                                    zeros = r8_149;
                                                    goto addr_41da_209;
                                                }
                                            }
                                            if (!r8_149->f20) 
                                                goto addr_353d_211; else 
                                                goto addr_4174_212;
                                        } else {
                                            rdi154 = r12_18->f8;
                                            if (!rdi154) {
                                                rdi155 = r12_18->f10;
                                                if (!rdi155) {
                                                    if (!r12_18->f20 && !r12_18->f19) {
                                                        zf156 = head == 0;
                                                        if (zf156) {
                                                            head = r12_18;
                                                        } else {
                                                            rax157 = zeros;
                                                            rax157->f28 = r12_18;
                                                        }
                                                        zeros = r12_18;
                                                        goto addr_3532_220;
                                                    }
                                                } else {
                                                    if (!r12_18->f20) 
                                                        goto addr_3501_222; else 
                                                        goto addr_538a_223;
                                                }
                                            } else {
                                                rsi24 = rbx20;
                                                al158 = recurse_tree(rdi154, rsi24, rdx23, rcx22);
                                                if (al158) 
                                                    goto addr_3388_225;
                                                rcx22 = rcx22;
                                                r8_149 = r8_149;
                                                rdi155 = r12_18->f10;
                                                if (r12_18->f20) 
                                                    goto addr_3529_227; else 
                                                    goto addr_3501_222;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    r13_19 = r14_28->f8;
                    if (!r13_19) {
                        r13_159 = r14_28->f10;
                        rax160 = r14_28->f20;
                        if (!r13_159) 
                            goto addr_4838_230;
                        if (rax160) 
                            goto addr_35be_232; else 
                            goto addr_358e_233;
                    } else {
                        r8_161 = r13_19->f8;
                        if (!r8_161) {
                            rcx22 = r13_19->f10;
                            rax162 = r13_19->f20;
                            if (!rcx22) 
                                goto addr_4956_236;
                            if (rax162) 
                                goto addr_3a16_238; else 
                                goto addr_39e6_239;
                        } else {
                            r9_163 = r8_161->f8;
                            if (!r9_163) {
                                rcx22 = r8_161->f10;
                                rax164 = r8_161->f20;
                                if (!rcx22) 
                                    goto addr_4ded_242;
                                if (!rax164) 
                                    goto addr_32ea_244; else 
                                    goto addr_3c6f_245;
                            } else {
                                rcx22 = r9_163->f8;
                                if (!rcx22) {
                                    rcx22 = r9_163->f10;
                                    if (!rcx22) {
                                        if (!r9_163->f20 && !r9_163->f19) {
                                            zf165 = head == 0;
                                            if (zf165) {
                                                head = r9_163;
                                            } else {
                                                rax166 = zeros;
                                                rax166->f28 = r9_163;
                                            }
                                            zeros = r9_163;
                                            goto addr_32df_253;
                                        }
                                    }
                                    if (!r9_163->f20) 
                                        goto addr_32af_255; else 
                                        goto addr_48ac_256;
                                } else {
                                    rdi167 = rcx22->f8;
                                    if (!rdi167) {
                                        rdi168 = rcx22->f10;
                                        if (!rdi168) {
                                            if (!rcx22->f20 && !rcx22->f19) {
                                                zf169 = head == 0;
                                                if (zf169) {
                                                    head = rcx22;
                                                } else {
                                                    rax170 = zeros;
                                                    rax170->f28 = rcx22;
                                                }
                                                zeros = rcx22;
                                                goto addr_32a4_264;
                                            }
                                        } else {
                                            if (!rcx22->f20) 
                                                goto addr_4057_266; else 
                                                goto addr_4b7a_267;
                                        }
                                    } else {
                                        rsi24 = rbx20;
                                        al171 = recurse_tree(rdi167, rsi24, rdx23, rcx22);
                                        if (al171) 
                                            goto addr_3388_225;
                                        rcx22 = rcx22;
                                        r8_161 = r8_161;
                                        r9_163 = r9_163;
                                        rdi168 = rcx22->f10;
                                        if (!rcx22->f20) 
                                            goto addr_4057_266; else 
                                            goto addr_329b_270;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            goto addr_33f0_272;
            addr_337f_178:
            if (r14_141) 
                goto addr_348e_179; else 
                goto addr_3388_225;
            addr_3b21_183:
            r13_172 = r12_18->f8;
            if (!r13_172) {
                r14_141 = r12_18->f10;
                if (!r14_141) {
                    rdx23 = head;
                    if (!r12_18->f20 && !r12_18->f19) {
                        if (!rdx23) {
                            head = r12_18;
                        } else {
                            rax173 = zeros;
                            rax173->f28 = r12_18;
                        }
                        zeros = r12_18;
                        goto addr_3388_225;
                    }
                }
                if (!r12_18->f20) 
                    goto addr_3f75_280;
            } else {
                r14_174 = r13_172->f8;
                if (!r14_174) {
                    r14_175 = r13_172->f10;
                    if (!r14_175) {
                        if (!r13_172->f20 && !r13_172->f19) {
                            zf176 = head == 0;
                            if (zf176) {
                                head = r13_172;
                            } else {
                                rax177 = zeros;
                                rax177->f28 = r13_172;
                            }
                            zeros = r13_172;
                            goto addr_3f68_289;
                        }
                    }
                    if (!r13_172->f20) 
                        goto addr_4c75_291; else 
                        goto addr_3f10_292;
                } else {
                    rcx22 = r14_174->f8;
                    if (!rcx22) {
                        rdi178 = r14_174->f10;
                        if (!rdi178) {
                            if (!r14_174->f20 && !r14_174->f19) {
                                zf179 = head == 0;
                                if (zf179) {
                                    head = r14_174;
                                } else {
                                    rax180 = zeros;
                                    rax180->f28 = r14_174;
                                }
                                zeros = r14_174;
                                goto addr_4c6a_300;
                            }
                        } else {
                            if (!r14_174->f20) 
                                goto addr_4c3a_302; else 
                                goto addr_5288_303;
                        }
                    }
                    rdi181 = rcx22->f8;
                    if (!rdi181) 
                        goto addr_4c05_305; else 
                        goto addr_3b56_306;
                }
            }
            addr_3fa6_307:
            r12_18 = r14_141->f8;
            if (!r12_18) {
                rdi182 = r14_141->f10;
                rax147 = r14_141->f20;
                if (!rdi182) {
                    addr_4aaf_181:
                    rdx23 = head;
                    if (!rax147 && !r14_141->f19) {
                        if (!rdx23) {
                            head = r14_141;
                        } else {
                            rax183 = zeros;
                            rax183->f28 = r14_141;
                        }
                        zeros = r14_141;
                        goto addr_3388_225;
                    }
                } else {
                    if (rax147) {
                        goto addr_404a_315;
                    }
                }
            }
            rdi184 = r12_18->f8;
            if (!rdi184) {
                rdi185 = r12_18->f10;
                if (!rdi185) {
                    if (!r12_18->f20 && !r12_18->f19) {
                        zf186 = head == 0;
                        if (zf186) {
                            head = r12_18;
                        } else {
                            rax187 = zeros;
                            rax187->f28 = r12_18;
                        }
                        zeros = r12_18;
                        goto addr_400f_323;
                    }
                } else {
                    if (r12_18->f20) 
                        goto addr_5103_325;
                }
            } else {
                rsi24 = rbx20;
                al188 = recurse_tree(rdi184, rsi24, rdx23, rcx22);
                if (al188) 
                    goto addr_3388_225;
                rdi185 = r12_18->f10;
                if (r12_18->f20) 
                    goto addr_4006_328;
            }
            if (!r12_18->f19) {
                zf189 = head == 0;
                if (zf189) {
                    head = r12_18;
                } else {
                    rax190 = zeros;
                    rax190->f28 = r12_18;
                }
                zeros = r12_18;
            }
            addr_4006_328:
            if (rdi185) {
                addr_5103_325:
                rsi24 = rbx20;
                al191 = recurse_tree(rdi185, rsi24, rdx23, rcx22);
                if (al191) {
                    goto addr_3388_225;
                }
            } else {
                addr_400f_323:
                rdi182 = r14_141->f10;
                if (r14_141->f20) {
                    addr_4041_335:
                    if (!rdi182) {
                        addr_3388_225:
                        rax140 = head;
                        if (rax140) {
                            while (1) {
                                addr_33f0_272:
                                rdi192 = *reinterpret_cast<struct s0**>(&rax140->f0);
                                r12_18 = rax140->f30;
                                fun_23d0(rdi192, rsi24, rdx23, rcx22);
                                rax193 = n_strings;
                                r8_194 = head;
                                *reinterpret_cast<int32_t*>(&rsi24) = 0;
                                *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                                rcx22 = zeros;
                                rdi195 = rax193 - 1;
                                r8_194->f19 = 1;
                                n_strings = rdi195;
                                if (!r12_18) {
                                    addr_345d_336:
                                    rax140 = r8_194->f28;
                                    head = rax140;
                                    if (rax140) 
                                        continue; else 
                                        break;
                                } else {
                                    do {
                                        rdx23 = *reinterpret_cast<struct s0**>(&r12_18->f0);
                                        rdx23->f20 = rdx23->f20 - 1;
                                        if (!rdx23->f20) {
                                            rcx22->f28 = rdx23;
                                            *reinterpret_cast<int32_t*>(&rsi24) = 1;
                                            *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
                                            rcx22 = rdx23;
                                        }
                                        r12_18 = r12_18->f8;
                                    } while (r12_18);
                                    if (!*reinterpret_cast<signed char*>(&rsi24)) 
                                        goto addr_345d_336;
                                }
                                zeros = rcx22;
                                goto addr_345d_336;
                            }
                        } else {
                            rdi195 = n_strings;
                            goto addr_33a0_344;
                        }
                    } else {
                        addr_404a_315:
                        rsi24 = rbx20;
                        recurse_tree(rdi182, rsi24, rdx23, rcx22);
                        goto addr_3388_225;
                    }
                } else {
                    if (!r14_141->f19) {
                        zf196 = head == 0;
                        if (zf196) {
                            head = r14_141;
                        } else {
                            rax197 = zeros;
                            rax197->f28 = r14_141;
                        }
                        zeros = r14_141;
                        goto addr_4041_335;
                    }
                }
            }
            addr_33a0_344:
            if (rdi195) 
                continue; else 
                break;
            addr_3f10_292:
            rdi198 = r14_175->f8;
            if (!rdi198) {
                rdi199 = r14_175->f10;
                if (!rdi199) {
                    if (!r14_175->f20 && !r14_175->f19) {
                        zf200 = head == 0;
                        if (zf200) {
                            head = r14_175;
                        } else {
                            rax201 = zeros;
                            rax201->f28 = r14_175;
                        }
                        zeros = r14_175;
                        goto addr_3f68_289;
                    }
                } else {
                    if (r14_175->f20) 
                        goto addr_501a_358;
                }
            } else {
                rsi24 = rbx20;
                al202 = recurse_tree(rdi198, rsi24, rdx23, rcx22);
                if (al202) 
                    goto addr_3388_225;
                rdi199 = r14_175->f10;
                if (r14_175->f20) 
                    goto addr_3f5f_361;
            }
            if (!r14_175->f19) {
                zf203 = head == 0;
                if (zf203) {
                    head = r14_175;
                } else {
                    rax204 = zeros;
                    rax204->f28 = r14_175;
                }
                zeros = r14_175;
            }
            addr_3f5f_361:
            if (rdi199) {
                addr_501a_358:
                rsi24 = rbx20;
                al205 = recurse_tree(rdi199, rsi24, rdx23, rcx22);
                if (al205) {
                    goto addr_3388_225;
                }
            } else {
                addr_3f68_289:
                r14_141 = r12_18->f10;
                if (r12_18->f20) {
                    addr_3f9d_368:
                    if (!r14_141) 
                        goto addr_3388_225; else 
                        goto addr_3fa6_307;
                } else {
                    addr_3f75_280:
                    if (!r12_18->f19) {
                        zf206 = head == 0;
                        if (zf206) {
                            head = r12_18;
                        } else {
                            rax207 = zeros;
                            rax207->f28 = r12_18;
                        }
                        zeros = r12_18;
                        goto addr_3f9d_368;
                    }
                }
            }
            addr_4c05_305:
            rdi208 = rcx22->f10;
            if (!rdi208) {
                if (!rcx22->f20 && !rcx22->f19) {
                    zf209 = head == 0;
                    if (zf209) {
                        head = rcx22;
                    } else {
                        rax210 = zeros;
                        rax210->f28 = rcx22;
                    }
                    zeros = rcx22;
                    goto addr_4c2f_378;
                }
            }
            if (!rcx22->f20) {
                addr_3b7b_380:
                if (!rcx22->f19) {
                    zf211 = head == 0;
                    if (zf211) {
                        head = rcx22;
                    } else {
                        rax212 = zeros;
                        rax212->f28 = rcx22;
                    }
                    zeros = rcx22;
                }
            } else {
                addr_4c1f_385:
                rsi24 = rbx20;
                al213 = recurse_tree(rdi208, rsi24, rdx23, rcx22);
                if (al213) 
                    goto addr_3388_225; else 
                    goto addr_4c2f_378;
            }
            addr_3ba1_386:
            if (!rdi208) {
                addr_4c2f_378:
                rdi178 = r14_174->f10;
                if (r14_174->f20) {
                    addr_4c61_387:
                    if (rdi178) {
                        addr_5288_303:
                        rsi24 = rbx20;
                        al214 = recurse_tree(rdi178, rsi24, rdx23, rcx22);
                        if (al214) {
                            goto addr_3388_225;
                        }
                    } else {
                        addr_4c6a_300:
                        r14_175 = r13_172->f10;
                        if (r13_172->f20) {
                            addr_4c9c_389:
                            if (!r14_175) 
                                goto addr_3f68_289;
                        } else {
                            addr_4c75_291:
                            if (!r13_172->f19) {
                                zf215 = head == 0;
                                if (zf215) {
                                    head = r13_172;
                                } else {
                                    rax216 = zeros;
                                    rax216->f28 = r13_172;
                                }
                                zeros = r13_172;
                                goto addr_4c9c_389;
                            }
                        }
                    }
                } else {
                    addr_4c3a_302:
                    if (!r14_174->f19) {
                        zf217 = head == 0;
                        if (zf217) {
                            head = r14_174;
                        } else {
                            rax218 = zeros;
                            rax218->f28 = r14_174;
                        }
                        zeros = r14_174;
                        goto addr_4c61_387;
                    }
                }
            } else {
                goto addr_4c1f_385;
            }
            goto addr_3f10_292;
            addr_3b56_306:
            rsi24 = rbx20;
            al219 = recurse_tree(rdi181, rsi24, rdx23, rcx22);
            if (al219) 
                goto addr_3388_225;
            rcx22 = rcx22;
            rdi208 = rcx22->f10;
            if (rcx22->f20) 
                goto addr_3ba1_386; else 
                goto addr_3b7b_380;
            addr_3af1_184:
            if (!r14_141->f19) {
                zf220 = head == 0;
                if (zf220) {
                    head = r14_141;
                } else {
                    rax221 = zeros;
                    rax221->f28 = r14_141;
                }
                zeros = r14_141;
                goto addr_3b18_405;
            }
            addr_3db3_190:
            r13_222 = r12_18->f8;
            if (!r13_222) {
                r13_146 = r12_18->f10;
                if (!r13_146) {
                    if (!r12_18->f20 && !r12_18->f19) {
                        zf223 = head == 0;
                        if (zf223) {
                            head = r12_18;
                        } else {
                            rax224 = zeros;
                            rax224->f28 = r12_18;
                        }
                        zeros = r12_18;
                        goto addr_422d_412;
                    }
                }
                if (!r12_18->f20) 
                    goto addr_3e77_414;
            } else {
                rcx22 = r13_222->f8;
                if (!rcx22) {
                    rdi225 = r13_222->f10;
                    if (!rdi225) {
                        if (!r13_222->f20 && !r13_222->f19) {
                            zf226 = head == 0;
                            if (zf226) {
                                head = r13_222;
                            } else {
                                rax227 = zeros;
                                rax227->f28 = r13_222;
                            }
                            zeros = r13_222;
                            goto addr_3e6a_422;
                        }
                    } else {
                        if (!r13_222->f20) 
                            goto addr_3e3a_424; else 
                            goto addr_5c86_425;
                    }
                }
                rdi228 = rcx22->f8;
                if (!rdi228) 
                    goto addr_5c3d_427; else 
                    goto addr_3ddb_428;
            }
            addr_49ab_429:
            rdi229 = r13_146->f8;
            if (!rdi229) {
                rdi230 = r13_146->f10;
                rax148 = r13_146->f20;
                if (!rdi230) {
                    addr_4d95_187:
                    if (!rax148 && !r13_146->f19) {
                        zf231 = head == 0;
                        if (zf231) {
                            head = r13_146;
                        } else {
                            rax232 = zeros;
                            rax232->f28 = r13_146;
                        }
                        zeros = r13_146;
                        goto addr_422d_412;
                    }
                } else {
                    if (rax148) 
                        goto addr_4a03_436;
                }
            } else {
                rsi24 = rbx20;
                al233 = recurse_tree(rdi229, rsi24, rdx23, rcx22);
                if (al233) 
                    goto addr_3388_225;
                rdi230 = r13_146->f10;
                if (r13_146->f20) 
                    goto addr_49fa_440;
            }
            if (!r13_146->f19) {
                zf234 = head == 0;
                if (zf234) {
                    head = r13_146;
                } else {
                    rax235 = zeros;
                    rax235->f28 = r13_146;
                }
                zeros = r13_146;
            }
            addr_49fa_440:
            if (!rdi230) {
                addr_422d_412:
                r12_18 = r14_141->f10;
                if (r14_141->f20) {
                    addr_3b18_405:
                    if (!r12_18) 
                        goto addr_3388_225; else 
                        goto addr_3b21_183;
                } else {
                    goto addr_3af1_184;
                }
            } else {
                addr_4a03_436:
                rsi24 = rbx20;
                al236 = recurse_tree(rdi230, rsi24, rdx23, rcx22);
                if (al236) {
                    goto addr_3388_225;
                }
            }
            addr_5c3d_427:
            rdi237 = rcx22->f10;
            if (!rdi237) {
                if (!rcx22->f20 && !rcx22->f19) {
                    zf238 = head == 0;
                    if (zf238) {
                        head = rcx22;
                    } else {
                        rax239 = zeros;
                        rax239->f28 = rcx22;
                    }
                    zeros = rcx22;
                    goto addr_3e2f_453;
                }
            } else {
                if (rcx22->f20) 
                    goto addr_5c57_455;
            }
            addr_3e00_456:
            if (!rcx22->f19) {
                zf240 = head == 0;
                if (zf240) {
                    head = rcx22;
                } else {
                    rax241 = zeros;
                    rax241->f28 = rcx22;
                }
                zeros = rcx22;
            }
            addr_3e26_461:
            if (rdi237) {
                addr_5c57_455:
                rsi24 = rbx20;
                al242 = recurse_tree(rdi237, rsi24, rdx23, rcx22);
                if (al242) {
                    goto addr_3388_225;
                }
            } else {
                addr_3e2f_453:
                rdi225 = r13_222->f10;
                if (r13_222->f20) {
                    addr_3e61_463:
                    if (rdi225) {
                        addr_5c86_425:
                        rsi24 = rbx20;
                        al243 = recurse_tree(rdi225, rsi24, rdx23, rcx22);
                        if (al243) {
                            goto addr_3388_225;
                        }
                    } else {
                        addr_3e6a_422:
                        r13_146 = r12_18->f10;
                        if (r12_18->f20) {
                            addr_3e9f_465:
                            if (!r13_146) 
                                goto addr_422d_412;
                        } else {
                            addr_3e77_414:
                            if (!r12_18->f19) {
                                zf244 = head == 0;
                                if (zf244) {
                                    head = r12_18;
                                } else {
                                    rax245 = zeros;
                                    rax245->f28 = r12_18;
                                }
                                zeros = r12_18;
                                goto addr_3e9f_465;
                            }
                        }
                    }
                } else {
                    addr_3e3a_424:
                    if (!r13_222->f19) {
                        zf246 = head == 0;
                        if (zf246) {
                            head = r13_222;
                        } else {
                            rax247 = zeros;
                            rax247->f28 = r13_222;
                        }
                        zeros = r13_222;
                        goto addr_3e61_463;
                    }
                }
            }
            goto addr_49ab_429;
            addr_3ddb_428:
            rsi24 = rbx20;
            al248 = recurse_tree(rdi228, rsi24, rdx23, rcx22);
            if (al248) 
                goto addr_3388_225;
            rcx22 = rcx22;
            rdi237 = rcx22->f10;
            if (rcx22->f20) 
                goto addr_3e26_461; else 
                goto addr_3e00_456;
            addr_4672_201:
            rcx22 = r12_18->f8;
            if (!rcx22) {
                rdi249 = r12_18->f10;
                if (!rdi249) {
                    if (!r12_18->f20 && !r12_18->f19) {
                        zf250 = head == 0;
                        if (zf250) {
                            head = r12_18;
                        } else {
                            rax251 = zeros;
                            rax251->f28 = r12_18;
                        }
                        zeros = r12_18;
                        goto addr_41f2_198;
                    }
                } else {
                    if (r12_18->f20) {
                        goto addr_471f_484;
                    }
                }
            }
            rdi252 = rcx22->f8;
            if (!rdi252) {
                rdi253 = rcx22->f10;
                if (!rdi253) {
                    if (!rcx22->f20 && !rcx22->f19) {
                        zf254 = head == 0;
                        if (zf254) {
                            head = rcx22;
                        } else {
                            rax255 = zeros;
                            rax255->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_46e1_492;
                    }
                } else {
                    if (rcx22->f20) 
                        goto addr_57b9_494;
                }
            } else {
                rsi24 = rbx20;
                al256 = recurse_tree(rdi252, rsi24, rdx23, rcx22);
                if (al256) 
                    goto addr_3388_225;
                rcx22 = rcx22;
                rdi253 = rcx22->f10;
                if (rcx22->f20) 
                    goto addr_46d8_497;
            }
            if (!rcx22->f19) {
                zf257 = head == 0;
                if (zf257) {
                    head = rcx22;
                } else {
                    rax258 = zeros;
                    rax258->f28 = rcx22;
                }
                zeros = rcx22;
            }
            addr_46d8_497:
            if (rdi253) {
                addr_57b9_494:
                rsi24 = rbx20;
                al259 = recurse_tree(rdi253, rsi24, rdx23, rcx22);
                if (al259) {
                    goto addr_3388_225;
                }
            } else {
                addr_46e1_492:
                rdi249 = r12_18->f10;
                if (r12_18->f20) {
                    addr_4716_504:
                    if (!rdi249) {
                        addr_41f2_198:
                        r12_18 = r13_146->f10;
                        if (r13_146->f20) {
                            addr_4224_505:
                            if (r12_18) 
                                goto addr_3db3_190; else 
                                goto addr_422d_412;
                        } else {
                            addr_41fd_189:
                            if (!r13_146->f19) {
                                zf260 = head == 0;
                                if (zf260) {
                                    head = r13_146;
                                } else {
                                    rax261 = zeros;
                                    rax261->f28 = r13_146;
                                }
                                zeros = r13_146;
                                goto addr_4224_505;
                            }
                        }
                    } else {
                        addr_471f_484:
                        rsi24 = rbx20;
                        al262 = recurse_tree(rdi249, rsi24, rdx23, rcx22);
                        if (al262) {
                            goto addr_3388_225;
                        }
                    }
                } else {
                    if (!r12_18->f19) {
                        zf263 = head == 0;
                        if (zf263) {
                            head = r12_18;
                        } else {
                            rax264 = zeros;
                            rax264->f28 = r12_18;
                        }
                        zeros = r12_18;
                        goto addr_4716_504;
                    }
                }
            }
            addr_4174_212:
            rdi265 = r12_18->f8;
            if (!rdi265) {
                rdi266 = r12_18->f10;
                if (!rdi266) {
                    if (!r12_18->f20 && !r12_18->f19) {
                        zf267 = head == 0;
                        if (zf267) {
                            head = r12_18;
                        } else {
                            rax268 = zeros;
                            rax268->f28 = r12_18;
                        }
                        zeros = r12_18;
                        goto addr_41da_209;
                    }
                } else {
                    if (r12_18->f20) 
                        goto addr_513c_523;
                }
            } else {
                rsi24 = rbx20;
                al269 = recurse_tree(rdi265, rsi24, rdx23, rcx22);
                if (al269) 
                    goto addr_3388_225;
                rcx22 = rcx22;
                rdi266 = r12_18->f10;
                if (r12_18->f20) 
                    goto addr_41d1_526;
            }
            if (!r12_18->f19) {
                zf270 = head == 0;
                if (zf270) {
                    head = r12_18;
                } else {
                    rax271 = zeros;
                    rax271->f28 = r12_18;
                }
                zeros = r12_18;
            }
            addr_41d1_526:
            if (rdi266) {
                addr_513c_523:
                rsi24 = rbx20;
                al272 = recurse_tree(rdi266, rsi24, rdx23, rcx22);
                rcx22 = rcx22;
                if (al272) {
                    goto addr_3388_225;
                }
            } else {
                addr_41da_209:
                r12_18 = rcx22->f10;
                if (!rcx22->f20) {
                    addr_4734_200:
                    if (!rcx22->f19) {
                        zf273 = head == 0;
                        if (zf273) {
                            head = rcx22;
                        } else {
                            rax274 = zeros;
                            rax274->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_41e9_537;
                    }
                } else {
                    addr_41e9_537:
                    if (r12_18) 
                        goto addr_4672_201; else 
                        goto addr_41f2_198;
                }
            }
            addr_3501_222:
            if (!r12_18->f19) {
                zf275 = head == 0;
                if (zf275) {
                    head = r12_18;
                } else {
                    rax276 = zeros;
                    rax276->f28 = r12_18;
                }
                zeros = r12_18;
            }
            addr_3529_227:
            if (rdi155) {
                addr_538a_223:
                rsi24 = rbx20;
                al277 = recurse_tree(rdi155, rsi24, rdx23, rcx22);
                rcx22 = rcx22;
                r8_149 = r8_149;
                if (al277) {
                    goto addr_3388_225;
                }
            } else {
                addr_3532_220:
                r12_18 = r8_149->f10;
                if (r8_149->f20) {
                    addr_3564_543:
                    if (!r12_18) 
                        goto addr_41da_209;
                } else {
                    addr_353d_211:
                    if (!r8_149->f19) {
                        zf278 = head == 0;
                        if (zf278) {
                            head = r8_149;
                        } else {
                            rax279 = zeros;
                            rax279->f28 = r8_149;
                        }
                        zeros = r8_149;
                        goto addr_3564_543;
                    }
                }
            }
            goto addr_4174_212;
            addr_35be_232:
            rcx22 = r13_159->f8;
            if (!rcx22) {
                r14_28 = r13_159->f10;
                rax280 = r13_159->f20;
                if (!r14_28) 
                    goto addr_4e26_550;
                if (!rax280) 
                    goto addr_4125_552;
            } else {
                r8_281 = rcx22->f8;
                if (!r8_281) {
                    r14_282 = rcx22->f10;
                    if (!r14_282) {
                        if (!rcx22->f20 && !rcx22->f19) {
                            zf283 = head == 0;
                            if (zf283) {
                                head = rcx22;
                            } else {
                                rax284 = zeros;
                                rax284->f28 = rcx22;
                            }
                            zeros = rcx22;
                            goto addr_411a_560;
                        }
                    }
                    if (!rcx22->f20) 
                        goto addr_47f8_562; else 
                        goto addr_4785_563;
                } else {
                    r14_285 = r8_281->f8;
                    if (!r14_285) {
                        r14_286 = r8_281->f10;
                        if (!r14_286) {
                            if (!r8_281->f20 && !r8_281->f19) {
                                zf287 = head == 0;
                                if (zf287) {
                                    head = r8_281;
                                } else {
                                    rax288 = zeros;
                                    rax288->f28 = r8_281;
                                }
                                zeros = r8_281;
                                goto addr_4102_571;
                            }
                        }
                        if (!r8_281->f20) 
                            goto addr_365c_573; else 
                            goto addr_40a0_574;
                    } else {
                        rdi289 = r14_285->f8;
                        if (!rdi289) {
                            rdi290 = r14_285->f10;
                            if (!rdi290) {
                                if (!r14_285->f20 && !r14_285->f19) {
                                    zf291 = head == 0;
                                    if (zf291) {
                                        head = r14_285;
                                    } else {
                                        rax292 = zeros;
                                        rax292->f28 = r14_285;
                                    }
                                    zeros = r14_285;
                                    goto addr_3651_582;
                                }
                            } else {
                                if (!r14_285->f20) 
                                    goto addr_3621_584; else 
                                    goto addr_540e_585;
                            }
                        } else {
                            rsi24 = rbx20;
                            al293 = recurse_tree(rdi289, rsi24, rdx23, rcx22);
                            if (al293) 
                                goto addr_3388_225;
                            rcx22 = rcx22;
                            r8_281 = r8_281;
                            rdi290 = r14_285->f10;
                            if (r14_285->f20) 
                                goto addr_3648_588; else 
                                goto addr_3621_584;
                        }
                    }
                }
            }
            addr_3bd5_589:
            r13_294 = r14_28->f8;
            if (!r13_294) {
                r13_159 = r14_28->f10;
                rax160 = r14_28->f20;
                if (r13_159) {
                    if (!rax160) {
                        addr_4d60_592:
                        if (!r14_28->f19) {
                            zf295 = head == 0;
                            if (zf295) {
                                head = r14_28;
                            } else {
                                rax296 = zeros;
                                rax296->f28 = r14_28;
                            }
                            zeros = r14_28;
                        }
                    } else {
                        addr_4af3_597:
                        rdi297 = r13_159->f8;
                        if (!rdi297) {
                            rdi298 = r13_159->f10;
                            rax280 = r13_159->f20;
                            if (!rdi298) {
                                addr_4e26_550:
                                if (!rax280 && !r13_159->f19) {
                                    zf299 = head == 0;
                                    if (zf299) {
                                        head = r13_159;
                                    } else {
                                        rax300 = zeros;
                                        rax300->f28 = r13_159;
                                    }
                                    zeros = r13_159;
                                    goto addr_334a_603;
                                }
                            } else {
                                if (rax280) 
                                    goto addr_4b4b_605;
                                goto addr_4b1b_607;
                            }
                        } else {
                            rsi24 = rbx20;
                            al301 = recurse_tree(rdi297, rsi24, rdx23, rcx22);
                            if (al301) 
                                goto addr_3388_225;
                            rdi298 = r13_159->f10;
                            if (r13_159->f20) 
                                goto addr_4b42_610; else 
                                goto addr_4b1b_607;
                        }
                    }
                } else {
                    addr_4838_230:
                    if (!rax160 && !r14_28->f19) {
                        zf302 = head == 0;
                        if (zf302) {
                            head = r14_28;
                        } else {
                            rax303 = zeros;
                            rax303->f28 = r14_28;
                        }
                        zeros = r14_28;
                        goto addr_334a_603;
                    }
                }
            } else {
                rcx22 = r13_294->f8;
                if (!rcx22) {
                    rdi304 = r13_294->f10;
                    if (!rdi304) {
                        if (!r13_294->f20 && !r13_294->f19) {
                            zf305 = head == 0;
                            if (zf305) {
                                head = r13_294;
                            } else {
                                rax306 = zeros;
                                rax306->f28 = r13_294;
                            }
                            zeros = r13_294;
                            goto addr_4d55_622;
                        }
                    } else {
                        if (!r13_294->f20) 
                            goto addr_4d25_624; else 
                            goto addr_5c10_625;
                    }
                }
                rdi307 = rcx22->f8;
                if (!rdi307) 
                    goto addr_4cf0_627; else 
                    goto addr_3bfc_628;
            }
            addr_4d87_629:
            if (!r13_159) 
                goto addr_334a_603;
            goto addr_4af3_597;
            addr_4b1b_607:
            if (!r13_159->f19) {
                zf308 = head == 0;
                if (zf308) {
                    head = r13_159;
                } else {
                    rax309 = zeros;
                    rax309->f28 = r13_159;
                }
                zeros = r13_159;
            }
            addr_4b42_610:
            if (!rdi298) {
                addr_334a_603:
                r14_141 = r12_18->f10;
                if (r12_18->f20) 
                    goto addr_337f_178; else 
                    goto addr_3357_173;
            } else {
                addr_4b4b_605:
                rsi24 = rbx20;
                al310 = recurse_tree(rdi298, rsi24, rdx23, rcx22);
                if (al310) {
                    goto addr_3388_225;
                }
            }
            addr_4cf0_627:
            rdi311 = rcx22->f10;
            if (!rdi311) {
                if (!rcx22->f20 && !rcx22->f19) {
                    zf312 = head == 0;
                    if (zf312) {
                        head = rcx22;
                    } else {
                        rax313 = zeros;
                        rax313->f28 = rcx22;
                    }
                    zeros = rcx22;
                    goto addr_4d1a_641;
                }
            }
            if (!rcx22->f20) {
                addr_3c21_643:
                if (!rcx22->f19) {
                    zf314 = head == 0;
                    if (zf314) {
                        head = rcx22;
                    } else {
                        rax315 = zeros;
                        rax315->f28 = rcx22;
                    }
                    zeros = rcx22;
                }
            } else {
                addr_4d0a_648:
                rsi24 = rbx20;
                al316 = recurse_tree(rdi311, rsi24, rdx23, rcx22);
                if (al316) 
                    goto addr_3388_225; else 
                    goto addr_4d1a_641;
            }
            addr_3c47_649:
            if (!rdi311) {
                addr_4d1a_641:
                rdi304 = r13_294->f10;
                if (r13_294->f20) {
                    addr_4d4c_650:
                    if (rdi304) {
                        addr_5c10_625:
                        rsi24 = rbx20;
                        al317 = recurse_tree(rdi304, rsi24, rdx23, rcx22);
                        if (al317) {
                            goto addr_3388_225;
                        }
                    } else {
                        addr_4d55_622:
                        r13_159 = r14_28->f10;
                        if (r14_28->f20) 
                            goto addr_4d87_629; else 
                            goto addr_4d60_592;
                    }
                } else {
                    addr_4d25_624:
                    if (!r13_294->f19) {
                        zf318 = head == 0;
                        if (zf318) {
                            head = r13_294;
                        } else {
                            rax319 = zeros;
                            rax319->f28 = r13_294;
                        }
                        zeros = r13_294;
                        goto addr_4d4c_650;
                    }
                }
            } else {
                goto addr_4d0a_648;
            }
            addr_3bfc_628:
            rsi24 = rbx20;
            al320 = recurse_tree(rdi307, rsi24, rdx23, rcx22);
            if (al320) 
                goto addr_3388_225;
            rcx22 = rcx22;
            rdi311 = rcx22->f10;
            if (rcx22->f20) 
                goto addr_3c47_649; else 
                goto addr_3c21_643;
            addr_4785_563:
            rcx22 = r14_282->f8;
            if (!rcx22) {
                rdi321 = r14_282->f10;
                if (!rdi321) {
                    if (!r14_282->f20 && !r14_282->f19) {
                        zf322 = head == 0;
                        if (zf322) {
                            head = r14_282;
                        } else {
                            rax323 = zeros;
                            rax323->f28 = r14_282;
                        }
                        zeros = r14_282;
                        goto addr_411a_560;
                    }
                } else {
                    if (r14_282->f20) {
                        goto addr_583b_666;
                    }
                }
            }
            rdi324 = rcx22->f8;
            if (!rdi324) {
                rdi325 = rcx22->f10;
                if (!rdi325) {
                    if (!rcx22->f20 && !rcx22->f19) {
                        zf326 = head == 0;
                        if (zf326) {
                            head = rcx22;
                        } else {
                            rax327 = zeros;
                            rax327->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_5800_674;
                    }
                }
                if (rcx22->f20) 
                    goto addr_57f0_676;
            } else {
                rsi24 = rbx20;
                al328 = recurse_tree(rdi324, rsi24, rdx23, rcx22);
                if (al328) 
                    goto addr_3388_225;
                rcx22 = rcx22;
                rdi325 = rcx22->f10;
                if (rcx22->f20) 
                    goto addr_47ea_679;
            }
            if (!rcx22->f19) {
                zf329 = head == 0;
                if (zf329) {
                    head = rcx22;
                } else {
                    rax330 = zeros;
                    rax330->f28 = rcx22;
                }
                zeros = rcx22;
            }
            addr_47ea_679:
            if (!rdi325) {
                addr_5800_674:
                rdi321 = r14_282->f10;
                if (r14_282->f20) {
                    addr_5832_685:
                    if (!rdi321) {
                        addr_411a_560:
                        r14_28 = r13_159->f10;
                        if (r13_159->f20) {
                            addr_414c_686:
                            if (!r14_28) 
                                goto addr_334a_603;
                        } else {
                            addr_4125_552:
                            if (!r13_159->f19) {
                                zf331 = head == 0;
                                if (zf331) {
                                    head = r13_159;
                                } else {
                                    rax332 = zeros;
                                    rax332->f28 = r13_159;
                                }
                                zeros = r13_159;
                                goto addr_414c_686;
                            }
                        }
                    } else {
                        addr_583b_666:
                        rsi24 = rbx20;
                        al333 = recurse_tree(rdi321, rsi24, rdx23, rcx22);
                        if (al333) {
                            goto addr_3388_225;
                        }
                    }
                } else {
                    if (!r14_282->f19) {
                        zf334 = head == 0;
                        if (zf334) {
                            head = r14_282;
                        } else {
                            rax335 = zeros;
                            rax335->f28 = r14_282;
                        }
                        zeros = r14_282;
                        goto addr_5832_685;
                    }
                }
            } else {
                goto addr_57f0_676;
            }
            goto addr_3bd5_589;
            addr_57f0_676:
            rsi24 = rbx20;
            al336 = recurse_tree(rdi325, rsi24, rdx23, rcx22);
            if (al336) 
                goto addr_3388_225; else 
                goto addr_5800_674;
            addr_40a0_574:
            rdi337 = r14_286->f8;
            if (!rdi337) {
                rdi338 = r14_286->f10;
                if (!rdi338) {
                    if (!r14_286->f20 && !r14_286->f19) {
                        zf339 = head == 0;
                        if (zf339) {
                            head = r14_286;
                        } else {
                            rax340 = zeros;
                            rax340->f28 = r14_286;
                        }
                        zeros = r14_286;
                        goto addr_4102_571;
                    }
                } else {
                    if (r14_286->f20) 
                        goto addr_5072_706;
                }
            } else {
                rsi24 = rbx20;
                al341 = recurse_tree(rdi337, rsi24, rdx23, rcx22);
                if (al341) 
                    goto addr_3388_225;
                rcx22 = rcx22;
                rdi338 = r14_286->f10;
                if (r14_286->f20) 
                    goto addr_40f9_709;
            }
            if (!r14_286->f19) {
                zf342 = head == 0;
                if (zf342) {
                    head = r14_286;
                } else {
                    rax343 = zeros;
                    rax343->f28 = r14_286;
                }
                zeros = r14_286;
            }
            addr_40f9_709:
            if (rdi338) {
                addr_5072_706:
                rsi24 = rbx20;
                al344 = recurse_tree(rdi338, rsi24, rdx23, rcx22);
                rcx22 = rcx22;
                if (al344) {
                    goto addr_3388_225;
                }
            } else {
                addr_4102_571:
                r14_282 = rcx22->f10;
                if (!rcx22->f20) {
                    addr_47f8_562:
                    if (!rcx22->f19) {
                        zf345 = head == 0;
                        if (zf345) {
                            head = rcx22;
                        } else {
                            rax346 = zeros;
                            rax346->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_4111_720;
                    }
                } else {
                    addr_4111_720:
                    if (r14_282) 
                        goto addr_4785_563; else 
                        goto addr_411a_560;
                }
            }
            addr_3621_584:
            if (!r14_285->f19) {
                zf347 = head == 0;
                if (zf347) {
                    head = r14_285;
                } else {
                    rax348 = zeros;
                    rax348->f28 = r14_285;
                }
                zeros = r14_285;
            }
            addr_3648_588:
            if (rdi290) {
                addr_540e_585:
                rsi24 = rbx20;
                al349 = recurse_tree(rdi290, rsi24, rdx23, rcx22);
                rcx22 = rcx22;
                r8_281 = r8_281;
                if (al349) {
                    goto addr_3388_225;
                }
            } else {
                addr_3651_582:
                r14_286 = r8_281->f10;
                if (r8_281->f20) {
                    addr_3683_726:
                    if (!r14_286) 
                        goto addr_4102_571;
                } else {
                    addr_365c_573:
                    if (!r8_281->f19) {
                        zf350 = head == 0;
                        if (zf350) {
                            head = r8_281;
                        } else {
                            rax351 = zeros;
                            rax351->f28 = r8_281;
                        }
                        zeros = r8_281;
                        goto addr_3683_726;
                    }
                }
            }
            goto addr_40a0_574;
            addr_3a16_238:
            r13_352 = rcx22->f8;
            if (!r13_352) {
                r13_19 = rcx22->f10;
                if (!r13_19) {
                    if (!rcx22->f20 && !rcx22->f19) {
                        zf353 = head == 0;
                        if (zf353) {
                            head = rcx22;
                        } else {
                            rax354 = zeros;
                            rax354->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_3332_738;
                    }
                }
                if (!rcx22->f20) 
                    goto addr_4595_740;
            } else {
                rdx23 = r13_352->f8;
                if (!rdx23) {
                    rdx23 = r13_352->f10;
                    if (!rdx23) {
                        if (!r13_352->f20 && !r13_352->f19) {
                            zf355 = head == 0;
                            if (zf355) {
                                head = r13_352;
                            } else {
                                rax356 = zeros;
                                rax356->f28 = r13_352;
                            }
                            zeros = r13_352;
                            goto addr_458a_748;
                        }
                    }
                    if (!r13_352->f20) 
                        goto addr_3aa6_750; else 
                        goto addr_451f_751;
                } else {
                    rdi357 = rdx23->f8;
                    if (!rdi357) {
                        rdi358 = rdx23->f10;
                        if (!rdi358) {
                            if (!rdx23->f20 && !rdx23->f19) {
                                zf359 = head == 0;
                                if (zf359) {
                                    head = rdx23;
                                } else {
                                    rax360 = zeros;
                                    rax360->f28 = rdx23;
                                }
                                zeros = rdx23;
                                goto addr_3a9b_759;
                            }
                        } else {
                            if (!rdx23->f20) 
                                goto addr_3a6c_761; else 
                                goto addr_569d_762;
                        }
                    } else {
                        rsi24 = rbx20;
                        al361 = recurse_tree(rdi357, rsi24, rdx23, rcx22);
                        if (al361) 
                            goto addr_3388_225;
                        rdx23 = rdx23;
                        rcx22 = rcx22;
                        rdi358 = rdx23->f10;
                        if (rdx23->f20) 
                            goto addr_3a92_765; else 
                            goto addr_3a6c_761;
                    }
                }
            }
            addr_4498_766:
            rdi362 = r13_19->f8;
            if (!rdi362) {
                rdi363 = r13_19->f10;
                rax162 = r13_19->f20;
                if (!rdi363) {
                    addr_4956_236:
                    if (!rax162 && !r13_19->f19) {
                        zf364 = head == 0;
                        if (zf364) {
                            head = r13_19;
                        } else {
                            rax365 = zeros;
                            rax365->f28 = r13_19;
                        }
                        zeros = r13_19;
                        goto addr_3332_738;
                    }
                } else {
                    if (rax162) {
                        goto addr_44f0_774;
                    }
                }
            } else {
                rsi24 = rbx20;
                al366 = recurse_tree(rdi362, rsi24, rdx23, rcx22);
                if (al366) 
                    goto addr_3388_225;
                rdi363 = r13_19->f10;
                if (r13_19->f20) 
                    goto addr_44e7_777;
            }
            if (!r13_19->f19) {
                zf367 = head == 0;
                if (zf367) {
                    head = r13_19;
                } else {
                    rax368 = zeros;
                    rax368->f28 = r13_19;
                }
                zeros = r13_19;
            }
            addr_44e7_777:
            if (!rdi363) {
                addr_3332_738:
                r13_159 = r14_28->f10;
                if (!r14_28->f20) {
                    addr_358e_233:
                    if (!r14_28->f19) {
                        zf369 = head == 0;
                        if (zf369) {
                            head = r14_28;
                        } else {
                            rax370 = zeros;
                            rax370->f28 = r14_28;
                        }
                        zeros = r14_28;
                        goto addr_3341_787;
                    }
                } else {
                    addr_3341_787:
                    if (r13_159) 
                        goto addr_35be_232; else 
                        goto addr_334a_603;
                }
            } else {
                addr_44f0_774:
                rsi24 = rbx20;
                al371 = recurse_tree(rdi363, rsi24, rdx23, rcx22);
                if (al371) {
                    goto addr_3388_225;
                }
            }
            addr_451f_751:
            rdi372 = rdx23->f8;
            if (!rdi372) {
                rdi373 = rdx23->f10;
                if (!rdi373) {
                    if (!rdx23->f20 && !rdx23->f19) {
                        zf374 = head == 0;
                        if (zf374) {
                            head = rdx23;
                        } else {
                            rax375 = zeros;
                            rax375->f28 = rdx23;
                        }
                        zeros = rdx23;
                        goto addr_458a_748;
                    }
                } else {
                    if (rdx23->f20) 
                        goto addr_4f1a_796;
                }
            } else {
                rsi24 = rbx20;
                al376 = recurse_tree(rdi372, rsi24, rdx23, rcx22);
                if (al376) 
                    goto addr_3388_225;
                rdx23 = rdx23;
                rcx22 = rcx22;
                rdi373 = rdx23->f10;
                if (rdx23->f20) 
                    goto addr_4581_799;
            }
            if (!rdx23->f19) {
                zf377 = head == 0;
                if (zf377) {
                    head = rdx23;
                } else {
                    rax378 = zeros;
                    rax378->f28 = rdx23;
                }
                zeros = rdx23;
            }
            addr_4581_799:
            if (rdi373) {
                addr_4f1a_796:
                rsi24 = rbx20;
                al379 = recurse_tree(rdi373, rsi24, rdx23, rcx22);
                rcx22 = rcx22;
                if (al379) {
                    goto addr_3388_225;
                }
            } else {
                addr_458a_748:
                r13_19 = rcx22->f10;
                if (rcx22->f20) {
                    addr_45bb_806:
                    if (!r13_19) 
                        goto addr_3332_738;
                } else {
                    addr_4595_740:
                    if (!rcx22->f19) {
                        zf380 = head == 0;
                        if (zf380) {
                            head = rcx22;
                        } else {
                            rax381 = zeros;
                            rax381->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_45bb_806;
                    }
                }
            }
            goto addr_4498_766;
            addr_3a6c_761:
            if (!rdx23->f19) {
                zf382 = head == 0;
                if (zf382) {
                    head = rdx23;
                } else {
                    rax383 = zeros;
                    rax383->f28 = rdx23;
                }
                zeros = rdx23;
            }
            addr_3a92_765:
            if (rdi358) {
                addr_569d_762:
                rsi24 = rbx20;
                al384 = recurse_tree(rdi358, rsi24, rdx23, rcx22);
                rcx22 = rcx22;
                if (al384) {
                    goto addr_3388_225;
                }
            } else {
                addr_3a9b_759:
                rdx23 = r13_352->f10;
                if (r13_352->f20) {
                    addr_3acd_817:
                    if (!rdx23) 
                        goto addr_458a_748;
                } else {
                    addr_3aa6_750:
                    if (!r13_352->f19) {
                        zf385 = head == 0;
                        if (zf385) {
                            head = r13_352;
                        } else {
                            rax386 = zeros;
                            rax386->f28 = r13_352;
                        }
                        zeros = r13_352;
                        goto addr_3acd_817;
                    }
                }
            }
            goto addr_451f_751;
            addr_3c6f_245:
            r8 = rcx22->f8;
            if (!r8) {
                r8_161 = rcx22->f10;
                if (!r8_161) {
                    if (!rcx22->f20 && !rcx22->f19) {
                        zf387 = head == 0;
                        if (zf387) {
                            head = rcx22;
                        } else {
                            rax388 = zeros;
                            rax388->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_331a_829;
                    }
                }
                if (!rcx22->f20) 
                    goto addr_3cf3_831;
            } else {
                rdi389 = r8->f8;
                if (!rdi389) {
                    addr_4ea7_17:
                    rdi390 = r8->f10;
                    if (!rdi390) {
                        if (!r8->f20 && !r8->f19) {
                            zf391 = head == 0;
                            if (zf391) {
                                head = r8;
                            } else {
                                rax392 = zeros;
                                rax392->f28 = r8;
                            }
                            zeros = r8;
                            goto addr_3ce8_839;
                        }
                    } else {
                        if (!r8->f20) 
                            goto addr_3cb8_841; else 
                            goto addr_4ec1_842;
                    }
                } else {
                    rsi24 = rbx20;
                    al393 = recurse_tree(rdi389, rsi24, rdx23, rcx22);
                    if (al393) 
                        goto addr_3388_225;
                    r8 = r8;
                    rcx22 = rcx22;
                    rdi390 = r8->f10;
                    if (r8->f20) 
                        goto addr_3cdf_845; else 
                        goto addr_3cb8_841;
                }
            }
            addr_3d22_846:
            rdi394 = r8_161->f8;
            if (!rdi394) {
                rdi395 = r8_161->f10;
                rax164 = r8_161->f20;
                if (!rdi395) {
                    addr_4ded_242:
                    if (!rax164 && !r8_161->f19) {
                        zf396 = head == 0;
                        if (zf396) {
                            head = r8_161;
                        } else {
                            rax397 = zeros;
                            rax397->f28 = r8_161;
                        }
                        zeros = r8_161;
                        goto addr_331a_829;
                    }
                } else {
                    if (rax164) 
                        goto addr_3d84_853;
                }
            } else {
                rsi24 = rbx20;
                al398 = recurse_tree(rdi394, rsi24, rdx23, rcx22);
                if (al398) 
                    goto addr_3388_225;
                r8_161 = r8_161;
                rdi395 = r8_161->f10;
                if (r8_161->f20) 
                    goto addr_3d7b_857;
            }
            if (!r8_161->f19) {
                zf399 = head == 0;
                if (zf399) {
                    head = r8_161;
                } else {
                    rax400 = zeros;
                    rax400->f28 = r8_161;
                }
                zeros = r8_161;
            }
            addr_3d7b_857:
            if (!rdi395) {
                addr_331a_829:
                rcx22 = r13_19->f10;
                if (!r13_19->f20) {
                    addr_39e6_239:
                    if (!r13_19->f19) {
                        zf401 = head == 0;
                        if (zf401) {
                            head = r13_19;
                        } else {
                            rax402 = zeros;
                            rax402->f28 = r13_19;
                        }
                        zeros = r13_19;
                        goto addr_3329_867;
                    }
                } else {
                    addr_3329_867:
                    if (rcx22) 
                        goto addr_3a16_238; else 
                        goto addr_3332_738;
                }
            } else {
                addr_3d84_853:
                rsi24 = rbx20;
                al403 = recurse_tree(rdi395, rsi24, rdx23, rcx22);
                if (al403) {
                    goto addr_3388_225;
                }
            }
            addr_3cb8_841:
            if (!r8->f19) {
                zf404 = head == 0;
                if (zf404) {
                    head = r8;
                } else {
                    rax405 = zeros;
                    rax405->f28 = r8;
                }
                zeros = r8;
            }
            addr_3cdf_845:
            if (rdi390) {
                addr_4ec1_842:
                rsi24 = rbx20;
                al406 = recurse_tree(rdi390, rsi24, rdx23, rcx22);
                rcx22 = rcx22;
                if (al406) {
                    goto addr_3388_225;
                }
            } else {
                addr_3ce8_839:
                r8_161 = rcx22->f10;
                if (rcx22->f20) {
                    addr_3d19_874:
                    if (!r8_161) 
                        goto addr_331a_829; else 
                        goto addr_3d22_846;
                } else {
                    addr_3cf3_831:
                    if (!rcx22->f19) {
                        zf407 = head == 0;
                        if (zf407) {
                            head = rcx22;
                        } else {
                            rax408 = zeros;
                            rax408->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_3d19_874;
                    }
                }
            }
            addr_48ac_256:
            rdi409 = rcx22->f8;
            if (!rdi409) {
                rdi410 = rcx22->f10;
                if (!rdi410) {
                    if (!rcx22->f20 && !rcx22->f19) {
                        zf411 = head == 0;
                        if (zf411) {
                            head = rcx22;
                        } else {
                            rax412 = zeros;
                            rax412->f28 = rcx22;
                        }
                        zeros = rcx22;
                        goto addr_32df_253;
                    }
                } else {
                    if (rcx22->f20) {
                        goto addr_4917_887;
                    }
                }
            } else {
                rsi24 = rbx20;
                al413 = recurse_tree(rdi409, rsi24, rdx23, rcx22);
                if (al413) 
                    goto addr_3388_225;
                rcx22 = rcx22;
                r8_161 = r8_161;
                rdi410 = rcx22->f10;
                if (rcx22->f20) 
                    goto addr_490e_890;
            }
            if (!rcx22->f19) {
                zf414 = head == 0;
                if (zf414) {
                    head = rcx22;
                } else {
                    rax415 = zeros;
                    rax415->f28 = rcx22;
                }
                zeros = rcx22;
            }
            addr_490e_890:
            if (!rdi410) {
                addr_32df_253:
                rcx22 = r8_161->f10;
                if (r8_161->f20) {
                    addr_3311_896:
                    if (rcx22) 
                        goto addr_3c6f_245; else 
                        goto addr_331a_829;
                } else {
                    addr_32ea_244:
                    if (!r8_161->f19) {
                        zf416 = head == 0;
                        if (zf416) {
                            head = r8_161;
                        } else {
                            rax417 = zeros;
                            rax417->f28 = r8_161;
                        }
                        zeros = r8_161;
                        goto addr_3311_896;
                    }
                }
            } else {
                addr_4917_887:
                rsi24 = rbx20;
                al418 = recurse_tree(rdi410, rsi24, rdx23, rcx22);
                r8_161 = r8_161;
                if (al418) {
                    goto addr_3388_225;
                }
            }
            addr_4057_266:
            if (!rcx22->f19) {
                zf419 = head == 0;
                if (zf419) {
                    head = rcx22;
                } else {
                    rax420 = zeros;
                    rax420->f28 = rcx22;
                }
                zeros = rcx22;
            }
            addr_329b_270:
            if (rdi168) {
                addr_4b7a_267:
                rsi24 = rbx20;
                al421 = recurse_tree(rdi168, rsi24, rdx23, rcx22);
                r8_161 = r8_161;
                r9_163 = r9_163;
                if (al421) {
                    goto addr_3388_225;
                }
            } else {
                addr_32a4_264:
                rcx22 = r9_163->f10;
                if (r9_163->f20) {
                    addr_32d6_907:
                    if (rcx22) 
                        goto addr_48ac_256; else 
                        goto addr_32df_253;
                } else {
                    addr_32af_255:
                    if (!r9_163->f19) {
                        zf422 = head == 0;
                        if (zf422) {
                            head = r9_163;
                        } else {
                            rax423 = zeros;
                            rax423->f28 = r9_163;
                        }
                        zeros = r9_163;
                        goto addr_32d6_907;
                    }
                }
            }
        }
    }
    if (v12) {
        while (1) {
            quotearg_n_style_colon();
            addr_5cb8_914:
            fun_2390();
            fun_25e0();
            addr_5cd5_6:
            quotearg_n_style_colon();
            fun_2390();
            fun_25e0();
        }
    } else {
        fun_2420();
        goto addr_5cb8_914;
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xe080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0xa44b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0xa444);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0xa44f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0xa440);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gde38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gde38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2353() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __uflow = 0x2030;

void fun_2363() {
    __asm__("cli ");
    goto __uflow;
}

int64_t free = 0x2040;

void fun_2373() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2383() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2393() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t puts = 0x20a0;

void fun_23d3() {
    __asm__("cli ");
    goto puts;
}

int64_t reallocarray = 0x20b0;

void fun_23e3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20c0;

void fun_23f3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2403() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2413() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2423() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2433() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2443() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2453() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2463() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2473() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x2150;

void fun_2483() {
    __asm__("cli ");
    goto dup2;
}

int64_t strrchr = 0x2160;

void fun_2493() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2170;

void fun_24a3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2180;

void fun_24b3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2190;

void fun_24c3() {
    __asm__("cli ");
    goto memset;
}

int64_t freopen = 0x21a0;

void fun_24d3() {
    __asm__("cli ");
    goto freopen;
}

int64_t close = 0x21b0;

void fun_24e3() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x21c0;

void fun_24f3() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memcmp = 0x21d0;

void fun_2503() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_2513() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2523() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2200;

void fun_2533() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2210;

void fun_2543() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2220;

void fun_2553() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_2563() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_2573() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_2583() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_2593() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25a3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2280;

void fun_25b3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2290;

void fun_25c3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22a0;

void fun_25d3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22b0;

void fun_25e3() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x22c0;

void fun_25f3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x22d0;

void fun_2603() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x22e0;

void fun_2613() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22f0;

void fun_2623() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2300;

void fun_2633() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2310;

void fun_2643() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2320;

void fun_2653() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2330;

void fun_2663() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2340;

void fun_2673() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s0* fun_25c0(int64_t rdi, ...);

void fun_2410(int64_t rdi, int64_t rsi);

void fun_23f0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

struct s0* Version = reinterpret_cast<struct s0*>(0xc2);

void parse_gnu_standard_options_only(int64_t rdi, struct s0* rsi, int64_t rdx, struct s0* rcx, struct s0* r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14);

void usage(int64_t rdi);

int32_t optind = 0;

int64_t quote(int64_t rdi, struct s0* rsi);

void fun_26c3(int32_t edi, struct s0* rsi) {
    struct s0* rdi3;
    int64_t rdi4;
    struct s0* r8_5;
    int64_t rbx6;
    int64_t rbp7;
    int64_t r12_8;
    int64_t rax9;
    struct s0* rdx10;
    struct s0* rdi11;
    int64_t rdi12;

    __asm__("cli ");
    rdi3 = *reinterpret_cast<struct s0**>(&rsi->f0);
    set_program_name(rdi3);
    fun_25c0(6, 6);
    fun_2410("coreutils", "/usr/local/share/locale");
    fun_23f0("coreutils", "/usr/local/share/locale");
    atexit(0x5eb0, "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&rdi4) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    r8_5 = Version;
    parse_gnu_standard_options_only(rdi4, rsi, "tsort", "GNU coreutils", r8_5, 1, usage, "Mark Kettenis", 0, 0x2710, rbx6, rbp7, r12_8, __return_address());
    rax9 = optind;
    *reinterpret_cast<int32_t*>(&rdx10) = edi - *reinterpret_cast<int32_t*>(&rax9);
    *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
    if (*reinterpret_cast<int32_t*>(&rdx10) <= 1) {
        rdi11 = reinterpret_cast<struct s0*>("-");
        if (*reinterpret_cast<int32_t*>(&rax9) != edi) {
            rdi11 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rsi) + rax9 * 8);
        }
        rax9 = tsort(rdi11, rsi, rdx10, "GNU coreutils", r8_5, 1, rbx6, rbp7, r12_8, __return_address());
    }
    rdi12 = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rsi) + rax9 * 8 + 8);
    quote(rdi12, rsi);
    fun_2420();
    fun_25e0();
    usage(1);
}

int64_t __libc_start_main = 0;

void fun_27b3() {
    __asm__("cli ");
    __libc_start_main(0x26c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xe008;

void fun_2350(int64_t rdi);

int64_t fun_2853() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2350(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2893() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

int64_t fun_28a3() {
    int64_t tmp64_1;

    __asm__("cli ");
    tmp64_1 = n_strings + 1;
    n_strings = tmp64_1;
    return 0;
}

int64_t fun_2923(struct s0* rdi) {
    int1_t zf2;
    struct s0* rax3;

    __asm__("cli ");
    if (!rdi->f20 && !rdi->f19) {
        zf2 = head == 0;
        if (zf2) {
            head = rdi;
        } else {
            rax3 = zeros;
            rax3->f28 = rdi;
        }
        zeros = rdi;
    }
    return 0;
}

int64_t fun_2963(struct s0* rdi) {
    int32_t r8d2;
    struct s0* rbp3;
    struct s0* rbx4;
    struct s0* rax5;
    struct s0** r13_6;
    struct s0* r12_7;
    int64_t rax8;
    struct s0* rdi9;
    struct s0* rax10;

    __asm__("cli ");
    r8d2 = 0;
    if (!rdi->f20) {
        return 0;
    }
    rbp3 = rdi;
    rbx4 = loop;
    if (!rbx4) {
        loop = rdi;
        return 0;
    }
    rax5 = rdi->f30;
    r13_6 = &rdi->f30;
    if (!rax5) 
        goto addr_299a_6;
    do {
        if (rbx4 == *reinterpret_cast<struct s0**>(&rax5->f0)) 
            break;
        r13_6 = &rax5->f8;
        rax5 = rax5->f8;
    } while (rax5);
    goto addr_2a80_9;
    if (!rbp3->f28) {
        rbp3->f28 = rbx4;
        r8d2 = 0;
        loop = rbp3;
    } else {
        do {
            rbx4 = rbx4->f28;
            fun_25e0();
            r12_7 = loop;
            if (rbp3 == r12_7) 
                goto addr_2a09_14;
            r12_7->f28 = reinterpret_cast<struct s0*>(0);
            loop = rbx4;
        } while (rbx4);
        goto addr_2a48_16;
    }
    addr_2a4e_17:
    *reinterpret_cast<int32_t*>(&rax8) = r8d2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
    addr_2a09_14:
    rdi9 = *r13_6;
    (*reinterpret_cast<struct s0**>(&rdi9->f0))->f20 = (*reinterpret_cast<struct s0**>(&rdi9->f0))->f20 - 1;
    *r13_6 = rdi9->f8;
    fun_2370(rdi9);
    do {
        rax10 = r12_7;
        r12_7 = r12_7->f28;
        rax10->f28 = reinterpret_cast<struct s0*>(0);
    } while (r12_7);
    loop = reinterpret_cast<struct s0*>(0);
    addr_2a48_16:
    r8d2 = 1;
    goto addr_2a4e_17;
    addr_2a80_9:
    return 0;
    addr_299a_6:
    goto addr_2a4e_17;
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_25d0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int64_t stdout = 0;

void fun_2510(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_23a0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int64_t stderr = 0;

void fun_2640(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2aa3(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    int64_t r12_7;
    struct s0* rax8;
    int64_t r12_9;
    struct s0* rax10;
    int64_t r12_11;
    struct s0* rax12;
    int64_t r12_13;
    struct s0* rax14;
    uint32_t eax15;
    struct s0* r13_16;
    struct s0* rax17;
    struct s0* rax18;
    int32_t eax19;
    struct s0* rax20;
    struct s0* rax21;
    struct s0* rax22;
    int32_t eax23;
    struct s0* rax24;
    int64_t r15_25;
    struct s0* rax26;
    struct s0* rax27;
    struct s0* rax28;
    int64_t rdi29;
    struct s0* r8_30;
    struct s0* r9_31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2420();
            fun_25d0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2420();
            fun_2510(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2420();
            fun_2510(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2420();
            fun_2510(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2420();
            fun_2510(rax14, r12_13, 5, rcx6);
            do {
                if (1) 
                    break;
                eax15 = fun_2530("tsort", 0, 5, "sha512sum");
            } while (eax15);
            r13_16 = v4;
            if (!r13_16) {
                rax17 = fun_2420();
                fun_25d0(1, rax17, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax18 = fun_25c0(5);
                if (!rax18 || (eax19 = fun_23a0(rax18, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax19)) {
                    rax20 = fun_2420();
                    r13_16 = reinterpret_cast<struct s0*>("tsort");
                    fun_25d0(1, rax20, "https://www.gnu.org/software/coreutils/", "tsort");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_16 = reinterpret_cast<struct s0*>("tsort");
                    goto addr_2dd0_9;
                }
            } else {
                rax21 = fun_2420();
                fun_25d0(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax22 = fun_25c0(5);
                if (!rax22 || (eax23 = fun_23a0(rax22, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax23)) {
                    addr_2cd6_11:
                    rax24 = fun_2420();
                    fun_25d0(1, rax24, "https://www.gnu.org/software/coreutils/", "tsort");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_16 == "tsort")) {
                        r12_2 = reinterpret_cast<struct s0*>(0xa0cf);
                    }
                } else {
                    addr_2dd0_9:
                    r15_25 = stdout;
                    rax26 = fun_2420();
                    fun_2510(rax26, r15_25, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_2cd6_11;
                }
            }
            rax27 = fun_2420();
            rcx6 = r12_2;
            fun_25d0(1, rax27, r13_16, rcx6);
            addr_2afe_14:
            fun_2620();
        }
    } else {
        rax28 = fun_2420();
        rdi29 = stderr;
        rcx6 = r12_2;
        fun_2640(rdi29, 1, rax28, rcx6, r8_30, r9_31, v32, v33, v34, v35, v36, v37);
        goto addr_2afe_14;
    }
}

int64_t file_name = 0;

void fun_5e93(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_5ea3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_23b0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_5eb3() {
    int64_t rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2390(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2420();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_5f43_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_25e0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_5f43_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_25e0();
    }
}

void fun_5f63() {
    __asm__("cli ");
}

struct s4 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2560(struct s4* rdi);

void fun_5f73(struct s4* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2560(rdi);
        goto 0x24f0;
    }
}

int32_t fun_25a0(struct s4* rdi);

int64_t fun_24a0(int64_t rdi, ...);

int32_t rpl_fflush(struct s4* rdi);

int64_t fun_2400(struct s4* rdi);

int64_t fun_5fa3(struct s4* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2560(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25a0(rdi);
        if (!(eax3 && (eax4 = fun_2560(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24a0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2390();
            r12d9 = *rax8;
            rax10 = fun_2400(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2400;
}

void rpl_fseeko(struct s4* rdi);

void fun_6033(struct s4* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25a0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int32_t fun_2480();

int32_t fun_25f0(int64_t rdi);

int64_t fun_24d0(int64_t rdi, int64_t rsi, struct s4* rdx);

void fun_24e0();

int64_t fun_6083(int64_t rdi, int64_t rsi, struct s4* rdx) {
    int32_t eax4;
    int32_t eax5;
    int32_t* rax6;
    int32_t* rbx7;
    int32_t eax8;
    int32_t r14d9;
    int32_t r15d10;
    signed char v11;
    int32_t eax12;
    int32_t eax13;
    int32_t eax14;
    int32_t* rax15;
    int64_t rax16;
    int64_t r12_17;
    int32_t* rax18;
    int32_t ebp19;
    int32_t eax20;
    int32_t eax21;

    __asm__("cli ");
    eax4 = fun_2560(rdx);
    if (eax4 == 1) {
        eax5 = fun_2480();
        rax6 = fun_2390();
        rbx7 = rax6;
        if (eax5) {
            eax8 = fun_25f0("/dev/null");
            if (eax8) {
                r14d9 = 0;
                r15d10 = 0;
                goto addr_61c8_5;
            } else {
                v11 = 1;
                r14d9 = 0;
                r15d10 = 0;
            }
        } else {
            addr_61fd_7:
            v11 = 0;
            r14d9 = 0;
            r15d10 = 0;
        }
    } else {
        if (eax4 == 2) {
            r14d9 = 0;
        } else {
            if (!eax4) 
                goto addr_61fd_7;
            eax12 = fun_2480();
            *reinterpret_cast<unsigned char*>(&r14d9) = reinterpret_cast<uint1_t>(eax12 != 2);
        }
        eax13 = fun_2480();
        *reinterpret_cast<unsigned char*>(&r15d10) = reinterpret_cast<uint1_t>(eax13 != 1);
        eax14 = fun_2480();
        rax15 = fun_2390();
        rbx7 = rax15;
        if (eax14) 
            goto addr_61b0_13; else 
            goto addr_610e_14;
    }
    rax16 = fun_24d0(rdi, rsi, rdx);
    r12_17 = rax16;
    rax18 = fun_2390();
    ebp19 = *rax18;
    rbx7 = rax18;
    addr_6223_16:
    if (*reinterpret_cast<unsigned char*>(&r14d9)) {
        addr_616c_17:
        fun_24e0();
        if (!*reinterpret_cast<unsigned char*>(&r15d10)) {
            addr_6235_18:
            if (v11) {
                addr_6195_19:
                fun_24e0();
                if (r12_17) {
                    addr_6249_20:
                    return r12_17;
                } else {
                    addr_61a5_21:
                    *rbx7 = ebp19;
                    goto addr_6249_20;
                }
            } else {
                addr_6240_22:
                if (!r12_17) 
                    goto addr_61a5_21; else 
                    goto addr_6249_20;
            }
        }
    } else {
        if (!*reinterpret_cast<unsigned char*>(&r15d10)) 
            goto addr_6235_18;
    }
    addr_6180_25:
    fun_24e0();
    if (!v11) 
        goto addr_6240_22; else 
        goto addr_6195_19;
    addr_61b0_13:
    eax8 = fun_25f0("/dev/null");
    if (!eax8) {
        v11 = 1;
        if (eax13 != 1) {
            addr_611e_27:
            eax20 = fun_25f0("/dev/null");
            if (eax20 != 1) {
                if (eax20 >= 0) {
                    ebp19 = 9;
                    fun_24e0();
                    *rbx7 = 9;
                } else {
                    ebp19 = *rbx7;
                }
                *reinterpret_cast<int32_t*>(&r12_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&r14d9)) {
                    fun_24e0();
                    goto addr_6180_25;
                }
            } else {
                r15d10 = 1;
            }
        } else {
            addr_62d0_34:
            r15d10 = 0;
        }
    } else {
        addr_61c8_5:
        if (eax8 >= 0) {
            ebp19 = 9;
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
            fun_24e0();
            *rbx7 = 9;
            v11 = 1;
            goto addr_6223_16;
        } else {
            v11 = 1;
            ebp19 = *rbx7;
            *reinterpret_cast<int32_t*>(&r12_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
            goto addr_6223_16;
        }
    }
    if (*reinterpret_cast<unsigned char*>(&r14d9) && (eax21 = fun_25f0("/dev/null"), eax21 != 2)) {
        if (eax21 >= 0) {
            ebp19 = 9;
            fun_24e0();
            *rbx7 = 9;
        } else {
            ebp19 = *rbx7;
        }
        *reinterpret_cast<int32_t*>(&r12_17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
        goto addr_616c_17;
    }
    addr_610e_14:
    v11 = 0;
    if (eax13 == 1) 
        goto addr_62d0_34; else 
        goto addr_611e_27;
}

int64_t fun_6353(struct s4* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2560(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24a0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

int32_t opterr = 0;

int32_t fun_2460();

void version_etc_va(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, void* r8);

void fun_63d3(int32_t edi) {
    signed char al2;
    struct s0* rax3;
    int32_t r14d4;
    int32_t eax5;
    void* rax6;
    int64_t rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t r9_11;

    __asm__("cli ");
    if (al2) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax3 = g28;
    r14d4 = opterr;
    opterr = 0;
    if (edi != 2) 
        goto addr_6450_4;
    eax5 = fun_2460();
    if (eax5 == -1) 
        goto addr_6450_4;
    if (eax5 != 0x68) {
        if (eax5 != 0x76) {
            addr_6450_4:
            opterr = r14d4;
            optind = 0;
            rax6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
            if (rax6) {
                fun_2450();
            } else {
                return;
            }
        } else {
            rdi7 = stdout;
            version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
            fun_2620();
        }
    }
    r9_11();
    goto addr_6450_4;
}

void fun_6513() {
    signed char al1;
    struct s0* rax2;
    signed char r9b3;
    int32_t ebx4;
    int32_t eax5;
    int64_t v6;
    int64_t rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t rdi11;
    void* rax12;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    if (!r9b3) {
    }
    ebx4 = opterr;
    opterr = 1;
    eax5 = fun_2460();
    if (eax5 != -1) {
        if (eax5 == 0x68) {
            addr_6640_7:
            v6();
        } else {
            if (eax5 == 0x76) {
                rdi7 = stdout;
                version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
                fun_2620();
                goto addr_6640_7;
            } else {
                *reinterpret_cast<int32_t*>(&rdi11) = exit_failure;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
                v6(rdi11);
            }
        }
    }
    opterr = ebx4;
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2450();
    } else {
        return;
    }
}

void fun_2630(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s5 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s5* fun_2490();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_6653(struct s0* rdi) {
    int64_t rcx2;
    struct s0* rbx3;
    struct s5* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2630("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2380("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2490();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_7df3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2390();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xe220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_7e33(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_7e53(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe220);
    }
    *rdi = esi;
    return 0xe220;
}

int64_t fun_7e73(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xe220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_7eb3(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xe220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_7ed3(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xe220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x268a;
    if (!rdx) 
        goto 0x268a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xe220;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_7f13(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s8* r8) {
    struct s8* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xe220);
    }
    rax7 = fun_2390();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x7f46);
    *rax7 = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_7f93(int64_t rdi, int64_t rsi, struct s0** rdx, struct s9* rcx) {
    struct s9* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xe220);
    }
    rax6 = fun_2390();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x7fc1);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x801c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_8083() {
    __asm__("cli ");
}

struct s0* ge078 = reinterpret_cast<struct s0*>(32);

int64_t slotvec0 = 0x100;

void fun_8093() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2370(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xe120) {
        fun_2370(rdi7);
        ge078 = reinterpret_cast<struct s0*>(0xe120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xe070) {
        fun_2370(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xe070);
    }
    nslots = 1;
    return;
}

void fun_8133() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8153() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8163(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8183(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_81a3(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2690;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_8233(struct s0* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2695;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s0* fun_82c3(int32_t edi, int64_t rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x269a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2450();
    } else {
        return rax5;
    }
}

struct s0* fun_8353(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x269f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_83e3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5e30]");
    __asm__("movdqa xmm1, [rip+0x5e38]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x5e21]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2450();
    } else {
        return rax10;
    }
}

struct s0* fun_8483(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5d90]");
    __asm__("movdqa xmm1, [rip+0x5d98]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x5d81]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s0* fun_8523(int64_t rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5cf0]");
    __asm__("movdqa xmm1, [rip+0x5cf8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x5cd9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2450();
    } else {
        return rax3;
    }
}

struct s0* fun_85b3(int64_t rdi, int64_t rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5c60]");
    __asm__("movdqa xmm1, [rip+0x5c68]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x5c56]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2450();
    } else {
        return rax4;
    }
}

struct s0* fun_8643(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26a4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_86e3(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5b2a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5b22]");
    __asm__("movdqa xmm2, [rip+0x5b2a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26a9;
    if (!rdx) 
        goto 0x26a9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s0* fun_8783(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5a8a]");
    __asm__("movdqa xmm1, [rip+0x5a92]");
    __asm__("movdqa xmm2, [rip+0x5a9a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ae;
    if (!rdx) 
        goto 0x26ae;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s0* fun_8833(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x59da]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x59d2]");
    __asm__("movdqa xmm2, [rip+0x59da]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b3;
    if (!rsi) 
        goto 0x26b3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_88d3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x593a]");
    __asm__("movdqa xmm1, [rip+0x5942]");
    __asm__("movdqa xmm2, [rip+0x594a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26b8;
    if (!rsi) 
        goto 0x26b8;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

void fun_8973() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8983(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_89a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_89c3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s10 {
    int64_t f0;
    int64_t f8;
};

void fun_89e3(struct s10* rdi) {
    __asm__("cli ");
    rdi->f0 = 0;
    rdi->f8 = 0;
    return;
}

struct s11 {
    signed char[8] pad8;
    unsigned char* f8;
    unsigned char* f10;
};

struct s12 {
    void* f0;
    struct s0* f8;
};

uint32_t fun_2360(struct s11* rdi, unsigned char* rsi);

void* fun_8a03(struct s11* rdi, unsigned char* rsi, int64_t rdx, struct s12* rcx) {
    unsigned char* r9_5;
    struct s12* r13_6;
    struct s11* rbp7;
    void* rsp8;
    struct s0* rax9;
    struct s0* v10;
    void* rdi11;
    uint64_t rcx12;
    uint64_t rax13;
    int64_t rax14;
    unsigned char* rax15;
    int64_t rbx16;
    uint32_t eax17;
    void* rax18;
    struct s0* r15_19;
    void* r12_20;
    unsigned char* r14_21;
    void* v22;
    void* r8_23;
    signed char* rdx24;
    struct s0* rax25;
    unsigned char* rax26;
    uint32_t eax27;
    void* rax28;

    __asm__("cli ");
    __asm__("pxor xmm0, xmm0");
    r9_5 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi) + rdx);
    r13_6 = rcx;
    rbp7 = rdi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 72);
    rax9 = g28;
    v10 = rax9;
    rdi11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) + 16);
    __asm__("movaps [rsp+0x10], xmm0");
    __asm__("movaps [rsp+0x20], xmm0");
    if (rdx) {
        do {
            *reinterpret_cast<uint32_t*>(&rcx12) = *rsi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
            ++rsi;
            rax13 = rcx12 >> 3;
            *reinterpret_cast<uint32_t*>(&rax14) = *reinterpret_cast<uint32_t*>(&rax13) & 24;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
            *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdi11) + rax14) = *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdi11) + rax14) | 1 << *reinterpret_cast<unsigned char*>(&rcx12);
        } while (r9_5 != rsi);
        rax15 = rbp7->f8;
        if (reinterpret_cast<uint64_t>(rax15) >= reinterpret_cast<uint64_t>(rbp7->f10)) 
            goto addr_8aa7_5;
        goto addr_8a80_7;
    }
    do {
        rax15 = rbp7->f8;
        if (reinterpret_cast<uint64_t>(rax15) < reinterpret_cast<uint64_t>(rbp7->f10)) {
            addr_8a80_7:
            rbp7->f8 = rax15 + 1;
            *reinterpret_cast<uint32_t*>(&rbx16) = *rax15;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
        } else {
            addr_8aa7_5:
            eax17 = fun_2360(rbp7, rsi);
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rbx16) = eax17;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
            if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0)) 
                goto addr_8ab5_9;
        }
    } while (static_cast<int1_t>(*reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp8) + (reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx16))) >> 6) * 8 + 16) >> rbx16));
    rax18 = r13_6->f0;
    r15_19 = r13_6->f8;
    *reinterpret_cast<int32_t*>(&r12_20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_20) + 4) = 0;
    r14_21 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp8) + 8);
    v22 = rax18;
    while (1) {
        if (r12_20 != rax18) {
            r8_23 = r12_20;
            rdx24 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_19) + reinterpret_cast<uint64_t>(r12_20));
            if (*reinterpret_cast<int32_t*>(&rbx16) < reinterpret_cast<int32_t>(0)) 
                break;
        } else {
            rsi = r14_21;
            rax25 = xpalloc();
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            r8_23 = r12_20;
            r15_19 = rax25;
            rdx24 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_19) + reinterpret_cast<uint64_t>(r12_20));
            if (*reinterpret_cast<int32_t*>(&rbx16) < reinterpret_cast<int32_t>(0)) 
                goto addr_8b6d_15;
        }
        if (static_cast<int1_t>(*reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp8) + (reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx16))) >> 6) * 8 + 16) >> rbx16)) 
            break;
        *rdx24 = *reinterpret_cast<signed char*>(&rbx16);
        r12_20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_20) + 1);
        rax26 = rbp7->f8;
        if (reinterpret_cast<uint64_t>(rax26) >= reinterpret_cast<uint64_t>(rbp7->f10)) {
            eax27 = fun_2360(rbp7, rsi);
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rbx16) = eax27;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
        } else {
            rbp7->f8 = rax26 + 1;
            *reinterpret_cast<uint32_t*>(&rbx16) = *rax26;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
        }
        rax18 = v22;
    }
    addr_8b70_21:
    *rdx24 = 0;
    r13_6->f8 = r15_19;
    r13_6->f0 = v22;
    addr_8abc_22:
    rax28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax28) {
        fun_2450();
    } else {
        return r8_23;
    }
    addr_8b6d_15:
    goto addr_8b70_21;
    addr_8ab5_9:
    r8_23 = reinterpret_cast<void*>(0xffffffffffffffff);
    goto addr_8abc_22;
}

struct s13 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s0* xnmalloc(unsigned char* rdi, int64_t rsi);

struct s0* xreallocarray(struct s0* rdi, unsigned char* rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

struct s0* fun_2550(struct s0* rdi, struct s0* rsi, unsigned char* rdx, struct s0* rcx, struct s0* r8);

unsigned char* fun_8ba3(struct s0* rdi, struct s13* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0** r9) {
    struct s0* v7;
    struct s0* v8;
    struct s0* v9;
    struct s0* v10;
    struct s0** v11;
    struct s0* rax12;
    struct s0* v13;
    unsigned char* rax14;
    unsigned char* rdi15;
    unsigned char* v16;
    unsigned char* r12_17;
    struct s0* rax18;
    struct s0* rbp19;
    struct s0* rax20;
    struct s0* r13_21;
    struct s0* v22;
    struct s0* rcx23;
    struct s0* rax24;
    struct s0* rax25;
    struct s0* rax26;
    void* rax27;
    struct s0** r14_28;
    struct s0** rbx29;
    unsigned char* rdx30;
    struct s0* rax31;
    struct s0* rax32;
    void* rax33;

    __asm__("cli ");
    v7 = rdi;
    v8 = rdx;
    v9 = rcx;
    v10 = r8;
    v11 = r9;
    rax12 = g28;
    v13 = rax12;
    if (!rsi) {
        *reinterpret_cast<int32_t*>(&rax14) = 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi15) = 64;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
    } else {
        rdi15 = &rsi->f1;
        rax14 = rdi15;
    }
    v16 = rax14;
    *reinterpret_cast<int32_t*>(&r12_17) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_17) + 4) = 0;
    rax18 = xnmalloc(rdi15, 8);
    rbp19 = rax18;
    rax20 = xnmalloc(v16, 8);
    r13_21 = rax20;
    v22 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78 - 8 + 8 - 8 + 8 + 80);
    while (1) {
        rcx23 = v22;
        rax24 = readtoken(v7, v8, v9, rcx23, r8);
        if (reinterpret_cast<int64_t>(v16) <= reinterpret_cast<int64_t>(r12_17)) {
            *reinterpret_cast<int32_t*>(&r8) = 8;
            *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
            rcx23 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
            rax25 = xpalloc();
            rbp19 = rax25;
            rax26 = xreallocarray(r13_21, v16, 8, 0xffffffffffffffff, 8);
            r13_21 = rax26;
        }
        rax27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_17) * 8);
        r14_28 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r13_21) + reinterpret_cast<uint64_t>(rax27));
        rbx29 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp19) + reinterpret_cast<uint64_t>(rax27));
        if (rax24 == 0xffffffffffffffff) 
            break;
        rdx30 = &rax24->f1;
        ++r12_17;
        rax31 = xnmalloc(rdx30, 1);
        *r14_28 = rax24;
        rax32 = fun_2550(rax31, 0, rdx30, rcx23, r8);
        *rbx29 = rax32;
    }
    *rbx29 = reinterpret_cast<struct s0*>(0);
    *r14_28 = reinterpret_cast<struct s0*>(0);
    fun_2370(0, 0);
    *reinterpret_cast<struct s0**>(&v10->f0) = rbp19;
    if (!v11) {
        fun_2370(r13_21, r13_21);
    } else {
        *v11 = r13_21;
    }
    rax33 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (rax33) {
        fun_2450();
    } else {
        return r12_17;
    }
}

struct s14 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2540(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_8d73(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s14* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2640(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2640(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2420();
    fun_2640(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2540(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2420();
    fun_2640(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2540(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2420();
        fun_2640(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xab28 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xab28;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_91e3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s15 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_9203(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s15* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s15* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2450();
    } else {
        return;
    }
}

void fun_92a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_9346_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_9350_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2450();
    } else {
        return;
    }
    addr_9346_5:
    goto addr_9350_7;
}

void fun_9383() {
    int64_t rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2540(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2420();
    fun_25d0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2420();
    fun_25d0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2420();
    goto fun_25d0;
}

int64_t fun_23e0();

void xalloc_die();

void fun_9423(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2570(unsigned char* rdi);

void fun_9463(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2570(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9483(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2570(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_94a3(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2570(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25b0();

void fun_94c3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25b0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_94f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9523(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_9563() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_95a3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_95d3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9623(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23e0();
            if (rax5) 
                break;
            addr_966d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_966d_5;
        rax8 = fun_23e0();
        if (rax8) 
            goto addr_9656_9;
        if (rbx4) 
            goto addr_966d_5;
        addr_9656_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_96b3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23e0();
            if (rax8) 
                break;
            addr_96fa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_96fa_5;
        rax11 = fun_23e0();
        if (rax11) 
            goto addr_96e2_9;
        if (!rbx6) 
            goto addr_96e2_9;
        if (r12_4) 
            goto addr_96fa_5;
        addr_96e2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_9743(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_97ed_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_9800_10:
                *r12_8 = 0;
            }
            addr_97a0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_97c6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_9814_14;
            if (rcx10 <= rsi9) 
                goto addr_97bd_16;
            if (rsi9 >= 0) 
                goto addr_9814_14;
            addr_97bd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_9814_14;
            addr_97c6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25b0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_9814_14;
            if (!rbp13) 
                break;
            addr_9814_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_97ed_9;
        } else {
            if (!r13_6) 
                goto addr_9800_10;
            goto addr_97a0_11;
        }
    }
}

int64_t fun_2520();

void fun_9843() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2520();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9873() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2520();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_98a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2520();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_98c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2520();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_98e3(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2570(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

void fun_9923(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2570(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

struct s16 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_9963(int64_t rdi, struct s16* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2570(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2550;
    }
}

struct s0* fun_2440(struct s0* rdi, ...);

void fun_99a3(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2440(rdi);
    rax3 = fun_2570(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2550;
    }
}

void fun_99e3() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2420();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_25e0();
    fun_2380(rdi1);
}

int64_t fun_23c0();

int64_t fun_9a23(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx) {
    int64_t rax5;
    uint32_t ebx6;
    int64_t rax7;
    int32_t* rax8;
    int32_t* rax9;

    __asm__("cli ");
    rax5 = fun_23c0();
    ebx6 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0)) & 32;
    rax7 = rpl_fclose(rdi, rsi, rdx, rcx);
    if (ebx6) {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            addr_9a7e_3:
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rax8 = fun_2390();
            *rax8 = 0;
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            if (rax5) 
                goto addr_9a7e_3;
            rax9 = fun_2390();
            *reinterpret_cast<int32_t*>(&rax7) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax9 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    }
    return rax7;
}

signed char* fun_2590(int64_t rdi);

signed char* fun_9a93() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2590(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2470(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_9ad3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2470(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2450();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_9b63() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax3;
    }
}

int64_t fun_9be3(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rax6;
    int32_t r13d7;
    struct s0* rax8;
    int64_t rax9;

    __asm__("cli ");
    rax6 = fun_25c0(rdi);
    if (!rax6) {
        r13d7 = 22;
        if (rdx) {
            *reinterpret_cast<struct s0**>(&rsi->f0) = reinterpret_cast<struct s0*>(0);
        }
    } else {
        rax8 = fun_2440(rax6);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax8)) {
            fun_2550(rsi, rax6, &rax8->f1, rcx, r8);
            return 0;
        } else {
            r13d7 = 34;
            if (rdx) {
                fun_2550(rsi, rax6, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff, rcx, r8);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax9) = r13d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

void fun_9c93() {
    __asm__("cli ");
    goto fun_25c0;
}

void fun_9ca3() {
    __asm__("cli ");
}

void fun_9cb7() {
    __asm__("cli ");
    return;
}

uint32_t fun_2500(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_2660(int64_t rdi, struct s0* rsi);

uint32_t fun_2650(struct s0* rdi, struct s0* rsi);

void** fun_2670(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_6885() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2420();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2420();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2440(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_6b83_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_6b83_22; else 
                            goto addr_6f7d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_703d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_7390_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_6b80_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_6b80_30; else 
                                goto addr_73a9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2440(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_7390_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2500(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_7390_28; else 
                            goto addr_6a2c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_74f0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_7370_40:
                        if (r11_27 == 1) {
                            addr_6efd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_74b8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_6b37_44;
                            }
                        } else {
                            goto addr_7380_46;
                        }
                    } else {
                        addr_74ff_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_6efd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_6b83_22:
                                if (v47 != 1) {
                                    addr_70d9_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2440(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_7124_54;
                                    }
                                } else {
                                    goto addr_6b90_56;
                                }
                            } else {
                                addr_6b35_57:
                                ebp36 = 0;
                                goto addr_6b37_44;
                            }
                        } else {
                            addr_7364_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_74ff_47; else 
                                goto addr_736e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_6efd_41;
                        if (v47 == 1) 
                            goto addr_6b90_56; else 
                            goto addr_70d9_52;
                    }
                }
                addr_6bf1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_6a88_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_6aad_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_6db0_65;
                    } else {
                        addr_6c19_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_7468_67;
                    }
                } else {
                    goto addr_6c10_69;
                }
                addr_6ac1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_6b0c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_7468_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_6b0c_81;
                }
                addr_6c10_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_6aad_64; else 
                    goto addr_6c19_66;
                addr_6b37_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_6bef_91;
                if (v22) 
                    goto addr_6b4f_93;
                addr_6bef_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6bf1_62;
                addr_7124_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_78ab_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_791b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_771f_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2660(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2650(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_721e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_6bdc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_7228_112;
                    }
                } else {
                    addr_7228_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_72f9_114;
                }
                addr_6be8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_6bef_91;
                while (1) {
                    addr_72f9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_7807_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_7266_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_7815_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_72e7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_72e7_128;
                        }
                    }
                    addr_7295_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_72e7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_7266_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_7295_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6b0c_81;
                addr_7815_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_7468_67;
                addr_78ab_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_721e_109;
                addr_791b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_721e_109;
                addr_6b90_56:
                rax93 = fun_2670(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_6bdc_110;
                addr_736e_59:
                goto addr_7370_40;
                addr_703d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_6b83_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_6be8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6b35_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_6b83_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_7082_160;
                if (!v22) 
                    goto addr_7457_162; else 
                    goto addr_7663_163;
                addr_7082_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_7457_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_7468_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_6f2b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_6d93_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_6ac1_70; else 
                    goto addr_6da7_169;
                addr_6f2b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_6a88_63;
                goto addr_6c10_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_7364_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_749f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_6b80_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_6a78_178; else 
                        goto addr_7422_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_7364_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_6b83_22;
                }
                addr_749f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_6b80_30:
                    r8d42 = 0;
                    goto addr_6b83_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_6bf1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_74b8_42;
                    }
                }
                addr_6a78_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_6a88_63;
                addr_7422_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_7380_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_6bf1_62;
                } else {
                    addr_7432_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_6b83_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_7be2_188;
                if (v28) 
                    goto addr_7457_162;
                addr_7be2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_6d93_168;
                addr_6a2c_37:
                if (v22) 
                    goto addr_7a23_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_6a43_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_74f0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_757b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_6b83_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_6a78_178; else 
                        goto addr_7557_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_7364_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_6b83_22;
                }
                addr_757b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_6b83_22;
                }
                addr_7557_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_7380_46;
                goto addr_7432_186;
                addr_6a43_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_6b83_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_6b83_22; else 
                    goto addr_6a54_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_7b2e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_79b4_210;
            if (1) 
                goto addr_79b2_212;
            if (!v29) 
                goto addr_75ee_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_7b21_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_6db0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_6b6b_219; else 
            goto addr_6dca_220;
        addr_6b4f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_6b63_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_6dca_220; else 
            goto addr_6b6b_219;
        addr_771f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_6dca_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_773d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_7bb0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_7616_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_7807_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_6b63_221;
        addr_7663_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_6b63_221;
        addr_6da7_169:
        goto addr_6db0_65;
        addr_7b2e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_6dca_220;
        goto addr_773d_222;
        addr_79b4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_7a0e_236;
        fun_2450();
        rsp25 = rsp25 - 8 + 8;
        goto addr_7bb0_225;
        addr_79b2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_79b4_210;
        addr_75ee_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_79b4_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_7616_226;
        }
        addr_7b21_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_6f7d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa5ec + rax113 * 4) + 0xa5ec;
    addr_73a9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa6ec + rax114 * 4) + 0xa6ec;
    addr_7a23_190:
    addr_6b6b_219:
    goto 0x6850;
    addr_6a54_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa4ec + rax115 * 4) + 0xa4ec;
    addr_7a0e_236:
    goto v116;
}

void fun_6a70() {
}

void fun_6c28() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x6922;
}

void fun_6c81() {
    goto 0x6922;
}

void fun_6d6e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x6bf1;
    }
    if (v2) 
        goto 0x7663;
    if (!r10_3) 
        goto addr_77ce_5;
    if (!v4) 
        goto addr_769e_7;
    addr_77ce_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_769e_7:
    goto 0x6aa4;
}

void fun_6d8c() {
}

void fun_6e37() {
    signed char v1;

    if (v1) {
        goto 0x6dbf;
    } else {
        goto 0x6afa;
    }
}

void fun_6e51() {
    signed char v1;

    if (!v1) 
        goto 0x6e4a; else 
        goto "???";
}

void fun_6e78() {
    goto 0x6d93;
}

void fun_6ef8() {
}

void fun_6f10() {
}

void fun_6f3f() {
    goto 0x6d93;
}

void fun_6f91() {
    goto 0x6f20;
}

void fun_6fc0() {
    goto 0x6f20;
}

void fun_6ff3() {
    goto 0x6f20;
}

void fun_73c0() {
    goto 0x6a78;
}

void fun_76be() {
    signed char v1;

    if (v1) 
        goto 0x7663;
    goto 0x6aa4;
}

void fun_7765() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x6aa4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x6a88;
        goto 0x6aa4;
    }
}

void fun_7b82() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x6df0;
    } else {
        goto 0x6922;
    }
}

void fun_8e48() {
    fun_2420();
}

void fun_6cae() {
    goto 0x6922;
}

void fun_6e84() {
    goto 0x6e3c;
}

void fun_6f4b() {
    goto 0x6a78;
}

void fun_6f9d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x6f20;
    goto 0x6b4f;
}

void fun_6fcf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x6f2b;
        goto 0x6950;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x6dca;
        goto 0x6b6b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x7768;
    if (r10_8 > r15_9) 
        goto addr_6eb5_9;
    addr_6eba_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x7773;
    goto 0x6aa4;
    addr_6eb5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_6eba_10;
}

void fun_7002() {
    goto 0x6b37;
}

void fun_73d0() {
    goto 0x6b37;
}

void fun_7b6f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x6c8c;
    } else {
        goto 0x6df0;
    }
}

void fun_8f00() {
}

void fun_700c() {
    goto 0x6fa7;
}

void fun_73da() {
    goto 0x6efd;
}

void fun_8f60() {
    fun_2420();
    goto fun_2640;
}

void fun_6cdd() {
    goto 0x6922;
}

void fun_7018() {
    goto 0x6fa7;
}

void fun_73e7() {
    goto 0x6f4e;
}

void fun_8fa0() {
    fun_2420();
    goto fun_2640;
}

void fun_6d0a() {
    goto 0x6922;
}

void fun_7024() {
    goto 0x6f20;
}

void fun_8fe0() {
    fun_2420();
    goto fun_2640;
}

void fun_6d2c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x76c0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x6bf1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x6bf1;
    }
    if (v11) 
        goto 0x7a23;
    if (r10_12 > r15_13) 
        goto addr_7a73_8;
    addr_7a78_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x77b1;
    addr_7a73_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_7a78_9;
}

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s19 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_9030() {
    int64_t r15_1;
    struct s17* rbx2;
    struct s0* r14_3;
    struct s18* rbx4;
    struct s0* r13_5;
    struct s19* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2420();
    fun_2640(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x9052, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_9088() {
    fun_2420();
    goto 0x9059;
}

struct s20 {
    signed char[32] pad32;
    int64_t f20;
};

struct s21 {
    signed char[24] pad24;
    int64_t f18;
};

struct s22 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s23 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s24 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_90c0() {
    int64_t rcx1;
    struct s20* rbx2;
    int64_t r15_3;
    struct s21* rbx4;
    struct s0* r14_5;
    struct s22* rbx6;
    struct s0* r13_7;
    struct s23* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s24* rbx12;
    struct s0* rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2420();
    fun_2640(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x90f4, __return_address(), rcx1);
    goto v15;
}

void fun_9138() {
    fun_2420();
    goto 0x90fb;
}
