#include <stdint.h>

/* /tmp/tmpp4rbmgt4 @ 0x27b0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmpp4rbmgt4 @ 0x6700 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000a44f;
        rdx = 0x0000a440;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000a447;
        rdx = 0x0000a449;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000a44b;
    rdx = 0x0000a444;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x9a90 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x2590 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmpp4rbmgt4 @ 0x67e0 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x2680)() ();
    }
    rdx = 0x0000a4c0;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xa4c0 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000a453;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000a449;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000a4ec;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xa4ec */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000a447;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000a449;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000a447;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000a449;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000a5ec;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xa5ec */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000a6ec;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xa6ec */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000a449;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000a447;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000a447;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000a449;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmpp4rbmgt4 @ 0x2680 */
 
void quotearg_buffer_restyled_cold (void) {
    /* [16] -r-x section size 30258 named .text */
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x7c00 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x2685)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2685 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x268a */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2380 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2690 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2695 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x269a */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x269f */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x26a4 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x26a9 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x26ae */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x26b3 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x26b8 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x28a0 */
 
int32_t dbg_count_items (item * unused) {
    rdi = unused;
    /* _Bool count_items(item * unused); */
    *(obj.n_strings)++;
    eax = 0;
    return eax;
}

/* /tmp/tmpp4rbmgt4 @ 0x28b0 */
 
uint64_t recurse_tree (int64_t arg_8h, uint32_t arg_10h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    do {
        rbx = rsi;
label_0:
        rdi = *((rbp + 8));
        if (rdi == 0) {
            goto label_2;
        }
        rsi = rbx;
        al = recurse_tree ();
    } while (1);
    if (al != 0) {
        goto label_3;
    }
label_1:
    rdi = rbp;
    al = void (*rbx)() ();
    if (al != 0) {
        goto label_3;
    }
    rbp = *((rbp + 0x10));
    if (rbp != 0) {
        goto label_0;
    }
    return al;
label_3:
    eax = 1;
    return eax;
label_2:
    if (*((rbp + 0x10)) != 0) {
        goto label_1;
    }
    rdi = rbp;
    rax = rbx;
    return void (*rax)() ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2920 */
 
int64_t scan_zeros (uint32_t arg1) {
    rdi = arg1;
    if (*((rdi + 0x20)) != 0) {
        goto label_0;
    }
    if (*((rdi + 0x19)) != 0) {
        goto label_0;
    }
    if (*(obj.head) == 0) {
        void (*0x2950)() ();
    }
    rax = zeros;
    *((rax + 0x28)) = rdi;
    *(obj.zeros) = rdi;
label_0:
    eax = 0;
    return rax;
    __asm ("in eax, dx");
}

/* /tmp/tmpp4rbmgt4 @ 0x2960 */
 
int64_t detect_loop (uint32_t arg_28h, uint32_t arg1) {
    rdi = arg1;
    r8d = 0;
    if (*((rdi + 0x20)) == 0) {
        goto label_0;
    }
    rbx = loop;
    if (rbx == 0) {
        goto label_1;
    }
    rax = *((rdi + 0x30));
    r13 = rdi + 0x30;
    if (rax != 0) {
        goto label_2;
    }
    goto label_3;
    do {
        r13 = rax + 8;
        rax = *((rax + 8));
        if (rax == 0) {
            goto label_4;
        }
label_2:
    } while (rbx != *(rax));
    if (*((rbp + 0x28)) == 0) {
        goto label_5;
    }
    r14 = 0x0000a3df;
    while (rbp != r12) {
        *((r12 + 0x28)) = 0;
        *(obj.loop) = rbx;
        if (rbx == 0) {
            goto label_6;
        }
        rax = rbx;
        rcx = *(rax);
        eax = 0;
        rbx = *((rbx + 0x28));
        error (0, 0, r14);
        r12 = loop;
    }
    rax = *(rdi);
    *((rax + 0x20))--;
    rax = *((rdi + 8));
    *(r13) = rax;
    free (*(r13));
    do {
        rax = r12;
        r12 = *((r12 + 0x28));
        *((rax + 0x28)) = 0;
    } while (r12 != 0);
    *(obj.loop) = 0;
label_6:
    r8d = 1;
    do {
label_3:
        eax = r8d;
        return rax;
label_0:
        eax = r8d;
        return rax;
label_1:
        eax = r8d;
        *(obj.loop) = rdi;
        return rax;
label_4:
        r8d = 0;
        eax = r8d;
        return rax;
label_5:
        *((rbp + 0x28)) = rbx;
        r8d = 0;
        *(obj.loop) = rbp;
    } while (1);
}

/* /tmp/tmpp4rbmgt4 @ 0x2e00 */
 
uint64_t dbg_search_item (int64_t arg_8h, uint32_t arg_10h, int64_t arg1, int64_t arg2) {
    uint32_t var_8h;
    rdi = arg1;
    rsi = arg2;
    /* item * search_item(item * root,char const * str); */
    r15 = rsi;
    r12 = *((rdi + 0x10));
    if (r12 == 0) {
        goto label_6;
    }
    if (rsi == 0) {
        goto label_7;
    }
    r13 = r12;
    while (eax != 0) {
        rdx = *((r12 + 0x10));
        __asm ("cmovs rdx, qword [r12 + 8]");
        if (rdx == 0) {
            goto label_8;
        }
        if (*((rdx + 0x18)) != 0) {
            r13 = rdx;
        }
        if (*((rdx + 0x18)) != 0) {
        }
        r12 = rdx;
        rsi = *(r12);
        if (rsi == 0) {
            goto label_7;
        }
        eax = strcmp (r15, rsi);
        ebx = eax;
    }
label_1:
    rax = r12;
    return rax;
label_8:
    rax = xzalloc (0x38);
    r14 = rax;
    rax = xstrdup (r15);
    *(r14) = rax;
    if (ebx < 0) {
        goto label_9;
    }
    *((r12 + 0x10)) = r14;
label_2:
    rsi = *(r13);
    if (rsi == 0) {
        goto label_10;
    }
    eax = strcmp (r15, rsi);
    r12d = eax;
    if (eax == 0) {
        goto label_10;
    }
    rax = *((r13 + 0x10));
    __asm ("cmovs rax, qword [r13 + 8]");
    r12d >>= 0x1f;
    r12d |= 1;
    *((rsp + 8)) = rax;
    if (rax == r14) {
        goto label_11;
    }
    if (*((rsp + 8)) == 0) {
        goto label_12;
    }
    rbx = *((rsp + 8));
    while (eax >= 0) {
        *((rbx + 0x18)) = 1;
        rbx = *((rbx + 0x10));
        if (rbx == r14) {
            goto label_11;
        }
label_0:
        if (rbx == 0) {
            goto label_12;
        }
        rsi = *(rbx);
        if (rsi == 0) {
            goto label_12;
        }
        eax = strcmp (r15, rsi);
        if (eax == 0) {
            goto label_12;
        }
    }
    *((rbx + 0x18)) = 0xff;
    rbx = *((rbx + 8));
    if (rbx != r14) {
        goto label_0;
    }
label_11:
    eax = *((r13 + 0x18));
    if (al == 0) {
        goto label_13;
    }
    edx = r12d;
    ecx = (int32_t) al;
    edx = -edx;
    if (ecx == edx) {
        goto label_13;
    }
    rax = *((rsp + 8));
    rcx = *((rsp + 8));
    eax = *((rax + 0x18));
    if (eax == r12d) {
        goto label_14;
    }
    rax = *((rcx + 8));
    rcx = *((rcx + 0x10));
    if (r12d == 0xffffffff) {
        goto label_15;
    }
    rcx = *((rax + 0x10));
    rdi = *((rsp + 8));
    *((rdi + 8)) = rcx;
    rcx = *((rax + 8));
    *((rax + 0x10)) = rdi;
    *((r13 + 0x10)) = rcx;
    *((rax + 8)) = r13;
label_5:
    rcx = *((rsp + 8));
    *((r13 + 0x18)) = 0;
    *((rcx + 0x18)) = 0;
    ecx = *((rax + 0x18));
    if (ecx == r12d) {
        goto label_16;
    }
    if (edx == ecx) {
        rcx = *((rsp + 8));
        *((rcx + 0x18)) = r12b;
    }
label_4:
    *((rax + 0x18)) = 0;
    do {
        if (r13 == *((rbp + 0x10))) {
            goto label_17;
        }
        *((rbp + 8)) = rax;
        r12 = r14;
        goto label_1;
label_13:
        eax += r12d;
        r12 = r14;
        *((r13 + 0x18)) = al;
        goto label_1;
label_9:
        *((r12 + 8)) = r14;
        goto label_2;
label_6:
        rax = xzalloc (0x38);
        r12 = rax;
        if (r15 != 0) {
            rax = xstrdup (r15);
            *(r12) = rax;
        }
        *((rbp + 0x10)) = r12;
        goto label_1;
label_14:
        if (r12d == 0xffffffff) {
            goto label_18;
        }
        rax = *((rcx + 8));
        *((r13 + 0x10)) = rax;
        *((rcx + 8)) = r13;
label_3:
        rax = *((rsp + 8));
        *((rax + 0x18)) = 0;
        *((r13 + 0x18)) = 0;
    } while (1);
label_17:
    *((rbp + 0x10)) = rax;
    r12 = r14;
    goto label_1;
label_18:
    rax = *((rcx + 0x10));
    *((r13 + 8)) = rax;
    *((rcx + 0x10)) = r13;
    goto label_3;
label_16:
    r12d = -r12d;
    *((r13 + 0x18)) = r12b;
    goto label_4;
label_15:
    rax = *((rcx + 8));
    rdi = *((rsp + 8));
    *((rdi + 0x10)) = rax;
    rax = *((rcx + 0x10));
    *((rcx + 8)) = rdi;
    *((r13 + 8)) = rax;
    rax = rcx;
    *((rcx + 0x10)) = r13;
    goto label_5;
label_12:
    assert_fail ("str && p && p->str && !STREQ (str, p->str)", "src/tsort.c", 0xb1, "search_item");
label_7:
    assert_fail ("str && p && p->str", "src/tsort.c", 0x8c, "search_item");
label_10:
    return assert_fail ("str && s && s->str && !STREQ (str, s->str)", "src/tsort.c", 0xa3, "search_item");
}

/* /tmp/tmpp4rbmgt4 @ 0x30f0 */
 
int64_t dbg_tsort (int64_t arg_10h, int64_t arg1) {
    token_buffer tokenbuffer;
    int64_t status;
    int64_t var_10h;
    int64_t var_18h;
    uint32_t var_24h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_48h;
    rdi = arg1;
    /* void tsort(char const * file); */
    r15 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x48)) = rax;
    eax = 0;
    eax = *(rdi);
    eax -= 0x2d;
    *((rsp + 0x24)) = eax;
    if (eax == 0) {
        eax = *((rdi + 1));
        *((rsp + 0x24)) = eax;
    }
    rax = xzalloc (0x38);
    eax = *((rsp + 0x24));
    if (eax != 0) {
        rax = freopen_safer (r15, 0x0000a3d9, *(obj.stdin));
        if (rax == 0) {
            goto label_186;
        }
    }
    r12 = rsp + 0x30;
    r13 = 0x0000a0cc;
    fadvise (*(obj.stdin), 2);
    rdi = r12;
    rax = init_tokenbuffer ();
label_8:
    ebx = 0;
    while (rax != -1) {
        if (rax == 0) {
            goto label_187;
        }
        rax = search_item (rbp, *((rsp + 0x38)), rdx, rcx);
        r14 = rax;
        if (rbx != 0) {
            goto label_188;
        }
        rbx = r14;
        rax = readtoken (*(obj.stdin), r13, 3, r12, r8, r9);
    }
    if (rbx != 0) {
        goto label_189;
    }
    rdi = *((rbp + 0x10));
    if (rdi != 0) {
        rsi = dbg_count_items;
        recurse_tree ();
    }
    *((rsp + 8)) = 1;
    rbx = sym_scan_zeros;
label_6:
    if (*(obj.n_strings) != 0) {
        r12 = *((rbp + 0x10));
        if (r12 == 0) {
            goto label_14;
        }
        r14 = *((r12 + 8));
        if (r14 == 0) {
            goto label_190;
        }
        r13 = *((r14 + 8));
        if (r13 == 0) {
            goto label_191;
        }
        r8 = *((r13 + 8));
        if (r8 == 0) {
            goto label_192;
        }
        r9 = *((r8 + 8));
        if (r9 == 0) {
            goto label_193;
        }
        rcx = *((r9 + 8));
        if (rcx == 0) {
            goto label_194;
        }
        rdi = *((rcx + 8));
        if (rdi == 0) {
            goto label_195;
        }
        rsi = rbx;
        *((rsp + 0x28)) = rcx;
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        al = recurse_tree ();
        if (al == 0) {
            rcx = *((rsp + 0x28));
            r8 = *((rsp + 0x10));
            r9 = *((rsp + 0x18));
            rdi = *((rcx + 0x10));
            if (*((rcx + 0x20)) == 0) {
                goto label_43;
            }
label_17:
            if (rdi != 0) {
                goto label_196;
            }
label_44:
            rcx = *((r9 + 0x10));
            if (*((r9 + 0x20)) == 0) {
label_37:
                if (*((r9 + 0x19)) != 0) {
                    goto label_197;
                }
                if (*(obj.head) == 0) {
                    goto label_198;
                }
                rax = zeros;
                *((rax + 0x28)) = r9;
label_87:
                *(obj.zeros) = r9;
            }
label_197:
            if (rcx != 0) {
                goto label_199;
            }
label_38:
            rcx = *((r8 + 0x10));
            if (*((r8 + 0x20)) == 0) {
label_12:
                if (*((r8 + 0x19)) != 0) {
                    goto label_200;
                }
                if (*(obj.head) == 0) {
                    goto label_201;
                }
                rax = zeros;
                *((rax + 0x28)) = r8;
label_54:
                *(obj.zeros) = r8;
            }
label_200:
            if (rcx != 0) {
                goto label_202;
            }
label_13:
            rcx = *((r13 + 0x10));
            if (*((r13 + 0x20)) == 0) {
                goto label_203;
            }
label_10:
            if (rcx != 0) {
                goto label_204;
            }
label_26:
            r13 = *((r14 + 0x10));
            if (*((r14 + 0x20)) == 0) {
                goto label_205;
            }
label_5:
            if (r13 != 0) {
                goto label_206;
            }
label_19:
            r14 = *((r12 + 0x10));
            if (*((r12 + 0x20)) == 0) {
label_3:
                if (*((r12 + 0x19)) != 0) {
                    goto label_207;
                }
                if (*(obj.head) == 0) {
                    goto label_208;
                }
                rax = zeros;
                *((rax + 0x28)) = r12;
label_11:
                *(obj.zeros) = r12;
            }
label_207:
            if (r14 != 0) {
                goto label_209;
            }
        }
label_4:
        rax = head;
        if (rax != 0) {
            goto label_0;
        }
        rdi = n_strings;
label_1:
        if (rdi != 0) {
            goto label_210;
        }
    }
    eax = rpl_fclose (*(obj.stdin));
    if (eax != 0) {
        goto label_211;
    }
    edi = *((rsp + 8));
    edi = (int32_t) dil;
    rax = exit (1);
label_2:
    if (rax == 0) {
        goto label_212;
    }
label_14:
    rax = head;
    if (rax == 0) {
        goto label_210;
    }
label_0:
    r12 = *((rax + 0x30));
    puts (*(rax));
    rax = n_strings;
    r8 = head;
    esi = 0;
    rcx = zeros;
    rdi = rax - 1;
    *((r8 + 0x19)) = 1;
    *(obj.n_strings) = rdi;
    if (r12 == 0) {
        goto label_213;
    }
    do {
        rdx = *(r12);
        *((rdx + 0x20))--;
        if (*((rdx + 0x20)) == 0) {
            *((rcx + 0x28)) = rdx;
            esi = 1;
            rcx = rdx;
        }
        r12 = *((r12 + 8));
    } while (r12 != 0);
    if (sil != 0) {
        *(obj.zeros) = rcx;
    }
label_213:
    rax = *((r8 + 0x28));
    *(obj.head) = rax;
    if (rax != 0) {
        goto label_0;
    }
    goto label_1;
label_190:
    r14 = *((r12 + 0x10));
    rax = *((r12 + 0x20));
    if (r14 == 0) {
        goto label_2;
    }
    if (rax == 0) {
        goto label_3;
    }
label_209:
    r13 = *((r14 + 8));
    if (r13 == 0) {
        goto label_214;
    }
    rcx = *((r13 + 8));
    if (rcx == 0) {
        goto label_215;
    }
    r8 = *((rcx + 8));
    if (r8 == 0) {
        goto label_216;
    }
    r12 = *((r8 + 8));
    if (r12 == 0) {
        goto label_217;
    }
    rdi = *((r12 + 8));
    if (rdi == 0) {
        goto label_218;
    }
    rsi = rbx;
    *((rsp + 0x18)) = r8;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    r8 = *((rsp + 0x18));
    rdi = *((r12 + 0x10));
    if (*((r12 + 0x20)) == 0) {
label_96:
        if (*((r12 + 0x19)) != 0) {
            goto label_219;
        }
        if (*(obj.head) == 0) {
            goto label_220;
        }
        rax = zeros;
        *((rax + 0x28)) = r12;
label_124:
        *(obj.zeros) = r12;
    }
label_219:
    if (rdi != 0) {
        goto label_221;
    }
label_97:
    r12 = *((r8 + 0x10));
    if (*((r8 + 0x20)) == 0) {
label_21:
        if (*((r8 + 0x19)) != 0) {
            goto label_222;
        }
        if (*(obj.head) == 0) {
            goto label_223;
        }
        rax = zeros;
        *((rax + 0x28)) = r8;
label_78:
        *(obj.zeros) = r8;
    }
label_222:
    if (r12 == 0) {
        goto label_77;
    }
    goto label_224;
label_191:
    r13 = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (r13 == 0) {
        goto label_225;
    }
    if (rax != 0) {
        goto label_206;
    }
label_205:
    if (*((r14 + 0x19)) != 0) {
        goto label_5;
    }
    if (*(obj.head) == 0) {
        goto label_226;
    }
    rax = zeros;
    *((rax + 0x28)) = r14;
label_33:
    *(obj.zeros) = r14;
    goto label_5;
label_206:
    rcx = *((r13 + 8));
    if (rcx == 0) {
        goto label_227;
    }
    r8 = *((rcx + 8));
    if (r8 == 0) {
        goto label_228;
    }
    r14 = *((r8 + 8));
    if (r14 == 0) {
        goto label_229;
    }
    rdi = *((r14 + 8));
    if (rdi == 0) {
        goto label_230;
    }
    rsi = rbx;
    *((rsp + 0x18)) = r8;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    r8 = *((rsp + 0x18));
    rdi = *((r14 + 0x10));
    if (*((r14 + 0x20)) == 0) {
label_99:
        if (*((r14 + 0x19)) != 0) {
            goto label_231;
        }
        if (*(obj.head) == 0) {
            goto label_232;
        }
        rax = zeros;
        *((rax + 0x28)) = r14;
label_126:
        *(obj.zeros) = r14;
    }
label_231:
    if (rdi != 0) {
        goto label_233;
    }
label_100:
    r14 = *((r8 + 0x10));
    if (*((r8 + 0x20)) == 0) {
label_18:
        if (*((r8 + 0x19)) != 0) {
            goto label_234;
        }
        if (*(obj.head) == 0) {
            goto label_235;
        }
        rax = zeros;
        *((rax + 0x28)) = r8;
label_85:
        *(obj.zeros) = r8;
    }
label_234:
    if (r14 == 0) {
        goto label_75;
    }
    goto label_236;
label_187:
    assert_fail ("len != 0", "src/tsort.c", 0x1ca, "tsort");
label_210:
    rdx = r15;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: input contains a loop:");
    rcx = r12;
    eax = 0;
    r12 = sym_detect_loop;
    error (0, 0, rax);
    do {
        r13 = *((rbp + 0x10));
        if (r13 != 0) {
            r14 = *((r13 + 8));
            if (r14 == 0) {
                goto label_237;
            }
            rax = *((r14 + 8));
            *((rsp + 8)) = rax;
            if (rax == 0) {
                goto label_238;
            }
            rax = *((rsp + 8));
            rax = *((rax + 8));
            *((rsp + 0x10)) = rax;
            if (rax == 0) {
                goto label_239;
            }
            rax = *((rsp + 0x10));
            rax = *((rax + 8));
            *((rsp + 0x18)) = rax;
            if (rax == 0) {
                goto label_240;
            }
            rax = *((rsp + 0x18));
            r8 = *((rax + 8));
            if (r8 == 0) {
                goto label_241;
            }
            rdi = *((r8 + 8));
            if (rdi == 0) {
                goto label_242;
            }
            rsi = r12;
            *((rsp + 0x28)) = r8;
            al = recurse_tree ();
            r8 = *((rsp + 0x28));
            if (al != 0) {
                goto label_7;
            }
label_101:
            rdi = r8;
            *((rsp + 0x28)) = r8;
            al = detect_loop ();
            r8 = *((rsp + 0x28));
            if (al != 0) {
                goto label_7;
            }
            rdi = *((r8 + 0x10));
            if (rdi != 0) {
                rsi = r12;
                al = recurse_tree ();
                if (al != 0) {
                    goto label_7;
                }
            }
label_47:
            rdi = *((rsp + 0x18));
            al = detect_loop ();
            if (al != 0) {
                goto label_7;
            }
            rax = *((rsp + 0x18));
            rdi = *((rax + 0x10));
            if (rdi == 0) {
                goto label_48;
            }
            rsi = r12;
            al = recurse_tree ();
            if (al == 0) {
                goto label_48;
            }
        }
label_7:
        *((rsp + 8)) = 0;
    } while (*(obj.loop) != 0);
    goto label_6;
label_237:
    if (*((r13 + 0x10)) == 0) {
        goto label_118;
    }
label_9:
    rdi = r13;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rax = *((r13 + 0x10));
    *((rsp + 8)) = rax;
    if (rax == 0) {
        goto label_7;
    }
    r13 = *((rax + 8));
    if (r13 == 0) {
        goto label_243;
    }
    r14 = *((r13 + 8));
    if (r14 == 0) {
        goto label_244;
    }
    r8 = *((r14 + 8));
    if (r8 == 0) {
        goto label_245;
    }
    rdi = *((r8 + 8));
    if (rdi == 0) {
        goto label_246;
    }
    rsi = r12;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    r8 = *((rsp + 0x10));
    if (al != 0) {
        goto label_7;
    }
label_72:
    rdi = r8;
    *((rsp + 0x10)) = r8;
    al = detect_loop ();
    r8 = *((rsp + 0x10));
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r8 + 0x10));
    if (rdi == 0) {
        goto label_45;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al != 0) {
        goto label_7;
    }
label_45:
    rdi = r14;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r14 + 0x10));
    if (rdi == 0) {
        goto label_46;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al == 0) {
        goto label_46;
    }
    goto label_7;
label_188:
    eax = strcmp (*(rbx), *(rax));
    if (eax == 0) {
        goto label_8;
    }
    *((r14 + 0x20))++;
    xmalloc (0x10);
    rdx = *((rbx + 0x30));
    *(rax) = r14;
    *((rax + 8)) = rdx;
    *((rbx + 0x30)) = rax;
    goto label_8;
label_238:
    if (*((r14 + 0x10)) == 0) {
        goto label_247;
    }
label_25:
    rdi = r14;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rax = *((r14 + 0x10));
    *((rsp + 8)) = rax;
    if (rax == 0) {
        goto label_9;
    }
    r14 = *((rax + 8));
    if (r14 == 0) {
        goto label_248;
    }
    r8 = *((r14 + 8));
    if (r8 == 0) {
        goto label_249;
    }
    rdi = *((r8 + 8));
    if (rdi == 0) {
        goto label_250;
    }
    rsi = r12;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    r8 = *((rsp + 0x10));
    if (al != 0) {
        goto label_7;
    }
label_88:
    rdi = r8;
    *((rsp + 0x10)) = r8;
    al = detect_loop ();
    r8 = *((rsp + 0x10));
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r8 + 0x10));
    if (rdi == 0) {
        goto label_49;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al != 0) {
        goto label_7;
    }
label_49:
    rdi = r14;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r14 + 0x10));
    if (rdi == 0) {
        goto label_50;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al == 0) {
        goto label_50;
    }
    goto label_7;
label_192:
    rcx = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (rcx == 0) {
        goto label_128;
    }
    if (rax != 0) {
        goto label_204;
    }
label_203:
    if (*((r13 + 0x19)) != 0) {
        goto label_10;
    }
    if (*(obj.head) == 0) {
        goto label_251;
    }
    rax = zeros;
    *((rax + 0x28)) = r13;
label_16:
    *(obj.zeros) = r13;
    goto label_10;
label_204:
    r13 = *((rcx + 8));
    if (r13 == 0) {
        goto label_252;
    }
    rdx = *((r13 + 8));
    if (rdx == 0) {
        goto label_253;
    }
    rdi = *((rdx + 8));
    if (rdi == 0) {
        goto label_254;
    }
    rsi = rbx;
    *((rsp + 0x18)) = rcx;
    *((rsp + 0x10)) = rdx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rdx = *((rsp + 0x10));
    rcx = *((rsp + 0x18));
    rdi = *((rdx + 0x10));
    if (*((rdx + 0x20)) == 0) {
label_122:
        if (*((rdx + 0x19)) != 0) {
            goto label_255;
        }
        if (*(obj.head) == 0) {
            goto label_256;
        }
        rax = zeros;
        *((rax + 0x28)) = rdx;
label_127:
        *(obj.zeros) = rdx;
    }
label_255:
    if (rdi != 0) {
        goto label_257;
    }
label_123:
    rdx = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_27:
        if (*((r13 + 0x19)) != 0) {
            goto label_258;
        }
        if (*(obj.head) == 0) {
            goto label_259;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_70:
        *(obj.zeros) = r13;
    }
label_258:
    if (rdx == 0) {
        goto label_68;
    }
    goto label_260;
label_214:
    r12 = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (r12 == 0) {
        goto label_131;
    }
    if (rax != 0) {
        goto label_261;
    }
label_24:
    if (*((r14 + 0x19)) == 0) {
        if (*(obj.head) == 0) {
            goto label_262;
        }
        rax = zeros;
        *((rax + 0x28)) = r14;
label_15:
        *(obj.zeros) = r14;
    }
label_23:
    if (r12 == 0) {
        goto label_4;
    }
label_261:
    r13 = *((r12 + 8));
    if (r13 == 0) {
        goto label_263;
    }
    r14 = *((r13 + 8));
    if (r14 == 0) {
        goto label_264;
    }
    rcx = *((r14 + 8));
    if (rcx == 0) {
        goto label_265;
    }
    rdi = *((rcx + 8));
    if (rdi == 0) {
        goto label_266;
    }
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    rdi = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
label_51:
        if (*((rcx + 0x19)) != 0) {
            goto label_267;
        }
        if (*(obj.head) == 0) {
            goto label_268;
        }
        rax = zeros;
        *((rax + 0x28)) = rcx;
label_89:
        *(obj.zeros) = rcx;
    }
label_267:
    if (rdi == 0) {
        goto label_92;
    }
    goto label_269;
label_208:
    *(obj.head) = r12;
    goto label_11;
label_227:
    r14 = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (r14 == 0) {
        goto label_108;
    }
    if (rax == 0) {
        goto label_270;
    }
label_20:
    r13 = *((r14 + 8));
    if (r13 == 0) {
        goto label_271;
    }
    rcx = *((r13 + 8));
    if (rcx == 0) {
        goto label_272;
    }
    rdi = *((rcx + 8));
    if (rdi == 0) {
        goto label_273;
    }
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    rdi = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
label_58:
        if (*((rcx + 0x19)) != 0) {
            goto label_274;
        }
        if (*(obj.head) == 0) {
            goto label_275;
        }
        rax = zeros;
        *((rax + 0x28)) = rcx;
label_169:
        *(obj.zeros) = rcx;
    }
label_274:
    if (rdi == 0) {
        goto label_165;
    }
    goto label_276;
label_193:
    rcx = *((r8 + 0x10));
    rax = *((r8 + 0x20));
    if (rcx == 0) {
        goto label_149;
    }
    if (rax == 0) {
        goto label_12;
    }
label_202:
    r8 = *((rcx + 8));
    if (r8 == 0) {
        goto label_277;
    }
    rdi = *((r8 + 8));
    if (rdi == 0) {
        goto label_278;
    }
    rsi = rbx;
    *((rsp + 0x18)) = rcx;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    r8 = *((rsp + 0x10));
    rcx = *((rsp + 0x18));
    rdi = *((r8 + 0x10));
    if (*((r8 + 0x20)) == 0) {
label_62:
        if (*((r8 + 0x19)) != 0) {
            goto label_279;
        }
        if (*(obj.head) == 0) {
            goto label_280;
        }
        rax = zeros;
        *((rax + 0x28)) = r8;
label_66:
        *(obj.zeros) = r8;
    }
label_279:
    if (rdi != 0) {
        goto label_281;
    }
label_63:
    r8 = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
label_60:
        if (*((rcx + 0x19)) != 0) {
            goto label_282;
        }
        if (*(obj.head) == 0) {
            goto label_283;
        }
        rax = zeros;
        *((rax + 0x28)) = rcx;
label_64:
        *(obj.zeros) = rcx;
    }
label_282:
    if (r8 == 0) {
        goto label_13;
    }
label_61:
    rdi = *((r8 + 8));
    if (rdi == 0) {
        goto label_284;
    }
    rsi = rbx;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    r8 = *((rsp + 0x10));
    rdi = *((r8 + 0x10));
    if (*((r8 + 0x20)) == 0) {
label_151:
        if (*((r8 + 0x19)) != 0) {
            goto label_285;
        }
        if (*(obj.head) == 0) {
            goto label_286;
        }
        rax = zeros;
        *((rax + 0x28)) = r8;
label_182:
        *(obj.zeros) = r8;
    }
label_285:
    if (rdi == 0) {
        goto label_13;
    }
label_150:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_13;
    }
    goto label_4;
label_215:
    r12 = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (r12 == 0) {
        goto label_113;
    }
    if (rax == 0) {
        goto label_287;
    }
label_22:
    r13 = *((r12 + 8));
    if (r13 == 0) {
        goto label_288;
    }
    rcx = *((r13 + 8));
    if (rcx == 0) {
        goto label_289;
    }
    rdi = *((rcx + 8));
    if (rdi == 0) {
        goto label_290;
    }
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    rdi = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
label_171:
        if (*((rcx + 0x19)) != 0) {
            goto label_291;
        }
        if (*(obj.head) == 0) {
            goto label_292;
        }
        rax = zeros;
        *((rax + 0x28)) = rcx;
label_180:
        *(obj.zeros) = rcx;
    }
label_291:
    if (rdi != 0) {
        goto label_293;
    }
label_172:
    rdi = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_173:
        if (*((r13 + 0x19)) != 0) {
            goto label_294;
        }
        if (*(obj.head) == 0) {
            goto label_295;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_181:
        *(obj.zeros) = r13;
    }
label_294:
    if (rdi != 0) {
        goto label_296;
    }
label_174:
    r13 = *((r12 + 0x10));
    if (*((r12 + 0x20)) == 0) {
label_40:
        if (*((r12 + 0x19)) != 0) {
            goto label_297;
        }
        if (*(obj.head) == 0) {
            goto label_298;
        }
        rax = zeros;
        *((rax + 0x28)) = r12;
label_105:
        *(obj.zeros) = r12;
    }
label_297:
    if (r13 == 0) {
        goto label_41;
    }
    goto label_299;
label_212:
    if (*((r12 + 0x19)) != 0) {
        goto label_14;
    }
    if (*(obj.head) == 0) {
        goto label_300;
    }
    rax = zeros;
    *((rax + 0x28)) = r12;
label_170:
    *(obj.zeros) = r12;
    goto label_14;
label_262:
    *(obj.head) = r14;
    goto label_15;
label_251:
    *(obj.head) = r13;
    goto label_16;
label_264:
    r14 = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (r14 == 0) {
        goto label_301;
    }
    if (rax == 0) {
        goto label_302;
    }
label_53:
    rdi = *((r14 + 8));
    if (rdi == 0) {
        goto label_303;
    }
    rsi = rbx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rdi = *((r14 + 0x10));
    if (*((r14 + 0x20)) == 0) {
label_71:
        if (*((r14 + 0x19)) != 0) {
            goto label_304;
        }
        if (*(obj.head) == 0) {
            goto label_305;
        }
        rax = zeros;
        *((rax + 0x28)) = r14;
label_95:
        *(obj.zeros) = r14;
    }
label_304:
    if (rdi != 0) {
        goto label_306;
    }
label_52:
    r14 = *((r12 + 0x10));
    if (*((r12 + 0x20)) == 0) {
label_35:
        if (*((r12 + 0x19)) != 0) {
            goto label_307;
        }
        if (*(obj.head) == 0) {
            goto label_308;
        }
        rax = zeros;
        *((rax + 0x28)) = r12;
label_120:
        *(obj.zeros) = r12;
    }
label_307:
    if (r14 == 0) {
        goto label_4;
    }
label_36:
    r12 = *((r14 + 8));
    if (r12 == 0) {
        goto label_309;
    }
    rdi = *((r12 + 8));
    if (rdi == 0) {
        goto label_310;
    }
    rsi = rbx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rdi = *((r12 + 0x10));
    if (*((r12 + 0x20)) == 0) {
label_79:
        if (*((r12 + 0x19)) != 0) {
            goto label_311;
        }
        if (*(obj.head) == 0) {
            goto label_312;
        }
        rax = zeros;
        *((rax + 0x28)) = r12;
label_83:
        *(obj.zeros) = r12;
    }
label_311:
    if (rdi != 0) {
        goto label_313;
    }
label_80:
    rdi = *((r14 + 0x10));
    if (*((r14 + 0x20)) == 0) {
label_132:
        if (*((r14 + 0x19)) != 0) {
            goto label_314;
        }
        if (*(obj.head) == 0) {
            goto label_315;
        }
        rax = zeros;
        *((rax + 0x28)) = r14;
label_81:
        *(obj.zeros) = r14;
    }
label_314:
    if (rdi == 0) {
        goto label_4;
    }
label_133:
    rsi = rbx;
    recurse_tree ();
    goto label_4;
label_43:
    if (*((rcx + 0x19)) != 0) {
        goto label_17;
    }
    if (*(obj.head) == 0) {
        goto label_316;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_145:
    *(obj.zeros) = rcx;
    goto label_17;
label_229:
    r14 = *((r8 + 0x10));
    rax = *((r8 + 0x20));
    if (r14 == 0) {
        goto label_317;
    }
    if (rax == 0) {
        goto label_18;
    }
label_236:
    rdi = *((r14 + 8));
    if (rdi == 0) {
        goto label_318;
    }
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    rdi = *((r14 + 0x10));
    if (*((r14 + 0x20)) == 0) {
label_74:
        if (*((r14 + 0x19)) != 0) {
            goto label_319;
        }
        if (*(obj.head) == 0) {
            goto label_320;
        }
        rax = zeros;
        *((rax + 0x28)) = r14;
label_69:
        *(obj.zeros) = r14;
    }
label_319:
    if (rdi != 0) {
        goto label_321;
    }
label_75:
    r14 = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
        goto label_322;
    }
label_34:
    if (r14 != 0) {
        goto label_323;
    }
label_134:
    r14 = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_270:
        if (*((r13 + 0x19)) != 0) {
            goto label_324;
        }
        if (*(obj.head) == 0) {
            goto label_325;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_55:
        *(obj.zeros) = r13;
    }
label_324:
    if (r14 == 0) {
        goto label_19;
    }
    goto label_20;
label_217:
    r12 = *((r8 + 0x10));
    rax = *((r8 + 0x20));
    if (r12 == 0) {
        goto label_326;
    }
    if (rax == 0) {
        goto label_21;
    }
label_224:
    rdi = *((r12 + 8));
    if (rdi == 0) {
        goto label_327;
    }
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    rdi = *((r12 + 0x10));
    if (*((r12 + 0x20)) == 0) {
label_82:
        if (*((r12 + 0x19)) != 0) {
            goto label_328;
        }
        if (*(obj.head) == 0) {
            goto label_329;
        }
        rax = zeros;
        *((rax + 0x28)) = r12;
label_76:
        *(obj.zeros) = r12;
    }
label_328:
    if (rdi != 0) {
        goto label_330;
    }
label_77:
    r12 = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
        goto label_331;
    }
label_32:
    if (r12 != 0) {
        goto label_332;
    }
label_31:
    r12 = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_287:
        if (*((r13 + 0x19)) != 0) {
            goto label_333;
        }
        if (*(obj.head) == 0) {
            goto label_334;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_57:
        *(obj.zeros) = r13;
    }
label_333:
    if (r12 != 0) {
        goto label_22;
    }
label_41:
    r12 = *((r14 + 0x10));
    if (*((r14 + 0x20)) != 0) {
        goto label_23;
    }
    goto label_24;
label_239:
    rax = *((rsp + 8));
    if (*((rax + 0x10)) == 0) {
        goto label_335;
    }
label_42:
    rdi = *((rsp + 8));
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rax = *((rsp + 8));
    rax = *((rax + 0x10));
    *((rsp + 8)) = rax;
    if (rax == 0) {
        goto label_25;
    }
    r8 = *((rax + 8));
    if (r8 == 0) {
        goto label_336;
    }
    rdi = *((r8 + 8));
    if (rdi == 0) {
        goto label_337;
    }
    rsi = r12;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    r8 = *((rsp + 0x10));
    if (al != 0) {
        goto label_7;
    }
label_107:
    rdi = r8;
    *((rsp + 0x10)) = r8;
    al = detect_loop ();
    r8 = *((rsp + 0x10));
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r8 + 0x10));
    if (rdi == 0) {
        goto label_56;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al != 0) {
        goto label_7;
    }
label_56:
    rdi = *((rsp + 8));
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rax = *((rsp + 8));
    rdi = *((rax + 0x10));
    if (rdi == 0) {
        goto label_25;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al == 0) {
        goto label_25;
    }
    goto label_7;
label_243:
    if (*((rax + 0x10)) == 0) {
        goto label_338;
    }
label_29:
    rdi = *((rsp + 8));
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rax = *((rsp + 8));
    r13 = *((rax + 0x10));
    if (r13 == 0) {
        goto label_7;
    }
    r14 = *((r13 + 8));
    if (r14 == 0) {
        goto label_339;
    }
    rdi = *((r14 + 8));
    if (rdi == 0) {
        goto label_340;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al != 0) {
        goto label_7;
    }
label_156:
    rdi = r14;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r14 + 0x10));
    if (rdi == 0) {
        goto label_30;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al != 0) {
        goto label_7;
    }
label_30:
    rdi = r13;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    r13 = *((r13 + 0x10));
    if (r13 == 0) {
        goto label_7;
    }
    rdi = *((r13 + 8));
    if (rdi == 0) {
        goto label_341;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al != 0) {
        goto label_7;
    }
label_117:
    rdi = r13;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r13 + 0x10));
    if (rdi == 0) {
        goto label_7;
    }
    rsi = r12;
    recurse_tree ();
    goto label_7;
label_248:
    if (*((rax + 0x10)) == 0) {
        goto label_342;
    }
label_50:
    rdi = *((rsp + 8));
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rax = *((rsp + 8));
    r14 = *((rax + 0x10));
    if (r14 == 0) {
        goto label_9;
    }
    rdi = *((r14 + 8));
    if (rdi == 0) {
        goto label_343;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al != 0) {
        goto label_7;
    }
label_39:
    rdi = r14;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r14 + 0x10));
    if (rdi == 0) {
        goto label_9;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al == 0) {
        goto label_9;
    }
    goto label_7;
label_252:
    r13 = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (r13 == 0) {
        goto label_344;
    }
    if (rax == 0) {
        goto label_345;
    }
label_28:
    rdi = *((r13 + 8));
    if (rdi == 0) {
        goto label_346;
    }
    rsi = rbx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rdi = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_129:
        if (*((r13 + 0x19)) != 0) {
            goto label_347;
        }
        if (*(obj.head) == 0) {
            goto label_348;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_175:
        *(obj.zeros) = r13;
    }
label_347:
    if (rdi == 0) {
        goto label_26;
    }
label_130:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_26;
    }
    goto label_4;
label_253:
    rdx = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (rdx == 0) {
        goto label_349;
    }
    if (rax == 0) {
        goto label_27;
    }
label_260:
    rdi = *((rdx + 8));
    if (rdi == 0) {
        goto label_350;
    }
    rsi = rbx;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rdx = *((rsp + 0x18));
    rcx = *((rsp + 0x10));
    rdi = *((rdx + 0x10));
    if (*((rdx + 0x20)) == 0) {
label_67:
        if (*((rdx + 0x19)) != 0) {
            goto label_351;
        }
        if (*(obj.head) == 0) {
            goto label_352;
        }
        rax = zeros;
        *((rax + 0x28)) = rdx;
label_84:
        *(obj.zeros) = rdx;
    }
label_351:
    if (rdi != 0) {
        goto label_353;
    }
label_68:
    r13 = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
label_345:
        if (*((rcx + 0x19)) != 0) {
            goto label_354;
        }
        if (*(obj.head) == 0) {
            goto label_355;
        }
        rax = zeros;
        *((rax + 0x28)) = rcx;
label_152:
        *(obj.zeros) = rcx;
    }
label_354:
    if (r13 == 0) {
        goto label_26;
    }
    goto label_28;
label_244:
    if (*((r13 + 0x10)) == 0) {
        goto label_356;
    }
label_46:
    rdi = r13;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    r13 = *((r13 + 0x10));
    if (r13 == 0) {
        goto label_29;
    }
    rdi = *((r13 + 8));
    if (rdi == 0) {
        goto label_357;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al != 0) {
        goto label_7;
    }
label_158:
    rdi = r13;
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r13 + 0x10));
    if (rdi == 0) {
        goto label_29;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al == 0) {
        goto label_29;
    }
    goto label_7;
label_339:
    if (*((r13 + 0x10)) != 0) {
        goto label_30;
    }
label_118:
    rdi = r13;
    detect_loop ();
    goto label_7;
label_216:
    r12 = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (r12 == 0) {
        goto label_358;
    }
    if (rax == 0) {
        goto label_331;
    }
label_332:
    rcx = *((r12 + 8));
    if (rcx == 0) {
        goto label_359;
    }
    rdi = *((rcx + 8));
    if (rdi == 0) {
        goto label_360;
    }
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    rdi = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
label_135:
        if (*((rcx + 0x19)) != 0) {
            goto label_361;
        }
        if (*(obj.head) == 0) {
            goto label_362;
        }
        rax = zeros;
        *((rax + 0x28)) = rcx;
label_144:
        *(obj.zeros) = rcx;
    }
label_361:
    if (rdi != 0) {
        goto label_363;
    }
label_136:
    rdi = *((r12 + 0x10));
    if (*((r12 + 0x20)) == 0) {
label_161:
        if (*((r12 + 0x19)) != 0) {
            goto label_364;
        }
        if (*(obj.head) == 0) {
            goto label_365;
        }
        rax = zeros;
        *((rax + 0x28)) = r12;
label_137:
        *(obj.zeros) = r12;
    }
label_364:
    if (rdi == 0) {
        goto label_31;
    }
label_162:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_31;
    }
    goto label_4;
label_331:
    if (*((rcx + 0x19)) != 0) {
        goto label_32;
    }
    if (*(obj.head) == 0) {
        goto label_366;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_164:
    *(obj.zeros) = rcx;
    goto label_32;
label_226:
    *(obj.head) = r14;
    goto label_33;
label_228:
    r14 = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (r14 == 0) {
        goto label_367;
    }
    if (rax == 0) {
        goto label_322;
    }
label_323:
    rcx = *((r14 + 8));
    if (rcx == 0) {
        goto label_368;
    }
    rdi = *((rcx + 8));
    if (rdi == 0) {
        goto label_369;
    }
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x10));
    rdi = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
label_138:
        if (*((rcx + 0x19)) != 0) {
            goto label_370;
        }
        if (*(obj.head) == 0) {
            goto label_371;
        }
        rax = zeros;
        *((rax + 0x28)) = rcx;
label_140:
        *(obj.zeros) = rcx;
    }
label_370:
    if (rdi == 0) {
        goto label_139;
    }
    goto label_372;
label_322:
    if (*((rcx + 0x19)) != 0) {
        goto label_34;
    }
    if (*(obj.head) == 0) {
        goto label_373;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_141:
    *(obj.zeros) = rcx;
    goto label_34;
label_271:
    r13 = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (r13 != 0) {
        goto label_374;
    }
label_225:
    if (rax != 0) {
        goto label_19;
    }
    if (*((r14 + 0x19)) != 0) {
        goto label_19;
    }
    if (*(obj.head) == 0) {
        goto label_375;
    }
    rax = zeros;
    *((rax + 0x28)) = r14;
label_142:
    *(obj.zeros) = r14;
    goto label_19;
label_263:
    r14 = *((r12 + 0x10));
    rax = *((r12 + 0x20));
    if (r14 == 0) {
        goto label_376;
    }
    if (rax == 0) {
        goto label_35;
    }
    goto label_36;
label_194:
    rcx = *((r9 + 0x10));
    rax = *((r9 + 0x20));
    if (rcx == 0) {
        goto label_377;
    }
    if (rax == 0) {
        goto label_37;
    }
label_199:
    rdi = *((rcx + 8));
    if (rdi == 0) {
        goto label_378;
    }
    rsi = rbx;
    *((rsp + 0x18)) = rcx;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rcx = *((rsp + 0x18));
    r8 = *((rsp + 0x10));
    rdi = *((rcx + 0x10));
    if (*((rcx + 0x20)) == 0) {
label_153:
        if (*((rcx + 0x19)) != 0) {
            goto label_379;
        }
        if (*(obj.head) == 0) {
            goto label_380;
        }
        rax = zeros;
        *((rax + 0x28)) = rcx;
label_106:
        *(obj.zeros) = rcx;
    }
label_379:
    if (rdi == 0) {
        goto label_38;
    }
label_154:
    rsi = rbx;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    r8 = *((rsp + 0x10));
    if (al == 0) {
        goto label_38;
    }
    goto label_4;
label_343:
    if (*((r14 + 0x10)) != 0) {
        goto label_39;
    }
label_247:
    rdi = r14;
    al = detect_loop ();
    if (al == 0) {
        goto label_9;
    }
    goto label_7;
label_128:
    if (rax != 0) {
        goto label_26;
    }
    if (*((r13 + 0x19)) != 0) {
        goto label_26;
    }
    if (*(obj.head) == 0) {
        goto label_381;
    }
    rax = zeros;
    *((rax + 0x28)) = r13;
label_116:
    *(obj.zeros) = r13;
    goto label_26;
label_288:
    r13 = *((r12 + 0x10));
    rax = *((r12 + 0x20));
    if (r13 == 0) {
        goto label_382;
    }
    if (rax == 0) {
        goto label_40;
    }
label_299:
    rdi = *((r13 + 8));
    if (rdi == 0) {
        goto label_383;
    }
    rsi = rbx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rdi = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_115:
        if (*((r13 + 0x19)) != 0) {
            goto label_384;
        }
        if (*(obj.head) == 0) {
            goto label_385;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_104:
        *(obj.zeros) = r13;
    }
label_384:
    if (rdi == 0) {
        goto label_41;
    }
label_114:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_41;
    }
    goto label_4;
label_240:
    rax = *((rsp + 0x10));
    if (*((rax + 0x10)) == 0) {
        goto label_386;
    }
label_48:
    rdi = *((rsp + 0x10));
    al = detect_loop ();
    if (al != 0) {
        goto label_7;
    }
    rax = *((rsp + 0x10));
    r8 = *((rax + 0x10));
    if (r8 == 0) {
        goto label_42;
    }
    rdi = *((r8 + 8));
    if (rdi == 0) {
        goto label_387;
    }
    rsi = r12;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    r8 = *((rsp + 0x10));
    if (al != 0) {
        goto label_7;
    }
label_111:
    rdi = r8;
    *((rsp + 0x10)) = r8;
    al = detect_loop ();
    r8 = *((rsp + 0x10));
    if (al != 0) {
        goto label_7;
    }
    rdi = *((r8 + 0x10));
    if (rdi == 0) {
        goto label_42;
    }
    rsi = r12;
    al = recurse_tree ();
    if (al == 0) {
        goto label_42;
    }
    goto label_7;
label_131:
    rdx = head;
    if (rax != 0) {
        goto label_4;
    }
    if (*((r14 + 0x19)) != 0) {
        goto label_4;
    }
    if (rdx == 0) {
        goto label_388;
    }
    rax = zeros;
    *((rax + 0x28)) = r14;
label_112:
    *(obj.zeros) = r14;
    goto label_4;
label_374:
    if (rax == 0) {
        goto label_389;
    }
label_59:
    rdi = *((r13 + 8));
    if (rdi == 0) {
        goto label_390;
    }
    rsi = rbx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
    rdi = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_110:
        if (*((r13 + 0x19)) != 0) {
            goto label_391;
        }
        if (*(obj.head) == 0) {
            goto label_392;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_178:
        *(obj.zeros) = r13;
    }
label_391:
    if (rdi == 0) {
        goto label_19;
    }
label_109:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_19;
    }
    goto label_4;
label_195:
    rdi = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (rdi == 0) {
        goto label_393;
    }
    if (rax == 0) {
        goto label_43;
    }
label_196:
    rsi = rbx;
    *((rsp + 0x18)) = r9;
    *((rsp + 0x10)) = r8;
    al = recurse_tree ();
    r8 = *((rsp + 0x10));
    r9 = *((rsp + 0x18));
    if (al == 0) {
        goto label_44;
    }
    goto label_4;
label_245:
    if (*((r14 + 0x10)) != 0) {
        goto label_45;
    }
    rdi = r14;
    al = detect_loop ();
    if (al == 0) {
        goto label_46;
    }
    goto label_7;
label_241:
    if (*((rax + 0x10)) != 0) {
        goto label_47;
    }
    rdi = *((rsp + 0x18));
    al = detect_loop ();
    if (al == 0) {
        goto label_48;
    }
    goto label_7;
label_249:
    if (*((r14 + 0x10)) != 0) {
        goto label_49;
    }
    rdi = r14;
    al = detect_loop ();
    if (al == 0) {
        goto label_50;
    }
    goto label_7;
label_266:
    rdi = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (rdi == 0) {
        goto label_394;
    }
    if (rax == 0) {
        goto label_51;
    }
label_269:
    rsi = rbx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
label_92:
    rdi = *((r14 + 0x10));
    if (*((r14 + 0x20)) == 0) {
label_90:
        if (*((r14 + 0x19)) != 0) {
            goto label_395;
        }
        if (*(obj.head) == 0) {
            goto label_396;
        }
        rax = zeros;
        *((rax + 0x28)) = r14;
label_93:
        *(obj.zeros) = r14;
    }
label_395:
    if (rdi != 0) {
        goto label_397;
    }
label_91:
    r14 = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_302:
        if (*((r13 + 0x19)) != 0) {
            goto label_398;
        }
        if (*(obj.head) == 0) {
            goto label_399;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_98:
        *(obj.zeros) = r13;
    }
label_398:
    if (r14 == 0) {
        goto label_52;
    }
    goto label_53;
label_201:
    *(obj.head) = r8;
    goto label_54;
label_325:
    *(obj.head) = r13;
    goto label_55;
label_336:
    if (*((rax + 0x10)) != 0) {
        goto label_56;
    }
label_335:
    rdi = *((rsp + 8));
    al = detect_loop ();
    if (al == 0) {
        goto label_25;
    }
    goto label_7;
label_334:
    *(obj.head) = r13;
    goto label_57;
label_273:
    rdi = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (rdi == 0) {
        goto label_400;
    }
    if (rax == 0) {
        goto label_58;
    }
label_276:
    rsi = rbx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
label_165:
    rdi = *((r13 + 0x10));
    if (*((r13 + 0x20)) == 0) {
label_167:
        if (*((r13 + 0x19)) != 0) {
            goto label_401;
        }
        if (*(obj.head) == 0) {
            goto label_402;
        }
        rax = zeros;
        *((rax + 0x28)) = r13;
label_166:
        *(obj.zeros) = r13;
    }
label_401:
    if (rdi != 0) {
        goto label_403;
    }
label_168:
    r13 = *((r14 + 0x10));
    if (*((r14 + 0x20)) == 0) {
label_389:
        if (*((r14 + 0x19)) != 0) {
            goto label_404;
        }
        if (*(obj.head) == 0) {
            goto label_405;
        }
        rax = zeros;
        *((rax + 0x28)) = r14;
label_184:
        *(obj.zeros) = r14;
    }
label_404:
    if (r13 == 0) {
        goto label_19;
    }
    goto label_59;
label_113:
    if (rax != 0) {
        goto label_41;
    }
    if (*((r13 + 0x19)) != 0) {
        goto label_41;
    }
    if (*(obj.head) == 0) {
        goto label_406;
    }
    rax = zeros;
    *((rax + 0x28)) = r13;
label_65:
    *(obj.zeros) = r13;
    goto label_41;
label_277:
    r8 = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (r8 == 0) {
        goto label_407;
    }
    if (rax == 0) {
        goto label_60;
    }
    goto label_61;
label_149:
    if (rax != 0) {
        goto label_13;
    }
    if (*((r8 + 0x19)) != 0) {
        goto label_13;
    }
    if (*(obj.head) == 0) {
        goto label_408;
    }
    rax = zeros;
    *((rax + 0x28)) = r8;
label_160:
    *(obj.zeros) = r8;
    goto label_13;
label_108:
    if (rax != 0) {
        goto label_19;
    }
    if (*((r13 + 0x19)) != 0) {
        goto label_19;
    }
    if (*(obj.head) == 0) {
        goto label_409;
    }
    rax = zeros;
    *((rax + 0x28)) = r13;
label_157:
    *(obj.zeros) = r13;
    goto label_19;
label_338:
    rdi = *((rsp + 8));
    detect_loop ();
    goto label_7;
label_189:
    rdx = r15;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: input contains an odd number of tokens");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_278:
    rdi = *((r8 + 0x10));
    rax = *((r8 + 0x20));
    if (rdi == 0) {
        goto label_410;
    }
    if (rax == 0) {
        goto label_62;
    }
label_281:
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    rcx = *((rsp + 0x10));
    if (al == 0) {
        goto label_63;
    }
    goto label_4;
label_283:
    *(obj.head) = rcx;
    goto label_64;
label_406:
    *(obj.head) = r13;
    goto label_65;
label_280:
    *(obj.head) = r8;
    goto label_66;
label_350:
    rdi = *((rdx + 0x10));
    rax = *((rdx + 0x20));
    if (rdi == 0) {
        goto label_411;
    }
    if (rax == 0) {
        goto label_67;
    }
label_353:
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    rcx = *((rsp + 0x10));
    if (al == 0) {
        goto label_68;
    }
    goto label_4;
label_411:
    if (rax != 0) {
        goto label_68;
    }
    if (*((rdx + 0x19)) != 0) {
        goto label_68;
    }
    if (*(obj.head) == 0) {
        goto label_412;
    }
    rax = zeros;
    *((rax + 0x28)) = rdx;
    do {
        *(obj.zeros) = rdx;
        goto label_68;
label_320:
        *(obj.head) = r14;
        goto label_69;
label_412:
        *(obj.head) = rdx;
    } while (1);
label_349:
    if (rax != 0) {
        goto label_68;
    }
    if (*((r13 + 0x19)) != 0) {
        goto label_68;
    }
    if (*(obj.head) == 0) {
        goto label_413;
    }
    rax = zeros;
    *((rax + 0x28)) = r13;
    do {
        *(obj.zeros) = r13;
        goto label_68;
label_259:
        *(obj.head) = r13;
        goto label_70;
label_413:
        *(obj.head) = r13;
    } while (1);
label_393:
    if (rax != 0) {
        goto label_44;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_44;
    }
    if (*(obj.head) == 0) {
        goto label_414;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_73:
    *(obj.zeros) = rcx;
    goto label_44;
label_303:
    rdi = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (rdi == 0) {
        goto label_415;
    }
    if (rax == 0) {
        goto label_71;
    }
label_306:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_52;
    }
    goto label_4;
label_246:
    if (*((r8 + 0x10)) != 0) {
        goto label_72;
    }
    rdi = r8;
    al = detect_loop ();
    if (al == 0) {
        goto label_45;
    }
    goto label_7;
label_414:
    *(obj.head) = rcx;
    goto label_73;
label_318:
    rdi = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (rdi == 0) {
        goto label_416;
    }
    if (rax == 0) {
        goto label_74;
    }
label_321:
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    rcx = *((rsp + 0x10));
    if (al == 0) {
        goto label_75;
    }
    goto label_4;
label_329:
    *(obj.head) = r12;
    goto label_76;
label_326:
    if (rax != 0) {
        goto label_77;
    }
    if (*((r8 + 0x19)) != 0) {
        goto label_77;
    }
    if (*(obj.head) == 0) {
        goto label_417;
    }
    rax = zeros;
    *((rax + 0x28)) = r8;
    do {
        *(obj.zeros) = r8;
        goto label_77;
label_223:
        *(obj.head) = r8;
        goto label_78;
label_417:
        *(obj.head) = r8;
    } while (1);
label_310:
    rdi = *((r12 + 0x10));
    rax = *((r12 + 0x20));
    if (rdi == 0) {
        goto label_418;
    }
    if (rax == 0) {
        goto label_79;
    }
label_313:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_80;
    }
    goto label_4;
label_315:
    *(obj.head) = r14;
    goto label_81;
label_327:
    rdi = *((r12 + 0x10));
    rax = *((r12 + 0x20));
    if (rdi == 0) {
        goto label_419;
    }
    if (rax == 0) {
        goto label_82;
    }
label_330:
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    rcx = *((rsp + 0x10));
    if (al == 0) {
        goto label_77;
    }
    goto label_4;
label_419:
    if (rax != 0) {
        goto label_77;
    }
    if (*((r12 + 0x19)) != 0) {
        goto label_77;
    }
    if (*(obj.head) == 0) {
        goto label_420;
    }
    rax = zeros;
    *((rax + 0x28)) = r12;
    do {
        *(obj.zeros) = r12;
        goto label_77;
label_312:
        *(obj.head) = r12;
        goto label_83;
label_420:
        *(obj.head) = r12;
    } while (1);
label_352:
    *(obj.head) = rdx;
    goto label_84;
label_416:
    if (rax != 0) {
        goto label_75;
    }
    if (*((r14 + 0x19)) != 0) {
        goto label_75;
    }
    if (*(obj.head) == 0) {
        goto label_421;
    }
    rax = zeros;
    *((rax + 0x28)) = r14;
label_86:
    *(obj.zeros) = r14;
    goto label_75;
label_317:
    if (rax != 0) {
        goto label_75;
    }
    if (*((r8 + 0x19)) != 0) {
        goto label_75;
    }
    if (*(obj.head) == 0) {
        goto label_422;
    }
    rax = zeros;
    *((rax + 0x28)) = r8;
    do {
        *(obj.zeros) = r8;
        goto label_75;
label_235:
        *(obj.head) = r8;
        goto label_85;
label_421:
        *(obj.head) = r14;
        goto label_86;
label_422:
        *(obj.head) = r8;
    } while (1);
label_198:
    *(obj.head) = r9;
    goto label_87;
label_250:
    if (*((r8 + 0x10)) != 0) {
        goto label_88;
    }
    rdi = r8;
    al = detect_loop ();
    if (al == 0) {
        goto label_49;
    }
    goto label_7;
label_268:
    *(obj.head) = rcx;
    goto label_89;
label_265:
    rdi = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (rdi == 0) {
        goto label_423;
    }
    if (rax == 0) {
        goto label_90;
    }
label_397:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_91;
    }
    goto label_4;
label_423:
    if (rax != 0) {
        goto label_91;
    }
    if (*((r14 + 0x19)) != 0) {
        goto label_91;
    }
    if (*(obj.head) == 0) {
        goto label_424;
    }
    rax = zeros;
    *((rax + 0x28)) = r14;
label_94:
    *(obj.zeros) = r14;
    goto label_91;
label_394:
    if (rax != 0) {
        goto label_92;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_92;
    }
    if (*(obj.head) == 0) {
        goto label_425;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
    do {
        *(obj.zeros) = rcx;
        goto label_92;
label_396:
        *(obj.head) = r14;
        goto label_93;
label_424:
        *(obj.head) = r14;
        goto label_94;
label_425:
        *(obj.head) = rcx;
    } while (1);
label_301:
    if (rax != 0) {
        goto label_52;
    }
    if (*((r13 + 0x19)) != 0) {
        goto label_52;
    }
    if (*(obj.head) == 0) {
        goto label_426;
    }
    rax = zeros;
    *((rax + 0x28)) = r13;
    do {
        *(obj.zeros) = r13;
        goto label_52;
label_305:
        *(obj.head) = r14;
        goto label_95;
label_426:
        *(obj.head) = r13;
    } while (1);
label_218:
    rdi = *((r12 + 0x10));
    rax = *((r12 + 0x20));
    if (rdi == 0) {
        goto label_427;
    }
    if (rax == 0) {
        goto label_96;
    }
label_221:
    rsi = rbx;
    *((rsp + 0x18)) = r8;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    rcx = *((rsp + 0x10));
    r8 = *((rsp + 0x18));
    if (al == 0) {
        goto label_97;
    }
    goto label_4;
label_415:
    if (rax != 0) {
        goto label_52;
    }
    if (*((r14 + 0x19)) != 0) {
        goto label_52;
    }
    if (*(obj.head) == 0) {
        goto label_428;
    }
    rax = zeros;
    *((rax + 0x28)) = r14;
label_102:
    *(obj.zeros) = r14;
    goto label_52;
label_399:
    *(obj.head) = r13;
    goto label_98;
label_230:
    rdi = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (rdi == 0) {
        goto label_429;
    }
    if (rax == 0) {
        goto label_99;
    }
label_233:
    rsi = rbx;
    *((rsp + 0x18)) = r8;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    rcx = *((rsp + 0x10));
    r8 = *((rsp + 0x18));
    if (al == 0) {
        goto label_100;
    }
    goto label_4;
label_429:
    if (rax != 0) {
        goto label_100;
    }
    if (*((r14 + 0x19)) != 0) {
        goto label_100;
    }
    if (*(obj.head) == 0) {
        goto label_430;
    }
    rax = zeros;
    *((rax + 0x28)) = r14;
label_103:
    *(obj.zeros) = r14;
    goto label_100;
label_242:
    if (*((r8 + 0x10)) != 0) {
        goto label_101;
    }
    rdi = r8;
    al = detect_loop ();
    if (al == 0) {
        goto label_47;
    }
    goto label_7;
label_428:
    *(obj.head) = r14;
    goto label_102;
label_430:
    *(obj.head) = r14;
    goto label_103;
label_385:
    *(obj.head) = r13;
    goto label_104;
label_298:
    *(obj.head) = r12;
    goto label_105;
label_380:
    *(obj.head) = rcx;
    goto label_106;
label_337:
    if (*((r8 + 0x10)) != 0) {
        goto label_107;
    }
    rdi = r8;
    al = detect_loop ();
    if (al == 0) {
        goto label_56;
    }
    goto label_7;
label_390:
    rdi = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (rdi == 0) {
        goto label_108;
    }
    if (rax != 0) {
        goto label_109;
    }
    goto label_110;
label_386:
    rdi = *((rsp + 0x10));
    al = detect_loop ();
    if (al == 0) {
        goto label_42;
    }
    goto label_7;
label_387:
    if (*((r8 + 0x10)) != 0) {
        goto label_111;
    }
    rdi = r8;
    al = detect_loop ();
    if (al == 0) {
        goto label_42;
    }
    goto label_7;
label_388:
    *(obj.head) = r14;
    goto label_112;
label_383:
    rdi = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (rdi == 0) {
        goto label_113;
    }
    if (rax != 0) {
        goto label_114;
    }
    goto label_115;
label_382:
    if (rax != 0) {
        goto label_41;
    }
    if (*((r12 + 0x19)) != 0) {
        goto label_41;
    }
    if (*(obj.head) == 0) {
        goto label_431;
    }
    rax = zeros;
    *((rax + 0x28)) = r12;
label_119:
    *(obj.zeros) = r12;
    goto label_41;
label_381:
    *(obj.head) = r13;
    goto label_116;
label_341:
    if (*((r13 + 0x10)) != 0) {
        goto label_117;
    }
    goto label_118;
label_431:
    *(obj.head) = r12;
    goto label_119;
label_308:
    *(obj.head) = r12;
    goto label_120;
label_376:
    rdx = head;
    if (rax != 0) {
        goto label_4;
    }
    if (*((r12 + 0x19)) != 0) {
        goto label_4;
    }
    if (rdx == 0) {
        goto label_432;
    }
    rax = zeros;
    *((rax + 0x28)) = r12;
label_121:
    *(obj.zeros) = r12;
    goto label_4;
label_418:
    if (rax != 0) {
        goto label_80;
    }
    if (*((r12 + 0x19)) != 0) {
        goto label_80;
    }
    if (*(obj.head) == 0) {
        goto label_433;
    }
    rax = zeros;
    *((rax + 0x28)) = r12;
    do {
        *(obj.zeros) = r12;
        goto label_80;
label_432:
        *(obj.head) = r12;
        goto label_121;
label_433:
        *(obj.head) = r12;
    } while (1);
label_427:
    if (rax != 0) {
        goto label_97;
    }
    if (*((r12 + 0x19)) != 0) {
        goto label_97;
    }
    if (*(obj.head) == 0) {
        goto label_434;
    }
    rax = zeros;
    *((rax + 0x28)) = r12;
label_125:
    *(obj.zeros) = r12;
    goto label_97;
label_254:
    rdi = *((rdx + 0x10));
    rax = *((rdx + 0x20));
    if (rdi == 0) {
        goto label_435;
    }
    if (rax == 0) {
        goto label_122;
    }
label_257:
    rsi = rbx;
    *((rsp + 0x10)) = rcx;
    al = recurse_tree ();
    rcx = *((rsp + 0x10));
    if (al == 0) {
        goto label_123;
    }
    goto label_4;
label_435:
    if (rax != 0) {
        goto label_123;
    }
    if (*((rdx + 0x19)) != 0) {
        goto label_123;
    }
    if (*(obj.head) == 0) {
        goto label_436;
    }
    rax = zeros;
    *((rax + 0x28)) = rdx;
    do {
        *(obj.zeros) = rdx;
        goto label_123;
label_220:
        *(obj.head) = r12;
        goto label_124;
label_434:
        *(obj.head) = r12;
        goto label_125;
label_436:
        *(obj.head) = rdx;
    } while (1);
label_232:
    *(obj.head) = r14;
    goto label_126;
label_256:
    *(obj.head) = rdx;
    goto label_127;
label_346:
    rdi = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (rdi == 0) {
        goto label_128;
    }
    if (rax == 0) {
        goto label_129;
    }
    goto label_130;
label_309:
    rdi = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (rdi == 0) {
        goto label_131;
    }
    if (rax == 0) {
        goto label_132;
    }
    goto label_133;
label_367:
    if (rax != 0) {
        goto label_134;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_134;
    }
    if (*(obj.head) == 0) {
        goto label_437;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_143:
    *(obj.zeros) = rcx;
    goto label_134;
label_360:
    rdi = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (rdi == 0) {
        goto label_438;
    }
    if (rax == 0) {
        goto label_135;
    }
label_363:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_136;
    }
    goto label_4;
label_365:
    *(obj.head) = r12;
    goto label_137;
label_369:
    rdi = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (rdi == 0) {
        goto label_439;
    }
    if (rax == 0) {
        goto label_138;
    }
label_372:
    rsi = rbx;
    al = recurse_tree ();
    if (al != 0) {
        goto label_4;
    }
label_139:
    rdi = *((r14 + 0x10));
    if (*((r14 + 0x20)) == 0) {
label_146:
        if (*((r14 + 0x19)) != 0) {
            goto label_440;
        }
        if (*(obj.head) == 0) {
            goto label_441;
        }
        rax = zeros;
        *((rax + 0x28)) = r14;
label_148:
        *(obj.zeros) = r14;
    }
label_440:
    if (rdi == 0) {
        goto label_134;
    }
label_147:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_134;
    }
    goto label_4;
label_439:
    if (rax != 0) {
        goto label_139;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_139;
    }
    if (*(obj.head) == 0) {
        goto label_442;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
    do {
        *(obj.zeros) = rcx;
        goto label_139;
label_371:
        *(obj.head) = rcx;
        goto label_140;
label_442:
        *(obj.head) = rcx;
    } while (1);
label_373:
    *(obj.head) = rcx;
    goto label_141;
label_375:
    *(obj.head) = r14;
    goto label_142;
label_437:
    *(obj.head) = rcx;
    goto label_143;
label_362:
    *(obj.head) = rcx;
    goto label_144;
label_316:
    *(obj.head) = rcx;
    goto label_145;
label_368:
    rdi = *((r14 + 0x10));
    rax = *((r14 + 0x20));
    if (rdi == 0) {
        goto label_443;
    }
    if (rax == 0) {
        goto label_146;
    }
    goto label_147;
label_441:
    *(obj.head) = r14;
    goto label_148;
label_443:
    if (rax != 0) {
        goto label_134;
    }
    if (*((r14 + 0x19)) != 0) {
        goto label_134;
    }
    if (*(obj.head) == 0) {
        goto label_444;
    }
    rax = zeros;
    *((rax + 0x28)) = r14;
label_176:
    *(obj.zeros) = r14;
    goto label_134;
label_377:
    if (rax != 0) {
        goto label_38;
    }
    if (*((r9 + 0x19)) != 0) {
        goto label_38;
    }
    if (*(obj.head) == 0) {
        goto label_445;
    }
    rax = zeros;
    *((rax + 0x28)) = r9;
label_183:
    *(obj.zeros) = r9;
    goto label_38;
label_284:
    rdi = *((r8 + 0x10));
    rax = *((r8 + 0x20));
    if (rdi == 0) {
        goto label_149;
    }
    if (rax != 0) {
        goto label_150;
    }
    goto label_151;
label_355:
    *(obj.head) = rcx;
    goto label_152;
label_378:
    rdi = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (rdi == 0) {
        goto label_446;
    }
    if (rax == 0) {
        goto label_153;
    }
    goto label_154;
label_344:
    if (rax != 0) {
        goto label_26;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_26;
    }
    if (*(obj.head) == 0) {
        goto label_447;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_155:
    *(obj.zeros) = rcx;
    goto label_26;
label_446:
    if (rax != 0) {
        goto label_38;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_38;
    }
    if (*(obj.head) == 0) {
        goto label_448;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
    do {
        *(obj.zeros) = rcx;
        goto label_38;
label_447:
        *(obj.head) = rcx;
        goto label_155;
label_448:
        *(obj.head) = rcx;
    } while (1);
label_342:
    rdi = *((rsp + 8));
    al = detect_loop ();
    if (al == 0) {
        goto label_9;
    }
    goto label_7;
label_340:
    if (*((r14 + 0x10)) != 0) {
        goto label_156;
    }
    rdi = r14;
    al = detect_loop ();
    if (al == 0) {
        goto label_30;
    }
    goto label_7;
label_409:
    *(obj.head) = r13;
    goto label_157;
label_358:
    if (rax != 0) {
        goto label_31;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_31;
    }
    if (*(obj.head) == 0) {
        goto label_449;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_159:
    *(obj.zeros) = rcx;
    goto label_31;
label_357:
    if (*((r13 + 0x10)) != 0) {
        goto label_158;
    }
label_356:
    rdi = r13;
    al = detect_loop ();
    if (al == 0) {
        goto label_29;
    }
    goto label_7;
label_449:
    *(obj.head) = rcx;
    goto label_159;
label_408:
    *(obj.head) = r8;
    goto label_160;
label_359:
    rdi = *((r12 + 0x10));
    rax = *((r12 + 0x20));
    if (rdi == 0) {
        goto label_450;
    }
    if (rax == 0) {
        goto label_161;
    }
    goto label_162;
label_438:
    if (rax != 0) {
        goto label_136;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_136;
    }
    if (*(obj.head) == 0) {
        goto label_451;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_163:
    *(obj.zeros) = rcx;
    goto label_136;
label_450:
    if (rax != 0) {
        goto label_31;
    }
    if (*((r12 + 0x19)) != 0) {
        goto label_31;
    }
    if (*(obj.head) == 0) {
        goto label_452;
    }
    rax = zeros;
    *((rax + 0x28)) = r12;
    do {
        *(obj.zeros) = r12;
        goto label_31;
label_451:
        *(obj.head) = rcx;
        goto label_163;
label_452:
        *(obj.head) = r12;
    } while (1);
label_366:
    *(obj.head) = rcx;
    goto label_164;
label_410:
    if (rax != 0) {
        goto label_63;
    }
    if (*((r8 + 0x19)) != 0) {
        goto label_63;
    }
    if (*(obj.head) == 0) {
        goto label_453;
    }
    rax = zeros;
    *((rax + 0x28)) = r8;
label_177:
    *(obj.zeros) = r8;
    goto label_63;
label_400:
    if (rax != 0) {
        goto label_165;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_165;
    }
    if (*(obj.head) == 0) {
        goto label_454;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
    do {
        *(obj.zeros) = rcx;
        goto label_165;
label_402:
        *(obj.head) = r13;
        goto label_166;
label_454:
        *(obj.head) = rcx;
    } while (1);
label_272:
    rdi = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (rdi == 0) {
        goto label_455;
    }
    if (rax == 0) {
        goto label_167;
    }
label_403:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_168;
    }
    goto label_4;
label_275:
    *(obj.head) = rcx;
    goto label_169;
label_300:
    *(obj.head) = r12;
    goto label_170;
label_290:
    rdi = *((rcx + 0x10));
    rax = *((rcx + 0x20));
    if (rdi == 0) {
        goto label_456;
    }
    if (rax == 0) {
        goto label_171;
    }
label_293:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_172;
    }
    goto label_4;
label_289:
    rdi = *((r13 + 0x10));
    rax = *((r13 + 0x20));
    if (rdi == 0) {
        goto label_457;
    }
    if (rax == 0) {
        goto label_173;
    }
label_296:
    rsi = rbx;
    al = recurse_tree ();
    if (al == 0) {
        goto label_174;
    }
    goto label_4;
label_211:
    if (*((rsp + 0x24)) != 0) {
        goto label_458;
    }
    edx = 5;
    rax = dcgettext (0, "standard input");
    r12 = rax;
    do {
        rax = errno_location ();
        rcx = r12;
        eax = 0;
        error (1, *(rax), 0x0000a3df);
label_186:
        rdx = r15;
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        r12 = rax;
        rax = errno_location ();
        rcx = r12;
        eax = 0;
        error (1, *(rax), 0x0000a3df);
label_458:
        rdx = r15;
        esi = 3;
        edi = 0;
        rax = quotearg_n_style_colon ();
        r12 = rax;
    } while (1);
label_348:
    *(obj.head) = r13;
    goto label_175;
label_444:
    *(obj.head) = r14;
    goto label_176;
label_407:
    if (rax != 0) {
        goto label_13;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_13;
    }
    if (*(obj.head) == 0) {
        goto label_459;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
    do {
        *(obj.zeros) = rcx;
        goto label_13;
label_453:
        *(obj.head) = r8;
        goto label_177;
label_459:
        *(obj.head) = rcx;
    } while (1);
label_455:
    if (rax != 0) {
        goto label_168;
    }
    if (*((r13 + 0x19)) != 0) {
        goto label_168;
    }
    if (*(obj.head) == 0) {
        goto label_460;
    }
    rax = zeros;
    *((rax + 0x28)) = r13;
label_185:
    *(obj.zeros) = r13;
    goto label_168;
label_392:
    *(obj.head) = r13;
    goto label_178;
label_456:
    if (rax != 0) {
        goto label_172;
    }
    if (*((rcx + 0x19)) != 0) {
        goto label_172;
    }
    if (*(obj.head) == 0) {
        goto label_461;
    }
    rax = zeros;
    *((rax + 0x28)) = rcx;
label_179:
    *(obj.zeros) = rcx;
    goto label_172;
label_457:
    if (rax != 0) {
        goto label_174;
    }
    if (*((r13 + 0x19)) != 0) {
        goto label_174;
    }
    if (*(obj.head) == 0) {
        goto label_462;
    }
    rax = zeros;
    *((rax + 0x28)) = r13;
    do {
        *(obj.zeros) = r13;
        goto label_174;
label_461:
        *(obj.head) = rcx;
        goto label_179;
label_462:
        *(obj.head) = r13;
    } while (1);
label_292:
    *(obj.head) = rcx;
    goto label_180;
label_295:
    *(obj.head) = r13;
    goto label_181;
label_286:
    *(obj.head) = r8;
    goto label_182;
label_445:
    *(obj.head) = r9;
    goto label_183;
label_405:
    *(obj.head) = r14;
    goto label_184;
label_460:
    *(obj.head) = r13;
    goto label_185;
}

/* /tmp/tmpp4rbmgt4 @ 0x27e0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x2810 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x2850 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002350 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmpp4rbmgt4 @ 0x2350 */
 
void fcn_00002350 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2890 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9ca0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8150 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8480 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2450 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmpp4rbmgt4 @ 0x92a0 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x7f90 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x2390 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmpp4rbmgt4 @ 0x94a0 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2570 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmpp4rbmgt4 @ 0x99e0 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000a3df);
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2420 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmpp4rbmgt4 @ 0x25e0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmpp4rbmgt4 @ 0x25b0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmpp4rbmgt4 @ 0x23e0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmpp4rbmgt4 @ 0x7eb0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x9ad0 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x81a0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x2690)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x7f10 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x6650 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2490 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmpp4rbmgt4 @ 0x23a0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2630 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmpp4rbmgt4 @ 0x86e0 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x26a9)() ();
    }
    if (rdx == 0) {
        void (*0x26a9)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x6080 */
 
uint64_t dbg_freopen_safer (int64_t arg1, int64_t arg2, FILE * stream) {
    uint32_t var_8h;
    uint32_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = stream;
    /* FILE * freopen_safer(char const * name,char const * mode,FILE * f); */
    r13 = rsi;
    r12 = rdx;
    eax = fileno (rdx);
    if (eax == 1) {
        goto label_11;
    }
    if (eax == 2) {
        goto label_12;
    }
    if (eax == 0) {
        goto label_13;
    }
    esi = 2;
    edi = 2;
    eax = dup2 ();
    r14b = (eax != 2) ? 1 : 0;
label_3:
    esi = 1;
    edi = 1;
    eax = dup2 ();
    *((rsp + 0xc)) = eax;
    r15b = (eax != 1) ? 1 : 0;
    esi = 0;
    edi = 0;
    eax = dup2 ();
    *((rsp + 8)) = eax;
    rax = errno_location ();
    edx = *((rsp + 8));
    rbx = rax;
    if (edx != 0) {
        goto label_14;
    }
    *((rsp + 8)) = 0;
    if (*((rsp + 0xc)) == 1) {
        goto label_15;
    }
label_5:
    eax = 0;
    eax = open ("/dev/null", 0, rdx);
    if (eax != 1) {
        goto label_16;
    }
    r15d = 1;
label_6:
    if (r14b == 0) {
        goto label_4;
    }
    eax = 0;
    eax = open ("/dev/null", 0, rdx);
    if (eax == 2) {
        goto label_4;
    }
    if (eax >= 0) {
        goto label_17;
    }
    ebp = *(rbx);
label_7:
    r12d = 0;
    do {
        close (2);
        if (r15b == 0) {
            goto label_18;
        }
label_0:
        close (1);
        if (*((rsp + 8)) == 0) {
            goto label_19;
        }
label_1:
        eax = close (0);
        if (r12 != 0) {
            goto label_20;
        }
label_2:
        *(rbx) = ebp;
        goto label_20;
label_14:
        eax = 0;
        eax = open ("/dev/null", 0, rdx);
        if (eax == 0) {
            goto label_21;
        }
label_9:
        if (eax >= 0) {
            goto label_22;
        }
        *((rsp + 8)) = 1;
        ebp = *(rbx);
        r12d = 0;
        goto label_8;
label_11:
        esi = 0;
        edi = 0;
        eax = dup2 ();
        r14d = eax;
        rax = errno_location ();
        rbx = rax;
        if (r14d != 0) {
            goto label_23;
        }
label_13:
        *((rsp + 8)) = 0;
        r14d = 0;
        r15d = 0;
label_4:
        rax = freopen (rbp, r13, r12);
        r12 = rax;
        rax = errno_location ();
        ebp = *(rax);
        rbx = rax;
label_8:
    } while (r14b != 0);
    if (r15b != 0) {
        goto label_0;
    }
label_18:
    if (*((rsp + 8)) != 0) {
        goto label_1;
    }
label_19:
    if (r12 == 0) {
        goto label_2;
    }
label_20:
    rax = r12;
    return rax;
label_12:
    r14d = 0;
    goto label_3;
label_16:
    if (eax >= 0) {
        goto label_24;
    }
    ebp = *(rbx);
label_10:
    r12d = 0;
    if (r14b == 0) {
        goto label_0;
    }
    eax = close (2);
    goto label_0;
label_23:
    eax = 0;
    eax = open ("/dev/null", 0, rdx);
    if (eax != 0) {
        goto label_25;
    }
    *((rsp + 8)) = 1;
    r14d = 0;
    r15d = 0;
    goto label_4;
label_21:
    *((rsp + 8)) = 1;
    if (*((rsp + 0xc)) != 1) {
        goto label_5;
    }
label_15:
    r15d = 0;
    goto label_6;
label_17:
    eax = close (eax);
    *(rbx) = 9;
    goto label_7;
label_22:
    r12d = 0;
    eax = close (eax);
    *(rbx) = 9;
    *((rsp + 8)) = 1;
    goto label_8;
label_25:
    r14d = 0;
    r15d = 0;
    goto label_9;
label_24:
    close (eax);
    *(rbx) = 9;
    goto label_10;
}

/* /tmp/tmpp4rbmgt4 @ 0x8780 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26ae)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x26ae)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x82c0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x269a)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x63d0 */
 
int64_t dbg_parse_long_options (int64_t arg_100h, uint32_t arg1, int64_t arg10, int64_t arg11, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6, int64_t arg7, int64_t arg8, int64_t arg9) {
    va_list authors;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    rdi = arg1;
    xmm3 = arg10;
    xmm4 = arg11;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* void parse_long_options(int argc,char ** argv,char const * command_name,char const * package,char const * version,void (*)() usage_func,va_args ...); */
    r13 = rcx;
    r12 = r8;
    rbx = r9;
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    r14d = opterr;
    *(obj.opterr) = 0;
    while (eax == 0xffffffff) {
label_0:
        *(obj.opterr) = r14d;
        *(obj.optind) = 0;
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        return rax;
        r8d = 0;
        rcx = obj_long_options;
        rdx = 0x0000a3ec;
        eax = getopt_long ();
    }
    if (eax == 0x68) {
        goto label_2;
    }
    if (eax != 0x76) {
        goto label_0;
    }
    rdi = stdout;
    r8 = rsp;
    rcx = r12;
    rdx = r13;
    rax = rsp + 0x100;
    rsi = rbp;
    *(rsp) = 0x30;
    *((rsp + 8)) = rax;
    rax = rsp + 0x20;
    *((rsp + 4)) = 0x30;
    *((rsp + 0x10)) = rax;
    version_etc_va ();
    exit (0);
label_2:
    edi = 0;
    void (*rbx)() ();
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8640 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x26a4)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9cb4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmpp4rbmgt4 @ 0x5f60 */
 
void fdadvise (void) {
    return posix_fadvise ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9520 */
 
uint64_t xreallocarray (int64_t arg1, signed int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9620 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x94c0 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x6030 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2580)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8ba0 */
 
int64_t dbg_readtokens (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, uint32_t arg6) {
    idx_t sz;
    token_buffer tb;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    uint32_t var_30h;
    int64_t var_38h;
    signed int64_t var_48h;
    int64_t var_50h;
    void * ptr;
    int64_t var_68h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* size_t readtokens(FILE * stream,size_t projected_n_tokens,char const * delim,size_t n_delim,char *** tokens_out,size_t ** token_lengths); */
    *((rsp + 8)) = rdi;
    *((rsp + 0x10)) = rdx;
    *((rsp + 0x18)) = rcx;
    *((rsp + 0x38)) = r8;
    *((rsp + 0x30)) = r9;
    rax = *(fs:0x28);
    *((rsp + 0x68)) = rax;
    eax = 0;
    if (rsi == 0) {
        goto label_2;
    }
    rax = rdi;
label_1:
    *((rsp + 0x48)) = rax;
    r12d = 0;
    rax = xnmalloc (rsi + 1, 8);
    rax = xnmalloc (*((rsp + 0x48)), 8);
    *((rsp + 0x50)) = 0;
    *((rsp + 0x58)) = 0;
    r13 = rax;
    rax = rsp + 0x50;
    *((rsp + 0x20)) = rax;
    rax = rsp + 0x48;
    *((rsp + 0x28)) = rax;
    while (*((rsp + 0x48)) > r12) {
label_0:
        rax = r12*8;
        r14 = r13 + rax;
        rbx = rbp + rax;
        if (r15 == -1) {
            goto label_3;
        }
        rdx = r15 + 1;
        r12++;
        rdi = rdx;
        *(rsp) = rdx;
        rax = xnmalloc (rdi, 1);
        *(r14) = r15;
        rax = memcpy (rax, *((rsp + 0x58)), *(rsp));
        *(rbx) = rax;
        rax = readtoken (*((rsp + 8)), *((rsp + 0x10)), *((rsp + 0x18)), *((rsp + 0x20)), r8, r9);
        r15 = rax;
    }
    r8d = 8;
    rax = xpalloc (rbp, *((rsp + 0x28)), 1, 0xffffffffffffffff);
    rsi = *((rsp + 0x48));
    rdi = r13;
    edx = 8;
    rax = xreallocarray ();
    r13 = rax;
    goto label_0;
label_3:
    rax = r14;
    *(rbx) = 0;
    *(rax) = 0;
    free (*((rsp + 0x58)));
    rax = *((rsp + 0x38));
    *(rax) = rbp;
    if (*((rsp + 0x30)) == 0) {
        goto label_4;
    }
    rax = *((rsp + 0x30));
    *(rax) = r13;
    do {
        rax = *((rsp + 0x68));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_5;
        }
        rax = r12;
        return rax;
label_2:
        eax = 0x40;
        edi = 0x40;
        goto label_1;
label_4:
        free (r13);
    } while (1);
label_5:
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9460 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8180 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpp4rbmgt4 @ 0x99a0 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2550)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2440 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmpp4rbmgt4 @ 0x9480 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x91e0 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x8d70)() ();
}

/* /tmp/tmpp4rbmgt4 @ 0x5eb0 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000a3df);
    } while (1);
}

/* /tmp/tmpp4rbmgt4 @ 0x9be0 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmpp4rbmgt4 @ 0x9420 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9b60 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8830 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26b3)() ();
    }
    if (rax == 0) {
        void (*0x26b3)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x85b0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9870 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2520 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmpp4rbmgt4 @ 0x7e50 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x8970 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmpp4rbmgt4 @ 0x7df0 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x98e0 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2550)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8090 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x5ea0 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmpp4rbmgt4 @ 0x94f0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x96b0 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x5fa0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2400)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpp4rbmgt4 @ 0x95d0 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8980 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpp4rbmgt4 @ 0x88d0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x26b8)() ();
    }
    if (rax == 0) {
        void (*0x26b8)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9920 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2550)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8080 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x7f90)() ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8160 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpp4rbmgt4 @ 0x26c0 */
 
uint64_t dbg_main (int32_t argc, char ** argv) {
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r12 = 0x0000a084;
    rdi = *(rsi);
    rbx = rsi;
    set_program_name (*(rdi), rsi, rdx);
    setlocale (6, 0x0000a0cf);
    bindtextdomain (r12, "/usr/local/share/locale");
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    rax = "Mark Kettenis";
    rax = dbg_usage;
    eax = 0;
    parse_gnu_standard_options_only (ebp, rbx, "tsort", "GNU coreutils", *(obj.Version), 1);
    rax = *(obj.optind);
    edx = ebp;
    edx -= eax;
    if (edx <= 1) {
        rdi = 0x0000a103;
        if (eax != ebp) {
        }
        tsort (*((rbx + rax*8)), rsi);
    }
    rax = quote (*((rbx + rax*8 + 8)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "extra operand %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    return usage (1);
}

/* /tmp/tmpp4rbmgt4 @ 0x25c0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2410 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmpp4rbmgt4 @ 0x23f0 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmpp4rbmgt4 @ 0x6510 */
 
int64_t dbg_parse_gnu_standard_options_only (int64_t arg_100h, int64_t arg_108h, int64_t arg10, int64_t arg11, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    void (*)() usage_func;
    va_list authors;
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_50h;
    int64_t var_60h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_a0h;
    int64_t var_b0h;
    int64_t var_c0h;
    xmm3 = arg10;
    xmm4 = arg11;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* void parse_gnu_standard_options_only(int argc,char ** argv,char const * command_name,char const * package,char const * version,_Bool scan_all,void (*)() usage_func,va_args ...); */
    r13 = r8;
    r12 = rcx;
    r14 = *((rsp + 0x100));
    if (al != 0) {
        *((rsp + 0x50)) = xmm0;
        *((rsp + 0x60)) = xmm1;
        *((rsp + 0x70)) = xmm2;
        *((rsp + 0x80)) = xmm3;
        *((rsp + 0x90)) = xmm4;
        *((rsp + 0xa0)) = xmm5;
        *((rsp + 0xb0)) = xmm6;
        *((rsp + 0xc0)) = xmm7;
    }
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rdx = 0x0000a0cf;
    rax = 0x0000a3ec;
    if (r9b == 0) {
        rdx = rax;
    }
    rcx = obj_long_options;
    r8d = 0;
    ebx = opterr;
    *(obj.opterr) = 1;
    eax = getopt_long ();
    if (eax == 0xffffffff) {
        goto label_0;
    }
    if (eax == 0x68) {
        goto label_1;
    }
    if (eax == 0x76) {
        goto label_2;
    }
    edi = *(obj.exit_failure);
    void (*r14)() ();
    do {
label_0:
        *(obj.opterr) = ebx;
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        return rax;
label_2:
        rdi = stdout;
        r8 = rsp;
        rcx = r13;
        rdx = r12;
        rax = rsp + 0x108;
        rsi = rbp;
        *(rsp) = 0x30;
        *((rsp + 8)) = rax;
        rax = rsp + 0x20;
        *((rsp + 4)) = 0x30;
        *((rsp + 0x10)) = rax;
        version_etc_va ();
        exit (0);
label_1:
        edi = 0;
        void (*r14)() ();
    } while (1);
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x89c0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2aa0 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    char * var_8h;
    int64_t var_10h;
    char * var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    char * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rbx = rsp;
    rax = dcgettext (0, "Usage: %s [OPTION] [FILE]\nWrite totally ordered list consistent with the partial ordering in FILE.\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nWith no FILE, or when FILE is -, read standard input.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, 0x0000a0ce);
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    r12 = "tsort";
    rdi = rax;
    fputs_unlocked ();
    rax = 0x0000a00a;
    rcx = "sha256sum";
    *((rsp + 0x60)) = 0;
    *(rsp) = rax;
    rax = "test invocation";
    *((rsp + 8)) = rax;
    rax = 0x0000a084;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x30)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rcx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = 0;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000a08e, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000a0cf;
    r12 = 0x0000a026;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000a08e, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "tsort";
        printf_chk ();
        r12 = 0x0000a026;
    }
label_5:
    r13 = "tsort";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmpp4rbmgt4 @ 0x2640 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2620 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmpp4rbmgt4 @ 0x25d0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2510 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2530 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmpp4rbmgt4 @ 0x7e30 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x8520 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8350 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x269f)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x8230 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x2695)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x7ed0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x268a)() ();
    }
    if (rdx == 0) {
        void (*0x268a)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x83e0 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x0000e230]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x0000e240]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9840 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x89e0 */
 
void init_tokenbuffer (int64_t arg1) {
    rdi = arg1;
    *(rdi) = 0;
    *((rdi + 8)) = 0;
}

/* /tmp/tmpp4rbmgt4 @ 0x98c0 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x5f70 */
 
uint32_t dbg_fadvise (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void fadvise(FILE * fp,fadvice_t advice); */
    if (rdi != 0) {
        r12d = esi;
        eax = fileno (rdi);
        ecx = r12d;
        edx = 0;
        esi = 0;
        edi = eax;
        void (*0x24f0)() ();
    }
    return eax;
}

/* /tmp/tmpp4rbmgt4 @ 0x2560 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmpp4rbmgt4 @ 0x8130 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9560 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x89a0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmpp4rbmgt4 @ 0x6350 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x2600)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmpp4rbmgt4 @ 0x7e70 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x9740 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmpp4rbmgt4 @ 0x8a00 */
 
int64_t dbg_readtoken (int64_t arg_8h, uint32_t arg_10h, int64_t arg1, uint32_t arg2, int64_t arg3, int64_t arg4) {
    idx_t n;
    word[4] isdelim;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* size_t readtoken(FILE * stream,char const * delim,size_t n_delim,token_buffer * tokenbuffer); */
    xmm0 = 0;
    r9 = rsi + rdx;
    r8d = 1;
    r13 = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rdi = rsp + 0x10;
    *((rsp + 0x10)) = xmm0;
    *((rsp + 0x20)) = xmm0;
    if (rdx == 0) {
        goto label_4;
    }
    do {
        ecx = *(rsi);
        rdx = r8;
        rsi++;
        rax = rcx;
        rdx <<= cl;
        rax >>= 3;
        eax &= 0x18;
        *((rdi + rax)) |= rdx;
    } while (r9 != rsi);
    rax = *((rbp + 8));
    if (rax >= *((rbp + 0x10))) {
        goto label_5;
    }
    do {
        rdx = rax + 1;
        *((rbp + 8)) = rdx;
        ebx = *(rax);
label_0:
        rax = (int64_t) ebx;
        rax >>= 6;
        rax = *((rsp + rax*8 + 0x10));
        if (((rax >> rbx) & 1) >= 0) {
            goto label_6;
        }
label_4:
        rax = *((rbp + 8));
    } while (rax < *((rbp + 0x10)));
label_5:
    rdi = rbp;
    eax = uflow ();
    ebx = eax;
    if (eax >= 0) {
        goto label_0;
    }
    r8 = 0xffffffffffffffff;
label_2:
    rax = *((rsp + 0x38));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_7;
    }
    rax = r8;
    return rax;
label_6:
    rax = *(r13);
    r15 = *((r13 + 8));
    r12d = 0;
    r14 = rsp + 8;
    *((rsp + 8)) = rax;
    while (r12 != rax) {
        r8 = r12;
        rdx = r15 + r12;
        if (ebx < 0) {
            goto label_8;
        }
label_1:
        rax = (int64_t) ebx;
        rax >>= 6;
        rax = *((rsp + rax*8 + 0x10));
        if (((rax >> rbx) & 1) < 0) {
            goto label_8;
        }
        *(rdx) = bl;
        r12++;
        rax = *((rbp + 8));
        if (rax >= *((rbp + 0x10))) {
            goto label_9;
        }
        rdx = rax + 1;
        *((rbp + 8)) = rdx;
        ebx = *(rax);
label_3:
        rax = *((rsp + 8));
    }
    r8d = 1;
    rax = xpalloc (r15, r14, 1, 0xffffffffffffffff);
    r8 = r12;
    r15 = rax;
    rdx = r15 + r12;
    if (ebx >= 0) {
        goto label_1;
    }
label_8:
    *(rdx) = 0;
    rax = *((rsp + 8));
    *((r13 + 8)) = r15;
    *(r13) = rax;
    goto label_2;
label_9:
    rdi = rbp;
    eax = uflow ();
    ebx = eax;
    goto label_3;
label_7:
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9380 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2540 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmpp4rbmgt4 @ 0x5e90 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmpp4rbmgt4 @ 0x9a20 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmpp4rbmgt4 @ 0x23c0 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmpp4rbmgt4 @ 0x8d70 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000a828;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000a83b);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000ab28;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xab28 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2640)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2640)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2640)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmpp4rbmgt4 @ 0x9200 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmpp4rbmgt4 @ 0x98a0 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9c90 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmpp4rbmgt4 @ 0x95a0 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x9960 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2550)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmpp4rbmgt4 @ 0x2360 */
 
void uflow (void) {
    /* [15] -r-x section size 800 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2370 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmpp4rbmgt4 @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdx = ubp_av;
    bh &= *(rdi);
    *(rax) += dh;
    *((rcx + rsi)) ^= esi;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    __asm ("out 0x41, eax");
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dil;
    *(0x2800403d) += cl;
    *(rdi) += ah;
    *(rsi) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    tmp_0 = rax;
    rax = eax;
    eax = tmp_0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += 0;
    *(rax) += al;
    eax += 0;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    cl += al;
    if (cl >= 0) {
    }
    *(rax) += al;
    *(rax) += al;
    cl += al;
    if (cl >= 0) {
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    *(rax) += ah;
    *(rax) += al;
    eax = 0x1f;
    do {
        *(rax) += al;
        *((rax + 0x1f)) += bh;
label_0:
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rcx) += al;
        *(rax) += al;
        *(rsi) += al;
        *(rax) += al;
        *((rax - 0x35)) += dh;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
    } while (*(rax) overflow 0);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    if (*(rax) overflow 0) {
        goto label_0;
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(0x0000017e) += al;
    *(rax) &= al;
    al = void (*0x18b)(uint64_t, uint64_t, uint64_t, uint64_t, uint64_t, uint64_t) (rdi, rsi, rdx, rcx, r8, r9);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdx) += al;
    *(rax) += al;
    *(rsi) += al;
    *(rax) += al;
    *(rax) += bh;
    __asm ("int3");
}

/* /tmp/tmpp4rbmgt4 @ 0x23b0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmpp4rbmgt4 @ 0x23d0 */
 
void puts (void) {
    __asm ("bnd jmp qword [reloc.puts]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2400 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2430 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2460 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2470 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2480 */
 
void dup2 (void) {
    __asm ("bnd jmp qword [reloc.dup2]");
}

/* /tmp/tmpp4rbmgt4 @ 0x24a0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmpp4rbmgt4 @ 0x24b0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmpp4rbmgt4 @ 0x24c0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmpp4rbmgt4 @ 0x24d0 */
 
void freopen (void) {
    __asm ("bnd jmp qword [reloc.freopen]");
}

/* /tmp/tmpp4rbmgt4 @ 0x24e0 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmpp4rbmgt4 @ 0x24f0 */
 
void posix_fadvise (void) {
    __asm ("bnd jmp qword [reloc.posix_fadvise]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2500 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2550 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2580 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmpp4rbmgt4 @ 0x25a0 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmpp4rbmgt4 @ 0x25f0 */
 
void open (void) {
    __asm ("bnd jmp qword [reloc.open]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2600 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2610 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2650 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2660 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2670 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 816 named .plt */
    __asm ("bnd jmp qword [0x0000de38]");
}

/* /tmp/tmpp4rbmgt4 @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmpp4rbmgt4 @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}
