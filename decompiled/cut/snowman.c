
int32_t* fun_2400(void** rdi, ...);

void** fun_26e0(void** rdi, int64_t rsi);

int64_t quotearg_n_style_colon();

void fun_26c0();

signed char have_read_stdin = 0;

void** stdin = reinterpret_cast<void**>(0);

void fadvise(void** rdi, void** rsi);

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9);

void fun_2470(void** rdi, void** rsi);

uint32_t cut_file(void** rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9) {
    uint32_t eax7;
    uint32_t ebp8;
    int32_t* rax9;
    void** rax10;
    void** rbp11;
    uint32_t eax12;
    int32_t r14d13;
    int64_t rax14;

    eax7 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 45);
    ebp8 = eax7;
    if (!eax7) {
        ebp8 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1));
    }
    rax9 = fun_2400(rdi);
    if (ebp8) {
        rax10 = fun_26e0(rdi, "r");
        rbp11 = rax10;
        if (!rax10) {
            quotearg_n_style_colon();
            fun_26c0();
            eax12 = 0;
            goto addr_342b_6;
        }
    } else {
        have_read_stdin = 1;
        rbp11 = stdin;
    }
    fadvise(rbp11, 2);
    rsi(rbp11, 2);
    r14d13 = *rax9;
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp11)) & 32)) {
        r14d13 = 0;
    }
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45) || *reinterpret_cast<void***>(rdi + 1)) {
        rax14 = rpl_fclose(rbp11, 2, rdx, rcx, r8, r9);
        if (*reinterpret_cast<int32_t*>(&rax14) == -1) {
            eax12 = 1;
            if (!*rax9) 
                goto addr_342b_6; else 
                goto addr_3450_13;
        }
    } else {
        fun_2470(rbp11, 2);
    }
    eax12 = 1;
    if (r14d13) {
        addr_3450_13:
        quotearg_n_style_colon();
        fun_26c0();
        return 0;
    } else {
        addr_342b_6:
        return eax12;
    }
}

uint64_t n_frp = 0;

uint64_t n_frp_allocated = 0;

void** frp = reinterpret_cast<void**>(0);

void** x2nrealloc();

struct s0 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void add_range_pair(void** rdi, void** rsi, ...) {
    uint64_t rdx3;
    int1_t zf4;
    void** rdi5;
    void** rax6;
    struct s0* rdi7;

    rdx3 = n_frp;
    zf4 = rdx3 == n_frp_allocated;
    rdi5 = frp;
    if (zf4) {
        rax6 = x2nrealloc();
        rdx3 = n_frp;
        frp = rax6;
        rdi5 = rax6;
    }
    rdi7 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi5) + (rdx3 << 4));
    rdi7->f0 = rdi;
    rdi7->f8 = rsi;
    n_frp = rdx3 + 1;
    return;
}

int64_t fun_24c0();

int64_t fun_23f0(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24c0();
    if (r8d > 10) {
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x8cc0 + rax11 * 4) + 0x8cc0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_2550();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_23e0(void** rdi, ...);

void** xcharalloc(void** rdi, ...);

void fun_24e0();

void** quotearg_n_options(void** rdi, void** rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x5cff;
    rax8 = fun_2400(rdi);
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x61f1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5d8b;
            fun_2550();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xc160) {
                fun_23e0(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x5e1a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_24e0();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, int64_t rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x8c5b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x8c54);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x8c5f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x8c50);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbdf8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbdf8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23c3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __uflow = 0x2030;

void fun_23d3() {
    __asm__("cli ");
    goto __uflow;
}

int64_t free = 0x2040;

void fun_23e3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_23f3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2403() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2413() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2423() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_2433() {
    __asm__("cli ");
    goto __fpending;
}

int64_t ferror = 0x20a0;

void fun_2443() {
    __asm__("cli ");
    goto ferror;
}

int64_t qsort = 0x20b0;

void fun_2453() {
    __asm__("cli ");
    goto qsort;
}

int64_t reallocarray = 0x20c0;

void fun_2463() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t clearerr_unlocked = 0x20d0;

void fun_2473() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t textdomain = 0x20e0;

void fun_2483() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20f0;

void fun_2493() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2100;

void fun_24a3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2110;

void fun_24b3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2120;

void fun_24c3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2130;

void fun_24d3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2140;

void fun_24e3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2150;

void fun_24f3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2160;

void fun_2503() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x2170;

void fun_2513() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2180;

void fun_2523() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2190;

void fun_2533() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21a0;

void fun_2543() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21b0;

void fun_2553() {
    __asm__("cli ");
    goto memset;
}

int64_t fgetc = 0x21c0;

void fun_2563() {
    __asm__("cli ");
    goto fgetc;
}

int64_t strspn = 0x21d0;

void fun_2573() {
    __asm__("cli ");
    goto strspn;
}

int64_t posix_fadvise = 0x21e0;

void fun_2583() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memchr = 0x21f0;

void fun_2593() {
    __asm__("cli ");
    goto memchr;
}

int64_t memcmp = 0x2200;

void fun_25a3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2210;

void fun_25b3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2220;

void fun_25c3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2230;

void fun_25d3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2240;

void fun_25e3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2250;

void fun_25f3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2260;

void fun_2603() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2270;

void fun_2613() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2280;

void fun_2623() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2290;

void fun_2633() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t ungetc = 0x22a0;

void fun_2643() {
    __asm__("cli ");
    goto ungetc;
}

int64_t __freading = 0x22b0;

void fun_2653() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x22c0;

void fun_2663() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x22d0;

void fun_2673() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22e0;

void fun_2683() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22f0;

void fun_2693() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t __fread_chk = 0x2300;

void fun_26a3() {
    __asm__("cli ");
    goto __fread_chk;
}

int64_t memmove = 0x2310;

void fun_26b3() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2320;

void fun_26c3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2330;

void fun_26d3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2340;

void fun_26e3() {
    __asm__("cli ");
    goto fopen;
}

int64_t __cxa_atexit = 0x2350;

void fun_26f3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2360;

void fun_2703() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2370;

void fun_2713() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2380;

void fun_2723() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2390;

void fun_2733() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23a0;

void fun_2743() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x23b0;

void fun_2753() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2680(int64_t rdi, ...);

void fun_24a0(int64_t rdi, int64_t rsi);

void fun_2480(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

unsigned char suppress_non_delimited = 0;

unsigned char delim = 0;

int32_t fun_24f0(int64_t rdi, void*** rsi, void** rdx, int64_t rcx);

void usage();

void** stdout = reinterpret_cast<void**>(0);

int64_t Version = 0x8bef;

void version_etc(void** rdi, int64_t rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_2700();

void** fun_24b0();

unsigned char complement = 0;

void set_fields(int64_t rdi);

void** output_delimiter_string = reinterpret_cast<void**>(0);

void** output_delimiter_length = reinterpret_cast<void**>(0);

signed char output_delimiter_default = 0;

int32_t optind = 0;

int64_t fun_27a3(int32_t edi, void*** rsi) {
    void** r13_3;
    int32_t ebp4;
    void*** rbx5;
    void** rdi6;
    int64_t r8_7;
    int64_t rcx8;
    void** rdx9;
    int64_t rdi10;
    int32_t eax11;
    void** rdi12;
    int64_t rax13;
    void** rax14;
    uint32_t esi15;
    void** rsi16;
    int1_t zf17;
    int1_t zf18;
    uint32_t esi19;
    int1_t zf20;
    uint32_t eax21;
    int32_t eax22;
    uint32_t eax23;
    uint32_t r12d24;
    void** rdi25;
    uint32_t eax26;
    int32_t eax27;
    int1_t zf28;
    void** rdi29;
    int64_t rax30;
    uint32_t r12d31;
    int64_t rax32;

    __asm__("cli ");
    r13_3 = reinterpret_cast<void**>("b:c:d:f:nsz");
    ebp4 = edi;
    rbx5 = rsi;
    rdi6 = *rsi;
    set_program_name(rdi6);
    fun_2680(6, 6);
    fun_24a0("coreutils", "/usr/local/share/locale");
    fun_2480("coreutils", "/usr/local/share/locale");
    atexit(0x4000, "/usr/local/share/locale");
    suppress_non_delimited = 0;
    delim = 0;
    have_read_stdin = 0;
    *reinterpret_cast<int32_t*>(&r8_7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_7) + 4) = 0;
    rcx8 = 0xba40;
    rdx9 = reinterpret_cast<void**>("b:c:d:f:nsz");
    *reinterpret_cast<int32_t*>(&rdi10) = ebp4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
    eax11 = fun_24f0(rdi10, rbx5, "b:c:d:f:nsz", 0xba40);
    if (eax11 != -1) {
        if (eax11 > 0x81) {
            addr_2b38_4:
            usage();
            goto addr_2b42_5;
        } else {
            if (eax11 <= 97) {
                if (eax11 == 0xffffff7d) {
                    rdi12 = stdout;
                    rcx8 = Version;
                    r8_7 = reinterpret_cast<int64_t>("David M. Ihnat");
                    rdx9 = reinterpret_cast<void**>("GNU coreutils");
                    version_etc(rdi12, "cut", "GNU coreutils", rcx8, "David M. Ihnat", "David MacKenzie", "Jim Meyering", 0);
                    eax11 = fun_2700();
                }
                if (eax11 != 0xffffff7e) 
                    goto addr_2b38_4;
            } else {
                *reinterpret_cast<uint32_t*>(&rax13) = eax11 - 98;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax13) <= 31) {
                    goto *reinterpret_cast<int32_t*>(0x89d0 + rax13 * 4) + 0x89d0;
                }
            }
        }
        usage();
    }
    *reinterpret_cast<int32_t*>(&rdx9) = 5;
    *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
    if (1) {
        addr_2b23_14:
        rax14 = fun_24b0();
        rdx9 = rax14;
        fun_26c0();
        goto addr_2b38_4;
    } else {
        if (1) {
            esi15 = complement;
            *reinterpret_cast<uint32_t*>(&rsi16) = esi15 + esi15;
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            set_fields(0);
            if (1) {
                delim = 9;
            }
            zf17 = output_delimiter_string == 0;
            r13_3 = reinterpret_cast<void**>(0x2c70);
            if (!zf17) 
                goto addr_2a0b_19; else 
                goto addr_2aa1_20;
        }
        if (0) 
            goto addr_2b23_14;
        zf18 = suppress_non_delimited == 0;
        *reinterpret_cast<int32_t*>(&rdx9) = 5;
        *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
        if (!zf18) 
            goto addr_2b23_14;
    }
    esi19 = complement;
    r13_3 = reinterpret_cast<void**>(0x3200);
    *reinterpret_cast<uint32_t*>(&rsi16) = esi19 + esi19 | 4;
    *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
    set_fields(0);
    zf20 = output_delimiter_string == 0;
    delim = 9;
    if (zf20) {
        addr_2aa1_20:
        eax21 = delim;
        r13_3 = reinterpret_cast<void**>(0x3200);
        output_delimiter_length = reinterpret_cast<void**>(1);
        output_delimiter_default = *reinterpret_cast<signed char*>(&eax21);
        output_delimiter_string = reinterpret_cast<void**>(0xc0f1);
        if (1) {
            r13_3 = reinterpret_cast<void**>(0x2c70);
        }
    } else {
        addr_2a0b_19:
        eax22 = optind;
        if (eax22 == ebp4) {
            addr_2b42_5:
            rsi16 = r13_3;
            eax23 = cut_file("-", rsi16, rdx9, rcx8, r8_7, "David MacKenzie");
            r12d24 = eax23;
            goto addr_2a4c_25;
        } else {
            r12d24 = 1;
            if (eax22 < ebp4) {
                do {
                    rsi16 = r13_3;
                    rdi25 = rbx5[eax22 * 8];
                    eax26 = cut_file(rdi25, rsi16, 5, rcx8, r8_7, "David MacKenzie");
                    r12d24 = r12d24 & eax26;
                    eax27 = optind;
                    eax22 = eax27 + 1;
                    optind = eax22;
                } while (eax22 < ebp4);
                goto addr_2a4c_25;
            }
        }
    }
    goto addr_2a0b_19;
    addr_2a4c_25:
    zf28 = have_read_stdin == 0;
    if (!zf28 && (rdi29 = stdin, rax30 = rpl_fclose(rdi29, rsi16, rdx9, rcx8, r8_7, "David MacKenzie"), !(*reinterpret_cast<int32_t*>(&rax30) + 1))) {
        fun_2400(rdi29, rdi29);
        r12d24 = 0;
        fun_26c0();
    }
    r12d31 = r12d24 ^ 1;
    *reinterpret_cast<uint32_t*>(&rax32) = *reinterpret_cast<unsigned char*>(&r12d31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
    return rax32;
}

int64_t __libc_start_main = 0;

void fun_2b83() {
    __asm__("cli ");
    __libc_start_main(0x27a0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_23c0(int64_t rdi);

int64_t fun_2c23() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23c0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2c63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** current_rp = reinterpret_cast<void**>(0);

uint32_t fun_23d0(void** rdi, ...);

void fun_2640(int64_t rdi, void** rsi, ...);

unsigned char line_delim = 10;

void** getndelim2(int64_t rdi, int64_t rsi);

void** field_1_buffer = reinterpret_cast<void**>(0);

void xalloc_die();

void fun_2660(void** rdi, int64_t rsi, void** rdx, void** rcx, ...);

void fun_2510();

void fun_2540(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void fun_2c73(void** rdi) {
    void** rbx2;
    void** rax3;
    void** rax4;
    uint32_t eax5;
    int64_t rdi6;
    void** r15_7;
    void** rax8;
    uint32_t r13d9;
    uint32_t r14d10;
    unsigned char r12b11;
    uint32_t ebp12;
    void** rax13;
    int64_t r9_14;
    int64_t r8_15;
    void** rax16;
    void** rdi17;
    void** rdi18;
    int1_t zf19;
    void** rax20;
    uint32_t ebp21;
    uint32_t eax22;
    uint32_t ecx23;
    uint32_t edx24;
    uint32_t eax25;
    uint32_t esi26;
    void** rcx27;
    void** rdx28;
    void** rdi29;
    void** rax30;
    uint32_t eax31;
    void** rdi32;
    unsigned char* rax33;
    uint32_t eax34;
    int64_t rdi35;
    void** rcx36;
    uint32_t eax37;
    int1_t zf38;
    void** rax39;
    int1_t zf40;
    void** rcx41;
    void** rcx42;
    void** rdi43;
    unsigned char* rdx44;
    void** rax45;
    int64_t rdi46;
    uint32_t eax47;
    uint32_t eax48;
    int1_t zf49;
    uint32_t ecx50;
    uint1_t zf51;
    int32_t edi52;
    void** rdi53;
    unsigned char* rax54;
    int1_t zf55;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = frp;
    current_rp = rax3;
    rax4 = *reinterpret_cast<void***>(rdi + 8);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 16)) <= reinterpret_cast<unsigned char>(rax4)) {
        eax5 = fun_23d0(rdi);
        *reinterpret_cast<uint32_t*>(&rdi6) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
        if (eax5 != 0xffffffff) {
            addr_2cac_3:
            *reinterpret_cast<int32_t*>(&r15_7) = 1;
            *reinterpret_cast<int32_t*>(&r15_7 + 4) = 0;
            fun_2640(rdi6, rbx2);
            rax8 = current_rp;
            r13d9 = 0;
            r14d10 = 0;
            r12b11 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax8)) > reinterpret_cast<unsigned char>(1))) ^ suppress_non_delimited);
        } else {
            addr_3122_4:
            return;
        }
        while (1) {
            *reinterpret_cast<unsigned char*>(&rax8) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r15_7 == 1)) & r12b11);
            ebp12 = *reinterpret_cast<uint32_t*>(&rax8);
            rax13 = current_rp;
            if (*reinterpret_cast<unsigned char*>(&rax8)) {
                *reinterpret_cast<uint32_t*>(&r9_14) = line_delim;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r8_15) = delim;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
                rax16 = getndelim2(0xc118, 0xc110);
                if (reinterpret_cast<signed char>(rax16) < reinterpret_cast<signed char>(0)) {
                    rdi17 = field_1_buffer;
                    fun_23e0(rdi17, rdi17);
                    field_1_buffer = reinterpret_cast<void**>(0);
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx2)) & 48) 
                        goto addr_3122_4;
                    xalloc_die();
                } else {
                    if (!rax16) 
                        break;
                    rdi18 = field_1_buffer;
                    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi18) + reinterpret_cast<unsigned char>(rax16) + 0xffffffffffffffff);
                    zf19 = delim == *reinterpret_cast<unsigned char*>(&rax8);
                    if (zf19) 
                        goto addr_2f90_11; else 
                        goto addr_2e6a_12;
                }
            } else {
                addr_2cf9_13:
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13)) > reinterpret_cast<unsigned char>(r15_7)) {
                    while (1) {
                        rax20 = *reinterpret_cast<void***>(rbx2 + 8);
                        if (reinterpret_cast<unsigned char>(rax20) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx2 + 16))) {
                            *reinterpret_cast<void***>(rbx2 + 8) = rax20 + 1;
                            ebp21 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax20));
                        } else {
                            eax22 = fun_23d0(rbx2, rbx2);
                            ebp21 = eax22;
                        }
                        ecx23 = delim;
                        edx24 = line_delim;
                        eax25 = ecx23;
                        esi26 = ecx23;
                        if (ecx23 == ebp21) 
                            goto addr_2fc8_18;
                        if (static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&edx24)) == ebp21) 
                            goto addr_2ed6_20;
                        if (ebp21 == 0xffffffff) 
                            goto addr_2f16_22;
                        r14d10 = ebp21;
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&r13d9)) {
                        rcx27 = stdout;
                        rdx28 = output_delimiter_length;
                        rdi29 = output_delimiter_string;
                        fun_2660(rdi29, 1, rdx28, rcx27, rdi29, 1, rdx28, rcx27);
                    }
                    while (1) {
                        rax30 = *reinterpret_cast<void***>(rbx2 + 8);
                        if (reinterpret_cast<unsigned char>(rax30) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx2 + 16))) {
                            *reinterpret_cast<void***>(rbx2 + 8) = rax30 + 1;
                            ebp21 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax30));
                        } else {
                            eax31 = fun_23d0(rbx2, rbx2);
                            ebp21 = eax31;
                        }
                        ecx23 = delim;
                        edx24 = line_delim;
                        eax25 = ecx23;
                        esi26 = ecx23;
                        if (ecx23 == ebp21) 
                            goto addr_2ff8_30;
                        if (static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&edx24)) == ebp21) 
                            goto addr_2ed0_32;
                        if (ebp21 == 0xffffffff) 
                            goto addr_2f10_34;
                        rdi32 = stdout;
                        rax33 = *reinterpret_cast<unsigned char**>(rdi32 + 40);
                        if (reinterpret_cast<uint64_t>(rax33) >= reinterpret_cast<uint64_t>(*reinterpret_cast<unsigned char**>(rdi32 + 48))) {
                            fun_2510();
                        } else {
                            *reinterpret_cast<unsigned char**>(rdi32 + 40) = rax33 + 1;
                            *rax33 = *reinterpret_cast<unsigned char*>(&ebp21);
                        }
                        r14d10 = ebp21;
                    }
                }
            }
            eax34 = fun_23d0(rbx2, rbx2);
            *reinterpret_cast<uint32_t*>(&rdi35) = eax34;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi35) + 4) = 0;
            if (eax34 != 0xffffffff) {
                addr_3152_40:
                fun_2640(rdi35, rbx2, rdi35, rbx2);
                goto addr_3103_41;
            } else {
                rax13 = current_rp;
                goto addr_2fa1_43;
            }
            addr_2f90_11:
            rax13 = current_rp;
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13)) <= reinterpret_cast<unsigned char>(1)) {
                rcx36 = stdout;
                fun_2660(rdi18, 1, rax16 + 0xffffffffffffffff, rcx36);
                eax37 = line_delim;
                zf38 = delim == *reinterpret_cast<unsigned char*>(&eax37);
                if (zf38) {
                    rax39 = *reinterpret_cast<void***>(rbx2 + 8);
                    if (reinterpret_cast<unsigned char>(rax39) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx2 + 16))) {
                        *reinterpret_cast<void***>(rbx2 + 8) = rax39 + 1;
                        *reinterpret_cast<uint32_t*>(&rdi35) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax39));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi35) + 4) = 0;
                        goto addr_3152_40;
                    }
                } else {
                    addr_3103_41:
                    rax13 = current_rp;
                    r13d9 = ebp12;
                    goto addr_2fa1_43;
                }
            } else {
                addr_2fa1_43:
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13 + 8)) > reinterpret_cast<unsigned char>(1)) {
                    *reinterpret_cast<int32_t*>(&r15_7) = 2;
                    *reinterpret_cast<int32_t*>(&r15_7 + 4) = 0;
                    r14d10 = 0;
                    goto addr_2cf9_13;
                } else {
                    rax13 = rax13 + 16;
                    *reinterpret_cast<int32_t*>(&r15_7) = 2;
                    *reinterpret_cast<int32_t*>(&r15_7 + 4) = 0;
                    r14d10 = 0;
                    current_rp = rax13;
                    goto addr_2cf9_13;
                }
            }
            addr_2e6a_12:
            zf40 = suppress_non_delimited == 0;
            if (!zf40) {
                *reinterpret_cast<int32_t*>(&r15_7) = 1;
                *reinterpret_cast<int32_t*>(&r15_7 + 4) = 0;
                r14d10 = 0;
                continue;
            } else {
                rcx41 = stdout;
                fun_2660(rdi18, 1, rax16, rcx41);
                rcx42 = field_1_buffer;
                r14d10 = line_delim;
                if (static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx42) + reinterpret_cast<unsigned char>(rax16) + 0xffffffffffffffff)) != r14d10) {
                    rdi43 = stdout;
                    rdx44 = *reinterpret_cast<unsigned char**>(rdi43 + 40);
                    if (reinterpret_cast<uint64_t>(rdx44) >= reinterpret_cast<uint64_t>(*reinterpret_cast<unsigned char**>(rdi43 + 48))) {
                        fun_2510();
                        r14d10 = line_delim;
                    } else {
                        *reinterpret_cast<unsigned char**>(rdi43 + 40) = rdx44 + 1;
                        *rdx44 = *reinterpret_cast<unsigned char*>(&r14d10);
                    }
                }
                *reinterpret_cast<int32_t*>(&r15_7) = 1;
                *reinterpret_cast<int32_t*>(&r15_7 + 4) = 0;
                continue;
            }
            addr_2fc8_18:
            if (*reinterpret_cast<unsigned char*>(&ecx23) == *reinterpret_cast<unsigned char*>(&edx24)) {
                addr_3016_55:
                rax45 = *reinterpret_cast<void***>(rbx2 + 8);
                if (reinterpret_cast<unsigned char>(rax45) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx2 + 16))) {
                    *reinterpret_cast<void***>(rbx2 + 8) = rax45 + 1;
                    *reinterpret_cast<uint32_t*>(&rdi46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax45));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi46) + 4) = 0;
                    goto addr_302f_57;
                }
            } else {
                r14d10 = ebp21;
                goto addr_2fcf_59;
            }
            eax47 = fun_23d0(rbx2, rbx2);
            *reinterpret_cast<uint32_t*>(&rdi46) = eax47;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi46) + 4) = 0;
            if (eax47 != 0xffffffff) {
                addr_302f_57:
                fun_2640(rdi46, rbx2);
                ecx23 = delim;
                esi26 = ecx23;
                if (ecx23 != ebp21) {
                    addr_2ee6_61:
                    *reinterpret_cast<uint32_t*>(&rax8) = line_delim;
                    edx24 = *reinterpret_cast<uint32_t*>(&rax8);
                    if (*reinterpret_cast<uint32_t*>(&rax8) == ebp21) {
                        addr_305f_62:
                        eax48 = r14d10;
                        r14d10 = ebp21;
                        if (*reinterpret_cast<signed char*>(&r13d9)) 
                            goto addr_2f43_63;
                    } else {
                        if (ebp21 == 0xffffffff) {
                            ebp21 = *reinterpret_cast<uint32_t*>(&rax8);
                            goto addr_2f1b_66;
                        } else {
                            addr_2f00_67:
                            r14d10 = ebp21;
                            continue;
                        }
                    }
                } else {
                    addr_3048_68:
                    r14d10 = ecx23;
                    goto addr_2fcf_59;
                }
            } else {
                ebp21 = line_delim;
                esi26 = delim;
                edx24 = ebp21;
                goto addr_2f1b_66;
            }
            zf49 = suppress_non_delimited == 1;
            if (!zf49 || r15_7 != 1) {
                ecx50 = ebp21;
            } else {
                addr_2f6c_72:
                rax8 = frp;
                *reinterpret_cast<int32_t*>(&r15_7) = 1;
                *reinterpret_cast<int32_t*>(&r15_7 + 4) = 0;
                r13d9 = 0;
                current_rp = rax8;
                continue;
            }
            addr_3083_73:
            zf51 = reinterpret_cast<uint1_t>(ecx50 == ebp21);
            r14d10 = ebp21;
            ebp21 = ecx50;
            *reinterpret_cast<unsigned char*>(&edi52) = zf51;
            addr_2f2f_74:
            if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(ebp21 != eax48)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&edx24) == *reinterpret_cast<unsigned char*>(&esi26))) || *reinterpret_cast<unsigned char*>(&edi52)) {
                addr_2f43_63:
                rdi53 = stdout;
                rax54 = *reinterpret_cast<unsigned char**>(rdi53 + 40);
                if (reinterpret_cast<uint64_t>(rax54) >= reinterpret_cast<uint64_t>(*reinterpret_cast<unsigned char**>(rdi53 + 48))) {
                    fun_2510();
                    goto addr_2f62_76;
                } else {
                    *reinterpret_cast<unsigned char**>(rdi53 + 40) = rax54 + 1;
                    *rax54 = *reinterpret_cast<unsigned char*>(&edx24);
                    goto addr_2f62_76;
                }
            } else {
                addr_2f62_76:
                if (r14d10 == 0xffffffff) 
                    goto addr_3122_4; else 
                    goto addr_2f6c_72;
            }
            addr_2f1b_66:
            if (!*reinterpret_cast<signed char*>(&r13d9)) {
                zf55 = suppress_non_delimited == 1;
                if (!zf55) 
                    goto addr_30cf_79;
                if (r15_7 == 1) 
                    goto addr_3122_4;
                addr_30cf_79:
                ecx50 = ebp21;
                eax48 = r14d10;
                ebp21 = 0xffffffff;
                goto addr_3083_73;
            } else {
                eax48 = r14d10;
                edi52 = 0;
                r14d10 = 0xffffffff;
                goto addr_2f2f_74;
            }
            addr_2fcf_59:
            rax8 = current_rp;
            ++r15_7;
            if (reinterpret_cast<unsigned char>(r15_7) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax8 + 8))) 
                continue;
            rax8 = rax8 + 16;
            current_rp = rax8;
            continue;
            addr_2ed6_20:
            if (*reinterpret_cast<unsigned char*>(&edx24) == *reinterpret_cast<unsigned char*>(&eax25)) {
                *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&edx24);
                esi26 = *reinterpret_cast<uint32_t*>(&rax8);
                edx24 = *reinterpret_cast<uint32_t*>(&rax8);
                if (*reinterpret_cast<uint32_t*>(&rax8) != ebp21) 
                    goto addr_2f00_67; else 
                    goto addr_305f_62;
            }
            if (ecx23 == ebp21) 
                goto addr_3048_68; else 
                goto addr_2ee6_61;
            addr_2f16_22:
            esi26 = eax25;
            ebp21 = *reinterpret_cast<unsigned char*>(&edx24);
            goto addr_2f1b_66;
            addr_2ff8_30:
            if (*reinterpret_cast<unsigned char*>(&ecx23) == *reinterpret_cast<unsigned char*>(&edx24)) {
                r13d9 = 1;
                goto addr_3016_55;
            } else {
                r14d10 = ebp21;
                r13d9 = 1;
                goto addr_2fcf_59;
            }
            addr_2ed0_32:
            r13d9 = 1;
            goto addr_2ed6_20;
            addr_2f10_34:
            r13d9 = 1;
            goto addr_2f16_22;
        }
        fun_2540("n_bytes != 0", "src/cut.c", 0x13b, "cut_fields", r8_15, r9_14);
    } else {
        *reinterpret_cast<void***>(rdi + 8) = rax4 + 1;
        *reinterpret_cast<uint32_t*>(&rdi6) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
        goto addr_2cac_3;
    }
}

void fun_3203(void** rdi) {
    void** rax2;
    int32_t r14d3;
    void** rbp4;
    void** rbx5;
    uint32_t esi6;
    uint32_t r13d7;
    void** rax8;
    void** rdi9;
    void** rcx10;
    void** rdx11;
    void** rdi12;
    unsigned char* rax13;
    void** rax14;
    uint32_t eax15;
    uint32_t r8d16;
    void** rdi17;
    unsigned char* rax18;
    void** rax19;
    void** rdi20;
    unsigned char* rdx21;

    __asm__("cli ");
    rax2 = frp;
    r14d3 = 0;
    current_rp = rax2;
    *reinterpret_cast<int32_t*>(&rbp4) = 0;
    *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
    rbx5 = rdi;
    goto addr_3230_2;
    addr_3312_3:
    return;
    addr_32e4_4:
    while (esi6 != 0xffffffff) {
        r13d7 = esi6;
        while (1) {
            rax8 = current_rp;
            ++rbp4;
            if (reinterpret_cast<unsigned char>(rbp4) > reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax8 + 8))) {
                rax8 = rax8 + 16;
                current_rp = rax8;
            }
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax8)) <= reinterpret_cast<unsigned char>(rbp4)) {
                rdi9 = output_delimiter_string;
                if (rdi9 != 0xc0f1) {
                    *reinterpret_cast<unsigned char*>(&r14d3) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r14d3) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax8) == rbp4)));
                    if (*reinterpret_cast<unsigned char*>(&r14d3)) {
                        rcx10 = stdout;
                        rdx11 = output_delimiter_length;
                        fun_2660(rdi9, 1, rdx11, rcx10);
                    } else {
                        r14d3 = 1;
                    }
                }
                rdi12 = stdout;
                rax13 = *reinterpret_cast<unsigned char**>(rdi12 + 40);
                if (reinterpret_cast<uint64_t>(rax13) >= reinterpret_cast<uint64_t>(*reinterpret_cast<unsigned char**>(rdi12 + 48))) {
                    fun_2510();
                } else {
                    *reinterpret_cast<unsigned char**>(rdi12 + 40) = rax13 + 1;
                    *rax13 = *reinterpret_cast<unsigned char*>(&r13d7);
                    rax14 = *reinterpret_cast<void***>(rbx5 + 8);
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx5 + 16)) > reinterpret_cast<unsigned char>(rax14)) 
                        goto addr_323e_16; else 
                        goto addr_32cd_17;
                }
            }
            while (1) {
                addr_3230_2:
                rax14 = *reinterpret_cast<void***>(rbx5 + 8);
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx5 + 16)) <= reinterpret_cast<unsigned char>(rax14)) {
                    addr_32cd_17:
                    eax15 = fun_23d0(rbx5, rbx5);
                    r8d16 = line_delim;
                    esi6 = eax15;
                    if (r8d16 != esi6) 
                        goto addr_32e4_4;
                } else {
                    addr_323e_16:
                    esi6 = line_delim;
                    *reinterpret_cast<void***>(rbx5 + 8) = rax14 + 1;
                    r13d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax14));
                    if (r13d7 != esi6) 
                        break;
                }
                rdi17 = stdout;
                rax18 = *reinterpret_cast<unsigned char**>(rdi17 + 40);
                if (reinterpret_cast<uint64_t>(rax18) >= reinterpret_cast<uint64_t>(*reinterpret_cast<unsigned char**>(rdi17 + 48))) {
                    fun_2510();
                } else {
                    *reinterpret_cast<unsigned char**>(rdi17 + 40) = rax18 + 1;
                    *rax18 = *reinterpret_cast<unsigned char*>(&esi6);
                }
                rax19 = frp;
                *reinterpret_cast<int32_t*>(&rbp4) = 0;
                *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
                r14d3 = 0;
                current_rp = rax19;
            }
        }
    }
    if (rbp4) {
        rdi20 = stdout;
        rdx21 = *reinterpret_cast<unsigned char**>(rdi20 + 40);
        if (reinterpret_cast<uint64_t>(rdx21) >= reinterpret_cast<uint64_t>(*reinterpret_cast<unsigned char**>(rdi20 + 48))) {
            goto fun_2510;
        } else {
            *reinterpret_cast<unsigned char**>(rdi20 + 40) = rdx21 + 1;
            *rdx21 = *reinterpret_cast<unsigned char*>(&r8d16);
            goto addr_3312_3;
        }
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2690(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_25b0(void** rdi, void** rsi, int64_t rdx, void** rcx);

int32_t fun_25d0(int64_t rdi);

int32_t fun_2410(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_2720(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_34d3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    void** r12_21;
    void** rax22;
    void** r12_23;
    void** rax24;
    void** r12_25;
    void** rax26;
    void** r12_27;
    void** rax28;
    void** r12_29;
    void** rax30;
    int32_t eax31;
    void** r13_32;
    void** rax33;
    void** rax34;
    int32_t eax35;
    void** rax36;
    void** rax37;
    void** rax38;
    int32_t eax39;
    void** rax40;
    void** r15_41;
    void** rax42;
    void** rax43;
    void** rax44;
    void** rdi45;
    void** r8_46;
    void** r9_47;
    int64_t v48;
    int64_t v49;
    int64_t v50;
    int64_t v51;
    int64_t v52;
    int64_t v53;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_24b0();
            fun_2690(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_24b0();
            fun_25b0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_24b0();
            fun_25b0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_24b0();
            fun_25b0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_24b0();
            fun_25b0(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_24b0();
            fun_25b0(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_24b0();
            fun_25b0(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_24b0();
            fun_25b0(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_24b0();
            fun_25b0(rax22, r12_21, 5, rcx6);
            r12_23 = stdout;
            rax24 = fun_24b0();
            fun_25b0(rax24, r12_23, 5, rcx6);
            r12_25 = stdout;
            rax26 = fun_24b0();
            fun_25b0(rax26, r12_25, 5, rcx6);
            r12_27 = stdout;
            rax28 = fun_24b0();
            fun_25b0(rax28, r12_27, 5, rcx6);
            r12_29 = stdout;
            rax30 = fun_24b0();
            fun_25b0(rax30, r12_29, 5, rcx6);
            do {
                if (1) 
                    break;
                eax31 = fun_25d0("cut");
            } while (eax31);
            r13_32 = v4;
            if (!r13_32) {
                rax33 = fun_24b0();
                fun_2690(1, rax33, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax34 = fun_2680(5);
                if (!rax34 || (eax35 = fun_2410(rax34, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax35)) {
                    rax36 = fun_24b0();
                    r13_32 = reinterpret_cast<void**>("cut");
                    fun_2690(1, rax36, "https://www.gnu.org/software/coreutils/", "cut");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_32 = reinterpret_cast<void**>("cut");
                    goto addr_3930_9;
                }
            } else {
                rax37 = fun_24b0();
                fun_2690(1, rax37, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax38 = fun_2680(5);
                if (!rax38 || (eax39 = fun_2410(rax38, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax39)) {
                    addr_3836_11:
                    rax40 = fun_24b0();
                    fun_2690(1, rax40, "https://www.gnu.org/software/coreutils/", "cut");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_32 == "cut")) {
                        r12_2 = reinterpret_cast<void**>(0x9081);
                    }
                } else {
                    addr_3930_9:
                    r15_41 = stdout;
                    rax42 = fun_24b0();
                    fun_25b0(rax42, r15_41, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3836_11;
                }
            }
            rax43 = fun_24b0();
            rcx6 = r12_2;
            fun_2690(1, rax43, r13_32, rcx6);
            addr_352e_14:
            fun_2700();
        }
    } else {
        rax44 = fun_24b0();
        rdi45 = stderr;
        rcx6 = r12_2;
        fun_2720(rdi45, 1, rax44, rcx6, r8_46, r9_47, v48, v49, v50, v51, v52, v53);
        goto addr_352e_14;
    }
}

int64_t fun_3963(uint64_t* rdi, uint64_t* rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>(*rdi > *rsi);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(*rdi < *rsi)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

struct s4 {
    signed char f0;
    signed char f1;
};

unsigned char** fun_2750(void** rdi, ...);

/* num_start.0 */
void** num_start_0 = reinterpret_cast<void**>(0);

int64_t fun_2570(void** rdi, int64_t rsi, void** rdx, int64_t rcx);

void** ximemdup0(void** rdi, int64_t rsi, void** rdx, int64_t rcx);

int64_t quote(void** rdi, ...);

void fun_2450(void** rdi);

struct s5 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s6 {
    signed char[1] pad1;
    void** f1;
};

struct s7 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s8 {
    uint64_t f0;
    uint64_t f8;
};

void fun_26b0();

void** xrealloc(void** rdi);

struct s9 {
    int64_t f0;
    int64_t f8;
};

void fun_3a03(struct s4* rdi, uint32_t esi) {
    struct s4* rbx3;
    uint32_t eax4;
    uint32_t v5;
    uint32_t v6;
    void** rdx7;
    void** rdi8;
    signed char* rbx9;
    uint32_t esi10;
    int32_t r13d11;
    uint32_t r15d12;
    void** r12_13;
    uint32_t ebp14;
    void** r14_15;
    unsigned char** rax16;
    int64_t rax17;
    int64_t rbp18;
    int1_t zf19;
    int64_t rcx20;
    int1_t zf21;
    void** rax22;
    void** rax23;
    void** rax24;
    void** rbp25;
    int64_t rax26;
    void** rax27;
    int64_t rax28;
    void** rax29;
    uint64_t rsi30;
    void** rax31;
    void** rdi32;
    uint64_t r14_33;
    void* rbp34;
    uint64_t rbx35;
    void** r15_36;
    void** rax37;
    void** rsi38;
    struct s5* rbp39;
    uint64_t r12_40;
    void** rdi41;
    void** rsi42;
    void** rdi43;
    void* r13_44;
    void* r12_45;
    struct s7* rsi46;
    struct s8* rdi47;
    uint64_t rax48;
    uint64_t rax49;
    uint64_t rax50;
    void** rax51;
    uint64_t rdx52;
    struct s9* rax53;
    int64_t rax54;
    void** rax55;
    void** rax56;
    void** rax57;

    __asm__("cli ");
    rbx3 = rdi;
    eax4 = esi & 1;
    v5 = esi;
    v6 = eax4;
    if (!eax4 || (rdi->f0 != 45 || rdi->f1)) {
        *reinterpret_cast<uint32_t*>(&rdx7) = 0;
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    } else {
        rbx3 = reinterpret_cast<struct s4*>(&rbx3->f1);
        *reinterpret_cast<uint32_t*>(&rdx7) = 1;
        *reinterpret_cast<int32_t*>(&rdi8) = 1;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    }
    rbx9 = &rbx3->f1;
    esi10 = *reinterpret_cast<uint32_t*>(&rdx7);
    r13d11 = 0;
    r15d12 = 0;
    *reinterpret_cast<int32_t*>(&r12_13) = 1;
    *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
    while (1) {
        ebp14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx9 + 0xffffffffffffffff));
        r14_15 = reinterpret_cast<void**>(rbx9 + 0xffffffffffffffff);
        if (*reinterpret_cast<unsigned char*>(&ebp14) != 45) {
            if (*reinterpret_cast<unsigned char*>(&ebp14) == 44 || ((rax16 = fun_2750(rdi8), esi10 = *reinterpret_cast<unsigned char*>(&esi10), *reinterpret_cast<uint32_t*>(&rdx7) = *reinterpret_cast<unsigned char*>(&rdx7), *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0, *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<unsigned char*>(&ebp14), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0, rdi8 = rdi8, !!((*rax16)[rax17 * 2] & 1)) || !*reinterpret_cast<unsigned char*>(&ebp14))) {
                if (!*reinterpret_cast<unsigned char*>(&esi10)) {
                    if (!rdi8) 
                        break;
                    add_range_pair(rdi8, rdi8);
                    if (!*reinterpret_cast<void***>(rbx9 + 0xffffffffffffffff)) 
                        goto addr_3b37_10;
                } else {
                    if (!*reinterpret_cast<unsigned char*>(&rdx7)) {
                        if (*reinterpret_cast<signed char*>(&r15d12)) 
                            goto addr_3c69_13;
                        if (!v6) 
                            goto addr_3e01_15;
                        *reinterpret_cast<int32_t*>(&r12_13) = 1;
                        *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
                        goto addr_3abe_17;
                    }
                    if (!*reinterpret_cast<signed char*>(&r15d12)) {
                        addr_3abe_17:
                        add_range_pair(r12_13, 0xffffffffffffffff);
                        goto addr_3acd_19;
                    } else {
                        addr_3c69_13:
                        if (reinterpret_cast<unsigned char>(r12_13) > reinterpret_cast<unsigned char>(rdi8)) 
                            goto addr_3e9c_20; else 
                            goto addr_3c72_21;
                    }
                }
            } else {
                *reinterpret_cast<int32_t*>(&rbp18) = *reinterpret_cast<signed char*>(&ebp14);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp18) + 4) = 0;
                if (static_cast<uint32_t>(rbp18 - 48) > 9) 
                    goto addr_3f78_23;
                if (!*reinterpret_cast<signed char*>(&r13d11)) 
                    goto addr_3d18_25;
                zf19 = num_start_0 == 0;
                if (zf19) 
                    goto addr_3d18_25; else 
                    goto addr_3cbf_27;
            }
        } else {
            if (*reinterpret_cast<unsigned char*>(&esi10)) 
                goto addr_3ecf_29;
            *reinterpret_cast<unsigned char*>(&rcx20) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi8 == 0)) & *reinterpret_cast<unsigned char*>(&rdx7));
            r13d11 = *reinterpret_cast<int32_t*>(&rcx20);
            if (*reinterpret_cast<unsigned char*>(&rcx20)) 
                goto addr_3ead_31;
            if (!*reinterpret_cast<unsigned char*>(&rdx7)) 
                goto addr_3c88_33; else 
                goto addr_3b17_34;
        }
        addr_3ad3_35:
        r13d11 = 0;
        esi10 = 0;
        r15d12 = 0;
        *reinterpret_cast<uint32_t*>(&rdx7) = 0;
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
        addr_3adf_36:
        ++rbx9;
        continue;
        addr_3acd_19:
        if (!*reinterpret_cast<void***>(rbx9 + 0xffffffffffffffff)) 
            goto addr_3b37_10; else 
            goto addr_3ad3_35;
        addr_3c72_21:
        add_range_pair(r12_13, rdi8);
        goto addr_3acd_19;
        addr_3d18_25:
        num_start_0 = r14_15;
        addr_3cbf_27:
        zf21 = *reinterpret_cast<unsigned char*>(&esi10) == 0;
        if (zf21) {
            *reinterpret_cast<uint32_t*>(&rdx7) = 1;
            *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
        }
        if (!zf21) {
            r15d12 = esi10;
        }
        if (reinterpret_cast<unsigned char>(rdi8) > reinterpret_cast<unsigned char>(0x1999999999999999)) 
            goto addr_3e2c_41;
        rax22 = reinterpret_cast<void**>(*reinterpret_cast<int32_t*>(&rbp18) - 48 + reinterpret_cast<uint64_t>(rdi8 + reinterpret_cast<unsigned char>(rdi8) * 4) * 2);
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rdi8)) 
            goto addr_3e2c_41;
        if (rax22 == 0xffffffffffffffff) 
            goto addr_3e2c_41;
        rdi8 = rax22;
        r13d11 = 1;
        goto addr_3adf_36;
        addr_3c88_33:
        esi10 = 1;
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r12_13) = 1;
        *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
        goto addr_3adf_36;
        addr_3b17_34:
        r12_13 = rdi8;
        esi10 = *reinterpret_cast<uint32_t*>(&rdx7);
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
        goto addr_3adf_36;
    }
    if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
        addr_3ef1_46:
        rax23 = fun_24b0();
        rdx7 = rax23;
    } else {
        rax24 = fun_24b0();
        rdx7 = rax24;
    }
    while (1) {
        addr_3e17_48:
        fun_26c0();
        usage();
        addr_3e2c_41:
        rbp25 = num_start_0;
        rax26 = fun_2570(rbp25, "0123456789", rdx7, rcx20);
        rax27 = ximemdup0(rbp25, rax26, rdx7, rcx20);
        rax28 = quote(rax27, rax27);
        if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
            fun_24b0();
        } else {
            fun_24b0();
        }
        rcx20 = rax28;
        fun_26c0();
        fun_23e0(rax27);
        usage();
        addr_3e9c_20:
        addr_3e0d_52:
        rax29 = fun_24b0();
        rdx7 = rax29;
    }
    addr_3b37_10:
    rsi30 = n_frp;
    if (!rsi30) {
        if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
            addr_3fb8_54:
            goto addr_3e0d_52;
        } else {
            rax31 = fun_24b0();
            rdx7 = rax31;
            goto addr_3e17_48;
        }
    } else {
        rdi32 = frp;
        *reinterpret_cast<int32_t*>(&r14_33) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_33) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp34) = 16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp34) + 4) = 0;
        fun_2450(rdi32);
        rbx35 = n_frp;
        r15_36 = frp;
        if (!rbx35) {
            addr_3c00_57:
            if (*reinterpret_cast<unsigned char*>(&v5) & 2) {
                frp = reinterpret_cast<void**>(0);
                rax37 = *reinterpret_cast<void***>(r15_36);
                n_frp = 0;
                n_frp_allocated = 0;
                if (reinterpret_cast<unsigned char>(rax37) > reinterpret_cast<unsigned char>(1)) {
                    rsi38 = rax37 + 0xffffffffffffffff;
                    add_range_pair(1, rsi38, 1, rsi38);
                }
                rbp39 = reinterpret_cast<struct s5*>(r15_36 + 8);
                *reinterpret_cast<int32_t*>(&r12_40) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_40) + 4) = 0;
                if (rbx35 > 1) {
                    do {
                        rdi41 = rbp39->f0 + 1;
                        if (rdi41 != rbp39->f8) {
                            rsi42 = rbp39->f8 - 1;
                            add_range_pair(rdi41, rsi42, rdi41, rsi42);
                        }
                        ++r12_40;
                        rbp39 = reinterpret_cast<struct s5*>(reinterpret_cast<uint64_t>(rbp39) + 16);
                    } while (r12_40 != rbx35);
                }
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s6**>(reinterpret_cast<unsigned char>(r15_36) + (rbx35 << 4) - 8) == -1)) {
                    rdi43 = reinterpret_cast<void**>(&(*reinterpret_cast<struct s6**>(reinterpret_cast<unsigned char>(r15_36) + (rbx35 << 4) - 8))->f1);
                    add_range_pair(rdi43, 0xffffffffffffffff, rdi43, 0xffffffffffffffff);
                    fun_23e0(r15_36, r15_36);
                    r15_36 = frp;
                } else {
                    fun_23e0(r15_36, r15_36);
                    r15_36 = frp;
                }
            }
        } else {
            do {
                ++r14_33;
                if (r14_33 >= rbx35) 
                    goto addr_3c00_57;
                r13_44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp34) - 16);
                r12_45 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp34) + 16);
                do {
                    rsi46 = reinterpret_cast<struct s7*>(reinterpret_cast<unsigned char>(r15_36) + reinterpret_cast<uint64_t>(r13_44));
                    rdi47 = reinterpret_cast<struct s8*>(reinterpret_cast<unsigned char>(r15_36) + reinterpret_cast<uint64_t>(rbp34));
                    if (rdi47->f0 > rsi46->f8) 
                        break;
                    rax48 = rdi47->f8;
                    if (rax48 < rsi46->f8) {
                        rax48 = rsi46->f8;
                    }
                    rsi46->f8 = rax48;
                    fun_26b0();
                    rax49 = n_frp;
                    r15_36 = frp;
                    rbx35 = rax49 - 1;
                    n_frp = rbx35;
                } while (rbx35 > r14_33);
                goto addr_3c00_57;
                rbx35 = n_frp;
                rbp34 = r12_45;
            } while (r14_33 < rbx35);
            goto addr_3bf9_76;
        }
    }
    rax50 = n_frp;
    n_frp = rax50 + 1;
    rax51 = xrealloc(r15_36);
    rdx52 = n_frp;
    frp = rax51;
    rax53 = reinterpret_cast<struct s9*>(reinterpret_cast<unsigned char>(rax51) + (rdx52 << 4) - 16);
    rax53->f8 = -1;
    rax53->f0 = -1;
    return;
    addr_3bf9_76:
    goto addr_3c00_57;
    addr_3e01_15:
    goto addr_3e0d_52;
    addr_3f78_23:
    rax54 = quote(r14_15);
    if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
        fun_24b0();
    } else {
        fun_24b0();
    }
    rcx20 = rax54;
    fun_26c0();
    usage();
    goto addr_3fb8_54;
    addr_3ecf_29:
    if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
        rax55 = fun_24b0();
        rdx7 = rax55;
        goto addr_3e17_48;
    } else {
        rax56 = fun_24b0();
        rdx7 = rax56;
        goto addr_3e17_48;
    }
    addr_3ead_31:
    if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) 
        goto addr_3ef1_46;
    rax57 = fun_24b0();
    rdx7 = rax57;
    goto addr_3e17_48;
}

int64_t file_name = 0;

void fun_3fe3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3ff3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2420(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4003() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2400(rdi1), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_24b0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4093_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_26c0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2420(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4093_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_26c0();
    }
}

void fun_40b3() {
    __asm__("cli ");
}

int32_t fun_2600(void** rdi, void* rsi);

void fun_40c3(void** rdi, void* rsi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2600(rdi, rsi);
        goto 0x2580;
    }
}

int32_t fun_2650(void** rdi);

int64_t fun_2530(int64_t rdi, ...);

int32_t rpl_fflush(void** rdi);

int64_t fun_2490(void** rdi);

int64_t fun_40f3(void** rdi, void* rsi) {
    int32_t eax3;
    int32_t eax4;
    int32_t eax5;
    int64_t rdi6;
    int64_t rax7;
    int32_t eax8;
    int32_t* rax9;
    int32_t r12d10;
    int64_t rax11;

    __asm__("cli ");
    eax3 = fun_2600(rdi, rsi);
    if (eax3 >= 0) {
        eax4 = fun_2650(rdi);
        if (!(eax4 && (eax5 = fun_2600(rdi, rsi), *reinterpret_cast<int32_t*>(&rdi6) = eax5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0, rax7 = fun_2530(rdi6), rax7 == -1) || (eax8 = rpl_fflush(rdi), eax8 == 0))) {
            rax9 = fun_2400(rdi);
            r12d10 = *rax9;
            rax11 = fun_2490(rdi);
            if (r12d10) {
                *rax9 = r12d10;
                *reinterpret_cast<int32_t*>(&rax11) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            }
            return rax11;
        }
    }
    goto fun_2490;
}

void rpl_fseeko(void** rdi);

void fun_4183(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2650(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_41d3(void** rdi, void* rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<unsigned char**>(rdi + 40) != *reinterpret_cast<unsigned char**>(rdi + 32) || *reinterpret_cast<int64_t*>(rdi + 72)))) {
        eax4 = fun_2600(rdi, rsi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2530(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2610(void** rdi);

void** freadptr(void** rdi, void* rsi, void** rdx);

void* memchr2(void** rdi, int64_t rsi, void** rdx, void** rcx);

struct s10 {
    signed char[1] pad1;
    void** f1;
    void** f2;
};

signed char* fun_2670(signed char* rdi, void** rsi);

void fun_25f0(signed char* rdi, void** rsi, void** rdx);

int32_t freadseek(void** rdi, void** rsi, void** rdx);

void* fun_4253(signed char** rdi, void*** rsi, void** rdx, void** rcx, uint32_t r8d, uint32_t r9d, void** a7) {
    void* rsp8;
    void** r12_9;
    signed char** v10;
    void** r14_11;
    void*** v12;
    void** v13;
    void** v14;
    uint32_t v15;
    uint32_t v16;
    void** rax17;
    void** v18;
    signed char* rax19;
    signed char* v20;
    signed char* rax21;
    void** rbx22;
    void* rax23;
    uint32_t ecx24;
    void* r15_25;
    signed char* rbp26;
    void** rax27;
    void** r8_28;
    int64_t rsi29;
    void** v30;
    void* rax31;
    void** rax32;
    uint32_t ecx33;
    uint32_t r13d34;
    struct s10* rax35;
    void** rax36;
    void* rbp37;
    signed char* rax38;
    int32_t eax39;
    void* rdx40;

    __asm__("cli ");
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68);
    r12_9 = *rsi;
    v10 = rdi;
    r14_11 = a7;
    v12 = rsi;
    v13 = rdx;
    v14 = rcx;
    v15 = r8d;
    v16 = r9d;
    rax17 = g28;
    v18 = rax17;
    rax19 = *rdi;
    v20 = rax19;
    if (!rax19) {
        *reinterpret_cast<int32_t*>(&r12_9) = 64;
        *reinterpret_cast<int32_t*>(&r12_9 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rcx) <= reinterpret_cast<unsigned char>(64)) {
            r12_9 = rcx;
        }
        rax21 = fun_2610(r12_9);
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        v20 = rax21;
        if (rax21) 
            goto addr_42aa_5;
    } else {
        addr_42aa_5:
        if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(v13) || (rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_9) - reinterpret_cast<unsigned char>(v13)), *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(rbx22 == 0), !!(*reinterpret_cast<unsigned char*>(&rdx) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r12_9) >= reinterpret_cast<unsigned char>(v14)))))) {
            addr_4410_6:
            *v10 = v20;
            *v12 = r12_9;
            rax23 = reinterpret_cast<void*>(0xffffffffffffffff);
            goto addr_442c_7;
        } else {
            if (v15 == 0xffffffff) {
                v15 = v16;
            } else {
                ecx24 = v16;
                if (ecx24 == 0xffffffff) {
                    ecx24 = v15;
                }
                v16 = ecx24;
            }
            r15_25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) + 80);
            rbp26 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(v20) + reinterpret_cast<unsigned char>(v13));
            do {
                rax27 = freadptr(r14_11, r15_25, rdx);
                r8_28 = rax27;
                if (rax27) {
                    if (v15 == 0xffffffff || (*reinterpret_cast<uint32_t*>(&rdx) = v16, *reinterpret_cast<int32_t*>(&rdx + 4) = 0, *reinterpret_cast<uint32_t*>(&rsi29) = v15, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi29) + 4) = 0, rax31 = memchr2(rax27, rsi29, rdx, v30), r8_28 = rax27, rax31 == 0)) {
                        rax32 = v30 + 1;
                        ecx33 = 0;
                        r13d34 = 0;
                    } else {
                        rax35 = reinterpret_cast<struct s10*>(reinterpret_cast<int64_t>(rax31) - reinterpret_cast<unsigned char>(r8_28));
                        ecx33 = 0;
                        r13d34 = 1;
                        rdx = reinterpret_cast<void**>(&rax35->f1);
                        rax32 = reinterpret_cast<void**>(&rax35->f2);
                        v30 = rdx;
                    }
                } else {
                    rax36 = *reinterpret_cast<void***>(r14_11 + 8);
                    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_11 + 16))) {
                        *reinterpret_cast<uint32_t*>(&rax36) = fun_23d0(r14_11, r14_11);
                        r8_28 = r8_28;
                        ecx33 = *reinterpret_cast<uint32_t*>(&rax36);
                        if (*reinterpret_cast<uint32_t*>(&rax36) == 0xffffffff) 
                            goto addr_4401_20;
                    } else {
                        rdx = rax36 + 1;
                        *reinterpret_cast<void***>(r14_11 + 8) = rdx;
                        ecx33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax36));
                    }
                    v30 = reinterpret_cast<void**>(1);
                    *reinterpret_cast<unsigned char*>(&r13d34) = reinterpret_cast<uint1_t>(v15 == ecx33);
                    *reinterpret_cast<unsigned char*>(&rax36) = reinterpret_cast<uint1_t>(v16 == ecx33);
                    r13d34 = r13d34 | *reinterpret_cast<uint32_t*>(&rax36);
                    *reinterpret_cast<int32_t*>(&rax32) = 2;
                    *reinterpret_cast<int32_t*>(&rax32 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(rbx22) < reinterpret_cast<unsigned char>(rax32) && reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(v14)) {
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_9) + reinterpret_cast<unsigned char>(r12_9));
                    if (reinterpret_cast<unsigned char>(r12_9) <= reinterpret_cast<unsigned char>(63)) {
                        rdx = r12_9 + 64;
                    }
                    rbp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp26) - reinterpret_cast<uint64_t>(v20));
                    if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<uint64_t>(rbp37)) < reinterpret_cast<unsigned char>(rax32)) {
                        rdx = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp37) + reinterpret_cast<unsigned char>(rax32));
                    }
                    if (reinterpret_cast<unsigned char>(r12_9) >= reinterpret_cast<unsigned char>(rdx) || reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(v14)) {
                        rdx = v14;
                    }
                    if (reinterpret_cast<signed char>(rdx) >= reinterpret_cast<signed char>(v13)) 
                        goto addr_44ba_31;
                    rdx = v13 - 0x8000000000000000;
                    if (r12_9 == rdx) 
                        goto addr_4410_6;
                    addr_44ba_31:
                    rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<uint64_t>(rbp37));
                    rax38 = fun_2670(v20, rdx);
                    if (!rax38) 
                        goto addr_4410_6;
                    v20 = rax38;
                    r12_9 = rdx;
                    rbp26 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp37) + reinterpret_cast<uint64_t>(rax38));
                    r8_28 = r8_28;
                    ecx33 = ecx33;
                }
                if (reinterpret_cast<unsigned char>(rbx22) > reinterpret_cast<unsigned char>(1)) {
                    rdx = rbx22 + 0xffffffffffffffff;
                    if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(v30)) {
                        rdx = v30;
                    }
                    if (!r8_28) {
                        *rbp26 = *reinterpret_cast<signed char*>(&ecx33);
                    } else {
                        fun_25f0(rbp26, r8_28, rdx);
                        r8_28 = r8_28;
                        rdx = rdx;
                    }
                    rbp26 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp26) + reinterpret_cast<unsigned char>(rdx));
                    rbx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx22) - reinterpret_cast<unsigned char>(rdx));
                }
                if (!r8_28) 
                    continue;
                eax39 = freadseek(r14_11, v30, rdx);
                if (eax39) 
                    goto addr_4410_6;
            } while (!*reinterpret_cast<unsigned char*>(&r13d34));
            goto addr_4558_44;
        }
    }
    goto addr_4585_46;
    addr_4401_20:
    if (rbp26 != v20) {
        addr_4558_44:
        *rbp26 = 0;
        *v10 = v20;
        *v12 = r12_9;
        rax23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp26) - (reinterpret_cast<unsigned char>(v13) + reinterpret_cast<uint64_t>(v20)));
        if (rax23) {
            addr_442c_7:
            rdx40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
            if (rdx40) {
                fun_24e0();
            } else {
                return rax23;
            }
        } else {
            addr_4585_46:
            rax23 = reinterpret_cast<void*>(0xffffffffffffffff);
            goto addr_442c_7;
        }
    } else {
        goto addr_4410_6;
    }
}

unsigned char* fun_45e3() {
    int32_t r10d1;
    int32_t edx2;
    uint64_t rdx3;
    uint64_t rcx4;
    unsigned char sil5;
    int32_t r9d6;
    int32_t esi7;
    int32_t r8d8;
    int64_t rcx9;
    unsigned char* rdi10;
    uint32_t eax11;
    uint32_t esi12;
    unsigned char sil13;
    uint32_t r10d14;
    uint64_t rcx15;
    uint64_t rax16;
    uint64_t rsi17;
    uint64_t rax18;
    uint64_t r11_19;
    uint64_t rbx20;
    uint64_t rsi21;
    uint64_t rax22;
    unsigned char* rdx23;
    unsigned char* rax24;
    uint32_t ecx25;

    __asm__("cli ");
    r10d1 = edx2;
    rdx3 = rcx4;
    if (sil5 == *reinterpret_cast<unsigned char*>(&r10d1)) {
    }
    r9d6 = esi7;
    r8d8 = r10d1;
    if (rcx9) {
        do {
            if (!(*reinterpret_cast<unsigned char*>(&rdi10) & 7)) 
                break;
            eax11 = *rdi10;
            if (*reinterpret_cast<signed char*>(&eax11) == *reinterpret_cast<signed char*>(&r8d8)) 
                goto addr_46f0_7;
            if (*reinterpret_cast<signed char*>(&eax11) == *reinterpret_cast<signed char*>(&r9d6)) 
                goto addr_46f0_7;
            ++rdi10;
            --rdx3;
        } while (rdx3);
    }
    esi12 = sil13;
    r10d14 = *reinterpret_cast<unsigned char*>(&r10d1);
    rcx15 = reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(esi12 << 8 | esi12)));
    rax16 = reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(r10d14 << 8 | r10d14)));
    rsi17 = rcx15 << 16 | rcx15;
    rax18 = rax16 | rax16 << 16;
    r11_19 = rsi17 << 32 | rsi17;
    rbx20 = rax18 << 32 | rax18;
    if (rdx3 > 7) {
        do {
            rsi21 = *rdi10 ^ r11_19;
            rax22 = *rdi10 ^ rbx20;
            if ((~rax22 & rax22 - 0x101010101010101 | rsi21 - 0x101010101010101 & ~rsi21) & 0x8080808080808080) 
                goto addr_46c6_14;
            rdx3 = rdx3 - 8;
            rdi10 = rdi10 + 8;
        } while (rdx3 > 7);
    }
    if (rdx3) {
        addr_46c6_14:
        rdx23 = reinterpret_cast<unsigned char*>(rdx3 + reinterpret_cast<uint64_t>(rdi10));
        rax24 = rdi10;
    } else {
        goto addr_4710_18;
    }
    do {
        ecx25 = *rax24;
        if (*reinterpret_cast<signed char*>(&ecx25) == *reinterpret_cast<signed char*>(&r9d6)) 
            break;
        if (*reinterpret_cast<signed char*>(&ecx25) == *reinterpret_cast<signed char*>(&r8d8)) 
            goto addr_4712_21;
        ++rax24;
    } while (rax24 != rdx23);
    goto addr_4710_18;
    return rax24;
    addr_4712_21:
    return rax24;
    addr_4710_18:
    *reinterpret_cast<int32_t*>(&rax24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax24) + 4) = 0;
    goto addr_4712_21;
    addr_46f0_7:
    return rdi10;
}

void fun_2710(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s11 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s11* fun_2520();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4723(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s11* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2710("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23f0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2520();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2410(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(void** rdi, int64_t rsi);

void fun_5ec3(void** rdi) {
    void** rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2400(rdi);
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = reinterpret_cast<void**>(0xc260);
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5f03(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc260);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5f23(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc260);
    }
    *rdi = esi;
    return 0xc260;
}

int64_t fun_5f43(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc260);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s12 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5f83(struct s12* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s12*>(0xc260);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s13 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s13* fun_5fa3(struct s13* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s13*>(0xc260);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x276a;
    if (!rdx) 
        goto 0x276a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc260;
}

struct s14 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5fe3(void** rdi, void** rsi, void** rdx, int64_t rcx, struct s14* r8) {
    struct s14* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s14*>(0xc260);
    }
    rax7 = fun_2400(rdi);
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x6016);
    *rax7 = r15d8;
    return rax13;
}

struct s15 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_6063(void** rdi, int64_t rsi, void*** rdx, struct s15* rcx) {
    struct s15* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s15*>(0xc260);
    }
    rax6 = fun_2400(rdi);
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x6091);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x60ec);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_6153() {
    __asm__("cli ");
}

void** gc098 = reinterpret_cast<void**>(96);

int64_t slotvec0 = 0x100;

void fun_6163() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23e0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0xc160) {
        fun_23e0(rdi7);
        gc098 = reinterpret_cast<void**>(0xc160);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc090) {
        fun_23e0(r12_2);
        slotvec = reinterpret_cast<void**>(0xc090);
    }
    nslots = 1;
    return;
}

void fun_6203() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6223() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6233(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6253(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_6273(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2770;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_6303(void** rdi, int32_t esi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2775;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void** fun_6393(int32_t edi, void** rsi) {
    void** rax3;
    struct s1* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x277a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_24e0();
    } else {
        return rax5;
    }
}

void** fun_6423(int32_t edi, void** rsi, int64_t rdx) {
    void** rax4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x277f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_64b3(void** rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5da0]");
    __asm__("movdqa xmm1, [rip+0x5da8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x5d91]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_24e0();
    } else {
        return rax10;
    }
}

void** fun_6553(void** rdi, uint32_t esi) {
    struct s1* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5d00]");
    __asm__("movdqa xmm1, [rip+0x5d08]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x5cf1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24e0();
    } else {
        return rax9;
    }
}

void** fun_65f3(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5c60]");
    __asm__("movdqa xmm1, [rip+0x5c68]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x5c49]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_24e0();
    } else {
        return rax3;
    }
}

void** fun_6683(void** rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5bd0]");
    __asm__("movdqa xmm1, [rip+0x5bd8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x5bc6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_24e0();
    } else {
        return rax4;
    }
}

void** fun_6713(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2784;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_67b3(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5a9a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5a92]");
    __asm__("movdqa xmm2, [rip+0x5a9a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2789;
    if (!rdx) 
        goto 0x2789;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void** fun_6853(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, int64_t r8) {
    void** rcx6;
    struct s1* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x59fa]");
    __asm__("movdqa xmm1, [rip+0x5a02]");
    __asm__("movdqa xmm2, [rip+0x5a0a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x278e;
    if (!rdx) 
        goto 0x278e;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24e0();
    } else {
        return rax9;
    }
}

void** fun_6903(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x594a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5942]");
    __asm__("movdqa xmm2, [rip+0x594a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2793;
    if (!rsi) 
        goto 0x2793;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_69a3(int64_t rdi, int64_t rsi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x58aa]");
    __asm__("movdqa xmm1, [rip+0x58b2]");
    __asm__("movdqa xmm2, [rip+0x58ba]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2798;
    if (!rsi) 
        goto 0x2798;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void fun_6a43() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6a53(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6a73() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6a93(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s16 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_25e0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_6ab3(void** rdi, void** rsi, void** rdx, void** rcx, struct s16* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2720(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2720(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_24b0();
    fun_2720(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_25e0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_24b0();
    fun_2720(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_25e0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_24b0();
        fun_2720(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x9328 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x9328;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6f23() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s17 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6f43(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s17* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s17* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_24e0();
    } else {
        return;
    }
}

void fun_6fe3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_7086_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7090_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_24e0();
    } else {
        return;
    }
    addr_7086_5:
    goto addr_7090_7;
}

void fun_70c3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_25e0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_24b0();
    fun_2690(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_24b0();
    fun_2690(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_24b0();
    goto fun_2690;
}

int64_t fun_2460();

void fun_7163(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2460();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_71a3(void** rdi) {
    signed char* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_71c3(void** rdi) {
    signed char* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_71e3(void** rdi) {
    signed char* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7203(signed char* rdi, void** rsi) {
    signed char* rax3;

    __asm__("cli ");
    rax3 = fun_2670(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7233(signed char* rdi, uint64_t rsi) {
    uint64_t rax3;
    signed char* rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2670(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7263(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2460();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_72a3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7313(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7363(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2460();
            if (rax5) 
                break;
            addr_73ad_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_73ad_5;
        rax8 = fun_2460();
        if (rax8) 
            goto addr_7396_9;
        if (rbx4) 
            goto addr_73ad_5;
        addr_7396_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_73f3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2460();
            if (rax8) 
                break;
            addr_743a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_743a_5;
        rax11 = fun_2460();
        if (rax11) 
            goto addr_7422_9;
        if (!rbx6) 
            goto addr_7422_9;
        if (r12_4) 
            goto addr_743a_5;
        addr_7422_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7483(signed char* rdi, void*** rsi, signed char* rdx, void** rcx, uint64_t r8) {
    signed char* r13_6;
    signed char* rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    signed char* rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_752d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_7540_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_74e0_11:
            if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<int64_t>(rdi7)) 
                goto addr_7506_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<uint64_t>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7554_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_74fd_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_7554_14;
            addr_74fd_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7554_14;
            addr_7506_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2670(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7554_14;
            if (!rbp13) 
                break;
            addr_7554_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_752d_9;
        } else {
            if (!r13_6) 
                goto addr_7540_10;
            goto addr_74e0_11;
        }
    }
}

int64_t fun_25c0();

void fun_7583() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_75b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_75e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7603() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7623(int64_t rdi, void** rsi) {
    signed char* rax3;

    __asm__("cli ");
    rax3 = fun_2610(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

void fun_7663(int64_t rdi, void** rsi) {
    signed char* rax3;

    __asm__("cli ");
    rax3 = fun_2610(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

struct s18 {
    signed char[1] pad1;
    void** f1;
};

void fun_76a3(int64_t rdi, struct s18* rsi) {
    signed char* rax3;

    __asm__("cli ");
    rax3 = fun_2610(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rax3) + reinterpret_cast<uint64_t>(rsi)) = 0;
        goto fun_25f0;
    }
}

void** fun_24d0(void** rdi, ...);

void fun_76e3(void** rdi) {
    void** rax2;
    signed char* rax3;

    __asm__("cli ");
    rax2 = fun_24d0(rdi);
    rax3 = fun_2610(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

void fun_7723() {
    void** rdi1;

    __asm__("cli ");
    fun_24b0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_26c0();
    fun_23f0(rdi1);
}

int64_t fun_2430();

int64_t fun_7763(void** rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t rax7;
    uint32_t ebx8;
    int64_t rax9;
    int32_t* rax10;
    int32_t* rax11;

    __asm__("cli ");
    rax7 = fun_2430();
    ebx8 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax9 = rpl_fclose(rdi, rsi, rdx, rcx, r8, r9);
    if (ebx8) {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            addr_77be_3:
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        } else {
            rax10 = fun_2400(rdi);
            *rax10 = 0;
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            if (rax7) 
                goto addr_77be_3;
            rax11 = fun_2400(rdi);
            *reinterpret_cast<int32_t*>(&rax9) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax11 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    }
    return rax9;
}

struct s19 {
    signed char[8] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    uint64_t f20;
    uint64_t f28;
};

int64_t fun_77d3(struct s19* rdi, int64_t* rsi) {
    int64_t r8_3;
    int64_t rax4;

    __asm__("cli ");
    if (rdi->f28 > rdi->f20 || (r8_3 = rdi->f8, rax4 = rdi->f10 - r8_3, rax4 == 0)) {
        return 0;
    } else {
        *rsi = rax4;
        return r8_3;
    }
}

uint64_t freadahead();

void* fun_26a0(void* rdi, void* rsi, void** rdx, void* rcx, void** r8);

int32_t fun_2560(void** rdi, void* rsi);

int32_t fun_2440(void** rdi, void* rsi, void** rdx, void* rcx, void** r8);

int64_t fun_7803(void** rdi, void* rsi, void** rdx, void* rcx, void** r8) {
    void** rax6;
    void** v7;
    int64_t rax8;
    void** r12_9;
    void* rbp10;
    uint64_t rax11;
    void* rsp12;
    uint64_t rbx13;
    int32_t eax14;
    int64_t rdi15;
    int64_t rax16;
    void* r13_17;
    void* rbx18;
    void* rax19;
    void* rax20;
    void* r13_21;
    void** rax22;
    void* rax23;
    void* v24;
    int32_t eax25;
    void* rdx26;
    int32_t eax27;
    uint32_t eax28;
    int32_t eax29;
    uint32_t eax30;

    __asm__("cli ");
    rax6 = g28;
    v7 = rax6;
    if (!rsi) {
        addr_78b0_2:
        *reinterpret_cast<uint32_t*>(&rax8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    } else {
        r12_9 = rdi;
        rbp10 = rsi;
        rax11 = freadahead();
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x1000 - 40 - 8 + 8);
        rbx13 = rax11;
        if (!rax11) {
            addr_78e0_4:
            eax14 = fun_2600(r12_9, rsi);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            *reinterpret_cast<int32_t*>(&rdi15) = eax14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
            if (eax14 < 0 || (rax16 = fun_2530(rdi15), rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8), rax16 < 0)) {
                r13_17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) + 16);
                do {
                    *reinterpret_cast<int32_t*>(&rbx18) = 0x1000;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx18) + 4) = 0;
                    if (reinterpret_cast<uint64_t>(rbp10) <= 0x1000) {
                        rbx18 = rbp10;
                    }
                    rax19 = fun_26a0(r13_17, 0x1000, 1, rbx18, r12_9);
                    if (reinterpret_cast<uint64_t>(rbx18) > reinterpret_cast<uint64_t>(rax19)) 
                        goto addr_7926_9;
                    rbp10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp10) - reinterpret_cast<uint64_t>(rbx18));
                } while (rbp10);
                goto addr_78b0_2;
            } else {
                rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
                if (!rax20) {
                    goto rpl_fseeko;
                }
            }
        } else {
            r13_21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) + 8);
            do {
                rsi = r13_21;
                rax22 = freadptr(r12_9, rsi, rdx);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                if (rax22 && (rax23 = v24, !!rax23)) {
                    if (reinterpret_cast<uint64_t>(rax23) > reinterpret_cast<uint64_t>(rbp10)) {
                        rax23 = rbp10;
                    }
                    *reinterpret_cast<void***>(r12_9 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_9 + 8)) + reinterpret_cast<uint64_t>(rax23));
                    rbp10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp10) - reinterpret_cast<uint64_t>(rax23));
                    if (!rbp10) 
                        goto addr_78b0_2;
                    rbx13 = rbx13 - reinterpret_cast<uint64_t>(rax23);
                    if (!rbx13) 
                        goto addr_78e0_4;
                }
                eax25 = fun_2560(r12_9, rsi);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                if (eax25 == -1) 
                    goto addr_789d_20;
                rbp10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp10) - 1);
                if (!rbp10) 
                    goto addr_78b0_2;
                --rbx13;
            } while (rbx13);
            goto addr_78e0_4;
        }
    }
    addr_78b2_23:
    rdx26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (!rdx26) {
        return rax8;
    }
    fun_24e0();
    addr_7926_9:
    eax27 = fun_2440(r12_9, 0x1000, 1, rbx18, r12_9);
    eax28 = reinterpret_cast<uint32_t>(-eax27);
    *reinterpret_cast<uint32_t*>(&rax8) = eax28 - (eax28 + reinterpret_cast<uint1_t>(eax28 < eax28 + reinterpret_cast<uint1_t>(!!eax27)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    goto addr_78b2_23;
    addr_789d_20:
    eax29 = fun_2440(r12_9, rsi, rdx, rcx, r8);
    eax30 = reinterpret_cast<uint32_t>(-eax29);
    *reinterpret_cast<uint32_t*>(&rax8) = eax30 - (eax30 + reinterpret_cast<uint1_t>(eax30 < eax30 + reinterpret_cast<uint1_t>(!!eax29)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    goto addr_78b2_23;
}

signed char* fun_2630(int64_t rdi);

signed char* fun_7993() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2630(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2500(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_79d3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2500(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_24e0();
    } else {
        return r12_7;
    }
}

struct s20 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    uint64_t f20;
    uint64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[8] pad88;
    int64_t f58;
};

int64_t fun_7a63(struct s20* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax2) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    if (rdi->f28 <= rdi->f20 && (rax2 = rdi->f10 - rdi->f8, !!(rdi->f0 & 0x100))) {
        rax2 = rax2 + (rdi->f58 - rdi->f48);
    }
    return rax2;
}

int32_t setlocale_null_r();

int64_t fun_7a93() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax3;
    }
}

int64_t fun_7b13(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2680(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_24d0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_25f0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_25f0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7bc3() {
    __asm__("cli ");
    goto fun_2680;
}

void fun_7bd3() {
    __asm__("cli ");
}

void fun_7be7() {
    __asm__("cli ");
    return;
}

void fun_287e() {
    int64_t r15_1;

    if (!r15_1) {
        goto 0x2838;
    }
}

void** optarg = reinterpret_cast<void**>(0);

void fun_2898() {
    void** rdx1;
    uint32_t eax2;

    rdx1 = optarg;
    eax2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx1));
    if (!*reinterpret_cast<unsigned char*>(&eax2) || !*reinterpret_cast<void***>(rdx1 + 1)) {
        delim = *reinterpret_cast<unsigned char*>(&eax2);
        goto 0x2838;
    } else {
        goto 0x2b23;
    }
}

void fun_28c0() {
    complement = 1;
    goto 0x2838;
}

void fun_2910() {
    line_delim = 0;
    goto 0x2838;
}

uint32_t fun_25a0(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, unsigned char* rsi);

int32_t fun_2740(int64_t rdi, unsigned char* rsi);

uint32_t fun_2730(void** rdi, unsigned char* rsi);

void fun_4955() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rax10;
    void** r11_11;
    void** v12;
    int32_t ebp13;
    void** rax14;
    void* r15_15;
    int32_t ebx16;
    void** rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void* rsi30;
    void* v31;
    void** v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rdx44;
    void** rcx45;
    uint32_t edx46;
    unsigned char al47;
    void** v48;
    int64_t v49;
    void** v50;
    void** v51;
    void** rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    void** v61;
    unsigned char v62;
    void* v63;
    void* v64;
    void** v65;
    signed char* v66;
    void** r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void* r14_71;
    void** r13_72;
    unsigned char* rsi73;
    void* v74;
    void** r15_75;
    unsigned char* rdx76;
    void* v77;
    int64_t rax78;
    int64_t rdi79;
    int32_t v80;
    int32_t eax81;
    void* rdi82;
    uint32_t edx83;
    unsigned char v84;
    void* rdi85;
    void* v86;
    uint32_t esi87;
    uint32_t ebp88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    uint32_t eax94;
    void* rdx95;
    void* rcx96;
    void* v97;
    unsigned char** rax98;
    uint1_t zf99;
    int32_t ecx100;
    uint32_t ecx101;
    uint32_t edi102;
    int32_t ecx103;
    uint32_t edi104;
    uint32_t edx105;
    uint32_t edi106;
    uint32_t edx107;
    int64_t rax108;
    uint32_t eax109;
    uint32_t r12d110;
    int64_t rax111;
    int64_t rax112;
    uint32_t r12d113;
    void* v114;
    void* rdx115;
    void* rax116;
    void* v117;
    uint64_t rax118;
    int64_t v119;
    int64_t rax120;
    int64_t rax121;
    int64_t rax122;
    int64_t v123;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_24b0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_24b0();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx17)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24d0(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4c53_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4c53_22; else 
                            goto addr_504d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_510d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5460_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4c50_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4c50_30; else 
                                goto addr_5479_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24d0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5460_28;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_25a0(rdi39, v26, v28, rcx45);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5460_28; else 
                            goto addr_4afc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_55c0_39:
                    *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5440_40:
                        if (r11_27 == 1) {
                            addr_4fcd_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_5588_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx45);
                                goto addr_4c07_44;
                            }
                        } else {
                            goto addr_5450_46;
                        }
                    } else {
                        addr_55cf_47:
                        rax24 = v48;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_4fcd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4c53_22:
                                if (v49 != 1) {
                                    addr_51a9_52:
                                    v50 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_24d0(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_51f4_54;
                                    }
                                } else {
                                    goto addr_4c60_56;
                                }
                            } else {
                                addr_4c05_57:
                                ebp36 = 0;
                                goto addr_4c07_44;
                            }
                        } else {
                            addr_5434_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_55cf_47; else 
                                goto addr_543e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4fcd_41;
                        if (v49 == 1) 
                            goto addr_4c60_56; else 
                            goto addr_51a9_52;
                    }
                }
                addr_4cc1_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<void**>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4b58_63:
                    if (!1 && (edx54 = *reinterpret_cast<uint32_t*>(&rcx45), *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<unsigned char*>(&rcx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_4b7d_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_4e80_65;
                    } else {
                        addr_4ce9_66:
                        ++r9_37;
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5538_67;
                    }
                } else {
                    goto addr_4ce0_69;
                }
                addr_4b91_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                ++r9_37;
                addr_4bdc_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5538_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_4bdc_81;
                }
                addr_4ce0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_4b7d_64; else 
                    goto addr_4ce9_66;
                addr_4c07_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_4cbf_91;
                if (v22) 
                    goto addr_4c1f_93;
                addr_4cbf_91:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4cc1_62;
                addr_51f4_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx45 = r12_67;
                    r13_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_597b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_59eb_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72)) + 1);
                        rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = *rdx76 - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_57ef_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    *reinterpret_cast<int32_t*>(&rdi79) = v80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi79) + 4) = 0;
                    eax81 = fun_2740(rdi79, rsi73);
                    if (!eax81) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2730(r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx83 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx83) & reinterpret_cast<unsigned char>(v32));
                addr_52ee_109:
                if (reinterpret_cast<uint64_t>(rdi82) <= 1) {
                    addr_4cac_110:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        edx83 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_52f8_112;
                    }
                } else {
                    addr_52f8_112:
                    v84 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi85 = v86;
                    esi87 = 0;
                    ebp88 = reinterpret_cast<unsigned char>(v22);
                    rcx45 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi82) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_53c9_114;
                }
                addr_4cb8_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_4cbf_91;
                while (1) {
                    addr_53c9_114:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        *reinterpret_cast<unsigned char*>(&esi87) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax89 = esi87;
                        if (*reinterpret_cast<signed char*>(&ebp88)) 
                            goto addr_58d7_117;
                        eax90 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax90) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax90) & *reinterpret_cast<unsigned char*>(&esi87));
                        if (*reinterpret_cast<unsigned char*>(&eax90)) 
                            goto addr_5336_119;
                    } else {
                        eax57 = (esi87 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                            goto addr_58e5_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_53b7_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_53b7_128;
                        }
                    }
                    addr_5365_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax91 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax91) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax91) >> 6);
                        eax92 = eax91 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax92);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax93 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax93) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax93) >> 3);
                        eax94 = (eax93 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax94);
                    }
                    ++r9_37;
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                        break;
                    esi87 = edx83;
                    addr_53b7_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi85) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_5336_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax90;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_5365_134;
                }
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4bdc_81;
                addr_58e5_125:
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_5538_67;
                addr_597b_96:
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx83 = reinterpret_cast<unsigned char>(v32);
                goto addr_52ee_109;
                addr_59eb_98:
                r11_27 = v65;
                rdi82 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx95 = rdi82;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx96 = v97;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx96) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx95 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx95) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx95));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi82 = rdx95;
                }
                edx83 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_52ee_109;
                addr_4c60_56:
                rax98 = fun_2750(rdi39, rdi39);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi82) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf99 = reinterpret_cast<uint1_t>(((*rax98 + reinterpret_cast<unsigned char>(rax24) * 2)[1] & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf99);
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf99) & reinterpret_cast<unsigned char>(v32));
                goto addr_4cac_110;
                addr_543e_59:
                goto addr_5440_40;
                addr_510d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4c53_22;
                *reinterpret_cast<uint32_t*>(&rcx45) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx45));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_4cb8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4c05_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4c53_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_5152_160;
                if (!v22) 
                    goto addr_5527_162; else 
                    goto addr_5733_163;
                addr_5152_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_5527_162:
                    ++r9_37;
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    goto addr_5538_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!v32) 
                        goto addr_4ffb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                addr_4e63_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_4b91_70; else 
                    goto addr_4e77_169;
                addr_4ffb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4b58_63;
                goto addr_4ce0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5434_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_556f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4c50_30;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx101 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_4b48_178; else 
                        goto addr_54f2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5434_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4c53_22;
                }
                addr_556f_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4c50_30:
                    r8d42 = 0;
                    goto addr_4c53_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4cc1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        goto addr_5588_42;
                    }
                }
                addr_4b48_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx101;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4b58_63;
                addr_54f2_179:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5450_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4cc1_62;
                } else {
                    addr_5502_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4c53_22;
                }
                edi102 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi102))) 
                    goto addr_5cb2_188;
                if (v28) 
                    goto addr_5527_162;
                addr_5cb2_188:
                *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                goto addr_4e63_168;
                addr_4afc_37:
                if (v22) 
                    goto addr_5af3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4b13_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_55c0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_564b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4c53_22;
                    ecx103 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx103));
                    ecx101 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_4b48_178; else 
                        goto addr_5627_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5434_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4c53_22;
                }
                addr_564b_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4c53_22;
                }
                addr_5627_199:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5450_46;
                goto addr_5502_186;
                addr_4b13_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4c53_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4c53_22; else 
                    goto addr_4b24_206;
            }
            edi104 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx45) = reinterpret_cast<uint1_t>(r15_15 == 0);
            edx105 = edi104 & *reinterpret_cast<uint32_t*>(&rax24);
            if (*reinterpret_cast<unsigned char*>(&rcx45) & *reinterpret_cast<unsigned char*>(&edx105)) 
                goto addr_5bfe_208;
            edi106 = edi104 ^ 1;
            edx107 = edi106;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi106));
            if (!rax24) 
                goto addr_5a84_210;
            if (1) 
                goto addr_5a82_212;
            if (!v29) 
                goto addr_56be_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax108 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v49 = rax108;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_5bf1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4e80_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax109 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax109)) 
            goto addr_4c3b_219; else 
            goto addr_4e9a_220;
        addr_4c1f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax89 = reinterpret_cast<unsigned char>(v32);
        addr_4c33_221:
        if (*reinterpret_cast<signed char*>(&eax89)) 
            goto addr_4e9a_220; else 
            goto addr_4c3b_219;
        addr_57ef_103:
        r12d110 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d110)) {
            addr_4e9a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax111 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax111;
        } else {
            addr_580d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax112 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax112;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_5c80_225:
        v31 = r13_34;
        addr_56e6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_58d7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4c33_221;
        addr_5733_163:
        eax89 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4c33_221;
        addr_4e77_169:
        goto addr_4e80_65;
        addr_5bfe_208:
        r14_35 = r12_21;
        r12d113 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d113)) 
            goto addr_4e9a_220;
        goto addr_580d_222;
        addr_5a84_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx107) && (*reinterpret_cast<uint32_t*>(&rcx45) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx45)))) {
            rsi30 = v114;
            rdx115 = r15_15;
            rax116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx115)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi30) + reinterpret_cast<uint64_t>(rdx115)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                rdx115 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx115) + 1);
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax116) + reinterpret_cast<uint64_t>(rdx115));
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx45));
            r15_15 = rdx115;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v117) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax118 = reinterpret_cast<uint64_t>(v119 - reinterpret_cast<unsigned char>(g28));
        if (!rax118) 
            goto addr_5ade_236;
        fun_24e0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5c80_225;
        addr_5a82_212:
        edx107 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5a84_210;
        addr_56be_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx107 = 0;
            goto addr_5a84_210;
        } else {
            goto addr_56e6_226;
        }
        addr_5bf1_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_504d_24:
    *reinterpret_cast<uint32_t*>(&rax120) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax120) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8dec + rax120 * 4) + 0x8dec;
    addr_5479_32:
    *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax121) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8eec + rax121 * 4) + 0x8eec;
    addr_5af3_190:
    addr_4c3b_219:
    goto 0x4920;
    addr_4b24_206:
    *reinterpret_cast<uint32_t*>(&rax122) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x8cec + rax122 * 4) + 0x8cec;
    addr_5ade_236:
    goto v123;
}

void fun_4b40() {
}

void fun_4cf8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x49f2;
}

void fun_4d51() {
    goto 0x49f2;
}

void fun_4e3e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4cc1;
    }
    if (v2) 
        goto 0x5733;
    if (!r10_3) 
        goto addr_589e_5;
    if (!v4) 
        goto addr_576e_7;
    addr_589e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_576e_7:
    goto 0x4b74;
}

void fun_4e5c() {
}

void fun_4f07() {
    signed char v1;

    if (v1) {
        goto 0x4e8f;
    } else {
        goto 0x4bca;
    }
}

void fun_4f21() {
    signed char v1;

    if (!v1) 
        goto 0x4f1a; else 
        goto "???";
}

void fun_4f48() {
    goto 0x4e63;
}

void fun_4fc8() {
}

void fun_4fe0() {
}

void fun_500f() {
    goto 0x4e63;
}

void fun_5061() {
    goto 0x4ff0;
}

void fun_5090() {
    goto 0x4ff0;
}

void fun_50c3() {
    goto 0x4ff0;
}

void fun_5490() {
    goto 0x4b48;
}

void fun_578e() {
    signed char v1;

    if (v1) 
        goto 0x5733;
    goto 0x4b74;
}

void fun_5835() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4b74;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4b58;
        goto 0x4b74;
    }
}

void fun_5c52() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4ec0;
    } else {
        goto 0x49f2;
    }
}

void fun_6b88() {
    fun_24b0();
}

void fun_28d0() {
    void** rdx1;
    void** rax2;

    rdx1 = optarg;
    *reinterpret_cast<int32_t*>(&rax2) = 1;
    *reinterpret_cast<int32_t*>(&rax2 + 4) = 0;
    if (*reinterpret_cast<void***>(rdx1)) {
        rax2 = fun_24d0(rdx1);
        rdx1 = rdx1;
    }
    output_delimiter_length = rax2;
    output_delimiter_string = rdx1;
    goto 0x2838;
}

void fun_2920() {
    suppress_non_delimited = 1;
    goto 0x2838;
}

void fun_4d7e() {
    goto 0x49f2;
}

void fun_4f54() {
    goto 0x4f0c;
}

void fun_501b() {
    goto 0x4b48;
}

void fun_506d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4ff0;
    goto 0x4c1f;
}

void fun_509f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4ffb;
        goto 0x4a20;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4e9a;
        goto 0x4c3b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5838;
    if (r10_8 > r15_9) 
        goto addr_4f85_9;
    addr_4f8a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5843;
    goto 0x4b74;
    addr_4f85_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4f8a_10;
}

void fun_50d2() {
    goto 0x4c07;
}

void fun_54a0() {
    goto 0x4c07;
}

void fun_5c3f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4d5c;
    } else {
        goto 0x4ec0;
    }
}

void fun_6c40() {
}

void fun_50dc() {
    goto 0x5077;
}

void fun_54aa() {
    goto 0x4fcd;
}

void fun_6ca0() {
    fun_24b0();
    goto fun_2720;
}

void fun_4dad() {
    goto 0x49f2;
}

void fun_50e8() {
    goto 0x5077;
}

void fun_54b7() {
    goto 0x501e;
}

void fun_6ce0() {
    fun_24b0();
    goto fun_2720;
}

void fun_4dda() {
    goto 0x49f2;
}

void fun_50f4() {
    goto 0x4ff0;
}

void fun_6d20() {
    fun_24b0();
    goto fun_2720;
}

void fun_4dfc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5790;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4cc1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4cc1;
    }
    if (v11) 
        goto 0x5af3;
    if (r10_12 > r15_13) 
        goto addr_5b43_8;
    addr_5b48_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5881;
    addr_5b43_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5b48_9;
}

struct s21 {
    signed char[24] pad24;
    int64_t f18;
};

struct s22 {
    signed char[16] pad16;
    void** f10;
};

struct s23 {
    signed char[8] pad8;
    void** f8;
};

void fun_6d70() {
    int64_t r15_1;
    struct s21* rbx2;
    void** r14_3;
    struct s22* rbx4;
    void** r13_5;
    struct s23* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_24b0();
    fun_2720(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6d92, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6dc8() {
    fun_24b0();
    goto 0x6d99;
}

struct s24 {
    signed char[32] pad32;
    int64_t f20;
};

struct s25 {
    signed char[24] pad24;
    int64_t f18;
};

struct s26 {
    signed char[16] pad16;
    void** f10;
};

struct s27 {
    signed char[8] pad8;
    void** f8;
};

struct s28 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6e00() {
    int64_t rcx1;
    struct s24* rbx2;
    int64_t r15_3;
    struct s25* rbx4;
    void** r14_5;
    struct s26* rbx6;
    void** r13_7;
    struct s27* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s28* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_24b0();
    fun_2720(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6e34, __return_address(), rcx1);
    goto v15;
}

void fun_6e78() {
    fun_24b0();
    goto 0x6e3b;
}
