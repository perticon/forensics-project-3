cut_bytes (FILE *stream)
{
  uintmax_t byte_idx;	/* Number of bytes in the line so far. */
  /* Whether to begin printing delimiters between ranges for the current line.
     Set after we've begun printing data corresponding to the first range.  */
  bool print_delimiter;

  byte_idx = 0;
  print_delimiter = false;
  current_rp = frp;
  while (true)
    {
      int c;		/* Each character from the file. */

      c = getc (stream);

      if (c == line_delim)
        {
          putchar (c);
          byte_idx = 0;
          print_delimiter = false;
          current_rp = frp;
        }
      else if (c == EOF)
        {
          if (byte_idx > 0)
            putchar (line_delim);
          break;
        }
      else
        {
          next_item (&byte_idx);
          if (print_kth (byte_idx))
            {
              if (output_delimiter_string != output_delimiter_default)
                {
                  if (print_delimiter && is_range_start_index (byte_idx))
                    {
                      fwrite (output_delimiter_string, sizeof (char),
                              output_delimiter_length, stdout);
                    }
                  print_delimiter = true;
                }

              putchar (c);
            }
        }
    }
}