void cut_bytes(_IO_FILE *param_1)

{
  char *pcVar1;
  bool bVar2;
  byte bVar3;
  byte *pbVar4;
  ulong uVar5;
  uint uVar6;
  uint uVar7;
  
  bVar2 = false;
  current_rp = frp;
  uVar5 = 0;
LAB_00103230:
  do {
    pbVar4 = (byte *)param_1->_IO_read_ptr;
    if (param_1->_IO_read_end < pbVar4 || (byte *)param_1->_IO_read_end == pbVar4)
    goto LAB_001032cd;
    while( true ) {
      uVar6 = (uint)line_delim;
      param_1->_IO_read_ptr = (char *)(pbVar4 + 1);
      uVar7 = (uint)*pbVar4;
      if (*pbVar4 == uVar6) break;
      while( true ) {
        uVar5 = uVar5 + 1;
        if (current_rp[1] <= uVar5 && uVar5 != current_rp[1]) {
          current_rp = current_rp + 2;
        }
        if (uVar5 < *current_rp) goto LAB_00103230;
        if (output_delimiter_string != &output_delimiter_default) {
          bVar2 = (bool)(bVar2 & *current_rp == uVar5);
          if (bVar2) {
            fwrite_unlocked(output_delimiter_string,1,output_delimiter_length,stdout);
          }
          else {
            bVar2 = true;
          }
        }
        pcVar1 = stdout->_IO_write_ptr;
        if (stdout->_IO_write_end <= pcVar1) {
          __overflow(stdout,uVar7 & 0xff);
          goto LAB_00103230;
        }
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar7;
        pbVar4 = (byte *)param_1->_IO_read_ptr;
        if (pbVar4 <= param_1->_IO_read_end && (byte *)param_1->_IO_read_end != pbVar4) break;
LAB_001032cd:
        uVar6 = __uflow(param_1);
        bVar3 = line_delim;
        if (line_delim == uVar6) goto LAB_00103320;
        uVar7 = uVar6;
        if (uVar6 == 0xffffffff) {
          if (uVar5 != 0) {
            pbVar4 = (byte *)stdout->_IO_write_ptr;
            if (stdout->_IO_write_end <= pbVar4) {
              __overflow(stdout,(uint)line_delim);
              return;
            }
            stdout->_IO_write_ptr = (char *)(pbVar4 + 1);
            *pbVar4 = bVar3;
          }
          return;
        }
      }
    }
LAB_00103320:
    pcVar1 = stdout->_IO_write_ptr;
    if (pcVar1 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar1 + 1;
      *pcVar1 = (char)uVar6;
    }
    else {
      __overflow(stdout,uVar6);
    }
    current_rp = frp;
    uVar5 = 0;
    bVar2 = false;
  } while( true );
}