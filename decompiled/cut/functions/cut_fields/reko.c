void cut_fields(FILE * rdi, struct Eq_742 * fs)
{
	ptr64 fp;
	uint64 rdi_32;
	current_rp = frp;
	ptr64 rsp_104 = fp - 0x38;
	byte * rax_27 = rdi->qw0008;
	if (rdi->qw0010 > rax_27)
	{
		rdi->qw0008 = rax_27 + 1;
		rdi_32 = (uint64) *rax_27;
	}
	else
	{
		int32 eax_30 = fn00000000000023D0(rdi);
		rdi_32 = (uint64) eax_30;
		if (eax_30 == ~0x00)
			return;
	}
	fn0000000000002640(rdi, (word32) rdi_32);
	Eq_17 rax_46 = current_rp;
	uint64 r15_170 = 0x01;
	word24 eax_24_8_65 = SLICE(rax_46, word24, 8);
	uint64 r13_1190 = 0x00;
	int32 r14d_173 = 0x00;
	ui8 r12b_54 = (int8) (*rax_46 > 0x01) ^ g_bC10A;
	while (true)
	{
l0000000000002CE0:
		byte al_62 = (int8) (r15_170 == 0x01) & r12b_54;
		word32 ebp_162 = SEQ(eax_24_8_65, al_62);
		Eq_17 rax_126 = current_rp;
		if (al_62 == 0x00)
			break;
		struct Eq_776 * rsp_73 = rsp_104 - 0x08;
		uint64 r9_75 = (uint64) g_bC010;
		uint64 r8_82 = (uint64) g_bC108;
		rsp_73->ptrFFFFFFF8 = rdi;
		Eq_48 rax_93 = getndelim2(~0x00, 0x00, &field_1_bufsize, &field_1_buffer, (word32) r8_82, (word32) r9_75, (word32) r13_1190, fs, rsp_73->ptrFFFFFFF8);
		rsp_104 = (char *) &rsp_73->ptrFFFFFFF8 + 16;
		if (rax_93 < 0x00)
		{
			fn00000000000023E0(field_1_buffer);
			field_1_buffer.u0 = 0x00;
			if ((rdi->dw0000 & 0x30) != 0x00)
				return;
			xalloc_die();
		}
		if (rax_93 == 0x00)
			fn0000000000002540("cut_fields", 0x013B, "src/cut.c", "n_bytes != 0");
		Eq_17 rdi_119 = field_1_buffer;
		word64 rax_122 = CONVERT(Mem85[rdi_119 - 1 + rax_93:byte], byte, uint64);
		eax_24_8_65 = SLICE(rax_122, word24, 8);
		if (g_bC108 == (byte) rax_122)
		{
			rax_126 = current_rp;
			if (*rax_126 > 0x01)
			{
l0000000000002FA1:
				if (*((word64) rax_126 + 8) <= 0x01)
				{
					rax_126 = (word64) rax_126 + 16;
					current_rp = rax_126;
					r15_170 = 0x02;
					r14d_173 = 0x00;
				}
				else
				{
					r15_170 = 0x02;
					r14d_173 = 0x00;
				}
				break;
			}
			fn0000000000002660(stdout, rax_93 - 1, 0x01, rdi_119);
			if (g_bC108 == g_bC010)
			{
				uint64 rdi_145;
				byte * rax_139 = rdi->qw0008;
				if (rax_139 < rdi->qw0010)
				{
					rdi->qw0008 = rax_139 + 1;
					rdi_145 = (uint64) *rax_139;
					goto l0000000000003152;
				}
				int32 eax_143 = fn00000000000023D0(rdi);
				rdi_145 = (uint64) eax_143;
				if (eax_143 == ~0x00)
				{
					rax_126 = current_rp;
					goto l0000000000002FA1;
				}
l0000000000003152:
				fn0000000000002640(rdi, (word32) rdi_145);
			}
			rax_126 = current_rp;
			r13_1190 = (uint64) ebp_162;
			goto l0000000000002FA1;
		}
		if (g_bC10A == 0x00)
		{
			eax_24_8_65 = SLICE((uint64) fn0000000000002660(stdout, rax_93, 0x01, rdi_119), word24, 8);
			uint64 r14_201 = (uint64) g_bC010;
			r14d_173 = (word32) r14_201;
			byte r14b_218 = (byte) r14_201;
			if (CONVERT(Mem85[Mem85[0x000000000000C118<p64>:word64] - 1 + rax_93:byte], byte, int32) != r14d_173)
			{
				FILE * rdi_206 = stdout;
				byte * rdx_207 = rdi_206->qw0028;
				if (rdx_207 < (&rdi_206->qw0028)[1])
				{
					rdi_206->qw0028 = rdx_207 + 1;
					*rdx_207 = r14b_218;
				}
				else
				{
					eax_24_8_65 = SLICE(fn0000000000002510(r14d_173, rdi_206), word24, 8);
					rsp_104 = (char *) &rsp_73->ptrFFFFFFF8 + 24;
					r14d_173 = (word32) g_bC010;
				}
			}
			r15_170 = 0x01;
		}
		else
		{
			r15_170 = 0x01;
			r14d_173 = 0x00;
		}
	}
	uint64 rcx_1193;
	uint64 rdx_1222;
	uint64 rsi_1231;
	uip64 rdi_1199;
	uint64 rdx_1195;
	uint64 rax_1194;
	uint64 rdx_1202;
	uint64 rdi_1219;
	uint64 rsi_1216;
	uint64 rsi_1209;
	uint64 rdi_1236;
	uint64 rdx_1239;
	uint64 rsi_1240;
	int32 ebp_250;
	int32 eax_524;
	int32 ecx_501;
	int32 ecx_348;
	byte al_367;
	byte dl_256;
	word56 rdi_56_8_915;
	byte r13b_270 = (byte) r13_1190;
	if (*rax_126 <= r15_170)
	{
		if (r13b_270 != 0x00)
			fn0000000000002660(stdout, output_delimiter_length, 0x01, output_delimiter_string);
		while (true)
		{
			byte * rax_285 = rdi->qw0008;
			if (rax_285 >= rdi->qw0010)
				ebp_250 = fn00000000000023D0(rdi);
			else
			{
				rdi->qw0008 = rax_285 + 1;
				ebp_250 = (word32) *rax_285;
			}
			rcx_1193 = (uint64) g_bC108;
			int32 ecx_301 = (word32) rcx_1193;
			rdx_1239 = (uint64) g_bC010;
			rax_1194 = (uint64) ecx_301;
			byte bpl_646 = (byte) ebp_250;
			byte cl_311 = (byte) rcx_1193;
			dl_256 = (byte) rdx_1239;
			al_367 = (byte) rax_1194;
			rsi_1240 = (uint64) ecx_301;
			if (ecx_301 == ebp_250)
				break;
			rdi_1236 = (uint64) dl_256;
			if ((word32) rdi_1236 == ebp_250)
			{
				r13_1190 = 0x01;
				goto l0000000000002ED6;
			}
			if (ebp_250 == ~0x00)
			{
				r13b_270 = 0x01;
				goto l0000000000002F16;
			}
			FILE * rdi_642 = stdout;
			byte * rax_643 = rdi_642->qw0028;
			if (rax_643 < (&rdi_642->qw0028)[1])
			{
				rdi_642->qw0028 = rax_643 + 1;
				*rax_643 = bpl_646;
			}
			else
			{
				fn0000000000002510((word32) bpl_646, rdi_642);
				rsp_104 += 0x08;
			}
			r14d_173 = ebp_250;
		}
		if (cl_311 != dl_256)
		{
			r14d_173 = ebp_250;
			r13_1190 = 0x01;
l0000000000002FCF:
			Eq_17 rax_391 = current_rp;
			eax_24_8_65 = SLICE(rax_391, word24, 8);
			++r15_170;
			if (r15_170 > *((word64) rax_391 + 8))
			{
				current_rp = (word64) rax_391 + 16;
				eax_24_8_65 = SLICE((word64) rax_391 + 16, word24, 8);
			}
			goto l0000000000002CE0;
		}
		r13_1190 = 0x01;
	}
	else
	{
		while (true)
		{
			byte * rax_229 = rdi->qw0008;
			if (rax_229 >= rdi->qw0010)
				ebp_250 = fn00000000000023D0(rdi);
			else
			{
				rdi->qw0008 = rax_229 + 1;
				ebp_250 = (word32) *rax_229;
			}
			rcx_1193 = (uint64) g_bC108;
			int32 ecx_245 = (word32) rcx_1193;
			rdx_1239 = (uint64) g_bC010;
			rax_1194 = (uint64) ecx_245;
			byte cl_255 = (byte) rcx_1193;
			dl_256 = (byte) rdx_1239;
			al_367 = (byte) rax_1194;
			rsi_1240 = (uint64) ecx_245;
			if (ecx_245 == ebp_250)
				break;
			rdi_1236 = (uint64) dl_256;
			if ((word32) rdi_1236 == ebp_250)
			{
l0000000000002ED6:
				rdi_56_8_915 = SLICE(rdi_1236, word56, 8);
				r13b_270 = (byte) r13_1190;
				ecx_348 = (word32) rcx_1193;
				if (dl_256 == al_367)
				{
					uint64 rax_373 = (uint64) dl_256;
					int32 eax_374 = (word32) rax_373;
					eax_24_8_65 = SLICE(rax_373, word24, 8);
					rsi_1209 = (uint64) eax_374;
					rdx_1222 = (uint64) eax_374;
					if (eax_374 == ebp_250)
						goto l000000000000305F;
					goto l0000000000002F00;
				}
				else
				{
					rsi_1231 = rsi_1240;
					rdi_1219 = rdi_1236;
					if (ecx_348 != ebp_250)
						goto l0000000000002EE6;
					goto l0000000000003048;
				}
			}
			if (ebp_250 == ~0x00)
			{
l0000000000002F16:
				rsi_1209 = (uint64) (word32) rax_1194;
				ebp_250 = (word32) (byte) rdx_1239;
				rdi_1219 = rdi_1236;
				rdx_1222 = rdx_1239;
				goto l0000000000002F1B;
			}
			r14d_173 = ebp_250;
		}
		if (cl_255 != dl_256)
		{
			r14d_173 = ebp_250;
			goto l0000000000002FCF;
		}
	}
	r13b_270 = (byte) r13_1190;
	byte * rax_321 = rdi->qw0008;
	if (rax_321 < rdi->qw0010)
	{
		rdi->qw0008 = rax_321 + 1;
		rdi_1219 = (uint64) *rax_321;
	}
	else
	{
		int32 eax_325 = fn00000000000023D0(rdi);
		rdi_1219 = (uint64) eax_325;
		if (eax_325 == ~0x00)
		{
			ebp_250 = (word32) g_bC010;
			rsi_1209 = (uint64) g_bC108;
			rdx_1222 = (uint64) ebp_250;
			goto l0000000000002F1B;
		}
	}
	fn0000000000002640(rdi, (word32) rdi_1219);
	ecx_348 = (word32) g_bC108;
	rsi_1231 = (uint64) ecx_348;
	if (ecx_348 == ebp_250)
	{
l0000000000003048:
		r14d_173 = ecx_348;
		goto l0000000000002FCF;
	}
l0000000000002EE6:
	uint64 rax_409 = (uint64) g_bC010;
	int32 eax_410 = (word32) rax_409;
	rdi_56_8_915 = SLICE(rdi_1219, word56, 8);
	r13b_270 = (byte) r13_1190;
	eax_24_8_65 = SLICE(rax_409, word24, 8);
	rdx_1222 = (uint64) eax_410;
	rsi_1209 = rsi_1231;
	if (eax_410 == ebp_250)
	{
l000000000000305F:
		eax_524 = r14d_173;
		r14d_173 = ebp_250;
		rdx_1195 = rdx_1222;
		if (r13b_270 != 0x00)
			goto l0000000000002F43;
		if (g_bC10A == 0x01 && r15_170 == 0x01)
		{
l0000000000002F6C:
			Eq_17 rax_633 = frp;
			current_rp = rax_633;
			eax_24_8_65 = SLICE(rax_633, word24, 8);
			r15_170 = 0x01;
			r13_1190 = 0x00;
			goto l0000000000002CE0;
		}
		ecx_501 = ebp_250;
		goto l0000000000003083;
	}
	if (ebp_250 != ~0x00)
	{
l0000000000002F00:
		r14d_173 = ebp_250;
		goto l0000000000002CE0;
	}
	ebp_250 = eax_410;
	rsi_1209 = rsi_1231;
l0000000000002F1B:
	rdi_56_8_915 = SLICE(rdi_1219, word56, 8);
	if (r13b_270 != 0x00)
	{
		eax_524 = r14d_173;
		r14d_173 = ~0x00;
		rdi_1199 = 0x00;
		rdx_1202 = rdx_1222;
		rsi_1216 = rsi_1209;
		goto l0000000000002F2F;
	}
	if (g_bC10A == 0x01 && r15_170 == 0x01)
		return;
	ecx_501 = ebp_250;
	eax_524 = r14d_173;
	ebp_250 = ~0x00;
l0000000000003083:
	r14d_173 = ebp_250;
	ebp_250 = ecx_501;
	rdi_1199 = SEQ(rdi_56_8_915, (int8) (ecx_501 == ebp_250));
	rdx_1202 = rdx_1222;
	rsi_1216 = rsi_1209;
l0000000000002F2F:
	byte dil_563 = (byte) rdi_1199;
	rdx_1195 = rdx_1202;
	if (((int8) (ebp_250 != eax_524) | (int8) ((byte) rdx_1202 == (byte) rsi_1216)) == 0x00)
	{
		rdx_1195 = rdx_1202;
		if (dil_563 == 0x00)
			goto l0000000000002F62;
	}
l0000000000002F43:
	FILE * rdi_573 = stdout;
	byte dl_604 = (byte) rdx_1195;
	byte * rax_574 = rdi_573->qw0028;
	if (rax_574 < (&rdi_573->qw0028)[1])
	{
		rdi_573->qw0028 = rax_574 + 1;
		*rax_574 = dl_604;
	}
	else
	{
		fn0000000000002510(ebp_250, rdi_573);
		rsp_104 += 0x08;
	}
l0000000000002F62:
	if (r14d_173 == ~0x00)
		return;
	goto l0000000000002F6C;
}