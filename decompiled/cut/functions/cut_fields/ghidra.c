void cut_fields(_IO_FILE *param_1)

{
  byte *pbVar1;
  char *pcVar2;
  byte bVar3;
  uint uVar4;
  uint uVar5;
  uint uVar6;
  size_t __n;
  uint uVar7;
  byte bVar8;
  uint uVar9;
  byte bVar10;
  byte bVar11;
  byte bVar12;
  ulong uVar13;
  bool bVar14;
  undefined8 uVar15;
  
  current_rp = frp;
  pbVar1 = (byte *)param_1->_IO_read_ptr;
  if (param_1->_IO_read_end < pbVar1 || (byte *)param_1->_IO_read_end == pbVar1) {
    uVar4 = __uflow(param_1);
    if (uVar4 == 0xffffffff) {
      return;
    }
  }
  else {
    param_1->_IO_read_ptr = (char *)(pbVar1 + 1);
    uVar4 = (uint)*pbVar1;
  }
  uVar13 = 1;
  uVar15 = 0x102cba;
  ungetc(uVar4,param_1);
  uVar4 = 0;
  bVar10 = 1 < *current_rp ^ suppress_non_delimited;
  bVar11 = 0;
LAB_00102ce0:
  do {
    bVar3 = uVar13 == 1 & bVar10;
    bVar12 = bVar11;
    if (bVar3 != 0) {
      __n = getndelim2(&field_1_buffer,&field_1_bufsize,0,0xffffffffffffffff,delim,line_delim,
                       param_1,uVar15);
      if ((long)__n < 0) {
        free(field_1_buffer);
        field_1_buffer = (void *)0x0;
        if ((*(byte *)&param_1->_flags & 0x30) != 0) {
          field_1_buffer = (void *)0x0;
          return;
        }
                    /* WARNING: Subroutine does not return */
        xalloc_die();
      }
      if (__n == 0) {
                    /* WARNING: Subroutine does not return */
        __assert_fail("n_bytes != 0","src/cut.c",0x13b,"cut_fields");
      }
      if (delim != *(byte *)((long)field_1_buffer + (__n - 1))) {
        if (suppress_non_delimited == 0) {
          uVar15 = 0x102e8b;
          fwrite_unlocked(field_1_buffer,1,__n,stdout);
          bVar12 = line_delim;
          uVar4 = (uint)line_delim;
          if ((int)*(char *)((long)field_1_buffer + (__n - 1)) != uVar4) {
            pbVar1 = (byte *)stdout->_IO_write_ptr;
            if (pbVar1 < stdout->_IO_write_end) {
              stdout->_IO_write_ptr = (char *)(pbVar1 + 1);
              *pbVar1 = bVar12;
            }
            else {
              uVar15 = 0x1031d3;
              __overflow(stdout,uVar4);
              uVar4 = (uint)line_delim;
            }
          }
          uVar13 = 1;
        }
        else {
          uVar13 = 1;
          uVar4 = 0;
        }
        goto LAB_00102ce0;
      }
      if (*current_rp < 2) {
        uVar15 = 0x1030f4;
        fwrite_unlocked(field_1_buffer,1,__n - 1,stdout);
        bVar12 = bVar3;
        if (delim == line_delim) {
          pbVar1 = (byte *)param_1->_IO_read_ptr;
          if (pbVar1 < param_1->_IO_read_end) {
            param_1->_IO_read_ptr = (char *)(pbVar1 + 1);
            uVar4 = (uint)*pbVar1;
          }
          else {
            uVar15 = 0x103190;
            uVar4 = __uflow(param_1);
            bVar12 = bVar11;
            if (uVar4 == 0xffffffff) goto LAB_00102fa1;
          }
          uVar15 = 0x10315a;
          ungetc(uVar4,param_1);
          bVar12 = bVar3;
        }
      }
LAB_00102fa1:
      if (current_rp[1] < 2) {
        current_rp = current_rp + 2;
        uVar13 = 2;
        uVar4 = 0;
      }
      else {
        uVar13 = 2;
        uVar4 = 0;
      }
    }
    if (*current_rp < uVar13 || *current_rp == uVar13) {
      uVar6 = uVar4;
      if (bVar12 != 0) {
        uVar15 = 0x102d2a;
        fwrite_unlocked(output_delimiter_string,1,output_delimiter_length,stdout);
      }
LAB_00102d88:
      pbVar1 = (byte *)param_1->_IO_read_ptr;
      if (pbVar1 < param_1->_IO_read_end) {
        param_1->_IO_read_ptr = (char *)(pbVar1 + 1);
        uVar4 = (uint)*pbVar1;
      }
      else {
        uVar15 = 0x102d9a;
        uVar4 = __uflow(param_1);
      }
      uVar7 = (uint)delim;
      uVar9 = (uint)line_delim;
      if (uVar7 == uVar4) {
        if (delim != line_delim) {
          bVar12 = 1;
          uVar7 = uVar4;
          goto LAB_00102fcf;
        }
        bVar12 = 1;
LAB_00103016:
        pbVar1 = (byte *)param_1->_IO_read_ptr;
        if (param_1->_IO_read_end <= pbVar1) {
          uVar15 = 0x1031ab;
          uVar9 = __uflow(param_1);
          if (uVar9 != 0xffffffff) goto LAB_0010302f;
          uVar5 = (uint)line_delim;
          bVar3 = delim;
          bVar8 = line_delim;
          goto LAB_00102f1b;
        }
        param_1->_IO_read_ptr = (char *)(pbVar1 + 1);
        uVar9 = (uint)*pbVar1;
LAB_0010302f:
        uVar15 = 0x103037;
        ungetc(uVar9,param_1);
        uVar7 = (uint)delim;
        if (uVar7 == uVar4) {
LAB_00102fcf:
          uVar4 = uVar7;
          uVar13 = uVar13 + 1;
          bVar11 = bVar12;
          if (current_rp[1] <= uVar13 && uVar13 != current_rp[1]) {
            current_rp = current_rp + 2;
          }
        }
        else {
LAB_00102ee6:
          bVar3 = (byte)uVar7;
          uVar5 = (uint)line_delim;
          uVar9 = uVar7;
          if (uVar5 == uVar4) goto LAB_0010305f;
          bVar11 = bVar12;
          bVar8 = line_delim;
          if (uVar4 == 0xffffffff) goto LAB_00102f1b;
        }
      }
      else {
        if (uVar9 != uVar4) {
          if (uVar4 != 0xffffffff) break;
          bVar12 = 1;
          uVar5 = uVar9;
          goto LAB_00102f16;
        }
        bVar12 = 1;
LAB_00102ed6:
        if ((char)uVar9 != (char)uVar7) {
          if (uVar7 == uVar4) goto LAB_00102fcf;
          goto LAB_00102ee6;
        }
        bVar11 = bVar12;
        uVar5 = uVar9;
        if (uVar9 == uVar4) {
LAB_0010305f:
          bVar3 = (byte)uVar9;
          bVar8 = (byte)uVar5;
          uVar9 = uVar4;
          if (bVar12 != 0) goto LAB_00102f43;
          uVar5 = uVar4;
          if ((suppress_non_delimited != 1) || (uVar13 != 1)) goto LAB_00103083;
          goto LAB_00102f6c;
        }
      }
    }
    else {
      do {
        uVar6 = uVar4;
        pbVar1 = (byte *)param_1->_IO_read_ptr;
        if (pbVar1 < param_1->_IO_read_end) {
          param_1->_IO_read_ptr = (char *)(pbVar1 + 1);
          uVar4 = (uint)*pbVar1;
        }
        else {
          uVar15 = 0x102dee;
          uVar4 = __uflow(param_1);
        }
        uVar7 = (uint)delim;
        uVar9 = (uint)line_delim;
        if (uVar7 == uVar4) {
          uVar7 = uVar4;
          if (delim == line_delim) goto LAB_00103016;
          goto LAB_00102fcf;
        }
        if (uVar9 == uVar4) goto LAB_00102ed6;
        uVar5 = uVar9;
      } while (uVar4 != 0xffffffff);
LAB_00102f16:
      bVar3 = delim;
      bVar8 = (byte)uVar5;
LAB_00102f1b:
      if (bVar12 == 0) {
        if ((suppress_non_delimited == 1) && (uVar13 == 1)) {
          return;
        }
        uVar4 = 0xffffffff;
LAB_00103083:
        bVar14 = uVar5 == uVar4;
        uVar9 = uVar4;
      }
      else {
        bVar14 = false;
        uVar9 = 0xffffffff;
      }
      uVar4 = uVar5;
      if ((uVar4 != uVar6 || bVar8 == bVar3) || (bVar14)) {
LAB_00102f43:
        pbVar1 = (byte *)stdout->_IO_write_ptr;
        if (pbVar1 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = (char *)(pbVar1 + 1);
          *pbVar1 = bVar8;
        }
        else {
          uVar15 = 0x103138;
          __overflow(stdout,uVar4);
        }
      }
      uVar4 = uVar9;
      if (uVar4 == 0xffffffff) {
        return;
      }
LAB_00102f6c:
      current_rp = frp;
      uVar13 = 1;
      bVar11 = 0;
    }
  } while( true );
  pcVar2 = stdout->_IO_write_ptr;
  uVar6 = uVar4;
  if (pcVar2 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar2 + 1;
    *pcVar2 = (char)uVar4;
  }
  else {
    uVar15 = 0x102e01;
    __overflow(stdout,uVar4 & 0xff);
  }
  goto LAB_00102d88;
}