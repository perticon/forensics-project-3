void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002720(fn00000000000024B0(0x05, "Try '%s --help' for more information.\n", 0x00), 0x01, stderr);
		goto l000000000000352E;
	}
	fn0000000000002690(fn00000000000024B0(0x05, "Usage: %s OPTION... [FILE]...\n", 0x00), 0x01);
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "Print selected parts of lines from each FILE to standard output.\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "  -b, --bytes=LIST        select only these bytes\n  -c, --characters=LIST   select only these characters\n  -d, --delimiter=DELIM   use DELIM instead of TAB for field delimiter\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "  -f, --fields=LIST       select only these fields;  also print any line\n                            that contains no delimiter character, unless\n                            the -s option is specified\n  -n                      (ignored)\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "      --complement        complement the set of selected bytes, characters\n                            or fields\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "  -s, --only-delimited    do not print lines not containing delimiters\n      --output-delimiter=STRING  use STRING as the output delimiter\n                            the default is to use the input delimiter\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "  -z, --zero-terminated    line delimiter is NUL, not newline\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "      --help        display this help and exit\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "      --version     output version information and exit\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "\nUse one, and only one of -b, -c or -f.  Each LIST is made up of one\nrange, or many ranges separated by commas.  Selected input is written\nin the same order that it is read, and is written exactly once.\n", 0x00));
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "Each range is one of:\n\n  N     N'th byte, character or field, counted from 1\n  N-    from N'th byte, character or field, to end of line\n  N-M   from N'th to M'th (included) byte, character or field\n  -M    from first to M'th (included) byte, character or field\n", 0x00));
	struct Eq_1727 * rbx_256 = fp - 0xB8 + 16;
	do
	{
		char * rsi_258 = rbx_256->qw0000;
		++rbx_256;
	} while (rsi_258 != null && fn00000000000025D0(rsi_258, "cut") != 0x00);
	ptr64 r13_271 = rbx_256->qw0008;
	if (r13_271 != 0x00)
	{
		fn0000000000002690(fn00000000000024B0(0x05, "\n%s online help: <%s>\n", 0x00), 0x01);
		Eq_17 rax_358 = fn0000000000002680(null, 0x05);
		if (rax_358 == 0x00 || fn0000000000002410(0x03, "en_", rax_358) == 0x00)
			goto l0000000000003836;
	}
	else
	{
		fn0000000000002690(fn00000000000024B0(0x05, "\n%s online help: <%s>\n", 0x00), 0x01);
		Eq_17 rax_300 = fn0000000000002680(null, 0x05);
		if (rax_300 == 0x00 || fn0000000000002410(0x03, "en_", rax_300) == 0x00)
		{
			fn0000000000002690(fn00000000000024B0(0x05, "Full documentation <%s%s>\n", 0x00), 0x01);
l0000000000003873:
			fn0000000000002690(fn00000000000024B0(0x05, "or available locally via: info '(coreutils) %s%s'\n", 0x00), 0x01);
l000000000000352E:
			fn0000000000002700(edi);
		}
		r13_271 = 0x801B;
	}
	fn00000000000025B0(stdout, fn00000000000024B0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", 0x00));
l0000000000003836:
	fn0000000000002690(fn00000000000024B0(0x05, "Full documentation <%s%s>\n", 0x00), 0x01);
	goto l0000000000003873;
}