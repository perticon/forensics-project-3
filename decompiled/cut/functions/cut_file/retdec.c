bool cut_file(char * file, void (*cut_stream)(struct _IO_FILE *)) {
    int64_t v1 = (int64_t)file;
    int32_t v2 = (int32_t)v1 % 256 - 45; // 0x33c1
    int32_t v3 = v2; // 0x33c6
    if (v2 == 0) {
        // 0x33c8
        v3 = (int32_t)*(char *)(v1 + 1);
    }
    int64_t v4 = function_2400(); // 0x33cc
    int64_t v5; // 0x33b0
    if (v3 != 0) {
        int64_t v6 = function_26e0(); // 0x348a
        v5 = v6;
        if (v6 == 0) {
            // 0x349b
            quotearg_n_style_colon();
            function_26c0();
            // 0x342b
            return false;
        }
    } else {
        // 0x33dc
        *(char *)&have_read_stdin = 1;
        v5 = (int64_t)g21;
    }
    struct _IO_FILE * v7 = (struct _IO_FILE *)v5;
    fadvise(v7, 2);
    int32_t * v8 = (int32_t *)v4; // 0x33fc
    int32_t v9 = *v8; // 0x33fc
    char v10 = *(char *)v5; // 0x3402
    if ((char)v5 != 45) {
        goto lab_0x3438;
    } else {
        // 0x3414
        if (*(char *)(v1 + 1) != 0) {
            goto lab_0x3438;
        } else {
            // 0x341c
            function_2470();
            goto lab_0x3421;
        }
    }
  lab_0x3438:
    // 0x3438
    if (rpl_fclose(v7) == -1) {
        // 0x3442
        if (*v8 == 0) {
            // 0x342b
            return true;
        }
        // 0x3450
        quotearg_n_style_colon();
        function_26c0();
        return false;
    }
    goto lab_0x3421;
  lab_0x3421:;
    bool v11 = (v10 & 32) == 0 ? ((int32_t)&g31 ^ (int32_t)&g31) != 0 : v9 != 0; // 0x3429
    if (v11) {
        // 0x3450
        quotearg_n_style_colon();
        function_26c0();
        return false;
    }
    // 0x342b
    return true;
}