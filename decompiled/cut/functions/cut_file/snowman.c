uint32_t cut_file(void** rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9) {
    uint32_t eax7;
    uint32_t ebp8;
    int32_t* rax9;
    void** rax10;
    void** rbp11;
    uint32_t eax12;
    int32_t r14d13;
    int64_t rax14;

    eax7 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 45);
    ebp8 = eax7;
    if (!eax7) {
        ebp8 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1));
    }
    rax9 = fun_2400(rdi);
    if (ebp8) {
        rax10 = fun_26e0(rdi, "r");
        rbp11 = rax10;
        if (!rax10) {
            quotearg_n_style_colon();
            fun_26c0();
            eax12 = 0;
            goto addr_342b_6;
        }
    } else {
        have_read_stdin = 1;
        rbp11 = stdin;
    }
    fadvise(rbp11, 2);
    rsi(rbp11, 2);
    r14d13 = *rax9;
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp11)) & 32)) {
        r14d13 = 0;
    }
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45) || *reinterpret_cast<void***>(rdi + 1)) {
        rax14 = rpl_fclose(rbp11, 2, rdx, rcx, r8, r9);
        if (*reinterpret_cast<int32_t*>(&rax14) == -1) {
            eax12 = 1;
            if (!*rax9) 
                goto addr_342b_6; else 
                goto addr_3450_13;
        }
    } else {
        fun_2470(rbp11, 2);
    }
    eax12 = 1;
    if (r14d13) {
        addr_3450_13:
        quotearg_n_style_colon();
        fun_26c0();
        return 0;
    } else {
        addr_342b_6:
        return eax12;
    }
}