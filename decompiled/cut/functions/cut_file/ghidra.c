undefined8 cut_file(byte *param_1,code *param_2)

{
  uint uVar1;
  int iVar2;
  int *piVar3;
  undefined8 uVar4;
  FILE *__stream;
  int iVar5;
  
  uVar1 = *param_1 - 0x2d;
  if (uVar1 == 0) {
    uVar1 = (uint)param_1[1];
  }
  piVar3 = __errno_location();
  if (uVar1 == 0) {
    have_read_stdin = 1;
    __stream = stdin;
  }
  else {
    __stream = fopen((char *)param_1,"r");
    if (__stream == (FILE *)0x0) {
      uVar4 = quotearg_n_style_colon(0,3,param_1);
      error(0,*piVar3,"%s",uVar4);
      return 0;
    }
  }
  fadvise(__stream,2);
  (*param_2)(__stream);
  iVar5 = *piVar3;
  if ((*(byte *)&__stream->_flags & 0x20) == 0) {
    iVar5 = 0;
  }
  if ((*param_1 == 0x2d) && (param_1[1] == 0)) {
    clearerr_unlocked(__stream);
  }
  else {
    iVar2 = rpl_fclose();
    if (iVar2 == -1) {
      iVar5 = *piVar3;
    }
  }
  if (iVar5 == 0) {
    return 1;
  }
  uVar4 = quotearg_n_style_colon(0,3,param_1);
  error(0,iVar5,"%s",uVar4);
  return 0;
}