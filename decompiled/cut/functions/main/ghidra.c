byte main(int param_1,undefined8 *param_2)

{
  bool bVar1;
  bool bVar2;
  byte bVar3;
  byte bVar4;
  int iVar5;
  size_t sVar6;
  int *piVar7;
  undefined8 uVar8;
  char *pcVar9;
  char *pcVar10;
  code *pcVar11;
  char *pcVar12;
  bool bVar13;
  
  pcVar10 = "b:c:d:f:nsz";
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  suppress_non_delimited = '\0';
  delim = '\0';
  have_read_stdin = '\0';
  bVar1 = false;
  bVar2 = false;
  pcVar12 = (char *)0x0;
switchD_0010287b_caseD_6e:
  iVar5 = getopt_long(param_1,param_2,"b:c:d:f:nsz",longopts,0);
  pcVar9 = optarg;
  if (iVar5 == -1) {
LAB_0010298b:
    pcVar9 = "you must specify a list of bytes, characters, or fields";
    if (pcVar12 == (char *)0x0) goto LAB_00102b23;
    if (bVar1) {
      pcVar9 = "an input delimiter may be specified only when operating on fields";
      if ((bVar2) ||
         (pcVar9 = "suppressing non-delimited lines makes sense\n\tonly when operating on fields",
         suppress_non_delimited != '\0')) goto LAB_00102b23;
      pcVar11 = cut_bytes;
      set_fields(pcVar12,(uint)complement * 2 | 4,5);
      delim = '\t';
    }
    else {
      set_fields(pcVar12,(uint)complement * 2,5);
      if (!bVar2) {
        delim = '\t';
      }
      pcVar11 = cut_fields;
    }
    if (output_delimiter_string == (char *)0x0) {
      pcVar11 = cut_bytes;
      output_delimiter_length = 1;
      output_delimiter_string = &output_delimiter_default;
      output_delimiter_default = delim;
      if (!bVar1) {
        pcVar11 = cut_fields;
      }
    }
    pcVar10 = (char *)pcVar11;
    if (optind != param_1) {
      bVar4 = 1;
      for (; optind < param_1; optind = optind + 1) {
        bVar3 = cut_file(param_2[optind],pcVar11);
        bVar4 = bVar4 & bVar3;
      }
      goto LAB_00102a4c;
    }
    goto LAB_00102b42;
  }
  if (iVar5 < 0x82) {
    if (0x61 < iVar5) goto code_r0x00102874;
    if (iVar5 == -0x83) {
      version_etc(stdout,&DAT_0010801b,"GNU coreutils",Version,"David M. Ihnat","David MacKenzie",
                  "Jim Meyering",0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar5 == -0x82) {
      usage(0);
      goto LAB_0010298b;
    }
  }
  goto switchD_0010287b_caseD_65;
code_r0x00102874:
  switch(iVar5) {
  case 0x62:
  case 99:
    bVar1 = true;
  case 0x66:
    bVar13 = pcVar12 == (char *)0x0;
    pcVar12 = optarg;
    if (bVar13) goto switchD_0010287b_caseD_6e;
    pcVar9 = "only one list may be specified";
    break;
  case 100:
    if ((*optarg == '\0') || (optarg[1] == '\0')) {
      bVar2 = true;
      delim = *optarg;
      goto switchD_0010287b_caseD_6e;
    }
    pcVar9 = "the delimiter must be a single character";
    break;
  default:
    goto switchD_0010287b_caseD_65;
  case 0x6e:
    goto switchD_0010287b_caseD_6e;
  case 0x73:
    suppress_non_delimited = '\x01';
    goto switchD_0010287b_caseD_6e;
  case 0x7a:
    line_delim = 0;
    goto switchD_0010287b_caseD_6e;
  case 0x80:
    sVar6 = 1;
    if (*optarg != '\0') {
      sVar6 = strlen(optarg);
    }
    output_delimiter_string = pcVar9;
    output_delimiter_length = sVar6;
    goto switchD_0010287b_caseD_6e;
  case 0x81:
    complement = 1;
    goto switchD_0010287b_caseD_6e;
  }
LAB_00102b23:
  uVar8 = dcgettext(0,pcVar9,5);
  error(0,0,uVar8);
switchD_0010287b_caseD_65:
  usage(1);
LAB_00102b42:
  bVar4 = cut_file(&DAT_00108112,pcVar10);
LAB_00102a4c:
  if ((have_read_stdin != '\0') && (iVar5 = rpl_fclose(stdin), iVar5 == -1)) {
    piVar7 = __errno_location();
    bVar4 = 0;
    error(0,*piVar7,&DAT_00108112);
  }
  return bVar4 ^ 1;
}