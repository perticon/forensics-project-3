main (int argc, char **argv)
{
  int optc;
  bool ok;
  bool delim_specified = false;
  bool byte_mode = false;
  char *spec_list_string = NULL;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  atexit (close_stdout);

  /* By default, all non-delimited lines are printed.  */
  suppress_non_delimited = false;

  delim = '\0';
  have_read_stdin = false;

  while ((optc = getopt_long (argc, argv, "b:c:d:f:nsz", longopts, NULL)) != -1)
    {
      switch (optc)
        {
        case 'b':
        case 'c':
          /* Build the byte list. */
          byte_mode = true;
          FALLTHROUGH;
        case 'f':
          /* Build the field list. */
          if (spec_list_string)
            FATAL_ERROR (_("only one list may be specified"));
          spec_list_string = optarg;
          break;

        case 'd':
          /* New delimiter. */
          /* Interpret -d '' to mean 'use the NUL byte as the delimiter.'  */
          if (optarg[0] != '\0' && optarg[1] != '\0')
            FATAL_ERROR (_("the delimiter must be a single character"));
          delim = optarg[0];
          delim_specified = true;
          break;

        case OUTPUT_DELIMITER_OPTION:
          /* Interpret --output-delimiter='' to mean
             'use the NUL byte as the delimiter.'  */
          output_delimiter_length = (optarg[0] == '\0'
                                     ? 1 : strlen (optarg));
          output_delimiter_string = optarg;
          break;

        case 'n':
          break;

        case 's':
          suppress_non_delimited = true;
          break;

        case 'z':
          line_delim = '\0';
          break;

        case COMPLEMENT_OPTION:
          complement = true;
          break;

        case_GETOPT_HELP_CHAR;

        case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);

        default:
          usage (EXIT_FAILURE);
        }
    }

  if (!spec_list_string)
    FATAL_ERROR (_("you must specify a list of bytes, characters, or fields"));

  if (byte_mode)
    {
      if (delim_specified)
        FATAL_ERROR (_("an input delimiter may be specified only\
 when operating on fields"));

      if (suppress_non_delimited)
        FATAL_ERROR (_("suppressing non-delimited lines makes sense\n\
\tonly when operating on fields"));
    }

  set_fields (spec_list_string,
              ((byte_mode ? SETFLD_ERRMSG_USE_POS : 0)
               | (complement ? SETFLD_COMPLEMENT : 0)));

  if (!delim_specified)
    delim = '\t';

  if (output_delimiter_string == NULL)
    {
      output_delimiter_default[0] = delim;
      output_delimiter_string = output_delimiter_default;
      output_delimiter_length = 1;
    }

  void (*cut_stream) (FILE *) = byte_mode ? cut_bytes : cut_fields;
  if (optind == argc)
    ok = cut_file ("-", cut_stream);
  else
    for (ok = true; optind < argc; optind++)
      ok &= cut_file (argv[optind], cut_stream);


  if (have_read_stdin && fclose (stdin) == EOF)
    {
      error (0, errno, "-");
      ok = false;
    }

  return ok ? EXIT_SUCCESS : EXIT_FAILURE;
}