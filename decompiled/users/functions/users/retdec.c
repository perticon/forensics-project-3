int64_t users(int64_t a1, int32_t a2) {
    int64_t v1 = __readfsqword(40); // 0x28e3
    struct utmpx * v2; // bp-72, 0x28d0
    int64_t v3; // bp-80, 0x28d0
    if (read_utmp((char *)a1, &v3, &v2, a2) != 0) {
        // 0x2a5f
        quotearg_n_style_colon();
        function_2390();
        return function_25f0();
    }
    char * v4 = xnmalloc(v3, 8); // 0x2921
    if (v3 == 0) {
        // 0x2a42
        function_23e0();
    } else {
        int64_t v5 = (int64_t)v4; // 0x2921
        int64_t v6 = v3; // 0x2935
        int64_t v7 = 0;
        int64_t v8 = (int64_t)v2;
        int64_t v9 = v7; // 0x293c
        char * v10; // 0x2a23
        if (*(char *)(v8 + 44) != 0) {
            // 0x293e
            v9 = v7;
            if (*(int16_t *)v8 == 7) {
                // 0x2a20
                v10 = extract_trimmed_name((struct utmpx *)v8);
                *(int64_t *)(8 * v7 + v5) = (int64_t)v10;
                v9 = v7 + 1;
            }
        }
        // 0x2948
        v6--;
        int64_t v11 = v9;
        int64_t v12 = v8 + 384; // 0x2953
        while (v6 != 0) {
            // 0x2938
            v7 = v11;
            v8 = v12;
            v9 = v7;
            if (*(char *)(v8 + 44) != 0) {
                // 0x293e
                v9 = v7;
                if (*(int16_t *)v8 == 7) {
                    // 0x2a20
                    v10 = extract_trimmed_name((struct utmpx *)v8);
                    *(int64_t *)(8 * v7 + v5) = (int64_t)v10;
                    v9 = v7 + 1;
                }
            }
            // 0x2948
            v6--;
            v11 = v9;
            v12 = v8 + 384;
        }
        // 0x2955
        function_23e0();
        if (v11 != 0) {
            uint64_t v13 = 0;
            function_24f0();
            int64_t v14 = (int64_t)g18; // 0x29a3
            int64_t * v15 = (int64_t *)(v14 + 40); // 0x29b0
            uint64_t v16 = *v15; // 0x29b0
            if (v16 >= *(int64_t *)(v14 + 48)) {
                // 0x2a38
                function_24a0();
            } else {
                // 0x29ba
                *v15 = v16 + 1;
                *(char *)v16 = v13 < v11 - 1 ? 32 : 10;
            }
            int64_t v17 = v13 + 1; // 0x29c5
            while (v17 != v11) {
                // 0x2980
                v13 = v17;
                function_24f0();
                v14 = (int64_t)g18;
                v15 = (int64_t *)(v14 + 40);
                v16 = *v15;
                if (v16 >= *(int64_t *)(v14 + 48)) {
                    // 0x2a38
                    function_24a0();
                } else {
                    // 0x29ba
                    *v15 = v16 + 1;
                    *(char *)v16 = v13 < v11 - 1 ? 32 : 10;
                }
                // 0x29c5
                v17 = v13 + 1;
            }
            int64_t v18 = v5; // 0x29d5
            v18 += 8;
            function_2370();
            while (8 * v17 + v5 != v18) {
                // 0x29d8
                v18 += 8;
                function_2370();
            }
        }
    }
    // 0x29ea
    function_2370();
    function_2370();
    int64_t result = v1 - __readfsqword(40); // 0x2a01
    if (result == 0) {
        // 0x2a0c
        return result;
    }
    // 0x2a5a
    function_2470();
    // 0x2a5f
    quotearg_n_style_colon();
    function_2390();
    return function_25f0();
}