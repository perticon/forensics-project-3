void users(undefined8 param_1,undefined4 param_2)

{
  FILE *pFVar1;
  undefined **ppuVar2;
  int iVar3;
  int iVar4;
  void **__base;
  void *pvVar5;
  undefined8 uVar6;
  int *piVar7;
  undefined8 uVar8;
  char *pcVar9;
  char *pcVar10;
  short *psVar11;
  ulong uVar12;
  undefined *puVar13;
  void **ppvVar14;
  size_t __nmemb;
  long in_FS_OFFSET;
  undefined *puStack272;
  char *pcStack264;
  char *apcStack256 [5];
  char *pcStack216;
  char *pcStack208;
  char *pcStack200;
  char *pcStack192;
  char *pcStack184;
  undefined8 uStack176;
  undefined8 uStack168;
  undefined8 uStack152;
  long local_50;
  short *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar3 = read_utmp(param_1,&local_50,&local_48,param_2);
  psVar11 = local_48;
  if (iVar3 == 0) {
    __base = (void **)xnmalloc(local_50);
    if (local_50 == 0) {
      qsort(__base,0,8,userid_compare);
    }
    else {
      __nmemb = 0;
      do {
        local_50 = local_50 + -1;
        if ((*(char *)(psVar11 + 0x16) != '\0') && (*psVar11 == 7)) {
          pvVar5 = (void *)extract_trimmed_name(psVar11);
          __base[__nmemb] = pvVar5;
          __nmemb = __nmemb + 1;
        }
        psVar11 = psVar11 + 0xc0;
      } while (local_50 != 0);
      qsort(__base,__nmemb,8,userid_compare);
      if (__nmemb != 0) {
        uVar12 = 0;
        do {
          fputs_unlocked((char *)__base[uVar12],stdout);
          pcVar9 = stdout->_IO_write_ptr;
          if (pcVar9 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar9 + 1;
            *pcVar9 = (-(uVar12 < __nmemb - 1) & 0x16U) + 10;
          }
          else {
            __overflow(stdout,(-(uint)(uVar12 < __nmemb - 1) & 0x16) + 10);
          }
          uVar12 = uVar12 + 1;
        } while (uVar12 != __nmemb);
        ppvVar14 = __base;
        do {
          pvVar5 = *ppvVar14;
          ppvVar14 = ppvVar14 + 1;
          free(pvVar5);
        } while (__base + uVar12 != ppvVar14);
      }
    }
    free(__base);
    free(local_48);
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return;
    }
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  uVar6 = quotearg_n_style_colon(0,3,param_1);
  piVar7 = __errno_location();
  iVar3 = 1;
  error(1,*piVar7,"%s",uVar6);
  uVar6 = program_name;
  uStack152 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  if (iVar3 != 0) {
    uVar8 = dcgettext(0,"Try \'%s --help\' for more information.\n",5);
    __fprintf_chk(stderr,1,uVar8,uVar6);
    goto LAB_00102aee;
  }
  uVar8 = dcgettext(0,"Usage: %s [OPTION]... [FILE]\n",5);
  __printf_chk(1,uVar8,uVar6);
  uVar6 = dcgettext(0,
                    "Output who is currently logged in according to FILE.\nIf FILE is not specified, use %s.  %s as FILE is common.\n\n"
                    ,5);
  __printf_chk(1,uVar6,"/var/run/utmp","/var/log/wtmp");
  pFVar1 = stdout;
  pcVar9 = (char *)dcgettext(0,"      --help        display this help and exit\n",5);
  fputs_unlocked(pcVar9,pFVar1);
  pFVar1 = stdout;
  pcVar9 = (char *)dcgettext(0,"      --version     output version information and exit\n",5);
  fputs_unlocked(pcVar9,pFVar1);
  uStack176 = 0;
  puStack272 = &DAT_00107044;
  pcStack264 = "test invocation";
  apcStack256[0] = "coreutils";
  apcStack256[1] = "Multi-call invocation";
  apcStack256[4] = "sha256sum";
  apcStack256[2] = "sha224sum";
  pcStack208 = "sha384sum";
  apcStack256[3] = "sha2 utilities";
  pcStack216 = "sha2 utilities";
  pcStack200 = "sha2 utilities";
  pcStack192 = "sha512sum";
  pcStack184 = "sha2 utilities";
  uStack168 = 0;
  ppuVar2 = &puStack272;
  do {
    puVar13 = (undefined *)ppuVar2;
    if (*(char **)(puVar13 + 0x10) == (char *)0x0) break;
    iVar4 = strcmp("users",*(char **)(puVar13 + 0x10));
    ppuVar2 = (undefined **)(puVar13 + 0x10);
  } while (iVar4 != 0);
  pcVar9 = *(char **)(puVar13 + 0x18);
  if (pcVar9 == (char *)0x0) {
    uVar6 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar6,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar9 = setlocale(5,(char *)0x0);
    if (pcVar9 != (char *)0x0) {
      iVar4 = strncmp(pcVar9,"en_",3);
      if (iVar4 != 0) {
        pcVar9 = "users";
        goto LAB_00102da8;
      }
    }
    uVar6 = dcgettext(0,"Full documentation <%s%s>\n",5);
    pcVar9 = "users";
    __printf_chk(1,uVar6,"https://www.gnu.org/software/coreutils/","users");
    pcVar10 = " invocation";
  }
  else {
    uVar6 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar6,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar10 = setlocale(5,(char *)0x0);
    if (pcVar10 != (char *)0x0) {
      iVar4 = strncmp(pcVar10,"en_",3);
      if (iVar4 != 0) {
LAB_00102da8:
        pFVar1 = stdout;
        pcVar10 = (char *)dcgettext(0,
                                    "Report any translation bugs to <https://translationproject.org/team/>\n"
                                    ,5);
        fputs_unlocked(pcVar10,pFVar1);
      }
    }
    uVar6 = dcgettext(0,"Full documentation <%s%s>\n",5);
    __printf_chk(1,uVar6,"https://www.gnu.org/software/coreutils/","users");
    pcVar10 = " invocation";
    if (pcVar9 != "users") {
      pcVar10 = "";
    }
  }
  uVar6 = dcgettext(0,"or available locally via: info \'(coreutils) %s%s\'\n",5);
  __printf_chk(1,uVar6,pcVar9,pcVar10);
LAB_00102aee:
                    /* WARNING: Subroutine does not return */
  exit(iVar3);
}