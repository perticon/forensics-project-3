int64_t users (int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    void * ptr;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    ecx = esi;
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    eax = read_utmp (rdi, rsp + 8, rsp + 0x10);
    if (eax != 0) {
        goto label_2;
    }
    r12 = *((rsp + 8));
    rbx = *((rsp + 0x10));
    rbp = r12 - 1;
    rax = xnmalloc (r12, 8);
    r14 = rax;
    if (r12 == 0) {
        goto label_3;
    }
    r15d = 0;
    do {
        if (*((rbx + 0x2c)) != 0) {
            if (*(rbx) == 7) {
                goto label_4;
            }
        }
label_0:
        rbx += 0x180;
        rbp--;
    } while (rbp >= 0);
    rcx = dbg_userid_compare;
    edx = 8;
    rsi = r15;
    rdi = r14;
    qsort ();
    if (r15 == 0) {
        goto label_5;
    }
    r13 = r15 - 1;
    ebx = 0;
    do {
        rdi = *((r14 + rbx*8));
        rsi = stdout;
        r12d -= r12d;
        r12d &= 0x16;
        r12d += 0xa;
        ebp -= ebp;
        fputs_unlocked ();
        rdi = stdout;
        ebp &= 0x16;
        ebp += 0xa;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_6;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = bpl;
label_1:
        rbx++;
    } while (rbx != r15);
    rbx = r14 + rbx*8;
    do {
        rbp += 8;
        free (*(rbp));
    } while (rbx != rbp);
    do {
label_5:
        free (r14);
        free (*((rsp + 0x10)));
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_7;
        }
        return rax;
label_4:
        rax = extract_trimmed_name (rbx);
        *((r14 + r15*8)) = rax;
        r15++;
        goto label_0;
label_6:
        esi = r12d;
        rax = overflow ();
        goto label_1;
label_3:
        rcx = dbg_userid_compare;
        edx = 8;
        esi = 0;
        rdi = rax;
        qsort ();
    } while (1);
label_7:
    stack_chk_fail ();
label_2:
    rdx = r12;
    esi = 3;
    edi = 0;
    rax = quotearg_n_style_colon ();
    r12 = rax;
    rax = errno_location ();
    rcx = r12;
    eax = 0;
    error (1, *(rax), 0x00007300);
}