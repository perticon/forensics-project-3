undefined8 main(int param_1,undefined8 *param_2)

{
  undefined8 uVar1;
  undefined8 uVar2;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,"users","GNU coreutils",Version,1,usage,"Joseph Arceneaux",
             "David MacKenzie",0);
  if (param_1 == optind) {
    users("/var/run/utmp",1);
  }
  else {
    if (param_1 - optind != 1) {
      uVar1 = quote(param_2[(long)optind + 1]);
      uVar2 = dcgettext(0,"extra operand %s",5);
      error(0,0,uVar2,uVar1);
                    /* WARNING: Subroutine does not return */
      usage(1);
    }
    users(param_2[optind],0);
  }
  return 0;
}