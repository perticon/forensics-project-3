
int32_t fun_3d10(void** rdi, void** rsi, void** rdx, ...);

void** fun_3700();

/* cache_fstatat.constprop.0 */
int32_t cache_fstatat_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    int32_t eax8;
    void** rax9;
    void** rcx10;
    void** rdx11;

    rax7 = *reinterpret_cast<void***>(rdx + 48);
    if (rax7 == 0xffffffffffffffff) {
        eax8 = fun_3d10(rdi, rsi, rdx, rdi, rsi, rdx);
        if (!eax8) {
            rax7 = *reinterpret_cast<void***>(rdx + 48);
            goto addr_4aae_4;
        } else {
            *reinterpret_cast<void***>(rdx + 48) = reinterpret_cast<void**>(0xfffffffffffffffe);
            rax9 = fun_3700();
            rcx10 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(rax9))));
            *reinterpret_cast<void***>(rdx + 8) = rcx10;
            rdx11 = rcx10;
        }
    } else {
        addr_4aae_4:
        if (reinterpret_cast<signed char>(rax7) < reinterpret_cast<signed char>(0)) {
            rax9 = fun_3700();
            rdx11 = *reinterpret_cast<void***>(rdx + 8);
        } else {
            return 0;
        }
    }
    *reinterpret_cast<void***>(rax9) = rdx11;
    return -1;
}

int64_t g28;

uint32_t fun_3880();

int32_t fun_3950();

void** quotearg_style(int64_t rdi, void** rsi, ...);

void** program_name = reinterpret_cast<void**>(0);

void** fun_3840();

void** stderr = reinterpret_cast<void**>(0);

void fun_3ca0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8, void** a9, int64_t a10, ...);

void fun_3bd0();

void** file_type(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

unsigned char yesno(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

signed char can_write_any_file(void** rdi, void** rsi, ...);

int32_t fun_3790(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9);

int32_t fun_3870(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9, int32_t a7, void** a8, void* a9);

void** fun_3c10(void** rdi, void** rsi, ...);

struct s0 {
    void** f0;
    signed char[17] pad18;
    unsigned char f12;
    void** f13;
    unsigned char f14;
    unsigned char f15;
};

struct s0* fun_3ad0(void** rdi, void** rsi, ...);

int32_t fun_3980(void** rdi, void** rsi, ...);

/* prompt.isra.0 */
uint32_t prompt_isra_0(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rbx9;
    void*** rsp10;
    void** r14_11;
    void** v12;
    int64_t v13;
    int64_t rax14;
    int64_t v15;
    void** v16;
    uint32_t r10d17;
    uint32_t eax18;
    int64_t rdx19;
    signed char eax20;
    void* rsp21;
    void** eax22;
    void** rbp23;
    uint32_t eax24;
    int32_t eax25;
    void** rax26;
    uint32_t eax27;
    uint32_t v28;
    void** rax29;
    void* rsp30;
    uint32_t r10d31;
    void** rax32;
    void* rsp33;
    void** rdx34;
    void** rax35;
    void** v36;
    void* rsp37;
    void** rax38;
    void* rsp39;
    void** rax40;
    void** rdi41;
    int32_t eax42;
    void* rsp43;
    void** rax44;
    void** rax45;
    void** rax46;
    void* rsp47;
    void** rax48;
    void* rsp49;
    void** rax50;
    unsigned char al51;
    signed char al52;
    int32_t eax53;
    uint32_t v54;
    int32_t eax55;
    void** rax56;
    void** rax57;
    void* v58;
    int32_t eax59;
    void** rax60;
    void** r15_61;
    void** rax62;
    void* rsp63;
    struct s0* rax64;
    void* rdx65;
    uint32_t eax66;

    *reinterpret_cast<int32_t*>(&r13_7) = *reinterpret_cast<int32_t*>(&rdi);
    r12_8 = rcx;
    rbx9 = rsi;
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8);
    r14_11 = *reinterpret_cast<void***>(rsi + 48);
    (&v12)[4] = edx;
    *reinterpret_cast<int32_t*>(&v13) = *reinterpret_cast<int32_t*>(&r8);
    rax14 = g28;
    v15 = rax14;
    v16 = *reinterpret_cast<void***>(rsi + 56);
    if (!r9) {
        r10d17 = 0;
        while (1) {
            eax18 = 3;
            if (*reinterpret_cast<void***>(rbx9 + 32)) {
                addr_4cf7_4:
                rdx19 = v15 - g28;
                if (!rdx19) 
                    break;
            } else {
                eax20 = *reinterpret_cast<signed char*>(r12_8 + 4);
                if (eax20 == 5) 
                    goto addr_4cf2_6;
                if (*reinterpret_cast<void***>(r12_8)) 
                    goto addr_4ced_8; else 
                    goto addr_4c44_9;
            }
            fun_3880();
            rsp21 = reinterpret_cast<void*>(rsp10 - 8 + 8);
            addr_4fcf_11:
            fun_3950();
            rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
            r10d17 = 0;
            eax22 = reinterpret_cast<void**>(3);
            addr_4c18_12:
            *reinterpret_cast<void***>(rbp23) = eax22;
            continue;
            addr_4ced_8:
            if (eax20 != 3) 
                goto addr_4cf2_6;
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
            addr_4d3a_14:
            eax24 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v12 + 4)) << 2;
            if (eax24) {
                if (eax24 != 4) 
                    goto addr_4fc0_16; else 
                    goto addr_4d51_17;
            }
            rbp23 = reinterpret_cast<void**>(rsp10 + 32);
            rsi = r14_11;
            *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_7);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            *reinterpret_cast<unsigned char*>(&v12 + 4) = *reinterpret_cast<unsigned char*>(&r10d17);
            eax25 = cache_fstatat_constprop_0(rdi, rsi, rbp23, rcx, r8, r9);
            rsp10 = rsp10 - 8 + 8;
            if (eax25) {
                addr_4ea8_19:
                rax26 = fun_3700();
                rsp10 = rsp10 - 8 + 8;
                rbp23 = *reinterpret_cast<void***>(rax26);
                *reinterpret_cast<int32_t*>(&rbp23 + 4) = 0;
                goto addr_4eaf_20;
            } else {
                eax27 = v28 & 0xf000;
                if (eax27 == 0xa000) {
                    if (*reinterpret_cast<signed char*>(r12_8 + 4) != 3) {
                        addr_4cf2_6:
                        eax18 = 2;
                        goto addr_4cf7_4;
                    } else {
                        goto addr_4e91_24;
                    }
                } else {
                    r10d17 = *reinterpret_cast<unsigned char*>(&v12 + 4);
                    if (eax27 == 0x4000) {
                        addr_4d51_17:
                        if (*reinterpret_cast<signed char*>(r12_8 + 9)) {
                            *reinterpret_cast<unsigned char*>(&v12 + 4) = *reinterpret_cast<unsigned char*>(&r10d17);
                            rax29 = quotearg_style(4, v16, 4, v16);
                            rsp30 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                            r10d31 = *reinterpret_cast<unsigned char*>(&v12 + 4);
                            r12_8 = rax29;
                            if (*reinterpret_cast<int32_t*>(&v13) != 2 || *reinterpret_cast<signed char*>(&r10d31)) {
                                rbp23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp30) + 32);
                            } else {
                                r13_7 = program_name;
                                if (*reinterpret_cast<int32_t*>(&rbx9)) {
                                    rax32 = fun_3840();
                                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                    rdx34 = rax32;
                                } else {
                                    rax35 = fun_3840();
                                    rsp33 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                                    rdx34 = rax35;
                                }
                                rdi = stderr;
                                r8 = r12_8;
                                rcx = r13_7;
                                rsi = reinterpret_cast<void**>(1);
                                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                fun_3ca0(rdi, 1, rdx34, rcx, r8, r9, v16, v12, v36, v13);
                                rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp33) - 8 + 8);
                                goto addr_4f2b_32;
                            }
                        } else {
                            if (!*reinterpret_cast<signed char*>(r12_8 + 10) || !*reinterpret_cast<unsigned char*>(&r10d17)) {
                                rbp23 = reinterpret_cast<void**>(21);
                                *reinterpret_cast<int32_t*>(&rbp23 + 4) = 0;
                                rax38 = quotearg_style(4, v16, 4, v16);
                                rsp39 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                                r12_8 = rax38;
                                goto addr_4d80_35;
                            } else {
                                addr_4fc0_16:
                                rbp23 = reinterpret_cast<void**>(rsp10 + 32);
                                goto addr_4e91_24;
                            }
                        }
                    } else {
                        addr_4e91_24:
                        rax40 = quotearg_style(4, v16, 4, v16);
                        rsp30 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                        r12_8 = rax40;
                    }
                }
            }
            *reinterpret_cast<int32_t*>(&rdi41) = *reinterpret_cast<int32_t*>(&r13_7);
            *reinterpret_cast<int32_t*>(&rdi41 + 4) = 0;
            eax42 = cache_fstatat_constprop_0(rdi41, r14_11, rbp23, rcx, r8, r9);
            rsp43 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
            if (eax42) {
                rax44 = fun_3840();
                r13_7 = rax44;
                rax45 = fun_3700();
                rcx = r12_8;
                *reinterpret_cast<int32_t*>(&rdi) = 0;
                *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                rsi = *reinterpret_cast<void***>(rax45);
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_3bd0();
                rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp43) - 8 + 8 - 8 + 8 - 8 + 8);
                eax18 = 4;
                goto addr_4cf7_4;
            } else {
                rax46 = file_type(rbp23, r14_11, rbp23, rcx, r8, r9);
                rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp43) - 8 + 8);
                r13_7 = program_name;
                rbp23 = rax46;
                if (*reinterpret_cast<int32_t*>(&rbx9)) {
                    rax48 = fun_3840();
                    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
                    rdx34 = rax48;
                } else {
                    rax50 = fun_3840();
                    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
                    rdx34 = rax50;
                }
                rdi = stderr;
                r9 = r12_8;
                r8 = rbp23;
                rcx = r13_7;
                rsi = reinterpret_cast<void**>(1);
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_3ca0(rdi, 1, rdx34, rcx, r8, r9, v16, v12, v36, v13);
                rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
            }
            addr_4f2b_32:
            al51 = yesno(rdi, 1, rdx34, rcx, r8, r9);
            rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
            eax18 = 3 - al51;
            goto addr_4cf7_4;
            addr_4d80_35:
            fun_3840();
            rcx = r12_8;
            rsi = rbp23;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi) = 0;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            fun_3bd0();
            rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp39) - 8 + 8 - 8 + 8);
            eax18 = 4;
            goto addr_4cf7_4;
            addr_4c44_9:
            if (eax20 == 3) 
                goto addr_4c55_42;
            if (!*reinterpret_cast<signed char*>(r12_8 + 25)) 
                goto addr_4cf2_6;
            addr_4c55_42:
            *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&r10d17);
            al52 = can_write_any_file(rdi, rsi, rdi, rsi);
            rsp10 = rsp10 - 8 + 8;
            r10d17 = *reinterpret_cast<unsigned char*>(&v36);
            if (al52) 
                goto addr_4ce8_44;
            rbp23 = reinterpret_cast<void**>(rsp10 + 32);
            rsi = r14_11;
            *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_7);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&r10d17);
            eax53 = cache_fstatat_constprop_0(rdi, rsi, rbp23, rcx, r8, r9);
            rsp10 = rsp10 - 8 + 8;
            if (eax53) 
                goto addr_4ea8_19;
            r10d17 = *reinterpret_cast<unsigned char*>(&v36);
            if ((v54 & 0xf000) == 0xa000 || (*reinterpret_cast<int32_t*>(&rcx) = 0x200, *reinterpret_cast<int32_t*>(&rcx + 4) = 0, rsi = r14_11, *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&r13_7), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&r10d17), eax55 = fun_3790(rdi, rsi, 2, 0x200, r8, r9), rsp10 = rsp10 - 8 + 8, r10d17 = *reinterpret_cast<unsigned char*>(&v36), eax55 == 0)) {
                addr_4ce8_44:
                eax20 = *reinterpret_cast<signed char*>(r12_8 + 4);
                goto addr_4ced_8;
            } else {
                *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&r10d17);
                rax56 = fun_3700();
                rsp10 = rsp10 - 8 + 8;
                r10d17 = *reinterpret_cast<unsigned char*>(&v36);
                rbp23 = *reinterpret_cast<void***>(rax56);
                *reinterpret_cast<int32_t*>(&rbp23 + 4) = 0;
                if (!reinterpret_cast<int1_t>(rbp23 == 13)) {
                    addr_4eaf_20:
                    rax57 = quotearg_style(4, v16, 4, v16);
                    rsp39 = reinterpret_cast<void*>(rsp10 - 8 + 8);
                    r12_8 = rax57;
                    goto addr_4d80_35;
                } else {
                    *reinterpret_cast<int32_t*>(&rbx9) = 1;
                    *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
                    goto addr_4d3a_14;
                }
            }
        }
        return eax18;
    }
    *reinterpret_cast<void***>(r9) = reinterpret_cast<void**>(2);
    rsi = r14_11;
    rbp23 = r9;
    eax59 = fun_3870(rdi, rsi, 0x30900, rcx, r8, r9, *reinterpret_cast<int32_t*>(&v16), v12, v58);
    rsp10 = rsp10 - 8 + 8;
    *reinterpret_cast<int32_t*>(&rdi) = eax59;
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    if (eax59 < 0) {
        addr_4c10_51:
        r10d17 = 0;
        eax22 = reinterpret_cast<void**>(3);
        goto addr_4c18_12;
    } else {
        *reinterpret_cast<int32_t*>(&v36) = eax59;
        rax60 = fun_3c10(rdi, rsi, rdi, rsi);
        rsp21 = reinterpret_cast<void*>(rsp10 - 8 + 8);
        *reinterpret_cast<int32_t*>(&rdi) = *reinterpret_cast<int32_t*>(&v36);
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        r15_61 = rax60;
        if (!rax60) 
            goto addr_4fcf_11;
        rax62 = fun_3700();
        rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
        v36 = rax62;
        *reinterpret_cast<void***>(rax62) = reinterpret_cast<void**>(0);
        do {
            rax64 = fun_3ad0(r15_61, rsi, r15_61, rsi);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
            if (!rax64) 
                break;
            if (*reinterpret_cast<void***>(&rax64->f13) != 46) 
                goto addr_4bb5_56;
            *reinterpret_cast<int32_t*>(&rdx65) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx65) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rdx65) = reinterpret_cast<uint1_t>(rax64->f14 == 46);
            eax66 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax64) + reinterpret_cast<int64_t>(rdx65) + 20);
        } while (!*reinterpret_cast<signed char*>(&eax66) || *reinterpret_cast<signed char*>(&eax66) == 47);
        goto addr_4e46_58;
    }
    rdi = r15_61;
    *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v13) + 4) = *reinterpret_cast<void***>(v36);
    fun_3980(rdi, rsi);
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
    r10d17 = 1;
    *reinterpret_cast<void***>(v36) = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v13) + 4);
    eax22 = reinterpret_cast<void**>(4);
    if (!*reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v13) + 4)) 
        goto addr_4c18_12;
    goto addr_4c10_51;
    addr_4bb5_56:
    rdi = r15_61;
    *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v13) + 4) = *reinterpret_cast<void***>(v36);
    fun_3980(rdi, rsi);
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
    r10d17 = 0;
    *reinterpret_cast<void***>(v36) = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&v13) + 4);
    eax22 = reinterpret_cast<void**>(3);
    goto addr_4c18_12;
    addr_4e46_58:
    goto addr_4bb5_56;
}

struct s1 {
    signed char[24] pad24;
    void** f18;
    signed char[19] pad44;
    int32_t f2c;
};

int32_t fun_3750(int64_t rdi, void** rsi, ...);

void fun_3b80(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

uint32_t excise(struct s1* rdi, void** rsi, void** rdx, void** rcx, void** r8, ...) {
    int32_t r12d6;
    void** rsi7;
    int64_t rdi8;
    int64_t rax9;
    int64_t v10;
    int32_t eax11;
    uint32_t eax12;
    void** rsi13;
    void** rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    void** rax18;
    int64_t r14_19;
    void** rsi20;
    void** rdi21;
    void** rdx22;
    int32_t eax23;
    int1_t zf24;
    void** rsi25;
    void** rax26;
    int64_t rdx27;

    r12d6 = *reinterpret_cast<int32_t*>(&rcx);
    rsi7 = *reinterpret_cast<void***>(rsi + 48);
    *reinterpret_cast<int32_t*>(&rdi8) = rdi->f2c;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = g28;
    v10 = rax9;
    eax11 = fun_3750(rdi8, rsi7);
    if (!eax11) {
        eax12 = 2;
        if (*reinterpret_cast<signed char*>(rdx + 26)) {
            rsi13 = *reinterpret_cast<void***>(rsi + 56);
            rax14 = quotearg_style(4, rsi13);
            if (!*reinterpret_cast<signed char*>(&r12d6)) {
                rax15 = fun_3840();
                rsi16 = rax15;
            } else {
                rax17 = fun_3840();
                rsi16 = rax17;
            }
            fun_3b80(1, rsi16, rax14, rcx);
            eax12 = 2;
            goto addr_50cf_7;
        }
    }
    rax18 = fun_3700();
    *reinterpret_cast<void***>(&r14_19) = *reinterpret_cast<void***>(rax18);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_19) + 4) = 0;
    if (*reinterpret_cast<void***>(&r14_19) == 30) {
        rsi20 = *reinterpret_cast<void***>(rsi + 48);
        *reinterpret_cast<int32_t*>(&rdi21) = rdi->f2c;
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        rdx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xa0 - 8 + 8) - 8 + 8);
        eax23 = fun_3d10(rdi21, rsi20, rdx22, rdi21, rsi20, rdx22);
        if (!eax23 || *reinterpret_cast<void***>(rax18) != 2) {
            zf24 = *reinterpret_cast<void***>(rdx) == 0;
            *reinterpret_cast<void***>(rax18) = reinterpret_cast<void**>(30);
            if (zf24) 
                goto addr_5063_11; else 
                goto addr_51fa_12;
        }
        eax12 = 2;
        *reinterpret_cast<void***>(&r14_19) = reinterpret_cast<void**>(2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_19) + 4) = 0;
        if (!*reinterpret_cast<void***>(rdx)) {
            addr_51fa_12:
            if (*reinterpret_cast<uint16_t*>(rsi + 0x68) != 4) {
                addr_5063_11:
                rsi25 = *reinterpret_cast<void***>(rsi + 56);
                quotearg_style(4, rsi25, 4, rsi25);
                fun_3840();
                fun_3bd0();
                rax26 = *reinterpret_cast<void***>(rsi + 8);
                if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax26 + 88)) >= reinterpret_cast<signed char>(0)) {
                    do {
                        if (*reinterpret_cast<void***>(rax26 + 32)) 
                            break;
                        *reinterpret_cast<void***>(rax26 + 32) = reinterpret_cast<void**>(1);
                        rax26 = *reinterpret_cast<void***>(rax26 + 8);
                    } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax26 + 88)) >= reinterpret_cast<signed char>(0));
                }
            } else {
                goto addr_512e_18;
            }
        } else {
            goto addr_50cf_7;
        }
    } else {
        if (!*reinterpret_cast<void***>(rdx)) 
            goto addr_5058_21;
        eax12 = 2;
        if (*reinterpret_cast<void***>(&r14_19) == 22) 
            goto addr_50cf_7;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&r14_19)) > reinterpret_cast<signed char>(22)) 
            goto addr_5160_24; else 
            goto addr_510d_25;
    }
    eax12 = 4;
    goto addr_50cf_7;
    addr_512e_18:
    if (static_cast<int1_t>(0x8000320000 >> r14_19) && (*reinterpret_cast<void***>(rsi + 64) == 1 || reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi + 64) == 13))) {
        *reinterpret_cast<void***>(rax18) = *reinterpret_cast<void***>(rsi + 64);
        goto addr_5063_11;
    }
    addr_5160_24:
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&r14_19) == 84)) {
        addr_5058_21:
        if (*reinterpret_cast<uint16_t*>(rsi + 0x68) != 4) 
            goto addr_5063_11;
    } else {
        goto addr_50cf_7;
    }
    addr_5124_29:
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r14_19)) > reinterpret_cast<unsigned char>(39)) 
        goto addr_5063_11; else 
        goto addr_512e_18;
    addr_510d_25:
    if (*reinterpret_cast<void***>(&r14_19) == 2 || *reinterpret_cast<void***>(&r14_19) == 20) {
        addr_50cf_7:
        rdx27 = v10 - g28;
        if (rdx27) {
            fun_3880();
        } else {
            return eax12;
        }
    } else {
        if (*reinterpret_cast<uint16_t*>(rsi + 0x68) != 4) 
            goto addr_5063_11; else 
            goto addr_5124_29;
    }
}

void** fun_38e0();

int32_t fun_39d0(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

void** create_hole(void** edi, void** rsi, void** edx, void** rcx, void** r8, ...) {
    void** r12_6;
    void** ebx7;
    void** rax8;
    int64_t rdi9;
    int32_t eax10;
    void** rax11;

    r12_6 = rcx;
    ebx7 = edx;
    rax8 = fun_38e0();
    if (reinterpret_cast<signed char>(rax8) < reinterpret_cast<signed char>(0)) {
        quotearg_style(4, rsi, 4, rsi);
        fun_3840();
        fun_3700();
        fun_3bd0();
        return 0;
    } else {
        if (!*reinterpret_cast<signed char*>(&ebx7) || ((*reinterpret_cast<void***>(&rdi9) = edi, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0, eax10 = fun_39d0(rdi9, 3, reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(r12_6), r12_6), eax10 >= 0) || (rax11 = fun_3700(), *reinterpret_cast<unsigned char*>(&r12_6) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax11) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax11) == 38))), !!*reinterpret_cast<unsigned char*>(&r12_6)))) {
            r12_6 = reinterpret_cast<void**>(1);
        } else {
            quotearg_style(4, rsi, 4, rsi);
            fun_3840();
            fun_3bd0();
        }
        return r12_6;
    }
}

struct s2 {
    signed char[24] pad24;
    uint32_t f18;
};

int32_t fun_3c50(int64_t rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

int32_t fun_3c00(int64_t rdi, int64_t rsi, int64_t rdx);

int32_t qset_acl(void** rdi, void** rsi);

/* set_owner.isra.0 */
int32_t set_owner_isra_0(void** rdi, void** rsi, void** edx, void** rcx, void** r8d, void** r9, int32_t a7, int32_t a8, unsigned char a9, struct s2* a10) {
    uint32_t edx11;
    uint32_t eax12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;
    int32_t eax16;
    void** rax17;
    void** ebp18;
    void** rcx19;
    int64_t rdi20;
    int64_t rdx21;
    int64_t rsi22;
    int64_t rdi23;
    int32_t eax24;
    void** rax25;
    void** r13d26;
    int64_t rdx27;
    int64_t rdi28;
    void** rsi29;
    int32_t eax30;
    void** rax31;

    if (a9) 
        goto addr_6770_2;
    if (!(0xff0000000000ff & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 24)))) {
        if (!*reinterpret_cast<signed char*>(rdi + 57)) 
            goto addr_6770_2;
        r9 = *reinterpret_cast<void***>(rdi + 16);
        edx11 = a10->f18;
    } else {
        edx11 = a10->f18;
    }
    eax12 = ~reinterpret_cast<unsigned char>(r9);
    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax12) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax12) + 1) | 14);
    if (!(eax12 & edx11 & 0xfff)) {
        addr_6770_2:
        if (r8d == 0xffffffff) {
            *reinterpret_cast<int32_t*>(&rcx13) = a8;
            *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx14) = a7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
            *reinterpret_cast<void***>(&rdi15) = edx;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
            eax16 = fun_3c50(rdi15, rcx, rdx14, rcx13, 0x100);
            if (!eax16) {
                addr_6820_9:
                return 1;
            } else {
                rax17 = fun_3700();
                ebp18 = *reinterpret_cast<void***>(rax17);
                if (ebp18 == 1 || reinterpret_cast<int1_t>(ebp18 == 22)) {
                    *reinterpret_cast<int32_t*>(&rcx19) = a8;
                    *reinterpret_cast<int32_t*>(&rcx19 + 4) = 0;
                    *reinterpret_cast<void***>(&rdi20) = edx;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                    fun_3c50(rdi20, rcx, 0xffffffff, rcx19, 0x100);
                    *reinterpret_cast<void***>(rax17) = ebp18;
                } else {
                    addr_67af_12:
                    quotearg_style(4, rsi, 4, rsi);
                    fun_3840();
                    fun_3bd0();
                    goto addr_67e4_13;
                }
            }
        } else {
            *reinterpret_cast<int32_t*>(&rdx21) = a8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx21) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi22) = a7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi22) + 4) = 0;
            *reinterpret_cast<void***>(&rdi23) = r8d;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
            eax24 = fun_3c00(rdi23, rsi22, rdx21);
            if (!eax24) 
                goto addr_6820_9;
            rax25 = fun_3700();
            r13d26 = *reinterpret_cast<void***>(rax25);
            if (r13d26 == 1) 
                goto addr_68a8_16;
            if (r13d26 != 22) 
                goto addr_67af_12;
            addr_68a8_16:
            *reinterpret_cast<int32_t*>(&rdx27) = a8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
            *reinterpret_cast<void***>(&rdi28) = r8d;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
            fun_3c00(rdi28, 0xffffffff, rdx27);
            *reinterpret_cast<void***>(rax25) = r13d26;
        }
    } else {
        rsi29 = r8d;
        *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
        eax30 = qset_acl(rsi, rsi29);
        if (eax30) {
            rax31 = fun_3700();
            if (*reinterpret_cast<void***>(rax31) != 1 && *reinterpret_cast<void***>(rax31) != 22 || *reinterpret_cast<signed char*>(rdi + 27)) {
                quotearg_style(4, rsi);
                fun_3840();
                fun_3bd0();
                goto addr_67e4_13;
            } else {
                goto addr_67e4_13;
            }
        } else {
            goto addr_6770_2;
        }
    }
    if (!*reinterpret_cast<signed char*>(rdi + 26)) {
        return 0;
    }
    addr_67e4_13:
    return -static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rdi + 50));
}

void** quotearg_n_style();

void** stdout = reinterpret_cast<void**>(0);

void emit_verbose(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** rax8;
    void** rax9;
    void** rax10;
    void** rdi11;
    void** rax12;

    rax7 = quotearg_n_style();
    rax8 = quotearg_n_style();
    fun_3b80(1, "%s -> %s", rax8, rax7);
    if (rdx) {
        rax9 = quotearg_style(4, rdx, 4, rdx);
        rax10 = fun_3840();
        fun_3b80(1, rax10, rax9, rax7);
    }
    rdi11 = stdout;
    rax12 = *reinterpret_cast<void***>(rdi11 + 40);
    if (reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi11 + 48))) {
        *reinterpret_cast<void***>(rdi11 + 40) = rax12 + 1;
        *reinterpret_cast<void***>(rax12) = reinterpret_cast<void**>(10);
        return;
    }
}

struct s3 {
    signed char[24] pad24;
    uint64_t f18;
    signed char[4056] pad4088;
    uint64_t fff8;
};

void** renameatu();

void record_file(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** seen_file(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** same_nameat();

void** fun_3940(void** rdi, void** rsi, int64_t rdx);

void fun_36a0(void** rdi, ...);

int32_t fun_3aa0(void** rdi, void** rsi, ...);

int32_t fun_39b0(void** rdi, void** rsi, ...);

void restore_default_fscreatecon_or_die(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t utimecmpat(void** rdi, void** rsi, void* rdx, void** rcx, void** r8);

signed char overwrite_ok(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9);

void** last_component(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** fun_3860(void** rdi, ...);

void** simple_backup_suffix = reinterpret_cast<void**>(0);

uint32_t fun_39c0(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_3a10(void** rdi, void** rsi, ...);

void** subst_suffix(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...);

void** backup_file_rename(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_3bb0(void** rdi, void** rsi, void** rdx);

void** fun_3ab0(void** rdi, void** rsi, void** rdx, ...);

void** quotearg_n_style_colon();

void** remember_copied(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void** src_to_dest_lookup(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** set_process_security_ctx(void** rdi, void** rsi, ...);

struct s4 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int32_t fun_3a80(int64_t rdi, void** rsi);

int32_t fun_3b00(int64_t rdi, void** rsi, ...);

void** set_file_security_ctx(void** rdi, ...);

void** savedir(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void** file_name_concat(void** rdi, void** rsi, ...);

struct s5 {
    void** f0;
    signed char[7] pad8;
    int32_t f8;
    signed char[4] pad16;
    int32_t f10;
    signed char[4] pad24;
    unsigned char f18;
    signed char[7] pad32;
    struct s2* f20;
};

struct s6 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    void** f28;
    signed char[7] pad48;
    void** f30;
};

int64_t fun_36e0(int64_t rdi, void** rsi, void* rdx, ...);

int32_t copy_acl(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

/* mask.0 */
void** mask_0 = reinterpret_cast<void**>(0xff);

void** fun_3a90();

int32_t set_acl(void** rdi, void** rsi, ...);

void forget_created(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_3c60(int64_t rdi, void** rsi, ...);

signed char create_hard_link(void** rdi, void** esi, void** rdx, void** rcx, void** r8d, void** r9, signed char a7, int32_t a8, unsigned char a9);

void** force_symlinkat(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

void** dir_name(void** rdi, void** rsi, ...);

void** open_safer(void** rdi, ...);

void** fun_3d00(int64_t rdi, void* rsi, ...);

void** openat_safer(int64_t rdi, void** rsi, ...);

void** fun_3a20(int64_t rdi, void** rsi, void** rdx, void** rcx);

struct s7 {
    signed char[20] pad20;
    void** f14;
};

int32_t fun_3920(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t fdutimensat(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_3b90(int64_t rdi, ...);

void fdadvise(int64_t rdi);

void** buffer_lcm(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

signed char write_zeros(void** edi, void** rsi);

void** sparse_copy(void** edi, void** esi, void** rdx, void** rcx, void** r8, void** r9d, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12);

int32_t fun_38d0(int64_t rdi, void** rsi, ...);

int32_t fun_3a60(int64_t rdi, void** rsi, ...);

int32_t fun_36d0(int64_t rdi, void** rsi);

void** areadlink_with_size(void** rdi, int64_t rsi);

void** areadlinkat_with_size(int64_t rdi, void** rsi);

void** chown_failure_ok(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

void** top_level_src_name = reinterpret_cast<void**>(0);

unsigned char copy_internal(void** rdi, void** rsi, void** edx, void** rcx, void** r8d, void** r9, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12) {
    void** rdx3;
    void** r8_5;
    void* rsp13;
    void* rbp14;
    void** r15_15;
    struct s3* rsp16;
    void** rdi17;
    void** v18;
    void** rsi19;
    void** v20;
    void** v21;
    void** rcx22;
    void** eax23;
    void** v24;
    void** v25;
    void** rbx26;
    void** v27;
    void** v28;
    void** r13_29;
    void** v30;
    void** v31;
    signed char v32;
    int64_t rax33;
    int64_t v34;
    uint32_t eax35;
    void** v36;
    void** eax37;
    void** rax38;
    void** r12_39;
    void** rsi40;
    void** v41;
    uint32_t ecx42;
    uint1_t zf43;
    int32_t eax44;
    void** v45;
    void** r12_46;
    void** v47;
    void** v48;
    void** r14_49;
    void** eax50;
    void* rsp51;
    void* rsp52;
    uint32_t eax53;
    uint32_t r14d54;
    uint32_t eax55;
    int32_t eax56;
    void** rax57;
    struct s3* rsp58;
    int64_t v59;
    int64_t v60;
    int64_t v61;
    int64_t v62;
    int64_t rdx63;
    uint32_t v64;
    uint32_t v65;
    uint32_t v66;
    uint32_t v67;
    uint64_t v68;
    void** eax69;
    uint32_t r12d70;
    uint32_t v71;
    uint32_t v72;
    void** eax73;
    uint32_t v74;
    uint32_t v75;
    uint64_t v76;
    void** rax77;
    uint32_t v78;
    void** eax79;
    int64_t v80;
    int64_t v81;
    void** v82;
    void** v83;
    uint32_t v84;
    int32_t eax85;
    uint32_t v86;
    void** eax87;
    uint32_t v88;
    uint32_t v89;
    int32_t eax90;
    int32_t eax91;
    int64_t v92;
    int64_t v93;
    uint32_t v94;
    uint32_t v95;
    int64_t v96;
    int64_t v97;
    void** eax98;
    void*** rsp99;
    void** rdx100;
    int64_t rax101;
    void*** rsp102;
    void** rdx103;
    void*** rsp104;
    int32_t eax105;
    int64_t v106;
    int64_t v107;
    int64_t v108;
    int64_t v109;
    void*** rsp110;
    void** rax111;
    void*** rsp112;
    struct s3* rsp113;
    int64_t v114;
    int64_t v115;
    void*** rsp116;
    int32_t eax117;
    uint32_t eax118;
    uint32_t v119;
    void*** rsp120;
    signed char al121;
    void** rdi122;
    void*** rsp123;
    int32_t eax124;
    void*** rsp125;
    signed char al126;
    uint32_t v127;
    void*** rsp128;
    void*** rsp129;
    void*** rsp130;
    uint32_t r13d131;
    void*** rsp132;
    void** rax133;
    void*** rsp134;
    uint32_t eax135;
    uint32_t v136;
    uint32_t v137;
    uint32_t v138;
    void*** rsp139;
    void** rax140;
    void** r13_141;
    uint32_t v142;
    void*** rsp143;
    void** rax144;
    void*** rsp145;
    void** rax146;
    void*** rsp147;
    void** rax148;
    void** r12_149;
    void*** rsp150;
    void** rax151;
    void*** rsp152;
    uint32_t eax153;
    void** rdi154;
    void*** rsp155;
    int64_t rax156;
    void*** rsp157;
    void** rax158;
    void** rsi159;
    void*** rsp160;
    void** rax161;
    void** rdi162;
    void** rdx163;
    void*** rsp164;
    int32_t eax165;
    void*** rsp166;
    int64_t v167;
    int64_t v168;
    int64_t rdi169;
    void*** rsp170;
    void** rax171;
    struct s3* rsp172;
    void** r12_173;
    void*** rsp174;
    void** rax175;
    void*** rsp176;
    void*** rsp177;
    void*** rsp178;
    void** r13_179;
    void*** rsp180;
    void** rax181;
    struct s3* rsp182;
    void* rax183;
    struct s3* rsi184;
    uint64_t rcx185;
    void* rsp186;
    void** rax187;
    int64_t* rsp188;
    void** rax189;
    int64_t* rsp190;
    int64_t* rsp191;
    void*** rsp192;
    struct s3* rsp193;
    void*** rsp194;
    void*** rsp195;
    void** rax196;
    void*** rsp197;
    struct s3* rsp198;
    void** rdi199;
    void*** rsp200;
    void** eax201;
    void*** rsp202;
    void** rax203;
    void*** rsp204;
    uint32_t v205;
    void*** rsp206;
    void*** rsp207;
    void*** rsp208;
    void** rax209;
    void*** rsp210;
    void** rax211;
    void*** rsp212;
    void* rdx213;
    uint32_t v214;
    void** rdi215;
    void*** rsp216;
    int32_t eax217;
    void*** rsp218;
    void** rax219;
    void*** rsp220;
    void*** rsp221;
    void*** rsp222;
    int64_t v223;
    void** v224;
    void** rax225;
    void*** rsp226;
    void** rax227;
    void*** rsp228;
    void** edx229;
    void** rsi230;
    void*** rsp231;
    void** eax232;
    uint32_t v233;
    uint32_t eax234;
    uint32_t eax235;
    void*** rsp236;
    void** eax237;
    void*** rsp238;
    void** eax239;
    void** rax240;
    void** v241;
    void** v242;
    void*** rsp243;
    void** rax244;
    struct s3* rsp245;
    void** r12_246;
    void*** rsp247;
    void** rax248;
    void*** rsp249;
    struct s3* rax250;
    struct s4* rax251;
    struct s4* v252;
    uint32_t v253;
    int64_t rdi254;
    void*** rsp255;
    int32_t eax256;
    struct s3* rsp257;
    void*** rsp258;
    void** rax259;
    struct s3* rsp260;
    void** rdi261;
    void*** rsp262;
    int32_t eax263;
    void*** rsp264;
    void** rax265;
    uint32_t v266;
    unsigned char v267;
    int64_t rdi268;
    void*** rsp269;
    int32_t eax270;
    void*** rsp271;
    void** rax272;
    void** v273;
    void*** rsp274;
    int64_t v275;
    void*** rsp276;
    void** eax277;
    void*** rsp278;
    void** rax279;
    void*** rsp280;
    void** rax281;
    void*** rsp282;
    void*** rsp283;
    void*** rsp284;
    void** rax285;
    void*** rsp286;
    void** rax287;
    void*** rsp288;
    void** v289;
    uint32_t eax290;
    uint32_t eax291;
    void*** rsp292;
    void** rax293;
    struct s3* rsp294;
    void** v295;
    void*** rsp296;
    void*** rsp297;
    void*** rsp298;
    void*** rsp299;
    void** v300;
    void* v301;
    void** v302;
    void** v303;
    void** r13_304;
    void** v305;
    void** rbx306;
    void** v307;
    void** v308;
    void*** rsp309;
    void** rax310;
    void*** rsp311;
    void** rax312;
    uint32_t eax313;
    void*** rsp314;
    void*** rsp315;
    unsigned char v316;
    void*** rsp317;
    void*** rsp318;
    struct s5* rsp319;
    struct s4** rsp320;
    struct s6* rsp321;
    unsigned char al322;
    void*** rsp323;
    void*** rsp324;
    uint32_t esi325;
    void*** rsp326;
    void** rax327;
    void*** rsp328;
    uint32_t edi329;
    void** rdi330;
    void*** rsp331;
    int32_t eax332;
    void** rdi333;
    void*** rsp334;
    void* rdx335;
    int64_t rdi336;
    int64_t v337;
    int64_t v338;
    int64_t v339;
    int64_t v340;
    void*** rsp341;
    int64_t rax342;
    void*** rsp343;
    void** rax344;
    void*** rsp345;
    void** rax346;
    void*** rsp347;
    void*** rsp348;
    void*** rsp349;
    int32_t eax350;
    uint32_t eax351;
    void*** rsp352;
    void** eax353;
    void*** rsp354;
    void** r13d355;
    void*** rsp356;
    void** eax357;
    void*** rsp358;
    int64_t rdi359;
    void*** rsp360;
    int32_t eax361;
    void** rdi362;
    void** rdx363;
    void*** rsp364;
    int32_t eax365;
    int32_t v366;
    void*** rsp367;
    struct s3* rsp368;
    void*** rsp369;
    int32_t eax370;
    void*** rsp371;
    void** rax372;
    void*** rsp373;
    void*** rsp374;
    void*** rsp375;
    void*** rsp376;
    void*** rsp377;
    void*** rsp378;
    void** rax379;
    int32_t v380;
    void** rdx381;
    int32_t v382;
    int32_t v383;
    int32_t v384;
    void** v385;
    void*** rsp386;
    void** rcx387;
    void*** rsp388;
    void*** rsp389;
    void*** rsp390;
    struct s5* rsp391;
    int32_t eax392;
    void** edx393;
    void*** rsp394;
    int64_t v395;
    int64_t v396;
    int64_t rdi397;
    void** rsi398;
    void*** rsp399;
    int32_t eax400;
    void*** rsp401;
    void*** rsp402;
    void** rax403;
    void*** rsp404;
    void** rax405;
    void*** rsp406;
    void** rax407;
    void*** rsp408;
    void** rax409;
    void** rdi410;
    void*** rsp411;
    void*** rsp412;
    void*** rsp413;
    struct s5* rsp414;
    signed char al415;
    void** rsi416;
    void*** rsp417;
    void** eax418;
    void*** rsp419;
    void** rax420;
    void*** rsp421;
    void** rax422;
    void*** rsp423;
    void** rax424;
    void*** rsp425;
    void*** rsp426;
    void** rax427;
    struct s3* rsp428;
    void** rsi429;
    void*** rsp430;
    int32_t eax431;
    void** rdi432;
    void** rdx433;
    void*** rsp434;
    int32_t eax435;
    void*** rsp436;
    int64_t v437;
    int64_t v438;
    void*** rsp439;
    void*** rsp440;
    void** rax441;
    void** v442;
    uint32_t eax443;
    void*** rsp444;
    void** eax445;
    struct s3* rsp446;
    void** r13d447;
    void*** rsp448;
    void** rax449;
    void* rsi450;
    int64_t rdi451;
    void*** rsp452;
    void** eax453;
    struct s3* rsp454;
    void*** rsp455;
    void** rax456;
    void*** rsp457;
    void** rax458;
    void*** rsp459;
    void** rax460;
    struct s3* rsp461;
    int64_t v462;
    int64_t v463;
    void*** rsp464;
    void** rax465;
    void*** rsp466;
    void** rax467;
    void** v468;
    int64_t rdi469;
    void*** rsp470;
    void** eax471;
    void** r12d472;
    void*** rsp473;
    void** rax474;
    void*** rsp475;
    void*** rsp476;
    int32_t eax477;
    struct s3* rsp478;
    void*** rsp479;
    void** rax480;
    void*** rsp481;
    void** rax482;
    void*** rsp483;
    void** rax484;
    void*** rsp485;
    void*** rsp486;
    void*** rsp487;
    uint32_t eax488;
    int64_t rdi489;
    void*** rsp490;
    int32_t eax491;
    void*** rsp492;
    void** eax493;
    int64_t rdi494;
    void** eax495;
    void*** rsp496;
    void** eax497;
    void*** rsp498;
    void** rax499;
    uint32_t eax500;
    unsigned char dl501;
    void*** rsp502;
    void** rax503;
    void*** rsp504;
    void** rax505;
    int64_t rdi506;
    void*** rsp507;
    void** rax508;
    uint32_t eax509;
    void** v510;
    uint32_t v511;
    void*** rsp512;
    void** rax513;
    struct s7* r8d514;
    void*** rsp515;
    void** rax516;
    void*** rsp517;
    void** rax518;
    void*** rsp519;
    int64_t rdi520;
    void*** rsp521;
    void** eax522;
    void*** rsp523;
    void** rax524;
    void*** rsp525;
    void** rax526;
    int64_t rdi527;
    void*** rsp528;
    int32_t eax529;
    void** eax530;
    void*** rsp531;
    int32_t eax532;
    void*** rsp533;
    int32_t eax534;
    void*** rsp535;
    void** rax536;
    void*** rsp537;
    void** rax538;
    void*** rsp539;
    void** rax540;
    void*** rsp541;
    void*** rsp542;
    int32_t eax543;
    uint32_t eax544;
    unsigned char al545;
    int1_t zf546;
    void*** rsp547;
    void** eax548;
    void*** rsp549;
    void** edx550;
    void*** rsp551;
    int32_t eax552;
    uint32_t eax553;
    void** eax554;
    int64_t rdi555;
    void*** rsp556;
    int32_t eax557;
    void*** rsp558;
    void** rax559;
    void*** rsp560;
    void** rax561;
    void*** rsp562;
    void** rax563;
    void*** rsp564;
    void*** rsp565;
    void** rax566;
    void*** rsp567;
    void** rax568;
    void*** rsp569;
    void** rax570;
    void*** rsp571;
    void** rax572;
    void*** rsp573;
    void* rsi574;
    int64_t rdi575;
    void*** rsp576;
    void** eax577;
    void*** rsp578;
    void** rax579;
    struct s3* rsp580;
    int64_t rdi581;
    void*** rsp582;
    int32_t eax583;
    void** v584;
    void** v585;
    void** rax586;
    void*** rsp587;
    void** eax588;
    void*** rsp589;
    void*** rsp590;
    void** eax591;
    void*** rsp592;
    int64_t rdi593;
    void*** rsp594;
    int32_t eax595;
    void*** rsp596;
    void** rax597;
    void*** rsp598;
    void** rax599;
    void*** rsp600;
    void** rax601;
    void*** rsp602;
    void*** rsp603;
    void** rax604;
    void*** rsp605;
    void** rax606;
    void*** rsp607;
    void** r8d608;
    int64_t rdi609;
    void*** rsp610;
    int64_t rdi611;
    int64_t v612;
    void*** rsp613;
    void** rax614;
    void** r8_615;
    void** v616;
    void* rsi617;
    void** rax618;
    int32_t eax619;
    int32_t v620;
    int64_t rdi621;
    void*** rsp622;
    int32_t eax623;
    signed char v624;
    int32_t eax625;
    uint1_t zf626;
    void** v627;
    void** rax628;
    void** v629;
    uint32_t edi630;
    void** v631;
    signed char v632;
    void** r14_633;
    void** v634;
    void** rbx635;
    void*** rsp636;
    void** rax637;
    void** r15_638;
    void** rax639;
    void*** rsp640;
    void** rax641;
    void*** rsp642;
    void** rax643;
    unsigned char v644;
    void*** rsp645;
    signed char al646;
    void*** rsp647;
    void** eax648;
    void*** rsp649;
    void*** rsp650;
    struct s4* rax651;
    void*** rsp652;
    void*** rsp653;
    struct s5* rsp654;
    struct s4** rsp655;
    struct s6* rsp656;
    void** eax657;
    void** edi658;
    void*** rsp659;
    void** rax660;
    void*** rsp661;
    void** rax662;
    void*** rsp663;
    signed char al664;
    uint32_t r8d665;
    void*** rsp666;
    void** rax667;
    struct s3* rsp668;
    int64_t rdi669;
    void*** rsp670;
    int32_t eax671;
    int64_t rdi672;
    void*** rsp673;
    int32_t eax674;
    void*** rsp675;
    void** rax676;
    void** r8b677;
    void*** rsp678;
    void** rax679;
    void*** rsp680;
    void** rax681;
    void*** rsp682;
    int64_t rdi683;
    void*** rsp684;
    int32_t eax685;
    int1_t zf686;
    void** rax687;
    int32_t v688;
    void** v689;
    int32_t v690;
    void** v691;
    void** v692;
    void*** rsp693;
    void** rcx694;
    void*** rsp695;
    void*** rsp696;
    void*** rsp697;
    struct s5* rsp698;
    int32_t eax699;
    void*** rsp700;
    void** rax701;
    void*** rsp702;
    void** rax703;
    void*** rsp704;
    void** rax705;
    void*** rsp706;
    void** r8_707;
    void*** rsp708;
    void** rax709;
    void*** rsp710;
    void** rax711;
    int1_t less_or_equal712;
    void*** rsp713;
    void** rax714;
    void*** rsp715;
    void*** rsp716;
    struct s4* rax717;
    void*** rsp718;
    void*** rsp719;
    struct s5* rsp720;
    struct s4** rsp721;
    struct s6* rsp722;
    void** eax723;
    int64_t v724;
    int64_t v725;
    void*** rsp726;
    void** rax727;
    void*** rsp728;
    void** rax729;
    void** eax730;
    void*** rsp731;
    void** rax732;
    void*** rsp733;
    void** rax734;
    void*** rsp735;
    void*** rsp736;
    void** rax737;
    void*** rsp738;
    void** rax739;
    void*** rsp740;
    void*** rsp741;
    void** eax742;
    int64_t rdi743;
    void*** rsp744;
    int32_t eax745;
    int64_t rdi746;
    void*** rsp747;
    int32_t eax748;
    void** eax749;
    void*** rsp750;
    void** rax751;
    uint32_t eax752;
    void** v753;
    int64_t rdi754;
    void*** rsp755;
    int32_t eax756;
    void*** rsp757;
    void** rax758;
    void*** rsp759;
    void** rax760;
    void*** rsp761;
    int64_t v762;
    void** rax763;
    struct s3* rsp764;
    void*** rsp765;
    void** rax766;
    void*** rsp767;
    void** eax768;
    struct s3* rsp769;
    void*** rsp770;
    uint32_t v771;
    void** v772;
    void*** rsp773;
    void** rax774;
    int64_t rdi775;
    void*** rsp776;
    void** rax777;
    void*** rsp778;
    void*** rsp779;
    void** rax780;
    void*** rsp781;
    void** rax782;
    void*** rsp783;
    void*** rsp784;
    int64_t rax785;
    struct s3* rsp786;
    void*** rsp787;
    void*** rsp788;
    void*** rsp789;
    void** v790;
    int64_t rdx791;
    int32_t v792;
    int64_t rdi793;
    void*** rsp794;
    int32_t eax795;
    void*** rsp796;
    void** eax797;
    void*** rsp798;
    void** rax799;
    void*** rsp800;
    void** rax801;
    void*** rsp802;
    void*** rsp803;
    void** eax804;
    void** eax805;
    uint32_t eax806;
    void*** rsp807;
    void** rdi808;
    void** rdx809;
    void*** rsp810;
    uint32_t eax811;
    int64_t v812;
    void*** rsp813;
    int64_t v814;
    void** v815;
    void** rax816;
    uint64_t v817;
    void*** rsp818;
    int64_t v819;
    void** v820;
    void** rax821;
    void*** rsp822;
    int32_t eax823;
    void*** rsp824;
    void** rax825;
    void*** rsp826;
    void** rax827;
    void*** rsp828;
    void*** rsp829;
    void*** rsp830;
    void*** rsp831;
    void** eax832;
    struct s3* rsp833;
    void*** rsp834;
    void** rax835;
    void*** rsp836;
    void** rax837;
    void*** rsp838;
    void** rax839;
    void*** rsp840;
    void*** rsp841;
    void** eax842;
    void** rax843;
    void*** rsp844;
    void** rax845;
    void*** rsp846;
    void*** rsp847;
    struct s5* rsp848;
    signed char al849;
    void*** rsp850;
    void** rax851;
    void*** rsp852;
    void** rax853;
    void*** rsp854;
    void** rax855;
    void*** rsp856;
    void** rax857;
    void*** rsp858;
    void*** rsp859;
    void*** rsp860;
    void** rax861;
    void*** rsp862;
    void** rax863;
    void*** rsp864;
    void** rax865;
    struct s3* rsp866;
    void** rcx867;
    void** rdx868;
    int64_t rdi869;
    void*** rsp870;
    int32_t eax871;
    void*** rsp872;
    void** rax873;
    unsigned char al874;
    void*** rsp875;
    void** rax876;
    void*** rsp877;
    void*** rsp878;
    void*** rsp879;
    void** rax880;
    void*** rsp881;
    void** rax882;
    void*** rsp883;
    void** rax884;
    void*** rsp885;
    void*** rsp886;
    int64_t v887;
    int64_t v888;
    void** rdi889;
    void*** rsp890;
    void** eax891;
    void*** rsp892;
    void** rax893;
    void*** rsp894;
    void*** rsp895;
    void*** rsp896;
    uint64_t v897;
    int1_t zf898;
    uint32_t v899;
    int64_t rdi900;
    void*** rsp901;
    int32_t eax902;
    void*** rsp903;
    void** rax904;
    void*** rsp905;
    void** rax906;
    void*** rsp907;
    void*** rsp908;
    uint32_t eax909;
    void*** rsp910;
    void** rax911;
    void*** rsp912;
    void** rax913;
    void*** rsp914;
    void*** rsp915;
    signed char al916;
    void*** rsp917;
    int64_t v918;
    void** v919;
    void** rax920;
    void** rax921;
    void*** rsp922;
    void** rax923;
    void*** rsp924;
    void*** rsp925;
    struct s5* rsp926;
    signed char al927;
    void** rsi928;
    void*** rsp929;
    void*** rsp930;
    void*** rsp931;

    rdx3 = edx;
    r8_5 = r8d;
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp14 = rsp13;
    r15_15 = rdi;
    rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(rsp13) - 8 - 8 - 8 - 8 - 8 - 0x3a8);
    rdi17 = a12;
    v18 = rsi;
    rsi19 = a10;
    v20 = rcx;
    v21 = a7;
    rcx22 = a11;
    eax23 = a9;
    v24 = rdx3;
    v25 = r9;
    rbx26 = a8;
    v27 = eax23;
    v28 = rsi19;
    r13_29 = *reinterpret_cast<void***>(rbx26 + 64);
    v30 = rcx22;
    v31 = rdi17;
    v32 = *reinterpret_cast<signed char*>(&eax23);
    rax33 = g28;
    v34 = rax33;
    *reinterpret_cast<void***>(rcx22) = reinterpret_cast<void**>(0);
    eax35 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
    *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax35);
    if (!*reinterpret_cast<unsigned char*>(&eax35)) {
        *reinterpret_cast<unsigned char*>(&v36) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r8_5) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r8_5 == 0)));
        if (!r13_29) 
            goto addr_6e2e_3; else 
            goto addr_6c2c_4;
    }
    if (reinterpret_cast<signed char>(r13_29) < reinterpret_cast<signed char>(0)) {
        rcx22 = v20;
        rdx3 = v24;
        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
        rsi19 = r15_15;
        rdi17 = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
        eax37 = renameatu();
        rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        if (eax37) {
            rax38 = fun_3700();
            rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
            r13_29 = *reinterpret_cast<void***>(rax38);
        } else {
            r8_5 = reinterpret_cast<void**>(1);
            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
            *reinterpret_cast<void***>(v31) = reinterpret_cast<void**>(1);
            goto addr_6e2e_3;
        }
    }
    rdx3 = v31;
    *reinterpret_cast<unsigned char*>(&v36) = reinterpret_cast<uint1_t>(r13_29 == 0);
    r8_5 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v36)));
    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
    *reinterpret_cast<void***>(rdx3) = r8_5;
    if (r13_29) {
        addr_6c2c_4:
        if (r13_29 != 17 || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx26 + 8) == 2)) {
            r12_39 = r15_15;
            rsi40 = r15_15;
            rdi17 = reinterpret_cast<void**>(0xffffff9c);
            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
        } else {
            v41 = reinterpret_cast<void**>(0);
            goto addr_6c91_12;
        }
    } else {
        addr_6e2e_3:
        if (*reinterpret_cast<signed char*>(rbx26 + 63)) {
            v41 = reinterpret_cast<void**>(0);
            r13_29 = reinterpret_cast<void**>(0);
            goto addr_6c91_12;
        } else {
            rdi17 = v24;
            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
            r12_39 = v18;
            r13_29 = reinterpret_cast<void**>(0);
            rsi40 = v20;
        }
    }
    ecx42 = 0;
    zf43 = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(rbx26 + 4) == 2);
    r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
    v41 = r8_5;
    *reinterpret_cast<unsigned char*>(&ecx42) = zf43;
    rdx3 = r9;
    rcx22 = reinterpret_cast<void**>(ecx42 << 8);
    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
    eax44 = fun_3d10(rdi17, rsi40, rdx3);
    rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    r8_5 = v41;
    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
    rsi19 = r12_39;
    if (eax44) 
        goto addr_7990_16;
    v41 = v45;
    if ((reinterpret_cast<unsigned char>(v45) & 0xf000) != 0x4000 || (r12_46 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 56)))), *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&r12_46))) {
        addr_6c91_12:
        if (!v27 || (rdi17 = *reinterpret_cast<void***>(rbx26 + 80), rdi17 == 0)) {
            addr_6ce1_18:
            if (*reinterpret_cast<signed char*>(rbx26 + 4) == 4) {
                v47 = reinterpret_cast<void**>(1);
            } else {
                if (*reinterpret_cast<signed char*>(rbx26 + 4) != 3 || !v27) {
                    v47 = reinterpret_cast<void**>(0);
                } else {
                    v47 = reinterpret_cast<void**>(1);
                    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r8_5) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r8_5 == 0))) {
                        v48 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                        goto addr_6f70_24;
                    }
                }
            }
        } else {
            r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
            if ((reinterpret_cast<unsigned char>(v41) & 0xf000) == 0x4000 || *reinterpret_cast<void***>(rbx26)) {
                addr_6cc8_26:
                rdx3 = r9;
                rsi19 = r15_15;
                record_file(rdi17, rsi19, rdx3, rcx22);
                rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                r8_5 = r8_5;
                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                goto addr_6ce1_18;
            } else {
                v47 = r8_5;
                eax50 = seen_file(rdi17, r15_15, r9, rcx22);
                rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                r12_46 = eax50;
                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                if (*reinterpret_cast<signed char*>(&eax50)) {
                    quotearg_style(4, r15_15, 4, r15_15);
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                    goto addr_8f8c_29;
                } else {
                    rdi17 = *reinterpret_cast<void***>(rbx26 + 80);
                    r9 = r9;
                    r8_5 = v47;
                    goto addr_6cc8_26;
                }
            }
        }
    } else {
        quotearg_style(4, r15_15, 4, r15_15);
        rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        if (*reinterpret_cast<signed char*>(rbx26 + 25)) {
            goto addr_8f8c_29;
        } else {
            fun_3840();
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp51) - 8 + 8);
            goto addr_792e_34;
        }
    }
    v48 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&r14_49) = 0;
    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r8_5) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r8_5 == 0))) 
        goto addr_6d20_36;
    if (r13_29 == 17 && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx26 + 8) == 2)) {
        *reinterpret_cast<uint32_t*>(&r14_49) = 0;
        r13_29 = reinterpret_cast<void**>(0);
        goto addr_7f73_39;
    }
    eax53 = reinterpret_cast<unsigned char>(v41) & 0xf000;
    if (eax53 == 0x8000) 
        goto addr_7880_41;
    *reinterpret_cast<unsigned char*>(&r14_49) = reinterpret_cast<uint1_t>(eax53 == 0xa000);
    *reinterpret_cast<unsigned char*>(&eax53) = reinterpret_cast<uint1_t>(eax53 == 0x4000);
    r14d54 = *reinterpret_cast<uint32_t*>(&r14_49) | eax53;
    eax55 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 20)) ^ 1;
    *reinterpret_cast<unsigned char*>(&r14_49) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r14d54) | *reinterpret_cast<unsigned char*>(&eax55));
    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
        goto addr_706c_43;
    addr_7880_41:
    *reinterpret_cast<uint32_t*>(&r14_49) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
        goto addr_706c_43;
    *reinterpret_cast<uint32_t*>(&r14_49) = *reinterpret_cast<unsigned char*>(rbx26 + 58);
    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
        goto addr_706c_43;
    *reinterpret_cast<uint32_t*>(&r14_49) = *reinterpret_cast<unsigned char*>(rbx26 + 23);
    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
        goto addr_706c_43;
    rdi17 = *reinterpret_cast<void***>(rbx26);
    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
    if (rdi17) {
        *reinterpret_cast<uint32_t*>(&r14_49) = 1;
        goto addr_706c_43;
    }
    *reinterpret_cast<uint32_t*>(&r14_49) = *reinterpret_cast<unsigned char*>(rbx26 + 21);
    if (*reinterpret_cast<unsigned char*>(&r14_49)) {
        addr_706c_43:
        r8_5 = reinterpret_cast<void**>(0x100);
        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
    } else {
        if (r8_5) 
            goto addr_78cb_50;
    }
    rsi19 = v20;
    rdi17 = v24;
    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
    rdx3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
    rcx22 = r8_5;
    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
    eax56 = fun_3d10(rdi17, rsi19, rdx3, rdi17, rsi19, rdx3);
    rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    if (eax56) {
        rax57 = fun_3700();
        rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        if (*reinterpret_cast<void***>(rax57) == 40) {
            if (*reinterpret_cast<unsigned char*>(rbx26 + 22)) {
                addr_78d2_54:
                v48 = reinterpret_cast<void**>(0);
                *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                if (!reinterpret_cast<int1_t>(r13_29 == 17)) 
                    goto addr_6d20_36;
            } else {
                goto addr_783c_56;
            }
        } else {
            if (*reinterpret_cast<void***>(rax57) == 2) {
                addr_78cb_50:
                *reinterpret_cast<unsigned char*>(&v36) = 1;
                goto addr_78d2_54;
            } else {
                addr_783c_56:
                quotearg_style(4, v18, 4, v18);
                rsp58 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                goto addr_785c_59;
            }
        }
    }
    if (*reinterpret_cast<void***>(rbx26 + 8) == 2) 
        goto addr_7f70_61;
    if (v59 != v60 || v61 != v62) {
        if (*reinterpret_cast<signed char*>(rbx26 + 4) != 2) 
            goto addr_7f70_61;
        *reinterpret_cast<void***>(&rdx63) = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx63) + 4) = 0;
        r9 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
    } else {
        r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 23)));
        if (*reinterpret_cast<unsigned char*>(&r13_29)) 
            goto addr_7f73_39;
        if (*reinterpret_cast<signed char*>(rbx26 + 4) == 2) 
            goto addr_97b0_67; else 
            goto addr_850c_68;
    }
    addr_70c3_69:
    r12_46 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
    r8_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
    if ((v64 & 0xf000) != 0xa000 || (v65 & 0xf000) != 0xa000) {
        rsi19 = *reinterpret_cast<void***>(rbx26);
        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
        if (!rsi19) {
            rcx22 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))));
            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
            if (!*reinterpret_cast<signed char*>(&rcx22) && !*reinterpret_cast<unsigned char*>(rbx26 + 21)) {
                if ((v66 & 0xf000) == 0xa000) 
                    goto addr_9052_73; else 
                    goto addr_901f_74;
            }
            if ((v67 & 0xf000) == 0xa000) 
                goto addr_7f70_61;
            if (!*reinterpret_cast<unsigned char*>(&rdx63)) 
                goto addr_936d_77;
            if (v68 > 1) 
                goto addr_9c48_79;
        } else {
            if (*reinterpret_cast<unsigned char*>(&rdx63)) {
                rcx22 = v20;
                rsi19 = r15_15;
                rdi17 = reinterpret_cast<void**>(0xffffff9c);
                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                r13_29 = reinterpret_cast<void**>(0);
                eax69 = same_nameat();
                rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
                r12d70 = reinterpret_cast<unsigned char>(eax69) ^ 1;
                goto addr_9549_82;
            } else {
                if (*reinterpret_cast<void***>(rbx26 + 24)) 
                    goto addr_7f70_61;
                if (*reinterpret_cast<signed char*>(rbx26 + 4) == 2) 
                    goto addr_7f70_61;
                if ((v71 & 0xf000) != 0xa000) 
                    goto addr_7f70_61;
                if ((v72 & 0xf000) == 0xa000) 
                    goto addr_7f70_61; else 
                    goto addr_7136_87;
            }
        }
    } else {
        rcx22 = v20;
        rsi19 = r15_15;
        rdi17 = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
        eax73 = same_nameat();
        rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        r13_29 = eax73;
        if (*reinterpret_cast<signed char*>(&eax73)) 
            goto addr_7136_87;
        if (*reinterpret_cast<void***>(rbx26)) 
            goto addr_7f73_39;
        r9 = r9;
        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        if (!r9) 
            goto addr_7f73_39;
        r13_29 = reinterpret_cast<void**>(1);
        r12d70 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))) ^ 1;
        goto addr_9549_82;
    }
    addr_936d_77:
    if ((v74 & 0xf000) == 0xa000) {
        addr_9038_92:
        if (!*reinterpret_cast<signed char*>(&rcx22) || ((v75 & 0xf000) != 0xa000 || (v76 <= 1 || (rdi17 = r15_15, rax77 = fun_3940(rdi17, rsi19, rdx63), rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8), r8_5 = r8_5, rax77 == 0)))) {
            addr_9052_73:
            if (!*reinterpret_cast<unsigned char*>(rbx26 + 58)) 
                goto addr_906d_93;
            if ((v78 & 0xf000) == 0xa000) 
                goto addr_7f70_61;
        } else {
            rcx22 = v20;
            rsi19 = rax77;
            eax79 = same_nameat();
            rdi17 = rax77;
            r13_29 = reinterpret_cast<void**>(0);
            r12d70 = reinterpret_cast<unsigned char>(eax79) ^ 1;
            fun_36a0(rdi17, rdi17);
            rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8 - 8 + 8);
            goto addr_9549_82;
        }
    } else {
        addr_9381_96:
        if (v80 != v81) 
            goto addr_7f70_61;
        if (v82 != v83) 
            goto addr_7f70_61; else 
            goto addr_939d_98;
    }
    addr_906d_93:
    if (*reinterpret_cast<signed char*>(rbx26 + 4) != 2) 
        goto addr_7136_87;
    if ((v84 & 0xf000) == 0xa000) {
        rsi19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30);
        rdi17 = r15_15;
        eax85 = fun_3aa0(rdi17, rsi19, rdi17, rsi19);
        rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
        if (eax85) {
            goto addr_7f70_61;
        }
    } else {
        __asm__("movdqa xmm5, [r8]");
        __asm__("movaps [rbp-0xd0], xmm5");
        __asm__("movdqa xmm6, [r8+0x10]");
        __asm__("movaps [rbp-0xc0], xmm6");
        __asm__("movdqa xmm7, [r8+0x20]");
        __asm__("movaps [rbp-0xb0], xmm7");
        __asm__("movdqa xmm5, [r8+0x30]");
        __asm__("movaps [rbp-0xa0], xmm5");
        __asm__("movdqa xmm6, [r8+0x40]");
        __asm__("movaps [rbp-0x90], xmm6");
        __asm__("movdqa xmm7, [r8+0x50]");
        __asm__("movaps [rbp-0x80], xmm7");
        __asm__("movdqa xmm5, [r8+0x60]");
        __asm__("movaps [rbp-0x70], xmm5");
        __asm__("movdqa xmm6, [r8+0x70]");
        __asm__("movaps [rbp-0x60], xmm6");
        __asm__("movdqa xmm7, [r8+0x80]");
        __asm__("movaps [rbp-0x50], xmm7");
    }
    if ((v86 & 0xf000) == 0xa000) 
        goto addr_a367_104;
    __asm__("movdqa xmm5, [r12]");
    __asm__("movaps [rbp-0x160], xmm5");
    __asm__("movdqa xmm6, [r12+0x10]");
    __asm__("movaps [rbp-0x150], xmm6");
    __asm__("movdqa xmm7, [r12+0x20]");
    __asm__("movaps [rbp-0x140], xmm7");
    __asm__("movdqa xmm5, [r12+0x30]");
    __asm__("movaps [rbp-0x130], xmm5");
    __asm__("movdqa xmm6, [r12+0x40]");
    __asm__("movaps [rbp-0x120], xmm6");
    __asm__("movdqa xmm7, [r12+0x50]");
    __asm__("movaps [rbp-0x110], xmm7");
    __asm__("movdqa xmm5, [r12+0x60]");
    __asm__("movaps [rbp-0x100], xmm5");
    __asm__("movdqa xmm6, [r12+0x70]");
    __asm__("movaps [rbp-0xf0], xmm6");
    __asm__("movdqa xmm7, [r12+0x80]");
    __asm__("movaps [rbp-0xe0], xmm7");
    goto addr_918b_106;
    addr_9549_82:
    if (!*reinterpret_cast<signed char*>(&r12d70)) 
        goto addr_7136_87;
    goto addr_7f73_39;
    addr_939d_98:
    r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 23)));
    if (*reinterpret_cast<unsigned char*>(&r13_29)) {
        goto addr_7f73_39;
    }
    addr_9034_109:
    rcx22 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))));
    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
    goto addr_9038_92;
    addr_9c48_79:
    rcx22 = v20;
    *reinterpret_cast<void***>(&rdx63) = v24;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx63) + 4) = 0;
    rsi19 = r15_15;
    rdi17 = reinterpret_cast<void**>(0xffffff9c);
    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
    eax87 = same_nameat();
    rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    r8_5 = r8_5;
    r13_29 = eax87;
    if (!*reinterpret_cast<signed char*>(&eax87)) {
        r12d70 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))) ^ 1;
        goto addr_9549_82;
    } else {
        if ((v88 & 0xf000) != 0xa000) {
            addr_901f_74:
            if ((v89 & 0xf000) != 0xa000) 
                goto addr_9381_96; else 
                goto addr_9034_109;
        } else {
            goto addr_9034_109;
        }
    }
    addr_97b0_67:
    *reinterpret_cast<void***>(&rdx63) = reinterpret_cast<void**>(1);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx63) + 4) = 0;
    r9 = reinterpret_cast<void**>(1);
    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
    goto addr_70c3_69;
    addr_850c_68:
    r12_46 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffea0);
    rdi17 = v24;
    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
    rcx22 = reinterpret_cast<void**>(0x100);
    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
    rsi19 = v20;
    eax90 = fun_3d10(rdi17, rsi19, r12_46, rdi17, rsi19, r12_46);
    rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    if (eax90) 
        goto addr_7f73_39;
    r8_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30);
    rdi17 = r15_15;
    rsi19 = r8_5;
    eax91 = fun_39b0(rdi17, rsi19, rdi17, rsi19);
    rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8);
    if (eax91) 
        goto addr_7f73_39;
    *reinterpret_cast<void***>(&rdx63) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx63) + 4) = 0;
    r8_5 = r8_5;
    if (v92 == v93) 
        goto addr_856f_115;
    addr_8580_116:
    if ((v94 & 0xf000) == 0xa000 && ((v95 & 0xf000) == 0xa000 && *reinterpret_cast<unsigned char*>(rbx26 + 21))) {
        goto addr_7f73_39;
    }
    addr_856f_115:
    *reinterpret_cast<unsigned char*>(&rdx63) = reinterpret_cast<uint1_t>(v96 == v97);
    goto addr_8580_116;
    addr_8f8c_29:
    fun_3840();
    rsp52 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp51) - 8 + 8);
    addr_792e_34:
    fun_3bd0();
    rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp52) - 8 + 8);
    goto addr_7193_118;
    addr_71a6_119:
    eax98 = r12_46;
    return *reinterpret_cast<unsigned char*>(&eax98);
    while (1) {
        addr_8070_120:
        if (*reinterpret_cast<void***>(rbx26 + 24)) 
            goto addr_85ca_121;
        if ((reinterpret_cast<unsigned char>(v41) & 0xf000) == 0x4000) 
            goto addr_80a1_123; else 
            goto addr_808c_124;
        addr_8063_125:
        rsp99 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
        *rsp99 = reinterpret_cast<void**>(0x8068);
        restore_default_fscreatecon_or_die(rdi17, rsi19, rdx100, rcx22, r8_5, r9);
        rsp16 = reinterpret_cast<struct s3*>(rsp99 + 8);
        continue;
        addr_847d_126:
        goto addr_8063_125;
        while (1) {
            addr_8668_127:
            if (v31) {
                *reinterpret_cast<void***>(v31) = reinterpret_cast<void**>(1);
            }
            while (1) {
                addr_7210_129:
                r12_46 = reinterpret_cast<void**>(1);
                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                while (1) {
                    addr_7193_118:
                    rax101 = v34 - g28;
                    if (!rax101) 
                        goto addr_71a6_119;
                    rsp102 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp102 = reinterpret_cast<void**>(0xa367);
                    fun_3880();
                    rsp16 = reinterpret_cast<struct s3*>(rsp102 + 8);
                    addr_a367_104:
                    rsi19 = v20;
                    rdi17 = v24;
                    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                    rcx22 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                    rdx103 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffea0);
                    rsp104 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp104 = reinterpret_cast<void**>(0xa382);
                    eax105 = fun_3d10(rdi17, rsi19, rdx103, rdi17, rsi19, rdx103);
                    rsp16 = reinterpret_cast<struct s3*>(rsp104 + 8);
                    if (eax105) {
                        goto addr_7f70_61;
                    }
                    addr_918b_106:
                    if (v106 != v107 || v108 != v109) {
                        addr_7f70_61:
                        r13_29 = reinterpret_cast<void**>(0);
                    } else {
                        if (!*reinterpret_cast<unsigned char*>(rbx26 + 23)) {
                            addr_7136_87:
                            rsp110 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp110 = reinterpret_cast<void**>(0x714c);
                            rax111 = quotearg_n_style();
                            rbx26 = rax111;
                            rsp112 = rsp110 + 8 - 8;
                            *rsp112 = reinterpret_cast<void**>(0x715e);
                            quotearg_n_style();
                            rsp113 = reinterpret_cast<struct s3*>(rsp112 + 8);
                            goto addr_716d_133;
                        } else {
                            *reinterpret_cast<unsigned char*>(&r13_29) = reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_46 + 24)) & 0xf000) != 0xa000);
                        }
                    }
                    addr_7f73_39:
                    if (!*reinterpret_cast<signed char*>(rbx26 + 59)) 
                        goto addr_8070_120;
                    if ((reinterpret_cast<unsigned char>(v41) & 0xf000) == 0x4000) {
                        if (!*reinterpret_cast<void***>(rbx26 + 24)) 
                            goto addr_80a1_123;
                    } else {
                        r8_5 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                        if (*reinterpret_cast<signed char*>(rbx26 + 31) && (r8_5 = reinterpret_cast<void**>(1), *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0, !!*reinterpret_cast<void***>(rbx26 + 24))) {
                            r8_5 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            r8_5 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(v114 != v115)));
                        }
                        r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
                        rsi19 = v20;
                        rdi17 = v24;
                        *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                        rcx22 = r9;
                        rsp116 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp116 = reinterpret_cast<void**>(0x7fe0);
                        eax117 = utimecmpat(rdi17, rsi19, reinterpret_cast<int64_t>(rbp14) - 0x1f0, rcx22, r8_5);
                        rsp16 = reinterpret_cast<struct s3*>(rsp116 + 8);
                        if (eax117 < 0) 
                            goto addr_8f10_140; else 
                            goto addr_7fe8_141;
                    }
                    addr_85ca_121:
                    if (*reinterpret_cast<void***>(rbx26 + 8) == 2) 
                        goto addr_8668_127;
                    if (*reinterpret_cast<void***>(rbx26 + 8) == 3) 
                        goto addr_8639_143;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx26 + 8) == 4)) 
                        goto addr_80a1_123;
                    if (!*reinterpret_cast<signed char*>(rbx26 + 61)) 
                        goto addr_80a1_123;
                    eax118 = v119;
                    rdx3 = reinterpret_cast<void**>(eax118 & 0xf000);
                    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                    if (rdx3 == 0xa000) {
                        if (*reinterpret_cast<unsigned char*>(&r13_29)) 
                            goto addr_9f34_148;
                    } else {
                        rsp120 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp120 = reinterpret_cast<void**>(0x860d);
                        al121 = can_write_any_file(rdi17, rsi19);
                        rsp16 = reinterpret_cast<struct s3*>(rsp120 + 8);
                        if (al121 || (rdi122 = v24, *reinterpret_cast<int32_t*>(&rdi122 + 4) = 0, rcx22 = reinterpret_cast<void**>(0x200), *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0, rsi19 = v20, rsp123 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp123 = reinterpret_cast<void**>(0x8631), eax124 = fun_3790(rdi122, rsi19, 2, 0x200, r8_5, r9), rsp16 = reinterpret_cast<struct s3*>(rsp123 + 8), eax124 == 0)) {
                            addr_80a1_123:
                            if (*reinterpret_cast<unsigned char*>(&r13_29)) 
                                goto addr_7210_129;
                        } else {
                            addr_8639_143:
                            rcx22 = v20;
                            r8_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                            rsi19 = v18;
                            rsp125 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp125 = reinterpret_cast<void**>(0x865c);
                            al126 = overwrite_ok(rbx26, rsi19, v24, rcx22, r8_5, r9);
                            rsp16 = reinterpret_cast<struct s3*>(rsp125 + 8);
                            if (al126) 
                                goto addr_80a1_123; else 
                                goto addr_8664_150;
                        }
                        eax118 = v127;
                        rdx3 = reinterpret_cast<void**>(eax118 & 0xf000);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        if (rdx3 == 0x4000) {
                            rdx3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v41) & 0xf000);
                            *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                            if (rdx3 == 0x4000) {
                                addr_8137_153:
                                r12_46 = *reinterpret_cast<void***>(rbx26);
                                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                goto addr_813a_154;
                            } else {
                                addr_8cc8_155:
                                if (!*reinterpret_cast<void***>(rbx26 + 24) || (r12_46 = *reinterpret_cast<void***>(rbx26), *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0, r12_46 == 0)) {
                                    rsp128 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp128 = reinterpret_cast<void**>(0x965b);
                                    quotearg_style(4, v18, 4, v18);
                                    rsp129 = rsp128 + 8 - 8;
                                    *rsp129 = reinterpret_cast<void**>(0x9671);
                                    fun_3840();
                                    r12_46 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                    rsp130 = rsp129 + 8 - 8;
                                    *rsp130 = reinterpret_cast<void**>(0x9685);
                                    fun_3bd0();
                                    rsp16 = reinterpret_cast<struct s3*>(rsp130 + 8);
                                    continue;
                                }
                            }
                        }
                    }
                    r13d131 = reinterpret_cast<unsigned char>(v41) & 0xf000;
                    if (r13d131 == 0x4000) {
                        if (!*reinterpret_cast<void***>(rbx26 + 24) || (r12_46 = *reinterpret_cast<void***>(rbx26), *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0, r12_46 == 0)) {
                            rsp132 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp132 = reinterpret_cast<void**>(0x95ff);
                            rax133 = quotearg_n_style();
                            rbx26 = rax133;
                            rsp134 = rsp132 + 8 - 8;
                            *rsp134 = reinterpret_cast<void**>(0x9615);
                            quotearg_n_style();
                            rsp113 = reinterpret_cast<struct s3*>(rsp134 + 8);
                        } else {
                            if (!v27) 
                                goto addr_8cde_161;
                            goto addr_80eb_163;
                        }
                    } else {
                        r12_46 = *reinterpret_cast<void***>(rbx26);
                        *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                        if (!v27) {
                            addr_813a_154:
                            eax135 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
                            if (*reinterpret_cast<unsigned char*>(&eax135)) {
                                if ((v136 & 0xf000) != 0x4000) 
                                    goto addr_9336_166;
                                eax118 = v137;
                                goto addr_948d_168;
                            } else {
                                if (r12_46) 
                                    goto addr_9305_170; else 
                                    goto addr_814f_171;
                            }
                        } else {
                            addr_80eb_163:
                            if (r12_46 == 3) {
                                if (*reinterpret_cast<void***>(rbx26 + 24)) {
                                    addr_8cde_161:
                                    rdx3 = reinterpret_cast<void**>(v138 & 0xf000);
                                    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                                    if (rdx3 == 0x4000) 
                                        goto addr_948d_168; else 
                                        goto addr_8cf6_173;
                                } else {
                                    addr_9305_170:
                                    rsp139 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp139 = reinterpret_cast<void**>(0x930d);
                                    rax140 = last_component(r15_15, rsi19, rdx3, rcx22, r15_15, rsi19, rdx3, rcx22);
                                    rsp16 = reinterpret_cast<struct s3*>(rsp139 + 8);
                                    r13_141 = rax140;
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax140) == 46)) {
                                        addr_8d32_174:
                                        if ((v142 & 0xf000) == 0x4000) {
                                            addr_81e9_175:
                                            v48 = reinterpret_cast<void**>(0);
                                            r13_29 = reinterpret_cast<void**>(17);
                                            goto addr_6d20_36;
                                        } else {
                                            addr_8d48_176:
                                            if (r12_46 == 3 || ((rsp143 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp143 = reinterpret_cast<void**>(0x8d5a), rax144 = fun_3860(r13_141, r13_141), v36 = rax144, rsp145 = rsp143 + 8 - 8, *rsp145 = reinterpret_cast<void**>(0x8d6d), rax146 = last_component(v20, rsi19, rdx3, rcx22, v20, rsi19, rdx3, rcx22), rsp147 = rsp145 + 8 - 8, *rsp147 = reinterpret_cast<void**>(0x8d7c), rax148 = fun_3860(rax146, rax146), r12_149 = simple_backup_suffix, rsp150 = rsp147 + 8 - 8, *rsp150 = reinterpret_cast<void**>(0x8d92), rax151 = fun_3860(r12_149, r12_149), rsp16 = reinterpret_cast<struct s3*>(rsp150 + 8), !reinterpret_cast<int1_t>(v36 == reinterpret_cast<unsigned char>(rax151) + reinterpret_cast<unsigned char>(rax148))) || ((v36 = rax148, rsp152 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp152 = reinterpret_cast<void**>(0x8dbf), eax153 = fun_39c0(r13_141, rax146, rax148, r13_141, rax146, rax148), rsp16 = reinterpret_cast<struct s3*>(rsp152 + 8), !!eax153) || ((rdi154 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_141) + reinterpret_cast<unsigned char>(v36)), rsp155 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp155 = reinterpret_cast<void**>(0x8dd7), rax156 = fun_3a10(rdi154, r12_149, rdi154, r12_149), rsp16 = reinterpret_cast<struct s3*>(rsp155 + 8), !!*reinterpret_cast<int32_t*>(&rax156)) || ((rsp157 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp157 = reinterpret_cast<void**>(0x8dea), rax158 = fun_3860(v20, v20), rsi159 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(rax158)), rsp160 = rsp157 + 8 - 8, *rsp160 = reinterpret_cast<void**>(0x8dfa), rax161 = subst_suffix(v20, rsi159, r12_149, rcx22, r8_5, r9, v20, rsi159, r12_149, rcx22), rdi162 = v24, *reinterpret_cast<int32_t*>(&rdi162 + 4) = 0, rcx22 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0, rdx163 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30), rsp164 = rsp160 + 8 - 8, *rsp164 = reinterpret_cast<void**>(0x8e14), eax165 = fun_3d10(rdi162, rax161, rdx163, rdi162, rax161, rdx163), rsp166 = rsp164 + 8 - 8, *rsp166 = reinterpret_cast<void**>(0x8e1f), fun_36a0(rax161, rax161), rsp16 = reinterpret_cast<struct s3*>(rsp166 + 8), !!eax165) || (v167 != v106 || v168 != v108)))))) {
                                                rdx3 = *reinterpret_cast<void***>(rbx26);
                                                *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                                                *reinterpret_cast<void***>(&rdi169) = v24;
                                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi169) + 4) = 0;
                                                rsp170 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                *rsp170 = reinterpret_cast<void**>(0x8e4f);
                                                rax171 = backup_file_rename(rdi169, v20, rdx3, rcx22, r8_5, r9);
                                                rsp172 = reinterpret_cast<struct s3*>(rsp170 + 8);
                                                r12_173 = rax171;
                                                if (!rax171) {
                                                    rsp174 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp172) - 8);
                                                    *rsp174 = reinterpret_cast<void**>(0xacd3);
                                                    rax175 = fun_3700();
                                                    rsp16 = reinterpret_cast<struct s3*>(rsp174 + 8);
                                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax175) == 2)) {
                                                        rsp176 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                        *rsp176 = reinterpret_cast<void**>(0xad1f);
                                                        quotearg_style(4, v18, 4, v18);
                                                        rsp58 = reinterpret_cast<struct s3*>(rsp176 + 8);
                                                        addr_785c_59:
                                                        rsp177 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp58) - 8);
                                                        *rsp177 = reinterpret_cast<void**>(0x7863);
                                                        fun_3840();
                                                        r12_46 = reinterpret_cast<void**>(0);
                                                        *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                                        rsp178 = rsp177 + 8 - 8;
                                                        *rsp178 = reinterpret_cast<void**>(0x7879);
                                                        fun_3bd0();
                                                        rsp16 = reinterpret_cast<struct s3*>(rsp178 + 8);
                                                        continue;
                                                    } else {
                                                        *reinterpret_cast<unsigned char*>(&v36) = 1;
                                                        r13_29 = reinterpret_cast<void**>(17);
                                                        v48 = reinterpret_cast<void**>(0);
                                                        goto addr_6d20_36;
                                                    }
                                                } else {
                                                    r13_179 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(v18));
                                                    rsp180 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp172) - 8);
                                                    *rsp180 = reinterpret_cast<void**>(0x8e6a);
                                                    rax181 = fun_3860(rax171, rax171);
                                                    rsp182 = reinterpret_cast<struct s3*>(rsp180 + 8);
                                                    r8_5 = rax181 + 1;
                                                    rax183 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r13_179) + reinterpret_cast<unsigned char>(rax181) + 24);
                                                    rsi184 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp182) - (reinterpret_cast<uint64_t>(rax183) & 0xfffffffffffff000));
                                                    rcx185 = reinterpret_cast<uint64_t>(rax183) & 0xfffffffffffffff0;
                                                    if (rsp182 != rsi184) {
                                                        do {
                                                            rsp182 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp182) - reinterpret_cast<int64_t>("__libc_start_main"));
                                                            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp182) + reinterpret_cast<int64_t>("l")) = 0x8e6a;
                                                        } while (rsp182 != rsi184);
                                                    }
                                                    rcx22 = reinterpret_cast<void**>(*reinterpret_cast<uint32_t*>(&rcx185) & 0xfff);
                                                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                                    rsp186 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp182) - reinterpret_cast<unsigned char>(rcx22));
                                                    if (rcx22) {
                                                        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp186) + reinterpret_cast<unsigned char>(rcx22) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp186) + reinterpret_cast<unsigned char>(rcx22) - 8);
                                                    }
                                                    rax187 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp186) + 15 & 0xfffffffffffffff0);
                                                    r13_29 = reinterpret_cast<void**>(17);
                                                    v48 = rax187;
                                                    rsp188 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp186) - 8);
                                                    *rsp188 = 0x8ee3;
                                                    rax189 = fun_3bb0(rax187, v18, r13_179);
                                                    rdx3 = r8_5;
                                                    rsp190 = rsp188 + 1 - 1;
                                                    *rsp190 = 0x8ef5;
                                                    fun_3ab0(rax189, r12_173, rdx3);
                                                    rsp191 = rsp190 + 1 - 1;
                                                    *rsp191 = 0x8efd;
                                                    fun_36a0(r12_173, r12_173);
                                                    rsp16 = reinterpret_cast<struct s3*>(rsp191 + 1);
                                                    *reinterpret_cast<unsigned char*>(&v36) = 1;
                                                    goto addr_6d20_36;
                                                }
                                            } else {
                                                if (!*reinterpret_cast<void***>(rbx26 + 24)) {
                                                    rsp192 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                    *rsp192 = reinterpret_cast<void**>(0xa871);
                                                    fun_3840();
                                                    rsp193 = reinterpret_cast<struct s3*>(rsp192 + 8);
                                                } else {
                                                    rsp194 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                    *rsp194 = reinterpret_cast<void**>(0xa825);
                                                    fun_3840();
                                                    rsp193 = reinterpret_cast<struct s3*>(rsp194 + 8);
                                                }
                                                rsp195 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp193) - 8);
                                                *rsp195 = reinterpret_cast<void**>(0xa83a);
                                                rax196 = quotearg_n_style();
                                                rbx26 = rax196;
                                                rsp197 = rsp195 + 8 - 8;
                                                *rsp197 = reinterpret_cast<void**>(0xa850);
                                                quotearg_n_style();
                                                rsp198 = reinterpret_cast<struct s3*>(rsp197 + 8);
                                                r8_5 = rbx26;
                                                goto addr_717d_190;
                                            }
                                        }
                                    } else {
                                        eax135 = 0;
                                        goto addr_8d0d_192;
                                    }
                                }
                            } else {
                                rdi199 = *reinterpret_cast<void***>(rbx26 + 72);
                                rsi19 = v20;
                                rdx3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                                rsp200 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp200 = reinterpret_cast<void**>(0x810c);
                                eax201 = seen_file(rdi199, rsi19, rdx3, rcx22, rdi199, rsi19, rdx3, rcx22);
                                rsp16 = reinterpret_cast<struct s3*>(rsp200 + 8);
                                if (*reinterpret_cast<signed char*>(&eax201)) {
                                    rsp202 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp202 = reinterpret_cast<void**>(0x9c1e);
                                    rax203 = quotearg_n_style();
                                    rbx26 = rax203;
                                    rsp204 = rsp202 + 8 - 8;
                                    *rsp204 = reinterpret_cast<void**>(0x9c34);
                                    quotearg_n_style();
                                    rsp113 = reinterpret_cast<struct s3*>(rsp204 + 8);
                                } else {
                                    if (r13d131 == 0x4000) 
                                        goto addr_8137_153;
                                    eax118 = v205;
                                    rdx3 = reinterpret_cast<void**>(eax118 & 0xf000);
                                    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                                    if (rdx3 == 0x4000) 
                                        goto addr_8cc8_155; else 
                                        goto addr_8137_153;
                                }
                            }
                        }
                    }
                    addr_716d_133:
                    rsp206 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp113) - 8);
                    *rsp206 = reinterpret_cast<void**>(0x7174);
                    fun_3840();
                    rsp198 = reinterpret_cast<struct s3*>(rsp206 + 8);
                    r8_5 = rbx26;
                    addr_717d_190:
                    rsp207 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp198) - 8);
                    *rsp207 = reinterpret_cast<void**>(0x7188);
                    fun_3bd0();
                    rsp16 = reinterpret_cast<struct s3*>(rsp207 + 8);
                    goto addr_7190_197;
                    addr_948d_168:
                    if ((eax118 & 0xf000) == 0x4000) {
                        addr_9336_166:
                        if (!r12_46) 
                            goto addr_81e9_175;
                    } else {
                        if (r12_46) {
                            addr_8cf6_173:
                            rsp208 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp208 = reinterpret_cast<void**>(0x8cfe);
                            rax209 = last_component(r15_15, rsi19, rdx3, rcx22, r15_15, rsi19, rdx3, rcx22);
                            rsp16 = reinterpret_cast<struct s3*>(rsp208 + 8);
                            r13_141 = rax209;
                            eax135 = 1;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_141) == 46)) 
                                goto addr_8d48_176; else 
                                goto addr_8d0d_192;
                        } else {
                            rsp210 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp210 = reinterpret_cast<void**>(0x94b9);
                            rax211 = quotearg_n_style_colon();
                            rbx26 = rax211;
                            rsp212 = rsp210 + 8 - 8;
                            *rsp212 = reinterpret_cast<void**>(0x94cb);
                            quotearg_n_style_colon();
                            rsp113 = reinterpret_cast<struct s3*>(rsp212 + 8);
                            goto addr_716d_133;
                        }
                    }
                    goto addr_8cf6_173;
                    addr_8d0d_192:
                    *reinterpret_cast<int32_t*>(&rdx213) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx213) + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rdx213) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r13_141 + 1) == 46);
                    rdx3 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_141) + reinterpret_cast<uint64_t>(rdx213) + 1)));
                    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                    if (!*reinterpret_cast<signed char*>(&rdx3) || *reinterpret_cast<signed char*>(&rdx3) == 47) {
                        addr_814f_171:
                        r13_29 = reinterpret_cast<void**>(17);
                        v48 = reinterpret_cast<void**>(0);
                        rdx3 = reinterpret_cast<void**>(v214 & 0xf000);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r12_46) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdx3 == 0x4000)) | *reinterpret_cast<unsigned char*>(&eax135));
                        if (*reinterpret_cast<unsigned char*>(&r12_46)) {
                            addr_6d20_36:
                            if (!v27) 
                                goto addr_6d2d_201;
                            addr_6f70_24:
                            if (!*reinterpret_cast<void***>(rbx26 + 72)) 
                                goto addr_6d2d_201;
                            r12_46 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))));
                            *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&r12_46)) 
                                goto addr_6d2d_201;
                        } else {
                            if (*reinterpret_cast<unsigned char*>(rbx26 + 21)) 
                                goto addr_81a7_204;
                            if (!*reinterpret_cast<unsigned char*>(rbx26 + 49)) 
                                goto addr_6d20_36; else 
                                goto addr_818f_206;
                        }
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&eax135)) 
                            goto addr_8d48_176; else 
                            goto addr_8d32_174;
                    }
                    rcx22 = *reinterpret_cast<void***>(rbx26);
                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                    if (rcx22) {
                        if (!*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                            if (!r13_29) {
                                goto addr_7238_212;
                            }
                        }
                    }
                    rdx3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                    if (*reinterpret_cast<unsigned char*>(&r14_49)) 
                        goto addr_6fa3_214;
                    r14_49 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30);
                    rdi215 = v24;
                    *reinterpret_cast<int32_t*>(&rdi215 + 4) = 0;
                    rcx22 = reinterpret_cast<void**>(0x100);
                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                    rdx3 = r14_49;
                    rsp216 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp216 = reinterpret_cast<void**>(0x86a9);
                    eax217 = fun_3d10(rdi215, v20, rdx3, rdi215, v20, rdx3);
                    rsp16 = reinterpret_cast<struct s3*>(rsp216 + 8);
                    if (eax217) {
                        addr_6d2d_201:
                        if (!*reinterpret_cast<unsigned char*>(rbx26 + 60) || *reinterpret_cast<void***>(rbx26 + 24)) {
                            addr_6d3d_216:
                            if (!r13_29) {
                                if (!*reinterpret_cast<void***>(rbx26 + 24)) {
                                    addr_7238_212:
                                    r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v36)));
                                    *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                                    *reinterpret_cast<int32_t*>(&r14_49 + 4) = 0;
                                } else {
                                    addr_71c6_218:
                                    if (*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                                        rsp218 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp218 = reinterpret_cast<void**>(0x8763);
                                        rax219 = fun_3840();
                                        rsp220 = rsp218 + 8 - 8;
                                        *rsp220 = reinterpret_cast<void**>(0x8772);
                                        fun_3b80(1, rax219, 5, rcx22, 1, rax219, 5, rcx22);
                                        rsp221 = rsp220 + 8 - 8;
                                        *rsp221 = reinterpret_cast<void**>(0x8788);
                                        emit_verbose(r15_15, v18, v48, rcx22, r8_5, r9);
                                        rsp16 = reinterpret_cast<struct s3*>(rsp221 + 8);
                                        goto addr_71d0_220;
                                    }
                                }
                            } else {
                                if (*reinterpret_cast<void***>(rbx26 + 56) && (reinterpret_cast<unsigned char>(v41) & 0xf000) == 0x4000) {
                                    if (v27) {
                                        rsp222 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp222 = reinterpret_cast<void**>(0x834f);
                                        rax225 = remember_copied(v20, v223, v224, rcx22, r8_5, r9);
                                        rsp16 = reinterpret_cast<struct s3*>(rsp222 + 8);
                                        r14_49 = rax225;
                                    } else {
                                        rsp226 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp226 = reinterpret_cast<void**>(0x6d89);
                                        rax227 = src_to_dest_lookup(v223, v224, v224, rcx22, r8_5, r9);
                                        rsp16 = reinterpret_cast<struct s3*>(rsp226 + 8);
                                        r14_49 = rax227;
                                    }
                                    if (!r14_49) 
                                        goto addr_6df0_226; else 
                                        goto addr_6d91_227;
                                }
                            }
                        } else {
                            if ((reinterpret_cast<unsigned char>(v41) & 0xf000) != 0x4000) {
                                rdx3 = v48;
                                rsp228 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp228 = reinterpret_cast<void**>(0x76fc);
                                emit_verbose(r15_15, v18, rdx3, rcx22, r8_5, r9);
                                rsp16 = reinterpret_cast<struct s3*>(rsp228 + 8);
                                goto addr_6d3d_216;
                            }
                        }
                    } else {
                        rdx3 = r14_49;
                        goto addr_6fa3_214;
                    }
                    addr_7243_231:
                    r12_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v41) & 0xfff);
                    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                    if (*reinterpret_cast<signed char*>(rbx26 + 57)) {
                        r12_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 16)) & 0xfff);
                        *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                    }
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 29)) {
                        edx229 = v41;
                        r8_5 = rbx26;
                        rcx22 = r13_29;
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        rsi230 = v18;
                        rsp231 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp231 = reinterpret_cast<void**>(0x72eb);
                        eax232 = set_process_security_ctx(r15_15, rsi230);
                        rsp16 = reinterpret_cast<struct s3*>(rsp231 + 8);
                        if (!*reinterpret_cast<signed char*>(&eax232)) 
                            goto addr_7190_197;
                        v233 = reinterpret_cast<unsigned char>(r12_46) & 63;
                        eax234 = reinterpret_cast<unsigned char>(v41) & 0xf000;
                        *reinterpret_cast<uint32_t*>(&v31) = eax234;
                        if (eax234 != 0x4000) 
                            goto addr_731b_236;
                    } else {
                        eax235 = reinterpret_cast<unsigned char>(v41) & 0xf000;
                        *reinterpret_cast<uint32_t*>(&v31) = eax235;
                        if (eax235 == 0x4000) {
                            r8_5 = rbx26;
                            rsp236 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp236 = reinterpret_cast<void**>(0x77a3);
                            eax237 = set_process_security_ctx(r15_15, v18, r15_15, v18);
                            rsp16 = reinterpret_cast<struct s3*>(rsp236 + 8);
                            if (!*reinterpret_cast<signed char*>(&eax237)) 
                                goto addr_7190_197;
                            v233 = reinterpret_cast<unsigned char>(r12_46) & 18;
                        } else {
                            edx229 = v41;
                            r8_5 = rbx26;
                            rcx22 = r13_29;
                            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                            rsi230 = v18;
                            rsp238 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp238 = reinterpret_cast<void**>(0x729f);
                            eax239 = set_process_security_ctx(r15_15, rsi230);
                            rsp16 = reinterpret_cast<struct s3*>(rsp238 + 8);
                            v233 = 0;
                            if (!*reinterpret_cast<signed char*>(&eax239)) {
                                goto addr_7190_197;
                            }
                        }
                    }
                    rax240 = v21;
                    rdx100 = v241;
                    rcx22 = v242;
                    if (rax240) {
                        do {
                            if (*reinterpret_cast<void***>(rax240 + 8) != rdx100) 
                                continue;
                            if (*reinterpret_cast<void***>(rax240 + 16) == rcx22) 
                                break;
                            rax240 = *reinterpret_cast<void***>(rax240);
                        } while (rax240);
                        goto addr_7b98_246;
                    } else {
                        goto addr_7b98_246;
                    }
                    rsp243 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp243 = reinterpret_cast<void**>(0x7805);
                    rax244 = quotearg_style(4, r15_15, 4, r15_15);
                    rsp245 = reinterpret_cast<struct s3*>(rsp243 + 8);
                    r12_246 = rax244;
                    addr_83d8_249:
                    rsp247 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp245) - 8);
                    *rsp247 = reinterpret_cast<void**>(0x83df);
                    rax248 = fun_3840();
                    rcx22 = r12_246;
                    rsi19 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rdi17 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                    rdx100 = rax248;
                    rsp249 = rsp247 + 8 - 8;
                    *rsp249 = reinterpret_cast<void**>(0x83f0);
                    fun_3bd0();
                    rsp16 = reinterpret_cast<struct s3*>(rsp249 + 8);
                    goto addr_7ae0_250;
                    addr_7b98_246:
                    rax250 = rsp16;
                    if (rsp16 != rax250) {
                        do {
                            rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - reinterpret_cast<int64_t>("__libc_start_main"));
                            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp16) + reinterpret_cast<int64_t>("l")) = 0x72eb;
                        } while (rsp16 != rax250);
                    }
                    rsp16 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rsp16) - 32);
                    rsp16->f18 = 0x72eb;
                    rax251 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(rsp16) + 15 & 0xfffffffffffffff0);
                    v252 = rax251;
                    rax251->f0 = v21;
                    rax251->f8 = rdx100;
                    rax251->f10 = rcx22;
                    if (*reinterpret_cast<unsigned char*>(&v36) || (v253 & 0xf000) != 0x4000) {
                        *reinterpret_cast<void***>(&rdi254) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi254) + 4) = 0;
                        rsp255 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp255 = reinterpret_cast<void**>(0x7c1d);
                        eax256 = fun_3a80(rdi254, v20);
                        rsp257 = reinterpret_cast<struct s3*>(rsp255 + 8);
                        if (eax256) {
                            rsp258 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp257) - 8);
                            *rsp258 = reinterpret_cast<void**>(0x8c79);
                            rax259 = quotearg_style(4, v18, 4, v18);
                            rsp260 = reinterpret_cast<struct s3*>(rsp258 + 8);
                            r13_29 = rax259;
                        } else {
                            rdi261 = v24;
                            *reinterpret_cast<int32_t*>(&rdi261 + 4) = 0;
                            rcx22 = reinterpret_cast<void**>(0x100);
                            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                            rdx100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                            rsp262 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp257) - 8);
                            *rsp262 = reinterpret_cast<void**>(0x7c43);
                            eax263 = fun_3d10(rdi261, v20, rdx100, rdi261, v20, rdx100);
                            rsp16 = reinterpret_cast<struct s3*>(rsp262 + 8);
                            if (eax263) {
                                rsp264 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp264 = reinterpret_cast<void**>(0x9443);
                                rax265 = quotearg_style(4, v18, 4, v18);
                                rsp260 = reinterpret_cast<struct s3*>(rsp264 + 8);
                                r13_29 = rax265;
                            } else {
                                if ((v266 & 0x1c0) == 0x1c0) {
                                    v267 = 0;
                                    goto addr_7c97_259;
                                }
                                rcx22 = reinterpret_cast<void**>(0x100);
                                *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                rdx100 = reinterpret_cast<void**>(v266 | 0x1c0);
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                *reinterpret_cast<void***>(&rdi268) = v24;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi268) + 4) = 0;
                                rsp269 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp269 = reinterpret_cast<void**>(0x7c88);
                                eax270 = fun_3b00(rdi268, v20, rdi268, v20);
                                rsp16 = reinterpret_cast<struct s3*>(rsp269 + 8);
                                v267 = 1;
                                if (eax270) {
                                    rsp271 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp271 = reinterpret_cast<void**>(0x98d3);
                                    rax272 = quotearg_style(4, v18, 4, v18);
                                    rsp260 = reinterpret_cast<struct s3*>(rsp271 + 8);
                                    r13_29 = rax272;
                                } else {
                                    addr_7c97_259:
                                    if (!*reinterpret_cast<void***>(v28)) {
                                        rdx100 = v273;
                                        rsp274 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp274 = reinterpret_cast<void**>(0x93d5);
                                        remember_copied(v20, v275, rdx100, 0x100, r8_5, r9);
                                        rsp16 = reinterpret_cast<struct s3*>(rsp274 + 8);
                                        *reinterpret_cast<void***>(v28) = reinterpret_cast<void**>(1);
                                        goto addr_7ca7_263;
                                    }
                                }
                            }
                        }
                    } else {
                        if (!*reinterpret_cast<void***>(rbx26 + 40) && !*reinterpret_cast<signed char*>(rbx26 + 51)) {
                            goto addr_8f4a_266;
                        }
                        rdi17 = v18;
                        rsi19 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rdx100 = rbx26;
                        rsp276 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp276 = reinterpret_cast<void**>(0x8f3c);
                        eax277 = set_file_security_ctx(rdi17, rdi17);
                        rsp16 = reinterpret_cast<struct s3*>(rsp276 + 8);
                        if (*reinterpret_cast<signed char*>(&eax277) || !*reinterpret_cast<unsigned char*>(rbx26 + 52)) {
                            addr_8f4a_266:
                            v267 = 0;
                            v233 = 0;
                            goto addr_7cf0_268;
                        } else {
                            addr_7ae0_250:
                            if (*reinterpret_cast<signed char*>(rbx26 + 51)) 
                                goto addr_8063_125; else 
                                goto addr_7aea_269;
                        }
                    }
                    addr_8c88_270:
                    rsp278 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp260) - 8);
                    *rsp278 = reinterpret_cast<void**>(0x8c8f);
                    rax279 = fun_3840();
                    rsp280 = rsp278 + 8 - 8;
                    *rsp280 = reinterpret_cast<void**>(0x8c97);
                    rax281 = fun_3700();
                    rcx22 = r13_29;
                    rdx100 = rax279;
                    rdi17 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                    rsi19 = *reinterpret_cast<void***>(rax281);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp282 = rsp280 + 8 - 8;
                    *rsp282 = reinterpret_cast<void**>(0x8ca8);
                    fun_3bd0();
                    rsp16 = reinterpret_cast<struct s3*>(rsp282 + 8);
                    goto addr_7ae0_250;
                    addr_7ca7_263:
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                        if (!*reinterpret_cast<void***>(rbx26 + 24)) {
                            rdx100 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsp283 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp283 = reinterpret_cast<void**>(0x9571);
                            emit_verbose(r15_15, v18, 0, 0x100, r8_5, r9);
                            rsp16 = reinterpret_cast<struct s3*>(rsp283 + 8);
                        } else {
                            rsp284 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp284 = reinterpret_cast<void**>(0x7cc8);
                            rax285 = quotearg_style(4, v18, 4, v18);
                            r12_46 = rax285;
                            rsp286 = rsp284 + 8 - 8;
                            *rsp286 = reinterpret_cast<void**>(0x7cde);
                            rax287 = fun_3840();
                            rdx100 = r12_46;
                            rsp288 = rsp286 + 8 - 8;
                            *rsp288 = reinterpret_cast<void**>(0x7cf0);
                            fun_3b80(1, rax287, rdx100, 0x100, 1, rax287, rdx100, 0x100);
                            rsp16 = reinterpret_cast<struct s3*>(rsp288 + 8);
                        }
                    }
                    addr_7cf0_268:
                    *reinterpret_cast<unsigned char*>(&r12_46) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v25)) & *reinterpret_cast<unsigned char*>(rbx26 + 28));
                    if (*reinterpret_cast<unsigned char*>(&r12_46)) {
                        if (*reinterpret_cast<void***>(v25) != v289) {
                            addr_7f31_275:
                            if (v27) {
                                eax290 = v267;
                                r9 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                r13_29 = reinterpret_cast<void**>(0);
                                r8_5 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                v32 = *reinterpret_cast<signed char*>(&eax290);
                            } else {
                                eax291 = v267;
                                r13_29 = reinterpret_cast<void**>(0);
                                v32 = *reinterpret_cast<signed char*>(&eax291);
                                goto addr_7468_278;
                            }
                        } else {
                            goto addr_7d08_280;
                        }
                    } else {
                        addr_7d08_280:
                        __asm__("movdqu xmm0, [rbx]");
                        __asm__("movdqu xmm1, [rbx+0x10]");
                        __asm__("movdqu xmm2, [rbx+0x20]");
                        __asm__("movdqu xmm3, [rbx+0x30]");
                        __asm__("movdqu xmm4, [rbx+0x40]");
                        __asm__("movaps [rbp-0x300], xmm0");
                        __asm__("movaps [rbp-0x2f0], xmm1");
                        __asm__("movaps [rbp-0x2e0], xmm2");
                        __asm__("movaps [rbp-0x2d0], xmm3");
                        __asm__("movaps [rbp-0x2c0], xmm4");
                        rsp292 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp292 = reinterpret_cast<void**>(0x7d5b);
                        rax293 = savedir(r15_15, 2, rdx100, rcx22, r8_5, r9);
                        rsp294 = reinterpret_cast<struct s3*>(rsp292 + 8);
                        v295 = rax293;
                        if (!rax293) {
                            rsp296 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp294) - 8);
                            *rsp296 = reinterpret_cast<void**>(0xac3c);
                            quotearg_style(4, r15_15, 4, r15_15);
                            rsp297 = rsp296 + 8 - 8;
                            *rsp297 = reinterpret_cast<void**>(0xac52);
                            fun_3840();
                            rsp298 = rsp297 + 8 - 8;
                            *rsp298 = reinterpret_cast<void**>(0xac5a);
                            fun_3700();
                            r12_46 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            rsp299 = rsp298 + 8 - 8;
                            *rsp299 = reinterpret_cast<void**>(0xac6e);
                            fun_3bd0();
                            rsp16 = reinterpret_cast<struct s3*>(rsp299 + 8);
                            goto addr_7f31_275;
                        } else {
                            if (*reinterpret_cast<signed char*>(rbx26 + 4) == 3) {
                                *reinterpret_cast<int32_t*>(&v300 + 4) = 2;
                            }
                            if (!*reinterpret_cast<void***>(v295)) {
                                *reinterpret_cast<unsigned char*>(&v21) = 0;
                                r12_46 = reinterpret_cast<void**>(1);
                                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            } else {
                                v301 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(v18));
                                v302 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf8);
                                v25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf6);
                                v47 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf7);
                                v303 = r13_29;
                                r13_304 = v295;
                                v305 = rbx26;
                                rbx306 = v30;
                                *reinterpret_cast<unsigned char*>(&v21) = 0;
                                v307 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd00);
                                v31 = reinterpret_cast<void**>(1);
                                v308 = r15_15;
                                v30 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
                                do {
                                    rsp309 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp294) - 8);
                                    *rsp309 = reinterpret_cast<void**>(0x7e5f);
                                    rax310 = file_name_concat(v308, r13_304);
                                    rsp311 = rsp309 + 8 - 8;
                                    *rsp311 = reinterpret_cast<void**>(0x7e73);
                                    rax312 = file_name_concat(v18, r13_304);
                                    r9 = v30;
                                    r8_5 = v303;
                                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                    eax313 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v28));
                                    rsp314 = rsp311 + 8 - 8;
                                    *rsp314 = v302;
                                    rsp315 = rsp314 - 8;
                                    *rsp315 = v25;
                                    v316 = *reinterpret_cast<unsigned char*>(&eax313);
                                    rsp317 = rsp315 - 8;
                                    *rsp317 = v47;
                                    rsp318 = rsp317 - 8;
                                    *rsp318 = reinterpret_cast<void**>(0);
                                    rsp319 = reinterpret_cast<struct s5*>(rsp318 - 8);
                                    rsp319->f0 = v307;
                                    rsp320 = reinterpret_cast<struct s4**>(reinterpret_cast<uint64_t>(rsp319) - 8);
                                    *rsp320 = v252;
                                    rsp321 = reinterpret_cast<struct s6*>(rsp320 - 1);
                                    rsp321->f0 = 0x7ed0;
                                    al322 = copy_internal(rax310, rax312, v24, reinterpret_cast<unsigned char>(rax312) + reinterpret_cast<uint64_t>(v301), r8_5, r9, rsp321->f8, rsp321->f10, 0, rsp321->f20, rsp321->f28, rsp321->f30);
                                    *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                                    *reinterpret_cast<void***>(rbx306) = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx306))));
                                    v31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v31) & al322);
                                    rsp323 = &rsp321->f8 + 48 - 8;
                                    *rsp323 = reinterpret_cast<void**>(0x7eed);
                                    fun_36a0(rax312, rax312);
                                    rsp324 = rsp323 + 8 - 8;
                                    *rsp324 = reinterpret_cast<void**>(0x7ef5);
                                    fun_36a0(rax310, rax310);
                                    rsp294 = reinterpret_cast<struct s3*>(rsp324 + 8);
                                    if (!1) 
                                        break;
                                    esi325 = v316;
                                    *reinterpret_cast<unsigned char*>(&v21) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&v21) | *reinterpret_cast<unsigned char*>(&esi325));
                                    rsp326 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp294) - 8);
                                    *rsp326 = reinterpret_cast<void**>(0x7e3e);
                                    rax327 = fun_3860(r13_304, r13_304);
                                    rsp294 = reinterpret_cast<struct s3*>(rsp326 + 8);
                                    r13_304 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_304) + reinterpret_cast<unsigned char>(rax327) + 1);
                                } while (*reinterpret_cast<void***>(r13_304));
                                r12_46 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v31)));
                                *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                r15_15 = v308;
                                rbx26 = v305;
                            }
                            rsp328 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp294) - 8);
                            *rsp328 = reinterpret_cast<void**>(0x7f20);
                            fun_36a0(v295, v295);
                            rsp16 = reinterpret_cast<struct s3*>(rsp328 + 8);
                            edi329 = *reinterpret_cast<unsigned char*>(&v21);
                            *reinterpret_cast<void***>(v28) = *reinterpret_cast<void***>(&edi329);
                            goto addr_7f31_275;
                        }
                    }
                    addr_73df_291:
                    if (*reinterpret_cast<void***>(rbx26 + 72) && (rdi330 = v24, *reinterpret_cast<int32_t*>(&rdi330 + 4) = 0, r14_49 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30), v31 = r8_5, v27 = r9, rsp331 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp331 = reinterpret_cast<void**>(0x7415), eax332 = fun_3d10(rdi330, v20, r14_49, rdi330, v20, r14_49), rsp16 = reinterpret_cast<struct s3*>(rsp331 + 8), r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v27))), *reinterpret_cast<int32_t*>(&r9 + 4) = 0, r8_5 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v31))), *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0, !eax332)) {
                        rdi333 = *reinterpret_cast<void***>(rbx26 + 72);
                        rsp334 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp334 = reinterpret_cast<void**>(0x743c);
                        record_file(rdi333, v20, r14_49, 0x100);
                        rsp16 = reinterpret_cast<struct s3*>(rsp334 + 8);
                        r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v27)));
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        r8_5 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v31)));
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    }
                    addr_7450_293:
                    if (!*reinterpret_cast<unsigned char*>(rbx26 + 23)) 
                        goto addr_745f_294;
                    if (r8_5) 
                        continue;
                    addr_745f_294:
                    if (r9) 
                        continue;
                    addr_7468_278:
                    if (!*reinterpret_cast<signed char*>(rbx26 + 31)) 
                        goto addr_7520_296;
                    rdx335 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - 0xd0);
                    *reinterpret_cast<void***>(&rdi336) = v24;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi336) + 4) = 0;
                    v108 = v337;
                    v106 = v338;
                    v339 = v340;
                    rsp341 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp341 = reinterpret_cast<void**>(0x74ca);
                    rax342 = fun_36e0(rdi336, v20, rdx335, rdi336, v20, rdx335);
                    rsp16 = reinterpret_cast<struct s3*>(rsp341 + 8);
                    if (!*reinterpret_cast<int32_t*>(&rax342)) 
                        goto addr_7520_296;
                    rsp343 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp343 = reinterpret_cast<void**>(0x74df);
                    rax344 = quotearg_style(4, v18, 4, v18);
                    v27 = rax344;
                    rsp345 = rsp343 + 8 - 8;
                    *rsp345 = reinterpret_cast<void**>(0x74f9);
                    rax346 = fun_3840();
                    r14_49 = rax346;
                    rsp347 = rsp345 + 8 - 8;
                    *rsp347 = reinterpret_cast<void**>(0x7501);
                    fun_3700();
                    rsp348 = rsp347 + 8 - 8;
                    *rsp348 = reinterpret_cast<void**>(0x7516);
                    fun_3bd0();
                    rsp16 = reinterpret_cast<struct s3*>(rsp348 + 8);
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 50)) 
                        goto addr_7190_197;
                    addr_7520_296:
                    if (*reinterpret_cast<unsigned char*>(&r13_29)) 
                        continue;
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 29)) 
                        goto addr_752f_300;
                    addr_75ae_301:
                    if (0xff0000000000ff & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))) {
                        r8_5 = v41;
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                        rsp349 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp349 = reinterpret_cast<void**>(0x8fe0);
                        eax350 = copy_acl(r15_15, 0xffffffff, v18, 0xffffffff, r8_5, r9);
                        rsp16 = reinterpret_cast<struct s3*>(rsp349 + 8);
                        if (!eax350) 
                            continue;
                    } else {
                        if (*reinterpret_cast<signed char*>(rbx26 + 57)) {
                            goto addr_92dd_306;
                        } else {
                            eax351 = *reinterpret_cast<unsigned char*>(&v36);
                            if (*reinterpret_cast<unsigned char*>(&eax351) & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 32))) {
                                rbx26 = mask_0;
                                *reinterpret_cast<int32_t*>(&rbx26 + 4) = 0;
                                if ((reinterpret_cast<unsigned char>(v41) & 0x7000) != 0x4000) {
                                }
                                if (rbx26 == 0xffffffff) {
                                    rsp352 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp352 = reinterpret_cast<void**>(0x9f17);
                                    eax353 = fun_3a90();
                                    rbx26 = eax353;
                                    *reinterpret_cast<int32_t*>(&rbx26 + 4) = 0;
                                    mask_0 = eax353;
                                    rsp354 = rsp352 + 8 - 8;
                                    *rsp354 = reinterpret_cast<void**>(0x9f26);
                                    fun_3a90();
                                    rsp16 = reinterpret_cast<struct s3*>(rsp354 + 8);
                                }
                                goto addr_92dd_306;
                            } else {
                                if (v233) {
                                    r13d355 = mask_0;
                                    if (r13d355 == 0xffffffff) {
                                        rsp356 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp356 = reinterpret_cast<void**>(0x9f9e);
                                        eax357 = fun_3a90();
                                        r13d355 = eax357;
                                        mask_0 = eax357;
                                        rsp358 = rsp356 + 8 - 8;
                                        *rsp358 = reinterpret_cast<void**>(0x9fae);
                                        fun_3a90();
                                        rsp16 = reinterpret_cast<struct s3*>(rsp358 + 8);
                                    }
                                    v233 = v233 & ~reinterpret_cast<unsigned char>(r13d355);
                                    if (!v233) 
                                        goto addr_75ea_317;
                                } else {
                                    addr_75ea_317:
                                    if (!v32) 
                                        continue; else 
                                        goto addr_75f7_318;
                                }
                                if (v32 == 1) {
                                    addr_75f7_318:
                                    *reinterpret_cast<void***>(&rdi359) = v24;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi359) + 4) = 0;
                                    rsp360 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp360 = reinterpret_cast<void**>(0x761a);
                                    eax361 = fun_3b00(rdi359, v20);
                                    rsp16 = reinterpret_cast<struct s3*>(rsp360 + 8);
                                    if (!eax361) 
                                        continue; else 
                                        goto addr_7622_320;
                                } else {
                                    if (!*reinterpret_cast<unsigned char*>(&v36) || (rdi362 = v24, *reinterpret_cast<int32_t*>(&rdi362 + 4) = 0, rdx363 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10), rsp364 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp364 = reinterpret_cast<void**>(0x797b), eax365 = fun_3d10(rdi362, v20, rdx363, rdi362, v20, rdx363), rsp16 = reinterpret_cast<struct s3*>(rsp364 + 8), eax365 == 0)) {
                                        if (!(v233 & reinterpret_cast<uint32_t>(~v366))) {
                                            continue;
                                        }
                                    } else {
                                        rsi19 = v18;
                                        addr_7990_16:
                                        rsp367 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp367 = reinterpret_cast<void**>(0x799a);
                                        quotearg_style(4, rsi19, 4, rsi19);
                                        rsp368 = reinterpret_cast<struct s3*>(rsp367 + 8);
                                        goto addr_79a9_325;
                                    }
                                }
                            }
                        }
                    }
                    addr_7662_326:
                    if (!*reinterpret_cast<unsigned char*>(rbx26 + 50)) 
                        continue;
                    goto addr_7190_197;
                    addr_92dd_306:
                    rsp369 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp369 = reinterpret_cast<void**>(0x92ee);
                    eax370 = set_acl(v18, 0xffffffff, v18, 0xffffffff);
                    rsp16 = reinterpret_cast<struct s3*>(rsp369 + 8);
                    if (!eax370) 
                        continue;
                    goto addr_7190_197;
                    addr_7622_320:
                    rsp371 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp371 = reinterpret_cast<void**>(0x7633);
                    rax372 = quotearg_style(4, v18, 4, v18);
                    r14_49 = rax372;
                    rsp373 = rsp371 + 8 - 8;
                    *rsp373 = reinterpret_cast<void**>(0x7649);
                    fun_3840();
                    rsp374 = rsp373 + 8 - 8;
                    *rsp374 = reinterpret_cast<void**>(0x7651);
                    fun_3700();
                    rsp375 = rsp374 + 8 - 8;
                    *rsp375 = reinterpret_cast<void**>(0x7662);
                    fun_3bd0();
                    rsp16 = reinterpret_cast<struct s3*>(rsp375 + 8);
                    goto addr_7662_326;
                    addr_79a9_325:
                    rsp376 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp368) - 8);
                    *rsp376 = reinterpret_cast<void**>(0x79b0);
                    fun_3840();
                    rsp377 = rsp376 + 8 - 8;
                    *rsp377 = reinterpret_cast<void**>(0x79b8);
                    fun_3700();
                    rsp378 = rsp377 + 8 - 8;
                    *rsp378 = reinterpret_cast<void**>(0x79c9);
                    fun_3bd0();
                    rsp16 = reinterpret_cast<struct s3*>(rsp378 + 8);
                    goto addr_7190_197;
                    addr_752f_300:
                    *reinterpret_cast<int32_t*>(&rax379) = v380;
                    *reinterpret_cast<int32_t*>(&rax379 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdx381) = v382;
                    *reinterpret_cast<int32_t*>(&rdx381 + 4) = 0;
                    if (*reinterpret_cast<unsigned char*>(&v36) || (v383 != *reinterpret_cast<int32_t*>(&rax379) || v384 != *reinterpret_cast<int32_t*>(&rdx381))) {
                        r9 = v385;
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        rsp386 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp386 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                        *reinterpret_cast<uint32_t*>(&rcx387) = *reinterpret_cast<unsigned char*>(&v36);
                        *reinterpret_cast<int32_t*>(&rcx387 + 4) = 0;
                        r8_5 = reinterpret_cast<void**>(0xffffffff);
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                        rsp388 = rsp386 - 8;
                        *rsp388 = rcx387;
                        rsp389 = rsp388 - 8;
                        *rsp389 = rdx381;
                        rsp390 = rsp389 - 8;
                        *rsp390 = rax379;
                        rsp391 = reinterpret_cast<struct s5*>(rsp390 - 8);
                        rsp391->f0 = reinterpret_cast<void**>(0x758b);
                        eax392 = set_owner_isra_0(rbx26, v18, v24, v20, 0xffffffff, r9, rsp391->f8, rsp391->f10, rsp391->f18, rsp391->f20);
                        rsp16 = reinterpret_cast<struct s3*>(&rsp391->f8 + 8);
                        if (eax392 == -1) 
                            goto addr_7190_197;
                        edx393 = v41;
                        *reinterpret_cast<unsigned char*>(&edx393 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx393 + 1) & 0xf1);
                        if (eax392) {
                            edx393 = v41;
                        }
                        v41 = edx393;
                        goto addr_75ae_301;
                    } else {
                        goto addr_75ae_301;
                    }
                    addr_7aea_269:
                    if (!r14_49) {
                        rsp394 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp394 = reinterpret_cast<void**>(0x87cb);
                        forget_created(v395, v396, rdx100, rcx22, r8_5, r9);
                        rsp16 = reinterpret_cast<struct s3*>(rsp394 + 8);
                    }
                    addr_7af3_335:
                    if (!v48) {
                        addr_7190_197:
                        r12_46 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                        continue;
                    } else {
                        *reinterpret_cast<void***>(&rdi397) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi397) + 4) = 0;
                        rsi398 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(v18)) + reinterpret_cast<unsigned char>(v48));
                        rsp399 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp399 = reinterpret_cast<void**>(0x7b24);
                        eax400 = fun_3c60(rdi397, rsi398, rdi397, rsi398);
                        rsp16 = reinterpret_cast<struct s3*>(rsp399 + 8);
                        if (eax400) {
                            rsp401 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp401 = reinterpret_cast<void**>(0x87a1);
                            quotearg_style(4, v18, 4, v18);
                            rsp368 = reinterpret_cast<struct s3*>(rsp401 + 8);
                            goto addr_79a9_325;
                        } else {
                            if (*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                                rsp402 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp402 = reinterpret_cast<void**>(0x7b4c);
                                rax403 = quotearg_n_style();
                                rsp404 = rsp402 + 8 - 8;
                                *rsp404 = reinterpret_cast<void**>(0x7b62);
                                rax405 = quotearg_n_style();
                                rsp406 = rsp404 + 8 - 8;
                                *rsp406 = reinterpret_cast<void**>(0x7b78);
                                rax407 = fun_3840();
                                rsp408 = rsp406 + 8 - 8;
                                *rsp408 = reinterpret_cast<void**>(0x7b8d);
                                fun_3b80(1, rax407, rax405, rax403, 1, rax407, rax405, rax403);
                                rsp16 = reinterpret_cast<struct s3*>(rsp408 + 8);
                                goto addr_7190_197;
                            }
                        }
                    }
                    addr_731b_236:
                    r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 58)));
                    if (!*reinterpret_cast<unsigned char*>(&r13_29)) {
                        if (*reinterpret_cast<unsigned char*>(rbx26 + 23)) {
                            *reinterpret_cast<int32_t*>(&rax409) = 1;
                            *reinterpret_cast<int32_t*>(&rax409 + 4) = 0;
                            if (!*reinterpret_cast<unsigned char*>(rbx26 + 22)) {
                                *reinterpret_cast<int32_t*>(&rax409) = 0;
                                *reinterpret_cast<int32_t*>(&rax409 + 4) = 0;
                                *reinterpret_cast<unsigned char*>(&rax409) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx26 + 8) == 3);
                            }
                            rdi410 = v47;
                            *reinterpret_cast<int32_t*>(&rdi410 + 4) = 0;
                            rdx100 = r15_15;
                            r8_5 = v24;
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            rcx22 = v18;
                            rsi19 = reinterpret_cast<void**>(0xffffff9c);
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            rsp411 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8 - 8);
                            *rsp411 = rdi410;
                            rdi17 = r15_15;
                            rsp412 = rsp411 - 8;
                            *rsp412 = reinterpret_cast<void**>(0);
                            rsp413 = rsp412 - 8;
                            *rsp413 = rax409;
                            rsp414 = reinterpret_cast<struct s5*>(rsp413 - 8);
                            rsp414->f0 = reinterpret_cast<void**>(0x831f);
                            al415 = create_hard_link(rdi17, 0xffffff9c, rdx100, rcx22, r8_5, v20, 1, 0, rsp414->f18);
                            rsp16 = reinterpret_cast<struct s3*>(&rsp414->f8 + 8);
                            r9 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            if (al415) 
                                goto addr_7393_344;
                            goto addr_7ae0_250;
                        }
                        if (*reinterpret_cast<uint32_t*>(&v31) == 0x8000) 
                            goto addr_88c8_347;
                        r9 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&v31) != 0xa000)) & *reinterpret_cast<unsigned char*>(rbx26 + 20));
                        if (!r9) 
                            goto addr_79fe_349;
                    } else {
                        if (*reinterpret_cast<void***>(r15_15) == 47) {
                            addr_7366_351:
                            rcx22 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 22)));
                            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                            r8_5 = reinterpret_cast<void**>(0xffffffff);
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            rsi416 = v24;
                            *reinterpret_cast<int32_t*>(&rsi416 + 4) = 0;
                            rsp417 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp417 = reinterpret_cast<void**>(0x7385);
                            eax418 = force_symlinkat(r15_15, rsi416, v20, rcx22, 0xffffffff, r15_15, rsi416, v20, rcx22, 0xffffffff);
                            rsp16 = reinterpret_cast<struct s3*>(rsp417 + 8);
                            r9 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax418) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(eax418 == 0))) {
                                rsp419 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp419 = reinterpret_cast<void**>(0x8c02);
                                rax420 = quotearg_n_style();
                                r15_15 = rax420;
                                rsp421 = rsp419 + 8 - 8;
                                *rsp421 = reinterpret_cast<void**>(0x8c18);
                                rax422 = quotearg_n_style();
                                r13_29 = rax422;
                                rsp423 = rsp421 + 8 - 8;
                                *rsp423 = reinterpret_cast<void**>(0x8c2e);
                                rax424 = fun_3840();
                                r8_5 = r15_15;
                                rcx22 = r13_29;
                                rsi19 = eax418;
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = rax424;
                                rdi17 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                                rsp425 = rsp423 + 8 - 8;
                                *rsp425 = reinterpret_cast<void**>(0x8c43);
                                fun_3bd0();
                                rsp16 = reinterpret_cast<struct s3*>(rsp425 + 8);
                                goto addr_7ae0_250;
                            }
                        } else {
                            rsp426 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp426 = reinterpret_cast<void**>(0x733b);
                            rax427 = dir_name(v20, rsi230, v20, rsi230);
                            rsp428 = reinterpret_cast<struct s3*>(rsp426 + 8);
                            if (reinterpret_cast<int1_t>(v24 == 0xffffff9c) && (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax427) == 46) && !*reinterpret_cast<void***>(rax427 + 1)) || ((rsi429 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffea0), rsp430 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp428) - 8), *rsp430 = reinterpret_cast<void**>(0x8373), eax431 = fun_3aa0(".", rsi429, ".", rsi429), rsp428 = reinterpret_cast<struct s3*>(rsp430 + 8), !!eax431) || (rdi432 = v24, *reinterpret_cast<int32_t*>(&rdi432 + 4) = 0, rdx433 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffff30), rsp434 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp428) - 8), *rsp434 = reinterpret_cast<void**>(0x8392), eax435 = fun_3d10(rdi432, rax427, rdx433, rdi432, rax427, rdx433), rsp428 = reinterpret_cast<struct s3*>(rsp434 + 8), !!eax435))) {
                                addr_735e_354:
                                rsp436 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp428) - 8);
                                *rsp436 = reinterpret_cast<void**>(0x7366);
                                fun_36a0(rax427, rax427);
                                rsp16 = reinterpret_cast<struct s3*>(rsp436 + 8);
                                goto addr_7366_351;
                            } else {
                                if (v437 != v106 || v438 != v108) {
                                    rsp439 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp428) - 8);
                                    *rsp439 = reinterpret_cast<void**>(0x83b6);
                                    fun_36a0(rax427, rax427);
                                    rsp440 = rsp439 + 8 - 8;
                                    *rsp440 = reinterpret_cast<void**>(0x83c9);
                                    rax441 = quotearg_n_style_colon();
                                    rsp245 = reinterpret_cast<struct s3*>(rsp440 + 8);
                                    r12_246 = rax441;
                                    goto addr_83d8_249;
                                } else {
                                    goto addr_735e_354;
                                }
                            }
                        }
                    }
                    addr_88c8_347:
                    v21 = v442;
                    eax443 = *reinterpret_cast<unsigned char*>(rbx26 + 49);
                    v28 = *reinterpret_cast<void***>(&eax443);
                    rsp444 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp444 = reinterpret_cast<void**>(0x8900);
                    eax445 = open_safer(r15_15, r15_15);
                    rsp446 = reinterpret_cast<struct s3*>(rsp444 + 8);
                    r13d447 = eax445;
                    if (reinterpret_cast<signed char>(eax445) < reinterpret_cast<signed char>(0)) {
                        rsp448 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp446) - 8);
                        *rsp448 = reinterpret_cast<void**>(0x9636);
                        rax449 = quotearg_style(4, r15_15, 4, r15_15);
                        rsp260 = reinterpret_cast<struct s3*>(rsp448 + 8);
                        r13_29 = rax449;
                        goto addr_8c88_270;
                    } else {
                        rsi450 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - 0xd0);
                        *reinterpret_cast<void***>(&rdi451) = eax445;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi451) + 4) = 0;
                        rsp452 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp446) - 8);
                        *rsp452 = reinterpret_cast<void**>(0x8919);
                        eax453 = fun_3d00(rdi451, rsi450, rdi451, rsi450);
                        rsp454 = reinterpret_cast<struct s3*>(rsp452 + 8);
                        if (eax453) {
                            rsp455 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp455 = reinterpret_cast<void**>(0x988f);
                            rax456 = quotearg_style(4, r15_15, 4, r15_15);
                            v28 = rax456;
                            rsp457 = rsp455 + 8 - 8;
                            *rsp457 = reinterpret_cast<void**>(0x98a9);
                            rax458 = fun_3840();
                            rsp459 = rsp457 + 8 - 8;
                            *rsp459 = reinterpret_cast<void**>(0x98b1);
                            rax460 = fun_3700();
                            rsp461 = reinterpret_cast<struct s3*>(rsp459 + 8);
                            rcx22 = v28;
                            rdx100 = rax458;
                            rsi19 = *reinterpret_cast<void***>(rax460);
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        } else {
                            if (v462 != v106 || v463 != v108) {
                                rsp464 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp464 = reinterpret_cast<void**>(0x93f1);
                                rax465 = quotearg_style(4, r15_15, 4, r15_15);
                                rsp466 = rsp464 + 8 - 8;
                                *rsp466 = reinterpret_cast<void**>(0x9407);
                                rax467 = fun_3840();
                                rsp461 = reinterpret_cast<struct s3*>(rsp466 + 8);
                                rcx22 = rax465;
                                rsi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = rax467;
                            } else {
                                v468 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_46) & 0x1ff);
                                if (*reinterpret_cast<unsigned char*>(&v36)) 
                                    goto addr_98e7_364;
                                rsi19 = v20;
                                *reinterpret_cast<void***>(&rdi469) = v24;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi469) + 4) = 0;
                                rdx100 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(edx229) - (reinterpret_cast<unsigned char>(edx229) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(edx229) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(edx229) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v28) < 1)))) & 0xfffffe00) + 0x201);
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                rsp470 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp470 = reinterpret_cast<void**>(0x898d);
                                eax471 = openat_safer(rdi469, rsi19, rdi469, rsi19);
                                r12d472 = eax471;
                                rsp473 = rsp470 + 8 - 8;
                                *rsp473 = reinterpret_cast<void**>(0x8995);
                                rax474 = fun_3700();
                                rsp454 = reinterpret_cast<struct s3*>(rsp473 + 8);
                                r8_5 = *reinterpret_cast<void***>(rax474);
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                r9 = rax474;
                                if (reinterpret_cast<signed char>(r12d472) < reinterpret_cast<signed char>(0)) 
                                    goto addr_9fb3_366; else 
                                    goto addr_89a4_367;
                            }
                        }
                    }
                    addr_940f_368:
                    rsp475 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp461) - 8);
                    *rsp475 = reinterpret_cast<void**>(0x941b);
                    fun_3bd0();
                    rsp454 = reinterpret_cast<struct s3*>(rsp475 + 8);
                    r9 = reinterpret_cast<void**>(0);
                    addr_8ba8_369:
                    v28 = r9;
                    rsp476 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp476 = reinterpret_cast<void**>(0x8bb7);
                    eax477 = fun_3950();
                    rsp478 = reinterpret_cast<struct s3*>(rsp476 + 8);
                    r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                    if (eax477 < 0) {
                        rsp479 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp478) - 8);
                        *rsp479 = reinterpret_cast<void**>(0x9846);
                        rax480 = quotearg_style(4, r15_15, 4, r15_15);
                        r15_15 = rax480;
                        rsp481 = rsp479 + 8 - 8;
                        *rsp481 = reinterpret_cast<void**>(0x985c);
                        rax482 = fun_3840();
                        r13_29 = rax482;
                        rsp483 = rsp481 + 8 - 8;
                        *rsp483 = reinterpret_cast<void**>(0x9864);
                        rax484 = fun_3700();
                        rcx22 = r15_15;
                        rdx100 = r13_29;
                        rsi19 = *reinterpret_cast<void***>(rax484);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rsp485 = rsp483 + 8 - 8;
                        *rsp485 = reinterpret_cast<void**>(0x9875);
                        fun_3bd0();
                        rdi17 = reinterpret_cast<void**>(0);
                        rsp486 = rsp485 + 8 - 8;
                        *rsp486 = reinterpret_cast<void**>(0x987d);
                        fun_36a0(0, 0);
                        rsp16 = reinterpret_cast<struct s3*>(rsp486 + 8);
                        goto addr_7ae0_250;
                    } else {
                        rdi17 = reinterpret_cast<void**>(0);
                        v28 = r9;
                        r13_29 = reinterpret_cast<void**>(0);
                        rsp487 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp478) - 8);
                        *rsp487 = reinterpret_cast<void**>(0x8bd9);
                        fun_36a0(0, 0);
                        rsp16 = reinterpret_cast<struct s3*>(rsp487 + 8);
                        r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        if (!r9) {
                            goto addr_7ae0_250;
                        }
                    }
                    addr_9fb3_366:
                    if (r8_5 == 2) 
                        goto addr_9ff9_373;
                    eax488 = *reinterpret_cast<unsigned char*>(rbx26 + 22);
                    if (!*reinterpret_cast<unsigned char*>(&eax488)) 
                        goto addr_9951_375;
                    *reinterpret_cast<void***>(&rdi489) = v24;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi489) + 4) = 0;
                    v30 = r9;
                    rsp490 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp490 = reinterpret_cast<void**>(0x9fe0);
                    eax491 = fun_3750(rdi489, v20);
                    rsp454 = reinterpret_cast<struct s3*>(rsp490 + 8);
                    r9 = v30;
                    if (!eax491) 
                        goto addr_9fef_377;
                    if (*reinterpret_cast<void***>(r9) == 2) {
                        addr_9ff9_373:
                        if (!*reinterpret_cast<void***>(rbx26 + 40) || (rdx100 = v468, *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0, r8_5 = rbx26, rcx22 = reinterpret_cast<void**>(1), *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0, rsi19 = v18, rsp492 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8), *rsp492 = reinterpret_cast<void**>(0xa021), eax493 = set_process_security_ctx(r15_15, rsi19, r15_15, rsi19), rsp454 = reinterpret_cast<struct s3*>(rsp492 + 8), r9 = eax493, *reinterpret_cast<int32_t*>(&r9 + 4) = 0, !!*reinterpret_cast<signed char*>(&eax493))) {
                            addr_98e7_364:
                            *reinterpret_cast<void***>(&rdi494) = v24;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi494) + 4) = 0;
                            rdx100 = reinterpret_cast<void**>(0xc1);
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsi19 = v20;
                            eax495 = reinterpret_cast<void**>(~v233 & reinterpret_cast<unsigned char>(v468));
                            rcx22 = eax495;
                            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                            v30 = eax495;
                            rsp496 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp496 = reinterpret_cast<void**>(0x9916);
                            eax497 = openat_safer(rdi494, rsi19, rdi494, rsi19);
                            r12d472 = eax497;
                            rsp498 = rsp496 + 8 - 8;
                            *rsp498 = reinterpret_cast<void**>(0x991e);
                            rax499 = fun_3700();
                            rsp454 = reinterpret_cast<struct s3*>(rsp498 + 8);
                            r8_5 = *reinterpret_cast<void***>(rax499);
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            eax500 = reinterpret_cast<unsigned char>(r12d472) >> 31;
                            dl501 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r8_5 == 17)) & *reinterpret_cast<unsigned char*>(&eax500));
                            *reinterpret_cast<unsigned char*>(&v36) = dl501;
                            if (!dl501) 
                                goto addr_a161_379;
                        } else {
                            *reinterpret_cast<unsigned char*>(&v36) = 0;
                            goto addr_8ba8_369;
                        }
                    } else {
                        v28 = r9;
                        rsp502 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp502 = reinterpret_cast<void**>(0xa898);
                        rax503 = quotearg_style(4, v18, 4, v18);
                        rsp504 = rsp502 + 8 - 8;
                        *rsp504 = reinterpret_cast<void**>(0xa8ae);
                        rax505 = fun_3840();
                        rsp461 = reinterpret_cast<struct s3*>(rsp504 + 8);
                        rcx22 = rax503;
                        rdx100 = rax505;
                        rsi19 = *reinterpret_cast<void***>(v28);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        goto addr_940f_368;
                    }
                    eax488 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
                    r8_5 = reinterpret_cast<void**>(17);
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    if (!*reinterpret_cast<unsigned char*>(&eax488)) {
                        rsi19 = v20;
                        *reinterpret_cast<void***>(&rdi506) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi506) + 4) = 0;
                        rdx100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xffffffffffffffc7);
                        rcx22 = reinterpret_cast<void**>(1);
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        v47 = reinterpret_cast<void**>(17);
                        v25 = rax499;
                        rsp507 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp507 = reinterpret_cast<void**>(0xa11b);
                        rax508 = fun_3a20(rdi506, rsi19, rdx100, 1);
                        rsp454 = reinterpret_cast<struct s3*>(rsp507 + 8);
                        r8_5 = reinterpret_cast<void**>(17);
                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                        if (reinterpret_cast<signed char>(rax508) < reinterpret_cast<signed char>(0)) {
                            addr_a161_379:
                            eax509 = reinterpret_cast<unsigned char>(r12d472) >> 31;
                            *reinterpret_cast<unsigned char*>(&rdx100) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r8_5 == 21)) & *reinterpret_cast<unsigned char*>(&eax509));
                            *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&rdx100);
                            if (!*reinterpret_cast<unsigned char*>(&rdx100)) {
                                v510 = reinterpret_cast<void**>(~reinterpret_cast<unsigned char>(v468) & reinterpret_cast<unsigned char>(v30));
                                if (reinterpret_cast<signed char>(r12d472) < reinterpret_cast<signed char>(0)) {
                                    *reinterpret_cast<unsigned char*>(&v36) = 1;
                                } else {
                                    *reinterpret_cast<unsigned char*>(&v36) = 1;
                                    v511 = v233;
                                    goto addr_89e5_387;
                                }
                            } else {
                                r8_5 = reinterpret_cast<void**>(21);
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                if (*reinterpret_cast<void***>(v18)) {
                                    rsp512 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                    *rsp512 = reinterpret_cast<void**>(0xa198);
                                    rax513 = fun_3860(v18, v18);
                                    rsp454 = reinterpret_cast<struct s3*>(rsp512 + 8);
                                    r8d514 = reinterpret_cast<struct s7*>(0);
                                    *reinterpret_cast<unsigned char*>(&r8d514) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(rax513) + 0xffffffffffffffff) != 47);
                                    r8_5 = reinterpret_cast<void**>(&r8d514->f14);
                                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                }
                            }
                        } else {
                            r9 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 62)));
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            if (!r9) {
                                rsp515 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp515 = reinterpret_cast<void**>(0xac97);
                                rax516 = quotearg_style(4, v18, 4, v18);
                                rsp517 = rsp515 + 8 - 8;
                                *rsp517 = reinterpret_cast<void**>(0xacad);
                                rax518 = fun_3840();
                                rcx22 = rax516;
                                rsi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = rax518;
                                rsp519 = rsp517 + 8 - 8;
                                *rsp519 = reinterpret_cast<void**>(0xacc1);
                                fun_3bd0();
                                rsp454 = reinterpret_cast<struct s3*>(rsp519 + 8);
                                r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(r9)));
                                goto addr_8ba8_369;
                            } else {
                                rcx22 = v30;
                                *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                rsi19 = v20;
                                rdx100 = reinterpret_cast<void**>(65);
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                *reinterpret_cast<void***>(&rdi520) = v24;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi520) + 4) = 0;
                                rsp521 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp521 = reinterpret_cast<void**>(0xa154);
                                eax522 = openat_safer(rdi520, rsi19, rdi520, rsi19);
                                rsp454 = reinterpret_cast<struct s3*>(rsp521 + 8);
                                r12d472 = eax522;
                                r8_5 = *reinterpret_cast<void***>(v25);
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                goto addr_a161_379;
                            }
                        }
                    } else {
                        addr_9951_375:
                        *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax488);
                    }
                    v28 = r8_5;
                    rsp523 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp523 = reinterpret_cast<void**>(0x996f);
                    rax524 = quotearg_style(4, v18, 4, v18);
                    rsp525 = rsp523 + 8 - 8;
                    *rsp525 = reinterpret_cast<void**>(0x9985);
                    rax526 = fun_3840();
                    rsp461 = reinterpret_cast<struct s3*>(rsp525 + 8);
                    rsi19 = v28;
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rcx22 = rax524;
                    rdx100 = rax526;
                    goto addr_940f_368;
                    addr_89e5_387:
                    if (!v28) 
                        goto addr_9a0d_394;
                    rdx100 = *reinterpret_cast<void***>(rbx26 + 68);
                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                    if (rdx100) {
                        rdx100 = r13d447;
                        *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                        rsi19 = reinterpret_cast<void**>(0x40049409);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        *reinterpret_cast<void***>(&rdi527) = r12d472;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi527) + 4) = 0;
                        rsp528 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp528 = reinterpret_cast<void**>(0xa2bc);
                        eax529 = fun_3920(rdi527, 0x40049409, rdx100, rcx22);
                        rsp454 = reinterpret_cast<struct s3*>(rsp528 + 8);
                        if (!eax529) {
                            addr_9a0d_394:
                            eax530 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 29)) | reinterpret_cast<unsigned char>(v510));
                            v28 = eax530;
                            if (eax530) {
                                v28 = reinterpret_cast<void**>(0);
                                goto addr_89fd_398;
                            } else {
                                if (!*reinterpret_cast<signed char*>(rbx26 + 31)) {
                                    addr_9b0b_400:
                                    if (0xff0000000000ff & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24))) {
                                        r8_5 = v21;
                                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                        rcx22 = r12d472;
                                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                        rsi19 = r13d447;
                                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                        rdx100 = v18;
                                        rsp531 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                        *rsp531 = reinterpret_cast<void**>(0xa1ff);
                                        eax532 = copy_acl(r15_15, rsi19, rdx100, rcx22, r8_5, r9);
                                        rsp454 = reinterpret_cast<struct s3*>(rsp531 + 8);
                                        r9 = reinterpret_cast<void**>(1);
                                        if (!eax532) {
                                            addr_8b7c_402:
                                            rsp533 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                            *rsp533 = reinterpret_cast<void**>(0x8b8b);
                                            eax534 = fun_3950();
                                            rsp454 = reinterpret_cast<struct s3*>(rsp533 + 8);
                                            if (eax534 < 0) {
                                                rsp535 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                                *rsp535 = reinterpret_cast<void**>(0xa08b);
                                                rax536 = quotearg_style(4, v18, 4, v18);
                                                v28 = rax536;
                                                rsp537 = rsp535 + 8 - 8;
                                                *rsp537 = reinterpret_cast<void**>(0xa0a5);
                                                rax538 = fun_3840();
                                                rsp539 = rsp537 + 8 - 8;
                                                *rsp539 = reinterpret_cast<void**>(0xa0ad);
                                                rax540 = fun_3700();
                                                rcx22 = v28;
                                                rdx100 = rax538;
                                                rsi19 = *reinterpret_cast<void***>(rax540);
                                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                                rsp541 = rsp539 + 8 - 8;
                                                *rsp541 = reinterpret_cast<void**>(0xa0c2);
                                                fun_3bd0();
                                                rsp454 = reinterpret_cast<struct s3*>(rsp541 + 8);
                                                r9 = reinterpret_cast<void**>(0);
                                                goto addr_8ba8_369;
                                            } else {
                                                r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(r9)));
                                                goto addr_8ba8_369;
                                            }
                                        }
                                    } else {
                                        if (*reinterpret_cast<signed char*>(rbx26 + 57)) {
                                            rdx100 = *reinterpret_cast<void***>(rbx26 + 16);
                                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                            rsi19 = r12d472;
                                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                            rsp542 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                            *rsp542 = reinterpret_cast<void**>(0xa3a1);
                                            eax543 = set_acl(v18, rsi19, v18, rsi19);
                                            rsp454 = reinterpret_cast<struct s3*>(rsp542 + 8);
                                            r9 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax543 == 0)));
                                            goto addr_8b7c_402;
                                        }
                                        eax544 = *reinterpret_cast<unsigned char*>(&v36);
                                        al545 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax544) & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 32)));
                                        *reinterpret_cast<unsigned char*>(&v21) = al545;
                                        if (al545) {
                                            zf546 = reinterpret_cast<int1_t>(mask_0 == 0xffffffff);
                                            if (zf546) {
                                                rsp547 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                                *rsp547 = reinterpret_cast<void**>(0xab2f);
                                                eax548 = fun_3a90();
                                                mask_0 = eax548;
                                                rsp549 = rsp547 + 8 - 8;
                                                *rsp549 = reinterpret_cast<void**>(0xab3c);
                                                fun_3a90();
                                                rsp454 = reinterpret_cast<struct s3*>(rsp549 + 8);
                                            }
                                            edx550 = mask_0;
                                            rsi19 = r12d472;
                                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                            rdx100 = reinterpret_cast<void**>(~reinterpret_cast<unsigned char>(edx550) & 0x1b6);
                                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                            rsp551 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                            *rsp551 = reinterpret_cast<void**>(0xa903);
                                            eax552 = set_acl(v18, rsi19);
                                            rsp454 = reinterpret_cast<struct s3*>(rsp551 + 8);
                                            eax553 = *reinterpret_cast<unsigned char*>(&v21);
                                            r9 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax552 == 0)));
                                            *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax553);
                                            goto addr_8b7c_402;
                                        }
                                        r9 = reinterpret_cast<void**>(1);
                                        if (!(v511 | reinterpret_cast<unsigned char>(v28))) 
                                            goto addr_8b7c_402;
                                        eax554 = mask_0;
                                        if (eax554 == 0xffffffff) 
                                            goto addr_abc6_414; else 
                                            goto addr_9b66_415;
                                    }
                                } else {
                                    addr_9a37_416:
                                    rdx100 = v20;
                                    r8_5 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                    *reinterpret_cast<void***>(&rdi555) = r12d472;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi555) + 4) = 0;
                                    rsi19 = v24;
                                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                    rcx22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd60);
                                    rsp556 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                    *rsp556 = reinterpret_cast<void**>(0x9a8e);
                                    eax557 = fdutimensat(rdi555, rsi19, rdx100, rcx22);
                                    rsp454 = reinterpret_cast<struct s3*>(rsp556 + 8);
                                    if (eax557 && (rsp558 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8), *rsp558 = reinterpret_cast<void**>(0xa775), rax559 = quotearg_style(4, v18, 4, v18), v25 = rax559, rsp560 = rsp558 + 8 - 8, *rsp560 = reinterpret_cast<void**>(0xa78f), rax561 = fun_3840(), v30 = rax561, rsp562 = rsp560 + 8 - 8, *rsp562 = reinterpret_cast<void**>(0xa79b), rax563 = fun_3700(), rcx22 = v25, rdx100 = v30, rsi19 = *reinterpret_cast<void***>(rax563), *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0, rsp564 = rsp562 + 8 - 8, *rsp564 = reinterpret_cast<void**>(0xa7b4), fun_3bd0(), rsp454 = reinterpret_cast<struct s3*>(rsp564 + 8), !!*reinterpret_cast<unsigned char*>(rbx26 + 50))) {
                                        goto addr_8b79_418;
                                    }
                                }
                            }
                        } else {
                            if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx26 + 68) == 2)) {
                                rsp565 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp565 = reinterpret_cast<void**>(0xa2e0);
                                rax566 = quotearg_n_style();
                                v30 = rax566;
                                rsp567 = rsp565 + 8 - 8;
                                *rsp567 = reinterpret_cast<void**>(0xa2fa);
                                rax568 = quotearg_n_style();
                                v21 = rax568;
                                rsp569 = rsp567 + 8 - 8;
                                *rsp569 = reinterpret_cast<void**>(0xa314);
                                rax570 = fun_3840();
                                v28 = rax570;
                                rsp571 = rsp569 + 8 - 8;
                                *rsp571 = reinterpret_cast<void**>(0xa320);
                                rax572 = fun_3700();
                                r8_5 = v30;
                                rcx22 = v21;
                                rsi19 = *reinterpret_cast<void***>(rax572);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = v28;
                                rsp573 = rsp571 + 8 - 8;
                                *rsp573 = reinterpret_cast<void**>(0xa340);
                                fun_3bd0();
                                rsp454 = reinterpret_cast<struct s3*>(rsp573 + 8);
                                r9 = reinterpret_cast<void**>(0);
                                goto addr_8b7c_402;
                            }
                        }
                    } else {
                        addr_89fd_398:
                        rsi574 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - 0x160);
                        *reinterpret_cast<void***>(&rdi575) = r12d472;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi575) + 4) = 0;
                        rsp576 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp576 = reinterpret_cast<void**>(0x8a0c);
                        eax577 = fun_3d00(rdi575, rsi574, rdi575, rsi574);
                        rsp454 = reinterpret_cast<struct s3*>(rsp576 + 8);
                        if (eax577) {
                            rsp578 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp578 = reinterpret_cast<void**>(0xa7d4);
                            rax579 = quotearg_style(4, v18, 4, v18);
                            rsp580 = reinterpret_cast<struct s3*>(rsp578 + 8);
                            v21 = rax579;
                            goto addr_a451_422;
                        } else {
                            v30 = eax577;
                            rsi19 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v510)));
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            if (rsi19) {
                                *reinterpret_cast<void***>(&rdi581) = r12d472;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi581) + 4) = 0;
                                rsp582 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp582 = reinterpret_cast<void**>(0x8a34);
                                eax583 = fun_3b90(rdi581, rdi581);
                                rsp454 = reinterpret_cast<struct s3*>(rsp582 + 8);
                                rdx100 = v30;
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                if (!eax583) {
                                    rdx100 = v510;
                                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                }
                                v510 = rdx100;
                            }
                            if (!v28) 
                                goto addr_9ef4_428;
                            v584 = v585;
                            v47 = v585;
                            if (reinterpret_cast<uint64_t>(v585 + 0xfffffffffffe0000) > 0x1ffffffffffe0000) {
                                v47 = reinterpret_cast<void**>(0x20000);
                                *reinterpret_cast<int32_t*>(&rax586) = 0x200;
                                *reinterpret_cast<int32_t*>(&rax586 + 4) = 0;
                                if (reinterpret_cast<unsigned char>(v584 + 0xffffffffffffffff) <= reinterpret_cast<unsigned char>(0x1fffffffffffffff)) {
                                    rax586 = v584;
                                }
                                v584 = rax586;
                            }
                            if ((*reinterpret_cast<uint32_t*>(&v339) & 0xf000) != 0x8000) 
                                goto addr_9da7_434; else 
                                goto addr_8ac6_435;
                        }
                    }
                    addr_9bec_436:
                    r9 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 50)) ^ 1);
                    goto addr_8b7c_402;
                    addr_abc6_414:
                    v30 = reinterpret_cast<void**>(1);
                    rsp587 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp587 = reinterpret_cast<void**>(0xabd4);
                    eax588 = fun_3a90();
                    mask_0 = eax588;
                    v21 = eax588;
                    rsp589 = rsp587 + 8 - 8;
                    *rsp589 = reinterpret_cast<void**>(0xabe7);
                    fun_3a90();
                    rsp454 = reinterpret_cast<struct s3*>(rsp589 + 8);
                    rdx100 = v21;
                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                    r9 = reinterpret_cast<void**>(1);
                    if (!(~reinterpret_cast<unsigned char>(rdx100) & v511 | reinterpret_cast<unsigned char>(v28))) 
                        goto addr_8b7c_402;
                    ++rdx100;
                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                    if (!rdx100) {
                        rsp590 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp590 = reinterpret_cast<void**>(0xac17);
                        eax591 = fun_3a90();
                        mask_0 = eax591;
                        rsp592 = rsp590 + 8 - 8;
                        *rsp592 = reinterpret_cast<void**>(0xac24);
                        fun_3a90();
                        rsp454 = reinterpret_cast<struct s3*>(rsp592 + 8);
                    }
                    eax554 = mask_0;
                    addr_9b7c_440:
                    *reinterpret_cast<void***>(&rdi593) = r12d472;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi593) + 4) = 0;
                    rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v468) & ~reinterpret_cast<unsigned char>(eax554));
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp594 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp594 = reinterpret_cast<void**>(0x9b8e);
                    eax595 = fun_3b90(rdi593, rdi593);
                    rsp454 = reinterpret_cast<struct s3*>(rsp594 + 8);
                    r9 = reinterpret_cast<void**>(1);
                    if (!eax595) 
                        goto addr_8b7c_402;
                    rsp596 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp596 = reinterpret_cast<void**>(0x9bad);
                    rax597 = quotearg_style(4, v18, 4, v18);
                    v21 = rax597;
                    rsp598 = rsp596 + 8 - 8;
                    *rsp598 = reinterpret_cast<void**>(0x9bc7);
                    rax599 = fun_3840();
                    v28 = rax599;
                    rsp600 = rsp598 + 8 - 8;
                    *rsp600 = reinterpret_cast<void**>(0x9bd3);
                    rax601 = fun_3700();
                    rcx22 = v21;
                    rdx100 = v28;
                    rsi19 = *reinterpret_cast<void***>(rax601);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp602 = rsp600 + 8 - 8;
                    *rsp602 = reinterpret_cast<void**>(0x9bec);
                    fun_3bd0();
                    rsp454 = reinterpret_cast<struct s3*>(rsp602 + 8);
                    goto addr_9bec_436;
                    addr_9b66_415:
                    rdx100 = reinterpret_cast<void**>(~reinterpret_cast<unsigned char>(eax554) & v511 | reinterpret_cast<unsigned char>(v28));
                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                    if (!rdx100) 
                        goto addr_8b7c_402; else 
                        goto addr_9b7c_440;
                    addr_a451_422:
                    rsp603 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp580) - 8);
                    *rsp603 = reinterpret_cast<void**>(0xa458);
                    rax604 = fun_3840();
                    v28 = rax604;
                    rsp605 = rsp603 + 8 - 8;
                    *rsp605 = reinterpret_cast<void**>(0xa464);
                    rax606 = fun_3700();
                    rcx22 = v21;
                    rdx100 = v28;
                    rsi19 = *reinterpret_cast<void***>(rax606);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp607 = rsp605 + 8 - 8;
                    *rsp607 = reinterpret_cast<void**>(0xa47d);
                    fun_3bd0();
                    rsp454 = reinterpret_cast<struct s3*>(rsp607 + 8);
                    goto addr_8b79_418;
                    addr_9da7_434:
                    r8d608 = reinterpret_cast<void**>(1);
                    addr_9dad_442:
                    if (!0 || *reinterpret_cast<signed char*>(rbx26 + 12) != 3 && (r8d608 == 1 || *reinterpret_cast<signed char*>(rbx26 + 12) != 2)) {
                        *reinterpret_cast<void***>(&rdi609) = r13d447;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi609) + 4) = 0;
                        v30 = r8d608;
                        rsp610 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp610 = reinterpret_cast<void**>(0x9ddb);
                        fdadvise(rdi609);
                        rdi611 = v612;
                        if (reinterpret_cast<uint64_t>(rdi611 - 0x20000) > 0x1ffffffffffe0000) {
                            rdi611 = 0x20000;
                        }
                        rsp613 = rsp610 + 8 - 8;
                        *rsp613 = reinterpret_cast<void**>(0x9e15);
                        rax614 = buffer_lcm(rdi611, v47, 0x7fffffffffffffff, 2);
                        rsp454 = reinterpret_cast<struct s3*>(rsp613 + 8);
                        r8_615 = v30;
                        *reinterpret_cast<int32_t*>(&r8_615 + 4) = 0;
                        rcx22 = rax614;
                        if ((*reinterpret_cast<uint32_t*>(&v339) & 0xf000) == 0x8000 && reinterpret_cast<unsigned char>(v616) < reinterpret_cast<unsigned char>(v47)) {
                            v47 = v616 + 1;
                        }
                        rsi617 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx22) + reinterpret_cast<unsigned char>(v47) + 0xffffffffffffffff);
                        rdx100 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsi617) % reinterpret_cast<unsigned char>(rcx22));
                        rax618 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsi617) - reinterpret_cast<unsigned char>(rdx100));
                        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax618) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax618 == 0))) {
                            rcx22 = rax618;
                        }
                        eax619 = 0;
                        *reinterpret_cast<unsigned char*>(&eax619) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbx26 + 68));
                        v47 = rcx22;
                        v620 = eax619;
                        if (r8_615 != 3) 
                            goto addr_9e7d_450;
                    } else {
                        rcx22 = reinterpret_cast<void**>(2);
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        rdx100 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                        *reinterpret_cast<void***>(&rdi621) = r13d447;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi621) + 4) = 0;
                        v30 = r8d608;
                        rsp622 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp622 = reinterpret_cast<void**>(0xa3df);
                        fdadvise(rdi621);
                        rsp454 = reinterpret_cast<struct s3*>(rsp622 + 8);
                        r8_615 = v30;
                        *reinterpret_cast<int32_t*>(&r8_615 + 4) = 0;
                        if (r8_615 == 3) {
                            eax623 = 0;
                            *reinterpret_cast<unsigned char*>(&eax623) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbx26 + 68));
                            v620 = eax623;
                            v624 = *reinterpret_cast<signed char*>(rbx26 + 12);
                            goto addr_a49a_453;
                        } else {
                            eax625 = 0;
                            *reinterpret_cast<unsigned char*>(&eax625) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbx26 + 68));
                            r9 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            zf626 = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(rbx26 + 12) == 3);
                            v620 = eax625;
                            r9 = reinterpret_cast<void**>(static_cast<unsigned char>(zf626));
                            goto addr_9e93_455;
                        }
                    }
                    v624 = reinterpret_cast<signed char>(1);
                    addr_a49a_453:
                    v30 = v627;
                    rax628 = v302;
                    if (reinterpret_cast<signed char>(rax628) < reinterpret_cast<signed char>(0)) {
                        v629 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<unsigned char*>(&rdx100) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(v30) > reinterpret_cast<signed char>(0));
                    } else {
                        edi630 = reinterpret_cast<unsigned char>(v28);
                        v25 = r15_15;
                        v629 = reinterpret_cast<void**>(0);
                        v631 = reinterpret_cast<void**>(0);
                        v632 = *reinterpret_cast<signed char*>(&edi630);
                        v302 = r14_49;
                        r14_633 = reinterpret_cast<void**>(0);
                        v634 = rbx26;
                        rbx635 = rax628;
                        while (1) {
                            rsp636 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp636 = reinterpret_cast<void**>(0xa509);
                            rax637 = fun_38e0();
                            rsp454 = reinterpret_cast<struct s3*>(rsp636 + 8);
                            r15_638 = rax637;
                            if (reinterpret_cast<signed char>(rax637) >= reinterpret_cast<signed char>(0)) {
                                rax639 = v30;
                                if (reinterpret_cast<signed char>(rax639) < reinterpret_cast<signed char>(r15_638)) {
                                    rax639 = r15_638;
                                }
                                v30 = rax639;
                            } else {
                                rsp640 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp640 = reinterpret_cast<void**>(0xa51a);
                                rax641 = fun_3700();
                                rsp454 = reinterpret_cast<struct s3*>(rsp640 + 8);
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax641) == 6)) 
                                    goto addr_a91b_464;
                                r15_638 = v30;
                                if (reinterpret_cast<signed char>(rbx635) >= reinterpret_cast<signed char>(v30)) 
                                    goto addr_a6d9_466;
                            }
                            addr_a536_467:
                            rsp642 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp642 = reinterpret_cast<void**>(0xa543);
                            rax643 = fun_38e0();
                            rsp454 = reinterpret_cast<struct s3*>(rsp642 + 8);
                            if (reinterpret_cast<signed char>(rax643) < reinterpret_cast<signed char>(0)) 
                                goto addr_a91b_464;
                            rcx22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx635) - reinterpret_cast<unsigned char>(v631)) - reinterpret_cast<unsigned char>(r14_633));
                            if (!rcx22) {
                                if (v624 != 1) {
                                    rcx22 = v584;
                                }
                                v644 = 0;
                                r14_633 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_638) - reinterpret_cast<unsigned char>(rbx635));
                                r8_615 = rcx22;
                            } else {
                                if (v624 == 1) {
                                    rsp645 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                    *rsp645 = reinterpret_cast<void**>(0xa714);
                                    al646 = write_zeros(r12d472, rcx22);
                                    rsp454 = reinterpret_cast<struct s3*>(rsp645 + 8);
                                    if (!al646) 
                                        goto addr_aae8_474;
                                    v644 = 0;
                                    r8_615 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&r8_615 + 4) = 0;
                                    r14_633 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_638) - reinterpret_cast<unsigned char>(rbx635));
                                } else {
                                    rdx100 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                    rsi19 = v18;
                                    *reinterpret_cast<unsigned char*>(&rdx100) = reinterpret_cast<uint1_t>(v624 == 3);
                                    rsp647 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                    *rsp647 = reinterpret_cast<void**>(0xa585);
                                    eax648 = create_hole(r12d472, rsi19, rdx100, rcx22, r8_615, r12d472, rsi19, rdx100, rcx22, r8_615);
                                    rsp454 = reinterpret_cast<struct s3*>(rsp647 + 8);
                                    v644 = *reinterpret_cast<unsigned char*>(&eax648);
                                    if (!*reinterpret_cast<unsigned char*>(&eax648)) 
                                        goto addr_aab1_477;
                                    r8_615 = v584;
                                    r14_633 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_638) - reinterpret_cast<unsigned char>(rbx635));
                                }
                            }
                            r9 = reinterpret_cast<void**>(1);
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            rsi19 = r12d472;
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            r15_15 = v25;
                            rsp649 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp649 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf7);
                            rcx22 = v47;
                            rsp650 = rsp649 - 8;
                            *rsp650 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd00);
                            *reinterpret_cast<int32_t*>(&rax651) = v620;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax651) + 4) = 0;
                            rdx100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf8);
                            rsp652 = rsp650 - 8;
                            *rsp652 = r14_633;
                            rsp653 = rsp652 - 8;
                            *rsp653 = v18;
                            rsp654 = reinterpret_cast<struct s5*>(rsp653 - 8);
                            rsp654->f0 = r15_15;
                            rsp655 = reinterpret_cast<struct s4**>(reinterpret_cast<uint64_t>(rsp654) - 8);
                            *rsp655 = rax651;
                            rsp656 = reinterpret_cast<struct s6*>(rsp655 - 1);
                            rsp656->f0 = 0xa5e7;
                            eax657 = sparse_copy(r13d447, rsi19, rdx100, rcx22, r8_615, 1, rsp656->f8, rsp656->f10, rsp656->f18, rsp656->f20, rsp656->f28, rsp656->f30);
                            rsp454 = reinterpret_cast<struct s3*>(&rsp656->f8 + 48);
                            if (!*reinterpret_cast<signed char*>(&eax657)) 
                                goto addr_aab1_477;
                            edi658 = reinterpret_cast<void**>(static_cast<uint32_t>(v644));
                            rdx100 = reinterpret_cast<void**>(static_cast<uint32_t>(v316));
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx635) + reinterpret_cast<unsigned char>(v300));
                            if (1) {
                                edi658 = rdx100;
                            }
                            v629 = rsi19;
                            v632 = *reinterpret_cast<signed char*>(&edi658);
                            if (reinterpret_cast<signed char>(v300) < reinterpret_cast<signed char>(r14_633)) 
                                goto addr_a984_483;
                            rdx100 = reinterpret_cast<void**>(3);
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsp659 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp659 = reinterpret_cast<void**>(0xa636);
                            rax660 = fun_38e0();
                            rsp454 = reinterpret_cast<struct s3*>(rsp659 + 8);
                            if (reinterpret_cast<signed char>(rax660) < reinterpret_cast<signed char>(0)) 
                                goto addr_a688_485;
                            v631 = rbx635;
                            rbx635 = rax660;
                            continue;
                            addr_a6d9_466:
                            rdx100 = reinterpret_cast<void**>(2);
                            *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                            rsi19 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            rsp661 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp661 = reinterpret_cast<void**>(0xa6e8);
                            rax662 = fun_38e0();
                            rsp454 = reinterpret_cast<struct s3*>(rsp661 + 8);
                            r15_638 = rax662;
                            if (reinterpret_cast<signed char>(rax662) < reinterpret_cast<signed char>(0)) 
                                goto addr_a91b_464;
                            if (reinterpret_cast<signed char>(rbx635) >= reinterpret_cast<signed char>(rax662)) 
                                goto addr_aacb_488;
                            v30 = rax662;
                            goto addr_a536_467;
                        }
                    }
                    addr_a9c2_490:
                    if (v624 == 1) {
                        rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v30) - reinterpret_cast<unsigned char>(v629));
                        rsp663 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp663 = reinterpret_cast<void**>(0xab57);
                        al664 = write_zeros(r12d472, rsi19);
                        rsp454 = reinterpret_cast<struct s3*>(rsp663 + 8);
                        if (al664) {
                            addr_a6c8_492:
                            r8d665 = reinterpret_cast<unsigned char>(v28);
                        } else {
                            addr_ab5f_493:
                            rsp666 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                            *rsp666 = reinterpret_cast<void**>(0xab70);
                            rax667 = quotearg_style(4, v18, 4, v18);
                            rsp668 = reinterpret_cast<struct s3*>(rsp666 + 8);
                            v30 = rax667;
                            goto addr_a950_494;
                        }
                    } else {
                        rsi19 = v30;
                        *reinterpret_cast<void***>(&rdi669) = r12d472;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi669) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&v25) = *reinterpret_cast<unsigned char*>(&rdx100);
                        rsp670 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp670 = reinterpret_cast<void**>(0xa9e4);
                        eax671 = fun_38d0(rdi669, rsi19, rdi669, rsi19);
                        rsp454 = reinterpret_cast<struct s3*>(rsp670 + 8);
                        if (eax671) 
                            goto addr_ab5f_493;
                        if (v624 != 3) 
                            goto addr_a6c8_492;
                        rdx100 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v25)));
                        *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                        if (!*reinterpret_cast<unsigned char*>(&rdx100)) 
                            goto addr_a6c8_492;
                        rdx100 = v629;
                        rsi19 = reinterpret_cast<void**>(3);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        *reinterpret_cast<void***>(&rdi672) = r12d472;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi672) + 4) = 0;
                        rcx22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v30) - reinterpret_cast<unsigned char>(rdx100));
                        rsp673 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp673 = reinterpret_cast<void**>(0xaa26);
                        eax674 = fun_39d0(rdi672, 3, rdx100, rcx22, rdi672, 3, rdx100, rcx22);
                        rsp454 = reinterpret_cast<struct s3*>(rsp673 + 8);
                        if (eax674 >= 0) 
                            goto addr_a6c8_492;
                        rsp675 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp675 = reinterpret_cast<void**>(0xaa33);
                        rax676 = fun_3700();
                        rsp454 = reinterpret_cast<struct s3*>(rsp675 + 8);
                        r9 = rax676;
                        r8b677 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax676) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax676) == 38)));
                        if (r8b677) 
                            goto addr_a6c8_492;
                        v30 = r8b677;
                        v25 = r9;
                        rsp678 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp678 = reinterpret_cast<void**>(0xaa6d);
                        rax679 = quotearg_style(4, v18);
                        v28 = rax679;
                        rsp680 = rsp678 + 8 - 8;
                        *rsp680 = reinterpret_cast<void**>(0xaa87);
                        rax681 = fun_3840();
                        r9 = v25;
                        rcx22 = v28;
                        rdx100 = rax681;
                        rsi19 = *reinterpret_cast<void***>(r9);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rsp682 = rsp680 + 8 - 8;
                        *rsp682 = reinterpret_cast<void**>(0xaaa4);
                        fun_3bd0();
                        rsp454 = reinterpret_cast<struct s3*>(rsp682 + 8);
                        r8d665 = reinterpret_cast<unsigned char>(v30);
                    }
                    addr_a6d0_501:
                    r8_5 = reinterpret_cast<void**>(r8d665 ^ 1);
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    addr_9ede_502:
                    if (r8_5) 
                        goto addr_8b79_418;
                    if (!0 || (rsi19 = v300, *reinterpret_cast<void***>(&rdi683) = r12d472, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi683) + 4) = 0, rsp684 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8), *rsp684 = reinterpret_cast<void**>(0xa425), eax685 = fun_38d0(rdi683, rsi19, rdi683, rsi19), rsp454 = reinterpret_cast<struct s3*>(rsp684 + 8), eax685 >= 0)) {
                        addr_9ef4_428:
                        zf686 = *reinterpret_cast<signed char*>(rbx26 + 31) == 0;
                        v28 = v510;
                        if (zf686) {
                            if (*reinterpret_cast<unsigned char*>(rbx26 + 29) && ((*reinterpret_cast<int32_t*>(&rax687) = v688, *reinterpret_cast<int32_t*>(&rax687 + 4) = 0, rdx100 = v689, *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0, *reinterpret_cast<int32_t*>(&rax687) != v690) || v691 != rdx100)) {
                                r9 = v692;
                                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                r8_5 = r12d472;
                                *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                rsp693 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp693 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffea0);
                                *reinterpret_cast<uint32_t*>(&rcx694) = *reinterpret_cast<unsigned char*>(&v36);
                                *reinterpret_cast<int32_t*>(&rcx694 + 4) = 0;
                                rsi19 = v18;
                                rsp695 = rsp693 - 8;
                                *rsp695 = rcx694;
                                rcx22 = v20;
                                rsp696 = rsp695 - 8;
                                *rsp696 = rdx100;
                                rdx100 = v24;
                                *reinterpret_cast<int32_t*>(&rdx100 + 4) = 0;
                                rsp697 = rsp696 - 8;
                                *rsp697 = rax687;
                                rsp698 = reinterpret_cast<struct s5*>(rsp697 - 8);
                                rsp698->f0 = reinterpret_cast<void**>(0x9af0);
                                eax699 = set_owner_isra_0(rbx26, rsi19, rdx100, rcx22, r8_5, r9, rsp698->f8, rsp698->f10, 1, rsp698->f20);
                                rsp454 = reinterpret_cast<struct s3*>(&rsp698->f8 + 8);
                                if (eax699 == -1) {
                                    addr_8b79_418:
                                    r9 = reinterpret_cast<void**>(0);
                                    goto addr_8b7c_402;
                                } else {
                                    if (!eax699) {
                                        v21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v21) & 0xfffff1ff);
                                        goto addr_9b0b_400;
                                    }
                                }
                            }
                        } else {
                            goto addr_9a37_416;
                        }
                    } else {
                        rsp700 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp700 = reinterpret_cast<void**>(0xa43e);
                        rax701 = quotearg_style(4, v18, 4, v18);
                        rsp580 = reinterpret_cast<struct s3*>(rsp700 + 8);
                        v21 = rax701;
                        goto addr_a451_422;
                    }
                    addr_a950_494:
                    rsp702 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp668) - 8);
                    *rsp702 = reinterpret_cast<void**>(0xa957);
                    rax703 = fun_3840();
                    v28 = rax703;
                    rsp704 = rsp702 + 8 - 8;
                    *rsp704 = reinterpret_cast<void**>(0xa963);
                    rax705 = fun_3700();
                    rcx22 = v30;
                    rdx100 = v28;
                    rsi19 = *reinterpret_cast<void***>(rax705);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp706 = rsp704 + 8 - 8;
                    *rsp706 = reinterpret_cast<void**>(0xa97c);
                    fun_3bd0();
                    rsp454 = reinterpret_cast<struct s3*>(rsp706 + 8);
                    addr_a97c_510:
                    r8d665 = 0;
                    goto addr_a6d0_501;
                    addr_a91b_464:
                    r14_49 = v302;
                    r15_15 = v25;
                    rbx26 = v634;
                    goto addr_a930_511;
                    addr_aab1_477:
                    r14_49 = v302;
                    r15_15 = v25;
                    rbx26 = v634;
                    goto addr_a97c_510;
                    addr_a984_483:
                    r14_49 = v302;
                    r15_15 = v25;
                    rbx26 = v634;
                    r8_707 = v629;
                    addr_a9a0_512:
                    v30 = r8_707;
                    goto addr_a6a4_513;
                    addr_a688_485:
                    rsp708 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp708 = reinterpret_cast<void**>(0xa68d);
                    rax709 = fun_3700();
                    rsp454 = reinterpret_cast<struct s3*>(rsp708 + 8);
                    r14_49 = v302;
                    rbx26 = v634;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax709) == 6)) {
                        addr_a930_511:
                        rsp710 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp710 = reinterpret_cast<void**>(0xa93d);
                        rax711 = quotearg_style(4, r15_15, 4, r15_15);
                        rsp668 = reinterpret_cast<struct s3*>(rsp710 + 8);
                        v30 = rax711;
                        goto addr_a950_494;
                    } else {
                        addr_a6a4_513:
                        less_or_equal712 = reinterpret_cast<signed char>(v30) <= reinterpret_cast<signed char>(v629);
                        *reinterpret_cast<unsigned char*>(&rdx100) = reinterpret_cast<uint1_t>(!less_or_equal712);
                        if (!less_or_equal712) 
                            goto addr_a9c2_490;
                        if (v632) 
                            goto addr_a9c2_490; else 
                            goto addr_a6c8_492;
                    }
                    addr_aae8_474:
                    r14_49 = v302;
                    r15_15 = v25;
                    rbx26 = v634;
                    rsp713 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp713 = reinterpret_cast<void**>(0xab10);
                    rax714 = quotearg_n_style_colon();
                    rsp668 = reinterpret_cast<struct s3*>(rsp713 + 8);
                    v30 = rax714;
                    goto addr_a950_494;
                    addr_aacb_488:
                    r14_49 = v302;
                    r15_15 = v25;
                    r8_707 = rax662;
                    rbx26 = v634;
                    goto addr_a9a0_512;
                    addr_9e7d_450:
                    v584 = reinterpret_cast<void**>(0);
                    r9 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                    r9 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(rbx26 + 12) == 3)));
                    addr_9e93_455:
                    rsi19 = r12d472;
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rsp715 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                    *rsp715 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf6);
                    rcx22 = v47;
                    rdx100 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffcf8);
                    rsp716 = rsp715 - 8;
                    *rsp716 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd00);
                    *reinterpret_cast<int32_t*>(&rax717) = v620;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax717) + 4) = 0;
                    rsp718 = rsp716 - 8;
                    *rsp718 = reinterpret_cast<void**>(0xff);
                    rsp719 = rsp718 - 8;
                    *rsp719 = v18;
                    rsp720 = reinterpret_cast<struct s5*>(rsp719 - 8);
                    rsp720->f0 = r15_15;
                    rsp721 = reinterpret_cast<struct s4**>(reinterpret_cast<uint64_t>(rsp720) - 8);
                    *rsp721 = rax717;
                    rsp722 = reinterpret_cast<struct s6*>(rsp721 - 1);
                    rsp722->f0 = 0x9ed4;
                    eax723 = sparse_copy(r13d447, rsi19, rdx100, rcx22, v584, r9, rsp722->f8, rsp722->f10, rsp722->f18, 0xff, rsp722->f28, rsp722->f30);
                    rsp454 = reinterpret_cast<struct s3*>(&rsp722->f8 + 48);
                    r8_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(eax723) ^ 1);
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    goto addr_9ede_502;
                    addr_8ac6_435:
                    r8d608 = reinterpret_cast<void**>(1);
                    if (v724 < v725 / 0x200) {
                        rsp726 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp726 = reinterpret_cast<void**>(0x8af9);
                        rax727 = fun_38e0();
                        rsp454 = reinterpret_cast<struct s3*>(rsp726 + 8);
                        v302 = rax727;
                        if (reinterpret_cast<signed char>(rax727) >= reinterpret_cast<signed char>(0) || (rsp728 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8), *rsp728 = reinterpret_cast<void**>(0x8b0e), rax729 = fun_3700(), rsp454 = reinterpret_cast<struct s3*>(rsp728 + 8), eax730 = *reinterpret_cast<void***>(rax729), v30 = rax729, eax730 == 6)) {
                            r8d608 = reinterpret_cast<void**>(3);
                            goto addr_9dad_442;
                        } else {
                            if (eax730 == 22 || eax730 == 95) {
                                r8d608 = reinterpret_cast<void**>(2);
                                goto addr_9dad_442;
                            } else {
                                rsp731 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                                *rsp731 = reinterpret_cast<void**>(0x8b42);
                                rax732 = quotearg_style(4, r15_15, 4, r15_15);
                                v28 = rax732;
                                rsp733 = rsp731 + 8 - 8;
                                *rsp733 = reinterpret_cast<void**>(0x8b5c);
                                rax734 = fun_3840();
                                r8_5 = v30;
                                rcx22 = v28;
                                rdx100 = rax734;
                                rsi19 = *reinterpret_cast<void***>(r8_5);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rsp735 = rsp733 + 8 - 8;
                                *rsp735 = reinterpret_cast<void**>(0x8b79);
                                fun_3bd0();
                                rsp454 = reinterpret_cast<struct s3*>(rsp735 + 8);
                                goto addr_8b79_418;
                            }
                        }
                    }
                    addr_9fef_377:
                    if (*reinterpret_cast<unsigned char*>(rbx26 + 60)) {
                        rsp736 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp736 = reinterpret_cast<void**>(0xab99);
                        rax737 = quotearg_style(4, v18, 4, v18);
                        rsp738 = rsp736 + 8 - 8;
                        *rsp738 = reinterpret_cast<void**>(0xabaf);
                        rax739 = fun_3840();
                        rsp740 = rsp738 + 8 - 8;
                        *rsp740 = reinterpret_cast<void**>(0xabc1);
                        fun_3b80(1, rax739, rax737, rcx22);
                        rsp454 = reinterpret_cast<struct s3*>(rsp740 + 8);
                        goto addr_9ff9_373;
                    }
                    addr_89a4_367:
                    if (!*reinterpret_cast<void***>(rbx26 + 40)) {
                        if (!*reinterpret_cast<signed char*>(rbx26 + 51)) {
                            v511 = 0;
                            v510 = reinterpret_cast<void**>(0);
                            goto addr_89e5_387;
                        } else {
                            goto addr_89af_524;
                        }
                    } else {
                        addr_89af_524:
                        rdx100 = rbx26;
                        rsi19 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rsp741 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp454) - 8);
                        *rsp741 = reinterpret_cast<void**>(0x89c0);
                        eax742 = set_file_security_ctx(v18);
                        rsp454 = reinterpret_cast<struct s3*>(rsp741 + 8);
                        r9 = eax742;
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        if (!*reinterpret_cast<signed char*>(&eax742) && *reinterpret_cast<unsigned char*>(rbx26 + 52)) {
                            *reinterpret_cast<unsigned char*>(&v36) = 0;
                            goto addr_8b7c_402;
                        }
                    }
                    addr_79fe_349:
                    if (*reinterpret_cast<uint32_t*>(&v31) == "__libc_start_main") {
                        rcx22 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        v28 = r9;
                        *reinterpret_cast<void***>(&rdi743) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi743) + 4) = 0;
                        rsp744 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp744 = reinterpret_cast<void**>(0x974d);
                        eax745 = fun_3a60(rdi743, v20, rdi743, v20);
                        rsp16 = reinterpret_cast<struct s3*>(rsp744 + 8);
                        r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                        if (!eax745 || (*reinterpret_cast<void***>(&rdi746) = v24, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi746) + 4) = 0, rsp747 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp747 = reinterpret_cast<void**>(0x9775), eax748 = fun_36d0(rdi746, v20), rsp16 = reinterpret_cast<struct s3*>(rsp747 + 8), r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28))), *reinterpret_cast<int32_t*>(&r9 + 4) = 0, eax748 == 0)) {
                            addr_7393_344:
                            r8_5 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&v31) != 0x4000)));
                            if (*reinterpret_cast<unsigned char*>(&v36)) 
                                goto addr_73bd_527;
                            eax749 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 20)) ^ 1);
                            *reinterpret_cast<unsigned char*>(&eax749) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax749) & reinterpret_cast<unsigned char>(r8_5));
                            if (!*reinterpret_cast<unsigned char*>(&eax749)) 
                                goto addr_73bd_527;
                        } else {
                            rsp750 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp750 = reinterpret_cast<void**>(0x9796);
                            rax751 = quotearg_style(4, v18, 4, v18);
                            rsp260 = reinterpret_cast<struct s3*>(rsp750 + 8);
                            r13_29 = rax751;
                            goto addr_8c88_270;
                        }
                    } else {
                        eax752 = *reinterpret_cast<uint32_t*>(&v31);
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax752) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax752) + 1) & 0xbf);
                        if (eax752 == 0x2000 || *reinterpret_cast<uint32_t*>(&v31) == 0xc000) {
                            rcx22 = v753;
                            r13_29 = reinterpret_cast<void**>(0);
                            v28 = r9;
                            *reinterpret_cast<void***>(&rdi754) = v24;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi754) + 4) = 0;
                            rsp755 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp755 = reinterpret_cast<void**>(0x92a5);
                            eax756 = fun_3a60(rdi754, v20, rdi754, v20);
                            rsp16 = reinterpret_cast<struct s3*>(rsp755 + 8);
                            r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                            if (!eax756) 
                                goto addr_7393_344;
                            rsp757 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp757 = reinterpret_cast<void**>(0x92c6);
                            rax758 = quotearg_style(4, v18, 4, v18);
                            rsp260 = reinterpret_cast<struct s3*>(rsp757 + 8);
                            r13_29 = rax758;
                            goto addr_8c88_270;
                        } else {
                            if (*reinterpret_cast<uint32_t*>(&v31) != 0xa000) {
                                rsp759 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp759 = reinterpret_cast<void**>(0x9814);
                                rax760 = quotearg_style(4, r15_15, 4, r15_15);
                                rsp245 = reinterpret_cast<struct s3*>(rsp759 + 8);
                                r12_246 = rax760;
                                goto addr_83d8_249;
                            } else {
                                rsp761 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp761 = reinterpret_cast<void**>(0x7a4f);
                                rax763 = areadlink_with_size(r15_15, v762);
                                rsp764 = reinterpret_cast<struct s3*>(rsp761 + 8);
                                r13_29 = rax763;
                                if (!rax763) {
                                    rsp765 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp764) - 8);
                                    *rsp765 = reinterpret_cast<void**>(0xa0de);
                                    rax766 = quotearg_style(4, r15_15, 4, r15_15);
                                    rsp260 = reinterpret_cast<struct s3*>(rsp765 + 8);
                                    r13_29 = rax766;
                                    goto addr_8c88_270;
                                } else {
                                    rcx22 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 22)));
                                    *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                    rdx100 = v20;
                                    r8_5 = reinterpret_cast<void**>(0xffffffff);
                                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                    rsi19 = v24;
                                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                    rsp767 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp764) - 8);
                                    *rsp767 = reinterpret_cast<void**>(0x7a7a);
                                    eax768 = force_symlinkat(rax763, rsi19, rdx100, rcx22, 0xffffffff);
                                    rsp769 = reinterpret_cast<struct s3*>(rsp767 + 8);
                                    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax768) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(eax768 == 0)) {
                                        rdi17 = r13_29;
                                        rsp770 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp769) - 8);
                                        *rsp770 = reinterpret_cast<void**>(0x9c9c);
                                        fun_36a0(rdi17, rdi17);
                                        rsp16 = reinterpret_cast<struct s3*>(rsp770 + 8);
                                    } else {
                                        if (*reinterpret_cast<signed char*>(rbx26 + 59) != 1 || (*reinterpret_cast<unsigned char*>(&v36) || ((v771 & 0xf000) != 0xa000 || ((v28 = v772, rsp773 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp769) - 8), *rsp773 = reinterpret_cast<void**>(0xa23e), rax774 = fun_3860(r13_29, r13_29), rsp769 = reinterpret_cast<struct s3*>(rsp773 + 8), rdx100 = v28, rdx100 != rax774) || (*reinterpret_cast<void***>(&rdi775) = v24, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi775) + 4) = 0, rsp776 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp769) - 8), *rsp776 = reinterpret_cast<void**>(0xa260), rax777 = areadlinkat_with_size(rdi775, v20), rsp769 = reinterpret_cast<struct s3*>(rsp776 + 8), rax777 == 0))))) {
                                            addr_7a98_540:
                                            rsp778 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp769) - 8);
                                            *rsp778 = reinterpret_cast<void**>(0x7aa0);
                                            fun_36a0(r13_29, r13_29);
                                            rsp779 = rsp778 + 8 - 8;
                                            *rsp779 = reinterpret_cast<void**>(0x7ab1);
                                            rax780 = quotearg_style(4, v18, 4, v18);
                                            r13_29 = rax780;
                                            rsp781 = rsp779 + 8 - 8;
                                            *rsp781 = reinterpret_cast<void**>(0x7ac7);
                                            rax782 = fun_3840();
                                            rcx22 = r13_29;
                                            rsi19 = eax768;
                                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                            rdi17 = reinterpret_cast<void**>(0);
                                            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                                            rdx100 = rax782;
                                            rsp783 = rsp781 + 8 - 8;
                                            *rsp783 = reinterpret_cast<void**>(0x7ad9);
                                            fun_3bd0();
                                            rsp16 = reinterpret_cast<struct s3*>(rsp783 + 8);
                                            goto addr_7ae0_250;
                                        } else {
                                            rsi19 = r13_29;
                                            v28 = rax777;
                                            rsp784 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp769) - 8);
                                            *rsp784 = reinterpret_cast<void**>(0xa27b);
                                            rax785 = fun_3a10(rax777, rsi19);
                                            rsp786 = reinterpret_cast<struct s3*>(rsp784 + 8);
                                            if (*reinterpret_cast<int32_t*>(&rax785)) {
                                                rsp787 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp786) - 8);
                                                *rsp787 = reinterpret_cast<void**>(0xad09);
                                                fun_36a0(v28, v28);
                                                rsp769 = reinterpret_cast<struct s3*>(rsp787 + 8);
                                                goto addr_7a98_540;
                                            } else {
                                                rsp788 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp786) - 8);
                                                *rsp788 = reinterpret_cast<void**>(0xa28f);
                                                fun_36a0(v28, v28);
                                                rdi17 = r13_29;
                                                rsp789 = rsp788 + 8 - 8;
                                                *rsp789 = reinterpret_cast<void**>(0xa297);
                                                fun_36a0(rdi17, rdi17);
                                                rsp16 = reinterpret_cast<struct s3*>(rsp789 + 8);
                                            }
                                        }
                                    }
                                    if (*reinterpret_cast<signed char*>(rbx26 + 51)) 
                                        goto addr_8063_125;
                                    r9 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rbx26 + 29)));
                                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                    r13_29 = reinterpret_cast<void**>(1);
                                    if (r9) {
                                        rcx22 = v790;
                                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                                        *reinterpret_cast<int32_t*>(&rdx791) = v792;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx791) + 4) = 0;
                                        r8_5 = reinterpret_cast<void**>(0x100);
                                        *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                                        v28 = r9;
                                        *reinterpret_cast<void***>(&rdi793) = v24;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi793) + 4) = 0;
                                        rsp794 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                        *rsp794 = reinterpret_cast<void**>(0x9ce5);
                                        eax795 = fun_3c50(rdi793, v20, rdx791, rcx22, 0x100);
                                        rsp16 = reinterpret_cast<struct s3*>(rsp794 + 8);
                                        if (!eax795) {
                                            r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                                            r9 = reinterpret_cast<void**>(0);
                                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                            goto addr_7393_344;
                                        } else {
                                            rsp796 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                            *rsp796 = reinterpret_cast<void**>(0x9cfd);
                                            eax797 = chown_failure_ok(rbx26, v20, rdx791, rcx22, 0x100);
                                            rsp16 = reinterpret_cast<struct s3*>(rsp796 + 8);
                                            r9 = reinterpret_cast<void**>(0);
                                            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                            r13_29 = eax797;
                                            if (!*reinterpret_cast<signed char*>(&eax797)) {
                                                rsp798 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                                *rsp798 = reinterpret_cast<void**>(0x9d1e);
                                                rax799 = fun_3840();
                                                rsp800 = rsp798 + 8 - 8;
                                                *rsp800 = reinterpret_cast<void**>(0x9d26);
                                                rax801 = fun_3700();
                                                rcx22 = v18;
                                                rdi17 = reinterpret_cast<void**>(0);
                                                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                                                rdx100 = rax799;
                                                rsi19 = *reinterpret_cast<void***>(rax801);
                                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                                rsp802 = rsp800 + 8 - 8;
                                                *rsp802 = reinterpret_cast<void**>(0x9d3b);
                                                fun_3bd0();
                                                rsp16 = reinterpret_cast<struct s3*>(rsp802 + 8);
                                                if (*reinterpret_cast<unsigned char*>(rbx26 + 50)) 
                                                    goto addr_7ae0_250;
                                                r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v28)));
                                                r9 = reinterpret_cast<void**>(0);
                                                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                                                goto addr_7393_344;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!*reinterpret_cast<void***>(rbx26 + 40)) {
                        if (!*reinterpret_cast<signed char*>(rbx26 + 51)) {
                            addr_84d0_553:
                            r8_5 = eax749;
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            goto addr_73bd_527;
                        }
                    }
                    rdi17 = v18;
                    rsi19 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    rdx100 = rbx26;
                    v31 = r9;
                    rsp803 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp803 = reinterpret_cast<void**>(0x84ab);
                    eax804 = set_file_security_ctx(rdi17);
                    rsp16 = reinterpret_cast<struct s3*>(rsp803 + 8);
                    r9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v31)));
                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                    r8_5 = eax804;
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&eax804)) {
                        addr_73bd_527:
                        eax805 = v27;
                        if (!*reinterpret_cast<signed char*>(&eax805)) {
                            r12_46 = reinterpret_cast<void**>(1);
                            *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            goto addr_7450_293;
                        } else {
                            r12_46 = eax805;
                            *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                            v32 = 0;
                            goto addr_73df_291;
                        }
                    } else {
                        eax806 = *reinterpret_cast<unsigned char*>(rbx26 + 52);
                        *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax806);
                        if (*reinterpret_cast<unsigned char*>(&eax806)) 
                            goto addr_7ae0_250; else 
                            goto addr_84d0_553;
                    }
                    addr_71d0_220:
                    if (*reinterpret_cast<void***>(rbx26 + 40)) {
                        rsp807 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp807 = reinterpret_cast<void**>(0x71eb);
                        set_file_security_ctx(v18, v18);
                        rsp16 = reinterpret_cast<struct s3*>(rsp807 + 8);
                    }
                    if (v31) {
                        *reinterpret_cast<void***>(v31) = reinterpret_cast<void**>(1);
                    }
                    if (!v27) 
                        goto addr_7210_129;
                    if (*reinterpret_cast<signed char*>(rbx26 + 63)) 
                        break;
                    rdi808 = *reinterpret_cast<void***>(rbx26 + 72);
                    rdx809 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffd80);
                    rsp810 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp810 = reinterpret_cast<void**>(0x86d7);
                    record_file(rdi808, v20, rdx809, rcx22, rdi808, v20, rdx809, rcx22);
                    rsp16 = reinterpret_cast<struct s3*>(rsp810 + 8);
                    r12_46 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v27)));
                    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                    continue;
                    eax811 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
                    if (!*reinterpret_cast<signed char*>(&eax811)) {
                        if (!*reinterpret_cast<void***>(rbx26 + 48)) 
                            goto addr_7238_212;
                        if (!*reinterpret_cast<unsigned char*>(rbx26 + 23)) 
                            goto addr_768c_568;
                    } else {
                        if (v812 == 1) {
                            rsp813 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp813 = reinterpret_cast<void**>(0x87e3);
                            rax816 = src_to_dest_lookup(v814, v815, rdx3, rcx22, r8_5, r9);
                            rsp16 = reinterpret_cast<struct s3*>(rsp813 + 8);
                            r14_49 = rax816;
                            goto addr_8415_571;
                        } else {
                            if (!*reinterpret_cast<void***>(rbx26 + 48) || *reinterpret_cast<unsigned char*>(rbx26 + 23)) {
                                *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                                *reinterpret_cast<int32_t*>(&r14_49 + 4) = 0;
                                goto addr_6eb5_574;
                            }
                        }
                    }
                    *reinterpret_cast<uint32_t*>(&r14_49) = 0;
                    *reinterpret_cast<int32_t*>(&r14_49 + 4) = 0;
                    goto addr_76c3_576;
                    addr_768c_568:
                    if (v817 > 1 || (v27 && *reinterpret_cast<signed char*>(rbx26 + 4) == 3 || (*reinterpret_cast<uint32_t*>(&r14_49) = 0, *reinterpret_cast<int32_t*>(&r14_49 + 4) = 0, *reinterpret_cast<signed char*>(rbx26 + 4) == 4))) {
                        rsp818 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp818 = reinterpret_cast<void**>(0x8412);
                        rax821 = remember_copied(v20, v819, v820, rcx22, r8_5, r9);
                        rsp16 = reinterpret_cast<struct s3*>(rsp818 + 8);
                        r14_49 = rax821;
                    } else {
                        addr_76bb_578:
                        if (*reinterpret_cast<signed char*>(&eax811)) {
                            addr_6eb5_574:
                            if (r13_29 == 17) {
                                rcx22 = v20;
                                rsp822 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp822 = reinterpret_cast<void**>(0x821a);
                                eax823 = fun_3c60(0xffffff9c, r15_15, 0xffffff9c, r15_15);
                                rsp16 = reinterpret_cast<struct s3*>(rsp822 + 8);
                                if (!eax823) 
                                    goto addr_71c6_218;
                                rsp824 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp824 = reinterpret_cast<void**>(0x8227);
                                rax825 = fun_3700();
                                rsp16 = reinterpret_cast<struct s3*>(rsp824 + 8);
                                r13_29 = *reinterpret_cast<void***>(rax825);
                                if (r13_29) 
                                    goto addr_6ebf_581; else 
                                    goto addr_8233_582;
                            } else {
                                addr_6ebf_581:
                                if (r13_29 == 22) {
                                    rsp826 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                    *rsp826 = reinterpret_cast<void**>(0x884e);
                                    rax827 = quotearg_n_style();
                                    rbx26 = rax827;
                                    rsp828 = rsp826 + 8 - 8;
                                    *rsp828 = reinterpret_cast<void**>(0x8864);
                                    quotearg_n_style();
                                    rsp829 = rsp828 + 8 - 8;
                                    *rsp829 = reinterpret_cast<void**>(0x887a);
                                    fun_3840();
                                    r8_5 = rbx26;
                                    r12_46 = reinterpret_cast<void**>(1);
                                    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
                                    rsp830 = rsp829 + 8 - 8;
                                    *rsp830 = reinterpret_cast<void**>(0x8894);
                                    fun_3bd0();
                                    rsp16 = reinterpret_cast<struct s3*>(rsp830 + 8);
                                    *reinterpret_cast<void***>(v30) = reinterpret_cast<void**>(1);
                                    continue;
                                }
                            }
                        } else {
                            addr_76c3_576:
                            r13_29 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v36)));
                            goto addr_7243_231;
                        }
                    }
                    addr_8415_571:
                    if (!r14_49) {
                        addr_6df0_226:
                        eax811 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx26 + 24));
                        goto addr_76bb_578;
                    } else {
                        if ((reinterpret_cast<unsigned char>(v41) & 0xf000) == 0x4000) {
                            addr_6d91_227:
                            rsp831 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp831 = reinterpret_cast<void**>(0x6da7);
                            eax832 = same_nameat();
                            rsp833 = reinterpret_cast<struct s3*>(rsp831 + 8);
                            if (*reinterpret_cast<signed char*>(&eax832)) {
                                rsp834 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp833) - 8);
                                *rsp834 = reinterpret_cast<void**>(0x9225);
                                rax835 = quotearg_n_style();
                                r13_29 = rax835;
                                rsp836 = rsp834 + 8 - 8;
                                *rsp836 = reinterpret_cast<void**>(0x923b);
                                rax837 = quotearg_n_style();
                                rsp838 = rsp836 + 8 - 8;
                                *rsp838 = reinterpret_cast<void**>(0x9251);
                                rax839 = fun_3840();
                                r8_5 = r13_29;
                                rcx22 = rax837;
                                rsi19 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                                rdx100 = rax839;
                                rdi17 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                                rsp840 = rsp838 + 8 - 8;
                                *rsp840 = reinterpret_cast<void**>(0x9265);
                                fun_3bd0();
                                rsp16 = reinterpret_cast<struct s3*>(rsp840 + 8);
                                *reinterpret_cast<void***>(v30) = reinterpret_cast<void**>(1);
                            } else {
                                rcx22 = r14_49;
                                rsp841 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp833) - 8);
                                *rsp841 = reinterpret_cast<void**>(0x6dc6);
                                eax842 = same_nameat();
                                rsp16 = reinterpret_cast<struct s3*>(rsp841 + 8);
                                if (*reinterpret_cast<signed char*>(&eax842)) 
                                    goto addr_97c0_587;
                                if (*reinterpret_cast<signed char*>(rbx26 + 4) == 4) 
                                    goto addr_6df0_226; else 
                                    goto addr_6dd6_589;
                            }
                        } else {
                            rax843 = v47;
                            *reinterpret_cast<int32_t*>(&rax843 + 4) = 0;
                            rdx100 = r14_49;
                            rdi17 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                            rsi19 = v24;
                            *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                            r9 = v20;
                            rsp844 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8 - 8);
                            *rsp844 = rax843;
                            *reinterpret_cast<uint32_t*>(&rax845) = *reinterpret_cast<unsigned char*>(rbx26 + 60);
                            *reinterpret_cast<int32_t*>(&rax845 + 4) = 0;
                            rcx22 = v18;
                            r8_5 = rsi19;
                            *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                            rsp846 = rsp844 - 8;
                            *rsp846 = rax845;
                            rsp847 = rsp846 - 8;
                            *rsp847 = reinterpret_cast<void**>(1);
                            rsp848 = reinterpret_cast<struct s5*>(rsp847 - 8);
                            rsp848->f0 = reinterpret_cast<void**>(0x8467);
                            al849 = create_hard_link(0, rsi19, rdx100, rcx22, r8_5, r9, 1, rsp848->f10, rsp848->f18);
                            rsp16 = reinterpret_cast<struct s3*>(&rsp848->f8 + 8);
                            if (al849) 
                                goto addr_7210_129;
                        }
                    }
                    addr_8473_591:
                    if (!*reinterpret_cast<signed char*>(rbx26 + 51)) 
                        goto addr_7af3_335; else 
                        goto addr_847d_126;
                    addr_6dd6_589:
                    if (*reinterpret_cast<signed char*>(rbx26 + 4) != 3 || !v27) {
                        r15_15 = v18;
                        rsp850 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp850 = reinterpret_cast<void**>(0x958f);
                        rax851 = subst_suffix(r15_15, v20, r14_49, rcx22, r8_5, r9, r15_15, v20, r14_49, rcx22);
                        rsp852 = rsp850 + 8 - 8;
                        *rsp852 = reinterpret_cast<void**>(0x95a4);
                        rax853 = quotearg_n_style();
                        r14_49 = rax853;
                        rsp854 = rsp852 + 8 - 8;
                        *rsp854 = reinterpret_cast<void**>(0x95b6);
                        rax855 = quotearg_n_style();
                        r13_29 = rax855;
                        rsp856 = rsp854 + 8 - 8;
                        *rsp856 = reinterpret_cast<void**>(0x95cc);
                        rax857 = fun_3840();
                        r8_5 = r14_49;
                        rcx22 = r13_29;
                        rdx100 = rax857;
                        rsi19 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        rsp858 = rsp856 + 8 - 8;
                        *rsp858 = reinterpret_cast<void**>(0x95e0);
                        fun_3bd0();
                        rdi17 = rax851;
                        rsp859 = rsp858 + 8 - 8;
                        *rsp859 = reinterpret_cast<void**>(0x95e8);
                        fun_36a0(rdi17, rdi17);
                        rsp16 = reinterpret_cast<struct s3*>(rsp859 + 8);
                        goto addr_8473_591;
                    } else {
                        goto addr_6df0_226;
                    }
                    addr_8233_582:
                    goto addr_71c6_218;
                    if (!reinterpret_cast<int1_t>(r13_29 == 18)) {
                        rsp860 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp860 = reinterpret_cast<void**>(0x8266);
                        rax861 = quotearg_n_style();
                        rbx26 = rax861;
                        rsp862 = rsp860 + 8 - 8;
                        *rsp862 = reinterpret_cast<void**>(0x8278);
                        rax863 = quotearg_n_style();
                        rsp864 = rsp862 + 8 - 8;
                        *rsp864 = reinterpret_cast<void**>(0x828e);
                        rax865 = fun_3840();
                        rsp866 = reinterpret_cast<struct s3*>(rsp864 + 8);
                        r8_5 = rbx26;
                        rcx867 = rax863;
                        rdx868 = rax865;
                    } else {
                        *reinterpret_cast<void***>(&rdi869) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi869) + 4) = 0;
                        rsp870 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp870 = reinterpret_cast<void**>(0x6f02);
                        eax871 = fun_3750(rdi869, v20, rdi869, v20);
                        rsp16 = reinterpret_cast<struct s3*>(rsp870 + 8);
                        if (!eax871 || (rsp872 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp872 = reinterpret_cast<void**>(0x6f0b), rax873 = fun_3700(), rsp16 = reinterpret_cast<struct s3*>(rsp872 + 8), reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax873) == 2))) {
                            al874 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(v41) & 0xf000) != 0x4000)) & *reinterpret_cast<unsigned char*>(rbx26 + 60));
                            *reinterpret_cast<unsigned char*>(&v36) = al874;
                            if (al874) {
                                r13_29 = reinterpret_cast<void**>(1);
                                rsp875 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                                *rsp875 = reinterpret_cast<void**>(0x8809);
                                rax876 = fun_3840();
                                rsp877 = rsp875 + 8 - 8;
                                *rsp877 = reinterpret_cast<void**>(0x8818);
                                fun_3b80(1, rax876, 5, rcx22, 1, rax876, 5, rcx22);
                                rsp878 = rsp877 + 8 - 8;
                                *rsp878 = reinterpret_cast<void**>(0x882e);
                                emit_verbose(r15_15, v18, v48, rcx22, r8_5, r9);
                                rsp16 = reinterpret_cast<struct s3*>(rsp878 + 8);
                                goto addr_7243_231;
                            } else {
                                *reinterpret_cast<unsigned char*>(&v36) = 1;
                                r13_29 = reinterpret_cast<void**>(1);
                                goto addr_7243_231;
                            }
                        } else {
                            rsp879 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp879 = reinterpret_cast<void**>(0x86fe);
                            rax880 = quotearg_n_style();
                            rbx26 = rax880;
                            rsp881 = rsp879 + 8 - 8;
                            *rsp881 = reinterpret_cast<void**>(0x8710);
                            rax882 = quotearg_n_style();
                            rsp883 = rsp881 + 8 - 8;
                            *rsp883 = reinterpret_cast<void**>(0x8726);
                            rax884 = fun_3840();
                            rsp866 = reinterpret_cast<struct s3*>(rsp883 + 8);
                            r8_5 = rbx26;
                            rcx867 = rax882;
                            rdx868 = rax884;
                        }
                    }
                    rsp885 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp866) - 8);
                    *rsp885 = reinterpret_cast<void**>(0x82a3);
                    fun_3bd0();
                    rsp886 = rsp885 + 8 - 8;
                    *rsp886 = reinterpret_cast<void**>(0x82b6);
                    forget_created(v887, v888, rdx868, rcx867, r8_5, r9);
                    rsp16 = reinterpret_cast<struct s3*>(rsp886 + 8);
                    goto addr_7190_197;
                    addr_6fa3_214:
                    if ((*reinterpret_cast<uint32_t*>(&v339) & 0xf000) == 0xa000 && (rdi889 = *reinterpret_cast<void***>(rbx26 + 72), rsp890 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp890 = reinterpret_cast<void**>(0x6fc6), eax891 = seen_file(rdi889, v20, rdx3, rcx22, rdi889, v20, rdx3, rcx22), rsp16 = reinterpret_cast<struct s3*>(rsp890 + 8), !!*reinterpret_cast<signed char*>(&eax891))) {
                        rsp892 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp892 = reinterpret_cast<void**>(0x6fe4);
                        rax893 = quotearg_n_style();
                        rbx26 = rax893;
                        rsp894 = rsp892 + 8 - 8;
                        *rsp894 = reinterpret_cast<void**>(0x6ff6);
                        quotearg_n_style();
                        rsp895 = rsp894 + 8 - 8;
                        *rsp895 = reinterpret_cast<void**>(0x700c);
                        fun_3840();
                        r8_5 = rbx26;
                        rsp896 = rsp895 + 8 - 8;
                        *rsp896 = reinterpret_cast<void**>(0x7020);
                        fun_3bd0();
                        rsp16 = reinterpret_cast<struct s3*>(rsp896 + 8);
                        continue;
                    }
                    addr_818f_206:
                    if (!*reinterpret_cast<void***>(rbx26 + 48)) 
                        goto addr_99be_603;
                    if (v897 > 1) 
                        goto addr_81a7_204;
                    addr_99be_603:
                    zf898 = *reinterpret_cast<signed char*>(rbx26 + 4) == 2;
                    r13_29 = reinterpret_cast<void**>(17);
                    v48 = reinterpret_cast<void**>(0);
                    if (!zf898) 
                        goto addr_6d20_36;
                    if ((v899 & 0xf000) != 0x8000) {
                        addr_81a7_204:
                        *reinterpret_cast<void***>(&rdi900) = v24;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi900) + 4) = 0;
                        rdx3 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                        rsp901 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp901 = reinterpret_cast<void**>(0x81bb);
                        eax902 = fun_3750(rdi900, v20, rdi900, v20);
                        rsp16 = reinterpret_cast<struct s3*>(rsp901 + 8);
                        if (eax902 && (rsp903 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8), *rsp903 = reinterpret_cast<void**>(0x81c4), rax904 = fun_3700(), rsp16 = reinterpret_cast<struct s3*>(rsp903 + 8), !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax904) == 2))) {
                            rsp905 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                            *rsp905 = reinterpret_cast<void**>(0xa04c);
                            rax906 = quotearg_style(4, v18, 4, v18);
                            r14_49 = rax906;
                            rsp907 = rsp905 + 8 - 8;
                            *rsp907 = reinterpret_cast<void**>(0xa062);
                            fun_3840();
                            rsp908 = rsp907 + 8 - 8;
                            *rsp908 = reinterpret_cast<void**>(0xa075);
                            fun_3bd0();
                            rsp16 = reinterpret_cast<struct s3*>(rsp908 + 8);
                            continue;
                        }
                    } else {
                        goto addr_6d20_36;
                    }
                    eax909 = *reinterpret_cast<unsigned char*>(rbx26 + 60);
                    *reinterpret_cast<unsigned char*>(&v36) = *reinterpret_cast<unsigned char*>(&eax909);
                    if (*reinterpret_cast<unsigned char*>(&eax909)) {
                        r13_29 = reinterpret_cast<void**>(17);
                        rsp910 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp910 = reinterpret_cast<void**>(0x9d6f);
                        rax911 = quotearg_style(4, v18, 4, v18);
                        rsp912 = rsp910 + 8 - 8;
                        *rsp912 = reinterpret_cast<void**>(0x9d85);
                        rax913 = fun_3840();
                        rdx3 = rax911;
                        rsp914 = rsp912 + 8 - 8;
                        *rsp914 = reinterpret_cast<void**>(0x9d97);
                        fun_3b80(1, rax913, rdx3, rcx22, 1, rax913, rdx3, rcx22);
                        rsp16 = reinterpret_cast<struct s3*>(rsp914 + 8);
                        v48 = reinterpret_cast<void**>(0);
                        goto addr_6d20_36;
                    } else {
                        *reinterpret_cast<unsigned char*>(&v36) = 1;
                        goto addr_81e9_175;
                    }
                    addr_8f10_140:
                    if (!*reinterpret_cast<void***>(rbx26 + 24)) {
                        addr_808c_124:
                        if (*reinterpret_cast<void***>(rbx26 + 8) == 2) 
                            goto addr_7210_129;
                        if (*reinterpret_cast<void***>(rbx26 + 8) != 3) 
                            goto addr_80a1_123;
                        rcx22 = v20;
                        r8_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) + 0xfffffffffffffe10);
                        rsi19 = v18;
                        rsp915 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                        *rsp915 = reinterpret_cast<void**>(0x947a);
                        al916 = overwrite_ok(rbx26, rsi19, v24, rcx22, r8_5, r9);
                        rsp16 = reinterpret_cast<struct s3*>(rsp915 + 8);
                        if (al916) 
                            goto addr_80a1_123; else 
                            goto addr_9482_613;
                    } else {
                        goto addr_85ca_121;
                    }
                    addr_7fe8_141:
                    if (v31) {
                        *reinterpret_cast<void***>(v31) = reinterpret_cast<void**>(1);
                    }
                    rsp917 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                    *rsp917 = reinterpret_cast<void**>(0x8011);
                    rax920 = remember_copied(v20, v918, v919, rcx22, r8_5, r9);
                    rsp16 = reinterpret_cast<struct s3*>(rsp917 + 8);
                    rdx100 = rax920;
                    if (!rax920) 
                        goto addr_7210_129;
                    rax921 = v47;
                    *reinterpret_cast<int32_t*>(&rax921 + 4) = 0;
                    rdi17 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
                    rsi19 = v24;
                    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                    r9 = v20;
                    rcx22 = v18;
                    rsp922 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8 - 8);
                    *rsp922 = rax921;
                    *reinterpret_cast<uint32_t*>(&rax923) = *reinterpret_cast<unsigned char*>(rbx26 + 60);
                    *reinterpret_cast<int32_t*>(&rax923 + 4) = 0;
                    r8_5 = rsi19;
                    *reinterpret_cast<int32_t*>(&r8_5 + 4) = 0;
                    rsp924 = rsp922 - 8;
                    *rsp924 = rax923;
                    rsp925 = rsp924 - 8;
                    *rsp925 = reinterpret_cast<void**>(1);
                    rsp926 = reinterpret_cast<struct s5*>(rsp925 - 8);
                    rsp926->f0 = reinterpret_cast<void**>(0x804d);
                    al927 = create_hard_link(0, rsi19, rdx100, rcx22, r8_5, r9, 1, rsp926->f10, rsp926->f18);
                    rsp16 = reinterpret_cast<struct s3*>(&rsp926->f8 + 8);
                    if (al927) 
                        goto addr_7210_129;
                    if (!*reinterpret_cast<signed char*>(rbx26 + 51)) 
                        goto addr_7190_197; else 
                        goto addr_8063_125;
                }
                continue;
                addr_97c0_587:
                rsi928 = top_level_src_name;
                rsp929 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp16) - 8);
                *rsp929 = reinterpret_cast<void**>(0x97d1);
                quotearg_style(4, rsi928, 4, rsi928);
                rsp930 = rsp929 + 8 - 8;
                *rsp930 = reinterpret_cast<void**>(0x97e7);
                fun_3840();
                rsp931 = rsp930 + 8 - 8;
                *rsp931 = reinterpret_cast<void**>(0x97f8);
                fun_3bd0();
                rsp16 = reinterpret_cast<struct s3*>(rsp931 + 8);
                if (*reinterpret_cast<void***>(rbx26 + 24)) 
                    goto addr_8668_127;
                continue;
                addr_9f34_148:
                continue;
                addr_9482_613:
            }
            addr_8664_150:
        }
    }
}

uint32_t force_linkat(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

signed char create_hard_link(void** rdi, void** esi, void** rdx, void** rcx, void** r8d, void** r9, signed char a7, int32_t a8, unsigned char a9) {
    int64_t rdi10;
    int64_t rdx11;
    int32_t r12d12;
    void** r8_13;
    uint32_t eax14;
    void** r9_15;
    void** v16;
    void** rax17;
    uint32_t ebp18;
    void** rax19;
    void** rax20;
    int32_t eax21;

    *reinterpret_cast<void***>(&rdi10) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
    *reinterpret_cast<void***>(&rdx11) = r8d;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
    r12d12 = a8;
    *reinterpret_cast<uint32_t*>(&r8_13) = static_cast<uint32_t>(a9) << 10;
    *reinterpret_cast<int32_t*>(&r8_13 + 4) = 0;
    eax14 = force_linkat(rdi10, rdx, rdx11, r9);
    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax14 == 0))) {
        *reinterpret_cast<int32_t*>(&r9_15) = 0;
        *reinterpret_cast<int32_t*>(&r9_15 + 4) = 0;
        if (!rdi) {
            rax17 = subst_suffix(rcx, r9, rdx, v16, r8_13, 0);
            r9_15 = rax17;
        }
        quotearg_n_style();
        quotearg_n_style();
        fun_3840();
        r12d12 = 0;
        fun_3bd0();
        fun_36a0(r9_15, r9_15);
    } else {
        ebp18 = eax14 >> 31;
        *reinterpret_cast<unsigned char*>(&r12d12) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d12) & *reinterpret_cast<unsigned char*>(&ebp18));
        if (*reinterpret_cast<unsigned char*>(&r12d12)) {
            rax19 = quotearg_style(4, rcx, 4, rcx);
            rax20 = fun_3840();
            fun_3b80(1, rax20, rax19, v16);
        } else {
            r12d12 = 1;
        }
    }
    eax21 = r12d12;
    return *reinterpret_cast<signed char*>(&eax21);
}

void restore_default_fscreatecon_or_die(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;
    void** rax8;
    void** rax9;
    void** rax10;
    void** rax11;
    void** rax12;
    void** rdi13;
    void** rax14;
    int64_t rbx15;

    rax7 = fun_3700();
    *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(95);
    rax8 = fun_3840();
    fun_3bd0();
    rax9 = quotearg_n_style();
    rax10 = quotearg_n_style();
    fun_3b80(1, "%s -> %s", rax10, rax9);
    if (rax8) {
        rax11 = quotearg_style(4, rax8, 4, rax8);
        rax12 = fun_3840();
        fun_3b80(1, rax12, rax11, rax9);
    }
    rdi13 = stdout;
    rax14 = *reinterpret_cast<void***>(rdi13 + 40);
    if (reinterpret_cast<unsigned char>(rax14) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi13 + 48))) {
        *reinterpret_cast<void***>(rdi13 + 40) = rax14 + 1;
        *reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(10);
        goto rbx15;
    }
}

void strmode(int64_t rdi, void* rsi, int64_t rdx, void** rcx);

signed char overwrite_ok(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9) {
    int64_t rax7;
    signed char al8;
    void** rdi9;
    int32_t eax10;
    void* rsp11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** rcx15;
    void** rdi16;
    void** rdx17;
    void** v18;
    void** v19;
    void** v20;
    int64_t rdi21;
    void** r12d22;
    void** rax23;
    void* rsp24;
    void** r13_25;
    void** v26;
    void** rax27;
    void* rsp28;
    void** rdx29;
    void** rax30;
    void** v31;
    int64_t v32;
    int64_t rax33;
    int32_t ebx34;
    void** rax35;
    int64_t v36;
    int64_t rdi37;
    void** rdx38;
    int32_t eax39;
    void** rax40;
    int64_t v41;

    rax7 = g28;
    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 24)) & 0xf000) == 0xa000 || ((al8 = can_write_any_file(rdi, rsi), !!al8) || (rdi9 = edx, *reinterpret_cast<int32_t*>(&rdi9 + 4) = 0, eax10 = fun_3790(rdi9, rcx, 2, 0x200, r8, r9), rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 32 - 8 + 8 - 8 + 8), eax10 == 0))) {
        rax12 = quotearg_style(4, rsi, 4, rsi);
        r12_13 = program_name;
        rax14 = fun_3840();
        rcx15 = r12_13;
        rdi16 = stderr;
        rdx17 = rax14;
        fun_3ca0(rdi16, 1, rdx17, rcx15, rax12, r9, v18, v19, v20, rax7);
    } else {
        *reinterpret_cast<void***>(&rdi21) = *reinterpret_cast<void***>(r8 + 24);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
        strmode(rdi21, reinterpret_cast<int64_t>(rsp11) + 12, 2, 0x200);
        r12d22 = *reinterpret_cast<void***>(r8 + 24);
        rax23 = quotearg_style(4, rsi, 4, rsi);
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
        *reinterpret_cast<uint32_t*>(&r12_13) = reinterpret_cast<unsigned char>(r12d22) & 0xfff;
        *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
        r13_25 = program_name;
        if (*reinterpret_cast<void***>(rdi + 24) || *reinterpret_cast<unsigned char*>(rdi + 20) & 0xffff00) {
            v26 = reinterpret_cast<void**>(0x5f4c);
            rax27 = fun_3840();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            rdx29 = rax27;
        } else {
            v26 = reinterpret_cast<void**>(0x5f93);
            rax30 = fun_3840();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
            rdx29 = rax30;
        }
        rcx15 = r13_25;
        rdi16 = stderr;
        fun_3ca0(rdi16, 1, rdx29, rcx15, rax23, r12_13, reinterpret_cast<int64_t>(rsp28) - 8 + 21, v26, v31, v32);
        rdx17 = v26;
    }
    rax33 = rax7 - g28;
    if (!rax33) {
    }
    fun_3880();
    ebx34 = *reinterpret_cast<int32_t*>(&rdx17);
    rax35 = fun_38e0();
    if (reinterpret_cast<signed char>(rax35) >= reinterpret_cast<signed char>(0)) 
        goto addr_5fcb_12;
    quotearg_style(4, 1, 4, 1);
    fun_3840();
    fun_3700();
    fun_3bd0();
    goto v36;
    addr_5fcb_12:
    if (!(!*reinterpret_cast<signed char*>(&ebx34) || ((*reinterpret_cast<int32_t*>(&rdi37) = *reinterpret_cast<int32_t*>(&rdi16), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi37) + 4) = 0, rdx38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax35) - reinterpret_cast<unsigned char>(rcx15)), eax39 = fun_39d0(rdi37, 3, rdx38, rcx15, rdi37, 3, rdx38, rcx15), eax39 >= 0) || (rax40 = fun_3700(), !!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax40) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax40) == 38))))))) {
        quotearg_style(4, 1, 4, 1);
        fun_3840();
        fun_3bd0();
    }
    goto v41;
}

void** ximalloc(void* rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** subst_suffix(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...) {
    void** rsi7;
    void** rax8;
    void** r15_9;
    void** rax10;

    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) - reinterpret_cast<unsigned char>(rdi));
    rax8 = fun_3860(rdx);
    r15_9 = rax8 + 1;
    rax10 = ximalloc(reinterpret_cast<unsigned char>(r15_9) + reinterpret_cast<unsigned char>(rsi7), rsi7, rdx, rcx, r8);
    fun_3ab0(reinterpret_cast<unsigned char>(rax10) + reinterpret_cast<unsigned char>(rsi7), rdx, r15_9);
}

void* fun_3930(int64_t rdi);

int32_t fun_3c70(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** xalignalloc(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** fun_39a0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** full_write(int64_t rdi, void** rsi, void** rdx);

void** sparse_copy(void** edi, void** esi, void** rdx, void** rcx, void** r8, void** r9d, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12) {
    void** r14d13;
    void** v14;
    void** v15;
    void** rdx16;
    void** v17;
    void** rcx18;
    void** eax19;
    void** v20;
    void** v21;
    void** rsi22;
    void** v23;
    void** v24;
    void** v25;
    void** v26;
    void** v27;
    unsigned char v28;
    void** r15d29;
    void** rdi30;
    void** v31;
    void** r13d32;
    void** r12_33;
    int64_t rdi34;
    void* rax35;
    void** rax36;
    int64_t rax37;
    void** r14d38;
    void** r15_39;
    void** v40;
    void** r12_41;
    void** r14d42;
    void** r8d43;
    void** r12_44;
    uint32_t ebx45;
    void** rax46;
    int64_t rdi47;
    int32_t eax48;
    void** rax49;
    void** eax50;
    int32_t eax51;
    void** rax52;
    void** r11_53;
    void** rax54;
    void** r11_55;
    void** v56;
    void** rax57;
    void** rbp58;
    void** r14_59;
    void** rbx60;
    unsigned char v61;
    void** r12_62;
    unsigned char v63;
    uint32_t v64;
    int64_t rdi65;
    void** rax66;
    void** eax67;
    uint32_t eax68;
    void** rax69;

    r14d13 = esi;
    v14 = rdx;
    v15 = rcx;
    rdx16 = a10;
    v17 = a8;
    rcx18 = a12;
    eax19 = a7;
    v20 = edi;
    v21 = a9;
    rsi22 = a11;
    *reinterpret_cast<void***>(rcx18) = reinterpret_cast<void**>(0);
    v23 = r8;
    v24 = r9d;
    v25 = rdx16;
    v26 = rsi22;
    v27 = rcx18;
    v28 = *reinterpret_cast<unsigned char*>(&r9d);
    *reinterpret_cast<void***>(rsi22) = reinterpret_cast<void**>(0);
    if (r8 || !*reinterpret_cast<signed char*>(&eax19)) {
        if (!v25) {
            addr_618f_3:
            r15d29 = reinterpret_cast<void**>(1);
        } else {
            addr_61a8_4:
            rdi30 = v15;
            v31 = r14d13;
            if (v23) {
                rdi30 = v23;
                goto addr_61be_6;
            }
        }
    } else {
        if (!rdx16) 
            goto addr_618f_3;
        r13d32 = edi;
        r12_33 = rdx16;
        do {
            addr_6150_9:
            r8 = reinterpret_cast<void**>(0x7fffffffc0000000);
            r9d = reinterpret_cast<void**>(0);
            rdx16 = r14d13;
            *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
            *reinterpret_cast<void***>(&rdi34) = r13d32;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
            if (reinterpret_cast<unsigned char>(r12_33) <= reinterpret_cast<unsigned char>(0x7fffffffc0000000)) {
                r8 = r12_33;
            }
            rcx18 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi22) = 0;
            *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
            rax35 = fun_3930(rdi34);
            if (!rax35) 
                goto addr_6663_12;
            if (reinterpret_cast<int64_t>(rax35) >= reinterpret_cast<int64_t>(0)) {
                *reinterpret_cast<void***>(v26) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)) + reinterpret_cast<uint64_t>(rax35));
                r12_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_33) - reinterpret_cast<uint64_t>(rax35));
                if (r12_33) 
                    goto addr_6150_9; else 
                    goto addr_618f_3;
            }
            rax36 = fun_3700();
            rcx18 = *reinterpret_cast<void***>(rax36);
            *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
            if (rcx18 == 38) 
                goto addr_65c0_16;
            if (reinterpret_cast<signed char>(rcx18) > reinterpret_cast<signed char>(26)) 
                goto addr_6540_18;
            if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rcx18) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rcx18 == 0)) 
                continue;
            rax37 = 1 << *reinterpret_cast<unsigned char*>(&rcx18);
            if (*reinterpret_cast<uint32_t*>(&rax37) & 0x4440200) 
                goto addr_65c0_16;
            if (rcx18 == 1) 
                goto addr_667c_22;
        } while (rcx18 == 4);
        goto addr_6537_24;
    }
    addr_6257_25:
    return r15d29;
    addr_61be_6:
    r14d38 = reinterpret_cast<void**>(0);
    r15_39 = reinterpret_cast<void**>(0);
    v40 = rdi30;
    goto addr_61cf_26;
    addr_6663_12:
    v25 = r12_33;
    if (!*reinterpret_cast<void***>(v26)) 
        goto addr_61a8_4;
    goto addr_618f_3;
    addr_65c0_16:
    v25 = r12_33;
    goto addr_61a8_4;
    addr_6540_18:
    v25 = r12_33;
    if (rcx18 == 95) 
        goto addr_61a8_4;
    addr_654e_28:
    r15d29 = reinterpret_cast<void**>(0);
    quotearg_n_style();
    quotearg_n_style();
    fun_3840();
    fun_3bd0();
    goto addr_6257_25;
    addr_6537_24:
    goto addr_654e_28;
    addr_667c_22:
    v25 = r12_33;
    if (!*reinterpret_cast<void***>(v26)) {
        goto addr_61a8_4;
    }
    addr_66d2_30:
    r12_41 = r15_39;
    r14d42 = v31;
    r8d43 = r14d38;
    addr_66a3_31:
    if (!*reinterpret_cast<signed char*>(&r8d43)) 
        goto addr_618f_3;
    r12_44 = r12_41;
    ebx45 = *reinterpret_cast<unsigned char*>(&v24);
    rax46 = fun_38e0();
    if (reinterpret_cast<signed char>(rax46) >= reinterpret_cast<signed char>(0)) 
        goto addr_5fcb_34;
    quotearg_style(4, v21, 4, v21);
    fun_3840();
    fun_3700();
    fun_3bd0();
    return 0;
    addr_5fcb_34:
    if (!*reinterpret_cast<signed char*>(&ebx45) || ((*reinterpret_cast<void***>(&rdi47) = r14d42, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi47) + 4) = 0, eax48 = fun_39d0(rdi47, 3, reinterpret_cast<unsigned char>(rax46) - reinterpret_cast<unsigned char>(r12_44), r12_44), eax48 >= 0) || (rax49 = fun_3700(), *reinterpret_cast<unsigned char*>(&r12_44) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax49) == 95)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax49) == 38))), !!*reinterpret_cast<unsigned char*>(&r12_44)))) {
        r12_44 = reinterpret_cast<void**>(1);
    } else {
        quotearg_style(4, v21, 4, v21);
        fun_3840();
        fun_3bd0();
    }
    return r12_44;
    addr_661d_39:
    r15d29 = rcx18;
    quotearg_style(4, v21, 4, v21);
    fun_3840();
    fun_3700();
    fun_3bd0();
    goto addr_6257_25;
    addr_6698_40:
    r14d42 = v31;
    r8d43 = r13d32;
    r12_41 = r15_39;
    goto addr_66a3_31;
    addr_63f5_41:
    r15d29 = eax50;
    goto addr_6257_25;
    addr_65d0_42:
    r15d29 = reinterpret_cast<void**>(0);
    quotearg_style(4, v17, 4, v17);
    fun_3840();
    fun_3bd0();
    goto addr_6257_25;
    addr_621e_43:
    r15d29 = reinterpret_cast<void**>(0);
    quotearg_style(4, v17, 4, v17);
    fun_3840();
    fun_3bd0();
    goto addr_6257_25;
    while (1) {
        addr_6478_44:
        eax51 = fun_3c70(rdi30, rsi22, rdx16, rcx18, r8);
        rax52 = xalignalloc(static_cast<int64_t>(eax51), v15, rdx16, rcx18, r8);
        r11_53 = rax52;
        *reinterpret_cast<void***>(v14) = r11_53;
        while (1) {
            do {
                rdx16 = v15;
                rsi22 = r11_53;
                rdi30 = v20;
                *reinterpret_cast<int32_t*>(&rdi30 + 4) = 0;
                if (reinterpret_cast<unsigned char>(v25) <= reinterpret_cast<unsigned char>(rdx16)) {
                    rdx16 = v25;
                }
                rax54 = fun_39a0(rdi30, rsi22, rdx16, rcx18, r8);
                r11_55 = r11_53;
                v56 = rax54;
                if (reinterpret_cast<signed char>(rax54) < reinterpret_cast<signed char>(0)) 
                    break;
                if (!rax54) 
                    goto addr_66d2_30;
                rax57 = v26;
                rbp58 = v56;
                rcx18 = r14d38;
                r14_59 = r11_55;
                rbx60 = v40;
                *reinterpret_cast<void***>(rax57) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax57)) + reinterpret_cast<unsigned char>(rbp58));
                v61 = reinterpret_cast<uint1_t>(!!v23);
                do {
                    addr_62a0_50:
                    r12_62 = rbp58;
                    if (reinterpret_cast<unsigned char>(rbx60) > reinterpret_cast<unsigned char>(rbp58)) {
                        rbx60 = rbp58;
                    }
                    r13d32 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rbx60)) & v61);
                    if (!r13d32) {
                        if (rbx60 == rbp58 && *reinterpret_cast<unsigned char*>(&rcx18) != 1 || (r13d32 = rcx18, rbx60 == 0)) {
                            addr_63b2_54:
                            r15_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_39) + reinterpret_cast<unsigned char>(rbx60));
                            r9d = reinterpret_cast<void**>(0);
                            v63 = *reinterpret_cast<unsigned char*>(&rcx18);
                            v64 = 1;
                            if (!*reinterpret_cast<unsigned char*>(&rcx18)) {
                                addr_6344_55:
                                *reinterpret_cast<void***>(&rdi65) = v31;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi65) + 4) = 0;
                                rdx16 = r15_39;
                                rsi22 = r11_55;
                                rax66 = full_write(rdi65, rsi22, rdx16);
                                r9d = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r9d)));
                                rcx18 = reinterpret_cast<void**>(static_cast<uint32_t>(v63));
                                *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                                if (r15_39 != rax66) 
                                    goto addr_661d_39;
                            } else {
                                addr_63cc_56:
                                rdx16 = reinterpret_cast<void**>(static_cast<uint32_t>(v28));
                                *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                                rsi22 = v21;
                                rcx18 = r15_39;
                                eax50 = create_hole(v31, rsi22, rdx16, rcx18, r8);
                                r9d = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r9d)));
                                if (!*reinterpret_cast<signed char*>(&eax50)) 
                                    goto addr_63f5_41;
                            }
                        } else {
                            goto addr_6419_58;
                        }
                        *reinterpret_cast<uint32_t*>(&rax57) = v64;
                        if (!*reinterpret_cast<uint32_t*>(&rax57)) {
                            r11_55 = r14_59;
                            rbp58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp58) - reinterpret_cast<unsigned char>(rbx60));
                            rcx18 = r13d32;
                            *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                            r14_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_59) + reinterpret_cast<unsigned char>(rbx60));
                            r15_39 = rbx60;
                            continue;
                        } else {
                            if (!rbx60) {
                                if (*reinterpret_cast<unsigned char*>(&r9d)) 
                                    goto addr_65b0_63;
                                *reinterpret_cast<int32_t*>(&r12_62) = 0;
                                *reinterpret_cast<int32_t*>(&r12_62 + 4) = 0;
                            } else {
                                if (*reinterpret_cast<unsigned char*>(&r9d)) {
                                    r15_39 = rbx60;
                                    rcx18 = r13d32;
                                    r11_55 = r14_59;
                                    *reinterpret_cast<int32_t*>(&rbx60) = 0;
                                    *reinterpret_cast<int32_t*>(&rbx60 + 4) = 0;
                                    goto addr_62a0_50;
                                }
                            }
                            r11_55 = r14_59;
                            rcx18 = r13d32;
                            *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                            r14_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_59) + reinterpret_cast<unsigned char>(rbx60));
                            rbp58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_62) - reinterpret_cast<unsigned char>(rbx60));
                            *reinterpret_cast<int32_t*>(&r15_39) = 0;
                            *reinterpret_cast<int32_t*>(&r15_39 + 4) = 0;
                            continue;
                        }
                    } else {
                        rsi22 = r14_59;
                        rdx16 = rbx60;
                        do {
                            if (*reinterpret_cast<void***>(rsi22)) 
                                break;
                            ++rsi22;
                            --rdx16;
                            if (!rdx16) 
                                goto addr_6400_71;
                        } while (*reinterpret_cast<unsigned char*>(&rdx16) & 15);
                        goto addr_62de_73;
                    }
                    eax67 = r13d32;
                    rdx16 = rcx18;
                    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                    r13d32 = reinterpret_cast<void**>(0);
                    addr_6309_75:
                    *reinterpret_cast<unsigned char*>(&r9d) = reinterpret_cast<uint1_t>(!!r15_39);
                    r9d = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9d) & reinterpret_cast<unsigned char>(rdx16));
                    if (rbx60 != rbp58 || !*reinterpret_cast<unsigned char*>(&eax67)) {
                        addr_6410_76:
                        if (*reinterpret_cast<unsigned char*>(&r9d)) {
                            v64 = 0;
                        } else {
                            addr_6419_58:
                            r15_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_39) + reinterpret_cast<unsigned char>(rbx60));
                            rax57 = r15_39 - 0x8000000000000000;
                            if (reinterpret_cast<unsigned char>(rax57) < reinterpret_cast<unsigned char>(rbx60)) 
                                goto addr_65d0_42; else 
                                goto addr_6432_78;
                        }
                    } else {
                        if (!*reinterpret_cast<unsigned char*>(&r9d)) {
                            r13d32 = reinterpret_cast<void**>(0);
                            goto addr_63b2_54;
                        } else {
                            v64 = 1;
                            r13d32 = reinterpret_cast<void**>(0);
                        }
                    }
                    v63 = *reinterpret_cast<unsigned char*>(&rcx18);
                    if (*reinterpret_cast<unsigned char*>(&rcx18)) 
                        goto addr_63cc_56; else 
                        goto addr_6344_55;
                    addr_6432_78:
                    rbp58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp58) - reinterpret_cast<unsigned char>(rbx60));
                    r14_59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_59) + reinterpret_cast<unsigned char>(rbx60));
                    rcx18 = r13d32;
                    *reinterpret_cast<int32_t*>(&rcx18 + 4) = 0;
                    continue;
                    addr_6400_71:
                    *reinterpret_cast<unsigned char*>(&rax57) = reinterpret_cast<uint1_t>(!!r15_39);
                    r9d = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rcx18) ^ 1) & *reinterpret_cast<uint32_t*>(&rax57));
                    goto addr_6410_76;
                    addr_62de_73:
                    eax68 = fun_39c0(r14_59, rsi22, rdx16, r14_59, rsi22, rdx16);
                    rcx18 = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rcx18)));
                    r11_55 = r11_55;
                    r13d32 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(eax68 == 0)));
                    rdx16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13d32) ^ reinterpret_cast<unsigned char>(rcx18));
                    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&eax67) = reinterpret_cast<uint1_t>(!!eax68);
                    goto addr_6309_75;
                } while (rbp58);
                addr_6448_85:
                rdi30 = v56;
                v25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(rdi30));
                *reinterpret_cast<void***>(v27) = r13d32;
                if (!v25) 
                    goto addr_6698_40; else 
                    continue;
                addr_65b0_63:
                *reinterpret_cast<int32_t*>(&r15_39) = 0;
                *reinterpret_cast<int32_t*>(&r15_39 + 4) = 0;
                goto addr_6448_85;
                r14d38 = r13d32;
                r11_53 = *reinterpret_cast<void***>(v14);
            } while (r11_53);
            break;
            rax69 = fun_3700();
            if (*reinterpret_cast<void***>(rax69) != 4) 
                goto addr_621e_43;
            addr_61cf_26:
            r11_53 = *reinterpret_cast<void***>(v14);
            if (!r11_53) 
                goto addr_6478_44;
        }
    }
}

void** fun_3ae0(void** rdi, void** rsi, void** rdx, ...);

void** simple_pattern = reinterpret_cast<void**>(67);

unsigned char g178e0 = 0;

void** samedir_template(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbx5;
    void** rax6;
    void** rax7;
    void** rdi8;
    void** rax9;
    void** rax10;
    void** rdx11;
    uint32_t edx12;
    void** rax13;

    rbx5 = rsi;
    rax6 = last_component(rdi, rsi, rdx, rcx);
    rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(rdi));
    rdi8 = rax7 + 9;
    if (reinterpret_cast<unsigned char>(rdi8) <= reinterpret_cast<unsigned char>(0x100) || (rax9 = fun_3ae0(rdi8, rsi, rdx), rbx5 = rax9, !!rax9)) {
        rax10 = fun_3bb0(rbx5, rdi, rax7);
        rdx11 = simple_pattern;
        *reinterpret_cast<void***>(rax10) = rdx11;
        edx12 = g178e0;
        *reinterpret_cast<void***>(rax10 + 8) = *reinterpret_cast<void***>(&edx12);
        rax13 = rbx5;
    } else {
        *reinterpret_cast<int32_t*>(&rax13) = 0;
        *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
    }
    return rax13;
}

void** fun_36f0();

int32_t i_ring_push(void*** rdi);

int32_t cwd_advance_fd(void** rdi, void** esi, signed char dl) {
    int32_t eax4;
    int32_t eax5;

    if (*reinterpret_cast<void***>(rdi + 44) == esi && !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) {
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
    }
    if (dl) {
        eax4 = i_ring_push(rdi + 96);
        if (eax4 < 0) {
            addr_d179_27:
            *reinterpret_cast<void***>(rdi + 44) = esi;
            return eax4;
        } else {
            eax5 = fun_3950();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_d179_27;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_d179_27;
        eax5 = fun_3950();
    }
    *reinterpret_cast<void***>(rdi + 44) = esi;
    return eax5;
}

void** fts_alloc(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    rax4 = fun_3ae0(reinterpret_cast<uint64_t>(rdx + 0x108) & 0xfffffffffffffff8, rsi, rdx);
    if (rax4) {
        fun_3ab0(rax4 + 0x100, rsi, rdx);
        rax5 = *reinterpret_cast<void***>(rdi + 32);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rdx) + 0x100) = 0;
        *reinterpret_cast<void***>(rax4 + 96) = rdx;
        *reinterpret_cast<void***>(rax4 + 80) = rdi;
        *reinterpret_cast<void***>(rax4 + 56) = rax5;
        *reinterpret_cast<void***>(rax4 + 64) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 24) = reinterpret_cast<void**>(0);
        *reinterpret_cast<unsigned char*>(rax4 + 0x6a) = reinterpret_cast<unsigned char>(0x30000);
        *reinterpret_cast<void***>(rax4 + 32) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 40) = reinterpret_cast<void**>(0);
    }
    return rax4;
}

void** free = reinterpret_cast<void**>(0);

void** hash_initialize(void** rdi);

void** hash_lookup(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t fun_3c30();

void** hash_insert(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** filesystem_type(void** rdi, void** esi, ...) {
    void** rsi2;
    void** rsp3;
    void** r12_4;
    int64_t rax5;
    void** rbp6;
    void** rbx7;
    void** r13d8;
    void** r8_9;
    void** rax10;
    void** v11;
    void** rax12;
    void** rax13;
    int32_t eax14;
    void** r12_15;
    void** v16;
    void** rax17;
    void** rax18;
    void** rax19;
    int64_t rdx20;
    int32_t eax21;
    void** v22;

    rsi2 = esi;
    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x98);
    r12_4 = *reinterpret_cast<void***>(rdi + 80);
    rax5 = g28;
    rbp6 = *reinterpret_cast<void***>(r12_4 + 80);
    if (!(*reinterpret_cast<unsigned char*>(r12_4 + 73) & 2)) 
        goto addr_d0c8_2;
    rbx7 = rdi;
    r13d8 = rsi2;
    if (!rbp6 && (r8_9 = free, rsi2 = reinterpret_cast<void**>(0), rdi = reinterpret_cast<void**>(13), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax10 = hash_initialize(13), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), *reinterpret_cast<void***>(r12_4 + 80) = rax10, rbp6 = rax10, !rax10) || (rsi2 = rsp3, rdi = rbp6, v11 = *reinterpret_cast<void***>(rbx7 + 0x70), rax12 = hash_lookup(rdi, rsi2, 0xcc80, 0xcc90, r8_9), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), rax12 == 0)) {
        if (reinterpret_cast<signed char>(r13d8) < reinterpret_cast<signed char>(0)) {
            addr_d0c8_2:
            *reinterpret_cast<int32_t*>(&rax13) = 0;
            *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
        } else {
            rsi2 = rsp3 + 16;
            rdi = r13d8;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax14 = fun_3c30();
            if (!eax14) {
                r12_15 = v16;
                if (!rbp6) {
                    addr_d136_7:
                    rax13 = r12_15;
                } else {
                    rdi = reinterpret_cast<void**>(16);
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    rax17 = fun_3ae0(16, rsi2, 0xcc80, 16, rsi2, 0xcc80);
                    if (!rax17) 
                        goto addr_d131_9;
                    rax18 = *reinterpret_cast<void***>(rbx7 + 0x70);
                    *reinterpret_cast<void***>(rax17 + 8) = r12_15;
                    rsi2 = rax17;
                    rdi = rbp6;
                    *reinterpret_cast<void***>(rax17) = rax18;
                    rax19 = hash_insert(rdi, rsi2, 0xcc80, 0xcc90, r8_9);
                    if (!rax19) 
                        goto addr_d140_11; else 
                        goto addr_d128_12;
                }
            } else {
                goto addr_d0c8_2;
            }
        }
    } else {
        rax13 = *reinterpret_cast<void***>(rax12 + 8);
    }
    rdx20 = rax5 - g28;
    if (!rdx20) {
        return rax13;
    }
    fun_3880();
    if (*reinterpret_cast<void***>(rdi + 44) != rsi2) 
        goto addr_d16b_19;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) 
        goto addr_3d35_21;
    addr_d16b_19:
    if (*reinterpret_cast<signed char*>(&rdx20)) {
        eax21 = i_ring_push(rdi + 96);
        if (eax21 < 0) {
            addr_d179_23:
            *reinterpret_cast<void***>(rdi + 44) = rsi2;
            goto v11;
        } else {
            fun_3950();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_d179_23;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_d179_23;
        fun_3950();
    }
    *reinterpret_cast<void***>(rdi + 44) = rsi2;
    goto v11;
    addr_3d35_21:
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    addr_d140_11:
    rdi = rax17;
    fun_36a0(rdi, rdi);
    goto addr_d131_9;
    addr_d128_12:
    if (rax17 != rax19) {
        fun_36f0();
        goto addr_3d35_21;
    } else {
        addr_d131_9:
        r12_15 = v22;
        goto addr_d136_7;
    }
}

void** fun_3b50(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_3760(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** fts_sort(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rbp6;
    void** rbx7;
    void** r13_8;
    void** rdi9;
    void** rsi10;
    void** r8_11;
    void** rax12;
    void** rdx13;
    void** r8_14;
    void** rax15;
    void** rdx16;
    void** rsi17;
    void** rcx18;
    void** rdx19;

    r12_5 = rdi;
    rbp6 = rdx;
    rbx7 = rsi;
    r13_8 = *reinterpret_cast<void***>(rdi + 64);
    rdi9 = *reinterpret_cast<void***>(rdi + 16);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 56)) < reinterpret_cast<unsigned char>(rdx)) {
        rsi10 = rdx + 40;
        r8_11 = rdi9;
        *reinterpret_cast<void***>(r12_5 + 56) = rsi10;
        if (reinterpret_cast<unsigned char>(rsi10) >> 61) {
            addr_cf42_3:
            fun_36a0(r8_11);
            *reinterpret_cast<void***>(r12_5 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_5 + 56) = reinterpret_cast<void**>(0);
            return rbx7;
        } else {
            rax12 = fun_3b50(rdi9, reinterpret_cast<unsigned char>(rsi10) << 3, rdx, rcx);
            rdi9 = rax12;
            if (!rax12) {
                r8_11 = *reinterpret_cast<void***>(r12_5 + 16);
                goto addr_cf42_3;
            } else {
                *reinterpret_cast<void***>(r12_5 + 16) = rax12;
            }
        }
    }
    rdx13 = rdi9;
    if (rbx7) {
        do {
            *reinterpret_cast<void***>(rdx13) = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdx13 = rdx13 + 8;
        } while (rbx7);
    }
    fun_3760(rdi9, rbp6, 8, r13_8, r8_11);
    r8_14 = *reinterpret_cast<void***>(r12_5 + 16);
    rax15 = *reinterpret_cast<void***>(r8_14);
    rdx16 = r8_14;
    rsi17 = rax15;
    rcx18 = rbp6 - 1;
    if (rcx18) {
        while (rdx16 = rdx16 + 8, *reinterpret_cast<void***>(rsi17 + 16) = *reinterpret_cast<void***>(rdx16 + 8), --rcx18, !!rcx18) {
            rsi17 = *reinterpret_cast<void***>(rdx16);
        }
        rdx19 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_14 + reinterpret_cast<unsigned char>(rbp6) * 8) - 8);
    } else {
        rdx19 = rax15;
    }
    *reinterpret_cast<void***>(rdx19 + 16) = reinterpret_cast<void**>(0);
    return rax15;
}

int32_t fun_3b40();

unsigned char i_ring_empty(void*** rdi, void** rsi, void** rdx);

void** i_ring_pop(void*** rdi, void** rsi, void** rdx);

uint32_t restore_initial_cwd(void** rdi, void** rsi) {
    void** eax3;
    uint32_t r12d4;
    int32_t eax5;
    void*** rbx6;
    unsigned char al7;
    void** eax8;

    eax3 = *reinterpret_cast<void***>(rdi + 72);
    r12d4 = reinterpret_cast<unsigned char>(eax3) & 4;
    if (r12d4) {
        r12d4 = 0;
    } else {
        if (!(*reinterpret_cast<unsigned char*>(&eax3 + 1) & 2)) {
            r12d4 = 0;
            eax5 = fun_3b40();
            *reinterpret_cast<unsigned char*>(&r12d4) = reinterpret_cast<uint1_t>(!!eax5);
        } else {
            rsi = reinterpret_cast<void**>(0xffffff9c);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            cwd_advance_fd(rdi, 0xffffff9c, 1);
        }
    }
    rbx6 = reinterpret_cast<void***>(rdi + 96);
    while (al7 = i_ring_empty(rbx6, rsi, 1), al7 == 0) {
        eax8 = i_ring_pop(rbx6, rsi, 1);
        if (reinterpret_cast<signed char>(eax8) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_3950();
    }
    return r12d4;
}

unsigned char fts_palloc(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rsi5;
    void** rdi6;
    void** tmp64_7;
    void** rax8;
    void** rax9;
    void** rdi10;

    rsi5 = rsi + 0x100;
    rdi6 = *reinterpret_cast<void***>(rdi + 32);
    tmp64_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi5) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48)));
    if (reinterpret_cast<unsigned char>(tmp64_7) < reinterpret_cast<unsigned char>(rsi5)) {
        fun_36a0(rdi6);
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
        rax8 = fun_3700();
        *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(36);
        return 0;
    } else {
        *reinterpret_cast<void***>(rdi + 48) = tmp64_7;
        rax9 = fun_3b50(rdi6, tmp64_7, rdx, rcx);
        if (!rax9) {
            rdi10 = *reinterpret_cast<void***>(rdi + 32);
            fun_36a0(rdi10);
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
            return 0;
        } else {
            *reinterpret_cast<void***>(rdi + 32) = rax9;
            return 1;
        }
    }
}

void cycle_check_init(void** rdi);

unsigned char setup_dir(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rax4 = fun_3ae0(32, rsi, rdx);
        *reinterpret_cast<void***>(rdi + 88) = rax4;
        if (!rax4) {
            return 0;
        } else {
            cycle_check_init(rax4);
            return 1;
        }
    } else {
        rax5 = hash_initialize(31);
        *reinterpret_cast<void***>(rdi + 88) = rax5;
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rax5));
    }
}

void** hash_remove(void** rdi, void** rsi);

void leave_dir(void** rdi, void** rsi, ...) {
    int64_t rax3;
    void** rdi4;
    void** v5;
    void** rax6;
    void** rdx7;
    void** rax8;
    int64_t rax9;
    void** eax10;
    void*** rbx11;
    unsigned char al12;
    void** eax13;

    rax3 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102) {
        rdi4 = *reinterpret_cast<void***>(rdi + 88);
        v5 = *reinterpret_cast<void***>(rsi + 0x70);
        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 40);
        rax6 = hash_remove(rdi4, rsi);
        rdi = rax6;
        if (!rax6) {
            addr_3d3a_3:
            fun_36f0();
        } else {
            fun_36a0(rdi, rdi);
            goto addr_d398_5;
        }
    } else {
        if (*reinterpret_cast<void***>(rsi + 8) && reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 88)) >= reinterpret_cast<signed char>(0)) {
            rdx7 = *reinterpret_cast<void***>(rdi + 88);
            if (!*reinterpret_cast<void***>(rdx7 + 16)) 
                goto addr_3d3a_3;
            if (*reinterpret_cast<void***>(rdx7) == *reinterpret_cast<void***>(rsi + 0x78)) {
                if (*reinterpret_cast<void***>(rdx7 + 8) == *reinterpret_cast<void***>(rsi + 0x70)) {
                    rax8 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x78);
                    *reinterpret_cast<void***>(rdx7 + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x70);
                    *reinterpret_cast<void***>(rdx7) = rax8;
                    goto addr_d398_5;
                }
            } else {
                goto addr_d398_5;
            }
        }
    }
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    addr_d398_5:
    rax9 = rax3 - g28;
    if (!rax9) {
        return;
    }
    fun_3880();
    eax10 = *reinterpret_cast<void***>(rdi + 72);
    if (!(reinterpret_cast<unsigned char>(eax10) & 4)) 
        goto addr_d416_36;
    addr_d433_38:
    rbx11 = reinterpret_cast<void***>(rdi + 96);
    while (al12 = i_ring_empty(rbx11, rsi, rdx7), al12 == 0) {
        eax13 = i_ring_pop(rbx11, rsi, rdx7);
        if (reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_3950();
    }
    goto v5;
    addr_d416_36:
    if (!(*reinterpret_cast<unsigned char*>(&eax10 + 1) & 2)) {
        fun_3b40();
        goto addr_d433_38;
    } else {
        *reinterpret_cast<int32_t*>(&rdx7) = 1;
        *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
        rsi = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        cwd_advance_fd(rdi, 0xffffff9c, 1);
        goto addr_d433_38;
    }
}

int32_t fts_safe_changedir(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r14d6;
    void** r12_7;
    void** rbx8;
    void* rsp9;
    int64_t rax10;
    int64_t v11;
    uint32_t eax12;
    void** ebp13;
    unsigned char v14;
    uint32_t eax15;
    void** r13d16;
    int64_t rax17;
    uint32_t eax18;
    int32_t eax19;
    int64_t rdi20;
    void** eax21;
    void** v22;
    void** v23;
    void** rax24;
    void** rax25;
    int64_t rdi26;
    void** eax27;
    void** eax28;
    void*** r13_29;
    unsigned char al30;
    void** eax31;

    r15_5 = rdi;
    r14d6 = rdx;
    r12_7 = rsi;
    rbx8 = rcx;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rax10 = g28;
    v11 = rax10;
    if (!rcx || ((eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)), eax12 != 46) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rcx + 1) == 46) || *reinterpret_cast<signed char*>(rcx + 2)))) {
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 4) {
            addr_d610_3:
            ebp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) & 0x200);
            if (!ebp13 || reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
            } else {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                fun_3950();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            }
        } else {
            if (reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                v14 = 0;
                eax15 = reinterpret_cast<unsigned char>(ebp13) & 0x200;
                goto addr_d72c_8;
            } else {
                v14 = 0;
                r13d16 = r14d6;
                if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) 
                    goto addr_d5d8_10; else 
                    goto addr_d4fa_11;
            }
        }
    } else {
        ebp13 = *reinterpret_cast<void***>(rdi + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 4) 
            goto addr_d610_3;
        if (reinterpret_cast<signed char>(rdx) >= reinterpret_cast<signed char>(0)) 
            goto addr_d760_14;
        if (reinterpret_cast<unsigned char>(ebp13) & 0x200) 
            goto addr_d690_16; else 
            goto addr_d595_17;
    }
    addr_d530_18:
    while (rax17 = v11 - g28, !!rax17) {
        eax12 = fun_3880();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        addr_d760_14:
        v14 = 1;
        r13d16 = rdx;
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) {
            goto addr_d5d8_10;
        }
        addr_d502_21:
        if (eax12 != 46 || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx8 + 1) == 46)) {
            addr_d50b_22:
            ebp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) & 0x200);
            if (ebp13) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                eax18 = static_cast<uint32_t>(v14) ^ 1;
                rdx = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&eax18)));
                cwd_advance_fd(r15_5, r13d16, *reinterpret_cast<signed char*>(&rdx));
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                continue;
            } else {
                eax19 = fun_3b40();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                *reinterpret_cast<int32_t*>(&r12_7) = eax19;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) {
                    continue;
                }
            }
        } else {
            if (!*reinterpret_cast<signed char*>(rbx8 + 2)) {
                addr_d5d8_10:
                *reinterpret_cast<void***>(&rdi20) = r13d16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                eax21 = fun_3d00(rdi20, reinterpret_cast<int64_t>(rsp9) + 16);
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                if (eax21) {
                    addr_d633_27:
                    *reinterpret_cast<int32_t*>(&r12_7) = -1;
                    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                    if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) 
                        continue;
                } else {
                    if (*reinterpret_cast<void***>(r12_7 + 0x70) != v22 || *reinterpret_cast<void***>(r12_7 + 0x78) != v23) {
                        rax24 = fun_3700();
                        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                        *reinterpret_cast<void***>(rax24) = reinterpret_cast<void**>(2);
                        goto addr_d633_27;
                    } else {
                        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
                        goto addr_d50b_22;
                    }
                }
            } else {
                goto addr_d50b_22;
            }
        }
        rax25 = fun_3700();
        ebp13 = *reinterpret_cast<void***>(rax25);
        rbx8 = rax25;
        fun_3950();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
        *reinterpret_cast<void***>(rbx8) = ebp13;
    }
    return *reinterpret_cast<int32_t*>(&r12_7);
    addr_d72c_8:
    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r15_5 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
    if (eax15) {
        addr_d6b9_34:
        eax27 = openat_safer(rdi26, rbx8);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        r13d16 = eax27;
    } else {
        goto addr_d5ab_36;
    }
    addr_d5ba_37:
    if (reinterpret_cast<signed char>(r13d16) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<int32_t*>(&r12_7) = -1;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        goto addr_d530_18;
    } else {
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) {
            goto addr_d5d8_10;
        }
    }
    addr_d4fa_11:
    if (!rbx8) 
        goto addr_d50b_22;
    eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8));
    goto addr_d502_21;
    addr_d5ab_36:
    eax28 = open_safer(rbx8, rbx8);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    r13d16 = eax28;
    goto addr_d5ba_37;
    addr_d690_16:
    r13_29 = reinterpret_cast<void***>(rdi + 96);
    al30 = i_ring_empty(r13_29, rsi, rdx);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    v14 = al30;
    if (!al30) {
        eax31 = i_ring_pop(r13_29, rsi, rdx);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        r13d16 = eax31;
        if (reinterpret_cast<signed char>(eax31) < reinterpret_cast<signed char>(0)) {
            v14 = 1;
            eax15 = reinterpret_cast<unsigned char>(ebp13) & 0x200;
            goto addr_d72c_8;
        } else {
            v14 = 1;
            r14d6 = eax31;
            if (!(*reinterpret_cast<unsigned char*>(&ebp13) & 2)) 
                goto addr_d50b_22;
            goto addr_d5d8_10;
        }
    } else {
        *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r15_5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
        goto addr_d6b9_34;
    }
    addr_d595_17:
    v14 = 1;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
    goto addr_d5ab_36;
}

void** opendirat(int64_t rdi, void** rsi, ...);

void** fun_3a30(void** rdi);

void** fts_stat(void** rdi, void** rsi, signed char dl, ...);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** rpl_fcntl();

struct s8 {
    signed char f0;
    void** f1;
};

void fun_3bc0(void** rdi, void** rsi, void** rdx, ...);

void** fts_build(void** rdi, void** rsi) {
    void** r14_3;
    void** rbp4;
    void** v5;
    int64_t rax6;
    int64_t v7;
    void** rax8;
    void** v9;
    void** eax10;
    int64_t rdi11;
    void** rax12;
    void** r13_13;
    void** eax14;
    void** v15;
    void** rdi16;
    void** rax17;
    void** rax18;
    int64_t rax19;
    void** r8_20;
    uint64_t rax21;
    void* rax22;
    void** v23;
    void** rax24;
    void** edi25;
    void** v26;
    int32_t r12d27;
    int32_t ebx28;
    void** rax29;
    void** eax30;
    void** rdx31;
    int32_t eax32;
    void** rax33;
    void** rdi34;
    void** edx35;
    unsigned char v36;
    void** rcx37;
    void** v38;
    void** v39;
    void** v40;
    struct s8* rax41;
    void** r15_42;
    void** rax43;
    int1_t zf44;
    void** v45;
    void** v46;
    signed char v47;
    void** r12_48;
    void** rax49;
    void** rdi50;
    void** v51;
    void** rbx52;
    unsigned char v53;
    void** v54;
    void** v55;
    void** v56;
    struct s0* rax57;
    void** rax58;
    void** r14_59;
    void** rax60;
    void** rsi61;
    void** rax62;
    void** r15_63;
    uint32_t eax64;
    void** tmp64_65;
    void** rsi66;
    void** rax67;
    void** edx68;
    void** rdx69;
    void** eax70;
    int64_t rax71;
    int64_t rdx72;
    void** eax73;
    void** rax74;
    void** rax75;
    uint32_t eax76;
    int32_t eax77;
    void** rax78;
    void** rax79;
    uint32_t eax80;
    void** rbp81;
    void** rdi82;
    void** rbp83;
    void** rdi84;
    uint64_t rax85;
    uint32_t eax86;
    void** rdi87;
    void** rax88;
    void** rax89;
    void** rdx90;
    void** r13_91;
    void** r14_92;
    void** rbp93;
    void** ebx94;
    void** r12_95;
    void** rdi96;
    void** rdi97;
    void** r13_98;
    void** rbp99;
    void** r14_100;
    void** r12_101;
    void** rdi102;
    void** rdi103;
    void** v104;
    void** v105;

    r14_3 = rdi;
    rbp4 = *reinterpret_cast<void***>(rdi);
    v5 = rsi;
    rax6 = g28;
    v7 = rax6;
    rax8 = *reinterpret_cast<void***>(rbp4 + 24);
    v9 = rax8;
    if (!rax8) {
        eax10 = *reinterpret_cast<void***>(rdi + 72);
        if (reinterpret_cast<unsigned char>(eax10) & 16 && *reinterpret_cast<unsigned char*>(&eax10) & 1) {
        }
        rsi = *reinterpret_cast<void***>(rbp4 + 48);
        *reinterpret_cast<void***>(&rdi11) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
        if ((reinterpret_cast<unsigned char>(eax10) & 0x204) == 0x200) {
            *reinterpret_cast<void***>(&rdi11) = *reinterpret_cast<void***>(r14_3 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
        }
        rax12 = opendirat(rdi11, rsi, rdi11, rsi);
        *reinterpret_cast<void***>(rbp4 + 24) = rax12;
        r13_13 = rax12;
        if (rax12) 
            goto addr_dc7a_7;
    } else {
        eax14 = fun_3a30(rax8);
        v15 = eax14;
        if (reinterpret_cast<signed char>(eax14) < reinterpret_cast<signed char>(0)) {
            rdi16 = *reinterpret_cast<void***>(rbp4 + 24);
            fun_3980(rdi16, rsi);
            *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
            if (reinterpret_cast<int1_t>(v5 == 3)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
                rax17 = fun_3700();
                *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax17);
                goto addr_dfca_11;
            }
        }
        if (!*reinterpret_cast<void***>(r14_3 + 64)) 
            goto addr_e070_13; else 
            goto addr_d7e9_14;
    }
    if (!reinterpret_cast<int1_t>(v5 == 3)) {
        addr_dfca_11:
        *reinterpret_cast<int32_t*>(&r13_13) = 0;
        *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
    } else {
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
        rax18 = fun_3700();
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax18);
    }
    addr_dde1_17:
    rax19 = v7 - g28;
    if (rax19) {
        fun_3880();
    } else {
        return r13_13;
    }
    addr_dc7a_7:
    if (*reinterpret_cast<uint16_t*>(rbp4 + 0x68) == 11) {
        rsi = rbp4;
        rax12 = fts_stat(r14_3, rsi, 0, r14_3, rsi, 0);
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&rax12);
        goto addr_dc90_22;
    }
    if (!(*reinterpret_cast<unsigned char*>(r14_3 + 73) & 1) || (leave_dir(r14_3, rbp4, r14_3, rbp4), fts_stat(r14_3, rbp4, 0, r14_3, rbp4, 0), rsi = rbp4, rax12 = enter_dir(r14_3, rsi, 0, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78 + 100, r8_20), !!*reinterpret_cast<signed char*>(&rax12))) {
        addr_dc90_22:
        rax21 = reinterpret_cast<unsigned char>(rax12) - (reinterpret_cast<unsigned char>(rax12) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax12) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 64)) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax22) = *reinterpret_cast<uint32_t*>(&rax21) & 0x186a1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
        v23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax22) - 1);
        if (v5 == 2) 
            goto addr_e020_24;
    } else {
        rax24 = fun_3700();
        *reinterpret_cast<int32_t*>(&r13_13) = 0;
        *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
        *reinterpret_cast<void***>(rax24) = reinterpret_cast<void**>(12);
        goto addr_dde1_17;
    }
    edi25 = v26;
    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 56) != 24 || *reinterpret_cast<int64_t*>(rbp4 + 0x80) != 2) {
        addr_dcc5_27:
        r12d27 = 1;
        *reinterpret_cast<unsigned char*>(&ebx28) = reinterpret_cast<uint1_t>(v5 == 3);
    } else {
        rsi = edi25;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rax29 = filesystem_type(rbp4, rsi, rbp4, rsi);
        if (rax29 == 0x9fa0) 
            goto addr_e1da_29;
        if (reinterpret_cast<signed char>(rax29) > reinterpret_cast<signed char>(0x9fa0)) 
            goto addr_e1c4_31; else 
            goto addr_de4c_32;
    }
    addr_dcd3_33:
    if (*reinterpret_cast<unsigned char*>(r14_3 + 73) & 2) {
        rsi = reinterpret_cast<void**>(0x406);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax30 = rpl_fcntl();
        v15 = eax30;
        edi25 = eax30;
    }
    if (reinterpret_cast<signed char>(edi25) < reinterpret_cast<signed char>(0) || (rdx31 = edi25, *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0, rsi = rbp4, eax32 = fts_safe_changedir(r14_3, rsi, rdx31, 0), !!eax32)) {
        if (*reinterpret_cast<unsigned char*>(&ebx28) && *reinterpret_cast<signed char*>(&r12d27)) {
            rax33 = fun_3700();
            *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax33);
        }
        *reinterpret_cast<unsigned char*>(rbp4 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(rbp4 + 0x6a) | 1);
        rdi34 = *reinterpret_cast<void***>(rbp4 + 24);
        fun_3980(rdi34, rsi, rdi34, rsi);
        edx35 = *reinterpret_cast<void***>(r14_3 + 72);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        if (*reinterpret_cast<unsigned char*>(&edx35 + 1) & 2 && reinterpret_cast<signed char>(v15) >= reinterpret_cast<signed char>(0)) {
            fun_3950();
            edx35 = *reinterpret_cast<void***>(r14_3 + 72);
        }
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        v36 = 0;
    } else {
        goto addr_d7f2_42;
    }
    addr_d7fb_43:
    rcx37 = *reinterpret_cast<void***>(rbp4 + 72);
    v38 = rcx37;
    v39 = rcx37 + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 56)) + reinterpret_cast<unsigned char>(rcx37) + 0xffffffffffffffff) != 47) {
        v39 = rcx37;
        v38 = rcx37 + 1;
    }
    v40 = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(edx35) & 4) {
        rax41 = reinterpret_cast<struct s8*>(reinterpret_cast<unsigned char>(v39) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 32)));
        rcx37 = reinterpret_cast<void**>(&rax41->f1);
        rax41->f0 = 47;
        v40 = rcx37;
    }
    r15_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 48)) - reinterpret_cast<unsigned char>(v38));
    rax43 = *reinterpret_cast<void***>(rbp4 + 88) + 1;
    zf44 = *reinterpret_cast<void***>(rbp4 + 24) == 0;
    v45 = *reinterpret_cast<void***>(rbp4 + 24);
    v46 = rax43;
    if (zf44) {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4)) {
            *reinterpret_cast<int32_t*>(&r13_13) = 0;
            *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
            if (!(v36 & static_cast<unsigned char>(reinterpret_cast<uint1_t>(v9 == 0)))) 
                goto addr_df95_50;
            v47 = 0;
            *reinterpret_cast<int32_t*>(&r12_48) = 0;
            *reinterpret_cast<int32_t*>(&r12_48 + 4) = 0;
        } else {
            v47 = 0;
            *reinterpret_cast<int32_t*>(&r13_13) = 0;
            *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_48) = 0;
            *reinterpret_cast<int32_t*>(&r12_48 + 4) = 0;
            goto addr_deeb_53;
        }
    } else {
        rax49 = fun_3700();
        rdi50 = v45;
        *reinterpret_cast<int32_t*>(&r12_48) = 0;
        *reinterpret_cast<int32_t*>(&r12_48 + 4) = 0;
        v51 = rax49;
        rbx52 = r14_3;
        v47 = 0;
        v53 = 0;
        v54 = reinterpret_cast<void**>(0);
        v55 = rbp4;
        v56 = reinterpret_cast<void**>(0);
        while (*reinterpret_cast<void***>(v51) = reinterpret_cast<void**>(0), rax57 = fun_3ad0(rdi50, rsi), !!rax57) {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx52 + 72)) & 32) 
                goto addr_d902_57;
            if (*reinterpret_cast<void***>(&rax57->f13) != 46) 
                goto addr_d902_57;
            if (!rax57->f14) {
                addr_d8c4_60:
                rax58 = v55;
                rdi50 = *reinterpret_cast<void***>(rax58 + 24);
                if (!rdi50) 
                    goto addr_da18_61; else 
                    continue;
            } else {
                if (rax57->f14 != 46) {
                    addr_d902_57:
                    r14_59 = reinterpret_cast<void**>(&rax57->f13);
                    rax60 = fun_3860(r14_59, r14_59);
                    rsi61 = r14_59;
                    rax62 = fts_alloc(rbx52, rsi61, rax60);
                    if (!rax62) 
                        goto addr_dd68_63;
                } else {
                    goto addr_d8c4_60;
                }
            }
            if (reinterpret_cast<unsigned char>(rax60) >= reinterpret_cast<unsigned char>(r15_42)) {
                r15_63 = *reinterpret_cast<void***>(rbx52 + 32);
                rsi61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v39) + reinterpret_cast<unsigned char>(rax60) + 2);
                *reinterpret_cast<unsigned char*>(&eax64) = fts_palloc(rbx52, rsi61, rax60, rcx37);
                if (!*reinterpret_cast<unsigned char*>(&eax64)) 
                    goto addr_dd68_63;
                rsi61 = *reinterpret_cast<void***>(rbx52 + 32);
                if (rsi61 != r15_63) 
                    goto addr_daeb_68;
            } else {
                addr_d934_69:
                tmp64_65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax60) + reinterpret_cast<unsigned char>(v38));
                if (reinterpret_cast<unsigned char>(tmp64_65) < reinterpret_cast<unsigned char>(rax60)) 
                    goto addr_e1e3_70; else 
                    goto addr_d942_71;
            }
            eax64 = v53;
            addr_daff_73:
            r15_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx52 + 48)) - reinterpret_cast<unsigned char>(v38));
            v53 = *reinterpret_cast<unsigned char*>(&eax64);
            goto addr_d934_69;
            addr_daeb_68:
            rsi61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi61) + reinterpret_cast<unsigned char>(v38));
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx52 + 72)) & 4)) {
                rsi61 = v40;
            }
            v40 = rsi61;
            goto addr_daff_73;
            addr_d942_71:
            rsi66 = rax62 + 0x100;
            *reinterpret_cast<void***>(rax62 + 88) = v46;
            rax67 = *reinterpret_cast<void***>(rbx52);
            *reinterpret_cast<void***>(rax62 + 72) = tmp64_65;
            edx68 = *reinterpret_cast<void***>(rbx52 + 72);
            *reinterpret_cast<void***>(rax62 + 8) = rax67;
            *reinterpret_cast<void***>(rax62 + 0x78) = rax57->f0;
            if (*reinterpret_cast<unsigned char*>(&edx68) & 4) {
                *reinterpret_cast<void***>(rax62 + 48) = *reinterpret_cast<void***>(rax62 + 56);
                rdx69 = *reinterpret_cast<void***>(rax62 + 96) + 1;
                fun_3bc0(v40, rsi66, rdx69);
                edx68 = *reinterpret_cast<void***>(rbx52 + 72);
            } else {
                *reinterpret_cast<void***>(rax62 + 48) = rsi66;
            }
            if (!*reinterpret_cast<void***>(rbx52 + 64) || *reinterpret_cast<unsigned char*>(&edx68 + 1) & 4) {
                eax70 = reinterpret_cast<void**>(static_cast<uint32_t>(rax57->f12));
                rsi = eax70;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                *reinterpret_cast<void***>(&rax71) = eax70 - 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax71) + 4) = 0;
                if (!(*reinterpret_cast<unsigned char*>(&edx68) & 8) || !(*reinterpret_cast<unsigned char*>(&rsi) & 0xfb)) {
                    rsi = reinterpret_cast<void**>(11);
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax62 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax71)) <= reinterpret_cast<unsigned char>(11)) {
                        addr_db28_81:
                        *reinterpret_cast<int32_t*>(&rdx72) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx72) + 4) = 0;
                    } else {
                        eax73 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx72) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx72) + 4) = 0;
                        goto addr_d9b8_83;
                    }
                } else {
                    if (!(reinterpret_cast<unsigned char>(edx68) & 16) && *reinterpret_cast<unsigned char*>(&rsi) == 10) {
                        *reinterpret_cast<uint16_t*>(rax62 + 0x68) = 11;
                        goto addr_db28_81;
                    }
                    *reinterpret_cast<int32_t*>(&rcx37) = 11;
                    *reinterpret_cast<int32_t*>(&rcx37 + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax62 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax71)) <= reinterpret_cast<unsigned char>(11)) 
                        goto addr_dd40_87; else 
                        goto addr_dbd2_88;
                }
            } else {
                rsi = rax62;
                rax74 = fts_stat(rbx52, rsi, 0);
                *reinterpret_cast<uint16_t*>(rax62 + 0x68) = *reinterpret_cast<uint16_t*>(&rax74);
                goto addr_d9c6_90;
            }
            addr_db2d_91:
            rcx37 = reinterpret_cast<void**>(0x17a20);
            eax73 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x17a20) + reinterpret_cast<uint64_t>(rax71 * 4));
            addr_d9b8_83:
            *reinterpret_cast<void***>(rax62 + 0x88) = eax73;
            *reinterpret_cast<int64_t*>(rax62 + 0xa0) = rdx72;
            addr_d9c6_90:
            *reinterpret_cast<void***>(rax62 + 16) = reinterpret_cast<void**>(0);
            if (!v56) {
                v56 = rax62;
            } else {
                *reinterpret_cast<void***>(v54 + 16) = rax62;
            }
            if (!reinterpret_cast<int1_t>(r12_48 == 0x2710)) {
                ++r12_48;
                if (reinterpret_cast<unsigned char>(r12_48) >= reinterpret_cast<unsigned char>(v23)) 
                    goto addr_dfd8_96;
                v54 = rax62;
                goto addr_d8c4_60;
            } else {
                if (!*reinterpret_cast<void***>(rbx52 + 64)) {
                    rsi = v15;
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    rax75 = filesystem_type(v55, rsi);
                    if (rax75 == 0x1021994 || ((*reinterpret_cast<int32_t*>(&rcx37) = 0xff534d42, *reinterpret_cast<int32_t*>(&rcx37 + 4) = 0, rax75 == 0xff534d42) || rax75 == 0x6969)) {
                        v54 = rax62;
                        *reinterpret_cast<int32_t*>(&r12_48) = 0x2711;
                        *reinterpret_cast<int32_t*>(&r12_48 + 4) = 0;
                        v47 = 0;
                        goto addr_d8c4_60;
                    } else {
                        v47 = 1;
                        goto addr_d9fb_102;
                    }
                } else {
                    addr_d9fb_102:
                    rax58 = v55;
                    v54 = rax62;
                    *reinterpret_cast<int32_t*>(&r12_48) = 0x2711;
                    *reinterpret_cast<int32_t*>(&r12_48 + 4) = 0;
                    rdi50 = *reinterpret_cast<void***>(rax58 + 24);
                    if (rdi50) 
                        continue; else 
                        goto addr_da18_61;
                }
            }
            addr_dd40_87:
            *reinterpret_cast<int32_t*>(&rdx72) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx72) + 4) = 0;
            goto addr_db2d_91;
            addr_dbd2_88:
            eax73 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx72) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx72) + 4) = 0;
            goto addr_d9b8_83;
        }
        goto addr_de80_103;
    }
    addr_df70_104:
    if (!*reinterpret_cast<void***>(rbp4 + 88)) {
        eax76 = restore_initial_cwd(r14_3, rsi);
        if (eax76) 
            goto addr_e0bd_106;
        goto addr_df90_108;
    }
    rsi = *reinterpret_cast<void***>(rbp4 + 8);
    rcx37 = reinterpret_cast<void**>("..");
    eax77 = fts_safe_changedir(r14_3, rsi, 0xffffffff, "..");
    if (!eax77) {
        addr_df90_108:
        if (r12_48) {
            addr_df1c_110:
            if (v47) {
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0xcca0);
                rax78 = fts_sort(r14_3, r13_13, r12_48, rcx37);
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0);
                r13_13 = rax78;
                goto addr_dde1_17;
            } else {
                if (*reinterpret_cast<void***>(r14_3 + 64) && r12_48 != 1) {
                    rax79 = fts_sort(r14_3, r13_13, r12_48, rcx37);
                    r13_13 = rax79;
                    goto addr_dde1_17;
                }
            }
        } else {
            addr_df95_50:
            if (v5 == 3 && ((eax80 = *reinterpret_cast<uint16_t*>(rbp4 + 0x68), *reinterpret_cast<int16_t*>(&eax80) != 7) && *reinterpret_cast<int16_t*>(&eax80) != 4)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 6;
            }
        }
    } else {
        addr_e0bd_106:
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 7;
        *reinterpret_cast<void***>(r14_3 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) | 0x2000);
        if (r13_13) {
            do {
                rbp81 = r13_13;
                r13_13 = *reinterpret_cast<void***>(r13_13 + 16);
                rdi82 = *reinterpret_cast<void***>(rbp81 + 24);
                if (rdi82) {
                    fun_3980(rdi82, rsi, rdi82, rsi);
                }
                fun_36a0(rbp81, rbp81);
            } while (r13_13);
            goto addr_dfca_11;
        }
    }
    if (r13_13) {
        do {
            rbp83 = r13_13;
            r13_13 = *reinterpret_cast<void***>(r13_13 + 16);
            rdi84 = *reinterpret_cast<void***>(rbp83 + 24);
            if (rdi84) {
                fun_3980(rdi84, rsi, rdi84, rsi);
            }
            fun_36a0(rbp83, rbp83);
        } while (r13_13);
        goto addr_dfca_11;
    }
    addr_de80_103:
    rbp4 = v55;
    r14_3 = rbx52;
    r13_13 = v56;
    if (*reinterpret_cast<void***>(v51)) {
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(v51);
        rax85 = reinterpret_cast<unsigned char>(v9) | reinterpret_cast<unsigned char>(r12_48);
        eax86 = (*reinterpret_cast<uint32_t*>(&rax85) - (*reinterpret_cast<uint32_t*>(&rax85) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax85) < *reinterpret_cast<uint32_t*>(&rax85) + reinterpret_cast<uint1_t>(rax85 < 1))) & 0xfffffffd) + 7;
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&eax86);
    }
    rdi87 = *reinterpret_cast<void***>(rbp4 + 24);
    if (rdi87) {
        fun_3980(rdi87, rsi);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
    }
    addr_dec9_128:
    if (v53) {
        addr_da2e_129:
        rax88 = *reinterpret_cast<void***>(r14_3 + 8);
        rcx37 = *reinterpret_cast<void***>(r14_3 + 32);
        if (rax88) {
            do {
                rsi = rax88 + 0x100;
                if (*reinterpret_cast<void***>(rax88 + 48) != rsi) {
                    *reinterpret_cast<void***>(rax88 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax88 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax88 + 56))) + reinterpret_cast<unsigned char>(rcx37));
                }
                *reinterpret_cast<void***>(rax88 + 56) = rcx37;
                rax88 = *reinterpret_cast<void***>(rax88 + 16);
            } while (rax88);
        }
    } else {
        addr_ded4_134:
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4) {
            if (*reinterpret_cast<void***>(r14_3 + 48) == v38 || !r12_48) {
                addr_deeb_53:
                --v40;
                goto addr_def1_136;
            } else {
                addr_def1_136:
                *reinterpret_cast<void***>(v40) = reinterpret_cast<void**>(0);
                goto addr_def9_137;
            }
        }
    }
    rax89 = r13_13;
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r13_13 + 88)) >= reinterpret_cast<signed char>(0)) {
        do {
            rsi = rax89 + 0x100;
            if (*reinterpret_cast<void***>(rax89 + 48) != rsi) {
                *reinterpret_cast<void***>(rax89 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax89 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax89 + 56))) + reinterpret_cast<unsigned char>(rcx37));
            }
            rdx90 = *reinterpret_cast<void***>(rax89 + 16);
            *reinterpret_cast<void***>(rax89 + 56) = rcx37;
            if (rdx90) {
                rax89 = rdx90;
            } else {
                rax89 = *reinterpret_cast<void***>(rax89 + 8);
            }
        } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax89 + 88)) >= reinterpret_cast<signed char>(0));
        goto addr_ded4_134;
    } else {
        goto addr_ded4_134;
    }
    addr_def9_137:
    if (v9) 
        goto addr_df90_108;
    if (!v36) 
        goto addr_df90_108;
    if (v5 == 1) 
        goto addr_df70_104;
    if (!r12_48) 
        goto addr_df70_104; else 
        goto addr_df1c_110;
    addr_dd68_63:
    r13_91 = v56;
    r14_92 = rbx52;
    rbp93 = v55;
    ebx94 = *reinterpret_cast<void***>(v51);
    fun_36a0(rax62, rax62);
    if (r13_91) {
        do {
            r12_95 = r13_91;
            r13_91 = *reinterpret_cast<void***>(r13_91 + 16);
            rdi96 = *reinterpret_cast<void***>(r12_95 + 24);
            if (rdi96) {
                fun_3980(rdi96, rsi61, rdi96, rsi61);
            }
            fun_36a0(r12_95, r12_95);
        } while (r13_91);
    }
    rdi97 = *reinterpret_cast<void***>(rbp93 + 24);
    *reinterpret_cast<int32_t*>(&r13_13) = 0;
    *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
    fun_3980(rdi97, rsi61, rdi97, rsi61);
    *reinterpret_cast<void***>(rbp93 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp93 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_92 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_92 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v51) = ebx94;
    goto addr_dde1_17;
    addr_e1e3_70:
    r13_98 = v56;
    rbp99 = v55;
    r14_100 = rbx52;
    fun_36a0(rax62, rax62);
    if (r13_98) {
        do {
            r12_101 = r13_98;
            r13_98 = *reinterpret_cast<void***>(r13_98 + 16);
            rdi102 = *reinterpret_cast<void***>(r12_101 + 24);
            if (rdi102) {
                fun_3980(rdi102, rsi61);
            }
            fun_36a0(r12_101, r12_101);
        } while (r13_98);
    }
    rdi103 = *reinterpret_cast<void***>(rbp99 + 24);
    *reinterpret_cast<int32_t*>(&r13_13) = 0;
    *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
    fun_3980(rdi103, rsi61);
    *reinterpret_cast<void***>(rbp99 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp99 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_100 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_100 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v51) = reinterpret_cast<void**>(36);
    goto addr_dde1_17;
    addr_dfd8_96:
    rbp4 = v55;
    r13_13 = v56;
    r14_3 = rbx52;
    goto addr_dec9_128;
    addr_da18_61:
    r13_13 = v56;
    rbp4 = rax58;
    r14_3 = rbx52;
    if (!v53) 
        goto addr_ded4_134; else 
        goto addr_da2e_129;
    addr_d7f2_42:
    v36 = 1;
    edx35 = *reinterpret_cast<void***>(r14_3 + 72);
    goto addr_d7fb_43;
    addr_e1c4_31:
    if (rax29 == 0x5346414f || reinterpret_cast<int1_t>(rax29 == 0xff534d42)) {
        addr_e1da_29:
        edi25 = v104;
        goto addr_dcc5_27;
    } else {
        addr_de61_158:
        if (!reinterpret_cast<int1_t>(v5 == 3)) {
            addr_e020_24:
            v36 = 0;
            edx35 = *reinterpret_cast<void***>(r14_3 + 72);
            goto addr_d7fb_43;
        } else {
            edi25 = v105;
            r12d27 = 0;
            ebx28 = 1;
            goto addr_dcd3_33;
        }
    }
    addr_de4c_32:
    if (!rax29) 
        goto addr_e1da_29;
    if (rax29 == 0x6969) 
        goto addr_e1da_29; else 
        goto addr_de61_158;
    addr_e070_13:
    v23 = reinterpret_cast<void**>(0x186a0);
    edx35 = *reinterpret_cast<void***>(r14_3 + 72);
    v36 = 1;
    goto addr_d7fb_43;
    addr_d7e9_14:
    v23 = reinterpret_cast<void**>(0xffffffffffffffff);
    goto addr_d7f2_42;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0x17ac0);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0x17ac0);
    if (*reinterpret_cast<void***>(rdi + 40) != 0x17ac0) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x87f8]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0x17ac0);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x865f]");
        if (1) 
            goto addr_f56c_6;
        __asm__("comiss xmm1, [rip+0x8656]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x8648]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_f54c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_f542_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_f54c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_f542_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_f542_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_f54c_13:
        return 0;
    } else {
        addr_f56c_6:
        return r8_3;
    }
}

struct s10 {
    signed char[16] pad16;
    unsigned char f10;
};

struct s9 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    signed char[8] pad40;
    struct s10* f28;
    int64_t f30;
    signed char[16] pad72;
    void** f48;
};

struct s11 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s12 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int32_t transfer_entries(struct s9* rdi, struct s9* rsi, int32_t edx) {
    void** rdx3;
    struct s9* r14_4;
    int32_t r12d5;
    struct s9* rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rax12;
    struct s11* rax13;
    void** rax14;
    void** rsi15;
    void** rax16;
    struct s12* r13_17;
    void** rax18;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = rsi->f0;
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rsi->f8)) {
        do {
            addr_f5d6_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_f5c8_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(rbp6->f8) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_f5d6_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = r14_4->f10;
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rax12 = reinterpret_cast<void**>(r14_4->f30(r15_11, rsi10)), rsi10 = r14_4->f10, reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax13 = reinterpret_cast<struct s11*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax13->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax13->f8;
                            rax13->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_f64e_9;
                        } else {
                            rax13->f0 = r15_11;
                            rax14 = r14_4->f48;
                            r14_4->f18 = r14_4->f18 + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14;
                            r14_4->f48 = r13_9;
                            if (!rdx3) 
                                goto addr_f64e_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_3d49_12;
                    addr_f64e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_f5c8_3;
            }
            rsi15 = r14_4->f10;
            rax16 = reinterpret_cast<void**>(r14_4->f30(r15_8, rsi15));
            if (reinterpret_cast<unsigned char>(rax16) >= reinterpret_cast<unsigned char>(r14_4->f10)) 
                goto addr_3d49_12;
            r13_17 = reinterpret_cast<struct s12*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax16) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
            if (r13_17->f0) 
                goto addr_f688_16;
            r13_17->f0 = r15_8;
            r14_4->f18 = r14_4->f18 + 1;
            continue;
            addr_f688_16:
            rax18 = r14_4->f48;
            if (!rax18) {
                rax18 = fun_3ae0(16, rsi15, rdx3);
                if (!rax18) 
                    goto addr_f6fa_19;
            } else {
                r14_4->f48 = *reinterpret_cast<void***>(rax18 + 8);
            }
            rdx3 = r13_17->f8;
            *reinterpret_cast<void***>(rax18) = r15_8;
            *reinterpret_cast<void***>(rax18 + 8) = rdx3;
            r13_17->f8 = rax18;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            rbp6->f18 = rbp6->f18 - 1;
        } while (reinterpret_cast<unsigned char>(rbp6->f8) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_3d49_12:
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    fun_36f0();
    addr_f6fa_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_f3ff_23:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_f3a4_26;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_f410_30;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_f410_30;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_f3ff_23;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_f3a4_26;
            }
        }
    }
    addr_f401_34:
    return rax11;
    addr_f3a4_26:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_f401_34;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_f410_30:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_3850();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_3850();
    if (r8d > 10) {
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x17ba0 + rax11 * 4) + 0x17ba0;
    }
}

struct s13 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_3900();

struct s14 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s13* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s14* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    int64_t rax29;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0x11d8f;
    rax8 = fun_3700();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
        fun_36f0();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x1e090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xc161]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x11e1b;
            fun_3900();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s14*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x1e580) {
                fun_36a0(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x11eaa);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = rax6 - g28;
        if (rax29) {
            fun_3880();
        } else {
            return r14_19;
        }
    }
}

uint32_t copy();

int64_t get_root_dev_ino(int64_t rdi, void* rsi);

int64_t rm(void* rdi, void* rsi);

void fun_38f0(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, ...);

uint32_t do_move(void** rdi, void** rsi, void** rdx, void** rcx, void* r8, int64_t r9) {
    void* rsp7;
    int64_t rax8;
    int64_t rax9;
    signed char v10;
    unsigned char v11;
    int64_t rdx12;
    int64_t rax13;
    void* rsp14;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 88);
    rax8 = g28;
    *reinterpret_cast<uint32_t*>(&rax9) = copy();
    if (!*reinterpret_cast<unsigned char*>(&rax9)) 
        goto addr_4556_2;
    if (!v10) {
        *reinterpret_cast<uint32_t*>(&rax9) = v11;
        if (*reinterpret_cast<unsigned char*>(&rax9)) {
            addr_4556_2:
            rdx12 = rax8 - g28;
            if (rdx12) {
                fun_3880();
                goto addr_4604_6;
            } else {
                return *reinterpret_cast<uint32_t*>(&rax9);
            }
        } else {
            rax13 = get_root_dev_ino(0x1e110, reinterpret_cast<int64_t>(rsp7) + 15);
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 - 8 - 8 + 8 + 8 + 8 - 8 + 8);
            if (!rax13) {
                addr_4623_9:
                quotearg_style(4, "/", 4, "/");
                fun_3840();
                fun_3700();
                fun_3bd0();
            } else {
                rax9 = rm(reinterpret_cast<int64_t>(rsp14) + 48, reinterpret_cast<int64_t>(rsp14) + 16);
                if (static_cast<uint32_t>(rax9 - 2) > 2) {
                    addr_4604_6:
                    fun_38f0("VALID_STATUS (status)", "src/mv.c", 0xe1, "do_move", "VALID_STATUS (status)", "src/mv.c", 0xe1, "do_move");
                    goto addr_4623_9;
                } else {
                    *reinterpret_cast<unsigned char*>(&rax9) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax9) != 4);
                    goto addr_4556_2;
                }
            }
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax9) = 0;
        goto addr_4556_2;
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x1e0a8;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

/* zeros.3 */
void** zeros_3 = reinterpret_cast<void**>(0);

/* nz.2 */
void** nz_2 = reinterpret_cast<void**>(0);

void** fun_39f0(void** rdi, int64_t rsi);

signed char write_zeros(void** edi, void** rsi) {
    int1_t zf3;
    void** r12d4;
    void** rbp5;
    void** rdi6;
    void** rax7;
    void** rbx8;
    void** rsi9;
    int64_t rdi10;
    void** rax11;

    zf3 = zeros_3 == 0;
    r12d4 = edi;
    rbp5 = rsi;
    if (zf3 && (rdi6 = nz_2, rax7 = fun_39f0(rdi6, 1), zeros_3 = rax7, !rax7)) {
        nz_2 = reinterpret_cast<void**>(0x400);
        zeros_3 = reinterpret_cast<void**>(0x1e120);
    }
    if (rbp5) {
        do {
            rbx8 = nz_2;
            rsi9 = zeros_3;
            *reinterpret_cast<void***>(&rdi10) = r12d4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            if (reinterpret_cast<unsigned char>(rbp5) <= reinterpret_cast<unsigned char>(rbx8)) {
                rbx8 = rbp5;
            }
            rax11 = full_write(rdi10, rsi9, rbx8);
            if (rax11 != rbx8) 
                break;
            rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) - reinterpret_cast<unsigned char>(rax11));
        } while (rbp5);
        goto addr_5b78_8;
    } else {
        goto addr_5b78_8;
    }
    return 0;
    addr_5b78_8:
    return 1;
}

void** fts_stat(void** rdi, void** rsi, signed char dl, ...) {
    void** rbp4;
    void** eax5;
    void** rsi6;
    void** rdi7;
    int32_t eax8;
    void** rax9;
    void** eax10;
    void** rdi11;
    int32_t eax12;
    uint32_t eax13;
    void** rax14;
    int64_t rax15;
    void** rdi16;
    int32_t eax17;
    void** rax18;
    int64_t* rdi19;
    int64_t rcx20;

    rbp4 = rsi + 0x70;
    eax5 = *reinterpret_cast<void***>(rdi + 72);
    if (*reinterpret_cast<unsigned char*>(&eax5) & 2) 
        goto addr_ccf0_2;
    if (*reinterpret_cast<unsigned char*>(&eax5) & 1 && !*reinterpret_cast<void***>(rsi + 88)) {
        goto addr_ccf0_2;
    }
    if (dl) {
        addr_ccf0_2:
        rsi6 = *reinterpret_cast<void***>(rsi + 48);
        rdi7 = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(&rdi7 + 4) = 0;
        eax8 = fun_3d10(rdi7, rsi6, rbp4);
        if (eax8 < 0) {
            rax9 = fun_3700();
            eax10 = *reinterpret_cast<void***>(rax9);
            if (eax10 == 2) {
                rsi6 = *reinterpret_cast<void***>(rsi + 48);
                rdi11 = *reinterpret_cast<void***>(rdi + 44);
                *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
                eax12 = fun_3d10(rdi11, rsi6, rbp4, rdi11, rsi6, rbp4);
                if (eax12 < 0) {
                    eax10 = *reinterpret_cast<void***>(rax9);
                } else {
                    *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(0);
                    return 13;
                }
            }
        } else {
            addr_cd07_10:
            eax13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x88)) & 0xf000;
            if (eax13 == 0x4000) {
                *reinterpret_cast<uint32_t*>(&rax14) = 1;
                *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi + 0x100) == 46) && (!*reinterpret_cast<signed char*>(rsi + 0x101) || (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x100)) & 0xffff00) == 0x2e00)) {
                    *reinterpret_cast<uint32_t*>(&rax14) = (1 - (1 + reinterpret_cast<uint1_t>(1 < 1 + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 88)) < reinterpret_cast<unsigned char>(1)))) & 0xfffffffc) + 5;
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_cd37_13;
                }
            } else {
                if (eax13 == 0xa000) {
                    *reinterpret_cast<uint32_t*>(&rax14) = 12;
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_cd37_13;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = reinterpret_cast<uint1_t>(eax13 == 0x8000);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax14) = static_cast<uint32_t>(rax15 + rax15 * 4 + 3);
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_cd37_13;
                }
            }
        }
    } else {
        rsi6 = *reinterpret_cast<void***>(rsi + 48);
        rdi16 = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
        eax17 = fun_3d10(rdi16, rsi6, rbp4, rdi16, rsi6, rbp4);
        if (eax17 >= 0) 
            goto addr_cd07_10;
        rax18 = fun_3700();
        eax10 = *reinterpret_cast<void***>(rax18);
    }
    *reinterpret_cast<void***>(rsi + 64) = eax10;
    rdi19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp4 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(rsi + 0x70) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbp4 + 0x88) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rcx20) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp4) - reinterpret_cast<uint64_t>(rdi19) + 0x90) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
    while (rcx20) {
        --rcx20;
        *rdi19 = 0;
        ++rdi19;
    }
    return 10;
    addr_cd37_13:
    return rax14;
}

void** cycle_check(void** rdi, void*** rsi);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rdi6;
    void** rax7;
    void** rax8;
    void** rax9;
    void** rdi10;
    void** rax11;
    void** rax12;
    void** rax13;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rdi6 = *reinterpret_cast<void***>(rdi + 88);
        rax7 = cycle_check(rdi6, rsi + 0x70);
        if (*reinterpret_cast<signed char*>(&rax7)) {
            *reinterpret_cast<void***>(rsi) = rsi;
            *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
            return rax7;
        }
    } else {
        rax8 = fun_3ae0(24, rsi, rdx);
        if (!rax8) 
            goto addr_d348_5;
        rax9 = *reinterpret_cast<void***>(rsi + 0x70);
        rdi10 = *reinterpret_cast<void***>(rdi + 88);
        *reinterpret_cast<void***>(rax8 + 16) = rsi;
        *reinterpret_cast<void***>(rax8) = rax9;
        *reinterpret_cast<void***>(rax8 + 8) = *reinterpret_cast<void***>(rsi + 0x78);
        rax11 = hash_insert(rdi10, rax8, rdx, rcx, r8);
        if (rax8 != rax11) 
            goto addr_d2f3_7;
    }
    addr_d310_8:
    *reinterpret_cast<int32_t*>(&rax12) = 1;
    *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
    addr_d315_9:
    return rax12;
    addr_d2f3_7:
    fun_36a0(rax8, rax8);
    if (!rax11) {
        addr_d348_5:
        *reinterpret_cast<int32_t*>(&rax12) = 0;
        *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
        goto addr_d315_9;
    } else {
        rax13 = *reinterpret_cast<void***>(rax11 + 16);
        *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
        *reinterpret_cast<void***>(rsi) = rax13;
        goto addr_d310_8;
    }
}

struct s15 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s15* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s15* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x17b43);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x17b3c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x17b47);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x17b38);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_3003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g1dc78 = 0;

void fun_3033() {
    __asm__("cli ");
    goto g1dc78;
}

void fun_3043() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3053() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3063() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3073() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3083() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3093() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3103() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3113() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3123() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3133() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3143() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3153() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3163() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3173() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3183() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3193() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3203() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3213() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3223() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3233() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3243() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3253() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3263() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3273() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3283() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3293() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3303() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3313() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3323() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3333() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3343() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3353() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3363() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3373() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3383() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3393() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3403() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3413() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3423() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3433() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3443() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3453() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3463() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3473() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3483() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3493() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3503() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3513() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3523() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3533() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3543() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3553() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3563() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3573() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3583() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3593() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3603() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3613() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3623() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3633() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3643() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3653() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3663() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3673() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3683() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3693() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_36a3() {
    __asm__("cli ");
    goto free;
}

int64_t __cxa_finalize = 0;

void fun_36b3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x3030;

void fun_36c3() {
    __asm__("cli ");
    goto getenv;
}

int64_t mkfifoat = 0x3040;

void fun_36d3() {
    __asm__("cli ");
    goto mkfifoat;
}

int64_t utimensat = 0x3050;

void fun_36e3() {
    __asm__("cli ");
    goto utimensat;
}

int64_t abort = 0x3060;

void fun_36f3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x3070;

void fun_3703() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x3080;

void fun_3713() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x3090;

void fun_3723() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x30a0;

void fun_3733() {
    __asm__("cli ");
    goto __fpending;
}

int64_t mkdir = 0x30b0;

void fun_3743() {
    __asm__("cli ");
    goto mkdir;
}

int64_t unlinkat = 0x30c0;

void fun_3753() {
    __asm__("cli ");
    goto unlinkat;
}

int64_t qsort = 0x30d0;

void fun_3763() {
    __asm__("cli ");
    goto qsort;
}

int64_t isatty = 0x30e0;

void fun_3773() {
    __asm__("cli ");
    goto isatty;
}

int64_t reallocarray = 0x30f0;

void fun_3783() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t faccessat = 0x3100;

void fun_3793() {
    __asm__("cli ");
    goto faccessat;
}

int64_t readlink = 0x3110;

void fun_37a3() {
    __asm__("cli ");
    goto readlink;
}

int64_t fcntl = 0x3120;

void fun_37b3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t clock_gettime = 0x3130;

void fun_37c3() {
    __asm__("cli ");
    goto clock_gettime;
}

int64_t write = 0x3140;

void fun_37d3() {
    __asm__("cli ");
    goto write;
}

int64_t textdomain = 0x3150;

void fun_37e3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t pathconf = 0x3160;

void fun_37f3() {
    __asm__("cli ");
    goto pathconf;
}

int64_t fclose = 0x3170;

void fun_3803() {
    __asm__("cli ");
    goto fclose;
}

int64_t opendir = 0x3180;

void fun_3813() {
    __asm__("cli ");
    goto opendir;
}

int64_t bindtextdomain = 0x3190;

void fun_3823() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t stpcpy = 0x31a0;

void fun_3833() {
    __asm__("cli ");
    goto stpcpy;
}

int64_t dcgettext = 0x31b0;

void fun_3843() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x31c0;

void fun_3853() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x31d0;

void fun_3863() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x31e0;

void fun_3873() {
    __asm__("cli ");
    goto openat;
}

int64_t __stack_chk_fail = 0x31f0;

void fun_3883() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x3200;

void fun_3893() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x3210;

void fun_38a3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x3220;

void fun_38b3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x3230;

void fun_38c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t ftruncate = 0x3240;

void fun_38d3() {
    __asm__("cli ");
    goto ftruncate;
}

int64_t lseek = 0x3250;

void fun_38e3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x3260;

void fun_38f3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x3270;

void fun_3903() {
    __asm__("cli ");
    goto memset;
}

int64_t geteuid = 0x3280;

void fun_3913() {
    __asm__("cli ");
    goto geteuid;
}

int64_t ioctl = 0x3290;

void fun_3923() {
    __asm__("cli ");
    goto ioctl;
}

int64_t copy_file_range = 0x32a0;

void fun_3933() {
    __asm__("cli ");
    goto copy_file_range;
}

int64_t canonicalize_file_name = 0x32b0;

void fun_3943() {
    __asm__("cli ");
    goto canonicalize_file_name;
}

int64_t close = 0x32c0;

void fun_3953() {
    __asm__("cli ");
    goto close;
}

int64_t rewinddir = 0x32d0;

void fun_3963() {
    __asm__("cli ");
    goto rewinddir;
}

int64_t strspn = 0x32e0;

void fun_3973() {
    __asm__("cli ");
    goto strspn;
}

int64_t closedir = 0x32f0;

void fun_3983() {
    __asm__("cli ");
    goto closedir;
}

int64_t posix_fadvise = 0x3300;

void fun_3993() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t read = 0x3310;

void fun_39a3() {
    __asm__("cli ");
    goto read;
}

int64_t lstat = 0x3320;

void fun_39b3() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x3330;

void fun_39c3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fallocate = 0x3340;

void fun_39d3() {
    __asm__("cli ");
    goto fallocate;
}

int64_t fputs_unlocked = 0x3350;

void fun_39e3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x3360;

void fun_39f3() {
    __asm__("cli ");
    goto calloc;
}

int64_t __getdelim = 0x3370;

void fun_3a03() {
    __asm__("cli ");
    goto __getdelim;
}

int64_t strcmp = 0x3380;

void fun_3a13() {
    __asm__("cli ");
    goto strcmp;
}

int64_t readlinkat = 0x3390;

void fun_3a23() {
    __asm__("cli ");
    goto readlinkat;
}

int64_t dirfd = 0x33a0;

void fun_3a33() {
    __asm__("cli ");
    goto dirfd;
}

int64_t fputc_unlocked = 0x33b0;

void fun_3a43() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t fpathconf = 0x33c0;

void fun_3a53() {
    __asm__("cli ");
    goto fpathconf;
}

int64_t mknodat = 0x33d0;

void fun_3a63() {
    __asm__("cli ");
    goto mknodat;
}

int64_t rpmatch = 0x33e0;

void fun_3a73() {
    __asm__("cli ");
    goto rpmatch;
}

int64_t mkdirat = 0x33f0;

void fun_3a83() {
    __asm__("cli ");
    goto mkdirat;
}

int64_t umask = 0x3400;

void fun_3a93() {
    __asm__("cli ");
    goto umask;
}

int64_t stat = 0x3410;

void fun_3aa3() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x3420;

void fun_3ab3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x3430;

void fun_3ac3() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x3440;

void fun_3ad3() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x3450;

void fun_3ae3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x3460;

void fun_3af3() {
    __asm__("cli ");
    goto fflush;
}

int64_t fchmodat = 0x3470;

void fun_3b03() {
    __asm__("cli ");
    goto fchmodat;
}

int64_t nl_langinfo = 0x3480;

void fun_3b13() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t renameat2 = 0x3490;

void fun_3b23() {
    __asm__("cli ");
    goto renameat2;
}

int64_t __freading = 0x34a0;

void fun_3b33() {
    __asm__("cli ");
    goto __freading;
}

int64_t fchdir = 0x34b0;

void fun_3b43() {
    __asm__("cli ");
    goto fchdir;
}

int64_t realloc = 0x34c0;

void fun_3b53() {
    __asm__("cli ");
    goto realloc;
}

int64_t linkat = 0x34d0;

void fun_3b63() {
    __asm__("cli ");
    goto linkat;
}

int64_t setlocale = 0x34e0;

void fun_3b73() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x34f0;

void fun_3b83() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t fchmod = 0x3500;

void fun_3b93() {
    __asm__("cli ");
    goto fchmod;
}

int64_t chmod = 0x3510;

void fun_3ba3() {
    __asm__("cli ");
    goto chmod;
}

int64_t mempcpy = 0x3520;

void fun_3bb3() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t memmove = 0x3530;

void fun_3bc3() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x3540;

void fun_3bd3() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x3550;

void fun_3be3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x3560;

void fun_3bf3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fchown = 0x3570;

void fun_3c03() {
    __asm__("cli ");
    goto fchown;
}

int64_t fdopendir = 0x3580;

void fun_3c13() {
    __asm__("cli ");
    goto fdopendir;
}

int64_t futimens = 0x3590;

void fun_3c23() {
    __asm__("cli ");
    goto futimens;
}

int64_t fstatfs = 0x35a0;

void fun_3c33() {
    __asm__("cli ");
    goto fstatfs;
}

int64_t __cxa_atexit = 0x35b0;

void fun_3c43() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t fchownat = 0x35c0;

void fun_3c53() {
    __asm__("cli ");
    goto fchownat;
}

int64_t renameat = 0x35d0;

void fun_3c63() {
    __asm__("cli ");
    goto renameat;
}

int64_t getpagesize = 0x35e0;

void fun_3c73() {
    __asm__("cli ");
    goto getpagesize;
}

int64_t exit = 0x35f0;

void fun_3c83() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x3600;

void fun_3c93() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x3610;

void fun_3ca3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t getrandom = 0x3620;

void fun_3cb3() {
    __asm__("cli ");
    goto getrandom;
}

int64_t aligned_alloc = 0x3630;

void fun_3cc3() {
    __asm__("cli ");
    goto aligned_alloc;
}

int64_t mbsinit = 0x3640;

void fun_3cd3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t symlinkat = 0x3650;

void fun_3ce3() {
    __asm__("cli ");
    goto symlinkat;
}

int64_t iswprint = 0x3660;

void fun_3cf3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x3670;

void fun_3d03() {
    __asm__("cli ");
    goto fstat;
}

int64_t fstatat = 0x3680;

void fun_3d13() {
    __asm__("cli ");
    goto fstatat;
}

int64_t __ctype_b_loc = 0x3690;

void fun_3d23() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_3b70(int64_t rdi, ...);

void fun_3820(int64_t rdi, int64_t rsi);

void fun_37e0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

void cp_options_default(void* rdi, int64_t rsi);

int32_t fun_3770();

int32_t target_directory_operand(void** rdi, void** rsi, void** rdx, void** rcx, void* r8, int64_t r9);

void strip_trailing_slashes(void** rdi, void** rsi, void** rdx, void** rcx, void* r8, int64_t r9);

void usage();

int32_t fun_3890(int64_t rdi, void** rsi, void** rdx, void** rcx);

int64_t Version = 0x178e1;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

void fun_3c80();

int32_t optind = 0;

int32_t xget_version(void** rdi, void** rsi, void** rdx, void** rcx, void* r8, int64_t r9);

void set_simple_backup_suffix(int64_t rdi, void** rsi, void** rdx, void** rcx, void* r8, int64_t r9);

void hash_init(int64_t rdi, void** rsi, void** rdx, void** rcx, void* r8, int64_t r9);

void dest_info_init(void* rdi, void** rsi, void** rdx, void** rcx, void* r8, int64_t r9);

void fun_3db3(int32_t edi, void** rsi) {
    void** r14_3;
    void** r13_4;
    int32_t ebp5;
    void** rbx6;
    void* rsp7;
    void** rdi8;
    void* r15_9;
    void** r12_10;
    void*** rsp11;
    int32_t v12;
    void** v13;
    void** v14;
    void** v15;
    int64_t rax16;
    void** rcx17;
    void** v18;
    void* r8_19;
    void** rdx20;
    void** rsi21;
    void** eax22;
    void*** rsp23;
    void** rax24;
    void** v25;
    int32_t eax26;
    void** rax27;
    int32_t eax28;
    void** rax29;
    void** rax30;
    void** rax31;
    void** rdi32;
    int64_t rdi33;
    int32_t eax34;
    void** rdi35;
    int64_t rcx36;
    int64_t rax37;
    int32_t eax38;
    void** rsi39;
    void** rax40;
    void* rsp41;
    void** rsi42;
    void** rax43;
    void** rax44;
    void*** rsp45;
    void** rdi46;
    uint32_t eax47;
    uint32_t ebx48;
    void** rax49;
    void** rax50;
    void** v51;
    void** v52;
    uint32_t eax53;

    __asm__("cli ");
    r14_3 = reinterpret_cast<void**>(0x1d7c0);
    r13_4 = reinterpret_cast<void**>("bfint:uvS:TZ");
    ebp5 = edi;
    rbx6 = rsi;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rdi8 = *reinterpret_cast<void***>(rsi);
    r15_9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 64);
    set_program_name(rdi8);
    fun_3b70(6, 6);
    fun_3820("coreutils", "/usr/local/share/locale");
    r12_10 = reinterpret_cast<void**>(0x16b60);
    fun_37e0("coreutils", "/usr/local/share/locale");
    atexit(0xc210, "/usr/local/share/locale");
    cp_options_default(r15_9, "/usr/local/share/locale");
    fun_3770();
    rsp11 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    *reinterpret_cast<signed char*>(&v12) = 0;
    v13 = reinterpret_cast<void**>(0);
    v14 = reinterpret_cast<void**>(0);
    *reinterpret_cast<signed char*>(&v15) = 0;
    goto addr_3f06_2;
    addr_439e_3:
    quotearg_style(4, r13_4, 4, r13_4);
    fun_3840();
    fun_3bd0();
    addr_43d6_4:
    fun_3840();
    fun_3bd0();
    fun_3840();
    fun_3bd0();
    addr_3f36_6:
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_10) + reinterpret_cast<uint64_t>(rax16 * 4)))) + reinterpret_cast<unsigned char>(r12_10);
    while (1) {
        addr_41d6_7:
        if (!v13) {
            rcx17 = v18;
            r13_4 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rcx17 + ebp5 * 8) - 8);
            if (ebp5 == 2) {
                *reinterpret_cast<int32_t*>(&r8_19) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_19) + 4) = 0;
                rcx17 = r13_4;
                *reinterpret_cast<int32_t*>(&rdx20) = -100;
                *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                rsi21 = *reinterpret_cast<void***>(v18);
                eax22 = renameatu();
                rsp23 = rsp23 - 8 + 8;
                if (eax22) {
                    rax24 = fun_3700();
                    rsp23 = rsp23 - 8 + 8;
                    eax22 = *reinterpret_cast<void***>(rax24);
                }
                v25 = eax22;
            }
            v12 = -100;
            if (v25) {
                rsi21 = reinterpret_cast<void**>(rsp23 + 0xa0);
                eax26 = target_directory_operand(r13_4, rsi21, rdx20, rcx17, r8_19, "David MacKenzie");
                rsp23 = rsp23 - 8 + 8;
                v12 = eax26;
                if (!(eax26 + 1)) {
                    rax27 = fun_3700();
                    rsp23 = rsp23 - 8 + 8;
                    v12 = -100;
                    r14_3 = *reinterpret_cast<void***>(rax27);
                    *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
                    if (ebp5 > 2) 
                        goto addr_439e_3;
                } else {
                    v25 = reinterpret_cast<void**>(0xffffffff);
                    --ebp5;
                    v13 = r13_4;
                }
            }
        } else {
            rsi21 = reinterpret_cast<void**>(rsp23 + 0xa0);
            eax28 = target_directory_operand(v13, rsi21, rdx20, rcx17, r8_19, "David MacKenzie");
            rsp23 = rsp23 - 8 + 8;
            v12 = eax28;
            if (!(eax28 + 1)) {
                rax29 = quotearg_style(4, v13, 4, v13);
                r13_4 = rax29;
                rax30 = fun_3840();
                r12_10 = rax30;
                rax31 = fun_3700();
                rcx17 = r13_4;
                rdx20 = r12_10;
                rsi21 = *reinterpret_cast<void***>(rax31);
                *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
                fun_3bd0();
                rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                goto addr_423d_18;
            }
        }
        while (1) {
            addr_40c4_19:
            if (!1) {
                rcx17 = v18;
                r13_4 = rcx17;
                rbx6 = rcx17 + ebp5 * 8;
                while (r13_4 != rbx6) {
                    rdi32 = *reinterpret_cast<void***>(r13_4);
                    r13_4 = r13_4 + 8;
                    strip_trailing_slashes(rdi32, rsi21, rdx20, rcx17, r8_19, "David MacKenzie");
                    rsp23 = rsp23 - 8 + 8;
                }
            }
            if (!0) 
                break;
            if (!*reinterpret_cast<signed char*>(&v15)) 
                goto addr_423d_18;
            while (1) {
                fun_3840();
                fun_3bd0();
                rsp23 = rsp23 - 8 + 8 - 8 + 8;
                while (1) {
                    addr_3fa7_27:
                    usage();
                    rsp11 = rsp23 - 8 + 8;
                    addr_3f06_2:
                    while (*reinterpret_cast<int32_t*>(&r8_19) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_19) + 4) = 0, rcx17 = r14_3, rdx20 = r13_4, rsi21 = rbx6, *reinterpret_cast<int32_t*>(&rdi33) = ebp5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0, eax34 = fun_3890(rdi33, rsi21, rdx20, rcx17), rsp23 = rsp11 - 8 + 8, eax34 != -1) {
                        if (eax34 > 0x80) 
                            goto addr_3fa7_27;
                        if (eax34 > 82) 
                            goto addr_3f2e_30;
                        if (eax34 != 0xffffff7d) 
                            goto addr_3f4b_32;
                        rdi35 = stdout;
                        rcx36 = Version;
                        version_etc(rdi35, "mv", "GNU coreutils", rcx36, "Mike Parker", "David MacKenzie", "Jim Meyering", 0);
                        fun_3c80();
                        rsp11 = rsp23 - 8 - 8 - 8 + 8 - 8 + 8;
                        *reinterpret_cast<signed char*>(&v14 + 3) = 1;
                    }
                    addr_3f59_34:
                    rax37 = optind;
                    ebp5 = ebp5 - *reinterpret_cast<int32_t*>(&rax37);
                    v18 = rbx6 + rax37 * 8;
                    eax38 = 0;
                    *reinterpret_cast<unsigned char*>(&eax38) = reinterpret_cast<uint1_t>(v13 == 0);
                    if (eax38 < ebp5) {
                        if (!*reinterpret_cast<signed char*>(&v12)) 
                            goto addr_41d6_7;
                        if (v13) 
                            goto addr_43d6_4;
                        v12 = -100;
                        if (ebp5 <= 2) 
                            goto addr_40c4_19;
                        rsi39 = *reinterpret_cast<void***>(v18 + 16);
                        rax40 = quotearg_style(4, rsi39, 4, rsi39);
                        rsp41 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                        r12_10 = rax40;
                    } else {
                        --ebp5;
                        if (ebp5) 
                            break;
                        rsi42 = *reinterpret_cast<void***>(v18);
                        rax43 = quotearg_style(4, rsi42, 4, rsi42);
                        rsp41 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                        r12_10 = rax43;
                    }
                    fun_3840();
                    fun_3bd0();
                    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp41) - 8 + 8 - 8 + 8);
                    continue;
                    addr_3f2e_30:
                    *reinterpret_cast<uint32_t*>(&rax16) = eax34 - 83;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rax16) > 45) 
                        continue; else 
                        goto addr_3f36_6;
                    addr_3f4b_32:
                    if (eax34 != 0xffffff7e) 
                        continue;
                    usage();
                    rsp23 = rsp23 - 8 + 8;
                    goto addr_3f59_34;
                }
            }
        }
        if (!*reinterpret_cast<signed char*>(&v15)) {
            addr_423d_18:
        } else {
            *reinterpret_cast<int32_t*>(&rdx20) = 5;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rax44 = fun_3840();
            rsi21 = v14;
            xget_version(rax44, rsi21, 5, rcx17, r8_19, "David MacKenzie");
            rsp23 = rsp23 - 8 + 8 - 8 + 8;
        }
        set_simple_backup_suffix(0, rsi21, rdx20, rcx17, r8_19, "David MacKenzie");
        hash_init(0, rsi21, rdx20, rcx17, r8_19, "David MacKenzie");
        rsp45 = rsp23 - 8 + 8 - 8 + 8;
        if (!v13) {
            r8_19 = r15_9;
            *reinterpret_cast<int32_t*>(&rdx20) = -100;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rsi21 = *reinterpret_cast<void***>(v18 + 8);
            rdi46 = *reinterpret_cast<void***>(v18);
            rcx17 = rsi21;
            eax47 = do_move(rdi46, rsi21, 0xffffff9c, rcx17, r8_19, "David MacKenzie");
            rsp45 = rsp45 - 8 + 8;
            ebx48 = eax47;
        } else {
            if (ebp5 > 1) {
                dest_info_init(r15_9, rsi21, rdx20, rcx17, r8_19, "David MacKenzie");
                rsp45 = rsp45 - 8 + 8;
            }
            r14_3 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
            ebx48 = 1;
            v15 = reinterpret_cast<void**>(rsp45 + 56);
            r12_10 = reinterpret_cast<void**>(0);
            while (ebp5 > *reinterpret_cast<int32_t*>(&r12_10)) {
                r14_3 = *reinterpret_cast<void***>(v18 + reinterpret_cast<unsigned char>(r12_10) * 8);
                ++r12_10;
                rax49 = last_component(r14_3, rsi21, rdx20, rcx17, r14_3, rsi21, rdx20, rcx17);
                rax50 = file_name_concat(v13, rax49, v13, rax49);
                r13_4 = rax50;
                strip_trailing_slashes(v51, rax49, v15, rcx17, r8_19, "David MacKenzie");
                rcx17 = v52;
                r8_19 = r15_9;
                rsi21 = r13_4;
                *reinterpret_cast<int32_t*>(&rdx20) = v12;
                *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                eax53 = do_move(r14_3, rsi21, rdx20, rcx17, r8_19, "David MacKenzie");
                ebx48 = ebx48 & eax53;
                fun_36a0(r13_4, r13_4);
                rsp45 = rsp45 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
            }
        }
        *reinterpret_cast<uint32_t*>(&rbx6) = ebx48 ^ 1;
        *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
        fun_3c80();
        rsp23 = rsp45 - 8 + 8;
    }
}

int64_t __libc_start_main = 0;

void fun_4423() {
    __asm__("cli ");
    __libc_start_main(0x3db0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x1e008;

void fun_36b0(int64_t rdi);

int64_t fun_44c3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_36b0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_4503() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_39e0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

unsigned char free;

unsigned char g1;

signed char g2;

int32_t fun_3710(void** rdi, void** rsi, void** rdx, ...);

void fun_4673(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** r8_4;
    void** r12_5;
    void** rax6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    void** r12_19;
    void** rax20;
    void** r12_21;
    void** rax22;
    uint32_t ecx23;
    uint32_t ecx24;
    int1_t zf25;
    void** rax26;
    void** rax27;
    int32_t eax28;
    void** rax29;
    void** r13_30;
    void** rax31;
    void** rax32;
    int32_t eax33;
    void** rax34;
    void** r14_35;
    void** rax36;
    void** rax37;
    void** rax38;
    void** rdi39;
    void** r8_40;
    void** r9_41;
    void** v42;
    void** v43;
    void** v44;
    int64_t v45;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_3840();
            r8_4 = r12_2;
            fun_3b80(1, rax3, r12_2, r12_2, 1, rax3, r12_2, r12_2);
            r12_5 = stdout;
            rax6 = fun_3840();
            fun_39e0(rax6, r12_5, 5, r12_2, r8_4);
            r12_7 = stdout;
            rax8 = fun_3840();
            fun_39e0(rax8, r12_7, 5, r12_2, r8_4);
            r12_9 = stdout;
            rax10 = fun_3840();
            fun_39e0(rax10, r12_9, 5, r12_2, r8_4);
            r12_11 = stdout;
            rax12 = fun_3840();
            fun_39e0(rax12, r12_11, 5, r12_2, r8_4);
            r12_13 = stdout;
            rax14 = fun_3840();
            fun_39e0(rax14, r12_13, 5, r12_2, r8_4);
            r12_15 = stdout;
            rax16 = fun_3840();
            fun_39e0(rax16, r12_15, 5, r12_2, r8_4);
            r12_17 = stdout;
            rax18 = fun_3840();
            fun_39e0(rax18, r12_17, 5, r12_2, r8_4);
            r12_19 = stdout;
            rax20 = fun_3840();
            fun_39e0(rax20, r12_19, 5, r12_2, r8_4);
            r12_21 = stdout;
            rax22 = fun_3840();
            fun_39e0(rax22, r12_21, 5, r12_2, r8_4);
            do {
                if (1) 
                    break;
                ecx23 = free;
            } while (0x6d != ecx23 || ((ecx24 = g1, 0x76 != ecx24) || (zf25 = g2 == 0, !zf25)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax26 = fun_3840();
                fun_3b80(1, rax26, "GNU coreutils", "https://www.gnu.org/software/coreutils/", 1, rax26, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax27 = fun_3b70(5);
                if (!rax27 || (eax28 = fun_3710(rax27, "en_", 3, rax27, "en_", 3), !eax28)) {
                    rax29 = fun_3840();
                    r12_2 = reinterpret_cast<void**>("mv");
                    r13_30 = reinterpret_cast<void**>(" invocation");
                    fun_3b80(1, rax29, "https://www.gnu.org/software/coreutils/", "mv", 1, rax29, "https://www.gnu.org/software/coreutils/", "mv");
                } else {
                    r12_2 = reinterpret_cast<void**>("mv");
                    goto addr_4a73_9;
                }
            } else {
                rax31 = fun_3840();
                fun_3b80(1, rax31, "GNU coreutils", "https://www.gnu.org/software/coreutils/", 1, rax31, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax32 = fun_3b70(5);
                if (!rax32 || (eax33 = fun_3710(rax32, "en_", 3, rax32, "en_", 3), !eax33)) {
                    addr_4976_11:
                    rax34 = fun_3840();
                    r13_30 = reinterpret_cast<void**>(" invocation");
                    fun_3b80(1, rax34, "https://www.gnu.org/software/coreutils/", "mv", 1, rax34, "https://www.gnu.org/software/coreutils/", "mv");
                    if (!reinterpret_cast<int1_t>(r12_2 == "mv")) {
                        r13_30 = reinterpret_cast<void**>(0x178bb);
                    }
                } else {
                    addr_4a73_9:
                    r14_35 = stdout;
                    rax36 = fun_3840();
                    fun_39e0(rax36, r14_35, 5, "https://www.gnu.org/software/coreutils/", r8_4);
                    goto addr_4976_11;
                }
            }
            rax37 = fun_3840();
            fun_3b80(1, rax37, r12_2, r13_30, 1, rax37, r12_2, r13_30);
            addr_46c9_14:
            fun_3c80();
        }
    } else {
        rax38 = fun_3840();
        rdi39 = stderr;
        fun_3ca0(rdi39, 1, rax38, r12_2, r8_40, r9_41, v42, v43, v44, v45);
        goto addr_46c9_14;
    }
}

struct s1* xfts_open();

void rpl_fts_set(struct s1* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** rpl_fts_read(struct s1* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t rpl_fts_close(struct s1* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int64_t fun_5243(int64_t* rdi, void** rsi) {
    void* rsp3;
    int64_t rax4;
    int64_t v5;
    uint32_t r12d6;
    void** rbx7;
    void** rdx8;
    void** rsi9;
    struct s1* rax10;
    void* rsp11;
    struct s1* rbp12;
    int64_t rax13;
    void** rsi14;
    void** r15_15;
    void** rax16;
    void** rcx17;
    void* rsp18;
    void* rsp19;
    void** rax20;
    void** r8_21;
    void** r9_22;
    void** rax23;
    void** rcx24;
    void** rdi25;
    int32_t v26;
    void** v27;
    void* v28;
    int32_t eax29;
    void** rdi30;
    void** rax31;
    void* rsp32;
    void** r14_33;
    void** rax34;
    void* rsp35;
    void** r13_36;
    struct s0* rax37;
    void* rdx38;
    uint32_t eax39;
    int32_t eax40;
    void** rax41;
    void** rax42;
    int64_t rax43;
    uint32_t ecx44;
    void** r14d45;
    void** rdi46;
    uint32_t eax47;
    void** r13_48;
    uint32_t eax49;
    void** rdi50;
    uint32_t eax51;
    int32_t v52;
    uint32_t eax53;
    void** rax54;
    void* rdx55;
    uint32_t eax56;
    void** rdx57;
    void** rax58;
    void* rsp59;
    int32_t eax60;
    void* rsp61;
    void** v62;
    void** rax63;
    void** rsi64;
    void** rax65;
    void** rax66;
    void** rax67;
    void** rax68;
    void** rax69;
    void** rsi70;
    void** rax71;
    void** rax72;
    void* rsp73;
    void** rax74;
    void** rax75;
    void** rax76;
    void** rsi77;
    void** rax78;
    void** rax79;
    void** rax80;
    void** rax81;
    void* rsp82;
    void** rax83;
    void** rsi84;
    int32_t eax85;
    int64_t rax86;
    int32_t eax87;
    void** r12d88;

    __asm__("cli ");
    rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8);
    rax4 = g28;
    v5 = rax4;
    if (!*rdi) {
        r12d6 = 2;
        goto addr_5398_3;
    } else {
        rbx7 = rsi;
        r12d6 = 2;
        rdx8 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rsi9) = (*reinterpret_cast<uint32_t*>(&rsi) - (*reinterpret_cast<uint32_t*>(&rsi) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rsi) < *reinterpret_cast<uint32_t*>(&rsi) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8)) < 1))) & 0xffffffc0) + 0x258;
        *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
        rax10 = xfts_open();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) - 8 + 8);
        rbp12 = rax10;
        goto addr_5298_5;
    }
    addr_53af_6:
    *reinterpret_cast<uint32_t*>(&rax13) = r12d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    return rax13;
    while (1) {
        addr_56a0_7:
        while (1) {
            rsi14 = *reinterpret_cast<void***>(r15_15 + 56);
            rax16 = quotearg_style(4, rsi14, 4, rsi14);
            fun_3840();
            rcx17 = rax16;
            fun_3bd0();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
            rax20 = *reinterpret_cast<void***>(r15_15 + 8);
            if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax20 + 88)) >= reinterpret_cast<signed char>(0)) {
                do {
                    if (*reinterpret_cast<void***>(rax20 + 32)) 
                        break;
                    *reinterpret_cast<void***>(rax20 + 32) = reinterpret_cast<void**>(1);
                    rax20 = *reinterpret_cast<void***>(rax20 + 8);
                } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax20 + 88)) >= reinterpret_cast<signed char>(0));
                goto addr_54f7_11;
            } else {
                goto addr_54f7_11;
            }
            while (1) {
                addr_54f7_11:
                rdx8 = reinterpret_cast<void**>(4);
                *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                rsi9 = r15_15;
                r12d6 = 4;
                rpl_fts_set(rbp12, rsi9, 4, rcx17, r8_21, r9_22);
                rpl_fts_read(rbp12, rsi9, 4, rcx17, r8_21, r9_22);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
                addr_5298_5:
                while (rax23 = rpl_fts_read(rbp12, rsi9, rdx8, rcx17, r8_21, r9_22), rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8), r15_15 = rax23, !!rax23) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rcx24) = *reinterpret_cast<uint16_t*>(rax23 + 0x68);
                        *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                        if (*reinterpret_cast<int16_t*>(&rcx24) == 1) {
                            addr_5430_15:
                            if (!*reinterpret_cast<signed char*>(rbx7 + 9)) {
                                if (!*reinterpret_cast<signed char*>(rbx7 + 10)) 
                                    goto addr_56a0_7;
                                rsi9 = *reinterpret_cast<void***>(rax23 + 48);
                                *reinterpret_cast<int32_t*>(&rdi25) = rbp12->f2c;
                                *reinterpret_cast<int32_t*>(&rdi25 + 4) = 0;
                                eax29 = fun_3870(rdi25, rsi9, 0x30900, rcx24, r8_21, r9_22, v26, v27, v28);
                                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                                if (eax29 < 0) 
                                    goto addr_54aa_18;
                                *reinterpret_cast<int32_t*>(&rdi30) = eax29;
                                *reinterpret_cast<int32_t*>(&rdi30 + 4) = 0;
                                rax31 = fun_3c10(rdi30, rsi9, rdi30, rsi9);
                                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                                r14_33 = rax31;
                                if (!rax31) 
                                    goto addr_5aa7_20;
                                rax34 = fun_3700();
                                rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                                *reinterpret_cast<void***>(rax34) = reinterpret_cast<void**>(0);
                                r13_36 = rax34;
                                do {
                                    rax37 = fun_3ad0(r14_33, rsi9, r14_33, rsi9);
                                    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                    if (!rax37) 
                                        break;
                                    if (*reinterpret_cast<void***>(&rax37->f13) != 46) 
                                        goto addr_549a_24;
                                    *reinterpret_cast<int32_t*>(&rdx38) = 0;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                    *reinterpret_cast<unsigned char*>(&rdx38) = reinterpret_cast<uint1_t>(rax37->f14 == 46);
                                    eax39 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax37) + reinterpret_cast<int64_t>(rdx38) + 20);
                                } while (!*reinterpret_cast<signed char*>(&eax39) || *reinterpret_cast<signed char*>(&eax39) == 47);
                                goto addr_58de_26;
                                (&v27)[4] = *reinterpret_cast<void***>(r13_36);
                                fun_3980(r14_33, rsi9, r14_33, rsi9);
                                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
                                rdx8 = (&v27)[4];
                                *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                                *reinterpret_cast<void***>(r13_36) = rdx8;
                                if (rdx8) 
                                    goto addr_54aa_18;
                            }
                        } else {
                            eax40 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx24 + 0xfffffffffffffffe));
                            if (*reinterpret_cast<uint16_t*>(&eax40) > 11) {
                                addr_53dc_30:
                                rax41 = quotearg_n_style_colon();
                                r12d6 = *reinterpret_cast<uint16_t*>(r15_15 + 0x68);
                                rbx7 = rax41;
                                rax42 = fun_3840();
                                r9_22 = reinterpret_cast<void**>("bug-coreutils@gnu.org");
                                *reinterpret_cast<uint32_t*>(&rsi9) = 0;
                                *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
                                rdx8 = rax42;
                                r8_21 = rbx7;
                                *reinterpret_cast<uint32_t*>(&rcx24) = r12d6;
                                *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
                                fun_3bd0();
                                rax23 = fun_36f0();
                                rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                goto addr_5430_15;
                            } else {
                                rax43 = 1 << *reinterpret_cast<unsigned char*>(&rcx24);
                                if (!(*reinterpret_cast<uint32_t*>(&rax43) & 0x3d58)) {
                                    if (*reinterpret_cast<int16_t*>(&rcx24) == 7) 
                                        goto addr_5520_33;
                                    if (*reinterpret_cast<int16_t*>(&rcx24) == 2) 
                                        goto addr_54c0_35; else 
                                        goto addr_53dc_30;
                                } else {
                                    if (*reinterpret_cast<int16_t*>(&rcx24) == 6 && (*reinterpret_cast<void***>(rbx7 + 8) && reinterpret_cast<signed char>(*reinterpret_cast<void***>(r15_15 + 88)) > reinterpret_cast<signed char>(0))) {
                                        if (*reinterpret_cast<void***>(r15_15 + 0x70) != rbp12->f18) 
                                            goto addr_59ac_38;
                                    }
                                    ecx44 = *reinterpret_cast<uint32_t*>(&rcx24) & 0xfffffffd;
                                    r14d45 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&rdi46) = rbp12->f2c;
                                    *reinterpret_cast<int32_t*>(&rdi46 + 4) = 0;
                                    *reinterpret_cast<int32_t*>(&r8_21) = 3;
                                    *reinterpret_cast<int32_t*>(&r8_21 + 4) = 0;
                                    rsi9 = r15_15;
                                    rcx17 = rbx7;
                                    *reinterpret_cast<unsigned char*>(&r14d45) = reinterpret_cast<uint1_t>(*reinterpret_cast<int16_t*>(&ecx44) == 4);
                                    *reinterpret_cast<int32_t*>(&r9_22) = 0;
                                    *reinterpret_cast<int32_t*>(&r9_22 + 4) = 0;
                                    rdx8 = r14d45;
                                    *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                                    eax47 = prompt_isra_0(rdi46, rsi9, rdx8, rcx17, 3, 0);
                                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                                    *reinterpret_cast<uint32_t*>(&r13_48) = eax47;
                                    *reinterpret_cast<int32_t*>(&r13_48 + 4) = 0;
                                    if (eax47 == 2) {
                                        rcx17 = r14d45;
                                        *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                                        rdx8 = rbx7;
                                        rsi9 = r15_15;
                                        eax49 = excise(rbp12, rsi9, rdx8, rcx17, 3);
                                        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                                        *reinterpret_cast<uint32_t*>(&r13_48) = eax49;
                                        *reinterpret_cast<int32_t*>(&r13_48 + 4) = 0;
                                        goto addr_5334_42;
                                    }
                                }
                            }
                        }
                        if (*reinterpret_cast<void***>(r15_15 + 88)) {
                            addr_55bc_44:
                            *reinterpret_cast<int32_t*>(&rdi50) = rbp12->f2c;
                            *reinterpret_cast<int32_t*>(&rdi50 + 4) = 0;
                            r9_22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp19) + 28);
                            rcx17 = rbx7;
                            rdx8 = reinterpret_cast<void**>(1);
                            *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&r8_21) = 2;
                            *reinterpret_cast<int32_t*>(&r8_21 + 4) = 0;
                            rsi9 = r15_15;
                            eax51 = prompt_isra_0(rdi50, rsi9, 1, rcx17, 2, r9_22);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                            *reinterpret_cast<uint32_t*>(&r13_48) = eax51;
                            *reinterpret_cast<int32_t*>(&r13_48 + 4) = 0;
                            if (eax51 == 2) {
                                if (v52 != 4) 
                                    goto addr_5298_5;
                                rcx17 = reinterpret_cast<void**>(1);
                                *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                                eax53 = excise(rbp12, r15_15, rbx7, 1, 2, rbp12, r15_15, rbx7, 1, 2);
                                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&r13_48) = eax53;
                                *reinterpret_cast<int32_t*>(&r13_48 + 4) = 0;
                                if (eax53 == 2) 
                                    break;
                            }
                        } else {
                            r13_48 = *reinterpret_cast<void***>(r15_15 + 48);
                            rax54 = last_component(r13_48, rsi9, rdx8, rcx24, r13_48, rsi9, rdx8, rcx24);
                            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                            if (*reinterpret_cast<void***>(rax54) != 46) 
                                goto addr_559c_48;
                            *reinterpret_cast<int32_t*>(&rdx55) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0;
                            *reinterpret_cast<unsigned char*>(&rdx55) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax54 + 1) == 46);
                            eax56 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax54) + reinterpret_cast<uint64_t>(rdx55) + 1);
                            if (!*reinterpret_cast<signed char*>(&eax56)) 
                                goto addr_5732_50;
                            if (*reinterpret_cast<signed char*>(&eax56) == 47) 
                                goto addr_5732_50;
                            addr_559c_48:
                            if (!*reinterpret_cast<void***>(rbx7 + 16)) 
                                goto addr_55b2_52;
                            if (*reinterpret_cast<void***>(r15_15 + 0x78) != *reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx7 + 16))) 
                                goto addr_55b2_52;
                            if (*reinterpret_cast<void***>(r15_15 + 0x70) == *reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx7 + 16) + 8)) 
                                goto addr_585e_55;
                            addr_55b2_52:
                            if (*reinterpret_cast<void***>(rbx7 + 24)) 
                                goto addr_57b0_56; else 
                                goto addr_55bc_44;
                        }
                        rdx57 = *reinterpret_cast<void***>(r15_15 + 8);
                        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx57 + 88)) >= reinterpret_cast<signed char>(0)) {
                            do {
                                if (*reinterpret_cast<void***>(rdx57 + 32)) 
                                    break;
                                *reinterpret_cast<void***>(rdx57 + 32) = reinterpret_cast<void**>(1);
                                rdx57 = *reinterpret_cast<void***>(rdx57 + 8);
                            } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx57 + 88)) >= reinterpret_cast<signed char>(0));
                        }
                        rdx8 = reinterpret_cast<void**>(4);
                        *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                        rsi9 = r15_15;
                        rpl_fts_set(rbp12, rsi9, 4, rcx17, 2, r9_22);
                        rpl_fts_read(rbp12, rsi9, 4, rcx17, 2, r9_22);
                        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
                        addr_5334_42:
                        if (static_cast<uint32_t>(reinterpret_cast<uint64_t>(r13_48 + 0xfffffffffffffffe)) > 2) 
                            goto addr_5a83_62;
                        if (*reinterpret_cast<uint32_t*>(&r13_48) == 4) 
                            goto addr_5ab4_64;
                        if (*reinterpret_cast<uint32_t*>(&r13_48) != 3) 
                            goto addr_5298_5;
                        if (r12d6 == 2) {
                            r12d6 = 3;
                            continue;
                        }
                        addr_57b0_56:
                        rax58 = file_name_concat(r13_48, "..");
                        rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
                        eax60 = fun_39b0(rax58, reinterpret_cast<int64_t>(rsp59) + 32);
                        rsp61 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
                        if (eax60) 
                            goto addr_5950_68;
                        fun_36a0(rax58, rax58);
                        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8);
                        if (rbp12->f18 == v62) 
                            goto addr_55bc_44; else 
                            goto addr_57f0_70;
                        rax23 = rpl_fts_read(rbp12, rsi9, rdx8, rcx17, r8_21, r9_22);
                        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        r15_15 = rax23;
                    } while (rax23);
                    break;
                    rdx8 = reinterpret_cast<void**>(4);
                    *reinterpret_cast<int32_t*>(&rdx8 + 4) = 0;
                    rsi9 = r15_15;
                    rpl_fts_set(rbp12, rsi9, 4, 1, 2, r9_22);
                    rpl_fts_read(rbp12, rsi9, 4, 1, 2, r9_22);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
                    continue;
                    addr_5ab4_64:
                    r12d6 = 4;
                    continue;
                    addr_59ac_38:
                    rax63 = *reinterpret_cast<void***>(r15_15 + 8);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax63 + 88)) >= reinterpret_cast<signed char>(0)) {
                        do {
                            if (*reinterpret_cast<void***>(rax63 + 32)) 
                                break;
                            *reinterpret_cast<void***>(rax63 + 32) = reinterpret_cast<void**>(1);
                            rax63 = *reinterpret_cast<void***>(rax63 + 8);
                        } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax63 + 88)) >= reinterpret_cast<signed char>(0));
                    }
                    rsi64 = *reinterpret_cast<void***>(r15_15 + 56);
                    rax65 = quotearg_style(4, rsi64, 4, rsi64);
                    rax66 = fun_3840();
                    rcx17 = rax65;
                    *reinterpret_cast<uint32_t*>(&rsi9) = 0;
                    *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
                    rdx8 = rax66;
                    r12d6 = 4;
                    fun_3bd0();
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                }
                break;
                addr_5732_50:
                rax67 = quotearg_n_style();
                rax68 = quotearg_n_style();
                rax69 = quotearg_n_style();
                fun_3840();
                r9_22 = rax67;
                r8_21 = rax68;
                rcx17 = rax69;
                fun_3bd0();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                continue;
                addr_585e_55:
                rsi70 = *reinterpret_cast<void***>(r15_15 + 56);
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi70) == 47) || *reinterpret_cast<void***>(rsi70 + 1)) {
                    rax71 = quotearg_n_style();
                    rax72 = quotearg_n_style();
                    fun_3840();
                    r8_21 = rax71;
                    rcx17 = rax72;
                    fun_3bd0();
                    rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                } else {
                    rax74 = quotearg_style(4, rsi70, 4, rsi70);
                    fun_3840();
                    rcx17 = rax74;
                    fun_3bd0();
                    rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                }
                addr_5831_80:
                fun_3840();
                fun_3bd0();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 + 8);
                continue;
                addr_5950_68:
                rax75 = quotearg_n_style();
                rax76 = quotearg_n_style();
                fun_3840();
                r8_21 = rax75;
                rcx17 = rax76;
                fun_3bd0();
                fun_36a0(rax58, rax58);
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp61) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                continue;
                addr_57f0_70:
                rsi77 = *reinterpret_cast<void***>(r15_15 + 56);
                rax78 = quotearg_style(4, rsi77, 4, rsi77);
                fun_3840();
                rcx17 = rax78;
                fun_3bd0();
                rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_5831_80;
                addr_5520_33:
                rax79 = quotearg_n_style_colon();
                fun_3840();
                rcx17 = rax79;
                fun_3bd0();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
                continue;
                addr_54c0_35:
                rax80 = quotearg_n_style_colon();
                fun_3840();
                rcx17 = rax80;
                fun_3bd0();
                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8);
            }
            rax81 = fun_3700();
            rsp82 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
            rbx7 = rax81;
            if (*reinterpret_cast<void***>(rax81)) {
                r12d6 = 4;
                rax83 = fun_3840();
                rsi84 = *reinterpret_cast<void***>(rbx7);
                *reinterpret_cast<int32_t*>(&rsi84 + 4) = 0;
                fun_3bd0();
                eax85 = rpl_fts_close(rbp12, rsi84, rax83, rcx17, r8_21, r9_22);
                rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp82) - 8 + 8 - 8 + 8 - 8 + 8);
                if (!eax85) {
                    addr_5398_3:
                    rax86 = v5 - g28;
                    if (!rax86) 
                        goto addr_53af_6;
                } else {
                    addr_591f_83:
                    r12d6 = 4;
                    fun_3840();
                    fun_3bd0();
                    rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) - 8 + 8 - 8 + 8);
                    goto addr_5398_3;
                }
            } else {
                eax87 = rpl_fts_close(rbp12, rsi9, rdx8, rcx17, r8_21, r9_22);
                rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp82) - 8 + 8);
                if (eax87) 
                    goto addr_591f_83; else 
                    goto addr_5398_3;
            }
            addr_5aa2_85:
            fun_3880();
            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp3) - 8 + 8);
            addr_5aa7_20:
            fun_3950();
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
            addr_54aa_18:
            if (!*reinterpret_cast<signed char*>(rbx7 + 10)) 
                break;
            continue;
            addr_5a83_62:
            fun_38f0("VALID_STATUS (s)", "src/remove.c", 0x263, "rm", "VALID_STATUS (s)", "src/remove.c", 0x263, "rm");
            rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
            goto addr_5aa2_85;
            addr_549a_24:
            r12d88 = *reinterpret_cast<void***>(r13_36);
            fun_3980(r14_33, rsi9, r14_33, rsi9);
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            *reinterpret_cast<void***>(r13_36) = r12d88;
            goto addr_54aa_18;
            addr_58de_26:
            goto addr_549a_24;
        }
    }
}

struct s16 {
    signed char[51] pad51;
    signed char f33;
};

struct s17 {
    signed char[40] pad40;
    int64_t f28;
};

struct s18 {
    signed char[49] pad49;
    signed char f31;
    signed char[2] pad52;
    unsigned char f34;
};

int64_t fun_6923(void** rdi) {
    struct s16* r8_2;
    int32_t r12d3;
    struct s17* r8_4;
    unsigned char cl5;
    void** rax6;
    int64_t rax7;
    void** rax8;
    struct s18* r8_9;
    uint32_t r12d10;
    int64_t rax11;

    __asm__("cli ");
    if (!r8_2->f33) {
        *reinterpret_cast<unsigned char*>(&r12d3) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!r8_4->f28)) & cl5);
        if (*reinterpret_cast<unsigned char*>(&r12d3)) {
            rax6 = fun_3700();
            *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(95);
            *reinterpret_cast<int32_t*>(&rax7) = r12d3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
            return rax7;
        } else {
            return 1;
        }
    } else {
        rax8 = fun_3700();
        if (!r8_9->f31 || (r12d10 = r8_9->f34, !!*reinterpret_cast<signed char*>(&r12d10))) {
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(95);
            quotearg_style(4, rdi);
            fun_3840();
            fun_3bd0();
            r12d10 = r8_9->f34;
        } else {
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(95);
        }
        *reinterpret_cast<uint32_t*>(&rax11) = r12d10 ^ 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        return rax11;
    }
}

struct s19 {
    signed char[49] pad49;
    signed char f31;
    signed char[2] pad52;
    signed char f34;
};

int64_t fun_6a03(int64_t rdi) {
    void** rax2;
    struct s19* rdx3;

    __asm__("cli ");
    rax2 = fun_3700();
    if (!rdx3->f31 || rdx3->f34) {
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(95);
        quotearg_n_style();
        fun_3840();
        fun_3bd0();
        return 0;
    } else {
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(95);
        return 0;
    }
}

struct s20 {
    signed char[72] pad72;
    void** f48;
};

void xalloc_die();

void fun_6a83(struct s20* rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = hash_initialize(61);
    rdi->f48 = rax2;
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

struct s21 {
    signed char[80] pad80;
    void** f50;
};

void fun_6ac3(struct s21* rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = hash_initialize(61);
    rdi->f50 = rax2;
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

struct s22 {
    signed char[26] pad26;
    unsigned char f1a;
    unsigned char f1b;
    signed char[36] pad64;
    int32_t f40;
};

struct s23 {
    signed char[72] pad72;
    int64_t f48;
};

int64_t fun_3910();

void fun_6b03(struct s22* rdi) {
    struct s22* rbx2;
    struct s23* rdi3;
    int64_t* rdi4;
    void* rcx5;
    int64_t rcx6;
    int64_t rax7;
    unsigned char al8;

    __asm__("cli ");
    rbx2 = rdi;
    rdi3 = reinterpret_cast<struct s23*>(reinterpret_cast<int64_t>(rdi) + 8);
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi3) - 8) = 0;
    rdi3->f48 = 0;
    rdi4 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi3) & 0xfffffffffffffff8);
    rcx5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbx2) - reinterpret_cast<uint64_t>(rdi4));
    *reinterpret_cast<uint32_t*>(&rcx6) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rcx5) + 88) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    while (rcx6) {
        --rcx6;
        *rdi4 = 0;
        ++rdi4;
    }
    rax7 = fun_3910();
    rbx2->f40 = -1;
    al8 = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax7) == 0);
    rbx2->f1b = al8;
    rbx2->f1a = al8;
    return;
}

struct s24 {
    signed char[26] pad26;
    unsigned char f1a;
};

void** fun_6b53(struct s24* rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_3700();
    *reinterpret_cast<unsigned char*>(&rax2) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax2) == 1)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax2) == 22)));
    if (*reinterpret_cast<unsigned char*>(&rax2)) {
        *reinterpret_cast<uint32_t*>(&rax2) = static_cast<uint32_t>(rdi->f1a) ^ 1;
        *reinterpret_cast<int32_t*>(&rax2 + 4) = 0;
    }
    return rax2;
}

void** top_level_dst_name = reinterpret_cast<void**>(0);

unsigned char fun_ad43(void** rdi, void** rsi, void** edx, void** rcx, void** r8d, void** r9, void** a7, void** a8) {
    int64_t rax9;
    int64_t rax10;
    unsigned char al11;
    int64_t rdx12;

    __asm__("cli ");
    rax9 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9)) <= reinterpret_cast<unsigned char>(3)) {
        *reinterpret_cast<signed char*>(&rax10) = *reinterpret_cast<signed char*>(r9 + 12);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        if (static_cast<uint32_t>(rax10 - 1) <= 2) {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 68)) > reinterpret_cast<unsigned char>(2)) {
                addr_ae2d_4:
                fun_38f0("VALID_REFLINK_MODE (co->reflink_mode)", "src/copy.c", 0xc11, "valid_options");
            } else {
                if (*reinterpret_cast<unsigned char*>(r9 + 23) && *reinterpret_cast<unsigned char*>(r9 + 58)) {
                    fun_38f0("!(co->hard_link && co->symbolic_link)", "src/copy.c", 0xc12, "valid_options");
                    goto addr_ae0e_7;
                }
                if (*reinterpret_cast<signed char*>(&rax10) == 2 || *reinterpret_cast<void***>(r9 + 68) != 2) {
                    top_level_src_name = rdi;
                    top_level_dst_name = rsi;
                    al11 = copy_internal(rdi, rsi, edx, rcx, r8d, 0, 0, r9, 1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16 - 8 - 8 + 23, a7, a8);
                    rdx12 = rax9 - g28;
                    if (!rdx12) {
                        return al11;
                    }
                } else {
                    addr_ae0e_7:
                    fun_38f0("! (co->reflink_mode == REFLINK_ALWAYS && co->sparse_mode != SPARSE_AUTO)", "src/copy.c", 0xc13, "valid_options");
                    goto addr_ae2d_4;
                }
            }
            fun_3880();
        }
        fun_38f0("VALID_SPARSE_MODE (co->sparse_mode)", "src/copy.c", 0xc10, "valid_options");
    }
    fun_38f0("VALID_BACKUP_TYPE (co->backup_type)", "src/copy.c", 0xc0f, "valid_options");
}

int64_t fun_ae93() {
    void** r12d1;
    void** eax2;
    int64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    r12d1 = mask_0;
    if (r12d1 == 0xffffffff) {
        eax2 = fun_3a90();
        mask_0 = eax2;
        fun_3a90();
        *reinterpret_cast<void***>(&rax3) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    } else {
        *reinterpret_cast<void***>(&rax4) = r12d1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        return rax4;
    }
}

uint64_t fun_aed3(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

struct s25 {
    int64_t f0;
    int64_t f8;
};

struct s26 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_aee3(struct s25* rdi, struct s26* rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (rdi->f0 == rsi->f0) {
        rax3 = rsi->f8;
        *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f8 == rax3);
        return rax3;
    } else {
        return 0;
    }
}

struct s27 {
    signed char[16] pad16;
    void** f10;
};

void fun_af03(struct s27* rdi) {
    void** rdi2;

    __asm__("cli ");
    rdi2 = rdi->f10;
    fun_36a0(rdi2, rdi2);
    goto fun_36a0;
}

void** src_to_dest = reinterpret_cast<void**>(0);

void fun_af23(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    void** rdi4;
    void** rax5;
    void** rdi6;
    int64_t rax7;

    __asm__("cli ");
    rax3 = g28;
    rdi4 = src_to_dest;
    rax5 = hash_remove(rdi4, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32);
    if (rax5) {
        rdi6 = *reinterpret_cast<void***>(rax5 + 16);
        fun_36a0(rdi6, rdi6);
        fun_36a0(rax5, rax5);
    }
    rax7 = rax3 - g28;
    if (rax7) {
        fun_3880();
    } else {
        return;
    }
}

void** fun_af93(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8) {
    int64_t rax6;
    void** rdi7;
    void** rax8;
    int64_t rdx9;

    __asm__("cli ");
    rax6 = g28;
    rdi7 = src_to_dest;
    rax8 = hash_lookup(rdi7, reinterpret_cast<int64_t>(__zero_stack_offset()) - 40, rdx, rcx, r8);
    if (rax8) {
        rax8 = *reinterpret_cast<void***>(rax8 + 16);
    }
    rdx9 = rax6 - g28;
    if (rdx9) {
        fun_3880();
    } else {
        return rax8;
    }
}

void** xmalloc(int64_t rdi);

void** xstrdup(void** rdi, void** rsi);

void** fun_aff3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    void** rax7;
    void** rdi8;
    void** rax9;
    void** rax10;
    void** rdi11;

    __asm__("cli ");
    rax6 = xmalloc(24);
    rax7 = xstrdup(rdi, rsi);
    rdi8 = src_to_dest;
    *reinterpret_cast<void***>(rax6) = rsi;
    *reinterpret_cast<void***>(rax6 + 16) = rax7;
    *reinterpret_cast<void***>(rax6 + 8) = rdx;
    rax9 = hash_insert(rdi8, rax6, rdx, rcx, r8);
    if (!rax9) {
        xalloc_die();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = 0;
        *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
        if (rax6 != rax9) {
            rdi11 = *reinterpret_cast<void***>(rax6 + 16);
            fun_36a0(rdi11, rdi11);
            fun_36a0(rax6, rax6);
            rax10 = *reinterpret_cast<void***>(rax9 + 16);
        }
        return rax10;
    }
}

void fun_b073() {
    void** rax1;

    __asm__("cli ");
    rax1 = hash_initialize(0x67);
    src_to_dest = rax1;
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_3b60();

void fun_b0b3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_3b60;
}

void** fun_3ce0();

void fun_b133(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_3ce0;
}

int32_t try_tempname_len(void** rdi, ...);

int64_t fun_b153(int32_t edi, int64_t rsi, int32_t edx, void** rcx, int32_t r8d, int32_t r9d, void** a7) {
    int32_t r15d8;
    void* rsp9;
    int64_t rdx10;
    void** rdx11;
    void** eax12;
    void** eax13;
    void** r12d14;
    int64_t rax15;
    int64_t rax16;
    void** rax17;
    void** rax18;
    void** rax19;
    void** rax20;
    int32_t eax21;
    int64_t rdi22;
    int32_t eax23;
    void** rax24;
    int64_t rdi25;
    void** rax26;

    __asm__("cli ");
    r15d8 = r9d;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148);
    rdx10 = g28;
    *reinterpret_cast<int32_t*>(&rdx11) = 0;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    eax12 = a7;
    if (reinterpret_cast<signed char>(eax12) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<int32_t*>(&rdx11) = edx;
        *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
        eax13 = fun_3b60();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        r12d14 = eax13;
        if (!eax13) {
            addr_b223_3:
            rax15 = rdx10 - g28;
            if (rax15) {
                fun_3880();
            } else {
                *reinterpret_cast<void***>(&rax16) = r12d14;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                return rax16;
            }
        } else {
            rax17 = fun_3700();
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            eax12 = *reinterpret_cast<void***>(rax17);
        }
    }
    if (*reinterpret_cast<signed char*>(&r15d8) != 1 || !reinterpret_cast<int1_t>(eax12 == 17)) {
        r12d14 = eax12;
        goto addr_b223_3;
    } else {
        rax18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 48);
        rax19 = samedir_template(rcx, rax18, rdx11, rcx);
        if (!rax19) {
            rax20 = fun_3700();
            r12d14 = *reinterpret_cast<void***>(rax20);
            goto addr_b223_3;
        } else {
            eax21 = try_tempname_len(rax19);
            if (!eax21) {
                *reinterpret_cast<int32_t*>(&rdi22) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
                eax23 = fun_3c60(rdi22, rax19, rdi22, rax19);
                r12d14 = reinterpret_cast<void**>(0xffffffff);
                if (eax23) {
                    rax24 = fun_3700();
                    r12d14 = *reinterpret_cast<void***>(rax24);
                }
                *reinterpret_cast<int32_t*>(&rdi25) = edx;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
                fun_3750(rdi25, rax19, rdi25, rax19);
            } else {
                rax26 = fun_3700();
                r12d14 = *reinterpret_cast<void***>(rax26);
            }
            if (rax19 != rax18) {
                fun_36a0(rax19, rax19);
                goto addr_b223_3;
            }
        }
    }
}

int64_t fun_b2c3(int64_t rdi, int32_t esi, void** rdx, void** rcx, void** r8d) {
    int32_t ebx6;
    void* rsp7;
    int64_t rax8;
    void** eax9;
    void** r12d10;
    int64_t rax11;
    int64_t rax12;
    void** rax13;
    void** rbx14;
    void** rax15;
    void** rax16;
    int32_t eax17;
    int64_t rdi18;
    int32_t eax19;
    void** rax20;
    int64_t rdi21;
    void** rax22;

    __asm__("cli ");
    ebx6 = *reinterpret_cast<int32_t*>(&rcx);
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128);
    rax8 = g28;
    if (reinterpret_cast<signed char>(r8d) < reinterpret_cast<signed char>(0)) {
        eax9 = fun_3ce0();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        r12d10 = eax9;
        if (!eax9) {
            addr_b363_3:
            rax11 = rax8 - g28;
            if (rax11) {
                fun_3880();
            } else {
                *reinterpret_cast<void***>(&rax12) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                return rax12;
            }
        } else {
            rax13 = fun_3700();
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            r8d = *reinterpret_cast<void***>(rax13);
        }
    }
    if (*reinterpret_cast<signed char*>(&ebx6) != 1 || !reinterpret_cast<int1_t>(r8d == 17)) {
        r12d10 = r8d;
        goto addr_b363_3;
    } else {
        rbx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 16);
        rax15 = samedir_template(rdx, rbx14, rdx, rcx);
        if (!rax15) {
            rax16 = fun_3700();
            r12d10 = *reinterpret_cast<void***>(rax16);
            goto addr_b363_3;
        } else {
            eax17 = try_tempname_len(rax15, rax15);
            if (!eax17) {
                *reinterpret_cast<int32_t*>(&rdi18) = esi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0;
                eax19 = fun_3c60(rdi18, rax15, rdi18, rax15);
                r12d10 = reinterpret_cast<void**>(0xffffffff);
                if (eax19) {
                    rax20 = fun_3700();
                    *reinterpret_cast<int32_t*>(&rdi21) = esi;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0;
                    r12d10 = *reinterpret_cast<void***>(rax20);
                    fun_3750(rdi21, rax15, rdi21, rax15);
                }
            } else {
                rax22 = fun_3700();
                r12d10 = *reinterpret_cast<void***>(rax22);
            }
            if (rax15 != rbx14) {
                fun_36a0(rax15, rax15);
                goto addr_b363_3;
            }
        }
    }
}

int32_t qcopy_acl();

void** quote(void** rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_b403(void** rdi, void** rsi, void** rdx, void** rcx) {
    int32_t eax5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    eax5 = qcopy_acl();
    if (eax5 == -2) {
        quote(rdi, rsi, rdx, rcx);
        fun_3700();
        fun_3bd0();
        *reinterpret_cast<int32_t*>(&rax6) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    } else {
        if (eax5 == -1) {
            quote(rdx, rsi, rdx, rcx);
            fun_3840();
            fun_3700();
            fun_3bd0();
        }
        *reinterpret_cast<int32_t*>(&rax7) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    }
}

int64_t fun_b4b3(void** rdi, void** rsi, void** rdx, void** rcx) {
    int32_t eax5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    eax5 = qset_acl(rdi, rsi);
    if (eax5) {
        quote(rdi, rsi, rdx, rcx);
        fun_3840();
        fun_3700();
        fun_3bd0();
        *reinterpret_cast<int32_t*>(&rax6) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    } else {
        *reinterpret_cast<int32_t*>(&rax7) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    }
}

void** fun_37a0(int64_t rdi, void** rsi, void** rdx);

void** fun_b533(int64_t rdi, void** rsi) {
    int64_t rbp3;
    void** rbx4;
    int64_t rax5;
    int64_t v6;
    int1_t zf7;
    unsigned char r14b8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t rax12;
    void** rdi13;
    void** rdx14;
    void** rax15;
    void** r13_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** rcx20;
    void** rax21;

    __asm__("cli ");
    rbp3 = rdi;
    *reinterpret_cast<int32_t*>(&rbx4) = 0x80;
    *reinterpret_cast<int32_t*>(&rbx4 + 4) = 0;
    rax5 = g28;
    v6 = rax5;
    zf7 = rsi == 0;
    r14b8 = reinterpret_cast<uint1_t>(!zf7);
    if (!zf7 && (rbx4 = rsi + 1, reinterpret_cast<unsigned char>(rsi) >= reinterpret_cast<unsigned char>(0x401))) {
        rbx4 = reinterpret_cast<void**>(0x401);
    }
    v9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 + 16);
    goto addr_b598_4;
    addr_b680_5:
    rax10 = fun_3700();
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(12);
    addr_b62b_6:
    rax12 = v6 - g28;
    if (rax12) {
        fun_3880();
    } else {
        return r15_11;
    }
    addr_b620_9:
    rdi13 = r15_11;
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    fun_36a0(rdi13, rdi13);
    goto addr_b62b_6;
    while (rax15 = fun_3ae0(rbx4, rsi, rdx14), r13_16 = rax15, !!rax15) {
        r15_11 = rax15;
        do {
            rdx14 = rbx4;
            rsi = r13_16;
            rax17 = fun_37a0(rbp3, rsi, rdx14);
            if (reinterpret_cast<signed char>(rax17) < reinterpret_cast<signed char>(0)) 
                goto addr_b620_9;
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(rax17)) 
                goto addr_b658_14;
            fun_36a0(r15_11, r15_11);
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(0x3fffffffffffffff)) {
                if (rbx4 == 0x7fffffffffffffff) 
                    goto addr_b680_5;
                rbx4 = reinterpret_cast<void**>(0x7fffffffffffffff);
                addr_b598_4:
                if (!reinterpret_cast<int1_t>(rbx4 == 0x80)) 
                    break;
            } else {
                rbx4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx4) + reinterpret_cast<unsigned char>(rbx4));
                if (rbx4 != 0x80) 
                    break;
            }
            r13_16 = v9;
            *reinterpret_cast<int32_t*>(&r15_11) = 0;
            *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        } while (!r14b8);
    }
    goto addr_b680_5;
    addr_b658_14:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_16) + reinterpret_cast<unsigned char>(rax17)) = 0;
    r12_18 = rax17 + 1;
    if (!r15_11) {
        rax19 = fun_3ae0(r12_18, rsi, rdx14);
        r15_11 = rax19;
        if (rax19) {
            fun_3ab0(rax19, r13_16, r12_18);
            goto addr_b62b_6;
        }
    } else {
        if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(r12_18)) {
            rax21 = fun_3b50(r15_11, r12_18, rdx14, rcx20);
            if (rax21) {
                r15_11 = rax21;
            }
            goto addr_b62b_6;
        }
    }
}

void** fun_b6c3(int32_t edi, void** rsi, void** rdx) {
    void** r13_4;
    void** rbp5;
    int32_t ebx6;
    int64_t rax7;
    int64_t v8;
    int1_t zf9;
    unsigned char r12b10;
    void** rax11;
    void** v12;
    void** rdi13;
    void** r15_14;
    int64_t rax15;
    void** rax16;
    void** rax17;
    void** r14_18;
    int64_t rdi19;
    void** rax20;
    void** r12_21;
    void** rax22;
    void** rax23;
    void** rax24;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_4) = 0x80;
    *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
    rbp5 = rsi;
    ebx6 = edi;
    rax7 = g28;
    v8 = rax7;
    zf9 = rdx == 0;
    r12b10 = reinterpret_cast<uint1_t>(!zf9);
    if (!zf9) {
        *reinterpret_cast<int32_t*>(&rax11) = 0x401;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rdx) < reinterpret_cast<unsigned char>(0x401)) {
            rax11 = rdx + 1;
        }
        r13_4 = rax11;
    }
    v12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 + 16);
    goto addr_b720_6;
    addr_b7b0_7:
    rdi13 = r15_14;
    *reinterpret_cast<int32_t*>(&r15_14) = 0;
    *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
    fun_36a0(rdi13, rdi13);
    addr_b7bb_8:
    rax15 = v8 - g28;
    if (rax15) {
        fun_3880();
    } else {
        return r15_14;
    }
    addr_b810_11:
    rax16 = fun_3700();
    *reinterpret_cast<int32_t*>(&r15_14) = 0;
    *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
    *reinterpret_cast<void***>(rax16) = reinterpret_cast<void**>(12);
    goto addr_b7bb_8;
    while (rax17 = fun_3ae0(r13_4, rsi, rdx, r13_4, rsi, rdx), r14_18 = rax17, !!rax17) {
        r15_14 = rax17;
        do {
            rdx = r14_18;
            rsi = rbp5;
            *reinterpret_cast<int32_t*>(&rdi19) = ebx6;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
            rax20 = fun_3a20(rdi19, rsi, rdx, r13_4);
            if (reinterpret_cast<signed char>(rax20) < reinterpret_cast<signed char>(0)) 
                goto addr_b7b0_7;
            if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(rax20)) 
                goto addr_b7e8_16;
            fun_36a0(r15_14, r15_14);
            if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(0x3fffffffffffffff)) {
                if (r13_4 == 0x7fffffffffffffff) 
                    goto addr_b810_11;
                r13_4 = reinterpret_cast<void**>(0x7fffffffffffffff);
                addr_b720_6:
                if (!reinterpret_cast<int1_t>(r13_4 == 0x80)) 
                    break;
            } else {
                r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_4) + reinterpret_cast<unsigned char>(r13_4));
                if (r13_4 != 0x80) 
                    break;
            }
            r14_18 = v12;
            *reinterpret_cast<int32_t*>(&r15_14) = 0;
            *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
        } while (!r12b10);
    }
    goto addr_b843_22;
    addr_b7e8_16:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_18) + reinterpret_cast<unsigned char>(rax20)) = 0;
    r12_21 = rax20 + 1;
    if (!r15_14) {
        rax22 = fun_3ae0(r12_21, rsi, rdx, r12_21, rsi, rdx);
        if (!rax22) {
            addr_b843_22:
            *reinterpret_cast<int32_t*>(&r15_14) = 0;
            *reinterpret_cast<int32_t*>(&r15_14 + 4) = 0;
            goto addr_b7bb_8;
        } else {
            rax23 = fun_3ab0(rax22, r14_18, r12_21, rax22, r14_18, r12_21);
            r15_14 = rax23;
            goto addr_b7bb_8;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(r12_21)) {
            rax24 = fun_3b50(r15_14, r12_21, rdx, r13_4);
            if (rax24) {
                r15_14 = rax24;
            }
            goto addr_b7bb_8;
        }
    }
}

void** fun_36c0(int64_t rdi);

void fun_b853(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbx5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rbx5 = rdi;
    if (!rdi) {
        rax6 = fun_36c0("SIMPLE_BACKUP_SUFFIX");
        rbx5 = rax6;
        if (!rax6) {
            simple_backup_suffix = reinterpret_cast<void**>("~");
            return;
        }
    }
    if (*reinterpret_cast<void***>(rbx5) && (rax7 = last_component(rbx5, rsi, rdx, rcx), rbx5 == rax7)) {
        simple_backup_suffix = rbx5;
        return;
    }
}

void** base_len(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_3960(void** rdi, void** rsi, void** rdx, void** rcx);

void* fun_37f0(void** rdi, int64_t rsi);

void* fun_3a50();

struct s28 {
    int16_t f0;
    signed char f2;
};

void** fun_b8b3(int32_t edi, void** rsi, void** rdx, void** rcx) {
    int32_t v5;
    void** v6;
    int32_t v7;
    unsigned char v8;
    int64_t rax9;
    int64_t v10;
    void** rax11;
    void** v12;
    void** rax13;
    void* rsp14;
    void* r14_15;
    void** rdi16;
    void** v17;
    void** v18;
    void** rax19;
    void** rax20;
    void** rax21;
    void** rax22;
    void** v23;
    void** rax24;
    void** rax25;
    void** v26;
    void** rax27;
    void* rsp28;
    void** r12_29;
    int64_t rax30;
    void** v31;
    void** rbp32;
    int32_t v33;
    void** rbx34;
    void** rdx35;
    void** rsi36;
    void* rsp37;
    void** rdi38;
    uint32_t r15d39;
    void** r8_40;
    uint16_t* r15_41;
    int64_t rdi42;
    uint32_t r13d43;
    void** rax44;
    void* rsp45;
    void** rax46;
    unsigned char al47;
    uint32_t eax48;
    uint32_t v49;
    void** rdi50;
    void** eax51;
    void** rax52;
    void** rax53;
    void** rcx54;
    void** rdx55;
    void** rax56;
    void** rax57;
    void* rsp58;
    uint32_t r8d59;
    void* rax60;
    uint32_t r8d61;
    void* rax62;
    void** rax63;
    void** v64;
    void** v65;
    struct s0* rax66;
    void** r13_67;
    void** rax68;
    void** rdi69;
    void** r15_70;
    uint32_t eax71;
    void** r13_72;
    int64_t rax73;
    uint32_t eax74;
    uint32_t eax75;
    void** r15_76;
    uint32_t eax77;
    signed char* r9_78;
    uint32_t eax79;
    signed char* r9_80;
    void** rdx81;
    void** rax82;
    void** rax83;
    void** r8_84;
    uint64_t rsi85;
    void** rsi86;
    void** rax87;
    struct s28* rax88;
    void** rax89;
    void* rsp90;
    void* rdi91;
    uint32_t edx92;
    unsigned char* rax93;
    void** rax94;
    void** r15d95;
    void** rdx96;
    void** rdi97;
    void** rdi98;
    void** rax99;

    __asm__("cli ");
    v5 = edi;
    v6 = rsi;
    v7 = *reinterpret_cast<int32_t*>(&rdx);
    v8 = *reinterpret_cast<unsigned char*>(&rcx);
    rax9 = g28;
    v10 = rax9;
    rax11 = last_component(rsi, rsi, rdx, rcx);
    v12 = rax11;
    rax13 = base_len(rax11, rsi, rdx, rcx);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8);
    r14_15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax11) - reinterpret_cast<unsigned char>(rsi));
    rdi16 = simple_backup_suffix;
    v17 = rax13;
    v18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(r14_15));
    if (!rdi16) {
        rax19 = fun_36c0("SIMPLE_BACKUP_SUFFIX");
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
        rdi16 = reinterpret_cast<void**>("~");
        if (rax19 && (*reinterpret_cast<void***>(rax19) && (rax20 = last_component(rax19, rsi, rdx, rcx), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8), rdi16 = rax20, rax19 != rax20))) {
            rdi16 = reinterpret_cast<void**>("~");
        }
        simple_backup_suffix = rdi16;
    }
    rax21 = fun_3860(rdi16);
    rax22 = rax21 + 1;
    v23 = rax22;
    *reinterpret_cast<int32_t*>(&rax24) = 9;
    *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
    if (reinterpret_cast<signed char>(rax22) >= reinterpret_cast<signed char>(9)) {
        rax24 = rax22;
    }
    rax25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(rax24) + 1);
    v26 = rax25;
    rax27 = fun_3ae0(rax25, v18, rdx);
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8);
    r12_29 = rax27;
    if (!rax27) {
        addr_bc20_8:
        rax30 = v10 - g28;
        if (rax30) {
            fun_3880();
        } else {
            return r12_29;
        }
    } else {
        v31 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rbp32) = 0;
        *reinterpret_cast<int32_t*>(&rbp32 + 4) = 0;
        v33 = v5;
        rbx34 = v17 + 4;
        do {
            rdx35 = v18;
            rsi36 = v6;
            fun_3ab0(r12_29, rsi36, rdx35, r12_29, rsi36, rdx35);
            rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            if (v7 == 1) {
                rsi36 = simple_backup_suffix;
                rdi38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<unsigned char>(v18));
                fun_3ab0(rdi38, rsi36, v23, rdi38, rsi36, v23);
                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                if (!v8) 
                    break;
                rsi36 = v6;
                r15d39 = v8;
                rcx = r12_29;
                *reinterpret_cast<uint32_t*>(&r8_40) = 0;
            } else {
                if (!rbp32) {
                    r15_41 = reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                    *reinterpret_cast<int32_t*>(&rdi42) = v5;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi42) + 4) = 0;
                    r13d43 = *r15_41;
                    *r15_41 = 46;
                    rcx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp37) + 0x84);
                    rsi36 = r12_29;
                    rax44 = opendirat(rdi42, rsi36);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                    rbp32 = rax44;
                    rdx35 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r15_41) + reinterpret_cast<unsigned char>(v17));
                    if (!rbp32) {
                        rax46 = fun_3700();
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rdx35 = rdx35;
                        *r15_41 = *reinterpret_cast<uint16_t*>(&r13d43);
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rax46) == 12);
                        *reinterpret_cast<void***>(rdx35) = reinterpret_cast<void**>(0x7e317e2e);
                        *reinterpret_cast<signed char*>(rdx35 + 4) = 0;
                        eax48 = al47 + 2;
                        v49 = eax48;
                        if (eax48 == 2) {
                            addr_bce0_18:
                            if (v7 == 2) {
                                rdx35 = v23;
                                rsi36 = simple_backup_suffix;
                                rdi50 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<unsigned char>(v18));
                                fun_3ab0(rdi50, rsi36, rdx35, rdi50, rsi36, rdx35);
                                rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                                v7 = 1;
                                goto addr_bceb_20;
                            }
                        } else {
                            if (v49 == 3) 
                                goto addr_be5c_22;
                            goto addr_bb8b_24;
                        }
                    } else {
                        *r15_41 = *reinterpret_cast<uint16_t*>(&r13d43);
                        *reinterpret_cast<void***>(rdx35) = reinterpret_cast<void**>(0x7e317e2e);
                        *reinterpret_cast<signed char*>(rdx35 + 4) = 0;
                        goto addr_b9b1_26;
                    }
                } else {
                    fun_3960(rbp32, rsi36, rdx35, rcx);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
                    goto addr_b9b1_26;
                }
            }
            addr_bbb6_28:
            eax51 = renameatu();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            if (!eax51) 
                break; else 
                continue;
            addr_bceb_20:
            r15d39 = 1;
            rax52 = last_component(r12_29, rsi36, rdx35, rcx);
            rax53 = base_len(rax52, rsi36, rdx35, rcx);
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8);
            rcx54 = rax53;
            if (reinterpret_cast<signed char>(rax53) > reinterpret_cast<signed char>(14)) {
                rdx55 = rax52;
                if (v31) {
                    addr_be15_30:
                    if (reinterpret_cast<signed char>(rcx54) <= reinterpret_cast<signed char>(v31)) {
                        r15d39 = 1;
                        goto addr_bd13_32;
                    } else {
                        addr_be20_33:
                        rsi36 = v31;
                        rax56 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(r12_29)) - reinterpret_cast<unsigned char>(rdx55));
                        if (reinterpret_cast<signed char>(rax56) >= reinterpret_cast<signed char>(rsi36)) {
                            rax56 = rsi36 + 0xffffffffffffffff;
                        }
                    }
                } else {
                    rax57 = fun_3700();
                    rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    if (v33 < 0) {
                        r8d59 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(rax52));
                        *reinterpret_cast<void***>(rax52) = reinterpret_cast<void**>(46);
                        *reinterpret_cast<void***>(rax57) = reinterpret_cast<void**>(0);
                        rax60 = fun_37f0(r12_29, 3);
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
                        rdx55 = rax52;
                        r8d61 = r8d59;
                        rcx54 = rax53;
                        v31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax60) - reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax60) < static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax57)) < reinterpret_cast<unsigned char>(1)))));
                        *reinterpret_cast<void***>(rdx55) = *reinterpret_cast<void***>(&r8d61);
                    } else {
                        *reinterpret_cast<void***>(rax57) = reinterpret_cast<void**>(0);
                        rax62 = fun_3a50();
                        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8);
                        rcx54 = rax53;
                        rdx55 = rax52;
                        v31 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax62) - reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax62) < static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax57)) < reinterpret_cast<unsigned char>(1)))));
                    }
                    rsi36 = v31;
                    if (reinterpret_cast<signed char>(rsi36) < reinterpret_cast<signed char>(0)) 
                        goto addr_be77_39; else 
                        goto addr_be15_30;
                }
            } else {
                addr_bd13_32:
                if (!v8) 
                    break; else 
                    goto addr_bd1e_40;
            }
            r15d39 = 0;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx55) + reinterpret_cast<unsigned char>(rax56)) = 0x7e;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx55) + reinterpret_cast<unsigned char>(rax56) + 1) = 0;
            goto addr_bd13_32;
            addr_be77_39:
            *reinterpret_cast<int32_t*>(&rax63) = 14;
            *reinterpret_cast<int32_t*>(&rax63 + 4) = 0;
            if (rsi36 == 0xffffffffffffffff) {
                rax63 = rsi36;
            }
            v31 = rax63;
            goto addr_be20_33;
            addr_bd1e_40:
            if (v7 != 1) {
                addr_bba7_44:
                rsi36 = v12;
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                *reinterpret_cast<uint32_t*>(&r8_40) = 1;
                goto addr_bbb6_28;
            } else {
                rsi36 = v6;
                rcx = r12_29;
                *reinterpret_cast<uint32_t*>(&r8_40) = 0;
                goto addr_bbb6_28;
            }
            addr_bb8b_24:
            if (v49 != 1) {
                if (!v8) 
                    break;
                r15d39 = v8;
                goto addr_bba7_44;
            }
            addr_b9b1_26:
            v49 = 2;
            v64 = reinterpret_cast<void**>(1);
            v65 = v26;
            addr_b9d0_48:
            while (rax66 = fun_3ad0(rbp32, rsi36, rbp32, rsi36), rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8), !!rax66) {
                do {
                    r13_67 = reinterpret_cast<void**>(&rax66->f13);
                    rax68 = fun_3860(r13_67, r13_67);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    if (reinterpret_cast<unsigned char>(rax68) < reinterpret_cast<unsigned char>(rbx34)) 
                        goto addr_b9d0_48;
                    rdi69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r14_15));
                    rsi36 = r13_67;
                    r15_70 = v17 + 2;
                    rdx35 = r15_70;
                    eax71 = fun_39c0(rdi69, rsi36, rdx35, rdi69, rsi36, rdx35);
                    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                    if (eax71) 
                        goto addr_b9d0_48;
                    r13_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_67) + reinterpret_cast<unsigned char>(r15_70));
                    *reinterpret_cast<uint32_t*>(&rax73) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_72));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax73) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rdx35) = static_cast<uint32_t>(rax73 - 49);
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    if (*reinterpret_cast<unsigned char*>(&rdx35) > 8) 
                        goto addr_b9d0_48;
                    eax74 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r13_72 + 1))));
                    *reinterpret_cast<unsigned char*>(&r8_40) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax73) == 57);
                    *reinterpret_cast<uint32_t*>(&rdx35) = eax74;
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    eax75 = eax74 - 48;
                    if (eax75 > 9) {
                        *reinterpret_cast<int32_t*>(&rcx) = 1;
                        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r15_76) = 1;
                        *reinterpret_cast<int32_t*>(&r15_76 + 4) = 0;
                    } else {
                        *reinterpret_cast<int32_t*>(&r15_76) = 1;
                        *reinterpret_cast<int32_t*>(&r15_76 + 4) = 0;
                        do {
                            *reinterpret_cast<unsigned char*>(&eax75) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rdx35) == 57);
                            ++r15_76;
                            *reinterpret_cast<uint32_t*>(&r8_40) = *reinterpret_cast<uint32_t*>(&r8_40) & eax75;
                            eax77 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_72) + reinterpret_cast<unsigned char>(r15_76))));
                            rcx = r15_76;
                            *reinterpret_cast<uint32_t*>(&rdx35) = eax77;
                            *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                            eax75 = eax77 - 48;
                        } while (eax75 <= 9);
                    }
                    if (*reinterpret_cast<unsigned char*>(&rdx35) != 0x7e) 
                        goto addr_b9d0_48;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_72) + reinterpret_cast<unsigned char>(rcx) + 1)) 
                        goto addr_b9d0_48;
                    if (reinterpret_cast<signed char>(v64) >= reinterpret_cast<signed char>(r15_76)) {
                        if (v64 != r15_76) 
                            goto addr_b9d0_48;
                        rdx35 = rcx;
                        rsi36 = r13_72;
                        r9_78 = reinterpret_cast<signed char*>(v18 + 2);
                        eax79 = fun_39c0(reinterpret_cast<unsigned char>(r12_29) + reinterpret_cast<uint64_t>(r9_78), rsi36, rdx35);
                        rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rcx = rcx;
                        r9_80 = r9_78;
                        *reinterpret_cast<uint32_t*>(&r8_40) = *reinterpret_cast<unsigned char*>(&r8_40);
                        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax79) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax79 == 0))) 
                            break;
                    } else {
                        r9_80 = reinterpret_cast<signed char*>(v18 + 2);
                    }
                    *reinterpret_cast<uint32_t*>(&rdx81) = *reinterpret_cast<unsigned char*>(&r8_40);
                    *reinterpret_cast<int32_t*>(&rdx81 + 4) = 0;
                    rax82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx81) + reinterpret_cast<unsigned char>(r15_76));
                    v49 = *reinterpret_cast<unsigned char*>(&r8_40);
                    v64 = rax82;
                    rax83 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax82) + reinterpret_cast<uint64_t>(r9_80) + 2);
                    if (reinterpret_cast<signed char>(rax83) <= reinterpret_cast<signed char>(v65)) {
                        r8_84 = r12_29;
                    } else {
                        if (__intrinsic()) {
                            v65 = rax83;
                        } else {
                            v65 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rax83) >> 1) + reinterpret_cast<unsigned char>(rax83));
                        }
                        *reinterpret_cast<int32_t*>(&rsi85) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi85) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rsi85) = reinterpret_cast<uint1_t>(v65 == 0);
                        rsi86 = reinterpret_cast<void**>(rsi85 | reinterpret_cast<unsigned char>(v65));
                        rax87 = fun_3b50(r12_29, rsi86, rdx81, rcx);
                        rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                        rcx = rcx;
                        rdx81 = rdx81;
                        r8_84 = rax87;
                        if (!rax87) 
                            goto addr_be54_68;
                    }
                    rsi36 = r13_72;
                    rax88 = reinterpret_cast<struct s28*>(reinterpret_cast<unsigned char>(v18) + reinterpret_cast<unsigned char>(r8_84));
                    rax88->f0 = 0x7e2e;
                    rax88->f2 = 48;
                    rax89 = fun_3ab0(reinterpret_cast<uint64_t>(rax88) + reinterpret_cast<unsigned char>(rdx81) + 2, rsi36, r15_76 + 2);
                    rsp90 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
                    rcx = rcx;
                    r8_40 = r8_84;
                    rdi91 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax89) + reinterpret_cast<unsigned char>(rcx));
                    edx92 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi91) + 0xffffffffffffffff);
                    rax93 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi91) + 0xffffffffffffffff);
                    if (*reinterpret_cast<signed char*>(&edx92) == 57) {
                        do {
                            *rax93 = 48;
                            edx92 = *(rax93 - 1);
                            --rax93;
                        } while (*reinterpret_cast<signed char*>(&edx92) == 57);
                    }
                    *reinterpret_cast<uint32_t*>(&rdx35) = edx92 + 1;
                    *reinterpret_cast<int32_t*>(&rdx35 + 4) = 0;
                    r12_29 = r8_40;
                    *rax93 = *reinterpret_cast<unsigned char*>(&rdx35);
                    rax66 = fun_3ad0(rbp32, rsi36);
                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp90) - 8 + 8);
                } while (rax66);
                goto addr_bb79_73;
            }
            addr_bb80_75:
            if (v49 == 2) 
                goto addr_bce0_18; else 
                goto addr_bb8b_24;
            addr_bb79_73:
            goto addr_bb80_75;
            rax94 = fun_3700();
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
        } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax94) == 17) && *reinterpret_cast<signed char*>(&r15d39) == 1);
        goto addr_bbe2_77;
    }
    if (rbp32) {
        fun_3980(rbp32, rsi36, rbp32, rsi36);
        goto addr_bc20_8;
    }
    addr_bbe2_77:
    r15d95 = *reinterpret_cast<void***>(rax94);
    rdx96 = rax94;
    if (rbp32) {
        fun_3980(rbp32, rsi36, rbp32, rsi36);
        rdx96 = rax94;
    }
    rdi97 = r12_29;
    *reinterpret_cast<int32_t*>(&r12_29) = 0;
    *reinterpret_cast<int32_t*>(&r12_29 + 4) = 0;
    fun_36a0(rdi97, rdi97);
    *reinterpret_cast<void***>(rdx96) = r15d95;
    goto addr_bc20_8;
    addr_be5c_22:
    rdi98 = r12_29;
    *reinterpret_cast<int32_t*>(&r12_29) = 0;
    *reinterpret_cast<int32_t*>(&r12_29 + 4) = 0;
    fun_36a0(rdi98, rdi98);
    rax99 = fun_3700();
    *reinterpret_cast<void***>(rax99) = reinterpret_cast<void**>(12);
    goto addr_bc20_8;
    addr_be54_68:
    fun_3980(rbp32, rsi86);
    goto addr_be5c_22;
}

int64_t backupfile_internal();

void fun_bfc3() {
    __asm__("cli ");
    goto backupfile_internal;
}

void fun_bfd3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = backupfile_internal();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t __xargmatch_internal(int64_t rdi);

int64_t fun_bff3(int64_t rdi, signed char* rsi) {
    int64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    if (!rsi) {
        return 2;
    } else {
        if (*rsi) {
            rax3 = __xargmatch_internal(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = *reinterpret_cast<int32_t*>(0x17960 + rax3 * 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
            return rax4;
        } else {
            return 2;
        }
    }
}

int64_t fun_c053(int64_t rdi, signed char* rsi) {
    void** rax3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!rsi || !*rsi) {
        rax3 = fun_36c0("VERSION_CONTROL");
        if (!rax3 || !*reinterpret_cast<void***>(rax3)) {
            return 2;
        } else {
            rax4 = __xargmatch_internal("$VERSION_CONTROL");
            *reinterpret_cast<int32_t*>(&rax5) = *reinterpret_cast<int32_t*>(0x17960 + rax4 * 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
            return rax5;
        }
    } else {
        rax6 = __xargmatch_internal(rdi);
        *reinterpret_cast<int32_t*>(&rax7) = *reinterpret_cast<int32_t*>(0x17960 + rax6 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    }
}

struct s29 {
    unsigned char f0;
    unsigned char f1;
};

struct s29* fun_c0f3(struct s29* rdi) {
    uint32_t edx2;
    struct s29* rax3;
    struct s29* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s29*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s29*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s29*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_c153(void** rdi) {
    void** rbx2;
    void** rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_3860(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        --rax3;
    }
    return;
}

uint64_t fun_c183(uint64_t rdi, uint64_t rsi, uint64_t rdx) {
    uint64_t rcx4;
    uint64_t rdi5;
    uint64_t rax6;
    uint64_t r8_7;
    uint64_t rax8;
    uint64_t rdx9;
    uint64_t rax10;
    int64_t rdx11;

    __asm__("cli ");
    rcx4 = rdi;
    rdi5 = rdx;
    if (!rcx4) {
        *reinterpret_cast<int32_t*>(&rcx4) = 0x2000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
        if (rsi) {
            rcx4 = rsi;
            goto addr_c19b_4;
        }
    }
    if (!rsi) {
        addr_c19b_4:
        rax6 = rdi5;
        if (rcx4 <= rdi5) {
            rax6 = rcx4;
        }
    } else {
        r8_7 = rsi;
        rax8 = rcx4;
        while (rdx9 = rax8 % r8_7, !!rdx9) {
            rax8 = r8_7;
            r8_7 = rdx9;
        }
        rax10 = rsi * (rcx4 / r8_7);
        *reinterpret_cast<uint32_t*>(&rdx11) = __intrinsic();
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        if (rax10 > rdi5) 
            goto addr_c19b_4;
        if (rdx11) 
            goto addr_c19b_4; else 
            goto addr_c1e6_12;
    }
    return rax6;
    addr_c1e6_12:
    return rax10;
}

int64_t file_name = 0;

void fun_c203(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

void** stdin = reinterpret_cast<void**>(0);

int64_t freadahead(void** rdi);

int32_t rpl_fseeko(void** rdi);

int32_t rpl_fflush(void** rdi);

int32_t close_stream(void** rdi);

void close_stdout();

int32_t exit_failure = 1;

void** fun_3720(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void** quotearg_colon(int64_t rdi, int64_t rsi, int64_t rdx);

void fun_c213() {
    void** rbp1;
    int64_t rax2;
    int32_t eax3;
    void** rdi4;
    int32_t eax5;
    int32_t eax6;
    void** rax7;
    int64_t r13_8;
    void** r12_9;
    void** rax10;
    int64_t rsi11;
    void** rcx12;
    int64_t rdx13;
    int64_t rdi14;
    void** r8_15;
    void** rax16;
    int32_t eax17;

    __asm__("cli ");
    rbp1 = stdin;
    rax2 = freadahead(rbp1);
    if (rax2) {
        eax3 = rpl_fseeko(rbp1);
        rdi4 = stdin;
        if (eax3 || (eax5 = rpl_fflush(rdi4), rdi4 = stdin, eax5 == 0)) {
            eax6 = close_stream(rdi4);
            if (!eax6) {
                addr_c239_4:
                goto close_stdout;
            } else {
                addr_c26f_5:
                rax7 = fun_3840();
                r13_8 = file_name;
                r12_9 = rax7;
                rax10 = fun_3700();
                if (!r13_8) {
                    while (1) {
                        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rax10);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
                        rcx12 = r12_9;
                        rdx13 = reinterpret_cast<int64_t>("%s");
                        fun_3bd0();
                        close_stdout();
                        addr_c2bf_7:
                        *reinterpret_cast<int32_t*>(&rdi14) = exit_failure;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
                        rax10 = fun_3720(rdi14, rsi11, rdx13, rcx12, r8_15);
                    }
                } else {
                    rax16 = quotearg_colon(r13_8, "error closing file", 5);
                    *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rax10);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
                    r8_15 = r12_9;
                    rcx12 = rax16;
                    rdx13 = reinterpret_cast<int64_t>("%s: %s");
                    fun_3bd0();
                    close_stdout();
                    goto addr_c2bf_7;
                }
            }
        } else {
            close_stream(rdi4);
            goto addr_c26f_5;
        }
    } else {
        eax17 = close_stream(rbp1);
        if (eax17) 
            goto addr_c26f_5; else 
            goto addr_c239_4;
    }
}

int64_t file_name = 0;

void fun_c313(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_c323(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

void fun_c333() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_3700(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_3840();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_c3c3_5;
        rax10 = quotearg_colon(rdi9, "write error", 5);
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_3bd0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_3720(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_c3c3_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_3bd0();
    }
}

int64_t mdir_name();

void fun_c3e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mdir_name();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_c403(void** rdi, void** rsi, void** rdx, void** rcx) {
    void* rbp5;
    void** rbx6;
    void** rax7;
    void* rax8;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp5) + 4) = 0;
    rbx6 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp5) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax7 = last_component(rdi, rsi, rdx, rcx);
    rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(rbx6));
    while (reinterpret_cast<uint64_t>(rax8) > reinterpret_cast<uint64_t>(rbp5) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(rax8) + 0xffffffffffffffff) == 47) {
        rax8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax8) + 0xffffffffffffffff);
    }
    return;
}

void** fun_c443(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbp5;
    void** rbx6;
    void** rax7;
    void** r12_8;
    void* rax9;
    uint32_t ebx10;
    void** rax11;
    void** r8_12;
    void** rax13;
    void** rax14;
    void** rax15;

    __asm__("cli ");
    rbp5 = rdi;
    *reinterpret_cast<int32_t*>(&rbx6) = 0;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx6) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rdi) == 47);
    rax7 = last_component(rdi, rsi, rdx, rcx);
    r12_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(rbp5));
    while (reinterpret_cast<unsigned char>(rbx6) < reinterpret_cast<unsigned char>(r12_8)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(r12_8) + 0xffffffffffffffff) != 47) 
            goto addr_c4c0_4;
        --r12_8;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_8) ^ 1);
    ebx10 = *reinterpret_cast<uint32_t*>(&rax9) & 1;
    rax11 = fun_3ae0(reinterpret_cast<unsigned char>(r12_8) + reinterpret_cast<uint64_t>(rax9) + 1, rsi, rdx);
    if (!rax11) {
        addr_c4ed_7:
        *reinterpret_cast<int32_t*>(&r8_12) = 0;
        *reinterpret_cast<int32_t*>(&r8_12 + 4) = 0;
    } else {
        rax13 = fun_3ab0(rax11, rbp5, r12_8);
        r8_12 = rax13;
        if (!*reinterpret_cast<signed char*>(&ebx10)) {
            *reinterpret_cast<int32_t*>(&r12_8) = 1;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
            goto addr_c4ae_10;
        } else {
            *reinterpret_cast<void***>(rax13) = reinterpret_cast<void**>(46);
            *reinterpret_cast<int32_t*>(&r12_8) = 1;
            *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
            goto addr_c4ae_10;
        }
    }
    addr_c4b3_12:
    return r8_12;
    addr_c4ae_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_12) + reinterpret_cast<unsigned char>(r12_8)) = 0;
    goto addr_c4b3_12;
    addr_c4c0_4:
    rax14 = fun_3ae0(r12_8 + 1, rsi, rdx);
    if (!rax14) 
        goto addr_c4ed_7;
    rax15 = fun_3ab0(rax14, rbp5, r12_8);
    r8_12 = rax15;
    goto addr_c4ae_10;
}

unsigned char fun_c503(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    void** rbx6;
    void** rax7;
    signed char* rbx8;
    int1_t zf9;

    __asm__("cli ");
    rax5 = last_component(rdi, rsi, rdx, rcx);
    rbx6 = rax5;
    if (!*reinterpret_cast<void***>(rax5)) {
        rbx6 = rdi;
    }
    rax7 = base_len(rbx6, rsi, rdx, rcx);
    rbx8 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<unsigned char>(rax7));
    zf9 = *rbx8 == 0;
    *rbx8 = 0;
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!zf9));
}

void fun_c543() {
    __asm__("cli ");
}

int32_t fun_3ac0(void** rdi);

void fun_c553(void** rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_3ac0(rdi);
        goto 0x3990;
    }
}

int32_t fun_3be0();

void fd_safer(int64_t rdi);

void fun_c583() {
    int64_t rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    int64_t rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_3be0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = rax1 - g28;
    if (rdx5) {
        fun_3880();
    } else {
        return;
    }
}

int64_t fun_3c20();

int64_t fun_c603(int32_t edi, int32_t esi, void** rdx, void* rcx, int32_t r8d) {
    int64_t rax6;
    void** rax7;
    void** rax8;
    int64_t rdi9;

    __asm__("cli ");
    if (edi >= 0) {
        rax6 = fun_3c20();
        if (*reinterpret_cast<int32_t*>(&rax6) != -1 || !rdx) {
            addr_c635_3:
            if (*reinterpret_cast<int32_t*>(&rax6) != 1) {
                return rax6;
            }
        } else {
            rax7 = fun_3700();
            if (*reinterpret_cast<void***>(rax7) != 38) {
                return 0xffffffff;
            }
        }
    } else {
        if (!rdx) {
            rax8 = fun_3700();
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(9);
            return 0xffffffff;
        } else {
            *reinterpret_cast<int32_t*>(&rdi9) = esi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
            rax6 = fun_36e0(rdi9, rdx, rcx, rdi9, rdx, rcx);
            goto addr_c635_3;
        }
    }
}

int32_t fun_3b30(void** rdi);

void fun_c6a3(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_3b30(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

void fun_c6f3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    void** rax7;
    void** rax8;

    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        rax6 = xmalloc(24);
        rax7 = xstrdup(rsi, rsi);
        *reinterpret_cast<void***>(rax6) = rax7;
        *reinterpret_cast<void***>(rax6 + 8) = *reinterpret_cast<void***>(rdx + 8);
        *reinterpret_cast<void***>(rax6 + 16) = *reinterpret_cast<void***>(rdx);
        rax8 = hash_insert(rdi, rax6, rdx, rcx, r8);
        if (!rax8) {
            xalloc_die();
        } else {
            if (rax6 == rax8) {
                return;
            }
        }
    }
}

void** fun_c783(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8) {
    int64_t rax6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rax6 = g28;
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
    if (rdi) {
        rax7 = hash_lookup(rdi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 40, rdx, rcx, r8);
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(!!rax7);
    }
    rdx8 = rax6 - g28;
    if (rdx8) {
        fun_3880();
    } else {
        return rax7;
    }
}

struct s30 {
    signed char[24] pad24;
    uint32_t f18;
    signed char[20] pad48;
    int64_t f30;
};

void fun_c7e3(struct s30* rdi) {
    uint32_t eax2;

    __asm__("cli ");
    eax2 = rdi->f18 & 0xf000;
    if (eax2 == 0x8000) {
        if (rdi->f30) {
            goto fun_3840;
        } else {
            goto fun_3840;
        }
    } else {
        if (eax2 == 0x4000) {
            goto fun_3840;
        } else {
            if (eax2 == 0xa000) {
                goto fun_3840;
            } else {
                if (eax2 == 0x6000) {
                    goto fun_3840;
                } else {
                    if (eax2 == 0x2000) {
                        goto fun_3840;
                    } else {
                        if (eax2 == "__libc_start_main") {
                            goto fun_3840;
                        } else {
                            if (eax2 == 0xc000) {
                                goto fun_3840;
                            } else {
                                goto fun_3840;
                            }
                        }
                    }
                }
            }
        }
    }
}

struct s31 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
    signed char f8;
    signed char f9;
    int16_t fa;
};

void fun_c8d3(uint32_t edi, struct s31* rsi) {
    uint32_t eax3;
    int32_t ecx4;
    uint32_t esi5;
    uint32_t ecx6;
    uint32_t ecx7;
    uint32_t ecx8;
    uint32_t ecx9;
    uint32_t ecx10;
    uint32_t ecx11;
    uint32_t ecx12;
    uint32_t ecx13;
    uint32_t ecx14;
    uint32_t ecx15;
    uint32_t ecx16;
    uint32_t ecx17;
    uint32_t ecx18;
    uint32_t ecx19;
    uint32_t ecx20;
    uint32_t ecx21;
    uint32_t ecx22;
    uint32_t ecx23;
    uint32_t ecx24;
    uint32_t eax25;
    uint32_t eax26;

    __asm__("cli ");
    eax3 = edi;
    ecx4 = 45;
    esi5 = edi & 0xf000;
    if (esi5 != 0x8000 && ((ecx4 = 100, esi5 != 0x4000) && ((ecx4 = 98, esi5 != 0x6000) && ((ecx4 = 99, esi5 != 0x2000) && ((ecx4 = 0x6c, esi5 != 0xa000) && ((ecx4 = 0x70, esi5 != "__libc_start_main") && (ecx4 = 0x73, esi5 != 0xc000))))))) {
        ecx4 = 63;
    }
    rsi->f0 = *reinterpret_cast<signed char*>(&ecx4);
    ecx6 = eax3 & 0x100;
    ecx7 = (ecx6 - (ecx6 + reinterpret_cast<uint1_t>(ecx6 < ecx6 + reinterpret_cast<uint1_t>(ecx6 < 1))) & 0xffffffbb) + 0x72;
    rsi->f1 = *reinterpret_cast<signed char*>(&ecx7);
    ecx8 = eax3 & 0x80;
    ecx9 = (ecx8 - (ecx8 + reinterpret_cast<uint1_t>(ecx8 < ecx8 + reinterpret_cast<uint1_t>(ecx8 < 1))) & 0xffffffb6) + 0x77;
    rsi->f2 = *reinterpret_cast<signed char*>(&ecx9);
    ecx10 = eax3 & 64;
    ecx11 = ecx10 - (ecx10 + reinterpret_cast<uint1_t>(ecx10 < ecx10 + reinterpret_cast<uint1_t>(ecx10 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 8)) {
        ecx12 = (ecx11 & 0xffffffb5) + 0x78;
    } else {
        ecx12 = (ecx11 & 0xffffffe0) + 0x73;
    }
    rsi->f3 = *reinterpret_cast<signed char*>(&ecx12);
    ecx13 = eax3 & 32;
    ecx14 = (ecx13 - (ecx13 + reinterpret_cast<uint1_t>(ecx13 < ecx13 + reinterpret_cast<uint1_t>(ecx13 < 1))) & 0xffffffbb) + 0x72;
    rsi->f4 = *reinterpret_cast<signed char*>(&ecx14);
    ecx15 = eax3 & 16;
    ecx16 = (ecx15 - (ecx15 + reinterpret_cast<uint1_t>(ecx15 < ecx15 + reinterpret_cast<uint1_t>(ecx15 < 1))) & 0xffffffb6) + 0x77;
    rsi->f5 = *reinterpret_cast<signed char*>(&ecx16);
    ecx17 = eax3 & 8;
    ecx18 = ecx17 - (ecx17 + reinterpret_cast<uint1_t>(ecx17 < ecx17 + reinterpret_cast<uint1_t>(ecx17 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 4)) {
        ecx19 = (ecx18 & 0xffffffb5) + 0x78;
    } else {
        ecx19 = (ecx18 & 0xffffffe0) + 0x73;
    }
    rsi->f6 = *reinterpret_cast<signed char*>(&ecx19);
    ecx20 = eax3 & 4;
    ecx21 = (ecx20 - (ecx20 + reinterpret_cast<uint1_t>(ecx20 < ecx20 + reinterpret_cast<uint1_t>(ecx20 < 1))) & 0xffffffbb) + 0x72;
    rsi->f7 = *reinterpret_cast<signed char*>(&ecx21);
    ecx22 = eax3 & 2;
    ecx23 = (ecx22 - (ecx22 + reinterpret_cast<uint1_t>(ecx22 < ecx22 + reinterpret_cast<uint1_t>(ecx22 < 1))) & 0xffffffb6) + 0x77;
    rsi->f8 = *reinterpret_cast<signed char*>(&ecx23);
    ecx24 = eax3 & 1;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 2)) {
        eax25 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffb5) + 0x78;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax25);
        rsi->fa = 32;
        return;
    } else {
        eax26 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffe0) + 0x74;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax26);
        rsi->fa = 32;
        return;
    }
}

void fun_ca53(int64_t rdi) {
    __asm__("cli ");
    goto strmode;
}

int64_t mfile_name_concat();

void fun_ca63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mfile_name_concat();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_ca83(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    void** rax6;
    void** r14_7;
    void** rax8;
    void* rbx9;
    uint1_t zf10;
    int32_t eax11;
    unsigned char v12;
    int1_t zf13;
    int32_t eax14;
    void** rax15;
    void** rax16;
    uint32_t ecx17;
    void** rdi18;
    void** rax19;

    __asm__("cli ");
    rax5 = last_component(rdi, rsi, rdx, rcx);
    rax6 = base_len(rax5, rsi, rdx, rcx);
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(rdi)) + reinterpret_cast<unsigned char>(rax6));
    rax8 = fun_3860(rsi);
    if (!rax6) {
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        zf10 = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rsi) == 47);
        eax11 = 46;
        if (!zf10) {
            eax11 = 0;
        }
        *reinterpret_cast<unsigned char*>(&rbx9) = zf10;
        v12 = *reinterpret_cast<unsigned char*>(&eax11);
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(r14_7) + 0xffffffffffffffff) == 47) {
            v12 = 0;
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rbx9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
            zf13 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi) == 47);
            eax14 = 47;
            if (zf13) {
                eax14 = 0;
            }
            *reinterpret_cast<unsigned char*>(&rbx9) = reinterpret_cast<uint1_t>(!zf13);
            v12 = *reinterpret_cast<unsigned char*>(&eax14);
        }
    }
    rax15 = fun_3ae0(reinterpret_cast<unsigned char>(r14_7) + reinterpret_cast<unsigned char>(rax8) + 1 + reinterpret_cast<uint64_t>(rbx9), rsi, rdx);
    if (rax15) {
        rax16 = fun_3bb0(rax15, rdi, r14_7);
        ecx17 = v12;
        rdi18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax16) + reinterpret_cast<uint64_t>(rbx9));
        *reinterpret_cast<void***>(rax16) = *reinterpret_cast<void***>(&ecx17);
        if (rdx) {
            *reinterpret_cast<void***>(rdx) = rdi18;
        }
        rax19 = fun_3bb0(rdi18, rsi, rax8);
        *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    }
    return rax15;
}

struct s32 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    uint64_t f20;
    uint64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[8] pad88;
    int64_t f58;
};

int64_t fun_cb83(struct s32* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax2) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    if (rdi->f28 <= rdi->f20 && (rax2 = rdi->f10 - rdi->f8, !!(rdi->f0 & 0x100))) {
        rax2 = rax2 + (rdi->f58 - rdi->f48);
    }
    return rax2;
}

int64_t fun_cbb3(void** rdi, int64_t rsi, int32_t edx) {
    void** rax4;
    int64_t rax5;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<void***>(rdi + 72)))) {
        fun_3ac0(rdi);
        rax4 = fun_38e0();
        if (rax4 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax5) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<void***>(rdi + 0x90) = rax4;
            *reinterpret_cast<uint32_t*>(&rax5) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
        return rax5;
    }
}

struct s33 {
    int64_t f0;
    int64_t f8;
};

struct s34 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_cc33(struct s33* rdi, struct s34* rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (rdi->f8 == rsi->f8) {
        rax3 = rsi->f0;
        *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f0 == rax3);
        return rax3;
    } else {
        return 0;
    }
}

struct s35 {
    signed char[8] pad8;
    int64_t f8;
};

uint64_t fun_cc63(struct s35* rdi, int64_t rsi) {
    __asm__("cli ");
    return rdi->f8 % reinterpret_cast<uint64_t>(rsi);
}

uint64_t fun_cc83(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_cc93(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

struct s36 {
    signed char[120] pad120;
    uint64_t f78;
};

struct s37 {
    signed char[120] pad120;
    uint64_t f78;
};

int64_t fun_cca3(struct s36** rdi, struct s37** rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>((*rdi)->f78 > (*rsi)->f78);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>((*rdi)->f78 < (*rsi)->f78)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

struct s38 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void i_ring_init(void*** rdi, int64_t rsi);

void** fun_e283(struct s38* rdi, void** esi, void** rdx, void** rcx) {
    void** ebp5;
    void** rax6;
    void** r12_7;
    struct s38* rbx8;
    void** r14_9;
    void** rax10;
    void** eax11;
    void** rdi12;
    void** eax13;
    void** rsi14;
    unsigned char al15;
    unsigned char v16;
    void** rdi17;
    void** r15_18;
    void** v19;
    void** rax20;
    void** rdi21;
    uint32_t eax22;
    void** rax23;
    unsigned char al24;
    void** eax25;
    int64_t rdi26;
    void** eax27;
    void** v28;
    void** r13_29;
    void** rbp30;
    uint32_t eax31;
    signed char v32;
    void** rax33;
    void** rdx34;
    void** rsi35;
    void** rax36;
    void** rax37;
    void** rax38;
    void** r13_39;
    void** rdi40;
    void** rax41;
    void** rax42;
    unsigned char al43;
    struct s38* r15_44;
    void** r13_45;
    void** rax46;

    __asm__("cli ");
    if (reinterpret_cast<unsigned char>(esi) & 0xfffff000 || ((ebp5 = esi, (reinterpret_cast<unsigned char>(esi) & 0x204) == 0x204) || !(*reinterpret_cast<unsigned char*>(&esi) & 18))) {
        rax6 = fun_3700();
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(22);
        goto addr_e49e_3;
    }
    rbx8 = rdi;
    r14_9 = rdx;
    rax10 = fun_39f0(1, 0x80);
    r12_7 = rax10;
    if (!rax10) {
        addr_e49e_3:
        return r12_7;
    } else {
        *reinterpret_cast<void***>(rax10 + 64) = r14_9;
        eax11 = ebp5;
        rdi12 = rbx8->f0;
        *reinterpret_cast<void***>(r12_7 + 44) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<unsigned char*>(&eax11 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax11 + 1) & 0xfd);
        eax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(eax11) | 4);
        if (!(*reinterpret_cast<unsigned char*>(&ebp5) & 2)) {
            eax13 = ebp5;
        }
        *reinterpret_cast<void***>(r12_7 + 72) = eax13;
        if (rdi12) 
            goto addr_e30a_8;
    }
    *reinterpret_cast<int32_t*>(&rsi14) = reinterpret_cast<int32_t>("__libc_start_main");
    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
    addr_e339_10:
    al15 = fts_palloc(r12_7, rsi14, rdx, rcx);
    v16 = al15;
    if (!al15) {
        addr_e4e6_11:
        rdi17 = r12_7;
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        fun_36a0(rdi17, rdi17);
        goto addr_e49e_3;
    } else {
        r15_18 = rbx8->f0;
        if (!r15_18) {
            v19 = reinterpret_cast<void**>(0);
        } else {
            rax20 = fts_alloc(r12_7, 0x178bb, 0);
            v19 = rax20;
            if (!rax20) {
                addr_e4dc_15:
                rdi21 = *reinterpret_cast<void***>(r12_7 + 32);
                fun_36a0(rdi21, rdi21);
                goto addr_e4e6_11;
            } else {
                *reinterpret_cast<void***>(rax20 + 88) = reinterpret_cast<void**>(0xffffffffffffffff);
                r15_18 = rbx8->f0;
            }
        }
    }
    if (r14_9) {
        eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) >> 10 & 1;
        v16 = *reinterpret_cast<unsigned char*>(&eax22);
    }
    if (!r15_18) {
        rax23 = fts_alloc(r12_7, 0x178bb, 0);
        *reinterpret_cast<void***>(r12_7) = rax23;
        if (!rax23) {
            addr_e4d2_21:
            fun_36a0(v19, v19);
            goto addr_e4dc_15;
        } else {
            *reinterpret_cast<void***>(rax23 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint16_t*>(rax23 + 0x68) = 9;
            *reinterpret_cast<void***>(rax23 + 88) = reinterpret_cast<void**>(1);
            al24 = setup_dir(r12_7, 0x178bb, 9);
            if (al24) {
                addr_e59f_23:
                eax25 = *reinterpret_cast<void***>(r12_7 + 72);
                if (!(reinterpret_cast<unsigned char>(eax25) & 0x204)) {
                    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r12_7 + 44);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(&eax25 + 1) & 2)) {
                        eax27 = open_safer(".", ".");
                    } else {
                        eax27 = openat_safer(rdi26, ".");
                    }
                    *reinterpret_cast<void***>(r12_7 + 40) = eax27;
                    if (reinterpret_cast<signed char>(eax27) < reinterpret_cast<signed char>(0)) {
                        *reinterpret_cast<void***>(r12_7 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) | 4);
                    }
                }
            } else {
                goto addr_e4d2_21;
            }
        }
    } else {
        v28 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r13_29) = 0;
        *reinterpret_cast<int32_t*>(&r13_29 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp30) = 0;
        *reinterpret_cast<int32_t*>(&rbp30 + 4) = 0;
        eax31 = (reinterpret_cast<unsigned char>(ebp5) >> 11 ^ 1) & 1;
        v32 = *reinterpret_cast<signed char*>(&eax31);
        do {
            addr_e43d_31:
            rax33 = fun_3860(r15_18, r15_18);
            rdx34 = rax33;
            if (reinterpret_cast<unsigned char>(rax33) <= reinterpret_cast<unsigned char>(2) || (!v32 || *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rax33) + 0xffffffffffffffff) != 47)) {
                addr_e3c0_32:
                rsi35 = r15_18;
                rax36 = fts_alloc(r12_7, rsi35, rdx34);
                if (!rax36) 
                    goto addr_e4cd_33;
            } else {
                do {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rdx34) + 0xfffffffffffffffe) != 47) 
                        goto addr_e3c0_32;
                    --rdx34;
                } while (!reinterpret_cast<int1_t>(rdx34 == 1));
                goto addr_e486_37;
            }
            *reinterpret_cast<void***>(rax36 + 88) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax36 + 8) = v19;
            *reinterpret_cast<void***>(rax36 + 48) = rax36 + 0x100;
            if (!rbp30 || !v16) {
                rax37 = fts_stat(r12_7, rax36, 0);
                *reinterpret_cast<uint16_t*>(rax36 + 0x68) = *reinterpret_cast<uint16_t*>(&rax37);
                if (r14_9) {
                    addr_e425_40:
                    *reinterpret_cast<void***>(rax36 + 16) = rbp30;
                    rbp30 = rax36;
                    continue;
                } else {
                    *reinterpret_cast<void***>(rax36 + 16) = reinterpret_cast<void**>(0);
                    if (!rbp30) {
                        ++r13_29;
                        v28 = rax36;
                        rbp30 = rax36;
                        r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_29) * 8);
                        if (r15_18) 
                            goto addr_e43d_31; else 
                            goto addr_e53d_43;
                    }
                }
            } else {
                *reinterpret_cast<int64_t*>(rax36 + 0xa0) = 2;
                *reinterpret_cast<uint16_t*>(rax36 + 0x68) = 11;
                if (r14_9) 
                    goto addr_e425_40;
                *reinterpret_cast<void***>(rax36 + 16) = reinterpret_cast<void**>(0);
            }
            rax38 = v28;
            v28 = rax36;
            *reinterpret_cast<void***>(rax38 + 16) = rax36;
            continue;
            addr_e486_37:
            goto addr_e3c0_32;
            ++r13_29;
            r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_29) * 8);
        } while (r15_18);
        goto addr_e540_48;
    }
    i_ring_init(r12_7 + 96, 0xffffffff);
    goto addr_e49e_3;
    addr_e4cd_33:
    while (rbp30) {
        r13_39 = rbp30;
        rbp30 = *reinterpret_cast<void***>(rbp30 + 16);
        rdi40 = *reinterpret_cast<void***>(r13_39 + 24);
        if (rdi40) {
            fun_3980(rdi40, rsi35);
        }
        fun_36a0(r13_39, r13_39);
    }
    goto addr_e4d2_21;
    addr_e540_48:
    if (r14_9 && reinterpret_cast<unsigned char>(r13_29) > reinterpret_cast<unsigned char>(1)) {
        rax41 = fts_sort(r12_7, rbp30, r13_29, rcx);
        rbp30 = rax41;
    }
    rsi35 = reinterpret_cast<void**>(0x178bb);
    rax42 = fts_alloc(r12_7, 0x178bb, 0);
    *reinterpret_cast<void***>(r12_7) = rax42;
    if (!rax42) 
        goto addr_e4cd_33;
    *reinterpret_cast<void***>(rax42 + 16) = rbp30;
    *reinterpret_cast<uint16_t*>(rax42 + 0x68) = 9;
    *reinterpret_cast<void***>(rax42 + 88) = reinterpret_cast<void**>(1);
    al43 = setup_dir(r12_7, 0x178bb, 0);
    if (!al43) 
        goto addr_e4cd_33; else 
        goto addr_e59f_23;
    addr_e53d_43:
    goto addr_e540_48;
    addr_e30a_8:
    r15_44 = rbx8;
    *reinterpret_cast<int32_t*>(&r13_45) = 0;
    *reinterpret_cast<int32_t*>(&r13_45 + 4) = 0;
    do {
        rax46 = fun_3860(rdi12, rdi12);
        if (reinterpret_cast<unsigned char>(r13_45) < reinterpret_cast<unsigned char>(rax46)) {
            r13_45 = rax46;
        }
        rdi12 = r15_44->f8;
        r15_44 = reinterpret_cast<struct s38*>(&r15_44->f8);
    } while (rdi12);
    rsi14 = r13_45 + 1;
    if (reinterpret_cast<unsigned char>(rsi14) >= reinterpret_cast<unsigned char>("__libc_start_main")) 
        goto addr_e339_10;
    rsi14 = reinterpret_cast<void**>("__libc_start_main");
    goto addr_e339_10;
}

void hash_free();

int64_t fun_e6a3(void** rdi, void** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void** rbp6;
    void** rbx7;
    void** rbp8;
    void** rdi9;
    void** rdi10;
    void** rdi11;
    void** eax12;
    int32_t eax13;
    void** rax14;
    void** r13d15;
    void** rbx16;
    int32_t eax17;
    void*** rbx18;
    unsigned char al19;
    void** eax20;
    void** rdi21;
    void** rax22;
    int64_t rax23;
    int32_t eax24;
    void** rax25;
    int32_t eax26;
    void** rax27;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *reinterpret_cast<void***>(rdi);
    if (rdi5) {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi5 + 88)) >= reinterpret_cast<signed char>(0)) {
            while (1) {
                rbp6 = *reinterpret_cast<void***>(rdi5 + 16);
                if (rbp6) {
                    fun_36a0(rdi5);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                } else {
                    rbp6 = *reinterpret_cast<void***>(rdi5 + 8);
                    fun_36a0(rdi5);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                }
                rdi5 = rbp6;
            }
        } else {
            rbp6 = rdi5;
        }
        fun_36a0(rbp6);
    }
    rbx7 = *reinterpret_cast<void***>(r12_4 + 8);
    if (rbx7) {
        do {
            rbp8 = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdi9 = *reinterpret_cast<void***>(rbp8 + 24);
            if (rdi9) {
                fun_3980(rdi9, rsi);
            }
            fun_36a0(rbp8);
        } while (rbx7);
    }
    rdi10 = *reinterpret_cast<void***>(r12_4 + 16);
    fun_36a0(rdi10);
    rdi11 = *reinterpret_cast<void***>(r12_4 + 32);
    fun_36a0(rdi11);
    eax12 = *reinterpret_cast<void***>(r12_4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
        if (!(*reinterpret_cast<unsigned char*>(&eax12) & 4)) {
            eax13 = fun_3b40();
            if (eax13) {
                rax14 = fun_3700();
                r13d15 = *reinterpret_cast<void***>(rax14);
                rbx16 = rax14;
                eax17 = fun_3950();
                if (r13d15 || !eax17) {
                    addr_e75c_19:
                    rbx18 = reinterpret_cast<void***>(r12_4 + 96);
                } else {
                    addr_e868_20:
                    r13d15 = *reinterpret_cast<void***>(rbx16);
                    goto addr_e75c_19;
                }
                while (al19 = i_ring_empty(rbx18, rsi, rdx), al19 == 0) {
                    eax20 = i_ring_pop(rbx18, rsi, rdx);
                    if (reinterpret_cast<signed char>(eax20) < reinterpret_cast<signed char>(0)) 
                        continue;
                    fun_3950();
                }
                if (*reinterpret_cast<void***>(r12_4 + 80)) {
                    hash_free();
                }
                rdi21 = *reinterpret_cast<void***>(r12_4 + 88);
                if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_4 + 72)) & 0x102)) {
                    fun_36a0(rdi21);
                } else {
                    if (rdi21) {
                        hash_free();
                    }
                }
                fun_36a0(r12_4);
                if (r13d15) {
                    rax22 = fun_3700();
                    *reinterpret_cast<void***>(rax22) = r13d15;
                    r13d15 = reinterpret_cast<void**>(0xffffffff);
                }
                *reinterpret_cast<void***>(&rax23) = r13d15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
                return rax23;
            } else {
                eax24 = fun_3950();
                if (eax24) {
                    rax25 = fun_3700();
                    rbx16 = rax25;
                    goto addr_e868_20;
                }
            }
        }
    } else {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_4 + 44)) >= reinterpret_cast<signed char>(0) && (eax26 = fun_3950(), !!eax26)) {
            rax27 = fun_3700();
            r13d15 = *reinterpret_cast<void***>(rax27);
            goto addr_e75c_19;
        }
    }
    r13d15 = reinterpret_cast<void**>(0);
    goto addr_e75c_19;
}

struct s39 {
    signed char f0;
    void** f1;
};

void** fun_38c0(void** rdi, void** rsi, void** rdx);

void** fun_e893(void** rdi, void** rsi) {
    void** r12_3;
    void** edx4;
    void** rbp5;
    uint32_t eax6;
    void** rax7;
    void** rcx8;
    int32_t eax9;
    void** rdx10;
    void** rax11;
    void** eax12;
    int64_t rdi13;
    void** eax14;
    void** r13_15;
    uint32_t eax16;
    void** r13_17;
    void** rax18;
    void** r14_19;
    void** rdi20;
    int32_t eax21;
    void** rax22;
    void** eax23;
    void** rax24;
    void** rax25;
    void** eax26;
    void** rax27;
    void** r8_28;
    void** rax29;
    void** rax30;
    void** r14_31;
    void** rdx32;
    void** rax33;
    void** rax34;
    void** rax35;
    void** rsi36;
    void** rdi37;
    struct s39* rdi38;
    void** rdi39;
    uint32_t eax40;
    void** rax41;
    uint32_t eax42;
    void** eax43;
    int32_t eax44;
    void** rax45;
    void** esi46;
    void** rsi47;
    int32_t eax48;
    uint32_t eax49;
    void** rdi50;
    void** rax51;
    void** rdi52;
    void** r14_53;
    void** rsi54;
    void** rax55;
    void** rax56;
    void** r13_57;
    void** rax58;
    void** rax59;
    void** eax60;
    int64_t rdi61;
    void** eax62;
    void** rax63;
    void** eax64;
    void** r13_65;
    void** r14_66;
    void** rdi67;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi);
    if (!r12_3) 
        goto addr_e9b8_2;
    edx4 = *reinterpret_cast<void***>(rdi + 72);
    rbp5 = rdi;
    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 32) 
        goto addr_e9b8_2;
    eax6 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
    if (*reinterpret_cast<int16_t*>(&eax6) == 1) {
        rax7 = fts_stat(rdi, r12_3, 0);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax7);
        goto addr_e9bb_6;
    }
    *reinterpret_cast<uint32_t*>(&rcx8) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&eax6) != 2) 
        goto addr_e8e2_8;
    eax9 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx8 + 0xfffffffffffffff4));
    if (*reinterpret_cast<uint16_t*>(&eax9) <= 1) {
        *reinterpret_cast<uint32_t*>(&rdx10) = 1;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fts_stat(rdi, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax11);
        if (*reinterpret_cast<uint16_t*>(&rax11) != 1) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            if (*reinterpret_cast<uint16_t*>(&rax11) != 11) 
                goto addr_e9bb_6;
            goto addr_ec68_13;
        }
        eax12 = *reinterpret_cast<void***>(rbp5 + 72);
        if (*reinterpret_cast<unsigned char*>(&eax12) & 4) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            goto addr_ebff_16;
        }
        *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(rbp5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(eax12) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
            eax14 = open_safer(".", ".");
        } else {
            eax14 = openat_safer(rdi13, ".");
        }
        *reinterpret_cast<void***>(r12_3 + 68) = eax14;
        if (reinterpret_cast<signed char>(eax14) >= reinterpret_cast<signed char>(0)) 
            goto addr_ef66_21;
    } else {
        if (*reinterpret_cast<int16_t*>(&rcx8) != 1) {
            do {
                addr_e918_23:
                r13_15 = r12_3;
                r12_3 = *reinterpret_cast<void***>(r12_3 + 16);
                if (!r12_3) 
                    goto addr_e925_24;
                *reinterpret_cast<void***>(rbp5) = r12_3;
                fun_36a0(r13_15);
                if (!*reinterpret_cast<void***>(r12_3 + 88)) 
                    goto addr_eaf0_26;
                eax16 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
            } while (*reinterpret_cast<int16_t*>(&eax16) == 4);
            goto addr_eba0_28;
        } else {
            addr_e9e7_29:
            if (!(*reinterpret_cast<unsigned char*>(&edx4) & 64) || *reinterpret_cast<void***>(r12_3 + 0x70) == *reinterpret_cast<void***>(rbp5 + 24)) {
                r13_17 = *reinterpret_cast<void***>(rbp5 + 8);
                if (!r13_17) {
                    addr_ecda_31:
                    rax18 = fts_build(rbp5, 3);
                    *reinterpret_cast<void***>(rbp5 + 8) = rax18;
                    if (!rax18) {
                        if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) {
                            if (*reinterpret_cast<void***>(r12_3 + 64) && *reinterpret_cast<uint16_t*>(r12_3 + 0x68) != 4) {
                                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                            }
                            leave_dir(rbp5, r12_3);
                            goto addr_e9bb_6;
                        }
                    } else {
                        r12_3 = rax18;
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 16) {
                        *reinterpret_cast<unsigned char*>(&edx4 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx4 + 1) & 0xef);
                        *reinterpret_cast<void***>(rbp5 + 72) = edx4;
                        do {
                            r14_19 = r13_17;
                            r13_17 = *reinterpret_cast<void***>(r13_17 + 16);
                            rdi20 = *reinterpret_cast<void***>(r14_19 + 24);
                            if (rdi20) {
                                fun_3980(rdi20, rsi);
                            }
                            fun_36a0(r14_19);
                        } while (r13_17);
                        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                        goto addr_ecda_31;
                    } else {
                        rcx8 = *reinterpret_cast<void***>(r12_3 + 48);
                        eax21 = fts_safe_changedir(rbp5, r12_3, 0xffffffff, rcx8);
                        if (!eax21) {
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                        } else {
                            rax22 = fun_3700();
                            eax23 = *reinterpret_cast<void***>(rax22);
                            *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 1);
                            *reinterpret_cast<void***>(r12_3 + 64) = eax23;
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                            if (r12_3) {
                                rax24 = r12_3;
                                do {
                                    *reinterpret_cast<void***>(rax24 + 48) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rax24 + 8) + 48);
                                    rax24 = *reinterpret_cast<void***>(rax24 + 16);
                                } while (rax24);
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                goto addr_ebaa_50;
            } else {
                addr_ea82_51:
                if (*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) & 2) {
                    fun_3950();
                    goto addr_ea8e_53;
                }
            }
        }
    }
    rax25 = fun_3700();
    eax26 = *reinterpret_cast<void***>(rax25);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
    *reinterpret_cast<void***>(r12_3 + 64) = eax26;
    *reinterpret_cast<void***>(rbp5) = r12_3;
    goto addr_e9bb_6;
    addr_ef66_21:
    *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
    *reinterpret_cast<uint32_t*>(&rax27) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    addr_ebeb_55:
    *reinterpret_cast<void***>(rbp5) = r12_3;
    if (*reinterpret_cast<uint16_t*>(&rax27) == 11) {
        addr_ec68_13:
        if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) == 2) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax27 = fts_stat(rbp5, r12_3, 0, rbp5, r12_3, 0);
            *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax27);
            goto addr_ebf5_57;
        } else {
            if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) != 1) {
                goto 0x3d3f;
            }
        }
    } else {
        addr_ebf5_57:
        if (*reinterpret_cast<uint16_t*>(&rax27) != 1) {
            addr_e9bb_6:
            return r12_3;
        } else {
            addr_ebff_16:
            if (!*reinterpret_cast<void***>(r12_3 + 88)) {
                *reinterpret_cast<void***>(rbp5 + 24) = *reinterpret_cast<void***>(r12_3 + 0x70);
            }
        }
    }
    rax29 = enter_dir(rbp5, r12_3, rdx10, rcx8, r8_28);
    if (!*reinterpret_cast<signed char*>(&rax29)) {
        rax30 = fun_3700();
        *reinterpret_cast<int32_t*>(&r12_3) = 0;
        *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
        *reinterpret_cast<void***>(rax30) = reinterpret_cast<void**>(12);
        goto addr_e9bb_6;
    }
    addr_e925_24:
    r14_31 = *reinterpret_cast<void***>(r13_15 + 8);
    if (*reinterpret_cast<void***>(r14_31 + 24)) {
        rdx32 = *reinterpret_cast<void***>(rbp5 + 32);
        rax33 = *reinterpret_cast<void***>(r14_31 + 72);
        *reinterpret_cast<void***>(rbp5) = r14_31;
        *reinterpret_cast<int32_t*>(&rsi) = 3;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx32) + reinterpret_cast<unsigned char>(rax33)) = 0;
        rax34 = fts_build(rbp5, 3);
        if (rax34) {
            r12_3 = rax34;
            fun_36a0(r13_15, r13_15);
        } else {
            if (*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32) {
                addr_e9b8_2:
                *reinterpret_cast<int32_t*>(&r12_3) = 0;
                *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
                goto addr_e9bb_6;
            } else {
                r14_31 = *reinterpret_cast<void***>(r13_15 + 8);
                goto addr_e934_67;
            }
        }
    } else {
        addr_e934_67:
        *reinterpret_cast<void***>(rbp5) = r14_31;
        fun_36a0(r13_15, r13_15);
        if (*reinterpret_cast<void***>(r14_31 + 88) == 0xffffffffffffffff) {
            fun_36a0(r14_31, r14_31);
            rax35 = fun_3700();
            *reinterpret_cast<void***>(rax35) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0);
            goto addr_e9bb_6;
        }
    }
    addr_ebaa_50:
    rsi36 = r12_3 + 0x100;
    rdi37 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72) + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 56)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72)) + 0xffffffffffffffff) != 47) {
        rdi37 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72);
    }
    rdi38 = reinterpret_cast<struct s39*>(reinterpret_cast<unsigned char>(rdi37) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)));
    rdi38->f0 = 47;
    rdi39 = reinterpret_cast<void**>(&rdi38->f1);
    rdx10 = *reinterpret_cast<void***>(r12_3 + 96) + 1;
    fun_3bc0(rdi39, rsi36, rdx10, rdi39, rsi36, rdx10);
    *reinterpret_cast<uint32_t*>(&rax27) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    goto addr_ebeb_55;
    if (*reinterpret_cast<uint16_t*>(r14_31 + 0x68) == 11) 
        goto 0x3d3f;
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_31 + 72))) = 0;
    if (*reinterpret_cast<void***>(r14_31 + 88)) 
        goto addr_e96e_73;
    eax40 = restore_initial_cwd(rbp5, rsi);
    if (!eax40) {
        addr_e983_75:
        if (*reinterpret_cast<uint16_t*>(r14_31 + 0x68) != 2) {
            if (*reinterpret_cast<void***>(r14_31 + 64)) {
                *reinterpret_cast<uint16_t*>(r14_31 + 0x68) = 7;
            } else {
                *reinterpret_cast<uint16_t*>(r14_31 + 0x68) = 6;
                leave_dir(rbp5, r14_31);
            }
        }
    } else {
        addr_edc8_79:
        rax41 = fun_3700();
        *reinterpret_cast<void***>(r14_31 + 64) = *reinterpret_cast<void***>(rax41);
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_e983_75;
    }
    r12_3 = r14_31;
    if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) 
        goto addr_e9bb_6;
    goto addr_e9b8_2;
    addr_e96e_73:
    eax42 = *reinterpret_cast<unsigned char*>(r14_31 + 0x6a);
    if (*reinterpret_cast<unsigned char*>(&eax42) & 2) {
        eax43 = *reinterpret_cast<void***>(rbp5 + 72);
        if (!(*reinterpret_cast<unsigned char*>(&eax43) & 4)) {
            if (!(*reinterpret_cast<unsigned char*>(&eax43 + 1) & 2)) {
                eax44 = fun_3b40();
                if (eax44) {
                    rax45 = fun_3700();
                    *reinterpret_cast<void***>(r14_31 + 64) = *reinterpret_cast<void***>(rax45);
                    *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
                }
            } else {
                esi46 = *reinterpret_cast<void***>(r14_31 + 68);
                cwd_advance_fd(rbp5, esi46, 1);
            }
        }
        fun_3950();
        goto addr_e983_75;
    } else {
        if (*reinterpret_cast<unsigned char*>(&eax42) & 1) 
            goto addr_e983_75;
        rsi47 = *reinterpret_cast<void***>(r14_31 + 8);
        eax48 = fts_safe_changedir(rbp5, rsi47, 0xffffffff, "..");
        if (!eax48) 
            goto addr_e983_75;
        goto addr_edc8_79;
    }
    addr_eaf0_26:
    eax49 = restore_initial_cwd(rbp5, rsi);
    if (eax49) {
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_e9b8_2;
    }
    rdi50 = *reinterpret_cast<void***>(rbp5 + 88);
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) & 0x102)) {
        fun_36a0(rdi50);
    } else {
        if (rdi50) {
            hash_free();
        }
    }
    rax51 = *reinterpret_cast<void***>(r12_3 + 96);
    rdi52 = *reinterpret_cast<void***>(rbp5 + 32);
    r14_53 = r12_3 + 0x100;
    *reinterpret_cast<void***>(r12_3 + 72) = rax51;
    rdx10 = rax51 + 1;
    fun_3bc0(rdi52, r14_53, rdx10);
    *reinterpret_cast<int32_t*>(&rsi54) = 47;
    *reinterpret_cast<int32_t*>(&rsi54 + 4) = 0;
    rax55 = fun_38c0(r14_53, 47, rdx10);
    if (!rax55) 
        goto addr_eb7b_98;
    if (r14_53 == rax55) {
        if (!*reinterpret_cast<void***>(r14_53 + 1)) {
            addr_eb7b_98:
            rax56 = *reinterpret_cast<void***>(rbp5 + 32);
            *reinterpret_cast<void***>(r12_3 + 56) = rax56;
            *reinterpret_cast<void***>(r12_3 + 48) = rax56;
            setup_dir(rbp5, rsi54, rdx10);
            *reinterpret_cast<uint32_t*>(&rax27) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
            goto addr_ebeb_55;
        } else {
            goto addr_eb58_102;
        }
    } else {
        addr_eb58_102:
        r13_57 = rax55 + 1;
        rax58 = fun_3860(r13_57, r13_57);
        rsi54 = r13_57;
        rdx10 = rax58 + 1;
        fun_3bc0(r14_53, rsi54, rdx10);
        *reinterpret_cast<void***>(r12_3 + 96) = rax58;
        goto addr_eb7b_98;
    }
    addr_eba0_28:
    if (*reinterpret_cast<int16_t*>(&eax16) == 2) {
        rax59 = fts_stat(rbp5, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax59);
        if (*reinterpret_cast<uint16_t*>(&rax59) == 1 && (eax60 = *reinterpret_cast<void***>(rbp5 + 72), !(*reinterpret_cast<unsigned char*>(&eax60) & 4))) {
            *reinterpret_cast<void***>(&rdi61) = *reinterpret_cast<void***>(rbp5 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi61) + 4) = 0;
            if (!(*reinterpret_cast<unsigned char*>(&eax60 + 1) & 2)) {
                eax62 = open_safer(".", ".");
            } else {
                eax62 = openat_safer(rdi61, ".");
            }
            *reinterpret_cast<void***>(r12_3 + 68) = eax62;
            if (reinterpret_cast<signed char>(eax62) < reinterpret_cast<signed char>(0)) {
                rax63 = fun_3700();
                eax64 = *reinterpret_cast<void***>(rax63);
                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                *reinterpret_cast<void***>(r12_3 + 64) = eax64;
            } else {
                *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
            }
        }
        *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
        goto addr_ebaa_50;
    }
    addr_ea8e_53:
    r13_65 = *reinterpret_cast<void***>(rbp5 + 8);
    if (r13_65) {
        do {
            r14_66 = r13_65;
            r13_65 = *reinterpret_cast<void***>(r13_65 + 16);
            rdi67 = *reinterpret_cast<void***>(r14_66 + 24);
            if (rdi67) {
                fun_3980(rdi67, rsi);
            }
            fun_36a0(r14_66);
        } while (r13_65);
        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
    }
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 6;
    leave_dir(rbp5, r12_3);
    goto addr_e9bb_6;
    addr_e8e2_8:
    if (*reinterpret_cast<int16_t*>(&rcx8) != 1) 
        goto addr_e918_23;
    if (*reinterpret_cast<int16_t*>(&eax6) != 4) 
        goto addr_e9e7_29; else 
        goto addr_ea82_51;
}

struct s40 {
    signed char[108] pad108;
    int16_t f6c;
};

int64_t fun_efe3() {
    uint32_t edx1;
    void** rax2;
    struct s40* rsi3;
    int16_t dx4;

    __asm__("cli ");
    if (edx1 > 4) {
        rax2 = fun_3700();
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(22);
        return 1;
    } else {
        rsi3->f6c = dx4;
        return 0;
    }
}

void** fun_f013(void** rdi, void** rsi) {
    uint32_t r13d3;
    void** rbp4;
    void** rax5;
    void** r14_6;
    void** r15_7;
    uint32_t edx8;
    void** rax9;
    void** rbx10;
    void** r12_11;
    void** rdi12;
    int32_t r12d13;
    void** eax14;
    void** rsi15;
    int64_t rdi16;
    void** eax17;
    void** r13d18;
    void** eax19;
    void** rsi20;
    void** rax21;
    int32_t eax22;
    void** ebx23;

    __asm__("cli ");
    r13d3 = *reinterpret_cast<uint32_t*>(&rsi);
    rbp4 = rdi;
    rax5 = fun_3700();
    r14_6 = rax5;
    if (r13d3 & 0xffffefff) {
        *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(22);
        return 0;
    }
    r15_7 = *reinterpret_cast<void***>(rbp4);
    *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 32) {
        return 0;
    }
    edx8 = *reinterpret_cast<uint16_t*>(r15_7 + 0x68);
    if (*reinterpret_cast<int16_t*>(&edx8) == 9) {
        return *reinterpret_cast<void***>(r15_7 + 16);
    }
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&edx8) == 1) 
        goto addr_f068_8;
    addr_f0dd_9:
    return rax9;
    addr_f068_8:
    rbx10 = *reinterpret_cast<void***>(rbp4 + 8);
    if (rbx10) {
        do {
            r12_11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 16);
            rdi12 = *reinterpret_cast<void***>(r12_11 + 24);
            if (rdi12) {
                fun_3980(rdi12, rsi);
            }
            fun_36a0(r12_11);
        } while (rbx10);
    }
    r12d13 = 1;
    if (reinterpret_cast<int1_t>(r13d3 == "__libc_start_main")) {
        *reinterpret_cast<void***>(rbp4 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 72)) | reinterpret_cast<uint32_t>("__libc_start_main"));
        r12d13 = 2;
    }
    if (*reinterpret_cast<void***>(r15_7 + 88)) 
        goto addr_f0ce_17;
    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(r15_7 + 48)) == 47) 
        goto addr_f0ce_17;
    eax14 = *reinterpret_cast<void***>(rbp4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax14) & 4)) 
        goto addr_f0f0_20;
    addr_f0ce_17:
    *reinterpret_cast<int32_t*>(&rsi15) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    rax9 = fts_build(rbp4, rsi15);
    *reinterpret_cast<void***>(rbp4 + 8) = rax9;
    goto addr_f0dd_9;
    addr_f0f0_20:
    *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rbp4 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&eax14 + 1) & 2)) {
        eax17 = open_safer(".", ".");
        r13d18 = eax17;
    } else {
        eax19 = openat_safer(rdi16, ".");
        r13d18 = eax19;
    }
    if (reinterpret_cast<signed char>(r13d18) >= reinterpret_cast<signed char>(0)) 
        goto addr_f127_24;
    *reinterpret_cast<void***>(rbp4 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    goto addr_f0dd_9;
    addr_f127_24:
    *reinterpret_cast<int32_t*>(&rsi20) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
    rax21 = fts_build(rbp4, rsi20);
    *reinterpret_cast<void***>(rbp4 + 8) = rax21;
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 2) {
        cwd_advance_fd(rbp4, r13d18, 1);
    } else {
        eax22 = fun_3b40();
        if (eax22) {
            ebx23 = *reinterpret_cast<void***>(r14_6);
            fun_3950();
            *reinterpret_cast<int32_t*>(&rax9) = 0;
            *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
            *reinterpret_cast<void***>(r14_6) = ebx23;
            goto addr_f0dd_9;
        } else {
            fun_3950();
        }
    }
    rax9 = *reinterpret_cast<void***>(rbp4 + 8);
    goto addr_f0dd_9;
}

int64_t safe_write(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t fun_f213(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t r13_4;
    int32_t r12d5;
    int64_t rbp6;
    int64_t rbx7;
    int64_t rdi8;
    int64_t rax9;
    void** rax10;

    __asm__("cli ");
    if (!rdx) {
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
    } else {
        r12d5 = edi;
        rbp6 = rsi;
        rbx7 = rdx;
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&rdi8) = r12d5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
            rax9 = safe_write(rdi8, rbp6, rbx7);
            if (rax9 == -1) 
                break;
            if (!rax9) 
                goto addr_f270_6;
            r13_4 = r13_4 + rax9;
            rbp6 = rbp6 + rax9;
            rbx7 = rbx7 - rax9;
        } while (rbx7);
    }
    return r13_4;
    addr_f270_6:
    rax10 = fun_3700();
    *reinterpret_cast<void***>(rax10) = reinterpret_cast<void**>(28);
    return r13_4;
}

uint64_t fun_f293(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_f2b3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s41 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_f713(struct s41* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s42 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_f723(struct s42* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s43 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_f733(struct s43* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s46 {
    signed char[8] pad8;
    struct s46* f8;
};

struct s45 {
    int64_t f0;
    struct s46* f8;
};

struct s44 {
    struct s45* f0;
    struct s45* f8;
};

uint64_t fun_f743(struct s44* rdi) {
    struct s45* rcx2;
    struct s45* rsi3;
    uint64_t r8_4;
    struct s46* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s49 {
    signed char[8] pad8;
    struct s49* f8;
};

struct s48 {
    int64_t f0;
    struct s49* f8;
};

struct s47 {
    struct s48* f0;
    struct s48* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_f7a3(struct s47* rdi) {
    struct s48* rcx2;
    struct s48* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s49* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s52 {
    signed char[8] pad8;
    struct s52* f8;
};

struct s51 {
    int64_t f0;
    struct s52* f8;
};

struct s50 {
    struct s51* f0;
    struct s51* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_f813(struct s50* rdi, void** rsi) {
    int64_t v3;
    int64_t r12_4;
    uint64_t r12_5;
    void** v6;
    void** rbp7;
    void** rbp8;
    void** v9;
    void** rbx10;
    struct s51* rcx11;
    struct s51* rsi12;
    void** r8_13;
    void** rbx14;
    void** r13_15;
    struct s52* rax16;
    uint64_t rdx17;
    void** r9_18;
    void** v19;
    void** r9_20;
    void** v21;
    void** r9_22;
    void** v23;

    __asm__("cli ");
    v3 = r12_4;
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    v6 = rbp7;
    rbp8 = rsi;
    v9 = rbx10;
    rcx11 = rdi->f0;
    rsi12 = rdi->f8;
    r8_13 = rdi->f20;
    rbx14 = rdi->f10;
    r13_15 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx11) < reinterpret_cast<uint64_t>(rsi12)) {
        while (1) {
            if (!rcx11->f0) {
                ++rcx11;
                if (reinterpret_cast<uint64_t>(rsi12) <= reinterpret_cast<uint64_t>(rcx11)) 
                    break;
            } else {
                rax16 = rcx11->f8;
                *reinterpret_cast<int32_t*>(&rdx17) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
                if (rax16) {
                    do {
                        rax16 = rax16->f8;
                        ++rdx17;
                    } while (rax16);
                }
                if (r12_5 < rdx17) {
                    r12_5 = rdx17;
                }
                ++rcx11;
                if (reinterpret_cast<uint64_t>(rsi12) <= reinterpret_cast<uint64_t>(rcx11)) 
                    break;
            }
        }
    }
    fun_3ca0(rbp8, 1, "# entries:         %lu\n", r8_13, r8_13, r9_18, v19, v9, v6, v3);
    fun_3ca0(rbp8, 1, "# buckets:         %lu\n", rbx14, r8_13, r9_20, v21, v9, v6, v3);
    if (reinterpret_cast<signed char>(r13_15) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x81ac]");
        if (reinterpret_cast<signed char>(rbx14) >= reinterpret_cast<signed char>(0)) {
            addr_f8ca_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_f949_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_3ca0(rbp8, 1, "# buckets used:    %lu (%.2f%%)\n", r13_15, r8_13, r9_22, v23, v9, v6, v3);
        goto fun_3ca0;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x822b]");
        if (reinterpret_cast<signed char>(rbx14) < reinterpret_cast<signed char>(0)) 
            goto addr_f949_14; else 
            goto addr_f8ca_13;
    }
}

struct s53 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s54 {
    int64_t f0;
    struct s54* f8;
};

int64_t fun_f973(struct s53* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s53* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s54* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x3d4e;
    rbx7 = reinterpret_cast<struct s54*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_f9d8_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_f9cb_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_f9cb_7;
    }
    addr_f9db_10:
    return r12_3;
    addr_f9d8_5:
    r12_3 = rbx7->f0;
    goto addr_f9db_10;
    addr_f9cb_7:
    return 0;
}

struct s55 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_f9f3(struct s55* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x3d53;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_fa2f_7;
    return *rax2;
    addr_fa2f_7:
    goto 0x3d53;
}

struct s57 {
    int64_t f0;
    struct s57* f8;
};

struct s56 {
    int64_t* f0;
    struct s57* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_fa43(struct s56* rdi, int64_t rsi) {
    struct s56* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s57* rax7;
    struct s57* rdx8;
    struct s57* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x3d59;
    rax7 = reinterpret_cast<struct s57*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_fa8e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_fa8e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_faac_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_faac_10:
    return r8_10;
}

struct s59 {
    int64_t f0;
    struct s59* f8;
};

struct s58 {
    struct s59* f0;
    struct s59* f8;
};

void fun_fad3(struct s58* rdi, int64_t rsi, uint64_t rdx) {
    struct s59* r9_4;
    uint64_t rax5;
    struct s59* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_fb12_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_fb12_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s61 {
    int64_t f0;
    struct s61* f8;
};

struct s60 {
    struct s61* f0;
    struct s61* f8;
};

int64_t fun_fb23(struct s60* rdi, int64_t rsi, int64_t rdx) {
    struct s61* r14_4;
    int64_t r12_5;
    struct s60* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s61* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_fb4f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_fb91_10;
            }
            addr_fb4f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_fb59_11:
    return r12_5;
    addr_fb91_10:
    goto addr_fb59_11;
}

uint64_t fun_fba3(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s62 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_fbe3(struct s62* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_fc13(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    void** rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    uint32_t esi12;
    void** rax13;
    void** rax14;
    void** rdi15;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0xf290);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<void**>(0xf2b0);
    }
    rax9 = fun_3ae0(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0x17ac0);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((esi12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), rax13 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&esi12)), *reinterpret_cast<void***>(r12_10 + 16) = rax13, rax13 == 0) || (rax14 = fun_39f0(rax13, 16), *reinterpret_cast<void***>(r12_10) = rax14, rax14 == 0))) {
            rdi15 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_36a0(rdi15, rdi15);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<void***>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s65 {
    int64_t f0;
    struct s65* f8;
};

struct s64 {
    int64_t f0;
    struct s65* f8;
};

struct s63 {
    struct s64* f0;
    struct s64* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s65* f48;
};

void fun_fd13(struct s63* rdi) {
    struct s63* rbp2;
    struct s64* r12_3;
    struct s65* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s65* rax7;
    struct s65* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s66 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_fdc3(struct s66* rdi) {
    struct s66* r12_2;
    void** r13_3;
    void** rax4;
    void** rbp5;
    void** rbx6;
    void** rdi7;
    void** rdi8;
    void** rbx9;
    void** rbx10;
    void** rdi11;
    void** rdi12;

    __asm__("cli ");
    r12_2 = rdi;
    r13_3 = rdi->f0;
    rax4 = rdi->f8;
    rbp5 = r13_3;
    if (!rdi->f40 || !rdi->f20) {
        addr_fe33_2:
        if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rbp5)) {
            do {
                rbx6 = *reinterpret_cast<void***>(rbp5 + 8);
                if (rbx6) {
                    do {
                        rdi7 = rbx6;
                        rbx6 = *reinterpret_cast<void***>(rbx6 + 8);
                        fun_36a0(rdi7);
                    } while (rbx6);
                }
                rbp5 = rbp5 + 16;
            } while (reinterpret_cast<unsigned char>(r12_2->f8) > reinterpret_cast<unsigned char>(rbp5));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_3) < reinterpret_cast<unsigned char>(rax4)) {
            while (1) {
                rdi8 = *reinterpret_cast<void***>(r13_3);
                if (!rdi8) {
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                } else {
                    rbx9 = r13_3;
                    while (r12_2->f40(rdi8), rbx9 = *reinterpret_cast<void***>(rbx9 + 8), !!rbx9) {
                        rdi8 = *reinterpret_cast<void***>(rbx9);
                    }
                    rax4 = r12_2->f8;
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                }
            }
            rbp5 = r12_2->f0;
            goto addr_fe33_2;
        }
    }
    rbx10 = r12_2->f48;
    if (rbx10) {
        do {
            rdi11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
            fun_36a0(rdi11);
        } while (rbx10);
    }
    rdi12 = r12_2->f0;
    fun_36a0(rdi12);
    goto fun_36a0;
}

int64_t fun_feb3(struct s9* rdi, uint64_t rsi) {
    struct s10* r12_3;
    int64_t rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    struct s9* r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    int64_t rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = rdi->f28;
    rax4 = g28;
    esi5 = r12_3->f10;
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_fff0_2;
    if (rdi->f10 == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_39f0(rax6, 16);
        if (!rax8) {
            addr_fff0_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<struct s9*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8 - 8 + 8);
            v10 = rdi->f48;
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = rdi->f0;
                fun_36a0(rdi12, rdi12);
                rdi->f0 = rax8;
                rdi->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                rdi->f10 = rax6;
                rdi->f18 = 0;
                rdi->f48 = v10;
            } else {
                rdi->f48 = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x3d5e;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x3d5e;
                fun_36a0(rax8, rax8);
            }
        }
    }
    rax15 = rax4 - g28;
    if (rax15) {
        fun_3880();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s67 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_10043(void** rdi, void** rsi, void*** rdx) {
    int64_t rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s67* v17;
    int32_t r8d18;
    void** rax19;
    int64_t rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x3d63;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_1015e_5; else 
                goto addr_100cf_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_100cf_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_1015e_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x7931]");
            if (!cf14) 
                goto addr_101b5_12;
            __asm__("comiss xmm4, [rip+0x78e1]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x78a0]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_101b5_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x3d63;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_3ae0(16, rsi, rdx6);
                if (!rax19) {
                    addr_101b5_12:
                    r8d18 = -1;
                } else {
                    goto addr_10112_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_10112_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_1008e_28:
    rax20 = rax4 - g28;
    if (rax20) {
        fun_3880();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_10112_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_1008e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_10263() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    int64_t rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = rax1 - g28;
    if (rdx6) {
        fun_3880();
    } else {
        return rax3;
    }
}

void** fun_102c3(void** rdi, void** rsi) {
    void** rbx3;
    int64_t rax4;
    int64_t v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    int64_t rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_10350_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_10406_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x774e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x76b8]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_36a0(rdi15, rdi15);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_10406_5; else 
                goto addr_10350_4;
        }
    }
    rax16 = v5 - g28;
    if (rax16) {
        fun_3880();
    } else {
        return r12_7;
    }
}

void fun_10453() {
    __asm__("cli ");
    goto hash_remove;
}

struct s68 {
    signed char[8] pad8;
    int64_t f8;
};

uint64_t fun_10463(struct s68* rdi, int64_t rsi) {
    __asm__("cli ");
    return rdi->f8 % reinterpret_cast<uint64_t>(rsi);
}

struct s69 {
    signed char[8] pad8;
    int64_t f8;
    int64_t f10;
};

struct s70 {
    signed char[8] pad8;
    int64_t f8;
    int64_t f10;
};

int64_t fun_10483(struct s69* rdi, struct s70* rsi) {
    __asm__("cli ");
    if (rdi->f8 != rsi->f8 || rdi->f10 != rsi->f10) {
        return 0;
    }
}

struct s71 {
    int64_t f0;
    uint64_t f8;
};

uint64_t hash_pjw(int64_t rdi);

uint64_t fun_104b3(struct s71* rdi, int64_t rsi) {
    int64_t rdi3;
    uint64_t rax4;

    __asm__("cli ");
    rdi3 = rdi->f0;
    rax4 = hash_pjw(rdi3);
    return (rax4 ^ rdi->f8) % rsi;
}

struct s72 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
    int64_t f10;
};

struct s73 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
    int64_t f10;
};

int64_t fun_104e3(struct s72* rdi, struct s73* rsi) {
    void** rsi3;
    void** rdi4;
    int64_t rax5;

    __asm__("cli ");
    if (rdi->f8 != rsi->f8 || rdi->f10 != rsi->f10) {
        return 0;
    } else {
        rsi3 = rsi->f0;
        rdi4 = rdi->f0;
        rax5 = fun_3a10(rdi4, rsi3);
        *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax5) == 0);
        return rax5;
    }
}

void fun_10523(void*** rdi) {
    void** rdi2;

    __asm__("cli ");
    rdi2 = *rdi;
    fun_36a0(rdi2, rdi2);
    goto fun_36a0;
}

struct s74 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int64_t f14;
    signed char f1c;
};

void fun_10543(struct s74* rdi, int32_t esi) {
    __asm__("cli ");
    rdi->f14 = 0;
    rdi->f1c = 1;
    rdi->f0 = esi;
    rdi->f4 = esi;
    rdi->f8 = esi;
    rdi->fc = esi;
    rdi->f10 = esi;
    return;
}

struct s75 {
    signed char[28] pad28;
    unsigned char f1c;
};

int64_t fun_10563(struct s75* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rax2) = rdi->f1c;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

struct s76 {
    int32_t f0;
    signed char[16] pad20;
    uint32_t f14;
    uint32_t f18;
    unsigned char f1c;
};

int64_t fun_10573(struct s76* rdi, int32_t esi) {
    uint32_t eax3;
    uint32_t eax4;
    uint32_t edx5;
    int64_t rcx6;
    int32_t r8d7;
    uint32_t ecx8;
    int64_t rax9;

    __asm__("cli ");
    eax3 = static_cast<uint32_t>(rdi->f1c) ^ 1;
    eax4 = *reinterpret_cast<unsigned char*>(&eax3);
    edx5 = rdi->f14 + eax4 & 3;
    *reinterpret_cast<uint32_t*>(&rcx6) = edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    r8d7 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4) = esi;
    ecx8 = rdi->f18;
    rdi->f14 = edx5;
    if (ecx8 == edx5) {
        rdi->f18 = eax4 + ecx8 & 3;
    }
    rdi->f1c = 0;
    *reinterpret_cast<int32_t*>(&rax9) = r8d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

struct s77 {
    int32_t f0;
    signed char[12] pad16;
    int32_t f10;
    uint32_t f14;
    uint32_t f18;
    signed char f1c;
};

int64_t fun_105b3(struct s77* rdi) {
    int64_t rdx2;
    int32_t r8d3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f1c) 
        goto 0x3d68;
    *reinterpret_cast<uint32_t*>(&rdx2) = rdi->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx2) + 4) = 0;
    r8d3 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4);
    rax4 = rdx2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4) = rdi->f10;
    if (*reinterpret_cast<uint32_t*>(&rdx2) == rdi->f18) {
        rdi->f1c = 1;
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        rdi->f14 = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax4) + 3) & 3;
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

void fun_105f3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, void** r8, void** r9) {
    void* rsp7;
    int64_t v8;
    void** rcx9;
    int64_t rax10;
    void** v11;
    void* v12;
    int32_t eax13;
    int64_t rdi14;
    int64_t rdx15;

    __asm__("cli ");
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 88);
    v8 = rcx;
    *reinterpret_cast<int32_t*>(&rcx9) = 0;
    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
    rax10 = g28;
    if (*reinterpret_cast<unsigned char*>(&rdx) & 64) {
        *reinterpret_cast<int32_t*>(&rcx9) = *reinterpret_cast<int32_t*>(&v8);
        *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
        v11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 96);
        v12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) + 32);
    }
    eax13 = fun_3870(rdi, rsi, rdx, rcx9, r8, r9, 24, v11, v12);
    *reinterpret_cast<int32_t*>(&rdi14) = eax13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    fd_safer(rdi14);
    rdx15 = rax10 - g28;
    if (rdx15) {
        fun_3880();
    } else {
        return;
    }
}

void** fun_10673(int64_t rdi, void** rsi, int32_t edx, void*** rcx) {
    void** r12_5;
    void** eax6;
    void** rdi7;
    void** rax8;
    void** rax9;
    void** r13d10;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(&r12_5 + 4) = 0;
    eax6 = openat_safer(rdi, rsi);
    if (reinterpret_cast<signed char>(eax6) >= reinterpret_cast<signed char>(0)) {
        rdi7 = eax6;
        *reinterpret_cast<int32_t*>(&rdi7 + 4) = 0;
        rax8 = fun_3c10(rdi7, rsi);
        r12_5 = rax8;
        if (!rax8) {
            rax9 = fun_3700();
            r13d10 = *reinterpret_cast<void***>(rax9);
            fun_3950();
            *reinterpret_cast<void***>(rax9) = r13d10;
        } else {
            *rcx = eax6;
        }
    }
    return r12_5;
}

void fun_3c90(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_106d3(void** rdi) {
    void** rcx2;
    void** rbx3;
    void** rdx4;
    void** rax5;
    void** r12_6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_3c90("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_36f0();
    } else {
        rbx3 = rdi;
        rax5 = fun_38c0(rdi, 47, rdx4);
        if (rax5 && ((r12_6 = rax5 + 1, reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_6) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_3710(rax5 + 0xfffffffffffffffa, "/.libs/", 7), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax5 + 1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_6 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_6 + 2) != 45)) {
                rbx3 = r12_6;
            } else {
                rbx3 = rax5 + 4;
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

int32_t get_permissions();

int32_t set_permissions(void* rdi, int64_t rsi, int64_t rdx, void* rcx);

void free_permission_context(void* rdi, int64_t rsi, int64_t rdx, void* rcx);

int64_t fun_10783() {
    int64_t rax1;
    void* rbp2;
    int32_t eax3;
    int32_t r12d4;
    int64_t rdx5;
    int32_t ecx6;
    int64_t rdx7;
    int32_t eax8;
    int64_t rax9;
    int64_t rax10;

    __asm__("cli ");
    rax1 = g28;
    rbp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16 + 4);
    eax3 = get_permissions();
    if (eax3) {
        r12d4 = -2;
    } else {
        *reinterpret_cast<int32_t*>(&rdx5) = ecx6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
        eax8 = set_permissions(rbp2, rdx7, rdx5, rbp2);
        r12d4 = eax8;
        free_permission_context(rbp2, rdx7, rdx5, rbp2);
    }
    rax9 = rax1 - g28;
    if (rax9) {
        fun_3880();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = r12d4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

int64_t fun_10803(int64_t rdi, int32_t esi, int32_t edx, void* rcx) {
    int64_t rax5;
    void* rbp6;
    int64_t rdx7;
    int32_t eax8;
    int64_t rax9;
    int64_t rax10;

    __asm__("cli ");
    rax5 = g28;
    rbp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 24 + 4);
    *reinterpret_cast<int32_t*>(&rdx7) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
    eax8 = set_permissions(rbp6, rdi, rdx7, rcx);
    free_permission_context(rbp6, rdi, rdx7, rcx);
    rax9 = rax5 - g28;
    if (rax9) {
        fun_3880();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = eax8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_11f53(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_3700();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x1e680;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_11f93(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x1e680);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_11fb3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x1e680);
    }
    *rdi = esi;
    return 0x1e680;
}

int64_t fun_11fd3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x1e680);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s78 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_12013(struct s78* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s78*>(0x1e680);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s79 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s79* fun_12033(struct s79* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s79*>(0x1e680);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x3d78;
    if (!rdx) 
        goto 0x3d78;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x1e680;
}

struct s80 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_12073(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s80* r8) {
    struct s80* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s80*>(0x1e680);
    }
    rax7 = fun_3700();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x120a6);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s81 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_120f3(int64_t rdi, int64_t rsi, void*** rdx, struct s81* rcx) {
    struct s81* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s81*>(0x1e680);
    }
    rax6 = fun_3700();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x12121);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x1217c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_121e3() {
    __asm__("cli ");
}

void** g1e098 = reinterpret_cast<void**>(0x80);

int64_t slotvec0 = 0x100;

void fun_121f3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_36a0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x1e580) {
        fun_36a0(rdi7);
        g1e098 = reinterpret_cast<void**>(0x1e580);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x1e090) {
        fun_36a0(r12_2);
        slotvec = reinterpret_cast<void**>(0x1e090);
    }
    nslots = 1;
    return;
}

void fun_12293() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_122b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_122c3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_122e3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_12303(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s13* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3d7e;
    rcx5 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_3880();
    } else {
        return rax6;
    }
}

void** fun_12393(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s13* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x3d83;
    rcx6 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_3880();
    } else {
        return rax7;
    }
}

void** fun_12423(int32_t edi, int64_t rsi) {
    int64_t rax3;
    struct s13* rcx4;
    void** rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x3d88;
    rcx4 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_3880();
    } else {
        return rax5;
    }
}

void** fun_124b3(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    struct s13* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x3d8d;
    rcx5 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_3880();
    } else {
        return rax6;
    }
}

void** fun_12543(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s13* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xc130]");
    __asm__("movdqa xmm1, [rip+0xc138]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xc121]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_3880();
    } else {
        return rax10;
    }
}

void** fun_125e3(int64_t rdi, uint32_t esi) {
    struct s13* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xc090]");
    __asm__("movdqa xmm1, [rip+0xc098]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xc081]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_3880();
    } else {
        return rax9;
    }
}

void** fun_12683(int64_t rdi) {
    int64_t rax2;
    void** rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbff0]");
    __asm__("movdqa xmm1, [rip+0xbff8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xbfd9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_3880();
    } else {
        return rax3;
    }
}

void** fun_12713(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbf60]");
    __asm__("movdqa xmm1, [rip+0xbf68]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xbf56]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_3880();
    } else {
        return rax4;
    }
}

void** fun_127a3(int32_t edi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s13* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3d92;
    rcx5 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_3880();
    } else {
        return rax6;
    }
}

void** fun_12843(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s13* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbe2a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xbe22]");
    __asm__("movdqa xmm2, [rip+0xbe2a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3d97;
    if (!rdx) 
        goto 0x3d97;
    rcx6 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_3880();
    } else {
        return rax7;
    }
}

void** fun_128e3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rcx6;
    struct s13* rcx7;
    void** rax8;
    int64_t rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbd8a]");
    __asm__("movdqa xmm1, [rip+0xbd92]");
    __asm__("movdqa xmm2, [rip+0xbd9a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3d9c;
    if (!rdx) 
        goto 0x3d9c;
    rcx7 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = rcx6 - g28;
    if (rdx9) {
        fun_3880();
    } else {
        return rax8;
    }
}

void** fun_12993(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rdx4;
    struct s13* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbcda]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xbcd2]");
    __asm__("movdqa xmm2, [rip+0xbcda]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3da1;
    if (!rsi) 
        goto 0x3da1;
    rcx5 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_3880();
    } else {
        return rax6;
    }
}

void** fun_12a33(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s13* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xbc3a]");
    __asm__("movdqa xmm1, [rip+0xbc42]");
    __asm__("movdqa xmm2, [rip+0xbc4a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3da6;
    if (!rsi) 
        goto 0x3da6;
    rcx6 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_3880();
    } else {
        return rax7;
    }
}

void fun_12ad3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_12ae3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_12b03() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_12b23(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int32_t fun_3b20();

int64_t fun_12b43(int32_t edi, void** rsi, int32_t edx, void** rcx, int32_t r8d) {
    int64_t rax6;
    int32_t eax7;
    int32_t r12d8;
    void** rax9;
    void*** rsp10;
    void** r8_11;
    int64_t rax12;
    uint32_t edx13;
    uint32_t r9d14;
    int64_t rax15;
    int64_t rax16;
    void** rax17;
    void** rax18;
    uint32_t r9d19;
    void** rdi20;
    void** rdx21;
    int32_t eax22;
    void** rdx23;
    void** rdi24;
    int32_t eax25;
    uint32_t r9d26;
    void** rdi27;
    void** rdx28;
    int32_t eax29;
    uint32_t v30;
    uint32_t v31;
    int64_t rdi32;
    int32_t eax33;
    uint32_t v34;
    uint32_t v35;

    __asm__("cli ");
    rax6 = g28;
    eax7 = fun_3b20();
    r12d8 = eax7;
    if (eax7 >= 0 || (rax9 = fun_3700(), rsp10 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8), r8_11 = rax9, *reinterpret_cast<void***>(&rax12) = *reinterpret_cast<void***>(rax9), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0, edx13 = static_cast<uint32_t>(rax12 - 22) & 0xffffffef, *reinterpret_cast<unsigned char*>(&edx13) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!edx13)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax12) == 95)))), r9d14 = edx13, !!*reinterpret_cast<unsigned char*>(&edx13))) {
        addr_12bf0_2:
        rax15 = rax6 - g28;
        if (rax15) {
            fun_3880();
        } else {
            *reinterpret_cast<int32_t*>(&rax16) = r12d8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
            return rax16;
        }
    } else {
        if (!r8d) {
            addr_12c36_6:
            rax17 = fun_3860(rsi, rsi);
            rax18 = fun_3860(rcx, rcx);
            rsp10 = rsp10 - 8 + 8 - 8 + 8;
            if (!rax17) 
                goto addr_12c75_7;
            r9d19 = *reinterpret_cast<unsigned char*>(&r9d14);
            if (!rax18) 
                goto addr_12c75_7;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rax17) + 0xffffffffffffffff) == 47) 
                goto addr_12c90_10;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(rax18) + 0xffffffffffffffff) != 47) 
                goto addr_12c75_7;
        } else {
            if (r8d != 1) {
                *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(95);
                r12d8 = -1;
                goto addr_12bf0_2;
            } else {
                *reinterpret_cast<int32_t*>(&rdi20) = edx;
                *reinterpret_cast<int32_t*>(&rdi20 + 4) = 0;
                rdx21 = reinterpret_cast<void**>(rsp10 + 0xa0);
                eax22 = fun_3d10(rdi20, rcx, rdx21, rdi20, rcx, rdx21);
                rsp10 = rsp10 - 8 + 8;
                r8_11 = r8_11;
                if (!eax22 || *reinterpret_cast<void***>(r8_11) == 75) {
                    *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(17);
                    r12d8 = -1;
                    goto addr_12bf0_2;
                } else {
                    if (*reinterpret_cast<void***>(r8_11) != 2) 
                        goto addr_12be5_17;
                    r9d14 = 1;
                    goto addr_12c36_6;
                }
            }
        }
    }
    addr_12c90_10:
    rdx23 = reinterpret_cast<void**>(rsp10 + 16);
    *reinterpret_cast<int32_t*>(&rdi24) = edi;
    *reinterpret_cast<int32_t*>(&rdi24 + 4) = 0;
    eax25 = fun_3d10(rdi24, rsi, rdx23, rdi24, rsi, rdx23);
    rsp10 = rsp10 - 8 + 8;
    if (eax25) {
        addr_12be5_17:
        r12d8 = -1;
        goto addr_12bf0_2;
    } else {
        r9d26 = *reinterpret_cast<unsigned char*>(&r9d19);
        if (!*reinterpret_cast<signed char*>(&r9d26)) {
            *reinterpret_cast<int32_t*>(&rdi27) = edx;
            *reinterpret_cast<int32_t*>(&rdi27 + 4) = 0;
            rdx28 = reinterpret_cast<void**>(rsp10 + 0xa0);
            eax29 = fun_3d10(rdi27, rcx, rdx28, rdi27, rcx, rdx28);
            if (!eax29) {
                if ((v30 & 0xf000) == 0x4000) {
                    if ((v31 & 0xf000) == 0x4000) {
                        addr_12c75_7:
                        *reinterpret_cast<int32_t*>(&rdi32) = edi;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
                        eax33 = fun_3c60(rdi32, rsi, rdi32, rsi);
                        r12d8 = eax33;
                        goto addr_12bf0_2;
                    } else {
                        *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(21);
                        r12d8 = -1;
                        goto addr_12bf0_2;
                    }
                } else {
                    *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(20);
                    goto addr_12be5_17;
                }
            } else {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r8_11) == 2) && (v34 & 0xf000) == 0x4000) {
                    goto addr_12c75_7;
                }
            }
        } else {
            if ((v35 & 0xf000) == 0x4000) 
                goto addr_12c75_7;
            *reinterpret_cast<void***>(r8_11) = reinterpret_cast<void**>(2);
            r12d8 = -1;
            goto addr_12bf0_2;
        }
    }
}

struct s82 {
    int64_t f0;
    int64_t f8;
};

struct s82* fun_12da3(struct s82* rdi) {
    int64_t rax2;
    int32_t eax3;
    struct s82* rax4;
    int64_t v5;
    int64_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax2 = g28;
    eax3 = fun_39b0("/", reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
    if (eax3) {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    } else {
        rdi->f0 = v5;
        rdi->f8 = v6;
        rax4 = rdi;
    }
    rdx7 = rax2 - g28;
    if (rdx7) {
        fun_3880();
    } else {
        return rax4;
    }
}

int64_t fun_37d0(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_12e23(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_37d0(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_3700();
        if (*reinterpret_cast<void***>(rax9) == 4) 
            continue;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax9) == 22)) 
            break;
        if (rbx6 <= 0x7ff00000) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

int64_t fun_12e93(int32_t edi, void** rsi, void** rdx, void** rcx) {
    int64_t rax5;
    void** rax6;
    void** rax7;
    void** rax8;
    void** rax9;
    uint32_t r9d10;
    uint32_t eax11;
    void** rax12;
    void* rsp13;
    void** rdx14;
    void** rdi15;
    void** rsi16;
    int32_t eax17;
    void* rsp18;
    void** rax19;
    void** rax20;
    void** rdi21;
    void** rdx22;
    int32_t eax23;
    int32_t r9d24;
    int64_t v25;
    int64_t v26;
    int64_t v27;
    int64_t v28;
    int64_t rax29;
    int64_t rax30;

    __asm__("cli ");
    rax5 = g28;
    rax6 = last_component(rsi, rsi, rdx, rcx);
    rax7 = last_component(rcx, rsi, rdx, rcx);
    rax8 = base_len(rax6, rsi, rdx, rcx);
    rax9 = base_len(rax7, rsi, rdx, rcx);
    r9d10 = 0;
    if (rax8 == rax9 && (eax11 = fun_39c0(rax6, rax7, rax8), r9d10 = 0, !eax11)) {
        rax12 = dir_name(rsi, rax7, rsi, rax7);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x148 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rdx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 16);
        *reinterpret_cast<int32_t*>(&rdi15) = edi;
        *reinterpret_cast<int32_t*>(&rdi15 + 4) = 0;
        rsi16 = rax12;
        eax17 = fun_3d10(rdi15, rsi16, rdx14, rdi15, rsi16, rdx14);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
        if (eax17) {
            rax19 = fun_3700();
            rsi16 = *reinterpret_cast<void***>(rax19);
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            fun_3bd0();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
        }
        fun_36a0(rax12, rax12);
        rax20 = dir_name(rcx, rsi16, rcx, rsi16);
        *reinterpret_cast<int32_t*>(&rdi21) = *reinterpret_cast<int32_t*>(&rdx);
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        rdx22 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8 + 0xa0);
        eax23 = fun_3d10(rdi21, rax20, rdx22, rdi21, rax20, rdx22);
        if (eax23) {
            fun_3700();
            fun_3bd0();
        }
        r9d24 = 0;
        if (v25 == v26) {
            *reinterpret_cast<unsigned char*>(&r9d24) = reinterpret_cast<uint1_t>(v27 == v28);
        }
        fun_36a0(rax20, rax20);
        r9d10 = *reinterpret_cast<unsigned char*>(&r9d24);
    }
    rax29 = rax5 - g28;
    if (rax29) {
        fun_3880();
    } else {
        *reinterpret_cast<uint32_t*>(&rax30) = r9d10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
        return rax30;
    }
}

void fun_13033(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto same_nameat;
}

struct s83 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s84 {
    signed char[8] pad8;
    uint64_t f8;
};

int64_t fun_13053(struct s83* rdi, struct s84* rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>(rdi->f8 > rsi->f8);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdi->f8 < rsi->f8)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

void fun_13073(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_3a10;
}

void** xirealloc(void** rdi, void* rsi, void** rdx, void** rcx, void** r8);

void* fun_3830(void* rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** fun_13083(void** rdi, int32_t esi) {
    void** rsi3;
    int64_t rax4;
    int64_t v5;
    void** v6;
    void** r14_7;
    void** rbp8;
    void* rbx9;
    void** r13_10;
    void** r15_11;
    void** rax12;
    void*** rsp13;
    void** r12_14;
    struct s0* rax15;
    void** r10_16;
    uint32_t eax17;
    uint32_t eax18;
    void** rax19;
    void*** rsp20;
    void** r10_21;
    void** r11_22;
    void** r8_23;
    void** rcx24;
    void** rax25;
    void** rdx26;
    void** rdi27;
    void** r11_28;
    struct s0* r9_29;
    void** rax30;
    void** rdx31;
    void** rax32;
    int64_t rax33;
    void** rdi34;
    void** rax35;
    void** rbx36;
    void* rbp37;
    void** rcx38;
    void* rbx39;
    void** r13_40;
    void** rbp41;
    void** rax42;
    void** rsi43;
    void* r12_44;
    void* rax45;
    void** rdi46;
    void** rax47;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rsi3) = esi;
    *reinterpret_cast<int32_t*>(&rsi3 + 4) = 0;
    rax4 = g28;
    v5 = rax4;
    v6 = *reinterpret_cast<void***>(0x1da40 + reinterpret_cast<unsigned char>(rsi3) * 8);
    if (!rdi) {
        *reinterpret_cast<int32_t*>(&r14_7) = 0;
        *reinterpret_cast<int32_t*>(&r14_7 + 4) = 0;
    } else {
        rbp8 = rdi;
        *reinterpret_cast<int32_t*>(&rbx9) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx9) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r13_10) = 0;
        *reinterpret_cast<int32_t*>(&r13_10 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r15_11) = 0;
        *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        rax12 = fun_3700();
        rsp13 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 72 - 8 + 8);
        *reinterpret_cast<int32_t*>(&r14_7) = 0;
        *reinterpret_cast<int32_t*>(&r14_7 + 4) = 0;
        r12_14 = rax12;
        while (*reinterpret_cast<void***>(r12_14) = reinterpret_cast<void**>(0), rax15 = fun_3ad0(rbp8, rsi3, rbp8, rsi3), rsp13 = rsp13 - 8 + 8, !!rax15) {
            r10_16 = reinterpret_cast<void**>(&rax15->f13);
            eax17 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax15->f13));
            if (*reinterpret_cast<signed char*>(&eax17) != 46 || (eax17 = rax15->f14, *reinterpret_cast<signed char*>(&eax17) != 46)) {
                if (!*reinterpret_cast<signed char*>(&eax17)) 
                    continue;
            } else {
                eax18 = rax15->f15;
                if (!*reinterpret_cast<signed char*>(&eax18)) 
                    continue;
            }
            rax19 = fun_3860(r10_16, r10_16);
            rsp20 = rsp13 - 8 + 8;
            r10_21 = r10_16;
            r11_22 = rax19 + 1;
            if (!v6) {
                if (reinterpret_cast<signed char>(-static_cast<uint64_t>(rbx9)) <= reinterpret_cast<signed char>(r11_22)) {
                    *reinterpret_cast<int32_t*>(&r8_23) = 1;
                    *reinterpret_cast<int32_t*>(&r8_23 + 4) = 0;
                    rcx24 = reinterpret_cast<void**>(0x7ffffffffffffffe);
                    rax25 = xpalloc();
                    rsp20 = rsp20 - 8 + 8;
                    r10_21 = r10_21;
                    r11_22 = r11_22;
                    r14_7 = rax25;
                }
                rdx26 = r11_22;
                rdi27 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_7) + reinterpret_cast<uint64_t>(rbx9));
                rsi3 = r10_21;
                fun_3ab0(rdi27, rsi3, rdx26, rdi27, rsi3, rdx26);
                rsp13 = rsp20 - 8 + 8;
                r11_28 = r11_22;
            } else {
                r9_29 = rax15;
                if (!r13_10) {
                    rsi3 = reinterpret_cast<void**>(rsp20 + 48);
                    *reinterpret_cast<int32_t*>(&r8_23) = 16;
                    *reinterpret_cast<int32_t*>(&r8_23 + 4) = 0;
                    rcx24 = reinterpret_cast<void**>(0xffffffffffffffff);
                    rax30 = xpalloc();
                    rsp20 = rsp20 - 8 + 8;
                    r10_21 = r10_21;
                    r9_29 = rax15;
                    r11_22 = r11_22;
                    r15_11 = rax30;
                }
                rdx31 = r13_10;
                ++r13_10;
                rax32 = xstrdup(r10_21, rsi3);
                rsp13 = rsp20 - 8 + 8;
                rdx26 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx31) << 4) + reinterpret_cast<unsigned char>(r15_11));
                r11_28 = r11_22;
                *reinterpret_cast<void***>(rdx26) = rax32;
                *reinterpret_cast<void***>(rdx26 + 8) = r9_29->f0;
            }
            rbx9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx9) + reinterpret_cast<unsigned char>(r11_28));
        }
        if (*reinterpret_cast<void***>(r12_14)) 
            goto addr_13343_18; else 
            goto addr_131bc_19;
    }
    addr_13248_20:
    rax33 = v5 - g28;
    if (rax33) {
        fun_3880();
    } else {
        return r14_7;
    }
    addr_13343_18:
    fun_36a0(r15_11, r15_11);
    rdi34 = r14_7;
    *reinterpret_cast<int32_t*>(&r14_7) = 0;
    *reinterpret_cast<int32_t*>(&r14_7 + 4) = 0;
    fun_36a0(rdi34, rdi34);
    goto addr_13248_20;
    addr_131bc_19:
    if (!v6) {
        if (!rbx9) {
            rax35 = xirealloc(r14_7, reinterpret_cast<uint64_t>(rbx9) + 1, rdx26, rcx24, r8_23);
            r14_7 = rax35;
        }
        rbx36 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx9) + reinterpret_cast<unsigned char>(r14_7));
    } else {
        rbp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx9) + 1);
        if (r13_10) {
            rcx38 = v6;
            *reinterpret_cast<int32_t*>(&rbx39) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx39) + 4) = 0;
            fun_3760(r15_11, r13_10, 16, rcx38, r8_23);
            r13_40 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_10) << 4) + reinterpret_cast<unsigned char>(r15_11));
            rbp41 = r15_11;
            rax42 = ximalloc(rbp37, r13_10, 16, rcx38, r8_23);
            r14_7 = rax42;
            do {
                rsi43 = *reinterpret_cast<void***>(rbp41);
                r12_44 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_7) + reinterpret_cast<uint64_t>(rbx39));
                rbp41 = rbp41 + 16;
                rax45 = fun_3830(r12_44, rsi43, 16, rcx38, r8_23);
                rdi46 = *reinterpret_cast<void***>(rbp41 + 0xfffffffffffffff0);
                rbx39 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx39) + (reinterpret_cast<int64_t>(rax45) - reinterpret_cast<uint64_t>(r12_44)) + 1);
                fun_36a0(rdi46, rdi46);
            } while (r13_40 != rbp41);
            rbx36 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx39) + reinterpret_cast<unsigned char>(r14_7));
        } else {
            rax47 = ximalloc(rbp37, rsi3, rdx26, rcx24, r8_23);
            r14_7 = rax47;
            rbx36 = rax47;
        }
        fun_36a0(r15_11, r15_11);
    }
    *reinterpret_cast<void***>(rbx36) = reinterpret_cast<void**>(0);
    goto addr_13248_20;
}

void** opendir_safer();

void** streamsavedir(void** rdi, void** rsi);

void** fun_13373() {
    void** rax1;
    void** rsi2;
    int32_t esi3;
    void** rax4;
    int32_t eax5;

    __asm__("cli ");
    rax1 = opendir_safer();
    if (!rax1) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rsi2) = esi3;
        *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
        rax4 = streamsavedir(rax1, rsi2);
        eax5 = fun_3980(rax1, rsi2);
        if (eax5) {
            fun_36a0(rax4, rax4);
            return 0;
        } else {
            return rax4;
        }
    }
}

unsigned char fun_133e3(int32_t edi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(edi != -1));
}

struct s85 {
    unsigned char f0;
    unsigned char f1;
};

int64_t fun_133f3(struct s85* rdi) {
    uint32_t eax2;
    struct s85* rcx3;
    struct s85* rdx4;
    uint32_t eax5;

    __asm__("cli ");
    eax2 = rdi->f0;
    rcx3 = rdi;
    do {
        rdx4 = reinterpret_cast<struct s85*>(&rcx3->f1);
        if (*reinterpret_cast<signed char*>(&eax2) != 46) 
            goto addr_1342c_3;
        eax5 = rcx3->f1;
        if (*reinterpret_cast<signed char*>(&eax5) != 47) 
            break;
        while (eax2 = rdx4->f1, rcx3 = reinterpret_cast<struct s85*>(&rdx4->f1), *reinterpret_cast<signed char*>(&eax2) == 47) {
            rdx4 = rcx3;
        }
    } while (*reinterpret_cast<signed char*>(&eax2));
    goto addr_13440_8;
    if (!*reinterpret_cast<signed char*>(&eax5)) {
        addr_13440_8:
        return 0xffffff9c;
    } else {
        addr_1342c_3:
        goto fun_3be0;
    }
}

int64_t fun_13453(void** rdi) {
    int64_t rax2;
    int32_t eax3;
    void** rax4;
    int64_t rax5;
    int32_t eax6;
    int64_t rdx7;

    __asm__("cli ");
    rax2 = g28;
    eax3 = fun_39b0(rdi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
    rax4 = fun_3700();
    if (!eax3 || *reinterpret_cast<void***>(rax4) == 75) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(17);
        *reinterpret_cast<int32_t*>(&rax5) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    } else {
        eax6 = 0;
        *reinterpret_cast<unsigned char*>(&eax6) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax4) == 2));
        *reinterpret_cast<int32_t*>(&rax5) = -eax6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    }
    rdx7 = rax2 - g28;
    if (rdx7) {
        fun_3880();
    } else {
        return rax5;
    }
}

void fun_134d3() {
    __asm__("cli ");
}

void fun_134e3() {
    __asm__("cli ");
    goto fun_3be0;
}

void* fun_3970(void* rdi, int64_t rsi);

int64_t fun_3cb0(void* rdi, int64_t rsi, void** rdx);

void fun_37c0(int64_t rdi, void* rsi, void** rdx);

int64_t fun_13503(void** rdi, int32_t esi, int64_t rdx, int64_t rcx, void* r8) {
    int64_t v6;
    void** v7;
    int64_t v8;
    void* v9;
    int64_t rax10;
    int64_t v11;
    void** rax12;
    void** rsp13;
    void** v14;
    void** v15;
    void** v16;
    void** rax17;
    void** rdx18;
    void* rax19;
    void* r15_20;
    void* v21;
    void* v22;
    void* rax23;
    void** rsp24;
    uint32_t r9d25;
    int64_t rax26;
    int32_t v27;
    unsigned char v28;
    int32_t v29;
    int64_t rdx30;
    int64_t rax31;
    void** r13_32;
    uint64_t v33;
    int32_t r8d34;
    uint32_t r9d35;
    void** rcx36;
    void* rbx37;
    uint64_t rax38;
    uint32_t eax39;
    void* v40;
    void** v41;

    __asm__("cli ");
    v6 = rdx;
    v7 = rdi;
    v8 = rcx;
    v9 = r8;
    rax10 = g28;
    v11 = rax10;
    rax12 = fun_3700();
    rsp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88 - 8 + 8);
    v14 = rax12;
    v15 = *reinterpret_cast<void***>(rax12);
    v16 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 + 80) >> 4);
    rax17 = fun_3860(rdi);
    rdx18 = reinterpret_cast<void**>(esi + reinterpret_cast<uint64_t>(r8));
    if (reinterpret_cast<unsigned char>(rdx18) > reinterpret_cast<unsigned char>(rax17) || (rax19 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax17) - reinterpret_cast<unsigned char>(rdx18)), r15_20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax19)), v21 = rax19, v22 = r15_20, rax23 = fun_3970(r15_20, "X"), rsp24 = rsp13 - 8 + 8 - 8 + 8, r9d25 = reinterpret_cast<uint1_t>(rcx == 0x13450), reinterpret_cast<uint64_t>(rax23) < reinterpret_cast<uint64_t>(r8))) {
        *reinterpret_cast<void***>(v14) = reinterpret_cast<void**>(22);
        *reinterpret_cast<int32_t*>(&rax26) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
    } else {
        v27 = 0x3a2f8;
        v28 = *reinterpret_cast<unsigned char*>(&r9d25);
        v29 = 0;
        goto addr_135f0_4;
    }
    addr_13739_5:
    rdx30 = v11 - g28;
    if (rdx30) {
        fun_3880();
    } else {
        return rax26;
    }
    addr_1375b_8:
    *reinterpret_cast<void***>(v14) = v15;
    goto addr_13739_5;
    addr_13734_9:
    *reinterpret_cast<int32_t*>(&rax26) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
    goto addr_13739_5;
    while (1) {
        *reinterpret_cast<int32_t*>(&rdx18) = 1;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        rax31 = fun_3cb0(rsp24 + 88, 8, 1);
        rsp24 = rsp24 - 8 + 8;
        if (rax31 != 8) {
            while (1) {
                fun_37c0(1, rsp24 + 96, rdx18);
                rsp24 = rsp24 - 8 + 8;
                r13_32 = reinterpret_cast<void**>((v33 ^ reinterpret_cast<unsigned char>(r13_32)) * 0x27bb2ee687b0b0fd + 0xb504f32d);
                if (reinterpret_cast<unsigned char>(r13_32) > reinterpret_cast<unsigned char>(0xf49998db0aa753ff)) 
                    break;
                addr_136e8_12:
                r8d34 = 9;
                r9d35 = 1;
                while (1) {
                    rcx36 = r13_32;
                    rbx37 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx37) + 1);
                    rdx18 = reinterpret_cast<void**>(__intrinsic() >> 4);
                    v16 = rdx18;
                    r13_32 = rdx18;
                    rax38 = reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx18) << 5) - reinterpret_cast<unsigned char>(rdx18);
                    eax39 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") + (reinterpret_cast<unsigned char>(rcx36) - (rax38 + rax38)));
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx37) + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&eax39);
                    if (rbx37 == v40) {
                        v28 = *reinterpret_cast<unsigned char*>(&r9d35);
                        v29 = r8d34;
                        do {
                            rax26 = reinterpret_cast<int64_t>(v8(v7, v6));
                            rsp24 = rsp24 - 8 + 8;
                            if (*reinterpret_cast<int32_t*>(&rax26) >= 0) 
                                goto addr_1375b_8;
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v14) == 17)) 
                                goto addr_13734_9;
                            --v27;
                            if (!v27) 
                                goto addr_13734_9;
                            addr_135f0_4:
                        } while (!v9);
                        r13_32 = v16;
                        rbx37 = v22;
                        r9d35 = v28;
                        v40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) + reinterpret_cast<uint64_t>(v9) + reinterpret_cast<uint64_t>(v21));
                        r8d34 = v29;
                    }
                    if (!r8d34) 
                        break;
                    --r8d34;
                }
                if (*reinterpret_cast<unsigned char*>(&r9d35)) 
                    break;
            }
        } else {
            r13_32 = v41;
            if (reinterpret_cast<unsigned char>(r13_32) > reinterpret_cast<unsigned char>(0xf49998db0aa753ff)) 
                continue;
            goto addr_136e8_12;
        }
    }
}

int32_t fun_13783(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    eax2 = try_tempname_len(rdi);
    return eax2;
}

int32_t fun_137b3(void** rdi) {
    int64_t rax2;
    int32_t eax3;
    int64_t rdx4;

    __asm__("cli ");
    rax2 = g28;
    eax3 = try_tempname_len(rdi);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_3880();
    } else {
        return eax3;
    }
}

void fun_13813() {
    __asm__("cli ");
    goto try_tempname_len;
}

int32_t dup_safer();

int64_t fun_13823(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_3700();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_3950();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

uint64_t fun_13883(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_13893(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

/* ht.1 */
void** ht_1 = reinterpret_cast<void**>(0);

/* new_dst_res.0 */
void** new_dst_res_0 = reinterpret_cast<void**>(0);

int64_t fun_138a3(int32_t edi, void** rsi, void** rdx, void** rcx, uint32_t r8d) {
    void* rsp6;
    void** r14_7;
    void** r13_8;
    void** r9_9;
    void** r10_10;
    int32_t v11;
    int32_t r11d12;
    int64_t rax13;
    int64_t v14;
    unsigned char bpl15;
    void** r8d16;
    int32_t r12d17;
    void* eax18;
    int64_t r12_19;
    int64_t rax20;
    int32_t eax21;
    void** v22;
    void** r8_23;
    void** r15_24;
    void** rbx25;
    void** rax26;
    void** rsi27;
    int64_t rax28;
    void** rax29;
    void** rdi30;
    void** rax31;
    void** rax32;
    int1_t zf33;
    void** rdx34;
    void** rdx35;
    void** rax36;
    void** eax37;
    int1_t zf38;
    void** v39;
    void** rax40;
    void** rdx41;
    uint64_t rdi42;
    void** rcx43;
    uint64_t v44;
    void** v45;
    uint16_t v46;
    int64_t rdi47;
    int64_t rsi48;
    int64_t rbx49;
    int32_t eax50;
    int64_t rax51;
    int64_t rax52;
    int32_t esi53;
    uint32_t edx54;
    int64_t rdi55;
    int32_t ecx56;
    int32_t eax57;
    int32_t ecx58;
    int32_t esi59;
    void** v60;
    void** rax61;
    int64_t r8_62;
    int64_t r8_63;
    int32_t r8d64;
    int64_t rax65;
    int64_t rax66;
    int32_t r8d67;
    int64_t rax68;
    int64_t rax69;
    int32_t eax70;
    int32_t v71;
    int32_t edx72;
    int64_t rbx73;
    void** v74;
    void** v75;
    void** r9d76;
    void** v77;
    int32_t r13d78;
    void* edi79;
    void** r8d80;
    void** edi81;
    int64_t rax82;
    int64_t rax83;
    int64_t rax84;
    int64_t rax85;
    int64_t rax86;
    int32_t eax87;
    int64_t rax88;
    int64_t rax89;
    int32_t eax90;
    int64_t rax91;
    int64_t rax92;
    int32_t eax93;
    int64_t rdi94;
    void* rbx95;
    int64_t rax96;
    void** rdi97;
    void** v98;
    void** rdx99;
    void** v100;
    int32_t v101;
    int32_t eax102;
    uint64_t rax103;
    uint64_t v104;
    uint64_t rsi105;
    uint64_t v106;
    int64_t rdi107;
    uint64_t v108;
    uint64_t v109;
    uint32_t edx110;
    void** esi111;
    int32_t ecx112;
    int64_t rax113;
    uint32_t eax114;
    void* r12d115;
    uint64_t rax116;

    __asm__("cli ");
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x138);
    r14_7 = *reinterpret_cast<void***>(rdx + 88);
    r13_8 = *reinterpret_cast<void***>(rcx + 88);
    r9_9 = *reinterpret_cast<void***>(rcx + 96);
    r10_10 = *reinterpret_cast<void***>(rdx + 96);
    v11 = edi;
    r11d12 = *reinterpret_cast<int32_t*>(&r9_9);
    rax13 = g28;
    v14 = rax13;
    bpl15 = reinterpret_cast<uint1_t>(r14_7 == r13_8);
    r8d16 = reinterpret_cast<void**>(r8d & 1);
    if (!r8d16) {
        while (1) {
            addr_139d0_2:
            r12d17 = 0;
            *reinterpret_cast<unsigned char*>(&r12d17) = reinterpret_cast<uint1_t>(r11d12 < *reinterpret_cast<int32_t*>(&r10_10));
            eax18 = reinterpret_cast<void*>(0);
            *reinterpret_cast<unsigned char*>(&eax18) = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r13_8) < reinterpret_cast<signed char>(r14_7));
            *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>((r12d17 - static_cast<unsigned char>(reinterpret_cast<uint1_t>(r11d12 > *reinterpret_cast<int32_t*>(&r10_10))) & -static_cast<uint32_t>(bpl15)) + (reinterpret_cast<int32_t>(eax18) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r13_8) > reinterpret_cast<signed char>(r14_7)))));
            addr_139ff_3:
            rax20 = v14 - g28;
            if (!rax20) 
                break;
            fun_3880();
            addr_140bb_5:
            *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(10);
            addr_13e9b_6:
            eax21 = 0;
            *reinterpret_cast<unsigned char*>(&eax21) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(&r12_19) == 0x77359400);
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) & reinterpret_cast<uint64_t>(static_cast<int64_t>(~eax21)));
            __asm__("cdq ");
            r11d12 = *reinterpret_cast<int32_t*>(&r9_9) - r11d12 % reinterpret_cast<signed char>(*reinterpret_cast<void***>(&r12_19));
            bpl15 = reinterpret_cast<uint1_t>(r14_7 == r13_8);
            addr_13ec8_7:
            *reinterpret_cast<void***>(v22 + 8) = *reinterpret_cast<void***>(&r12_19);
            *reinterpret_cast<signed char*>(v22 + 12) = 1;
        }
    } else {
        if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r10_10) == *reinterpret_cast<int32_t*>(&r9_9))) & bpl15) {
            *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(0);
            goto addr_139ff_3;
        }
        if (reinterpret_cast<signed char>(r13_8 + 0xffffffffffffffff) > reinterpret_cast<signed char>(r14_7)) 
            goto addr_13a38_11;
        *reinterpret_cast<void***>(&r12_19) = r8d16;
        if (reinterpret_cast<signed char>(r14_7 + 0xffffffffffffffff) > reinterpret_cast<signed char>(r13_8)) 
            goto addr_139ff_3;
        r8_23 = ht_1;
        r15_24 = rsi;
        rbx25 = rdx;
        if (r8_23) 
            goto addr_13936_14;
        rcx = reinterpret_cast<void**>(0x13890);
        rdx = reinterpret_cast<void**>(0x13880);
        rax26 = hash_initialize(16);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
        r11d12 = *reinterpret_cast<int32_t*>(&r9_9);
        r10_10 = r10_10;
        ht_1 = rax26;
        r9_9 = r9_9;
        r8_23 = rax26;
        if (!rax26) 
            goto addr_13a92_16;
        addr_13936_14:
        rsi27 = new_dst_res_0;
        if (!rsi27) 
            goto addr_13f40_17; else 
            goto addr_13946_18;
    }
    *reinterpret_cast<void***>(&rax28) = *reinterpret_cast<void***>(&r12_19);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
    return rax28;
    addr_13f40_17:
    rax29 = fun_3ae0(16, rsi27, rdx, 16, rsi27, rdx);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
    r11d12 = r11d12;
    r10_10 = r10_10;
    new_dst_res_0 = rax29;
    r9_9 = r9_9;
    rsi27 = rax29;
    if (!rax29) {
        rdi30 = ht_1;
        goto addr_13ef0_21;
    } else {
        *reinterpret_cast<void***>(rax29 + 8) = reinterpret_cast<void**>(0x77359400);
        r8_23 = r8_23;
        *reinterpret_cast<signed char*>(rax29 + 12) = 0;
    }
    addr_13946_18:
    rax31 = *reinterpret_cast<void***>(rbx25);
    *reinterpret_cast<void***>(rsi27) = rax31;
    rax32 = hash_insert(r8_23, rsi27, rdx, rcx, r8_23);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
    r11d12 = r11d12;
    r10_10 = r10_10;
    v22 = rax32;
    r9_9 = r9_9;
    if (rax32) {
        zf33 = new_dst_res_0 == rax32;
        if (zf33) {
            new_dst_res_0 = reinterpret_cast<void**>(0);
            goto addr_1398d_25;
        }
    }
    rdi30 = ht_1;
    if (!rdi30) {
        addr_13a92_16:
        rdx34 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 96);
    } else {
        addr_13ef0_21:
        rdx35 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 96);
        rax36 = hash_lookup(rdi30, rdx35, rdx35, rcx, r8_23);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
        rdx34 = rdx35;
        r11d12 = r11d12;
        v22 = rax36;
        r10_10 = r10_10;
        r9_9 = r9_9;
        if (rax36) {
            addr_1398d_25:
            eax37 = *reinterpret_cast<void***>(v22 + 8);
            zf38 = *reinterpret_cast<signed char*>(v22 + 12) == 0;
            v39 = eax37;
            if (zf38) {
                addr_13ab1_27:
                rax40 = *reinterpret_cast<void***>(rbx25 + 72);
                rdx41 = *reinterpret_cast<void***>(rbx25 + 80);
                rdi42 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r10_10)));
                rcx43 = *reinterpret_cast<void***>(rbx25 + 0x70);
                v44 = rdi42;
                v45 = rax40;
                v46 = *reinterpret_cast<uint16_t*>(rbx25 + 0x68);
                rdi47 = reinterpret_cast<int64_t>(rdi42 * 0x66666667) >> 34;
                rsi48 = *reinterpret_cast<int32_t*>(&rdx41) * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rbx49) = *reinterpret_cast<int32_t*>(&rsi48) - (*reinterpret_cast<int32_t*>(&rdx41) >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx49) + 4) = 0;
                eax50 = static_cast<int32_t>(rbx49 + rbx49 * 4);
                rax51 = *reinterpret_cast<int32_t*>(&rcx43) * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rax52) = *reinterpret_cast<int32_t*>(&rax51) - (*reinterpret_cast<int32_t*>(&rcx43) >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax52) + 4) = 0;
                esi53 = static_cast<int32_t>(rax52 + rax52 * 4);
                edx54 = *reinterpret_cast<int32_t*>(&rdx41) - reinterpret_cast<uint32_t>(eax50 + eax50) | *reinterpret_cast<int32_t*>(&rcx43) - reinterpret_cast<uint32_t>(esi53 + esi53);
                *reinterpret_cast<int32_t*>(&rdi55) = *reinterpret_cast<int32_t*>(&rdi47) - (*reinterpret_cast<int32_t*>(&r10_10) >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
                ecx56 = static_cast<int32_t>(rdi55 + rdi55 * 4);
                if (*reinterpret_cast<int32_t*>(&r10_10) - reinterpret_cast<uint32_t>(ecx56 + ecx56) | edx54) 
                    goto addr_13ec8_7; else 
                    goto addr_13b39_28;
            } else {
                eax57 = 0;
                *reinterpret_cast<unsigned char*>(&eax57) = reinterpret_cast<uint1_t>(eax37 == 0x77359400);
                r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) & reinterpret_cast<uint64_t>(static_cast<int64_t>(~eax57)));
                __asm__("cdq ");
                r11d12 = *reinterpret_cast<int32_t*>(&r9_9) - r11d12 % reinterpret_cast<signed char>(eax37);
                bpl15 = reinterpret_cast<uint1_t>(r14_7 == r13_8);
                goto addr_139d0_2;
            }
        }
    }
    v39 = reinterpret_cast<void**>(0x77359400);
    v22 = rdx34;
    goto addr_13ab1_27;
    addr_13b39_28:
    ecx58 = *reinterpret_cast<int32_t*>(&rax52);
    esi59 = *reinterpret_cast<int32_t*>(&rdi55);
    if (reinterpret_cast<signed char>(v39) <= reinterpret_cast<signed char>(10)) {
        v60 = reinterpret_cast<void**>(10);
        *reinterpret_cast<void***>(v22 + 8) = reinterpret_cast<void**>(10);
        rax61 = r13_8;
    } else {
        __asm__("cdq ");
        v60 = reinterpret_cast<void**>(10);
        r8_62 = *reinterpret_cast<int32_t*>(&rax52) * 0x66666667 >> 34;
        *reinterpret_cast<uint32_t*>(&r8_63) = *reinterpret_cast<int32_t*>(&r8_62) - edx54;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_63) + 4) = 0;
        r8d64 = static_cast<int32_t>(r8_63 + r8_63 * 4);
        rax65 = *reinterpret_cast<int32_t*>(&rbx49) * 0x66666667 >> 34;
        *reinterpret_cast<int32_t*>(&rax66) = *reinterpret_cast<int32_t*>(&rax65) - (*reinterpret_cast<int32_t*>(&rbx49) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax66) + 4) = 0;
        r8d67 = static_cast<int32_t>(rax66 + rax66 * 4);
        rax68 = *reinterpret_cast<int32_t*>(&rdi55) * 0x66666667 >> 34;
        *reinterpret_cast<int32_t*>(&rax69) = *reinterpret_cast<int32_t*>(&rax68) - (*reinterpret_cast<int32_t*>(&rdi55) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
        eax70 = static_cast<int32_t>(rax69 + rax69 * 4);
        if (*reinterpret_cast<int32_t*>(&rdi55) - reinterpret_cast<uint32_t>(eax70 + eax70) | (*reinterpret_cast<int32_t*>(&rax52) - reinterpret_cast<uint32_t>(r8d64 + r8d64) | *reinterpret_cast<int32_t*>(&rbx49) - reinterpret_cast<uint32_t>(r8d67 + r8d67))) {
            *reinterpret_cast<void***>(v22 + 8) = reinterpret_cast<void**>(10);
            rax61 = r13_8;
        } else {
            v71 = r11d12;
            edx72 = *reinterpret_cast<int32_t*>(&rbx49);
            *reinterpret_cast<void***>(&rbx73) = reinterpret_cast<void**>(10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx73) + 4) = 0;
            v74 = r14_7;
            v75 = r9_9;
            r9d76 = v39;
            v77 = r13_8;
            r13d78 = 8;
            do {
                edi79 = reinterpret_cast<void*>(static_cast<uint32_t>(rbx73 + rbx73 * 4));
                r8d80 = *reinterpret_cast<void***>(&rbx73);
                edi81 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(edi79) + reinterpret_cast<uint32_t>(edi79));
                *reinterpret_cast<void***>(&rbx73) = edi81;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx73) + 4) = 0;
                rax82 = edx72 * 0x66666667 >> 34;
                edx72 = *reinterpret_cast<int32_t*>(&rax82) - (edx72 >> 31);
                rax83 = ecx58 * 0x66666667 >> 34;
                ecx58 = *reinterpret_cast<int32_t*>(&rax83) - (ecx58 >> 31);
                rax84 = esi59 * 0x66666667 >> 34;
                esi59 = *reinterpret_cast<int32_t*>(&rax84) - (esi59 >> 31);
                if (reinterpret_cast<signed char>(edi81) >= reinterpret_cast<signed char>(r9d76)) 
                    goto addr_13fd0_37;
                rax85 = edx72 * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rax86) = *reinterpret_cast<int32_t*>(&rax85) - (edx72 >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax86) + 4) = 0;
                eax87 = static_cast<int32_t>(rax86 + rax86 * 4);
                rax88 = ecx58 * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rax89) = *reinterpret_cast<int32_t*>(&rax88) - (ecx58 >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0;
                eax90 = static_cast<int32_t>(rax89 + rax89 * 4);
                rax91 = esi59 * 0x66666667 >> 34;
                *reinterpret_cast<int32_t*>(&rax92) = *reinterpret_cast<int32_t*>(&rax91) - (esi59 >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax92) + 4) = 0;
                eax93 = static_cast<int32_t>(rax92 + rax92 * 4);
                if (esi59 - reinterpret_cast<uint32_t>(eax93 + eax93) | (edx72 - reinterpret_cast<uint32_t>(eax87 + eax87) | ecx58 - reinterpret_cast<uint32_t>(eax90 + eax90))) 
                    goto addr_14010_39;
                --r13d78;
            } while (r13d78);
            goto addr_13cc1_41;
        }
    }
    addr_13d01_42:
    if (reinterpret_cast<signed char>(r14_7) > reinterpret_cast<signed char>(r13_8)) 
        goto addr_139ff_3;
    if (*reinterpret_cast<int32_t*>(&r10_10) < *reinterpret_cast<int32_t*>(&r9_9)) 
        goto addr_13d18_44;
    if (bpl15) 
        goto addr_139ff_3;
    addr_13d18_44:
    if (reinterpret_cast<signed char>(r14_7) < reinterpret_cast<signed char>(rax61) || r14_7 == rax61 && *reinterpret_cast<int32_t*>(&r9_9) - r11d12 % reinterpret_cast<signed char>(v60) > *reinterpret_cast<int32_t*>(&r10_10)) {
        addr_13a38_11:
        *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(0xffffffff);
        goto addr_139ff_3;
    } else {
        *reinterpret_cast<int32_t*>(&rdi94) = v11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi94) + 4) = 0;
        rbx95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) + 0x70);
        rax96 = fun_36e0(rdi94, r15_24, rbx95, rdi94, r15_24, rbx95);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
        r10_10 = r10_10;
        r11d12 = r11d12;
        r9_9 = r9_9;
        if (*reinterpret_cast<int32_t*>(&rax96)) 
            goto addr_1402a_47;
        *reinterpret_cast<int32_t*>(&rdi97) = v11;
        *reinterpret_cast<int32_t*>(&rdi97 + 4) = 0;
        v98 = r9_9;
        rdx99 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0x90);
        v100 = r10_10;
        v101 = r11d12;
        eax102 = fun_3d10(rdi97, r15_24, rdx99, rdi97, r15_24, rdx99);
        r11d12 = v101;
        r10_10 = v100;
        r9_9 = v98;
        rax103 = v104;
        rsi105 = v106;
        if (!(reinterpret_cast<uint64_t>(static_cast<int64_t>(eax102)) | (v44 ^ rsi105 | rax103 ^ reinterpret_cast<unsigned char>(r14_7)))) 
            goto addr_13e1e_49;
    }
    *reinterpret_cast<int32_t*>(&rdi107) = v11;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi107) + 4) = 0;
    fun_36e0(rdi107, r15_24, rbx95, rdi107, r15_24, rbx95);
    r11d12 = v101;
    r10_10 = v100;
    r9_9 = v98;
    if (eax102) {
        addr_1402a_47:
        *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(0xfffffffe);
        goto addr_139ff_3;
    } else {
        rax103 = v108;
        rsi105 = v109;
    }
    addr_13e1e_49:
    edx110 = (*reinterpret_cast<uint32_t*>(&rax103) & 1) * 0x3b9aca00 + *reinterpret_cast<uint32_t*>(&rsi105);
    __asm__("ror eax, 1");
    if (edx110 * 0xcccccccd + 0x19999998 > 0x19999998) 
        goto addr_13e9b_6;
    esi111 = v60;
    if (esi111 == 10) 
        goto addr_140bb_5;
    ecx112 = 9;
    *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(10);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_19) + 4) = 0;
    while (1) {
        rax113 = reinterpret_cast<int32_t>(edx110) * 0x66666667 >> 34;
        eax114 = *reinterpret_cast<int32_t*>(&rax113) - reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(edx110) >> 31);
        edx110 = eax114;
        __asm__("ror eax, 1");
        if (eax114 * 0xcccccccd + 0x19999998 > 0x19999998) 
            goto addr_13e9b_6;
        --ecx112;
        if (!ecx112) 
            break;
        r12d115 = reinterpret_cast<void*>(static_cast<uint32_t>(r12_19 + r12_19 * 4));
        *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(r12d115) + reinterpret_cast<uint32_t>(r12d115));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_19) + 4) = 0;
        if (*reinterpret_cast<void***>(&r12_19) == esi111) 
            goto addr_13e9b_6;
    }
    *reinterpret_cast<void***>(&r12_19) = reinterpret_cast<void**>(0x77359400);
    goto addr_13e9b_6;
    addr_13fd0_37:
    r13_8 = v77;
    r11d12 = v71;
    v60 = edi81;
    r14_7 = v74;
    r9_9 = v75;
    addr_13fe8_58:
    *reinterpret_cast<void***>(v22 + 8) = *reinterpret_cast<void***>(&rbx73);
    rax61 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(~static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r8d80 == 0xbebc200)))))) & reinterpret_cast<unsigned char>(r13_8));
    goto addr_13d01_42;
    addr_14010_39:
    r13_8 = v77;
    r11d12 = v71;
    v60 = edi81;
    r14_7 = v74;
    r9_9 = v75;
    goto addr_13fe8_58;
    addr_13cc1_41:
    r14_7 = v74;
    r13_8 = v77;
    r11d12 = v71;
    rax116 = reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v45) | reinterpret_cast<unsigned char>(r14_7)) | v46;
    r9_9 = v75;
    if (!(*reinterpret_cast<unsigned char*>(&rax116) & 1)) {
        *reinterpret_cast<void***>(v22 + 8) = reinterpret_cast<void**>(0x77359400);
        rax61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) & 0xfffffffffffffffe);
        v60 = reinterpret_cast<void**>(0x77359400);
        goto addr_13d01_42;
    } else {
        v60 = reinterpret_cast<void**>(0x3b9aca00);
        *reinterpret_cast<void***>(v22 + 8) = reinterpret_cast<void**>(0x3b9aca00);
        rax61 = r13_8;
        goto addr_13d01_42;
    }
}

void fun_140e3(int64_t rdi, int64_t rsi, int64_t rdx, int32_t ecx) {
    __asm__("cli ");
    goto utimecmpat;
}

struct s86 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    void** f28;
    signed char[7] pad48;
    int64_t f30;
};

void fun_3a40(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_14103(void** rdi, void** rsi, void** rdx, void** rcx, struct s86* r8, void** r9) {
    void** r12_7;
    void** v8;
    void** v9;
    void** v10;
    int64_t v11;
    void** v12;
    void** v13;
    void** v14;
    int64_t v15;
    void** rax16;
    void** v17;
    void** v18;
    void** v19;
    int64_t v20;
    void** rax21;
    void** v22;
    void** v23;
    void** v24;
    int64_t v25;
    int64_t r9_26;
    void** r8_27;
    void** rcx28;
    void** r15_29;
    void** r14_30;
    void** r13_31;
    void** r12_32;
    void** rax33;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_3ca0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11);
    } else {
        r9 = rcx;
        fun_3ca0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v12, v13, v14, v15);
    }
    rax16 = fun_3840();
    fun_3ca0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax16, 0x7e6, r9, v17, v18, v19, v20);
    fun_3a40(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax16, 0x7e6, r9);
    rax21 = fun_3840();
    fun_3ca0(rdi, 1, rax21, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v22, v23, v24, v25);
    fun_3a40(10, rdi, rax21, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r9_26 = r8->f30;
        r8_27 = r8->f28;
        rcx28 = r8->f20;
        r15_29 = r8->f18;
        r14_30 = r8->f10;
        r13_31 = r8->f8;
        r12_32 = r8->f0;
        rax33 = fun_3840();
        fun_3ca0(rdi, 1, rax33, r12_32, r13_31, r14_30, r15_29, rcx28, r8_27, r9_26, rdi, 1, rax33, r12_32, r13_31, r14_30, r15_29, rcx28, r8_27, r9_26);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x18248 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x18248;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_14573() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s87 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_14593(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s87* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s87* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_3880();
    } else {
        return;
    }
}

void fun_14633(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_146d6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_146e0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_3880();
    } else {
        return;
    }
    addr_146d6_5:
    goto addr_146e0_7;
}

void fun_14713() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_3a40(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_3840();
    fun_3b80(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_3840();
    fun_3b80(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_3840();
    goto fun_3b80;
}

/* initialized.1 */
signed char initialized_1 = 0;

/* can_write.0 */
unsigned char can_write_0 = 0;

int64_t fun_147b3() {
    int1_t zf1;
    int64_t rax2;
    int64_t rax3;

    __asm__("cli ");
    zf1 = initialized_1 == 0;
    if (zf1) {
        rax2 = fun_3910();
        initialized_1 = 1;
        *reinterpret_cast<unsigned char*>(&rax2) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax2) == 0);
        can_write_0 = *reinterpret_cast<unsigned char*>(&rax2);
        return rax2;
    } else {
        *reinterpret_cast<uint32_t*>(&rax3) = can_write_0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    }
}

int64_t fun_3cc0();

void fun_147f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_3cc0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_3780();

void fun_14813(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3780();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_14853(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ae0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14873(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ae0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14893(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ae0(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_148b3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;

    __asm__("cli ");
    rax5 = fun_3b50(rdi, rsi, rdx, rcx);
    if (rax5 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_148e3(void** rdi, uint64_t rsi, void** rdx, void** rcx) {
    uint64_t rax5;
    void** rax6;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(rsi == 0);
    rax6 = fun_3b50(rdi, rsi | rax5, rdx, rcx);
    if (!rax6) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14913(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3780();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_14953() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_3780();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14993(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_3780();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_149c3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_3780();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14a13(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_3780();
            if (rax5) 
                break;
            addr_14a5d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_14a5d_5;
        rax8 = fun_3780();
        if (rax8) 
            goto addr_14a46_9;
        if (rbx4) 
            goto addr_14a5d_5;
        addr_14a46_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_14aa3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_3780();
            if (rax8) 
                break;
            addr_14aea_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_14aea_5;
        rax11 = fun_3780();
        if (rax11) 
            goto addr_14ad2_9;
        if (!rbx6) 
            goto addr_14ad2_9;
        if (r12_4) 
            goto addr_14aea_5;
        addr_14ad2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_14b33(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_14bdd_9:
            rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8));
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<unsigned char>(rdx));
            if (!r13_6) {
                addr_14bf0_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_14b90_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_14bb6_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_14c04_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_14bad_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_14c04_14;
            addr_14bad_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_14c04_14;
            addr_14bb6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_3b50(rdi7, rsi9, rdx, rcx10);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_14c04_14;
            if (!rbp13) 
                break;
            addr_14c04_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_14bdd_9;
        } else {
            if (!r13_6) 
                goto addr_14bf0_10;
            goto addr_14b90_11;
        }
    }
}

void fun_14c33(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_39f0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14c63(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_39f0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14c93(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_39f0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14cb3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_39f0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_14cd3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ae0(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_3ab0;
    }
}

void fun_14d13(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ae0(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_3ab0;
    }
}

void fun_14d53(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3ae0(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_3ab0;
    }
}

void fun_14d93(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_3860(rdi);
    rax5 = fun_3ae0(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_3ab0;
    }
}

void fun_14dd3() {
    __asm__("cli ");
    fun_3840();
    fun_3bd0();
    fun_36f0();
}

int64_t rpl_fts_open();

void fun_14e13() {
    int64_t rax1;
    void** rax2;

    __asm__("cli ");
    rax1 = rpl_fts_open();
    if (!rax1) {
        rax2 = fun_3700();
        if (*reinterpret_cast<void***>(rax2) != 22) {
            xalloc_die();
        }
        fun_38f0("errno != EINVAL", "lib/xfts.c", 41, "xfts_open");
    } else {
        return;
    }
}

struct s88 {
    signed char[72] pad72;
    uint32_t f48;
};

struct s89 {
    signed char[88] pad88;
    int64_t f58;
};

int64_t fun_14e63(struct s88* rdi, struct s89* rsi) {
    int32_t r8d3;
    uint32_t eax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    r8d3 = 1;
    eax4 = rdi->f48 & 17;
    if (eax4 == 16 || (r8d3 = 0, eax4 != 17)) {
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(!!rsi->f58);
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

int64_t fun_3a00(void* rdi, void* rsi, int64_t rdx, void** rcx);

int32_t fun_3a70(int64_t rdi, void* rsi, int64_t rdx, void** rcx);

int64_t fun_14ea3() {
    int32_t r12d1;
    void* rsp2;
    void** rcx3;
    int64_t rax4;
    void* rsi5;
    int64_t rax6;
    signed char* rax7;
    int32_t eax8;
    int64_t rax9;
    int64_t rax10;

    __asm__("cli ");
    r12d1 = 0;
    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32);
    rcx3 = stdin;
    rax4 = g28;
    rsi5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 16);
    rax6 = fun_3a00(reinterpret_cast<int64_t>(rsp2) + 8, rsi5, 10, rcx3);
    if (!(reinterpret_cast<uint1_t>(rax6 < 0) | reinterpret_cast<uint1_t>(rax6 == 0))) {
        rax7 = reinterpret_cast<signed char*>(rax6 - 1);
        if (*rax7 == 10) {
            *rax7 = 0;
        }
        eax8 = fun_3a70(0, rsi5, 10, rcx3);
        *reinterpret_cast<unsigned char*>(&r12d1) = reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(eax8 < 0) | reinterpret_cast<uint1_t>(eax8 == 0)));
    }
    fun_36a0(0, 0);
    rax9 = rax4 - g28;
    if (rax9) {
        fun_3880();
    } else {
        *reinterpret_cast<int32_t*>(&rax10) = r12d1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        return rax10;
    }
}

void fun_14f43() {
    __asm__("cli ");
    return;
}

int64_t fun_14f53() {
    int32_t* rcx1;
    int32_t edx2;

    __asm__("cli ");
    *rcx1 = edx2;
    return 0;
}

int32_t fun_3ba0(int64_t rdi, int64_t rsi);

void fun_14f63(int64_t rdi, int32_t esi, int32_t edx) {
    __asm__("cli ");
    if (esi == -1) {
        goto fun_3ba0;
    } else {
        goto fun_3b90;
    }
}

int64_t fun_14f83(int32_t* rdi, int64_t rsi, int32_t edx) {
    int64_t rsi4;
    int32_t eax5;
    uint32_t eax6;
    int64_t rax7;
    int64_t rdi8;
    int32_t eax9;
    uint32_t eax10;
    int64_t rax11;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rsi4) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi4) + 4) = 0;
    if (edx == -1) {
        eax5 = fun_3ba0(rsi, rsi4);
        eax6 = reinterpret_cast<uint32_t>(-eax5);
        *reinterpret_cast<uint32_t*>(&rax7) = eax6 - (eax6 + reinterpret_cast<uint1_t>(eax6 < eax6 + reinterpret_cast<uint1_t>(!!eax5)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    } else {
        *reinterpret_cast<int32_t*>(&rdi8) = edx;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        eax9 = fun_3b90(rdi8, rdi8);
        eax10 = reinterpret_cast<uint32_t>(-eax9);
        *reinterpret_cast<uint32_t*>(&rax11) = eax10 - (eax10 + reinterpret_cast<uint1_t>(eax10 < eax10 + reinterpret_cast<uint1_t>(!!eax9)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        return rax11;
    }
}

void fun_14fc3() {
    __asm__("cli ");
    goto usage;
}

int64_t fun_14fd3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_3860(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_3710(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_15053_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_150a0_6; else 
                    continue;
            } else {
                rax18 = fun_3860(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_150d0_8;
                if (v16 == -1) 
                    goto addr_1508e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_15053_5;
            } else {
                eax19 = fun_39c0(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_15053_5;
            }
            addr_1508e_10:
            v16 = rbx15;
            goto addr_15053_5;
        }
    }
    addr_150b5_16:
    return v12;
    addr_150a0_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_150b5_16;
    addr_150d0_8:
    v12 = rbx15;
    goto addr_150b5_16;
}

int64_t fun_150e3(void** rdi, void*** rsi) {
    void** r12_3;
    void** rdi4;
    void*** rbp5;
    int64_t rbx6;
    int64_t rax7;

    __asm__("cli ");
    r12_3 = rdi;
    rdi4 = *rsi;
    if (!rdi4) {
        addr_15128_2:
        return -1;
    } else {
        rbp5 = rsi;
        *reinterpret_cast<int32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        do {
            rax7 = fun_3a10(rdi4, r12_3);
            if (!*reinterpret_cast<int32_t*>(&rax7)) 
                break;
            ++rbx6;
            rdi4 = rbp5[rbx6 * 8];
        } while (rdi4);
        goto addr_15128_2;
    }
    return rbx6;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

void fun_15143(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_3840();
    } else {
        fun_3840();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_3bd0;
}

void fun_151d3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rdx9;
    void** rbp10;
    int64_t v11;
    int64_t rbx12;
    void** r14_13;
    void** v14;
    void** rax15;
    void** rsi16;
    void** r15_17;
    int64_t rbx18;
    uint32_t eax19;
    void** rax20;
    void** rdi21;
    void** v22;
    void** v23;
    void** rax24;
    void** rdi25;
    void** v26;
    void** v27;
    void** rdi28;
    void** rax29;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    r12_8 = rdx;
    *reinterpret_cast<int32_t*>(&rdx9) = 5;
    *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
    rbp10 = rsi;
    v11 = rbx12;
    r14_13 = stderr;
    v14 = rdi;
    rax15 = fun_3840();
    rsi16 = r14_13;
    fun_39e0(rax15, rsi16, 5, rcx, r8);
    r15_17 = *reinterpret_cast<void***>(rdi);
    *reinterpret_cast<int32_t*>(&rbx18) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx18) + 4) = 0;
    if (r15_17) {
        do {
            if (!rbx18 || (rdx9 = r12_8, rsi16 = rbp10, eax19 = fun_39c0(r13_7, rsi16, rdx9, r13_7, rsi16, rdx9), !!eax19)) {
                r13_7 = rbp10;
                rax20 = quote(r15_17, rsi16, rdx9, rcx);
                rdi21 = stderr;
                rdx9 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                rcx = rax20;
                fun_3ca0(rdi21, 1, "\n  - %s", rcx, r8, r9, v22, v14, v23, v11);
            } else {
                rax24 = quote(r15_17, rsi16, rdx9, rcx);
                rdi25 = stderr;
                *reinterpret_cast<int32_t*>(&rsi16) = 1;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                rdx9 = reinterpret_cast<void**>(", %s");
                rcx = rax24;
                fun_3ca0(rdi25, 1, ", %s", rcx, r8, r9, v26, v14, v27, v11);
            }
            ++rbx18;
            rbp10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r12_8));
            r15_17 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v14) + reinterpret_cast<uint64_t>(rbx18 * 8));
        } while (r15_17);
    }
    rdi28 = stderr;
    rax29 = *reinterpret_cast<void***>(rdi28 + 40);
    if (reinterpret_cast<unsigned char>(rax29) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi28 + 48))) {
        goto 0x38b0;
    } else {
        *reinterpret_cast<void***>(rdi28 + 40) = rax29 + 1;
        *reinterpret_cast<void***>(rax29) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(void** rdi, void*** rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(void*** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_15303(int64_t rdi, void** rsi, void*** rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    int64_t r13_10;
    int64_t r12_11;
    void*** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int64_t rax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_15347_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_153be_4;
        } else {
            addr_153be_4:
            return rax14;
        }
    }
    rdi15 = *rdx;
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            rax17 = fun_3a10(rdi15, r14_9);
            if (!*reinterpret_cast<int32_t*>(&rax17)) 
                break;
            ++rbx16;
            rdi15 = rbp12[rbx16 * 8];
        } while (rdi15);
        goto addr_15340_8;
    } else {
        goto addr_15340_8;
    }
    return rbx16;
    addr_15340_8:
    rax14 = -1;
    goto addr_15347_3;
}

struct s90 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_153d3(void** rdi, struct s90* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_39c0(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t fun_3730();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_15433(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_3730();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_1548e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_3700();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_1548e_3;
            rax6 = fun_3700();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s91 {
    signed char[16] pad16;
    int64_t f10;
    int32_t f18;
};

void fun_154a3(struct s91* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f18 = 0x95f616;
    return;
}

struct s92 {
    int64_t f0;
    int64_t f8;
    uint64_t f10;
    int32_t f18;
};

struct s93 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_154c3(struct s92* rdi, struct s93* rsi) {
    uint64_t rax3;
    int64_t rdx4;
    uint64_t rcx5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f18 != 0x95f616) {
        fun_38f0("state->magic == 9827862", "lib/cycle-check.c", 60, "cycle_check", "state->magic == 9827862", "lib/cycle-check.c", 60, "cycle_check");
    } else {
        rax3 = rdi->f10;
        rdx4 = rsi->f8;
        if (!rax3) {
            rdi->f10 = 1;
        } else {
            if (rdi->f0 != rdx4 || rsi->f0 != rdi->f8) {
                rcx5 = rax3 + 1;
                rdi->f10 = rcx5;
                if (!(rax3 & rcx5)) {
                    if (!rcx5) {
                        return 1;
                    }
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        }
        rax6 = rsi->f0;
        rdi->f0 = rdx4;
        rdi->f8 = rax6;
        return 0;
    }
}

void** fun_3810();

void** fun_15553() {
    void** rax1;
    void** eax2;
    void** eax3;
    void** rax4;
    void** rdi5;
    void** rax6;
    void** r14d7;
    void** r13_8;

    __asm__("cli ");
    rax1 = fun_3810();
    if (!rax1 || (eax2 = fun_3a30(rax1), reinterpret_cast<unsigned char>(eax2) > reinterpret_cast<unsigned char>(2))) {
        return rax1;
    } else {
        eax3 = rpl_fcntl();
        rax4 = fun_3700();
        if (reinterpret_cast<signed char>(eax3) >= reinterpret_cast<signed char>(0)) {
            rdi5 = eax3;
            *reinterpret_cast<int32_t*>(&rdi5 + 4) = 0;
            rax6 = fun_3c10(rdi5, 0x406, rdi5, 0x406);
            r14d7 = *reinterpret_cast<void***>(rax4);
            r13_8 = rax6;
            if (!rax6) {
                fun_3950();
            }
        } else {
            r14d7 = *reinterpret_cast<void***>(rax4);
            *reinterpret_cast<int32_t*>(&r13_8) = 0;
            *reinterpret_cast<int32_t*>(&r13_8 + 4) = 0;
        }
        fun_3980(rax1, 0x406, rax1, 0x406);
        *reinterpret_cast<void***>(rax4) = r14d7;
        return r13_8;
    }
}

int64_t fun_3800(void** rdi);

int64_t fun_155f3(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    void** rax4;
    int32_t eax5;
    void** rax6;
    void** r12d7;
    int64_t rax8;

    __asm__("cli ");
    eax2 = fun_3ac0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_3b30(rdi);
        if (!(eax3 && (fun_3ac0(rdi), rax4 = fun_38e0(), reinterpret_cast<int1_t>(rax4 == 0xffffffffffffffff)) || (eax5 = rpl_fflush(rdi), eax5 == 0))) {
            rax6 = fun_3700();
            r12d7 = *reinterpret_cast<void***>(rax6);
            rax8 = fun_3800(rdi);
            if (r12d7) {
                *reinterpret_cast<void***>(rax6) = r12d7;
                *reinterpret_cast<int32_t*>(&rax8) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
            }
            return rax8;
        }
    }
    goto fun_3800;
}

uint32_t fun_37b0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_15683(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    int64_t rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    int64_t rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_37b0(rdi);
        r12d10 = eax9;
        goto addr_15784_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_37b0(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_15784_3:
                rax14 = rax8 - g28;
                if (rax14) {
                    fun_3880();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_15839_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_37b0(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_37b0(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_3700();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_3950();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_15784_3;
                }
            }
        } else {
            eax22 = fun_37b0(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_3700(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_15784_3;
            } else {
                eax25 = fun_37b0(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_15784_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_15839_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_156e9_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_156ed_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_156ed_18:
            if (0) {
            }
        } else {
            addr_15735_23:
            eax28 = fun_37b0(rdi);
            r12d10 = eax28;
            goto addr_15784_3;
        }
        eax29 = fun_37b0(rdi);
        r12d10 = eax29;
        goto addr_15784_3;
    }
    if (0) {
    }
    eax30 = fun_37b0(rdi);
    r12d10 = eax30;
    goto addr_15784_3;
    addr_156e9_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_156ed_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_15735_23;
        goto addr_156ed_18;
    }
}

uint64_t fun_158f3(signed char* rdi, int64_t rsi) {
    int64_t rdx3;
    int64_t rax4;

    __asm__("cli ");
    rdx3 = *rdi;
    if (!*reinterpret_cast<signed char*>(&rdx3)) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        do {
            __asm__("rol rax, 0x9");
            ++rdi;
            rax4 = rax4 + rdx3;
            rdx3 = *rdi;
        } while (*reinterpret_cast<signed char*>(&rdx3));
        return rax4 % reinterpret_cast<uint64_t>(rsi);
    }
}

signed char* fun_3b10(int64_t rdi);

signed char* fun_15933() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_3b10(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_38a0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_15973(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_38a0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_3880();
    } else {
        return r12_7;
    }
}

void fun_15a03() {
    __asm__("cli ");
    goto rpl_fcntl;
}

int32_t setlocale_null_r();

int64_t fun_15a23() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_3880();
    } else {
        return rax3;
    }
}

int64_t fun_15aa3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_3b70(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_3860(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_3ab0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_3ab0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_15b53() {
    __asm__("cli ");
    goto fun_3b70;
}

void fun_15b63() {
    __asm__("cli ");
}

void fun_15b77() {
    __asm__("cli ");
    return;
}

void fun_3fe0() {
    goto 0x3f06;
}

void fun_4026() {
    goto 0x3f06;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_3cf0(int64_t rdi, void** rsi);

uint32_t fun_3cd0(void** rdi, void** rsi);

void** fun_3d20(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_109e5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_3840();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_3840();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_3860(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_10ce3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_10ce3_22; else 
                            goto addr_110dd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_1119d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_114f0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_10ce0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_10ce0_30; else 
                                goto addr_11509_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_3860(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_114f0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_39c0(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_114f0_28; else 
                            goto addr_10b8c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_11650_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_114d0_40:
                        if (r11_27 == 1) {
                            addr_1105d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_11618_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_10c97_44;
                            }
                        } else {
                            goto addr_114e0_46;
                        }
                    } else {
                        addr_1165f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_1105d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_10ce3_22:
                                if (v47 != 1) {
                                    addr_11239_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_3860(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_11284_54;
                                    }
                                } else {
                                    goto addr_10cf0_56;
                                }
                            } else {
                                addr_10c95_57:
                                ebp36 = 0;
                                goto addr_10c97_44;
                            }
                        } else {
                            addr_114c4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_1165f_47; else 
                                goto addr_114ce_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_1105d_41;
                        if (v47 == 1) 
                            goto addr_10cf0_56; else 
                            goto addr_11239_52;
                    }
                }
                addr_10d51_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_10be8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_10c0d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_10f10_65;
                    } else {
                        addr_10d79_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_115c8_67;
                    }
                } else {
                    goto addr_10d70_69;
                }
                addr_10c21_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_10c6c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_115c8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_10c6c_81;
                }
                addr_10d70_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_10c0d_64; else 
                    goto addr_10d79_66;
                addr_10c97_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_10d4f_91;
                if (v22) 
                    goto addr_10caf_93;
                addr_10d4f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_10d51_62;
                addr_11284_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_11a0b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_11a7b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_1187f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_3cf0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_3cd0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_1137e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_10d3c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_11388_112;
                    }
                } else {
                    addr_11388_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_11459_114;
                }
                addr_10d48_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_10d4f_91;
                while (1) {
                    addr_11459_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_11967_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_113c6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_11975_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_11447_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_11447_128;
                        }
                    }
                    addr_113f5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_11447_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_113c6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_113f5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_10c6c_81;
                addr_11975_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_115c8_67;
                addr_11a0b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_1137e_109;
                addr_11a7b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_1137e_109;
                addr_10cf0_56:
                rax93 = fun_3d20(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_10d3c_110;
                addr_114ce_59:
                goto addr_114d0_40;
                addr_1119d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_10ce3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_10d48_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_10c95_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_10ce3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_111e2_160;
                if (!v22) 
                    goto addr_115b7_162; else 
                    goto addr_117c3_163;
                addr_111e2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_115b7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_115c8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_1108b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_10ef3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_10c21_70; else 
                    goto addr_10f07_169;
                addr_1108b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_10be8_63;
                goto addr_10d70_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_114c4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_115ff_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_10ce0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_10bd8_178; else 
                        goto addr_11582_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_114c4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_10ce3_22;
                }
                addr_115ff_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_10ce0_30:
                    r8d42 = 0;
                    goto addr_10ce3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_10d51_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_11618_42;
                    }
                }
                addr_10bd8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_10be8_63;
                addr_11582_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_114e0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_10d51_62;
                } else {
                    addr_11592_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_10ce3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_11d42_188;
                if (v28) 
                    goto addr_115b7_162;
                addr_11d42_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_10ef3_168;
                addr_10b8c_37:
                if (v22) 
                    goto addr_11b83_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_10ba3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_11650_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_116db_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_10ce3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_10bd8_178; else 
                        goto addr_116b7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_114c4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_10ce3_22;
                }
                addr_116db_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_10ce3_22;
                }
                addr_116b7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_114e0_46;
                goto addr_11592_186;
                addr_10ba3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_10ce3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_10ce3_22; else 
                    goto addr_10bb4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_11c8e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_11b14_210;
            if (1) 
                goto addr_11b12_212;
            if (!v29) 
                goto addr_1174e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_3850();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_11c81_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_10f10_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_10ccb_219; else 
            goto addr_10f2a_220;
        addr_10caf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_10cc3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_10f2a_220; else 
            goto addr_10ccb_219;
        addr_1187f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_10f2a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_3850();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_1189d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_3850();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_11d10_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_11776_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_11967_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_10cc3_221;
        addr_117c3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_10cc3_221;
        addr_10f07_169:
        goto addr_10f10_65;
        addr_11c8e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_10f2a_220;
        goto addr_1189d_222;
        addr_11b14_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - g28;
        if (!rax111) 
            goto addr_11b6e_236;
        fun_3880();
        rsp25 = rsp25 - 8 + 8;
        goto addr_11d10_225;
        addr_11b12_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_11b14_210;
        addr_1174e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_11b14_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_11776_226;
        }
        addr_11c81_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_110dd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x17ccc + rax113 * 4) + 0x17ccc;
    addr_11509_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x17dcc + rax114 * 4) + 0x17dcc;
    addr_11b83_190:
    addr_10ccb_219:
    goto 0x109b0;
    addr_10bb4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x17bcc + rax115 * 4) + 0x17bcc;
    addr_11b6e_236:
    goto v116;
}

void fun_10bd0() {
}

void fun_10d88() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x10a82;
}

void fun_10de1() {
    goto 0x10a82;
}

void fun_10ece() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x10d51;
    }
    if (v2) 
        goto 0x117c3;
    if (!r10_3) 
        goto addr_1192e_5;
    if (!v4) 
        goto addr_117fe_7;
    addr_1192e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_117fe_7:
    goto 0x10c04;
}

void fun_10eec() {
}

void fun_10f97() {
    signed char v1;

    if (v1) {
        goto 0x10f1f;
    } else {
        goto 0x10c5a;
    }
}

void fun_10fb1() {
    signed char v1;

    if (!v1) 
        goto 0x10faa; else 
        goto "???";
}

void fun_10fd8() {
    goto 0x10ef3;
}

void fun_11058() {
}

void fun_11070() {
}

void fun_1109f() {
    goto 0x10ef3;
}

void fun_110f1() {
    goto 0x11080;
}

void fun_11120() {
    goto 0x11080;
}

void fun_11153() {
    goto 0x11080;
}

void fun_11520() {
    goto 0x10bd8;
}

void fun_1181e() {
    signed char v1;

    if (v1) 
        goto 0x117c3;
    goto 0x10c04;
}

void fun_118c5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x10c04;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x10be8;
        goto 0x10c04;
    }
}

void fun_11ce2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x10f50;
    } else {
        goto 0x10a82;
    }
}

void fun_141d8() {
    fun_3840();
}

void fun_3fbb() {
    goto 0x3f06;
}

void fun_3fed() {
    goto 0x3f06;
}

void fun_10e0e() {
    goto 0x10a82;
}

void fun_10fe4() {
    goto 0x10f9c;
}

void fun_110ab() {
    goto 0x10bd8;
}

void fun_110fd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x11080;
    goto 0x10caf;
}

void fun_1112f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x1108b;
        goto 0x10ab0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x10f2a;
        goto 0x10ccb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x118c8;
    if (r10_8 > r15_9) 
        goto addr_11015_9;
    addr_1101a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x118d3;
    goto 0x10c04;
    addr_11015_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_1101a_10;
}

void fun_11162() {
    goto 0x10c97;
}

void fun_11530() {
    goto 0x10c97;
}

void fun_11ccf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x10dec;
    } else {
        goto 0x10f50;
    }
}

void fun_14290() {
}

void fun_3fc5() {
    if (__return_address()) 
        goto 0x43fa;
    goto 0x3f06;
}

void fun_4088() {
    goto 0x3f06;
}

void fun_3ffa() {
    goto 0x3f06;
}

void fun_1116c() {
    goto 0x11107;
}

void fun_1153a() {
    goto 0x1105d;
}

void fun_142f0() {
    fun_3840();
    goto fun_3ca0;
}

int64_t optarg = 0;

void fun_4007() {
    int64_t rax1;

    rax1 = optarg;
    if (!rax1) {
    }
    goto 0x3f06;
}

void fun_10e3d() {
    goto 0x10a82;
}

void fun_11178() {
    goto 0x11107;
}

void fun_11547() {
    goto 0x110ae;
}

void fun_14330() {
    fun_3840();
    goto fun_3ca0;
}

void fun_10e6a() {
    goto 0x10a82;
}

void fun_11184() {
    goto 0x11080;
}

void fun_14370() {
    fun_3840();
    goto fun_3ca0;
}

void fun_10e8c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x11820;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x10d51;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x10d51;
    }
    if (v11) 
        goto 0x11b83;
    if (r10_12 > r15_13) 
        goto addr_11bd3_8;
    addr_11bd8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x11911;
    addr_11bd3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_11bd8_9;
}

struct s94 {
    signed char[24] pad24;
    void** f18;
};

struct s95 {
    signed char[16] pad16;
    void** f10;
};

struct s96 {
    signed char[8] pad8;
    void** f8;
};

void fun_143c0() {
    void** r15_1;
    struct s94* rbx2;
    void** r14_3;
    struct s95* rbx4;
    void** r13_5;
    struct s96* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_3840();
    fun_3ca0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x143e2, __return_address(), v11);
    goto v12;
}

void fun_14418() {
    fun_3840();
    goto 0x143e9;
}

struct s97 {
    signed char[32] pad32;
    void** f20;
};

struct s98 {
    signed char[24] pad24;
    void** f18;
};

struct s99 {
    signed char[16] pad16;
    void** f10;
};

struct s100 {
    signed char[8] pad8;
    void** f8;
};

struct s101 {
    signed char[40] pad40;
    void** f28;
};

void fun_14450() {
    void** rcx1;
    struct s97* rbx2;
    void** r15_3;
    struct s98* rbx4;
    void** r14_5;
    struct s99* rbx6;
    void** r13_7;
    struct s100* rbx8;
    void** r12_9;
    void*** rbx10;
    void** v11;
    struct s101* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_3840();
    fun_3ca0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x14484, rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x14484);
    goto v15;
}

void fun_144c8() {
    fun_3840();
    goto 0x1448b;
}
