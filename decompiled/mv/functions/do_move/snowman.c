uint32_t do_move(void** rdi, void** rsi, void** rdx, void** rcx, void* r8, int64_t r9) {
    void* rsp7;
    int64_t rax8;
    int64_t rax9;
    signed char v10;
    unsigned char v11;
    int64_t rdx12;
    int64_t rax13;
    void* rsp14;

    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 88);
    rax8 = g28;
    *reinterpret_cast<uint32_t*>(&rax9) = copy();
    if (!*reinterpret_cast<unsigned char*>(&rax9)) 
        goto addr_4556_2;
    if (!v10) {
        *reinterpret_cast<uint32_t*>(&rax9) = v11;
        if (*reinterpret_cast<unsigned char*>(&rax9)) {
            addr_4556_2:
            rdx12 = rax8 - g28;
            if (rdx12) {
                fun_3880();
                goto addr_4604_6;
            } else {
                return *reinterpret_cast<uint32_t*>(&rax9);
            }
        } else {
            rax13 = get_root_dev_ino(0x1e110, reinterpret_cast<int64_t>(rsp7) + 15);
            rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 - 8 - 8 + 8 + 8 + 8 - 8 + 8);
            if (!rax13) {
                addr_4623_9:
                quotearg_style(4, "/", 4, "/");
                fun_3840();
                fun_3700();
                fun_3bd0();
            } else {
                rax9 = rm(reinterpret_cast<int64_t>(rsp14) + 48, reinterpret_cast<int64_t>(rsp14) + 16);
                if (static_cast<uint32_t>(rax9 - 2) > 2) {
                    addr_4604_6:
                    fun_38f0("VALID_STATUS (status)", "src/mv.c", 0xe1, "do_move", "VALID_STATUS (status)", "src/mv.c", 0xe1, "do_move");
                    goto addr_4623_9;
                } else {
                    *reinterpret_cast<unsigned char*>(&rax9) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax9) != 4);
                    goto addr_4556_2;
                }
            }
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax9) = 0;
        goto addr_4556_2;
    }
}