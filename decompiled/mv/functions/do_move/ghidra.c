uint do_move(EVP_PKEY_CTX *param_1,EVP_PKEY_CTX *param_2,undefined8 param_3,undefined8 param_4,
            long param_5)

{
  uint uVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  int *piVar4;
  long in_FS_OFFSET;
  char local_6a;
  byte local_69;
  undefined local_68 [4];
  undefined4 local_64;
  undefined2 local_60;
  undefined local_5e;
  long local_58;
  undefined local_50;
  undefined local_4f;
  ushort local_4e;
  EVP_PKEY_CTX *local_48;
  undefined8 local_40;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = copy(param_1,param_2);
  if ((char)uVar1 != '\0') {
    if (local_6a == '\0') {
      uVar1 = (uint)local_69;
      if (local_69 == 0) {
        local_68[0] = 0;
        local_64 = 5;
        local_60 = 0x100;
        local_5e = 1;
        local_4f = 0;
        local_4e = 0x100;
        local_58 = get_root_dev_ino(dev_ino_buf_0,&local_69,0x100,&local_6a);
        if (local_58 == 0) {
          uVar2 = quotearg_style(4,"/");
          uVar3 = dcgettext(0,"failed to get attributes of %s",5);
          piVar4 = __errno_location();
                    /* WARNING: Subroutine does not return */
          error(1,*piVar4,uVar3,uVar2);
        }
        local_50 = 0;
        local_4e = local_4e & 0xff00 | (ushort)*(byte *)(param_5 + 0x3c);
        local_40 = 0;
        local_48 = param_1;
        uVar1 = rm(&local_48,local_68);
        if (2 < uVar1 - 2) {
                    /* WARNING: Subroutine does not return */
          __assert_fail("VALID_STATUS (status)","src/mv.c",0xe1,"do_move");
        }
        uVar1 = uVar1 & 0xffffff00 | (uint)(uVar1 != 4);
      }
    }
    else {
      uVar1 = 0;
    }
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}