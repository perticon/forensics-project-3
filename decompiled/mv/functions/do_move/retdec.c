int64_t do_move(int64_t a1, int64_t a2, int32_t a3, int64_t a4, int64_t * a5) {
    int64_t v1 = __readfsqword(40); // 0x4526
    char v2; // bp-105, 0x4510
    char v3; // bp-106, 0x4510
    bool v4 = copy((char *)a1, (char *)a2, a3, (char *)a4, 0, (int32_t *)a5, (bool *)&v3, (bool *)&v2); // 0x4542
    int64_t result = 0; // 0x454b
    if (v4 == v3 == 0) {
        // 0x4578
        result = v2;
        if (v2 != 0) {
            goto lab_0x4556;
        } else {
            char v5 = 0; // bp-104, 0x4592
            if (get_root_dev_ino((int32_t *)&g41) == NULL) {
                // 0x4623
                quotearg_style();
                function_3840();
                function_3700();
                return function_3bd0();
            }
            int64_t v6 = a1; // bp-72, 0x45d5
            int32_t v7 = rm((char **)&v6, (int32_t *)&v5); // 0x45e7
            if (v7 < 5) {
                // 0x45f4
                result = (int64_t)(v7 != 4) | (int64_t)(v7 & -256);
                goto lab_0x4556;
            } else {
                goto lab_0x4604;
            }
        }
    } else {
        goto lab_0x4556;
    }
  lab_0x4556:
    // 0x4556
    if (v1 == __readfsqword(40)) {
        // 0x456a
        return result;
    }
    // 0x45ff
    function_3880();
    goto lab_0x4604;
  lab_0x4604:
    // 0x4604
    function_38f0();
    // 0x4623
    quotearg_style();
    function_3840();
    function_3700();
    return function_3bd0();
}