void main(int param_1,undefined8 *param_2)

{
  bool bVar1;
  bool bVar2;
  bool bVar3;
  undefined uVar4;
  int iVar5;
  uint uVar6;
  undefined8 uVar7;
  void *__ptr;
  int *piVar8;
  undefined8 uVar9;
  uint uVar10;
  char *pcVar11;
  long lVar12;
  undefined8 *puVar13;
  long in_FS_OFFSET;
  long local_178;
  long local_168;
  int local_154;
  undefined8 local_140;
  undefined4 local_138;
  undefined4 local_134;
  int local_130;
  undefined4 uStack300;
  undefined8 local_128;
  undefined2 local_120;
  undefined4 local_11c;
  undefined local_118;
  undefined8 local_110;
  undefined8 local_108;
  undefined2 local_100;
  undefined local_fe;
  undefined local_fd;
  undefined uStack252;
  undefined local_fb;
  undefined local_fa;
  undefined local_f9;
  int local_f8;
  undefined4 local_f4;
  undefined8 local_f0;
  undefined8 local_e8;
  undefined local_d8 [24];
  undefined4 local_c0;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdin);
  cp_options_default();
  local_108 = 0x10000000101;
  local_100 = 1;
  local_f4 = 1;
  local_11c = 0x1010100;
  local_118 = 0;
  local_110 = 0;
  local_fe = 0;
  local_134 = 2;
  local_130 = 4;
  uStack300 = 2;
  local_128 = 0;
  local_120 = 1;
  iVar5 = isatty(0);
  local_fa = 0;
  local_fb = iVar5 != 0;
  bVar1 = false;
  local_fd = 0;
  uStack252 = 0;
  local_f0 = 0;
  local_e8 = 0;
  local_178 = 0;
  bVar2 = false;
  local_168 = 0;
  bVar3 = false;
  lVar12 = local_168;
switchD_00103f3d_caseD_5a:
  local_168 = lVar12;
  iVar5 = getopt_long(param_1,param_2,"bfint:uvS:TZ",long_options,0);
  if (iVar5 != -1) {
    lVar12 = local_168;
    if (0x80 < iVar5) goto switchD_00103f3d_caseD_55;
    if (0x52 < iVar5) goto code_r0x00103f36;
    if (iVar5 == -0x83) {
      version_etc(stdout,&DAT_00116023,"GNU coreutils",Version,"Mike Parker","David MacKenzie",
                  "Jim Meyering",0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar5 != -0x82) goto switchD_00103f3d_caseD_55;
    usage(0);
  }
  param_1 = param_1 - optind;
  param_2 = param_2 + optind;
  if (param_1 <= (int)(uint)(local_178 == 0)) {
    if (param_1 == 1) {
      uVar7 = quotearg_style(4,*param_2);
      pcVar11 = "missing destination file operand after %s";
LAB_00104265:
      uVar9 = dcgettext(0,pcVar11,5);
                    /* WARNING: Subroutine does not return */
      error(0,0,uVar9,uVar7);
    }
    pcVar11 = "missing file operand";
LAB_00103f92:
    uVar7 = dcgettext(0,pcVar11,5);
                    /* WARNING: Subroutine does not return */
    error(0,0,uVar7);
  }
  local_c0 = 0;
  if (bVar1) {
    if (local_178 != 0) {
      uVar7 = dcgettext(0,"cannot combine --target-directory (-t) and --no-target-directory (-T)",5)
      ;
                    /* WARNING: Subroutine does not return */
      error(1,0,uVar7);
    }
    local_154 = -100;
    if (2 < param_1) {
      uVar7 = quotearg_style(4,param_2[2]);
      pcVar11 = "extra operand %s";
      goto LAB_00104265;
    }
  }
  else if (local_178 == 0) {
    lVar12 = param_2[(long)param_1 + -1];
    if ((param_1 == 2) &&
       (local_f8 = renameatu(0xffffff9c,*param_2,0xffffff9c,lVar12,1), local_f8 != 0)) {
      piVar8 = __errno_location();
      local_f8 = *piVar8;
    }
    local_154 = -100;
    if (local_f8 != 0) {
      local_154 = target_directory_operand(lVar12,local_d8);
      if (local_154 == -1) {
        piVar8 = __errno_location();
        local_154 = -100;
        iVar5 = *piVar8;
        if (2 < param_1) {
          uVar7 = quotearg_style(4,lVar12);
          uVar9 = dcgettext(0,"target %s",5);
                    /* WARNING: Subroutine does not return */
          error(1,iVar5,uVar9,uVar7);
        }
      }
      else {
        local_f8 = -1;
        param_1 = param_1 + -1;
        local_178 = lVar12;
      }
    }
  }
  else {
    local_154 = target_directory_operand(local_178,local_d8);
    if (local_154 == -1) {
      uVar7 = quotearg_style(4,local_178);
      uVar9 = dcgettext(0,"target directory %s",5);
      piVar8 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar8,uVar9,uVar7);
    }
  }
  if (bVar2) {
    for (puVar13 = param_2; puVar13 != param_2 + param_1; puVar13 = puVar13 + 1) {
      strip_trailing_slashes(*puVar13);
    }
  }
  if (local_130 == 2) {
    local_fd = 0;
    uVar4 = local_fd;
    local_fd = 0;
    if (bVar3) {
      pcVar11 = "options --backup and --no-clobber are mutually exclusive";
      goto LAB_00103f92;
    }
  }
  else {
    uVar4 = local_fd;
    if (bVar3) {
      uVar7 = dcgettext(0,"backup type",5);
      local_138 = xget_version(uVar7,local_168);
      goto LAB_00104124;
    }
  }
  local_fd = uVar4;
  local_138 = 0;
LAB_00104124:
  set_simple_backup_suffix();
  hash_init();
  if (local_178 == 0) {
    local_f9 = 1;
    uVar10 = do_move(*param_2,param_2[1],0xffffff9c,param_2[1],&local_138);
  }
  else {
    if (1 < param_1) {
      dest_info_init();
    }
    uVar10 = 1;
    for (lVar12 = 0; (int)lVar12 < param_1; lVar12 = lVar12 + 1) {
      local_f9 = (int)lVar12 + 1 == param_1;
      uVar7 = param_2[lVar12];
      uVar9 = last_component(uVar7);
      __ptr = (void *)file_name_concat(local_178,uVar9,&local_140);
      strip_trailing_slashes(local_140);
      uVar6 = do_move(uVar7,__ptr,local_154,local_140,&local_138);
      uVar10 = uVar10 & uVar6;
      free(__ptr);
    }
  }
                    /* WARNING: Subroutine does not return */
  exit((uVar10 ^ 1) & 0xff);
code_r0x00103f36:
  switch(iVar5) {
  case 0x53:
    bVar3 = true;
    break;
  case 0x54:
    bVar1 = true;
    break;
  default:
switchD_00103f3d_caseD_55:
    usage();
  case 0x76:
    uStack252 = 1;
    break;
  case 0x5a:
    break;
  case 0x62:
    bVar3 = true;
    lVar12 = optarg;
    if (optarg == 0) {
      lVar12 = local_168;
    }
    break;
  case 0x66:
    local_130 = 1;
    break;
  case 0x69:
    local_130 = 3;
    break;
  case 0x6e:
    local_130 = 2;
    break;
  case 0x74:
    if (local_178 != 0) {
      uVar7 = dcgettext(0,"multiple target directories specified",5);
                    /* WARNING: Subroutine does not return */
      error(1,0,uVar7);
    }
    local_178 = optarg;
    break;
  case 0x75:
    local_fd = 1;
    break;
  case 0x80:
    bVar2 = true;
  }
  goto switchD_00103f3d_caseD_5a;
}