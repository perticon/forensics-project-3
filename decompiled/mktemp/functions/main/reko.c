void main(char * rsi[], word32 edi, struct Eq_399 * fs)
{
	set_program_name(rsi[0]);
	fn0000000000002600("", 0x06);
	fn0000000000002470("/usr/local/share/locale", "coreutils");
	fn0000000000002450("coreutils");
	atexit(&g_t2E30);
	byte r13b_225 = 0x00;
	uint64 rdi_80 = (uint64) edi;
	word32 edi_863 = (word32) rdi_80;
	int32 eax_81 = fn00000000000024C0(&g_tAAA0, "dp:qtuV", rsi, (word32) rdi_80, null);
	if (eax_81 != ~0x00)
	{
		if (eax_81 <= 0x80)
		{
			if (eax_81 > 0x55)
			{
				uint64 rax_851 = (uint64) (eax_81 - 0x56);
				if ((word32) rax_851 <= 0x2A)
				{
					<anonymous> * rax_859 = (int64) g_a78F8[rax_851 * 0x04] + 0x78F8;
					rax_859();
					return;
				}
			}
			else
			{
				if (eax_81 == ~0x82)
				{
					version_etc(0x7004, stdout, fs);
					fn0000000000002670(0x00);
				}
				if (eax_81 == ~0x81)
					usage(0x00);
			}
		}
		goto l000000000000283C;
	}
	int64 rax_135 = (int64) g_dwB090;
	up32 ebp_139 = edi - (word32) rax_135;
	if (ebp_139 > 0x01)
	{
		fn0000000000002630(fn0000000000002480(0x05, "too many templates", null), 0x00, 0x00);
l000000000000283C:
		usage(0x01);
	}
	Eq_17 rbp_154;
	Eq_17 rsi_146;
	Eq_17 r12_175;
	Eq_17 rsi_151;
	Eq_17 rbx_167;
	if (ebp_139 != 0x00)
	{
		rsi_146 = rsi[rax_135];
		if (true)
			goto l0000000000002C13;
		Eq_17 rax_150 = fn00000000000024A0(rsi_146);
		rsi_151 = rsi_146;
		rbp_154 = rax_150;
		if (rax_150 == 0x00 || *((word64) rax_150 + (rsi_146 - 1)) != 88)
			goto l0000000000002CD2;
	}
	else
	{
		r13b_225 = 0x01;
		rsi_146.u0 = 0x70AE;
		if (true)
			goto l0000000000002C13;
		rbp_154.u0 = 0x0E;
		r13b_225 = 0x01;
		rsi_151.u0 = 0x70AE;
	}
	Eq_17 rax_164 = fn00000000000024A0(0x00);
	Eq_17 rax_168 = xcharalloc((word64) rbp_154 + ((word64) rax_164 + 1));
	fn0000000000002590(rbp_154, rsi_151, rax_168);
	rbp_154 += rax_168;
	fn0000000000002590((word64) rax_164 + 1, 0x00, rbp_154);
	rbx_167 = rax_164;
	r12_175 = rax_168;
l0000000000002959:
	if (rbx_167 != 0x00 && rbp_154 != last_component(rbp_154))
	{
l0000000000002D04:
		quote(rbp_154, fs);
		Eq_17 rax_475 = fn0000000000002480(0x05, "invalid suffix %s, contains directory separator", null);
		fn0000000000002630(rax_475, 0x00, 0x01);
		_start(rax_475, 0x00);
	}
	int64 rdi_204 = rbp_154 - r12_175;
	int64 rax_206 = rdi_204;
	do
	{
		up64 r14_210 = rdi_204 - rax_206;
		if (rax_206 == 0x00)
			break;
		--rax_206;
	} while (*((word64) r12_175 + rax_206) == 88);
	if (r14_210 <= 0x02)
	{
l0000000000002CA0:
		quote(r12_175, fs);
		fn0000000000002630(fn0000000000002480(0x05, "too few X's in template %s", null), 0x00, 0x01);
		rsi_151.u0 = 0x00;
l0000000000002CD2:
		quote(rsi_151, fs);
		fn0000000000002630(fn0000000000002480(0x05, "with --suffix, template %s must end in X", null), 0x00, 0x01);
		goto l0000000000002D04;
	}
	if (r13b_225 == 0x00)
	{
l0000000000002A02:
		int32 r13d_639;
		char * rsi_634;
		Eq_17 rax_523 = xstrdup(r12_175);
		if (true)
		{
			if (false)
			{
				if (gen_tempname_len(0x02) >= 0x00)
				{
					fn0000000000002420(rax_523);
					r13d_639 = 0x00;
					goto l0000000000002A46;
				}
			}
			else
			{
				int32 eax_579 = gen_tempname_len(0x00);
				if (eax_579 >= 0x00 && fn0000000000002510(eax_579) == 0x00)
					goto l0000000000002AD8;
			}
			if (true)
			{
				quote(r12_175, fs);
				rsi_634 = (char *) "failed to create file via template %s";
l0000000000002B74:
				fn0000000000002630(fn0000000000002480(0x05, rsi_634, null), *fn00000000000023C0(), 0x00);
				r13d_639 = 0x01;
l0000000000002A46:
				fn0000000000002670(r13d_639);
			}
			goto l0000000000002AB2;
		}
		else
		{
			if (false)
			{
				int32 eax_729 = gen_tempname_len(0x02);
				r13d_639 = eax_729;
				if (eax_729 == 0x00)
				{
					fn0000000000002420(rax_523);
					goto l0000000000002A46;
				}
l0000000000002B52:
				if (true)
				{
					quote(r12_175, fs);
					rsi_634 = (char *) "failed to create directory via template %s";
					goto l0000000000002B74;
				}
				goto l0000000000002AB2;
			}
			if (gen_tempname_len(0x01) != 0x00)
				goto l0000000000002B52;
l0000000000002AD8:
			fn0000000000002420(rax_523);
			FILE * rdi_670 = stdout;
			g_bB0C9 = 0x01;
			int32 eax_679 = close_stream(rdi_670);
			r13d_639 = eax_679;
			if (eax_679 == 0x00)
				goto l0000000000002A46;
			word32 r12d_701 = *fn00000000000023C0();
			fn00000000000023D0();
			if (true)
				fn0000000000002630(fn0000000000002480(0x05, "write error", null), r12d_701, 0x00);
l0000000000002AB2:
			r13d_639 = 0x01;
			goto l0000000000002A46;
		}
	}
	Eq_17 rax_258;
	if (true)
	{
		rax_258.u0 = 0x00;
		if (false)
		{
			rbp_154.u0 = 0x00;
			if (null != 0x00)
			{
l00000000000029DC:
				if (*r12_175 != 0x2F)
					goto l00000000000029E7;
				r12_175 = quote(r12_175, fs);
				fn0000000000002630(fn0000000000002480(0x05, "invalid template, %s; with --tmpdir, it may not be absolute", null), 0x00, 0x01);
				goto l0000000000002CA0;
			}
		}
		rax_258 = fn0000000000002390("TMPDIR");
		rbp_154 = rax_258;
		if (rax_258 == 0x00 || *rax_258.u0 == 0x00)
			rbp_154.u0 = 0x70BD;
		goto l00000000000029DC;
	}
	Eq_17 rax_237 = fn0000000000002390("TMPDIR");
	rbp_154 = rax_237;
	if (rax_237 == 0x00 || *rax_237 == 0x00)
	{
		rbp_154.u0 = 0x70BD;
		if (false && null != 0x00)
			rbp_154.u0 = 0x00;
	}
	rax_258 = last_component(r12_175);
	if (r12_175 == rax_258)
	{
l00000000000029E7:
		file_name_concat(rax_258, null, r12_175, rbp_154);
		fn00000000000023A0(r12_175);
		r12_175 = rax_258;
		goto l0000000000002A02;
	}
	quote(r12_175, fs);
	fn0000000000002630(fn0000000000002480(0x05, "invalid template, %s, contains directory separator", null), 0x00, 0x01);
	rsi_146.u0 = 0x00;
l0000000000002C13:
	Eq_17 rax_802 = xstrdup(rsi_146);
	struct Eq_494 * rax_814 = fn00000000000024E0(88, rax_802);
	r12_175 = rax_802;
	rbp_154 = &rax_814->b0001;
	if (rax_814 == null)
		rbp_154 = rax_802 + fn00000000000024A0(rax_802);
	rbx_167 = fn00000000000024A0(rbp_154);
	goto l0000000000002959;
}