void main(uint param_1,undefined8 *param_2)

{
  bool bVar1;
  int iVar2;
  long lVar3;
  undefined8 uVar4;
  char *pcVar5;
  size_t sVar6;
  char *__s;
  char *pcVar7;
  undefined8 uVar8;
  int *piVar9;
  ulong uVar10;
  ulong uVar11;
  undefined auVar12 [16];
  char *local_58;
  char *local_50;
  char local_44;
  char local_43;
  char local_42;
  char local_41;
  char *local_40;
  
  bVar1 = false;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(maybe_close_stdout);
  local_43 = '\0';
  local_44 = '\0';
  local_42 = '\0';
  local_58 = (char *)0x0;
  local_41 = '\0';
  local_50 = (char *)0x0;
  do {
    while( true ) {
      lVar3 = getopt_long(param_1,param_2,"dp:qtuV",longopts,0);
      iVar2 = (int)lVar3;
      if (iVar2 != -1) break;
LAB_00102809:
      param_1 = param_1 - optind;
      if (param_1 < 2) {
        if (param_1 == 0) {
          bVar1 = true;
          pcVar5 = "tmp.XXXXXXXXXX";
          if (local_58 == (char *)0x0) goto LAB_00102c13;
          pcVar5 = (char *)0xe;
          bVar1 = true;
          local_40 = "tmp.XXXXXXXXXX";
        }
        else {
          pcVar5 = (char *)param_2[optind];
          if (local_58 == (char *)0x0) goto LAB_00102c13;
          local_40 = pcVar5;
          pcVar5 = (char *)strlen(pcVar5);
          pcVar7 = local_40;
          if ((pcVar5 == (char *)0x0) || ((local_40 + -1)[(long)pcVar5] != 'X')) goto LAB_00102cd2;
        }
        pcVar7 = local_58;
        sVar6 = strlen(local_58);
        __s = (char *)xcharalloc(pcVar5 + sVar6 + 1);
        memcpy(__s,local_40,(size_t)pcVar5);
        pcVar5 = pcVar5 + (long)__s;
        memcpy(pcVar5,pcVar7,sVar6 + 1);
        goto LAB_00102959;
      }
      uVar4 = dcgettext(0,"too many templates",5);
      error(0,0,uVar4);
switchD_001027ea_caseD_57:
      usage();
switchD_001027ea_caseD_80:
      local_58 = optarg;
    }
    if (0x80 < iVar2) goto switchD_001027ea_caseD_57;
    if (iVar2 < 0x56) {
      if (iVar2 != -0x83) {
        if (iVar2 == -0x82) {
          usage(0);
          goto LAB_00102809;
        }
        goto switchD_001027ea_caseD_57;
      }
switchD_001027ea_caseD_56:
      version_etc(stdout,"mktemp","GNU coreutils",Version,"Jim Meyering","Eric Blake",0,lVar3);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    lVar3 = (long)&switchD_001027ea::switchdataD_001078f8 +
            (long)(int)(&switchD_001027ea::switchdataD_001078f8)[iVar2 - 0x56];
    switch(iVar2) {
    case 0x56:
      goto switchD_001027ea_caseD_56;
    default:
      goto switchD_001027ea_caseD_57;
    case 100:
      local_44 = '\x01';
      break;
    case 0x70:
      local_50 = optarg;
      bVar1 = true;
      break;
    case 0x71:
      local_41 = '\x01';
      break;
    case 0x74:
      local_42 = '\x01';
      bVar1 = true;
      break;
    case 0x75:
      local_43 = '\x01';
      break;
    case 0x80:
      goto switchD_001027ea_caseD_80;
    }
  } while( true );
LAB_00102959:
  if ((sVar6 != 0) && (pcVar7 = (char *)last_component(pcVar5), pcVar5 != pcVar7))
  goto LAB_00102d04;
  lVar3 = (long)pcVar5 - (long)__s;
  do {
    uVar11 = ((long)pcVar5 - (long)__s) - lVar3;
    if (lVar3 == 0) break;
    lVar3 = lVar3 + -1;
  } while (__s[lVar3] == 'X');
  if (uVar11 < 3) {
LAB_00102ca0:
    uVar4 = quote(__s);
    uVar8 = dcgettext(0,"too few X\'s in template %s",5);
    pcVar7 = (char *)0x0;
    error(1,0,uVar8,uVar4);
LAB_00102cd2:
    uVar4 = quote(pcVar7);
    uVar8 = dcgettext(0,"with --suffix, template %s must end in X",5);
    error(1,0,uVar8,uVar4);
LAB_00102d04:
    uVar4 = quote(pcVar5);
    uVar8 = dcgettext(0,"invalid suffix %s, contains directory separator",5);
    auVar12 = error(1,0,uVar8,uVar4);
    pcVar5 = local_58;
    local_58 = SUB168(auVar12,0);
    (*(code *)PTR___libc_start_main_0010afc8)
              (main,pcVar5,&local_50,0,0,SUB168(auVar12 >> 0x40,0),&local_58);
    do {
                    /* WARNING: Do nothing block with infinite loop */
    } while( true );
  }
  pcVar5 = __s;
  if (!bVar1) goto LAB_00102a02;
  if (local_42 != '\0') {
    pcVar5 = getenv("TMPDIR");
    if ((((pcVar5 == (char *)0x0) || (*pcVar5 == '\0')) &&
        (pcVar5 = "/tmp", local_50 != (char *)0x0)) && (*local_50 != '\0')) {
      pcVar5 = local_50;
    }
    pcVar7 = (char *)last_component(__s);
    if (__s == pcVar7) goto LAB_001029e7;
    uVar4 = quote(__s);
    uVar8 = dcgettext(0,"invalid template, %s, contains directory separator",5);
    pcVar5 = (char *)0x0;
    error(1,0,uVar8,uVar4);
LAB_00102c13:
    __s = (char *)xstrdup(pcVar5);
    pcVar7 = strrchr(__s,0x58);
    pcVar5 = pcVar7 + 1;
    if (pcVar7 == (char *)0x0) {
      sVar6 = strlen(__s);
      pcVar5 = __s + sVar6;
    }
    sVar6 = strlen(pcVar5);
    goto LAB_00102959;
  }
  if (((local_50 == (char *)0x0) || (pcVar5 = local_50, *local_50 == '\0')) &&
     ((pcVar5 = getenv("TMPDIR"), pcVar5 == (char *)0x0 || (*pcVar5 == '\0')))) {
    pcVar5 = "/tmp";
  }
  if (*__s == '/') {
    __s = (char *)quote(__s);
    uVar4 = dcgettext(0,"invalid template, %s; with --tmpdir, it may not be absolute",5);
    error(1,0,uVar4,__s);
    goto LAB_00102ca0;
  }
LAB_001029e7:
  pcVar5 = (char *)file_name_concat(pcVar5,__s,0);
  free(__s);
LAB_00102a02:
  pcVar5 = (char *)xstrdup(pcVar5);
  uVar10 = sVar6 & 0xffffffff;
  if (local_44 == '\0') {
    if (local_43 == '\0') {
      iVar2 = gen_tempname_len(pcVar5,uVar10,0,0,uVar11);
      if ((-1 < iVar2) && (iVar2 = close(iVar2), iVar2 == 0)) goto LAB_00102ad8;
    }
    else {
      iVar2 = gen_tempname_len(pcVar5,uVar10,0,2,uVar11);
      if (-1 < iVar2) {
        iVar2 = 0;
        puts(pcVar5);
        goto LAB_00102a46;
      }
    }
    if (local_41 != '\0') goto LAB_00102ab2;
    uVar4 = quote();
    pcVar5 = "failed to create file via template %s";
  }
  else {
    if (local_43 == '\0') {
      iVar2 = gen_tempname_len(pcVar5,uVar10,0,1,uVar11);
      if (iVar2 == 0) {
LAB_00102ad8:
        puts(pcVar5);
        stdout_closed = 1;
        iVar2 = close_stream();
        if (iVar2 == 0) goto LAB_00102a46;
        piVar9 = __errno_location();
        iVar2 = *piVar9;
        remove(pcVar5);
        if (local_41 == '\0') {
          uVar4 = dcgettext(0,"write error",5);
          error(0,iVar2,uVar4);
        }
        goto LAB_00102ab2;
      }
    }
    else {
      iVar2 = gen_tempname_len(pcVar5,uVar10,0,2,uVar11);
      if (iVar2 == 0) {
        puts(pcVar5);
        goto LAB_00102a46;
      }
    }
    if (local_41 != '\0') {
LAB_00102ab2:
      iVar2 = 1;
      goto LAB_00102a46;
    }
    uVar4 = quote();
    pcVar5 = "failed to create directory via template %s";
  }
  uVar8 = dcgettext(0,pcVar5,5);
  piVar9 = __errno_location();
  iVar2 = 1;
  error(0,*piVar9,uVar8,uVar4);
LAB_00102a46:
                    /* WARNING: Subroutine does not return */
  exit(iVar2);
}