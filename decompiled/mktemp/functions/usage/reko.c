void usage(int32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002690(fn0000000000002480(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002ECE;
	}
	fn0000000000002610(fn0000000000002480(0x05, "Usage: %s [OPTION]... [TEMPLATE]\n", null), 0x01);
	fn0000000000002550(stdout, fn0000000000002480(0x05, "Create a temporary file or directory, safely, and print its name.\nTEMPLATE must contain at least 3 consecutive 'X's in last component.\nIf TEMPLATE is not specified, use tmp.XXXXXXXXXX, and --tmpdir is implied.\n", null));
	fn0000000000002550(stdout, fn0000000000002480(0x05, "Files are created u+rw, and directories u+rwx, minus umask restrictions.\n", null));
	fn0000000000002580();
	fn0000000000002550(stdout, fn0000000000002480(0x05, "  -d, --directory     create a directory, not a file\n  -u, --dry-run       do not create anything; merely print a name (unsafe)\n  -q, --quiet         suppress diagnostics about file/dir-creation failure\n", null));
	fn0000000000002550(stdout, fn0000000000002480(0x05, "      --suffix=SUFF   append SUFF to TEMPLATE; SUFF must not contain a slash.\n                        This option is implied if TEMPLATE does not end in X\n", null));
	fn0000000000002550(stdout, fn0000000000002480(0x05, "  -p DIR, --tmpdir[=DIR]  interpret TEMPLATE relative to DIR; if DIR is not\n                        specified, use $TMPDIR if set, else /tmp.  With\n                        this option, TEMPLATE must not be an absolute name;\n                        unlike with -t, TEMPLATE may contain slashes, but\n                        mktemp creates only the final component\n", null));
	fn0000000000002550(stdout, fn0000000000002480(0x05, "  -t                  interpret TEMPLATE as a single file name component,\n                        relative to a directory: $TMPDIR, if set; else the\n                        directory specified via -p; else /tmp [deprecated]\n", null));
	fn0000000000002550(stdout, fn0000000000002480(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002550(stdout, fn0000000000002480(0x05, "      --version     output version information and exit\n", null));
	struct Eq_1004 * rbx_207 = fp - 0xB8 + 16;
	do
	{
		char * rsi_209 = rbx_207->qw0000;
		++rbx_207;
	} while (rsi_209 != null && fn0000000000002570(rsi_209, "mktemp") != 0x00);
	ptr64 r13_222 = rbx_207->qw0008;
	if (r13_222 != 0x00)
	{
		fn0000000000002610(fn0000000000002480(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_17 rax_309 = fn0000000000002600(null, 0x05);
		if (rax_309 == 0x00 || fn00000000000023E0(0x03, "en_", rax_309) == 0x00)
			goto l000000000000314E;
	}
	else
	{
		fn0000000000002610(fn0000000000002480(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_17 rax_251 = fn0000000000002600(null, 0x05);
		if (rax_251 == 0x00 || fn00000000000023E0(0x03, "en_", rax_251) == 0x00)
		{
			fn0000000000002610(fn0000000000002480(0x05, "Full documentation <%s%s>\n", null), 0x01);
l000000000000318B:
			fn0000000000002610(fn0000000000002480(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002ECE:
			fn0000000000002670(edi);
		}
		r13_222 = 0x7004;
	}
	fn0000000000002550(stdout, fn0000000000002480(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l000000000000314E:
	fn0000000000002610(fn0000000000002480(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l000000000000318B;
}