void initbuf(void** rdi, void** rsi, void** rdx) {
    void** r13_4;
    void** r12_5;
    void** rbp6;
    void** rbx7;
    void* rsp8;
    void** rbp9;
    void** rax10;
    void** rax11;
    int64_t v12;
    int64_t rax13;
    void** rdx14;
    void* rsp15;
    void** rax16;
    int64_t rdi17;
    int32_t eax18;
    void* rsp19;
    void** rsi20;
    void** rax21;
    void** rdi22;
    void** rsi23;
    struct s3* rax24;
    void* rax25;
    int64_t v26;
    uint32_t eax27;
    uint32_t v28;
    void** rsi29;
    void** rax30;
    void** rcx31;
    void** rax32;
    void** rdx33;
    void* rsp34;
    void** rax35;
    void** rax36;
    void** r8_37;

    r13_4 = rsi;
    r12_5 = rsi + 1;
    rbp6 = rdx;
    rbx7 = rdi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8);
    do {
        rbp9 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp6) & 0xffffffffffffffe0) + 32);
        rax10 = fun_3760(rbp9, rsi, rdx);
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        *reinterpret_cast<void***>(rbx7) = rax10;
        if (rax10) 
            break;
        rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) >> 1);
    } while (reinterpret_cast<unsigned char>(r12_5) < reinterpret_cast<unsigned char>(rbp6));
    goto addr_7496_4;
    *reinterpret_cast<void***>(rbx7 + 40) = r13_4;
    *reinterpret_cast<void***>(rbx7 + 24) = rbp9;
    *reinterpret_cast<void***>(rbx7 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx7 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx7 + 48) = reinterpret_cast<void**>(0);
    return;
    addr_7496_4:
    xalloc_die();
    rax11 = fun_37d0();
    *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(0);
    fun_3ac0(rbp9, rsi, rdx);
    if (!*reinterpret_cast<void***>(rax11)) {
        goto v12;
    }
    fun_3920();
    fun_3c90();
    fun_3920();
    fun_3c90();
    rax13 = quotearg_n_style();
    fun_3920();
    fun_3c90();
    *reinterpret_cast<uint32_t*>(&rdx14) = 0;
    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 48);
    rax16 = g28;
    *reinterpret_cast<int32_t*>(&rdi17) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
    if (!1) 
        goto addr_758c_10;
    *reinterpret_cast<int32_t*>(&rdi17) = 2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
    addr_758c_10:
    *reinterpret_cast<unsigned char*>(&rdx14) = 0;
    eax18 = fun_3ca0(rdi17, reinterpret_cast<int64_t>(rsp15) + 12, 0, rax13);
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
    if (eax18 < 0) {
        addr_7605_12:
        rsi20 = compress_program;
        quotearg_style(4, rsi20, rdx14, 4, rsi20, rdx14);
        rax21 = fun_3920();
        fun_37d0();
        rdx14 = rax21;
        fun_3c90();
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_7648_13;
    } else {
        if (!eax18) 
            goto addr_75ba_15;
        if (!0) 
            goto addr_75a1_17;
    }
    rdi22 = proctab;
    rsi23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp19) + 16);
    rax24 = hash_remove(rdi22, rsi23, 0, rdi22, rsi23, 0);
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
    if (!rax24) {
        addr_75ba_15:
        rax25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax16) - reinterpret_cast<unsigned char>(g28));
        if (rax25) {
            fun_3950();
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
            goto addr_7605_12;
        } else {
            goto v26;
        }
    } else {
        rax24->fc = 2;
    }
    addr_75a1_17:
    eax27 = v28;
    *reinterpret_cast<uint32_t*>(&rdx14) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax27) + 1)) | eax27 & 0x7f;
    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
    if (1) {
        addr_7648_13:
        rsi29 = compress_program;
        rax30 = quotearg_style(4, rsi29, rdx14, 4, rsi29, rdx14);
        fun_3920();
        rcx31 = rax30;
        fun_3c90();
    } else {
        --nprocs;
        goto addr_75ba_15;
    }
    rax32 = fun_3940(0, 0);
    rdx33 = rax32;
    fun_38b0(2, 0, rdx33, rcx31);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8);
    if (!1) 
        goto addr_76ca_24;
    while (1) {
        rax35 = inttostr(2, reinterpret_cast<int64_t>(rsp34) + 12, rdx33, rcx31);
        fun_38b0(2, ": errno ", 8, rcx31);
        rax36 = fun_3940(rax35, rax35);
        fun_38b0(2, rax35, rax36, rcx31);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_76ca_24:
        *reinterpret_cast<int32_t*>(&rdx33) = 1;
        *reinterpret_cast<int32_t*>(&rdx33 + 4) = 0;
        fun_38b0(2, "\n", 1, rcx31);
        fun_3800(2, "\n", 1, rcx31, r8_37);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
    }
}