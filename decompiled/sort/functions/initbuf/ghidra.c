void initbuf(void **param_1,void *param_2,ulong param_3)

{
  void *pvVar1;
  void *__size;
  
  do {
    __size = (void *)((param_3 & 0xffffffffffffffe0) + 0x20);
    pvVar1 = malloc((size_t)__size);
    *param_1 = pvVar1;
    if (pvVar1 != (void *)0x0) {
      param_1[5] = param_2;
      param_1[3] = __size;
      param_1[2] = (void *)0x0;
      param_1[4] = (void *)0x0;
      param_1[1] = (void *)0x0;
      *(undefined *)(param_1 + 6) = 0;
      return;
    }
    param_3 = (ulong)__size >> 1;
  } while ((long)param_2 + 1U < param_3);
                    /* WARNING: Subroutine does not return */
  xalloc_die();
}