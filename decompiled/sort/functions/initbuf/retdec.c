int64_t initbuf(int64_t a1, int64_t a2, int64_t a3) {
    uint64_t v1 = a3 + 32 & -32; // 0x744b
    int64_t result = function_3760(); // 0x7452
    *(int64_t *)a1 = result;
    while (result == 0) {
        uint64_t v2 = v1 / 2; // 0x748e
        if ((uint64_t)(a2 + 1) >= v2) {
            // 0x7496
            xalloc_die();
            return &g109;
        }
        v1 = v2 + 32 & -32;
        result = function_3760();
        *(int64_t *)a1 = result;
    }
    // 0x745f
    *(int64_t *)(a1 + 40) = a2;
    *(int64_t *)(a1 + 24) = v1;
    *(int64_t *)(a1 + 16) = 0;
    *(int64_t *)(a1 + 32) = 0;
    *(int64_t *)(a1 + 8) = 0;
    *(char *)(a1 + 48) = 0;
    return result;
}