void** parse_field_count(void** rdi, void** rsi, int64_t rdx) {
    void** rax4;
    uint32_t eax5;
    int64_t rax6;
    void* rdx7;
    void** v8;

    rax4 = g28;
    eax5 = xstrtoumax(rdi, rdi);
    if (eax5 <= 4) {
        *reinterpret_cast<uint32_t*>(&rax6) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x150b8 + rax6 * 4) + 0x150b8;
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (!rdx7) 
        goto addr_70b5_5;
    fun_3950();
    quote(rdi, rdi);
    fun_3920();
    fun_3920();
    fun_3c90();
    if (!1) 
        goto addr_7142_9;
    while (1) {
        fun_3920();
        addr_7142_9:
        quotearg_n_style_colon();
        fun_37d0();
        fun_3c90();
    }
    addr_70b5_5:
    return v8;
}