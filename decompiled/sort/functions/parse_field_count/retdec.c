char * parse_field_count(char * string, int64_t * val, char * msgid) {
    int64_t v1 = __readfsqword(40); // 0x705d
    int64_t n; // bp-48, 0x7040
    int64_t v2; // bp-40, 0x7040
    int32_t v3 = xstrtoumax(string, (char **)&n, 10, &v2, (char *)&g6); // 0x7077
    if (v3 < 5) {
        int32_t v4 = *(int32_t *)((4 * (int64_t)v3 & 0x3fffffffc) + (int64_t)&g2); // 0x708a
        return (char *)((int64_t)v4 + (int64_t)&g2);
    }
    // 0x70a0
    if (v1 == __readfsqword(40)) {
        // 0x70b5
        return (char *)n;
    }
    // 0x70d9
    function_3950();
    quote(string);
    function_3920();
    function_3920();
    return (char *)function_3c90();
}