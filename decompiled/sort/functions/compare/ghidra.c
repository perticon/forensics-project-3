int compare(void **param_1,void **param_2)

{
  ulong uVar1;
  int iVar2;
  ulong __n;
  ulong uVar3;
  
  if (keylist != 0) {
    iVar2 = keycompare();
    if (iVar2 != 0) {
      return iVar2;
    }
    if (unique != '\0') {
      return 0;
    }
    if (stable != '\0') {
      return 0;
    }
  }
  uVar1 = (long)param_2[1] - 1;
  uVar3 = (long)param_1[1] - 1;
  if (uVar3 == 0) {
    iVar2 = -(uint)(uVar1 != 0);
  }
  else {
    if (uVar1 == 0) {
      if (reverse == '\0') {
        return 1;
      }
      return -1;
    }
    if (hard_LC_COLLATE == '\0') {
      __n = uVar1;
      if (uVar3 <= uVar1) {
        __n = uVar3;
      }
      iVar2 = memcmp(*param_1,*param_2,__n);
      if (iVar2 == 0) {
        iVar2 = (uint)(uVar1 < uVar3) - (uint)(uVar3 < uVar1);
      }
    }
    else {
      iVar2 = xmemcoll0(*param_1,param_1[1],*param_2);
    }
  }
  if (reverse != '\0') {
    if (iVar2 < 0) {
      return 1;
    }
    return -iVar2;
  }
  return iVar2;
}