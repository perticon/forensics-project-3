void** compare(void** rdi, void** rsi, void** rdx, ...) {
    int1_t zf4;
    void** rax5;
    int1_t zf6;
    int1_t zf7;
    void** rsi8;
    void** rdi9;
    void** rbx10;
    void** rbp11;
    int1_t zf12;
    int1_t zf13;
    void** rdx14;
    void** rdx15;
    void** rsi16;
    uint32_t eax17;
    int1_t zf18;

    zf4 = keylist == 0;
    if (zf4) 
        goto addr_9df1_2;
    rax5 = keycompare(rdi, rsi);
    if (*reinterpret_cast<uint32_t*>(&rax5)) 
        goto addr_9e24_4;
    zf6 = unique == 0;
    if (!zf6) 
        goto addr_9e24_4;
    zf7 = stable == 0;
    if (!zf7) 
        goto addr_9e24_4;
    addr_9df1_2:
    rsi8 = *reinterpret_cast<void***>(rdi + 8);
    rdi9 = *reinterpret_cast<void***>(rdi);
    rbx10 = *reinterpret_cast<void***>(rsi + 8) + 0xffffffffffffffff;
    rbp11 = rsi8 - 1;
    if (rbp11) {
        if (!rbx10) {
            zf12 = reverse == 0;
            *reinterpret_cast<uint32_t*>(&rax5) = 0xffffffff;
            *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
            if (!zf12) {
                addr_9e24_4:
                return rax5;
            }
        } else {
            zf13 = hard_LC_COLLATE == 0;
            if (!zf13) {
                rdx14 = *reinterpret_cast<void***>(rsi);
                rax5 = xmemcoll0(rdi9, rsi8, rdx14);
                goto addr_9e12_12;
            } else {
                rdx15 = rbx10;
                rsi16 = *reinterpret_cast<void***>(rsi);
                if (reinterpret_cast<unsigned char>(rbp11) <= reinterpret_cast<unsigned char>(rbx10)) {
                    rdx15 = rbp11;
                }
                rax5 = fun_3a80(rdi9, rsi16, rdx15);
                if (!*reinterpret_cast<uint32_t*>(&rax5)) {
                    eax17 = 0;
                    *reinterpret_cast<unsigned char*>(&eax17) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp11) > reinterpret_cast<unsigned char>(rbx10));
                    *reinterpret_cast<uint32_t*>(&rax5) = eax17 - reinterpret_cast<uint1_t>(eax17 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp11) < reinterpret_cast<unsigned char>(rbx10))));
                    *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
                    goto addr_9e12_12;
                }
            }
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax5) = *reinterpret_cast<uint32_t*>(&rax5) - (*reinterpret_cast<uint32_t*>(&rax5) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax5) < *reinterpret_cast<uint32_t*>(&rax5) + reinterpret_cast<uint1_t>(!!rbx10)));
        *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
        goto addr_9e12_12;
    }
    *reinterpret_cast<uint32_t*>(&rax5) = 1;
    *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
    goto addr_9e24_4;
    addr_9e12_12:
    zf18 = reverse == 0;
    if (zf18) 
        goto addr_9e24_4;
    if (*reinterpret_cast<int32_t*>(&rax5) >= reinterpret_cast<int32_t>(0)) {
        *reinterpret_cast<uint32_t*>(&rax5) = -*reinterpret_cast<uint32_t*>(&rax5);
        *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
        goto addr_9e24_4;
    }
}