int64_t compare(int64_t a1, int64_t a2) {
    // 0x9dc0
    if (keylist != NULL) {
        int32_t result = keycompare((int32_t *)a1, (int32_t *)a2); // 0x9dd6
        if (result != 0 || *(char *)&unique != 0 || *(char *)&stable != 0) {
            // 0x9e24
            return result;
        }
    }
    int64_t v1 = *(int64_t *)(a1 + 8); // 0x9df1
    int64_t v2 = *(int64_t *)(a2 + 8); // 0x9df5
    uint64_t v3 = v2 - 1; // 0x9e03
    uint64_t v4 = v1 - 1; // 0x9e07
    int64_t result2; // 0x9dc0
    if (v4 != 0) {
        if (v3 == 0) {
            // 0x9e68
            if (*(char *)&reverse != 0) {
                // 0x9e24
                return 0xffffffff;
            }
            // 0x9e24
            return 1;
        }
        // 0x9e35
        if (*(char *)&hard_LC_COLLATE != 0) {
            // 0x9e88
            result2 = xmemcoll0((char *)a1, v1, (char *)v1, v2);
        } else {
            int64_t v5 = function_3a80(); // 0x9e4b
            result2 = v5;
            if ((int32_t)v5 == 0) {
                // 0x9e54
                result2 = (int64_t)(v4 > v3) - (int64_t)(v4 < v3) & 0xffffffff;
            }
        }
    } else {
        // 0x9e0d
        result2 = v3 == 0 ? 0 : 0xffffffff;
    }
    // 0x9e12
    if (*(char *)&reverse == 0) {
        // 0x9e24
        return result2;
    }
    if ((int32_t)result2 < 0) {
        // 0x9e24
        return 1;
    }
    // 0x9e24
    return -result2 & 0xffffffff;
}