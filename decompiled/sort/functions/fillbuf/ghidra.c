undefined8 fillbuf(byte **param_1,FILE *param_2,undefined8 param_3)

{
  byte *pbVar1;
  long lVar2;
  byte bVar3;
  long *plVar4;
  ulong __n;
  size_t sVar5;
  byte *pbVar6;
  byte *pbVar7;
  undefined8 uVar8;
  byte *pbVar9;
  byte *pbVar10;
  byte *pbVar11;
  byte *pbVar12;
  byte *pbVar13;
  byte **ppbVar14;
  byte **ppbVar15;
  long in_FS_OFFSET;
  byte *local_88;
  ulong local_48;
  long local_40;
  
  plVar4 = keylist;
  bVar3 = eolchar;
  pbVar10 = merge_buffer_size;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  pbVar1 = param_1[5];
  if (*(char *)(param_1 + 6) == '\0') {
    pbVar12 = param_1[1];
    pbVar6 = param_1[4];
    if (pbVar12 == pbVar6) {
      pbVar6 = param_1[2];
    }
    else {
      memmove(*param_1,*param_1 + ((long)pbVar12 - (long)pbVar6),(size_t)pbVar6);
      pbVar12 = param_1[4];
      param_1[2] = (byte *)0x0;
      pbVar6 = (byte *)0x0;
      param_1[1] = pbVar12;
    }
    pbVar10 = pbVar10 + -0x22;
    pbVar11 = *param_1;
    pbVar9 = param_1[3];
    do {
      pbVar12 = pbVar11 + (long)pbVar12;
      ppbVar14 = (byte **)(pbVar11 + (long)pbVar9 + (long)pbVar6 * -0x20);
      pbVar9 = pbVar11 + (long)pbVar9 + (-(long)pbVar12 - (long)pbVar1 * (long)pbVar6);
      if (pbVar6 != (byte *)0x0) {
        pbVar11 = ppbVar14[1] + (long)*ppbVar14;
      }
      do {
        if (pbVar9 <= pbVar1 + 1) break;
        __n = (ulong)(pbVar9 + -1) / (ulong)(pbVar1 + 1);
        sVar5 = fread_unlocked(pbVar12,1,__n,param_2);
        pbVar9 = pbVar9 + -sVar5;
        local_88 = pbVar12 + sVar5;
        if (__n != sVar5) {
          if ((param_2->_flags & 0x20U) != 0) {
            uVar8 = dcgettext(0,"read failed",5);
                    /* WARNING: Subroutine does not return */
            sort_die(uVar8,param_3);
          }
          if ((param_2->_flags & 0x10U) != 0) {
            *(undefined *)(param_1 + 6) = 1;
            if (*param_1 == local_88) goto LAB_00107d95;
            if ((pbVar11 != local_88) && (local_88[-1] != bVar3)) {
              *local_88 = bVar3;
              local_88 = local_88 + 1;
            }
          }
        }
        pbVar6 = pbVar11;
        ppbVar15 = ppbVar14;
        while (ppbVar14 = ppbVar15, pbVar11 = pbVar6,
              pbVar7 = (byte *)memchr(pbVar12,(int)(char)bVar3,(long)local_88 - (long)pbVar12),
              pbVar7 != (byte *)0x0) {
          pbVar12 = pbVar7 + 1;
          ppbVar15 = ppbVar14 + -4;
          *pbVar7 = 0;
          *ppbVar15 = pbVar11;
          pbVar13 = pbVar12 + -(long)pbVar11;
          ppbVar14[-3] = pbVar13;
          if (pbVar10 < pbVar13) {
            pbVar10 = pbVar13;
          }
          pbVar9 = pbVar9 + -(long)pbVar1;
          pbVar6 = pbVar12;
          if (plVar4 != (long *)0x0) {
            if (plVar4[2] != -1) {
              pbVar7 = (byte *)limfield_isra_0(pbVar11,pbVar13,plVar4);
            }
            lVar2 = *plVar4;
            ppbVar14[-1] = pbVar7;
            if (lVar2 == -1) {
              if (*(char *)(plVar4 + 6) != '\0') {
                while (blanks[*pbVar11] != '\0') {
                  pbVar11 = pbVar11 + 1;
                }
              }
              ppbVar14[-2] = pbVar11;
              pbVar6 = pbVar12;
            }
            else {
              pbVar6 = (byte *)begfield_isra_0(pbVar11,pbVar13,plVar4);
              ppbVar14[-2] = pbVar6;
              pbVar6 = pbVar12;
            }
          }
        }
        pbVar12 = local_88;
      } while (*(char *)(param_1 + 6) == '\0');
      pbVar6 = *param_1;
      param_1[1] = pbVar12 + -(long)pbVar6;
      pbVar9 = (byte *)((long)(pbVar6 + (long)param_1[3]) - (long)ppbVar14 >> 5);
      param_1[2] = pbVar9;
      if (pbVar9 != (byte *)0x0) goto LAB_00107ec0;
      local_48 = (ulong)param_1[3] >> 5;
      pbVar11 = (byte *)x2nrealloc(pbVar6,&local_48,0x20);
      pbVar9 = (byte *)(local_48 << 5);
      *param_1 = pbVar11;
      pbVar12 = param_1[1];
      param_1[3] = pbVar9;
      pbVar6 = param_1[2];
    } while( true );
  }
LAB_00107d95:
  uVar8 = 0;
LAB_00107d97:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar8;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
LAB_00107ec0:
  merge_buffer_size = pbVar10 + 0x22;
  uVar8 = 1;
  param_1[4] = pbVar12 + -(long)pbVar11;
  goto LAB_00107d97;
}