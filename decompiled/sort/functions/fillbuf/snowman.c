signed char fillbuf(void** rdi, void** rsi, void** rdx) {
    void** rbx4;
    void** rbp5;
    void** v6;
    void** v7;
    void** v8;
    void** rax9;
    void** v10;
    uint32_t eax11;
    int1_t zf12;
    unsigned char v13;
    void** v14;
    int32_t eax15;
    void** r10_16;
    void** rdx17;
    void** rdi18;
    void** rax19;
    void** rbp20;
    void** r15_21;
    void** r12_22;
    void** v23;
    void*** rcx24;
    void** r14_25;
    struct s0* r12_26;
    void** v27;
    void** r13_28;
    void** r15_29;
    struct s0* r14_30;
    struct s0* r12_31;
    void** r14_32;
    void** r15_33;
    void** rax34;
    void** rax35;
    void** v36;
    void** r10_37;
    struct s0* r15_38;
    void** r14_39;
    void** r13_40;
    int32_t ebx41;
    void** r12_42;
    void** eax43;
    int64_t rsi44;
    void** rax45;
    void** r11_46;
    int1_t zf47;
    void** rax48;
    int64_t rax49;
    uint32_t ecx50;
    void** rdi51;
    void** rdx52;
    void** rax53;
    void** rax54;
    void* rdx55;
    void** rax56;

    rbx4 = keylist;
    rbp5 = merge_buffer_size;
    v6 = rdi;
    v7 = rsi;
    v8 = rdx;
    rax9 = g28;
    v10 = rax9;
    eax11 = eolchar;
    zf12 = *reinterpret_cast<void***>(rdi + 48) == 0;
    v13 = *reinterpret_cast<unsigned char*>(&eax11);
    v14 = *reinterpret_cast<void***>(rdi + 40);
    if (!zf12) {
        addr_7d95_2:
        eax15 = 0;
    } else {
        r10_16 = *reinterpret_cast<void***>(rdi + 8);
        rdx17 = *reinterpret_cast<void***>(rdi + 32);
        if (r10_16 != rdx17) {
            rdi18 = *reinterpret_cast<void***>(v6);
            fun_3c70(rdi18, reinterpret_cast<unsigned char>(rdi18) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r10_16) - reinterpret_cast<unsigned char>(rdx17)), rdx17);
            r10_16 = *reinterpret_cast<void***>(v6 + 32);
            *reinterpret_cast<void***>(v6 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax19) = 0;
            *reinterpret_cast<int32_t*>(&rax19 + 4) = 0;
            *reinterpret_cast<void***>(v6 + 8) = r10_16;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 16);
        }
        rbp20 = rbp5 - 34;
        r15_21 = *reinterpret_cast<void***>(v6);
        r12_22 = *reinterpret_cast<void***>(v6 + 24);
        v23 = v14 + 1;
        while (1) {
            rcx24 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(r12_22));
            r14_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(r10_16));
            r12_26 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rcx24) - (reinterpret_cast<unsigned char>(rax19) << 5));
            v27 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx24) - reinterpret_cast<unsigned char>(v14) * reinterpret_cast<unsigned char>(rax19) - reinterpret_cast<unsigned char>(r14_25));
            if (!rax19) {
                r13_28 = r15_21;
                r15_29 = r14_25;
                r14_30 = r12_26;
            } else {
                r15_29 = r14_25;
                r14_30 = r12_26;
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_26->f8) + reinterpret_cast<unsigned char>(r12_26->f0));
            }
            if (reinterpret_cast<unsigned char>(v27) <= reinterpret_cast<unsigned char>(v23)) {
                addr_7e39_11:
                r12_31 = r14_30;
                r14_32 = r15_29;
                r15_33 = r13_28;
            } else {
                do {
                    rax34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27 - 1) / reinterpret_cast<unsigned char>(v23));
                    rax35 = fun_38c0(r15_29, 1, rax34, v7);
                    v36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_29) + reinterpret_cast<unsigned char>(rax35));
                    if (rax34 == rax35) {
                        addr_7cf0_13:
                        r10_37 = r15_29;
                        r15_38 = r14_30;
                        r14_39 = r13_28;
                        r13_40 = rbx4;
                        ebx41 = reinterpret_cast<signed char>(v13);
                        r12_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(rax35));
                    } else {
                        eax43 = *reinterpret_cast<void***>(v7);
                        if (*reinterpret_cast<unsigned char*>(&eax43) & 32) 
                            goto addr_7edf_15;
                        if (!(*reinterpret_cast<unsigned char*>(&eax43) & 16)) 
                            goto addr_7cf0_13; else 
                            goto addr_7cbd_17;
                    }
                    while (*reinterpret_cast<int32_t*>(&rsi44) = ebx41, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi44) + 4) = 0, rax45 = fun_3a70(r10_37, rsi44), !!rax45) {
                        r10_37 = rax45 + 1;
                        r15_38 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r15_38) - 32);
                        *reinterpret_cast<void***>(rax45) = reinterpret_cast<void**>(0);
                        r15_38->f0 = r14_39;
                        r11_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_37) - reinterpret_cast<unsigned char>(r14_39));
                        r15_38->f8 = r11_46;
                        if (reinterpret_cast<unsigned char>(rbp20) < reinterpret_cast<unsigned char>(r11_46)) {
                            rbp20 = r11_46;
                        }
                        r12_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_42) - reinterpret_cast<unsigned char>(v14));
                        if (r13_40) {
                            if (*reinterpret_cast<void***>(r13_40 + 16) != 0xffffffffffffffff) {
                                rax45 = limfield_isra_0(r14_39, r11_46, r13_40, r14_39, r11_46, r13_40);
                            }
                            zf47 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_40) == 0xffffffffffffffff);
                            r15_38->f18 = rax45;
                            if (!zf47) {
                                rax48 = begfield_isra_0(r14_39, r11_46, r13_40, r14_39, r11_46, r13_40);
                                r15_38->f10 = rax48;
                            } else {
                                if (*reinterpret_cast<void***>(r13_40 + 48)) {
                                    while (*reinterpret_cast<uint32_t*>(&rax49) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_39)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax49) + 4) = 0, !!*reinterpret_cast<signed char*>(0x1d760 + rax49)) {
                                        ++r14_39;
                                    }
                                }
                                r15_38->f10 = r14_39;
                            }
                        }
                        r14_39 = r10_37;
                    }
                    rbx4 = r13_40;
                    v27 = r12_42;
                    r13_28 = r14_39;
                    r14_30 = r15_38;
                    if (*reinterpret_cast<void***>(v6 + 48)) 
                        goto addr_7eb4_33; else 
                        continue;
                    addr_7cbd_17:
                    *reinterpret_cast<void***>(v6 + 48) = reinterpret_cast<void**>(1);
                    if (*reinterpret_cast<void***>(v6) == v36) 
                        goto addr_7d95_2;
                    if (r13_28 == v36) 
                        goto addr_7cf0_13;
                    ecx50 = v13;
                    if (*reinterpret_cast<void***>(v36 + 0xffffffffffffffff) == *reinterpret_cast<void***>(&ecx50)) 
                        goto addr_7cf0_13;
                    *reinterpret_cast<void***>(v36) = *reinterpret_cast<void***>(&ecx50);
                    ++v36;
                    goto addr_7cf0_13;
                    r15_29 = v36;
                } while (reinterpret_cast<unsigned char>(v27) > reinterpret_cast<unsigned char>(v23));
                goto addr_7e39_11;
            }
            addr_7e42_38:
            rdi51 = *reinterpret_cast<void***>(v6);
            rdx52 = *reinterpret_cast<void***>(v6 + 24);
            *reinterpret_cast<void***>(v6 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_32) - reinterpret_cast<unsigned char>(rdi51));
            rax53 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rdi51) + reinterpret_cast<unsigned char>(rdx52) - reinterpret_cast<uint64_t>(r12_31)) >> 5);
            *reinterpret_cast<void***>(v6 + 16) = rax53;
            if (rax53) 
                goto addr_7ec0_39;
            rax54 = x2nrealloc();
            r15_21 = rax54;
            r12_22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx52) >> 5) << 5);
            *reinterpret_cast<void***>(v6) = r15_21;
            r10_16 = *reinterpret_cast<void***>(v6 + 8);
            *reinterpret_cast<void***>(v6 + 24) = r12_22;
            rax19 = *reinterpret_cast<void***>(v6 + 16);
            continue;
            addr_7eb4_33:
            r12_31 = r15_38;
            r14_32 = v36;
            r15_33 = r13_28;
            goto addr_7e42_38;
        }
    }
    addr_7d97_41:
    rdx55 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (!rdx55) {
        return *reinterpret_cast<signed char*>(&eax15);
    }
    addr_7eff_43:
    fun_3950();
    addr_7ec0_39:
    eax15 = 1;
    merge_buffer_size = rbp20 + 34;
    *reinterpret_cast<void***>(v6 + 32) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_32) - reinterpret_cast<unsigned char>(r15_33));
    goto addr_7d97_41;
    addr_7edf_15:
    rax56 = fun_3920();
    sort_die(rax56, v8, 5, rax56, v8, 5);
    goto addr_7eff_43;
}