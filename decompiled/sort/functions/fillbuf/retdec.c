bool fillbuf(int32_t * buf, struct _IO_FILE * fp, char * file) {
    int64_t v1 = (int64_t)buf;
    int64_t v2 = __readfsqword(40); // 0x7bbb
    char * v3 = (char *)(v1 + 48); // 0x7bd2
    int64_t v4 = *(int64_t *)(v1 + 40); // 0x7bda
    int64_t v5 = 0; // 0x7be3
    if (*v3 == 0) {
        int64_t * v6 = (int64_t *)(v1 + 8); // 0x7be9
        int64_t v7 = *v6; // 0x7be9
        int64_t * v8 = (int64_t *)(v1 + 32); // 0x7bed
        int64_t * v9; // 0x7b90
        int64_t v10; // 0x7b90
        int64_t v11; // 0x7b90
        if (v7 != *v8) {
            // 0x7dd7
            function_3c70();
            int64_t v12 = *v8; // 0x7deb
            int64_t * v13 = (int64_t *)(v1 + 16);
            *v13 = 0;
            *v6 = v12;
            v9 = v13;
            v11 = 0;
            v10 = v12;
        } else {
            int64_t * v14 = (int64_t *)(v1 + 16);
            v9 = v14;
            v11 = *v14;
            v10 = v7;
        }
        int64_t v15 = (int64_t)keylist; // 0x7b9e
        int64_t * v16 = (int64_t *)(v1 + 24); // 0x7c0a
        uint64_t v17 = v4 + 1; // 0x7c13
        int64_t v18 = merge_buffer_size - 34; // 0x7c17
        int64_t v19 = *v16; // 0x7c17
        int64_t v20 = v1 + v19; // 0x7c1c
        int64_t v21 = v20 - 32 * v11; // 0x7c2e
        int64_t v22 = v1; // 0x7c48
        if (v11 != 0) {
            // 0x7c4e
            v22 = *(int64_t *)v21 + *(int64_t *)(v21 + 8);
        }
        int64_t v23 = v1 + v10; // 0x7c23
        int64_t v24 = v20 - v11 * v4 - v23; // 0x7c3d
        int64_t v25 = v24; // 0x7c6a
        int64_t v26 = v22; // 0x7c6a
        int64_t v27 = v21; // 0x7c6a
        int64_t v28 = v23; // 0x7c6a
        int64_t v29 = v19; // 0x7c6a
        int64_t v30 = v18; // 0x7c6a
        int64_t v31 = v1; // 0x7c6a
        int64_t v32 = v21; // 0x7c6a
        int64_t v33 = v23; // 0x7c6a
        int64_t v34 = v22; // 0x7c6a
        int64_t v35; // 0x7b90
        int64_t v36; // 0x7b90
        int64_t v37; // 0x7b90
        int64_t v38; // 0x7b90
        int64_t v39; // 0x7b90
        int64_t v40; // 0x7b90
        int64_t v41; // 0x7b90
        int64_t v42; // 0x7b90
        int64_t v43; // 0x7b90
        int64_t v44; // 0x7b90
        int64_t v45; // 0x7b90
        int64_t v46; // 0x7b90
        int64_t v47; // 0x7b90
        int64_t v48; // 0x7b90
        int64_t v49; // 0x7b90
        int64_t v50; // 0x7b90
        int64_t v51; // 0x7b90
        int64_t v52; // 0x7b90
        uint64_t v53; // 0x7b90
        int64_t v54; // 0x7b90
        int64_t v55; // 0x7b90
        int64_t v56; // 0x7b90
        int64_t v57; // 0x7c93
        int64_t v58; // 0x7c98
        int64_t v59; // 0x7d31
        int32_t v60; // 0x7caf
        int64_t v61; // 0x7c9d
        int64_t v62; // 0x7d3f
        int64_t v63; // 0x7d43
        uint64_t v64; // 0x7d50
        int64_t v65; // 0x7d5a
        int64_t v66; // 0x7d5e
        int64_t v67; // 0x7d31
        unsigned char v68; // 0x7dc4
        if (v24 > v17) {
            v57 = function_38c0();
            if (v57 != (v25 - 1) / v17) {
                // 0x7caa
                v60 = *(int32_t *)&v56;
                if ((v60 & 32) != 0) {
                    // 0x7edf
                    sort_die((char *)function_3920(), file);
                    return function_3950() % 2 != 0;
                }
                if ((v60 & 16) != 0) {
                    // 0x7cbd
                    *v3 = 1;
                    v5 = 0;
                    goto lab_0x7d97;
                }
            }
            // 0x7cf0
            v58 = v25 - v57;
            v59 = function_3a70();
            v35 = v59;
            v46 = v27;
            v40 = v26;
            v38 = v58;
            v54 = v18;
            v37 = v28;
            v39 = v58;
            v42 = v26;
            v48 = v27;
            if (v59 != 0) {
                v53 = v18;
                v41 = v40;
                v47 = v46;
                v36 = v35;
                v62 = v36 + 1;
                v63 = v47 - 32;
                *(char *)v36 = 0;
                *(int64_t *)v63 = v41;
                v64 = v62 - v41;
                *(int64_t *)(v47 - 24) = v64;
                if (keylist != NULL) {
                    // 0x7d68
                    v50 = v36;
                    if (*(int64_t *)(v15 + 16) != -1) {
                        // 0x7d6f
                        v50 = limfield_isra_0(v41, v64, v15);
                    }
                    // 0x7d7d
                    *(int64_t *)(v47 - 8) = v50;
                    if (*(int64_t *)keylist != -1) {
                        // 0x7d10
                        *(int64_t *)(v47 - 16) = begfield_isra_0(v41, v64, v15);
                    } else {
                        // 0x7d88
                        v44 = v41;
                        v45 = v41;
                        if (*(char *)(v15 + 48) != 0) {
                            v68 = *(char *)v45;
                            v44 = v45;
                            v45++;
                            while (*(char *)((int64_t)v68 + (int64_t)&blanks) != 0) {
                                // 0x7dc4
                                v68 = *(char *)v45;
                                v44 = v45;
                                v45++;
                            }
                        }
                        // 0x7d8f
                        *(int64_t *)(v47 - 16) = v44;
                    }
                }
                // 0x7d22
                v65 = v53 < v64 ? v64 : v53;
                v66 = v38 - v4;
                v67 = function_3a70();
                v38 = v66;
                v54 = v65;
                v37 = v62;
                v39 = v66;
                v42 = v62;
                v48 = v63;
                while (v67 != 0) {
                    // 0x7d3f
                    v53 = v65;
                    v41 = v62;
                    v47 = v63;
                    v36 = v67;
                    v62 = v36 + 1;
                    v63 = v47 - 32;
                    *(char *)v36 = 0;
                    *(int64_t *)v63 = v41;
                    v64 = v62 - v41;
                    *(int64_t *)(v47 - 24) = v64;
                    if (keylist != NULL) {
                        // 0x7d68
                        v50 = v36;
                        if (*(int64_t *)(v15 + 16) != -1) {
                            // 0x7d6f
                            v50 = limfield_isra_0(v41, v64, v15);
                        }
                        // 0x7d7d
                        *(int64_t *)(v47 - 8) = v50;
                        if (*(int64_t *)keylist != -1) {
                            // 0x7d10
                            *(int64_t *)(v47 - 16) = begfield_isra_0(v41, v64, v15);
                        } else {
                            // 0x7d88
                            v44 = v41;
                            v45 = v41;
                            if (*(char *)(v15 + 48) != 0) {
                                v68 = *(char *)v45;
                                v44 = v45;
                                v45++;
                                while (*(char *)((int64_t)v68 + (int64_t)&blanks) != 0) {
                                    // 0x7dc4
                                    v68 = *(char *)v45;
                                    v44 = v45;
                                    v45++;
                                }
                            }
                            // 0x7d8f
                            *(int64_t *)(v47 - 16) = v44;
                        }
                    }
                    // 0x7d22
                    v65 = v53 < v64 ? v64 : v53;
                    v66 = v38 - v4;
                    v67 = function_3a70();
                    v38 = v66;
                    v54 = v65;
                    v37 = v62;
                    v39 = v66;
                    v42 = v62;
                    v48 = v63;
                }
            }
            // 0x7e08
            v61 = v57 + v28;
            v49 = v48;
            v43 = v42;
            v55 = v54;
            v25 = v39;
            v51 = v55;
            v26 = v43;
            v27 = v49;
            v28 = v61;
            while (v39 > v17 == *v3 == 0) {
                // 0x7c70
                v57 = function_38c0();
                if (v57 != (v25 - 1) / v17) {
                    // 0x7caa
                    v60 = *(int32_t *)&v56;
                    if ((v60 & 32) != 0) {
                        // 0x7edf
                        sort_die((char *)function_3920(), file);
                        return function_3950() % 2 != 0;
                    }
                    if ((v60 & 16) != 0) {
                        // 0x7cbd
                        *v3 = 1;
                        v5 = 0;
                        goto lab_0x7d97;
                    }
                }
                // 0x7cf0
                v58 = v25 - v57;
                v59 = function_3a70();
                v35 = v59;
                v46 = v27;
                v40 = v26;
                v38 = v58;
                v52 = v51;
                v54 = v51;
                v37 = v28;
                v39 = v58;
                v42 = v26;
                v48 = v27;
                if (v59 != 0) {
                    v53 = v52;
                    v41 = v40;
                    v47 = v46;
                    v36 = v35;
                    v62 = v36 + 1;
                    v63 = v47 - 32;
                    *(char *)v36 = 0;
                    *(int64_t *)v63 = v41;
                    v64 = v62 - v41;
                    *(int64_t *)(v47 - 24) = v64;
                    if (keylist != NULL) {
                        // 0x7d68
                        v50 = v36;
                        if (*(int64_t *)(v15 + 16) != -1) {
                            // 0x7d6f
                            v50 = limfield_isra_0(v41, v64, v15);
                        }
                        // 0x7d7d
                        *(int64_t *)(v47 - 8) = v50;
                        if (*(int64_t *)keylist != -1) {
                            // 0x7d10
                            *(int64_t *)(v47 - 16) = begfield_isra_0(v41, v64, v15);
                        } else {
                            // 0x7d88
                            v44 = v41;
                            v45 = v41;
                            if (*(char *)(v15 + 48) != 0) {
                                v68 = *(char *)v45;
                                v44 = v45;
                                v45++;
                                while (*(char *)((int64_t)v68 + (int64_t)&blanks) != 0) {
                                    // 0x7dc4
                                    v68 = *(char *)v45;
                                    v44 = v45;
                                    v45++;
                                }
                            }
                            // 0x7d8f
                            *(int64_t *)(v47 - 16) = v44;
                        }
                    }
                    // 0x7d22
                    v65 = v53 < v64 ? v64 : v53;
                    v66 = v38 - v4;
                    v67 = function_3a70();
                    v38 = v66;
                    v54 = v65;
                    v37 = v62;
                    v39 = v66;
                    v42 = v62;
                    v48 = v63;
                    while (v67 != 0) {
                        // 0x7d3f
                        v53 = v65;
                        v41 = v62;
                        v47 = v63;
                        v36 = v67;
                        v62 = v36 + 1;
                        v63 = v47 - 32;
                        *(char *)v36 = 0;
                        *(int64_t *)v63 = v41;
                        v64 = v62 - v41;
                        *(int64_t *)(v47 - 24) = v64;
                        if (keylist != NULL) {
                            // 0x7d68
                            v50 = v36;
                            if (*(int64_t *)(v15 + 16) != -1) {
                                // 0x7d6f
                                v50 = limfield_isra_0(v41, v64, v15);
                            }
                            // 0x7d7d
                            *(int64_t *)(v47 - 8) = v50;
                            if (*(int64_t *)keylist != -1) {
                                // 0x7d10
                                *(int64_t *)(v47 - 16) = begfield_isra_0(v41, v64, v15);
                            } else {
                                // 0x7d88
                                v44 = v41;
                                v45 = v41;
                                if (*(char *)(v15 + 48) != 0) {
                                    v68 = *(char *)v45;
                                    v44 = v45;
                                    v45++;
                                    while (*(char *)((int64_t)v68 + (int64_t)&blanks) != 0) {
                                        // 0x7dc4
                                        v68 = *(char *)v45;
                                        v44 = v45;
                                        v45++;
                                    }
                                }
                                // 0x7d8f
                                *(int64_t *)(v47 - 16) = v44;
                            }
                        }
                        // 0x7d22
                        v65 = v53 < v64 ? v64 : v53;
                        v66 = v38 - v4;
                        v67 = function_3a70();
                        v38 = v66;
                        v54 = v65;
                        v37 = v62;
                        v39 = v66;
                        v42 = v62;
                        v48 = v63;
                    }
                }
                // 0x7e08
                v61 = v57 + v28;
                v49 = v48;
                v43 = v42;
                v55 = v54;
                v25 = v39;
                v51 = v55;
                v26 = v43;
                v27 = v49;
                v28 = v61;
            }
            // 0x7e42
            v29 = *v16;
            v30 = v55;
            v31 = v37;
            v32 = v49;
            v33 = v61;
            v34 = v43;
        }
        int64_t v69 = v33;
        int64_t v70 = v31;
        int64_t v71 = v30;
        *v6 = v69 - v70;
        int64_t v72 = v70 + v29 - v32 >> 5; // 0x7e5f
        *v9 = v72;
        while (v72 == 0) {
            int64_t v73 = x2nrealloc(); // 0x7e7c
            v19 = v29 & -32;
            *(int64_t *)buf = v73;
            *v16 = v19;
            int64_t v74 = *v9; // 0x7e9d
            v20 = v73 + v19;
            v21 = v20 - 32 * v74;
            v22 = v73;
            if (v74 != 0) {
                // 0x7c4e
                v22 = *(int64_t *)v21 + *(int64_t *)(v21 + 8);
            }
            // 0x7c60
            v23 = v73 + *v6;
            v24 = v20 - v74 * v4 - v23;
            v25 = v24;
            v51 = v71;
            v26 = v22;
            v27 = v21;
            v28 = v23;
            v29 = v19;
            v30 = v71;
            v31 = v70;
            v32 = v21;
            v33 = v23;
            v34 = v22;
            if (v24 > v17) {
                v57 = function_38c0();
                if (v57 != (v25 - 1) / v17) {
                    // 0x7caa
                    v60 = *(int32_t *)&v56;
                    if ((v60 & 32) != 0) {
                        // 0x7edf
                        sort_die((char *)function_3920(), file);
                        return function_3950() % 2 != 0;
                    }
                    if ((v60 & 16) != 0) {
                        // 0x7cbd
                        *v3 = 1;
                        v5 = 0;
                        goto lab_0x7d97;
                    }
                }
                // 0x7cf0
                v58 = v25 - v57;
                v59 = function_3a70();
                v35 = v59;
                v46 = v27;
                v40 = v26;
                v38 = v58;
                v52 = v51;
                v54 = v51;
                v37 = v28;
                v39 = v58;
                v42 = v26;
                v48 = v27;
                if (v59 != 0) {
                    v53 = v52;
                    v41 = v40;
                    v47 = v46;
                    v36 = v35;
                    v62 = v36 + 1;
                    v63 = v47 - 32;
                    *(char *)v36 = 0;
                    *(int64_t *)v63 = v41;
                    v64 = v62 - v41;
                    *(int64_t *)(v47 - 24) = v64;
                    if (keylist != NULL) {
                        // 0x7d68
                        v50 = v36;
                        if (*(int64_t *)(v15 + 16) != -1) {
                            // 0x7d6f
                            v50 = limfield_isra_0(v41, v64, v15);
                        }
                        // 0x7d7d
                        *(int64_t *)(v47 - 8) = v50;
                        if (*(int64_t *)keylist != -1) {
                            // 0x7d10
                            *(int64_t *)(v47 - 16) = begfield_isra_0(v41, v64, v15);
                        } else {
                            // 0x7d88
                            v44 = v41;
                            v45 = v41;
                            if (*(char *)(v15 + 48) != 0) {
                                v68 = *(char *)v45;
                                v44 = v45;
                                v45++;
                                while (*(char *)((int64_t)v68 + (int64_t)&blanks) != 0) {
                                    // 0x7dc4
                                    v68 = *(char *)v45;
                                    v44 = v45;
                                    v45++;
                                }
                            }
                            // 0x7d8f
                            *(int64_t *)(v47 - 16) = v44;
                        }
                    }
                    // 0x7d22
                    v65 = v53 < v64 ? v64 : v53;
                    v66 = v38 - v4;
                    v67 = function_3a70();
                    v38 = v66;
                    v54 = v65;
                    v37 = v62;
                    v39 = v66;
                    v42 = v62;
                    v48 = v63;
                    while (v67 != 0) {
                        // 0x7d3f
                        v53 = v65;
                        v41 = v62;
                        v47 = v63;
                        v36 = v67;
                        v62 = v36 + 1;
                        v63 = v47 - 32;
                        *(char *)v36 = 0;
                        *(int64_t *)v63 = v41;
                        v64 = v62 - v41;
                        *(int64_t *)(v47 - 24) = v64;
                        if (keylist != NULL) {
                            // 0x7d68
                            v50 = v36;
                            if (*(int64_t *)(v15 + 16) != -1) {
                                // 0x7d6f
                                v50 = limfield_isra_0(v41, v64, v15);
                            }
                            // 0x7d7d
                            *(int64_t *)(v47 - 8) = v50;
                            if (*(int64_t *)keylist != -1) {
                                // 0x7d10
                                *(int64_t *)(v47 - 16) = begfield_isra_0(v41, v64, v15);
                            } else {
                                // 0x7d88
                                v44 = v41;
                                v45 = v41;
                                if (*(char *)(v15 + 48) != 0) {
                                    v68 = *(char *)v45;
                                    v44 = v45;
                                    v45++;
                                    while (*(char *)((int64_t)v68 + (int64_t)&blanks) != 0) {
                                        // 0x7dc4
                                        v68 = *(char *)v45;
                                        v44 = v45;
                                        v45++;
                                    }
                                }
                                // 0x7d8f
                                *(int64_t *)(v47 - 16) = v44;
                            }
                        }
                        // 0x7d22
                        v65 = v53 < v64 ? v64 : v53;
                        v66 = v38 - v4;
                        v67 = function_3a70();
                        v38 = v66;
                        v54 = v65;
                        v37 = v62;
                        v39 = v66;
                        v42 = v62;
                        v48 = v63;
                    }
                }
                // 0x7e08
                v61 = v57 + v28;
                v49 = v48;
                v43 = v42;
                v55 = v54;
                v25 = v39;
                v51 = v55;
                v26 = v43;
                v27 = v49;
                v28 = v61;
                while (v39 > v17 == *v3 == 0) {
                    // 0x7c70
                    v57 = function_38c0();
                    if (v57 != (v25 - 1) / v17) {
                        // 0x7caa
                        v60 = *(int32_t *)&v56;
                        if ((v60 & 32) != 0) {
                            // 0x7edf
                            sort_die((char *)function_3920(), file);
                            return function_3950() % 2 != 0;
                        }
                        if ((v60 & 16) != 0) {
                            // 0x7cbd
                            *v3 = 1;
                            v5 = 0;
                            goto lab_0x7d97;
                        }
                    }
                    // 0x7cf0
                    v58 = v25 - v57;
                    v59 = function_3a70();
                    v35 = v59;
                    v46 = v27;
                    v40 = v26;
                    v38 = v58;
                    v52 = v51;
                    v54 = v51;
                    v37 = v28;
                    v39 = v58;
                    v42 = v26;
                    v48 = v27;
                    if (v59 != 0) {
                        v53 = v52;
                        v41 = v40;
                        v47 = v46;
                        v36 = v35;
                        v62 = v36 + 1;
                        v63 = v47 - 32;
                        *(char *)v36 = 0;
                        *(int64_t *)v63 = v41;
                        v64 = v62 - v41;
                        *(int64_t *)(v47 - 24) = v64;
                        if (keylist != NULL) {
                            // 0x7d68
                            v50 = v36;
                            if (*(int64_t *)(v15 + 16) != -1) {
                                // 0x7d6f
                                v50 = limfield_isra_0(v41, v64, v15);
                            }
                            // 0x7d7d
                            *(int64_t *)(v47 - 8) = v50;
                            if (*(int64_t *)keylist != -1) {
                                // 0x7d10
                                *(int64_t *)(v47 - 16) = begfield_isra_0(v41, v64, v15);
                            } else {
                                // 0x7d88
                                v44 = v41;
                                v45 = v41;
                                if (*(char *)(v15 + 48) != 0) {
                                    v68 = *(char *)v45;
                                    v44 = v45;
                                    v45++;
                                    while (*(char *)((int64_t)v68 + (int64_t)&blanks) != 0) {
                                        // 0x7dc4
                                        v68 = *(char *)v45;
                                        v44 = v45;
                                        v45++;
                                    }
                                }
                                // 0x7d8f
                                *(int64_t *)(v47 - 16) = v44;
                            }
                        }
                        // 0x7d22
                        v65 = v53 < v64 ? v64 : v53;
                        v66 = v38 - v4;
                        v67 = function_3a70();
                        v38 = v66;
                        v54 = v65;
                        v37 = v62;
                        v39 = v66;
                        v42 = v62;
                        v48 = v63;
                        while (v67 != 0) {
                            // 0x7d3f
                            v53 = v65;
                            v41 = v62;
                            v47 = v63;
                            v36 = v67;
                            v62 = v36 + 1;
                            v63 = v47 - 32;
                            *(char *)v36 = 0;
                            *(int64_t *)v63 = v41;
                            v64 = v62 - v41;
                            *(int64_t *)(v47 - 24) = v64;
                            if (keylist != NULL) {
                                // 0x7d68
                                v50 = v36;
                                if (*(int64_t *)(v15 + 16) != -1) {
                                    // 0x7d6f
                                    v50 = limfield_isra_0(v41, v64, v15);
                                }
                                // 0x7d7d
                                *(int64_t *)(v47 - 8) = v50;
                                if (*(int64_t *)keylist != -1) {
                                    // 0x7d10
                                    *(int64_t *)(v47 - 16) = begfield_isra_0(v41, v64, v15);
                                } else {
                                    // 0x7d88
                                    v44 = v41;
                                    v45 = v41;
                                    if (*(char *)(v15 + 48) != 0) {
                                        v68 = *(char *)v45;
                                        v44 = v45;
                                        v45++;
                                        while (*(char *)((int64_t)v68 + (int64_t)&blanks) != 0) {
                                            // 0x7dc4
                                            v68 = *(char *)v45;
                                            v44 = v45;
                                            v45++;
                                        }
                                    }
                                    // 0x7d8f
                                    *(int64_t *)(v47 - 16) = v44;
                                }
                            }
                            // 0x7d22
                            v65 = v53 < v64 ? v64 : v53;
                            v66 = v38 - v4;
                            v67 = function_3a70();
                            v38 = v66;
                            v54 = v65;
                            v37 = v62;
                            v39 = v66;
                            v42 = v62;
                            v48 = v63;
                        }
                    }
                    // 0x7e08
                    v61 = v57 + v28;
                    v49 = v48;
                    v43 = v42;
                    v55 = v54;
                    v25 = v39;
                    v51 = v55;
                    v26 = v43;
                    v27 = v49;
                    v28 = v61;
                }
                // 0x7e42
                v29 = *v16;
                v30 = v55;
                v31 = v37;
                v32 = v49;
                v33 = v61;
                v34 = v43;
            }
            // 0x7e42
            v69 = v33;
            v70 = v31;
            v71 = v30;
            *v6 = v69 - v70;
            v72 = v70 + v29 - v32 >> 5;
            *v9 = v72;
        }
        // 0x7ec0
        merge_buffer_size = v71 + 34;
        *v8 = v69 - v34;
        v5 = 1;
    }
    goto lab_0x7d97;
  lab_0x7d97:
    // 0x7d97
    if (v2 == __readfsqword(40)) {
        // 0x7dab
        return v5 != 0;
    }
  lab_0x7eff:
    // 0x7eff
    return function_3950() % 2 != 0;
}