unlock_node (struct merge_node *node)
{
  pthread_mutex_unlock (&node->lock);
}