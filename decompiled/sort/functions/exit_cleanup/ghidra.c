void exit_cleanup(void)

{
  undefined8 *puVar1;
  int iVar2;
  long in_FS_OFFSET;
  __sigset_t local_a0;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  if (temphead != (undefined8 *)0x0) {
    iVar2 = pthread_sigmask(0,(__sigset_t *)caught_signals,&local_a0);
    for (puVar1 = temphead; puVar1 != (undefined8 *)0x0; puVar1 = (undefined8 *)*puVar1) {
      unlink((char *)((long)puVar1 + 0xd));
    }
    temphead = (undefined8 *)0x0;
    if (iVar2 == 0) {
      pthread_sigmask(2,&local_a0,(__sigset_t *)0x0);
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    close_stdout();
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}