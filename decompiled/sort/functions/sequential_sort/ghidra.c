void sequential_sort(undefined4 *param_1,ulong param_2,undefined4 *param_3,char param_4)

{
  undefined4 uVar1;
  undefined4 uVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  undefined4 uVar8;
  undefined4 uVar9;
  undefined4 uVar10;
  undefined4 uVar11;
  undefined4 uVar12;
  undefined4 uVar13;
  undefined4 uVar14;
  undefined4 uVar15;
  undefined4 uVar16;
  undefined4 *puVar17;
  int iVar18;
  undefined4 *puVar19;
  undefined4 *puVar20;
  undefined4 *puVar21;
  undefined4 *puVar22;
  long lVar23;
  undefined4 *puVar24;
  ulong local_48;
  
  if (param_2 == 2) {
    iVar18 = compare(param_1 + -8,param_1 + -0x10);
    if (param_4 == '\0') {
      if (0 < iVar18) {
        uVar1 = param_1[-8];
        uVar2 = param_1[-7];
        uVar3 = param_1[-6];
        uVar4 = param_1[-5];
        uVar5 = param_1[-4];
        uVar6 = param_1[-3];
        uVar7 = param_1[-2];
        uVar8 = param_1[-1];
        uVar9 = param_1[-0x10];
        uVar10 = param_1[-0xf];
        uVar11 = param_1[-0xe];
        uVar12 = param_1[-0xd];
        uVar13 = param_1[-0xc];
        uVar14 = param_1[-0xb];
        uVar15 = param_1[-10];
        uVar16 = param_1[-9];
        param_3[-8] = uVar1;
        param_3[-7] = uVar2;
        param_3[-6] = uVar3;
        param_3[-5] = uVar4;
        param_3[-4] = uVar5;
        param_3[-3] = uVar6;
        param_3[-2] = uVar7;
        param_3[-1] = uVar8;
        param_1[-8] = uVar9;
        param_1[-7] = uVar10;
        param_1[-6] = uVar11;
        param_1[-5] = uVar12;
        param_1[-4] = uVar13;
        param_1[-3] = uVar14;
        param_1[-2] = uVar15;
        param_1[-1] = uVar16;
        param_1[-0x10] = uVar1;
        param_1[-0xf] = uVar2;
        param_1[-0xe] = uVar3;
        param_1[-0xd] = uVar4;
        param_1[-0xc] = uVar5;
        param_1[-0xb] = uVar6;
        param_1[-10] = uVar7;
        param_1[-9] = uVar8;
      }
    }
    else {
      puVar21 = param_1 + (long)(int)((0 < iVar18) - 2) * 8;
      param_1 = param_1 + (long)(int)~(uint)(0 < iVar18) * 8;
      uVar1 = param_1[1];
      uVar2 = param_1[2];
      uVar3 = param_1[3];
      uVar4 = param_1[4];
      uVar5 = param_1[5];
      uVar6 = param_1[6];
      uVar7 = param_1[7];
      param_3[-8] = *param_1;
      param_3[-7] = uVar1;
      param_3[-6] = uVar2;
      param_3[-5] = uVar3;
      uVar1 = *puVar21;
      uVar2 = puVar21[1];
      uVar3 = puVar21[2];
      uVar8 = puVar21[3];
      param_3[-4] = uVar4;
      param_3[-3] = uVar5;
      param_3[-2] = uVar6;
      param_3[-1] = uVar7;
      uVar4 = puVar21[4];
      uVar5 = puVar21[5];
      uVar6 = puVar21[6];
      uVar7 = puVar21[7];
      param_3[-0x10] = uVar1;
      param_3[-0xf] = uVar2;
      param_3[-0xe] = uVar3;
      param_3[-0xd] = uVar8;
      param_3[-0xc] = uVar4;
      param_3[-0xb] = uVar5;
      param_3[-10] = uVar6;
      param_3[-9] = uVar7;
    }
  }
  else {
    local_48 = param_2 >> 1;
    lVar23 = param_2 - local_48;
    puVar21 = param_1 + local_48 * -8;
    if (param_4 == '\0') {
      sequential_sort(puVar21,lVar23,param_3,0);
      puVar19 = param_1;
      puVar24 = puVar21;
      if (param_2 < 4) {
        uVar1 = param_1[-7];
        uVar2 = param_1[-6];
        uVar3 = param_1[-5];
        uVar4 = param_1[-4];
        uVar5 = param_1[-3];
        uVar6 = param_1[-2];
        uVar7 = param_1[-1];
        param_3[-8] = param_1[-8];
        param_3[-7] = uVar1;
        param_3[-6] = uVar2;
        param_3[-5] = uVar3;
        param_3[-4] = uVar4;
        param_3[-3] = uVar5;
        param_3[-2] = uVar6;
        param_3[-1] = uVar7;
        param_1 = param_3;
      }
      else {
        sequential_sort(param_1,local_48,param_3,1);
        param_1 = param_3;
      }
    }
    else {
      puVar24 = param_3 + local_48 * -8;
      sequential_sort(puVar21,lVar23,puVar24,1);
      puVar19 = param_3;
      if (3 < param_2) {
        sequential_sort(param_1,local_48,param_3,0);
      }
    }
    puVar21 = puVar24 + -8;
    puVar17 = param_1 + -8;
    while( true ) {
      while( true ) {
        puVar22 = puVar17;
        puVar20 = puVar21;
        puVar21 = puVar19 + -8;
        iVar18 = compare(puVar22,puVar20);
        if (iVar18 < 1) break;
        uVar1 = puVar24[-7];
        uVar2 = puVar24[-6];
        uVar3 = puVar24[-5];
        *puVar21 = puVar24[-8];
        puVar19[-7] = uVar1;
        puVar19[-6] = uVar2;
        puVar19[-5] = uVar3;
        uVar1 = puVar24[-3];
        uVar2 = puVar24[-2];
        uVar3 = puVar24[-1];
        puVar19[-4] = puVar24[-4];
        puVar19[-3] = uVar1;
        puVar19[-2] = uVar2;
        puVar19[-1] = uVar3;
        lVar23 = lVar23 + -1;
        if (lVar23 == 0) {
          memmove(puVar21 + (1 - local_48) * 8 + -8,param_1 + (1 - local_48) * 8 + -8,local_48 << 5)
          ;
          return;
        }
        puVar19 = puVar21;
        puVar21 = puVar20 + -8;
        puVar17 = puVar22;
        puVar24 = puVar20;
      }
      local_48 = local_48 - 1;
      uVar1 = param_1[-7];
      uVar2 = param_1[-6];
      uVar3 = param_1[-5];
      uVar4 = param_1[-4];
      uVar5 = param_1[-3];
      uVar6 = param_1[-2];
      uVar7 = param_1[-1];
      *puVar21 = param_1[-8];
      puVar19[-7] = uVar1;
      puVar19[-6] = uVar2;
      puVar19[-5] = uVar3;
      puVar19[-4] = uVar4;
      puVar19[-3] = uVar5;
      puVar19[-2] = uVar6;
      puVar19[-1] = uVar7;
      if (local_48 == 0) break;
      puVar19 = puVar21;
      puVar21 = puVar20;
      puVar17 = puVar22 + -8;
      param_1 = puVar22;
    }
  }
  return;
}