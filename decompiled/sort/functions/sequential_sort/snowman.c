void** sequential_sort(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r14_5;
    int32_t r12d6;
    void** rbx7;
    void** rax8;
    void** rax9;
    void** v10;
    void** r13_11;
    uint64_t rax12;
    void** r9_13;
    void** r15_14;
    void** rax15;
    void** rax16;
    void** r12_17;
    void** rbp18;

    r14_5 = rdi;
    r12d6 = ecx;
    rbx7 = rdx;
    if (rsi == 2) {
        rax8 = compare(rdi + 0xffffffffffffffe0, rdi + 0xffffffffffffffc0, rdx);
        if (!*reinterpret_cast<signed char*>(&r12d6)) {
            if (!(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) == 0))) {
                __asm__("movdqu xmm1, [r14-0x20]");
                __asm__("movdqu xmm0, [r14-0x10]");
                __asm__("movdqu xmm6, [r14-0x40]");
                __asm__("movdqu xmm7, [r14-0x30]");
                __asm__("movups [rbx-0x20], xmm1");
                __asm__("movups [rbx-0x10], xmm0");
                __asm__("movups [r14-0x20], xmm6");
                __asm__("movups [r14-0x10], xmm7");
                __asm__("movups [r14-0x40], xmm1");
                __asm__("movups [r14-0x30], xmm0");
            }
        } else {
            rax8 = reinterpret_cast<void**>((static_cast<int64_t>(reinterpret_cast<int32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) == 0)))) - 2)) << 5) + reinterpret_cast<unsigned char>(r14_5));
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movdqu xmm1, [rdx+0x10]");
            __asm__("movups [rbx-0x20], xmm0");
            __asm__("movdqu xmm0, [rax]");
            __asm__("movups [rbx-0x10], xmm1");
            __asm__("movdqu xmm1, [rax+0x10]");
            __asm__("movups [rbx-0x40], xmm0");
            __asm__("movups [rbx-0x30], xmm1");
        }
    } else {
        rax9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) >> 1);
        v10 = rax9;
        r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) - reinterpret_cast<unsigned char>(rax9));
        rax12 = reinterpret_cast<unsigned char>(rax9) << 5;
        r9_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) - rax12);
        if (*reinterpret_cast<signed char*>(&ecx)) {
            r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(-rax12) + reinterpret_cast<unsigned char>(rdx));
            rdx = r15_14;
            sequential_sort(r9_13, r13_11, rdx, 1);
            if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(3)) {
                rdx = rbx7;
                sequential_sort(r14_5, v10, rdx, 0);
            }
        } else {
            sequential_sort(r9_13, r13_11, rdx, 0);
            if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(3)) {
                rdx = rbx7;
                sequential_sort(r14_5, v10, rdx, 1);
                rax15 = rbx7;
                r15_14 = r9_13;
                r14_5 = rax15;
            } else {
                __asm__("movdqu xmm6, [r14-0x20]");
                __asm__("movdqu xmm7, [r14-0x10]");
                rax16 = rbx7;
                r15_14 = r9_13;
                __asm__("movups [rbx-0x20], xmm6");
                __asm__("movups [rbx-0x10], xmm7");
                r14_5 = rax16;
            }
        }
        r12_17 = r14_5 + 0xffffffffffffffe0;
        rbp18 = r15_14 + 0xffffffffffffffe0;
        while (1) {
            rax8 = compare(r12_17, rbp18, rdx, r12_17, rbp18, rdx);
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) == 0)) {
                --v10;
                __asm__("movdqu xmm4, [r14-0x20]");
                __asm__("movdqu xmm5, [r14-0x10]");
                __asm__("movups [rbx], xmm4");
                __asm__("movups [rbx+0x10], xmm5");
                if (!v10) 
                    break;
                r12_17 = r12_17 - 32;
            } else {
                __asm__("movdqu xmm2, [r15-0x20]");
                __asm__("movups [rbx], xmm2");
                __asm__("movdqu xmm3, [r15-0x10]");
                __asm__("movups [rbx+0x10], xmm3");
                --r13_11;
                if (!r13_11) 
                    goto addr_a050_17;
                rbp18 = rbp18 - 32;
            }
        }
    }
    return rax8;
    addr_a050_17:
}