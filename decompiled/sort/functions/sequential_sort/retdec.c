void sequential_sort(int32_t * lines, uint64_t nlines, int32_t * temp, bool to_temp) {
    int64_t v1 = (int64_t)temp;
    int64_t v2 = (int64_t)lines;
    if (nlines == 2) {
        int64_t v3 = v2 - 64; // 0x9fe0
        int64_t v4 = v2 - 32; // 0x9fe4
        int32_t v5 = compare(v4, v3);
        if (to_temp) {
            int64_t v6 = (v5 >= 0 == (v5 != 0) ? -32 : -64) + v2; // 0xa012
            int64_t v7 = (32 * (int64_t)(v5 >= 0 == (v5 != 0)) ^ -32) + v2; // 0xa015
            int128_t v8 = __asm_movdqu(*(int128_t *)v7); // 0xa018
            int128_t v9 = __asm_movdqu(*(int128_t *)(v7 + 16)); // 0xa01c
            __asm_movups(*(int128_t *)(v1 - 32), v8);
            int128_t v10 = __asm_movdqu(*(int128_t *)v6); // 0xa025
            __asm_movups(*(int128_t *)(v1 - 16), v9);
            int128_t v11 = __asm_movdqu(*(int128_t *)(v6 + 16)); // 0xa02d
            __asm_movups(*(int128_t *)(v1 - 64), v10);
            __asm_movups(*(int128_t *)(v1 - 48), v11);
            // 0xa03a
            return;
        }
        if (v5 >= 1) {
            int128_t * v12 = (int128_t *)v4; // 0xa089
            int128_t v13 = __asm_movdqu(*v12); // 0xa089
            int128_t * v14 = (int128_t *)(v2 - 16); // 0xa08f
            int128_t v15 = __asm_movdqu(*v14); // 0xa08f
            int128_t * v16 = (int128_t *)v3; // 0xa095
            int128_t v17 = __asm_movdqu(*v16); // 0xa095
            int128_t * v18 = (int128_t *)(v2 - 48); // 0xa09b
            int128_t v19 = __asm_movdqu(*v18); // 0xa09b
            __asm_movups(*(int128_t *)(v1 - 32), v13);
            __asm_movups(*(int128_t *)(v1 - 16), v15);
            __asm_movups(*v12, v17);
            __asm_movups(*v14, v19);
            __asm_movups(*v16, v13);
            __asm_movups(*v18, v15);
        }
        // 0xa03a
        return;
    }
    int64_t v20 = nlines % 2; // 0x9ed4
    int64_t v21 = 0; // 0x9ed7
    int64_t v22 = v2 - v21; // 0x9ede
    int64_t v23; // 0x9ea0
    int64_t v24; // 0x9ea0
    int64_t v25; // 0x9ea0
    if (to_temp) {
        int64_t v26 = v1 - v21; // 0x9f78
        sequential_sort((int32_t *)v22, v20, (int32_t *)v26, true);
        v25 = v1;
        v23 = v2;
        v24 = v26;
        if (nlines >= 4) {
            // 0x9f94
            sequential_sort(lines, 0, temp, false);
            v25 = v1;
            v23 = v2;
            v24 = v26;
        }
    } else {
        // 0x9eec
        sequential_sort((int32_t *)v22, v20, temp, false);
        if (nlines < 4) {
            int128_t v27 = __asm_movdqu(*(int128_t *)(v2 - 32)); // 0x9f0d
            int128_t v28 = __asm_movdqu(*(int128_t *)(v2 - 16)); // 0x9f13
            __asm_movups(*(int128_t *)(v1 - 32), v27);
            __asm_movups(*(int128_t *)(v1 - 16), v28);
            v25 = v2;
            v23 = v1;
            v24 = v22;
        } else {
            // 0xa0c2
            sequential_sort(lines, 0, temp, true);
            v25 = v2;
            v23 = v1;
            v24 = v22;
        }
    }
    int64_t v29 = 0; // 0x9f35
    int64_t v30 = v24 - 32; // 0x9f35
    int64_t v31 = v20; // 0x9f35
    int64_t v32 = v24; // 0x9f35
    int64_t v33 = v23 - 32;
    int64_t v34 = v25 - 32; // 0x9f3e
    int64_t v35 = compare(v33, v30); // 0x9f42
    int64_t v36 = v34; // 0x9f49
    int64_t v37 = v32; // 0x9f49
    int64_t v38 = v31; // 0x9f49
    int64_t v39 = v30; // 0x9f49
    int64_t v40 = v25; // 0x9f49
    int64_t v41 = v34; // 0x9f49
    int64_t v42; // 0x9ea0
    int64_t v43; // 0x9ea0
    int64_t v44; // 0x9ea0
    int128_t v45; // 0x9f54
    int64_t v46; // 0x9f5e
    int64_t v47; // 0x9f6b
    int64_t v48; // 0x9f3e
    int64_t v49; // 0x9f42
    if ((int32_t)v35 >= 1) {
        __asm_movups(*(int128_t *)v36, __asm_movdqu(*(int128_t *)(v37 - 32)));
        v45 = __asm_movdqu(*(int128_t *)(v37 - 16));
        __asm_movups(*(int128_t *)(v25 - 16), v45);
        v46 = v38 - 1;
        if (v46 == 0) {
            // 0xa050
            function_3c70();
            return;
        }
        // 0x9f68
        v43 = v39;
        v47 = v43 - 32;
        v48 = v36 - 32;
        v49 = compare(v33, v47);
        v37 = v43;
        v38 = v46;
        v39 = v47;
        v44 = v36;
        v40 = v36;
        v30 = v47;
        v31 = v46;
        v32 = v43;
        v41 = v48;
        while ((int32_t)v49 >= 1) {
            // 0x9f4b
            v42 = v48;
            __asm_movups(*(int128_t *)v42, __asm_movdqu(*(int128_t *)(v37 - 32)));
            v45 = __asm_movdqu(*(int128_t *)(v37 - 16));
            __asm_movups(*(int128_t *)(v44 - 16), v45);
            v46 = v38 - 1;
            if (v46 == 0) {
                // 0xa050
                function_3c70();
                return;
            }
            // 0x9f68
            v43 = v39;
            v47 = v43 - 32;
            v48 = v42 - 32;
            v49 = compare(v33, v47);
            v37 = v43;
            v38 = v46;
            v39 = v47;
            v44 = v42;
            v40 = v42;
            v30 = v47;
            v31 = v46;
            v32 = v43;
            v41 = v48;
        }
    }
    int64_t v50 = v41;
    v29--;
    int128_t v51 = __asm_movdqu(*(int128_t *)v33); // 0x9fb5
    int128_t v52 = __asm_movdqu(*(int128_t *)(v23 - 16)); // 0x9fbb
    __asm_movups(*(int128_t *)v50, v51);
    __asm_movups(*(int128_t *)(v40 - 16), v52);
    while (v29 != 0) {
        int64_t v53 = v33;
        v33 = v53 - 32;
        v34 = v50 - 32;
        v35 = compare(v33, v30);
        v36 = v34;
        v37 = v32;
        v38 = v31;
        v39 = v30;
        v44 = v50;
        v40 = v50;
        v41 = v34;
        if ((int32_t)v35 >= 1) {
            __asm_movups(*(int128_t *)v36, __asm_movdqu(*(int128_t *)(v37 - 32)));
            v45 = __asm_movdqu(*(int128_t *)(v37 - 16));
            __asm_movups(*(int128_t *)(v44 - 16), v45);
            v46 = v38 - 1;
            if (v46 == 0) {
                // 0xa050
                function_3c70();
                return;
            }
            // 0x9f68
            v43 = v39;
            v47 = v43 - 32;
            v48 = v36 - 32;
            v49 = compare(v33, v47);
            v37 = v43;
            v38 = v46;
            v39 = v47;
            v44 = v36;
            v40 = v36;
            v30 = v47;
            v31 = v46;
            v32 = v43;
            v41 = v48;
            while ((int32_t)v49 >= 1) {
                // 0x9f4b
                v42 = v48;
                __asm_movups(*(int128_t *)v42, __asm_movdqu(*(int128_t *)(v37 - 32)));
                v45 = __asm_movdqu(*(int128_t *)(v37 - 16));
                __asm_movups(*(int128_t *)(v44 - 16), v45);
                v46 = v38 - 1;
                if (v46 == 0) {
                    // 0xa050
                    function_3c70();
                    return;
                }
                // 0x9f68
                v43 = v39;
                v47 = v43 - 32;
                v48 = v42 - 32;
                v49 = compare(v33, v47);
                v37 = v43;
                v38 = v46;
                v39 = v47;
                v44 = v42;
                v40 = v42;
                v30 = v47;
                v31 = v46;
                v32 = v43;
                v41 = v48;
            }
        }
        // 0x9fb0
        v50 = v41;
        v29--;
        v51 = __asm_movdqu(*(int128_t *)v33);
        v52 = __asm_movdqu(*(int128_t *)(v53 - 16));
        __asm_movups(*(int128_t *)v50, v51);
        __asm_movups(*(int128_t *)(v40 - 16), v52);
    }
}