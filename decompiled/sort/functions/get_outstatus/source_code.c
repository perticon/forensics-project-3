get_outstatus (void)
{
  static int outstat_errno;
  static struct stat outstat;
  if (outstat_errno == 0)
    outstat_errno = fstat (STDOUT_FILENO, &outstat) == 0 ? -1 : errno;
  return outstat_errno < 0 ? &outstat : NULL;
}