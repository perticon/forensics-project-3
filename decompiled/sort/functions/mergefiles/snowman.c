void** mergefiles(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rsp7;
    void** rax8;
    void** rax9;
    void*** rsp10;
    void** r9_11;
    void** v12;
    void** rdx13;
    void** rsi14;
    void** rdi15;
    void* rsp16;
    void* rax17;
    void** rbp18;
    void** rax19;
    void** r14_20;
    void** rbp21;
    void* rsp22;
    void** rsi23;
    void** v24;
    void** r12_25;
    void** v26;
    void** rax27;
    void** v28;
    void** v29;
    void** rbx30;
    void** v31;
    void** r13_32;
    void** rbp33;
    void** rsi34;
    uint32_t eax35;
    uint32_t r14d36;
    void** eax37;
    void** eax38;
    int32_t eax39;
    void** rax40;
    int32_t eax41;
    int64_t rax42;
    int64_t v43;
    void** rdi44;
    int32_t eax45;
    int64_t rax46;
    int64_t v47;
    int64_t rax48;
    int64_t v49;
    void** r14_50;
    void** rax51;
    void** v52;
    void** v53;
    void** r13_54;
    void** v55;
    void** rdx56;
    void** rax57;
    void* rsp58;
    void** rbx59;
    void** rax60;
    void* rsp61;
    void** rax62;
    void** rbx63;
    void** rbp64;
    void** rsi65;
    void** rdi66;
    void*** v67;
    int32_t esi68;
    void** rax69;
    void** v70;
    void** v71;
    void** r8_72;
    void** rbx73;
    struct s15* r13_74;
    void** rdx75;
    void** rax76;
    void** rdi77;
    void** rdx78;
    void** rax79;
    void** rsi80;
    void** v81;
    void** r8_82;
    void** rax83;
    void** rdx84;
    void** r10_85;
    void** rax86;
    void** rax87;
    void** rsi88;
    void** rdx89;
    void** r8_90;
    void** v91;
    void** rax92;
    void** v93;
    void** rdx94;
    void** rdi95;
    void** rsi96;
    void* rsp97;
    void* rax98;
    int64_t v99;
    uint64_t r15_100;
    void** r13_101;
    void* rsp102;
    void** r14_103;
    void** r12_104;
    void** rcx105;
    void** v106;
    void** v107;
    void** v108;
    void** rax109;
    void** v110;
    void** v111;
    void** r8_112;
    uint64_t rax113;
    void** r15_114;
    void* r9_115;
    int32_t eax116;
    void** rax117;
    void** rcx118;
    int64_t v119;
    void*** r14_120;
    void** rbp121;
    void** rdi122;
    void** rsi123;
    void** rdi124;
    void** rax125;
    void** rbx126;
    void** rax127;
    void** v128;
    int64_t rax129;
    int32_t ecx130;
    void** rdx131;
    void** rdi132;
    void** v133;
    void** r9_134;
    void** v135;
    void** rax136;
    void** r10_137;
    void** v138;
    void** r15_139;
    void** rbp140;
    void** rbx141;
    void** r12_142;
    void** rax143;
    void** rdi144;
    void** rdi145;
    void** rdi146;
    void** rax147;
    void** r15_148;
    void** r11_149;
    uint32_t r8d150;
    void** r9_151;
    void** rdx152;
    void** v153;
    void** r15_154;
    void** rbx155;
    void** r12_156;
    void** rax157;
    void** r11_158;
    void** r12_159;
    void** v160;
    void** r15_161;
    void** rbp162;
    void** r12_163;
    void** rdi164;
    int1_t cf165;
    void** rdi166;
    void** r12_167;
    void** rdi168;
    void** v169;
    void** r15_170;
    void** rbp171;
    void** r12_172;
    void** rdi173;
    int1_t cf174;
    void** r9_175;
    void** rax176;
    void** rax177;
    void** rdi178;
    void* rax179;
    int64_t v180;
    void** rbp181;
    void** rax182;

    r15_6 = rcx;
    rsp7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    rax8 = g28;
    rax9 = open_input_files(rdi, rdx, rsp7);
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8);
    if (reinterpret_cast<unsigned char>(rdx) <= reinterpret_cast<unsigned char>(rax9) || reinterpret_cast<unsigned char>(rax9) > reinterpret_cast<unsigned char>(1)) {
        r9_11 = v12;
        r8 = r8;
        rcx = r15_6;
        rdx13 = rax9;
        rsi14 = rsi;
        rdi15 = rdi;
        mergefps(rdi15, rsi14, rdx13, rcx, r8, r9_11);
        rsp16 = reinterpret_cast<void*>(rsp10 - 8 + 8);
        rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
        if (!rax17) {
            return rax9;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx13) = 5;
        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
        rbp18 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax9) << 4));
        rax19 = fun_3920();
        rdi15 = rax19;
        rsi14 = rbp18;
        sort_die(rdi15, rsi14, 5);
        rsp16 = reinterpret_cast<void*>(rsp10 - 8 + 8 - 8 + 8);
    }
    fun_3950();
    r14_20 = rdi15;
    rbp21 = rsi14;
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xe8);
    *reinterpret_cast<uint32_t*>(&rsi23) = nmerge;
    *reinterpret_cast<int32_t*>(&rsi23 + 4) = 0;
    v24 = rdx13;
    r12_25 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22) + 56);
    v26 = rcx;
    rax27 = g28;
    v28 = rax27;
    if (reinterpret_cast<unsigned char>(rsi23) < reinterpret_cast<unsigned char>(rdx13)) 
        goto addr_ab58_7;
    while (1) {
        addr_ac45_8:
        if (reinterpret_cast<unsigned char>(rbp21) < reinterpret_cast<unsigned char>(v24)) {
            v29 = rbp21;
            rbx30 = rbp21;
            r9_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp21) << 4);
            v31 = r14_20;
            r13_32 = v26;
            *reinterpret_cast<int32_t*>(&r12_25) = 0;
            *reinterpret_cast<int32_t*>(&r12_25 + 4) = 0;
            rbp33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<unsigned char>(r9_11));
            r15_6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22) + 64);
            do {
                addr_aca9_10:
                rsi34 = *reinterpret_cast<void***>(rbp33);
                eax35 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi34) - 45);
                r14d36 = eax35;
                if (!eax35) {
                    r14d36 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi34 + 1));
                }
                if (!r13_32) 
                    goto addr_acd3_13;
                eax37 = fun_3ad0(r13_32, rsi34, rdx13);
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                if (eax37) 
                    goto addr_acd3_13;
                if (r14d36) 
                    goto addr_ad16_16;
                addr_acd3_13:
                eax38 = outstat_errno_4;
                if (!eax38) {
                    eax39 = fun_3e50();
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                    if (eax39) {
                        rax40 = fun_37d0();
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                        eax38 = *reinterpret_cast<void***>(rax40);
                        outstat_errno_4 = eax38;
                        goto addr_ace1_19;
                    } else {
                        outstat_errno_4 = reinterpret_cast<void**>(0xffffffff);
                    }
                } else {
                    addr_ace1_19:
                    if (reinterpret_cast<signed char>(eax38) >= reinterpret_cast<signed char>(0)) 
                        break;
                }
                if (!r14d36) {
                    eax41 = fun_3e50();
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                    if (eax41) 
                        goto addr_ac96_23;
                    rax42 = g1d248;
                    if (v43 != rax42) 
                        goto addr_ac96_23;
                } else {
                    rdi44 = *reinterpret_cast<void***>(rbp33);
                    eax45 = fun_3b10(rdi44, r15_6, rdx13);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                    if (eax45) 
                        goto addr_ac96_23;
                    rax46 = g1d248;
                    if (v47 != rax46) 
                        goto addr_ac96_23;
                }
                rax48 = outstat_3;
                if (v49 != rax48) {
                    addr_ac96_23:
                    ++rbx30;
                    rbp33 = rbp33 + 16;
                    if (rbx30 == v24) 
                        break; else 
                        goto addr_aca9_10;
                } else {
                    addr_ad16_16:
                    r14_50 = r12_25 + 13;
                    if (!r12_25) {
                        rax51 = maybe_create_temp(reinterpret_cast<uint64_t>(rsp22) + 56, 0, rdx13);
                        rcx = v52;
                        r14_50 = rax51 + 13;
                        *reinterpret_cast<int32_t*>(&rdx13) = 1;
                        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                        r12_25 = rax51;
                        r8 = r14_50;
                        mergefiles(rbp33, 0, 1, rcx, r8);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 + 8);
                    }
                }
                *reinterpret_cast<void***>(rbp33) = r14_50;
                ++rbx30;
                rbp33 = rbp33 + 16;
                *reinterpret_cast<void***>(rbp33 + 0xfffffffffffffff8) = r12_25;
            } while (rbx30 != v24);
            r14_20 = v31;
            rbp21 = v29;
        }
        v53 = rbp21;
        r13_54 = v24;
        v55 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22) + 48);
        while (1) {
            rdx56 = v55;
            rax57 = open_input_files(r14_20, r13_54, rdx56);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
            rbx59 = rax57;
            if (r13_54 == rax57) {
                rax60 = stream_open(v26, "w", rdx56);
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8);
                if (rax60) 
                    goto addr_afa3_34;
                rax62 = fun_37d0();
                rsp58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp61) - 8 + 8);
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax62) == 24)) 
                    break;
                if (reinterpret_cast<unsigned char>(r13_54) <= reinterpret_cast<unsigned char>(2)) 
                    break;
            } else {
                if (reinterpret_cast<unsigned char>(rax57) <= reinterpret_cast<unsigned char>(2)) 
                    goto addr_afe8_38;
            }
            r15_6 = rbx59 + 0xffffffffffffffff;
            rbx63 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp58) + 56);
            rbp64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_6) << 4));
            while (rsi65 = *reinterpret_cast<void***>(rbp64), r12_25 = rbp64, rdi66 = v67[reinterpret_cast<unsigned char>(r15_6) * 8], xfclose(rdi66, rsi65, rdx56), esi68 = 0, *reinterpret_cast<unsigned char*>(&esi68) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_6) > reinterpret_cast<unsigned char>(2)), rbp64 = rbp64 - 16, rax69 = maybe_create_temp(rbx63, esi68, rdx56), rsp58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 + 8), rax69 == 0) {
                --r15_6;
            }
            r9_11 = v70;
            rcx = v71;
            r8_72 = rax69 + 13;
            v24 = rax69;
            rbx73 = v53;
            if (reinterpret_cast<unsigned char>(v53) > reinterpret_cast<unsigned char>(r15_6)) {
                rbx73 = r15_6;
            }
            r13_74 = reinterpret_cast<struct s15*>(reinterpret_cast<unsigned char>(r13_54) - reinterpret_cast<unsigned char>(r15_6));
            rbp21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v53) - reinterpret_cast<unsigned char>(rbx73));
            mergefps(r14_20, rbx73, r15_6, rcx, r8_72, r9_11);
            r8 = r8_72;
            r13_54 = reinterpret_cast<void**>(&r13_74->f1);
            *reinterpret_cast<void***>(r14_20) = r8;
            *reinterpret_cast<void***>(r14_20 + 8) = v24;
            fun_3c70(r14_20 + 16, r12_25, reinterpret_cast<uint64_t>(r13_74) << 4);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 + 8);
            v53 = rbp21 + 1;
        }
        *reinterpret_cast<int32_t*>(&rdx75) = 5;
        *reinterpret_cast<int32_t*>(&rdx75 + 4) = 0;
        rax76 = fun_3920();
        rsi23 = v26;
        rdi77 = rax76;
        sort_die(rdi77, rsi23, 5);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 + 8);
        while (1) {
            rdx78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi77) + reinterpret_cast<unsigned char>(rdx75) + 1 - reinterpret_cast<unsigned char>(rsi23));
            rax79 = maybe_create_temp(r12_25, 0, rdx78);
            rsi80 = rbp21;
            rcx = v81;
            r8_82 = rax79 + 13;
            if (reinterpret_cast<unsigned char>(rdx78) <= reinterpret_cast<unsigned char>(rbp21)) {
                rsi80 = rdx78;
            }
            rax83 = mergefiles(r15_6, rsi80, rdx78, rcx, r8_82);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 + 8);
            rdx84 = rbp21;
            r8 = r8_82;
            if (reinterpret_cast<unsigned char>(rax83) <= reinterpret_cast<unsigned char>(rbp21)) {
                rdx84 = rax83;
            }
            ++r13_54;
            rbx59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx59) + reinterpret_cast<unsigned char>(rax83));
            *reinterpret_cast<void***>(r10_85) = r8;
            *reinterpret_cast<void***>(r10_85 + 8) = rax79;
            rbp21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp21) - reinterpret_cast<unsigned char>(rdx84));
            r15_6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx59) << 4) + reinterpret_cast<unsigned char>(r14_20));
            r10_85 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_54) << 4) + reinterpret_cast<unsigned char>(r14_20));
            do {
                rbp21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp21) + reinterpret_cast<unsigned char>(r13_54));
                fun_3c70(r10_85, r15_6, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(rbx59)) << 4);
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rsi23) = nmerge;
                *reinterpret_cast<int32_t*>(&rsi23 + 4) = 0;
                r9_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_54) - reinterpret_cast<unsigned char>(rbx59));
                v24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v24) + reinterpret_cast<unsigned char>(r9_11));
                rdx13 = v24;
                if (reinterpret_cast<unsigned char>(rsi23) >= reinterpret_cast<unsigned char>(rdx13)) 
                    goto addr_ac45_8;
                addr_ab58_7:
                *reinterpret_cast<int32_t*>(&r13_54) = 0;
                *reinterpret_cast<int32_t*>(&r13_54 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbx59) = 0;
                *reinterpret_cast<int32_t*>(&rbx59 + 4) = 0;
                if (reinterpret_cast<unsigned char>(v24) < reinterpret_cast<unsigned char>(rsi23)) {
                    rdi77 = v24;
                    rax86 = rsi23;
                    r10_85 = r14_20;
                    r15_6 = r14_20;
                    *reinterpret_cast<int32_t*>(&rdx75) = 0;
                    *reinterpret_cast<int32_t*>(&rdx75 + 4) = 0;
                } else {
                    do {
                        rax87 = maybe_create_temp(r12_25, 0, rdx13);
                        rsi88 = rbp21;
                        *reinterpret_cast<uint32_t*>(&rdx89) = nmerge;
                        *reinterpret_cast<int32_t*>(&rdx89 + 4) = 0;
                        r8_90 = rax87 + 13;
                        if (reinterpret_cast<unsigned char>(rdx89) <= reinterpret_cast<unsigned char>(rbp21)) {
                            rsi88 = rdx89;
                        }
                        rax92 = mergefiles(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx59) << 4) + reinterpret_cast<unsigned char>(r14_20), rsi88, rdx89, v91, r8_90);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 + 8);
                        rdx13 = rbp21;
                        r8 = r8_90;
                        *reinterpret_cast<uint32_t*>(&rsi23) = nmerge;
                        *reinterpret_cast<int32_t*>(&rsi23 + 4) = 0;
                        if (reinterpret_cast<unsigned char>(rax92) <= reinterpret_cast<unsigned char>(rbp21)) {
                            rdx13 = rax92;
                        }
                        rbx59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx59) + reinterpret_cast<unsigned char>(rax92));
                        rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_54) << 4);
                        ++r13_54;
                        rdi77 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(rbx59));
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<unsigned char>(rcx)) = r8;
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<unsigned char>(rcx) + 8) = rax87;
                        rbp21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp21) - reinterpret_cast<unsigned char>(rdx13));
                    } while (reinterpret_cast<unsigned char>(rsi23) <= reinterpret_cast<unsigned char>(rdi77));
                    r10_85 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<unsigned char>(rcx) + 16);
                    rdx75 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_54) % reinterpret_cast<unsigned char>(rsi23));
                    r15_6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx59) << 4) + reinterpret_cast<unsigned char>(r14_20));
                    rax86 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi23) - reinterpret_cast<unsigned char>(rdx75));
                }
            } while (reinterpret_cast<unsigned char>(rdi77) <= reinterpret_cast<unsigned char>(rax86));
        }
    }
    addr_afa3_34:
    r9_11 = v93;
    rcx = rax60;
    rdx94 = r13_54;
    r8 = v26;
    rdi95 = r14_20;
    rsi96 = v53;
    mergefps(rdi95, rsi96, rdx94, rcx, r8, r9_11);
    rsp97 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp61) - 8 + 8);
    rax98 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v28) - reinterpret_cast<unsigned char>(g28));
    if (!rax98) {
        goto v99;
    }
    addr_b011_62:
    fun_3950();
    r15_100 = reinterpret_cast<unsigned char>(rsi96) >> 1;
    r13_101 = r8;
    rsp102 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp97) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    r14_103 = *reinterpret_cast<void***>(rcx + 40);
    r12_104 = *reinterpret_cast<void***>(rcx + 48);
    rcx105 = v53;
    v106 = rdx94;
    v107 = r9_11;
    v108 = rcx105;
    rax109 = g28;
    v110 = rax109;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_103) + reinterpret_cast<unsigned char>(r12_104)) <= 0x1ffff || (v111 = rsi96, reinterpret_cast<unsigned char>(rsi96) <= reinterpret_cast<unsigned char>(1))) {
        addr_b0b7_64:
        r8_112 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi95) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v106) << 5));
        rax113 = reinterpret_cast<unsigned char>(r14_103) << 5;
        r15_114 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi95) - rax113);
        r9_115 = reinterpret_cast<void*>(-rax113);
        if (reinterpret_cast<unsigned char>(r12_104) > reinterpret_cast<unsigned char>(1)) {
            *reinterpret_cast<int32_t*>(&rcx105) = 0;
            *reinterpret_cast<int32_t*>(&rcx105 + 4) = 0;
            rsi96 = r12_104;
            rdx94 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_112) - (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_103) >> 1) << 5));
            sequential_sort(r15_114, rsi96, rdx94, 0);
            r9_115 = r9_115;
            r8_112 = r8_112;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rsi96) = 0;
        *reinterpret_cast<int32_t*>(&rsi96 + 4) = 0;
        rcx105 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp102) + 80);
        rdx94 = reinterpret_cast<void**>(0xb880);
        eax116 = fun_3c60(reinterpret_cast<uint64_t>(rsp102) + 72);
        if (!eax116) {
            rax117 = *reinterpret_cast<void***>(rcx + 40);
            rcx118 = *reinterpret_cast<void***>(rcx + 72);
            sortlines(reinterpret_cast<unsigned char>(rdi95) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax117) << 5), reinterpret_cast<unsigned char>(v111) - r15_100, v106, rcx118, r13_101, v107, v107);
            fun_3e00(v119);
            goto addr_b722_68;
        } else {
            r14_103 = *reinterpret_cast<void***>(rcx + 40);
            r12_104 = *reinterpret_cast<void***>(rcx + 48);
            goto addr_b0b7_64;
        }
    }
    if (reinterpret_cast<unsigned char>(r14_103) > reinterpret_cast<unsigned char>(1)) {
        *reinterpret_cast<int32_t*>(&rcx105) = 0;
        *reinterpret_cast<int32_t*>(&rcx105 + 4) = 0;
        rdx94 = r8_112;
        rsi96 = r14_103;
        sequential_sort(rdi95, rsi96, rdx94, 0);
        r9_115 = r9_115;
    }
    *reinterpret_cast<void***>(rcx) = rdi95;
    r14_120 = reinterpret_cast<void***>(r13_101 + 48);
    *reinterpret_cast<void***>(rcx + 8) = r15_114;
    *reinterpret_cast<void***>(rcx + 16) = r15_114;
    *reinterpret_cast<void***>(rcx + 24) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi95) + (reinterpret_cast<int64_t>(r9_115) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_104) << 5)));
    rbp121 = r13_101 + 8;
    fun_3e80(rbp121, rsi96, rdx94, rcx105);
    rdi122 = *reinterpret_cast<void***>(r13_101);
    rsi123 = rcx;
    heap_insert(rdi122, rsi123, rdx94, rcx105);
    *reinterpret_cast<unsigned char*>(rcx + 84) = 1;
    fun_3a90(r14_120, rsi123, rdx94, rcx105);
    fun_3b80(rbp121, rsi123, rdx94, rcx105);
    while (1) {
        fun_3e80(rbp121, rsi123, rdx94, rcx105);
        while (rdi124 = *reinterpret_cast<void***>(r13_101), rax125 = heap_remove_top(rdi124, rsi123, rdx94, rcx105), rax125 == 0) {
            rsi123 = rbp121;
            fun_38e0(r14_120, rsi123, rdx94, rcx105);
        }
        rbx126 = rax125;
        fun_3b80(rbp121, rsi123, rdx94, rcx105);
        rax127 = rbx126 + 88;
        v128 = rax127;
        fun_3e80(rax127, rsi123, rdx94, rcx105);
        *reinterpret_cast<uint32_t*>(&rax129) = *reinterpret_cast<uint32_t*>(rbx126 + 80);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax129) + 4) = 0;
        *reinterpret_cast<unsigned char*>(rbx126 + 84) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax129)) 
            break;
        ecx130 = static_cast<int32_t>(rax129 + rax129 + 2);
        rdx131 = *reinterpret_cast<void***>(rbx126);
        rdi132 = *reinterpret_cast<void***>(rbx126 + 8);
        v133 = rdx131;
        r9_134 = rdx131;
        v135 = rdi132;
        rsi123 = *reinterpret_cast<void***>(rbx126 + 16);
        rcx105 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(v106) >> *reinterpret_cast<signed char*>(&ecx130)) + 1);
        if (*reinterpret_cast<uint32_t*>(&rax129) == 1) {
            rax136 = rdi132;
            if (rdx131 == rsi123) {
                *reinterpret_cast<int32_t*>(&r10_137) = 0;
                *reinterpret_cast<int32_t*>(&r10_137 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx94) = 0;
                *reinterpret_cast<int32_t*>(&rdx94 + 4) = 0;
            } else {
                v138 = rbp121;
                r15_139 = v107;
                rbp140 = rbx126;
                rbx141 = v108;
                while (*reinterpret_cast<void***>(rbp140 + 24) != rax136) {
                    r12_142 = rcx105 + 0xffffffffffffffff;
                    if (!rcx105) 
                        goto addr_b6d8_83;
                    rax143 = compare(r9_134 + 0xffffffffffffffe0, rax136 + 0xffffffffffffffe0, rdx131);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax143) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax143) == 0))) {
                        rdx131 = rbx141;
                        rsi123 = r15_139;
                        rdi144 = *reinterpret_cast<void***>(rbp140 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp140 + 8) = rdi144;
                        write_unique(rdi144, rsi123, rdx131);
                        r9_134 = *reinterpret_cast<void***>(rbp140);
                        rax136 = *reinterpret_cast<void***>(rbp140 + 8);
                        if (r9_134 == *reinterpret_cast<void***>(rbp140 + 16)) 
                            goto addr_b3f5_86;
                    } else {
                        rdx131 = rbx141;
                        rsi123 = r15_139;
                        rdi145 = *reinterpret_cast<void***>(rbp140) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp140) = rdi145;
                        write_unique(rdi145, rsi123, rdx131);
                        r9_134 = *reinterpret_cast<void***>(rbp140);
                        rax136 = *reinterpret_cast<void***>(rbp140 + 8);
                        if (r9_134 == *reinterpret_cast<void***>(rbp140 + 16)) 
                            goto addr_b3f5_86;
                    }
                    rcx105 = r12_142;
                }
                goto addr_b600_89;
            }
        } else {
            rdx94 = *reinterpret_cast<void***>(rbx126 + 32);
            rdi146 = v133;
            rax147 = v135;
            r15_148 = *reinterpret_cast<void***>(rdx94);
            r11_149 = rdi146;
            if (rdi146 == rsi123) {
                r8d150 = 0;
                *reinterpret_cast<int32_t*>(&r10_137) = 0;
                *reinterpret_cast<int32_t*>(&r10_137 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r9_151) = 0;
                *reinterpret_cast<int32_t*>(&r9_151 + 4) = 0;
                goto addr_b2e8_92;
            } else {
                rdx152 = r15_148;
                v153 = r9_134;
                r15_154 = rbx126;
                rbx155 = rdx152;
                while (*reinterpret_cast<void***>(r15_154 + 24) != rax147) {
                    r12_156 = rcx105 + 0xffffffffffffffff;
                    if (!rcx105) 
                        goto addr_b5c0_96;
                    rbx155 = rbx155 - 32;
                    rax157 = compare(r11_149 + 0xffffffffffffffe0, rax147 + 0xffffffffffffffe0, rdx152);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax157) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax157) == 0))) {
                        r11_149 = *reinterpret_cast<void***>(r15_154);
                        rsi123 = *reinterpret_cast<void***>(r15_154 + 16);
                        __asm__("movdqu xmm0, [rcx-0x20]");
                        rax147 = *reinterpret_cast<void***>(r15_154 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_154 + 8) = rax147;
                        __asm__("movups [rbx], xmm0");
                        __asm__("movdqu xmm1, [rcx-0x10]");
                        __asm__("movups [rbx+0x10], xmm1");
                        if (rsi123 == r11_149) 
                            goto addr_b2b8_99;
                    } else {
                        rsi123 = *reinterpret_cast<void***>(r15_154 + 16);
                        __asm__("movdqu xmm2, [rax-0x20]");
                        r11_149 = *reinterpret_cast<void***>(r15_154) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_154) = r11_149;
                        __asm__("movups [rbx], xmm2");
                        __asm__("movdqu xmm3, [rax-0x10]");
                        rax147 = *reinterpret_cast<void***>(r15_154 + 8);
                        __asm__("movups [rbx+0x10], xmm3");
                        if (rsi123 == r11_149) 
                            goto addr_b2b8_99;
                    }
                    rcx105 = r12_156;
                }
                goto addr_b440_102;
            }
        }
        addr_b418_103:
        r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
        rsi123 = r11_158;
        if (*reinterpret_cast<void***>(rbx126 + 48) != r10_137) {
            addr_b6a8_104:
            r11_158 = rsi123;
            if (rsi123 != rdx94 || (rax136 == *reinterpret_cast<void***>(rbx126 + 24) || !rcx105)) {
                rdi146 = r9_134;
                r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
                r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
                r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx126 + 8))) >> 5);
            } else {
                v160 = rbp121;
                r15_161 = v107;
                rbp162 = rcx105 + 0xffffffffffffffff;
                r12_163 = v108;
                do {
                    rdi164 = rax136 + 0xffffffffffffffe0;
                    rdx94 = r12_163;
                    rsi123 = r15_161;
                    *reinterpret_cast<void***>(rbx126 + 8) = rdi164;
                    write_unique(rdi164, rsi123, rdx94);
                    rax136 = *reinterpret_cast<void***>(rbx126 + 8);
                    if (rax136 == *reinterpret_cast<void***>(rbx126 + 24)) 
                        break;
                    cf165 = reinterpret_cast<unsigned char>(rbp162) < reinterpret_cast<unsigned char>(1);
                    --rbp162;
                } while (!cf165);
                rbp121 = v160;
                r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
                rdi146 = *reinterpret_cast<void***>(rbx126);
                r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
                r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
                r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax136)) >> 5);
            }
        } else {
            r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
            rdi146 = r9_134;
            r12_159 = r10_137;
        }
        addr_b301_111:
        *reinterpret_cast<void***>(rbx126 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_158) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v133) - reinterpret_cast<unsigned char>(rdi146)) >> 5));
        *reinterpret_cast<void***>(rbx126 + 48) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_159) - reinterpret_cast<unsigned char>(r10_137));
        if (!*reinterpret_cast<signed char*>(&r8d150)) {
            rsi123 = rbx126;
            queue_check_insert_part_0(r13_101, rsi123, rdx94);
        }
        if (*reinterpret_cast<uint32_t*>(rbx126 + 80) > 1) {
            rdi166 = *reinterpret_cast<void***>(rbx126 + 56) + 88;
            fun_3e80(rdi166, rsi123, rdx94, rcx105);
            rsi123 = *reinterpret_cast<void***>(rbx126 + 56);
            if (!*reinterpret_cast<unsigned char*>(rsi123 + 84)) {
                queue_check_insert_part_0(r13_101, rsi123, rdx94);
                rsi123 = *reinterpret_cast<void***>(rbx126 + 56);
            }
            fun_3b80(rsi123 + 88, rsi123, rdx94, rcx105);
        } else {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx126 + 48)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx126 + 40)))) {
                r12_167 = *reinterpret_cast<void***>(rbx126 + 56);
                fun_3e80(rbp121, rsi123, rdx94, rcx105);
                rdi168 = *reinterpret_cast<void***>(r13_101);
                rsi123 = r12_167;
                heap_insert(rdi168, rsi123, rdx94, rcx105);
                *reinterpret_cast<unsigned char*>(r12_167 + 84) = 1;
                fun_3a90(r14_120, rsi123, rdx94, rcx105);
                fun_3b80(rbp121, rsi123, rdx94, rcx105);
            }
        }
        fun_3b80(v128, rsi123, rdx94, rcx105);
        continue;
        addr_b600_89:
        rbx126 = rbp140;
        rbp121 = v138;
        addr_b608_120:
        r10_137 = *reinterpret_cast<void***>(rbx126 + 48);
        rdx94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax136)) >> 5);
        if (rdx94 != r10_137) {
            rsi123 = *reinterpret_cast<void***>(rbx126 + 40);
            rdx94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v133) - reinterpret_cast<unsigned char>(r9_134)) >> 5);
            goto addr_b6a8_104;
        } else {
            rdi146 = r9_134;
            if (*reinterpret_cast<void***>(rbx126 + 16) == r9_134 || !rcx105) {
                r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
                r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
                r12_159 = r10_137;
                goto addr_b301_111;
            } else {
                v169 = rbp121;
                r15_170 = v107;
                rbp171 = rcx105 + 0xffffffffffffffff;
                r12_172 = v108;
                do {
                    rdi173 = rdi146 - 32;
                    rdx94 = r12_172;
                    rsi123 = r15_170;
                    *reinterpret_cast<void***>(rbx126) = rdi173;
                    write_unique(rdi173, rsi123, rdx94);
                    rdi146 = *reinterpret_cast<void***>(rbx126);
                    if (rdi146 == *reinterpret_cast<void***>(rbx126 + 16)) 
                        break;
                    cf174 = reinterpret_cast<unsigned char>(rbp171) < reinterpret_cast<unsigned char>(1);
                    --rbp171;
                } while (!cf174);
                rbp121 = v169;
                r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
                r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx126 + 8))) >> 5);
                r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
                r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
                goto addr_b301_111;
            }
        }
        addr_b6d8_83:
        rbx126 = rbp140;
        rcx105 = reinterpret_cast<void**>(0xffffffffffffffff);
        rbp121 = v138;
        goto addr_b608_120;
        addr_b3f5_86:
        rbx126 = rbp140;
        rcx105 = r12_142;
        rbp121 = v138;
        rdx94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v133) - reinterpret_cast<unsigned char>(r9_134)) >> 5);
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax136)) >> 5);
        goto addr_b418_103;
        addr_b2e8_92:
        r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
        r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
        if (r10_137 == r12_159) 
            goto addr_b2fe_128;
        if (r9_151 != r11_158 || ((r9_175 = *reinterpret_cast<void***>(rbx126 + 24), r9_175 == rax147) || (rsi123 = rcx105 + 0xffffffffffffffff, rcx105 == 0))) {
            addr_b2fe_128:
            *reinterpret_cast<void***>(rdx94) = r15_148;
            goto addr_b301_111;
        } else {
            rax176 = rax147 - 32;
            do {
                __asm__("movdqu xmm6, [rax]");
                r15_148 = r15_148 - 32;
                *reinterpret_cast<void***>(rbx126 + 8) = rax176;
                rcx105 = rax176;
                __asm__("movups [r15], xmm6");
                __asm__("movdqu xmm7, [rax+0x10]");
                __asm__("movups [r15+0x10], xmm7");
                if (rax176 == r9_175) 
                    break;
                --rsi123;
                rax176 = rax176 - 32;
            } while (rsi123 != 0xffffffffffffffff);
            goto addr_b7f7_133;
        }
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax176)) >> 5);
        goto addr_b2fe_128;
        addr_b7f7_133:
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rcx105)) >> 5);
        goto addr_b2fe_128;
        addr_b440_102:
        rbx126 = r15_154;
        rdi146 = *reinterpret_cast<void***>(rbx126);
        r15_148 = rbx155;
        r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
        rdx94 = *reinterpret_cast<void***>(rbx126 + 32);
        r9_151 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v153) - reinterpret_cast<unsigned char>(r11_149)) >> 5);
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax147)) >> 5);
        addr_b46d_135:
        r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
        r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
        if (r12_159 == r10_137) {
            if (rsi123 == rdi146) 
                goto addr_b2fe_128;
            rax177 = rcx105 + 0xffffffffffffffff;
            if (!rcx105) 
                goto addr_b2fe_128;
            rcx105 = rdi146 + 0xffffffffffffffe0;
            do {
                __asm__("movdqu xmm4, [rcx]");
                r15_148 = r15_148 - 32;
                *reinterpret_cast<void***>(rbx126) = rcx105;
                rdi146 = rcx105;
                __asm__("movups [r15], xmm4");
                __asm__("movdqu xmm5, [rcx+0x10]");
                __asm__("movups [r15+0x10], xmm5");
                if (rcx105 == rsi123) 
                    break;
                --rax177;
                rcx105 = rcx105 - 32;
            } while (rax177 != 0xffffffffffffffff);
            goto addr_b2fe_128;
            goto addr_b2fe_128;
        }
        addr_b5c0_96:
        rbx126 = r15_154;
        rdi146 = *reinterpret_cast<void***>(rbx126);
        r15_148 = rbx155;
        r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
        rcx105 = reinterpret_cast<void**>(0xffffffffffffffff);
        rdx94 = *reinterpret_cast<void***>(rbx126 + 32);
        r9_151 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v153) - reinterpret_cast<unsigned char>(r11_149)) >> 5);
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax147)) >> 5);
        goto addr_b46d_135;
        addr_b2b8_99:
        rbx126 = r15_154;
        r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
        r15_148 = rbx155;
        rdx94 = *reinterpret_cast<void***>(rbx126 + 32);
        rdi146 = r11_149;
        rcx105 = r12_156;
        r9_151 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v153) - reinterpret_cast<unsigned char>(r11_149)) >> 5);
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax147)) >> 5);
        goto addr_b2e8_92;
    }
    fun_3b80(v128, rsi123, rdx94, rcx105);
    fun_3e80(rbp121, rsi123, rdx94, rcx105);
    rdi178 = *reinterpret_cast<void***>(r13_101);
    heap_insert(rdi178, rbx126, rdx94, rcx105);
    *reinterpret_cast<unsigned char*>(rbx126 + 84) = 1;
    fun_3a90(r14_120, rbx126, rdx94, rcx105);
    fun_3b80(rbp121, rbx126, rdx94, rcx105);
    addr_b722_68:
    rax179 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v110) - reinterpret_cast<unsigned char>(g28));
    if (rax179) {
        fun_3950();
    } else {
        goto v180;
    }
    addr_afe8_38:
    rbp181 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax57) << 4));
    *reinterpret_cast<int32_t*>(&rdx94) = 5;
    *reinterpret_cast<int32_t*>(&rdx94 + 4) = 0;
    rax182 = fun_3920();
    rdi95 = rax182;
    rsi96 = rbp181;
    sort_die(rdi95, rsi96, 5);
    rsp97 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 + 8);
    goto addr_b011_62;
}