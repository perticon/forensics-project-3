int64_t mergefiles(int32_t * files, int64_t ntemps, uint64_t nfiles, struct _IO_FILE * ofp, char * output_file) {
    int64_t v1 = __readfsqword(40); // 0xaa80
    struct _IO_FILE ** v2; // bp-72, 0xaa60
    uint64_t result = open_input_files(files, nfiles, &v2); // 0xaa93
    if (result < nfiles == result < 2) {
        int64_t v3 = *(int64_t *)(16 * result + (int64_t)files); // 0xaaf2
        sort_die((char *)function_3920(), (char *)v3);
        // 0xab07
        return function_3950();
    }
    // 0xaaa6
    mergefps(files, ntemps, result, ofp, output_file, v2);
    if (v1 != __readfsqword(40)) {
        // 0xab07
        return function_3950();
    }
    // 0xaace
    return result;
}