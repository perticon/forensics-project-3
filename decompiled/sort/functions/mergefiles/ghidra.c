ulong mergefiles(long param_1,undefined8 param_2,ulong param_3,undefined8 param_4,undefined8 param_5
                )

{
  undefined8 uVar1;
  ulong uVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = open_input_files(param_1,param_3,&local_48);
  if ((uVar2 < param_3) && (uVar2 < 2)) {
    uVar1 = *(undefined8 *)(param_1 + uVar2 * 0x10);
    uVar3 = dcgettext(0,"open failed",5);
                    /* WARNING: Subroutine does not return */
    sort_die(uVar3,uVar1);
  }
  mergefps(param_1,param_2,uVar2,param_4,param_5,local_48);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar2;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}