char * set_ordering(char * s, int32_t * key, int32_t blanktype) {
    // 0x6ea0
    int64_t v1; // 0x6ea0
    uint64_t v2 = v1;
    if ((char)v2 == 0) {
        // 0x6efd
        return s;
    }
    int64_t v3 = v2 % 256 + 0xffffffb3; // 0x6ed0
    if ((char)v3 >= 38) {
        // 0x6efd
        return s;
    }
    int32_t v4 = *(int32_t *)((4 * v3 & 1020) + (int64_t)&g1); // 0x6eda
    return (char *)((int64_t)v4 + (int64_t)&g1);
}