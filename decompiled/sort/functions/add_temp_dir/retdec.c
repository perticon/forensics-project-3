void add_temp_dir(char * dir) {
    int64_t v1 = (int64_t)temp_dirs; // 0x73e9
    if (temp_dir_count == temp_dir_alloc) {
        // 0x7400
        v1 = x2nrealloc();
        *(int64_t *)&temp_dirs = v1;
    }
    // 0x73eb
    *(int64_t *)(v1 + 8 * temp_dir_count) = (int64_t)dir;
    temp_dir_count++;
}