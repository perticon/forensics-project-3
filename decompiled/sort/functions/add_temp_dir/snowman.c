void add_temp_dir(void** rdi, ...) {
    int64_t rdx2;
    int1_t zf3;
    void** rdi4;
    void** rax5;

    rdx2 = temp_dir_count;
    zf3 = rdx2 == temp_dir_alloc;
    rdi4 = temp_dirs;
    if (zf3) {
        rax5 = x2nrealloc();
        rdx2 = temp_dir_count;
        temp_dirs = rax5;
        rdi4 = rax5;
    }
    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi4) + reinterpret_cast<uint64_t>(rdx2 * 8)) = rdi;
    temp_dir_count = rdx2 + 1;
    return;
}