void add_temp_dir(undefined8 param_1)

{
  if (temp_dir_count == temp_dir_alloc) {
    temp_dirs = x2nrealloc(temp_dirs,&temp_dir_alloc,8);
  }
  *(undefined8 *)(temp_dirs + temp_dir_count * 8) = param_1;
  temp_dir_count = temp_dir_count + 1;
  return;
}