FILE * stream_open(char *param_1,char *param_2)

{
  int iVar1;
  int iVar2;
  FILE *pFVar3;
  int *piVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  
  if (*param_2 == 'r') {
    if ((*param_1 == '-') && (param_1[1] == '\0')) {
      have_read_stdin = 1;
      pFVar3 = stdin;
    }
    else {
      pFVar3 = (FILE *)0x0;
      iVar1 = open(param_1,0x80000);
      if (-1 < iVar1) {
        pFVar3 = fdopen(iVar1,param_2);
      }
    }
    fadvise(pFVar3,2);
    return pFVar3;
  }
  if (*param_2 != 'w') {
                    /* WARNING: Subroutine does not return */
    __assert_fail("!\"unexpected mode passed to stream_open\"","src/sort.c",0x3d5,"stream_open");
  }
  if (param_1 == (char *)0x0) {
    return stdout;
  }
  iVar1 = ftruncate(1,0);
  if (iVar1 == 0) {
    return stdout;
  }
  piVar4 = __errno_location();
  iVar1 = *piVar4;
  if (outstat_errno_4 == 0) {
    iVar2 = fstat(1,(stat *)outstat_3);
    if (iVar2 != 0) {
      outstat_errno_4 = *piVar4;
      goto LAB_00107245;
    }
    outstat_errno_4 = -1;
  }
  else {
LAB_00107245:
    if (-1 < outstat_errno_4) goto LAB_0010725f;
  }
  if ((outstat_3._24_4_ & 0xf000) != 0x8000) {
    return stdout;
  }
LAB_0010725f:
  uVar5 = quotearg_n_style_colon(0,3,param_1);
  uVar6 = dcgettext(0,"%s: error truncating",5);
                    /* WARNING: Subroutine does not return */
  error(2,iVar1,uVar6,uVar5);
}