void** stream_open(void** rdi, void** rsi, void** rdx, ...) {
    uint32_t eax4;
    void** r13_5;
    int32_t eax6;
    int64_t rdi7;
    void** rax8;
    int32_t eax9;
    void** rax10;
    void** r12_11;
    void** eax12;
    int32_t eax13;
    void** rax14;
    uint32_t eax15;
    void** r13_16;
    uint32_t eax17;
    int32_t eax18;
    void** rax19;
    int64_t rax20;
    int64_t rbp21;

    eax4 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi));
    if (*reinterpret_cast<signed char*>(&eax4) == 0x72) {
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45) || *reinterpret_cast<void***>(rdi + 1)) {
            *reinterpret_cast<int32_t*>(&r13_5) = 0;
            *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0;
            eax6 = fun_3cd0(rdi, rdi);
            *reinterpret_cast<int32_t*>(&rdi7) = eax6;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
            if (eax6 >= 0) {
                rax8 = fun_3c20(rdi7, rsi, rdx);
                r13_5 = rax8;
            }
        } else {
            have_read_stdin = 1;
            r13_5 = stdin;
        }
        fadvise(r13_5, 2);
        return r13_5;
    }
    if (*reinterpret_cast<signed char*>(&eax4) == 0x77) {
        if (rdi && (eax9 = fun_39e0(1), !!eax9)) {
            rax10 = fun_37d0();
            r12_11 = rax10;
            eax12 = outstat_errno_4;
            if (eax12) 
                goto addr_7245_10;
            while (1) {
                eax13 = fun_3e50();
                if (eax13) {
                    eax12 = *reinterpret_cast<void***>(r12_11);
                    outstat_errno_4 = eax12;
                    addr_7245_10:
                    if (reinterpret_cast<signed char>(eax12) < reinterpret_cast<signed char>(0)) 
                        goto addr_7249_13;
                } else {
                    outstat_errno_4 = reinterpret_cast<void**>(0xffffffff);
                    goto addr_7249_13;
                }
                addr_725f_15:
                rax14 = quotearg_n_style_colon();
                r12_11 = rax14;
                fun_3920();
                fun_3c90();
                continue;
                addr_7249_13:
                eax15 = g1d258;
                if ((eax15 & 0xf000) != 0x8000) 
                    break; else 
                    goto addr_725f_15;
            }
        }
        r13_16 = stdout;
        return r13_16;
    }
    fun_3a10("!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5, "stream_open");
    eax17 = fun_3b40("!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5, "!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5);
    if (eax17) 
        goto addr_7316_19;
    addr_7316_19:
    if (eax17 == 1) {
        eax18 = fun_3df0("!\"unexpected mode passed to stream_open\"");
        if (eax18) {
            while (1) {
                rax19 = fun_3920();
                sort_die(rax19, "src/sort.c", 5, rax19, "src/sort.c", 5);
                addr_736a_25:
            }
        }
    } else {
        rax20 = rpl_fclose("!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5, "!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5);
        if (*reinterpret_cast<int32_t*>(&rax20)) 
            goto addr_736a_25;
    }
    goto rbp21;
}