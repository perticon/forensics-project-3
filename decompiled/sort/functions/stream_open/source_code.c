stream_open (char const *file, char const *how)
{
  FILE *fp;

  if (*how == 'r')
    {
      if (STREQ (file, "-"))
        {
          have_read_stdin = true;
          fp = stdin;
        }
      else
        {
          int fd = open (file, O_RDONLY | O_CLOEXEC);
          fp = fd < 0 ? NULL : fdopen (fd, how);
        }
      fadvise (fp, FADVISE_SEQUENTIAL);
    }
  else if (*how == 'w')
    {
      if (file && ftruncate (STDOUT_FILENO, 0) != 0)
        {
          int ftruncate_errno = errno;
          struct stat *outst = get_outstatus ();
          if (!outst || S_ISREG (outst->st_mode) || S_TYPEISSHM (outst))
            die (SORT_FAILURE, ftruncate_errno, _("%s: error truncating"),
                 quotef (file));
        }
      fp = stdout;
    }
  else
    assert (!"unexpected mode passed to stream_open");

  return fp;
}