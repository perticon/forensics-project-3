void sortlines(int32_t * lines, uint64_t nthreads, uint64_t total_lines, int32_t * node, int32_t * queue, struct _IO_FILE * tfp, char * temp_output) {
    int64_t v1 = (int64_t)node;
    int64_t v2 = (int64_t)lines;
    int64_t * v3 = (int64_t *)(v1 + 40); // 0xb040
    int64_t v4 = *v3; // 0xb040
    int64_t * v5 = (int64_t *)(v1 + 48); // 0xb044
    int64_t v6 = *v5; // 0xb044
    int64_t v7 = __readfsqword(40); // 0xb05f
    int64_t v8 = v6; // 0xb0a6
    int64_t v9 = v4; // 0xb0a6
    if (nthreads < 2 || v6 + v4 < 0x20000) {
        goto lab_0xb0b7;
    } else {
        int64_t v10 = function_3c60(); // 0xb7dc
        v9 = *v3;
        if ((int32_t)v10 == 0) {
            int64_t v11 = *(int64_t *)(v1 + 72); // 0xb81e
            sortlines((int32_t *)(v2 - 32 * v9), nthreads % 2, total_lines, (int32_t *)v11, queue, tfp, temp_output);
            function_3e00();
            goto lab_0xb722;
        } else {
            // 0xb7ea
            v8 = *v5;
            goto lab_0xb0b7;
        }
    }
  lab_0xb608_2:;
    // 0xb608
    int64_t v12; // 0xb020
    int64_t v13 = v12;
    int64_t v14; // 0xb1dd
    int64_t v15 = v14;
    int64_t v16; // 0xb1e0
    int64_t v17 = v16;
    int64_t v18; // 0xb020
    int64_t * v19 = (int64_t *)(v18 + 48);
    int64_t v20 = *v19; // 0xb60d
    char * v21; // 0xb020
    int64_t * v22; // 0xb020
    int64_t v23; // 0xb020
    int64_t v24; // 0xb020
    int64_t v25; // 0xb020
    int64_t v26; // 0xb020
    int64_t v27; // 0xb020
    int64_t v28; // 0xb020
    int64_t v29; // 0xb020
    int64_t v30; // 0xb020
    int64_t v31; // 0xb020
    int64_t v32; // 0xb020
    char * v33; // 0xb020
    int64_t * v34; // 0xb1e0
    int64_t * v35; // 0xb1f7
    int64_t v36; // 0xb1f7
    if (v16 - v17 >> 5 != v20) {
        // 0xb698
        v23 = v17;
        v29 = v13;
        v31 = v14 - v15 >> 5;
        v32 = *(int64_t *)(v18 + 40);
        v28 = v15;
        goto lab_0xb6a8;
    } else {
        int64_t v37 = v13; // 0xb624
        int64_t v38 = v15; // 0xb624
        if (v36 == v15 || v13 == 0) {
            // 0xb865
            v22 = v19;
            v30 = v15;
            v27 = (int64_t)*v33;
            v24 = v20;
            v25 = *(int64_t *)(v18 + 40);
            v26 = v20;
        } else {
            int64_t v39 = v37 - 1;
            int64_t v40 = v38 - 32; // 0xb656
            *(int64_t *)v21 = v40;
            write_unique((int32_t *)v40, tfp, temp_output);
            int64_t v41 = *(int64_t *)v21; // 0xb668
            v37 = v39;
            while (!((v39 == 0 | v41 == *v35))) {
                // 0xb656
                v39 = v37 - 1;
                v40 = v41 - 32;
                *(int64_t *)v21 = v40;
                write_unique((int32_t *)v40, tfp, temp_output);
                v41 = *(int64_t *)v21;
                v37 = v39;
            }
            // 0xb671
            v22 = v19;
            v30 = v41;
            v27 = (int64_t)*v33;
            v24 = v16 - *v34 >> 5;
            v25 = *(int64_t *)(v18 + 40);
            v26 = *v19;
        }
        goto lab_0xb301;
    }
  lab_0xb46d_2:;
    // 0xb46d
    int64_t * v42; // 0xb208
    int64_t v43 = *v42; // 0xb020
    int64_t v44 = *(int64_t *)v21; // 0xb020
    char v45 = *v33; // 0xb020
    int64_t v46; // 0xb020
    int64_t v47 = v46; // 0xb020
    int64_t v48; // 0xb020
    int64_t v49 = v48; // 0xb020
    int64_t v50; // 0xb020
    int64_t v51 = v50; // 0xb020
    int64_t v52; // 0xb020
    int64_t v53 = v52; // 0xb020
    goto lab_0xb46d_3;
  lab_0xb418:;
    int64_t v166 = 0;
    int64_t v96; // 0xb020
    int64_t v167 = v96;
    int64_t v168 = *(int64_t *)(v18 + 40); // 0xb418
    int64_t * v169 = (int64_t *)(v18 + 48);
    int64_t v94; // 0xb020
    v23 = v94;
    int64_t v95; // 0xb020
    v29 = v95;
    v31 = 0;
    v32 = v168;
    v28 = v167;
    if (*v169 != v166) {
        goto lab_0xb6a8;
    } else {
        // 0xb429
        v22 = v169;
        v30 = v167;
        v27 = (int64_t)*v33;
        v24 = v166;
        v25 = v168;
        v26 = v166;
        goto lab_0xb301;
    }
  lab_0xb2e8:;
    int64_t v170 = *(int64_t *)(v18 + 48); // 0xb2e8
    int64_t v171 = *(int64_t *)(v18 + 40); // 0xb2ec
    int64_t v126; // 0xb020
    int64_t v172 = v126; // 0xb2f3
    int64_t v127; // 0xb020
    int64_t v173 = v127; // 0xb2f3
    int64_t v128; // 0xb020
    int64_t v174 = v128; // 0xb2f3
    int64_t v129; // 0xb020
    int64_t v175 = v129; // 0xb2f3
    int64_t v130; // 0xb020
    int64_t v176 = v130; // 0xb2f3
    int64_t v131; // 0xb020
    int64_t v177 = v131; // 0xb2f3
    int64_t v132; // 0xb020
    int64_t v178 = v132; // 0xb2f3
    int64_t v179 = v171; // 0xb2f3
    int64_t v180 = v170; // 0xb2f3
    int64_t v133; // 0xb020
    int64_t v181 = v133; // 0xb2f3
    int64_t v60 = v128; // 0xb2f3
    int64_t v61 = v129; // 0xb2f3
    int64_t v62 = v130; // 0xb2f3
    int64_t v63 = v132; // 0xb2f3
    int64_t v64 = v171; // 0xb2f3
    int64_t v65 = v132; // 0xb2f3
    int64_t v66 = v133; // 0xb2f3
    if (v132 == v170) {
        goto lab_0xb2fe;
    } else {
        goto lab_0xb2f5;
    }
  lab_0xb6a8:;
    int64_t v76 = v28;
    int64_t v78 = v32;
    int64_t v77 = v23; // 0xb6b9
    if (v78 == v31) {
        int64_t * v182 = (int64_t *)(v18 + 24); // 0xb74b
        int64_t v183 = v77; // 0xb74f
        int64_t v184 = v29; // 0xb74f
        if (v29 == 0 | v77 == *v182) {
            goto lab_0xb6b4;
        } else {
            int64_t v185 = v184 - 1;
            int64_t v186 = v183 - 32; // 0xb786
            *v34 = v186;
            write_unique((int32_t *)v186, tfp, temp_output);
            int64_t v187 = *v34; // 0xb799
            v184 = v185;
            while (!((v185 == 0 | v187 == *v182))) {
                // 0xb786
                v185 = v184 - 1;
                v186 = v187 - 32;
                *v34 = v186;
                write_unique((int32_t *)v186, tfp, temp_output);
                v187 = *v34;
                v184 = v185;
            }
            int64_t * v188 = (int64_t *)(v18 + 48);
            v22 = v188;
            v30 = *(int64_t *)v21;
            v27 = (int64_t)*v33;
            v24 = v16 - v187 >> 5;
            v25 = *(int64_t *)(v18 + 40);
            v26 = *v188;
            goto lab_0xb301;
        }
    } else {
        goto lab_0xb6b4;
    }
  lab_0xb2fe:
    // 0xb2fe
    *(int64_t *)v60 = v66;
    v22 = (int64_t *)(v18 + 48);
    v30 = v61;
    v27 = v62;
    v24 = v63;
    v25 = v64;
    v26 = v65;
    goto lab_0xb301;
  lab_0xb2f5:;
    int64_t v189 = v181;
    int64_t v190 = v180;
    int64_t v191 = v178;
    int64_t v192 = v177;
    int64_t v193 = v176;
    int64_t v194 = v175;
    int64_t v195 = v174;
    v60 = v195;
    v61 = v194;
    v62 = v193;
    v63 = v191;
    v64 = v179;
    v65 = v190;
    v66 = v189;
    if (v192 == v179) {
        int64_t v196 = *(int64_t *)(v18 + 24); // 0xb518
        v60 = v195;
        v61 = v194;
        v62 = v193;
        v63 = v191;
        v64 = v192;
        v65 = v190;
        v66 = v189;
        int64_t v197 = v172; // 0xb51f
        int64_t v198 = v173; // 0xb51f
        int64_t v199 = v189; // 0xb51f
        if (v173 != 0 && v196 != v172) {
            int64_t v200 = v198;
            int64_t v201 = v197 - 32;
            int128_t v202 = __asm_movdqu(*(int128_t *)v201); // 0xb552
            int64_t v203 = v199 - 32; // 0xb556
            *v34 = v201;
            __asm_movups(*(int128_t *)v203, v202);
            int128_t v204 = __asm_movdqu(*(int128_t *)(v197 - 16)); // 0xb565
            __asm_movups(*(int128_t *)(v199 - 16), v204);
            while (v201 != v196) {
                int64_t v205 = v200 - 1;
                v197 = v201;
                v198 = v205;
                v199 = v203;
                if (v205 == 0) {
                    // 0xb7f7
                    v60 = v195;
                    v61 = v194;
                    v62 = v193;
                    v63 = v16 - v201 >> 5;
                    v64 = v192;
                    v65 = v190;
                    v66 = v203;
                    goto lab_0xb2fe;
                }
                v200 = v198;
                v201 = v197 - 32;
                v202 = __asm_movdqu(*(int128_t *)v201);
                v203 = v199 - 32;
                *v34 = v201;
                __asm_movups(*(int128_t *)v203, v202);
                v204 = __asm_movdqu(*(int128_t *)(v197 - 16));
                __asm_movups(*(int128_t *)(v199 - 16), v204);
            }
            // 0xb574
            v60 = v195;
            v61 = v194;
            v62 = v193;
            v63 = v16 - v201 >> 5;
            v64 = v192;
            v65 = v190;
            v66 = v203;
        }
    }
    goto lab_0xb2fe;
  lab_0xb46d_3:;
    int64_t v54 = v43;
    int64_t v55 = v45;
    int64_t v56 = v16 - v47 >> 5;
    int64_t v57 = *(int64_t *)(v18 + 48); // 0xb46d
    int64_t v58 = *(int64_t *)(v18 + 40); // 0xb471
    if (v57 != v56) {
        goto lab_0xb2f5;
    } else {
        int64_t v59 = v51;
        v60 = v54;
        v61 = v44;
        v62 = v55;
        v63 = v56;
        v64 = v58;
        v65 = v57;
        v66 = v49;
        int64_t v67 = v53; // 0xb481
        int64_t v68 = v44; // 0xb481
        int64_t v69 = v49; // 0xb481
        if (v53 != 0 && v59 != v44) {
            int64_t v70 = v68 - 32;
            int64_t v71 = v67 - 1;
            int128_t v72 = __asm_movdqu(*(int128_t *)v70); // 0xb4b2
            int64_t v73 = v69 - 32; // 0xb4b6
            *(int64_t *)v21 = v70;
            __asm_movups(*(int128_t *)v73, v72);
            int128_t v74 = __asm_movdqu(*(int128_t *)(v68 - 16)); // 0xb4c4
            __asm_movups(*(int128_t *)(v69 - 16), v74);
            v60 = v54;
            v61 = v70;
            v62 = v55;
            v63 = v56;
            v64 = v58;
            v65 = v57;
            v66 = v73;
            v67 = v71;
            v68 = v70;
            v69 = v73;
            while (v71 != 0 && v70 != v59) {
                // 0xb4b2
                v70 = v68 - 32;
                v71 = v67 - 1;
                v72 = __asm_movdqu(*(int128_t *)v70);
                v73 = v69 - 32;
                *(int64_t *)v21 = v70;
                __asm_movups(*(int128_t *)v73, v72);
                v74 = __asm_movdqu(*(int128_t *)(v68 - 16));
                __asm_movups(*(int128_t *)(v69 - 16), v74);
                v60 = v54;
                v61 = v70;
                v62 = v55;
                v63 = v56;
                v64 = v58;
                v65 = v57;
                v66 = v73;
                v67 = v71;
                v68 = v70;
                v69 = v73;
            }
        }
        goto lab_0xb2fe;
    }
  lab_0xb6b4:;
    int64_t * v75 = (int64_t *)(v18 + 48);
    v22 = v75;
    v30 = v76;
    v27 = (int64_t)*v33;
    v24 = v16 - v77 >> 5;
    v25 = v78;
    v26 = *v75;
    goto lab_0xb301;
  lab_0xb301:;
    int64_t v79 = v27;
    int64_t * v80 = v22;
    int64_t * v81 = (int64_t *)(v18 + 40); // 0xb316
    *v81 = v25 - (v14 - v30 >> 5);
    *v80 = v26 - v24;
    int64_t v82; // 0xb020
    if ((char)v79 == 0) {
        // 0xb323
        queue_check_insert_part_0(v82, v18);
    }
    // 0xb32e
    int32_t * v83; // 0xb1c5
    if (*v83 < 2) {
        // 0xb338
        if (*v80 == -*v81) {
            int64_t v84 = *(int64_t *)(v18 + 56); // 0xb588
            function_3e80();
            heap_insert((int32_t *)v79, (char *)v84);
            *(char *)(v84 + 84) = 1;
            function_3a90();
            function_3b80();
        }
    } else {
        // 0xb4e0
        function_3e80();
        int64_t v85 = *(int64_t *)(v18 + 56); // 0xb4ed
        if (*(char *)(v85 + 84) == 0) {
            // 0xb4f7
            queue_check_insert_part_0(v82, v85);
        }
        // 0xb503
        function_3b80();
    }
    // 0xb346
    function_3b80();
    int64_t v86 = v79; // 0xb350
    goto lab_0xb180;
  lab_0xb0b7:;
    int64_t v162 = v2 - 32 * total_lines; // 0xb0c6
    int64_t v163 = 32 * v9; // 0xb0cc
    int64_t v164 = v2 - v163; // 0xb0d3
    if (v8 >= 2) {
        // 0xb0df
        sequential_sort((int32_t *)v164, v8, (int32_t *)(v162 - 32 * v9 / 2), false);
    }
    int32_t * v165 = (int32_t *)v162;
    if (v9 >= 2) {
        // 0xb116
        sequential_sort(lines, v9, v165, false);
    }
    // 0xb130
    v82 = (int64_t)queue;
    *(int64_t *)node = v2;
    *(int64_t *)(v1 + 8) = v164;
    *(int64_t *)(v1 + 16) = v164;
    *(int64_t *)(v1 + 24) = v2 - 32 * v8 - v163;
    function_3e80();
    heap_insert(v165, (char *)node);
    *(char *)(v1 + 84) = 1;
    function_3a90();
    function_3b80();
    v86 = v162;
    int32_t * v87; // 0xb020
    while (true) {
      lab_0xb180:
        // 0xb180
        function_3e80();
        v87 = (int32_t *)v86;
        char * v88 = heap_remove_top(v87); // 0xb19f
        char * v89 = v88; // 0xb1a7
        if (v88 == NULL) {
            function_38e0();
            char * v90 = heap_remove_top(v87); // 0xb19f
            v89 = v90;
            while (v90 == NULL) {
                // 0xb190
                function_38e0();
                v90 = heap_remove_top(v87);
                v89 = v90;
            }
        }
        // 0xb1a9
        v21 = v89;
        v18 = (int64_t)v21;
        function_3b80();
        function_3e80();
        v83 = (int32_t *)(v18 + 80);
        uint32_t v91 = *v83; // 0xb1c5
        v33 = (char *)(v18 + 84);
        *v33 = 0;
        if (v91 == 0) {
            // break -> 0xb6f0
            break;
        }
        // 0xb1d4
        v14 = *(int64_t *)v21;
        v34 = (int64_t *)(v18 + 8);
        v16 = *v34;
        int64_t v92 = total_lines >> (2 * (int64_t)v91 + 2 & 62);
        v35 = (int64_t *)(v18 + 16);
        v36 = *v35;
        int64_t v93 = v92 + 1; // 0xb1fb
        if (v91 == 1) {
            // 0xb358
            v94 = v16;
            v95 = v93;
            v96 = v14;
            if (v14 == v36) {
                goto lab_0xb418;
            } else {
                // 0xb364
                v12 = v93;
                if (*(int64_t *)(v18 + 24) == v16) {
                    goto lab_0xb608_2;
                } else {
                    // 0xb3b2
                    v12 = -1;
                    if (v93 == 0) {
                        goto lab_0xb608_2;
                    } else {
                        int64_t v97; // 0xb020
                        int64_t v98; // 0xb020
                        int64_t v99; // 0xb020
                        while (true) {
                            // 0xb3bf
                            int64_t v100; // 0xb020
                            int64_t v101 = v100;
                            int64_t v102; // 0xb020
                            int64_t v103 = v102;
                            int64_t v104; // 0xb020
                            v97 = v104;
                            int32_t v105 = compare(v101 - 32, v103 - 32); // 0xb3cc
                            int64_t v106; // 0xb020
                            int64_t v107; // 0xb020
                            if (v105 >= 0 == (v105 != 0)) {
                                int64_t v108 = *v34; // 0xb380
                                int64_t v109 = v108 - 32; // 0xb38a
                                *v34 = v109;
                                write_unique((int32_t *)v109, tfp, temp_output);
                                int64_t v110 = *(int64_t *)v21; // 0xb397
                                int64_t v111 = *v34; // 0xb39b
                                int64_t v112 = *v35; // 0xb39f
                                v107 = v111;
                                v106 = v110;
                                v99 = v111;
                                v98 = v110;
                                if (v110 == v112) {
                                    // break -> 0xb3f5
                                    break;
                                }
                            } else {
                                int64_t v113 = *(int64_t *)v21; // 0xb3d0
                                int64_t v114 = v113 - 32; // 0xb3da
                                *(int64_t *)v21 = v114;
                                write_unique((int32_t *)v114, tfp, temp_output);
                                int64_t v115 = *(int64_t *)v21; // 0xb3e7
                                int64_t v116 = *v34; // 0xb3eb
                                int64_t v117 = *v35; // 0xb3ef
                                v107 = v116;
                                v106 = v115;
                                v99 = v116;
                                v98 = v115;
                                if (v115 == v117) {
                                    // break -> 0xb3f5
                                    break;
                                }
                            }
                            int64_t v118 = v106;
                            int64_t v119 = v107;
                            int64_t * v120; // 0xb3a8
                            int64_t v121 = *v120; // 0xb3a8
                            v12 = v97;
                            if (v121 == v119) {
                                goto lab_0xb608_2;
                            }
                            // 0xb3b2
                            v104 = v97 - 1;
                            v102 = v119;
                            v100 = v118;
                            v12 = -1;
                            if (v97 == 0) {
                                goto lab_0xb608_2;
                            }
                        }
                        int64_t v122 = v98;
                        int64_t v123 = v99;
                        v94 = v123;
                        v95 = v97;
                        v96 = v122;
                        goto lab_0xb418;
                    }
                }
            }
        } else {
            // 0xb208
            v42 = (int64_t *)(v18 + 32);
            int64_t v124 = *v42; // 0xb208
            int64_t v125 = *(int64_t *)v124; // 0xb216
            v126 = v16;
            v127 = v93;
            v128 = v124;
            v129 = v14;
            v130 = 0;
            v131 = 0;
            v132 = 0;
            v133 = v125;
            if (v14 == v36) {
                goto lab_0xb2e8;
            } else {
                int64_t * v134 = (int64_t *)(v18 + 24); // 0xb264
                v43 = v124;
                v44 = v14;
                v45 = 0;
                v47 = v16;
                v49 = v125;
                v51 = v36;
                v53 = v93;
                if (*v134 == v16) {
                    goto lab_0xb46d_3;
                } else {
                    int64_t v135 = v92; // 0xb275
                    int64_t v136 = v16; // 0xb275
                    int64_t v137 = v125; // 0xb275
                    int64_t v138 = v14; // 0xb275
                    v46 = v16;
                    v48 = v125;
                    v50 = v36;
                    v52 = -1;
                    if (v93 == 0) {
                        goto lab_0xb46d_2;
                    } else {
                        int64_t v139; // 0xb020
                        int64_t v140; // 0xb020
                        int64_t v141; // 0xb020
                        int64_t v142; // 0xb283
                        while (true) {
                            int64_t v143 = v137;
                            v139 = v135;
                            v142 = v143 - 32;
                            int32_t v144 = compare(v138 - 32, v136 - 32); // 0xb28c
                            int64_t v145; // 0xb020
                            int64_t v146; // 0xb020
                            int64_t v147; // 0xb020
                            if (v144 >= 0 == (v144 != 0)) {
                                int64_t v148 = *v34; // 0xb238
                                int64_t v149 = *(int64_t *)v21; // 0xb23c
                                int64_t v150 = *v35; // 0xb23f
                                int64_t v151 = v148 - 32; // 0xb243
                                int128_t v152 = __asm_movdqu(*(int128_t *)v151); // 0xb243
                                *v34 = v151;
                                __asm_movups(*(int128_t *)v142, v152);
                                __asm_movups(*(int128_t *)(v143 - 16), __asm_movdqu(*(int128_t *)(v148 - 16)));
                                v146 = v151;
                                v147 = v150;
                                v145 = v149;
                                v141 = v151;
                                v140 = v149;
                                if (v150 == v149) {
                                    // break -> 0xb2b8
                                    break;
                                }
                            } else {
                                int64_t v153 = *(int64_t *)v21; // 0xb290
                                int64_t v154 = *v35; // 0xb293
                                int64_t v155 = v153 - 32; // 0xb297
                                int128_t v156 = __asm_movdqu(*(int128_t *)v155); // 0xb297
                                *(int64_t *)v21 = v155;
                                __asm_movups(*(int128_t *)v142, v156);
                                int128_t v157 = __asm_movdqu(*(int128_t *)(v153 - 16)); // 0xb2a6
                                int64_t v158 = *v34; // 0xb2ab
                                __asm_movups(*(int128_t *)(v143 - 16), v157);
                                v146 = v158;
                                v147 = v154;
                                v145 = v155;
                                v141 = v158;
                                v140 = v155;
                                if (v154 == v155) {
                                    // break -> 0xb2b8
                                    break;
                                }
                            }
                            int64_t v159 = v145;
                            int64_t v160 = v147;
                            int64_t v161 = v146;
                            v46 = v161;
                            v48 = v142;
                            v50 = v160;
                            v52 = v139;
                            if (*v134 == v161) {
                                goto lab_0xb46d_2;
                            }
                            // 0xb26e
                            v135 = v139 - 1;
                            v136 = v161;
                            v137 = v142;
                            v138 = v159;
                            v46 = v161;
                            v48 = v142;
                            v50 = v160;
                            v52 = -1;
                            if (v139 == 0) {
                                goto lab_0xb46d_2;
                            }
                        }
                        // 0xb2b8
                        v126 = v141;
                        v127 = v139;
                        v128 = *v42;
                        v129 = v140;
                        v130 = (int64_t)*v33;
                        v131 = v14 - v140 >> 5;
                        v132 = v16 - v141 >> 5;
                        v133 = v142;
                        goto lab_0xb2e8;
                    }
                }
            }
        }
    }
    // 0xb6f0
    function_3b80();
    function_3e80();
    heap_insert(v87, v21);
    *v33 = 1;
    function_3a90();
    function_3b80();
    goto lab_0xb722;
  lab_0xb722:
    // 0xb722
    if (v7 == __readfsqword(40)) {
        // 0xb739
        return;
    }
    // 0xb876
    function_3950();
}