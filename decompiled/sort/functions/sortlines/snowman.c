void sortlines(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7) {
    uint64_t r15_8;
    void** r13_9;
    void* rsp10;
    void** r14_11;
    void** r12_12;
    void** rcx13;
    void** v14;
    void** v15;
    void** v16;
    void** rax17;
    void** v18;
    void** v19;
    void** r8_20;
    uint64_t rax21;
    void** r15_22;
    void* r9_23;
    int32_t eax24;
    void** rax25;
    void** rcx26;
    int64_t v27;
    void*** r14_28;
    void** rbp29;
    void** rdi30;
    void** rsi31;
    void** rdi32;
    void** rax33;
    void** rbx34;
    void** rax35;
    void** v36;
    int64_t rax37;
    int32_t ecx38;
    void** rdx39;
    void** rdi40;
    void** v41;
    void** r9_42;
    void** v43;
    void** rax44;
    void** r10_45;
    void** v46;
    void** r15_47;
    void** rbp48;
    void** rbx49;
    void** r12_50;
    void** rax51;
    void** rdi52;
    void** rdi53;
    void** rdi54;
    void** rax55;
    void** r15_56;
    void** r11_57;
    uint32_t r8d58;
    void** r9_59;
    void** rdx60;
    void** v61;
    void** r15_62;
    void** rbx63;
    void** r12_64;
    void** rax65;
    void** r11_66;
    void** r12_67;
    void** v68;
    void** r15_69;
    void** rbp70;
    void** r12_71;
    void** rdi72;
    int1_t cf73;
    void** rdi74;
    void** r12_75;
    void** rdi76;
    void** v77;
    void** r15_78;
    void** rbp79;
    void** r12_80;
    void** rdi81;
    int1_t cf82;
    void** r9_83;
    void** rax84;
    void** rax85;
    void** rdi86;
    void* rax87;

    r15_8 = reinterpret_cast<unsigned char>(rsi) >> 1;
    r13_9 = r8;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    r14_11 = *reinterpret_cast<void***>(rcx + 40);
    r12_12 = *reinterpret_cast<void***>(rcx + 48);
    rcx13 = a7;
    v14 = rdx;
    v15 = r9;
    v16 = rcx13;
    rax17 = g28;
    v18 = rax17;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_11) + reinterpret_cast<unsigned char>(r12_12)) <= 0x1ffff || (v19 = rsi, reinterpret_cast<unsigned char>(rsi) <= reinterpret_cast<unsigned char>(1))) {
        addr_b0b7_2:
        r8_20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v14) << 5));
        rax21 = reinterpret_cast<unsigned char>(r14_11) << 5;
        r15_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) - rax21);
        r9_23 = reinterpret_cast<void*>(-rax21);
        if (reinterpret_cast<unsigned char>(r12_12) > reinterpret_cast<unsigned char>(1)) {
            *reinterpret_cast<int32_t*>(&rcx13) = 0;
            *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
            rsi = r12_12;
            rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_20) - (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_11) >> 1) << 5));
            sequential_sort(r15_22, rsi, rdx, 0);
            r9_23 = r9_23;
            r8_20 = r8_20;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rsi) = 0;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rcx13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 80);
        rdx = reinterpret_cast<void**>(0xb880);
        eax24 = fun_3c60(reinterpret_cast<int64_t>(rsp10) + 72);
        if (!eax24) {
            rax25 = *reinterpret_cast<void***>(rcx + 40);
            rcx26 = *reinterpret_cast<void***>(rcx + 72);
            sortlines(reinterpret_cast<unsigned char>(rdi) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax25) << 5), reinterpret_cast<unsigned char>(v19) - r15_8, v14, rcx26, r13_9, v15, v15);
            fun_3e00(v27);
            goto addr_b722_6;
        } else {
            r14_11 = *reinterpret_cast<void***>(rcx + 40);
            r12_12 = *reinterpret_cast<void***>(rcx + 48);
            goto addr_b0b7_2;
        }
    }
    if (reinterpret_cast<unsigned char>(r14_11) > reinterpret_cast<unsigned char>(1)) {
        *reinterpret_cast<int32_t*>(&rcx13) = 0;
        *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
        rdx = r8_20;
        rsi = r14_11;
        sequential_sort(rdi, rsi, rdx, 0);
        r9_23 = r9_23;
    }
    *reinterpret_cast<void***>(rcx) = rdi;
    r14_28 = reinterpret_cast<void***>(r13_9 + 48);
    *reinterpret_cast<void***>(rcx + 8) = r15_22;
    *reinterpret_cast<void***>(rcx + 16) = r15_22;
    *reinterpret_cast<void***>(rcx + 24) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + (reinterpret_cast<int64_t>(r9_23) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_12) << 5)));
    rbp29 = r13_9 + 8;
    fun_3e80(rbp29, rsi, rdx, rcx13);
    rdi30 = *reinterpret_cast<void***>(r13_9);
    rsi31 = rcx;
    heap_insert(rdi30, rsi31, rdx, rcx13);
    *reinterpret_cast<unsigned char*>(rcx + 84) = 1;
    fun_3a90(r14_28, rsi31, rdx, rcx13);
    fun_3b80(rbp29, rsi31, rdx, rcx13);
    while (1) {
        fun_3e80(rbp29, rsi31, rdx, rcx13);
        while (rdi32 = *reinterpret_cast<void***>(r13_9), rax33 = heap_remove_top(rdi32, rsi31, rdx, rcx13), rax33 == 0) {
            rsi31 = rbp29;
            fun_38e0(r14_28, rsi31, rdx, rcx13);
        }
        rbx34 = rax33;
        fun_3b80(rbp29, rsi31, rdx, rcx13);
        rax35 = rbx34 + 88;
        v36 = rax35;
        fun_3e80(rax35, rsi31, rdx, rcx13);
        *reinterpret_cast<uint32_t*>(&rax37) = *reinterpret_cast<uint32_t*>(rbx34 + 80);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax37) + 4) = 0;
        *reinterpret_cast<unsigned char*>(rbx34 + 84) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax37)) 
            break;
        ecx38 = static_cast<int32_t>(rax37 + rax37 + 2);
        rdx39 = *reinterpret_cast<void***>(rbx34);
        rdi40 = *reinterpret_cast<void***>(rbx34 + 8);
        v41 = rdx39;
        r9_42 = rdx39;
        v43 = rdi40;
        rsi31 = *reinterpret_cast<void***>(rbx34 + 16);
        rcx13 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(v14) >> *reinterpret_cast<signed char*>(&ecx38)) + 1);
        if (*reinterpret_cast<uint32_t*>(&rax37) == 1) {
            rax44 = rdi40;
            if (rdx39 == rsi31) {
                *reinterpret_cast<int32_t*>(&r10_45) = 0;
                *reinterpret_cast<int32_t*>(&r10_45 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            } else {
                v46 = rbp29;
                r15_47 = v15;
                rbp48 = rbx34;
                rbx49 = v16;
                while (*reinterpret_cast<void***>(rbp48 + 24) != rax44) {
                    r12_50 = rcx13 + 0xffffffffffffffff;
                    if (!rcx13) 
                        goto addr_b6d8_21;
                    rax51 = compare(r9_42 + 0xffffffffffffffe0, rax44 + 0xffffffffffffffe0, rdx39);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax51) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax51) == 0))) {
                        rdx39 = rbx49;
                        rsi31 = r15_47;
                        rdi52 = *reinterpret_cast<void***>(rbp48 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp48 + 8) = rdi52;
                        write_unique(rdi52, rsi31, rdx39);
                        r9_42 = *reinterpret_cast<void***>(rbp48);
                        rax44 = *reinterpret_cast<void***>(rbp48 + 8);
                        if (r9_42 == *reinterpret_cast<void***>(rbp48 + 16)) 
                            goto addr_b3f5_24;
                    } else {
                        rdx39 = rbx49;
                        rsi31 = r15_47;
                        rdi53 = *reinterpret_cast<void***>(rbp48) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp48) = rdi53;
                        write_unique(rdi53, rsi31, rdx39);
                        r9_42 = *reinterpret_cast<void***>(rbp48);
                        rax44 = *reinterpret_cast<void***>(rbp48 + 8);
                        if (r9_42 == *reinterpret_cast<void***>(rbp48 + 16)) 
                            goto addr_b3f5_24;
                    }
                    rcx13 = r12_50;
                }
                goto addr_b600_27;
            }
        } else {
            rdx = *reinterpret_cast<void***>(rbx34 + 32);
            rdi54 = v41;
            rax55 = v43;
            r15_56 = *reinterpret_cast<void***>(rdx);
            r11_57 = rdi54;
            if (rdi54 == rsi31) {
                r8d58 = 0;
                *reinterpret_cast<int32_t*>(&r10_45) = 0;
                *reinterpret_cast<int32_t*>(&r10_45 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r9_59) = 0;
                *reinterpret_cast<int32_t*>(&r9_59 + 4) = 0;
                goto addr_b2e8_30;
            } else {
                rdx60 = r15_56;
                v61 = r9_42;
                r15_62 = rbx34;
                rbx63 = rdx60;
                while (*reinterpret_cast<void***>(r15_62 + 24) != rax55) {
                    r12_64 = rcx13 + 0xffffffffffffffff;
                    if (!rcx13) 
                        goto addr_b5c0_34;
                    rbx63 = rbx63 - 32;
                    rax65 = compare(r11_57 + 0xffffffffffffffe0, rax55 + 0xffffffffffffffe0, rdx60);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax65) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax65) == 0))) {
                        r11_57 = *reinterpret_cast<void***>(r15_62);
                        rsi31 = *reinterpret_cast<void***>(r15_62 + 16);
                        __asm__("movdqu xmm0, [rcx-0x20]");
                        rax55 = *reinterpret_cast<void***>(r15_62 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_62 + 8) = rax55;
                        __asm__("movups [rbx], xmm0");
                        __asm__("movdqu xmm1, [rcx-0x10]");
                        __asm__("movups [rbx+0x10], xmm1");
                        if (rsi31 == r11_57) 
                            goto addr_b2b8_37;
                    } else {
                        rsi31 = *reinterpret_cast<void***>(r15_62 + 16);
                        __asm__("movdqu xmm2, [rax-0x20]");
                        r11_57 = *reinterpret_cast<void***>(r15_62) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_62) = r11_57;
                        __asm__("movups [rbx], xmm2");
                        __asm__("movdqu xmm3, [rax-0x10]");
                        rax55 = *reinterpret_cast<void***>(r15_62 + 8);
                        __asm__("movups [rbx+0x10], xmm3");
                        if (rsi31 == r11_57) 
                            goto addr_b2b8_37;
                    }
                    rcx13 = r12_64;
                }
                goto addr_b440_40;
            }
        }
        addr_b418_41:
        r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
        rsi31 = r11_66;
        if (*reinterpret_cast<void***>(rbx34 + 48) != r10_45) {
            addr_b6a8_42:
            r11_66 = rsi31;
            if (rsi31 != rdx || (rax44 == *reinterpret_cast<void***>(rbx34 + 24) || !rcx13)) {
                rdi54 = r9_42;
                r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
                r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
                r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx34 + 8))) >> 5);
            } else {
                v68 = rbp29;
                r15_69 = v15;
                rbp70 = rcx13 + 0xffffffffffffffff;
                r12_71 = v16;
                do {
                    rdi72 = rax44 + 0xffffffffffffffe0;
                    rdx = r12_71;
                    rsi31 = r15_69;
                    *reinterpret_cast<void***>(rbx34 + 8) = rdi72;
                    write_unique(rdi72, rsi31, rdx);
                    rax44 = *reinterpret_cast<void***>(rbx34 + 8);
                    if (rax44 == *reinterpret_cast<void***>(rbx34 + 24)) 
                        break;
                    cf73 = reinterpret_cast<unsigned char>(rbp70) < reinterpret_cast<unsigned char>(1);
                    --rbp70;
                } while (!cf73);
                rbp29 = v68;
                r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
                rdi54 = *reinterpret_cast<void***>(rbx34);
                r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
                r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
                r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax44)) >> 5);
            }
        } else {
            r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
            rdi54 = r9_42;
            r12_67 = r10_45;
        }
        addr_b301_49:
        *reinterpret_cast<void***>(rbx34 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_66) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(rdi54)) >> 5));
        *reinterpret_cast<void***>(rbx34 + 48) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_67) - reinterpret_cast<unsigned char>(r10_45));
        if (!*reinterpret_cast<signed char*>(&r8d58)) {
            rsi31 = rbx34;
            queue_check_insert_part_0(r13_9, rsi31, rdx);
        }
        if (*reinterpret_cast<uint32_t*>(rbx34 + 80) > 1) {
            rdi74 = *reinterpret_cast<void***>(rbx34 + 56) + 88;
            fun_3e80(rdi74, rsi31, rdx, rcx13);
            rsi31 = *reinterpret_cast<void***>(rbx34 + 56);
            if (!*reinterpret_cast<unsigned char*>(rsi31 + 84)) {
                queue_check_insert_part_0(r13_9, rsi31, rdx);
                rsi31 = *reinterpret_cast<void***>(rbx34 + 56);
            }
            fun_3b80(rsi31 + 88, rsi31, rdx, rcx13);
        } else {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx34 + 48)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx34 + 40)))) {
                r12_75 = *reinterpret_cast<void***>(rbx34 + 56);
                fun_3e80(rbp29, rsi31, rdx, rcx13);
                rdi76 = *reinterpret_cast<void***>(r13_9);
                rsi31 = r12_75;
                heap_insert(rdi76, rsi31, rdx, rcx13);
                *reinterpret_cast<unsigned char*>(r12_75 + 84) = 1;
                fun_3a90(r14_28, rsi31, rdx, rcx13);
                fun_3b80(rbp29, rsi31, rdx, rcx13);
            }
        }
        fun_3b80(v36, rsi31, rdx, rcx13);
        continue;
        addr_b600_27:
        rbx34 = rbp48;
        rbp29 = v46;
        addr_b608_58:
        r10_45 = *reinterpret_cast<void***>(rbx34 + 48);
        rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax44)) >> 5);
        if (rdx != r10_45) {
            rsi31 = *reinterpret_cast<void***>(rbx34 + 40);
            rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(r9_42)) >> 5);
            goto addr_b6a8_42;
        } else {
            rdi54 = r9_42;
            if (*reinterpret_cast<void***>(rbx34 + 16) == r9_42 || !rcx13) {
                r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
                r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
                r12_67 = r10_45;
                goto addr_b301_49;
            } else {
                v77 = rbp29;
                r15_78 = v15;
                rbp79 = rcx13 + 0xffffffffffffffff;
                r12_80 = v16;
                do {
                    rdi81 = rdi54 - 32;
                    rdx = r12_80;
                    rsi31 = r15_78;
                    *reinterpret_cast<void***>(rbx34) = rdi81;
                    write_unique(rdi81, rsi31, rdx);
                    rdi54 = *reinterpret_cast<void***>(rbx34);
                    if (rdi54 == *reinterpret_cast<void***>(rbx34 + 16)) 
                        break;
                    cf82 = reinterpret_cast<unsigned char>(rbp79) < reinterpret_cast<unsigned char>(1);
                    --rbp79;
                } while (!cf82);
                rbp29 = v77;
                r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
                r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx34 + 8))) >> 5);
                r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
                r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
                goto addr_b301_49;
            }
        }
        addr_b6d8_21:
        rbx34 = rbp48;
        rcx13 = reinterpret_cast<void**>(0xffffffffffffffff);
        rbp29 = v46;
        goto addr_b608_58;
        addr_b3f5_24:
        rbx34 = rbp48;
        rcx13 = r12_50;
        rbp29 = v46;
        rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(r9_42)) >> 5);
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax44)) >> 5);
        goto addr_b418_41;
        addr_b2e8_30:
        r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
        r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
        if (r10_45 == r12_67) 
            goto addr_b2fe_66;
        if (r9_59 != r11_66 || ((r9_83 = *reinterpret_cast<void***>(rbx34 + 24), r9_83 == rax55) || (rsi31 = rcx13 + 0xffffffffffffffff, rcx13 == 0))) {
            addr_b2fe_66:
            *reinterpret_cast<void***>(rdx) = r15_56;
            goto addr_b301_49;
        } else {
            rax84 = rax55 - 32;
            do {
                __asm__("movdqu xmm6, [rax]");
                r15_56 = r15_56 - 32;
                *reinterpret_cast<void***>(rbx34 + 8) = rax84;
                rcx13 = rax84;
                __asm__("movups [r15], xmm6");
                __asm__("movdqu xmm7, [rax+0x10]");
                __asm__("movups [r15+0x10], xmm7");
                if (rax84 == r9_83) 
                    break;
                --rsi31;
                rax84 = rax84 - 32;
            } while (rsi31 != 0xffffffffffffffff);
            goto addr_b7f7_71;
        }
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax84)) >> 5);
        goto addr_b2fe_66;
        addr_b7f7_71:
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rcx13)) >> 5);
        goto addr_b2fe_66;
        addr_b440_40:
        rbx34 = r15_62;
        rdi54 = *reinterpret_cast<void***>(rbx34);
        r15_56 = rbx63;
        r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
        rdx = *reinterpret_cast<void***>(rbx34 + 32);
        r9_59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v61) - reinterpret_cast<unsigned char>(r11_57)) >> 5);
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax55)) >> 5);
        addr_b46d_73:
        r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
        r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
        if (r12_67 == r10_45) {
            if (rsi31 == rdi54) 
                goto addr_b2fe_66;
            rax85 = rcx13 + 0xffffffffffffffff;
            if (!rcx13) 
                goto addr_b2fe_66;
            rcx13 = rdi54 + 0xffffffffffffffe0;
            do {
                __asm__("movdqu xmm4, [rcx]");
                r15_56 = r15_56 - 32;
                *reinterpret_cast<void***>(rbx34) = rcx13;
                rdi54 = rcx13;
                __asm__("movups [r15], xmm4");
                __asm__("movdqu xmm5, [rcx+0x10]");
                __asm__("movups [r15+0x10], xmm5");
                if (rcx13 == rsi31) 
                    break;
                --rax85;
                rcx13 = rcx13 - 32;
            } while (rax85 != 0xffffffffffffffff);
            goto addr_b2fe_66;
            goto addr_b2fe_66;
        }
        addr_b5c0_34:
        rbx34 = r15_62;
        rdi54 = *reinterpret_cast<void***>(rbx34);
        r15_56 = rbx63;
        r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
        rcx13 = reinterpret_cast<void**>(0xffffffffffffffff);
        rdx = *reinterpret_cast<void***>(rbx34 + 32);
        r9_59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v61) - reinterpret_cast<unsigned char>(r11_57)) >> 5);
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax55)) >> 5);
        goto addr_b46d_73;
        addr_b2b8_37:
        rbx34 = r15_62;
        r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
        r15_56 = rbx63;
        rdx = *reinterpret_cast<void***>(rbx34 + 32);
        rdi54 = r11_57;
        rcx13 = r12_64;
        r9_59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v61) - reinterpret_cast<unsigned char>(r11_57)) >> 5);
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax55)) >> 5);
        goto addr_b2e8_30;
    }
    fun_3b80(v36, rsi31, rdx, rcx13);
    fun_3e80(rbp29, rsi31, rdx, rcx13);
    rdi86 = *reinterpret_cast<void***>(r13_9);
    heap_insert(rdi86, rbx34, rdx, rcx13);
    *reinterpret_cast<unsigned char*>(rbx34 + 84) = 1;
    fun_3a90(r14_28, rbx34, rdx, rcx13);
    fun_3b80(rbp29, rbx34, rdx, rcx13);
    addr_b722_6:
    rax87 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
    if (rax87) {
        fun_3950();
    } else {
        return;
    }
}