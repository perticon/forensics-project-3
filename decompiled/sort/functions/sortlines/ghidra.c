void sortlines(long param_1,ulong param_2,ulong param_3,long *param_4,undefined8 *param_5,
              undefined8 param_6,undefined8 param_7)

{
  pthread_cond_t *__cond;
  pthread_mutex_t *__mutex;
  pthread_mutex_t *__mutex_00;
  undefined4 *puVar1;
  undefined4 *puVar2;
  undefined4 uVar3;
  undefined4 uVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  int iVar7;
  long *plVar8;
  undefined4 *puVar9;
  long lVar10;
  undefined8 *puVar11;
  undefined4 *puVar12;
  undefined4 *puVar13;
  undefined4 *puVar14;
  long lVar15;
  undefined4 *puVar16;
  char cVar17;
  long lVar18;
  long lVar19;
  long lVar20;
  undefined4 *puVar21;
  ulong uVar22;
  ulong uVar23;
  ulong uVar24;
  long lVar25;
  long in_FS_OFFSET;
  pthread_t local_80;
  long local_78;
  ulong local_70;
  ulong local_68;
  long local_60;
  undefined8 *local_58;
  undefined8 local_50;
  undefined8 local_48;
  long local_40;
  
  uVar24 = param_2 >> 1;
  uVar23 = param_4[5];
  uVar22 = param_4[6];
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_60 = param_4[8];
  local_48 = param_7;
  local_78 = param_1;
  local_70 = uVar24;
  local_68 = param_3;
  local_58 = param_5;
  local_50 = param_6;
  if ((0x1ffff < uVar23 + uVar22) && (1 < param_2)) {
    iVar7 = pthread_create(&local_80,(pthread_attr_t *)0x0,sortlines_thread,&local_78);
    if (iVar7 == 0) {
      sortlines(param_1 + param_4[5] * -0x20,param_2 - uVar24,param_3,param_4[9],param_5,param_6,
                param_7);
      pthread_join(local_80,(void **)0x0);
      goto LAB_0010b722;
    }
    uVar23 = param_4[5];
    uVar22 = param_4[6];
  }
  lVar18 = param_1 + param_3 * -0x20;
  lVar25 = param_1 + uVar23 * -0x20;
  if (1 < uVar22) {
    sequential_sort(lVar25,uVar22,lVar18 + (uVar23 >> 1) * -0x20);
  }
  if (1 < uVar23) {
    sequential_sort(param_1,uVar23,lVar18);
  }
  *param_4 = param_1;
  __cond = (pthread_cond_t *)(param_5 + 6);
  param_4[1] = lVar25;
  param_4[2] = lVar25;
  param_4[3] = param_1 + uVar23 * -0x20 + uVar22 * -0x20;
  __mutex = (pthread_mutex_t *)(param_5 + 1);
  pthread_mutex_lock(__mutex);
  heap_insert(*param_5,param_4);
  *(undefined *)((long)param_4 + 0x54) = 1;
  pthread_cond_signal(__cond);
  pthread_mutex_unlock(__mutex);
LAB_0010b180:
  pthread_mutex_lock(__mutex);
  while (plVar8 = (long *)heap_remove_top(*param_5), plVar8 == (long *)0x0) {
    pthread_cond_wait(__cond,__mutex);
  }
  pthread_mutex_unlock(__mutex);
  __mutex_00 = (pthread_mutex_t *)(plVar8 + 0xb);
  pthread_mutex_lock(__mutex_00);
  iVar7 = *(int *)(plVar8 + 10);
  *(undefined *)((long)plVar8 + 0x54) = 0;
  if (iVar7 != 0) {
    puVar1 = (undefined4 *)*plVar8;
    puVar2 = (undefined4 *)plVar8[1];
    puVar14 = (undefined4 *)plVar8[2];
    lVar18 = (param_3 >> ((char)iVar7 * '\x02' + 2U & 0x3f)) + 1;
    puVar16 = puVar1;
    if (iVar7 != 1) {
      puVar11 = (undefined8 *)plVar8[4];
      puVar12 = (undefined4 *)*puVar11;
      puVar9 = puVar2;
      lVar10 = lVar18;
      puVar21 = puVar1;
      if (puVar1 == puVar14) {
        cVar17 = '\0';
        lVar20 = 0;
        lVar19 = 0;
      }
      else {
LAB_0010b264:
        do {
          if ((undefined4 *)plVar8[3] == puVar9) {
            puVar16 = (undefined4 *)*plVar8;
            cVar17 = *(char *)((long)plVar8 + 0x54);
            puVar11 = (undefined8 *)plVar8[4];
LAB_0010b46d:
            lVar20 = (long)puVar2 - (long)puVar9 >> 5;
            lVar19 = (long)puVar1 - (long)puVar21 >> 5;
            lVar25 = plVar8[6];
            lVar15 = plVar8[5];
            if (lVar25 != lVar20) goto LAB_0010b2f5;
            if ((puVar14 == puVar16) ||
               (lVar18 = lVar10 + -1, puVar9 = puVar12, puVar2 = puVar16, lVar10 == 0))
            goto LAB_0010b2fe;
            goto LAB_0010b4b2;
          }
          lVar18 = lVar10 + -1;
          if (lVar10 == 0) {
            puVar16 = (undefined4 *)*plVar8;
            cVar17 = *(char *)((long)plVar8 + 0x54);
            lVar10 = -1;
            puVar11 = (undefined8 *)plVar8[4];
            goto LAB_0010b46d;
          }
          puVar13 = puVar12 + -8;
          iVar7 = compare(puVar21 + -8,puVar9 + -8);
          lVar10 = lVar18;
          if (iVar7 < 1) {
            lVar25 = *plVar8;
            puVar14 = (undefined4 *)plVar8[2];
            uVar3 = *(undefined4 *)(lVar25 + -0x20);
            uVar4 = *(undefined4 *)(lVar25 + -0x1c);
            uVar5 = *(undefined4 *)(&DAT_ffffffffffffffe8 + lVar25);
            uVar6 = *(undefined4 *)(lVar25 + -0x14);
            puVar21 = (undefined4 *)(lVar25 + -0x20);
            *plVar8 = (long)puVar21;
            *puVar13 = uVar3;
            puVar12[-7] = uVar4;
            puVar12[-6] = uVar5;
            puVar12[-5] = uVar6;
            uVar3 = *(undefined4 *)(lVar25 + -0xc);
            uVar4 = *(undefined4 *)(lVar25 + -8);
            uVar5 = *(undefined4 *)(lVar25 + -4);
            puVar9 = (undefined4 *)plVar8[1];
            puVar12[-4] = *(undefined4 *)(lVar25 + -0x10);
            puVar12[-3] = uVar3;
            puVar12[-2] = uVar4;
            puVar12[-1] = uVar5;
            puVar12 = puVar13;
            if (puVar14 == puVar21) break;
            goto LAB_0010b264;
          }
          lVar25 = plVar8[1];
          puVar21 = (undefined4 *)*plVar8;
          puVar14 = (undefined4 *)plVar8[2];
          uVar3 = *(undefined4 *)(lVar25 + -0x20);
          uVar4 = *(undefined4 *)(lVar25 + -0x1c);
          uVar5 = *(undefined4 *)(&DAT_ffffffffffffffe8 + lVar25);
          uVar6 = *(undefined4 *)(lVar25 + -0x14);
          puVar9 = (undefined4 *)(lVar25 + -0x20);
          plVar8[1] = (long)puVar9;
          *puVar13 = uVar3;
          puVar12[-7] = uVar4;
          puVar12[-6] = uVar5;
          puVar12[-5] = uVar6;
          uVar3 = *(undefined4 *)(lVar25 + -0xc);
          uVar4 = *(undefined4 *)(lVar25 + -8);
          uVar5 = *(undefined4 *)(lVar25 + -4);
          puVar12[-4] = *(undefined4 *)(lVar25 + -0x10);
          puVar12[-3] = uVar3;
          puVar12[-2] = uVar4;
          puVar12[-1] = uVar5;
          puVar12 = puVar13;
        } while (puVar14 != puVar21);
        cVar17 = *(char *)((long)plVar8 + 0x54);
        puVar11 = (undefined8 *)plVar8[4];
        lVar19 = (long)puVar1 - (long)puVar21 >> 5;
        lVar20 = (long)puVar2 - (long)puVar9 >> 5;
        puVar16 = puVar21;
        puVar12 = puVar13;
      }
      lVar25 = plVar8[6];
      lVar15 = plVar8[5];
      lVar10 = lVar18;
      if (lVar20 != lVar25) {
LAB_0010b2f5:
        if (((lVar19 == lVar15) && (puVar14 = (undefined4 *)plVar8[3], puVar14 != puVar9)) &&
           (lVar18 = lVar10 + -1, puVar21 = puVar12, lVar10 != 0)) {
          do {
            puVar13 = puVar9 + -8;
            uVar3 = *puVar13;
            uVar4 = puVar9[-7];
            uVar5 = puVar9[-6];
            uVar6 = puVar9[-5];
            puVar12 = puVar21 + -8;
            plVar8[1] = (long)puVar13;
            *puVar12 = uVar3;
            puVar21[-7] = uVar4;
            puVar21[-6] = uVar5;
            puVar21[-5] = uVar6;
            uVar3 = puVar9[-3];
            uVar4 = puVar9[-2];
            uVar5 = puVar9[-1];
            puVar21[-4] = puVar9[-4];
            puVar21[-3] = uVar3;
            puVar21[-2] = uVar4;
            puVar21[-1] = uVar5;
            if (puVar13 == puVar14) {
              lVar20 = (long)puVar2 - (long)puVar13 >> 5;
              goto LAB_0010b2fe;
            }
            lVar18 = lVar18 + -1;
            puVar21 = puVar12;
            puVar9 = puVar13;
          } while (lVar18 != -1);
          lVar20 = (long)puVar2 - (long)puVar13 >> 5;
        }
      }
      goto LAB_0010b2fe;
    }
    puVar9 = puVar2;
    lVar25 = lVar18;
    if (puVar1 == puVar14) {
      lVar20 = 0;
      lVar10 = 0;
    }
    else {
LAB_0010b3a8:
      do {
        if ((undefined4 *)plVar8[3] == puVar9) {
LAB_0010b608:
          lVar20 = plVar8[6];
          if ((long)puVar2 - (long)puVar9 >> 5 != lVar20) {
            lVar15 = plVar8[5];
            lVar10 = (long)puVar1 - (long)puVar16 >> 5;
            goto LAB_0010b6a8;
          }
          if (((undefined4 *)plVar8[2] != puVar16) && (lVar25 != 0)) goto LAB_0010b656;
          cVar17 = *(char *)((long)plVar8 + 0x54);
          lVar15 = plVar8[5];
          lVar25 = lVar20;
          goto LAB_0010b301;
        }
        lVar18 = lVar25 + -1;
        if (lVar25 == 0) {
          lVar25 = -1;
          goto LAB_0010b608;
        }
        iVar7 = compare(puVar16 + -8,puVar9 + -8);
        lVar25 = lVar18;
        if (iVar7 < 1) {
          lVar20 = *plVar8;
          *plVar8 = lVar20 + -0x20;
          write_unique(lVar20 + -0x20,param_6,param_7);
          puVar16 = (undefined4 *)*plVar8;
          puVar9 = (undefined4 *)plVar8[1];
          if (puVar16 == (undefined4 *)plVar8[2]) break;
          goto LAB_0010b3a8;
        }
        lVar20 = plVar8[1];
        plVar8[1] = lVar20 + -0x20;
        write_unique(lVar20 + -0x20,param_6,param_7);
        puVar16 = (undefined4 *)*plVar8;
        puVar9 = (undefined4 *)plVar8[1];
      } while (puVar16 != (undefined4 *)plVar8[2]);
      lVar10 = (long)puVar1 - (long)puVar16 >> 5;
      lVar20 = (long)puVar2 - (long)puVar9 >> 5;
    }
    lVar15 = plVar8[5];
    lVar25 = lVar18;
    if (plVar8[6] == lVar20) {
      cVar17 = *(char *)((long)plVar8 + 0x54);
      lVar25 = lVar20;
    }
    else {
LAB_0010b6a8:
      if (((lVar15 == lVar10) && (puVar9 != (undefined4 *)plVar8[3])) && (lVar25 != 0)) {
        do {
          lVar25 = lVar25 + -1;
          plVar8[1] = (long)(puVar9 + -8);
          write_unique(puVar9 + -8,param_6,param_7);
          puVar9 = (undefined4 *)plVar8[1];
          if (puVar9 == (undefined4 *)plVar8[3]) break;
        } while (lVar25 != 0);
        cVar17 = *(char *)((long)plVar8 + 0x54);
        puVar16 = (undefined4 *)*plVar8;
        lVar15 = plVar8[5];
        lVar20 = (long)puVar2 - (long)puVar9 >> 5;
        lVar25 = plVar8[6];
      }
      else {
        cVar17 = *(char *)((long)plVar8 + 0x54);
        lVar20 = (long)puVar2 - plVar8[1] >> 5;
        lVar25 = plVar8[6];
      }
    }
    goto LAB_0010b301;
  }
  pthread_mutex_unlock(__mutex_00);
  pthread_mutex_lock(__mutex);
  heap_insert(*param_5,plVar8);
  *(undefined *)((long)plVar8 + 0x54) = 1;
  pthread_cond_signal(__cond);
  pthread_mutex_unlock(__mutex);
LAB_0010b722:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
  while (lVar25 != 0) {
LAB_0010b656:
    lVar25 = lVar25 + -1;
    *plVar8 = (long)(puVar16 + -8);
    write_unique(puVar16 + -8,param_6,param_7);
    puVar16 = (undefined4 *)*plVar8;
    if (puVar16 == (undefined4 *)plVar8[2]) break;
  }
  cVar17 = *(char *)((long)plVar8 + 0x54);
  lVar20 = (long)puVar2 - plVar8[1] >> 5;
  lVar15 = plVar8[5];
  lVar25 = plVar8[6];
  goto LAB_0010b301;
  while (lVar18 = lVar18 + -1, puVar9 = puVar12, puVar2 = puVar16, lVar18 != -1) {
LAB_0010b4b2:
    puVar16 = puVar2 + -8;
    uVar3 = *puVar16;
    uVar4 = puVar2[-7];
    uVar5 = puVar2[-6];
    uVar6 = puVar2[-5];
    puVar12 = puVar9 + -8;
    *plVar8 = (long)puVar16;
    *puVar12 = uVar3;
    puVar9[-7] = uVar4;
    puVar9[-6] = uVar5;
    puVar9[-5] = uVar6;
    uVar3 = puVar2[-3];
    uVar4 = puVar2[-2];
    uVar5 = puVar2[-1];
    puVar9[-4] = puVar2[-4];
    puVar9[-3] = uVar3;
    puVar9[-2] = uVar4;
    puVar9[-1] = uVar5;
    if (puVar16 == puVar14) break;
  }
LAB_0010b2fe:
  *puVar11 = puVar12;
LAB_0010b301:
  plVar8[5] = lVar15 - ((long)puVar1 - (long)puVar16 >> 5);
  plVar8[6] = lVar25 - lVar20;
  if (cVar17 == '\0') {
    queue_check_insert_part_0(param_5,plVar8);
  }
  if (*(uint *)(plVar8 + 10) < 2) {
    if (plVar8[6] + plVar8[5] == 0) {
      lVar18 = plVar8[7];
      pthread_mutex_lock(__mutex);
      heap_insert(*param_5,lVar18);
      *(undefined *)(lVar18 + 0x54) = 1;
      pthread_cond_signal(__cond);
      pthread_mutex_unlock(__mutex);
    }
  }
  else {
    pthread_mutex_lock((pthread_mutex_t *)(plVar8[7] + 0x58));
    lVar18 = plVar8[7];
    if (*(char *)(lVar18 + 0x54) == '\0') {
      queue_check_insert_part_0(param_5);
      lVar18 = plVar8[7];
    }
    pthread_mutex_unlock((pthread_mutex_t *)(lVar18 + 0x58));
  }
  pthread_mutex_unlock(__mutex_00);
  goto LAB_0010b180;
}