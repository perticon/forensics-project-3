bool default_key_compare(int32_t * key) {
    int64_t v1 = (int64_t)key;
    if (*(int64_t *)(v1 + 32) != 0 || *(int64_t *)(v1 + 40) != 0) {
        // 0x6d99
        return false;
    }
    int64_t v2 = v1 + 48; // 0x6da7
    unsigned char v3 = *(char *)v2; // 0x6da7
    if (v3 != 0) {
        // 0x6de0
        return false;
    }
    // 0x6daf
    if (*(char *)(v1 + 49) != 0 || (*(int64_t *)v2 & 0xffff00ff0000) != 0) {
        // 0x6d99
        return (int64_t)v3 % 2 != 0;
    }
    unsigned char v4 = *(char *)(v1 + 54); // 0x6dc5
    if (v4 != 0) {
        // 0x6de0
        return false;
    }
    // 0x6dcd
    if (*(char *)(v1 + 56) != 0) {
        // 0x6d99
        return (int64_t)v4 % 2 != 0;
    }
    // 0x6dd3
    return *(char *)(v1 + 51) % 2 == 0;
}