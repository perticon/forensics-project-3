byte default_key_compare(long param_1)

{
  byte bVar1;
  
  bVar1 = 0;
  if ((*(long *)(param_1 + 0x20) == 0) && (*(long *)(param_1 + 0x28) == 0)) {
    bVar1 = *(byte *)(param_1 + 0x30);
    if (bVar1 != 0) {
      return 0;
    }
    if ((*(char *)(param_1 + 0x31) == '\0') && ((*(ulong *)(param_1 + 0x30) & 0xffff00ff0000) == 0))
    {
      bVar1 = *(byte *)(param_1 + 0x36);
      if (bVar1 != 0) {
        return 0;
      }
      if (*(char *)(param_1 + 0x38) == '\0') {
        return *(byte *)(param_1 + 0x33) ^ 1;
      }
    }
  }
  return bVar1;
}