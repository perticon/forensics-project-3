signed char default_key_compare(void** rdi, ...) {
    uint32_t eax2;
    uint32_t eax3;

    eax2 = 0;
    if (*reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<void***>(rdi + 40)) {
        addr_6d99_2:
        return *reinterpret_cast<signed char*>(&eax2);
    } else {
        eax2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48));
        if (*reinterpret_cast<signed char*>(&eax2)) 
            goto addr_6de0_4;
        if (*reinterpret_cast<signed char*>(rdi + 49)) 
            goto addr_6d99_2;
        if (0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48))) 
            goto addr_6d99_2;
    }
    eax2 = *reinterpret_cast<unsigned char*>(rdi + 54);
    if (*reinterpret_cast<signed char*>(&eax2)) {
        addr_6de0_4:
        return 0;
    } else {
        if (!*reinterpret_cast<void***>(rdi + 56)) {
            eax3 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rdi + 51)) ^ 1;
            return *reinterpret_cast<signed char*>(&eax3);
        }
    }
}