bool compare_nodes(long param_1,long param_2)

{
  if (*(uint *)(param_1 + 0x50) != *(uint *)(param_2 + 0x50)) {
    return *(uint *)(param_1 + 0x50) < *(uint *)(param_2 + 0x50);
  }
  return (ulong)(*(long *)(param_1 + 0x30) + *(long *)(param_1 + 0x28)) <
         (ulong)(*(long *)(param_2 + 0x30) + *(long *)(param_2 + 0x28));
}