check_output (char const *outfile)
{
  if (outfile)
    {
      int oflags = O_WRONLY | O_BINARY | O_CLOEXEC | O_CREAT;
      int outfd = open (outfile, oflags, MODE_RW_UGO);
      if (outfd < 0)
        sort_die (_("open failed"), outfile);
      move_fd (outfd, STDOUT_FILENO);
    }
}