int32_t pipe_fork(int32_t * pipefds, int64_t tries) {
    int64_t v1 = __readfsqword(40); // 0x842e
    int64_t result = 0xffffffff; // 0x8448
    int64_t v2; // 0x8410
    int64_t v3; // 0x8410
    int32_t * v4; // 0x8410
    int64_t v5; // 0x851c
    int32_t v6; // 0x8521
    int32_t v7; // 0x852a
    if (rpl_pipe2(pipefds, 0x80000) < 0) {
        goto lab_0x8568;
    } else {
        // 0x844e
        if (nmerge + 1 < nprocs) {
            // 0x85b0
            reap(-1);
            while (nprocs >= 1) {
                // 0x85c8
                if (reap(0) == 0) {
                    // break -> 0x8463
                    break;
                }
            }
        }
        // 0x8463
        v4 = (int32_t *)function_37d0();
        v3 = __asm_movsd_1(__asm_movsd(0x3fd0000000000000));
        v2 = tries;
        while (true) {
          lab_0x84f6:;
            int64_t v8 = function_3c00(); // 0x84fe
            *(int64_t *)&temphead = 0;
            v5 = function_3e70();
            v6 = *v4;
            v7 = v5;
            if (v7 != 0) {
                // 0x8490
                *(int64_t *)&temphead = (int64_t)temphead;
                if ((int32_t)v8 == 0) {
                    // 0x8539
                    function_3c00();
                    goto lab_0x84a2;
                } else {
                    goto lab_0x84a2;
                }
            } else {
                if ((int32_t)v8 != 0) {
                    // break -> 0x8598
                    break;
                }
                // 0x8539
                function_3c00();
                goto lab_0x84a2;
            }
        }
        goto lab_0x8598;
    }
  lab_0x85e4:
    // 0x85e4
    function_3a40();
    function_3a40();
    int32_t v9; // 0x8410
    *v4 = v9;
    result = v5 & 0xffffffff;
    goto lab_0x8568;
  lab_0x84a2:
    // 0x84a2
    *v4 = v6;
    if (v7 >= 0) {
        if (v7 == 0) {
            goto lab_0x8598;
        } else {
            int32_t v10 = nprocs; // 0x8561
            nprocs = v10 + 1;
            result = v5 & 0xffffffff;
            goto lab_0x8568;
        }
    }
    // 0x84ae
    v9 = v6;
    if (v6 != 11) {
        goto lab_0x85e4;
    }
    int64_t v11 = v2 - 1;
    xnanosleep((float64_t)(int64_t)__asm_movsd(v3));
    int128_t v12 = __asm_movsd(v3); // 0x84c3
    int64_t v13 = __asm_movsd_1(__asm_addsd(__asm_movapd(v12), v12)); // 0x84d1
    while (nprocs >= 1) {
        // 0x84e1
        if (reap(0) == 0) {
            // break -> 0x84ec
            break;
        }
    }
    // 0x84ec
    v3 = v13;
    v2 = v11;
    if (v11 == 0) {
        // 0x85e0
        v9 = *v4;
        goto lab_0x85e4;
    }
    goto lab_0x84f6;
  lab_0x8568:
    // 0x8568
    if (v1 != __readfsqword(40)) {
        // 0x8616
        return function_3950();
    }
    // 0x857f
    return result;
  lab_0x8598:
    // 0x8598
    function_3a40();
    function_3a40();
    result = 0;
    goto lab_0x8568;
}