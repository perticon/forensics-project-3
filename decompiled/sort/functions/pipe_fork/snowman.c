void** pipe_fork(void** rdi, void** rsi, void** rdx, ...) {
    void** rbx4;
    void** rsi5;
    void** v6;
    void** rax7;
    void** v8;
    int32_t eax9;
    void* rsp10;
    int64_t r14_11;
    uint32_t eax12;
    int1_t cf13;
    uint32_t edx14;
    int32_t eax15;
    void** rax16;
    void** r12_17;
    void** rbp18;
    int32_t eax19;
    void** r13_20;
    unsigned char v21;
    void** eax22;
    void** r8d23;
    void** ecx24;
    void** rdi25;
    void** rsi26;
    void** rsi27;
    uint32_t eax28;
    int32_t eax29;
    int1_t cf30;
    void* rax31;
    void** r13_32;
    void** rbp33;
    void** rbx34;
    void** rax35;
    void** v36;
    void** rax37;
    void* rsp38;
    void** v39;
    void* rax40;
    void** rsi41;
    void** rax42;
    void** rax43;
    void** r13d44;
    int64_t v45;
    void** r12_46;
    void** r15_47;
    uint32_t eax48;
    void** rdi49;
    void** rax50;
    void** r9d51;
    void** rdi52;
    void** rsi53;
    struct s3* rax54;
    int32_t eax55;
    void** eax56;
    int64_t rdi57;
    int32_t v58;
    void** rax59;
    void** rax60;
    int32_t v61;
    void** rax62;
    void** edi63;
    uint32_t tmp32_64;

    rbx4 = rsi;
    *reinterpret_cast<int32_t*>(&rsi5) = 0x80000;
    *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
    v6 = rdi;
    rax7 = g28;
    v8 = rax7;
    eax9 = rpl_pipe2();
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8);
    if (eax9 < 0) {
        *reinterpret_cast<void***>(&r14_11) = reinterpret_cast<void**>(0xffffffff);
    } else {
        eax12 = nmerge;
        cf13 = eax12 + 1 < nprocs;
        if (cf13) {
            rdi = reinterpret_cast<void**>(0xffffffff);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            reap(0xffffffff, 0x80000);
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            while ((edx14 = nprocs, !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(edx14) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(edx14 == 0))) && (rdi = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, eax15 = reap(0, 0x80000), rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), !!eax15)) {
            }
        }
        rax16 = fun_37d0();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        --rbx4;
        *reinterpret_cast<void***>(rdi) = g80000;
        r12_17 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 40);
        rbp18 = rax16;
        *reinterpret_cast<void***>(rdi + 4) = g80004;
        while (1) {
            rdi = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            rdx = r12_17;
            rsi5 = reinterpret_cast<void**>(0x1d3a0);
            eax19 = fun_3c00();
            r13_20 = temphead;
            temphead = reinterpret_cast<void**>(0);
            v21 = reinterpret_cast<uint1_t>(eax19 == 0);
            eax22 = fun_3e70();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
            r8d23 = *reinterpret_cast<void***>(rbp18);
            ecx24 = eax22;
            *reinterpret_cast<void***>(&r14_11) = eax22;
            if (eax22) {
                temphead = r13_20;
                if (v21) {
                    addr_8539_16:
                    *reinterpret_cast<int32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rsi5 = r12_17;
                    rdi = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    fun_3c00();
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                    ecx24 = ecx24;
                    r8d23 = r8d23;
                    goto addr_84a2_17;
                } else {
                    addr_84a2_17:
                    *reinterpret_cast<void***>(rbp18) = r8d23;
                    if (reinterpret_cast<signed char>(ecx24) >= reinterpret_cast<signed char>(0)) 
                        goto addr_855f_18;
                }
                if (!reinterpret_cast<int1_t>(r8d23 == 11)) 
                    goto addr_85e4_20;
                *reinterpret_cast<void***>(rdi) = *reinterpret_cast<void***>(rsi5);
                rdi25 = rdi + 4;
                rsi26 = rsi5 + 4;
                xnanosleep();
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                *reinterpret_cast<void***>(rdi25) = *reinterpret_cast<void***>(rsi26);
                rsi27 = rsi26 + 4;
                __asm__("movapd xmm1, xmm2");
                __asm__("addsd xmm1, xmm2");
                *reinterpret_cast<void***>(rdi25 + 4) = *reinterpret_cast<void***>(rsi27);
                rsi5 = rsi27 + 4;
                do {
                    eax28 = nprocs;
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax28) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax28 == 0)) 
                        break;
                    eax29 = reap(0, rsi5);
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                } while (eax29);
                cf30 = reinterpret_cast<unsigned char>(rbx4) < reinterpret_cast<unsigned char>(1);
                --rbx4;
                if (cf30) 
                    goto addr_85e0_33;
            } else {
                if (!v21) 
                    goto addr_8598_35; else 
                    goto addr_8539_16;
            }
        }
    }
    addr_8568_36:
    rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rax31) {
        return *reinterpret_cast<void***>(&r14_11);
    }
    fun_3950();
    r13_32 = rsi5;
    rbp33 = rdx;
    rbx34 = rdi;
    rax35 = g28;
    v36 = rax35;
    rax37 = xnmalloc(r13_32, 8, rdx);
    rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    v39 = rax37;
    *reinterpret_cast<void***>(rbp33) = rax37;
    if (r13_32) 
        goto addr_8666_40;
    addr_8727_42:
    while (rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v36) - reinterpret_cast<unsigned char>(g28)), !!rax40) {
        addr_885c_43:
        fun_3950();
        addr_8861_44:
        rsi41 = compress_program;
        quotearg_style(4, rsi41, rdx, 4, rsi41, rdx);
        rax42 = fun_3920();
        rdx = rax42;
        fun_3c90();
        addr_889c_45:
        rax43 = fun_37d0();
        r13d44 = *reinterpret_cast<void***>(rax43);
        fun_3a40();
        *reinterpret_cast<void***>(rbp33) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax43) = r13d44;
    }
    goto v45;
    addr_8666_40:
    rbp33 = rax37;
    *reinterpret_cast<int32_t*>(&r12_46) = 0;
    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
    do {
        r15_47 = *reinterpret_cast<void***>(rbx34 + 8);
        if (!r15_47 || (eax48 = *reinterpret_cast<unsigned char*>(r15_47 + 12), *reinterpret_cast<signed char*>(&eax48) == 0)) {
            rdi49 = *reinterpret_cast<void***>(rbx34);
            rax50 = stream_open(rdi49, "r", rdx);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
            *reinterpret_cast<void***>(rbp33) = rax50;
            if (!rax50) 
                goto addr_8727_42;
        } else {
            if (*reinterpret_cast<signed char*>(&eax48) == 1 && (r9d51 = *reinterpret_cast<void***>(r15_47 + 8), rdi52 = proctab, rsi53 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp38) + 16), rax54 = hash_remove(rdi52, rsi53, rdx), rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8), !!rax54)) {
                rax54->fc = 2;
                reap(r9d51, rsi53);
                rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
            }
            eax55 = fun_3cd0(r15_47 + 13);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
            if (eax55 < 0) 
                break;
            eax56 = pipe_fork(reinterpret_cast<int64_t>(rsp38) + 16, 9, rdx);
            if (reinterpret_cast<int1_t>(eax56 == 0xffffffff)) 
                goto addr_86f1_53;
            if (!eax56) 
                goto addr_87fa_55;
            *reinterpret_cast<void***>(r15_47 + 8) = eax56;
            register_proc(r15_47, 9, rdx);
            fun_3a40();
            fun_3a40();
            *reinterpret_cast<int32_t*>(&rdi57) = v58;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi57) + 4) = 0;
            rax59 = fun_3c20(rdi57, "r", rdx);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            if (!rax59) 
                goto addr_889c_45;
            *reinterpret_cast<void***>(rbp33) = rax59;
        }
        ++r12_46;
        rbx34 = rbx34 + 16;
        rbp33 = rbp33 + 8;
    } while (r13_32 != r12_46);
    goto addr_8727_42;
    *reinterpret_cast<void***>(v39 + reinterpret_cast<unsigned char>(r12_46) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_42;
    addr_86f1_53:
    rax60 = fun_37d0();
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax60) == 24)) 
        goto addr_8861_44;
    fun_3a40();
    *reinterpret_cast<void***>(rax60) = reinterpret_cast<void**>(24);
    *reinterpret_cast<void***>(v39 + reinterpret_cast<unsigned char>(r12_46) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_42;
    addr_87fa_55:
    fun_3a40();
    if (eax55) {
        move_fd_part_0(eax55);
    }
    if (v61 != 1) {
        move_fd_part_0(v61, v61);
    }
    rdx = reinterpret_cast<void**>("-d");
    fun_3e20();
    rax62 = fun_37d0();
    edi63 = *reinterpret_cast<void***>(rax62);
    async_safe_die(edi63, "couldn't execute compress program (with -d)", edi63, "couldn't execute compress program (with -d)");
    goto addr_885c_43;
    addr_855f_18:
    if (!ecx24) {
        addr_8598_35:
        *reinterpret_cast<void***>(&r14_11) = reinterpret_cast<void**>(0);
        fun_3a40();
        rdi = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        fun_3a40();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
        goto addr_8568_36;
    } else {
        tmp32_64 = nprocs + 1;
        nprocs = tmp32_64;
        goto addr_8568_36;
    }
    addr_85e4_20:
    fun_3a40();
    rdi = *reinterpret_cast<void***>(v6 + 4);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    fun_3a40();
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
    *reinterpret_cast<void***>(rbp18) = r8d23;
    goto addr_8568_36;
    addr_85e0_33:
    r8d23 = *reinterpret_cast<void***>(rbp18);
    goto addr_85e4_20;
}