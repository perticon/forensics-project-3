__pid_t pipe_fork(int *param_1,long param_2)

{
  undefined8 uVar1;
  int iVar2;
  int iVar3;
  __pid_t _Var4;
  int *piVar5;
  long in_FS_OFFSET;
  bool bVar6;
  double local_e0;
  __sigset_t local_c0;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar2 = rpl_pipe2(param_1,0x80000);
  if (iVar2 < 0) {
    _Var4 = -1;
  }
  else {
    if (nmerge + 1U < nprocs) {
      reap();
      do {
        if ((int)nprocs < 1) break;
        iVar2 = reap();
      } while (iVar2 != 0);
    }
    piVar5 = __errno_location();
    param_2 = param_2 + -1;
    local_e0 = DAT_001170e8;
    do {
      iVar3 = pthread_sigmask(0,(__sigset_t *)caught_signals,&local_c0);
      uVar1 = temphead;
      temphead = 0;
      _Var4 = fork();
      iVar2 = *piVar5;
      if (_Var4 == 0) {
        if (iVar3 == 0) goto LAB_00108539;
LAB_00108598:
        _Var4 = 0;
        close(0);
        close(1);
        goto LAB_00108568;
      }
      temphead = uVar1;
      if (iVar3 == 0) {
LAB_00108539:
        pthread_sigmask(2,&local_c0,(__sigset_t *)0x0);
      }
      *piVar5 = iVar2;
      if (-1 < _Var4) {
        if (_Var4 == 0) goto LAB_00108598;
        nprocs = nprocs + 1;
        goto LAB_00108568;
      }
      if (iVar2 != 0xb) goto LAB_001085e4;
      xnanosleep(local_e0);
      local_e0 = local_e0 + local_e0;
      do {
        if ((int)nprocs < 1) break;
        iVar2 = reap();
      } while (iVar2 != 0);
      bVar6 = param_2 != 0;
      param_2 = param_2 + -1;
    } while (bVar6);
    iVar2 = *piVar5;
LAB_001085e4:
    close(*param_1);
    close(param_1[1]);
    *piVar5 = iVar2;
  }
LAB_00108568:
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return _Var4;
}