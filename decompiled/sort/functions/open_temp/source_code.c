open_temp (struct tempnode *temp)
{
  int tempfd, pipefds[2];
  FILE *fp = NULL;

  if (temp->state == UNREAPED)
    wait_proc (temp->pid);

  tempfd = open (temp->name, O_RDONLY);
  if (tempfd < 0)
    return NULL;

  pid_t child = pipe_fork (pipefds, MAX_FORK_TRIES_DECOMPRESS);

  switch (child)
    {
    case -1:
      if (errno != EMFILE)
        die (SORT_FAILURE, errno, _("couldn't create process for %s -d"),
             quoteaf (compress_program));
      close (tempfd);
      errno = EMFILE;
      break;

    case 0:
      /* Being the child of a multithreaded program before exec,
         we're restricted to calling async-signal-safe routines here.  */
      close (pipefds[0]);
      move_fd (tempfd, STDIN_FILENO);
      move_fd (pipefds[1], STDOUT_FILENO);

      execlp (compress_program, compress_program, "-d", (char *) NULL);

      async_safe_die (errno, "couldn't execute compress program (with -d)");

    default:
      temp->pid = child;
      register_proc (temp);
      close (tempfd);
      close (pipefds[1]);

      fp = fdopen (pipefds[0], "r");
      if (! fp)
        {
          int saved_errno = errno;
          close (pipefds[0]);
          errno = saved_errno;
        }
      break;
    }

  return fp;
}