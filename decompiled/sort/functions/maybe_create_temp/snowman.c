void** maybe_create_temp(void** rdi, int32_t esi, void** rdx) {
    int64_t rdx4;
    int32_t v5;
    void** rax6;
    void** rax7;
    void** r14_8;
    void** rax9;
    void** rax10;
    void** r15_11;
    void** r12_12;
    void* rsp13;
    int64_t rax14;
    int32_t eax15;
    int64_t rax16;
    int64_t rax17;
    int1_t zf18;
    void** r13_19;
    void** rdx20;
    int32_t eax21;
    void** rsi22;
    int32_t v23;
    int32_t eax24;
    int32_t ebp25;
    void** rax26;
    void* rsp27;
    void** ebp28;
    void** rax29;
    int1_t zf30;
    void** eax31;
    uint1_t zf32;
    void** rdi33;
    int32_t v34;
    void** edi35;
    int64_t rdi36;
    void** rax37;
    void** rax38;
    void* rax39;

    rdx4 = temp_dir_index_2;
    v5 = esi;
    rax6 = g28;
    rax7 = temp_dirs;
    r14_8 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(rdx4 * 8));
    rax9 = fun_3940(r14_8);
    rax10 = xmalloc(reinterpret_cast<uint64_t>(rax9 + 32) & 0xfffffffffffffff8);
    r15_11 = rax10 + 13;
    r12_12 = rax10;
    fun_3b30(r15_11, r14_8, rax9);
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 - 8 + 8 - 8 + 8 - 8 + 8);
    rax14 = slashbase_1;
    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r12_12) + reinterpret_cast<unsigned char>(rax9) + 13) = rax14;
    eax15 = g152f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_11) + reinterpret_cast<unsigned char>(rax9) + 8) = eax15;
    rax16 = temp_dir_index_2;
    *reinterpret_cast<void***>(r12_12) = reinterpret_cast<void**>(0);
    rax17 = rax16 + 1;
    zf18 = rax17 == temp_dir_count;
    temp_dir_index_2 = rax17;
    if (zf18) {
        temp_dir_index_2 = 0;
    }
    r13_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 24);
    rdx20 = r13_19;
    eax21 = fun_3c00();
    *reinterpret_cast<int32_t*>(&rsi22) = 0x80000;
    *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&v23) = reinterpret_cast<uint1_t>(eax21 == 0);
    eax24 = mkostemp_safer(r15_11, 0x80000, rdx20);
    ebp25 = eax24;
    rax26 = fun_37d0();
    rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8 - 8 + 8);
    if (ebp25 < 0) {
        ebp28 = *reinterpret_cast<void***>(rax26);
        if (*reinterpret_cast<unsigned char*>(&v23)) {
            *reinterpret_cast<int32_t*>(&rdx20) = 0;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rsi22 = r13_19;
            fun_3c00();
            *reinterpret_cast<void***>(rax26) = ebp28;
        }
        if (!reinterpret_cast<int1_t>(ebp28 == 24)) 
            goto addr_8b30_7;
        if (*reinterpret_cast<signed char*>(&v5) == 1) 
            goto addr_8a7d_9;
    } else {
        rax29 = temptail;
        temptail = r12_12;
        *reinterpret_cast<void***>(rax29) = r12_12;
        if (*reinterpret_cast<unsigned char*>(&v23)) {
            r14_8 = *reinterpret_cast<void***>(rax26);
            *reinterpret_cast<int32_t*>(&r14_8 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx20) = 0;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            fun_3c00();
            rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
            *reinterpret_cast<void***>(rax26) = r14_8;
        }
        zf30 = compress_program == 0;
        *reinterpret_cast<unsigned char*>(r12_12 + 12) = 0;
        if (zf30) 
            goto addr_89f6_13;
        eax31 = pipe_fork(reinterpret_cast<int64_t>(rsp27) + 16, 4, rdx20);
        *reinterpret_cast<void***>(r12_12 + 8) = eax31;
        zf32 = reinterpret_cast<uint1_t>(eax31 == 0);
        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax31) < reinterpret_cast<signed char>(0)) | zf32)) 
            goto addr_8a40_15; else 
            goto addr_89f0_16;
    }
    addr_8b30_7:
    quotearg_style(4, r14_8, rdx20);
    fun_3920();
    fun_3c90();
    addr_8a7d_9:
    rdi33 = r12_12;
    *reinterpret_cast<int32_t*>(&r12_12) = 0;
    *reinterpret_cast<int32_t*>(&r12_12 + 4) = 0;
    fun_3750(rdi33, rsi22, rdi33, rsi22);
    goto addr_8a14_17;
    addr_8a40_15:
    fun_3a40();
    fun_3a40();
    ebp25 = v34;
    register_proc(r12_12, 4, rdx20);
    goto addr_89f6_13;
    addr_89f0_16:
    if (zf32) {
        fun_3a40();
        if (ebp25 != 1) {
            move_fd_part_0(ebp25, ebp25);
        }
        if (!0) {
            move_fd_part_0(v23);
        }
        fun_3e20();
        edi35 = *reinterpret_cast<void***>(rax26);
        async_safe_die(edi35, "couldn't execute compress program");
    } else {
        addr_89f6_13:
        *reinterpret_cast<int32_t*>(&rdi36) = ebp25;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi36) + 4) = 0;
        rax37 = fun_3c20(rdi36, "w", rdx20);
        *reinterpret_cast<void***>(rdi) = rax37;
        if (!rax37) {
            addr_8b12_23:
            *reinterpret_cast<int32_t*>(&rdx20) = 5;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rax38 = fun_3920();
            sort_die(rax38, r15_11, 5);
            goto addr_8b30_7;
        } else {
            addr_8a14_17:
            rax39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
            if (!rax39) {
                return r12_12;
            }
        }
    }
    fun_3950();
    goto addr_8b12_23;
}