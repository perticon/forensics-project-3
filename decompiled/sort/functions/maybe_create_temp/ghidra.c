undefined8 * maybe_create_temp(FILE **param_1,char param_2)

{
  void *__dest;
  char *__s;
  int iVar1;
  int iVar2;
  size_t __n;
  undefined8 *__ptr;
  int *piVar3;
  FILE *pFVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  undefined8 *puVar7;
  long in_FS_OFFSET;
  bool bVar8;
  uint local_c8;
  int local_c4;
  __sigset_t local_c0;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  __s = *(char **)(temp_dirs + temp_dir_index_2 * 8);
  __n = strlen(__s);
  __ptr = (undefined8 *)xmalloc(__n + 0x20 & 0xfffffffffffffff8);
  __dest = (void *)((long)__ptr + 0xd);
  memcpy(__dest,__s,__n);
  *(undefined8 *)((long)__ptr + __n + 0xd) = slashbase_1._0_8_;
  *(undefined4 *)((long)__ptr + __n + 0x15) = slashbase_1._8_4_;
  *__ptr = 0;
  temp_dir_index_2 = temp_dir_index_2 + 1;
  if (temp_dir_index_2 == temp_dir_count) {
    temp_dir_index_2 = 0;
  }
  iVar1 = pthread_sigmask(0,(__sigset_t *)caught_signals,&local_c0);
  local_c8 = local_c8 & 0xffffff00 | (uint)(iVar1 == 0);
  iVar1 = mkostemp_safer(__dest,0x80000);
  piVar3 = __errno_location();
  if (iVar1 < 0) {
    iVar1 = *piVar3;
    if ((char)local_c8 != '\0') {
      pthread_sigmask(2,&local_c0,(__sigset_t *)0x0);
      *piVar3 = iVar1;
    }
    if ((iVar1 != 0x18) || (param_2 != '\x01')) {
      uVar5 = quotearg_style(4,__s);
      uVar6 = dcgettext(0,"cannot create temporary file in %s",5);
                    /* WARNING: Subroutine does not return */
      error(2,*piVar3,uVar6,uVar5);
    }
    puVar7 = (undefined8 *)0x0;
    free(__ptr);
  }
  else {
    puVar7 = __ptr;
    *(undefined8 **)temptail = __ptr;
    temptail = (undefined *)puVar7;
    if ((char)local_c8 != '\0') {
      iVar2 = *piVar3;
      pthread_sigmask(2,&local_c0,(__sigset_t *)0x0);
      *piVar3 = iVar2;
    }
    bVar8 = compress_program != (char *)0x0;
    *(undefined *)((long)__ptr + 0xc) = 0;
    if (bVar8) {
      iVar2 = pipe_fork(&local_c8,4);
      *(int *)(__ptr + 1) = iVar2;
      if (iVar2 < 1) {
        if (iVar2 == 0) {
          close(local_c4);
          if (iVar1 != 1) {
            move_fd_part_0(iVar1,1);
          }
          if (local_c8 != 0) {
            move_fd_part_0(local_c8,0);
          }
          execlp(compress_program,compress_program,0);
          async_safe_die(*piVar3,"couldn\'t execute compress program");
          goto LAB_00108b0d;
        }
      }
      else {
        close(iVar1);
        close(local_c8);
        register_proc();
        iVar1 = local_c4;
      }
    }
    pFVar4 = fdopen(iVar1,"w");
    *param_1 = pFVar4;
    puVar7 = __ptr;
    if (pFVar4 == (FILE *)0x0) {
      uVar5 = dcgettext(0,"couldn\'t create temporary file",5);
                    /* WARNING: Subroutine does not return */
      sort_die(uVar5,__dest);
    }
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return puVar7;
  }
LAB_00108b0d:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}