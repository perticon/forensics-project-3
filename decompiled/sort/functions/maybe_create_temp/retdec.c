int32_t * maybe_create_temp(struct _IO_FILE ** pfp, bool survive_fd_exhaustion) {
    int64_t v1 = __readfsqword(40); // 0x88e0
    int64_t v2 = function_3940(); // 0x8901
    int64_t v3 = xmalloc(); // 0x8911
    int64_t v4 = v3 + 13; // 0x891c
    function_3b30();
    *(int64_t *)(v4 + v2) = 0x58585874726f732f;
    *(int32_t *)(v2 + 21 + v3) = 0x585858;
    *(int64_t *)v3 = 0;
    int64_t v5 = g47 + 1; // 0x8951
    g47 = v5 != temp_dir_count ? v5 : 0;
    int64_t v6 = function_3c00(); // 0x8981
    int32_t v7 = (int32_t)v6 == 0; // bp-200, 0x8990
    char * v8 = (char *)v4; // 0x8995
    int32_t v9 = mkostemp_safer(v8, 0x80000); // 0x8995
    int64_t v10 = function_37d0(); // 0x899c
    if (v9 < 0) {
        if ((int32_t)v6 == 0) {
            int32_t * v11 = (int32_t *)v10; // 0x8a65
            function_3c00();
        }
        // 0x8b30
        quotearg_style();
        function_3920();
        return (int32_t *)function_3c90();
    }
    // 0x89ac
    *(int64_t *)&temptail = v3;
    *(int64_t *)temptail = v3;
    if ((int32_t)v6 == 0) {
        int32_t * v12 = (int32_t *)v10; // 0x8a90
        function_3c00();
    }
    // 0x89c8
    *(char *)(v3 + 12) = 0;
    if (compress_program == 0) {
        goto lab_0x89f6;
    } else {
        int32_t v13 = pipe_fork(&v7, 4); // 0x89e2
        *(int32_t *)(v3 + 8) = v13;
        if (v13 >= 0 == (v13 != 0)) {
            // 0x8a40
            function_3a40();
            function_3a40();
            register_proc((int32_t *)v3);
            goto lab_0x89f6;
        } else {
            if (v13 == 0) {
                // 0x8ac3
                function_3a40();
                if (v9 != 1) {
                    // 0x8ad1
                    move_fd_part_0((int64_t)v9);
                }
                int32_t v14 = 0x1000000 * v7;
                if (v14 != 0) {
                    // 0x8ae5
                    move_fd_part_0((int64_t)(v14 >> 24));
                }
                // 0x8aec
                function_3e20();
                async_safe_die(*(int32_t *)v10, "couldn't execute compress program");
                // 0x8b0d
                function_3950();
                goto lab_0x8b12;
            } else {
                goto lab_0x89f6;
            }
        }
    }
  lab_0x89f6:;
    int64_t v15 = function_3c20(); // 0x89ff
    *(int64_t *)pfp = v15;
    if (v15 == 0) {
        goto lab_0x8b12;
    } else {
        // 0x8a14
        if (v1 == __readfsqword(40)) {
            // 0x8a2b
            return (int32_t *)v3;
        }
        // 0x8b0d
        function_3950();
        goto lab_0x8b12;
    }
  lab_0x8b12:
    // 0x8b12
    sort_die((char *)function_3920(), v8);
    // 0x8b30
    quotearg_style();
    function_3920();
    return (int32_t *)function_3c90();
}