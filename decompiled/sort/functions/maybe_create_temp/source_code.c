maybe_create_temp (FILE **pfp, bool survive_fd_exhaustion)
{
  int tempfd;
  struct tempnode *node = create_temp_file (&tempfd, survive_fd_exhaustion);
  if (! node)
    return NULL;

  node->state = UNCOMPRESSED;

  if (compress_program)
    {
      int pipefds[2];

      node->pid = pipe_fork (pipefds, MAX_FORK_TRIES_COMPRESS);
      if (0 < node->pid)
        {
          close (tempfd);
          close (pipefds[0]);
          tempfd = pipefds[1];

          register_proc (node);
        }
      else if (node->pid == 0)
        {
          /* Being the child of a multithreaded program before exec,
             we're restricted to calling async-signal-safe routines here.  */
          close (pipefds[1]);
          move_fd (tempfd, STDOUT_FILENO);
          move_fd (pipefds[0], STDIN_FILENO);

          execlp (compress_program, compress_program, (char *) NULL);

          async_safe_die (errno, "couldn't execute compress program");
        }
    }

  *pfp = fdopen (tempfd, "w");
  if (! *pfp)
    sort_die (_("couldn't create temporary file"), node->name);

  return node;
}