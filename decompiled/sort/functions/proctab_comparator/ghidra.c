uint proctab_comparator(long param_1,long param_2)

{
  return *(uint *)(param_2 + 8) & 0xffffff00 |
         (uint)(*(uint *)(param_1 + 8) == *(uint *)(param_2 + 8));
}