int32_t keycompare(int32_t * a, int32_t * b) {
    int64_t v1 = (int64_t)b;
    int64_t v2 = (int64_t)a;
    __readfsqword(40);
    int64_t v3; // bp-8584, 0x8e40
    int64_t v4 = &v3; // 0x8ebd
    int64_t v5 = *(int64_t *)(v1 + 16); // 0x8ebd
    int64_t v6 = *(int64_t *)(v2 + 24); // 0x8ebd
    int64_t v7 = *(int64_t *)(v2 + 16); // 0x8ebd
    int64_t v8 = *(int64_t *)(v1 + 24); // 0x8ebd
    int64_t v9 = (int64_t)keylist; // 0x8ebd
    int3_t v10; // 0x8e40
    int3_t v11; // 0x8e40
    int3_t v12; // 0x8e40
    int3_t v13; // 0x8e40
    int64_t v14; // 0x8e40
    int64_t v15; // 0x8e40
    int64_t v16; // 0x8e40
    int64_t v17; // 0x8e40
    int64_t v18; // 0x8e40
    uint64_t v19; // 0x8e40
    int64_t v20; // 0x8e40
    int64_t v21; // 0x8e40
    int64_t v22; // 0x8e40
    int64_t v23; // 0x8e40
    int64_t v24; // 0x8e40
    int64_t v25; // 0x8e40
    int64_t v26; // 0x8e40
    int64_t v27; // 0x8e40
    int64_t v28; // 0x8e40
    int64_t v29; // 0x8e40
    int64_t v30; // 0x8e40
    int64_t v31; // 0x8e40
    int64_t v32; // 0x8e40
    int64_t v33; // 0x8e40
    int64_t v34; // 0x8e40
    int64_t v35; // 0x8e40
    int64_t v36; // 0x8e40
    int64_t v37; // 0x8e40
    int64_t v38; // 0x8e40
    int64_t v39; // 0x8e40
    int64_t v40; // 0x8e40
    int64_t v41; // 0x8e40
    uint64_t v42; // 0x8e40
    int64_t v43; // 0x8e40
    int64_t v44; // 0x8e40
    int64_t v45; // 0x8e40
    uint64_t v46; // 0x8ecb
    int64_t v47; // 0x8ed2
    int64_t v48; // 0x8edc
    int64_t v49; // 0x8edf
    int64_t v50; // 0x8ec3
    int64_t v51; // 0x8ec7
    while (true) {
      lab_0x8ec0:
        // 0x8ec0
        v23 = v9;
        uint64_t v52 = v8;
        v16 = v7;
        uint64_t v53 = v6;
        v27 = v5;
        v43 = v4;
        v11 = v10;
        v50 = *(int64_t *)(v23 + 40);
        v51 = *(int64_t *)(v23 + 32);
        v46 = v16 >= v53 ? v16 : v53;
        v47 = v27 >= v52 ? v27 : v52;
        v48 = v46 - v16;
        v49 = v47 - v27;
        if (*(char *)&hard_LC_COLLATE != 0) {
            goto lab_0x8eff;
        } else {
            // 0x8eeb
            if ((*(int64_t *)(v23 + 48) & 0xffffffffff0000) == 0) {
                // 0x93a0
                if (*(char *)(v23 + 56) != 0) {
                    goto lab_0x8eff;
                } else {
                    if (v51 == 0) {
                        // 0x947f
                        v40 = v49;
                        v26 = v48;
                        if ((v48 > v49 ? v49 : v48) == 0) {
                            goto lab_0x9cae;
                        } else {
                            // 0x9492
                            if (v50 != 0) {
                                int64_t v54; // 0x8e40
                                int64_t v55 = v54;
                                unsigned char v56 = *(char *)(v55 + v16); // 0x9a36
                                unsigned char v57 = *(char *)(v50 + (int64_t)v56); // 0x9a3c
                                unsigned char v58 = *(char *)(v55 + v27); // 0x9a41
                                unsigned char v59 = *(char *)(v50 + (int64_t)v58); // 0x9a46
                                uint32_t v60 = (int32_t)v57 - (int32_t)v59; // 0x9a4a
                                while (v60 == 0) {
                                    int64_t v61 = v55 + 1; // 0x9a29
                                    v54 = v61;
                                    v40 = v49;
                                    v26 = v48;
                                    int64_t v62; // 0x9485
                                    if (v62 == v61) {
                                        goto lab_0x9cae;
                                    }
                                    v55 = v54;
                                    v56 = *(char *)(v55 + v16);
                                    v57 = *(char *)(v50 + (int64_t)v56);
                                    v58 = *(char *)(v55 + v27);
                                    v59 = *(char *)(v50 + (int64_t)v58);
                                    v60 = (int32_t)v57 - (int32_t)v59;
                                }
                                // 0x94d0
                                v13 = v11;
                                v39 = 0;
                                v45 = v43;
                                v31 = v27;
                                v15 = v60;
                                v17 = v16;
                                v22 = v47;
                                v25 = v23;
                                goto lab_0x94d0_3;
                            } else {
                                int64_t * v63 = (int64_t *)(v43 + 16); // 0x94a4
                                *v63 = v48;
                                int64_t * v64 = (int64_t *)v43; // 0x94a9
                                *v64 = v49;
                                int64_t v65 = function_3a80(); // 0x94ad
                                v13 = v11;
                                v39 = 0;
                                v45 = v43;
                                v31 = v27;
                                v15 = v65 & 0xffffffff;
                                v17 = v16;
                                v22 = v47;
                                v25 = v23;
                                v40 = *v64;
                                v26 = *v63;
                                if ((int32_t)v65 == 0) {
                                    goto lab_0x9cae;
                                } else {
                                    goto lab_0x94d0_3;
                                }
                            }
                        }
                    } else {
                        // 0x93b4
                        v41 = v16;
                        v28 = v27;
                        v33 = v27;
                        v18 = v16;
                        if (v50 == 0) {
                            while (true) {
                              lab_0x9540:
                                // 0x9540
                                v19 = v18;
                                v34 = v33;
                                if (v19 >= v46) {
                                    goto lab_0x9550;
                                } else {
                                    // 0x9545
                                    v32 = v34;
                                    if (*(char *)(v51 + (int64_t)*(char *)v19) != 0) {
                                        goto lab_0x953c;
                                    } else {
                                        goto lab_0x9550;
                                    }
                                }
                            }
                          lab_0x9d28_3:
                            // 0x9d28
                            v12 = v11;
                            v38 = v51;
                            v44 = v43;
                            v37 = v36;
                            v14 = (int64_t)(v19 < v46) - (int64_t)(v36 < v47) & 0xffffffff;
                            v20 = v19;
                            v21 = v47;
                            v24 = v23;
                        } else {
                            while (true) {
                              lab_0x93c0:
                                // 0x93c0
                                v29 = v28;
                                v42 = v41;
                                if (v42 >= v46) {
                                    goto lab_0x93ce;
                                } else {
                                    // 0x93c5
                                    v30 = v29;
                                    if (*(char *)(v51 + (int64_t)*(char *)v42) != 0) {
                                        goto lab_0x9422;
                                    } else {
                                        goto lab_0x93ce;
                                    }
                                }
                            }
                          lab_0x9c98_3:;
                            int64_t v66 = (int64_t)(v42 < v46) - (int64_t)(v35 < v47) & 0xffffffff; // 0x9ca3
                            v12 = v11;
                            v38 = v66;
                            v44 = v43;
                            v37 = v35;
                            v14 = v66;
                            v20 = v16;
                            v21 = v47;
                            v24 = v23;
                        }
                        goto lab_0x9016;
                    }
                }
            } else {
                goto lab_0x8eff;
            }
        }
    }
  lab_0x9501:;
    // 0x9501
    int64_t result; // 0x8e40
    return result;
  lab_0x92cc:;
    // 0x92cc
    unsigned char v67; // 0x92b8
    char v68 = v67; // 0x92cf
    int64_t v69; // 0x8e40
    int64_t v70; // 0x92b8
    if (v69 != 0) {
        // 0x92d1
        v68 = *(char *)(v69 + v70);
    }
    // 0x92d5
    int64_t v71; // 0x8e40
    int64_t v72; // 0x8e40
    *(char *)(v71 + v72) = v68;
    int64_t v73 = v71 + 1; // 0x92d9
    goto lab_0x92dd;
  lab_0x92dd:;
    int64_t v74 = v73;
    int64_t v75; // 0x8e40
    int64_t v76 = v75 + 1; // 0x92dd
    int64_t v77 = v74; // 0x92e4
    v75 = v76;
    int64_t v78; // 0x8e40
    if (v78 + v27 == v76) {
        // break -> 0x92e6
        goto lab_0x92e6;
    }
    goto lab_0x92b8;
  lab_0x9285:;
    // 0x9285
    unsigned char v79; // 0x9270
    char v80 = v79; // 0x9288
    int64_t v81; // 0x8e40
    int64_t v82; // 0x9270
    if (v81 != 0) {
        // 0x928a
        v80 = *(char *)(v81 + v82);
    }
    // 0x928e
    int64_t v83; // 0x8e40
    int64_t v84; // 0x8e40
    *(char *)(v83 + v84) = v80;
    int64_t v85 = v83 + 1; // 0x9292
    goto lab_0x9296;
  lab_0x9296:;
    int64_t v86 = v85;
    int64_t v87; // 0x8e40
    int64_t v88 = v87 + 1; // 0x9296
    int64_t v89 = v86; // 0x929d
    int64_t v90 = v88; // 0x929d
    int64_t v91; // 0x9260
    if (v91 == v88) {
        // break -> 0x929f
        goto lab_0x929f;
    }
    goto lab_0x9270;
  lab_0x9550:;
    int64_t v92 = v34; // 0x9553
    v36 = v34;
    if (v34 >= v47) {
        // break -> 0x9d28
        goto lab_0x9d28_3;
    }
    int64_t v93 = v92;
    unsigned char v94 = *(char *)v93; // 0x956d
    while (*(char *)(v51 + (int64_t)v94) != 0) {
        int64_t v95 = v93 + 1; // 0x9560
        v92 = v95;
        v36 = v47;
        if (v47 == v95) {
            // break (via goto) -> 0x9d28
            goto lab_0x9d28_3;
        }
        v93 = v92;
        v94 = *(char *)v93;
    }
    // 0x9577
    v36 = v93;
    if (v46 <= v19 || v93 >= v47) {
        // break -> 0x9d28
        goto lab_0x9d28_3;
    }
    uint32_t v96 = (int32_t)*(char *)v19 - (int32_t)v94; // 0x9533
    if (v96 != 0) {
        // 0x94d0
        v13 = v11;
        v39 = v51;
        v45 = v43;
        v31 = v93;
        v15 = v96;
        v17 = v19;
        v22 = v47;
        v25 = v23;
        goto lab_0x94d0_3;
    }
    // 0x9538
    v32 = v93 + 1;
    goto lab_0x953c;
  lab_0x953c:
    // 0x953c
    v33 = v32;
    v18 = v19 + 1;
    goto lab_0x9540;
  lab_0x93ce:
    // 0x93ce
    if (v29 >= v47) {
        // break -> 0x9c98
        goto lab_0x9c98_3;
    }
    int64_t v97 = v29;
    int64_t v98 = (int64_t)*(char *)v97; // 0x93ed
    while (*(char *)(v51 + v98) != 0) {
        int64_t v99 = v97 + 1; // 0x93e0
        int64_t v100 = v99; // 0x93e7
        v35 = v47;
        if (v47 == v99) {
            // break (via goto) -> 0x9c98
            goto lab_0x9c98_3;
        }
        v97 = v100;
        v98 = (int64_t)*(char *)v97;
    }
    // 0x93f7
    v35 = v97;
    if (v42 < v46 != v47 > v97) {
        // break -> 0x9c98
        goto lab_0x9c98_3;
    }
    unsigned char v101 = *(char *)(v50 + (int64_t)*(char *)v42); // 0x9410
    uint32_t v102 = (int32_t)v101 - (int32_t)*(char *)(v50 + v98); // 0x9415
    if (v102 != 0) {
        // 0x94d0
        v13 = v11;
        v39 = v51;
        v45 = v43;
        v31 = v97;
        v15 = v102;
        v17 = v16;
        v22 = v47;
        v25 = v23;
        goto lab_0x94d0_3;
    }
    // 0x941e
    v30 = v97 + 1;
    goto lab_0x9422;
  lab_0x9422:
    // 0x9422
    v41 = v42 + 1;
    v28 = v30;
    goto lab_0x93c0;
  lab_0x9cae:;
    uint64_t v103 = v26;
    uint64_t v104 = v40;
    int64_t v105 = (int64_t)(v103 > v104) - (int64_t)(v103 < v104) & 0xffffffff; // 0x9cb6
    v12 = v11;
    v38 = v105;
    v44 = v43;
    v37 = v27;
    v14 = v105;
    v20 = v16;
    v21 = v47;
    v24 = v23;
    goto lab_0x9016;
  lab_0x8eff:
    // 0x8eff
    *(char *)(v43 + 16) = *(char *)v46;
    *(char *)(v43 + 99) = *(char *)v47;
    int64_t v381 = v51 | v50; // 0x8f12
    int64_t * v382 = (int64_t *)v43; // 0x8f15
    *v382 = v381;
    int64_t v383 = v49; // 0x8f19
    int64_t v384 = v27; // 0x8f19
    int64_t v385 = v48; // 0x8f19
    int64_t v386 = v46; // 0x8f19
    int64_t v387 = v16; // 0x8f19
    int64_t v388 = v47; // 0x8f19
    int64_t v389; // 0x8e40
    int64_t v390; // 0x8e40
    int64_t v391; // 0x8e40
    int64_t v392; // 0x8e40
    int64_t v393; // 0x8e40
    int64_t v394; // 0x8e40
    int64_t v395; // 0x8e40
    int64_t v396; // 0x8e40
    int64_t v397; // 0x8e40
    int64_t v398; // 0x8e40
    if (v381 != 0) {
        // 0x9230
        *v382 = 0;
        if (v48 + 2 + v49 > (int64_t)&g107) {
            int64_t * v399 = (int64_t *)(v43 + 64); // 0x9430
            *v399 = v48;
            int64_t * v400 = (int64_t *)(v43 + 48); // 0x9435
            *v400 = v49;
            int64_t * v401 = (int64_t *)(v43 + 32); // 0x943a
            *v401 = v50;
            int64_t v402 = xmalloc(); // 0x943f
            int64_t v403 = *v399; // 0x9444
            int64_t v404 = *v400; // 0x9449
            *v382 = v402;
            int64_t v405 = *v401; // 0x9452
            int64_t v406 = v402 + 1 + v403; // 0x9457
            v391 = v402;
            v393 = v404;
            v395 = v405;
            v397 = v406;
            v389 = v403;
            v392 = v402;
            v394 = v404;
            v396 = v405;
            v398 = v406;
            v390 = v403;
            if (v403 != 0) {
                goto lab_0x9260;
            } else {
                goto lab_0x9465;
            }
        } else {
            int64_t v407 = v43 + 512; // 0x923d
            int64_t v408 = v43 + 513 + v48; // 0x9252
            v391 = v407;
            v393 = v49;
            v395 = v50;
            v397 = v408;
            v389 = v48;
            v392 = v407;
            v394 = v49;
            v396 = v50;
            v398 = v408;
            v390 = 0;
            if (v48 == 0) {
                goto lab_0x9465;
            } else {
                goto lab_0x9260;
            }
        }
    } else {
        goto lab_0x8f1f;
    }
  lab_0x8f1f:;
    int64_t v336 = v388;
    int64_t v334 = v387;
    int64_t v409 = v386;
    int64_t v410 = v385;
    int64_t v380 = v384;
    int64_t v411 = v383;
    *(char *)v409 = 0;
    *(char *)v336 = 0;
    int64_t v412 = v334; // 0x8f2c
    char * v413; // 0x8e40
    int64_t v298; // 0x8e40
    int64_t * v297; // 0x8e40
    char * v414; // 0x8e40
    int3_t v299; // 0x8e40
    int3_t v327; // 0x8e40
    int3_t v272; // 0x8e40
    int64_t v415; // 0x8e40
    int64_t v331; // 0x8e40
    int64_t v303; // 0x8e40
    int64_t v332; // 0x8e40
    int64_t v304; // 0x8e40
    int64_t v276; // 0x8e40
    int64_t v305; // 0x8e40
    int64_t v416; // 0x8e40
    int64_t v333; // 0x8e40
    int64_t v306; // 0x8e40
    int64_t v335; // 0x8e40
    int64_t v337; // 0x8e40
    int64_t v307; // 0x8e40
    int64_t v275; // 0x8e40
    int64_t v302; // 0x8e40
    int64_t v417; // 0x8e40
    int64_t v330; // 0x8e40
    int64_t v418; // 0x8e40
    int64_t v300; // 0x8e40
    int64_t v328; // 0x8e40
    int64_t v273; // 0x8e40
    int64_t v301; // 0x8e40
    int64_t v329; // 0x8e40
    int64_t v274; // 0x8e40
    int64_t v321; // 0x9321
    int3_t v312; // 0x9342
    if (*(char *)(v23 + 50) != 0) {
        int64_t v419 = v412;
        char * v420 = (char *)v419;
        v412 = v419 + 1;
        while (*(char *)((int64_t)*v420 + (int64_t)&blanks) != 0) {
            // 0x8f8c
            v419 = v412;
            v420 = (char *)v419;
            v412 = v419 + 1;
        }
        char * v421 = (char *)v380;
        int64_t v422 = v380; // 0x8fa1
        v413 = v421;
        v414 = v420;
        v418 = v51;
        v417 = v380;
        v415 = v409;
        v416 = v419;
        if (*(char *)((int64_t)*v421 + (int64_t)&blanks) != 0) {
            int64_t v423 = v422 + 1; // 0x8fa8
            char * v424 = (char *)v423;
            v422 = v423;
            v413 = v424;
            v414 = v420;
            v418 = v51;
            v417 = v423;
            v415 = v409;
            v416 = v419;
            while (*(char *)((int64_t)*v424 + (int64_t)&blanks) != 0) {
                // 0x8fa8
                v423 = v422 + 1;
                v424 = (char *)v423;
                v422 = v423;
                v413 = v424;
                v414 = v420;
                v418 = v51;
                v417 = v423;
                v415 = v409;
                v416 = v419;
            }
        }
        goto lab_0x8fb7;
    } else {
        // 0x8f2e
        if (*(char *)(v23 + 52) != 0) {
            int64_t * v425 = (int64_t *)(v43 + 48); // 0x9303
            *v425 = v409;
            function_3c80();
            float80_t v426 = __frontend_reg_load_fpr(v11); // 0x9318
            float80_t * v427 = (float80_t *)(v43 + 32); // 0x9318
            *v427 = v426;
            function_3c80();
            v321 = *v425;
            __frontend_reg_store_fpr(v11, *v427);
            if (v334 == *(int64_t *)(v43 + 144)) {
                // 0x9a0e
                __frontend_reg_store_fpr(v11, __frontend_reg_load_fpr(v11));
                int3_t v428 = v11 + 1; // 0x9a0e
                __frontend_reg_store_fpr(v428, __frontend_reg_load_fpr(v428));
                int64_t v429 = *(int64_t *)(v43 + 152) != v380 ? 0xffffffff : 0; // 0x9a1f
                v327 = v11 + 2;
                v328 = v429;
                v329 = v43;
                v330 = v380;
                v331 = v321;
                v332 = v429;
                v333 = v334;
                v335 = v336;
                v337 = v23;
                goto lab_0x8ff0;
            } else {
                // 0x9338
                if (v380 == *(int64_t *)(v43 + 152)) {
                    // 0x938a
                    __frontend_reg_store_fpr(v11, __frontend_reg_load_fpr(v11));
                    int3_t v430 = v11 + 1; // 0x938a
                    __frontend_reg_store_fpr(v430, __frontend_reg_load_fpr(v430));
                    goto lab_0x938e;
                } else {
                    // 0x9342
                    v312 = v11 + 1;
                    float80_t v431 = __frontend_reg_load_fpr(v11); // 0x9342
                    __frontend_reg_store_fpr(v11, __frontend_reg_load_fpr(v312));
                    __frontend_reg_store_fpr(v312, v431);
                    float80_t v432 = __frontend_reg_load_fpr(v11); // 0x9344
                    float80_t v433 = __frontend_reg_load_fpr(v312); // 0x9344
                    float80_t v434 = __frontend_reg_load_fpr(v11);
                    if (v432 <= v433) {
                        // 0x934c
                        __frontend_reg_store_fpr(v11, __frontend_reg_load_fpr(v312));
                        __frontend_reg_store_fpr(v312, v434);
                        float80_t v435 = __frontend_reg_load_fpr(v11); // 0x934e
                        float80_t v436 = __frontend_reg_load_fpr(v312); // 0x934e
                        float80_t v437 = __frontend_reg_load_fpr(v11);
                        if (v435 <= v436) {
                            float80_t v438 = __frontend_reg_load_fpr(v312); // 0x935c
                            float80_t v439 = __frontend_reg_load_fpr(v11);
                            if (v437 != v438 == v437 == v438) {
                                // 0x9370
                                __frontend_reg_store_fpr(v11, __frontend_reg_load_fpr(v312));
                                __frontend_reg_store_fpr(v312, v439);
                                goto lab_0x9372;
                            } else {
                                if (v437 != v438) {
                                    // 0x936c
                                    __frontend_reg_store_fpr(v11, __frontend_reg_load_fpr(v312));
                                    __frontend_reg_store_fpr(v312, v439);
                                    goto lab_0x9372;
                                } else {
                                    // 0x8fe6
                                    __frontend_reg_store_fpr(v11, v439);
                                    __frontend_reg_store_fpr(v312, __frontend_reg_load_fpr(v312));
                                    v327 = v11 + 2;
                                    v328 = v51;
                                    v329 = v43;
                                    v330 = v380;
                                    v331 = v321;
                                    v332 = 0;
                                    v333 = v334;
                                    v335 = v336;
                                    v337 = v23;
                                    goto lab_0x8ff0;
                                }
                            }
                        } else {
                            // 0x8fe0
                            __frontend_reg_store_fpr(v11, v437);
                            __frontend_reg_store_fpr(v312, __frontend_reg_load_fpr(v312));
                            v327 = v11 + 2;
                            v328 = v51;
                            v329 = v43;
                            v330 = v380;
                            v331 = v321;
                            v332 = 1;
                            v333 = v334;
                            v335 = v336;
                            v337 = v23;
                            goto lab_0x8ff0;
                        }
                    } else {
                        // 0x9d3e
                        __frontend_reg_store_fpr(v11, v434);
                        __frontend_reg_store_fpr(v312, __frontend_reg_load_fpr(v312));
                        goto lab_0x9d48;
                    }
                }
            }
        } else {
            int64_t v440 = v334; // 0x8f3e
            if (*(char *)(v23 + 53) != 0) {
                int64_t v441 = v440;
                char * v442 = (char *)v441;
                unsigned char v443 = *v442; // 0x90ac
                v440 = v441 + 1;
                while (*(char *)((int64_t)v443 + (int64_t)&blanks) != 0) {
                    // 0x90ac
                    v441 = v440;
                    v442 = (char *)v441;
                    v443 = *v442;
                    v440 = v441 + 1;
                }
                int64_t v444 = (int64_t)*(char *)v380; // 0x90bb
                int64_t v445 = v380; // 0x90c7
                if (*(char *)(v444 + (int64_t)&blanks) != 0) {
                    int64_t v446 = v445 + 1; // 0x90d0
                    int64_t v447 = (int64_t)*(char *)v446; // 0x90d0
                    v445 = v446;
                    while (*(char *)(v447 + (int64_t)&blanks) != 0) {
                        // 0x90d0
                        v446 = v445 + 1;
                        v447 = (int64_t)*(char *)v446;
                        v445 = v446;
                    }
                }
                int64_t v448 = v380;
                int64_t v449 = v444;
                int64_t v450 = v43 + 152; // 0x90e5
                int64_t * v451 = (int64_t *)(v43 + 48); // 0x90ed
                *v451 = v409;
                int64_t * v452 = (int64_t *)(v43 + 32); // 0x90f5
                *v452 = v450;
                char * v453 = (char *)(v43 + 64); // 0x9100
                *v453 = v443;
                int64_t * v454 = (int64_t *)v450; // 0x9107
                *v454 = v441 + (int64_t)(v443 == 45);
                int64_t v455 = 0; // 0x9120
                if (traverse_raw_number((char **)v450) >= 49) {
                    unsigned char v456 = *(char *)*v454; // 0x9136
                    char v457 = *(char *)((int64_t)v456 + (int64_t)"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x06\x00\x03\x00\x00\x00\x01\x00\x02\x00\x00\x05\x00\x00\x00\x04\x00\x00\x00\x00\b\a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"); // 0x9139
                    int64_t v458 = v457; // 0x913e
                    v455 = (*v453 == 45 ? -v458 : v458) & 0xffffffff;
                }
                int64_t v459 = v455;
                *v452 = *v451;
                *v454 = v448 + (int64_t)(v449 == 45);
                unsigned char v460 = traverse_raw_number((char **)*v452); // 0x9162
                int64_t v461 = *v452; // 0x9167
                int64_t v462 = v459; // 0x916e
                if (v460 >= 49) {
                    unsigned char v463 = *(char *)*v454; // 0x917f
                    char v464 = *(char *)((int64_t)v463 + (int64_t)"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x06\x00\x03\x00\x00\x00\x01\x00\x02\x00\x00\x05\x00\x00\x00\x04\x00\x00\x00\x00\b\a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"); // 0x9182
                    int64_t v465 = v464; // 0x9189
                    v462 = (v449 == 45 ? v465 : -v465) + v459 & 0xffffffff;
                }
                // 0x9197
                v327 = v11;
                v328 = v449;
                v329 = v43;
                v330 = v448;
                v331 = v461;
                v332 = v462;
                v333 = v441;
                v335 = v336;
                v337 = v23;
                if ((int32_t)v462 != 0) {
                    goto lab_0x8ff0;
                } else {
                    // 0x9197
                    v413 = (char *)v448;
                    v414 = v442;
                    v418 = v449;
                    v417 = v448;
                    v415 = v461;
                    v416 = v441;
                    goto lab_0x8fb7;
                }
            } else {
                // 0x8f44
                if (*(char *)(v23 + 54) != 0) {
                    int64_t * v466 = (int64_t *)(v43 + 32); // 0x95c5
                    *v466 = v409;
                    int32_t v467 = getmonth((char *)v334, NULL); // 0x95ca
                    int64_t v468 = v467 - getmonth((char *)v380, NULL); // 0x95e0
                    v327 = v11;
                    v328 = v468;
                    v329 = v43;
                    v330 = v380;
                    v331 = *v466;
                    v332 = v468;
                    v333 = v334;
                    v335 = v336;
                    v337 = v23;
                    goto lab_0x8ff0;
                } else {
                    // 0x8f4f
                    if (*(char *)(v23 + 51) != 0) {
                        int128_t v469 = __asm_movdqa(*(int128_t *)&random_md5_state); // 0x95f8
                        int128_t v470 = __asm_movdqa(g52); // 0x9601
                        int128_t v471 = __asm_movdqa(g53); // 0x9609
                        int128_t v472 = __asm_movdqa(g54); // 0x9611
                        int128_t v473 = __asm_movdqa(g55); // 0x9619
                        int128_t v474 = __asm_movdqa(g56); // 0x9621
                        int64_t v475 = v43 + 348; // 0x9629
                        __asm_movups(*(int128_t *)v475, v469);
                        int128_t v476 = __asm_movdqa(g57); // 0x9632
                        int128_t v477 = __asm_movdqa(g58); // 0x963a
                        __asm_movups(*(int128_t *)(v43 + 364), v470);
                        int128_t v478 = __asm_movdqa(g59); // 0x964a
                        __asm_movups(*(int128_t *)(v43 + 380), v471);
                        __asm_movups(*(int128_t *)(v43 + 396), v472);
                        *(int64_t *)(v43 + 492) = g60;
                        *(int32_t *)(v43 + 500) = g61;
                        *(int64_t *)(v43 + 336) = g60;
                        __asm_movups(*(int128_t *)(v43 + 412), v473);
                        __asm_movups(*(int128_t *)(v43 + 428), v474);
                        __asm_movups(*(int128_t *)(v43 + 444), v476);
                        __asm_movups(*(int128_t *)(v43 + 460), v477);
                        __asm_movups(*(int128_t *)(v43 + 476), v478);
                        int64_t v479 = __asm_movaps(v469); // 0x96ae
                        int64_t v480 = v43 + 192; // 0x96ae
                        *(int128_t *)v480 = (int128_t)v479;
                        *(int128_t *)(v43 + 208) = (int128_t)__asm_movaps(v470);
                        *(int128_t *)(v43 + 224) = (int128_t)__asm_movaps(v471);
                        *(int128_t *)(v43 + 240) = (int128_t)__asm_movaps(v472);
                        *(int128_t *)(v43 + 256) = (int128_t)__asm_movaps(v473);
                        *(int128_t *)(v43 + 272) = (int128_t)__asm_movaps(v474);
                        *(int128_t *)(v43 + 288) = (int128_t)__asm_movaps(v476);
                        *(int128_t *)(v43 + 304) = (int128_t)__asm_movaps(v477);
                        *(int128_t *)(v43 + 320) = (int128_t)__asm_movaps(v478);
                        *(int32_t *)(v43 + 344) = g61;
                        if (*(char *)&hard_LC_COLLATE == 0) {
                            int64_t * v481 = (int64_t *)(v43 + 72);
                            *v481 = v409;
                            int64_t v482 = v43 + 64;
                            int64_t * v483 = (int64_t *)v482;
                            *v483 = v410;
                            int64_t v484 = v43 + 160; // 0x9a7a
                            int64_t * v485 = (int64_t *)(v43 + 32); // 0x9a82
                            *v485 = v411;
                            int64_t * v486 = (int64_t *)(v43 + 120); // 0x9a87
                            *v486 = v480;
                            md5_process_bytes((int32_t *)v334, v410, (int32_t *)v480);
                            md5_finish_ctx((int32_t *)*v486, (char *)v484);
                            int64_t v487 = *v485; // 0x9a9e
                            int64_t * v488 = (int64_t *)(v43 + 112); // 0x9ab1
                            *v488 = v475;
                            int64_t * v489 = (int64_t *)(v43 + 48);
                            *v489 = v487;
                            md5_process_bytes((int32_t *)v380, v487, (int32_t *)v475);
                            int64_t v490 = v43 + 176; // 0x9ac8
                            *v485 = v490;
                            md5_finish_ctx((int32_t *)*v488, (char *)v490);
                            int64_t v491 = function_3a80(); // 0x9ae7
                            *v485 = 0;
                            int64_t v492 = *v481; // 0x9b01
                            v297 = v489;
                            v298 = v482;
                            v299 = v11;
                            v300 = v484;
                            v301 = v43;
                            v302 = v380;
                            v303 = v492;
                            v304 = v491 & 0xffffffff;
                            v305 = v334;
                            v306 = v336;
                            v307 = v23;
                            int64_t * v493 = v483; // 0x9b09
                            int64_t v494 = v482; // 0x9b09
                            int64_t * v495 = v481; // 0x9b09
                            int64_t * v496 = v489; // 0x9b09
                            int3_t v497 = v11; // 0x9b09
                            int64_t v498 = *v489; // 0x9b09
                            int64_t v499 = v484; // 0x9b09
                            int64_t v500 = v43; // 0x9b09
                            int64_t v501 = v380; // 0x9b09
                            int64_t v502 = *v483; // 0x9b09
                            int64_t v503 = v492; // 0x9b09
                            int64_t v504 = v334; // 0x9b09
                            int64_t v505 = v336; // 0x9b09
                            int64_t v506 = v23; // 0x9b09
                            if ((int32_t)v491 != 0) {
                                goto lab_0x99eb;
                            } else {
                                goto lab_0x9b0f;
                            }
                        } else {
                            int64_t v507 = v410 + v411; // 0x970b
                            *(int64_t *)(v43 + 64) = v409;
                            *(int64_t *)(v43 + 136) = v23;
                            *(int64_t *)(v43 + 104) = v507 + 2 + 2 * v507;
                            *(int64_t *)(v43 + 32) = 0;
                            *(int64_t *)(v43 + 48) = *(int64_t *)(v43 + 128);
                            *(int64_t *)(v43 + 120) = v480;
                            *(int64_t *)(v43 + 112) = v475;
                            *(int64_t *)(v43 + 80) = v336;
                            *(int32_t *)(v43 + 100) = 0;
                            v272 = v11;
                            v273 = &g107;
                            v274 = v43;
                            v275 = v380;
                            v276 = v334;
                            goto lab_0x9770;
                        }
                    } else {
                        // 0x8f5a
                        if (*(char *)(v23 + 56) == 0) {
                            if (v410 == 0) {
                                // 0x9a54
                                v327 = v11;
                                v328 = v51;
                                v329 = v43;
                                v330 = v380;
                                v331 = v409;
                                v332 = v411 == 0 ? 0 : 0xffffffff;
                                v333 = v334;
                                v335 = v336;
                                v337 = v23;
                            } else {
                                // 0x9582
                                v327 = v11;
                                v328 = v51;
                                v329 = v43;
                                v330 = v380;
                                v331 = v409;
                                v332 = 1;
                                v333 = v334;
                                v335 = v336;
                                v337 = v23;
                                if (v411 != 0) {
                                    int64_t * v508 = (int64_t *)(v43 + 32); // 0x959f
                                    *v508 = v409;
                                    uint32_t v509 = xmemcoll0((char *)v334, v410 + 1, (char *)v380, v411 + 1); // 0x95a4
                                    v327 = v11;
                                    v328 = v51;
                                    v329 = v43;
                                    v330 = v380;
                                    v331 = *v508;
                                    v332 = v509;
                                    v333 = v334;
                                    v335 = v336;
                                    v337 = v23;
                                }
                            }
                        } else {
                            int64_t * v510 = (int64_t *)(v43 + 32); // 0x8f6e
                            *v510 = v409;
                            uint32_t v511 = filenvercmp((char *)v334, v410, (char *)v380, v411); // 0x8f73
                            v327 = v11;
                            v328 = v51;
                            v329 = v43;
                            v330 = v380;
                            v331 = *v510;
                            v332 = v511;
                            v333 = v334;
                            v335 = v336;
                            v337 = v23;
                        }
                        goto lab_0x8ff0;
                    }
                }
            }
        }
    }
  lab_0x9260:;
    int64_t v512 = v397;
    v81 = v395;
    int64_t v513 = v393;
    v84 = v391;
    v91 = v389 + v16;
    v89 = 0;
    v90 = v16;
    while (true) {
      lab_0x9270:
        // 0x9270
        v87 = v90;
        v83 = v89;
        v79 = *(char *)v87;
        v82 = v79;
        if (v51 == 0) {
            goto lab_0x9285;
        } else {
            // 0x927a
            v85 = v83;
            if (*(char *)(v51 + v82) != 0) {
                goto lab_0x9296;
            } else {
                goto lab_0x9285;
            }
        }
    }
  lab_0x929f:;
    int64_t v514 = v86 + v84; // 0x929f
    v383 = 0;
    v384 = v512;
    v385 = v86;
    v386 = v514;
    v387 = v84;
    v388 = v512;
    int64_t v515 = v84; // 0x92a6
    v78 = v513;
    int64_t v516 = v81; // 0x92a6
    int64_t v517 = v512; // 0x92a6
    int64_t v518 = v86; // 0x92a6
    int64_t v519 = v514; // 0x92a6
    if (v513 == 0) {
        goto lab_0x8f1f;
    } else {
        goto lab_0x92ac;
    }
  lab_0x9465:
    // 0x9465
    v383 = v394;
    v384 = v398;
    v385 = v390;
    v386 = v392;
    v387 = v392;
    v388 = v398;
    v515 = v392;
    v78 = v394;
    v516 = v396;
    v517 = v398;
    v518 = v390;
    v519 = v392;
    if (v394 != 0) {
        goto lab_0x92ac;
    } else {
        goto lab_0x8f1f;
    }
  lab_0x8fb7:;
    int64_t * v520 = (int64_t *)(v43 + 32); // 0x8fca
    *v520 = v415;
    uint32_t v521 = strnumcmp(v414, v413, (int32_t)decimal_point, thousands_sep); // 0x8fcf
    v327 = v11;
    v328 = v418;
    v329 = v43;
    v330 = v417;
    v331 = *v520;
    v332 = v521;
    v333 = v416;
    v335 = v336;
    v337 = v23;
    goto lab_0x8ff0;
  lab_0x92ac:;
    int64_t v522 = v519;
    int64_t v523 = v518;
    v72 = v517;
    v69 = v516;
    int64_t v524 = v515;
    v77 = 0;
    v75 = v27;
    while (true) {
      lab_0x92b8:
        // 0x92b8
        v71 = v77;
        v67 = *(char *)v75;
        v70 = v67;
        if (v51 == 0) {
            goto lab_0x92cc;
        } else {
            // 0x92c1
            v73 = v71;
            if (*(char *)(v51 + v70) != 0) {
                goto lab_0x92dd;
            } else {
                goto lab_0x92cc;
            }
        }
    }
  lab_0x92e6:
    // 0x92e6
    v383 = v74;
    v384 = v72;
    v385 = v523;
    v386 = v522;
    v387 = v524;
    v388 = v74 + v72;
    goto lab_0x8f1f;
  lab_0x8ff0:
    // 0x8ff0
    *(char *)v331 = *(char *)(v329 + 16);
    *(char *)v335 = *(char *)(v329 + 99);
    v12 = v327;
    v38 = v328;
    v44 = v329;
    v37 = v330;
    v14 = v332;
    v20 = v333;
    v21 = v335;
    v24 = v337;
    if (*(int64_t *)v329 != 0) {
        // 0x9009
        int64_t v355; // 0x8e40
        int32_t * v356 = (int32_t *)v355; // 0x9009
        int64_t v357; // 0x8e40
        *v356 = (int32_t)v357;
        function_3750();
        uint32_t v358 = *v356; // 0x9012
        int3_t v359; // 0x8e40
        v12 = v359;
        int64_t v360; // 0x8e40
        v38 = v360;
        v44 = v355;
        int64_t v361; // 0x8e40
        v37 = v361;
        v14 = v358;
        int64_t v362; // 0x8e40
        v20 = v362;
        int64_t v363; // 0x8e40
        v21 = v363;
        int64_t v364; // 0x8e40
        v24 = v364;
    }
    goto lab_0x9016;
  lab_0x9016:;
    int64_t v106 = v24;
    int64_t v107 = v21;
    int64_t v108 = v20;
    int64_t v109 = v14;
    int64_t v110 = v37;
    v4 = v44;
    int64_t v111 = v38;
    int3_t v112 = v12;
    v13 = v112;
    v39 = v111;
    v45 = v4;
    v31 = v110;
    v15 = v109;
    v17 = v108;
    v22 = v107;
    v25 = v106;
    int3_t v113; // 0x8e40
    int64_t v114; // 0x8e40
    int64_t v115; // 0x8e40
    int64_t v116; // 0x8e40
    int64_t v117; // 0x8e40
    int64_t v118; // 0x8e40
    if ((int32_t)v109 != 0) {
        goto lab_0x94d0_3;
    } else {
        // 0x901f
        v9 = *(int64_t *)(v106 + 64);
        if (v9 == 0) {
            // 0x95f0
            v113 = v112;
            result = v109 & 0xffffffff;
            v117 = v111;
            v118 = v4;
            v116 = v110;
            v114 = v108;
            v115 = v107;
            goto lab_0x94ea;
        } else {
            int64_t v119 = *(int64_t *)(v4 + 24); // 0x902c
            int64_t v120 = *(int64_t *)v119; // 0x9036
            int64_t v121 = *(int64_t *)(v119 + 8); // 0x9039
            if (*(int64_t *)(v9 + 16) == -1) {
                int64_t v122 = *(int64_t *)(v4 + 8); // 0x9210
                v6 = v120 - 1 + v121;
                v8 = *(int64_t *)(v122 + 8) - 1 + *(int64_t *)v122;
            } else {
                int64_t v123 = limfield_isra_0(v120, v121, v9); // 0x904c
                int64_t v124 = *(int64_t *)(v4 + 8); // 0x9057
                v6 = v123;
                v8 = limfield_isra_0(*(int64_t *)v124, *(int64_t *)(v124 + 8), v9);
            }
            // 0x906b
            if (*(int64_t *)v9 == -1) {
                // 0x91a8
                v5 = *(int64_t *)*(int64_t *)(v4 + 8);
                v7 = v120;
                if (*(char *)(v9 + 48) != 0) {
                    int64_t v125 = v120; // 0x91be
                    int64_t v126 = v120; // 0x91be
                    int64_t v127; // 0x8e40
                    if (v127 > v120) {
                        int64_t v128 = v125;
                        unsigned char v129 = *(char *)v128; // 0x91d1
                        char v130 = *(char *)((int64_t)v129 + (int64_t)&blanks); // 0x91d6
                        v126 = v128;
                        while (v130 != 0) {
                            int64_t v131 = v128 + 1; // 0x91c8
                            v125 = v131;
                            v126 = v127;
                            if (v127 == v131) {
                                // break -> 0x91dd
                                break;
                            }
                            v128 = v125;
                            v129 = *(char *)v128;
                            v130 = *(char *)((int64_t)v129 + (int64_t)&blanks);
                            v126 = v128;
                        }
                    }
                    int64_t v132 = v126;
                    int64_t v133; // 0x91b2
                    v5 = v133;
                    v7 = v132;
                    int64_t v134 = v133; // 0x91e0
                    int64_t v135; // 0x8e40
                    if (v135 > v133) {
                        int64_t v136 = v134;
                        unsigned char v137 = *(char *)v136; // 0x91fd
                        char v138 = *(char *)((int64_t)v137 + (int64_t)&blanks); // 0x9201
                        v5 = v136;
                        v7 = v132;
                        while (v138 != 0) {
                            int64_t v139 = v136 + 1; // 0x91f0
                            v5 = v135;
                            v7 = v132;
                            v134 = v139;
                            if (v135 == v139) {
                                // break -> 0x8ec0
                                break;
                            }
                            v136 = v134;
                            v137 = *(char *)v136;
                            v138 = *(char *)((int64_t)v137 + (int64_t)&blanks);
                            v5 = v136;
                            v7 = v132;
                        }
                    }
                }
            } else {
                int64_t v140 = begfield_isra_0(v120, v121, v9); // 0x907e
                int64_t v141 = *(int64_t *)(v4 + 8); // 0x9089
                v5 = begfield_isra_0(*(int64_t *)v141, *(int64_t *)(v141 + 8), v9);
                v7 = v140;
            }
            // 0x8ec0
            v10 = v112;
            goto lab_0x8ec0;
        }
    }
  lab_0x94d0_3:;
    int64_t v142 = v22;
    int64_t v143 = v17;
    int64_t v144 = v15;
    int64_t v145 = v31;
    int64_t v146 = v45;
    int64_t v147 = v39;
    int3_t v148 = v13;
    v113 = v148;
    result = v144 & 0xffffffff;
    v117 = v147;
    v118 = v146;
    v116 = v145;
    v114 = v143;
    v115 = v142;
    if (*(char *)(v25 + 55) != 0) {
        // 0x94da
        v113 = v148;
        result = 0xffffffff;
        v117 = v147;
        v118 = v146;
        v116 = v145;
        v114 = v143;
        v115 = v142;
        if ((int32_t)v144 >= 0) {
            // 0x94e6
            v113 = v148;
            result = -v144 & 0xffffffff;
            v117 = v147;
            v118 = v146;
            v116 = v145;
            v114 = v143;
            v115 = v142;
        }
    }
    goto lab_0x94ea;
  lab_0x938e:
    // 0x938e
    v327 = v11 + 2;
    v328 = v51;
    v329 = v43;
    v330 = v380;
    v331 = v321;
    v332 = 1;
    v333 = v334;
    v335 = v336;
    v337 = v23;
    goto lab_0x8ff0;
  lab_0x94ea:
    // 0x94ea
    if (*(int64_t *)(v118 + (int64_t)&g84) == __readfsqword(40)) {
        // break -> 0x9501
        goto lab_0x9501;
    }
    // 0x9d5d
    function_3950();
    int3_t v149 = v113; // 0x9d5d
    int64_t v150 = v117; // 0x9d5d
    int64_t v151 = v118; // 0x9d5d
    int64_t v152 = v116; // 0x9d5d
    int64_t v153 = v114; // 0x9d5d
    int64_t v154 = v115; // 0x9d5d
    goto lab_0x9d62;
  lab_0x9d48:
    // 0x9d48
    v327 = v11 + 2;
    v328 = v51;
    v329 = v43;
    v330 = v380;
    v331 = v321;
    v332 = 0xffffffff;
    v333 = v334;
    v335 = v336;
    v337 = v23;
    goto lab_0x8ff0;
  lab_0x9d62:;
    int3_t v155 = v149; // 0x9d71
    int64_t v156 = v150 - v154; // 0x9d71
    int64_t v157 = v150; // 0x9d71
    int64_t v158 = v151; // 0x9d71
    int64_t v159 = v152; // 0x9d71
    int64_t v160 = *(int64_t *)(v151 + 48) + v154; // 0x9d71
    int64_t v161 = v153; // 0x9d71
    int64_t v162 = v154; // 0x9d71
    goto lab_0x98be;
  lab_0x99eb:;
    int32_t * v354 = (int32_t *)v298; // 0x99f0
    *v354 = (int32_t)v304;
    *v297 = v303;
    function_3750();
    v327 = v299;
    v328 = v300;
    v329 = v301;
    v330 = v302;
    v331 = *v297;
    v332 = (int64_t)*v354;
    v333 = v305;
    v335 = v306;
    v337 = v307;
    goto lab_0x8ff0;
  lab_0x9b0f:;
    // 0x9b0f
    int64_t v285; // 0x8e40
    int64_t v365 = v285;
    int64_t v286; // 0x8e40
    int64_t v366 = v286;
    int64_t v287; // 0x8e40
    int64_t v367 = v287;
    int64_t * v293; // 0x8e40
    int64_t v368 = *v293;
    int64_t v278; // 0x8e40
    int64_t v369 = v278;
    int64_t v291; // 0x998e
    int64_t v370 = v291;
    int3_t v279; // 0x8e40
    int3_t v371 = v279;
    int64_t * v282; // 0x8e40
    int64_t * v372 = v282;
    int64_t * v373 = v293;
    int64_t v280; // 0x8e40
    int64_t v374 = v280;
    int64_t * v281; // 0x8e40
    int64_t * v375 = v281;
    int64_t v295; // 0x99d8
    *v372 = v295;
    *v373 = *v282;
    *v375 = *v281;
    int64_t v376 = function_3a80(); // 0x9b2e
    int64_t v377 = *v372; // 0x9b33
    v297 = v372;
    v298 = v374;
    v299 = v371;
    v300 = v370;
    v301 = v369;
    v302 = v368;
    v303 = v377;
    v304 = v376 & 0xffffffff;
    v305 = v367;
    v306 = v366;
    v307 = v365;
    if ((int32_t)v376 == 0) {
        uint64_t v378 = *v375; // 0x9b43
        uint64_t v379 = *v373; // 0x9b48
        v297 = v372;
        v298 = v374;
        v299 = v371;
        v300 = v370;
        v301 = v369;
        v302 = v368;
        v303 = v377;
        v304 = (int64_t)(v378 > v379) - (int64_t)(v378 < v379) & 0xffffffff;
        v305 = v367;
        v306 = v366;
        v307 = v365;
    }
    goto lab_0x99eb;
  lab_0x9770:;
    // 0x9770
    int64_t * v205; // 0x8e40
    int3_t v208; // 0x8e40
    int64_t v216; // 0x8e40
    int64_t v214; // 0x8e40
    int64_t v210; // 0x8e40
    int64_t v212; // 0x8e40
    if (*(int64_t *)(v274 + 104) > v273) {
        goto lab_0x984a;
    } else {
        // 0x9770
        v205 = (int64_t *)(v274 + 48);
        v208 = v272;
        v210 = v273;
        v212 = v274;
        v214 = v275;
        v216 = v276;
        goto lab_0x977b;
    }
  lab_0x98be:;
    int64_t v163 = xstrxfrm((char *)v160, (char *)v159, v156) + 1; // 0x98c6
    *(int64_t *)(v158 + 88) = v163;
    int3_t v164 = v155; // 0x98cf
    int64_t v165 = v157; // 0x98cf
    int64_t v166 = v158; // 0x98cf
    int64_t v167 = v159; // 0x98cf
    int64_t v168 = v161; // 0x98cf
    int64_t v169 = v162; // 0x98cf
    int64_t v170 = v163 + v162; // 0x98cf
    goto lab_0x98d3;
  lab_0x9372:;
    float80_t v309 = __frontend_reg_load_fpr(v11); // 0x9372
    float80_t v310 = __frontend_reg_load_fpr(v11); // 0x9372
    float80_t v311 = __frontend_reg_load_fpr(v11);
    if (v309 == v309 && v310 == v310) {
        // 0x9d44
        __frontend_reg_store_fpr(v11, v311);
        __frontend_reg_store_fpr(v312, __frontend_reg_load_fpr(v312));
        goto lab_0x9d48;
    } else {
        // 0x937a
        __frontend_reg_store_fpr(v11, __frontend_reg_load_fpr(v312));
        __frontend_reg_store_fpr(v312, v311);
        float80_t v313 = __frontend_reg_load_fpr(v11); // 0x937c
        float80_t v314 = __frontend_reg_load_fpr(v11); // 0x937c
        float80_t v315 = __frontend_reg_load_fpr(v11);
        if (v313 != v313 || v314 != v314) {
            // 0x9b60
            __frontend_reg_store_fpr(v11, __frontend_reg_load_fpr(v312));
            __frontend_reg_store_fpr(v312, v315);
            float80_t v316 = __frontend_reg_load_fpr(v11); // 0x9b62
            int64_t v317; // 0x9303
            float80_t * v318 = (float80_t *)v317; // 0x9b62
            *v318 = v316;
            int64_t v319; // 0x9318
            int64_t * v320 = (int64_t *)v319; // 0x9b8d
            *v320 = v321;
            float80_t v322 = __frontend_reg_load_fpr(v312); // 0x9b9a
            *(float80_t *)(v43 - 16) = v322;
            function_37a0();
            float80_t v323 = *v318; // 0x9ba2
            __frontend_reg_store_fpr(v312, v323);
            float80_t v324 = __frontend_reg_load_fpr(v312); // 0x9bc5
            *(float80_t *)(v43 - 32) = v324;
            function_37a0();
            int64_t v325 = function_3ad0(); // 0x9bd7
            int64_t v326 = *v320; // 0x9bdc
            v327 = v11 + 2;
            v328 = v43 + 192;
            v329 = v43;
            v330 = v43 + 327;
            v331 = v326;
            v332 = v325 & 0xffffffff;
            v333 = v334;
            v335 = v336;
            v337 = v23;
            goto lab_0x8ff0;
        } else {
            // 0x9384
            __frontend_reg_store_fpr(v11, v315);
            __frontend_reg_store_fpr(v312, __frontend_reg_load_fpr(v312));
            goto lab_0x938e;
        }
    }
  lab_0x984a:;
    // 0x984a
    int64_t v338; // 0x8e40
    int64_t v339 = v338;
    int64_t v340; // 0x8e40
    int64_t v341 = v340;
    int64_t v342; // 0x8e40
    int64_t v343 = v342;
    int64_t v344; // 0x8e40
    int64_t v345 = v344;
    int3_t v346; // 0x8e40
    int3_t v347 = v346;
    int64_t v348; // 0x8e40
    uint64_t v349 = v348; // 0x984a
    function_3750();
    int64_t v350 = function_3760(); // 0x986a
    *(int64_t *)(v343 + 32) = v350;
    int64_t v206; // 0x8e40
    int64_t v217; // 0x8e40
    int3_t v218; // 0x8e40
    int3_t v207; // 0x8e40
    int64_t v215; // 0x8e40
    int64_t v222; // 0x8e40
    int64_t v213; // 0x8e40
    int64_t v221; // 0x8e40
    int64_t v219; // 0x8e40
    int64_t v209; // 0x8e40
    int64_t v220; // 0x8e40
    int64_t v211; // 0x8e40
    if (v350 == 0) {
        int64_t * v351 = (int64_t *)(v343 + 48);
        *v351 = *(int64_t *)(v343 + 128);
        v205 = v351;
        v208 = v347;
        v210 = &g107;
        v212 = v343;
        v214 = v341;
        v216 = v339;
        goto lab_0x977b;
    } else {
        uint64_t v352 = 3 * v345 / 2; // 0x9858
        int64_t v353 = v349 >= v352 ? v349 : v352; // 0x985e
        *(int64_t *)(v343 + 48) = v350;
        v206 = v350;
        v207 = v347;
        v209 = v353;
        v211 = v343;
        v213 = v341;
        v215 = v339;
        v217 = v350;
        v218 = v347;
        v219 = v353;
        v220 = v343;
        v221 = v341;
        v222 = v339;
        if (v339 >= *(int64_t *)(v343 + 64)) {
            goto lab_0x9786;
        } else {
            goto lab_0x9892;
        }
    }
  lab_0x98d3:;
    uint64_t v171 = v170;
    int64_t v172 = v169;
    uint64_t v173 = v168;
    int64_t v174 = v167;
    int64_t v175 = v166;
    int64_t v176 = v165;
    int3_t v177 = v164;
    int64_t * v178; // 0x8e40
    int3_t v179; // 0x8e40
    int3_t v180; // 0x8e40
    int3_t v181; // 0x8e40
    int64_t v182; // 0x8e40
    int64_t v183; // 0x8e40
    int64_t v184; // 0x8e40
    int64_t v185; // 0x8e40
    int64_t v186; // 0x8e40
    int64_t v187; // 0x8e40
    int64_t v188; // 0x8e40
    int64_t v189; // 0x8e40
    int64_t v190; // 0x8e40
    int64_t v191; // 0x8e40
    int64_t v192; // 0x8e40
    int64_t v193; // 0x8e40
    int64_t v194; // 0x8e40
    int64_t v195; // 0x8e40
    int64_t v196; // 0x8e40
    int64_t v197; // 0x8e40
    int64_t * v198; // 0x8e40
    int64_t v199; // 0x8e40
    int64_t * v200; // 0x8e40
    if (v176 < v172 || v176 < v171) {
        // 0x98dc
        v188 = v171 < 0x5555555555555555 ? 3 * v171 / 2 : v171;
        function_3750();
        int64_t v201 = xmalloc(); // 0x98ff
        v198 = (int64_t *)(v175 + 32);
        *v198 = v201;
        v199 = v175 + 64;
        v200 = (int64_t *)v199;
        if (v173 < *v200) {
            // 0x9c18
            function_3ac0();
            *(int64_t *)(v175 + 72) = v201 + v172;
            if (v174 >= *(int64_t *)(v175 + 80)) {
                goto lab_0x992c;
            } else {
                // 0x9c3f
                function_3ac0();
                int64_t v202 = function_3940(); // 0x9c57
                *(int64_t *)(v175 + 48) = *v198;
                v181 = v177;
                v192 = v188;
                v197 = v175;
                v190 = v174;
                v183 = v173 + 1 + v202;
                v186 = v172;
                goto lab_0x97b2;
            }
        } else {
            // 0x9914
            *(int64_t *)(v175 + 72) = v201 + v172;
            if (v174 < *(int64_t *)(v175 + 80)) {
                // 0x9c70
                function_3ac0();
                *(int64_t *)(v175 + 48) = *v198;
                v181 = v177;
                v192 = v188;
                v197 = v175;
                v190 = v174;
                v183 = v173;
                v186 = v172;
                goto lab_0x97b2;
            } else {
                goto lab_0x992c;
            }
        }
    } else {
        // 0x9cca
        *(int64_t *)(v175 + 72) = *(int64_t *)(v175 + 48) + v172;
        if (v173 >= *(int64_t *)(v175 + 64)) {
            // 0x9cca
            v178 = (int64_t *)(v175 + 80);
            v180 = v177;
            v194 = v176;
            v196 = v175;
            v189 = v174;
            v182 = v173;
            v185 = v172;
            goto lab_0x97a7;
        } else {
            int64_t v203 = v173 + 1 + function_3940(); // 0x9cea
            v181 = v177;
            v192 = v176;
            v197 = v175;
            v190 = v174;
            v183 = v203;
            v186 = v172;
            v179 = v177;
            v193 = v176;
            v195 = v175;
            v191 = v174;
            v184 = v203;
            v187 = v172;
            if (v174 >= *(int64_t *)(v175 + 80)) {
                goto lab_0x97bf;
            } else {
                goto lab_0x97b2;
            }
        }
    }
  lab_0x977b:;
    int64_t v204 = *v205;
    v206 = v204;
    v207 = v208;
    v209 = v210;
    v211 = v212;
    v213 = v214;
    v215 = v216;
    v217 = v204;
    v218 = v208;
    v219 = v210;
    v220 = v212;
    v221 = v214;
    v222 = v216;
    if (v216 < *(int64_t *)(v212 + 64)) {
        goto lab_0x9892;
    } else {
        goto lab_0x9786;
    }
  lab_0x9786:;
    int64_t v223 = v215;
    int64_t v224 = v213;
    int64_t v225 = v211;
    int64_t v226 = v209;
    int3_t v227 = v207;
    int64_t v228 = v206;
    int64_t * v229 = (int64_t *)(v225 + 80);
    v155 = v227;
    v156 = v226;
    v157 = v226;
    v158 = v225;
    v159 = v224;
    v160 = v228;
    v161 = v223;
    v162 = 0;
    if (v224 < *v229) {
        goto lab_0x98be;
    } else {
        // 0x9791
        *(int64_t *)(v225 + 88) = 0;
        *(int64_t *)(v225 + 72) = v228;
        v178 = v229;
        v180 = v227;
        v194 = v226;
        v196 = v225;
        v189 = v224;
        v182 = v223;
        v185 = 0;
        goto lab_0x97a7;
    }
  lab_0x9892:;
    int64_t v230 = v222;
    int64_t v231 = v221;
    int64_t v232 = v220;
    int64_t v233 = v219;
    int3_t v234 = v218;
    int64_t v235 = xstrxfrm((char *)v217, (char *)v230, v233) + 1; // 0x98a2
    if (v231 >= *(int64_t *)(v232 + 80)) {
        // 0x9d00
        *(int64_t *)(v232 + 88) = 0;
        v164 = v234;
        v165 = v233;
        v166 = v232;
        v167 = v231;
        v168 = v230;
        v169 = v235;
        v170 = v235;
        goto lab_0x98d3;
    } else {
        // 0x98b1
        v155 = v234;
        v156 = 0;
        v157 = v233;
        v158 = v232;
        v159 = v231;
        v160 = 0;
        v161 = v230;
        v162 = v235;
        v149 = v234;
        v150 = v233;
        v151 = v232;
        v152 = v231;
        v153 = v230;
        v154 = v235;
        if (v233 >= v235) {
            goto lab_0x9d62;
        } else {
            goto lab_0x98be;
        }
    }
  lab_0x992c:;
    uint64_t v236 = *v200; // 0x992c
    int64_t * v237; // 0x8e40
    int64_t v238; // 0x8e40
    int64_t * v239; // 0x8e40
    int3_t v240; // 0x8e40
    int64_t v241; // 0x8e40
    int64_t v242; // 0x8e40
    int64_t v243; // 0x8e40
    int64_t * v244; // 0x8e40
    if (v173 < v236) {
        int64_t v245 = function_3940(); // 0x9bff
        *(int64_t *)(v175 + 48) = *v198;
        v179 = v177;
        v193 = v188;
        v195 = v175;
        v191 = v174;
        v184 = v173 + 1 + v245;
        v187 = v172;
        goto lab_0x97bf;
    } else {
        // 0x992c
        v237 = (int64_t *)(v175 + 48);
        v239 = v200;
        v238 = v199;
        v244 = v198;
        v240 = v177;
        v243 = v175;
        v242 = v172;
        v241 = v236;
        goto lab_0x9959;
    }
  lab_0x97a7:;
    int64_t v246 = v185;
    int64_t v247 = v196;
    int3_t v248 = v180;
    v181 = v248;
    v192 = v194;
    v197 = v247;
    v190 = v189;
    v183 = v182;
    v186 = v246;
    int64_t v249; // 0x8e40
    int64_t * v250; // 0x8e40
    int64_t v251; // 0x8e40
    int3_t v252; // 0x8e40
    int64_t v253; // 0x8e40
    int64_t v254; // 0x8e40
    if (v189 >= *v178) {
        int64_t v255 = v247 + 64; // 0x9d90
        int64_t * v256 = (int64_t *)v255; // 0x9d90
        v251 = *v256;
        v250 = v256;
        v249 = v255;
        v252 = v248;
        v254 = v247;
        v253 = v246;
        goto lab_0x9d8d;
    } else {
        goto lab_0x97b2;
    }
  lab_0x97bf:;
    uint64_t v257 = v187;
    int64_t v258 = v184;
    int64_t v259 = v191;
    int64_t v260 = v195;
    int64_t v261 = v193;
    int3_t v262 = v179;
    int64_t v263 = v260 + 64;
    int64_t * v264 = (int64_t *)v263;
    uint64_t v265 = *v264; // 0x97bf
    if (v258 < v265) {
        goto lab_0x97d1;
    } else {
        // 0x97c6
        v251 = v265;
        v250 = v264;
        v249 = v263;
        v252 = v262;
        v254 = v260;
        v253 = v257;
        if (v259 >= *(int64_t *)(v260 + 80)) {
            goto lab_0x9d8d;
        } else {
            goto lab_0x97d1;
        }
    }
  lab_0x97b2:
    // 0x97b2
    v179 = v181;
    v193 = v192;
    v195 = v197;
    v191 = v190 + 1 + function_3940();
    v184 = v183;
    v187 = v186;
    goto lab_0x97bf;
  lab_0x97d1:;
    int64_t v266 = *(int64_t *)(v260 + 120); // 0x97d1
    int64_t * v267 = (int64_t *)(v260 + 48);
    md5_process_bytes((int32_t *)*v267, v257, (int32_t *)v266);
    uint64_t v268 = *(int64_t *)(v260 + 88); // 0x97e3
    int64_t v269 = *(int64_t *)(v260 + 112); // 0x97e8
    int64_t v270 = *(int64_t *)(v260 + 72); // 0x97ed
    md5_process_bytes((int32_t *)v270, v268, (int32_t *)v269);
    int32_t * v271 = (int32_t *)(v260 + 100); // 0x97fa
    v272 = v262;
    v273 = v261;
    v274 = v260;
    v275 = v259;
    v276 = v258;
    if (*v271 != 0) {
        goto lab_0x9770;
    } else {
        int32_t v277 = function_3a80(); // 0x981f
        *v271 = v277;
        v272 = v262;
        v273 = v261;
        v274 = v260;
        v275 = v259;
        v276 = v258;
        if (v277 != 0) {
            goto lab_0x9770;
        } else {
            // 0x982b
            *v271 = (int32_t)(v268 < v257) - (int32_t)(v268 > v257);
            v205 = v267;
            v208 = v262;
            v210 = v261;
            v212 = v260;
            v214 = v259;
            v216 = v258;
            if (*(int64_t *)(v260 + 104) > v261) {
                goto lab_0x984a;
            } else {
                goto lab_0x977b;
            }
        }
    }
  lab_0x9959:
    // 0x9959
    v278 = v243;
    v279 = v240;
    v280 = v238;
    v281 = v239;
    v282 = v237;
    int64_t v283 = v278 + 88;
    int64_t * v284 = (int64_t *)(v278 + 80);
    v285 = *(int64_t *)(v278 + 136);
    v286 = *v284;
    v287 = *v244;
    int32_t * v288 = (int32_t *)v283; // 0x9964
    *v288 = *(int32_t *)(v278 + 100);
    int64_t v289 = v278 + 160; // 0x9969
    *v282 = *(int64_t *)v283;
    *v284 = v241;
    *v281 = v242;
    int32_t * v290 = (int32_t *)*(int64_t *)(v278 + 120); // 0x9983
    md5_process_bytes((int32_t *)v287, v242, v290);
    v291 = v278 + 176;
    md5_finish_ctx(v290, (char *)v289);
    int64_t * v292 = (int64_t *)(v278 + 112); // 0x999b
    v293 = (int64_t *)(v278 + 72);
    md5_process_bytes((int32_t *)*v293, *v282, (int32_t *)*v292);
    md5_finish_ctx((int32_t *)*v292, (char *)v291);
    int64_t v294 = function_3a80(); // 0x99c7
    v295 = *v284;
    if ((int32_t)v294 == 0) {
        uint32_t v296 = *v288; // 0x99dd
        v297 = v282;
        v298 = v280;
        v299 = v279;
        v300 = v291;
        v301 = v278;
        v302 = v289;
        v303 = v295;
        v304 = v296;
        v305 = v287;
        v306 = v286;
        v307 = v285;
        if (v296 != 0) {
            goto lab_0x99eb;
        } else {
            // 0x9d1a
            goto lab_0x9b0f;
        }
    } else {
        // 0x99e8
        v297 = v282;
        v298 = v280;
        v299 = v279;
        v300 = v291;
        v301 = v278;
        v302 = v289;
        v303 = v295;
        v304 = v294 & 0xffffffff;
        v305 = v287;
        v306 = v286;
        v307 = v285;
        goto lab_0x99eb;
    }
  lab_0x9d8d:;
    int64_t * v308 = (int64_t *)(v254 + 48);
    v237 = v308;
    v239 = v250;
    v238 = v249;
    v244 = v308;
    v240 = v252;
    v243 = v254;
    v242 = v253;
    v241 = v251;
    goto lab_0x9959;
}