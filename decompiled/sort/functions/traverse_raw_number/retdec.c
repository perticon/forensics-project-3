char traverse_raw_number(char ** number) {
    int64_t v1 = (int64_t)number;
    int64_t v2; // 0x6c20
    char v3 = v2;
    int64_t v4 = v1 + 1; // 0x6c29
    int64_t v5 = 0x100000000000000 * v2 >> 56 & 0xffffffff; // 0x6c2d
    int64_t v6 = v5; // 0x6c35
    int64_t v7 = v4; // 0x6c35
    int64_t result = 0; // 0x6c35
    if (v3 == 57 || (int32_t)v3 < 57) {
        int64_t v8 = 0;
        int64_t v9 = v5;
        char v10 = v8; // 0x6c48
        char v11 = v10 - (char)v9; // 0x6c48
        result = v11 < 0 == ((v11 ^ v10) & (char)(v8 ^ v9)) < 0 ? v8 : v9;
        char v12 = *(char *)(v1 + 1); // 0x6c55
        int64_t v13 = thousands_sep == (int32_t)v12 ? v1 + 2 : v4;
        char v14 = *(char *)v13; // 0x6c67
        v7 = v13 + 1;
        v6 = (int64_t)v14 & 0xffffffff;
        int64_t v15 = v13; // 0x6c76
        while (v14 == 57 || (int32_t)v14 < 57) {
            // 0x6c48
            v8 = result;
            v9 = v6;
            v10 = v8;
            v11 = v10 - (char)v9;
            result = v11 < 0 == ((v11 ^ v10) & (char)(v8 ^ v9)) < 0 ? v8 : v9;
            v12 = *(char *)(v15 + 1);
            v13 = thousands_sep == (int32_t)v12 ? v15 + 2 : v7;
            v14 = *(char *)v13;
            v7 = v13 + 1;
            v6 = (int64_t)v14 & 0xffffffff;
            v15 = v13;
        }
        // 0x6c78
        if (thousands_sep == (int32_t)v12) {
            // 0x6cc8
            *(int64_t *)number = v13 - 1;
            return result;
        }
    }
    // 0x6c7d
    if (decimal_point != (char)v6) {
        // 0x6c89
        *(int64_t *)number = v7 - 1;
        return result;
    }
    char v16 = *(char *)v7; // 0x6c90
    int64_t v17 = v7; // 0x6c9f
    if (v16 != 57 && (int32_t)v16 >= 57) {
        // 0x6c89
        *(int64_t *)number = v7;
        return result;
    }
    char v18 = v16;
    v17++;
    char v19 = result; // 0x6ca8
    char v20 = v19 - v18; // 0x6ca8
    int64_t v21 = v20 < 0 == ((v20 ^ v19) & (v18 ^ v19)) < 0 ? result : (int64_t)v18;
    int64_t result2 = v21 & 0xffffffff; // 0x6cae
    char v22 = *(char *)v17; // 0x6cb2
    while (v22 == 57 || (int32_t)v22 < 57) {
        // 0x6ca8
        v18 = v22;
        v17++;
        v19 = result2;
        v20 = v19 - v18;
        v21 = v20 < 0 == ((v20 ^ v19) & (v18 ^ v19)) < 0 ? result2 : (int64_t)v18;
        result2 = v21 & 0xffffffff;
        v22 = *(char *)v17;
    }
    // 0x6c89
    *(int64_t *)number = v17;
    return result2;
}