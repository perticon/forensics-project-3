signed char traverse_raw_number(struct s4** rdi, void** rsi, ...) {
    struct s4* rax3;
    struct s4** r10_4;
    int32_t ecx5;
    struct s4* rsi6;
    int32_t edx7;
    int32_t r8d8;
    uint32_t edi9;
    int32_t r9d10;
    int32_t ecx11;
    int1_t zf12;
    int32_t ecx13;
    struct s4* rdx14;
    int32_t eax15;
    int32_t ecx16;
    int32_t eax17;
    int32_t eax18;

    rax3 = *rdi;
    r10_4 = rdi;
    ecx5 = rax3->f0;
    rsi6 = reinterpret_cast<struct s4*>(&rax3->f1);
    edx7 = ecx5;
    if (ecx5 - 48 > 9) {
        r8d8 = 0;
    } else {
        edi9 = thousands_sep;
        r8d8 = 0;
        do {
            r9d10 = 1;
            if (*reinterpret_cast<signed char*>(&r8d8) < *reinterpret_cast<signed char*>(&edx7)) {
                r8d8 = edx7;
            }
            ++rax3;
            if (static_cast<int32_t>(rax3->f1) != edi9) {
                rax3 = rsi6;
                r9d10 = 0;
            }
            ecx11 = rax3->f0;
            rsi6 = reinterpret_cast<struct s4*>(&rax3->f1);
            edx7 = ecx11;
        } while (ecx11 - 48 <= 9);
        if (*reinterpret_cast<signed char*>(&r9d10)) 
            goto addr_6cc8_10;
    }
    zf12 = decimal_point == *reinterpret_cast<unsigned char*>(&edx7);
    if (zf12) {
        ecx13 = rsi6->f0;
        rdx14 = reinterpret_cast<struct s4*>(&rsi6->f1);
        eax15 = ecx13;
        if (ecx13 - 48 <= 9) {
            do {
                rsi6 = rdx14;
                if (*reinterpret_cast<signed char*>(&r8d8) < *reinterpret_cast<signed char*>(&eax15)) {
                    r8d8 = eax15;
                }
                ecx16 = rdx14->f0;
                rdx14 = reinterpret_cast<struct s4*>(&rdx14->f1);
                eax15 = ecx16;
            } while (ecx16 - 48 <= 9);
        }
    } else {
        rsi6 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(rsi6) - 1);
    }
    *r10_4 = rsi6;
    eax17 = r8d8;
    return *reinterpret_cast<signed char*>(&eax17);
    addr_6cc8_10:
    eax18 = r8d8;
    *r10_4 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(rax3) - 1);
    return *reinterpret_cast<signed char*>(&eax18);
}