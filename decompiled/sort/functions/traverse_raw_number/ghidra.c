int traverse_raw_number(char **param_1)

{
  char *pcVar1;
  char *pcVar2;
  int iVar3;
  char cVar4;
  char *pcVar5;
  int iVar6;
  
  pcVar2 = *param_1;
  cVar4 = *pcVar2;
  iVar3 = (int)cVar4;
  pcVar5 = pcVar2 + 1;
  if (iVar3 - 0x30U < 10) {
    iVar6 = 0;
    do {
      if ((char)iVar6 < (char)iVar3) {
        iVar6 = iVar3;
      }
      pcVar1 = pcVar2 + 1;
      pcVar2 = pcVar2 + 2;
      if (*pcVar1 != thousands_sep) {
        pcVar2 = pcVar5;
      }
      cVar4 = *pcVar2;
      iVar3 = (int)cVar4;
      pcVar5 = pcVar2 + 1;
    } while (iVar3 - 0x30U < 10);
    if (*pcVar1 == thousands_sep) {
      *param_1 = pcVar2 + -1;
      return iVar6;
    }
  }
  else {
    iVar6 = 0;
  }
  if (decimal_point == cVar4) {
    cVar4 = *pcVar5;
    while ((int)cVar4 - 0x30U < 10) {
      pcVar5 = pcVar5 + 1;
      if ((char)iVar6 < cVar4) {
        iVar6 = (int)cVar4;
      }
      cVar4 = *pcVar5;
    }
  }
  else {
    pcVar5 = pcVar5 + -1;
  }
  *param_1 = pcVar5;
  return iVar6;
}