void key_to_opts(void** rdi, void** rsi, void** rdx, ...) {
    void** rax4;

    if (*reinterpret_cast<void***>(rdi + 48)) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(98);
        ++rsi;
    }
    if (*reinterpret_cast<void***>(rdi + 32) == 0x1d560) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(100);
        ++rsi;
    }
    rax4 = rsi;
    if (*reinterpret_cast<void***>(rdi + 40)) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0x66);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 52)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x67);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 53)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x68);
        ++rax4;
    }
    if (*reinterpret_cast<void***>(rdi + 32) == 0x1d660) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x69);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 54)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(77);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 50)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x6e);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 51)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(82);
        ++rax4;
    }
    if (*reinterpret_cast<signed char*>(rdi + 55)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x72);
        ++rax4;
    }
    if (*reinterpret_cast<void***>(rdi + 56)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(86);
        ++rax4;
    }
    *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0);
    return;
}