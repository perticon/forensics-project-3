void insertkey(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rdx5;
    void*** rcx6;
    void** rcx7;

    rax4 = xmemdup(rdi, 72);
    rdx5 = keylist;
    rcx6 = reinterpret_cast<void***>(0x1d430);
    if (rdx5) {
        do {
            rcx7 = rdx5;
            rdx5 = *reinterpret_cast<void***>(rdx5 + 64);
        } while (rdx5);
        rcx6 = reinterpret_cast<void***>(rcx7 + 64);
    }
    *rcx6 = rax4;
    *reinterpret_cast<void***>(rax4 + 64) = reinterpret_cast<void**>(0);
    return;
}