void insertkey(int32_t * key_arg) {
    int64_t v1 = xmemdup(); // 0x7389
    if (keylist == NULL) {
        // 0x73b1
        *(int64_t *)(int64_t)&keylist = v1;
        *(int64_t *)(v1 + 64) = 0;
        return;
    }
    int64_t v2 = (int64_t)keylist + 64; // 0x73a4
    int64_t v3 = *(int64_t *)v2; // 0x73a4
    while (v3 != 0) {
        // 0x73a1
        v2 = v3 + 64;
        v3 = *(int64_t *)v2;
    }
    // 0x73b1
    *(int64_t *)v2 = v1;
    *(int64_t *)(v1 + 64) = 0;
}