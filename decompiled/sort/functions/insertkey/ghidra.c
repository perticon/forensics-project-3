void insertkey(undefined8 param_1)

{
  long lVar1;
  long lVar2;
  long *plVar3;
  long lVar4;
  
  lVar2 = xmemdup(param_1,0x48);
  plVar3 = &keylist;
  lVar1 = keylist;
  if (keylist != 0) {
    do {
      lVar4 = lVar1;
      lVar1 = *(long *)(lVar4 + 0x40);
    } while (lVar1 != 0);
    plVar3 = (long *)(lVar4 + 0x40);
  }
  *plVar3 = lVar2;
  *(undefined8 *)(lVar2 + 0x40) = 0;
  return;
}