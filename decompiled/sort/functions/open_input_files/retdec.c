int64_t open_input_files(int32_t * files, int64_t nfiles, struct _IO_FILE *** pfps) {
    // 0x8620
    int32_t v1; // 0x8620
    uint32_t v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x863f
    int64_t v4 = (int64_t)xnmalloc(nfiles, 8); // 0x864f
    *(int64_t *)pfps = v4;
    int64_t v5; // 0x8620
    int64_t v6; // 0x8620
    int64_t v7; // 0x8620
    int64_t result; // 0x8620
    int64_t v8; // 0x8620
    int64_t v9; // 0x8620
    int64_t v10; // 0x8620
    int64_t v11; // 0x8620
    int64_t v12; // 0x8620
    int64_t v13; // 0x8620
    int64_t v14; // bp-88, 0x8620
    int64_t v15; // 0x86a5
    if (nfiles == 0) {
        // 0x87f2
        v11 = (int64_t)pfps;
        result = 0;
    } else {
        // 0x8666
        v12 = (int64_t)files;
        v8 = v4;
        v5 = 0;
        while (true) {
          lab_0x86a5:
            // 0x86a5
            v6 = v5;
            v9 = v8;
            v13 = v12;
            v15 = *(int64_t *)(v13 + 8);
            if (v15 == 0) {
                goto lab_0x8678;
            } else {
                // 0x86ae
                switch (*(char *)(v15 + 12)) {
                    case 0: {
                        goto lab_0x8678;
                    }
                    case 1: {
                        int32_t v16 = *(int32_t *)(v15 + 8); // 0x87b8
                        char * v17 = hash_remove(proctab, (int32_t *)&v14); // 0x87d2
                        if (v17 != NULL) {
                            // 0x87e0
                            *(char *)((int64_t)v17 + 12) = 2;
                            reap(v16);
                        }
                        goto lab_0x86bf;
                    }
                    default: {
                        goto lab_0x86bf;
                    }
                }
            }
        }
      lab_0x8727_3:
        // 0x8727
        v11 = v10;
        result = v7;
    }
    goto lab_0x8727;
  lab_0x8678:;
    struct _IO_FILE * v25 = stream_open((char *)*(int64_t *)v13, "r"); // 0x867e
    *(int64_t *)v9 = (int64_t)v25;
    v10 = v9;
    v7 = v6;
    if (v25 == NULL) {
        // break -> 0x8727
        goto lab_0x8727_3;
    }
    goto lab_0x8690;
  lab_0x8690:;
    int64_t v26 = v6 + 1; // 0x8690
    v12 = v13 + 16;
    v8 = v9 + 8;
    v5 = v26;
    if (v26 == nfiles) {
        // break -> 0x8727
        goto lab_0x8727_3;
    }
    goto lab_0x86a5;
  lab_0x86bf:;
    int64_t v27 = function_3cd0(); // 0x86c7
    int32_t v28 = v27;
    if (v28 < 0) {
        // 0x87a0
        *(int64_t *)(8 * v6 + v4) = 0;
        v11 = v9;
        result = v6;
        goto lab_0x8727;
    }
    int32_t v29 = pipe_fork((int32_t *)&v14, 9); // 0x86e2
    int64_t v19; // 0x8620
    if (v29 == -1) {
        int32_t * v30 = (int32_t *)function_37d0(); // 0x8700
        v19 = v9;
        if (*v30 != 24) {
            goto lab_0x8861;
        } else {
            // 0x870c
            function_3a40();
            *v30 = 24;
            *(int64_t *)(8 * v6 + v4) = 0;
            v11 = v9;
            result = v6;
            goto lab_0x8727;
        }
    }
    if (v29 == 0) {
        // 0x87fa
        function_3a40();
        if (v28 == 0) {
            goto lab_0x881c;
        } else {
            // 0x8812
            move_fd_part_0(v27 & 0xffffffff);
            goto lab_0x881c;
        }
    }
    // 0x8758
    *(int32_t *)(v15 + 8) = v29;
    register_proc((int32_t *)v15);
    function_3a40();
    function_3a40();
    int64_t v31 = function_3c20(); // 0x8782
    int64_t v21 = v9; // 0x878a
    int64_t v22 = v6; // 0x878a
    if (v31 == 0) {
        goto lab_0x889c;
    }
    // 0x8790
    *(int64_t *)v9 = v31;
    goto lab_0x8690;
  lab_0x8727:;
    int64_t v18 = v11; // 0x8735
    if (v3 == __readfsqword(40)) {
        // 0x873b
        return result;
    }
    goto lab_0x885c;
  lab_0x885c:
    // 0x885c
    function_3950();
    v19 = v18;
    goto lab_0x8861;
  lab_0x8861:;
    int64_t v20 = quotearg_style(); // 0x886d
    function_3920();
    function_3c90();
    v21 = v19;
    v22 = v20;
  lab_0x889c:;
    int32_t * v23 = (int32_t *)function_37d0(); // 0x88a5
    function_3a40();
    *(int64_t *)v21 = 0;
    v11 = v21;
    result = v22;
    goto lab_0x8727;
  lab_0x881c:
    if (v2 == 1) {
        goto lab_0x882f;
    } else {
        // 0x8825
        move_fd_part_0((int64_t)v2);
        goto lab_0x882f;
    }
  lab_0x882f:
    // 0x882f
    function_3e20();
    int32_t v24 = *(int32_t *)function_37d0(); // 0x8855
    async_safe_die(v24, "couldn't execute compress program (with -d)");
    v18 = v9;
    goto lab_0x885c;
}