long open_input_files(undefined8 *param_1,long param_2,long **param_3)

{
  undefined4 uVar1;
  long lVar2;
  int iVar3;
  int iVar4;
  FILE **ppFVar5;
  int *piVar6;
  FILE *pFVar7;
  long lVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  FILE **ppFVar11;
  long lVar12;
  long in_FS_OFFSET;
  int local_58;
  int local_54;
  undefined4 local_50;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  ppFVar5 = (FILE **)xnmalloc(param_2);
  *param_3 = (long *)ppFVar5;
  if (param_2 == 0) {
    lVar12 = 0;
  }
  else {
    lVar12 = 0;
    ppFVar11 = ppFVar5;
    do {
      lVar2 = param_1[1];
      if ((lVar2 == 0) || (*(char *)(lVar2 + 0xc) == '\0')) {
        pFVar7 = (FILE *)stream_open(*param_1);
        *ppFVar11 = pFVar7;
        if (pFVar7 == (FILE *)0x0) break;
      }
      else {
        if (*(char *)(lVar2 + 0xc) == '\x01') {
          uVar1 = *(undefined4 *)(lVar2 + 8);
          local_50 = uVar1;
          lVar8 = hash_remove(proctab);
          if (lVar8 != 0) {
            *(undefined *)(lVar8 + 0xc) = 2;
            reap(uVar1);
          }
        }
        iVar3 = open((char *)(lVar2 + 0xd),0);
        if (iVar3 < 0) {
          ppFVar5[lVar12] = (FILE *)0x0;
          break;
        }
        iVar4 = pipe_fork(&local_58,9);
        if (iVar4 == -1) {
          piVar6 = __errno_location();
          if (*piVar6 != 0x18) {
            uVar9 = quotearg_style(4,compress_program);
            uVar10 = dcgettext(0,"couldn\'t create process for %s -d",5);
                    /* WARNING: Subroutine does not return */
            error(2,*piVar6,uVar10,uVar9);
          }
          close(iVar3);
          *piVar6 = 0x18;
          ppFVar5[lVar12] = (FILE *)0x0;
          break;
        }
        if (iVar4 == 0) {
          close(local_58);
          if (iVar3 != 0) {
            move_fd_part_0(iVar3,0);
          }
          if (local_54 != 1) {
            move_fd_part_0(local_54,1);
          }
          execlp(compress_program,compress_program,&DAT_00116be6,0);
          piVar6 = __errno_location();
          async_safe_die(*piVar6,"couldn\'t execute compress program (with -d)");
          goto LAB_0010885c;
        }
        *(int *)(lVar2 + 8) = iVar4;
        register_proc();
        close(iVar3);
        close(local_54);
        pFVar7 = fdopen(local_58,"r");
        if (pFVar7 == (FILE *)0x0) {
          piVar6 = __errno_location();
          iVar3 = *piVar6;
          close(local_58);
          *ppFVar11 = (FILE *)0x0;
          *piVar6 = iVar3;
          break;
        }
        *ppFVar11 = pFVar7;
      }
      lVar12 = lVar12 + 1;
      param_1 = param_1 + 2;
      ppFVar11 = ppFVar11 + 1;
    } while (param_2 != lVar12);
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return lVar12;
  }
LAB_0010885c:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}