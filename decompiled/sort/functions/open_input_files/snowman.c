void** open_input_files(void** rdi, void** rsi, void** rdx) {
    void** r13_4;
    void** rbp5;
    void** rbx6;
    void** rax7;
    void** v8;
    void** rax9;
    void* rsp10;
    void** v11;
    void** r12_12;
    void** r15_13;
    uint32_t eax14;
    void** rdi15;
    void** rax16;
    void** r9d17;
    void** rdi18;
    void** rsi19;
    struct s3* rax20;
    int32_t eax21;
    void** eax22;
    int64_t rdi23;
    int32_t v24;
    void** rax25;
    void* rax26;
    void** rsi27;
    void** rax28;
    void** rax29;
    void** rax30;
    void** r13d31;
    void** rax32;
    int32_t v33;
    void** rax34;
    void** edi35;

    r13_4 = rsi;
    rbp5 = rdx;
    rbx6 = rdi;
    rax7 = g28;
    v8 = rax7;
    rax9 = xnmalloc(r13_4, 8, rdx);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    v11 = rax9;
    *reinterpret_cast<void***>(rbp5) = rax9;
    if (!r13_4) {
        *reinterpret_cast<int32_t*>(&r12_12) = 0;
        *reinterpret_cast<int32_t*>(&r12_12 + 4) = 0;
    } else {
        rbp5 = rax9;
        *reinterpret_cast<int32_t*>(&r12_12) = 0;
        *reinterpret_cast<int32_t*>(&r12_12 + 4) = 0;
        do {
            r15_13 = *reinterpret_cast<void***>(rbx6 + 8);
            if (!r15_13 || (eax14 = *reinterpret_cast<unsigned char*>(r15_13 + 12), *reinterpret_cast<signed char*>(&eax14) == 0)) {
                rdi15 = *reinterpret_cast<void***>(rbx6);
                rax16 = stream_open(rdi15, "r", rdx);
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                *reinterpret_cast<void***>(rbp5) = rax16;
                if (!rax16) 
                    break;
            } else {
                if (*reinterpret_cast<signed char*>(&eax14) == 1 && (r9d17 = *reinterpret_cast<void***>(r15_13 + 8), rdi18 = proctab, rsi19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 16), rax20 = hash_remove(rdi18, rsi19, rdx), rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), !!rax20)) {
                    rax20->fc = 2;
                    reap(r9d17, rsi19);
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                }
                eax21 = fun_3cd0(r15_13 + 13);
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                if (eax21 < 0) 
                    goto addr_87a0_9;
                eax22 = pipe_fork(reinterpret_cast<int64_t>(rsp10) + 16, 9, rdx);
                if (reinterpret_cast<int1_t>(eax22 == 0xffffffff)) 
                    goto addr_86f1_11;
                if (!eax22) 
                    goto addr_87fa_13;
                *reinterpret_cast<void***>(r15_13 + 8) = eax22;
                register_proc(r15_13, 9, rdx);
                fun_3a40();
                fun_3a40();
                *reinterpret_cast<int32_t*>(&rdi23) = v24;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
                rax25 = fun_3c20(rdi23, "r", rdx);
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                if (!rax25) 
                    goto addr_889c_15;
                *reinterpret_cast<void***>(rbp5) = rax25;
            }
            ++r12_12;
            rbx6 = rbx6 + 16;
            rbp5 = rbp5 + 8;
        } while (r13_4 != r12_12);
    }
    addr_8727_18:
    while (rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28)), !!rax26) {
        addr_885c_19:
        fun_3950();
        addr_8861_20:
        rsi27 = compress_program;
        rax28 = quotearg_style(4, rsi27, rdx, 4, rsi27, rdx);
        r12_12 = rax28;
        rax29 = fun_3920();
        rdx = rax29;
        fun_3c90();
        addr_889c_15:
        rax30 = fun_37d0();
        r13d31 = *reinterpret_cast<void***>(rax30);
        fun_3a40();
        *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax30) = r13d31;
    }
    return r12_12;
    addr_87a0_9:
    *reinterpret_cast<void***>(v11 + reinterpret_cast<unsigned char>(r12_12) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_18;
    addr_86f1_11:
    rax32 = fun_37d0();
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax32) == 24)) 
        goto addr_8861_20;
    fun_3a40();
    *reinterpret_cast<void***>(rax32) = reinterpret_cast<void**>(24);
    *reinterpret_cast<void***>(v11 + reinterpret_cast<unsigned char>(r12_12) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_18;
    addr_87fa_13:
    fun_3a40();
    if (eax21) {
        move_fd_part_0(eax21);
    }
    if (v33 != 1) {
        move_fd_part_0(v33, v33);
    }
    rdx = reinterpret_cast<void**>("-d");
    fun_3e20();
    rax34 = fun_37d0();
    edi35 = *reinterpret_cast<void***>(rax34);
    async_safe_die(edi35, "couldn't execute compress program (with -d)", edi35, "couldn't execute compress program (with -d)");
    goto addr_885c_19;
}