void mergefps(long param_1,ulong param_2,ulong param_3,undefined8 param_4,undefined8 param_5,
             void *param_6)

{
  long lVar1;
  size_t __n;
  undefined4 *puVar2;
  undefined8 uVar3;
  ulong uVar4;
  undefined4 uVar5;
  undefined4 uVar6;
  undefined4 uVar7;
  undefined4 uVar8;
  undefined4 uVar9;
  undefined4 uVar10;
  undefined4 uVar11;
  undefined4 uVar12;
  undefined4 uVar13;
  undefined4 uVar14;
  undefined4 uVar15;
  undefined4 uVar16;
  undefined4 uVar17;
  undefined4 uVar18;
  undefined4 uVar19;
  undefined4 uVar20;
  long lVar21;
  char cVar22;
  int iVar23;
  void *__ptr;
  void *__ptr_00;
  void *__ptr_01;
  ulong *__ptr_02;
  void *pvVar24;
  void *pvVar25;
  ulong *puVar26;
  undefined4 *puVar27;
  ulong uVar28;
  ulong uVar29;
  ulong uVar30;
  long lVar31;
  void **ppvVar32;
  undefined8 *puVar33;
  undefined8 *puVar34;
  ulong uVar35;
  long in_FS_OFFSET;
  bool bVar36;
  ulong local_e0;
  ulong local_d8;
  void **local_b0;
  ulong local_98;
  void *local_90;
  void *local_68;
  void *local_60;
  long local_58;
  long local_50;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  __ptr = (void *)xnmalloc(param_3,0x38);
  __ptr_00 = (void *)xnmalloc(param_3,8);
  __ptr_01 = (void *)xnmalloc(param_3,8);
  __ptr_02 = (ulong *)xnmalloc(param_3,8);
  lVar21 = keylist;
  local_68 = (void *)0x0;
  if (param_3 != 0) {
    uVar29 = 0;
    local_d8 = param_3;
    local_98 = param_2;
    do {
      while( true ) {
        uVar28 = sort_size / local_d8;
        if (sort_size / local_d8 < merge_buffer_size) {
          uVar28 = merge_buffer_size;
        }
        ppvVar32 = (void **)((long)__ptr + uVar29 * 0x38);
        while( true ) {
          pvVar25 = (void *)((uVar28 & 0xffffffffffffffe0) + 0x20);
          pvVar24 = malloc((size_t)pvVar25);
          *ppvVar32 = pvVar24;
          if (pvVar24 != (void *)0x0) break;
          uVar28 = (ulong)pvVar25 >> 1;
          if (pvVar25 < (void *)0x43) {
                    /* WARNING: Subroutine does not return */
            xalloc_die();
          }
        }
        ppvVar32[3] = pvVar25;
        *(undefined *)(ppvVar32 + 6) = 0;
        lVar31 = uVar29 * 8;
        ppvVar32[5] = (void *)0x20;
        puVar34 = (undefined8 *)(param_1 + uVar29 * 0x10);
        ppvVar32[2] = (void *)0x0;
        uVar3 = *puVar34;
        ppvVar32[4] = (void *)0x0;
        puVar33 = (undefined8 *)((long)param_6 + lVar31);
        ppvVar32[1] = (void *)0x0;
        cVar22 = fillbuf(ppvVar32,*puVar33,uVar3);
        if (cVar22 != '\0') break;
        xfclose(*puVar33,*puVar34);
        if (uVar29 < local_98) {
          local_98 = local_98 - 1;
          zaptemp(*puVar34);
        }
        free(*ppvVar32);
        uVar28 = local_d8 - 1;
        if (uVar28 <= uVar29) {
          local_d8 = uVar28;
          if (uVar28 == 0) goto LAB_0010a86f;
          goto LAB_0010a30e;
        }
        if ((puVar34 < (undefined8 *)((long)param_6 + uVar28 * 8)) &&
           (puVar33 < (undefined8 *)(local_d8 * 0x10 + param_1))) {
          lVar1 = local_d8 * 8;
          do {
            puVar2 = (undefined4 *)(param_1 + 0x10 + lVar31 * 2);
            uVar5 = puVar2[1];
            uVar6 = puVar2[2];
            uVar7 = puVar2[3];
            uVar3 = *(undefined8 *)((long)param_6 + lVar31 + 8);
            puVar27 = (undefined4 *)(param_1 + lVar31 * 2);
            *puVar27 = *puVar2;
            puVar27[1] = uVar5;
            puVar27[2] = uVar6;
            puVar27[3] = uVar7;
            *(undefined8 *)((long)param_6 + lVar31) = uVar3;
            lVar31 = lVar31 + 8;
            local_d8 = uVar28;
          } while (lVar31 != lVar1 + -8);
        }
        else {
          memmove(puVar34,(void *)(param_1 + 0x10 + uVar29 * 0x10),(uVar28 - uVar29) * 0x10);
          memmove(puVar33,(void *)((long)param_6 + lVar31 + 8),(uVar28 - uVar29) * 8);
          local_d8 = uVar28;
        }
      }
      pvVar25 = ppvVar32[3];
      pvVar24 = *ppvVar32;
      *(long *)((long)__ptr_00 + uVar29 * 8) = (long)pvVar25 + (long)pvVar24 + -0x20;
      *(long *)((long)__ptr_01 + uVar29 * 8) =
           (long)pvVar25 + (long)pvVar24 + (long)ppvVar32[2] * -0x20;
      uVar29 = uVar29 + 1;
    } while (uVar29 < local_d8);
LAB_0010a30e:
    uVar29 = 0;
    do {
      __ptr_02[uVar29] = uVar29;
      uVar29 = uVar29 + 1;
    } while (uVar29 != local_d8);
    if (local_d8 != 1) {
      uVar29 = 1;
      do {
        while( true ) {
          puVar26 = __ptr_02 + uVar29;
          uVar28 = *puVar26;
          uVar30 = __ptr_02[uVar29 - 1];
          iVar23 = compare(*(undefined8 *)((long)__ptr_00 + uVar30 * 8),
                           *(undefined8 *)((long)__ptr_00 + uVar28 * 8));
          if (iVar23 < 1) break;
          __ptr_02[uVar29 - 1] = uVar28;
          uVar29 = 1;
          *puVar26 = uVar30;
        }
        uVar29 = uVar29 + 1;
      } while (uVar29 < local_d8);
    }
    local_90 = (void *)0x0;
    local_b0 = (void **)0x0;
LAB_0010a3b0:
    local_e0 = *__ptr_02;
    lVar31 = local_e0 * 8;
    puVar26 = (ulong *)((long)__ptr_00 + lVar31);
    ppvVar32 = (void **)*puVar26;
    if (unique == '\0') {
      write_line(ppvVar32,param_4,param_5);
    }
    else {
      if (local_b0 != (void **)0x0) {
        iVar23 = compare(local_b0,ppvVar32);
        if (iVar23 == 0) goto LAB_0010a3f6;
        write_line(&local_68,param_4,param_5);
      }
      pvVar25 = ppvVar32[1];
      if (local_90 < pvVar25) {
        do {
          pvVar24 = pvVar25;
          if (local_90 == (void *)0x0) break;
          local_90 = (void *)((long)local_90 * 2);
          pvVar24 = local_90;
        } while (local_90 < pvVar25);
        free(local_68);
        local_68 = (void *)xmalloc(pvVar24);
        pvVar25 = ppvVar32[1];
        local_90 = pvVar24;
      }
      local_60 = pvVar25;
      pvVar25 = memcpy(local_68,*ppvVar32,(size_t)pvVar25);
      local_b0 = &local_68;
      if (lVar21 != 0) {
        local_58 = ((long)ppvVar32[2] - (long)*ppvVar32) + (long)pvVar25;
        local_50 = (long)pvVar25 + ((long)ppvVar32[3] - (long)*ppvVar32);
      }
    }
LAB_0010a3f6:
    if (*(void ***)(ulong *)((long)__ptr_01 + lVar31) < ppvVar32) {
      *puVar26 = (ulong)(ppvVar32 + -4);
      if (local_d8 == 1) goto LAB_0010a523;
LAB_0010a479:
      uVar28 = *puVar26;
      uVar29 = 1;
      uVar30 = local_d8;
      uVar35 = 1;
      do {
        while( true ) {
          uVar4 = __ptr_02[uVar35];
          iVar23 = compare(uVar28,*(undefined8 *)((long)__ptr_00 + uVar4 * 8));
          if ((-1 < iVar23) && ((iVar23 != 0 || (uVar4 <= local_e0)))) break;
          bVar36 = uVar35 <= uVar29;
          uVar30 = uVar35;
          uVar35 = uVar29 + uVar35 >> 1;
          if (bVar36) goto LAB_0010a4da;
        }
        uVar29 = uVar35 + 1;
        uVar35 = uVar29 + uVar30 >> 1;
      } while (uVar29 < uVar30);
LAB_0010a4da:
      lVar31 = uVar29 - 1;
      if (lVar31 != 0) {
        memmove(__ptr_02,__ptr_02 + 1,lVar31 * 8);
      }
      __ptr_02[lVar31] = local_e0;
      goto LAB_0010a3b0;
    }
    puVar34 = (undefined8 *)(lVar31 + (long)param_6);
    puVar33 = (undefined8 *)(local_e0 * 0x10 + param_1);
    ppvVar32 = (void **)((long)__ptr + local_e0 * 0x38);
    cVar22 = fillbuf(ppvVar32,*puVar34,*puVar33);
    if (cVar22 != '\0') {
      pvVar25 = ppvVar32[3];
      pvVar24 = *ppvVar32;
      *puVar26 = ((long)pvVar25 + (long)pvVar24) - 0x20;
      *(ulong *)((long)__ptr_01 + lVar31) =
           (long)pvVar25 + (long)pvVar24 + (long)ppvVar32[2] * -0x20;
      if (local_d8 != 1) goto LAB_0010a479;
LAB_0010a523:
      *__ptr_02 = local_e0;
      goto LAB_0010a3b0;
    }
    puVar26 = __ptr_02 + 1;
    if (local_d8 != 1) {
      do {
        if (local_e0 < *puVar26) {
          *puVar26 = *puVar26 - 1;
          local_e0 = *__ptr_02;
        }
        puVar26 = puVar26 + 1;
      } while (puVar26 != __ptr_02 + local_d8);
      puVar33 = (undefined8 *)(local_e0 * 0x10 + param_1);
      puVar34 = (undefined8 *)((long)param_6 + local_e0 * 8);
      ppvVar32 = (void **)((long)__ptr + local_e0 * 0x38);
    }
    uVar29 = local_d8 - 1;
    xfclose(*puVar34,*puVar33);
    if (local_e0 < local_98) {
      local_98 = local_98 - 1;
      zaptemp(*puVar33);
    }
    free(*ppvVar32);
    if (local_e0 < uVar29) {
      lVar31 = local_e0 * 8;
      pvVar25 = (void *)(param_1 + local_e0 * 0x10);
      if (((void *)((long)param_6 + lVar31) < (void *)(uVar29 * 0x10 + param_1)) &&
         (pvVar25 < (void *)((long)param_6 + local_d8 * 8))) {
        puVar27 = (undefined4 *)((long)__ptr + local_e0 * 0x38);
        do {
          puVar2 = (undefined4 *)(param_1 + 0x10 + lVar31 * 2);
          uVar5 = *puVar2;
          uVar6 = puVar2[1];
          uVar7 = puVar2[2];
          uVar8 = puVar2[3];
          uVar9 = puVar27[0xe];
          uVar10 = puVar27[0xf];
          uVar11 = puVar27[0x10];
          uVar12 = puVar27[0x11];
          uVar13 = puVar27[0x12];
          uVar14 = puVar27[0x13];
          uVar15 = puVar27[0x14];
          uVar16 = puVar27[0x15];
          *(undefined8 *)((long)param_6 + lVar31) = *(undefined8 *)((long)param_6 + lVar31 + 8);
          uVar3 = *(undefined8 *)(puVar27 + 0x1a);
          uVar17 = puVar27[0x16];
          uVar18 = puVar27[0x17];
          uVar19 = puVar27[0x18];
          uVar20 = puVar27[0x19];
          puVar2 = (undefined4 *)(param_1 + lVar31 * 2);
          *puVar2 = uVar5;
          puVar2[1] = uVar6;
          puVar2[2] = uVar7;
          puVar2[3] = uVar8;
          *(undefined8 *)(puVar27 + 0xc) = uVar3;
          uVar3 = *(undefined8 *)((long)__ptr_00 + lVar31 + 8);
          *puVar27 = uVar9;
          puVar27[1] = uVar10;
          puVar27[2] = uVar11;
          puVar27[3] = uVar12;
          *(undefined8 *)((long)__ptr_00 + lVar31) = uVar3;
          uVar3 = *(undefined8 *)((long)__ptr_01 + lVar31 + 8);
          puVar27[4] = uVar13;
          puVar27[5] = uVar14;
          puVar27[6] = uVar15;
          puVar27[7] = uVar16;
          *(undefined8 *)((long)__ptr_01 + lVar31) = uVar3;
          lVar31 = lVar31 + 8;
          puVar27[8] = uVar17;
          puVar27[9] = uVar18;
          puVar27[10] = uVar19;
          puVar27[0xb] = uVar20;
          puVar27 = puVar27 + 0xe;
        } while (local_d8 * 8 + -8 != lVar31);
      }
      else {
        lVar1 = lVar31 + 8;
        __n = (uVar29 - local_e0) * 8;
        memmove((void *)((long)param_6 + lVar31),(void *)((long)param_6 + lVar1),__n);
        memmove(pvVar25,(void *)(param_1 + 0x10 + local_e0 * 0x10),(uVar29 - local_e0) * 0x10);
        memmove((void *)(local_e0 * 0x38 + (long)__ptr),
                (void *)((long)__ptr + local_e0 * 0x38 + 0x38),(local_d8 - local_e0) * 0x38 - 0x38);
        memmove((void *)((long)__ptr_00 + lVar31),(void *)((long)__ptr_00 + lVar1),__n);
        memmove((void *)((long)__ptr_01 + lVar31),(void *)((long)__ptr_01 + lVar1),__n);
      }
    }
    if (uVar29 != 0) {
      memmove(__ptr_02,__ptr_02 + 1,uVar29 * 8);
      local_d8 = uVar29;
      goto LAB_0010a3b0;
    }
    if ((local_b0 != (void **)0x0) && (unique != '\0')) {
      write_line(&local_68,param_4,param_5);
      free(local_68);
    }
  }
LAB_0010a86f:
  xfclose(param_4,param_5);
  free(param_6);
  free(__ptr);
  free(__ptr_02);
  free(__ptr_01);
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  free(__ptr_00);
  return;
}