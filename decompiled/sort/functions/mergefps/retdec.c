void mergefps(int32_t * files, int64_t ntemps, int64_t nfiles, struct _IO_FILE * ofp, char * output_file, struct _IO_FILE ** fps) {
    int64_t v1 = __readfsqword(40); // 0xa19a
    char * v2 = xnmalloc(nfiles, 56); // 0xa1ad
    char * v3 = xnmalloc(nfiles, 8); // 0xa1bf
    char * v4 = xnmalloc(nfiles, 8); // 0xa1cf
    char * v5 = xnmalloc(nfiles, 8); // 0xa1e1
    int64_t v6 = 0; // bp-104, 0xa1e6
    int64_t v7; // 0xa160
    uint64_t v8; // 0xa160
    int64_t v9; // 0xa160
    int64_t v10; // 0xa160
    int32_t * v11; // bp-176, 0xa160
    int64_t v12; // 0xa160
    int64_t v13; // 0xa160
    int64_t v14; // 0xa160
    int64_t v15; // 0xa7dd
    int64_t v16; // 0xa160
    if (nfiles != 0) {
        // 0xa20a
        v11 = (int32_t *)v5;
        v16 = (int64_t)files;
        v13 = (int64_t)fps;
        v12 = nfiles;
        v9 = ntemps;
        v7 = 0;
        while (true) {
          lab_0xa218:;
            int64_t v17 = v9;
            uint64_t v18 = v12;
            v8 = v7;
            uint64_t v19 = sort_size / v18;
            uint64_t v20 = merge_buffer_size; // 0xa22b
            int64_t v21 = 56 * v8 + (int64_t)v2; // 0xa244
            int64_t * v22 = (int64_t *)v21;
            uint64_t v23 = (v19 < v20 ? v20 : v19) + 32 & -32; // 0xa24c
            int64_t v24 = function_3760(); // 0xa253
            *v22 = v24;
            while (v24 == 0) {
                // 0xaa41
                if (v23 < 67) {
                    // break (via goto) -> 0xaa51
                    goto lab_0xaa51;
                }
                v23 = v23 / 2 + 32 & -32;
                v24 = function_3760();
                *v22 = v24;
            }
            int64_t * v25 = (int64_t *)(v21 + 24); // 0xa26d
            *v25 = v23;
            *(char *)(v21 + 48) = 0;
            int64_t v26 = 8 * v8; // 0xa27f
            *(int64_t *)(v21 + 40) = 32;
            uint64_t v27 = 16 * v8 + v16; // 0xa290
            int64_t * v28 = (int64_t *)(v21 + 16); // 0xa299
            *v28 = 0;
            int64_t * v29 = (int64_t *)v27;
            *(int64_t *)(v21 + 32) = 0;
            uint64_t v30 = v26 + v13; // 0xa2af
            *(int64_t *)(v21 + 8) = 0;
            int64_t * v31 = (int64_t *)v30;
            while (fillbuf((int32_t *)v21, (struct _IO_FILE *)*v31, (char *)*v29)) {
                int64_t v32 = *v22 + *v25; // 0xa2d5
                *(int64_t *)(v26 + (int64_t)v3) = v32 - 32;
                *(int64_t *)(v26 + (int64_t)v4) = v32 - 32 * *v28;
                int64_t v33 = v8 + 1; // 0xa2f6
                if (v33 >= v18) {
                    goto lab_0xa318;
                }
                v8 = v33;
                v19 = sort_size / v18;
                v20 = merge_buffer_size;
                v21 = 56 * v8 + (int64_t)v2;
                v22 = (int64_t *)v21;
                v23 = (v19 < v20 ? v20 : v19) + 32 & -32;
                v24 = function_3760();
                *v22 = v24;
                while (v24 == 0) {
                    // 0xaa41
                    if (v23 < 67) {
                        // break (via goto) -> 0xaa51
                        goto lab_0xaa51;
                    }
                    v23 = v23 / 2 + 32 & -32;
                    v24 = function_3760();
                    *v22 = v24;
                }
                // 0xa265
                v25 = (int64_t *)(v21 + 24);
                *v25 = v23;
                *(char *)(v21 + 48) = 0;
                v26 = 8 * v8;
                *(int64_t *)(v21 + 40) = 32;
                v27 = 16 * v8 + v16;
                v28 = (int64_t *)(v21 + 16);
                *v28 = 0;
                v29 = (int64_t *)v27;
                *(int64_t *)(v21 + 32) = 0;
                v30 = v26 + v13;
                *(int64_t *)(v21 + 8) = 0;
                v31 = (int64_t *)v30;
            }
            // 0xa7b8
            xfclose((struct _IO_FILE *)*v31, (char *)*v29);
            int64_t v34 = v17; // 0xa7c9
            if (v17 > v8) {
                // 0xa94f
                zaptemp((char *)*v29);
                v34 = v17 - 1;
            }
            // 0xa7cf
            v10 = v34;
            function_3750();
            v15 = v18 - 1;
            if (v15 <= v8) {
                // 0xa9a1
                if (v15 == 0) {
                    goto lab_0xa86f;
                } else {
                    goto lab_0xa318;
                }
            }
            if (v27 >= 8 * v15 + v13) {
                // 0xa80d
                function_3c70();
                function_3c70();
                goto lab_0xa850;
            } else {
                if (v30 < 16 * v18 + v16) {
                    int64_t v35 = 8 * v18 - 8; // 0xa972
                    int64_t v36 = v26; // 0xa97f
                    int64_t v37 = v36;
                    int64_t v38 = 2 * v37; // 0xa980
                    int128_t v39 = *(int128_t *)(v38 + v14); // 0xa980
                    int128_t v40 = __asm_movdqu(v39); // 0xa980
                    int64_t v41 = v37 + v13;
                    int64_t v42 = *(int64_t *)(v41 + 8); // 0xa986
                    int128_t v43 = *(int128_t *)(v38 + v16); // 0xa98b
                    __asm_movups(v43, v40);
                    *(int64_t *)v41 = v42;
                    int64_t v44 = v37 + 8; // 0xa993
                    v36 = v44;
                    while (v44 != v35) {
                        // 0xa980
                        v37 = v36;
                        v38 = 2 * v37;
                        v39 = *(int128_t *)(v38 + v14);
                        v40 = __asm_movdqu(v39);
                        v41 = v37 + v13;
                        v42 = *(int64_t *)(v41 + 8);
                        v43 = *(int128_t *)(v38 + v16);
                        __asm_movups(v43, v40);
                        *(int64_t *)v41 = v42;
                        v44 = v37 + 8;
                        v36 = v44;
                    }
                    goto lab_0xa850;
                } else {
                    // 0xa80d
                    function_3c70();
                    function_3c70();
                    goto lab_0xa850;
                }
            }
        }
      lab_0xaa51:
        // 0xaa51
        xalloc_die();
        // 0xaa56
        function_3950();
        return;
    }
    goto lab_0xa86f;
  lab_0xa30e:
    // 0xa30e
    goto lab_0xa318;
  lab_0xa850:
    // 0xa850
    v12 = v15;
    v9 = v10;
    v7 = v8;
    goto lab_0xa218;
  lab_0xa86f:
    // 0xa86f
    xfclose(ofp, output_file);
    function_3750();
    function_3750();
    function_3750();
    function_3750();
    if (v1 == __readfsqword(40)) {
        // 0xa8bb
        function_3750();
        return;
    }
    // 0xaa56
    function_3950();
  lab_0xa318:;
    // 0xa318
    int64_t v45; // 0xa160
    int64_t v46 = v45;
    int64_t v47; // 0xa160
    *(int64_t *)(8 * v46 + v47) = v46;
    int64_t v48 = v46 + 1; // 0xa31d
    v45 = v48;
    int64_t v49; // 0xa160
    int64_t v50; // 0xa160
    if (v48 != v50) {
        goto lab_0xa318;
    } else {
        // 0xa326
        v49 = 1;
        if (v50 == 1) {
            goto lab_0xa38e;
        } else {
            goto lab_0xa340;
        }
    }
  lab_0xa38e:
    // 0xa38e
    v11 = NULL;
    int32_t * v51 = (int32_t *)&v6; // bp-136, 0xa3a8
    int64_t v52 = v50; // 0xa3ad
    int64_t v53; // 0xa160
    int64_t v54 = v53; // 0xa3ad
    int64_t v55 = 0; // 0xa3ad
    goto lab_0xa3b0;
  lab_0xa340:;
    int64_t v127 = v49;
    int64_t v128 = 8 * v127 + v47;
    int64_t * v129 = (int64_t *)v128; // 0xa355
    int64_t v130 = *v129; // 0xa355
    int64_t * v131 = (int64_t *)(v128 - 8); // 0xa359
    int64_t v132 = *v131; // 0xa359
    int64_t v64; // 0xa160
    int64_t v133 = *(int64_t *)(8 * v132 + v64); // 0xa360
    if ((int32_t)compare(v133, *(int64_t *)(8 * v130 + v64)) < 1) {
        int64_t v134 = v127 + 1; // 0xa380
        int64_t v135 = v134; // 0xa389
        if (v134 < v50) {
            // 0xa340
            v49 = v135;
            goto lab_0xa340;
        } else {
            goto lab_0xa38e;
        }
    } else {
        // 0xa371
        *v131 = v130;
        *v129 = v132;
        // 0xa340
        v49 = 1;
        goto lab_0xa340;
    }
  lab_0xa3b0:;
    int64_t v56 = v54;
    int64_t v57 = v52;
    int64_t v58 = v55; // 0xa160
    goto lab_0xa3b0_2;
  lab_0xa3b0_2:;
    int64_t v59 = v58;
    int32_t * v60; // 0xa160
    uint64_t v61 = *(int64_t *)v60; // 0xa3b0
    int64_t v62 = 8 * v61; // 0xa3bb
    int64_t * v63 = (int64_t *)(v62 + v64); // 0xa3cc
    uint64_t v65 = *v63; // 0xa3cc
    int64_t v66; // 0xa160
    if (*(char *)&unique == 0) {
        // 0xa538
        write_line((int32_t *)v65, ofp, output_file);
        v66 = v59;
        goto lab_0xa3f6;
    } else {
        int32_t * v67 = v11; // 0xa3d5
        if (v67 == NULL) {
            goto lab_0xa564;
        } else {
            // 0xa3e3
            v66 = v59;
            if ((int32_t)compare((int64_t)v67, v65) != 0) {
                // 0xa550
                write_line(v51, ofp, output_file);
                goto lab_0xa564;
            } else {
                goto lab_0xa3f6;
            }
        }
    }
  lab_0xa3f6:;
    int64_t v68 = v66;
    int64_t v69; // 0xa1cf
    int64_t * v70 = (int64_t *)(v62 + v69); // 0xa3ff
    int64_t v71; // 0xa160
    int64_t v72; // 0xa160
    int64_t v73; // 0xa160
    int64_t v74; // 0xa1ad
    int64_t v75; // 0xa5d9
    if (*v70 < v65) {
        int64_t v76 = v65 - 32; // 0xa510
        *v63 = v76;
        v71 = v76;
        if (v57 != 1) {
            goto lab_0xa479;
        } else {
            // 0xa523
            *(int64_t *)v60 = v61;
            // 0xa3b0
            v58 = v68;
            goto lab_0xa3b0_2;
        }
    } else {
        int64_t * v77 = (int64_t *)(v62 + v13);
        int64_t v78 = *(int64_t *)(16 * v61 + v16); // 0xa435
        int64_t v79 = 56 * v61 + v74; // 0xa438
        if (!fillbuf((int32_t *)v79, (struct _IO_FILE *)*v77, (char *)v78)) {
            // 0xa5d0
            v75 = 8 * v57;
            v72 = v47 + 8;
            v73 = v61;
            if (v57 == 1) {
                goto lab_0xa63a;
            } else {
                goto lab_0xa5f0;
            }
        } else {
            int64_t v80 = *(int64_t *)v79 + *(int64_t *)(v79 + 24); // 0xa454
            *v63 = v80 - 32;
            *v70 = v80 - 32 * *(int64_t *)(v79 + 16);
            if (v57 == 1) {
                // 0xa523
                *(int64_t *)v60 = v61;
                // 0xa3b0
                v58 = v68;
                goto lab_0xa3b0_2;
            } else {
                // 0xa450
                v71 = *v63;
                goto lab_0xa479;
            }
        }
    }
  lab_0xa564:;
    uint64_t v81 = *(int64_t *)(v65 + 8); // 0xa564
    int64_t v82 = v59; // 0xa575
    int64_t v83 = v59; // 0xa575
    if (v81 > v59) {
        goto lab_0xa8e8;
    } else {
        goto lab_0xa57b;
    }
  lab_0xa8e8:;
    int64_t v84 = v83;
    int64_t v85 = v81; // 0xa8eb
    if (v84 != 0) {
        int64_t v86 = 2 * v84; // 0xa8e0
        v83 = v86;
        v85 = v86;
        if (v81 > v86) {
            goto lab_0xa8e8;
        } else {
            goto lab_0xa8ed;
        }
    } else {
        goto lab_0xa8ed;
    }
  lab_0xa57b:
    // 0xa57b
    function_3b30();
    *(int64_t *)&v11 = (int64_t)v51;
    v66 = v82;
    goto lab_0xa3f6;
  lab_0xa479:;
    int64_t v87 = v71; // 0xa479
    int64_t v88 = v57; // 0xa491
    int64_t v89 = 1; // 0xa491
    int64_t v90 = 1; // 0xa491
    goto lab_0xa4b2;
  lab_0xa8ed:
    // 0xa8ed
    function_3750();
    v6 = xmalloc();
    v82 = v85;
    goto lab_0xa57b;
  lab_0xa4b2:;
    int64_t v91 = v90;
    int64_t v92 = v89;
    uint64_t v93 = *(int64_t *)(8 * v91 + v47); // 0xa4b9
    int32_t v94 = compare(v87, *(int64_t *)(8 * v93 + v64)); // 0xa4c6
    int64_t v95 = v92; // 0xa4c8
    int64_t v96 = v91; // 0xa4c8
    int64_t v97; // 0xa160
    uint64_t v98; // 0xa160
    if (v94 >= 0) {
        // 0xa498
        v98 = v88;
        v97 = v91;
        v95 = v92;
        v96 = v91;
        if (v94 == 0 == v61 < v93) {
            goto lab_0xa4ca;
        } else {
            goto lab_0xa4a1;
        }
    } else {
        goto lab_0xa4ca;
    }
  lab_0xa63a:;
    // 0xa63a
    int64_t v99; // 0xa160
    uint64_t v100 = v99;
    int64_t * v101 = (int64_t *)(16 * v99 + v16);
    int64_t v102 = *(int64_t *)(8 * v99 + v13); // 0xa642
    xfclose((struct _IO_FILE *)v102, (char *)*v101);
    int64_t v103 = v56; // 0xa65c
    if (v56 > v100) {
        // 0xa913
        zaptemp((char *)*v101);
        v103 = v56 - 1;
        goto lab_0xa662;
    } else {
        goto lab_0xa662;
    }
  lab_0xa5f0:;
    int64_t * v104 = (int64_t *)v72; // 0xa5f0
    uint64_t v105 = *v104; // 0xa5f0
    int64_t v106 = v73; // 0xa5f6
    if (v105 > v73) {
        // 0xa5f8
        *v104 = v105 - 1;
        v106 = *(int64_t *)v60;
        goto lab_0xa603;
    } else {
        goto lab_0xa603;
    }
  lab_0xa4ca:
    // 0xa4ca
    v88 = v96;
    v89 = v95;
    v90 = (v96 + v95) / 2;
    int64_t v107 = v95; // 0xa4d8
    if (v95 < v96) {
        goto lab_0xa4b2;
    } else {
        goto lab_0xa4da;
    }
  lab_0xa662:;
    int64_t v108 = v57 - 1; // 0xa645
    int64_t v109 = v103;
    function_3750();
    int64_t v110; // 0xa160
    int64_t v111; // 0xa160
    if (v100 < v108) {
        int64_t v112 = 8 * v100; // 0xa699
        if (v112 + v13 >= 16 * v108 + v16) {
            goto lab_0xa785_2;
        } else {
            if (16 * v100 + v16 < v75 + v13) {
                // 0xa9bd
                v110 = 56 * v100 + v74;
                v111 = v112;
                goto lab_0xa9e8;
            } else {
                goto lab_0xa785_2;
            }
        }
    } else {
        goto lab_0xa785;
    }
  lab_0xa603:
    // 0xa603
    v99 = v106;
    int64_t v113 = v72 + 8; // 0xa603
    v72 = v113;
    v73 = v99;
    if (v113 != v75 + v47) {
        goto lab_0xa5f0;
    } else {
        // 0xa60c
        goto lab_0xa63a;
    }
  lab_0xa4a1:;
    int64_t v114 = v97 + 1; // 0xa4a1
    v107 = v114;
    if (v114 >= v98) {
        goto lab_0xa4da;
    } else {
        int64_t v115 = (v114 + v98) / 2; // 0xa4aa
        uint64_t v116 = *(int64_t *)(8 * v115 + v47); // 0xa4b9
        int32_t v117 = compare(v87, *(int64_t *)(8 * v116 + v64)); // 0xa4c6
        v95 = v114;
        v96 = v115;
        if (v117 >= 0) {
            // 0xa498
            v97 = v115;
            v95 = v114;
            v96 = v115;
            if (v117 == 0 == v61 < v116) {
                goto lab_0xa4ca;
            } else {
                goto lab_0xa4a1;
            }
        } else {
            goto lab_0xa4ca;
        }
    }
  lab_0xa4da:;
    int64_t v118 = v107 - 1; // 0xa4de
    if (v118 == 0) {
        // 0xa503
        *(int64_t *)(8 * v118 + v47) = v61;
        // 0xa3b0
        v58 = v68;
        goto lab_0xa3b0_2;
    } else {
        // 0xa4f4
        function_3c70();
        // 0xa503
        *(int64_t *)(8 * v118 + v47) = v61;
        // 0xa3b0
        v58 = v68;
        goto lab_0xa3b0_2;
    }
  lab_0xa785:
    if (v108 == 0) {
        // 0xa85a
        if (v11 == NULL) {
            goto lab_0xa86f;
        } else {
            // 0xa862
            if (*(char *)&unique != 0) {
                // 0xa926
                write_line((int32_t *)&v6, ofp, output_file);
                function_3750();
                goto lab_0xa86f;
            } else {
                goto lab_0xa86f;
            }
        }
    } else {
        goto lab_0xa790;
    }
  lab_0xa785_2:
    // 0xa785
    function_3c70();
    function_3c70();
    function_3c70();
    function_3c70();
    function_3c70();
    goto lab_0xa790;
  lab_0xa790:
    // 0xa790
    function_3c70();
    v52 = v108;
    v54 = v109;
    v55 = v68;
    goto lab_0xa3b0;
  lab_0xa9e8:;
    int64_t v119 = v110;
    int64_t v120 = v111 + 8;
    int64_t v121 = 2 * v111; // 0xa9ed
    int128_t v122 = __asm_movdqu(*(int128_t *)(v121 + v14)); // 0xa9ed
    int64_t v123 = v119 + 56; // 0xa9f3
    int128_t v124 = __asm_movdqu(*(int128_t *)v123); // 0xa9f7
    int128_t v125 = __asm_movdqu(*(int128_t *)(v119 + 72)); // 0xa9fb
    *(int64_t *)(v111 + v13) = *(int64_t *)(v120 + v13);
    int128_t v126 = __asm_movdqu(*(int128_t *)(v119 + 88)); // 0xaa08
    __asm_movups(*(int128_t *)(v121 + v16), v122);
    *(int64_t *)(v119 + 48) = *(int64_t *)(v119 + 104);
    __asm_movups(*(int128_t *)v119, v124);
    *(int64_t *)(v111 + v64) = *(int64_t *)(v120 + v64);
    __asm_movups(*(int128_t *)(v119 + 16), v125);
    *(int64_t *)(v111 + v69) = *(int64_t *)(v120 + v69);
    __asm_movups(*(int128_t *)(v119 + 32), v126);
    v110 = v123;
    v111 = v120;
    if (v75 - 8 != v120) {
        goto lab_0xa9e8;
    } else {
        goto lab_0xa785;
    }
}