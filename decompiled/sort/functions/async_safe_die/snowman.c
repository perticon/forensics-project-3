void async_safe_die(void** edi, void** rsi, ...) {
    void** r12d3;
    void** rax4;
    void** rdx5;
    void** rcx6;
    void* rsp7;
    int64_t rdi8;
    void** rcx9;
    void** rax10;
    void** rcx11;
    void** rax12;
    void** rcx13;
    void** rcx14;
    void** rcx15;
    void** r8_16;

    r12d3 = edi;
    rax4 = fun_3940(rsi);
    rdx5 = rax4;
    fun_38b0(2, rsi, rdx5, rcx6);
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 40 - 8 + 8 - 8 + 8);
    if (!r12d3) 
        goto addr_76ca_2;
    while (1) {
        *reinterpret_cast<void***>(&rdi8) = r12d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        rax10 = inttostr(rdi8, reinterpret_cast<int64_t>(rsp7) + 12, rdx5, rcx9);
        fun_38b0(2, ": errno ", 8, rcx11);
        rax12 = fun_3940(rax10, rax10);
        fun_38b0(2, rax10, rax12, rcx13);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_76ca_2:
        *reinterpret_cast<int32_t*>(&rdx5) = 1;
        *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
        fun_38b0(2, "\n", 1, rcx14);
        fun_3800(2, "\n", 1, rcx15, r8_16);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
    }
}