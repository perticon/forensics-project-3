async_safe_die (int errnum, char const *errstr)
{
  ignore_value (write (STDERR_FILENO, errstr, strlen (errstr)));

  /* Even if defined HAVE_STRERROR_R, we can't use it,
     as it may return a translated string etc. and even if not
     may call malloc which is unsafe.  We might improve this
     by testing for sys_errlist and using that if available.
     For now just report the error number.  */
  if (errnum)
    {
      char errbuf[INT_BUFSIZE_BOUND (errnum)];
      char *p = inttostr (errnum, errbuf);
      ignore_value (write (STDERR_FILENO, ": errno ", 8));
      ignore_value (write (STDERR_FILENO, p, strlen (p)));
    }

  ignore_value (write (STDERR_FILENO, "\n", 1));

  _exit (SORT_FAILURE);
}