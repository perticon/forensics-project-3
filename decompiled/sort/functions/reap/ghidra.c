__pid_t reap(int param_1)

{
  __pid_t _Var1;
  long lVar2;
  int *piVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  long in_FS_OFFSET;
  uint local_3c;
  undefined local_38 [8];
  __pid_t local_30;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  _Var1 = -1;
  if (param_1 != 0) {
    _Var1 = param_1;
  }
  _Var1 = waitpid(_Var1,(int *)&local_3c,(uint)(param_1 == 0));
  if (_Var1 < 0) {
    uVar4 = quotearg_style(4,compress_program);
    uVar5 = dcgettext(0,"waiting for %s [-d]",5);
    piVar3 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(2,*piVar3,uVar5,uVar4);
  }
  if (_Var1 != 0) {
    if (param_1 < 1) {
      local_30 = _Var1;
      lVar2 = hash_remove(proctab,local_38);
      if (lVar2 == 0) goto LAB_001075ba;
      *(undefined *)(lVar2 + 0xc) = 2;
    }
    if ((local_3c >> 8 & 0xff | local_3c & 0x7f) != 0) {
      uVar4 = quotearg_style(4,compress_program);
      uVar5 = dcgettext(0,"%s [-d] terminated abnormally",5);
                    /* WARNING: Subroutine does not return */
      error(2,0,uVar5,uVar4);
    }
    nprocs = nprocs + -1;
  }
LAB_001075ba:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return _Var1;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}