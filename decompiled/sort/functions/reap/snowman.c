int32_t reap(void** edi, void** rsi) {
    void** rdx3;
    void* rsp4;
    void** rax5;
    uint1_t zf6;
    int64_t rdi7;
    int64_t rcx8;
    int32_t eax9;
    void* rsp10;
    void** rsi11;
    void** rax12;
    void** rdi13;
    struct s3* rax14;
    void* rax15;
    uint32_t eax16;
    uint32_t v17;
    void** rsi18;
    void** rax19;
    void** rcx20;
    void** rax21;
    void** rdx22;
    void* rsp23;
    void** rax24;
    void** rax25;
    void** r8_26;

    *reinterpret_cast<uint32_t*>(&rdx3) = 0;
    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 48);
    rax5 = g28;
    zf6 = reinterpret_cast<uint1_t>(edi == 0);
    *reinterpret_cast<void***>(&rdi7) = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
    if (!zf6) {
        *reinterpret_cast<void***>(&rdi7) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
    }
    *reinterpret_cast<unsigned char*>(&rdx3) = zf6;
    eax9 = fun_3ca0(rdi7, reinterpret_cast<int64_t>(rsp4) + 12, rdx3, rcx8);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
    if (eax9 < 0) {
        addr_7605_4:
        rsi11 = compress_program;
        quotearg_style(4, rsi11, rdx3);
        rax12 = fun_3920();
        fun_37d0();
        rdx3 = rax12;
        fun_3c90();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_7648_5;
    } else {
        if (!eax9) 
            goto addr_75ba_7;
        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(edi) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(edi == 0))) 
            goto addr_75a1_9;
    }
    rdi13 = proctab;
    rax14 = hash_remove(rdi13, reinterpret_cast<int64_t>(rsp10) + 16, rdx3);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
    if (!rax14) {
        addr_75ba_7:
        rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
        if (rax15) {
            fun_3950();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            goto addr_7605_4;
        } else {
            return eax9;
        }
    } else {
        rax14->fc = 2;
    }
    addr_75a1_9:
    eax16 = v17;
    *reinterpret_cast<uint32_t*>(&rdx3) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax16) + 1)) | eax16 & 0x7f;
    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
    if (1) {
        addr_7648_5:
        rsi18 = compress_program;
        rax19 = quotearg_style(4, rsi18, rdx3, 4, rsi18, rdx3);
        fun_3920();
        rcx20 = rax19;
        fun_3c90();
    } else {
        --nprocs;
        goto addr_75ba_7;
    }
    rax21 = fun_3940(0, 0);
    rdx22 = rax21;
    fun_38b0(2, 0, rdx22, rcx20);
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8);
    if (!1) 
        goto addr_76ca_16;
    while (1) {
        rax24 = inttostr(2, reinterpret_cast<int64_t>(rsp23) + 12, rdx22, rcx20);
        fun_38b0(2, ": errno ", 8, rcx20);
        rax25 = fun_3940(rax24, rax24);
        fun_38b0(2, rax24, rax25, rcx20);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_76ca_16:
        *reinterpret_cast<int32_t*>(&rdx22) = 1;
        *reinterpret_cast<int32_t*>(&rdx22 + 4) = 0;
        fun_38b0(2, "\n", 1, rcx20);
        fun_3800(2, "\n", 1, rcx20, r8_26);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8);
    }
}