int32_t reap(int32_t pid) {
    // 0x7560
    int32_t v1; // 0x7560
    uint32_t v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x756d
    int32_t result = function_3ca0(); // 0x7594
    if (result < 0) {
        goto lab_0x7605;
    } else {
        if (result == 0) {
            goto lab_0x75ba;
        } else {
            if (pid < 1) {
                // 0x75e0
                int64_t v4; // bp-56, 0x7560
                char * v5 = hash_remove(proctab, (int32_t *)&v4); // 0x75f0
                if (v5 == NULL) {
                    goto lab_0x75ba;
                } else {
                    // 0x75fa
                    *(char *)((int64_t)v5 + 12) = 2;
                    goto lab_0x75a1;
                }
            } else {
                goto lab_0x75a1;
            }
        }
    }
  lab_0x7605:
    // 0x7605
    quotearg_style();
    function_3920();
    function_37d0();
    function_3c90();
    // 0x7648
    quotearg_style();
    function_3920();
    return function_3c90();
  lab_0x75ba:
    // 0x75ba
    if (v3 == __readfsqword(40)) {
        // 0x75ca
        return result;
    }
    // 0x7600
    function_3950();
    goto lab_0x7605;
  lab_0x75a1:
    if ((v2 / 256 % 256 || v2 % 128) != 0) {
        // 0x7648
        quotearg_style();
        function_3920();
        return function_3c90();
    }
    // 0x75b3
    nprocs--;
    goto lab_0x75ba;
}