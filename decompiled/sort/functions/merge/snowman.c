void merge(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** rbp6;
    void* rsp7;
    void** rsi8;
    void** v9;
    void** r12_10;
    void** v11;
    void** rax12;
    void** v13;
    void** v14;
    void** rbx15;
    void** r9_16;
    void** v17;
    void** r13_18;
    void** rbp19;
    void** r15_20;
    void** rsi21;
    uint32_t eax22;
    uint32_t r14d23;
    void** eax24;
    void** eax25;
    int32_t eax26;
    void** rax27;
    int32_t eax28;
    int64_t rax29;
    int64_t v30;
    void** rdi31;
    int32_t eax32;
    int64_t rax33;
    int64_t v34;
    int64_t rax35;
    int64_t v36;
    void** r14_37;
    void** rax38;
    void** v39;
    void** r8_40;
    void** v41;
    void** r13_42;
    void** v43;
    void** rdx44;
    void** rax45;
    void* rsp46;
    void** rbx47;
    void** rax48;
    void* rsp49;
    void** rax50;
    void** rbx51;
    void** rbp52;
    void** rsi53;
    void** rdi54;
    void*** v55;
    int32_t esi56;
    void** rax57;
    void** v58;
    void** v59;
    void** r8_60;
    void** rbx61;
    struct s1* r13_62;
    void** rdx63;
    void** rax64;
    void** rdi65;
    void** rdx66;
    void** rax67;
    void** rsi68;
    void** v69;
    void** r8_70;
    void** rax71;
    void** rdx72;
    void** r10_73;
    void** rax74;
    void** rax75;
    void** rsi76;
    void** rdx77;
    void** r8_78;
    void** v79;
    void** rax80;
    void** v81;
    void** rdx82;
    void** rdi83;
    void** rsi84;
    void* rsp85;
    void* rax86;
    uint64_t r15_87;
    void** r13_88;
    void* rsp89;
    void** r14_90;
    void** r12_91;
    void** rcx92;
    void** v93;
    void** v94;
    void** v95;
    void** rax96;
    void** v97;
    void** v98;
    void** r8_99;
    uint64_t rax100;
    void** r15_101;
    void* r9_102;
    int32_t eax103;
    void** rax104;
    void** rcx105;
    int64_t v106;
    void*** r14_107;
    void** rbp108;
    void** rdi109;
    void** rsi110;
    void** rdi111;
    void** rax112;
    void** rbx113;
    void** rax114;
    void** v115;
    int64_t rax116;
    int32_t ecx117;
    void** rdx118;
    void** rdi119;
    void** v120;
    void** r9_121;
    void** v122;
    void** rax123;
    void** r10_124;
    void** v125;
    void** r15_126;
    void** rbp127;
    void** rbx128;
    void** r12_129;
    void** rax130;
    void** rdi131;
    void** rdi132;
    void** rdi133;
    void** rax134;
    void** r15_135;
    void** r11_136;
    uint32_t r8d137;
    void** r9_138;
    void** rdx139;
    void** v140;
    void** r15_141;
    void** rbx142;
    void** r12_143;
    void** rax144;
    void** r11_145;
    void** r12_146;
    void** v147;
    void** r15_148;
    void** rbp149;
    void** r12_150;
    void** rdi151;
    int1_t cf152;
    void** rdi153;
    void** r12_154;
    void** rdi155;
    void** v156;
    void** r15_157;
    void** rbp158;
    void** r12_159;
    void** rdi160;
    int1_t cf161;
    void** r9_162;
    void** rax163;
    void** rax164;
    void** rdi165;
    void* rax166;
    int64_t v167;
    void** rbp168;
    void** rax169;

    r14_5 = rdi;
    rbp6 = rsi;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xe8);
    *reinterpret_cast<uint32_t*>(&rsi8) = nmerge;
    *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
    v9 = rdx;
    r12_10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 56);
    v11 = rcx;
    rax12 = g28;
    v13 = rax12;
    if (reinterpret_cast<unsigned char>(rsi8) < reinterpret_cast<unsigned char>(rdx)) 
        goto addr_ab58_2;
    while (1) {
        addr_ac45_3:
        if (reinterpret_cast<unsigned char>(rbp6) < reinterpret_cast<unsigned char>(v9)) {
            v14 = rbp6;
            rbx15 = rbp6;
            r9_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) << 4);
            v17 = r14_5;
            r13_18 = v11;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            rbp19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<unsigned char>(r9_16));
            r15_20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 64);
            do {
                addr_aca9_5:
                rsi21 = *reinterpret_cast<void***>(rbp19);
                eax22 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi21) - 45);
                r14d23 = eax22;
                if (!eax22) {
                    r14d23 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi21 + 1));
                }
                if (!r13_18) 
                    goto addr_acd3_8;
                eax24 = fun_3ad0(r13_18, rsi21, rdx);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                if (eax24) 
                    goto addr_acd3_8;
                if (r14d23) 
                    goto addr_ad16_11;
                addr_acd3_8:
                eax25 = outstat_errno_4;
                if (!eax25) {
                    eax26 = fun_3e50();
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                    if (eax26) {
                        rax27 = fun_37d0();
                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                        eax25 = *reinterpret_cast<void***>(rax27);
                        outstat_errno_4 = eax25;
                        goto addr_ace1_14;
                    } else {
                        outstat_errno_4 = reinterpret_cast<void**>(0xffffffff);
                    }
                } else {
                    addr_ace1_14:
                    if (reinterpret_cast<signed char>(eax25) >= reinterpret_cast<signed char>(0)) 
                        break;
                }
                if (!r14d23) {
                    eax28 = fun_3e50();
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                    if (eax28) 
                        goto addr_ac96_18;
                    rax29 = g1d248;
                    if (v30 != rax29) 
                        goto addr_ac96_18;
                } else {
                    rdi31 = *reinterpret_cast<void***>(rbp19);
                    eax32 = fun_3b10(rdi31, r15_20, rdx);
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                    if (eax32) 
                        goto addr_ac96_18;
                    rax33 = g1d248;
                    if (v34 != rax33) 
                        goto addr_ac96_18;
                }
                rax35 = outstat_3;
                if (v36 != rax35) {
                    addr_ac96_18:
                    ++rbx15;
                    rbp19 = rbp19 + 16;
                    if (rbx15 == v9) 
                        break; else 
                        goto addr_aca9_5;
                } else {
                    addr_ad16_11:
                    r14_37 = r12_10 + 13;
                    if (!r12_10) {
                        rax38 = maybe_create_temp(reinterpret_cast<int64_t>(rsp7) + 56, 0, rdx);
                        rcx = v39;
                        r14_37 = rax38 + 13;
                        *reinterpret_cast<int32_t*>(&rdx) = 1;
                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                        r12_10 = rax38;
                        r8_40 = r14_37;
                        mergefiles(rbp19, 0, 1, rcx, r8_40);
                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
                    }
                }
                *reinterpret_cast<void***>(rbp19) = r14_37;
                ++rbx15;
                rbp19 = rbp19 + 16;
                *reinterpret_cast<void***>(rbp19 + 0xfffffffffffffff8) = r12_10;
            } while (rbx15 != v9);
            r14_5 = v17;
            rbp6 = v14;
        }
        v41 = rbp6;
        r13_42 = v9;
        v43 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 48);
        while (1) {
            rdx44 = v43;
            rax45 = open_input_files(r14_5, r13_42, rdx44);
            rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            rbx47 = rax45;
            if (r13_42 == rax45) {
                rax48 = stream_open(v11, "w", rdx44);
                rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8);
                if (rax48) 
                    goto addr_afa3_29;
                rax50 = fun_37d0();
                rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax50) == 24)) 
                    break;
                if (reinterpret_cast<unsigned char>(r13_42) <= reinterpret_cast<unsigned char>(2)) 
                    break;
            } else {
                if (reinterpret_cast<unsigned char>(rax45) <= reinterpret_cast<unsigned char>(2)) 
                    goto addr_afe8_33;
            }
            r15_20 = rbx47 + 0xffffffffffffffff;
            rbx51 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp46) + 56);
            rbp52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_20) << 4));
            while (rsi53 = *reinterpret_cast<void***>(rbp52), r12_10 = rbp52, rdi54 = v55[reinterpret_cast<unsigned char>(r15_20) * 8], xfclose(rdi54, rsi53, rdx44), esi56 = 0, *reinterpret_cast<unsigned char*>(&esi56) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_20) > reinterpret_cast<unsigned char>(2)), rbp52 = rbp52 - 16, rax57 = maybe_create_temp(rbx51, esi56, rdx44), rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8), rax57 == 0) {
                --r15_20;
            }
            r9_16 = v58;
            rcx = v59;
            r8_60 = rax57 + 13;
            v9 = rax57;
            rbx61 = v41;
            if (reinterpret_cast<unsigned char>(v41) > reinterpret_cast<unsigned char>(r15_20)) {
                rbx61 = r15_20;
            }
            r13_62 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(r13_42) - reinterpret_cast<unsigned char>(r15_20));
            rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(rbx61));
            mergefps(r14_5, rbx61, r15_20, rcx, r8_60, r9_16);
            r8_40 = r8_60;
            r13_42 = reinterpret_cast<void**>(&r13_62->f1);
            *reinterpret_cast<void***>(r14_5) = r8_40;
            *reinterpret_cast<void***>(r14_5 + 8) = v9;
            fun_3c70(r14_5 + 16, r12_10, reinterpret_cast<uint64_t>(r13_62) << 4);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8);
            v41 = rbp6 + 1;
        }
        *reinterpret_cast<int32_t*>(&rdx63) = 5;
        *reinterpret_cast<int32_t*>(&rdx63 + 4) = 0;
        rax64 = fun_3920();
        rsi8 = v11;
        rdi65 = rax64;
        sort_die(rdi65, rsi8, 5);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8);
        while (1) {
            rdx66 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi65) + reinterpret_cast<unsigned char>(rdx63) + 1 - reinterpret_cast<unsigned char>(rsi8));
            rax67 = maybe_create_temp(r12_10, 0, rdx66);
            rsi68 = rbp6;
            rcx = v69;
            r8_70 = rax67 + 13;
            if (reinterpret_cast<unsigned char>(rdx66) <= reinterpret_cast<unsigned char>(rbp6)) {
                rsi68 = rdx66;
            }
            rax71 = mergefiles(r15_20, rsi68, rdx66, rcx, r8_70);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
            rdx72 = rbp6;
            r8_40 = r8_70;
            if (reinterpret_cast<unsigned char>(rax71) <= reinterpret_cast<unsigned char>(rbp6)) {
                rdx72 = rax71;
            }
            ++r13_42;
            rbx47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx47) + reinterpret_cast<unsigned char>(rax71));
            *reinterpret_cast<void***>(r10_73) = r8_40;
            *reinterpret_cast<void***>(r10_73 + 8) = rax67;
            rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) - reinterpret_cast<unsigned char>(rdx72));
            r15_20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx47) << 4) + reinterpret_cast<unsigned char>(r14_5));
            r10_73 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_42) << 4) + reinterpret_cast<unsigned char>(r14_5));
            do {
                rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) + reinterpret_cast<unsigned char>(r13_42));
                fun_3c70(r10_73, r15_20, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(rbx47)) << 4);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rsi8) = nmerge;
                *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                r9_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_42) - reinterpret_cast<unsigned char>(rbx47));
                v9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v9) + reinterpret_cast<unsigned char>(r9_16));
                rdx = v9;
                if (reinterpret_cast<unsigned char>(rsi8) >= reinterpret_cast<unsigned char>(rdx)) 
                    goto addr_ac45_3;
                addr_ab58_2:
                *reinterpret_cast<int32_t*>(&r13_42) = 0;
                *reinterpret_cast<int32_t*>(&r13_42 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbx47) = 0;
                *reinterpret_cast<int32_t*>(&rbx47 + 4) = 0;
                if (reinterpret_cast<unsigned char>(v9) < reinterpret_cast<unsigned char>(rsi8)) {
                    rdi65 = v9;
                    rax74 = rsi8;
                    r10_73 = r14_5;
                    r15_20 = r14_5;
                    *reinterpret_cast<int32_t*>(&rdx63) = 0;
                    *reinterpret_cast<int32_t*>(&rdx63 + 4) = 0;
                } else {
                    do {
                        rax75 = maybe_create_temp(r12_10, 0, rdx);
                        rsi76 = rbp6;
                        *reinterpret_cast<uint32_t*>(&rdx77) = nmerge;
                        *reinterpret_cast<int32_t*>(&rdx77 + 4) = 0;
                        r8_78 = rax75 + 13;
                        if (reinterpret_cast<unsigned char>(rdx77) <= reinterpret_cast<unsigned char>(rbp6)) {
                            rsi76 = rdx77;
                        }
                        rax80 = mergefiles(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx47) << 4) + reinterpret_cast<unsigned char>(r14_5), rsi76, rdx77, v79, r8_78);
                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
                        rdx = rbp6;
                        r8_40 = r8_78;
                        *reinterpret_cast<uint32_t*>(&rsi8) = nmerge;
                        *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                        if (reinterpret_cast<unsigned char>(rax80) <= reinterpret_cast<unsigned char>(rbp6)) {
                            rdx = rax80;
                        }
                        rbx47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx47) + reinterpret_cast<unsigned char>(rax80));
                        rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_42) << 4);
                        ++r13_42;
                        rdi65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(rbx47));
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<unsigned char>(rcx)) = r8_40;
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<unsigned char>(rcx) + 8) = rax75;
                        rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) - reinterpret_cast<unsigned char>(rdx));
                    } while (reinterpret_cast<unsigned char>(rsi8) <= reinterpret_cast<unsigned char>(rdi65));
                    r10_73 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<unsigned char>(rcx) + 16);
                    rdx63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_42) % reinterpret_cast<unsigned char>(rsi8));
                    r15_20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx47) << 4) + reinterpret_cast<unsigned char>(r14_5));
                    rax74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi8) - reinterpret_cast<unsigned char>(rdx63));
                }
            } while (reinterpret_cast<unsigned char>(rdi65) <= reinterpret_cast<unsigned char>(rax74));
        }
    }
    addr_afa3_29:
    r9_16 = v81;
    rcx = rax48;
    rdx82 = r13_42;
    r8_40 = v11;
    rdi83 = r14_5;
    rsi84 = v41;
    mergefps(rdi83, rsi84, rdx82, rcx, r8_40, r9_16);
    rsp85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
    rax86 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (!rax86) {
        return;
    }
    addr_b011_57:
    fun_3950();
    r15_87 = reinterpret_cast<unsigned char>(rsi84) >> 1;
    r13_88 = r8_40;
    rsp89 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp85) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    r14_90 = *reinterpret_cast<void***>(rcx + 40);
    r12_91 = *reinterpret_cast<void***>(rcx + 48);
    rcx92 = v41;
    v93 = rdx82;
    v94 = r9_16;
    v95 = rcx92;
    rax96 = g28;
    v97 = rax96;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_90) + reinterpret_cast<unsigned char>(r12_91)) <= 0x1ffff || (v98 = rsi84, reinterpret_cast<unsigned char>(rsi84) <= reinterpret_cast<unsigned char>(1))) {
        addr_b0b7_59:
        r8_99 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi83) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v93) << 5));
        rax100 = reinterpret_cast<unsigned char>(r14_90) << 5;
        r15_101 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi83) - rax100);
        r9_102 = reinterpret_cast<void*>(-rax100);
        if (reinterpret_cast<unsigned char>(r12_91) > reinterpret_cast<unsigned char>(1)) {
            *reinterpret_cast<int32_t*>(&rcx92) = 0;
            *reinterpret_cast<int32_t*>(&rcx92 + 4) = 0;
            rsi84 = r12_91;
            rdx82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_99) - (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_90) >> 1) << 5));
            sequential_sort(r15_101, rsi84, rdx82, 0);
            r9_102 = r9_102;
            r8_99 = r8_99;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rsi84) = 0;
        *reinterpret_cast<int32_t*>(&rsi84 + 4) = 0;
        rcx92 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp89) + 80);
        rdx82 = reinterpret_cast<void**>(0xb880);
        eax103 = fun_3c60(reinterpret_cast<int64_t>(rsp89) + 72);
        if (!eax103) {
            rax104 = *reinterpret_cast<void***>(rcx + 40);
            rcx105 = *reinterpret_cast<void***>(rcx + 72);
            sortlines(reinterpret_cast<unsigned char>(rdi83) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax104) << 5), reinterpret_cast<unsigned char>(v98) - r15_87, v93, rcx105, r13_88, v94, v94);
            fun_3e00(v106);
            goto addr_b722_63;
        } else {
            r14_90 = *reinterpret_cast<void***>(rcx + 40);
            r12_91 = *reinterpret_cast<void***>(rcx + 48);
            goto addr_b0b7_59;
        }
    }
    if (reinterpret_cast<unsigned char>(r14_90) > reinterpret_cast<unsigned char>(1)) {
        *reinterpret_cast<int32_t*>(&rcx92) = 0;
        *reinterpret_cast<int32_t*>(&rcx92 + 4) = 0;
        rdx82 = r8_99;
        rsi84 = r14_90;
        sequential_sort(rdi83, rsi84, rdx82, 0);
        r9_102 = r9_102;
    }
    *reinterpret_cast<void***>(rcx) = rdi83;
    r14_107 = reinterpret_cast<void***>(r13_88 + 48);
    *reinterpret_cast<void***>(rcx + 8) = r15_101;
    *reinterpret_cast<void***>(rcx + 16) = r15_101;
    *reinterpret_cast<void***>(rcx + 24) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi83) + (reinterpret_cast<int64_t>(r9_102) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_91) << 5)));
    rbp108 = r13_88 + 8;
    fun_3e80(rbp108, rsi84, rdx82, rcx92);
    rdi109 = *reinterpret_cast<void***>(r13_88);
    rsi110 = rcx;
    heap_insert(rdi109, rsi110, rdx82, rcx92);
    *reinterpret_cast<unsigned char*>(rcx + 84) = 1;
    fun_3a90(r14_107, rsi110, rdx82, rcx92);
    fun_3b80(rbp108, rsi110, rdx82, rcx92);
    while (1) {
        fun_3e80(rbp108, rsi110, rdx82, rcx92);
        while (rdi111 = *reinterpret_cast<void***>(r13_88), rax112 = heap_remove_top(rdi111, rsi110, rdx82, rcx92), rax112 == 0) {
            rsi110 = rbp108;
            fun_38e0(r14_107, rsi110, rdx82, rcx92);
        }
        rbx113 = rax112;
        fun_3b80(rbp108, rsi110, rdx82, rcx92);
        rax114 = rbx113 + 88;
        v115 = rax114;
        fun_3e80(rax114, rsi110, rdx82, rcx92);
        *reinterpret_cast<uint32_t*>(&rax116) = *reinterpret_cast<uint32_t*>(rbx113 + 80);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax116) + 4) = 0;
        *reinterpret_cast<unsigned char*>(rbx113 + 84) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax116)) 
            break;
        ecx117 = static_cast<int32_t>(rax116 + rax116 + 2);
        rdx118 = *reinterpret_cast<void***>(rbx113);
        rdi119 = *reinterpret_cast<void***>(rbx113 + 8);
        v120 = rdx118;
        r9_121 = rdx118;
        v122 = rdi119;
        rsi110 = *reinterpret_cast<void***>(rbx113 + 16);
        rcx92 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(v93) >> *reinterpret_cast<signed char*>(&ecx117)) + 1);
        if (*reinterpret_cast<uint32_t*>(&rax116) == 1) {
            rax123 = rdi119;
            if (rdx118 == rsi110) {
                *reinterpret_cast<int32_t*>(&r10_124) = 0;
                *reinterpret_cast<int32_t*>(&r10_124 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx82) = 0;
                *reinterpret_cast<int32_t*>(&rdx82 + 4) = 0;
            } else {
                v125 = rbp108;
                r15_126 = v94;
                rbp127 = rbx113;
                rbx128 = v95;
                while (*reinterpret_cast<void***>(rbp127 + 24) != rax123) {
                    r12_129 = rcx92 + 0xffffffffffffffff;
                    if (!rcx92) 
                        goto addr_b6d8_78;
                    rax130 = compare(r9_121 + 0xffffffffffffffe0, rax123 + 0xffffffffffffffe0, rdx118);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax130) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax130) == 0))) {
                        rdx118 = rbx128;
                        rsi110 = r15_126;
                        rdi131 = *reinterpret_cast<void***>(rbp127 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp127 + 8) = rdi131;
                        write_unique(rdi131, rsi110, rdx118);
                        r9_121 = *reinterpret_cast<void***>(rbp127);
                        rax123 = *reinterpret_cast<void***>(rbp127 + 8);
                        if (r9_121 == *reinterpret_cast<void***>(rbp127 + 16)) 
                            goto addr_b3f5_81;
                    } else {
                        rdx118 = rbx128;
                        rsi110 = r15_126;
                        rdi132 = *reinterpret_cast<void***>(rbp127) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp127) = rdi132;
                        write_unique(rdi132, rsi110, rdx118);
                        r9_121 = *reinterpret_cast<void***>(rbp127);
                        rax123 = *reinterpret_cast<void***>(rbp127 + 8);
                        if (r9_121 == *reinterpret_cast<void***>(rbp127 + 16)) 
                            goto addr_b3f5_81;
                    }
                    rcx92 = r12_129;
                }
                goto addr_b600_84;
            }
        } else {
            rdx82 = *reinterpret_cast<void***>(rbx113 + 32);
            rdi133 = v120;
            rax134 = v122;
            r15_135 = *reinterpret_cast<void***>(rdx82);
            r11_136 = rdi133;
            if (rdi133 == rsi110) {
                r8d137 = 0;
                *reinterpret_cast<int32_t*>(&r10_124) = 0;
                *reinterpret_cast<int32_t*>(&r10_124 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r9_138) = 0;
                *reinterpret_cast<int32_t*>(&r9_138 + 4) = 0;
                goto addr_b2e8_87;
            } else {
                rdx139 = r15_135;
                v140 = r9_121;
                r15_141 = rbx113;
                rbx142 = rdx139;
                while (*reinterpret_cast<void***>(r15_141 + 24) != rax134) {
                    r12_143 = rcx92 + 0xffffffffffffffff;
                    if (!rcx92) 
                        goto addr_b5c0_91;
                    rbx142 = rbx142 - 32;
                    rax144 = compare(r11_136 + 0xffffffffffffffe0, rax134 + 0xffffffffffffffe0, rdx139);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax144) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax144) == 0))) {
                        r11_136 = *reinterpret_cast<void***>(r15_141);
                        rsi110 = *reinterpret_cast<void***>(r15_141 + 16);
                        __asm__("movdqu xmm0, [rcx-0x20]");
                        rax134 = *reinterpret_cast<void***>(r15_141 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_141 + 8) = rax134;
                        __asm__("movups [rbx], xmm0");
                        __asm__("movdqu xmm1, [rcx-0x10]");
                        __asm__("movups [rbx+0x10], xmm1");
                        if (rsi110 == r11_136) 
                            goto addr_b2b8_94;
                    } else {
                        rsi110 = *reinterpret_cast<void***>(r15_141 + 16);
                        __asm__("movdqu xmm2, [rax-0x20]");
                        r11_136 = *reinterpret_cast<void***>(r15_141) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_141) = r11_136;
                        __asm__("movups [rbx], xmm2");
                        __asm__("movdqu xmm3, [rax-0x10]");
                        rax134 = *reinterpret_cast<void***>(r15_141 + 8);
                        __asm__("movups [rbx+0x10], xmm3");
                        if (rsi110 == r11_136) 
                            goto addr_b2b8_94;
                    }
                    rcx92 = r12_143;
                }
                goto addr_b440_97;
            }
        }
        addr_b418_98:
        r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
        rsi110 = r11_145;
        if (*reinterpret_cast<void***>(rbx113 + 48) != r10_124) {
            addr_b6a8_99:
            r11_145 = rsi110;
            if (rsi110 != rdx82 || (rax123 == *reinterpret_cast<void***>(rbx113 + 24) || !rcx92)) {
                rdi133 = r9_121;
                r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
                r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
                r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx113 + 8))) >> 5);
            } else {
                v147 = rbp108;
                r15_148 = v94;
                rbp149 = rcx92 + 0xffffffffffffffff;
                r12_150 = v95;
                do {
                    rdi151 = rax123 + 0xffffffffffffffe0;
                    rdx82 = r12_150;
                    rsi110 = r15_148;
                    *reinterpret_cast<void***>(rbx113 + 8) = rdi151;
                    write_unique(rdi151, rsi110, rdx82);
                    rax123 = *reinterpret_cast<void***>(rbx113 + 8);
                    if (rax123 == *reinterpret_cast<void***>(rbx113 + 24)) 
                        break;
                    cf152 = reinterpret_cast<unsigned char>(rbp149) < reinterpret_cast<unsigned char>(1);
                    --rbp149;
                } while (!cf152);
                rbp108 = v147;
                r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
                rdi133 = *reinterpret_cast<void***>(rbx113);
                r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
                r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
                r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax123)) >> 5);
            }
        } else {
            r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
            rdi133 = r9_121;
            r12_146 = r10_124;
        }
        addr_b301_106:
        *reinterpret_cast<void***>(rbx113 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_145) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v120) - reinterpret_cast<unsigned char>(rdi133)) >> 5));
        *reinterpret_cast<void***>(rbx113 + 48) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_146) - reinterpret_cast<unsigned char>(r10_124));
        if (!*reinterpret_cast<signed char*>(&r8d137)) {
            rsi110 = rbx113;
            queue_check_insert_part_0(r13_88, rsi110, rdx82);
        }
        if (*reinterpret_cast<uint32_t*>(rbx113 + 80) > 1) {
            rdi153 = *reinterpret_cast<void***>(rbx113 + 56) + 88;
            fun_3e80(rdi153, rsi110, rdx82, rcx92);
            rsi110 = *reinterpret_cast<void***>(rbx113 + 56);
            if (!*reinterpret_cast<unsigned char*>(rsi110 + 84)) {
                queue_check_insert_part_0(r13_88, rsi110, rdx82);
                rsi110 = *reinterpret_cast<void***>(rbx113 + 56);
            }
            fun_3b80(rsi110 + 88, rsi110, rdx82, rcx92);
        } else {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx113 + 48)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx113 + 40)))) {
                r12_154 = *reinterpret_cast<void***>(rbx113 + 56);
                fun_3e80(rbp108, rsi110, rdx82, rcx92);
                rdi155 = *reinterpret_cast<void***>(r13_88);
                rsi110 = r12_154;
                heap_insert(rdi155, rsi110, rdx82, rcx92);
                *reinterpret_cast<unsigned char*>(r12_154 + 84) = 1;
                fun_3a90(r14_107, rsi110, rdx82, rcx92);
                fun_3b80(rbp108, rsi110, rdx82, rcx92);
            }
        }
        fun_3b80(v115, rsi110, rdx82, rcx92);
        continue;
        addr_b600_84:
        rbx113 = rbp127;
        rbp108 = v125;
        addr_b608_115:
        r10_124 = *reinterpret_cast<void***>(rbx113 + 48);
        rdx82 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax123)) >> 5);
        if (rdx82 != r10_124) {
            rsi110 = *reinterpret_cast<void***>(rbx113 + 40);
            rdx82 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v120) - reinterpret_cast<unsigned char>(r9_121)) >> 5);
            goto addr_b6a8_99;
        } else {
            rdi133 = r9_121;
            if (*reinterpret_cast<void***>(rbx113 + 16) == r9_121 || !rcx92) {
                r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
                r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
                r12_146 = r10_124;
                goto addr_b301_106;
            } else {
                v156 = rbp108;
                r15_157 = v94;
                rbp158 = rcx92 + 0xffffffffffffffff;
                r12_159 = v95;
                do {
                    rdi160 = rdi133 - 32;
                    rdx82 = r12_159;
                    rsi110 = r15_157;
                    *reinterpret_cast<void***>(rbx113) = rdi160;
                    write_unique(rdi160, rsi110, rdx82);
                    rdi133 = *reinterpret_cast<void***>(rbx113);
                    if (rdi133 == *reinterpret_cast<void***>(rbx113 + 16)) 
                        break;
                    cf161 = reinterpret_cast<unsigned char>(rbp158) < reinterpret_cast<unsigned char>(1);
                    --rbp158;
                } while (!cf161);
                rbp108 = v156;
                r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
                r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx113 + 8))) >> 5);
                r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
                r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
                goto addr_b301_106;
            }
        }
        addr_b6d8_78:
        rbx113 = rbp127;
        rcx92 = reinterpret_cast<void**>(0xffffffffffffffff);
        rbp108 = v125;
        goto addr_b608_115;
        addr_b3f5_81:
        rbx113 = rbp127;
        rcx92 = r12_129;
        rbp108 = v125;
        rdx82 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v120) - reinterpret_cast<unsigned char>(r9_121)) >> 5);
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax123)) >> 5);
        goto addr_b418_98;
        addr_b2e8_87:
        r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
        r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
        if (r10_124 == r12_146) 
            goto addr_b2fe_123;
        if (r9_138 != r11_145 || ((r9_162 = *reinterpret_cast<void***>(rbx113 + 24), r9_162 == rax134) || (rsi110 = rcx92 + 0xffffffffffffffff, rcx92 == 0))) {
            addr_b2fe_123:
            *reinterpret_cast<void***>(rdx82) = r15_135;
            goto addr_b301_106;
        } else {
            rax163 = rax134 - 32;
            do {
                __asm__("movdqu xmm6, [rax]");
                r15_135 = r15_135 - 32;
                *reinterpret_cast<void***>(rbx113 + 8) = rax163;
                rcx92 = rax163;
                __asm__("movups [r15], xmm6");
                __asm__("movdqu xmm7, [rax+0x10]");
                __asm__("movups [r15+0x10], xmm7");
                if (rax163 == r9_162) 
                    break;
                --rsi110;
                rax163 = rax163 - 32;
            } while (rsi110 != 0xffffffffffffffff);
            goto addr_b7f7_128;
        }
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax163)) >> 5);
        goto addr_b2fe_123;
        addr_b7f7_128:
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rcx92)) >> 5);
        goto addr_b2fe_123;
        addr_b440_97:
        rbx113 = r15_141;
        rdi133 = *reinterpret_cast<void***>(rbx113);
        r15_135 = rbx142;
        r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
        rdx82 = *reinterpret_cast<void***>(rbx113 + 32);
        r9_138 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v140) - reinterpret_cast<unsigned char>(r11_136)) >> 5);
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax134)) >> 5);
        addr_b46d_130:
        r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
        r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
        if (r12_146 == r10_124) {
            if (rsi110 == rdi133) 
                goto addr_b2fe_123;
            rax164 = rcx92 + 0xffffffffffffffff;
            if (!rcx92) 
                goto addr_b2fe_123;
            rcx92 = rdi133 + 0xffffffffffffffe0;
            do {
                __asm__("movdqu xmm4, [rcx]");
                r15_135 = r15_135 - 32;
                *reinterpret_cast<void***>(rbx113) = rcx92;
                rdi133 = rcx92;
                __asm__("movups [r15], xmm4");
                __asm__("movdqu xmm5, [rcx+0x10]");
                __asm__("movups [r15+0x10], xmm5");
                if (rcx92 == rsi110) 
                    break;
                --rax164;
                rcx92 = rcx92 - 32;
            } while (rax164 != 0xffffffffffffffff);
            goto addr_b2fe_123;
            goto addr_b2fe_123;
        }
        addr_b5c0_91:
        rbx113 = r15_141;
        rdi133 = *reinterpret_cast<void***>(rbx113);
        r15_135 = rbx142;
        r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
        rcx92 = reinterpret_cast<void**>(0xffffffffffffffff);
        rdx82 = *reinterpret_cast<void***>(rbx113 + 32);
        r9_138 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v140) - reinterpret_cast<unsigned char>(r11_136)) >> 5);
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax134)) >> 5);
        goto addr_b46d_130;
        addr_b2b8_94:
        rbx113 = r15_141;
        r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
        r15_135 = rbx142;
        rdx82 = *reinterpret_cast<void***>(rbx113 + 32);
        rdi133 = r11_136;
        rcx92 = r12_143;
        r9_138 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v140) - reinterpret_cast<unsigned char>(r11_136)) >> 5);
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax134)) >> 5);
        goto addr_b2e8_87;
    }
    fun_3b80(v115, rsi110, rdx82, rcx92);
    fun_3e80(rbp108, rsi110, rdx82, rcx92);
    rdi165 = *reinterpret_cast<void***>(r13_88);
    heap_insert(rdi165, rbx113, rdx82, rcx92);
    *reinterpret_cast<unsigned char*>(rbx113 + 84) = 1;
    fun_3a90(r14_107, rbx113, rdx82, rcx92);
    fun_3b80(rbp108, rbx113, rdx82, rcx92);
    addr_b722_63:
    rax166 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v97) - reinterpret_cast<unsigned char>(g28));
    if (rax166) {
        fun_3950();
    } else {
        goto v167;
    }
    addr_afe8_33:
    rbp168 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax45) << 4));
    *reinterpret_cast<int32_t*>(&rdx82) = 5;
    *reinterpret_cast<int32_t*>(&rdx82 + 4) = 0;
    rax169 = fun_3920();
    rdi83 = rax169;
    rsi84 = rbp168;
    sort_die(rdi83, rsi84, 5);
    rsp85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8);
    goto addr_b011_57;
}