void merge(long *param_1,ulong param_2,ulong param_3,char *param_4)

{
  uint uVar1;
  int iVar2;
  long lVar3;
  ulong uVar4;
  ulong uVar5;
  int *piVar6;
  byte *pbVar7;
  undefined8 uVar8;
  ulong uVar9;
  ulong uVar10;
  long lVar11;
  byte **ppbVar12;
  long *plVar13;
  ulong uVar14;
  ulong uVar15;
  byte *pbVar16;
  long *__src;
  long in_FS_OFFSET;
  ulong local_110;
  ulong local_108;
  long local_e8;
  undefined8 local_e0;
  stat local_d8;
  long local_40;
  
  uVar5 = (ulong)nmerge;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_108 = param_3;
  if (uVar5 < param_3) {
    do {
      uVar4 = 0;
      lVar11 = 0;
      if (local_108 < uVar5) {
        uVar9 = 0;
        uVar10 = uVar5;
        uVar14 = local_108;
        plVar13 = param_1;
        __src = param_1;
      }
      else {
        do {
          uVar15 = uVar4;
          lVar3 = maybe_create_temp(&local_e0,0);
          uVar4 = (ulong)nmerge;
          uVar5 = param_2;
          if (uVar4 <= param_2) {
            uVar5 = uVar4;
          }
          uVar4 = mergefiles(param_1 + lVar11 * 2,uVar5,uVar4,local_e0);
          uVar10 = (ulong)nmerge;
          uVar5 = param_2;
          if (uVar4 <= param_2) {
            uVar5 = uVar4;
          }
          lVar11 = lVar11 + uVar4;
          uVar4 = uVar15 + 1;
          param_1[uVar15 * 2] = lVar3 + 0xd;
          param_1[uVar15 * 2 + 1] = lVar3;
          param_2 = param_2 - uVar5;
        } while (uVar10 <= local_108 - lVar11);
        uVar9 = uVar4 % uVar10;
        uVar5 = uVar10 - uVar9;
        uVar14 = local_108 - lVar11;
        plVar13 = param_1 + uVar15 * 2 + 2;
        __src = param_1 + lVar11 * 2;
      }
      if (uVar5 < uVar14) {
        uVar10 = (uVar14 + 1 + uVar9) - uVar10;
        lVar3 = maybe_create_temp(&local_e0,0);
        uVar5 = param_2;
        if (uVar10 <= param_2) {
          uVar5 = uVar10;
        }
        uVar10 = mergefiles(__src,uVar5,uVar10,local_e0);
        uVar5 = param_2;
        if (uVar10 <= param_2) {
          uVar5 = uVar10;
        }
        uVar4 = uVar4 + 1;
        lVar11 = lVar11 + uVar10;
        *plVar13 = lVar3 + 0xd;
        plVar13[1] = lVar3;
        param_2 = param_2 - uVar5;
        __src = param_1 + lVar11 * 2;
        plVar13 = param_1 + uVar4 * 2;
      }
      param_2 = param_2 + uVar4;
      memmove(plVar13,__src,(local_108 - lVar11) * 0x10);
      uVar5 = (ulong)nmerge;
      local_108 = local_108 + (uVar4 - lVar11);
    } while (uVar5 < local_108);
  }
  if (param_2 < local_108) {
    pbVar7 = (byte *)0x0;
    uVar5 = param_2;
    ppbVar12 = (byte **)(param_1 + param_2 * 2);
    do {
      while( true ) {
        pbVar16 = *ppbVar12;
        uVar1 = *pbVar16 - 0x2d;
        if (uVar1 == 0) {
          uVar1 = (uint)pbVar16[1];
        }
        if (((param_4 == (char *)0x0) || (iVar2 = strcmp(param_4,(char *)pbVar16), iVar2 != 0)) ||
           (uVar1 == 0)) break;
LAB_0010ad16:
        pbVar16 = pbVar7 + 0xd;
        if (pbVar7 == (byte *)0x0) {
          pbVar7 = (byte *)maybe_create_temp(&local_e0,0);
          pbVar16 = pbVar7 + 0xd;
          mergefiles(ppbVar12,0,1,local_e0,pbVar16);
        }
        *ppbVar12 = pbVar16;
        uVar5 = uVar5 + 1;
        ppbVar12[1] = pbVar7;
        ppbVar12 = ppbVar12 + 2;
        if (uVar5 == local_108) goto LAB_0010ad49;
      }
      if (outstat_errno_4 == 0) {
        iVar2 = fstat(1,(stat *)outstat_3);
        if (iVar2 != 0) {
          piVar6 = __errno_location();
          outstat_errno_4 = *piVar6;
          goto LAB_0010ace1;
        }
        outstat_errno_4 = -1;
      }
      else {
LAB_0010ace1:
        if (-1 < outstat_errno_4) break;
      }
      if (uVar1 == 0) {
        iVar2 = fstat(0,&local_d8);
        if ((iVar2 == 0) && (local_d8.st_ino == outstat_3._8_8_)) goto LAB_0010ad08;
      }
      else {
        iVar2 = stat((char *)*ppbVar12,&local_d8);
        if ((iVar2 == 0) && (local_d8.st_ino == outstat_3._8_8_)) {
LAB_0010ad08:
          if (local_d8.st_dev == outstat_3._0_8_) goto LAB_0010ad16;
        }
      }
      uVar5 = uVar5 + 1;
      ppbVar12 = ppbVar12 + 2;
    } while (uVar5 != local_108);
  }
LAB_0010ad49:
  local_110 = param_2;
  do {
    uVar5 = open_input_files(param_1,local_108,&local_e8);
    if (local_108 == uVar5) {
      lVar11 = stream_open(param_4,&DAT_00116be9);
      if (lVar11 != 0) {
        mergefps(param_1,local_110,local_108,lVar11,param_4,local_e8);
        if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
          __stack_chk_fail();
        }
        return;
      }
      piVar6 = __errno_location();
      if ((*piVar6 != 0x18) || (local_108 < 3)) {
        uVar8 = dcgettext(0,"open failed",5);
                    /* WARNING: Subroutine does not return */
        sort_die(uVar8,param_4);
      }
    }
    else if (uVar5 < 3) {
      lVar11 = param_1[uVar5 * 2];
      uVar8 = dcgettext(0,"open failed",5);
                    /* WARNING: Subroutine does not return */
      sort_die(uVar8,lVar11);
    }
    uVar5 = uVar5 - 1;
    plVar13 = param_1 + uVar5 * 2;
    while( true ) {
      xfclose(*(undefined8 *)(local_e8 + uVar5 * 8),*plVar13);
      lVar11 = maybe_create_temp(&local_e0,2 < uVar5);
      if (lVar11 != 0) break;
      uVar5 = uVar5 - 1;
      plVar13 = plVar13 + -2;
    }
    uVar4 = local_110;
    if (uVar5 < local_110) {
      uVar4 = uVar5;
    }
    lVar3 = local_108 - uVar5;
    mergefps(param_1,uVar4,uVar5,local_e0,lVar11 + 0xd,local_e8);
    local_108 = lVar3 + 1;
    *param_1 = lVar11 + 0xd;
    param_1[1] = lVar11;
    memmove(param_1 + 2,plVar13,lVar3 * 0x10);
    local_110 = (local_110 - uVar4) + 1;
  } while( true );
}