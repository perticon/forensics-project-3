void write_line(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rbp6;
    void* rsp7;
    void** r14_8;
    void** r15_9;
    void** v10;
    void** rbx11;
    void** rax12;
    void** v13;
    int1_t zf14;
    uint32_t eax15;
    void** rax16;
    void** rdx17;
    void** rax18;
    void** rdi19;
    void** rbx20;
    void** rsi21;
    void** v22;
    void** rax23;
    void** v24;
    int32_t eax25;
    void* rsp26;
    uint32_t eax27;
    int1_t cf28;
    uint32_t edx29;
    int32_t eax30;
    void** rax31;
    int32_t eax32;
    void** r13_33;
    unsigned char v34;
    void** eax35;
    void** r8d36;
    void** ecx37;
    void** rdi38;
    void** rsi39;
    void** rsi40;
    uint32_t eax41;
    int32_t eax42;
    int1_t cf43;
    void* rax44;
    int64_t v45;
    void** r13_46;
    void** rbp47;
    void** rbx48;
    void** rax49;
    void** v50;
    void** rax51;
    void* rsp52;
    void** v53;
    void* rax54;
    void** rsi55;
    void** rax56;
    void** rax57;
    void** r13d58;
    int64_t v59;
    void** r12_60;
    void** r15_61;
    uint32_t eax62;
    void** rdi63;
    void** rax64;
    void** r9d65;
    void** rdi66;
    void** rsi67;
    struct s3* rax68;
    void** rdi69;
    int32_t eax70;
    void** rdi71;
    void** eax72;
    int64_t rdi73;
    int32_t v74;
    void** rax75;
    void** rax76;
    int32_t v77;
    void** rax78;
    void** edi79;
    uint32_t tmp32_80;
    void* rax81;
    void** rbx82;
    int32_t eax83;
    void* rsp84;
    void* rax85;
    int32_t eax86;
    void* rax87;
    void** rdi88;
    void** rax89;
    int1_t cf90;
    void** rax91;
    void** rdi92;
    void** rax93;
    void** rdi94;
    void** rax95;
    int1_t zf96;
    int1_t zf97;
    void** r10_98;
    void** rax99;
    uint32_t r15d100;
    struct s11* rax101;
    struct s12* rax102;
    void** v103;
    void** rax104;
    void* rax105;
    struct s4** rdi106;
    signed char al107;
    void** rax108;
    int64_t rcx109;
    void** rax110;
    int32_t ecx111;
    uint32_t eax112;
    void** rax113;
    int32_t eax114;

    r12_5 = rsi;
    rbp6 = rdx;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    r14_8 = *reinterpret_cast<void***>(rdi);
    r15_9 = *reinterpret_cast<void***>(rdi + 8);
    v10 = rdi;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<unsigned char>(r15_9));
    rax12 = g28;
    v13 = rax12;
    if (rdx || (zf14 = debug == 0, zf14)) {
        eax15 = eolchar;
        *reinterpret_cast<void***>(rbx11 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax15);
        rax16 = fun_3bf0(r14_8, 1, r15_9, r12_5);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (r15_9 != rax16) {
            addr_83ee_3:
            *reinterpret_cast<int32_t*>(&rdx17) = 5;
            *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
            rax18 = fun_3920();
            rdi19 = rax18;
            sort_die(rdi19, rbp6, 5, rdi19, rbp6, 5);
        } else {
            *reinterpret_cast<void***>(rbx11 + 0xffffffffffffffff) = reinterpret_cast<void**>(0);
            goto addr_7fd3_5;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r14_8) < reinterpret_cast<unsigned char>(rbx11)) 
            goto addr_803c_7;
        goto addr_80a8_9;
    }
    rbx20 = rbp6;
    *reinterpret_cast<int32_t*>(&rsi21) = 0x80000;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    v22 = rdi19;
    rax23 = g28;
    v24 = rax23;
    eax25 = rpl_pipe2();
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8);
    if (eax25 >= 0) {
        eax27 = nmerge;
        cf28 = eax27 + 1 < nprocs;
        if (cf28) {
            rdi19 = reinterpret_cast<void**>(0xffffffff);
            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
            reap(0xffffffff, 0x80000);
            rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
            while ((edx29 = nprocs, !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(edx29) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(edx29 == 0))) && (rdi19 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0, eax30 = reap(0, 0x80000), rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8), !!eax30)) {
            }
        }
        rax31 = fun_37d0();
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
        --rbx20;
        *reinterpret_cast<void***>(rdi19) = g80000;
        r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp26) + 40);
        rbp6 = rax31;
        *reinterpret_cast<void***>(rdi19 + 4) = g80004;
        while (1) {
            rdi19 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
            rdx17 = r12_5;
            rsi21 = reinterpret_cast<void**>(0x1d3a0);
            eax32 = fun_3c00();
            r13_33 = temphead;
            temphead = reinterpret_cast<void**>(0);
            v34 = reinterpret_cast<uint1_t>(eax32 == 0);
            eax35 = fun_3e70();
            rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
            r8d36 = *reinterpret_cast<void***>(rbp6);
            ecx37 = eax35;
            if (eax35) {
                temphead = r13_33;
                if (v34) {
                    addr_8539_25:
                    *reinterpret_cast<int32_t*>(&rdx17) = 0;
                    *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
                    rsi21 = r12_5;
                    rdi19 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                    fun_3c00();
                    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                    ecx37 = ecx37;
                    r8d36 = r8d36;
                    goto addr_84a2_26;
                } else {
                    addr_84a2_26:
                    *reinterpret_cast<void***>(rbp6) = r8d36;
                    if (reinterpret_cast<signed char>(ecx37) >= reinterpret_cast<signed char>(0)) 
                        goto addr_855f_27;
                }
                if (!reinterpret_cast<int1_t>(r8d36 == 11)) 
                    goto addr_85e4_29;
                *reinterpret_cast<void***>(rdi19) = *reinterpret_cast<void***>(rsi21);
                rdi38 = rdi19 + 4;
                rsi39 = rsi21 + 4;
                xnanosleep();
                rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                *reinterpret_cast<void***>(rdi38) = *reinterpret_cast<void***>(rsi39);
                rsi40 = rsi39 + 4;
                __asm__("movapd xmm1, xmm2");
                __asm__("addsd xmm1, xmm2");
                *reinterpret_cast<void***>(rdi38 + 4) = *reinterpret_cast<void***>(rsi40);
                rsi21 = rsi40 + 4;
                do {
                    eax41 = nprocs;
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax41) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax41 == 0)) 
                        break;
                    eax42 = reap(0, rsi21);
                    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                } while (eax42);
                cf43 = reinterpret_cast<unsigned char>(rbx20) < reinterpret_cast<unsigned char>(1);
                --rbx20;
                if (cf43) 
                    goto addr_85e0_42;
            } else {
                if (!v34) 
                    goto addr_8598_44; else 
                    goto addr_8539_25;
            }
        }
    }
    addr_8568_45:
    rax44 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
    if (!rax44) {
        goto v45;
    }
    fun_3950();
    r13_46 = rsi21;
    rbp47 = rdx17;
    rbx48 = rdi19;
    rax49 = g28;
    v50 = rax49;
    rax51 = xnmalloc(r13_46, 8, rdx17, r13_46, 8, rdx17);
    rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    v53 = rax51;
    *reinterpret_cast<void***>(rbp47) = rax51;
    if (r13_46) 
        goto addr_8666_49;
    addr_8727_51:
    while (rax54 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v50) - reinterpret_cast<unsigned char>(g28)), !!rax54) {
        addr_885c_52:
        fun_3950();
        addr_8861_53:
        rsi55 = compress_program;
        quotearg_style(4, rsi55, rdx17, 4, rsi55, rdx17);
        rax56 = fun_3920();
        rdx17 = rax56;
        fun_3c90();
        addr_889c_54:
        rax57 = fun_37d0();
        r13d58 = *reinterpret_cast<void***>(rax57);
        fun_3a40();
        *reinterpret_cast<void***>(rbp47) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax57) = r13d58;
    }
    goto v59;
    addr_8666_49:
    rbp47 = rax51;
    *reinterpret_cast<int32_t*>(&r12_60) = 0;
    *reinterpret_cast<int32_t*>(&r12_60 + 4) = 0;
    do {
        r15_61 = *reinterpret_cast<void***>(rbx48 + 8);
        if (!r15_61 || (eax62 = *reinterpret_cast<unsigned char*>(r15_61 + 12), *reinterpret_cast<signed char*>(&eax62) == 0)) {
            rdi63 = *reinterpret_cast<void***>(rbx48);
            rax64 = stream_open(rdi63, "r", rdx17, rdi63, "r", rdx17);
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            *reinterpret_cast<void***>(rbp47) = rax64;
            if (!rax64) 
                goto addr_8727_51;
        } else {
            if (*reinterpret_cast<signed char*>(&eax62) == 1 && (r9d65 = *reinterpret_cast<void***>(r15_61 + 8), rdi66 = proctab, rsi67 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp52) + 16), rax68 = hash_remove(rdi66, rsi67, rdx17, rdi66, rsi67, rdx17), rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8), !!rax68)) {
                rax68->fc = 2;
                reap(r9d65, rsi67);
                rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            }
            rdi69 = r15_61 + 13;
            eax70 = fun_3cd0(rdi69, rdi69);
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            if (eax70 < 0) 
                break;
            rdi71 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp52) + 16);
            eax72 = pipe_fork(rdi71, 9, rdx17, rdi71, 9, rdx17);
            if (reinterpret_cast<int1_t>(eax72 == 0xffffffff)) 
                goto addr_86f1_62;
            if (!eax72) 
                goto addr_87fa_64;
            *reinterpret_cast<void***>(r15_61 + 8) = eax72;
            register_proc(r15_61, 9, rdx17, r15_61, 9, rdx17);
            fun_3a40();
            fun_3a40();
            *reinterpret_cast<int32_t*>(&rdi73) = v74;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0;
            rax75 = fun_3c20(rdi73, "r", rdx17, rdi73, "r", rdx17);
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            if (!rax75) 
                goto addr_889c_54;
            *reinterpret_cast<void***>(rbp47) = rax75;
        }
        ++r12_60;
        rbx48 = rbx48 + 16;
        rbp47 = rbp47 + 8;
    } while (r13_46 != r12_60);
    goto addr_8727_51;
    *reinterpret_cast<void***>(v53 + reinterpret_cast<unsigned char>(r12_60) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_51;
    addr_86f1_62:
    rax76 = fun_37d0();
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax76) == 24)) 
        goto addr_8861_53;
    fun_3a40();
    *reinterpret_cast<void***>(rax76) = reinterpret_cast<void**>(24);
    *reinterpret_cast<void***>(v53 + reinterpret_cast<unsigned char>(r12_60) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_51;
    addr_87fa_64:
    fun_3a40();
    if (eax70) {
        move_fd_part_0(eax70, eax70);
    }
    if (v77 != 1) {
        move_fd_part_0(v77, v77);
    }
    rdx17 = reinterpret_cast<void**>("-d");
    fun_3e20();
    rax78 = fun_37d0();
    edi79 = *reinterpret_cast<void***>(rax78);
    async_safe_die(edi79, "couldn't execute compress program (with -d)", edi79, "couldn't execute compress program (with -d)");
    goto addr_885c_52;
    addr_855f_27:
    if (!ecx37) {
        addr_8598_44:
        fun_3a40();
        rdi19 = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
        fun_3a40();
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
        goto addr_8568_45;
    } else {
        tmp32_80 = nprocs + 1;
        nprocs = tmp32_80;
        goto addr_8568_45;
    }
    addr_85e4_29:
    fun_3a40();
    rdi19 = *reinterpret_cast<void***>(v22 + 4);
    *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
    fun_3a40();
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
    *reinterpret_cast<void***>(rbp6) = r8d36;
    goto addr_8568_45;
    addr_85e0_42:
    r8d36 = *reinterpret_cast<void***>(rbp6);
    goto addr_85e4_29;
    addr_7fd3_5:
    rax81 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (rax81) {
        fun_3950();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        goto addr_83ee_3;
    } else {
        return;
    }
    addr_80a8_9:
    rbp6 = keylist;
    r13_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<unsigned char>(r15_9) + 0xffffffffffffffff);
    if (!rbp6) {
        while (1) {
            rbx82 = r14_8;
            while (1) {
                eax83 = mbsnwidth(r14_8);
                rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                r15_9 = reinterpret_cast<void**>(static_cast<int64_t>(eax83));
                if (reinterpret_cast<unsigned char>(r14_8) < reinterpret_cast<unsigned char>(rbx82)) {
                    do {
                        ++r14_8;
                        *reinterpret_cast<int32_t*>(&rax85) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax85) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax85) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r14_8 + 0xffffffffffffffff) == 9);
                        r15_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_9) + reinterpret_cast<uint64_t>(rax85));
                    } while (rbx82 != r14_8);
                }
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_33) - reinterpret_cast<unsigned char>(rbx82));
                eax86 = mbsnwidth(rbx82);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
                r14_8 = reinterpret_cast<void**>(static_cast<int64_t>(eax86));
                if (reinterpret_cast<unsigned char>(rbx82) < reinterpret_cast<unsigned char>(r13_33)) {
                    do {
                        ++rbx82;
                        *reinterpret_cast<int32_t*>(&rax87) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax87) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax87) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx82 + 0xffffffffffffffff) == 9);
                        r14_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<uint64_t>(rax87));
                    } while (r13_33 != rbx82);
                }
                rbx11 = r15_9 + 0xffffffffffffffff;
                if (r15_9) {
                    do {
                        rdi88 = stdout;
                        rax89 = *reinterpret_cast<void***>(rdi88 + 40);
                        if (reinterpret_cast<unsigned char>(rax89) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi88 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi) = 32;
                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                            fun_39c0(rdi88, 32);
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                        } else {
                            *reinterpret_cast<void***>(rdi88 + 40) = rax89 + 1;
                            *reinterpret_cast<void***>(rax89) = reinterpret_cast<void**>(32);
                        }
                        cf90 = reinterpret_cast<unsigned char>(rbx11) < reinterpret_cast<unsigned char>(1);
                        --rbx11;
                    } while (!cf90);
                }
                if (!r14_8) {
                    *reinterpret_cast<int32_t*>(&rdx) = 5;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rax91 = fun_3920();
                    rsi = rax91;
                    fun_3c40(1, rsi, 5);
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
                } else {
                    do {
                        rdi92 = stdout;
                        rax93 = *reinterpret_cast<void***>(rdi92 + 40);
                        if (reinterpret_cast<unsigned char>(rax93) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi92 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi) = 95;
                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                            fun_39c0(rdi92, 95);
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                        } else {
                            *reinterpret_cast<void***>(rdi92 + 40) = rax93 + 1;
                            *reinterpret_cast<void***>(rax93) = reinterpret_cast<void**>(95);
                        }
                        --r14_8;
                    } while (r14_8);
                    rdi94 = stdout;
                    rax95 = *reinterpret_cast<void***>(rdi94 + 40);
                    if (reinterpret_cast<unsigned char>(rax95) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi94 + 48))) 
                        goto addr_83b2_98; else 
                        goto addr_82a7_99;
                }
                addr_82b2_100:
                if (!rbp6) 
                    goto addr_7fd3_5;
                rbp6 = *reinterpret_cast<void***>(rbp6 + 64);
                if (!rbp6) {
                    zf96 = unique == 0;
                    if (!zf96) 
                        goto addr_7fd3_5;
                    zf97 = stable == 0;
                    if (!zf97) 
                        goto addr_7fd3_5;
                    r14_8 = *reinterpret_cast<void***>(v10);
                    rbx82 = r14_8;
                    r13_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v10 + 8)) + 0xffffffffffffffff);
                    continue;
                }
                r14_8 = *reinterpret_cast<void***>(v10);
                r15_9 = *reinterpret_cast<void***>(v10 + 8);
                r13_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<unsigned char>(r15_9) + 0xffffffffffffffff);
                if (!rbp6) 
                    break;
                addr_80c8_106:
                r10_98 = *reinterpret_cast<void***>(rbp6);
                if (r10_98 != 0xffffffffffffffff) 
                    goto addr_80d6_107;
                rbx82 = r14_8;
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp6 + 16) == 0xffffffffffffffff)) {
                    addr_80ee_109:
                    rdx = rbp6;
                    rsi = r15_9;
                    rax99 = limfield_isra_0(r14_8, rsi, rdx);
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                    r13_33 = rax99;
                }
                if (!reinterpret_cast<int1_t>(r10_98 == 0xffffffffffffffff) || !*reinterpret_cast<void***>(rbp6 + 48)) {
                    addr_810b_112:
                    if (*reinterpret_cast<unsigned char*>(rbp6 + 54)) 
                        goto addr_8125_113;
                    if (!(0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 48)))) 
                        continue; else 
                        goto addr_8125_113;
                } else {
                    addr_8125_113:
                    r15d100 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_33));
                    *reinterpret_cast<void***>(r13_33) = reinterpret_cast<void**>(0);
                    *reinterpret_cast<uint32_t*>(&rax101) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx82));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax101) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d760) + reinterpret_cast<uint64_t>(rax101))) {
                        do {
                            *reinterpret_cast<uint32_t*>(&rax102) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx82 + 1));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax102) + 4) = 0;
                            ++rbx82;
                        } while (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d760) + reinterpret_cast<uint64_t>(rax102)));
                    }
                }
                v103 = rbx82;
                if (reinterpret_cast<unsigned char>(rbx82) > reinterpret_cast<unsigned char>(r13_33)) 
                    goto addr_8310_118;
                if (*reinterpret_cast<unsigned char*>(rbp6 + 54)) {
                    getmonth(rbx82, reinterpret_cast<int64_t>(rsp7) + 24, rdx);
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                } else {
                    if (*reinterpret_cast<unsigned char*>(rbp6 + 52)) {
                        fun_3c80(rbx82, reinterpret_cast<int64_t>(rsp7) + 24, rdx);
                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                        __asm__("fstp st0");
                    } else {
                        if (!(0xff0000ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 48)))) {
                            addr_8310_118:
                            v103 = r13_33;
                        } else {
                            rax104 = rbx82;
                            if (reinterpret_cast<unsigned char>(rbx82) < reinterpret_cast<unsigned char>(r13_33)) {
                                *reinterpret_cast<int32_t*>(&rax105) = 0;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
                                *reinterpret_cast<unsigned char*>(&rax105) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx82) == 45);
                                rax104 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax105) + reinterpret_cast<unsigned char>(rbx82));
                            }
                            rdi106 = reinterpret_cast<struct s4**>(reinterpret_cast<int64_t>(rsp7) + 32);
                            al107 = traverse_raw_number(rdi106, rsi, rdi106, rsi);
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                            if (al107 > 47) {
                                rax108 = rax104;
                                *reinterpret_cast<uint32_t*>(&rcx109) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax108));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx109) + 4) = 0;
                                if (*reinterpret_cast<unsigned char*>(rbp6 + 53)) {
                                    rax108 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax108) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax108) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(0x15360 + rcx109) < 1)))))));
                                }
                                v103 = rax108;
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(r13_33) = *reinterpret_cast<void***>(&r15d100);
                r13_33 = v103;
                continue;
                addr_80d6_107:
                rdx = rbp6;
                rsi = r15_9;
                rax110 = begfield_isra_0(r14_8, rsi, rdx);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                rbx82 = rax110;
                if (*reinterpret_cast<void***>(rbp6 + 16) == 0xffffffffffffffff) 
                    goto addr_810b_112; else 
                    goto addr_80ee_109;
                addr_83b2_98:
                *reinterpret_cast<uint32_t*>(&rsi) = 10;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_39c0(rdi94, 10);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                goto addr_82b2_100;
                addr_82a7_99:
                rdx = rax95 + 1;
                *reinterpret_cast<void***>(rdi94 + 40) = rdx;
                *reinterpret_cast<void***>(rax95) = reinterpret_cast<void**>(10);
                goto addr_82b2_100;
            }
        }
    } else {
        goto addr_80c8_106;
    }
    addr_809c_132:
    r14_8 = *reinterpret_cast<void***>(v10);
    r15_9 = *reinterpret_cast<void***>(v10 + 8);
    goto addr_80a8_9;
    while (1) {
        addr_8090_133:
        ecx111 = 10;
        eax112 = 10;
        goto addr_8020_134;
        addr_806e_135:
        rax113 = fun_3920();
        *reinterpret_cast<uint32_t*>(&rsi) = 0;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        sort_die(rax113, 0, 5);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
        continue;
        while (*reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<unsigned char*>(&ecx111), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, eax114 = fun_39c0(r12_5, rsi), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), eax114 != -1) {
            while (1) {
                if (rbx11 == r14_8) 
                    goto addr_809c_132;
                addr_803c_7:
                eax112 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_8));
                ++r14_8;
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax112) == 9)) {
                    ecx111 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&eax112));
                    if (rbx11 == r14_8) 
                        goto addr_8090_133;
                    addr_8020_134:
                    rdx = *reinterpret_cast<void***>(r12_5 + 40);
                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 48))) 
                        break;
                } else {
                    rdx = *reinterpret_cast<void***>(r12_5 + 40);
                    ecx111 = 62;
                    eax112 = 62;
                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 48))) 
                        break;
                }
                *reinterpret_cast<void***>(r12_5 + 40) = rdx + 1;
                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax112);
            }
        }
        goto addr_806e_135;
    }
}