void write_line(int32_t * line, struct _IO_FILE * fp, char * output_file) {
    int64_t v1 = (int64_t)line;
    int64_t * v2 = (int64_t *)(v1 + 8); // 0x7f87
    int64_t v3 = *v2; // 0x7f87
    uint64_t v4 = v3 + v1; // 0x7f90
    int64_t v5 = __readfsqword(40); // 0x7f94
    int64_t v6; // 0x7f70
    int64_t v7; // 0x7f70
    int64_t v8; // 0x7f70
    int64_t v9; // 0x7f70
    int64_t v10; // 0x7f70
    int64_t v11; // 0x7f70
    char v12; // 0x7f70
    char v13; // 0x7f70
    int64_t v14; // 0x7f70
    int64_t v15; // 0x7f70
    int64_t v16; // 0x7f70
    int64_t v17; // 0x7f70
    int64_t v18; // 0x7f70
    int64_t v19; // 0x7f70
    int64_t v20; // 0x7f70
    int64_t v21; // 0x7f70
    int64_t v22; // 0x7f70
    int64_t v23; // 0x7f70
    int64_t v24; // 0x83fc
    int64_t v25; // 0x7f70
    int64_t * v26; // 0x7f70
    int64_t * v27; // 0x7f70
    int64_t v28; // 0x8040
    if (output_file != NULL | *(char *)&debug == 0) {
        int64_t v29 = (int64_t)output_file;
        char * v30 = (char *)(v4 - 1); // 0x7fbe
        *v30 = eolchar;
        if (v3 != function_3bf0()) {
            // 0x83ee
            v24 = function_3920();
            sort_die((char *)v24, (char *)v29);
            return;
        }
        // 0x7fcf
        *v30 = 0;
        v14 = v29;
        goto lab_0x7fd3;
    } else {
        int64_t v31 = v1; // 0x800c
        int64_t v32 = v3; // 0x800c
        if (v4 > v1) {
            // 0x803c
            v25 = (int64_t)fp;
            v26 = (int64_t *)(v25 + 40);
            v27 = (int64_t *)(v25 + 48);
            v21 = v1;
            v8 = v1;
            while (true) {
              lab_0x803c:;
                int64_t v33 = v21;
                char v34 = *(char *)v8; // 0x803c
                v28 = v8 + 1;
                if (v34 != 9) {
                    // 0x8018
                    v12 = v34;
                    v18 = v33;
                    v22 = v33;
                    if (v4 == v28) {
                        goto lab_0x8090;
                    } else {
                        goto lab_0x8020;
                    }
                } else {
                    uint64_t v35 = *v26; // 0x8048
                    v13 = 62;
                    v23 = v35;
                    v19 = v33;
                    if (v35 < *v27) {
                        goto lab_0x802c;
                    } else {
                        goto lab_0x805e;
                    }
                }
            }
          lab_0x809c:
            // 0x809c
            v32 = *v2;
            v31 = v20;
        }
        int64_t v36 = (int64_t)keylist; // 0x80a8
        int64_t v37 = v31 - 1 + v32; // 0x80b6
        v15 = v36;
        v6 = v37;
        v9 = v31;
        v11 = v32;
        v17 = v31;
        v16 = v36;
        v7 = v37;
        v10 = v31;
        if (keylist == NULL) {
            goto lab_0x81d1;
        } else {
            goto lab_0x80c8;
        }
    }
  lab_0x8090:
    // 0x8090
    v12 = 10;
    v18 = v22;
    goto lab_0x8020;
  lab_0x8020:;
    uint64_t v38 = *v26; // 0x8020
    v13 = v12;
    v23 = v38;
    v19 = v18;
    if (v38 >= *v27) {
        goto lab_0x805e;
    } else {
        goto lab_0x802c;
    }
  lab_0x802c:
    // 0x802c
    *v26 = v23 + 1;
    *(char *)v23 = v13;
    int64_t v39 = v19; // 0x8035
    goto lab_0x8037;
  lab_0x805e:
    // 0x805e
    v39 = v25;
    if ((int32_t)function_39c0() != -1) {
        goto lab_0x8037;
    } else {
        int64_t v108 = function_3920(); // 0x807c
        sort_die((char *)v108, (char *)((int32_t)"write failed" ^ (int32_t)"write failed"));
        v22 = v108;
        goto lab_0x8090;
    }
  lab_0x8037:
    // 0x8037
    v20 = v39;
    v21 = v20;
    v8 = v28;
    if (v4 == v28) {
        // break -> 0x809c
        goto lab_0x809c;
    }
    goto lab_0x803c;
  lab_0x7fd3:
    // 0x7fd3
    if (v5 == __readfsqword(40)) {
        // 0x7fe7
        return;
    }
    // 0x83e9
    function_3950();
    // 0x83ee
    v24 = function_3920();
    sort_die((char *)v24, (char *)v14);
  lab_0x81d1:;
    int64_t v40 = v16; // 0x7f70
    int64_t v41 = v10;
    uint64_t v42 = v7;
    uint64_t v43 = v17;
    int64_t v44 = mbsnwidth((char *)v41, v43 - v41, 0); // 0x81dc
    int64_t v45 = v41; // 0x81e7
    int64_t v46 = v44; // 0x81e7
    int64_t v47 = v44; // 0x81e7
    int64_t v48; // 0x81f0
    int64_t v49; // 0x81fe
    if (v43 > v41) {
        v48 = v45 + 1;
        v49 = v46 + (int64_t)(*(char *)v45 == 9);
        v45 = v48;
        v46 = v49;
        v47 = v49;
        while (v43 != v48) {
            // 0x81f0
            v48 = v45 + 1;
            v49 = v46 + (int64_t)(*(char *)v45 == 9);
            v45 = v48;
            v46 = v49;
            v47 = v49;
        }
    }
    int64_t v50 = mbsnwidth((char *)v43, v42 - v43, 0); // 0x8211
    int64_t v51 = v43; // 0x821c
    int64_t v52 = v50; // 0x821c
    int64_t v53 = v50; // 0x821c
    int64_t v54; // 0x8220
    int64_t v55; // 0x822d
    if (v42 > v43) {
        v54 = v51 + 1;
        v55 = v52 + (int64_t)(*(char *)v51 == 9);
        v51 = v54;
        v52 = v55;
        v53 = v55;
        while (v42 != v54) {
            // 0x8220
            v54 = v51 + 1;
            v55 = v52 + (int64_t)(*(char *)v51 == 9);
            v51 = v54;
            v52 = v55;
            v53 = v55;
        }
    }
    int64_t v56 = v47; // 0x823c
    int64_t v57; // 0x7f70
    int64_t v58; // 0x8240
    int64_t * v59; // 0x8247
    uint64_t v60; // 0x8247
    if (v47 != 0) {
        v58 = (int64_t)g38;
        v59 = (int64_t *)(v58 + 40);
        v60 = *v59;
        if (v60 >= *(int64_t *)(v58 + 48)) {
            // 0x82f0
            function_39c0();
        } else {
            // 0x8255
            *v59 = v60 + 1;
            *(char *)v60 = 32;
        }
        // 0x8260
        v57 = v56 - 1;
        v56 = v57;
        while (v57 != 0) {
            // 0x8240
            v58 = (int64_t)g38;
            v59 = (int64_t *)(v58 + 40);
            v60 = *v59;
            if (v60 >= *(int64_t *)(v58 + 48)) {
                // 0x82f0
                function_39c0();
            } else {
                // 0x8255
                *v59 = v60 + 1;
                *(char *)v60 = 32;
            }
            // 0x8260
            v57 = v56 - 1;
            v56 = v57;
        }
    }
    int64_t v61 = v53; // 0x8269
    int64_t v62; // 0x7f70
    int64_t v63; // 0x8270
    int64_t * v64; // 0x8277
    uint64_t v65; // 0x8277
    int64_t v66; // 0x828c
    int64_t v67; // 0x8292
    int64_t * v68; // 0x8299
    uint64_t v69; // 0x8299
    if (v53 == 0) {
        // 0x8320
        function_3920();
        function_3c40();
        v62 = 1;
    } else {
        v63 = (int64_t)g38;
        v64 = (int64_t *)(v63 + 40);
        v65 = *v64;
        if (v65 >= *(int64_t *)(v63 + 48)) {
            // 0x8300
            function_39c0();
        } else {
            // 0x8281
            *v64 = v65 + 1;
            *(char *)v65 = 95;
        }
        // 0x828c
        v66 = v61 - 1;
        v61 = v66;
        while (v66 != 0) {
            // 0x8270
            v63 = (int64_t)g38;
            v64 = (int64_t *)(v63 + 40);
            v65 = *v64;
            if (v65 >= *(int64_t *)(v63 + 48)) {
                // 0x8300
                function_39c0();
            } else {
                // 0x8281
                *v64 = v65 + 1;
                *(char *)v65 = 95;
            }
            // 0x828c
            v66 = v61 - 1;
            v61 = v66;
        }
        // 0x8292
        v67 = (int64_t)g38;
        v68 = (int64_t *)(v67 + 40);
        v69 = *v68;
        if (v69 >= *(int64_t *)(v67 + 48)) {
            // 0x83b2
            function_39c0();
            v62 = v67;
        } else {
            // 0x82a7
            *v68 = v69 + 1;
            *(char *)v69 = 10;
            v62 = v67;
        }
    }
    // 0x82b2
    v14 = 0;
    int3_t v70; // 0x7f70
    int3_t v71; // 0x7f70
    while (v40 != 0) {
        int64_t v72 = v62;
        int64_t v73 = *(int64_t *)(v40 + 64); // 0x82bb
        if (v73 != 0) {
            int64_t v74 = *v2; // 0x82d0
            v70 = v71;
            v15 = v73;
            v6 = v72 - 1 + v74;
            v9 = v72;
            v11 = v74;
            goto lab_0x80c8;
        }
        // 0x8350
        v14 = 0;
        if (*(char *)&unique != 0) {
            // break -> 0x7fd3
            break;
        }
        // 0x835d
        v14 = 0;
        if (*(char *)&stable != 0) {
            // break -> 0x7fd3
            break;
        }
        // 0x836a
        v40 = 0;
        v41 = v72;
        v42 = v72 - 1 + *v2;
        v43 = v72;
        v44 = mbsnwidth((char *)v41, v43 - v41, 0);
        v45 = v41;
        v46 = v44;
        v47 = v44;
        if (v43 > v41) {
            v48 = v45 + 1;
            v49 = v46 + (int64_t)(*(char *)v45 == 9);
            v45 = v48;
            v46 = v49;
            v47 = v49;
            while (v43 != v48) {
                // 0x81f0
                v48 = v45 + 1;
                v49 = v46 + (int64_t)(*(char *)v45 == 9);
                v45 = v48;
                v46 = v49;
                v47 = v49;
            }
        }
        // 0x8206
        v50 = mbsnwidth((char *)v43, v42 - v43, 0);
        v51 = v43;
        v52 = v50;
        v53 = v50;
        if (v42 > v43) {
            v54 = v51 + 1;
            v55 = v52 + (int64_t)(*(char *)v51 == 9);
            v51 = v54;
            v52 = v55;
            v53 = v55;
            while (v42 != v54) {
                // 0x8220
                v54 = v51 + 1;
                v55 = v52 + (int64_t)(*(char *)v51 == 9);
                v51 = v54;
                v52 = v55;
                v53 = v55;
            }
        }
        // 0x8235
        v56 = v47;
        if (v47 != 0) {
            v58 = (int64_t)g38;
            v59 = (int64_t *)(v58 + 40);
            v60 = *v59;
            if (v60 >= *(int64_t *)(v58 + 48)) {
                // 0x82f0
                function_39c0();
            } else {
                // 0x8255
                *v59 = v60 + 1;
                *(char *)v60 = 32;
            }
            // 0x8260
            v57 = v56 - 1;
            v56 = v57;
            while (v57 != 0) {
                // 0x8240
                v58 = (int64_t)g38;
                v59 = (int64_t *)(v58 + 40);
                v60 = *v59;
                if (v60 >= *(int64_t *)(v58 + 48)) {
                    // 0x82f0
                    function_39c0();
                } else {
                    // 0x8255
                    *v59 = v60 + 1;
                    *(char *)v60 = 32;
                }
                // 0x8260
                v57 = v56 - 1;
                v56 = v57;
            }
        }
        // 0x8266
        v61 = v53;
        if (v53 == 0) {
            // 0x8320
            function_3920();
            function_3c40();
            v62 = 1;
        } else {
            v63 = (int64_t)g38;
            v64 = (int64_t *)(v63 + 40);
            v65 = *v64;
            if (v65 >= *(int64_t *)(v63 + 48)) {
                // 0x8300
                function_39c0();
            } else {
                // 0x8281
                *v64 = v65 + 1;
                *(char *)v65 = 95;
            }
            // 0x828c
            v66 = v61 - 1;
            v61 = v66;
            while (v66 != 0) {
                // 0x8270
                v63 = (int64_t)g38;
                v64 = (int64_t *)(v63 + 40);
                v65 = *v64;
                if (v65 >= *(int64_t *)(v63 + 48)) {
                    // 0x8300
                    function_39c0();
                } else {
                    // 0x8281
                    *v64 = v65 + 1;
                    *(char *)v65 = 95;
                }
                // 0x828c
                v66 = v61 - 1;
                v61 = v66;
            }
            // 0x8292
            v67 = (int64_t)g38;
            v68 = (int64_t *)(v67 + 40);
            v69 = *v68;
            if (v69 >= *(int64_t *)(v67 + 48)) {
                // 0x83b2
                function_39c0();
                v62 = v67;
            } else {
                // 0x82a7
                *v68 = v69 + 1;
                *(char *)v69 = 10;
                v62 = v67;
            }
        }
        // 0x82b2
        v14 = 0;
    }
    goto lab_0x7fd3;
  lab_0x80c8:;
    int64_t v75 = v11;
    int64_t v76 = v9;
    int64_t v77 = v6;
    int64_t v78 = v15;
    int3_t v79 = v70;
    int64_t v80; // 0x7f70
    int64_t v81; // 0x7f70
    int64_t v82; // 0x7f70
    int64_t v83; // 0x7f70
    if (*(int64_t *)v78 == -1) {
        int64_t v84 = v77; // 0x8390
        if (*(int64_t *)(v78 + 16) != -1) {
            // 0x80ff
            v84 = limfield_isra_0(v76, v75, v78);
        }
        // 0x8105
        v82 = v76;
        v80 = v84;
        v83 = v76;
        v81 = v84;
        if (*(char *)(v78 + 48) != 0) {
            goto lab_0x8125;
        } else {
            goto lab_0x810b;
        }
    } else {
        int64_t v85 = begfield_isra_0(v76, v75, v78); // 0x80df
        v82 = v85;
        v80 = v77;
        if (*(int64_t *)(v78 + 16) != -1) {
            // 0x80ff
            v82 = v85;
            v80 = limfield_isra_0(v76, v75, v78);
        }
        goto lab_0x810b;
    }
  lab_0x8125:;
    uint64_t v86 = v81;
    char * v87 = (char *)v86; // 0x8125
    *v87 = 0;
    unsigned char v88 = *(char *)v83; // 0x812f
    int64_t v89 = v83; // 0x8137
    int64_t v90 = v83; // 0x8137
    if (*(char *)((int64_t)v88 + (int64_t)&blanks) != 0) {
        int64_t v91 = v89 + 1; // 0x8140
        unsigned char v92 = *(char *)v91; // 0x8140
        v89 = v91;
        v90 = v91;
        while (*(char *)((int64_t)v92 + (int64_t)&blanks) != 0) {
            // 0x8140
            v91 = v89 + 1;
            v92 = *(char *)v91;
            v89 = v91;
            v90 = v91;
        }
    }
    uint64_t v93 = v90;
    struct _IO_FILE * v94 = (struct _IO_FILE *)v93;
    struct _IO_FILE * v95 = v94; // bp-80, 0x814f
    int3_t v96; // 0x7f70
    if (v93 > v86) {
        // 0x8310
        v95 = (struct _IO_FILE *)v86;
        v96 = v79;
        goto lab_0x81c8;
    } else {
        // 0x815d
        if (*(char *)(v78 + 54) != 0) {
            // 0x83a0
            getmonth((char *)v93, (char **)&v95);
            v96 = v79;
            goto lab_0x81c8;
        } else {
            // 0x8167
            if (*(char *)(v78 + 52) != 0) {
                // 0x83c1
                function_3c80();
                __frontend_reg_store_fpr(v79, __frontend_reg_load_fpr(v79));
                v96 = v79 + 1;
                goto lab_0x81c8;
            } else {
                // 0x8171
                if ((*(int64_t *)(v78 + 48) & 0xff0000ff0000) == 0) {
                    // 0x8310
                    v95 = (struct _IO_FILE *)v86;
                    v96 = v79;
                    goto lab_0x81c8;
                } else {
                    struct _IO_FILE * v97 = v94; // 0x818b
                    if (v93 < v86) {
                        char v98 = *(char *)v93; // 0x818f
                        v97 = (struct _IO_FILE *)(v93 + (int64_t)(v98 == 45));
                    }
                    struct _IO_FILE * v99 = v97; // bp-72, 0x819d
                    unsigned char v100 = traverse_raw_number((char **)&v99); // 0x81a2
                    v96 = v79;
                    if (v100 >= 48) {
                        struct _IO_FILE * v101 = v99; // 0x81ab
                        int64_t v102 = (int64_t)v101; // 0x81ab
                        int64_t v103 = v102; // 0x81b7
                        if (*(char *)(v78 + 53) != 0) {
                            unsigned char v104 = *(char *)v101; // 0x81b4
                            char v105 = *(char *)((int64_t)v104 + (int64_t)"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x06\x00\x03\x00\x00\x00\x01\x00\x02\x00\x00\x05\x00\x00\x00\x04\x00\x00\x00\x00\b\a\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"); // 0x83dc
                            v103 = (int64_t)(v105 != 0) + v102;
                        }
                        // 0x81bd
                        v95 = (struct _IO_FILE *)v103;
                        v96 = v79;
                    }
                    goto lab_0x81c8;
                }
            }
        }
    }
  lab_0x810b:;
    int64_t v106 = v80;
    int64_t v107 = v82;
    v83 = v107;
    v81 = v106;
    if (*(char *)(v78 + 54) != 0) {
        goto lab_0x8125;
    } else {
        // 0x8111
        v83 = v107;
        v81 = v106;
        v71 = v79;
        v17 = v107;
        v16 = v78;
        v7 = v106;
        v10 = v76;
        if ((*(int64_t *)(v78 + 48) & 0xffff00ff0000) == 0) {
            goto lab_0x81d1;
        } else {
            goto lab_0x8125;
        }
    }
  lab_0x81c8:
    // 0x81c8
    v71 = v96;
    v17 = v93;
    v16 = v78;
    v7 = (int64_t)v95;
    v10 = v76;
    goto lab_0x81d1;
}