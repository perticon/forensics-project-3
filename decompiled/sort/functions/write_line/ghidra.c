void write_line(byte **param_1,_IO_FILE *param_2,long param_3)

{
  char *pcVar1;
  byte bVar2;
  char cVar3;
  int iVar4;
  byte *pbVar5;
  byte *__nptr;
  byte *pbVar6;
  undefined8 uVar7;
  uint uVar8;
  long *plVar9;
  byte *pbVar10;
  long lVar11;
  long lVar12;
  long in_FS_OFFSET;
  byte *local_50;
  byte *local_48;
  long local_40;
  
  pbVar5 = *param_1;
  pbVar10 = param_1[1];
  pbVar6 = pbVar5 + (long)pbVar10;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if ((param_3 != 0) || (debug == '\0')) {
    pbVar6[-1] = eolchar;
    pbVar5 = (byte *)fwrite_unlocked(pbVar5,1,(size_t)pbVar10,param_2);
    if (pbVar10 != pbVar5) {
      uVar7 = dcgettext(0,"write failed",5);
                    /* WARNING: Subroutine does not return */
      sort_die(uVar7,param_3);
    }
    pbVar6[-1] = 0;
LAB_00107fd3:
    if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return;
  }
  if (pbVar5 < pbVar6) {
    do {
      bVar2 = *pbVar5;
      pbVar5 = pbVar5 + 1;
      if (bVar2 == 9) {
        pbVar10 = (byte *)param_2->_IO_write_ptr;
        uVar8 = 0x3e;
        bVar2 = 0x3e;
        if (pbVar10 < param_2->_IO_write_end) goto LAB_0010802c;
LAB_0010805e:
        iVar4 = __overflow(param_2,uVar8 & 0xff);
        if (iVar4 == -1) {
          uVar7 = dcgettext(0,"write failed",5);
                    /* WARNING: Subroutine does not return */
          sort_die(uVar7,0);
        }
      }
      else {
        uVar8 = (uint)(char)bVar2;
        if (pbVar6 == pbVar5) {
          uVar8 = 10;
          bVar2 = 10;
        }
        pbVar10 = (byte *)param_2->_IO_write_ptr;
        if (param_2->_IO_write_end <= pbVar10) goto LAB_0010805e;
LAB_0010802c:
        param_2->_IO_write_ptr = (char *)(pbVar10 + 1);
        *pbVar10 = bVar2;
      }
    } while (pbVar6 != pbVar5);
    pbVar5 = *param_1;
    pbVar10 = param_1[1];
  }
  pbVar6 = pbVar5 + -1 + (long)pbVar10;
  plVar9 = keylist;
  do {
    __nptr = pbVar5;
    if (plVar9 != (long *)0x0) {
      lVar12 = *plVar9;
      if (lVar12 == -1) {
        if (plVar9[2] != -1) goto LAB_001080ee;
LAB_001080ff:
        if ((lVar12 != -1) || (*(char *)(plVar9 + 6) == '\0')) goto LAB_0010810b;
      }
      else {
        __nptr = (byte *)begfield_isra_0(pbVar5,pbVar10,plVar9);
        if (plVar9[2] != -1) {
LAB_001080ee:
          pbVar6 = (byte *)limfield_isra_0(pbVar5,pbVar10,plVar9);
          goto LAB_001080ff;
        }
LAB_0010810b:
        if ((*(char *)((long)plVar9 + 0x36) == '\0') && ((plVar9[6] & 0xffff00ff0000U) == 0))
        goto LAB_001081d1;
      }
      bVar2 = *pbVar6;
      *pbVar6 = 0;
      cVar3 = blanks[*__nptr];
      while (cVar3 != '\0') {
        pbVar10 = __nptr + 1;
        __nptr = __nptr + 1;
        cVar3 = blanks[*pbVar10];
      }
      local_50 = pbVar6;
      if (__nptr <= pbVar6) {
        if (*(char *)((long)plVar9 + 0x36) == '\0') {
          if (*(char *)((long)plVar9 + 0x34) == '\0') {
            if ((plVar9[6] & 0xff0000ff0000U) != 0) {
              local_48 = __nptr;
              if (__nptr < pbVar6) {
                local_48 = __nptr + (*__nptr == 0x2d);
              }
              local_50 = __nptr;
              cVar3 = traverse_raw_number(&local_48);
              if (('/' < cVar3) && (local_50 = local_48, *(char *)((long)plVar9 + 0x35) != '\0')) {
                local_50 = local_48 + (unit_order[*local_48] != '\0');
              }
            }
          }
          else {
            local_50 = __nptr;
            strtold((char *)__nptr,(char **)&local_50);
          }
        }
        else {
          local_50 = __nptr;
          getmonth(__nptr,&local_50);
        }
      }
      *pbVar6 = bVar2;
      pbVar6 = local_50;
    }
LAB_001081d1:
    while( true ) {
      iVar4 = mbsnwidth(pbVar5,(long)__nptr - (long)pbVar5,0);
      lVar12 = (long)iVar4;
      if (pbVar5 < __nptr) {
        do {
          pbVar10 = pbVar5 + 1;
          lVar12 = lVar12 + (ulong)(*pbVar5 == 9);
          pbVar5 = pbVar10;
        } while (__nptr != pbVar10);
      }
      iVar4 = mbsnwidth(__nptr,(long)pbVar6 - (long)__nptr,0);
      lVar11 = (long)iVar4;
      if (__nptr < pbVar6) {
        do {
          pbVar10 = __nptr + 1;
          lVar11 = lVar11 + (ulong)(*__nptr == 9);
          __nptr = pbVar10;
        } while (pbVar6 != pbVar10);
      }
      while (lVar12 != 0) {
        lVar12 = lVar12 + -1;
        pcVar1 = stdout->_IO_write_ptr;
        if (pcVar1 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar1 + 1;
          *pcVar1 = ' ';
        }
        else {
          __overflow(stdout,0x20);
        }
      }
      if (lVar11 == 0) {
        uVar7 = dcgettext(0,"^ no match for key\n",5);
        __printf_chk(1,uVar7);
      }
      else {
        do {
          pcVar1 = stdout->_IO_write_ptr;
          if (pcVar1 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar1 + 1;
            *pcVar1 = '_';
          }
          else {
            __overflow(stdout,0x5f);
          }
          lVar11 = lVar11 + -1;
        } while (lVar11 != 0);
        pcVar1 = stdout->_IO_write_ptr;
        if (pcVar1 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar1 + 1;
          *pcVar1 = '\n';
        }
        else {
          __overflow(stdout,10);
        }
      }
      if (plVar9 == (long *)0x0) goto LAB_00107fd3;
      plVar9 = (long *)plVar9[8];
      if (plVar9 != (long *)0x0) break;
      if ((unique != '\0') || (stable != '\0')) goto LAB_00107fd3;
      pbVar5 = *param_1;
      pbVar6 = pbVar5 + -1 + (long)param_1[1];
      __nptr = pbVar5;
    }
    pbVar5 = *param_1;
    pbVar10 = param_1[1];
    pbVar6 = pbVar5 + -1 + (long)pbVar10;
  } while( true );
}