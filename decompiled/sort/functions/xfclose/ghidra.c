void xfclose(FILE *param_1,undefined8 param_2)

{
  undefined8 uVar1;
  int iVar2;
  char *pcVar3;
  
  iVar2 = fileno(param_1);
  if (iVar2 == 0) {
    (*(code *)PTR_clearerr_unlocked_0011ccb0)(param_1);
    return;
  }
  if (iVar2 == 1) {
    iVar2 = fflush_unlocked(param_1);
    if (iVar2 != 0) {
      pcVar3 = "fflush failed";
      goto LAB_00107358;
    }
  }
  else {
    iVar2 = rpl_fclose(param_1);
    if (iVar2 != 0) {
      pcVar3 = "close failed";
LAB_00107358:
      uVar1 = dcgettext(0,pcVar3,5);
                    /* WARNING: Subroutine does not return */
      sort_die(uVar1,param_2);
    }
  }
  return;
}