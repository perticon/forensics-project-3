void zaptemp(char * name) {
    int64_t v1 = __readfsqword(40); // 0x8c4b
    int64_t v2 = (int64_t)temphead; // 0x8c5e
    int64_t v3 = (int64_t)&temphead; // 0x8c6c
    int64_t v4 = v2; // 0x8c6c
    if (name != (char *)&g62) {
        int64_t v5 = *(int64_t *)v2; // 0x8c71
        v3 = v2;
        v4 = v5;
        while (v5 + 13 != (int64_t)name) {
            int64_t v6 = v5;
            v5 = *(int64_t *)v6;
            v3 = v6;
            v4 = v5;
        }
    }
    // 0x8c7e
    char v7; // bp-200, 0x8c30
    if (*(char *)(v4 + 12) == 1) {
        char * v8 = hash_remove(proctab, (int32_t *)&v7); // 0x8d95
        if (v8 != NULL) {
            // 0x8da3
            *(char *)((int64_t)v8 + 12) = 2;
            reap(*(int32_t *)(v4 + 8));
        }
    }
    int64_t v9 = *(int64_t *)v4; // 0x8c96
    v7 = (int32_t)function_3c00() == 0;
    int64_t v10 = function_37e0(); // 0x8cac
    function_37d0();
    *(int64_t *)v3 = v9;
    if (v7 != 0) {
        // 0x8d08
        function_3c00();
        if ((int32_t)v10 == 0) {
            goto lab_0x8ccb;
        } else {
            goto lab_0x8d26;
        }
    } else {
        if ((int32_t)v10 != 0) {
            goto lab_0x8d26;
        } else {
            goto lab_0x8ccb;
        }
    }
  lab_0x8ccb:
    if (v9 == 0) {
        // 0x8d6c
        *(int64_t *)&temptail = v3;
        goto lab_0x8cd4;
    } else {
        goto lab_0x8cd4;
    }
  lab_0x8d26:
    // 0x8d26
    quotearg_n_style_colon();
    function_3920();
    function_3c90();
    if (v9 != 0) {
        goto lab_0x8cd4;
    } else {
        // 0x8d6c
        *(int64_t *)&temptail = v3;
        goto lab_0x8cd4;
    }
  lab_0x8cd4:
    // 0x8cd4
    function_3750();
    if (v1 == __readfsqword(40)) {
        // 0x8cf3
        return;
    }
    // 0x8db4
    function_3950();
}