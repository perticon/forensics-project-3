void zaptemp(char *param_1)

{
  char *pcVar1;
  long *plVar2;
  long **pplVar3;
  long **__ptr;
  int iVar4;
  int iVar5;
  int *piVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  long lVar9;
  long in_FS_OFFSET;
  char local_c8 [8];
  undefined4 local_c0;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  pcVar1 = (char *)((long)temphead + 0xd);
  pplVar3 = (long **)&temphead;
  __ptr = temphead;
  while (param_1 != pcVar1) {
    pcVar1 = (char *)((long)*__ptr + 0xd);
    pplVar3 = __ptr;
    __ptr = (long **)*__ptr;
  }
  if (*(char *)((long)__ptr + 0xc) == '\x01') {
    local_c0 = *(undefined4 *)(__ptr + 1);
    lVar9 = hash_remove(proctab,local_c8);
    if (lVar9 != 0) {
      *(undefined *)(lVar9 + 0xc) = 2;
      reap();
    }
  }
  plVar2 = *__ptr;
  iVar4 = pthread_sigmask(0,(__sigset_t *)caught_signals,(__sigset_t *)&local_c0);
  local_c8[0] = iVar4 == 0;
  iVar5 = unlink(param_1);
  piVar6 = __errno_location();
  iVar4 = *piVar6;
  *pplVar3 = plVar2;
  if (local_c8[0] != '\0') {
    pthread_sigmask(2,(__sigset_t *)&local_c0,(__sigset_t *)0x0);
  }
  if (iVar5 != 0) {
    uVar7 = quotearg_n_style_colon(0,3,param_1);
    uVar8 = dcgettext(0,"warning: cannot remove: %s",5);
                    /* WARNING: Subroutine does not return */
    error(0,iVar4,uVar8,uVar7);
  }
  if (plVar2 == (long *)0x0) {
    temptail = (undefined *)pplVar3;
  }
  free(__ptr);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}