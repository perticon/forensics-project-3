int32_t * init_node(int32_t * parent, int32_t * node_pool, int32_t * dest, uint64_t nthreads, int64_t total_lines, bool is_lo_child) {
    int64_t v1 = (int64_t)parent;
    int64_t v2; // 0x7790
    int64_t v3; // 0x7790
    if (!is_lo_child) {
        // 0x7848
        v2 = *(int64_t *)(v1 + 48) % 2;
        v3 = v1 + 24;
    } else {
        // 0x77c0
        v2 = *(int64_t *)(v1 + 40) % 2;
        v3 = v1 + 16;
    }
    int64_t v4 = (int64_t)node_pool;
    int64_t v5 = (int64_t)dest - 32 * total_lines; // 0x77ad
    int64_t v6 = 0;
    int64_t v7 = v5 - 32 * v6;
    *(int64_t *)(v4 + 56) = v1;
    int64_t v8 = v4 + 128; // 0x77e9
    *(int64_t *)(v4 + 32) = v3;
    *(int64_t *)(v4 + 16) = v5;
    *(int64_t *)node_pool = v5;
    *(int64_t *)(v4 + 24) = v7;
    *(int64_t *)(v4 + 8) = v7;
    *(int64_t *)(v4 + 40) = v6;
    *(int64_t *)(v4 + 48) = v2;
    *(int32_t *)(v4 + 80) = *(int32_t *)(v1 + 80) + 1;
    *(char *)(v4 + 84) = 0;
    function_3e40();
    int64_t * v9 = (int64_t *)(v4 + 64);
    if (nthreads < 2) {
        // 0x7823
        *v9 = 0;
        *(int64_t *)(v4 + 72) = 0;
        return (int32_t *)v8;
    }
    // 0x7870
    *v9 = v8;
    int32_t * v10 = init_node(node_pool, (int32_t *)v8, (int32_t *)v5, 0, total_lines, true); // 0x788f
    *(int64_t *)(v4 + 72) = (int64_t)v10;
    return init_node(node_pool, v10, (int32_t *)v7, nthreads % 2, total_lines, false);
}