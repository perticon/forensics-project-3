long * init_node(long param_1,long *param_2,long param_3,ulong param_4,long param_5,char param_6)

{
  int iVar1;
  long lVar2;
  long *plVar3;
  ulong uVar4;
  long lVar5;
  long lVar6;
  
  param_3 = param_3 + param_5 * -0x20;
  if (param_6 == '\0') {
    uVar4 = *(ulong *)(param_1 + 0x30) >> 1;
    lVar5 = *(ulong *)(param_1 + 0x30) - uVar4;
    lVar2 = param_1 + 0x18;
  }
  else {
    uVar4 = *(ulong *)(param_1 + 0x28) >> 1;
    lVar5 = *(ulong *)(param_1 + 0x28) - uVar4;
    lVar2 = param_1 + 0x10;
  }
  lVar6 = param_3 + uVar4 * -0x20;
  param_2[7] = param_1;
  iVar1 = *(int *)(param_1 + 0x50);
  plVar3 = param_2 + 0x10;
  param_2[4] = lVar2;
  param_2[2] = param_3;
  *param_2 = param_3;
  param_2[3] = lVar6;
  param_2[1] = lVar6;
  param_2[5] = uVar4;
  param_2[6] = lVar5;
  *(int *)(param_2 + 10) = iVar1 + 1;
  *(undefined *)((long)param_2 + 0x54) = 0;
  pthread_mutex_init((pthread_mutex_t *)(param_2 + 0xb),(pthread_mutexattr_t *)0x0);
  if (param_4 < 2) {
    param_2[8] = 0;
    param_2[9] = 0;
    return plVar3;
  }
  param_2[8] = (long)plVar3;
  lVar2 = init_node(param_2,plVar3,param_3,param_4 >> 1,param_5,1);
  param_2[9] = lVar2;
  plVar3 = (long *)init_node(param_2,lVar2,lVar6,param_4 - (param_4 >> 1),param_5,0);
  return plVar3;
}