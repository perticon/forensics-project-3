void* getmonth(void** rdi, void** rsi, void** rdx) {
    int64_t rax4;
    void** rbx5;
    int64_t rax6;
    uint64_t r10_7;
    uint64_t r11_8;
    void** rcx9;
    uint64_t r9_10;
    struct s24* rdx11;
    uint32_t eax12;
    int64_t rsi13;
    void* eax14;

    *reinterpret_cast<uint32_t*>(&rax4) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    rbx5 = rsi;
    if (*reinterpret_cast<signed char*>(0x1d760 + rax4)) {
        do {
            *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
            ++rdi;
        } while (*reinterpret_cast<signed char*>(0x1d760 + rax6));
    }
    *reinterpret_cast<int32_t*>(&r10_7) = 12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_7) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r11_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_8) + 4) = 0;
    do {
        rcx9 = rdi;
        r9_10 = r11_8 + r10_7 >> 1;
        rdx11 = *reinterpret_cast<struct s24**>(0x1d060 + (r9_10 << 4));
        eax12 = rdx11->f0;
        if (!*reinterpret_cast<unsigned char*>(&eax12)) 
            break;
        do {
            *reinterpret_cast<uint32_t*>(&rsi13) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx9));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(0x1d460 + rsi13) < *reinterpret_cast<unsigned char*>(&eax12)) 
                break;
            if (*reinterpret_cast<unsigned char*>(0x1d460 + rsi13) > *reinterpret_cast<unsigned char*>(&eax12)) 
                goto addr_6d88_8;
            eax12 = rdx11->f1;
            rdx11 = reinterpret_cast<struct s24*>(&rdx11->f1);
            ++rcx9;
        } while (*reinterpret_cast<unsigned char*>(&eax12));
        goto addr_6d70_10;
        r10_7 = r9_10;
        continue;
        addr_6d88_8:
        r11_8 = r9_10 + 1;
    } while (r11_8 < r10_7);
    goto addr_6d63_13;
    addr_6d70_10:
    if (rbx5) {
        *reinterpret_cast<void***>(rbx5) = rcx9;
    }
    eax14 = *reinterpret_cast<void**>(0x1d060 + (r9_10 << 4) + 8);
    addr_6d81_17:
    return eax14;
    addr_6d63_13:
    eax14 = reinterpret_cast<void*>(0);
    goto addr_6d81_17;
}