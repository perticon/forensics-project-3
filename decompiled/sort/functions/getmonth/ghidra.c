undefined4 getmonth(byte *param_1,byte **param_2)

{
  char cVar1;
  byte bVar2;
  byte *pbVar3;
  byte *pbVar4;
  ulong uVar5;
  ulong uVar6;
  ulong uVar7;
  bool bVar8;
  
  cVar1 = blanks[*param_1];
  while (cVar1 != '\0') {
    pbVar4 = param_1 + 1;
    param_1 = param_1 + 1;
    cVar1 = blanks[*pbVar4];
  }
  uVar7 = 0;
  uVar6 = 0xc;
  do {
    uVar5 = uVar7 + uVar6 >> 1;
    pbVar4 = *(byte **)(monthtab + uVar5 * 0x10);
    bVar2 = *pbVar4;
    pbVar3 = param_1;
    while( true ) {
      if (bVar2 == 0) {
        if (param_2 != (byte **)0x0) {
          *param_2 = pbVar3;
        }
        return *(undefined4 *)(monthtab + uVar5 * 0x10 + 8);
      }
      bVar8 = bVar2 <= (byte)fold_toupper[*pbVar3];
      if (!bVar8) goto LAB_00106d5e;
      if (bVar8 && fold_toupper[*pbVar3] != bVar2) break;
      bVar2 = pbVar4[1];
      pbVar4 = pbVar4 + 1;
      pbVar3 = pbVar3 + 1;
    }
    uVar7 = uVar5 + 1;
    uVar5 = uVar6;
LAB_00106d5e:
    uVar6 = uVar5;
    if (uVar5 <= uVar7) {
      return 0;
    }
  } while( true );
}