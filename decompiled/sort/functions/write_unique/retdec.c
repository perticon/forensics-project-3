void write_unique(int32_t * line, struct _IO_FILE * tfp, char * temp_output) {
    int64_t v1 = (int64_t)line;
    int64_t v2 = v1;
    if (*(char *)&unique == 0) {
        // 0xa145
        write_line((int32_t *)v1, tfp, temp_output);
        return;
    }
    // 0xa102
    if (saved_line != 0) {
        int64_t v3 = compare(v1, (int64_t)&saved_line); // 0xa11c
        v2 = v1;
        if ((int32_t)v3 == 0) {
            // 0xa158
            return;
        }
    }
    int128_t v4 = *(int128_t *)&v2; // 0xa12e
    *(int128_t *)&saved_line = (int128_t)__asm_movaps(__asm_movdqu(v4));
    int64_t v5 = v4;
    int128_t v6 = *(int128_t *)(v5 + 16); // 0xa139
    *(int128_t *)&g78 = (int128_t)__asm_movaps(__asm_movdqu(v6));
    // 0xa145
    write_line((int32_t *)v5, tfp, temp_output);
}