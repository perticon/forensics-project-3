void write_unique(undefined4 *param_1,undefined8 param_2,undefined8 param_3)

{
  int iVar1;
  
  if (unique != '\0') {
    if (CONCAT44(saved_line._4_4_,saved_line._0_4_) != 0) {
      iVar1 = compare(param_1,saved_line);
      if (iVar1 == 0) {
        return;
      }
    }
    saved_line._0_4_ = *param_1;
    saved_line._4_4_ = param_1[1];
    saved_line._8_4_ = param_1[2];
    saved_line._12_4_ = param_1[3];
    saved_line._16_4_ = param_1[4];
    saved_line._20_4_ = param_1[5];
    saved_line._24_4_ = param_1[6];
    saved_line._28_4_ = param_1[7];
  }
  write_line(param_1,param_2,param_3);
  return;
}