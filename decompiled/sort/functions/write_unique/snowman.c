void write_unique(void** rdi, void** rsi, void** rdx) {
    void*** rsp4;
    int1_t zf5;
    int1_t zf6;
    void** rax7;
    void** rsi8;
    void** r12_9;
    void** rbp10;
    void* rsp11;
    void** r14_12;
    void** r15_13;
    void** v14;
    void** rbx15;
    void** rax16;
    void** v17;
    int1_t zf18;
    uint32_t eax19;
    void** rax20;
    void** rdx21;
    void** rax22;
    void** rdi23;
    void** rbx24;
    void** rsi25;
    void** v26;
    void** rax27;
    void** v28;
    int32_t eax29;
    void* rsp30;
    uint32_t eax31;
    int1_t cf32;
    uint32_t edx33;
    int32_t eax34;
    void** rax35;
    int32_t eax36;
    void** r13_37;
    unsigned char v38;
    void** eax39;
    void** r8d40;
    void** ecx41;
    void** rdi42;
    void** rsi43;
    void** rsi44;
    uint32_t eax45;
    int32_t eax46;
    int1_t cf47;
    void* rax48;
    int64_t v49;
    void** r13_50;
    void** rbp51;
    void** rbx52;
    void** rax53;
    void** v54;
    void** rax55;
    void* rsp56;
    void** v57;
    void* rax58;
    void** rsi59;
    void** rax60;
    void** rax61;
    void** r13d62;
    int64_t v63;
    void** r12_64;
    void** r15_65;
    uint32_t eax66;
    void** rdi67;
    void** rax68;
    void** r9d69;
    void** rdi70;
    void** rsi71;
    struct s3* rax72;
    void** rdi73;
    int32_t eax74;
    void** rdi75;
    void** eax76;
    int64_t rdi77;
    int32_t v78;
    void** rax79;
    void** rax80;
    int32_t v81;
    void** rax82;
    void** edi83;
    uint32_t tmp32_84;
    void* rax85;
    void** rbx86;
    int32_t eax87;
    void* rsp88;
    void* rax89;
    int32_t eax90;
    void* rax91;
    void** rdi92;
    void** rax93;
    int1_t cf94;
    void** rax95;
    void** rdi96;
    void** rax97;
    void** rdi98;
    void** rax99;
    int1_t zf100;
    int1_t zf101;
    void** r10_102;
    void** rax103;
    uint32_t r15d104;
    struct s16* rax105;
    struct s17* rax106;
    void** v107;
    void** rax108;
    void* rax109;
    struct s4** rdi110;
    signed char al111;
    void** rax112;
    int64_t rcx113;
    void** rax114;
    int32_t ecx115;
    uint32_t eax116;
    void** rax117;
    int32_t eax118;

    rsp4 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16);
    zf5 = unique == 0;
    if (!zf5) {
        zf6 = saved_line == 0;
        if (zf6 || (rax7 = compare(rdi, 0x1d860, rdx), rsp4 = rsp4 - 8 + 8, rdi = rdi, rdx = rdx, !!*reinterpret_cast<int32_t*>(&rax7))) {
            __asm__("movdqu xmm0, [rdi]");
            __asm__("movaps [rip+0x13727], xmm0");
            __asm__("movdqu xmm1, [rdi+0x10]");
            __asm__("movaps [rip+0x1372b], xmm1");
        } else {
            return;
        }
    }
    rsi8 = rsi;
    r12_9 = rsi8;
    rbp10 = rdx;
    rsp11 = reinterpret_cast<void*>(rsp4 + 16 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    r14_12 = *reinterpret_cast<void***>(rdi);
    r15_13 = *reinterpret_cast<void***>(rdi + 8);
    v14 = rdi;
    rbx15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r15_13));
    rax16 = g28;
    v17 = rax16;
    if (rdx || (zf18 = debug == 0, zf18)) {
        eax19 = eolchar;
        *reinterpret_cast<void***>(rbx15 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax19);
        rax20 = fun_3bf0(r14_12, 1, r15_13, r12_9);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        if (r15_13 != rax20) {
            addr_83ee_8:
            *reinterpret_cast<int32_t*>(&rdx21) = 5;
            *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
            rax22 = fun_3920();
            rdi23 = rax22;
            sort_die(rdi23, rbp10, 5, rdi23, rbp10, 5);
        } else {
            *reinterpret_cast<void***>(rbx15 + 0xffffffffffffffff) = reinterpret_cast<void**>(0);
            goto addr_7fd3_10;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r14_12) < reinterpret_cast<unsigned char>(rbx15)) 
            goto addr_803c_12;
        goto addr_80a8_14;
    }
    rbx24 = rbp10;
    *reinterpret_cast<int32_t*>(&rsi25) = 0x80000;
    *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
    v26 = rdi23;
    rax27 = g28;
    v28 = rax27;
    eax29 = rpl_pipe2();
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8);
    if (eax29 >= 0) {
        eax31 = nmerge;
        cf32 = eax31 + 1 < nprocs;
        if (cf32) {
            rdi23 = reinterpret_cast<void**>(0xffffffff);
            *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
            reap(0xffffffff, 0x80000);
            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
            while ((edx33 = nprocs, !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(edx33) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(edx33 == 0))) && (rdi23 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0, eax34 = reap(0, 0x80000), rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8), !!eax34)) {
            }
        }
        rax35 = fun_37d0();
        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
        --rbx24;
        *reinterpret_cast<void***>(rdi23) = g80000;
        r12_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp30) + 40);
        rbp10 = rax35;
        *reinterpret_cast<void***>(rdi23 + 4) = g80004;
        while (1) {
            rdi23 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
            rdx21 = r12_9;
            rsi25 = reinterpret_cast<void**>(0x1d3a0);
            eax36 = fun_3c00();
            r13_37 = temphead;
            temphead = reinterpret_cast<void**>(0);
            v38 = reinterpret_cast<uint1_t>(eax36 == 0);
            eax39 = fun_3e70();
            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
            r8d40 = *reinterpret_cast<void***>(rbp10);
            ecx41 = eax39;
            if (eax39) {
                temphead = r13_37;
                if (v38) {
                    addr_8539_30:
                    *reinterpret_cast<int32_t*>(&rdx21) = 0;
                    *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
                    rsi25 = r12_9;
                    rdi23 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
                    fun_3c00();
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                    ecx41 = ecx41;
                    r8d40 = r8d40;
                    goto addr_84a2_31;
                } else {
                    addr_84a2_31:
                    *reinterpret_cast<void***>(rbp10) = r8d40;
                    if (reinterpret_cast<signed char>(ecx41) >= reinterpret_cast<signed char>(0)) 
                        goto addr_855f_32;
                }
                if (!reinterpret_cast<int1_t>(r8d40 == 11)) 
                    goto addr_85e4_34;
                *reinterpret_cast<void***>(rdi23) = *reinterpret_cast<void***>(rsi25);
                rdi42 = rdi23 + 4;
                rsi43 = rsi25 + 4;
                xnanosleep();
                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                *reinterpret_cast<void***>(rdi42) = *reinterpret_cast<void***>(rsi43);
                rsi44 = rsi43 + 4;
                __asm__("movapd xmm1, xmm2");
                __asm__("addsd xmm1, xmm2");
                *reinterpret_cast<void***>(rdi42 + 4) = *reinterpret_cast<void***>(rsi44);
                rsi25 = rsi44 + 4;
                do {
                    eax45 = nprocs;
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax45) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax45 == 0)) 
                        break;
                    eax46 = reap(0, rsi25);
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                } while (eax46);
                cf47 = reinterpret_cast<unsigned char>(rbx24) < reinterpret_cast<unsigned char>(1);
                --rbx24;
                if (cf47) 
                    goto addr_85e0_47;
            } else {
                if (!v38) 
                    goto addr_8598_49; else 
                    goto addr_8539_30;
            }
        }
    }
    addr_8568_50:
    rax48 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v28) - reinterpret_cast<unsigned char>(g28));
    if (!rax48) {
        goto v49;
    }
    fun_3950();
    r13_50 = rsi25;
    rbp51 = rdx21;
    rbx52 = rdi23;
    rax53 = g28;
    v54 = rax53;
    rax55 = xnmalloc(r13_50, 8, rdx21, r13_50, 8, rdx21);
    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    v57 = rax55;
    *reinterpret_cast<void***>(rbp51) = rax55;
    if (r13_50) 
        goto addr_8666_54;
    addr_8727_56:
    while (rax58 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v54) - reinterpret_cast<unsigned char>(g28)), !!rax58) {
        addr_885c_57:
        fun_3950();
        addr_8861_58:
        rsi59 = compress_program;
        quotearg_style(4, rsi59, rdx21, 4, rsi59, rdx21);
        rax60 = fun_3920();
        rdx21 = rax60;
        fun_3c90();
        addr_889c_59:
        rax61 = fun_37d0();
        r13d62 = *reinterpret_cast<void***>(rax61);
        fun_3a40();
        *reinterpret_cast<void***>(rbp51) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax61) = r13d62;
    }
    goto v63;
    addr_8666_54:
    rbp51 = rax55;
    *reinterpret_cast<int32_t*>(&r12_64) = 0;
    *reinterpret_cast<int32_t*>(&r12_64 + 4) = 0;
    do {
        r15_65 = *reinterpret_cast<void***>(rbx52 + 8);
        if (!r15_65 || (eax66 = *reinterpret_cast<unsigned char*>(r15_65 + 12), *reinterpret_cast<signed char*>(&eax66) == 0)) {
            rdi67 = *reinterpret_cast<void***>(rbx52);
            rax68 = stream_open(rdi67, "r", rdx21, rdi67, "r", rdx21);
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            *reinterpret_cast<void***>(rbp51) = rax68;
            if (!rax68) 
                goto addr_8727_56;
        } else {
            if (*reinterpret_cast<signed char*>(&eax66) == 1 && (r9d69 = *reinterpret_cast<void***>(r15_65 + 8), rdi70 = proctab, rsi71 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp56) + 16), rax72 = hash_remove(rdi70, rsi71, rdx21, rdi70, rsi71, rdx21), rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8), !!rax72)) {
                rax72->fc = 2;
                reap(r9d69, rsi71);
                rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            }
            rdi73 = r15_65 + 13;
            eax74 = fun_3cd0(rdi73, rdi73);
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            if (eax74 < 0) 
                break;
            rdi75 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp56) + 16);
            eax76 = pipe_fork(rdi75, 9, rdx21, rdi75, 9, rdx21);
            if (reinterpret_cast<int1_t>(eax76 == 0xffffffff)) 
                goto addr_86f1_67;
            if (!eax76) 
                goto addr_87fa_69;
            *reinterpret_cast<void***>(r15_65 + 8) = eax76;
            register_proc(r15_65, 9, rdx21, r15_65, 9, rdx21);
            fun_3a40();
            fun_3a40();
            *reinterpret_cast<int32_t*>(&rdi77) = v78;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi77) + 4) = 0;
            rax79 = fun_3c20(rdi77, "r", rdx21, rdi77, "r", rdx21);
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            if (!rax79) 
                goto addr_889c_59;
            *reinterpret_cast<void***>(rbp51) = rax79;
        }
        ++r12_64;
        rbx52 = rbx52 + 16;
        rbp51 = rbp51 + 8;
    } while (r13_50 != r12_64);
    goto addr_8727_56;
    *reinterpret_cast<void***>(v57 + reinterpret_cast<unsigned char>(r12_64) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_56;
    addr_86f1_67:
    rax80 = fun_37d0();
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax80) == 24)) 
        goto addr_8861_58;
    fun_3a40();
    *reinterpret_cast<void***>(rax80) = reinterpret_cast<void**>(24);
    *reinterpret_cast<void***>(v57 + reinterpret_cast<unsigned char>(r12_64) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_56;
    addr_87fa_69:
    fun_3a40();
    if (eax74) {
        move_fd_part_0(eax74, eax74);
    }
    if (v81 != 1) {
        move_fd_part_0(v81, v81);
    }
    rdx21 = reinterpret_cast<void**>("-d");
    fun_3e20();
    rax82 = fun_37d0();
    edi83 = *reinterpret_cast<void***>(rax82);
    async_safe_die(edi83, "couldn't execute compress program (with -d)", edi83, "couldn't execute compress program (with -d)");
    goto addr_885c_57;
    addr_855f_32:
    if (!ecx41) {
        addr_8598_49:
        fun_3a40();
        rdi23 = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
        fun_3a40();
        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
        goto addr_8568_50;
    } else {
        tmp32_84 = nprocs + 1;
        nprocs = tmp32_84;
        goto addr_8568_50;
    }
    addr_85e4_34:
    fun_3a40();
    rdi23 = *reinterpret_cast<void***>(v26 + 4);
    *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
    fun_3a40();
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
    *reinterpret_cast<void***>(rbp10) = r8d40;
    goto addr_8568_50;
    addr_85e0_47:
    r8d40 = *reinterpret_cast<void***>(rbp10);
    goto addr_85e4_34;
    addr_7fd3_10:
    rax85 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v17) - reinterpret_cast<unsigned char>(g28));
    if (rax85) {
        fun_3950();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        goto addr_83ee_8;
    } else {
        return;
    }
    addr_80a8_14:
    rbp10 = keylist;
    r13_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r15_13) + 0xffffffffffffffff);
    if (!rbp10) {
        while (1) {
            rbx86 = r14_12;
            while (1) {
                eax87 = mbsnwidth(r14_12);
                rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                r15_13 = reinterpret_cast<void**>(static_cast<int64_t>(eax87));
                if (reinterpret_cast<unsigned char>(r14_12) < reinterpret_cast<unsigned char>(rbx86)) {
                    do {
                        ++r14_12;
                        *reinterpret_cast<int32_t*>(&rax89) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax89) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r14_12 + 0xffffffffffffffff) == 9);
                        r15_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_13) + reinterpret_cast<uint64_t>(rax89));
                    } while (rbx86 != r14_12);
                }
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                rsi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_37) - reinterpret_cast<unsigned char>(rbx86));
                eax90 = mbsnwidth(rbx86);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp88) - 8 + 8);
                r14_12 = reinterpret_cast<void**>(static_cast<int64_t>(eax90));
                if (reinterpret_cast<unsigned char>(rbx86) < reinterpret_cast<unsigned char>(r13_37)) {
                    do {
                        ++rbx86;
                        *reinterpret_cast<int32_t*>(&rax91) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax91) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx86 + 0xffffffffffffffff) == 9);
                        r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rax91));
                    } while (r13_37 != rbx86);
                }
                rbx15 = r15_13 + 0xffffffffffffffff;
                if (r15_13) {
                    do {
                        rdi92 = stdout;
                        rax93 = *reinterpret_cast<void***>(rdi92 + 40);
                        if (reinterpret_cast<unsigned char>(rax93) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi92 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi8) = 32;
                            *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                            fun_39c0(rdi92, 32);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        } else {
                            *reinterpret_cast<void***>(rdi92 + 40) = rax93 + 1;
                            *reinterpret_cast<void***>(rax93) = reinterpret_cast<void**>(32);
                        }
                        cf94 = reinterpret_cast<unsigned char>(rbx15) < reinterpret_cast<unsigned char>(1);
                        --rbx15;
                    } while (!cf94);
                }
                if (!r14_12) {
                    *reinterpret_cast<int32_t*>(&rdx) = 5;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rax95 = fun_3920();
                    rsi8 = rax95;
                    fun_3c40(1, rsi8, 5);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
                } else {
                    do {
                        rdi96 = stdout;
                        rax97 = *reinterpret_cast<void***>(rdi96 + 40);
                        if (reinterpret_cast<unsigned char>(rax97) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi96 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi8) = 95;
                            *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                            fun_39c0(rdi96, 95);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        } else {
                            *reinterpret_cast<void***>(rdi96 + 40) = rax97 + 1;
                            *reinterpret_cast<void***>(rax97) = reinterpret_cast<void**>(95);
                        }
                        --r14_12;
                    } while (r14_12);
                    rdi98 = stdout;
                    rax99 = *reinterpret_cast<void***>(rdi98 + 40);
                    if (reinterpret_cast<unsigned char>(rax99) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi98 + 48))) 
                        goto addr_83b2_103; else 
                        goto addr_82a7_104;
                }
                addr_82b2_105:
                if (!rbp10) 
                    goto addr_7fd3_10;
                rbp10 = *reinterpret_cast<void***>(rbp10 + 64);
                if (!rbp10) {
                    zf100 = unique == 0;
                    if (!zf100) 
                        goto addr_7fd3_10;
                    zf101 = stable == 0;
                    if (!zf101) 
                        goto addr_7fd3_10;
                    r14_12 = *reinterpret_cast<void***>(v14);
                    rbx86 = r14_12;
                    r13_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v14 + 8)) + 0xffffffffffffffff);
                    continue;
                }
                r14_12 = *reinterpret_cast<void***>(v14);
                r15_13 = *reinterpret_cast<void***>(v14 + 8);
                r13_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r15_13) + 0xffffffffffffffff);
                if (!rbp10) 
                    break;
                addr_80c8_111:
                r10_102 = *reinterpret_cast<void***>(rbp10);
                if (r10_102 != 0xffffffffffffffff) 
                    goto addr_80d6_112;
                rbx86 = r14_12;
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp10 + 16) == 0xffffffffffffffff)) {
                    addr_80ee_114:
                    rdx = rbp10;
                    rsi8 = r15_13;
                    rax103 = limfield_isra_0(r14_12, rsi8, rdx);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    r13_37 = rax103;
                }
                if (!reinterpret_cast<int1_t>(r10_102 == 0xffffffffffffffff) || !*reinterpret_cast<void***>(rbp10 + 48)) {
                    addr_810b_117:
                    if (*reinterpret_cast<unsigned char*>(rbp10 + 54)) 
                        goto addr_8125_118;
                    if (!(0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp10 + 48)))) 
                        continue; else 
                        goto addr_8125_118;
                } else {
                    addr_8125_118:
                    r15d104 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_37));
                    *reinterpret_cast<void***>(r13_37) = reinterpret_cast<void**>(0);
                    *reinterpret_cast<uint32_t*>(&rax105) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx86));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d760) + reinterpret_cast<uint64_t>(rax105))) {
                        do {
                            *reinterpret_cast<uint32_t*>(&rax106) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx86 + 1));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
                            ++rbx86;
                        } while (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d760) + reinterpret_cast<uint64_t>(rax106)));
                    }
                }
                v107 = rbx86;
                if (reinterpret_cast<unsigned char>(rbx86) > reinterpret_cast<unsigned char>(r13_37)) 
                    goto addr_8310_123;
                if (*reinterpret_cast<unsigned char*>(rbp10 + 54)) {
                    getmonth(rbx86, reinterpret_cast<int64_t>(rsp11) + 24, rdx);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                } else {
                    if (*reinterpret_cast<unsigned char*>(rbp10 + 52)) {
                        fun_3c80(rbx86, reinterpret_cast<int64_t>(rsp11) + 24, rdx);
                        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        __asm__("fstp st0");
                    } else {
                        if (!(0xff0000ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp10 + 48)))) {
                            addr_8310_123:
                            v107 = r13_37;
                        } else {
                            rax108 = rbx86;
                            if (reinterpret_cast<unsigned char>(rbx86) < reinterpret_cast<unsigned char>(r13_37)) {
                                *reinterpret_cast<int32_t*>(&rax109) = 0;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax109) + 4) = 0;
                                *reinterpret_cast<unsigned char*>(&rax109) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx86) == 45);
                                rax108 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax109) + reinterpret_cast<unsigned char>(rbx86));
                            }
                            rdi110 = reinterpret_cast<struct s4**>(reinterpret_cast<int64_t>(rsp11) + 32);
                            al111 = traverse_raw_number(rdi110, rsi8, rdi110, rsi8);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                            if (al111 > 47) {
                                rax112 = rax108;
                                *reinterpret_cast<uint32_t*>(&rcx113) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax112));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx113) + 4) = 0;
                                if (*reinterpret_cast<unsigned char*>(rbp10 + 53)) {
                                    rax112 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax112) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax112) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(0x15360 + rcx113) < 1)))))));
                                }
                                v107 = rax112;
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(r13_37) = *reinterpret_cast<void***>(&r15d104);
                r13_37 = v107;
                continue;
                addr_80d6_112:
                rdx = rbp10;
                rsi8 = r15_13;
                rax114 = begfield_isra_0(r14_12, rsi8, rdx);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                rbx86 = rax114;
                if (*reinterpret_cast<void***>(rbp10 + 16) == 0xffffffffffffffff) 
                    goto addr_810b_117; else 
                    goto addr_80ee_114;
                addr_83b2_103:
                *reinterpret_cast<uint32_t*>(&rsi8) = 10;
                *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                fun_39c0(rdi98, 10);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                goto addr_82b2_105;
                addr_82a7_104:
                rdx = rax99 + 1;
                *reinterpret_cast<void***>(rdi98 + 40) = rdx;
                *reinterpret_cast<void***>(rax99) = reinterpret_cast<void**>(10);
                goto addr_82b2_105;
            }
        }
    } else {
        goto addr_80c8_111;
    }
    addr_809c_137:
    r14_12 = *reinterpret_cast<void***>(v14);
    r15_13 = *reinterpret_cast<void***>(v14 + 8);
    goto addr_80a8_14;
    while (1) {
        addr_8090_138:
        ecx115 = 10;
        eax116 = 10;
        goto addr_8020_139;
        addr_806e_140:
        rax117 = fun_3920();
        *reinterpret_cast<uint32_t*>(&rsi8) = 0;
        *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
        sort_die(rax117, 0, 5);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
        continue;
        while (*reinterpret_cast<uint32_t*>(&rsi8) = *reinterpret_cast<unsigned char*>(&ecx115), *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0, eax118 = fun_39c0(r12_9, rsi8), rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8), eax118 != -1) {
            while (1) {
                if (rbx15 == r14_12) 
                    goto addr_809c_137;
                addr_803c_12:
                eax116 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_12));
                ++r14_12;
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax116) == 9)) {
                    ecx115 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&eax116));
                    if (rbx15 == r14_12) 
                        goto addr_8090_138;
                    addr_8020_139:
                    rdx = *reinterpret_cast<void***>(r12_9 + 40);
                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_9 + 48))) 
                        break;
                } else {
                    rdx = *reinterpret_cast<void***>(r12_9 + 40);
                    ecx115 = 62;
                    eax116 = 62;
                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_9 + 48))) 
                        break;
                }
                *reinterpret_cast<void***>(r12_9 + 40) = rdx + 1;
                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax116);
            }
        }
        goto addr_806e_140;
    }
}