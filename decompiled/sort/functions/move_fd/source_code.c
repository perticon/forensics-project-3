move_fd (int oldfd, int newfd)
{
  if (oldfd != newfd)
    {
      /* This should never fail for our usage.  */
      dup2 (oldfd, newfd);
      close (oldfd);
    }
}