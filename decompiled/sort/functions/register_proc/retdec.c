void register_proc(int32_t * temp) {
    int64_t v1 = (int64_t)proctab; // 0x773e
    if (proctab == NULL) {
        int32_t * v2 = hash_initialize(47, NULL, (int64_t (*)(int32_t *, int64_t))0x6bf0, (bool (*)(int32_t *, int32_t *))0x6c10, NULL); // 0x7770
        v1 = (int64_t)v2;
        *(int64_t *)&proctab = v1;
        if (v2 == NULL) {
            // 0x7784
            xalloc_die();
            return;
        }
    }
    // 0x7740
    *(char *)((int64_t)temp + 12) = 1;
    if (hash_insert((int32_t *)v1, temp) != NULL) {
        // 0x7751
        return;
    }
    // 0x7784
    xalloc_die();
}