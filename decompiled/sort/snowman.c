
signed char default_key_compare(void** rdi, ...) {
    uint32_t eax2;
    uint32_t eax3;

    eax2 = 0;
    if (*reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<void***>(rdi + 40)) {
        addr_6d99_2:
        return *reinterpret_cast<signed char*>(&eax2);
    } else {
        eax2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48));
        if (*reinterpret_cast<signed char*>(&eax2)) 
            goto addr_6de0_4;
        if (*reinterpret_cast<signed char*>(rdi + 49)) 
            goto addr_6d99_2;
        if (0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48))) 
            goto addr_6d99_2;
    }
    eax2 = *reinterpret_cast<unsigned char*>(rdi + 54);
    if (*reinterpret_cast<signed char*>(&eax2)) {
        addr_6de0_4:
        return 0;
    } else {
        if (!*reinterpret_cast<void***>(rdi + 56)) {
            eax3 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rdi + 51)) ^ 1;
            return *reinterpret_cast<signed char*>(&eax3);
        }
    }
}

void** fun_3920();

void fun_3c90();

void** quote(void** rdi, ...);

void** g28;

uint32_t xstrtoumax(void** rdi, ...);

void fun_3950();

void** quotearg_n_style_colon();

void** fun_37d0();

void incompatible_options(void** rdi, ...) {
    void** rax2;
    uint32_t eax3;
    int64_t rax4;
    void* rdx5;
    int64_t v6;

    fun_3920();
    fun_3c90();
    quote(2, 2);
    fun_3920();
    fun_3920();
    fun_3c90();
    rax2 = g28;
    eax3 = xstrtoumax(2, 2);
    if (eax3 <= 4) {
        *reinterpret_cast<uint32_t*>(&rax4) = eax3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x150b8 + rax4 * 4) + 0x150b8;
    }
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (!rdx5) 
        goto addr_70b5_7;
    fun_3950();
    quote(2, 2);
    fun_3920();
    fun_3920();
    fun_3c90();
    if (!1) 
        goto addr_7142_11;
    while (1) {
        fun_3920();
        addr_7142_11:
        quotearg_n_style_colon();
        fun_37d0();
        fun_3c90();
    }
    addr_70b5_7:
    goto v6;
}

void** parse_field_count(void** rdi, void** rsi, int64_t rdx) {
    void** rax4;
    uint32_t eax5;
    int64_t rax6;
    void* rdx7;
    void** v8;

    rax4 = g28;
    eax5 = xstrtoumax(rdi, rdi);
    if (eax5 <= 4) {
        *reinterpret_cast<uint32_t*>(&rax6) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x150b8 + rax6 * 4) + 0x150b8;
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (!rdx7) 
        goto addr_70b5_5;
    fun_3950();
    quote(rdi, rdi);
    fun_3920();
    fun_3920();
    fun_3c90();
    if (!1) 
        goto addr_7142_9;
    while (1) {
        fun_3920();
        addr_7142_9:
        quotearg_n_style_colon();
        fun_37d0();
        fun_3c90();
    }
    addr_70b5_5:
    return v8;
}

void** xmemdup(void** rdi, int64_t rsi);

void** keylist = reinterpret_cast<void**>(0);

void insertkey(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rdx5;
    void*** rcx6;
    void** rcx7;

    rax4 = xmemdup(rdi, 72);
    rdx5 = keylist;
    rcx6 = reinterpret_cast<void***>(0x1d430);
    if (rdx5) {
        do {
            rcx7 = rdx5;
            rdx5 = *reinterpret_cast<void***>(rdx5 + 64);
        } while (rdx5);
        rcx6 = reinterpret_cast<void***>(rcx7 + 64);
    }
    *rcx6 = rax4;
    *reinterpret_cast<void***>(rax4 + 64) = reinterpret_cast<void**>(0);
    return;
}

int64_t temp_dir_count = 0;

int64_t temp_dir_alloc = 0;

void** temp_dirs = reinterpret_cast<void**>(0);

void** x2nrealloc();

void add_temp_dir(void** rdi, ...) {
    int64_t rdx2;
    int1_t zf3;
    void** rdi4;
    void** rax5;

    rdx2 = temp_dir_count;
    zf3 = rdx2 == temp_dir_alloc;
    rdi4 = temp_dirs;
    if (zf3) {
        rax5 = x2nrealloc();
        rdx2 = temp_dir_count;
        temp_dirs = rax5;
        rdi4 = rax5;
    }
    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi4) + reinterpret_cast<uint64_t>(rdx2 * 8)) = rdi;
    temp_dir_count = rdx2 + 1;
    return;
}

void fun_3980();

/* move_fd.part.0 */
void move_fd_part_0(int32_t edi, ...) {
    fun_3980();
}

int32_t fun_3cd0(void** rdi, ...);

void** fun_3c20(int64_t rdi, void** rsi, void** rdx, ...);

signed char have_read_stdin = 0;

void** stdin = reinterpret_cast<void**>(0);

void fadvise(void** rdi, int64_t rsi);

int32_t fun_39e0(int64_t rdi);

/* outstat_errno.4 */
void** outstat_errno_4 = reinterpret_cast<void**>(0);

int32_t fun_3e50();

uint32_t g1d258 = 0;

void** stdout = reinterpret_cast<void**>(0);

void fun_3a10(void** rdi, void** rsi, void** rdx, void** rcx);

uint32_t fun_3b40(void** rdi, void** rsi, void** rdx, ...);

int32_t fun_3df0(void** rdi);

void sort_die(void** rdi, void** rsi, void** rdx, ...);

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx, ...);

void** stream_open(void** rdi, void** rsi, void** rdx, ...) {
    uint32_t eax4;
    void** r13_5;
    int32_t eax6;
    int64_t rdi7;
    void** rax8;
    int32_t eax9;
    void** rax10;
    void** r12_11;
    void** eax12;
    int32_t eax13;
    void** rax14;
    uint32_t eax15;
    void** r13_16;
    uint32_t eax17;
    int32_t eax18;
    void** rax19;
    int64_t rax20;
    int64_t rbp21;

    eax4 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi));
    if (*reinterpret_cast<signed char*>(&eax4) == 0x72) {
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45) || *reinterpret_cast<void***>(rdi + 1)) {
            *reinterpret_cast<int32_t*>(&r13_5) = 0;
            *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0;
            eax6 = fun_3cd0(rdi, rdi);
            *reinterpret_cast<int32_t*>(&rdi7) = eax6;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
            if (eax6 >= 0) {
                rax8 = fun_3c20(rdi7, rsi, rdx);
                r13_5 = rax8;
            }
        } else {
            have_read_stdin = 1;
            r13_5 = stdin;
        }
        fadvise(r13_5, 2);
        return r13_5;
    }
    if (*reinterpret_cast<signed char*>(&eax4) == 0x77) {
        if (rdi && (eax9 = fun_39e0(1), !!eax9)) {
            rax10 = fun_37d0();
            r12_11 = rax10;
            eax12 = outstat_errno_4;
            if (eax12) 
                goto addr_7245_10;
            while (1) {
                eax13 = fun_3e50();
                if (eax13) {
                    eax12 = *reinterpret_cast<void***>(r12_11);
                    outstat_errno_4 = eax12;
                    addr_7245_10:
                    if (reinterpret_cast<signed char>(eax12) < reinterpret_cast<signed char>(0)) 
                        goto addr_7249_13;
                } else {
                    outstat_errno_4 = reinterpret_cast<void**>(0xffffffff);
                    goto addr_7249_13;
                }
                addr_725f_15:
                rax14 = quotearg_n_style_colon();
                r12_11 = rax14;
                fun_3920();
                fun_3c90();
                continue;
                addr_7249_13:
                eax15 = g1d258;
                if ((eax15 & 0xf000) != 0x8000) 
                    break; else 
                    goto addr_725f_15;
            }
        }
        r13_16 = stdout;
        return r13_16;
    }
    fun_3a10("!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5, "stream_open");
    eax17 = fun_3b40("!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5, "!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5);
    if (eax17) 
        goto addr_7316_19;
    addr_7316_19:
    if (eax17 == 1) {
        eax18 = fun_3df0("!\"unexpected mode passed to stream_open\"");
        if (eax18) {
            while (1) {
                rax19 = fun_3920();
                sort_die(rax19, "src/sort.c", 5, rax19, "src/sort.c", 5);
                addr_736a_25:
            }
        }
    } else {
        rax20 = rpl_fclose("!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5, "!\"unexpected mode passed to stream_open\"", "src/sort.c", 0x3d5);
        if (*reinterpret_cast<int32_t*>(&rax20)) 
            goto addr_736a_25;
    }
    goto rbp21;
}

void** merge_buffer_size = reinterpret_cast<void**>(0);

unsigned char eolchar = 10;

void fun_3c70(void** rdi, void** rsi, void** rdx);

struct s0 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
};

void** fun_38c0(void** rdi, int64_t rsi, void** rdx, void** rcx);

void** fun_3a70(void** rdi, int64_t rsi);

/* limfield.isra.0 */
void** limfield_isra_0(void** rdi, void** rsi, void** rdx, ...);

/* begfield.isra.0 */
void** begfield_isra_0(void** rdi, void** rsi, void** rdx, ...);

signed char fillbuf(void** rdi, void** rsi, void** rdx) {
    void** rbx4;
    void** rbp5;
    void** v6;
    void** v7;
    void** v8;
    void** rax9;
    void** v10;
    uint32_t eax11;
    int1_t zf12;
    unsigned char v13;
    void** v14;
    int32_t eax15;
    void** r10_16;
    void** rdx17;
    void** rdi18;
    void** rax19;
    void** rbp20;
    void** r15_21;
    void** r12_22;
    void** v23;
    void*** rcx24;
    void** r14_25;
    struct s0* r12_26;
    void** v27;
    void** r13_28;
    void** r15_29;
    struct s0* r14_30;
    struct s0* r12_31;
    void** r14_32;
    void** r15_33;
    void** rax34;
    void** rax35;
    void** v36;
    void** r10_37;
    struct s0* r15_38;
    void** r14_39;
    void** r13_40;
    int32_t ebx41;
    void** r12_42;
    void** eax43;
    int64_t rsi44;
    void** rax45;
    void** r11_46;
    int1_t zf47;
    void** rax48;
    int64_t rax49;
    uint32_t ecx50;
    void** rdi51;
    void** rdx52;
    void** rax53;
    void** rax54;
    void* rdx55;
    void** rax56;

    rbx4 = keylist;
    rbp5 = merge_buffer_size;
    v6 = rdi;
    v7 = rsi;
    v8 = rdx;
    rax9 = g28;
    v10 = rax9;
    eax11 = eolchar;
    zf12 = *reinterpret_cast<void***>(rdi + 48) == 0;
    v13 = *reinterpret_cast<unsigned char*>(&eax11);
    v14 = *reinterpret_cast<void***>(rdi + 40);
    if (!zf12) {
        addr_7d95_2:
        eax15 = 0;
    } else {
        r10_16 = *reinterpret_cast<void***>(rdi + 8);
        rdx17 = *reinterpret_cast<void***>(rdi + 32);
        if (r10_16 != rdx17) {
            rdi18 = *reinterpret_cast<void***>(v6);
            fun_3c70(rdi18, reinterpret_cast<unsigned char>(rdi18) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r10_16) - reinterpret_cast<unsigned char>(rdx17)), rdx17);
            r10_16 = *reinterpret_cast<void***>(v6 + 32);
            *reinterpret_cast<void***>(v6 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax19) = 0;
            *reinterpret_cast<int32_t*>(&rax19 + 4) = 0;
            *reinterpret_cast<void***>(v6 + 8) = r10_16;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 16);
        }
        rbp20 = rbp5 - 34;
        r15_21 = *reinterpret_cast<void***>(v6);
        r12_22 = *reinterpret_cast<void***>(v6 + 24);
        v23 = v14 + 1;
        while (1) {
            rcx24 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(r12_22));
            r14_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(r10_16));
            r12_26 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rcx24) - (reinterpret_cast<unsigned char>(rax19) << 5));
            v27 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx24) - reinterpret_cast<unsigned char>(v14) * reinterpret_cast<unsigned char>(rax19) - reinterpret_cast<unsigned char>(r14_25));
            if (!rax19) {
                r13_28 = r15_21;
                r15_29 = r14_25;
                r14_30 = r12_26;
            } else {
                r15_29 = r14_25;
                r14_30 = r12_26;
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_26->f8) + reinterpret_cast<unsigned char>(r12_26->f0));
            }
            if (reinterpret_cast<unsigned char>(v27) <= reinterpret_cast<unsigned char>(v23)) {
                addr_7e39_11:
                r12_31 = r14_30;
                r14_32 = r15_29;
                r15_33 = r13_28;
            } else {
                do {
                    rax34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27 - 1) / reinterpret_cast<unsigned char>(v23));
                    rax35 = fun_38c0(r15_29, 1, rax34, v7);
                    v36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_29) + reinterpret_cast<unsigned char>(rax35));
                    if (rax34 == rax35) {
                        addr_7cf0_13:
                        r10_37 = r15_29;
                        r15_38 = r14_30;
                        r14_39 = r13_28;
                        r13_40 = rbx4;
                        ebx41 = reinterpret_cast<signed char>(v13);
                        r12_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(rax35));
                    } else {
                        eax43 = *reinterpret_cast<void***>(v7);
                        if (*reinterpret_cast<unsigned char*>(&eax43) & 32) 
                            goto addr_7edf_15;
                        if (!(*reinterpret_cast<unsigned char*>(&eax43) & 16)) 
                            goto addr_7cf0_13; else 
                            goto addr_7cbd_17;
                    }
                    while (*reinterpret_cast<int32_t*>(&rsi44) = ebx41, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi44) + 4) = 0, rax45 = fun_3a70(r10_37, rsi44), !!rax45) {
                        r10_37 = rax45 + 1;
                        r15_38 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(r15_38) - 32);
                        *reinterpret_cast<void***>(rax45) = reinterpret_cast<void**>(0);
                        r15_38->f0 = r14_39;
                        r11_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_37) - reinterpret_cast<unsigned char>(r14_39));
                        r15_38->f8 = r11_46;
                        if (reinterpret_cast<unsigned char>(rbp20) < reinterpret_cast<unsigned char>(r11_46)) {
                            rbp20 = r11_46;
                        }
                        r12_42 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_42) - reinterpret_cast<unsigned char>(v14));
                        if (r13_40) {
                            if (*reinterpret_cast<void***>(r13_40 + 16) != 0xffffffffffffffff) {
                                rax45 = limfield_isra_0(r14_39, r11_46, r13_40, r14_39, r11_46, r13_40);
                            }
                            zf47 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_40) == 0xffffffffffffffff);
                            r15_38->f18 = rax45;
                            if (!zf47) {
                                rax48 = begfield_isra_0(r14_39, r11_46, r13_40, r14_39, r11_46, r13_40);
                                r15_38->f10 = rax48;
                            } else {
                                if (*reinterpret_cast<void***>(r13_40 + 48)) {
                                    while (*reinterpret_cast<uint32_t*>(&rax49) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_39)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax49) + 4) = 0, !!*reinterpret_cast<signed char*>(0x1d760 + rax49)) {
                                        ++r14_39;
                                    }
                                }
                                r15_38->f10 = r14_39;
                            }
                        }
                        r14_39 = r10_37;
                    }
                    rbx4 = r13_40;
                    v27 = r12_42;
                    r13_28 = r14_39;
                    r14_30 = r15_38;
                    if (*reinterpret_cast<void***>(v6 + 48)) 
                        goto addr_7eb4_33; else 
                        continue;
                    addr_7cbd_17:
                    *reinterpret_cast<void***>(v6 + 48) = reinterpret_cast<void**>(1);
                    if (*reinterpret_cast<void***>(v6) == v36) 
                        goto addr_7d95_2;
                    if (r13_28 == v36) 
                        goto addr_7cf0_13;
                    ecx50 = v13;
                    if (*reinterpret_cast<void***>(v36 + 0xffffffffffffffff) == *reinterpret_cast<void***>(&ecx50)) 
                        goto addr_7cf0_13;
                    *reinterpret_cast<void***>(v36) = *reinterpret_cast<void***>(&ecx50);
                    ++v36;
                    goto addr_7cf0_13;
                    r15_29 = v36;
                } while (reinterpret_cast<unsigned char>(v27) > reinterpret_cast<unsigned char>(v23));
                goto addr_7e39_11;
            }
            addr_7e42_38:
            rdi51 = *reinterpret_cast<void***>(v6);
            rdx52 = *reinterpret_cast<void***>(v6 + 24);
            *reinterpret_cast<void***>(v6 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_32) - reinterpret_cast<unsigned char>(rdi51));
            rax53 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rdi51) + reinterpret_cast<unsigned char>(rdx52) - reinterpret_cast<uint64_t>(r12_31)) >> 5);
            *reinterpret_cast<void***>(v6 + 16) = rax53;
            if (rax53) 
                goto addr_7ec0_39;
            rax54 = x2nrealloc();
            r15_21 = rax54;
            r12_22 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx52) >> 5) << 5);
            *reinterpret_cast<void***>(v6) = r15_21;
            r10_16 = *reinterpret_cast<void***>(v6 + 8);
            *reinterpret_cast<void***>(v6 + 24) = r12_22;
            rax19 = *reinterpret_cast<void***>(v6 + 16);
            continue;
            addr_7eb4_33:
            r12_31 = r15_38;
            r14_32 = v36;
            r15_33 = r13_28;
            goto addr_7e42_38;
        }
    }
    addr_7d97_41:
    rdx55 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (!rdx55) {
        return *reinterpret_cast<signed char*>(&eax15);
    }
    addr_7eff_43:
    fun_3950();
    addr_7ec0_39:
    eax15 = 1;
    merge_buffer_size = rbp20 + 34;
    *reinterpret_cast<void***>(v6 + 32) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_32) - reinterpret_cast<unsigned char>(r15_33));
    goto addr_7d97_41;
    addr_7edf_15:
    rax56 = fun_3920();
    sort_die(rax56, v8, 5, rax56, v8, 5);
    goto addr_7eff_43;
}

/* temp_dir_index.2 */
int64_t temp_dir_index_2 = 0;

void** fun_3940(void** rdi, ...);

void** xmalloc(void** rdi, ...);

void** fun_3b30(void** rdi, void** rsi, void** rdx);

/* slashbase.1 */
int64_t slashbase_1 = 0x58585874726f732f;

int32_t g152f8 = 0x585858;

int32_t fun_3c00();

int32_t mkostemp_safer(void** rdi, void** rsi, void** rdx);

void** temptail = reinterpret_cast<void**>(0x88);

void** compress_program = reinterpret_cast<void**>(0);

void** pipe_fork(void** rdi, void** rsi, void** rdx, ...);

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, ...);

void fun_3750(void** rdi, void** rsi, ...);

void fun_3a40();

void register_proc(void** rdi, void** rsi, void** rdx, ...);

void fun_3e20();

void async_safe_die(void** edi, void** rsi, ...);

void** maybe_create_temp(void** rdi, int32_t esi, void** rdx) {
    int64_t rdx4;
    int32_t v5;
    void** rax6;
    void** rax7;
    void** r14_8;
    void** rax9;
    void** rax10;
    void** r15_11;
    void** r12_12;
    void* rsp13;
    int64_t rax14;
    int32_t eax15;
    int64_t rax16;
    int64_t rax17;
    int1_t zf18;
    void** r13_19;
    void** rdx20;
    int32_t eax21;
    void** rsi22;
    int32_t v23;
    int32_t eax24;
    int32_t ebp25;
    void** rax26;
    void* rsp27;
    void** ebp28;
    void** rax29;
    int1_t zf30;
    void** eax31;
    uint1_t zf32;
    void** rdi33;
    int32_t v34;
    void** edi35;
    int64_t rdi36;
    void** rax37;
    void** rax38;
    void* rax39;

    rdx4 = temp_dir_index_2;
    v5 = esi;
    rax6 = g28;
    rax7 = temp_dirs;
    r14_8 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(rdx4 * 8));
    rax9 = fun_3940(r14_8);
    rax10 = xmalloc(reinterpret_cast<uint64_t>(rax9 + 32) & 0xfffffffffffffff8);
    r15_11 = rax10 + 13;
    r12_12 = rax10;
    fun_3b30(r15_11, r14_8, rax9);
    rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 - 8 + 8 - 8 + 8 - 8 + 8);
    rax14 = slashbase_1;
    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r12_12) + reinterpret_cast<unsigned char>(rax9) + 13) = rax14;
    eax15 = g152f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_11) + reinterpret_cast<unsigned char>(rax9) + 8) = eax15;
    rax16 = temp_dir_index_2;
    *reinterpret_cast<void***>(r12_12) = reinterpret_cast<void**>(0);
    rax17 = rax16 + 1;
    zf18 = rax17 == temp_dir_count;
    temp_dir_index_2 = rax17;
    if (zf18) {
        temp_dir_index_2 = 0;
    }
    r13_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 24);
    rdx20 = r13_19;
    eax21 = fun_3c00();
    *reinterpret_cast<int32_t*>(&rsi22) = 0x80000;
    *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&v23) = reinterpret_cast<uint1_t>(eax21 == 0);
    eax24 = mkostemp_safer(r15_11, 0x80000, rdx20);
    ebp25 = eax24;
    rax26 = fun_37d0();
    rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8 - 8 + 8);
    if (ebp25 < 0) {
        ebp28 = *reinterpret_cast<void***>(rax26);
        if (*reinterpret_cast<unsigned char*>(&v23)) {
            *reinterpret_cast<int32_t*>(&rdx20) = 0;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rsi22 = r13_19;
            fun_3c00();
            *reinterpret_cast<void***>(rax26) = ebp28;
        }
        if (!reinterpret_cast<int1_t>(ebp28 == 24)) 
            goto addr_8b30_7;
        if (*reinterpret_cast<signed char*>(&v5) == 1) 
            goto addr_8a7d_9;
    } else {
        rax29 = temptail;
        temptail = r12_12;
        *reinterpret_cast<void***>(rax29) = r12_12;
        if (*reinterpret_cast<unsigned char*>(&v23)) {
            r14_8 = *reinterpret_cast<void***>(rax26);
            *reinterpret_cast<int32_t*>(&r14_8 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx20) = 0;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            fun_3c00();
            rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8);
            *reinterpret_cast<void***>(rax26) = r14_8;
        }
        zf30 = compress_program == 0;
        *reinterpret_cast<unsigned char*>(r12_12 + 12) = 0;
        if (zf30) 
            goto addr_89f6_13;
        eax31 = pipe_fork(reinterpret_cast<int64_t>(rsp27) + 16, 4, rdx20);
        *reinterpret_cast<void***>(r12_12 + 8) = eax31;
        zf32 = reinterpret_cast<uint1_t>(eax31 == 0);
        if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(eax31) < reinterpret_cast<signed char>(0)) | zf32)) 
            goto addr_8a40_15; else 
            goto addr_89f0_16;
    }
    addr_8b30_7:
    quotearg_style(4, r14_8, rdx20);
    fun_3920();
    fun_3c90();
    addr_8a7d_9:
    rdi33 = r12_12;
    *reinterpret_cast<int32_t*>(&r12_12) = 0;
    *reinterpret_cast<int32_t*>(&r12_12 + 4) = 0;
    fun_3750(rdi33, rsi22, rdi33, rsi22);
    goto addr_8a14_17;
    addr_8a40_15:
    fun_3a40();
    fun_3a40();
    ebp25 = v34;
    register_proc(r12_12, 4, rdx20);
    goto addr_89f6_13;
    addr_89f0_16:
    if (zf32) {
        fun_3a40();
        if (ebp25 != 1) {
            move_fd_part_0(ebp25, ebp25);
        }
        if (!0) {
            move_fd_part_0(v23);
        }
        fun_3e20();
        edi35 = *reinterpret_cast<void***>(rax26);
        async_safe_die(edi35, "couldn't execute compress program");
    } else {
        addr_89f6_13:
        *reinterpret_cast<int32_t*>(&rdi36) = ebp25;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi36) + 4) = 0;
        rax37 = fun_3c20(rdi36, "w", rdx20);
        *reinterpret_cast<void***>(rdi) = rax37;
        if (!rax37) {
            addr_8b12_23:
            *reinterpret_cast<int32_t*>(&rdx20) = 5;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rax38 = fun_3920();
            sort_die(rax38, r15_11, 5);
            goto addr_8b30_7;
        } else {
            addr_8a14_17:
            rax39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
            if (!rax39) {
                return r12_12;
            }
        }
    }
    fun_3950();
    goto addr_8b12_23;
}

void fun_3e40(void** rdi);

void** init_node(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...) {
    void** r12_6;
    void** rcx7;
    void** rdx8;
    void** r14_9;
    void** rsi10;
    uint32_t eax11;
    void** r15_12;
    void** rcx13;
    void** rax14;

    while (1) {
        r12_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8) << 5));
        if (1) {
            rcx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48)) >> 1);
            rdx8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48)) - reinterpret_cast<unsigned char>(rcx7));
            r14_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_6) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx7) << 5));
            rsi10 = rdi + 24;
        } else {
            rcx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) >> 1);
            rdx8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) - reinterpret_cast<unsigned char>(rcx7));
            r14_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_6) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx7) << 5));
            rsi10 = rdi + 16;
        }
        *reinterpret_cast<void***>(rsi + 56) = rdi;
        eax11 = *reinterpret_cast<uint32_t*>(rdi + 80);
        r15_12 = rsi + 0x80;
        *reinterpret_cast<void***>(rsi + 32) = rsi10;
        *reinterpret_cast<void***>(rsi + 16) = r12_6;
        *reinterpret_cast<void***>(rsi) = r12_6;
        *reinterpret_cast<void***>(rsi + 24) = r14_9;
        *reinterpret_cast<void***>(rsi + 8) = r14_9;
        *reinterpret_cast<void***>(rsi + 40) = rcx7;
        *reinterpret_cast<void***>(rsi + 48) = rdx8;
        *reinterpret_cast<uint32_t*>(rsi + 80) = eax11 + 1;
        *reinterpret_cast<unsigned char*>(rsi + 84) = 0;
        fun_3e40(rsi + 88);
        if (reinterpret_cast<unsigned char>(rcx) <= reinterpret_cast<unsigned char>(1)) 
            break;
        *reinterpret_cast<void***>(rsi + 64) = r15_12;
        rcx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx) >> 1);
        rax14 = init_node(rsi, r15_12, r12_6, rcx13, r8, rsi, r15_12, r12_6, rcx13, r8);
        *reinterpret_cast<void***>(rsi + 72) = rax14;
    }
    *reinterpret_cast<void***>(rsi + 64) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rsi + 72) = reinterpret_cast<void**>(0);
    return r15_12;
}

void** sequential_sort(void** rdi, void** rsi, void** rdx, int32_t ecx);

int32_t fun_3c60(void* rdi);

void fun_3e00(int64_t rdi);

void fun_3e80(void** rdi, void** rsi, void** rdx, void** rcx);

void heap_insert(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_3a90(void*** rdi, void** rsi, void** rdx, void** rcx);

void fun_3b80(void** rdi, void** rsi, void** rdx, void** rcx);

void** heap_remove_top(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_38e0(void*** rdi, void** rsi, void** rdx, void** rcx);

void** compare(void** rdi, void** rsi, void** rdx, ...);

void write_unique(void** rdi, void** rsi, void** rdx);

/* queue_check_insert.part.0 */
void queue_check_insert_part_0(void** rdi, void** rsi, void** rdx);

void sortlines(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7) {
    uint64_t r15_8;
    void** r13_9;
    void* rsp10;
    void** r14_11;
    void** r12_12;
    void** rcx13;
    void** v14;
    void** v15;
    void** v16;
    void** rax17;
    void** v18;
    void** v19;
    void** r8_20;
    uint64_t rax21;
    void** r15_22;
    void* r9_23;
    int32_t eax24;
    void** rax25;
    void** rcx26;
    int64_t v27;
    void*** r14_28;
    void** rbp29;
    void** rdi30;
    void** rsi31;
    void** rdi32;
    void** rax33;
    void** rbx34;
    void** rax35;
    void** v36;
    int64_t rax37;
    int32_t ecx38;
    void** rdx39;
    void** rdi40;
    void** v41;
    void** r9_42;
    void** v43;
    void** rax44;
    void** r10_45;
    void** v46;
    void** r15_47;
    void** rbp48;
    void** rbx49;
    void** r12_50;
    void** rax51;
    void** rdi52;
    void** rdi53;
    void** rdi54;
    void** rax55;
    void** r15_56;
    void** r11_57;
    uint32_t r8d58;
    void** r9_59;
    void** rdx60;
    void** v61;
    void** r15_62;
    void** rbx63;
    void** r12_64;
    void** rax65;
    void** r11_66;
    void** r12_67;
    void** v68;
    void** r15_69;
    void** rbp70;
    void** r12_71;
    void** rdi72;
    int1_t cf73;
    void** rdi74;
    void** r12_75;
    void** rdi76;
    void** v77;
    void** r15_78;
    void** rbp79;
    void** r12_80;
    void** rdi81;
    int1_t cf82;
    void** r9_83;
    void** rax84;
    void** rax85;
    void** rdi86;
    void* rax87;

    r15_8 = reinterpret_cast<unsigned char>(rsi) >> 1;
    r13_9 = r8;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    r14_11 = *reinterpret_cast<void***>(rcx + 40);
    r12_12 = *reinterpret_cast<void***>(rcx + 48);
    rcx13 = a7;
    v14 = rdx;
    v15 = r9;
    v16 = rcx13;
    rax17 = g28;
    v18 = rax17;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_11) + reinterpret_cast<unsigned char>(r12_12)) <= 0x1ffff || (v19 = rsi, reinterpret_cast<unsigned char>(rsi) <= reinterpret_cast<unsigned char>(1))) {
        addr_b0b7_2:
        r8_20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v14) << 5));
        rax21 = reinterpret_cast<unsigned char>(r14_11) << 5;
        r15_22 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) - rax21);
        r9_23 = reinterpret_cast<void*>(-rax21);
        if (reinterpret_cast<unsigned char>(r12_12) > reinterpret_cast<unsigned char>(1)) {
            *reinterpret_cast<int32_t*>(&rcx13) = 0;
            *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
            rsi = r12_12;
            rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_20) - (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_11) >> 1) << 5));
            sequential_sort(r15_22, rsi, rdx, 0);
            r9_23 = r9_23;
            r8_20 = r8_20;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rsi) = 0;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rcx13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 80);
        rdx = reinterpret_cast<void**>(0xb880);
        eax24 = fun_3c60(reinterpret_cast<int64_t>(rsp10) + 72);
        if (!eax24) {
            rax25 = *reinterpret_cast<void***>(rcx + 40);
            rcx26 = *reinterpret_cast<void***>(rcx + 72);
            sortlines(reinterpret_cast<unsigned char>(rdi) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax25) << 5), reinterpret_cast<unsigned char>(v19) - r15_8, v14, rcx26, r13_9, v15, v15);
            fun_3e00(v27);
            goto addr_b722_6;
        } else {
            r14_11 = *reinterpret_cast<void***>(rcx + 40);
            r12_12 = *reinterpret_cast<void***>(rcx + 48);
            goto addr_b0b7_2;
        }
    }
    if (reinterpret_cast<unsigned char>(r14_11) > reinterpret_cast<unsigned char>(1)) {
        *reinterpret_cast<int32_t*>(&rcx13) = 0;
        *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
        rdx = r8_20;
        rsi = r14_11;
        sequential_sort(rdi, rsi, rdx, 0);
        r9_23 = r9_23;
    }
    *reinterpret_cast<void***>(rcx) = rdi;
    r14_28 = reinterpret_cast<void***>(r13_9 + 48);
    *reinterpret_cast<void***>(rcx + 8) = r15_22;
    *reinterpret_cast<void***>(rcx + 16) = r15_22;
    *reinterpret_cast<void***>(rcx + 24) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + (reinterpret_cast<int64_t>(r9_23) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_12) << 5)));
    rbp29 = r13_9 + 8;
    fun_3e80(rbp29, rsi, rdx, rcx13);
    rdi30 = *reinterpret_cast<void***>(r13_9);
    rsi31 = rcx;
    heap_insert(rdi30, rsi31, rdx, rcx13);
    *reinterpret_cast<unsigned char*>(rcx + 84) = 1;
    fun_3a90(r14_28, rsi31, rdx, rcx13);
    fun_3b80(rbp29, rsi31, rdx, rcx13);
    while (1) {
        fun_3e80(rbp29, rsi31, rdx, rcx13);
        while (rdi32 = *reinterpret_cast<void***>(r13_9), rax33 = heap_remove_top(rdi32, rsi31, rdx, rcx13), rax33 == 0) {
            rsi31 = rbp29;
            fun_38e0(r14_28, rsi31, rdx, rcx13);
        }
        rbx34 = rax33;
        fun_3b80(rbp29, rsi31, rdx, rcx13);
        rax35 = rbx34 + 88;
        v36 = rax35;
        fun_3e80(rax35, rsi31, rdx, rcx13);
        *reinterpret_cast<uint32_t*>(&rax37) = *reinterpret_cast<uint32_t*>(rbx34 + 80);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax37) + 4) = 0;
        *reinterpret_cast<unsigned char*>(rbx34 + 84) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax37)) 
            break;
        ecx38 = static_cast<int32_t>(rax37 + rax37 + 2);
        rdx39 = *reinterpret_cast<void***>(rbx34);
        rdi40 = *reinterpret_cast<void***>(rbx34 + 8);
        v41 = rdx39;
        r9_42 = rdx39;
        v43 = rdi40;
        rsi31 = *reinterpret_cast<void***>(rbx34 + 16);
        rcx13 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(v14) >> *reinterpret_cast<signed char*>(&ecx38)) + 1);
        if (*reinterpret_cast<uint32_t*>(&rax37) == 1) {
            rax44 = rdi40;
            if (rdx39 == rsi31) {
                *reinterpret_cast<int32_t*>(&r10_45) = 0;
                *reinterpret_cast<int32_t*>(&r10_45 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            } else {
                v46 = rbp29;
                r15_47 = v15;
                rbp48 = rbx34;
                rbx49 = v16;
                while (*reinterpret_cast<void***>(rbp48 + 24) != rax44) {
                    r12_50 = rcx13 + 0xffffffffffffffff;
                    if (!rcx13) 
                        goto addr_b6d8_21;
                    rax51 = compare(r9_42 + 0xffffffffffffffe0, rax44 + 0xffffffffffffffe0, rdx39);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax51) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax51) == 0))) {
                        rdx39 = rbx49;
                        rsi31 = r15_47;
                        rdi52 = *reinterpret_cast<void***>(rbp48 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp48 + 8) = rdi52;
                        write_unique(rdi52, rsi31, rdx39);
                        r9_42 = *reinterpret_cast<void***>(rbp48);
                        rax44 = *reinterpret_cast<void***>(rbp48 + 8);
                        if (r9_42 == *reinterpret_cast<void***>(rbp48 + 16)) 
                            goto addr_b3f5_24;
                    } else {
                        rdx39 = rbx49;
                        rsi31 = r15_47;
                        rdi53 = *reinterpret_cast<void***>(rbp48) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp48) = rdi53;
                        write_unique(rdi53, rsi31, rdx39);
                        r9_42 = *reinterpret_cast<void***>(rbp48);
                        rax44 = *reinterpret_cast<void***>(rbp48 + 8);
                        if (r9_42 == *reinterpret_cast<void***>(rbp48 + 16)) 
                            goto addr_b3f5_24;
                    }
                    rcx13 = r12_50;
                }
                goto addr_b600_27;
            }
        } else {
            rdx = *reinterpret_cast<void***>(rbx34 + 32);
            rdi54 = v41;
            rax55 = v43;
            r15_56 = *reinterpret_cast<void***>(rdx);
            r11_57 = rdi54;
            if (rdi54 == rsi31) {
                r8d58 = 0;
                *reinterpret_cast<int32_t*>(&r10_45) = 0;
                *reinterpret_cast<int32_t*>(&r10_45 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r9_59) = 0;
                *reinterpret_cast<int32_t*>(&r9_59 + 4) = 0;
                goto addr_b2e8_30;
            } else {
                rdx60 = r15_56;
                v61 = r9_42;
                r15_62 = rbx34;
                rbx63 = rdx60;
                while (*reinterpret_cast<void***>(r15_62 + 24) != rax55) {
                    r12_64 = rcx13 + 0xffffffffffffffff;
                    if (!rcx13) 
                        goto addr_b5c0_34;
                    rbx63 = rbx63 - 32;
                    rax65 = compare(r11_57 + 0xffffffffffffffe0, rax55 + 0xffffffffffffffe0, rdx60);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax65) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax65) == 0))) {
                        r11_57 = *reinterpret_cast<void***>(r15_62);
                        rsi31 = *reinterpret_cast<void***>(r15_62 + 16);
                        __asm__("movdqu xmm0, [rcx-0x20]");
                        rax55 = *reinterpret_cast<void***>(r15_62 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_62 + 8) = rax55;
                        __asm__("movups [rbx], xmm0");
                        __asm__("movdqu xmm1, [rcx-0x10]");
                        __asm__("movups [rbx+0x10], xmm1");
                        if (rsi31 == r11_57) 
                            goto addr_b2b8_37;
                    } else {
                        rsi31 = *reinterpret_cast<void***>(r15_62 + 16);
                        __asm__("movdqu xmm2, [rax-0x20]");
                        r11_57 = *reinterpret_cast<void***>(r15_62) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_62) = r11_57;
                        __asm__("movups [rbx], xmm2");
                        __asm__("movdqu xmm3, [rax-0x10]");
                        rax55 = *reinterpret_cast<void***>(r15_62 + 8);
                        __asm__("movups [rbx+0x10], xmm3");
                        if (rsi31 == r11_57) 
                            goto addr_b2b8_37;
                    }
                    rcx13 = r12_64;
                }
                goto addr_b440_40;
            }
        }
        addr_b418_41:
        r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
        rsi31 = r11_66;
        if (*reinterpret_cast<void***>(rbx34 + 48) != r10_45) {
            addr_b6a8_42:
            r11_66 = rsi31;
            if (rsi31 != rdx || (rax44 == *reinterpret_cast<void***>(rbx34 + 24) || !rcx13)) {
                rdi54 = r9_42;
                r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
                r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
                r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx34 + 8))) >> 5);
            } else {
                v68 = rbp29;
                r15_69 = v15;
                rbp70 = rcx13 + 0xffffffffffffffff;
                r12_71 = v16;
                do {
                    rdi72 = rax44 + 0xffffffffffffffe0;
                    rdx = r12_71;
                    rsi31 = r15_69;
                    *reinterpret_cast<void***>(rbx34 + 8) = rdi72;
                    write_unique(rdi72, rsi31, rdx);
                    rax44 = *reinterpret_cast<void***>(rbx34 + 8);
                    if (rax44 == *reinterpret_cast<void***>(rbx34 + 24)) 
                        break;
                    cf73 = reinterpret_cast<unsigned char>(rbp70) < reinterpret_cast<unsigned char>(1);
                    --rbp70;
                } while (!cf73);
                rbp29 = v68;
                r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
                rdi54 = *reinterpret_cast<void***>(rbx34);
                r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
                r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
                r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax44)) >> 5);
            }
        } else {
            r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
            rdi54 = r9_42;
            r12_67 = r10_45;
        }
        addr_b301_49:
        *reinterpret_cast<void***>(rbx34 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_66) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(rdi54)) >> 5));
        *reinterpret_cast<void***>(rbx34 + 48) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_67) - reinterpret_cast<unsigned char>(r10_45));
        if (!*reinterpret_cast<signed char*>(&r8d58)) {
            rsi31 = rbx34;
            queue_check_insert_part_0(r13_9, rsi31, rdx);
        }
        if (*reinterpret_cast<uint32_t*>(rbx34 + 80) > 1) {
            rdi74 = *reinterpret_cast<void***>(rbx34 + 56) + 88;
            fun_3e80(rdi74, rsi31, rdx, rcx13);
            rsi31 = *reinterpret_cast<void***>(rbx34 + 56);
            if (!*reinterpret_cast<unsigned char*>(rsi31 + 84)) {
                queue_check_insert_part_0(r13_9, rsi31, rdx);
                rsi31 = *reinterpret_cast<void***>(rbx34 + 56);
            }
            fun_3b80(rsi31 + 88, rsi31, rdx, rcx13);
        } else {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx34 + 48)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx34 + 40)))) {
                r12_75 = *reinterpret_cast<void***>(rbx34 + 56);
                fun_3e80(rbp29, rsi31, rdx, rcx13);
                rdi76 = *reinterpret_cast<void***>(r13_9);
                rsi31 = r12_75;
                heap_insert(rdi76, rsi31, rdx, rcx13);
                *reinterpret_cast<unsigned char*>(r12_75 + 84) = 1;
                fun_3a90(r14_28, rsi31, rdx, rcx13);
                fun_3b80(rbp29, rsi31, rdx, rcx13);
            }
        }
        fun_3b80(v36, rsi31, rdx, rcx13);
        continue;
        addr_b600_27:
        rbx34 = rbp48;
        rbp29 = v46;
        addr_b608_58:
        r10_45 = *reinterpret_cast<void***>(rbx34 + 48);
        rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax44)) >> 5);
        if (rdx != r10_45) {
            rsi31 = *reinterpret_cast<void***>(rbx34 + 40);
            rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(r9_42)) >> 5);
            goto addr_b6a8_42;
        } else {
            rdi54 = r9_42;
            if (*reinterpret_cast<void***>(rbx34 + 16) == r9_42 || !rcx13) {
                r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
                r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
                r12_67 = r10_45;
                goto addr_b301_49;
            } else {
                v77 = rbp29;
                r15_78 = v15;
                rbp79 = rcx13 + 0xffffffffffffffff;
                r12_80 = v16;
                do {
                    rdi81 = rdi54 - 32;
                    rdx = r12_80;
                    rsi31 = r15_78;
                    *reinterpret_cast<void***>(rbx34) = rdi81;
                    write_unique(rdi81, rsi31, rdx);
                    rdi54 = *reinterpret_cast<void***>(rbx34);
                    if (rdi54 == *reinterpret_cast<void***>(rbx34 + 16)) 
                        break;
                    cf82 = reinterpret_cast<unsigned char>(rbp79) < reinterpret_cast<unsigned char>(1);
                    --rbp79;
                } while (!cf82);
                rbp29 = v77;
                r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
                r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx34 + 8))) >> 5);
                r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
                r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
                goto addr_b301_49;
            }
        }
        addr_b6d8_21:
        rbx34 = rbp48;
        rcx13 = reinterpret_cast<void**>(0xffffffffffffffff);
        rbp29 = v46;
        goto addr_b608_58;
        addr_b3f5_24:
        rbx34 = rbp48;
        rcx13 = r12_50;
        rbp29 = v46;
        rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(r9_42)) >> 5);
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax44)) >> 5);
        goto addr_b418_41;
        addr_b2e8_30:
        r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
        r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
        if (r10_45 == r12_67) 
            goto addr_b2fe_66;
        if (r9_59 != r11_66 || ((r9_83 = *reinterpret_cast<void***>(rbx34 + 24), r9_83 == rax55) || (rsi31 = rcx13 + 0xffffffffffffffff, rcx13 == 0))) {
            addr_b2fe_66:
            *reinterpret_cast<void***>(rdx) = r15_56;
            goto addr_b301_49;
        } else {
            rax84 = rax55 - 32;
            do {
                __asm__("movdqu xmm6, [rax]");
                r15_56 = r15_56 - 32;
                *reinterpret_cast<void***>(rbx34 + 8) = rax84;
                rcx13 = rax84;
                __asm__("movups [r15], xmm6");
                __asm__("movdqu xmm7, [rax+0x10]");
                __asm__("movups [r15+0x10], xmm7");
                if (rax84 == r9_83) 
                    break;
                --rsi31;
                rax84 = rax84 - 32;
            } while (rsi31 != 0xffffffffffffffff);
            goto addr_b7f7_71;
        }
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax84)) >> 5);
        goto addr_b2fe_66;
        addr_b7f7_71:
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rcx13)) >> 5);
        goto addr_b2fe_66;
        addr_b440_40:
        rbx34 = r15_62;
        rdi54 = *reinterpret_cast<void***>(rbx34);
        r15_56 = rbx63;
        r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
        rdx = *reinterpret_cast<void***>(rbx34 + 32);
        r9_59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v61) - reinterpret_cast<unsigned char>(r11_57)) >> 5);
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax55)) >> 5);
        addr_b46d_73:
        r12_67 = *reinterpret_cast<void***>(rbx34 + 48);
        r11_66 = *reinterpret_cast<void***>(rbx34 + 40);
        if (r12_67 == r10_45) {
            if (rsi31 == rdi54) 
                goto addr_b2fe_66;
            rax85 = rcx13 + 0xffffffffffffffff;
            if (!rcx13) 
                goto addr_b2fe_66;
            rcx13 = rdi54 + 0xffffffffffffffe0;
            do {
                __asm__("movdqu xmm4, [rcx]");
                r15_56 = r15_56 - 32;
                *reinterpret_cast<void***>(rbx34) = rcx13;
                rdi54 = rcx13;
                __asm__("movups [r15], xmm4");
                __asm__("movdqu xmm5, [rcx+0x10]");
                __asm__("movups [r15+0x10], xmm5");
                if (rcx13 == rsi31) 
                    break;
                --rax85;
                rcx13 = rcx13 - 32;
            } while (rax85 != 0xffffffffffffffff);
            goto addr_b2fe_66;
            goto addr_b2fe_66;
        }
        addr_b5c0_34:
        rbx34 = r15_62;
        rdi54 = *reinterpret_cast<void***>(rbx34);
        r15_56 = rbx63;
        r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
        rcx13 = reinterpret_cast<void**>(0xffffffffffffffff);
        rdx = *reinterpret_cast<void***>(rbx34 + 32);
        r9_59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v61) - reinterpret_cast<unsigned char>(r11_57)) >> 5);
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax55)) >> 5);
        goto addr_b46d_73;
        addr_b2b8_37:
        rbx34 = r15_62;
        r8d58 = *reinterpret_cast<unsigned char*>(rbx34 + 84);
        r15_56 = rbx63;
        rdx = *reinterpret_cast<void***>(rbx34 + 32);
        rdi54 = r11_57;
        rcx13 = r12_64;
        r9_59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v61) - reinterpret_cast<unsigned char>(r11_57)) >> 5);
        r10_45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rax55)) >> 5);
        goto addr_b2e8_30;
    }
    fun_3b80(v36, rsi31, rdx, rcx13);
    fun_3e80(rbp29, rsi31, rdx, rcx13);
    rdi86 = *reinterpret_cast<void***>(r13_9);
    heap_insert(rdi86, rbx34, rdx, rcx13);
    *reinterpret_cast<unsigned char*>(rbx34 + 84) = 1;
    fun_3a90(r14_28, rbx34, rdx, rcx13);
    fun_3b80(rbp29, rbx34, rdx, rcx13);
    addr_b722_6:
    rax87 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
    if (rax87) {
        fun_3950();
    } else {
        return;
    }
}

void xfclose(void** rdi, void** rsi, void** rdx, ...) {
    void** r12_4;
    uint32_t eax5;
    int32_t eax6;
    void** rax7;
    int64_t rax8;

    r12_4 = rsi;
    eax5 = fun_3b40(rdi, rsi, rdx);
    if (eax5) {
        if (eax5 == 1) {
            eax6 = fun_3df0(rdi);
            if (eax6) {
                while (1) {
                    rax7 = fun_3920();
                    sort_die(rax7, r12_4, 5);
                    addr_736a_8:
                }
            }
        } else {
            rax8 = rpl_fclose(rdi, rsi, rdx);
            if (*reinterpret_cast<int32_t*>(&rax8)) 
                goto addr_736a_8;
        }
        return;
    }
}

void** set_ordering(void** rdi, void** rsi, uint32_t edx) {
    uint32_t eax4;
    uint32_t eax5;
    int64_t rax6;

    eax4 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (!*reinterpret_cast<signed char*>(&eax4)) {
        addr_6efd_2:
        return rdi;
    } else {
        eax5 = eax4 - 77;
        if (*reinterpret_cast<unsigned char*>(&eax5) > 37) 
            goto addr_6efd_2;
    }
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax5);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x15020 + rax6 * 4) + 0x15020;
}

uint32_t nmerge = 16;

void** fun_3ad0(void** rdi, void** rsi, void** rdx, ...);

int64_t g1d248 = 0;

int32_t fun_3b10(void** rdi, void** rsi, void** rdx);

/* outstat.3 */
int64_t outstat_3 = 0;

void** mergefiles(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** open_input_files(void** rdi, void** rsi, void** rdx);

struct s1 {
    signed char[1] pad1;
    void** f1;
};

void mergefps(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void merge(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** rbp6;
    void* rsp7;
    void** rsi8;
    void** v9;
    void** r12_10;
    void** v11;
    void** rax12;
    void** v13;
    void** v14;
    void** rbx15;
    void** r9_16;
    void** v17;
    void** r13_18;
    void** rbp19;
    void** r15_20;
    void** rsi21;
    uint32_t eax22;
    uint32_t r14d23;
    void** eax24;
    void** eax25;
    int32_t eax26;
    void** rax27;
    int32_t eax28;
    int64_t rax29;
    int64_t v30;
    void** rdi31;
    int32_t eax32;
    int64_t rax33;
    int64_t v34;
    int64_t rax35;
    int64_t v36;
    void** r14_37;
    void** rax38;
    void** v39;
    void** r8_40;
    void** v41;
    void** r13_42;
    void** v43;
    void** rdx44;
    void** rax45;
    void* rsp46;
    void** rbx47;
    void** rax48;
    void* rsp49;
    void** rax50;
    void** rbx51;
    void** rbp52;
    void** rsi53;
    void** rdi54;
    void*** v55;
    int32_t esi56;
    void** rax57;
    void** v58;
    void** v59;
    void** r8_60;
    void** rbx61;
    struct s1* r13_62;
    void** rdx63;
    void** rax64;
    void** rdi65;
    void** rdx66;
    void** rax67;
    void** rsi68;
    void** v69;
    void** r8_70;
    void** rax71;
    void** rdx72;
    void** r10_73;
    void** rax74;
    void** rax75;
    void** rsi76;
    void** rdx77;
    void** r8_78;
    void** v79;
    void** rax80;
    void** v81;
    void** rdx82;
    void** rdi83;
    void** rsi84;
    void* rsp85;
    void* rax86;
    uint64_t r15_87;
    void** r13_88;
    void* rsp89;
    void** r14_90;
    void** r12_91;
    void** rcx92;
    void** v93;
    void** v94;
    void** v95;
    void** rax96;
    void** v97;
    void** v98;
    void** r8_99;
    uint64_t rax100;
    void** r15_101;
    void* r9_102;
    int32_t eax103;
    void** rax104;
    void** rcx105;
    int64_t v106;
    void*** r14_107;
    void** rbp108;
    void** rdi109;
    void** rsi110;
    void** rdi111;
    void** rax112;
    void** rbx113;
    void** rax114;
    void** v115;
    int64_t rax116;
    int32_t ecx117;
    void** rdx118;
    void** rdi119;
    void** v120;
    void** r9_121;
    void** v122;
    void** rax123;
    void** r10_124;
    void** v125;
    void** r15_126;
    void** rbp127;
    void** rbx128;
    void** r12_129;
    void** rax130;
    void** rdi131;
    void** rdi132;
    void** rdi133;
    void** rax134;
    void** r15_135;
    void** r11_136;
    uint32_t r8d137;
    void** r9_138;
    void** rdx139;
    void** v140;
    void** r15_141;
    void** rbx142;
    void** r12_143;
    void** rax144;
    void** r11_145;
    void** r12_146;
    void** v147;
    void** r15_148;
    void** rbp149;
    void** r12_150;
    void** rdi151;
    int1_t cf152;
    void** rdi153;
    void** r12_154;
    void** rdi155;
    void** v156;
    void** r15_157;
    void** rbp158;
    void** r12_159;
    void** rdi160;
    int1_t cf161;
    void** r9_162;
    void** rax163;
    void** rax164;
    void** rdi165;
    void* rax166;
    int64_t v167;
    void** rbp168;
    void** rax169;

    r14_5 = rdi;
    rbp6 = rsi;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xe8);
    *reinterpret_cast<uint32_t*>(&rsi8) = nmerge;
    *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
    v9 = rdx;
    r12_10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 56);
    v11 = rcx;
    rax12 = g28;
    v13 = rax12;
    if (reinterpret_cast<unsigned char>(rsi8) < reinterpret_cast<unsigned char>(rdx)) 
        goto addr_ab58_2;
    while (1) {
        addr_ac45_3:
        if (reinterpret_cast<unsigned char>(rbp6) < reinterpret_cast<unsigned char>(v9)) {
            v14 = rbp6;
            rbx15 = rbp6;
            r9_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) << 4);
            v17 = r14_5;
            r13_18 = v11;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            rbp19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<unsigned char>(r9_16));
            r15_20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 64);
            do {
                addr_aca9_5:
                rsi21 = *reinterpret_cast<void***>(rbp19);
                eax22 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi21) - 45);
                r14d23 = eax22;
                if (!eax22) {
                    r14d23 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi21 + 1));
                }
                if (!r13_18) 
                    goto addr_acd3_8;
                eax24 = fun_3ad0(r13_18, rsi21, rdx);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                if (eax24) 
                    goto addr_acd3_8;
                if (r14d23) 
                    goto addr_ad16_11;
                addr_acd3_8:
                eax25 = outstat_errno_4;
                if (!eax25) {
                    eax26 = fun_3e50();
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                    if (eax26) {
                        rax27 = fun_37d0();
                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                        eax25 = *reinterpret_cast<void***>(rax27);
                        outstat_errno_4 = eax25;
                        goto addr_ace1_14;
                    } else {
                        outstat_errno_4 = reinterpret_cast<void**>(0xffffffff);
                    }
                } else {
                    addr_ace1_14:
                    if (reinterpret_cast<signed char>(eax25) >= reinterpret_cast<signed char>(0)) 
                        break;
                }
                if (!r14d23) {
                    eax28 = fun_3e50();
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                    if (eax28) 
                        goto addr_ac96_18;
                    rax29 = g1d248;
                    if (v30 != rax29) 
                        goto addr_ac96_18;
                } else {
                    rdi31 = *reinterpret_cast<void***>(rbp19);
                    eax32 = fun_3b10(rdi31, r15_20, rdx);
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                    if (eax32) 
                        goto addr_ac96_18;
                    rax33 = g1d248;
                    if (v34 != rax33) 
                        goto addr_ac96_18;
                }
                rax35 = outstat_3;
                if (v36 != rax35) {
                    addr_ac96_18:
                    ++rbx15;
                    rbp19 = rbp19 + 16;
                    if (rbx15 == v9) 
                        break; else 
                        goto addr_aca9_5;
                } else {
                    addr_ad16_11:
                    r14_37 = r12_10 + 13;
                    if (!r12_10) {
                        rax38 = maybe_create_temp(reinterpret_cast<int64_t>(rsp7) + 56, 0, rdx);
                        rcx = v39;
                        r14_37 = rax38 + 13;
                        *reinterpret_cast<int32_t*>(&rdx) = 1;
                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                        r12_10 = rax38;
                        r8_40 = r14_37;
                        mergefiles(rbp19, 0, 1, rcx, r8_40);
                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
                    }
                }
                *reinterpret_cast<void***>(rbp19) = r14_37;
                ++rbx15;
                rbp19 = rbp19 + 16;
                *reinterpret_cast<void***>(rbp19 + 0xfffffffffffffff8) = r12_10;
            } while (rbx15 != v9);
            r14_5 = v17;
            rbp6 = v14;
        }
        v41 = rbp6;
        r13_42 = v9;
        v43 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 48);
        while (1) {
            rdx44 = v43;
            rax45 = open_input_files(r14_5, r13_42, rdx44);
            rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
            rbx47 = rax45;
            if (r13_42 == rax45) {
                rax48 = stream_open(v11, "w", rdx44);
                rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8);
                if (rax48) 
                    goto addr_afa3_29;
                rax50 = fun_37d0();
                rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax50) == 24)) 
                    break;
                if (reinterpret_cast<unsigned char>(r13_42) <= reinterpret_cast<unsigned char>(2)) 
                    break;
            } else {
                if (reinterpret_cast<unsigned char>(rax45) <= reinterpret_cast<unsigned char>(2)) 
                    goto addr_afe8_33;
            }
            r15_20 = rbx47 + 0xffffffffffffffff;
            rbx51 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp46) + 56);
            rbp52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_20) << 4));
            while (rsi53 = *reinterpret_cast<void***>(rbp52), r12_10 = rbp52, rdi54 = v55[reinterpret_cast<unsigned char>(r15_20) * 8], xfclose(rdi54, rsi53, rdx44), esi56 = 0, *reinterpret_cast<unsigned char*>(&esi56) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_20) > reinterpret_cast<unsigned char>(2)), rbp52 = rbp52 - 16, rax57 = maybe_create_temp(rbx51, esi56, rdx44), rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8), rax57 == 0) {
                --r15_20;
            }
            r9_16 = v58;
            rcx = v59;
            r8_60 = rax57 + 13;
            v9 = rax57;
            rbx61 = v41;
            if (reinterpret_cast<unsigned char>(v41) > reinterpret_cast<unsigned char>(r15_20)) {
                rbx61 = r15_20;
            }
            r13_62 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(r13_42) - reinterpret_cast<unsigned char>(r15_20));
            rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v41) - reinterpret_cast<unsigned char>(rbx61));
            mergefps(r14_5, rbx61, r15_20, rcx, r8_60, r9_16);
            r8_40 = r8_60;
            r13_42 = reinterpret_cast<void**>(&r13_62->f1);
            *reinterpret_cast<void***>(r14_5) = r8_40;
            *reinterpret_cast<void***>(r14_5 + 8) = v9;
            fun_3c70(r14_5 + 16, r12_10, reinterpret_cast<uint64_t>(r13_62) << 4);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8);
            v41 = rbp6 + 1;
        }
        *reinterpret_cast<int32_t*>(&rdx63) = 5;
        *reinterpret_cast<int32_t*>(&rdx63 + 4) = 0;
        rax64 = fun_3920();
        rsi8 = v11;
        rdi65 = rax64;
        sort_die(rdi65, rsi8, 5);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8);
        while (1) {
            rdx66 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi65) + reinterpret_cast<unsigned char>(rdx63) + 1 - reinterpret_cast<unsigned char>(rsi8));
            rax67 = maybe_create_temp(r12_10, 0, rdx66);
            rsi68 = rbp6;
            rcx = v69;
            r8_70 = rax67 + 13;
            if (reinterpret_cast<unsigned char>(rdx66) <= reinterpret_cast<unsigned char>(rbp6)) {
                rsi68 = rdx66;
            }
            rax71 = mergefiles(r15_20, rsi68, rdx66, rcx, r8_70);
            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
            rdx72 = rbp6;
            r8_40 = r8_70;
            if (reinterpret_cast<unsigned char>(rax71) <= reinterpret_cast<unsigned char>(rbp6)) {
                rdx72 = rax71;
            }
            ++r13_42;
            rbx47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx47) + reinterpret_cast<unsigned char>(rax71));
            *reinterpret_cast<void***>(r10_73) = r8_40;
            *reinterpret_cast<void***>(r10_73 + 8) = rax67;
            rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) - reinterpret_cast<unsigned char>(rdx72));
            r15_20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx47) << 4) + reinterpret_cast<unsigned char>(r14_5));
            r10_73 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_42) << 4) + reinterpret_cast<unsigned char>(r14_5));
            do {
                rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) + reinterpret_cast<unsigned char>(r13_42));
                fun_3c70(r10_73, r15_20, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(rbx47)) << 4);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rsi8) = nmerge;
                *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                r9_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_42) - reinterpret_cast<unsigned char>(rbx47));
                v9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v9) + reinterpret_cast<unsigned char>(r9_16));
                rdx = v9;
                if (reinterpret_cast<unsigned char>(rsi8) >= reinterpret_cast<unsigned char>(rdx)) 
                    goto addr_ac45_3;
                addr_ab58_2:
                *reinterpret_cast<int32_t*>(&r13_42) = 0;
                *reinterpret_cast<int32_t*>(&r13_42 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbx47) = 0;
                *reinterpret_cast<int32_t*>(&rbx47 + 4) = 0;
                if (reinterpret_cast<unsigned char>(v9) < reinterpret_cast<unsigned char>(rsi8)) {
                    rdi65 = v9;
                    rax74 = rsi8;
                    r10_73 = r14_5;
                    r15_20 = r14_5;
                    *reinterpret_cast<int32_t*>(&rdx63) = 0;
                    *reinterpret_cast<int32_t*>(&rdx63 + 4) = 0;
                } else {
                    do {
                        rax75 = maybe_create_temp(r12_10, 0, rdx);
                        rsi76 = rbp6;
                        *reinterpret_cast<uint32_t*>(&rdx77) = nmerge;
                        *reinterpret_cast<int32_t*>(&rdx77 + 4) = 0;
                        r8_78 = rax75 + 13;
                        if (reinterpret_cast<unsigned char>(rdx77) <= reinterpret_cast<unsigned char>(rbp6)) {
                            rsi76 = rdx77;
                        }
                        rax80 = mergefiles(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx47) << 4) + reinterpret_cast<unsigned char>(r14_5), rsi76, rdx77, v79, r8_78);
                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
                        rdx = rbp6;
                        r8_40 = r8_78;
                        *reinterpret_cast<uint32_t*>(&rsi8) = nmerge;
                        *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                        if (reinterpret_cast<unsigned char>(rax80) <= reinterpret_cast<unsigned char>(rbp6)) {
                            rdx = rax80;
                        }
                        rbx47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx47) + reinterpret_cast<unsigned char>(rax80));
                        rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_42) << 4);
                        ++r13_42;
                        rdi65 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(rbx47));
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<unsigned char>(rcx)) = r8_40;
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<unsigned char>(rcx) + 8) = rax75;
                        rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) - reinterpret_cast<unsigned char>(rdx));
                    } while (reinterpret_cast<unsigned char>(rsi8) <= reinterpret_cast<unsigned char>(rdi65));
                    r10_73 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<unsigned char>(rcx) + 16);
                    rdx63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_42) % reinterpret_cast<unsigned char>(rsi8));
                    r15_20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx47) << 4) + reinterpret_cast<unsigned char>(r14_5));
                    rax74 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi8) - reinterpret_cast<unsigned char>(rdx63));
                }
            } while (reinterpret_cast<unsigned char>(rdi65) <= reinterpret_cast<unsigned char>(rax74));
        }
    }
    addr_afa3_29:
    r9_16 = v81;
    rcx = rax48;
    rdx82 = r13_42;
    r8_40 = v11;
    rdi83 = r14_5;
    rsi84 = v41;
    mergefps(rdi83, rsi84, rdx82, rcx, r8_40, r9_16);
    rsp85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
    rax86 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (!rax86) {
        return;
    }
    addr_b011_57:
    fun_3950();
    r15_87 = reinterpret_cast<unsigned char>(rsi84) >> 1;
    r13_88 = r8_40;
    rsp89 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp85) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    r14_90 = *reinterpret_cast<void***>(rcx + 40);
    r12_91 = *reinterpret_cast<void***>(rcx + 48);
    rcx92 = v41;
    v93 = rdx82;
    v94 = r9_16;
    v95 = rcx92;
    rax96 = g28;
    v97 = rax96;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_90) + reinterpret_cast<unsigned char>(r12_91)) <= 0x1ffff || (v98 = rsi84, reinterpret_cast<unsigned char>(rsi84) <= reinterpret_cast<unsigned char>(1))) {
        addr_b0b7_59:
        r8_99 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi83) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v93) << 5));
        rax100 = reinterpret_cast<unsigned char>(r14_90) << 5;
        r15_101 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi83) - rax100);
        r9_102 = reinterpret_cast<void*>(-rax100);
        if (reinterpret_cast<unsigned char>(r12_91) > reinterpret_cast<unsigned char>(1)) {
            *reinterpret_cast<int32_t*>(&rcx92) = 0;
            *reinterpret_cast<int32_t*>(&rcx92 + 4) = 0;
            rsi84 = r12_91;
            rdx82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_99) - (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_90) >> 1) << 5));
            sequential_sort(r15_101, rsi84, rdx82, 0);
            r9_102 = r9_102;
            r8_99 = r8_99;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rsi84) = 0;
        *reinterpret_cast<int32_t*>(&rsi84 + 4) = 0;
        rcx92 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp89) + 80);
        rdx82 = reinterpret_cast<void**>(0xb880);
        eax103 = fun_3c60(reinterpret_cast<int64_t>(rsp89) + 72);
        if (!eax103) {
            rax104 = *reinterpret_cast<void***>(rcx + 40);
            rcx105 = *reinterpret_cast<void***>(rcx + 72);
            sortlines(reinterpret_cast<unsigned char>(rdi83) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax104) << 5), reinterpret_cast<unsigned char>(v98) - r15_87, v93, rcx105, r13_88, v94, v94);
            fun_3e00(v106);
            goto addr_b722_63;
        } else {
            r14_90 = *reinterpret_cast<void***>(rcx + 40);
            r12_91 = *reinterpret_cast<void***>(rcx + 48);
            goto addr_b0b7_59;
        }
    }
    if (reinterpret_cast<unsigned char>(r14_90) > reinterpret_cast<unsigned char>(1)) {
        *reinterpret_cast<int32_t*>(&rcx92) = 0;
        *reinterpret_cast<int32_t*>(&rcx92 + 4) = 0;
        rdx82 = r8_99;
        rsi84 = r14_90;
        sequential_sort(rdi83, rsi84, rdx82, 0);
        r9_102 = r9_102;
    }
    *reinterpret_cast<void***>(rcx) = rdi83;
    r14_107 = reinterpret_cast<void***>(r13_88 + 48);
    *reinterpret_cast<void***>(rcx + 8) = r15_101;
    *reinterpret_cast<void***>(rcx + 16) = r15_101;
    *reinterpret_cast<void***>(rcx + 24) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi83) + (reinterpret_cast<int64_t>(r9_102) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_91) << 5)));
    rbp108 = r13_88 + 8;
    fun_3e80(rbp108, rsi84, rdx82, rcx92);
    rdi109 = *reinterpret_cast<void***>(r13_88);
    rsi110 = rcx;
    heap_insert(rdi109, rsi110, rdx82, rcx92);
    *reinterpret_cast<unsigned char*>(rcx + 84) = 1;
    fun_3a90(r14_107, rsi110, rdx82, rcx92);
    fun_3b80(rbp108, rsi110, rdx82, rcx92);
    while (1) {
        fun_3e80(rbp108, rsi110, rdx82, rcx92);
        while (rdi111 = *reinterpret_cast<void***>(r13_88), rax112 = heap_remove_top(rdi111, rsi110, rdx82, rcx92), rax112 == 0) {
            rsi110 = rbp108;
            fun_38e0(r14_107, rsi110, rdx82, rcx92);
        }
        rbx113 = rax112;
        fun_3b80(rbp108, rsi110, rdx82, rcx92);
        rax114 = rbx113 + 88;
        v115 = rax114;
        fun_3e80(rax114, rsi110, rdx82, rcx92);
        *reinterpret_cast<uint32_t*>(&rax116) = *reinterpret_cast<uint32_t*>(rbx113 + 80);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax116) + 4) = 0;
        *reinterpret_cast<unsigned char*>(rbx113 + 84) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax116)) 
            break;
        ecx117 = static_cast<int32_t>(rax116 + rax116 + 2);
        rdx118 = *reinterpret_cast<void***>(rbx113);
        rdi119 = *reinterpret_cast<void***>(rbx113 + 8);
        v120 = rdx118;
        r9_121 = rdx118;
        v122 = rdi119;
        rsi110 = *reinterpret_cast<void***>(rbx113 + 16);
        rcx92 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(v93) >> *reinterpret_cast<signed char*>(&ecx117)) + 1);
        if (*reinterpret_cast<uint32_t*>(&rax116) == 1) {
            rax123 = rdi119;
            if (rdx118 == rsi110) {
                *reinterpret_cast<int32_t*>(&r10_124) = 0;
                *reinterpret_cast<int32_t*>(&r10_124 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx82) = 0;
                *reinterpret_cast<int32_t*>(&rdx82 + 4) = 0;
            } else {
                v125 = rbp108;
                r15_126 = v94;
                rbp127 = rbx113;
                rbx128 = v95;
                while (*reinterpret_cast<void***>(rbp127 + 24) != rax123) {
                    r12_129 = rcx92 + 0xffffffffffffffff;
                    if (!rcx92) 
                        goto addr_b6d8_78;
                    rax130 = compare(r9_121 + 0xffffffffffffffe0, rax123 + 0xffffffffffffffe0, rdx118);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax130) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax130) == 0))) {
                        rdx118 = rbx128;
                        rsi110 = r15_126;
                        rdi131 = *reinterpret_cast<void***>(rbp127 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp127 + 8) = rdi131;
                        write_unique(rdi131, rsi110, rdx118);
                        r9_121 = *reinterpret_cast<void***>(rbp127);
                        rax123 = *reinterpret_cast<void***>(rbp127 + 8);
                        if (r9_121 == *reinterpret_cast<void***>(rbp127 + 16)) 
                            goto addr_b3f5_81;
                    } else {
                        rdx118 = rbx128;
                        rsi110 = r15_126;
                        rdi132 = *reinterpret_cast<void***>(rbp127) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp127) = rdi132;
                        write_unique(rdi132, rsi110, rdx118);
                        r9_121 = *reinterpret_cast<void***>(rbp127);
                        rax123 = *reinterpret_cast<void***>(rbp127 + 8);
                        if (r9_121 == *reinterpret_cast<void***>(rbp127 + 16)) 
                            goto addr_b3f5_81;
                    }
                    rcx92 = r12_129;
                }
                goto addr_b600_84;
            }
        } else {
            rdx82 = *reinterpret_cast<void***>(rbx113 + 32);
            rdi133 = v120;
            rax134 = v122;
            r15_135 = *reinterpret_cast<void***>(rdx82);
            r11_136 = rdi133;
            if (rdi133 == rsi110) {
                r8d137 = 0;
                *reinterpret_cast<int32_t*>(&r10_124) = 0;
                *reinterpret_cast<int32_t*>(&r10_124 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r9_138) = 0;
                *reinterpret_cast<int32_t*>(&r9_138 + 4) = 0;
                goto addr_b2e8_87;
            } else {
                rdx139 = r15_135;
                v140 = r9_121;
                r15_141 = rbx113;
                rbx142 = rdx139;
                while (*reinterpret_cast<void***>(r15_141 + 24) != rax134) {
                    r12_143 = rcx92 + 0xffffffffffffffff;
                    if (!rcx92) 
                        goto addr_b5c0_91;
                    rbx142 = rbx142 - 32;
                    rax144 = compare(r11_136 + 0xffffffffffffffe0, rax134 + 0xffffffffffffffe0, rdx139);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax144) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax144) == 0))) {
                        r11_136 = *reinterpret_cast<void***>(r15_141);
                        rsi110 = *reinterpret_cast<void***>(r15_141 + 16);
                        __asm__("movdqu xmm0, [rcx-0x20]");
                        rax134 = *reinterpret_cast<void***>(r15_141 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_141 + 8) = rax134;
                        __asm__("movups [rbx], xmm0");
                        __asm__("movdqu xmm1, [rcx-0x10]");
                        __asm__("movups [rbx+0x10], xmm1");
                        if (rsi110 == r11_136) 
                            goto addr_b2b8_94;
                    } else {
                        rsi110 = *reinterpret_cast<void***>(r15_141 + 16);
                        __asm__("movdqu xmm2, [rax-0x20]");
                        r11_136 = *reinterpret_cast<void***>(r15_141) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_141) = r11_136;
                        __asm__("movups [rbx], xmm2");
                        __asm__("movdqu xmm3, [rax-0x10]");
                        rax134 = *reinterpret_cast<void***>(r15_141 + 8);
                        __asm__("movups [rbx+0x10], xmm3");
                        if (rsi110 == r11_136) 
                            goto addr_b2b8_94;
                    }
                    rcx92 = r12_143;
                }
                goto addr_b440_97;
            }
        }
        addr_b418_98:
        r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
        rsi110 = r11_145;
        if (*reinterpret_cast<void***>(rbx113 + 48) != r10_124) {
            addr_b6a8_99:
            r11_145 = rsi110;
            if (rsi110 != rdx82 || (rax123 == *reinterpret_cast<void***>(rbx113 + 24) || !rcx92)) {
                rdi133 = r9_121;
                r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
                r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
                r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx113 + 8))) >> 5);
            } else {
                v147 = rbp108;
                r15_148 = v94;
                rbp149 = rcx92 + 0xffffffffffffffff;
                r12_150 = v95;
                do {
                    rdi151 = rax123 + 0xffffffffffffffe0;
                    rdx82 = r12_150;
                    rsi110 = r15_148;
                    *reinterpret_cast<void***>(rbx113 + 8) = rdi151;
                    write_unique(rdi151, rsi110, rdx82);
                    rax123 = *reinterpret_cast<void***>(rbx113 + 8);
                    if (rax123 == *reinterpret_cast<void***>(rbx113 + 24)) 
                        break;
                    cf152 = reinterpret_cast<unsigned char>(rbp149) < reinterpret_cast<unsigned char>(1);
                    --rbp149;
                } while (!cf152);
                rbp108 = v147;
                r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
                rdi133 = *reinterpret_cast<void***>(rbx113);
                r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
                r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
                r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax123)) >> 5);
            }
        } else {
            r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
            rdi133 = r9_121;
            r12_146 = r10_124;
        }
        addr_b301_106:
        *reinterpret_cast<void***>(rbx113 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_145) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v120) - reinterpret_cast<unsigned char>(rdi133)) >> 5));
        *reinterpret_cast<void***>(rbx113 + 48) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_146) - reinterpret_cast<unsigned char>(r10_124));
        if (!*reinterpret_cast<signed char*>(&r8d137)) {
            rsi110 = rbx113;
            queue_check_insert_part_0(r13_88, rsi110, rdx82);
        }
        if (*reinterpret_cast<uint32_t*>(rbx113 + 80) > 1) {
            rdi153 = *reinterpret_cast<void***>(rbx113 + 56) + 88;
            fun_3e80(rdi153, rsi110, rdx82, rcx92);
            rsi110 = *reinterpret_cast<void***>(rbx113 + 56);
            if (!*reinterpret_cast<unsigned char*>(rsi110 + 84)) {
                queue_check_insert_part_0(r13_88, rsi110, rdx82);
                rsi110 = *reinterpret_cast<void***>(rbx113 + 56);
            }
            fun_3b80(rsi110 + 88, rsi110, rdx82, rcx92);
        } else {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx113 + 48)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx113 + 40)))) {
                r12_154 = *reinterpret_cast<void***>(rbx113 + 56);
                fun_3e80(rbp108, rsi110, rdx82, rcx92);
                rdi155 = *reinterpret_cast<void***>(r13_88);
                rsi110 = r12_154;
                heap_insert(rdi155, rsi110, rdx82, rcx92);
                *reinterpret_cast<unsigned char*>(r12_154 + 84) = 1;
                fun_3a90(r14_107, rsi110, rdx82, rcx92);
                fun_3b80(rbp108, rsi110, rdx82, rcx92);
            }
        }
        fun_3b80(v115, rsi110, rdx82, rcx92);
        continue;
        addr_b600_84:
        rbx113 = rbp127;
        rbp108 = v125;
        addr_b608_115:
        r10_124 = *reinterpret_cast<void***>(rbx113 + 48);
        rdx82 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax123)) >> 5);
        if (rdx82 != r10_124) {
            rsi110 = *reinterpret_cast<void***>(rbx113 + 40);
            rdx82 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v120) - reinterpret_cast<unsigned char>(r9_121)) >> 5);
            goto addr_b6a8_99;
        } else {
            rdi133 = r9_121;
            if (*reinterpret_cast<void***>(rbx113 + 16) == r9_121 || !rcx92) {
                r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
                r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
                r12_146 = r10_124;
                goto addr_b301_106;
            } else {
                v156 = rbp108;
                r15_157 = v94;
                rbp158 = rcx92 + 0xffffffffffffffff;
                r12_159 = v95;
                do {
                    rdi160 = rdi133 - 32;
                    rdx82 = r12_159;
                    rsi110 = r15_157;
                    *reinterpret_cast<void***>(rbx113) = rdi160;
                    write_unique(rdi160, rsi110, rdx82);
                    rdi133 = *reinterpret_cast<void***>(rbx113);
                    if (rdi133 == *reinterpret_cast<void***>(rbx113 + 16)) 
                        break;
                    cf161 = reinterpret_cast<unsigned char>(rbp158) < reinterpret_cast<unsigned char>(1);
                    --rbp158;
                } while (!cf161);
                rbp108 = v156;
                r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
                r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx113 + 8))) >> 5);
                r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
                r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
                goto addr_b301_106;
            }
        }
        addr_b6d8_78:
        rbx113 = rbp127;
        rcx92 = reinterpret_cast<void**>(0xffffffffffffffff);
        rbp108 = v125;
        goto addr_b608_115;
        addr_b3f5_81:
        rbx113 = rbp127;
        rcx92 = r12_129;
        rbp108 = v125;
        rdx82 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v120) - reinterpret_cast<unsigned char>(r9_121)) >> 5);
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax123)) >> 5);
        goto addr_b418_98;
        addr_b2e8_87:
        r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
        r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
        if (r10_124 == r12_146) 
            goto addr_b2fe_123;
        if (r9_138 != r11_145 || ((r9_162 = *reinterpret_cast<void***>(rbx113 + 24), r9_162 == rax134) || (rsi110 = rcx92 + 0xffffffffffffffff, rcx92 == 0))) {
            addr_b2fe_123:
            *reinterpret_cast<void***>(rdx82) = r15_135;
            goto addr_b301_106;
        } else {
            rax163 = rax134 - 32;
            do {
                __asm__("movdqu xmm6, [rax]");
                r15_135 = r15_135 - 32;
                *reinterpret_cast<void***>(rbx113 + 8) = rax163;
                rcx92 = rax163;
                __asm__("movups [r15], xmm6");
                __asm__("movdqu xmm7, [rax+0x10]");
                __asm__("movups [r15+0x10], xmm7");
                if (rax163 == r9_162) 
                    break;
                --rsi110;
                rax163 = rax163 - 32;
            } while (rsi110 != 0xffffffffffffffff);
            goto addr_b7f7_128;
        }
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax163)) >> 5);
        goto addr_b2fe_123;
        addr_b7f7_128:
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rcx92)) >> 5);
        goto addr_b2fe_123;
        addr_b440_97:
        rbx113 = r15_141;
        rdi133 = *reinterpret_cast<void***>(rbx113);
        r15_135 = rbx142;
        r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
        rdx82 = *reinterpret_cast<void***>(rbx113 + 32);
        r9_138 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v140) - reinterpret_cast<unsigned char>(r11_136)) >> 5);
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax134)) >> 5);
        addr_b46d_130:
        r12_146 = *reinterpret_cast<void***>(rbx113 + 48);
        r11_145 = *reinterpret_cast<void***>(rbx113 + 40);
        if (r12_146 == r10_124) {
            if (rsi110 == rdi133) 
                goto addr_b2fe_123;
            rax164 = rcx92 + 0xffffffffffffffff;
            if (!rcx92) 
                goto addr_b2fe_123;
            rcx92 = rdi133 + 0xffffffffffffffe0;
            do {
                __asm__("movdqu xmm4, [rcx]");
                r15_135 = r15_135 - 32;
                *reinterpret_cast<void***>(rbx113) = rcx92;
                rdi133 = rcx92;
                __asm__("movups [r15], xmm4");
                __asm__("movdqu xmm5, [rcx+0x10]");
                __asm__("movups [r15+0x10], xmm5");
                if (rcx92 == rsi110) 
                    break;
                --rax164;
                rcx92 = rcx92 - 32;
            } while (rax164 != 0xffffffffffffffff);
            goto addr_b2fe_123;
            goto addr_b2fe_123;
        }
        addr_b5c0_91:
        rbx113 = r15_141;
        rdi133 = *reinterpret_cast<void***>(rbx113);
        r15_135 = rbx142;
        r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
        rcx92 = reinterpret_cast<void**>(0xffffffffffffffff);
        rdx82 = *reinterpret_cast<void***>(rbx113 + 32);
        r9_138 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v140) - reinterpret_cast<unsigned char>(r11_136)) >> 5);
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax134)) >> 5);
        goto addr_b46d_130;
        addr_b2b8_94:
        rbx113 = r15_141;
        r8d137 = *reinterpret_cast<unsigned char*>(rbx113 + 84);
        r15_135 = rbx142;
        rdx82 = *reinterpret_cast<void***>(rbx113 + 32);
        rdi133 = r11_136;
        rcx92 = r12_143;
        r9_138 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v140) - reinterpret_cast<unsigned char>(r11_136)) >> 5);
        r10_124 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v122) - reinterpret_cast<unsigned char>(rax134)) >> 5);
        goto addr_b2e8_87;
    }
    fun_3b80(v115, rsi110, rdx82, rcx92);
    fun_3e80(rbp108, rsi110, rdx82, rcx92);
    rdi165 = *reinterpret_cast<void***>(r13_88);
    heap_insert(rdi165, rbx113, rdx82, rcx92);
    *reinterpret_cast<unsigned char*>(rbx113 + 84) = 1;
    fun_3a90(r14_107, rbx113, rdx82, rcx92);
    fun_3b80(rbp108, rbx113, rdx82, rcx92);
    addr_b722_63:
    rax166 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v97) - reinterpret_cast<unsigned char>(g28));
    if (rax166) {
        fun_3950();
    } else {
        goto v167;
    }
    addr_afe8_33:
    rbp168 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax45) << 4));
    *reinterpret_cast<int32_t*>(&rdx82) = 5;
    *reinterpret_cast<int32_t*>(&rdx82 + 4) = 0;
    rax169 = fun_3920();
    rdi83 = rax169;
    rsi84 = rbp168;
    sort_die(rdi83, rsi84, 5);
    rsp85 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8 - 8 + 8);
    goto addr_b011_57;
}

void key_to_opts(void** rdi, void** rsi, void** rdx, ...) {
    void** rax4;

    if (*reinterpret_cast<void***>(rdi + 48)) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(98);
        ++rsi;
    }
    if (*reinterpret_cast<void***>(rdi + 32) == 0x1d560) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(100);
        ++rsi;
    }
    rax4 = rsi;
    if (*reinterpret_cast<void***>(rdi + 40)) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0x66);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 52)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x67);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 53)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x68);
        ++rax4;
    }
    if (*reinterpret_cast<void***>(rdi + 32) == 0x1d660) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x69);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 54)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(77);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 50)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x6e);
        ++rax4;
    }
    if (*reinterpret_cast<unsigned char*>(rdi + 51)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(82);
        ++rax4;
    }
    if (*reinterpret_cast<signed char*>(rdi + 55)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0x72);
        ++rax4;
    }
    if (*reinterpret_cast<void***>(rdi + 56)) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(86);
        ++rax4;
    }
    *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0);
    return;
}

void** fun_3760(void** rdi, void** rsi, void** rdx);

void xalloc_die();

struct s2 {
    signed char[1] pad1;
    void** f1;
};

struct s2* fun_3ac0(void** rdi, void** rsi, void** rdx);

int64_t quotearg_n_style();

int32_t fun_3ca0(int64_t rdi, void* rsi, void** rdx, int64_t rcx);

void** proctab = reinterpret_cast<void**>(0);

struct s3 {
    signed char[12] pad12;
    signed char fc;
};

struct s3* hash_remove(void** rdi, void** rsi, void** rdx, ...);

uint32_t nprocs = 0;

void fun_38b0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** inttostr(int64_t rdi, void* rsi, void** rdx, void** rcx);

void** fun_3800(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void initbuf(void** rdi, void** rsi, void** rdx) {
    void** r13_4;
    void** r12_5;
    void** rbp6;
    void** rbx7;
    void* rsp8;
    void** rbp9;
    void** rax10;
    void** rax11;
    int64_t v12;
    int64_t rax13;
    void** rdx14;
    void* rsp15;
    void** rax16;
    int64_t rdi17;
    int32_t eax18;
    void* rsp19;
    void** rsi20;
    void** rax21;
    void** rdi22;
    void** rsi23;
    struct s3* rax24;
    void* rax25;
    int64_t v26;
    uint32_t eax27;
    uint32_t v28;
    void** rsi29;
    void** rax30;
    void** rcx31;
    void** rax32;
    void** rdx33;
    void* rsp34;
    void** rax35;
    void** rax36;
    void** r8_37;

    r13_4 = rsi;
    r12_5 = rsi + 1;
    rbp6 = rdx;
    rbx7 = rdi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8);
    do {
        rbp9 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rbp6) & 0xffffffffffffffe0) + 32);
        rax10 = fun_3760(rbp9, rsi, rdx);
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        *reinterpret_cast<void***>(rbx7) = rax10;
        if (rax10) 
            break;
        rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) >> 1);
    } while (reinterpret_cast<unsigned char>(r12_5) < reinterpret_cast<unsigned char>(rbp6));
    goto addr_7496_4;
    *reinterpret_cast<void***>(rbx7 + 40) = r13_4;
    *reinterpret_cast<void***>(rbx7 + 24) = rbp9;
    *reinterpret_cast<void***>(rbx7 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx7 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbx7 + 48) = reinterpret_cast<void**>(0);
    return;
    addr_7496_4:
    xalloc_die();
    rax11 = fun_37d0();
    *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(0);
    fun_3ac0(rbp9, rsi, rdx);
    if (!*reinterpret_cast<void***>(rax11)) {
        goto v12;
    }
    fun_3920();
    fun_3c90();
    fun_3920();
    fun_3c90();
    rax13 = quotearg_n_style();
    fun_3920();
    fun_3c90();
    *reinterpret_cast<uint32_t*>(&rdx14) = 0;
    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 48);
    rax16 = g28;
    *reinterpret_cast<int32_t*>(&rdi17) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
    if (!1) 
        goto addr_758c_10;
    *reinterpret_cast<int32_t*>(&rdi17) = 2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
    addr_758c_10:
    *reinterpret_cast<unsigned char*>(&rdx14) = 0;
    eax18 = fun_3ca0(rdi17, reinterpret_cast<int64_t>(rsp15) + 12, 0, rax13);
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
    if (eax18 < 0) {
        addr_7605_12:
        rsi20 = compress_program;
        quotearg_style(4, rsi20, rdx14, 4, rsi20, rdx14);
        rax21 = fun_3920();
        fun_37d0();
        rdx14 = rax21;
        fun_3c90();
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_7648_13;
    } else {
        if (!eax18) 
            goto addr_75ba_15;
        if (!0) 
            goto addr_75a1_17;
    }
    rdi22 = proctab;
    rsi23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp19) + 16);
    rax24 = hash_remove(rdi22, rsi23, 0, rdi22, rsi23, 0);
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
    if (!rax24) {
        addr_75ba_15:
        rax25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax16) - reinterpret_cast<unsigned char>(g28));
        if (rax25) {
            fun_3950();
            rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8);
            goto addr_7605_12;
        } else {
            goto v26;
        }
    } else {
        rax24->fc = 2;
    }
    addr_75a1_17:
    eax27 = v28;
    *reinterpret_cast<uint32_t*>(&rdx14) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax27) + 1)) | eax27 & 0x7f;
    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
    if (1) {
        addr_7648_13:
        rsi29 = compress_program;
        rax30 = quotearg_style(4, rsi29, rdx14, 4, rsi29, rdx14);
        fun_3920();
        rcx31 = rax30;
        fun_3c90();
    } else {
        --nprocs;
        goto addr_75ba_15;
    }
    rax32 = fun_3940(0, 0);
    rdx33 = rax32;
    fun_38b0(2, 0, rdx33, rcx31);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8);
    if (!1) 
        goto addr_76ca_24;
    while (1) {
        rax35 = inttostr(2, reinterpret_cast<int64_t>(rsp34) + 12, rdx33, rcx31);
        fun_38b0(2, ": errno ", 8, rcx31);
        rax36 = fun_3940(rax35, rax35);
        fun_38b0(2, rax35, rax36, rcx31);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_76ca_24:
        *reinterpret_cast<int32_t*>(&rdx33) = 1;
        *reinterpret_cast<int32_t*>(&rdx33 + 4) = 0;
        fun_38b0(2, "\n", 1, rcx31);
        fun_3800(2, "\n", 1, rcx31, r8_37);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
    }
}

void badfieldspec(void** rdi, int64_t rsi, void** rdx, ...) {
    void** rax4;
    uint32_t eax5;
    int64_t rax6;
    void* rdx7;
    int64_t v8;

    quote(rdi);
    fun_3920();
    fun_3920();
    fun_3c90();
    rax4 = g28;
    eax5 = xstrtoumax(2, 2);
    if (eax5 <= 4) {
        *reinterpret_cast<uint32_t*>(&rax6) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x150b8 + rax6 * 4) + 0x150b8;
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (!rdx7) 
        goto addr_70b5_6;
    fun_3950();
    quote(2, 2);
    fun_3920();
    fun_3920();
    fun_3c90();
    if (!1) 
        goto addr_7142_10;
    while (1) {
        fun_3920();
        addr_7142_10:
        quotearg_n_style_colon();
        fun_37d0();
        fun_3c90();
    }
    addr_70b5_6:
    goto v8;
}

void sort_die(void** rdi, void** rsi, void** rdx, ...) {
    if (rsi) 
        goto addr_7142_2;
    while (1) {
        fun_3920();
        addr_7142_2:
        quotearg_n_style_colon();
        fun_37d0();
        fun_3c90();
    }
}

uint32_t tab = 0x80;

/* limfield.isra.0 */
void** limfield_isra_0(void** rdi, void** rsi, void** rdx, ...) {
    void** r9_4;
    void** rdx5;
    uint32_t esi6;
    void** rax7;
    void** r8_8;
    void* rcx9;
    void* rdi10;
    int64_t rcx11;
    void** r8_12;
    int64_t rcx13;
    uint32_t ecx14;
    int64_t rcx15;
    int1_t cf16;
    void* rdi17;
    int1_t cf18;

    r9_4 = rdx;
    rdx5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi) + 0xffffffffffffffff);
    esi6 = tab;
    rax7 = rdi;
    r8_8 = *reinterpret_cast<void***>(r9_4 + 24);
    rcx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9_4 + 16)) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_8) < reinterpret_cast<unsigned char>(1))));
    if (esi6 == 0x80) {
        if (reinterpret_cast<unsigned char>(rdx5) <= reinterpret_cast<unsigned char>(rdi) || (rdi10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx9) - 1), rcx9 == 0)) {
            addr_7990_3:
            if (r8_8) {
                if (*reinterpret_cast<signed char*>(r9_4 + 49)) {
                    if (reinterpret_cast<unsigned char>(rdx5) > reinterpret_cast<unsigned char>(rax7)) {
                        do {
                            *reinterpret_cast<uint32_t*>(&rcx11) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax7));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
                            if (!*reinterpret_cast<signed char*>(0x1d760 + rcx11)) 
                                break;
                            ++rax7;
                        } while (rdx5 != rax7);
                    }
                }
                r8_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_8) + reinterpret_cast<unsigned char>(rax7));
                rax7 = r8_12;
                if (reinterpret_cast<unsigned char>(rdx5) <= reinterpret_cast<unsigned char>(r8_12)) {
                    rax7 = rdx5;
                }
            }
        } else {
            do {
                *reinterpret_cast<uint32_t*>(&rcx13) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax7));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx13) + 4) = 0;
                ecx14 = *reinterpret_cast<unsigned char*>(0x1d760 + rcx13);
                do {
                    if (*reinterpret_cast<signed char*>(&ecx14)) 
                        break;
                    if (reinterpret_cast<unsigned char>(rdx5) <= reinterpret_cast<unsigned char>(rax7)) 
                        goto addr_7990_3;
                    do {
                        ++rax7;
                        if (rdx5 == rax7) 
                            goto addr_7990_3;
                        *reinterpret_cast<uint32_t*>(&rcx15) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax7));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx15) + 4) = 0;
                        ecx14 = *reinterpret_cast<unsigned char*>(0x1d760 + rcx15);
                    } while (!*reinterpret_cast<signed char*>(&ecx14));
                    if (reinterpret_cast<unsigned char>(rdx5) <= reinterpret_cast<unsigned char>(rax7)) 
                        goto addr_7990_3;
                    cf16 = reinterpret_cast<uint64_t>(rdi10) < 1;
                    rdi10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi10) - 1);
                } while (!cf16);
                goto addr_7a5f_20;
                ++rax7;
            } while (reinterpret_cast<unsigned char>(rdx5) > reinterpret_cast<unsigned char>(rax7));
            goto addr_7a1c_22;
        }
    } else {
        if (reinterpret_cast<unsigned char>(rdx5) <= reinterpret_cast<unsigned char>(rdi)) 
            goto addr_7990_3;
        rdi17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx9) - 1);
        if (!rcx9) 
            goto addr_7990_3;
        while (1) {
            if (esi6 == static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax7)))) {
                if (reinterpret_cast<unsigned char>(rdx5) <= reinterpret_cast<unsigned char>(rax7)) 
                    goto addr_7990_3;
                if (!(reinterpret_cast<unsigned char>(r8_8) | reinterpret_cast<uint64_t>(rdi17))) 
                    break;
                ++rax7;
                if (reinterpret_cast<unsigned char>(rdx5) <= reinterpret_cast<unsigned char>(rax7)) 
                    goto addr_7990_3;
                cf18 = reinterpret_cast<uint64_t>(rdi17) < 1;
                rdi17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi17) - 1);
                if (cf18) 
                    goto addr_79ec_31;
            } else {
                ++rax7;
                if (reinterpret_cast<unsigned char>(rdx5) <= reinterpret_cast<unsigned char>(rax7)) 
                    goto addr_7990_3;
            }
        }
    }
    return rax7;
    addr_7a5f_20:
    goto addr_7990_3;
    addr_7a1c_22:
    goto addr_7990_3;
    addr_79ec_31:
    goto addr_7990_3;
}

/* begfield.isra.0 */
void** begfield_isra_0(void** rdi, void** rsi, void** rdx, ...) {
    void** rax4;
    void** rcx5;
    void** r9_6;
    uint32_t edi7;
    void** r8_8;
    int1_t cf9;
    void** rdx10;
    void** r8_11;
    int64_t rdx12;
    int64_t rsi13;
    uint32_t esi14;
    int64_t rsi15;
    int1_t cf16;
    void** rdx17;
    void** rdx18;

    rax4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi) + 0xffffffffffffffff);
    rcx5 = rdi;
    r9_6 = *reinterpret_cast<void***>(rdx + 8);
    edi7 = tab;
    if (edi7 != 0x80) {
        if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rcx5)) {
            r8_8 = *reinterpret_cast<void***>(rdx) + 0xffffffffffffffff;
            if (!*reinterpret_cast<void***>(rdx)) 
                goto addr_7ad4_4;
            while (1) {
                if (edi7 == static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rcx5)))) {
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(rcx5)) 
                        goto addr_7ab0_8;
                    ++rcx5;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(rcx5)) 
                        goto addr_7ab0_8;
                    cf9 = reinterpret_cast<unsigned char>(r8_8) < reinterpret_cast<unsigned char>(1);
                    --r8_8;
                    if (cf9) 
                        goto addr_7ad4_4;
                } else {
                    ++rcx5;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(rcx5)) 
                        goto addr_7ab0_8;
                }
            }
        }
    }
    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(rcx5)) {
        addr_7ab0_8:
        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx5) + reinterpret_cast<unsigned char>(r9_6));
        if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rdx10)) {
            rax4 = rdx10;
        }
    } else {
        r8_11 = *reinterpret_cast<void***>(rdx) + 0xffffffffffffffff;
        if (!*reinterpret_cast<void***>(rdx)) {
            addr_7ad4_4:
            if (*reinterpret_cast<void***>(rdx + 48)) {
                do {
                    *reinterpret_cast<uint32_t*>(&rdx12) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx5));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
                    if (!*reinterpret_cast<signed char*>(0x1d760 + rdx12)) 
                        goto addr_7afa_16;
                    ++rcx5;
                } while (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rcx5));
                goto addr_7ab0_8;
            } else {
                goto addr_7ab0_8;
            }
        } else {
            do {
                *reinterpret_cast<uint32_t*>(&rsi13) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx5));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
                esi14 = *reinterpret_cast<unsigned char*>(0x1d760 + rsi13);
                do {
                    if (*reinterpret_cast<signed char*>(&esi14)) 
                        break;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(rcx5)) 
                        goto addr_7ab0_8;
                    do {
                        ++rcx5;
                        if (rax4 == rcx5) 
                            goto addr_7ab0_8;
                        *reinterpret_cast<uint32_t*>(&rsi15) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx5));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi15) + 4) = 0;
                        esi14 = *reinterpret_cast<unsigned char*>(0x1d760 + rsi15);
                    } while (!*reinterpret_cast<signed char*>(&esi14));
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(rcx5)) 
                        goto addr_7ab0_8;
                    cf16 = reinterpret_cast<unsigned char>(r8_11) < reinterpret_cast<unsigned char>(1);
                    --r8_11;
                } while (!cf16);
                goto addr_7b80_28;
                ++rcx5;
            } while (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rcx5));
            goto addr_7b35_30;
        }
    }
    return rax4;
    addr_7afa_16:
    rdx17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx5) + reinterpret_cast<unsigned char>(r9_6));
    if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rdx17)) {
        rax4 = rdx17;
    }
    return rax4;
    addr_7b80_28:
    goto addr_7ad4_4;
    addr_7b35_30:
    rdx18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx5) + reinterpret_cast<unsigned char>(r9_6));
    if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rdx18)) {
        rax4 = rdx18;
    }
    return rax4;
}

int32_t reap(void** edi, void** rsi) {
    void** rdx3;
    void* rsp4;
    void** rax5;
    uint1_t zf6;
    int64_t rdi7;
    int64_t rcx8;
    int32_t eax9;
    void* rsp10;
    void** rsi11;
    void** rax12;
    void** rdi13;
    struct s3* rax14;
    void* rax15;
    uint32_t eax16;
    uint32_t v17;
    void** rsi18;
    void** rax19;
    void** rcx20;
    void** rax21;
    void** rdx22;
    void* rsp23;
    void** rax24;
    void** rax25;
    void** r8_26;

    *reinterpret_cast<uint32_t*>(&rdx3) = 0;
    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 48);
    rax5 = g28;
    zf6 = reinterpret_cast<uint1_t>(edi == 0);
    *reinterpret_cast<void***>(&rdi7) = reinterpret_cast<void**>(0xffffffff);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
    if (!zf6) {
        *reinterpret_cast<void***>(&rdi7) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
    }
    *reinterpret_cast<unsigned char*>(&rdx3) = zf6;
    eax9 = fun_3ca0(rdi7, reinterpret_cast<int64_t>(rsp4) + 12, rdx3, rcx8);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
    if (eax9 < 0) {
        addr_7605_4:
        rsi11 = compress_program;
        quotearg_style(4, rsi11, rdx3);
        rax12 = fun_3920();
        fun_37d0();
        rdx3 = rax12;
        fun_3c90();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_7648_5;
    } else {
        if (!eax9) 
            goto addr_75ba_7;
        if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(edi) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(edi == 0))) 
            goto addr_75a1_9;
    }
    rdi13 = proctab;
    rax14 = hash_remove(rdi13, reinterpret_cast<int64_t>(rsp10) + 16, rdx3);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
    if (!rax14) {
        addr_75ba_7:
        rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
        if (rax15) {
            fun_3950();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            goto addr_7605_4;
        } else {
            return eax9;
        }
    } else {
        rax14->fc = 2;
    }
    addr_75a1_9:
    eax16 = v17;
    *reinterpret_cast<uint32_t*>(&rdx3) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax16) + 1)) | eax16 & 0x7f;
    *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
    if (1) {
        addr_7648_5:
        rsi18 = compress_program;
        rax19 = quotearg_style(4, rsi18, rdx3, 4, rsi18, rdx3);
        fun_3920();
        rcx20 = rax19;
        fun_3c90();
    } else {
        --nprocs;
        goto addr_75ba_7;
    }
    rax21 = fun_3940(0, 0);
    rdx22 = rax21;
    fun_38b0(2, 0, rdx22, rcx20);
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8);
    if (!1) 
        goto addr_76ca_16;
    while (1) {
        rax24 = inttostr(2, reinterpret_cast<int64_t>(rsp23) + 12, rdx22, rcx20);
        fun_38b0(2, ": errno ", 8, rcx20);
        rax25 = fun_3940(rax24, rax24);
        fun_38b0(2, rax24, rax25, rcx20);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_76ca_16:
        *reinterpret_cast<int32_t*>(&rdx22) = 1;
        *reinterpret_cast<int32_t*>(&rdx22 + 4) = 0;
        fun_38b0(2, "\n", 1, rcx20);
        fun_3800(2, "\n", 1, rcx20, r8_26);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8);
    }
}

int32_t rpl_pipe2();

void** g80000;

void** g80004;

void** temphead = reinterpret_cast<void**>(0);

void** fun_3e70();

void xnanosleep();

void** xnmalloc(void** rdi, void** rsi, void** rdx, ...);

void** pipe_fork(void** rdi, void** rsi, void** rdx, ...) {
    void** rbx4;
    void** rsi5;
    void** v6;
    void** rax7;
    void** v8;
    int32_t eax9;
    void* rsp10;
    int64_t r14_11;
    uint32_t eax12;
    int1_t cf13;
    uint32_t edx14;
    int32_t eax15;
    void** rax16;
    void** r12_17;
    void** rbp18;
    int32_t eax19;
    void** r13_20;
    unsigned char v21;
    void** eax22;
    void** r8d23;
    void** ecx24;
    void** rdi25;
    void** rsi26;
    void** rsi27;
    uint32_t eax28;
    int32_t eax29;
    int1_t cf30;
    void* rax31;
    void** r13_32;
    void** rbp33;
    void** rbx34;
    void** rax35;
    void** v36;
    void** rax37;
    void* rsp38;
    void** v39;
    void* rax40;
    void** rsi41;
    void** rax42;
    void** rax43;
    void** r13d44;
    int64_t v45;
    void** r12_46;
    void** r15_47;
    uint32_t eax48;
    void** rdi49;
    void** rax50;
    void** r9d51;
    void** rdi52;
    void** rsi53;
    struct s3* rax54;
    int32_t eax55;
    void** eax56;
    int64_t rdi57;
    int32_t v58;
    void** rax59;
    void** rax60;
    int32_t v61;
    void** rax62;
    void** edi63;
    uint32_t tmp32_64;

    rbx4 = rsi;
    *reinterpret_cast<int32_t*>(&rsi5) = 0x80000;
    *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
    v6 = rdi;
    rax7 = g28;
    v8 = rax7;
    eax9 = rpl_pipe2();
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8);
    if (eax9 < 0) {
        *reinterpret_cast<void***>(&r14_11) = reinterpret_cast<void**>(0xffffffff);
    } else {
        eax12 = nmerge;
        cf13 = eax12 + 1 < nprocs;
        if (cf13) {
            rdi = reinterpret_cast<void**>(0xffffffff);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            reap(0xffffffff, 0x80000);
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            while ((edx14 = nprocs, !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(edx14) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(edx14 == 0))) && (rdi = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, eax15 = reap(0, 0x80000), rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), !!eax15)) {
            }
        }
        rax16 = fun_37d0();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        --rbx4;
        *reinterpret_cast<void***>(rdi) = g80000;
        r12_17 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 40);
        rbp18 = rax16;
        *reinterpret_cast<void***>(rdi + 4) = g80004;
        while (1) {
            rdi = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            rdx = r12_17;
            rsi5 = reinterpret_cast<void**>(0x1d3a0);
            eax19 = fun_3c00();
            r13_20 = temphead;
            temphead = reinterpret_cast<void**>(0);
            v21 = reinterpret_cast<uint1_t>(eax19 == 0);
            eax22 = fun_3e70();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
            r8d23 = *reinterpret_cast<void***>(rbp18);
            ecx24 = eax22;
            *reinterpret_cast<void***>(&r14_11) = eax22;
            if (eax22) {
                temphead = r13_20;
                if (v21) {
                    addr_8539_16:
                    *reinterpret_cast<int32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rsi5 = r12_17;
                    rdi = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    fun_3c00();
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                    ecx24 = ecx24;
                    r8d23 = r8d23;
                    goto addr_84a2_17;
                } else {
                    addr_84a2_17:
                    *reinterpret_cast<void***>(rbp18) = r8d23;
                    if (reinterpret_cast<signed char>(ecx24) >= reinterpret_cast<signed char>(0)) 
                        goto addr_855f_18;
                }
                if (!reinterpret_cast<int1_t>(r8d23 == 11)) 
                    goto addr_85e4_20;
                *reinterpret_cast<void***>(rdi) = *reinterpret_cast<void***>(rsi5);
                rdi25 = rdi + 4;
                rsi26 = rsi5 + 4;
                xnanosleep();
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                *reinterpret_cast<void***>(rdi25) = *reinterpret_cast<void***>(rsi26);
                rsi27 = rsi26 + 4;
                __asm__("movapd xmm1, xmm2");
                __asm__("addsd xmm1, xmm2");
                *reinterpret_cast<void***>(rdi25 + 4) = *reinterpret_cast<void***>(rsi27);
                rsi5 = rsi27 + 4;
                do {
                    eax28 = nprocs;
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax28) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax28 == 0)) 
                        break;
                    eax29 = reap(0, rsi5);
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                } while (eax29);
                cf30 = reinterpret_cast<unsigned char>(rbx4) < reinterpret_cast<unsigned char>(1);
                --rbx4;
                if (cf30) 
                    goto addr_85e0_33;
            } else {
                if (!v21) 
                    goto addr_8598_35; else 
                    goto addr_8539_16;
            }
        }
    }
    addr_8568_36:
    rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (!rax31) {
        return *reinterpret_cast<void***>(&r14_11);
    }
    fun_3950();
    r13_32 = rsi5;
    rbp33 = rdx;
    rbx34 = rdi;
    rax35 = g28;
    v36 = rax35;
    rax37 = xnmalloc(r13_32, 8, rdx);
    rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    v39 = rax37;
    *reinterpret_cast<void***>(rbp33) = rax37;
    if (r13_32) 
        goto addr_8666_40;
    addr_8727_42:
    while (rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v36) - reinterpret_cast<unsigned char>(g28)), !!rax40) {
        addr_885c_43:
        fun_3950();
        addr_8861_44:
        rsi41 = compress_program;
        quotearg_style(4, rsi41, rdx, 4, rsi41, rdx);
        rax42 = fun_3920();
        rdx = rax42;
        fun_3c90();
        addr_889c_45:
        rax43 = fun_37d0();
        r13d44 = *reinterpret_cast<void***>(rax43);
        fun_3a40();
        *reinterpret_cast<void***>(rbp33) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax43) = r13d44;
    }
    goto v45;
    addr_8666_40:
    rbp33 = rax37;
    *reinterpret_cast<int32_t*>(&r12_46) = 0;
    *reinterpret_cast<int32_t*>(&r12_46 + 4) = 0;
    do {
        r15_47 = *reinterpret_cast<void***>(rbx34 + 8);
        if (!r15_47 || (eax48 = *reinterpret_cast<unsigned char*>(r15_47 + 12), *reinterpret_cast<signed char*>(&eax48) == 0)) {
            rdi49 = *reinterpret_cast<void***>(rbx34);
            rax50 = stream_open(rdi49, "r", rdx);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
            *reinterpret_cast<void***>(rbp33) = rax50;
            if (!rax50) 
                goto addr_8727_42;
        } else {
            if (*reinterpret_cast<signed char*>(&eax48) == 1 && (r9d51 = *reinterpret_cast<void***>(r15_47 + 8), rdi52 = proctab, rsi53 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp38) + 16), rax54 = hash_remove(rdi52, rsi53, rdx), rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8), !!rax54)) {
                rax54->fc = 2;
                reap(r9d51, rsi53);
                rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
            }
            eax55 = fun_3cd0(r15_47 + 13);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
            if (eax55 < 0) 
                break;
            eax56 = pipe_fork(reinterpret_cast<int64_t>(rsp38) + 16, 9, rdx);
            if (reinterpret_cast<int1_t>(eax56 == 0xffffffff)) 
                goto addr_86f1_53;
            if (!eax56) 
                goto addr_87fa_55;
            *reinterpret_cast<void***>(r15_47 + 8) = eax56;
            register_proc(r15_47, 9, rdx);
            fun_3a40();
            fun_3a40();
            *reinterpret_cast<int32_t*>(&rdi57) = v58;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi57) + 4) = 0;
            rax59 = fun_3c20(rdi57, "r", rdx);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            if (!rax59) 
                goto addr_889c_45;
            *reinterpret_cast<void***>(rbp33) = rax59;
        }
        ++r12_46;
        rbx34 = rbx34 + 16;
        rbp33 = rbp33 + 8;
    } while (r13_32 != r12_46);
    goto addr_8727_42;
    *reinterpret_cast<void***>(v39 + reinterpret_cast<unsigned char>(r12_46) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_42;
    addr_86f1_53:
    rax60 = fun_37d0();
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax60) == 24)) 
        goto addr_8861_44;
    fun_3a40();
    *reinterpret_cast<void***>(rax60) = reinterpret_cast<void**>(24);
    *reinterpret_cast<void***>(v39 + reinterpret_cast<unsigned char>(r12_46) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_42;
    addr_87fa_55:
    fun_3a40();
    if (eax55) {
        move_fd_part_0(eax55);
    }
    if (v61 != 1) {
        move_fd_part_0(v61, v61);
    }
    rdx = reinterpret_cast<void**>("-d");
    fun_3e20();
    rax62 = fun_37d0();
    edi63 = *reinterpret_cast<void***>(rax62);
    async_safe_die(edi63, "couldn't execute compress program (with -d)", edi63, "couldn't execute compress program (with -d)");
    goto addr_885c_43;
    addr_855f_18:
    if (!ecx24) {
        addr_8598_35:
        *reinterpret_cast<void***>(&r14_11) = reinterpret_cast<void**>(0);
        fun_3a40();
        rdi = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        fun_3a40();
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
        goto addr_8568_36;
    } else {
        tmp32_64 = nprocs + 1;
        nprocs = tmp32_64;
        goto addr_8568_36;
    }
    addr_85e4_20:
    fun_3a40();
    rdi = *reinterpret_cast<void***>(v6 + 4);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    fun_3a40();
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
    *reinterpret_cast<void***>(rbp18) = r8d23;
    goto addr_8568_36;
    addr_85e0_33:
    r8d23 = *reinterpret_cast<void***>(rbp18);
    goto addr_85e4_20;
}

void** hash_initialize(int64_t rdi);

int64_t hash_insert();

void register_proc(void** rdi, void** rsi, void** rdx, ...) {
    int64_t v4;
    int64_t rbp5;
    void** rdi6;
    uint64_t rcx7;
    void** rsi8;
    void** rax9;
    int64_t rax10;
    void** r12_11;
    void** rcx12;
    void** rdx13;
    void** r14_14;
    void** rsi15;
    uint32_t eax16;
    void** r15_17;
    void** rcx18;
    void** rax19;

    v4 = rbp5;
    rdi6 = proctab;
    if (!rdi6 && (rcx7 = 0x6c10, *reinterpret_cast<int32_t*>(&rsi8) = 0, *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0, rdx = reinterpret_cast<void**>(0x6bf0), rax9 = hash_initialize(47), proctab = rax9, rdi6 = rax9, !rax9) || (*reinterpret_cast<unsigned char*>(rdi + 12) = 1, rsi8 = rdi, rax10 = hash_insert(), rax10 == 0)) {
        xalloc_die();
        while (1) {
            r12_11 = rdx;
            if (1) {
                rcx12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 48)) >> 1);
                rdx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 48)) - reinterpret_cast<unsigned char>(rcx12));
                r14_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_11) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx12) << 5));
                rsi15 = rdi6 + 24;
            } else {
                rcx12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 40)) >> 1);
                rdx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 40)) - reinterpret_cast<unsigned char>(rcx12));
                r14_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_11) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx12) << 5));
                rsi15 = rdi6 + 16;
            }
            *reinterpret_cast<void***>(rsi8 + 56) = rdi6;
            eax16 = *reinterpret_cast<uint32_t*>(rdi6 + 80);
            r15_17 = rsi8 + 0x80;
            *reinterpret_cast<void***>(rsi8 + 32) = rsi15;
            *reinterpret_cast<void***>(rsi8 + 16) = r12_11;
            *reinterpret_cast<void***>(rsi8) = r12_11;
            *reinterpret_cast<void***>(rsi8 + 24) = r14_14;
            *reinterpret_cast<void***>(rsi8 + 8) = r14_14;
            *reinterpret_cast<void***>(rsi8 + 40) = rcx12;
            *reinterpret_cast<void***>(rsi8 + 48) = rdx13;
            *reinterpret_cast<uint32_t*>(rsi8 + 80) = eax16 + 1;
            *reinterpret_cast<unsigned char*>(rsi8 + 84) = 0;
            fun_3e40(rsi8 + 88);
            if (rcx7 <= 1) 
                break;
            *reinterpret_cast<void***>(rsi8 + 64) = r15_17;
            rcx18 = reinterpret_cast<void**>(rcx7 >> 1);
            rax19 = init_node(rsi8, r15_17, r12_11, rcx18, 0, rsi8, r15_17, r12_11, rcx18, 0);
            rcx7 = rcx7 - reinterpret_cast<unsigned char>(rcx18);
            rdx = r14_14;
            *reinterpret_cast<void***>(rsi8 + 72) = rax19;
            rdi6 = rsi8;
            rsi8 = rax19;
        }
        *reinterpret_cast<void***>(rsi8 + 64) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rsi8 + 72) = reinterpret_cast<void**>(0);
        goto v4;
    } else {
        return;
    }
}

struct s4 {
    signed char f0;
    signed char f1;
};

uint32_t thousands_sep = 0;

unsigned char decimal_point = 0;

signed char traverse_raw_number(struct s4** rdi, void** rsi, ...) {
    struct s4* rax3;
    struct s4** r10_4;
    int32_t ecx5;
    struct s4* rsi6;
    int32_t edx7;
    int32_t r8d8;
    uint32_t edi9;
    int32_t r9d10;
    int32_t ecx11;
    int1_t zf12;
    int32_t ecx13;
    struct s4* rdx14;
    int32_t eax15;
    int32_t ecx16;
    int32_t eax17;
    int32_t eax18;

    rax3 = *rdi;
    r10_4 = rdi;
    ecx5 = rax3->f0;
    rsi6 = reinterpret_cast<struct s4*>(&rax3->f1);
    edx7 = ecx5;
    if (ecx5 - 48 > 9) {
        r8d8 = 0;
    } else {
        edi9 = thousands_sep;
        r8d8 = 0;
        do {
            r9d10 = 1;
            if (*reinterpret_cast<signed char*>(&r8d8) < *reinterpret_cast<signed char*>(&edx7)) {
                r8d8 = edx7;
            }
            ++rax3;
            if (static_cast<int32_t>(rax3->f1) != edi9) {
                rax3 = rsi6;
                r9d10 = 0;
            }
            ecx11 = rax3->f0;
            rsi6 = reinterpret_cast<struct s4*>(&rax3->f1);
            edx7 = ecx11;
        } while (ecx11 - 48 <= 9);
        if (*reinterpret_cast<signed char*>(&r9d10)) 
            goto addr_6cc8_10;
    }
    zf12 = decimal_point == *reinterpret_cast<unsigned char*>(&edx7);
    if (zf12) {
        ecx13 = rsi6->f0;
        rdx14 = reinterpret_cast<struct s4*>(&rsi6->f1);
        eax15 = ecx13;
        if (ecx13 - 48 <= 9) {
            do {
                rsi6 = rdx14;
                if (*reinterpret_cast<signed char*>(&r8d8) < *reinterpret_cast<signed char*>(&eax15)) {
                    r8d8 = eax15;
                }
                ecx16 = rdx14->f0;
                rdx14 = reinterpret_cast<struct s4*>(&rdx14->f1);
                eax15 = ecx16;
            } while (ecx16 - 48 <= 9);
        }
    } else {
        rsi6 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(rsi6) - 1);
    }
    *r10_4 = rsi6;
    eax17 = r8d8;
    return *reinterpret_cast<signed char*>(&eax17);
    addr_6cc8_10:
    eax18 = r8d8;
    *r10_4 = reinterpret_cast<struct s4*>(reinterpret_cast<int64_t>(rax3) - 1);
    return *reinterpret_cast<signed char*>(&eax18);
}

struct s2* xstrxfrm(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    struct s2* rax5;
    int64_t rax6;
    void** rdx7;
    void* rsp8;
    void** rax9;
    int64_t rdi10;
    int32_t eax11;
    void* rsp12;
    void** rsi13;
    void** rax14;
    void** rdi15;
    void** rsi16;
    struct s3* rax17;
    void* rax18;
    int64_t v19;
    uint32_t eax20;
    uint32_t v21;
    void** rsi22;
    void** rax23;
    void** rcx24;
    void** rax25;
    void** rdx26;
    void* rsp27;
    void** rax28;
    void** rax29;
    void** r8_30;

    rax4 = fun_37d0();
    *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0);
    rax5 = fun_3ac0(rdi, rsi, rdx);
    if (!*reinterpret_cast<void***>(rax4)) {
        return rax5;
    }
    fun_3920();
    fun_3c90();
    fun_3920();
    fun_3c90();
    rax6 = quotearg_n_style();
    fun_3920();
    fun_3c90();
    *reinterpret_cast<uint32_t*>(&rdx7) = 0;
    *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 48);
    rax9 = g28;
    *reinterpret_cast<int32_t*>(&rdi10) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
    if (!1) 
        goto addr_758c_5;
    *reinterpret_cast<int32_t*>(&rdi10) = 2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
    addr_758c_5:
    *reinterpret_cast<unsigned char*>(&rdx7) = 0;
    eax11 = fun_3ca0(rdi10, reinterpret_cast<int64_t>(rsp8) + 12, 0, rax6);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
    if (eax11 < 0) {
        addr_7605_7:
        rsi13 = compress_program;
        quotearg_style(4, rsi13, rdx7, 4, rsi13, rdx7);
        rax14 = fun_3920();
        fun_37d0();
        rdx7 = rax14;
        fun_3c90();
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_7648_8;
    } else {
        if (!eax11) 
            goto addr_75ba_10;
        if (!0) 
            goto addr_75a1_12;
    }
    rdi15 = proctab;
    rsi16 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp12) + 16);
    rax17 = hash_remove(rdi15, rsi16, 0, rdi15, rsi16, 0);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    if (!rax17) {
        addr_75ba_10:
        rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax9) - reinterpret_cast<unsigned char>(g28));
        if (rax18) {
            fun_3950();
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            goto addr_7605_7;
        } else {
            goto v19;
        }
    } else {
        rax17->fc = 2;
    }
    addr_75a1_12:
    eax20 = v21;
    *reinterpret_cast<uint32_t*>(&rdx7) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax20) + 1)) | eax20 & 0x7f;
    *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
    if (1) {
        addr_7648_8:
        rsi22 = compress_program;
        rax23 = quotearg_style(4, rsi22, rdx7, 4, rsi22, rdx7);
        fun_3920();
        rcx24 = rax23;
        fun_3c90();
    } else {
        --nprocs;
        goto addr_75ba_10;
    }
    rax25 = fun_3940(0, 0);
    rdx26 = rax25;
    fun_38b0(2, 0, rdx26, rcx24);
    rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8);
    if (!1) 
        goto addr_76ca_19;
    while (1) {
        rax28 = inttostr(2, reinterpret_cast<int64_t>(rsp27) + 12, rdx26, rcx24);
        fun_38b0(2, ": errno ", 8, rcx24);
        rax29 = fun_3940(rax28, rax28);
        fun_38b0(2, rax28, rax29, rcx24);
        rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_76ca_19:
        *reinterpret_cast<int32_t*>(&rdx26) = 1;
        *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
        fun_38b0(2, "\n", 1, rcx24);
        fun_3800(2, "\n", 1, rcx24, r8_30);
        rsp27 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp27) - 8 + 8 - 8 + 8);
    }
}

void md5_process_bytes(void** rdi, void** rsi, void** rdx);

void** fun_3a80(void** rdi, void** rsi, void** rdx, ...);

struct s5 {
    signed char[120672] pad120672;
    signed char f1d760;
};

signed char hard_LC_COLLATE = 0;

struct s6 {
    signed char[120672] pad120672;
    signed char f1d760;
};

struct s7 {
    signed char[120672] pad120672;
    signed char f1d760;
};

struct s8 {
    signed char[120672] pad120672;
    signed char f1d760;
};

void fun_3c80(void** rdi, void** rsi, void** rdx);

struct s9 {
    signed char[120672] pad120672;
    signed char f1d760;
};

void* getmonth(void** rdi, void** rsi, void** rdx);

void** strnumcmp(void** rdi, void** rsi, int64_t rdx, int64_t rcx);

struct s10 {
    signed char[120672] pad120672;
    signed char f1d760;
};

void fun_37a0(void** rdi, int64_t rsi, void** rdx, int64_t rcx, int64_t r8);

void** xmemcoll0(void** rdi, void** rsi, void** rdx);

void** filenvercmp(void** rdi, void** rsi, void** rdx);

void** g1d370 = reinterpret_cast<void**>(0);

void md5_finish_ctx(void** rdi, void** rsi, void** rdx);

void** keycompare(void** rdi, void** rsi) {
    void*** rsp3;
    void** rbx4;
    void** r11_5;
    void** r15_6;
    void** rbp7;
    void** r10_8;
    void** r13_9;
    void** r14_10;
    void** v11;
    void** rax12;
    void** v13;
    void** v14;
    void** v15;
    void** rax16;
    void** ecx17;
    void** rbx18;
    void** r15_19;
    void** rax20;
    void** v21;
    void** v22;
    void** v23;
    void** v24;
    void** v25;
    void** rdx26;
    void** v27;
    void** v28;
    void** v29;
    void** v30;
    void** rax31;
    void* edx32;
    void** v33;
    void** rax34;
    void** rax35;
    void** rax36;
    void** rax37;
    void** r11d38;
    void** rax39;
    void** r10_40;
    void* rsp41;
    uint32_t eax42;
    void** v43;
    void** rdi44;
    void** v45;
    uint32_t eax46;
    unsigned char v47;
    void** rbp48;
    void** rax49;
    void** rdi50;
    void** rax51;
    void** rax52;
    void** rdi53;
    void** rax54;
    void** rdx55;
    struct s5* rax56;
    void** r8_57;
    void** rcx58;
    int1_t zf59;
    void** rax60;
    void** r10_61;
    void* rsi62;
    void* rsi63;
    void** rax64;
    void* ebx65;
    void** rcx66;
    void* rax67;
    void* rax68;
    void* rax69;
    void* rax70;
    void* rsi71;
    void* r11d72;
    void* ebx73;
    struct s6* rax74;
    uint32_t eax75;
    uint32_t eax76;
    void** rax77;
    void** rdi78;
    void** rax79;
    void* rdi80;
    void* r9_81;
    void** r13_82;
    struct s7* rax83;
    struct s8* rax84;
    void* rsp85;
    void** v86;
    int32_t ebx87;
    void** v88;
    int1_t zf89;
    int1_t below_or_equal90;
    struct s9* rax91;
    struct s9* rdx92;
    void** rax93;
    void* eax94;
    void* eax95;
    void* rdi96;
    void* r9_97;
    int64_t rdx98;
    int64_t rcx99;
    void** eax100;
    struct s10* rax101;
    void* rsp102;
    void** eax103;
    struct s4** rdi104;
    void* rax105;
    signed char al106;
    uint32_t edx107;
    int64_t rax108;
    void* rax109;
    signed char al110;
    int64_t rax111;
    void* eax112;
    void** edx113;
    void** rax114;
    void** rax115;
    void** eax116;
    void* rax117;
    int1_t zf118;
    void** rax119;
    void* rsp120;
    void** rax121;
    void* rsp122;
    void** rsi123;
    void** rax124;
    void** rcx125;
    void** r8_126;
    void* rcx127;
    void** v128;
    void** rdx129;
    void** rax130;
    void* r11d131;
    void** rax132;
    void** rsi133;
    struct s2* rax134;
    void** rdx135;
    void** rdi136;
    void** rax137;
    void** r10_138;
    void** r8_139;
    void** rcx140;
    void** r11d141;
    void* rsp142;
    struct s2* rax143;
    void** rax144;

    rsp3 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1000 - 0x1000 - 0x158);
    rbx4 = keylist;
    r11_5 = *reinterpret_cast<void***>(rsi + 24);
    r15_6 = rbx4;
    rbp7 = *reinterpret_cast<void***>(rsi + 16);
    r10_8 = *reinterpret_cast<void***>(rdi + 24);
    r13_9 = *reinterpret_cast<void***>(rdi + 16);
    r14_10 = r11_5;
    v11 = rdi;
    rax12 = g28;
    v13 = rax12;
    v14 = rsi;
    v15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp3) + reinterpret_cast<uint64_t>("k"));
    goto addr_8ec0_2;
    addr_9501_3:
    rax16 = ecx17;
    *reinterpret_cast<int32_t*>(&rax16 + 4) = 0;
    return rax16;
    while (1) {
        addr_9bf9_4:
        rbx18 = r15_19;
        rax20 = fun_3940(r13_9, r13_9);
        rsp3 = rsp3 - 8 + 8;
        r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_9) + reinterpret_cast<unsigned char>(rax20) + 1);
        v21 = v22;
        while (1) {
            if (reinterpret_cast<unsigned char>(r13_9) < reinterpret_cast<unsigned char>(v23)) 
                goto addr_97d1_6;
            if (reinterpret_cast<unsigned char>(rbp7) >= reinterpret_cast<unsigned char>(v24)) 
                goto addr_9d8d_8;
            addr_97d1_6:
            md5_process_bytes(v21, r14_10, v25);
            rdx26 = v27;
            rsi = v28;
            md5_process_bytes(v29, rsi, rdx26);
            rsp3 = rsp3 - 8 + 8 - 8 + 8;
            if (v30) 
                goto addr_9770_9;
            rdx26 = v28;
            rsi = v29;
            if (reinterpret_cast<unsigned char>(v28) > reinterpret_cast<unsigned char>(r14_10)) {
                rdx26 = r14_10;
            }
            rax31 = fun_3a80(v21, rsi, rdx26, v21, rsi, rdx26);
            rsp3 = rsp3 - 8 + 8;
            v30 = rax31;
            if (rax31) 
                goto addr_9770_9;
            edx32 = reinterpret_cast<void*>(0);
            *reinterpret_cast<unsigned char*>(&edx32) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v28) < reinterpret_cast<unsigned char>(r14_10));
            rdx26 = reinterpret_cast<void**>(reinterpret_cast<int32_t>(edx32) - static_cast<unsigned char>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v28) > reinterpret_cast<unsigned char>(r14_10))));
            *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
            v30 = rdx26;
            if (reinterpret_cast<unsigned char>(v33) <= reinterpret_cast<unsigned char>(rbx18)) 
                goto addr_977b_14; else 
                goto addr_984a_15;
            addr_9c3f_16:
            rbx18 = r15_19;
            fun_3ac0(v29, rbp7, v28);
            rax34 = fun_3940(r13_9, r13_9);
            rsp3 = rsp3 - 8 + 8 - 8 + 8;
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_9) + reinterpret_cast<unsigned char>(rax34) + 1);
            v21 = v22;
            addr_97b2_17:
            rax35 = fun_3940(rbp7, rbp7);
            rsp3 = rsp3 - 8 + 8;
            rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(rax35) + 1);
            continue;
            addr_9c70_18:
            rbx18 = r15_19;
            fun_3ac0(rax36, rbp7, v28);
            rsp3 = rsp3 - 8 + 8;
            v21 = v22;
            goto addr_97b2_17;
            addr_9ce2_19:
            rax37 = fun_3940(r13_9, r13_9);
            rsp3 = rsp3 - 8 + 8;
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_9) + reinterpret_cast<unsigned char>(rax37) + 1);
            if (reinterpret_cast<unsigned char>(rbp7) >= reinterpret_cast<unsigned char>(v24)) 
                continue;
            goto addr_97b2_17;
            while (1) {
                r11d38 = rax39;
                while (1) {
                    v23 = r11d38;
                    v21 = r10_40;
                    fun_3750(v22, rsi, v22, rsi);
                    rsp3 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
                    r10_8 = v21;
                    r11_5 = v23;
                    while (1) {
                        eax42 = *reinterpret_cast<unsigned char*>(&v43);
                        rdi44 = v45;
                        *reinterpret_cast<void***>(r10_8) = *reinterpret_cast<void***>(&eax42);
                        eax46 = v47;
                        *reinterpret_cast<void***>(r14_10) = *reinterpret_cast<void***>(&eax46);
                        if (rdi44) {
                            v45 = r11_5;
                            fun_3750(rdi44, rsi, rdi44, rsi);
                            rsp3 = rsp3 - 8 + 8;
                            r11_5 = v45;
                        }
                        while (!r11_5) {
                            r15_6 = *reinterpret_cast<void***>(r15_6 + 64);
                            if (!r15_6) 
                                goto addr_95f0_27;
                            r13_9 = *reinterpret_cast<void***>(v11);
                            rbp48 = *reinterpret_cast<void***>(v11 + 8);
                            if (*reinterpret_cast<void***>(r15_6 + 16) == 0xffffffffffffffff) {
                                rsi = v14;
                                r10_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_9) + reinterpret_cast<unsigned char>(rbp48) + 0xffffffffffffffff);
                                r14_10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8)) + 0xffffffffffffffff);
                            } else {
                                rax49 = limfield_isra_0(r13_9, rbp48, r15_6, r13_9, rbp48, r15_6);
                                r10_8 = rax49;
                                rsi = *reinterpret_cast<void***>(v14 + 8);
                                rdi50 = *reinterpret_cast<void***>(v14);
                                rax51 = limfield_isra_0(rdi50, rsi, r15_6, rdi50, rsi, r15_6);
                                rsp3 = rsp3 - 8 + 8 - 8 + 8;
                                r14_10 = rax51;
                            }
                            if (*reinterpret_cast<void***>(r15_6) != 0xffffffffffffffff) {
                                rax52 = begfield_isra_0(r13_9, rbp48, r15_6, r13_9, rbp48, r15_6);
                                r13_9 = rax52;
                                rsi = *reinterpret_cast<void***>(v14 + 8);
                                rdi53 = *reinterpret_cast<void***>(v14);
                                rax54 = begfield_isra_0(rdi53, rsi, r15_6, rdi53, rsi, r15_6);
                                rsp3 = rsp3 - 8 + 8 - 8 + 8;
                                rbp7 = rax54;
                                goto addr_8ec0_2;
                            }
                            rbp7 = *reinterpret_cast<void***>(v14);
                            if (!*reinterpret_cast<void***>(r15_6 + 48)) {
                                addr_8ec0_2:
                                rdx55 = *reinterpret_cast<void***>(r15_6 + 40);
                                rbx18 = *reinterpret_cast<void***>(r15_6 + 32);
                                if (reinterpret_cast<unsigned char>(r13_9) >= reinterpret_cast<unsigned char>(r10_8)) {
                                    r10_8 = r13_9;
                                }
                            } else {
                                if (reinterpret_cast<unsigned char>(r10_8) > reinterpret_cast<unsigned char>(r13_9)) {
                                    do {
                                        *reinterpret_cast<uint32_t*>(&rax56) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_9));
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax56) + 4) = 0;
                                        if (!rax56->f1d760) 
                                            break;
                                        ++r13_9;
                                    } while (r10_8 != r13_9);
                                }
                                if (reinterpret_cast<unsigned char>(r14_10) > reinterpret_cast<unsigned char>(rbp7)) 
                                    goto addr_91fd_40; else 
                                    goto addr_91e2_41;
                            }
                            if (reinterpret_cast<unsigned char>(rbp7) >= reinterpret_cast<unsigned char>(r14_10)) {
                                r14_10 = rbp7;
                            }
                            r8_57 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r10_8) - reinterpret_cast<unsigned char>(r13_9));
                            rcx58 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_10) - reinterpret_cast<unsigned char>(rbp7));
                            zf59 = hard_LC_COLLATE == 0;
                            if (!zf59) 
                                goto addr_8eff_45;
                            rax60 = reinterpret_cast<void**>(0xffffffffff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 48)));
                            if (rax60) 
                                goto addr_8eff_45;
                            if (*reinterpret_cast<void***>(r15_6 + 56)) 
                                goto addr_8eff_45;
                            if (!rbx18) {
                                r10_61 = rcx58;
                                if (reinterpret_cast<unsigned char>(r8_57) <= reinterpret_cast<unsigned char>(rcx58)) {
                                    r10_61 = r8_57;
                                }
                                if (r10_61) {
                                    if (rdx55) {
                                        do {
                                            *reinterpret_cast<uint32_t*>(&rsi62) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_9) + reinterpret_cast<unsigned char>(rax60));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi62) + 4) = 0;
                                            *reinterpret_cast<uint32_t*>(&rsi63) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(rax60));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi63) + 4) = 0;
                                            r11_5 = reinterpret_cast<void**>(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx55) + reinterpret_cast<uint64_t>(rsi62)) - *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx55) + reinterpret_cast<uint64_t>(rsi63)));
                                            if (r11_5) 
                                                goto addr_9a4f_54;
                                            ++rax60;
                                        } while (r10_61 != rax60);
                                    } else {
                                        v43 = r8_57;
                                        v45 = rcx58;
                                        rax64 = fun_3a80(r13_9, rbp7, r10_61);
                                        rsp3 = rsp3 - 8 + 8;
                                        rcx58 = v45;
                                        r8_57 = v43;
                                        r11_5 = rax64;
                                        if (rax64) 
                                            goto addr_94c6_57;
                                    }
                                }
                                ebx65 = reinterpret_cast<void*>(0);
                                *reinterpret_cast<unsigned char*>(&ebx65) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_57) > reinterpret_cast<unsigned char>(rcx58));
                                rbx18 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(ebx65) - reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(ebx65) < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_57) < reinterpret_cast<unsigned char>(rcx58)))));
                                *reinterpret_cast<int32_t*>(&rbx18 + 4) = 0;
                                r11_5 = rbx18;
                                continue;
                            }
                            rcx66 = r13_9;
                            if (!rdx55) {
                                while (1) {
                                    if (reinterpret_cast<unsigned char>(r13_9) >= reinterpret_cast<unsigned char>(r10_8) || (*reinterpret_cast<uint32_t*>(&rax67) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_9)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax67) + 4) = 0, !*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<uint64_t>(rax67)))) {
                                        if (reinterpret_cast<unsigned char>(rbp7) >= reinterpret_cast<unsigned char>(r14_10)) 
                                            break;
                                        do {
                                            *reinterpret_cast<uint32_t*>(&rax68) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax68) + 4) = 0;
                                            if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<uint64_t>(rax68))) 
                                                break;
                                            ++rbp7;
                                        } while (r14_10 != rbp7);
                                        goto addr_9d28_64;
                                        if (reinterpret_cast<unsigned char>(r10_8) <= reinterpret_cast<unsigned char>(r13_9)) 
                                            goto addr_9d28_64;
                                        if (reinterpret_cast<unsigned char>(rbp7) >= reinterpret_cast<unsigned char>(r14_10)) 
                                            goto addr_9d28_64;
                                        r11_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_9)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
                                        if (r11_5) 
                                            goto addr_94d0_69;
                                        ++rbp7;
                                    }
                                    ++r13_9;
                                }
                            } else {
                                while (1) {
                                    if (reinterpret_cast<unsigned char>(rcx66) >= reinterpret_cast<unsigned char>(r10_8) || (*reinterpret_cast<uint32_t*>(&rax69) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx66)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0, !*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<uint64_t>(rax69)))) {
                                        if (reinterpret_cast<unsigned char>(rbp7) >= reinterpret_cast<unsigned char>(r14_10)) 
                                            goto addr_93d3_74;
                                        do {
                                            *reinterpret_cast<uint32_t*>(&rax70) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax70) + 4) = 0;
                                            if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<uint64_t>(rax70))) 
                                                break;
                                            ++rbp7;
                                        } while (r14_10 != rbp7);
                                        goto addr_9c98_77;
                                        if (reinterpret_cast<unsigned char>(r10_8) <= reinterpret_cast<unsigned char>(rcx66)) 
                                            goto addr_9c98_77;
                                        if (reinterpret_cast<unsigned char>(r14_10) <= reinterpret_cast<unsigned char>(rbp7)) 
                                            goto addr_9c98_77;
                                        *reinterpret_cast<uint32_t*>(&rsi71) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx66));
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi71) + 4) = 0;
                                        r11_5 = reinterpret_cast<void**>(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx55) + reinterpret_cast<uint64_t>(rsi71)) - *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx55) + reinterpret_cast<uint64_t>(rax70)));
                                        if (r11_5) 
                                            goto addr_94d0_69;
                                        ++rbp7;
                                    }
                                    ++rcx66;
                                }
                            }
                            addr_9d28_64:
                            r11d72 = reinterpret_cast<void*>(0);
                            *reinterpret_cast<unsigned char*>(&r11d72) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r13_9) < reinterpret_cast<unsigned char>(r10_8));
                            r11_5 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(r11d72) - reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(r11d72) < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp7) < reinterpret_cast<unsigned char>(r14_10)))));
                            continue;
                            addr_93d3_74:
                            addr_9c98_77:
                            ebx73 = reinterpret_cast<void*>(0);
                            *reinterpret_cast<unsigned char*>(&ebx73) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rcx66) < reinterpret_cast<unsigned char>(r10_8));
                            rbx18 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(ebx73) - reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(ebx73) < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp7) < reinterpret_cast<unsigned char>(r14_10)))));
                            *reinterpret_cast<int32_t*>(&rbx18 + 4) = 0;
                            r11_5 = rbx18;
                            continue;
                            do {
                                addr_91fd_40:
                                *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                                if (!rax74->f1d760) 
                                    break;
                                ++rbp7;
                            } while (r14_10 != rbp7);
                            goto addr_8ec0_2;
                            goto addr_8ec0_2;
                            addr_91e2_41:
                            goto addr_8ec0_2;
                        }
                        break;
                        addr_8eff_45:
                        eax75 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r10_8));
                        *reinterpret_cast<unsigned char*>(&v43) = *reinterpret_cast<unsigned char*>(&eax75);
                        eax76 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_10));
                        v47 = *reinterpret_cast<unsigned char*>(&eax76);
                        rax77 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx55) | reinterpret_cast<unsigned char>(rbx18));
                        v45 = rax77;
                        if (rax77) {
                            rdi78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx58) + reinterpret_cast<unsigned char>(r8_57) + 2);
                            v45 = reinterpret_cast<void**>(0);
                            rax79 = reinterpret_cast<void**>(rsp3 + 0x200);
                            if (reinterpret_cast<unsigned char>(rdi78) <= reinterpret_cast<unsigned char>(0xfa0)) {
                                rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax79) + reinterpret_cast<unsigned char>(r8_57) + 1);
                                if (!r8_57) 
                                    goto addr_9465_88; else 
                                    goto addr_9260_89;
                            }
                            v23 = r8_57;
                            v21 = rcx58;
                            v22 = rdx55;
                            rax79 = xmalloc(rdi78, rdi78);
                            rsp3 = rsp3 - 8 + 8;
                            r8_57 = v23;
                            rcx58 = v21;
                            v45 = rax79;
                            rdx55 = v22;
                            rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax79) + reinterpret_cast<unsigned char>(r8_57) + 1);
                            if (r8_57) {
                                addr_9260_89:
                                r11_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_9) + reinterpret_cast<unsigned char>(r8_57));
                                *reinterpret_cast<int32_t*>(&r8_57) = 0;
                                *reinterpret_cast<int32_t*>(&r8_57 + 4) = 0;
                            } else {
                                addr_9465_88:
                                r10_8 = rax79;
                                if (rcx58) 
                                    goto addr_92ac_91; else 
                                    goto addr_9471_92;
                            }
                            do {
                                *reinterpret_cast<uint32_t*>(&rdi80) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_9));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi80) + 4) = 0;
                                if (!rbx18 || (*reinterpret_cast<uint32_t*>(&r9_81) = *reinterpret_cast<unsigned char*>(&rdi80), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_81) + 4) = 0, !*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<uint64_t>(r9_81)))) {
                                    if (rdx55) {
                                        *reinterpret_cast<uint32_t*>(&rdi80) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx55) + reinterpret_cast<uint64_t>(rdi80));
                                    }
                                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax79) + reinterpret_cast<unsigned char>(r8_57)) = *reinterpret_cast<unsigned char*>(&rdi80);
                                    ++r8_57;
                                }
                                ++r13_9;
                            } while (r11_5 != r13_9);
                            r10_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax79) + reinterpret_cast<unsigned char>(r8_57));
                            if (!rcx58) {
                                addr_9471_92:
                                rbp7 = rsi;
                                r14_10 = rsi;
                                r13_9 = rax79;
                                goto addr_8f1f_99;
                            } else {
                                addr_92ac_91:
                                r13_82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(rcx58));
                                *reinterpret_cast<int32_t*>(&rcx58) = 0;
                                *reinterpret_cast<int32_t*>(&rcx58 + 4) = 0;
                            }
                        } else {
                            addr_8f1f_99:
                            *reinterpret_cast<void***>(r10_8) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r14_10) = reinterpret_cast<void**>(0);
                            if (*reinterpret_cast<unsigned char*>(r15_6 + 50)) {
                                while (*reinterpret_cast<uint32_t*>(&rax83) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_9)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax83) + 4) = 0, !!rax83->f1d760) {
                                    ++r13_9;
                                }
                                *reinterpret_cast<uint32_t*>(&rax84) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax84) + 4) = 0;
                                if (!rax84->f1d760) 
                                    goto addr_8fb7_103; else 
                                    goto addr_8fa3_104;
                            } else {
                                if (*reinterpret_cast<unsigned char*>(r15_6 + 52)) {
                                    v21 = r10_8;
                                    fun_3c80(r13_9, rsp3 + 0x90, rdx55);
                                    rsp85 = reinterpret_cast<void*>(rsp3 - 8 + 8);
                                    rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp85) + 0x98);
                                    __asm__("fstp tword [rsp+0x20]");
                                    fun_3c80(rbp7, rsi, rdx55);
                                    rsp3 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp85) - 8 + 8);
                                    r10_8 = v21;
                                    __asm__("fld tword [rsp+0x20]");
                                    if (r13_9 == v86) {
                                        __asm__("fstp st0");
                                        __asm__("fstp st0");
                                        ebx87 = 0;
                                        *reinterpret_cast<unsigned char*>(&ebx87) = reinterpret_cast<uint1_t>(v88 != rbp7);
                                        rbx18 = reinterpret_cast<void**>(-ebx87);
                                        *reinterpret_cast<int32_t*>(&rbx18 + 4) = 0;
                                        r11_5 = rbx18;
                                        continue;
                                    }
                                    zf89 = rbp7 == v88;
                                    below_or_equal90 = reinterpret_cast<unsigned char>(rbp7) <= reinterpret_cast<unsigned char>(v88);
                                    if (zf89) 
                                        goto addr_938a_109; else 
                                        goto addr_9342_110;
                                } else {
                                    if (*reinterpret_cast<unsigned char*>(r15_6 + 53)) {
                                        while (*reinterpret_cast<uint32_t*>(&rax91) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_9)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0, rdx92 = rax91, !!rax91->f1d760) {
                                            ++r13_9;
                                        }
                                        *reinterpret_cast<uint32_t*>(&rax93) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7));
                                        *reinterpret_cast<int32_t*>(&rax93 + 4) = 0;
                                        rbx18 = rax93;
                                        if (!*reinterpret_cast<signed char*>(rax93 + 0x1d760)) 
                                            goto addr_90e2_115; else 
                                            goto addr_90c9_116;
                                    } else {
                                        if (*reinterpret_cast<unsigned char*>(r15_6 + 54)) {
                                            v22 = r10_8;
                                            eax94 = getmonth(r13_9, 0, rdx55);
                                            *reinterpret_cast<int32_t*>(&rsi) = 0;
                                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                            eax95 = getmonth(rbp7, 0, rdx55);
                                            rsp3 = rsp3 - 8 + 8 - 8 + 8;
                                            r10_8 = v22;
                                            rbx18 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(eax94) - reinterpret_cast<uint32_t>(eax95));
                                            *reinterpret_cast<int32_t*>(&rbx18 + 4) = 0;
                                            r11_5 = rbx18;
                                            continue;
                                        }
                                        if (*reinterpret_cast<unsigned char*>(r15_6 + 51)) 
                                            goto addr_95f8_120;
                                        if (!*reinterpret_cast<void***>(r15_6 + 56)) 
                                            goto addr_9579_122; else 
                                            goto addr_8f65_123;
                                    }
                                }
                            }
                        }
                        do {
                            *reinterpret_cast<uint32_t*>(&rdi96) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi96) + 4) = 0;
                            if (!rbx18 || (*reinterpret_cast<uint32_t*>(&r9_97) = *reinterpret_cast<unsigned char*>(&rdi96), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_97) + 4) = 0, !*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<uint64_t>(r9_97)))) {
                                if (rdx55) {
                                    *reinterpret_cast<uint32_t*>(&rdi96) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx55) + reinterpret_cast<uint64_t>(rdi96));
                                }
                                *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rcx58)) = *reinterpret_cast<unsigned char*>(&rdi96);
                                ++rcx58;
                            }
                            ++rbp7;
                        } while (r13_82 != rbp7);
                        r14_10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rcx58));
                        rbp7 = rsi;
                        r13_9 = rax79;
                        goto addr_8f1f_99;
                        addr_8fb7_103:
                        *reinterpret_cast<int32_t*>(&rdx98) = reinterpret_cast<signed char>(decimal_point);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx98) + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&rcx99) = thousands_sep;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx99) + 4) = 0;
                        rsi = rbp7;
                        v22 = r10_8;
                        eax100 = strnumcmp(r13_9, rsi, rdx98, rcx99);
                        rsp3 = rsp3 - 8 + 8;
                        r10_8 = v22;
                        r11_5 = eax100;
                        continue;
                        addr_8fa3_104:
                        do {
                            *reinterpret_cast<uint32_t*>(&rax101) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 1));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax101) + 4) = 0;
                            ++rbp7;
                        } while (rax101->f1d760);
                        goto addr_8fb7_103;
                        addr_938a_109:
                        __asm__("fstp st0");
                        __asm__("fstp st0");
                        addr_938e_131:
                        r11_5 = reinterpret_cast<void**>(1);
                        continue;
                        addr_9342_110:
                        __asm__("fxch st0, st1");
                        __asm__("fcomi st0, st1");
                        if (!below_or_equal90) {
                            __asm__("fstp st0");
                            __asm__("fstp st0");
                        } else {
                            __asm__("fxch st0, st1");
                            __asm__("fcomi st0, st1");
                            r11_5 = reinterpret_cast<void**>(1);
                            if (!below_or_equal90) {
                                __asm__("fstp st0");
                                __asm__("fstp st0");
                                continue;
                            }
                            __asm__("fucomi st0, st1");
                            if (__intrinsic()) {
                                __asm__("fxch st0, st1");
                            } else {
                                r11_5 = reinterpret_cast<void**>(0);
                                if (zf89) {
                                    __asm__("fstp st0");
                                    __asm__("fstp st0");
                                    continue;
                                } else {
                                    __asm__("fxch st0, st1");
                                }
                            }
                            __asm__("fucomi st0, st0");
                            if (!__intrinsic()) 
                                goto addr_9d44_141; else 
                                goto addr_937a_142;
                        }
                        addr_9d48_143:
                        r11_5 = reinterpret_cast<void**>(0xffffffff);
                        continue;
                        addr_9d44_141:
                        __asm__("fstp st0");
                        __asm__("fstp st0");
                        goto addr_9d48_143;
                        addr_937a_142:
                        __asm__("fxch st0, st1");
                        __asm__("fucomi st0, st0");
                        if (__intrinsic()) {
                            __asm__("fxch st0, st1");
                            __asm__("fstp tword [rsp+0x30]");
                            rbx18 = reinterpret_cast<void**>(rsp3 + 0xc0);
                            rsp102 = reinterpret_cast<void*>(rsp3 - 16);
                            v22 = r10_8;
                            rbp7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp102) + 0x157);
                            __asm__("fstp tword [rsp]");
                            fun_37a0(rbx18, 0x87, 1, 0x10e, "%Lf");
                            __asm__("fld tword [rsp+0x40]");
                            __asm__("fstp tword [rsp]");
                            fun_37a0(rbp7, 0x87, 1, 0x87, "%Lf");
                            rsi = rbp7;
                            eax103 = fun_3ad0(rbx18, rsi, 1, rbx18, rsi, 1);
                            rsp3 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp102) - 8 + 8 - 16 - 8 + 8 + 32 - 8 + 8);
                            r10_8 = v22;
                            r11_5 = eax103;
                            continue;
                        } else {
                            __asm__("fstp st0");
                            __asm__("fstp st0");
                            goto addr_938e_131;
                        }
                        addr_90e2_115:
                        rdi104 = reinterpret_cast<struct s4**>(rsp3 + 0x98);
                        v21 = r10_8;
                        r11_5 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<uint32_t*>(&rax105) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rdx92) == 45);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&v23) = *reinterpret_cast<unsigned char*>(&rdx92);
                        al106 = traverse_raw_number(rdi104, rsi);
                        if (al106 > 48 && (edx107 = *reinterpret_cast<unsigned char*>(&v23), *reinterpret_cast<uint32_t*>(&rax108) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax105) + reinterpret_cast<unsigned char>(r13_9)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax108) + 4) = 0, r11_5 = reinterpret_cast<void**>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x15360 + rax108))), *reinterpret_cast<signed char*>(&edx107) == 45)) {
                            r11_5 = reinterpret_cast<void**>(-reinterpret_cast<unsigned char>(r11_5));
                        }
                        *reinterpret_cast<int32_t*>(&rax109) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax109) + 4) = 0;
                        v22 = v21;
                        *reinterpret_cast<unsigned char*>(&rax109) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rbx18) == 45);
                        v88 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax109) + reinterpret_cast<unsigned char>(rbp7));
                        al110 = traverse_raw_number(rdi104, rsi);
                        rsp3 = rsp3 - 8 + 8 - 8 + 8;
                        r10_8 = v22;
                        if (al110 > 48) {
                            *reinterpret_cast<uint32_t*>(&rax111) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v88));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax111) + 4) = 0;
                            eax112 = reinterpret_cast<void*>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x15360 + rax111)));
                            edx113 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_5) - reinterpret_cast<uint32_t>(eax112));
                            if (*reinterpret_cast<signed char*>(&rbx18) == 45) {
                                edx113 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(eax112) + reinterpret_cast<unsigned char>(r11_5));
                            }
                            r11_5 = edx113;
                        }
                        if (r11_5) 
                            continue;
                        goto addr_8fb7_103;
                        addr_90c9_116:
                        do {
                            *reinterpret_cast<uint32_t*>(&rax114) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 1));
                            *reinterpret_cast<int32_t*>(&rax114 + 4) = 0;
                            ++rbp7;
                            rbx18 = rax114;
                        } while (*reinterpret_cast<signed char*>(rax114 + 0x1d760));
                        goto addr_90e2_115;
                        addr_9579_122:
                        if (!r8_57) {
                            r11_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_5) - (reinterpret_cast<unsigned char>(r11_5) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r11_5) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(r11_5) + reinterpret_cast<uint1_t>(!!rcx58)))));
                            continue;
                        } else {
                            r11_5 = reinterpret_cast<void**>(1);
                            if (!rcx58) 
                                continue;
                            rsi = r8_57 + 1;
                            v22 = r10_8;
                            rax115 = xmemcoll0(r13_9, rsi, rbp7);
                            rsp3 = rsp3 - 8 + 8;
                            r10_8 = v22;
                            r11_5 = rax115;
                            continue;
                        }
                        addr_8f65_123:
                        rsi = r8_57;
                        v22 = r10_8;
                        eax116 = filenvercmp(r13_9, rsi, rbp7);
                        rsp3 = rsp3 - 8 + 8;
                        r10_8 = v22;
                        r11_5 = eax116;
                    }
                    addr_94d0_69:
                    ecx17 = r11_5;
                    if (*reinterpret_cast<signed char*>(r15_6 + 55)) {
                        if (reinterpret_cast<signed char>(r11_5) < reinterpret_cast<signed char>(0)) {
                            ecx17 = reinterpret_cast<void**>(0xffffffff);
                        } else {
                            ecx17 = reinterpret_cast<void**>(-reinterpret_cast<unsigned char>(r11_5));
                        }
                    }
                    addr_94ea_160:
                    rax117 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
                    if (!rax117) 
                        goto addr_9501_3;
                    fun_3950();
                    rsp3 = rsp3 - 8 + 8;
                    goto addr_9d62_162;
                    addr_95f0_27:
                    ecx17 = r11_5;
                    goto addr_94ea_160;
                    addr_9a4f_54:
                    goto addr_94d0_69;
                    addr_94c6_57:
                    goto addr_94d0_69;
                    addr_95f8_120:
                    __asm__("movdqa xmm8, [rip+0x13cdf]");
                    __asm__("movdqa xmm7, [rip+0x13ce7]");
                    __asm__("movdqa xmm6, [rip+0x13cef]");
                    __asm__("movdqa xmm5, [rip+0x13cf7]");
                    __asm__("movdqa xmm4, [rip+0x13cff]");
                    __asm__("movdqa xmm3, [rip+0x13d07]");
                    __asm__("movups [rsp+0x15c], xmm8");
                    __asm__("movdqa xmm2, [rip+0x13d06]");
                    __asm__("movdqa xmm1, [rip+0x13d0e]");
                    __asm__("movups [rsp+0x16c], xmm7");
                    __asm__("movdqa xmm0, [rip+0x13d0e]");
                    rdx26 = g1d370;
                    __asm__("movups [rsp+0x17c], xmm6");
                    __asm__("movups [rsp+0x18c], xmm5");
                    __asm__("movups [rsp+0x19c], xmm4");
                    __asm__("movups [rsp+0x1ac], xmm3");
                    __asm__("movups [rsp+0x1bc], xmm2");
                    __asm__("movups [rsp+0x1cc], xmm1");
                    __asm__("movups [rsp+0x1dc], xmm0");
                    __asm__("movaps [rsp+0xc0], xmm8");
                    __asm__("movaps [rsp+0xd0], xmm7");
                    __asm__("movaps [rsp+0xe0], xmm6");
                    __asm__("movaps [rsp+0xf0], xmm5");
                    __asm__("movaps [rsp+0x100], xmm4");
                    __asm__("movaps [rsp+0x110], xmm3");
                    __asm__("movaps [rsp+0x120], xmm2");
                    __asm__("movaps [rsp+0x130], xmm1");
                    __asm__("movaps [rsp+0x140], xmm0");
                    zf118 = hard_LC_COLLATE == 0;
                    if (zf118) {
                        rax119 = reinterpret_cast<void**>(rsp3 + 0xc0);
                        v23 = r8_57;
                        rbx18 = reinterpret_cast<void**>(rsp3 + 0xa0);
                        v25 = rax119;
                        md5_process_bytes(r13_9, r8_57, rax119);
                        md5_finish_ctx(v25, rbx18, rax119);
                        rsp120 = reinterpret_cast<void*>(rsp3 - 8 + 8 - 8 + 8);
                        rax121 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp120) + 0x15c);
                        v27 = rax121;
                        md5_process_bytes(rbp7, rcx58, rax121);
                        rsp122 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp120) - 8 + 8);
                        rsi123 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp122) + 0xb0);
                        md5_finish_ctx(v27, rsi123, rax121);
                        rsi = rsi123;
                        rax124 = fun_3a80(rbx18, rsi, 16);
                        rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp122) - 8 + 8 - 8 + 8);
                        rcx125 = rcx58;
                        r8_126 = v23;
                        v22 = reinterpret_cast<void**>(0);
                        r10_40 = r10_8;
                        r11d38 = rax124;
                        if (rax124) 
                            continue;
                    } else {
                        rcx127 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx58) + reinterpret_cast<unsigned char>(r8_57));
                        v23 = r10_8;
                        rbx18 = reinterpret_cast<void**>(0xfa0);
                        *reinterpret_cast<int32_t*>(&rbx18 + 4) = 0;
                        v128 = r15_6;
                        v33 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rcx127) + reinterpret_cast<uint64_t>(rcx127) * 2 + 2);
                        v22 = reinterpret_cast<void**>(0);
                        v21 = v15;
                        v25 = reinterpret_cast<void**>(rsp3 + 0xc0);
                        v27 = reinterpret_cast<void**>(rsp3 + 0x15c);
                        v24 = r14_10;
                        v30 = reinterpret_cast<void**>(0);
                        addr_9770_9:
                        if (reinterpret_cast<unsigned char>(v33) > reinterpret_cast<unsigned char>(rbx18)) 
                            goto addr_984a_15; else 
                            goto addr_977b_14;
                    }
                    addr_9b0f_165:
                    rdx129 = rcx125;
                    rsi = rbp7;
                    if (reinterpret_cast<unsigned char>(r8_126) <= reinterpret_cast<unsigned char>(rcx125)) {
                        rdx129 = r8_126;
                    }
                    v23 = r8_126;
                    rax130 = fun_3a80(r13_9, rsi, rdx129);
                    rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
                    r10_40 = r10_40;
                    r11d38 = rax130;
                    if (rax130) 
                        continue;
                    r11d131 = reinterpret_cast<void*>(0);
                    *reinterpret_cast<unsigned char*>(&r11d131) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v23) > reinterpret_cast<unsigned char>(rcx125));
                    r11d38 = reinterpret_cast<void**>(reinterpret_cast<uint32_t>(r11d131) - reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(r11d131) < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v23) < reinterpret_cast<unsigned char>(rcx125)))));
                    continue;
                    addr_984a_15:
                    rbx18 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx18 + reinterpret_cast<unsigned char>(rbx18) * 2) >> 1);
                    if (reinterpret_cast<unsigned char>(v33) >= reinterpret_cast<unsigned char>(rbx18)) {
                        rbx18 = v33;
                    }
                    fun_3750(v22, rsi, v22, rsi);
                    rax132 = fun_3760(rbx18, rsi, rdx26);
                    rsp3 = rsp3 - 8 + 8 - 8 + 8;
                    v22 = rax132;
                    if (!rax132) {
                        rbx18 = reinterpret_cast<void**>(0xfa0);
                        *reinterpret_cast<int32_t*>(&rbx18 + 4) = 0;
                        v21 = v15;
                    } else {
                        v21 = v22;
                        if (reinterpret_cast<unsigned char>(r13_9) >= reinterpret_cast<unsigned char>(v23)) 
                            goto addr_9786_173; else 
                            goto addr_9892_174;
                    }
                    addr_977b_14:
                    if (reinterpret_cast<unsigned char>(r13_9) < reinterpret_cast<unsigned char>(v23)) {
                        addr_9892_174:
                        rsi133 = r13_9;
                        rax134 = xstrxfrm(v21, rsi133, rbx18);
                        rsp3 = rsp3 - 8 + 8;
                        r14_10 = reinterpret_cast<void**>(&rax134->f1);
                        if (reinterpret_cast<unsigned char>(rbp7) >= reinterpret_cast<unsigned char>(v24)) {
                            v28 = reinterpret_cast<void**>(0);
                            r15_19 = r14_10;
                        } else {
                            if (reinterpret_cast<unsigned char>(rbx18) >= reinterpret_cast<unsigned char>(r14_10)) {
                                addr_9d62_162:
                                rdx135 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx18) - reinterpret_cast<unsigned char>(r14_10));
                                rdi136 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v21) + reinterpret_cast<unsigned char>(r14_10));
                                goto addr_98be_177;
                            } else {
                                *reinterpret_cast<int32_t*>(&rdx135) = 0;
                                *reinterpret_cast<int32_t*>(&rdx135 + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rdi136) = 0;
                                *reinterpret_cast<int32_t*>(&rdi136 + 4) = 0;
                                goto addr_98be_177;
                            }
                        }
                    } else {
                        addr_9786_173:
                        if (reinterpret_cast<unsigned char>(rbp7) < reinterpret_cast<unsigned char>(v24)) {
                            rdi136 = v21;
                            rdx135 = rbx18;
                            *reinterpret_cast<int32_t*>(&r14_10) = 0;
                            *reinterpret_cast<int32_t*>(&r14_10 + 4) = 0;
                            goto addr_98be_177;
                        } else {
                            *reinterpret_cast<int32_t*>(&r14_10) = 0;
                            *reinterpret_cast<int32_t*>(&r14_10 + 4) = 0;
                            v28 = reinterpret_cast<void**>(0);
                            v29 = v21;
                            goto addr_97a7_181;
                        }
                    }
                    addr_98d3_182:
                    if (reinterpret_cast<unsigned char>(rbx18) < reinterpret_cast<unsigned char>(r14_10) || reinterpret_cast<unsigned char>(rbx18) < reinterpret_cast<unsigned char>(r15_19)) {
                        if (reinterpret_cast<unsigned char>(r15_19) <= reinterpret_cast<unsigned char>(0x5555555555555554)) {
                            r15_19 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r15_19 + reinterpret_cast<unsigned char>(r15_19) * 2) >> 1);
                        }
                        fun_3750(v22, rsi133, v22, rsi133);
                        rax137 = xmalloc(r15_19, r15_19);
                        rsp3 = rsp3 - 8 + 8 - 8 + 8;
                        v22 = rax137;
                        if (reinterpret_cast<unsigned char>(r13_9) < reinterpret_cast<unsigned char>(v23)) {
                            fun_3ac0(rax137, r13_9, r14_10);
                            rsp3 = rsp3 - 8 + 8;
                            v29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax137) + reinterpret_cast<unsigned char>(r14_10));
                            if (reinterpret_cast<unsigned char>(rbp7) < reinterpret_cast<unsigned char>(v24)) 
                                goto addr_9c3f_16;
                        } else {
                            rax36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v22) + reinterpret_cast<unsigned char>(r14_10));
                            v29 = rax36;
                            if (reinterpret_cast<unsigned char>(rbp7) < reinterpret_cast<unsigned char>(v24)) 
                                goto addr_9c70_18;
                        }
                        r10_138 = v23;
                        if (reinterpret_cast<unsigned char>(r13_9) < reinterpret_cast<unsigned char>(r10_138)) 
                            goto addr_9bf9_4;
                        r8_139 = r14_10;
                        rcx140 = v28;
                        r14_10 = v24;
                        r15_6 = v128;
                        r11d141 = v30;
                        r13_9 = v22;
                    } else {
                        v29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v21) + reinterpret_cast<unsigned char>(r14_10));
                        if (reinterpret_cast<unsigned char>(r13_9) >= reinterpret_cast<unsigned char>(v23)) 
                            goto addr_97a7_181; else 
                            goto addr_9ce2_19;
                    }
                    addr_9959_191:
                    rbp7 = reinterpret_cast<void**>(rsp3 + 0xa0);
                    v24 = r10_138;
                    v23 = r8_139;
                    md5_process_bytes(r13_9, r8_139, v25);
                    rsp142 = reinterpret_cast<void*>(rsp3 - 8 + 8);
                    rbx18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp142) + 0xb0);
                    md5_finish_ctx(v25, rbp7, v25);
                    md5_process_bytes(v29, rcx140, v27);
                    md5_finish_ctx(v27, rbx18, v27);
                    rsi = rbx18;
                    rax39 = fun_3a80(rbp7, rsi, 16);
                    rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp142) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    rcx125 = rcx140;
                    r8_126 = v23;
                    r10_40 = v24;
                    r11d38 = r11d141;
                    if (rax39) 
                        break;
                    if (r11d38) 
                        continue;
                    rbp7 = v29;
                    goto addr_9b0f_165;
                    addr_97a7_181:
                    if (reinterpret_cast<unsigned char>(rbp7) < reinterpret_cast<unsigned char>(v24)) 
                        goto addr_97b2_17;
                    addr_9d8d_8:
                    r8_139 = r14_10;
                    r10_138 = v23;
                    rcx140 = v28;
                    r15_6 = v128;
                    r14_10 = v24;
                    r11d141 = v30;
                    r13_9 = v21;
                    goto addr_9959_191;
                    addr_98be_177:
                    rsi133 = rbp7;
                    rax143 = xstrxfrm(rdi136, rsi133, rdx135);
                    rsp3 = rsp3 - 8 + 8;
                    rax144 = reinterpret_cast<void**>(&rax143->f1);
                    v28 = rax144;
                    r15_19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax144) + reinterpret_cast<unsigned char>(r14_10));
                    goto addr_98d3_182;
                }
            }
        }
    }
}

void** sequential_sort(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r14_5;
    int32_t r12d6;
    void** rbx7;
    void** rax8;
    void** rax9;
    void** v10;
    void** r13_11;
    uint64_t rax12;
    void** r9_13;
    void** r15_14;
    void** rax15;
    void** rax16;
    void** r12_17;
    void** rbp18;

    r14_5 = rdi;
    r12d6 = ecx;
    rbx7 = rdx;
    if (rsi == 2) {
        rax8 = compare(rdi + 0xffffffffffffffe0, rdi + 0xffffffffffffffc0, rdx);
        if (!*reinterpret_cast<signed char*>(&r12d6)) {
            if (!(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) == 0))) {
                __asm__("movdqu xmm1, [r14-0x20]");
                __asm__("movdqu xmm0, [r14-0x10]");
                __asm__("movdqu xmm6, [r14-0x40]");
                __asm__("movdqu xmm7, [r14-0x30]");
                __asm__("movups [rbx-0x20], xmm1");
                __asm__("movups [rbx-0x10], xmm0");
                __asm__("movups [r14-0x20], xmm6");
                __asm__("movups [r14-0x10], xmm7");
                __asm__("movups [r14-0x40], xmm1");
                __asm__("movups [r14-0x30], xmm0");
            }
        } else {
            rax8 = reinterpret_cast<void**>((static_cast<int64_t>(reinterpret_cast<int32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) == 0)))) - 2)) << 5) + reinterpret_cast<unsigned char>(r14_5));
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movdqu xmm1, [rdx+0x10]");
            __asm__("movups [rbx-0x20], xmm0");
            __asm__("movdqu xmm0, [rax]");
            __asm__("movups [rbx-0x10], xmm1");
            __asm__("movdqu xmm1, [rax+0x10]");
            __asm__("movups [rbx-0x40], xmm0");
            __asm__("movups [rbx-0x30], xmm1");
        }
    } else {
        rax9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) >> 1);
        v10 = rax9;
        r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) - reinterpret_cast<unsigned char>(rax9));
        rax12 = reinterpret_cast<unsigned char>(rax9) << 5;
        r9_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) - rax12);
        if (*reinterpret_cast<signed char*>(&ecx)) {
            r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(-rax12) + reinterpret_cast<unsigned char>(rdx));
            rdx = r15_14;
            sequential_sort(r9_13, r13_11, rdx, 1);
            if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(3)) {
                rdx = rbx7;
                sequential_sort(r14_5, v10, rdx, 0);
            }
        } else {
            sequential_sort(r9_13, r13_11, rdx, 0);
            if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(3)) {
                rdx = rbx7;
                sequential_sort(r14_5, v10, rdx, 1);
                rax15 = rbx7;
                r15_14 = r9_13;
                r14_5 = rax15;
            } else {
                __asm__("movdqu xmm6, [r14-0x20]");
                __asm__("movdqu xmm7, [r14-0x10]");
                rax16 = rbx7;
                r15_14 = r9_13;
                __asm__("movups [rbx-0x20], xmm6");
                __asm__("movups [rbx-0x10], xmm7");
                r14_5 = rax16;
            }
        }
        r12_17 = r14_5 + 0xffffffffffffffe0;
        rbp18 = r15_14 + 0xffffffffffffffe0;
        while (1) {
            rax8 = compare(r12_17, rbp18, rdx, r12_17, rbp18, rdx);
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax8) == 0)) {
                --v10;
                __asm__("movdqu xmm4, [r14-0x20]");
                __asm__("movdqu xmm5, [r14-0x10]");
                __asm__("movups [rbx], xmm4");
                __asm__("movups [rbx+0x10], xmm5");
                if (!v10) 
                    break;
                r12_17 = r12_17 - 32;
            } else {
                __asm__("movdqu xmm2, [r15-0x20]");
                __asm__("movups [rbx], xmm2");
                __asm__("movdqu xmm3, [r15-0x10]");
                __asm__("movups [rbx+0x10], xmm3");
                --r13_11;
                if (!r13_11) 
                    goto addr_a050_17;
                rbp18 = rbp18 - 32;
            }
        }
    }
    return rax8;
    addr_a050_17:
}

unsigned char unique = 0;

signed char stable = 0;

signed char reverse = 0;

void** compare(void** rdi, void** rsi, void** rdx, ...) {
    int1_t zf4;
    void** rax5;
    int1_t zf6;
    int1_t zf7;
    void** rsi8;
    void** rdi9;
    void** rbx10;
    void** rbp11;
    int1_t zf12;
    int1_t zf13;
    void** rdx14;
    void** rdx15;
    void** rsi16;
    uint32_t eax17;
    int1_t zf18;

    zf4 = keylist == 0;
    if (zf4) 
        goto addr_9df1_2;
    rax5 = keycompare(rdi, rsi);
    if (*reinterpret_cast<uint32_t*>(&rax5)) 
        goto addr_9e24_4;
    zf6 = unique == 0;
    if (!zf6) 
        goto addr_9e24_4;
    zf7 = stable == 0;
    if (!zf7) 
        goto addr_9e24_4;
    addr_9df1_2:
    rsi8 = *reinterpret_cast<void***>(rdi + 8);
    rdi9 = *reinterpret_cast<void***>(rdi);
    rbx10 = *reinterpret_cast<void***>(rsi + 8) + 0xffffffffffffffff;
    rbp11 = rsi8 - 1;
    if (rbp11) {
        if (!rbx10) {
            zf12 = reverse == 0;
            *reinterpret_cast<uint32_t*>(&rax5) = 0xffffffff;
            *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
            if (!zf12) {
                addr_9e24_4:
                return rax5;
            }
        } else {
            zf13 = hard_LC_COLLATE == 0;
            if (!zf13) {
                rdx14 = *reinterpret_cast<void***>(rsi);
                rax5 = xmemcoll0(rdi9, rsi8, rdx14);
                goto addr_9e12_12;
            } else {
                rdx15 = rbx10;
                rsi16 = *reinterpret_cast<void***>(rsi);
                if (reinterpret_cast<unsigned char>(rbp11) <= reinterpret_cast<unsigned char>(rbx10)) {
                    rdx15 = rbp11;
                }
                rax5 = fun_3a80(rdi9, rsi16, rdx15);
                if (!*reinterpret_cast<uint32_t*>(&rax5)) {
                    eax17 = 0;
                    *reinterpret_cast<unsigned char*>(&eax17) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp11) > reinterpret_cast<unsigned char>(rbx10));
                    *reinterpret_cast<uint32_t*>(&rax5) = eax17 - reinterpret_cast<uint1_t>(eax17 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp11) < reinterpret_cast<unsigned char>(rbx10))));
                    *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
                    goto addr_9e12_12;
                }
            }
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax5) = *reinterpret_cast<uint32_t*>(&rax5) - (*reinterpret_cast<uint32_t*>(&rax5) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax5) < *reinterpret_cast<uint32_t*>(&rax5) + reinterpret_cast<uint1_t>(!!rbx10)));
        *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
        goto addr_9e12_12;
    }
    *reinterpret_cast<uint32_t*>(&rax5) = 1;
    *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
    goto addr_9e24_4;
    addr_9e12_12:
    zf18 = reverse == 0;
    if (zf18) 
        goto addr_9e24_4;
    if (*reinterpret_cast<int32_t*>(&rax5) >= reinterpret_cast<int32_t>(0)) {
        *reinterpret_cast<uint32_t*>(&rax5) = -*reinterpret_cast<uint32_t*>(&rax5);
        *reinterpret_cast<int32_t*>(&rax5 + 4) = 0;
        goto addr_9e24_4;
    }
}

signed char debug = 0;

void** fun_3bf0(void** rdi, int64_t rsi, void** rdx, void** rcx);

int32_t mbsnwidth(void** rdi);

int32_t fun_39c0(void** rdi, void** rsi);

void fun_3c40(int64_t rdi, void** rsi, void** rdx, ...);

struct s11 {
    signed char[120672] pad120672;
    signed char f1d760;
};

struct s12 {
    signed char[120672] pad120672;
    signed char f1d760;
};

void write_line(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rbp6;
    void* rsp7;
    void** r14_8;
    void** r15_9;
    void** v10;
    void** rbx11;
    void** rax12;
    void** v13;
    int1_t zf14;
    uint32_t eax15;
    void** rax16;
    void** rdx17;
    void** rax18;
    void** rdi19;
    void** rbx20;
    void** rsi21;
    void** v22;
    void** rax23;
    void** v24;
    int32_t eax25;
    void* rsp26;
    uint32_t eax27;
    int1_t cf28;
    uint32_t edx29;
    int32_t eax30;
    void** rax31;
    int32_t eax32;
    void** r13_33;
    unsigned char v34;
    void** eax35;
    void** r8d36;
    void** ecx37;
    void** rdi38;
    void** rsi39;
    void** rsi40;
    uint32_t eax41;
    int32_t eax42;
    int1_t cf43;
    void* rax44;
    int64_t v45;
    void** r13_46;
    void** rbp47;
    void** rbx48;
    void** rax49;
    void** v50;
    void** rax51;
    void* rsp52;
    void** v53;
    void* rax54;
    void** rsi55;
    void** rax56;
    void** rax57;
    void** r13d58;
    int64_t v59;
    void** r12_60;
    void** r15_61;
    uint32_t eax62;
    void** rdi63;
    void** rax64;
    void** r9d65;
    void** rdi66;
    void** rsi67;
    struct s3* rax68;
    void** rdi69;
    int32_t eax70;
    void** rdi71;
    void** eax72;
    int64_t rdi73;
    int32_t v74;
    void** rax75;
    void** rax76;
    int32_t v77;
    void** rax78;
    void** edi79;
    uint32_t tmp32_80;
    void* rax81;
    void** rbx82;
    int32_t eax83;
    void* rsp84;
    void* rax85;
    int32_t eax86;
    void* rax87;
    void** rdi88;
    void** rax89;
    int1_t cf90;
    void** rax91;
    void** rdi92;
    void** rax93;
    void** rdi94;
    void** rax95;
    int1_t zf96;
    int1_t zf97;
    void** r10_98;
    void** rax99;
    uint32_t r15d100;
    struct s11* rax101;
    struct s12* rax102;
    void** v103;
    void** rax104;
    void* rax105;
    struct s4** rdi106;
    signed char al107;
    void** rax108;
    int64_t rcx109;
    void** rax110;
    int32_t ecx111;
    uint32_t eax112;
    void** rax113;
    int32_t eax114;

    r12_5 = rsi;
    rbp6 = rdx;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    r14_8 = *reinterpret_cast<void***>(rdi);
    r15_9 = *reinterpret_cast<void***>(rdi + 8);
    v10 = rdi;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<unsigned char>(r15_9));
    rax12 = g28;
    v13 = rax12;
    if (rdx || (zf14 = debug == 0, zf14)) {
        eax15 = eolchar;
        *reinterpret_cast<void***>(rbx11 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax15);
        rax16 = fun_3bf0(r14_8, 1, r15_9, r12_5);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        if (r15_9 != rax16) {
            addr_83ee_3:
            *reinterpret_cast<int32_t*>(&rdx17) = 5;
            *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
            rax18 = fun_3920();
            rdi19 = rax18;
            sort_die(rdi19, rbp6, 5, rdi19, rbp6, 5);
        } else {
            *reinterpret_cast<void***>(rbx11 + 0xffffffffffffffff) = reinterpret_cast<void**>(0);
            goto addr_7fd3_5;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r14_8) < reinterpret_cast<unsigned char>(rbx11)) 
            goto addr_803c_7;
        goto addr_80a8_9;
    }
    rbx20 = rbp6;
    *reinterpret_cast<int32_t*>(&rsi21) = 0x80000;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    v22 = rdi19;
    rax23 = g28;
    v24 = rax23;
    eax25 = rpl_pipe2();
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8);
    if (eax25 >= 0) {
        eax27 = nmerge;
        cf28 = eax27 + 1 < nprocs;
        if (cf28) {
            rdi19 = reinterpret_cast<void**>(0xffffffff);
            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
            reap(0xffffffff, 0x80000);
            rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
            while ((edx29 = nprocs, !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(edx29) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(edx29 == 0))) && (rdi19 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0, eax30 = reap(0, 0x80000), rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8), !!eax30)) {
            }
        }
        rax31 = fun_37d0();
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
        --rbx20;
        *reinterpret_cast<void***>(rdi19) = g80000;
        r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp26) + 40);
        rbp6 = rax31;
        *reinterpret_cast<void***>(rdi19 + 4) = g80004;
        while (1) {
            rdi19 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
            rdx17 = r12_5;
            rsi21 = reinterpret_cast<void**>(0x1d3a0);
            eax32 = fun_3c00();
            r13_33 = temphead;
            temphead = reinterpret_cast<void**>(0);
            v34 = reinterpret_cast<uint1_t>(eax32 == 0);
            eax35 = fun_3e70();
            rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
            r8d36 = *reinterpret_cast<void***>(rbp6);
            ecx37 = eax35;
            if (eax35) {
                temphead = r13_33;
                if (v34) {
                    addr_8539_25:
                    *reinterpret_cast<int32_t*>(&rdx17) = 0;
                    *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
                    rsi21 = r12_5;
                    rdi19 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                    fun_3c00();
                    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                    ecx37 = ecx37;
                    r8d36 = r8d36;
                    goto addr_84a2_26;
                } else {
                    addr_84a2_26:
                    *reinterpret_cast<void***>(rbp6) = r8d36;
                    if (reinterpret_cast<signed char>(ecx37) >= reinterpret_cast<signed char>(0)) 
                        goto addr_855f_27;
                }
                if (!reinterpret_cast<int1_t>(r8d36 == 11)) 
                    goto addr_85e4_29;
                *reinterpret_cast<void***>(rdi19) = *reinterpret_cast<void***>(rsi21);
                rdi38 = rdi19 + 4;
                rsi39 = rsi21 + 4;
                xnanosleep();
                rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                *reinterpret_cast<void***>(rdi38) = *reinterpret_cast<void***>(rsi39);
                rsi40 = rsi39 + 4;
                __asm__("movapd xmm1, xmm2");
                __asm__("addsd xmm1, xmm2");
                *reinterpret_cast<void***>(rdi38 + 4) = *reinterpret_cast<void***>(rsi40);
                rsi21 = rsi40 + 4;
                do {
                    eax41 = nprocs;
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax41) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax41 == 0)) 
                        break;
                    eax42 = reap(0, rsi21);
                    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                } while (eax42);
                cf43 = reinterpret_cast<unsigned char>(rbx20) < reinterpret_cast<unsigned char>(1);
                --rbx20;
                if (cf43) 
                    goto addr_85e0_42;
            } else {
                if (!v34) 
                    goto addr_8598_44; else 
                    goto addr_8539_25;
            }
        }
    }
    addr_8568_45:
    rax44 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
    if (!rax44) {
        goto v45;
    }
    fun_3950();
    r13_46 = rsi21;
    rbp47 = rdx17;
    rbx48 = rdi19;
    rax49 = g28;
    v50 = rax49;
    rax51 = xnmalloc(r13_46, 8, rdx17, r13_46, 8, rdx17);
    rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    v53 = rax51;
    *reinterpret_cast<void***>(rbp47) = rax51;
    if (r13_46) 
        goto addr_8666_49;
    addr_8727_51:
    while (rax54 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v50) - reinterpret_cast<unsigned char>(g28)), !!rax54) {
        addr_885c_52:
        fun_3950();
        addr_8861_53:
        rsi55 = compress_program;
        quotearg_style(4, rsi55, rdx17, 4, rsi55, rdx17);
        rax56 = fun_3920();
        rdx17 = rax56;
        fun_3c90();
        addr_889c_54:
        rax57 = fun_37d0();
        r13d58 = *reinterpret_cast<void***>(rax57);
        fun_3a40();
        *reinterpret_cast<void***>(rbp47) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax57) = r13d58;
    }
    goto v59;
    addr_8666_49:
    rbp47 = rax51;
    *reinterpret_cast<int32_t*>(&r12_60) = 0;
    *reinterpret_cast<int32_t*>(&r12_60 + 4) = 0;
    do {
        r15_61 = *reinterpret_cast<void***>(rbx48 + 8);
        if (!r15_61 || (eax62 = *reinterpret_cast<unsigned char*>(r15_61 + 12), *reinterpret_cast<signed char*>(&eax62) == 0)) {
            rdi63 = *reinterpret_cast<void***>(rbx48);
            rax64 = stream_open(rdi63, "r", rdx17, rdi63, "r", rdx17);
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            *reinterpret_cast<void***>(rbp47) = rax64;
            if (!rax64) 
                goto addr_8727_51;
        } else {
            if (*reinterpret_cast<signed char*>(&eax62) == 1 && (r9d65 = *reinterpret_cast<void***>(r15_61 + 8), rdi66 = proctab, rsi67 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp52) + 16), rax68 = hash_remove(rdi66, rsi67, rdx17, rdi66, rsi67, rdx17), rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8), !!rax68)) {
                rax68->fc = 2;
                reap(r9d65, rsi67);
                rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            }
            rdi69 = r15_61 + 13;
            eax70 = fun_3cd0(rdi69, rdi69);
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            if (eax70 < 0) 
                break;
            rdi71 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp52) + 16);
            eax72 = pipe_fork(rdi71, 9, rdx17, rdi71, 9, rdx17);
            if (reinterpret_cast<int1_t>(eax72 == 0xffffffff)) 
                goto addr_86f1_62;
            if (!eax72) 
                goto addr_87fa_64;
            *reinterpret_cast<void***>(r15_61 + 8) = eax72;
            register_proc(r15_61, 9, rdx17, r15_61, 9, rdx17);
            fun_3a40();
            fun_3a40();
            *reinterpret_cast<int32_t*>(&rdi73) = v74;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi73) + 4) = 0;
            rax75 = fun_3c20(rdi73, "r", rdx17, rdi73, "r", rdx17);
            rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            if (!rax75) 
                goto addr_889c_54;
            *reinterpret_cast<void***>(rbp47) = rax75;
        }
        ++r12_60;
        rbx48 = rbx48 + 16;
        rbp47 = rbp47 + 8;
    } while (r13_46 != r12_60);
    goto addr_8727_51;
    *reinterpret_cast<void***>(v53 + reinterpret_cast<unsigned char>(r12_60) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_51;
    addr_86f1_62:
    rax76 = fun_37d0();
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax76) == 24)) 
        goto addr_8861_53;
    fun_3a40();
    *reinterpret_cast<void***>(rax76) = reinterpret_cast<void**>(24);
    *reinterpret_cast<void***>(v53 + reinterpret_cast<unsigned char>(r12_60) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_51;
    addr_87fa_64:
    fun_3a40();
    if (eax70) {
        move_fd_part_0(eax70, eax70);
    }
    if (v77 != 1) {
        move_fd_part_0(v77, v77);
    }
    rdx17 = reinterpret_cast<void**>("-d");
    fun_3e20();
    rax78 = fun_37d0();
    edi79 = *reinterpret_cast<void***>(rax78);
    async_safe_die(edi79, "couldn't execute compress program (with -d)", edi79, "couldn't execute compress program (with -d)");
    goto addr_885c_52;
    addr_855f_27:
    if (!ecx37) {
        addr_8598_44:
        fun_3a40();
        rdi19 = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
        fun_3a40();
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
        goto addr_8568_45;
    } else {
        tmp32_80 = nprocs + 1;
        nprocs = tmp32_80;
        goto addr_8568_45;
    }
    addr_85e4_29:
    fun_3a40();
    rdi19 = *reinterpret_cast<void***>(v22 + 4);
    *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
    fun_3a40();
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
    *reinterpret_cast<void***>(rbp6) = r8d36;
    goto addr_8568_45;
    addr_85e0_42:
    r8d36 = *reinterpret_cast<void***>(rbp6);
    goto addr_85e4_29;
    addr_7fd3_5:
    rax81 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28));
    if (rax81) {
        fun_3950();
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
        goto addr_83ee_3;
    } else {
        return;
    }
    addr_80a8_9:
    rbp6 = keylist;
    r13_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<unsigned char>(r15_9) + 0xffffffffffffffff);
    if (!rbp6) {
        while (1) {
            rbx82 = r14_8;
            while (1) {
                eax83 = mbsnwidth(r14_8);
                rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                r15_9 = reinterpret_cast<void**>(static_cast<int64_t>(eax83));
                if (reinterpret_cast<unsigned char>(r14_8) < reinterpret_cast<unsigned char>(rbx82)) {
                    do {
                        ++r14_8;
                        *reinterpret_cast<int32_t*>(&rax85) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax85) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax85) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r14_8 + 0xffffffffffffffff) == 9);
                        r15_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_9) + reinterpret_cast<uint64_t>(rax85));
                    } while (rbx82 != r14_8);
                }
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_33) - reinterpret_cast<unsigned char>(rbx82));
                eax86 = mbsnwidth(rbx82);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
                r14_8 = reinterpret_cast<void**>(static_cast<int64_t>(eax86));
                if (reinterpret_cast<unsigned char>(rbx82) < reinterpret_cast<unsigned char>(r13_33)) {
                    do {
                        ++rbx82;
                        *reinterpret_cast<int32_t*>(&rax87) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax87) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax87) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx82 + 0xffffffffffffffff) == 9);
                        r14_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<uint64_t>(rax87));
                    } while (r13_33 != rbx82);
                }
                rbx11 = r15_9 + 0xffffffffffffffff;
                if (r15_9) {
                    do {
                        rdi88 = stdout;
                        rax89 = *reinterpret_cast<void***>(rdi88 + 40);
                        if (reinterpret_cast<unsigned char>(rax89) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi88 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi) = 32;
                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                            fun_39c0(rdi88, 32);
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                        } else {
                            *reinterpret_cast<void***>(rdi88 + 40) = rax89 + 1;
                            *reinterpret_cast<void***>(rax89) = reinterpret_cast<void**>(32);
                        }
                        cf90 = reinterpret_cast<unsigned char>(rbx11) < reinterpret_cast<unsigned char>(1);
                        --rbx11;
                    } while (!cf90);
                }
                if (!r14_8) {
                    *reinterpret_cast<int32_t*>(&rdx) = 5;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rax91 = fun_3920();
                    rsi = rax91;
                    fun_3c40(1, rsi, 5);
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
                } else {
                    do {
                        rdi92 = stdout;
                        rax93 = *reinterpret_cast<void***>(rdi92 + 40);
                        if (reinterpret_cast<unsigned char>(rax93) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi92 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi) = 95;
                            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                            fun_39c0(rdi92, 95);
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                        } else {
                            *reinterpret_cast<void***>(rdi92 + 40) = rax93 + 1;
                            *reinterpret_cast<void***>(rax93) = reinterpret_cast<void**>(95);
                        }
                        --r14_8;
                    } while (r14_8);
                    rdi94 = stdout;
                    rax95 = *reinterpret_cast<void***>(rdi94 + 40);
                    if (reinterpret_cast<unsigned char>(rax95) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi94 + 48))) 
                        goto addr_83b2_98; else 
                        goto addr_82a7_99;
                }
                addr_82b2_100:
                if (!rbp6) 
                    goto addr_7fd3_5;
                rbp6 = *reinterpret_cast<void***>(rbp6 + 64);
                if (!rbp6) {
                    zf96 = unique == 0;
                    if (!zf96) 
                        goto addr_7fd3_5;
                    zf97 = stable == 0;
                    if (!zf97) 
                        goto addr_7fd3_5;
                    r14_8 = *reinterpret_cast<void***>(v10);
                    rbx82 = r14_8;
                    r13_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v10 + 8)) + 0xffffffffffffffff);
                    continue;
                }
                r14_8 = *reinterpret_cast<void***>(v10);
                r15_9 = *reinterpret_cast<void***>(v10 + 8);
                r13_33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_8) + reinterpret_cast<unsigned char>(r15_9) + 0xffffffffffffffff);
                if (!rbp6) 
                    break;
                addr_80c8_106:
                r10_98 = *reinterpret_cast<void***>(rbp6);
                if (r10_98 != 0xffffffffffffffff) 
                    goto addr_80d6_107;
                rbx82 = r14_8;
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp6 + 16) == 0xffffffffffffffff)) {
                    addr_80ee_109:
                    rdx = rbp6;
                    rsi = r15_9;
                    rax99 = limfield_isra_0(r14_8, rsi, rdx);
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                    r13_33 = rax99;
                }
                if (!reinterpret_cast<int1_t>(r10_98 == 0xffffffffffffffff) || !*reinterpret_cast<void***>(rbp6 + 48)) {
                    addr_810b_112:
                    if (*reinterpret_cast<unsigned char*>(rbp6 + 54)) 
                        goto addr_8125_113;
                    if (!(0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 48)))) 
                        continue; else 
                        goto addr_8125_113;
                } else {
                    addr_8125_113:
                    r15d100 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_33));
                    *reinterpret_cast<void***>(r13_33) = reinterpret_cast<void**>(0);
                    *reinterpret_cast<uint32_t*>(&rax101) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx82));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax101) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d760) + reinterpret_cast<uint64_t>(rax101))) {
                        do {
                            *reinterpret_cast<uint32_t*>(&rax102) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx82 + 1));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax102) + 4) = 0;
                            ++rbx82;
                        } while (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d760) + reinterpret_cast<uint64_t>(rax102)));
                    }
                }
                v103 = rbx82;
                if (reinterpret_cast<unsigned char>(rbx82) > reinterpret_cast<unsigned char>(r13_33)) 
                    goto addr_8310_118;
                if (*reinterpret_cast<unsigned char*>(rbp6 + 54)) {
                    getmonth(rbx82, reinterpret_cast<int64_t>(rsp7) + 24, rdx);
                    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                } else {
                    if (*reinterpret_cast<unsigned char*>(rbp6 + 52)) {
                        fun_3c80(rbx82, reinterpret_cast<int64_t>(rsp7) + 24, rdx);
                        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                        __asm__("fstp st0");
                    } else {
                        if (!(0xff0000ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 48)))) {
                            addr_8310_118:
                            v103 = r13_33;
                        } else {
                            rax104 = rbx82;
                            if (reinterpret_cast<unsigned char>(rbx82) < reinterpret_cast<unsigned char>(r13_33)) {
                                *reinterpret_cast<int32_t*>(&rax105) = 0;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
                                *reinterpret_cast<unsigned char*>(&rax105) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx82) == 45);
                                rax104 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax105) + reinterpret_cast<unsigned char>(rbx82));
                            }
                            rdi106 = reinterpret_cast<struct s4**>(reinterpret_cast<int64_t>(rsp7) + 32);
                            al107 = traverse_raw_number(rdi106, rsi, rdi106, rsi);
                            rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                            if (al107 > 47) {
                                rax108 = rax104;
                                *reinterpret_cast<uint32_t*>(&rcx109) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax108));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx109) + 4) = 0;
                                if (*reinterpret_cast<unsigned char*>(rbp6 + 53)) {
                                    rax108 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax108) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax108) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(0x15360 + rcx109) < 1)))))));
                                }
                                v103 = rax108;
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(r13_33) = *reinterpret_cast<void***>(&r15d100);
                r13_33 = v103;
                continue;
                addr_80d6_107:
                rdx = rbp6;
                rsi = r15_9;
                rax110 = begfield_isra_0(r14_8, rsi, rdx);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                rbx82 = rax110;
                if (*reinterpret_cast<void***>(rbp6 + 16) == 0xffffffffffffffff) 
                    goto addr_810b_112; else 
                    goto addr_80ee_109;
                addr_83b2_98:
                *reinterpret_cast<uint32_t*>(&rsi) = 10;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                fun_39c0(rdi94, 10);
                rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
                goto addr_82b2_100;
                addr_82a7_99:
                rdx = rax95 + 1;
                *reinterpret_cast<void***>(rdi94 + 40) = rdx;
                *reinterpret_cast<void***>(rax95) = reinterpret_cast<void**>(10);
                goto addr_82b2_100;
            }
        }
    } else {
        goto addr_80c8_106;
    }
    addr_809c_132:
    r14_8 = *reinterpret_cast<void***>(v10);
    r15_9 = *reinterpret_cast<void***>(v10 + 8);
    goto addr_80a8_9;
    while (1) {
        addr_8090_133:
        ecx111 = 10;
        eax112 = 10;
        goto addr_8020_134;
        addr_806e_135:
        rax113 = fun_3920();
        *reinterpret_cast<uint32_t*>(&rsi) = 0;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        sort_die(rax113, 0, 5);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
        continue;
        while (*reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<unsigned char*>(&ecx111), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, eax114 = fun_39c0(r12_5, rsi), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), eax114 != -1) {
            while (1) {
                if (rbx11 == r14_8) 
                    goto addr_809c_132;
                addr_803c_7:
                eax112 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_8));
                ++r14_8;
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax112) == 9)) {
                    ecx111 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&eax112));
                    if (rbx11 == r14_8) 
                        goto addr_8090_133;
                    addr_8020_134:
                    rdx = *reinterpret_cast<void***>(r12_5 + 40);
                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 48))) 
                        break;
                } else {
                    rdx = *reinterpret_cast<void***>(r12_5 + 40);
                    ecx111 = 62;
                    eax112 = 62;
                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 48))) 
                        break;
                }
                *reinterpret_cast<void***>(r12_5 + 40) = rdx + 1;
                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax112);
            }
        }
        goto addr_806e_135;
    }
}

void** sort_size = reinterpret_cast<void**>(0);

void zaptemp(void** rdi, void** rsi, void** rdx);

struct s13 {
    signed char[1] pad1;
    void** f1;
};

struct s14 {
    void** f0;
    signed char[47] pad48;
    int64_t f30;
};

void mergefps(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** v7;
    void** v8;
    void** v9;
    void** v10;
    void** v11;
    void** v12;
    void** rax13;
    void** v14;
    void** rax15;
    void** v16;
    void** rax17;
    void** rax18;
    void** rsi19;
    void** v20;
    void** rax21;
    void*** rsp22;
    void** v23;
    void** rbp24;
    void** rax25;
    void** v26;
    void** v27;
    void** rbx28;
    void** v29;
    void** rax30;
    void** rax31;
    void** rdx32;
    void** r12_33;
    void** r13_34;
    void** rdi35;
    void** rax36;
    void** r15_37;
    void** r14_38;
    signed char al39;
    void** rdi40;
    void* rsp41;
    void** rdi42;
    void** rdi43;
    void* r12_44;
    void** rcx45;
    void** rdx46;
    void*** rax47;
    void* rsp48;
    void** r15_49;
    void** rsp50;
    void** rax51;
    void** rax52;
    void*** rsp53;
    void** v54;
    void** rdx55;
    void** rsi56;
    void** rdi57;
    void* rsp58;
    void* rax59;
    void** rbp60;
    void** rax61;
    void** r14_62;
    void** rbp63;
    void* rsp64;
    void** rsi65;
    void** v66;
    void** r12_67;
    void** v68;
    void** rax69;
    void** v70;
    void** v71;
    void** rbx72;
    void** v73;
    void** r13_74;
    void** rbp75;
    void** rsi76;
    uint32_t eax77;
    uint32_t r14d78;
    void** eax79;
    void** eax80;
    int32_t eax81;
    void** rax82;
    int32_t eax83;
    int64_t rax84;
    int64_t v85;
    void** rdi86;
    int32_t eax87;
    int64_t rax88;
    int64_t v89;
    int64_t rax90;
    int64_t v91;
    void** r14_92;
    void** rax93;
    void** v94;
    void** v95;
    void** r13_96;
    void** v97;
    void** rdx98;
    void** rax99;
    void* rsp100;
    void** rbx101;
    void** rax102;
    void* rsp103;
    void** rax104;
    void** rbx105;
    void** rbp106;
    void** rsi107;
    void** rdi108;
    void*** v109;
    int32_t esi110;
    void** rax111;
    void** v112;
    void** v113;
    void** r8_114;
    void** rbx115;
    struct s13* r13_116;
    void** rdx117;
    void** rax118;
    void** rdi119;
    void** rdx120;
    void** rax121;
    void** rsi122;
    void** v123;
    void** r8_124;
    void** rax125;
    void** rdx126;
    void** r10_127;
    void** rax128;
    void** rax129;
    void** rsi130;
    void** rdx131;
    void** r8_132;
    void** v133;
    void** rax134;
    void** v135;
    void** rdx136;
    void** rdi137;
    void** rsi138;
    void* rsp139;
    void* rax140;
    int64_t v141;
    uint64_t r15_142;
    void** r13_143;
    void* rsp144;
    void** r14_145;
    void** r12_146;
    void** rcx147;
    void** v148;
    void** v149;
    void** v150;
    void** rax151;
    void** v152;
    void** v153;
    void** r8_154;
    uint64_t rax155;
    void** r15_156;
    void* r9_157;
    int32_t eax158;
    void** rax159;
    void** rcx160;
    int64_t v161;
    void*** r14_162;
    void** rbp163;
    void** rdi164;
    void** rsi165;
    void** rdi166;
    void** rax167;
    void** rbx168;
    void** rax169;
    void** v170;
    int64_t rax171;
    int32_t ecx172;
    void** rdx173;
    void** rdi174;
    void** v175;
    void** r9_176;
    void** v177;
    void** rax178;
    void** r10_179;
    void** v180;
    void** r15_181;
    void** rbp182;
    void** rbx183;
    void** r12_184;
    void** rax185;
    void** rdi186;
    void** rdi187;
    void** rdi188;
    void** rax189;
    void** r15_190;
    void** r11_191;
    uint32_t r8d192;
    void** r9_193;
    void** rdx194;
    void** v195;
    void** r15_196;
    void** rbx197;
    void** r12_198;
    void** rax199;
    void** r11_200;
    void** r12_201;
    void** v202;
    void** r15_203;
    void** rbp204;
    void** r12_205;
    void** rdi206;
    int1_t cf207;
    void** rdi208;
    void** r12_209;
    void** rdi210;
    void** v211;
    void** r15_212;
    void** rbp213;
    void** r12_214;
    void** rdi215;
    int1_t cf216;
    void** r9_217;
    void** rax218;
    void** rax219;
    void** rdi220;
    void* rax221;
    int64_t v222;
    void** rbp223;
    void** rax224;
    void* rax225;
    void** rdx226;
    void** rax227;
    void** rbx228;
    void* rax229;
    void*** r13_230;
    void*** r15_231;
    void** rbp232;
    void** r14_233;
    void** rdi234;
    void** rax235;
    void** v236;
    void** v237;
    void** v238;
    int1_t zf239;
    void* r15_240;
    void** v241;
    void** r14_242;
    void** rax243;
    void** r9_244;
    void** r14_245;
    void** v246;
    void** r15_247;
    void** r13_248;
    void** rbx249;
    void** rbp250;
    void** rax251;
    signed char al252;
    void*** rax253;
    void*** rsi254;
    void** rcx255;
    void* rax256;
    void** rsi257;
    void** rdi258;
    void* rsp259;
    void** rdi260;
    void** rdi261;
    void* rdx262;
    void** rdi263;
    void** r11_264;
    void* rdx265;
    void* rdi266;
    void** rdi267;
    struct s14* rax268;
    void*** rax269;
    void** r13_270;
    void** rdi271;
    void** rax272;
    void** rax273;
    int1_t zf274;

    v7 = rdx;
    v8 = rcx;
    v9 = r8;
    v10 = r9;
    v11 = rdi;
    v12 = rsi;
    rax13 = g28;
    v14 = rax13;
    rax15 = xnmalloc(rdx, 56, rdx);
    v16 = rax15;
    rax17 = xnmalloc(rdx, 8, rdx);
    rax18 = xnmalloc(rdx, 8, rdx);
    *reinterpret_cast<int32_t*>(&rsi19) = 8;
    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
    v20 = rax18;
    rax21 = xnmalloc(rdx, 8, rdx);
    rsp22 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v23 = reinterpret_cast<void**>(0);
    rbp24 = rax21;
    rax25 = keylist;
    v26 = rax25;
    if (!rdx) 
        goto addr_a86f_2;
    v27 = rax17;
    *reinterpret_cast<int32_t*>(&rbx28) = 0;
    *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
    v29 = rbp24;
    while (1) {
        rax30 = sort_size;
        rcx = v16;
        rax31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax30) / reinterpret_cast<unsigned char>(v7));
        rdx32 = merge_buffer_size;
        if (reinterpret_cast<unsigned char>(rax31) < reinterpret_cast<unsigned char>(rdx32)) {
            rax31 = rdx32;
        }
        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx28) * 8 - reinterpret_cast<unsigned char>(rbx28));
        r12_33 = rcx + reinterpret_cast<unsigned char>(rdx) * 8;
        do {
            r13_34 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(rax31) & 0xffffffffffffffe0) + 32);
            rdi35 = r13_34;
            rax36 = fun_3760(rdi35, rsi19, rdx);
            rsp22 = rsp22 - 8 + 8;
            *reinterpret_cast<void***>(r12_33) = rax36;
            if (rax36) 
                break;
            rax31 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_34) >> 1);
        } while (reinterpret_cast<unsigned char>(r13_34) > reinterpret_cast<unsigned char>(66));
        break;
        *reinterpret_cast<void***>(r12_33 + 24) = r13_34;
        r15_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx28) << 4);
        *reinterpret_cast<void***>(r12_33 + 48) = reinterpret_cast<void**>(0);
        rbp24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx28) * 8);
        *reinterpret_cast<void***>(r12_33 + 40) = reinterpret_cast<void**>(32);
        r13_34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v11) + reinterpret_cast<unsigned char>(r15_37));
        *reinterpret_cast<void***>(r12_33 + 16) = reinterpret_cast<void**>(0);
        rdx = *reinterpret_cast<void***>(r13_34);
        *reinterpret_cast<void***>(r12_33 + 32) = reinterpret_cast<void**>(0);
        r14_38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(rbp24));
        *reinterpret_cast<void***>(r12_33 + 8) = reinterpret_cast<void**>(0);
        rsi19 = *reinterpret_cast<void***>(r14_38);
        al39 = fillbuf(r12_33, rsi19, rdx);
        rsp22 = rsp22 - 8 + 8;
        if (!al39) {
            rsi19 = *reinterpret_cast<void***>(r13_34);
            rdi40 = *reinterpret_cast<void***>(r14_38);
            xfclose(rdi40, rsi19, rdx);
            rsp41 = reinterpret_cast<void*>(rsp22 - 8 + 8);
            if (reinterpret_cast<unsigned char>(v12) > reinterpret_cast<unsigned char>(rbx28)) {
                rdi42 = *reinterpret_cast<void***>(r13_34);
                --v12;
                zaptemp(rdi42, rsi19, rdx);
                rsp41 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
            }
            rdi43 = *reinterpret_cast<void***>(r12_33);
            fun_3750(rdi43, rsi19);
            rsp22 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp41) - 8 + 8);
            rcx = v7;
            r8 = rcx + 0xffffffffffffffff;
            if (reinterpret_cast<unsigned char>(r8) <= reinterpret_cast<unsigned char>(rbx28)) 
                goto addr_a9a1_13;
            if (reinterpret_cast<unsigned char>(r13_34) >= reinterpret_cast<unsigned char>(v10 + reinterpret_cast<unsigned char>(r8) * 8) || reinterpret_cast<unsigned char>(r14_38) >= reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx) << 4) + reinterpret_cast<unsigned char>(v11))) {
                r12_44 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8) - reinterpret_cast<unsigned char>(rbx28));
                fun_3c70(r13_34, reinterpret_cast<unsigned char>(v11) + reinterpret_cast<unsigned char>(r15_37) + 16, reinterpret_cast<uint64_t>(r12_44) << 4);
                rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(rbp24) + 8);
                fun_3c70(r14_38, rsi19, reinterpret_cast<uint64_t>(r12_44) * 8);
                rsp22 = rsp22 - 8 + 8 - 8 + 8;
                r8 = r8;
            } else {
                rcx45 = v10;
                rdx46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v7) * 8 + 0xfffffffffffffff8);
                do {
                    __asm__("movdqu xmm4, [rax+rbp*2+0x10]");
                    rsi19 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rcx45) + reinterpret_cast<unsigned char>(rbp24) + 8);
                    __asm__("movups [rax+rbp*2], xmm4");
                    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rcx45) + reinterpret_cast<unsigned char>(rbp24)) = rsi19;
                    rbp24 = rbp24 + 8;
                } while (rbp24 != rdx46);
            }
            v7 = r8;
        } else {
            rax47 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_33 + 24)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_33)));
            *reinterpret_cast<void***>(v27 + reinterpret_cast<unsigned char>(rbx28) * 8) = reinterpret_cast<void**>(rax47 + 0xffffffffffffffe0);
            rcx = v20;
            *reinterpret_cast<void***>(rcx + reinterpret_cast<unsigned char>(rbx28) * 8) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax47) - (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_33 + 16)) << 5));
            ++rbx28;
            if (reinterpret_cast<unsigned char>(rbx28) >= reinterpret_cast<unsigned char>(v7)) 
                goto addr_a305_21;
        }
    }
    xalloc_die();
    rsp48 = reinterpret_cast<void*>(rsp22 - 8 + 8);
    fun_3950();
    r15_49 = rcx;
    rsp50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp48) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    rax51 = g28;
    rax52 = open_input_files(rdi35, rdx, rsp50);
    rsp53 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp50 - 8) + 8);
    if (reinterpret_cast<unsigned char>(rdx) <= reinterpret_cast<unsigned char>(rax52) || reinterpret_cast<unsigned char>(rax52) > reinterpret_cast<unsigned char>(1)) {
        r9 = v54;
        r8 = r8;
        rcx = r15_49;
        rdx55 = rax52;
        rsi56 = rsi19;
        rdi57 = rdi35;
        mergefps(rdi57, rsi56, rdx55, rcx, r8, r9);
        rsp58 = reinterpret_cast<void*>(rsp53 - 8 + 8);
        rax59 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax51) - reinterpret_cast<unsigned char>(g28));
        if (!rax59) {
            goto v27;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx55) = 5;
        *reinterpret_cast<int32_t*>(&rdx55 + 4) = 0;
        rbp60 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi35) + reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax52) << 4));
        rax61 = fun_3920();
        rdi57 = rax61;
        rsi56 = rbp60;
        sort_die(rdi57, rsi56, 5);
        rsp58 = reinterpret_cast<void*>(rsp53 - 8 + 8 - 8 + 8);
    }
    fun_3950();
    r14_62 = rdi57;
    rbp63 = rsi56;
    rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xe8);
    *reinterpret_cast<uint32_t*>(&rsi65) = nmerge;
    *reinterpret_cast<int32_t*>(&rsi65 + 4) = 0;
    v66 = rdx55;
    r12_67 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp64) + 56);
    v68 = rcx;
    rax69 = g28;
    v70 = rax69;
    if (reinterpret_cast<unsigned char>(rsi65) < reinterpret_cast<unsigned char>(rdx55)) 
        goto addr_ab58_30;
    while (1) {
        addr_ac45_31:
        if (reinterpret_cast<unsigned char>(rbp63) < reinterpret_cast<unsigned char>(v66)) {
            v71 = rbp63;
            rbx72 = rbp63;
            r9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp63) << 4);
            v73 = r14_62;
            r13_74 = v68;
            *reinterpret_cast<int32_t*>(&r12_67) = 0;
            *reinterpret_cast<int32_t*>(&r12_67 + 4) = 0;
            rbp75 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_62) + reinterpret_cast<unsigned char>(r9));
            r15_49 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp64) + 64);
            do {
                addr_aca9_33:
                rsi76 = *reinterpret_cast<void***>(rbp75);
                eax77 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi76) - 45);
                r14d78 = eax77;
                if (!eax77) {
                    r14d78 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi76 + 1));
                }
                if (!r13_74) 
                    goto addr_acd3_36;
                eax79 = fun_3ad0(r13_74, rsi76, rdx55, r13_74, rsi76, rdx55);
                rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8);
                if (eax79) 
                    goto addr_acd3_36;
                if (r14d78) 
                    goto addr_ad16_39;
                addr_acd3_36:
                eax80 = outstat_errno_4;
                if (!eax80) {
                    eax81 = fun_3e50();
                    rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8);
                    if (eax81) {
                        rax82 = fun_37d0();
                        rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8);
                        eax80 = *reinterpret_cast<void***>(rax82);
                        outstat_errno_4 = eax80;
                        goto addr_ace1_42;
                    } else {
                        outstat_errno_4 = reinterpret_cast<void**>(0xffffffff);
                    }
                } else {
                    addr_ace1_42:
                    if (reinterpret_cast<signed char>(eax80) >= reinterpret_cast<signed char>(0)) 
                        break;
                }
                if (!r14d78) {
                    eax83 = fun_3e50();
                    rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8);
                    if (eax83) 
                        goto addr_ac96_46;
                    rax84 = g1d248;
                    if (v85 != rax84) 
                        goto addr_ac96_46;
                } else {
                    rdi86 = *reinterpret_cast<void***>(rbp75);
                    eax87 = fun_3b10(rdi86, r15_49, rdx55);
                    rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8);
                    if (eax87) 
                        goto addr_ac96_46;
                    rax88 = g1d248;
                    if (v89 != rax88) 
                        goto addr_ac96_46;
                }
                rax90 = outstat_3;
                if (v91 != rax90) {
                    addr_ac96_46:
                    ++rbx72;
                    rbp75 = rbp75 + 16;
                    if (rbx72 == v66) 
                        break; else 
                        goto addr_aca9_33;
                } else {
                    addr_ad16_39:
                    r14_92 = r12_67 + 13;
                    if (!r12_67) {
                        rax93 = maybe_create_temp(reinterpret_cast<uint64_t>(rsp64) + 56, 0, rdx55);
                        rcx = v94;
                        r14_92 = rax93 + 13;
                        *reinterpret_cast<int32_t*>(&rdx55) = 1;
                        *reinterpret_cast<int32_t*>(&rdx55 + 4) = 0;
                        r12_67 = rax93;
                        r8 = r14_92;
                        mergefiles(rbp75, 0, 1, rcx, r8);
                        rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8 - 8 + 8);
                    }
                }
                *reinterpret_cast<void***>(rbp75) = r14_92;
                ++rbx72;
                rbp75 = rbp75 + 16;
                *reinterpret_cast<void***>(rbp75 + 0xfffffffffffffff8) = r12_67;
            } while (rbx72 != v66);
            r14_62 = v73;
            rbp63 = v71;
        }
        v95 = rbp63;
        r13_96 = v66;
        v97 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp64) + 48);
        while (1) {
            rdx98 = v97;
            rax99 = open_input_files(r14_62, r13_96, rdx98);
            rsp100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8);
            rbx101 = rax99;
            if (r13_96 == rax99) {
                rax102 = stream_open(v68, "w", rdx98);
                rsp103 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp100) - 8 + 8);
                if (rax102) 
                    goto addr_afa3_57;
                rax104 = fun_37d0();
                rsp100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp103) - 8 + 8);
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax104) == 24)) 
                    break;
                if (reinterpret_cast<unsigned char>(r13_96) <= reinterpret_cast<unsigned char>(2)) 
                    break;
            } else {
                if (reinterpret_cast<unsigned char>(rax99) <= reinterpret_cast<unsigned char>(2)) 
                    goto addr_afe8_61;
            }
            r15_49 = rbx101 + 0xffffffffffffffff;
            rbx105 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp100) + 56);
            rbp106 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_62) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_49) << 4));
            while (rsi107 = *reinterpret_cast<void***>(rbp106), r12_67 = rbp106, rdi108 = v109[reinterpret_cast<unsigned char>(r15_49) * 8], xfclose(rdi108, rsi107, rdx98), esi110 = 0, *reinterpret_cast<unsigned char*>(&esi110) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_49) > reinterpret_cast<unsigned char>(2)), rbp106 = rbp106 - 16, rax111 = maybe_create_temp(rbx105, esi110, rdx98), rsp100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp100) - 8 + 8 - 8 + 8), rax111 == 0) {
                --r15_49;
            }
            r9 = v112;
            rcx = v113;
            r8_114 = rax111 + 13;
            v66 = rax111;
            rbx115 = v95;
            if (reinterpret_cast<unsigned char>(v95) > reinterpret_cast<unsigned char>(r15_49)) {
                rbx115 = r15_49;
            }
            r13_116 = reinterpret_cast<struct s13*>(reinterpret_cast<unsigned char>(r13_96) - reinterpret_cast<unsigned char>(r15_49));
            rbp63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v95) - reinterpret_cast<unsigned char>(rbx115));
            mergefps(r14_62, rbx115, r15_49, rcx, r8_114, r9);
            r8 = r8_114;
            r13_96 = reinterpret_cast<void**>(&r13_116->f1);
            *reinterpret_cast<void***>(r14_62) = r8;
            *reinterpret_cast<void***>(r14_62 + 8) = v66;
            fun_3c70(r14_62 + 16, r12_67, reinterpret_cast<uint64_t>(r13_116) << 4);
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp100) - 8 + 8 - 8 + 8);
            v95 = rbp63 + 1;
        }
        *reinterpret_cast<int32_t*>(&rdx117) = 5;
        *reinterpret_cast<int32_t*>(&rdx117 + 4) = 0;
        rax118 = fun_3920();
        rsi65 = v68;
        rdi119 = rax118;
        sort_die(rdi119, rsi65, 5);
        rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp100) - 8 + 8 - 8 + 8);
        while (1) {
            rdx120 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi119) + reinterpret_cast<unsigned char>(rdx117) + 1 - reinterpret_cast<unsigned char>(rsi65));
            rax121 = maybe_create_temp(r12_67, 0, rdx120);
            rsi122 = rbp63;
            rcx = v123;
            r8_124 = rax121 + 13;
            if (reinterpret_cast<unsigned char>(rdx120) <= reinterpret_cast<unsigned char>(rbp63)) {
                rsi122 = rdx120;
            }
            rax125 = mergefiles(r15_49, rsi122, rdx120, rcx, r8_124);
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8 - 8 + 8);
            rdx126 = rbp63;
            r8 = r8_124;
            if (reinterpret_cast<unsigned char>(rax125) <= reinterpret_cast<unsigned char>(rbp63)) {
                rdx126 = rax125;
            }
            ++r13_96;
            rbx101 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx101) + reinterpret_cast<unsigned char>(rax125));
            *reinterpret_cast<void***>(r10_127) = r8;
            *reinterpret_cast<void***>(r10_127 + 8) = rax121;
            rbp63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp63) - reinterpret_cast<unsigned char>(rdx126));
            r15_49 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx101) << 4) + reinterpret_cast<unsigned char>(r14_62));
            r10_127 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_96) << 4) + reinterpret_cast<unsigned char>(r14_62));
            do {
                rbp63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp63) + reinterpret_cast<unsigned char>(r13_96));
                fun_3c70(r10_127, r15_49, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v66) - reinterpret_cast<unsigned char>(rbx101)) << 4);
                rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rsi65) = nmerge;
                *reinterpret_cast<int32_t*>(&rsi65 + 4) = 0;
                r9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_96) - reinterpret_cast<unsigned char>(rbx101));
                v66 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v66) + reinterpret_cast<unsigned char>(r9));
                rdx55 = v66;
                if (reinterpret_cast<unsigned char>(rsi65) >= reinterpret_cast<unsigned char>(rdx55)) 
                    goto addr_ac45_31;
                addr_ab58_30:
                *reinterpret_cast<int32_t*>(&r13_96) = 0;
                *reinterpret_cast<int32_t*>(&r13_96 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbx101) = 0;
                *reinterpret_cast<int32_t*>(&rbx101 + 4) = 0;
                if (reinterpret_cast<unsigned char>(v66) < reinterpret_cast<unsigned char>(rsi65)) {
                    rdi119 = v66;
                    rax128 = rsi65;
                    r10_127 = r14_62;
                    r15_49 = r14_62;
                    *reinterpret_cast<int32_t*>(&rdx117) = 0;
                    *reinterpret_cast<int32_t*>(&rdx117 + 4) = 0;
                } else {
                    do {
                        rax129 = maybe_create_temp(r12_67, 0, rdx55);
                        rsi130 = rbp63;
                        *reinterpret_cast<uint32_t*>(&rdx131) = nmerge;
                        *reinterpret_cast<int32_t*>(&rdx131 + 4) = 0;
                        r8_132 = rax129 + 13;
                        if (reinterpret_cast<unsigned char>(rdx131) <= reinterpret_cast<unsigned char>(rbp63)) {
                            rsi130 = rdx131;
                        }
                        rax134 = mergefiles(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx101) << 4) + reinterpret_cast<unsigned char>(r14_62), rsi130, rdx131, v133, r8_132);
                        rsp64 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp64) - 8 + 8 - 8 + 8);
                        rdx55 = rbp63;
                        r8 = r8_132;
                        *reinterpret_cast<uint32_t*>(&rsi65) = nmerge;
                        *reinterpret_cast<int32_t*>(&rsi65 + 4) = 0;
                        if (reinterpret_cast<unsigned char>(rax134) <= reinterpret_cast<unsigned char>(rbp63)) {
                            rdx55 = rax134;
                        }
                        rbx101 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx101) + reinterpret_cast<unsigned char>(rax134));
                        rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_96) << 4);
                        ++r13_96;
                        rdi119 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v66) - reinterpret_cast<unsigned char>(rbx101));
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_62) + reinterpret_cast<unsigned char>(rcx)) = r8;
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_62) + reinterpret_cast<unsigned char>(rcx) + 8) = rax129;
                        rbp63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp63) - reinterpret_cast<unsigned char>(rdx55));
                    } while (reinterpret_cast<unsigned char>(rsi65) <= reinterpret_cast<unsigned char>(rdi119));
                    r10_127 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_62) + reinterpret_cast<unsigned char>(rcx) + 16);
                    rdx117 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_96) % reinterpret_cast<unsigned char>(rsi65));
                    r15_49 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx101) << 4) + reinterpret_cast<unsigned char>(r14_62));
                    rax128 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi65) - reinterpret_cast<unsigned char>(rdx117));
                }
            } while (reinterpret_cast<unsigned char>(rdi119) <= reinterpret_cast<unsigned char>(rax128));
        }
    }
    addr_afa3_57:
    r9 = v135;
    rcx = rax102;
    rdx136 = r13_96;
    r8 = v68;
    rdi137 = r14_62;
    rsi138 = v95;
    mergefps(rdi137, rsi138, rdx136, rcx, r8, r9);
    rsp139 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp103) - 8 + 8);
    rax140 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v70) - reinterpret_cast<unsigned char>(g28));
    if (!rax140) {
        goto v141;
    }
    addr_b011_85:
    fun_3950();
    r15_142 = reinterpret_cast<unsigned char>(rsi138) >> 1;
    r13_143 = r8;
    rsp144 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp139) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    r14_145 = *reinterpret_cast<void***>(rcx + 40);
    r12_146 = *reinterpret_cast<void***>(rcx + 48);
    rcx147 = v95;
    v148 = rdx136;
    v149 = r9;
    v150 = rcx147;
    rax151 = g28;
    v152 = rax151;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_145) + reinterpret_cast<unsigned char>(r12_146)) <= 0x1ffff || (v153 = rsi138, reinterpret_cast<unsigned char>(rsi138) <= reinterpret_cast<unsigned char>(1))) {
        addr_b0b7_87:
        r8_154 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi137) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v148) << 5));
        rax155 = reinterpret_cast<unsigned char>(r14_145) << 5;
        r15_156 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi137) - rax155);
        r9_157 = reinterpret_cast<void*>(-rax155);
        if (reinterpret_cast<unsigned char>(r12_146) > reinterpret_cast<unsigned char>(1)) {
            *reinterpret_cast<int32_t*>(&rcx147) = 0;
            *reinterpret_cast<int32_t*>(&rcx147 + 4) = 0;
            rsi138 = r12_146;
            rdx136 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_154) - (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_145) >> 1) << 5));
            sequential_sort(r15_156, rsi138, rdx136, 0);
            r9_157 = r9_157;
            r8_154 = r8_154;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rsi138) = 0;
        *reinterpret_cast<int32_t*>(&rsi138 + 4) = 0;
        rcx147 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp144) + 80);
        rdx136 = reinterpret_cast<void**>(0xb880);
        eax158 = fun_3c60(reinterpret_cast<uint64_t>(rsp144) + 72);
        if (!eax158) {
            rax159 = *reinterpret_cast<void***>(rcx + 40);
            rcx160 = *reinterpret_cast<void***>(rcx + 72);
            sortlines(reinterpret_cast<unsigned char>(rdi137) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax159) << 5), reinterpret_cast<unsigned char>(v153) - r15_142, v148, rcx160, r13_143, v149, v149);
            fun_3e00(v161);
            goto addr_b722_91;
        } else {
            r14_145 = *reinterpret_cast<void***>(rcx + 40);
            r12_146 = *reinterpret_cast<void***>(rcx + 48);
            goto addr_b0b7_87;
        }
    }
    if (reinterpret_cast<unsigned char>(r14_145) > reinterpret_cast<unsigned char>(1)) {
        *reinterpret_cast<int32_t*>(&rcx147) = 0;
        *reinterpret_cast<int32_t*>(&rcx147 + 4) = 0;
        rdx136 = r8_154;
        rsi138 = r14_145;
        sequential_sort(rdi137, rsi138, rdx136, 0);
        r9_157 = r9_157;
    }
    *reinterpret_cast<void***>(rcx) = rdi137;
    r14_162 = reinterpret_cast<void***>(r13_143 + 48);
    *reinterpret_cast<void***>(rcx + 8) = r15_156;
    *reinterpret_cast<void***>(rcx + 16) = r15_156;
    *reinterpret_cast<void***>(rcx + 24) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi137) + (reinterpret_cast<int64_t>(r9_157) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_146) << 5)));
    rbp163 = r13_143 + 8;
    fun_3e80(rbp163, rsi138, rdx136, rcx147);
    rdi164 = *reinterpret_cast<void***>(r13_143);
    rsi165 = rcx;
    heap_insert(rdi164, rsi165, rdx136, rcx147);
    *reinterpret_cast<unsigned char*>(rcx + 84) = 1;
    fun_3a90(r14_162, rsi165, rdx136, rcx147);
    fun_3b80(rbp163, rsi165, rdx136, rcx147);
    while (1) {
        fun_3e80(rbp163, rsi165, rdx136, rcx147);
        while (rdi166 = *reinterpret_cast<void***>(r13_143), rax167 = heap_remove_top(rdi166, rsi165, rdx136, rcx147), rax167 == 0) {
            rsi165 = rbp163;
            fun_38e0(r14_162, rsi165, rdx136, rcx147);
        }
        rbx168 = rax167;
        fun_3b80(rbp163, rsi165, rdx136, rcx147);
        rax169 = rbx168 + 88;
        v170 = rax169;
        fun_3e80(rax169, rsi165, rdx136, rcx147);
        *reinterpret_cast<uint32_t*>(&rax171) = *reinterpret_cast<uint32_t*>(rbx168 + 80);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax171) + 4) = 0;
        *reinterpret_cast<unsigned char*>(rbx168 + 84) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax171)) 
            break;
        ecx172 = static_cast<int32_t>(rax171 + rax171 + 2);
        rdx173 = *reinterpret_cast<void***>(rbx168);
        rdi174 = *reinterpret_cast<void***>(rbx168 + 8);
        v175 = rdx173;
        r9_176 = rdx173;
        v177 = rdi174;
        rsi165 = *reinterpret_cast<void***>(rbx168 + 16);
        rcx147 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(v148) >> *reinterpret_cast<signed char*>(&ecx172)) + 1);
        if (*reinterpret_cast<uint32_t*>(&rax171) == 1) {
            rax178 = rdi174;
            if (rdx173 == rsi165) {
                *reinterpret_cast<int32_t*>(&r10_179) = 0;
                *reinterpret_cast<int32_t*>(&r10_179 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx136) = 0;
                *reinterpret_cast<int32_t*>(&rdx136 + 4) = 0;
            } else {
                v180 = rbp163;
                r15_181 = v149;
                rbp182 = rbx168;
                rbx183 = v150;
                while (*reinterpret_cast<void***>(rbp182 + 24) != rax178) {
                    r12_184 = rcx147 + 0xffffffffffffffff;
                    if (!rcx147) 
                        goto addr_b6d8_106;
                    rax185 = compare(r9_176 + 0xffffffffffffffe0, rax178 + 0xffffffffffffffe0, rdx173);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax185) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax185) == 0))) {
                        rdx173 = rbx183;
                        rsi165 = r15_181;
                        rdi186 = *reinterpret_cast<void***>(rbp182 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp182 + 8) = rdi186;
                        write_unique(rdi186, rsi165, rdx173);
                        r9_176 = *reinterpret_cast<void***>(rbp182);
                        rax178 = *reinterpret_cast<void***>(rbp182 + 8);
                        if (r9_176 == *reinterpret_cast<void***>(rbp182 + 16)) 
                            goto addr_b3f5_109;
                    } else {
                        rdx173 = rbx183;
                        rsi165 = r15_181;
                        rdi187 = *reinterpret_cast<void***>(rbp182) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp182) = rdi187;
                        write_unique(rdi187, rsi165, rdx173);
                        r9_176 = *reinterpret_cast<void***>(rbp182);
                        rax178 = *reinterpret_cast<void***>(rbp182 + 8);
                        if (r9_176 == *reinterpret_cast<void***>(rbp182 + 16)) 
                            goto addr_b3f5_109;
                    }
                    rcx147 = r12_184;
                }
                goto addr_b600_112;
            }
        } else {
            rdx136 = *reinterpret_cast<void***>(rbx168 + 32);
            rdi188 = v175;
            rax189 = v177;
            r15_190 = *reinterpret_cast<void***>(rdx136);
            r11_191 = rdi188;
            if (rdi188 == rsi165) {
                r8d192 = 0;
                *reinterpret_cast<int32_t*>(&r10_179) = 0;
                *reinterpret_cast<int32_t*>(&r10_179 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r9_193) = 0;
                *reinterpret_cast<int32_t*>(&r9_193 + 4) = 0;
                goto addr_b2e8_115;
            } else {
                rdx194 = r15_190;
                v195 = r9_176;
                r15_196 = rbx168;
                rbx197 = rdx194;
                while (*reinterpret_cast<void***>(r15_196 + 24) != rax189) {
                    r12_198 = rcx147 + 0xffffffffffffffff;
                    if (!rcx147) 
                        goto addr_b5c0_119;
                    rbx197 = rbx197 - 32;
                    rax199 = compare(r11_191 + 0xffffffffffffffe0, rax189 + 0xffffffffffffffe0, rdx194);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax199) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax199) == 0))) {
                        r11_191 = *reinterpret_cast<void***>(r15_196);
                        rsi165 = *reinterpret_cast<void***>(r15_196 + 16);
                        __asm__("movdqu xmm0, [rcx-0x20]");
                        rax189 = *reinterpret_cast<void***>(r15_196 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_196 + 8) = rax189;
                        __asm__("movups [rbx], xmm0");
                        __asm__("movdqu xmm1, [rcx-0x10]");
                        __asm__("movups [rbx+0x10], xmm1");
                        if (rsi165 == r11_191) 
                            goto addr_b2b8_122;
                    } else {
                        rsi165 = *reinterpret_cast<void***>(r15_196 + 16);
                        __asm__("movdqu xmm2, [rax-0x20]");
                        r11_191 = *reinterpret_cast<void***>(r15_196) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_196) = r11_191;
                        __asm__("movups [rbx], xmm2");
                        __asm__("movdqu xmm3, [rax-0x10]");
                        rax189 = *reinterpret_cast<void***>(r15_196 + 8);
                        __asm__("movups [rbx+0x10], xmm3");
                        if (rsi165 == r11_191) 
                            goto addr_b2b8_122;
                    }
                    rcx147 = r12_198;
                }
                goto addr_b440_125;
            }
        }
        addr_b418_126:
        r11_200 = *reinterpret_cast<void***>(rbx168 + 40);
        rsi165 = r11_200;
        if (*reinterpret_cast<void***>(rbx168 + 48) != r10_179) {
            addr_b6a8_127:
            r11_200 = rsi165;
            if (rsi165 != rdx136 || (rax178 == *reinterpret_cast<void***>(rbx168 + 24) || !rcx147)) {
                rdi188 = r9_176;
                r8d192 = *reinterpret_cast<unsigned char*>(rbx168 + 84);
                r12_201 = *reinterpret_cast<void***>(rbx168 + 48);
                r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx168 + 8))) >> 5);
            } else {
                v202 = rbp163;
                r15_203 = v149;
                rbp204 = rcx147 + 0xffffffffffffffff;
                r12_205 = v150;
                do {
                    rdi206 = rax178 + 0xffffffffffffffe0;
                    rdx136 = r12_205;
                    rsi165 = r15_203;
                    *reinterpret_cast<void***>(rbx168 + 8) = rdi206;
                    write_unique(rdi206, rsi165, rdx136);
                    rax178 = *reinterpret_cast<void***>(rbx168 + 8);
                    if (rax178 == *reinterpret_cast<void***>(rbx168 + 24)) 
                        break;
                    cf207 = reinterpret_cast<unsigned char>(rbp204) < reinterpret_cast<unsigned char>(1);
                    --rbp204;
                } while (!cf207);
                rbp163 = v202;
                r8d192 = *reinterpret_cast<unsigned char*>(rbx168 + 84);
                rdi188 = *reinterpret_cast<void***>(rbx168);
                r11_200 = *reinterpret_cast<void***>(rbx168 + 40);
                r12_201 = *reinterpret_cast<void***>(rbx168 + 48);
                r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(rax178)) >> 5);
            }
        } else {
            r8d192 = *reinterpret_cast<unsigned char*>(rbx168 + 84);
            rdi188 = r9_176;
            r12_201 = r10_179;
        }
        addr_b301_134:
        *reinterpret_cast<void***>(rbx168 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_200) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v175) - reinterpret_cast<unsigned char>(rdi188)) >> 5));
        *reinterpret_cast<void***>(rbx168 + 48) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_201) - reinterpret_cast<unsigned char>(r10_179));
        if (!*reinterpret_cast<signed char*>(&r8d192)) {
            rsi165 = rbx168;
            queue_check_insert_part_0(r13_143, rsi165, rdx136);
        }
        if (*reinterpret_cast<uint32_t*>(rbx168 + 80) > 1) {
            rdi208 = *reinterpret_cast<void***>(rbx168 + 56) + 88;
            fun_3e80(rdi208, rsi165, rdx136, rcx147);
            rsi165 = *reinterpret_cast<void***>(rbx168 + 56);
            if (!*reinterpret_cast<unsigned char*>(rsi165 + 84)) {
                queue_check_insert_part_0(r13_143, rsi165, rdx136);
                rsi165 = *reinterpret_cast<void***>(rbx168 + 56);
            }
            fun_3b80(rsi165 + 88, rsi165, rdx136, rcx147);
        } else {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx168 + 48)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx168 + 40)))) {
                r12_209 = *reinterpret_cast<void***>(rbx168 + 56);
                fun_3e80(rbp163, rsi165, rdx136, rcx147);
                rdi210 = *reinterpret_cast<void***>(r13_143);
                rsi165 = r12_209;
                heap_insert(rdi210, rsi165, rdx136, rcx147);
                *reinterpret_cast<unsigned char*>(r12_209 + 84) = 1;
                fun_3a90(r14_162, rsi165, rdx136, rcx147);
                fun_3b80(rbp163, rsi165, rdx136, rcx147);
            }
        }
        fun_3b80(v170, rsi165, rdx136, rcx147);
        continue;
        addr_b600_112:
        rbx168 = rbp182;
        rbp163 = v180;
        addr_b608_143:
        r10_179 = *reinterpret_cast<void***>(rbx168 + 48);
        rdx136 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(rax178)) >> 5);
        if (rdx136 != r10_179) {
            rsi165 = *reinterpret_cast<void***>(rbx168 + 40);
            rdx136 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v175) - reinterpret_cast<unsigned char>(r9_176)) >> 5);
            goto addr_b6a8_127;
        } else {
            rdi188 = r9_176;
            if (*reinterpret_cast<void***>(rbx168 + 16) == r9_176 || !rcx147) {
                r8d192 = *reinterpret_cast<unsigned char*>(rbx168 + 84);
                r11_200 = *reinterpret_cast<void***>(rbx168 + 40);
                r12_201 = r10_179;
                goto addr_b301_134;
            } else {
                v211 = rbp163;
                r15_212 = v149;
                rbp213 = rcx147 + 0xffffffffffffffff;
                r12_214 = v150;
                do {
                    rdi215 = rdi188 - 32;
                    rdx136 = r12_214;
                    rsi165 = r15_212;
                    *reinterpret_cast<void***>(rbx168) = rdi215;
                    write_unique(rdi215, rsi165, rdx136);
                    rdi188 = *reinterpret_cast<void***>(rbx168);
                    if (rdi188 == *reinterpret_cast<void***>(rbx168 + 16)) 
                        break;
                    cf216 = reinterpret_cast<unsigned char>(rbp213) < reinterpret_cast<unsigned char>(1);
                    --rbp213;
                } while (!cf216);
                rbp163 = v211;
                r8d192 = *reinterpret_cast<unsigned char*>(rbx168 + 84);
                r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx168 + 8))) >> 5);
                r11_200 = *reinterpret_cast<void***>(rbx168 + 40);
                r12_201 = *reinterpret_cast<void***>(rbx168 + 48);
                goto addr_b301_134;
            }
        }
        addr_b6d8_106:
        rbx168 = rbp182;
        rcx147 = reinterpret_cast<void**>(0xffffffffffffffff);
        rbp163 = v180;
        goto addr_b608_143;
        addr_b3f5_109:
        rbx168 = rbp182;
        rcx147 = r12_184;
        rbp163 = v180;
        rdx136 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v175) - reinterpret_cast<unsigned char>(r9_176)) >> 5);
        r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(rax178)) >> 5);
        goto addr_b418_126;
        addr_b2e8_115:
        r12_201 = *reinterpret_cast<void***>(rbx168 + 48);
        r11_200 = *reinterpret_cast<void***>(rbx168 + 40);
        if (r10_179 == r12_201) 
            goto addr_b2fe_151;
        if (r9_193 != r11_200 || ((r9_217 = *reinterpret_cast<void***>(rbx168 + 24), r9_217 == rax189) || (rsi165 = rcx147 + 0xffffffffffffffff, rcx147 == 0))) {
            addr_b2fe_151:
            *reinterpret_cast<void***>(rdx136) = r15_190;
            goto addr_b301_134;
        } else {
            rax218 = rax189 - 32;
            do {
                __asm__("movdqu xmm6, [rax]");
                r15_190 = r15_190 - 32;
                *reinterpret_cast<void***>(rbx168 + 8) = rax218;
                rcx147 = rax218;
                __asm__("movups [r15], xmm6");
                __asm__("movdqu xmm7, [rax+0x10]");
                __asm__("movups [r15+0x10], xmm7");
                if (rax218 == r9_217) 
                    break;
                --rsi165;
                rax218 = rax218 - 32;
            } while (rsi165 != 0xffffffffffffffff);
            goto addr_b7f7_156;
        }
        r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(rax218)) >> 5);
        goto addr_b2fe_151;
        addr_b7f7_156:
        r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(rcx147)) >> 5);
        goto addr_b2fe_151;
        addr_b440_125:
        rbx168 = r15_196;
        rdi188 = *reinterpret_cast<void***>(rbx168);
        r15_190 = rbx197;
        r8d192 = *reinterpret_cast<unsigned char*>(rbx168 + 84);
        rdx136 = *reinterpret_cast<void***>(rbx168 + 32);
        r9_193 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v195) - reinterpret_cast<unsigned char>(r11_191)) >> 5);
        r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(rax189)) >> 5);
        addr_b46d_158:
        r12_201 = *reinterpret_cast<void***>(rbx168 + 48);
        r11_200 = *reinterpret_cast<void***>(rbx168 + 40);
        if (r12_201 == r10_179) {
            if (rsi165 == rdi188) 
                goto addr_b2fe_151;
            rax219 = rcx147 + 0xffffffffffffffff;
            if (!rcx147) 
                goto addr_b2fe_151;
            rcx147 = rdi188 + 0xffffffffffffffe0;
            do {
                __asm__("movdqu xmm4, [rcx]");
                r15_190 = r15_190 - 32;
                *reinterpret_cast<void***>(rbx168) = rcx147;
                rdi188 = rcx147;
                __asm__("movups [r15], xmm4");
                __asm__("movdqu xmm5, [rcx+0x10]");
                __asm__("movups [r15+0x10], xmm5");
                if (rcx147 == rsi165) 
                    break;
                --rax219;
                rcx147 = rcx147 - 32;
            } while (rax219 != 0xffffffffffffffff);
            goto addr_b2fe_151;
            goto addr_b2fe_151;
        }
        addr_b5c0_119:
        rbx168 = r15_196;
        rdi188 = *reinterpret_cast<void***>(rbx168);
        r15_190 = rbx197;
        r8d192 = *reinterpret_cast<unsigned char*>(rbx168 + 84);
        rcx147 = reinterpret_cast<void**>(0xffffffffffffffff);
        rdx136 = *reinterpret_cast<void***>(rbx168 + 32);
        r9_193 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v195) - reinterpret_cast<unsigned char>(r11_191)) >> 5);
        r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(rax189)) >> 5);
        goto addr_b46d_158;
        addr_b2b8_122:
        rbx168 = r15_196;
        r8d192 = *reinterpret_cast<unsigned char*>(rbx168 + 84);
        r15_190 = rbx197;
        rdx136 = *reinterpret_cast<void***>(rbx168 + 32);
        rdi188 = r11_191;
        rcx147 = r12_198;
        r9_193 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v195) - reinterpret_cast<unsigned char>(r11_191)) >> 5);
        r10_179 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v177) - reinterpret_cast<unsigned char>(rax189)) >> 5);
        goto addr_b2e8_115;
    }
    fun_3b80(v170, rsi165, rdx136, rcx147);
    fun_3e80(rbp163, rsi165, rdx136, rcx147);
    rdi220 = *reinterpret_cast<void***>(r13_143);
    heap_insert(rdi220, rbx168, rdx136, rcx147);
    *reinterpret_cast<unsigned char*>(rbx168 + 84) = 1;
    fun_3a90(r14_162, rbx168, rdx136, rcx147);
    fun_3b80(rbp163, rbx168, rdx136, rcx147);
    addr_b722_91:
    rax221 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v152) - reinterpret_cast<unsigned char>(g28));
    if (rax221) {
        fun_3950();
    } else {
        goto v222;
    }
    addr_afe8_61:
    rbp223 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_62) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax99) << 4));
    *reinterpret_cast<int32_t*>(&rdx136) = 5;
    *reinterpret_cast<int32_t*>(&rdx136 + 4) = 0;
    rax224 = fun_3920();
    rdi137 = rax224;
    rsi138 = rbp223;
    sort_die(rdi137, rsi138, 5);
    rsp139 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp100) - 8 + 8 - 8 + 8);
    goto addr_b011_85;
    addr_a9a1_13:
    r12_33 = v27;
    rbp24 = v29;
    if (!r8) {
        addr_a86f_2:
        rsi19 = v9;
        xfclose(v8, rsi19, rdx);
        fun_3750(v10, rsi19);
        fun_3750(v16, rsi19);
        fun_3750(rbp24, rsi19);
        rdi35 = v20;
        fun_3750(rdi35, rsi19);
        rsp48 = reinterpret_cast<void*>(rsp22 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rax225 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v14) - reinterpret_cast<unsigned char>(g28));
        if (!rax225) {
        }
    } else {
        v7 = r8;
    }
    addr_a30e_171:
    rdx226 = v7;
    *reinterpret_cast<int32_t*>(&rax227) = 0;
    *reinterpret_cast<int32_t*>(&rax227 + 4) = 0;
    do {
        *reinterpret_cast<void***>(rbp24 + reinterpret_cast<unsigned char>(rax227) * 8) = rax227;
        ++rax227;
    } while (rax227 != rdx226);
    if (v7 != 1) {
        *reinterpret_cast<int32_t*>(&rbx228) = 1;
        *reinterpret_cast<int32_t*>(&rbx228 + 4) = 0;
        rdx226 = rbp24;
        while (1) {
            rax229 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx228) * 8);
            r13_230 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx226) + reinterpret_cast<uint64_t>(rax229));
            r15_231 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx226) + reinterpret_cast<uint64_t>(rax229) - 8);
            rbp232 = *r13_230;
            r14_233 = *r15_231;
            rsi19 = *reinterpret_cast<void***>(r12_33 + reinterpret_cast<unsigned char>(rbp232) * 8);
            rdi234 = *reinterpret_cast<void***>(r12_33 + reinterpret_cast<unsigned char>(r14_233) * 8);
            rax235 = compare(rdi234, rsi19, rdx226);
            rsp22 = rsp22 - 8 + 8;
            rdx226 = rdx226;
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax235) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax235) == 0)) {
                ++rbx228;
                if (reinterpret_cast<unsigned char>(rbx228) >= reinterpret_cast<unsigned char>(v7)) 
                    break;
            } else {
                *r15_231 = rbp232;
                *reinterpret_cast<int32_t*>(&rbx228) = 1;
                *reinterpret_cast<int32_t*>(&rbx228 + 4) = 0;
                *r13_230 = r14_233;
            }
        }
        rbp24 = rdx226;
    }
    v236 = reinterpret_cast<void**>(0);
    v237 = reinterpret_cast<void**>(0);
    v238 = reinterpret_cast<void**>(rsp22 + 0x80);
    while (1) {
        zf239 = unique == 0;
        r15_240 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp24)) * 8);
        v241 = *reinterpret_cast<void***>(rbp24);
        rbx28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_33) + reinterpret_cast<uint64_t>(r15_240));
        r14_242 = *reinterpret_cast<void***>(rbx28);
        if (zf239) {
            rdx226 = v9;
            rsi19 = v8;
            write_line(r14_242, rsi19, rdx226, rcx);
            rsp22 = rsp22 - 8 + 8;
            goto addr_a3f6_182;
        }
        if (v237) {
            rsi19 = r14_242;
            rax243 = compare(v237, rsi19, rdx226);
            rsp22 = rsp22 - 8 + 8;
            if (*reinterpret_cast<int32_t*>(&rax243)) {
                rsi19 = v8;
                write_line(v238, rsi19, v9, rcx);
                rsp22 = rsp22 - 8 + 8;
            } else {
                addr_a3f6_182:
                r9_244 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<uint64_t>(r15_240));
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9_244)) < reinterpret_cast<unsigned char>(r14_242)) {
                    *reinterpret_cast<void***>(rbx28) = r14_242 - 32;
                    if (!reinterpret_cast<int1_t>(v7 == 1)) {
                        addr_a479_187:
                        r14_245 = *reinterpret_cast<void***>(rbx28);
                        v246 = rbp24;
                        *reinterpret_cast<int32_t*>(&r15_247) = 1;
                        *reinterpret_cast<int32_t*>(&r15_247 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r13_248) = 1;
                        *reinterpret_cast<int32_t*>(&r13_248 + 4) = 0;
                        rbx249 = v7;
                    } else {
                        addr_a523_188:
                        *reinterpret_cast<void***>(rbp24) = v241;
                        continue;
                    }
                    while (1) {
                        rbp250 = *reinterpret_cast<void***>(v246 + reinterpret_cast<unsigned char>(r15_247) * 8);
                        rsi19 = *reinterpret_cast<void***>(r12_33 + reinterpret_cast<unsigned char>(rbp250) * 8);
                        rax251 = compare(r14_245, rsi19, rdx226);
                        rsp22 = rsp22 - 8 + 8;
                        if (*reinterpret_cast<int32_t*>(&rax251) < 0 || !*reinterpret_cast<int32_t*>(&rax251) && reinterpret_cast<unsigned char>(v241) < reinterpret_cast<unsigned char>(rbp250)) {
                            rbx249 = r15_247;
                            r15_247 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_248) + reinterpret_cast<unsigned char>(rbx249)) >> 1);
                            if (reinterpret_cast<unsigned char>(r13_248) >= reinterpret_cast<unsigned char>(rbx249)) 
                                break;
                        } else {
                            r13_248 = r15_247 + 1;
                            r15_247 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_248) + reinterpret_cast<unsigned char>(rbx249)) >> 1);
                            if (reinterpret_cast<unsigned char>(r13_248) >= reinterpret_cast<unsigned char>(rbx249)) 
                                break;
                        }
                    }
                    rbp24 = v246;
                    rdx226 = r13_248 + 0xffffffffffffffff;
                    r8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx226) * 8);
                    if (rdx226) {
                        rsi19 = rbp24 + 8;
                        rdx226 = r8;
                        fun_3c70(rbp24, rsi19, rdx226);
                        rsp22 = rsp22 - 8 + 8;
                    }
                    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rbp24) + reinterpret_cast<unsigned char>(r8)) = v241;
                    continue;
                } else {
                    r15_37 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r15_240) + reinterpret_cast<unsigned char>(v10));
                    rsi19 = *reinterpret_cast<void***>(r15_37);
                    rcx = v16;
                    r14_38 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v241) << 4) + reinterpret_cast<unsigned char>(v11));
                    rdx = *reinterpret_cast<void***>(r14_38);
                    r13_34 = rcx + (reinterpret_cast<unsigned char>(v241) * 8 - reinterpret_cast<unsigned char>(v241)) * 8;
                    al252 = fillbuf(r13_34, rsi19, rdx);
                    rsp22 = rsp22 - 8 + 8;
                    r9 = r9_244;
                    if (!al252) {
                        rax253 = reinterpret_cast<void***>(rbp24 + 8);
                        rsi254 = reinterpret_cast<void***>(rbp24 + reinterpret_cast<unsigned char>(v7) * 8);
                        if (v7 != 1) {
                            rcx255 = v241;
                            do {
                                rdx = *rax253;
                                if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rcx255)) {
                                    --rdx;
                                    *rax253 = rdx;
                                    rcx255 = *reinterpret_cast<void***>(rbp24);
                                }
                                rax253 = rax253 + 8;
                            } while (rax253 != rsi254);
                            rax256 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx255) * 8);
                            v241 = rcx255;
                            r14_38 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx255) << 4) + reinterpret_cast<unsigned char>(v11));
                            r15_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<uint64_t>(rax256));
                            r13_34 = v16 + (reinterpret_cast<uint64_t>(rax256) - reinterpret_cast<unsigned char>(rcx255)) * 8;
                        }
                        rsi257 = *reinterpret_cast<void***>(r14_38);
                        rdi258 = *reinterpret_cast<void***>(r15_37);
                        v27 = v7 - 1;
                        xfclose(rdi258, rsi257, rdx);
                        rsp259 = reinterpret_cast<void*>(rsp22 - 8 + 8);
                        if (reinterpret_cast<unsigned char>(v12) > reinterpret_cast<unsigned char>(v241)) {
                            rdi260 = *reinterpret_cast<void***>(r14_38);
                            --v12;
                            zaptemp(rdi260, rsi257, rdx);
                            rsp259 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp259) - 8 + 8);
                        }
                        rdi261 = *reinterpret_cast<void***>(r13_34);
                        fun_3750(rdi261, rsi257);
                        rsp22 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp259) - 8 + 8);
                        rcx = v241;
                        if (reinterpret_cast<unsigned char>(v27) > reinterpret_cast<unsigned char>(rcx)) {
                            rcx = v11;
                            rdx262 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) * 8);
                            rbx28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v241) * 8);
                            r10_127 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v241) << 4);
                            rdi263 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(rbx28));
                            r11_264 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(r10_127));
                            if (reinterpret_cast<unsigned char>(rdi263) >= reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v27) << 4) + reinterpret_cast<unsigned char>(rcx)) || reinterpret_cast<unsigned char>(r11_264) >= reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v10) + reinterpret_cast<uint64_t>(rdx262))) {
                                r14_38 = rbx28 + 8;
                                r9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(v241));
                                r13_34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9) * 8);
                                fun_3c70(rdi263, reinterpret_cast<unsigned char>(v10) + reinterpret_cast<unsigned char>(r14_38), r13_34);
                                r10_127 = r10_127;
                                fun_3c70(r11_264, reinterpret_cast<unsigned char>(v11) + reinterpret_cast<unsigned char>(r10_127) + 16, reinterpret_cast<unsigned char>(r9) << 4);
                                rdx265 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(v241));
                                rdi266 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v241) * 8 - reinterpret_cast<unsigned char>(v241) << 3);
                                fun_3c70(reinterpret_cast<uint64_t>(rdi266) + reinterpret_cast<unsigned char>(v16), reinterpret_cast<unsigned char>(v16) + reinterpret_cast<uint64_t>(rdi266) + 56, (reinterpret_cast<uint64_t>(rdx265) * 8 - reinterpret_cast<uint64_t>(rdx265)) * 8 + 0xffffffffffffffc8);
                                fun_3c70(reinterpret_cast<unsigned char>(r12_33) + reinterpret_cast<unsigned char>(rbx28), reinterpret_cast<unsigned char>(r12_33) + reinterpret_cast<unsigned char>(r14_38), r13_34);
                                rdx = r13_34;
                                fun_3c70(reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(rbx28), reinterpret_cast<unsigned char>(v20) + reinterpret_cast<unsigned char>(r14_38), rdx);
                                rsp22 = rsp22 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                            } else {
                                rdx = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx262) - 8);
                                rdi267 = v10;
                                rax268 = reinterpret_cast<struct s14*>(v16 + (reinterpret_cast<unsigned char>(rbx28) - reinterpret_cast<unsigned char>(v241)) * 8);
                                rcx = v20;
                                do {
                                    __asm__("movdqu xmm0, [rsi+rbx*2+0x10]");
                                    ++rax268;
                                    __asm__("movdqu xmm1, [rax]");
                                    __asm__("movdqu xmm2, [rax+0x10]");
                                    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rdi267) + reinterpret_cast<unsigned char>(rbx28)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rdi267) + reinterpret_cast<unsigned char>(rbx28) + 8);
                                    __asm__("movdqu xmm3, [rax+0x20]");
                                    __asm__("movups [rsi+rbx*2], xmm0");
                                    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rax268) - 8) = rax268->f30;
                                    __asm__("movups [rax-0x38], xmm1");
                                    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r12_33) + reinterpret_cast<unsigned char>(rbx28)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r12_33) + reinterpret_cast<unsigned char>(rbx28) + 8);
                                    r8 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(rbx28) + 8);
                                    __asm__("movups [rax-0x28], xmm2");
                                    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<unsigned char>(rbx28)) = r8;
                                    rbx28 = rbx28 + 8;
                                    __asm__("movups [rax-0x18], xmm3");
                                } while (rdx != rbx28);
                            }
                        }
                        if (!v27) 
                            break;
                        rsi19 = rbp24 + 8;
                        rdx226 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v27) * 8);
                        fun_3c70(rbp24, rsi19, rdx226);
                        rsp22 = rsp22 - 8 + 8;
                        v7 = v27;
                        continue;
                    }
                    rax269 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_34 + 24)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_34)));
                    *reinterpret_cast<void***>(rbx28) = reinterpret_cast<void**>(rax269 + 0xffffffffffffffe0);
                    rdx226 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_34 + 16)) << 5);
                    *reinterpret_cast<void***>(r9) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax269) - reinterpret_cast<unsigned char>(rdx226));
                    if (v7 == 1) 
                        goto addr_a523_188; else 
                        goto addr_a479_187;
                }
            }
        }
        r13_270 = *reinterpret_cast<void***>(r14_242 + 8);
        rdi271 = v23;
        if (reinterpret_cast<unsigned char>(r13_270) > reinterpret_cast<unsigned char>(v236)) {
            rax272 = v236;
            do {
                if (!rax272) 
                    break;
                rax272 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax272) + reinterpret_cast<unsigned char>(rax272));
            } while (reinterpret_cast<unsigned char>(r13_270) > reinterpret_cast<unsigned char>(rax272));
            goto addr_a963_217;
        } else {
            addr_a57b_218:
            rsi19 = *reinterpret_cast<void***>(r14_242);
            rdx226 = r13_270;
            fun_3b30(rdi271, rsi19, rdx226);
            rsp22 = rsp22 - 8 + 8;
            v237 = v238;
            if (v26) {
                goto addr_a3f6_182;
            }
        }
        addr_a8ed_220:
        fun_3750(rdi271, rsi19, rdi271, rsi19);
        rax273 = xmalloc(r13_270, r13_270);
        rsp22 = rsp22 - 8 + 8 - 8 + 8;
        v236 = r13_270;
        r13_270 = *reinterpret_cast<void***>(r14_242 + 8);
        v23 = rax273;
        rdi271 = rax273;
        goto addr_a57b_218;
        addr_a963_217:
        r13_270 = rax272;
        goto addr_a8ed_220;
    }
    if (v237 && (zf274 = unique == 0, !zf274)) {
        rdx = v9;
        write_line(rsp22 + 0x80, v8, rdx, rcx);
        fun_3750(v23, v8);
        rsp22 = rsp22 - 8 + 8 - 8 + 8;
        goto addr_a86f_2;
    }
    addr_a305_21:
    r12_33 = v27;
    rbp24 = v29;
    goto addr_a30e_171;
}

struct s15 {
    signed char[1] pad1;
    void** f1;
};

void** mergefiles(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rsp7;
    void** rax8;
    void** rax9;
    void*** rsp10;
    void** r9_11;
    void** v12;
    void** rdx13;
    void** rsi14;
    void** rdi15;
    void* rsp16;
    void* rax17;
    void** rbp18;
    void** rax19;
    void** r14_20;
    void** rbp21;
    void* rsp22;
    void** rsi23;
    void** v24;
    void** r12_25;
    void** v26;
    void** rax27;
    void** v28;
    void** v29;
    void** rbx30;
    void** v31;
    void** r13_32;
    void** rbp33;
    void** rsi34;
    uint32_t eax35;
    uint32_t r14d36;
    void** eax37;
    void** eax38;
    int32_t eax39;
    void** rax40;
    int32_t eax41;
    int64_t rax42;
    int64_t v43;
    void** rdi44;
    int32_t eax45;
    int64_t rax46;
    int64_t v47;
    int64_t rax48;
    int64_t v49;
    void** r14_50;
    void** rax51;
    void** v52;
    void** v53;
    void** r13_54;
    void** v55;
    void** rdx56;
    void** rax57;
    void* rsp58;
    void** rbx59;
    void** rax60;
    void* rsp61;
    void** rax62;
    void** rbx63;
    void** rbp64;
    void** rsi65;
    void** rdi66;
    void*** v67;
    int32_t esi68;
    void** rax69;
    void** v70;
    void** v71;
    void** r8_72;
    void** rbx73;
    struct s15* r13_74;
    void** rdx75;
    void** rax76;
    void** rdi77;
    void** rdx78;
    void** rax79;
    void** rsi80;
    void** v81;
    void** r8_82;
    void** rax83;
    void** rdx84;
    void** r10_85;
    void** rax86;
    void** rax87;
    void** rsi88;
    void** rdx89;
    void** r8_90;
    void** v91;
    void** rax92;
    void** v93;
    void** rdx94;
    void** rdi95;
    void** rsi96;
    void* rsp97;
    void* rax98;
    int64_t v99;
    uint64_t r15_100;
    void** r13_101;
    void* rsp102;
    void** r14_103;
    void** r12_104;
    void** rcx105;
    void** v106;
    void** v107;
    void** v108;
    void** rax109;
    void** v110;
    void** v111;
    void** r8_112;
    uint64_t rax113;
    void** r15_114;
    void* r9_115;
    int32_t eax116;
    void** rax117;
    void** rcx118;
    int64_t v119;
    void*** r14_120;
    void** rbp121;
    void** rdi122;
    void** rsi123;
    void** rdi124;
    void** rax125;
    void** rbx126;
    void** rax127;
    void** v128;
    int64_t rax129;
    int32_t ecx130;
    void** rdx131;
    void** rdi132;
    void** v133;
    void** r9_134;
    void** v135;
    void** rax136;
    void** r10_137;
    void** v138;
    void** r15_139;
    void** rbp140;
    void** rbx141;
    void** r12_142;
    void** rax143;
    void** rdi144;
    void** rdi145;
    void** rdi146;
    void** rax147;
    void** r15_148;
    void** r11_149;
    uint32_t r8d150;
    void** r9_151;
    void** rdx152;
    void** v153;
    void** r15_154;
    void** rbx155;
    void** r12_156;
    void** rax157;
    void** r11_158;
    void** r12_159;
    void** v160;
    void** r15_161;
    void** rbp162;
    void** r12_163;
    void** rdi164;
    int1_t cf165;
    void** rdi166;
    void** r12_167;
    void** rdi168;
    void** v169;
    void** r15_170;
    void** rbp171;
    void** r12_172;
    void** rdi173;
    int1_t cf174;
    void** r9_175;
    void** rax176;
    void** rax177;
    void** rdi178;
    void* rax179;
    int64_t v180;
    void** rbp181;
    void** rax182;

    r15_6 = rcx;
    rsp7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 24);
    rax8 = g28;
    rax9 = open_input_files(rdi, rdx, rsp7);
    rsp10 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8);
    if (reinterpret_cast<unsigned char>(rdx) <= reinterpret_cast<unsigned char>(rax9) || reinterpret_cast<unsigned char>(rax9) > reinterpret_cast<unsigned char>(1)) {
        r9_11 = v12;
        r8 = r8;
        rcx = r15_6;
        rdx13 = rax9;
        rsi14 = rsi;
        rdi15 = rdi;
        mergefps(rdi15, rsi14, rdx13, rcx, r8, r9_11);
        rsp16 = reinterpret_cast<void*>(rsp10 - 8 + 8);
        rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
        if (!rax17) {
            return rax9;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx13) = 5;
        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
        rbp18 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax9) << 4));
        rax19 = fun_3920();
        rdi15 = rax19;
        rsi14 = rbp18;
        sort_die(rdi15, rsi14, 5);
        rsp16 = reinterpret_cast<void*>(rsp10 - 8 + 8 - 8 + 8);
    }
    fun_3950();
    r14_20 = rdi15;
    rbp21 = rsi14;
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp16) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xe8);
    *reinterpret_cast<uint32_t*>(&rsi23) = nmerge;
    *reinterpret_cast<int32_t*>(&rsi23 + 4) = 0;
    v24 = rdx13;
    r12_25 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22) + 56);
    v26 = rcx;
    rax27 = g28;
    v28 = rax27;
    if (reinterpret_cast<unsigned char>(rsi23) < reinterpret_cast<unsigned char>(rdx13)) 
        goto addr_ab58_7;
    while (1) {
        addr_ac45_8:
        if (reinterpret_cast<unsigned char>(rbp21) < reinterpret_cast<unsigned char>(v24)) {
            v29 = rbp21;
            rbx30 = rbp21;
            r9_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp21) << 4);
            v31 = r14_20;
            r13_32 = v26;
            *reinterpret_cast<int32_t*>(&r12_25) = 0;
            *reinterpret_cast<int32_t*>(&r12_25 + 4) = 0;
            rbp33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<unsigned char>(r9_11));
            r15_6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22) + 64);
            do {
                addr_aca9_10:
                rsi34 = *reinterpret_cast<void***>(rbp33);
                eax35 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rsi34) - 45);
                r14d36 = eax35;
                if (!eax35) {
                    r14d36 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi34 + 1));
                }
                if (!r13_32) 
                    goto addr_acd3_13;
                eax37 = fun_3ad0(r13_32, rsi34, rdx13);
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                if (eax37) 
                    goto addr_acd3_13;
                if (r14d36) 
                    goto addr_ad16_16;
                addr_acd3_13:
                eax38 = outstat_errno_4;
                if (!eax38) {
                    eax39 = fun_3e50();
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                    if (eax39) {
                        rax40 = fun_37d0();
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                        eax38 = *reinterpret_cast<void***>(rax40);
                        outstat_errno_4 = eax38;
                        goto addr_ace1_19;
                    } else {
                        outstat_errno_4 = reinterpret_cast<void**>(0xffffffff);
                    }
                } else {
                    addr_ace1_19:
                    if (reinterpret_cast<signed char>(eax38) >= reinterpret_cast<signed char>(0)) 
                        break;
                }
                if (!r14d36) {
                    eax41 = fun_3e50();
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                    if (eax41) 
                        goto addr_ac96_23;
                    rax42 = g1d248;
                    if (v43 != rax42) 
                        goto addr_ac96_23;
                } else {
                    rdi44 = *reinterpret_cast<void***>(rbp33);
                    eax45 = fun_3b10(rdi44, r15_6, rdx13);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                    if (eax45) 
                        goto addr_ac96_23;
                    rax46 = g1d248;
                    if (v47 != rax46) 
                        goto addr_ac96_23;
                }
                rax48 = outstat_3;
                if (v49 != rax48) {
                    addr_ac96_23:
                    ++rbx30;
                    rbp33 = rbp33 + 16;
                    if (rbx30 == v24) 
                        break; else 
                        goto addr_aca9_10;
                } else {
                    addr_ad16_16:
                    r14_50 = r12_25 + 13;
                    if (!r12_25) {
                        rax51 = maybe_create_temp(reinterpret_cast<uint64_t>(rsp22) + 56, 0, rdx13);
                        rcx = v52;
                        r14_50 = rax51 + 13;
                        *reinterpret_cast<int32_t*>(&rdx13) = 1;
                        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                        r12_25 = rax51;
                        r8 = r14_50;
                        mergefiles(rbp33, 0, 1, rcx, r8);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 + 8);
                    }
                }
                *reinterpret_cast<void***>(rbp33) = r14_50;
                ++rbx30;
                rbp33 = rbp33 + 16;
                *reinterpret_cast<void***>(rbp33 + 0xfffffffffffffff8) = r12_25;
            } while (rbx30 != v24);
            r14_20 = v31;
            rbp21 = v29;
        }
        v53 = rbp21;
        r13_54 = v24;
        v55 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp22) + 48);
        while (1) {
            rdx56 = v55;
            rax57 = open_input_files(r14_20, r13_54, rdx56);
            rsp58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
            rbx59 = rax57;
            if (r13_54 == rax57) {
                rax60 = stream_open(v26, "w", rdx56);
                rsp61 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8);
                if (rax60) 
                    goto addr_afa3_34;
                rax62 = fun_37d0();
                rsp58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp61) - 8 + 8);
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax62) == 24)) 
                    break;
                if (reinterpret_cast<unsigned char>(r13_54) <= reinterpret_cast<unsigned char>(2)) 
                    break;
            } else {
                if (reinterpret_cast<unsigned char>(rax57) <= reinterpret_cast<unsigned char>(2)) 
                    goto addr_afe8_38;
            }
            r15_6 = rbx59 + 0xffffffffffffffff;
            rbx63 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp58) + 56);
            rbp64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_6) << 4));
            while (rsi65 = *reinterpret_cast<void***>(rbp64), r12_25 = rbp64, rdi66 = v67[reinterpret_cast<unsigned char>(r15_6) * 8], xfclose(rdi66, rsi65, rdx56), esi68 = 0, *reinterpret_cast<unsigned char*>(&esi68) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r15_6) > reinterpret_cast<unsigned char>(2)), rbp64 = rbp64 - 16, rax69 = maybe_create_temp(rbx63, esi68, rdx56), rsp58 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 + 8), rax69 == 0) {
                --r15_6;
            }
            r9_11 = v70;
            rcx = v71;
            r8_72 = rax69 + 13;
            v24 = rax69;
            rbx73 = v53;
            if (reinterpret_cast<unsigned char>(v53) > reinterpret_cast<unsigned char>(r15_6)) {
                rbx73 = r15_6;
            }
            r13_74 = reinterpret_cast<struct s15*>(reinterpret_cast<unsigned char>(r13_54) - reinterpret_cast<unsigned char>(r15_6));
            rbp21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v53) - reinterpret_cast<unsigned char>(rbx73));
            mergefps(r14_20, rbx73, r15_6, rcx, r8_72, r9_11);
            r8 = r8_72;
            r13_54 = reinterpret_cast<void**>(&r13_74->f1);
            *reinterpret_cast<void***>(r14_20) = r8;
            *reinterpret_cast<void***>(r14_20 + 8) = v24;
            fun_3c70(r14_20 + 16, r12_25, reinterpret_cast<uint64_t>(r13_74) << 4);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 + 8);
            v53 = rbp21 + 1;
        }
        *reinterpret_cast<int32_t*>(&rdx75) = 5;
        *reinterpret_cast<int32_t*>(&rdx75 + 4) = 0;
        rax76 = fun_3920();
        rsi23 = v26;
        rdi77 = rax76;
        sort_die(rdi77, rsi23, 5);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 + 8);
        while (1) {
            rdx78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi77) + reinterpret_cast<unsigned char>(rdx75) + 1 - reinterpret_cast<unsigned char>(rsi23));
            rax79 = maybe_create_temp(r12_25, 0, rdx78);
            rsi80 = rbp21;
            rcx = v81;
            r8_82 = rax79 + 13;
            if (reinterpret_cast<unsigned char>(rdx78) <= reinterpret_cast<unsigned char>(rbp21)) {
                rsi80 = rdx78;
            }
            rax83 = mergefiles(r15_6, rsi80, rdx78, rcx, r8_82);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 + 8);
            rdx84 = rbp21;
            r8 = r8_82;
            if (reinterpret_cast<unsigned char>(rax83) <= reinterpret_cast<unsigned char>(rbp21)) {
                rdx84 = rax83;
            }
            ++r13_54;
            rbx59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx59) + reinterpret_cast<unsigned char>(rax83));
            *reinterpret_cast<void***>(r10_85) = r8;
            *reinterpret_cast<void***>(r10_85 + 8) = rax79;
            rbp21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp21) - reinterpret_cast<unsigned char>(rdx84));
            r15_6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx59) << 4) + reinterpret_cast<unsigned char>(r14_20));
            r10_85 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_54) << 4) + reinterpret_cast<unsigned char>(r14_20));
            do {
                rbp21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp21) + reinterpret_cast<unsigned char>(r13_54));
                fun_3c70(r10_85, r15_6, reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(rbx59)) << 4);
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rsi23) = nmerge;
                *reinterpret_cast<int32_t*>(&rsi23 + 4) = 0;
                r9_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_54) - reinterpret_cast<unsigned char>(rbx59));
                v24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v24) + reinterpret_cast<unsigned char>(r9_11));
                rdx13 = v24;
                if (reinterpret_cast<unsigned char>(rsi23) >= reinterpret_cast<unsigned char>(rdx13)) 
                    goto addr_ac45_8;
                addr_ab58_7:
                *reinterpret_cast<int32_t*>(&r13_54) = 0;
                *reinterpret_cast<int32_t*>(&r13_54 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbx59) = 0;
                *reinterpret_cast<int32_t*>(&rbx59 + 4) = 0;
                if (reinterpret_cast<unsigned char>(v24) < reinterpret_cast<unsigned char>(rsi23)) {
                    rdi77 = v24;
                    rax86 = rsi23;
                    r10_85 = r14_20;
                    r15_6 = r14_20;
                    *reinterpret_cast<int32_t*>(&rdx75) = 0;
                    *reinterpret_cast<int32_t*>(&rdx75 + 4) = 0;
                } else {
                    do {
                        rax87 = maybe_create_temp(r12_25, 0, rdx13);
                        rsi88 = rbp21;
                        *reinterpret_cast<uint32_t*>(&rdx89) = nmerge;
                        *reinterpret_cast<int32_t*>(&rdx89 + 4) = 0;
                        r8_90 = rax87 + 13;
                        if (reinterpret_cast<unsigned char>(rdx89) <= reinterpret_cast<unsigned char>(rbp21)) {
                            rsi88 = rdx89;
                        }
                        rax92 = mergefiles(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx59) << 4) + reinterpret_cast<unsigned char>(r14_20), rsi88, rdx89, v91, r8_90);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp22) - 8 + 8 - 8 + 8);
                        rdx13 = rbp21;
                        r8 = r8_90;
                        *reinterpret_cast<uint32_t*>(&rsi23) = nmerge;
                        *reinterpret_cast<int32_t*>(&rsi23 + 4) = 0;
                        if (reinterpret_cast<unsigned char>(rax92) <= reinterpret_cast<unsigned char>(rbp21)) {
                            rdx13 = rax92;
                        }
                        rbx59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx59) + reinterpret_cast<unsigned char>(rax92));
                        rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_54) << 4);
                        ++r13_54;
                        rdi77 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(rbx59));
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<unsigned char>(rcx)) = r8;
                        *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<unsigned char>(rcx) + 8) = rax87;
                        rbp21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp21) - reinterpret_cast<unsigned char>(rdx13));
                    } while (reinterpret_cast<unsigned char>(rsi23) <= reinterpret_cast<unsigned char>(rdi77));
                    r10_85 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<unsigned char>(rcx) + 16);
                    rdx75 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_54) % reinterpret_cast<unsigned char>(rsi23));
                    r15_6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx59) << 4) + reinterpret_cast<unsigned char>(r14_20));
                    rax86 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi23) - reinterpret_cast<unsigned char>(rdx75));
                }
            } while (reinterpret_cast<unsigned char>(rdi77) <= reinterpret_cast<unsigned char>(rax86));
        }
    }
    addr_afa3_34:
    r9_11 = v93;
    rcx = rax60;
    rdx94 = r13_54;
    r8 = v26;
    rdi95 = r14_20;
    rsi96 = v53;
    mergefps(rdi95, rsi96, rdx94, rcx, r8, r9_11);
    rsp97 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp61) - 8 + 8);
    rax98 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v28) - reinterpret_cast<unsigned char>(g28));
    if (!rax98) {
        goto v99;
    }
    addr_b011_62:
    fun_3950();
    r15_100 = reinterpret_cast<unsigned char>(rsi96) >> 1;
    r13_101 = r8;
    rsp102 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp97) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x98);
    r14_103 = *reinterpret_cast<void***>(rcx + 40);
    r12_104 = *reinterpret_cast<void***>(rcx + 48);
    rcx105 = v53;
    v106 = rdx94;
    v107 = r9_11;
    v108 = rcx105;
    rax109 = g28;
    v110 = rax109;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_103) + reinterpret_cast<unsigned char>(r12_104)) <= 0x1ffff || (v111 = rsi96, reinterpret_cast<unsigned char>(rsi96) <= reinterpret_cast<unsigned char>(1))) {
        addr_b0b7_64:
        r8_112 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi95) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v106) << 5));
        rax113 = reinterpret_cast<unsigned char>(r14_103) << 5;
        r15_114 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi95) - rax113);
        r9_115 = reinterpret_cast<void*>(-rax113);
        if (reinterpret_cast<unsigned char>(r12_104) > reinterpret_cast<unsigned char>(1)) {
            *reinterpret_cast<int32_t*>(&rcx105) = 0;
            *reinterpret_cast<int32_t*>(&rcx105 + 4) = 0;
            rsi96 = r12_104;
            rdx94 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_112) - (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_103) >> 1) << 5));
            sequential_sort(r15_114, rsi96, rdx94, 0);
            r9_115 = r9_115;
            r8_112 = r8_112;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rsi96) = 0;
        *reinterpret_cast<int32_t*>(&rsi96 + 4) = 0;
        rcx105 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp102) + 80);
        rdx94 = reinterpret_cast<void**>(0xb880);
        eax116 = fun_3c60(reinterpret_cast<uint64_t>(rsp102) + 72);
        if (!eax116) {
            rax117 = *reinterpret_cast<void***>(rcx + 40);
            rcx118 = *reinterpret_cast<void***>(rcx + 72);
            sortlines(reinterpret_cast<unsigned char>(rdi95) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax117) << 5), reinterpret_cast<unsigned char>(v111) - r15_100, v106, rcx118, r13_101, v107, v107);
            fun_3e00(v119);
            goto addr_b722_68;
        } else {
            r14_103 = *reinterpret_cast<void***>(rcx + 40);
            r12_104 = *reinterpret_cast<void***>(rcx + 48);
            goto addr_b0b7_64;
        }
    }
    if (reinterpret_cast<unsigned char>(r14_103) > reinterpret_cast<unsigned char>(1)) {
        *reinterpret_cast<int32_t*>(&rcx105) = 0;
        *reinterpret_cast<int32_t*>(&rcx105 + 4) = 0;
        rdx94 = r8_112;
        rsi96 = r14_103;
        sequential_sort(rdi95, rsi96, rdx94, 0);
        r9_115 = r9_115;
    }
    *reinterpret_cast<void***>(rcx) = rdi95;
    r14_120 = reinterpret_cast<void***>(r13_101 + 48);
    *reinterpret_cast<void***>(rcx + 8) = r15_114;
    *reinterpret_cast<void***>(rcx + 16) = r15_114;
    *reinterpret_cast<void***>(rcx + 24) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi95) + (reinterpret_cast<int64_t>(r9_115) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_104) << 5)));
    rbp121 = r13_101 + 8;
    fun_3e80(rbp121, rsi96, rdx94, rcx105);
    rdi122 = *reinterpret_cast<void***>(r13_101);
    rsi123 = rcx;
    heap_insert(rdi122, rsi123, rdx94, rcx105);
    *reinterpret_cast<unsigned char*>(rcx + 84) = 1;
    fun_3a90(r14_120, rsi123, rdx94, rcx105);
    fun_3b80(rbp121, rsi123, rdx94, rcx105);
    while (1) {
        fun_3e80(rbp121, rsi123, rdx94, rcx105);
        while (rdi124 = *reinterpret_cast<void***>(r13_101), rax125 = heap_remove_top(rdi124, rsi123, rdx94, rcx105), rax125 == 0) {
            rsi123 = rbp121;
            fun_38e0(r14_120, rsi123, rdx94, rcx105);
        }
        rbx126 = rax125;
        fun_3b80(rbp121, rsi123, rdx94, rcx105);
        rax127 = rbx126 + 88;
        v128 = rax127;
        fun_3e80(rax127, rsi123, rdx94, rcx105);
        *reinterpret_cast<uint32_t*>(&rax129) = *reinterpret_cast<uint32_t*>(rbx126 + 80);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax129) + 4) = 0;
        *reinterpret_cast<unsigned char*>(rbx126 + 84) = 0;
        if (!*reinterpret_cast<uint32_t*>(&rax129)) 
            break;
        ecx130 = static_cast<int32_t>(rax129 + rax129 + 2);
        rdx131 = *reinterpret_cast<void***>(rbx126);
        rdi132 = *reinterpret_cast<void***>(rbx126 + 8);
        v133 = rdx131;
        r9_134 = rdx131;
        v135 = rdi132;
        rsi123 = *reinterpret_cast<void***>(rbx126 + 16);
        rcx105 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(v106) >> *reinterpret_cast<signed char*>(&ecx130)) + 1);
        if (*reinterpret_cast<uint32_t*>(&rax129) == 1) {
            rax136 = rdi132;
            if (rdx131 == rsi123) {
                *reinterpret_cast<int32_t*>(&r10_137) = 0;
                *reinterpret_cast<int32_t*>(&r10_137 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdx94) = 0;
                *reinterpret_cast<int32_t*>(&rdx94 + 4) = 0;
            } else {
                v138 = rbp121;
                r15_139 = v107;
                rbp140 = rbx126;
                rbx141 = v108;
                while (*reinterpret_cast<void***>(rbp140 + 24) != rax136) {
                    r12_142 = rcx105 + 0xffffffffffffffff;
                    if (!rcx105) 
                        goto addr_b6d8_83;
                    rax143 = compare(r9_134 + 0xffffffffffffffe0, rax136 + 0xffffffffffffffe0, rdx131);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax143) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax143) == 0))) {
                        rdx131 = rbx141;
                        rsi123 = r15_139;
                        rdi144 = *reinterpret_cast<void***>(rbp140 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp140 + 8) = rdi144;
                        write_unique(rdi144, rsi123, rdx131);
                        r9_134 = *reinterpret_cast<void***>(rbp140);
                        rax136 = *reinterpret_cast<void***>(rbp140 + 8);
                        if (r9_134 == *reinterpret_cast<void***>(rbp140 + 16)) 
                            goto addr_b3f5_86;
                    } else {
                        rdx131 = rbx141;
                        rsi123 = r15_139;
                        rdi145 = *reinterpret_cast<void***>(rbp140) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(rbp140) = rdi145;
                        write_unique(rdi145, rsi123, rdx131);
                        r9_134 = *reinterpret_cast<void***>(rbp140);
                        rax136 = *reinterpret_cast<void***>(rbp140 + 8);
                        if (r9_134 == *reinterpret_cast<void***>(rbp140 + 16)) 
                            goto addr_b3f5_86;
                    }
                    rcx105 = r12_142;
                }
                goto addr_b600_89;
            }
        } else {
            rdx94 = *reinterpret_cast<void***>(rbx126 + 32);
            rdi146 = v133;
            rax147 = v135;
            r15_148 = *reinterpret_cast<void***>(rdx94);
            r11_149 = rdi146;
            if (rdi146 == rsi123) {
                r8d150 = 0;
                *reinterpret_cast<int32_t*>(&r10_137) = 0;
                *reinterpret_cast<int32_t*>(&r10_137 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r9_151) = 0;
                *reinterpret_cast<int32_t*>(&r9_151 + 4) = 0;
                goto addr_b2e8_92;
            } else {
                rdx152 = r15_148;
                v153 = r9_134;
                r15_154 = rbx126;
                rbx155 = rdx152;
                while (*reinterpret_cast<void***>(r15_154 + 24) != rax147) {
                    r12_156 = rcx105 + 0xffffffffffffffff;
                    if (!rcx105) 
                        goto addr_b5c0_96;
                    rbx155 = rbx155 - 32;
                    rax157 = compare(r11_149 + 0xffffffffffffffe0, rax147 + 0xffffffffffffffe0, rdx152);
                    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax157) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax157) == 0))) {
                        r11_149 = *reinterpret_cast<void***>(r15_154);
                        rsi123 = *reinterpret_cast<void***>(r15_154 + 16);
                        __asm__("movdqu xmm0, [rcx-0x20]");
                        rax147 = *reinterpret_cast<void***>(r15_154 + 8) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_154 + 8) = rax147;
                        __asm__("movups [rbx], xmm0");
                        __asm__("movdqu xmm1, [rcx-0x10]");
                        __asm__("movups [rbx+0x10], xmm1");
                        if (rsi123 == r11_149) 
                            goto addr_b2b8_99;
                    } else {
                        rsi123 = *reinterpret_cast<void***>(r15_154 + 16);
                        __asm__("movdqu xmm2, [rax-0x20]");
                        r11_149 = *reinterpret_cast<void***>(r15_154) + 0xffffffffffffffe0;
                        *reinterpret_cast<void***>(r15_154) = r11_149;
                        __asm__("movups [rbx], xmm2");
                        __asm__("movdqu xmm3, [rax-0x10]");
                        rax147 = *reinterpret_cast<void***>(r15_154 + 8);
                        __asm__("movups [rbx+0x10], xmm3");
                        if (rsi123 == r11_149) 
                            goto addr_b2b8_99;
                    }
                    rcx105 = r12_156;
                }
                goto addr_b440_102;
            }
        }
        addr_b418_103:
        r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
        rsi123 = r11_158;
        if (*reinterpret_cast<void***>(rbx126 + 48) != r10_137) {
            addr_b6a8_104:
            r11_158 = rsi123;
            if (rsi123 != rdx94 || (rax136 == *reinterpret_cast<void***>(rbx126 + 24) || !rcx105)) {
                rdi146 = r9_134;
                r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
                r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
                r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx126 + 8))) >> 5);
            } else {
                v160 = rbp121;
                r15_161 = v107;
                rbp162 = rcx105 + 0xffffffffffffffff;
                r12_163 = v108;
                do {
                    rdi164 = rax136 + 0xffffffffffffffe0;
                    rdx94 = r12_163;
                    rsi123 = r15_161;
                    *reinterpret_cast<void***>(rbx126 + 8) = rdi164;
                    write_unique(rdi164, rsi123, rdx94);
                    rax136 = *reinterpret_cast<void***>(rbx126 + 8);
                    if (rax136 == *reinterpret_cast<void***>(rbx126 + 24)) 
                        break;
                    cf165 = reinterpret_cast<unsigned char>(rbp162) < reinterpret_cast<unsigned char>(1);
                    --rbp162;
                } while (!cf165);
                rbp121 = v160;
                r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
                rdi146 = *reinterpret_cast<void***>(rbx126);
                r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
                r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
                r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax136)) >> 5);
            }
        } else {
            r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
            rdi146 = r9_134;
            r12_159 = r10_137;
        }
        addr_b301_111:
        *reinterpret_cast<void***>(rbx126 + 40) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_158) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v133) - reinterpret_cast<unsigned char>(rdi146)) >> 5));
        *reinterpret_cast<void***>(rbx126 + 48) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_159) - reinterpret_cast<unsigned char>(r10_137));
        if (!*reinterpret_cast<signed char*>(&r8d150)) {
            rsi123 = rbx126;
            queue_check_insert_part_0(r13_101, rsi123, rdx94);
        }
        if (*reinterpret_cast<uint32_t*>(rbx126 + 80) > 1) {
            rdi166 = *reinterpret_cast<void***>(rbx126 + 56) + 88;
            fun_3e80(rdi166, rsi123, rdx94, rcx105);
            rsi123 = *reinterpret_cast<void***>(rbx126 + 56);
            if (!*reinterpret_cast<unsigned char*>(rsi123 + 84)) {
                queue_check_insert_part_0(r13_101, rsi123, rdx94);
                rsi123 = *reinterpret_cast<void***>(rbx126 + 56);
            }
            fun_3b80(rsi123 + 88, rsi123, rdx94, rcx105);
        } else {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx126 + 48)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx126 + 40)))) {
                r12_167 = *reinterpret_cast<void***>(rbx126 + 56);
                fun_3e80(rbp121, rsi123, rdx94, rcx105);
                rdi168 = *reinterpret_cast<void***>(r13_101);
                rsi123 = r12_167;
                heap_insert(rdi168, rsi123, rdx94, rcx105);
                *reinterpret_cast<unsigned char*>(r12_167 + 84) = 1;
                fun_3a90(r14_120, rsi123, rdx94, rcx105);
                fun_3b80(rbp121, rsi123, rdx94, rcx105);
            }
        }
        fun_3b80(v128, rsi123, rdx94, rcx105);
        continue;
        addr_b600_89:
        rbx126 = rbp140;
        rbp121 = v138;
        addr_b608_120:
        r10_137 = *reinterpret_cast<void***>(rbx126 + 48);
        rdx94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax136)) >> 5);
        if (rdx94 != r10_137) {
            rsi123 = *reinterpret_cast<void***>(rbx126 + 40);
            rdx94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v133) - reinterpret_cast<unsigned char>(r9_134)) >> 5);
            goto addr_b6a8_104;
        } else {
            rdi146 = r9_134;
            if (*reinterpret_cast<void***>(rbx126 + 16) == r9_134 || !rcx105) {
                r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
                r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
                r12_159 = r10_137;
                goto addr_b301_111;
            } else {
                v169 = rbp121;
                r15_170 = v107;
                rbp171 = rcx105 + 0xffffffffffffffff;
                r12_172 = v108;
                do {
                    rdi173 = rdi146 - 32;
                    rdx94 = r12_172;
                    rsi123 = r15_170;
                    *reinterpret_cast<void***>(rbx126) = rdi173;
                    write_unique(rdi173, rsi123, rdx94);
                    rdi146 = *reinterpret_cast<void***>(rbx126);
                    if (rdi146 == *reinterpret_cast<void***>(rbx126 + 16)) 
                        break;
                    cf174 = reinterpret_cast<unsigned char>(rbp171) < reinterpret_cast<unsigned char>(1);
                    --rbp171;
                } while (!cf174);
                rbp121 = v169;
                r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
                r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx126 + 8))) >> 5);
                r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
                r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
                goto addr_b301_111;
            }
        }
        addr_b6d8_83:
        rbx126 = rbp140;
        rcx105 = reinterpret_cast<void**>(0xffffffffffffffff);
        rbp121 = v138;
        goto addr_b608_120;
        addr_b3f5_86:
        rbx126 = rbp140;
        rcx105 = r12_142;
        rbp121 = v138;
        rdx94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v133) - reinterpret_cast<unsigned char>(r9_134)) >> 5);
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax136)) >> 5);
        goto addr_b418_103;
        addr_b2e8_92:
        r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
        r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
        if (r10_137 == r12_159) 
            goto addr_b2fe_128;
        if (r9_151 != r11_158 || ((r9_175 = *reinterpret_cast<void***>(rbx126 + 24), r9_175 == rax147) || (rsi123 = rcx105 + 0xffffffffffffffff, rcx105 == 0))) {
            addr_b2fe_128:
            *reinterpret_cast<void***>(rdx94) = r15_148;
            goto addr_b301_111;
        } else {
            rax176 = rax147 - 32;
            do {
                __asm__("movdqu xmm6, [rax]");
                r15_148 = r15_148 - 32;
                *reinterpret_cast<void***>(rbx126 + 8) = rax176;
                rcx105 = rax176;
                __asm__("movups [r15], xmm6");
                __asm__("movdqu xmm7, [rax+0x10]");
                __asm__("movups [r15+0x10], xmm7");
                if (rax176 == r9_175) 
                    break;
                --rsi123;
                rax176 = rax176 - 32;
            } while (rsi123 != 0xffffffffffffffff);
            goto addr_b7f7_133;
        }
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax176)) >> 5);
        goto addr_b2fe_128;
        addr_b7f7_133:
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rcx105)) >> 5);
        goto addr_b2fe_128;
        addr_b440_102:
        rbx126 = r15_154;
        rdi146 = *reinterpret_cast<void***>(rbx126);
        r15_148 = rbx155;
        r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
        rdx94 = *reinterpret_cast<void***>(rbx126 + 32);
        r9_151 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v153) - reinterpret_cast<unsigned char>(r11_149)) >> 5);
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax147)) >> 5);
        addr_b46d_135:
        r12_159 = *reinterpret_cast<void***>(rbx126 + 48);
        r11_158 = *reinterpret_cast<void***>(rbx126 + 40);
        if (r12_159 == r10_137) {
            if (rsi123 == rdi146) 
                goto addr_b2fe_128;
            rax177 = rcx105 + 0xffffffffffffffff;
            if (!rcx105) 
                goto addr_b2fe_128;
            rcx105 = rdi146 + 0xffffffffffffffe0;
            do {
                __asm__("movdqu xmm4, [rcx]");
                r15_148 = r15_148 - 32;
                *reinterpret_cast<void***>(rbx126) = rcx105;
                rdi146 = rcx105;
                __asm__("movups [r15], xmm4");
                __asm__("movdqu xmm5, [rcx+0x10]");
                __asm__("movups [r15+0x10], xmm5");
                if (rcx105 == rsi123) 
                    break;
                --rax177;
                rcx105 = rcx105 - 32;
            } while (rax177 != 0xffffffffffffffff);
            goto addr_b2fe_128;
            goto addr_b2fe_128;
        }
        addr_b5c0_96:
        rbx126 = r15_154;
        rdi146 = *reinterpret_cast<void***>(rbx126);
        r15_148 = rbx155;
        r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
        rcx105 = reinterpret_cast<void**>(0xffffffffffffffff);
        rdx94 = *reinterpret_cast<void***>(rbx126 + 32);
        r9_151 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v153) - reinterpret_cast<unsigned char>(r11_149)) >> 5);
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax147)) >> 5);
        goto addr_b46d_135;
        addr_b2b8_99:
        rbx126 = r15_154;
        r8d150 = *reinterpret_cast<unsigned char*>(rbx126 + 84);
        r15_148 = rbx155;
        rdx94 = *reinterpret_cast<void***>(rbx126 + 32);
        rdi146 = r11_149;
        rcx105 = r12_156;
        r9_151 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v153) - reinterpret_cast<unsigned char>(r11_149)) >> 5);
        r10_137 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v135) - reinterpret_cast<unsigned char>(rax147)) >> 5);
        goto addr_b2e8_92;
    }
    fun_3b80(v128, rsi123, rdx94, rcx105);
    fun_3e80(rbp121, rsi123, rdx94, rcx105);
    rdi178 = *reinterpret_cast<void***>(r13_101);
    heap_insert(rdi178, rbx126, rdx94, rcx105);
    *reinterpret_cast<unsigned char*>(rbx126 + 84) = 1;
    fun_3a90(r14_120, rbx126, rdx94, rcx105);
    fun_3b80(rbp121, rbx126, rdx94, rcx105);
    addr_b722_68:
    rax179 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v110) - reinterpret_cast<unsigned char>(g28));
    if (rax179) {
        fun_3950();
    } else {
        goto v180;
    }
    addr_afe8_38:
    rbp181 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_20) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax57) << 4));
    *reinterpret_cast<int32_t*>(&rdx94) = 5;
    *reinterpret_cast<int32_t*>(&rdx94 + 4) = 0;
    rax182 = fun_3920();
    rdi95 = rax182;
    rsi96 = rbp181;
    sort_die(rdi95, rsi96, 5);
    rsp97 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp58) - 8 + 8 - 8 + 8);
    goto addr_b011_62;
}

/* queue_check_insert.part.0 */
void queue_check_insert_part_0(void** rdi, void** rsi, void** rdx) {
    void** rdx4;
    void** rcx5;
    void** rdi6;

    rdx4 = *reinterpret_cast<void***>(rsi + 8);
    rcx5 = *reinterpret_cast<void***>(rsi + 16);
    if (*reinterpret_cast<void***>(rsi) == rcx5) {
        if (rdx4 == *reinterpret_cast<void***>(rsi + 24) || *reinterpret_cast<void***>(rsi + 40)) {
            addr_8e1f_3:
            return;
        }
    } else {
        if (rdx4 != *reinterpret_cast<void***>(rsi + 24)) 
            goto addr_8de0_6;
        if (*reinterpret_cast<void***>(rsi + 48)) 
            goto addr_8e1f_3;
    }
    addr_8de0_6:
    fun_3e80(rdi + 8, rsi, rdx4, rcx5);
    rdi6 = *reinterpret_cast<void***>(rdi);
    heap_insert(rdi6, rsi, rdx4, rcx5);
    *reinterpret_cast<unsigned char*>(rsi + 84) = 1;
    fun_3a90(rdi + 48, rsi, rdx4, rcx5);
}

int64_t saved_line = 0;

struct s16 {
    signed char[120672] pad120672;
    signed char f1d760;
};

struct s17 {
    signed char[120672] pad120672;
    signed char f1d760;
};

void write_unique(void** rdi, void** rsi, void** rdx) {
    void*** rsp4;
    int1_t zf5;
    int1_t zf6;
    void** rax7;
    void** rsi8;
    void** r12_9;
    void** rbp10;
    void* rsp11;
    void** r14_12;
    void** r15_13;
    void** v14;
    void** rbx15;
    void** rax16;
    void** v17;
    int1_t zf18;
    uint32_t eax19;
    void** rax20;
    void** rdx21;
    void** rax22;
    void** rdi23;
    void** rbx24;
    void** rsi25;
    void** v26;
    void** rax27;
    void** v28;
    int32_t eax29;
    void* rsp30;
    uint32_t eax31;
    int1_t cf32;
    uint32_t edx33;
    int32_t eax34;
    void** rax35;
    int32_t eax36;
    void** r13_37;
    unsigned char v38;
    void** eax39;
    void** r8d40;
    void** ecx41;
    void** rdi42;
    void** rsi43;
    void** rsi44;
    uint32_t eax45;
    int32_t eax46;
    int1_t cf47;
    void* rax48;
    int64_t v49;
    void** r13_50;
    void** rbp51;
    void** rbx52;
    void** rax53;
    void** v54;
    void** rax55;
    void* rsp56;
    void** v57;
    void* rax58;
    void** rsi59;
    void** rax60;
    void** rax61;
    void** r13d62;
    int64_t v63;
    void** r12_64;
    void** r15_65;
    uint32_t eax66;
    void** rdi67;
    void** rax68;
    void** r9d69;
    void** rdi70;
    void** rsi71;
    struct s3* rax72;
    void** rdi73;
    int32_t eax74;
    void** rdi75;
    void** eax76;
    int64_t rdi77;
    int32_t v78;
    void** rax79;
    void** rax80;
    int32_t v81;
    void** rax82;
    void** edi83;
    uint32_t tmp32_84;
    void* rax85;
    void** rbx86;
    int32_t eax87;
    void* rsp88;
    void* rax89;
    int32_t eax90;
    void* rax91;
    void** rdi92;
    void** rax93;
    int1_t cf94;
    void** rax95;
    void** rdi96;
    void** rax97;
    void** rdi98;
    void** rax99;
    int1_t zf100;
    int1_t zf101;
    void** r10_102;
    void** rax103;
    uint32_t r15d104;
    struct s16* rax105;
    struct s17* rax106;
    void** v107;
    void** rax108;
    void* rax109;
    struct s4** rdi110;
    signed char al111;
    void** rax112;
    int64_t rcx113;
    void** rax114;
    int32_t ecx115;
    uint32_t eax116;
    void** rax117;
    int32_t eax118;

    rsp4 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16);
    zf5 = unique == 0;
    if (!zf5) {
        zf6 = saved_line == 0;
        if (zf6 || (rax7 = compare(rdi, 0x1d860, rdx), rsp4 = rsp4 - 8 + 8, rdi = rdi, rdx = rdx, !!*reinterpret_cast<int32_t*>(&rax7))) {
            __asm__("movdqu xmm0, [rdi]");
            __asm__("movaps [rip+0x13727], xmm0");
            __asm__("movdqu xmm1, [rdi+0x10]");
            __asm__("movaps [rip+0x1372b], xmm1");
        } else {
            return;
        }
    }
    rsi8 = rsi;
    r12_9 = rsi8;
    rbp10 = rdx;
    rsp11 = reinterpret_cast<void*>(rsp4 + 16 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56);
    r14_12 = *reinterpret_cast<void***>(rdi);
    r15_13 = *reinterpret_cast<void***>(rdi + 8);
    v14 = rdi;
    rbx15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r15_13));
    rax16 = g28;
    v17 = rax16;
    if (rdx || (zf18 = debug == 0, zf18)) {
        eax19 = eolchar;
        *reinterpret_cast<void***>(rbx15 + 0xffffffffffffffff) = *reinterpret_cast<void***>(&eax19);
        rax20 = fun_3bf0(r14_12, 1, r15_13, r12_9);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        if (r15_13 != rax20) {
            addr_83ee_8:
            *reinterpret_cast<int32_t*>(&rdx21) = 5;
            *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
            rax22 = fun_3920();
            rdi23 = rax22;
            sort_die(rdi23, rbp10, 5, rdi23, rbp10, 5);
        } else {
            *reinterpret_cast<void***>(rbx15 + 0xffffffffffffffff) = reinterpret_cast<void**>(0);
            goto addr_7fd3_10;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r14_12) < reinterpret_cast<unsigned char>(rbx15)) 
            goto addr_803c_12;
        goto addr_80a8_14;
    }
    rbx24 = rbp10;
    *reinterpret_cast<int32_t*>(&rsi25) = 0x80000;
    *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
    v26 = rdi23;
    rax27 = g28;
    v28 = rax27;
    eax29 = rpl_pipe2();
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8);
    if (eax29 >= 0) {
        eax31 = nmerge;
        cf32 = eax31 + 1 < nprocs;
        if (cf32) {
            rdi23 = reinterpret_cast<void**>(0xffffffff);
            *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
            reap(0xffffffff, 0x80000);
            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
            while ((edx33 = nprocs, !(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(edx33) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(edx33 == 0))) && (rdi23 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0, eax34 = reap(0, 0x80000), rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8), !!eax34)) {
            }
        }
        rax35 = fun_37d0();
        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
        --rbx24;
        *reinterpret_cast<void***>(rdi23) = g80000;
        r12_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp30) + 40);
        rbp10 = rax35;
        *reinterpret_cast<void***>(rdi23 + 4) = g80004;
        while (1) {
            rdi23 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
            rdx21 = r12_9;
            rsi25 = reinterpret_cast<void**>(0x1d3a0);
            eax36 = fun_3c00();
            r13_37 = temphead;
            temphead = reinterpret_cast<void**>(0);
            v38 = reinterpret_cast<uint1_t>(eax36 == 0);
            eax39 = fun_3e70();
            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
            r8d40 = *reinterpret_cast<void***>(rbp10);
            ecx41 = eax39;
            if (eax39) {
                temphead = r13_37;
                if (v38) {
                    addr_8539_30:
                    *reinterpret_cast<int32_t*>(&rdx21) = 0;
                    *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
                    rsi25 = r12_9;
                    rdi23 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
                    fun_3c00();
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                    ecx41 = ecx41;
                    r8d40 = r8d40;
                    goto addr_84a2_31;
                } else {
                    addr_84a2_31:
                    *reinterpret_cast<void***>(rbp10) = r8d40;
                    if (reinterpret_cast<signed char>(ecx41) >= reinterpret_cast<signed char>(0)) 
                        goto addr_855f_32;
                }
                if (!reinterpret_cast<int1_t>(r8d40 == 11)) 
                    goto addr_85e4_34;
                *reinterpret_cast<void***>(rdi23) = *reinterpret_cast<void***>(rsi25);
                rdi42 = rdi23 + 4;
                rsi43 = rsi25 + 4;
                xnanosleep();
                rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                *reinterpret_cast<void***>(rdi42) = *reinterpret_cast<void***>(rsi43);
                rsi44 = rsi43 + 4;
                __asm__("movapd xmm1, xmm2");
                __asm__("addsd xmm1, xmm2");
                *reinterpret_cast<void***>(rdi42 + 4) = *reinterpret_cast<void***>(rsi44);
                rsi25 = rsi44 + 4;
                do {
                    eax45 = nprocs;
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax45) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax45 == 0)) 
                        break;
                    eax46 = reap(0, rsi25);
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
                } while (eax46);
                cf47 = reinterpret_cast<unsigned char>(rbx24) < reinterpret_cast<unsigned char>(1);
                --rbx24;
                if (cf47) 
                    goto addr_85e0_47;
            } else {
                if (!v38) 
                    goto addr_8598_49; else 
                    goto addr_8539_30;
            }
        }
    }
    addr_8568_50:
    rax48 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v28) - reinterpret_cast<unsigned char>(g28));
    if (!rax48) {
        goto v49;
    }
    fun_3950();
    r13_50 = rsi25;
    rbp51 = rdx21;
    rbx52 = rdi23;
    rax53 = g28;
    v54 = rax53;
    rax55 = xnmalloc(r13_50, 8, rdx21, r13_50, 8, rdx21);
    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    v57 = rax55;
    *reinterpret_cast<void***>(rbp51) = rax55;
    if (r13_50) 
        goto addr_8666_54;
    addr_8727_56:
    while (rax58 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v54) - reinterpret_cast<unsigned char>(g28)), !!rax58) {
        addr_885c_57:
        fun_3950();
        addr_8861_58:
        rsi59 = compress_program;
        quotearg_style(4, rsi59, rdx21, 4, rsi59, rdx21);
        rax60 = fun_3920();
        rdx21 = rax60;
        fun_3c90();
        addr_889c_59:
        rax61 = fun_37d0();
        r13d62 = *reinterpret_cast<void***>(rax61);
        fun_3a40();
        *reinterpret_cast<void***>(rbp51) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax61) = r13d62;
    }
    goto v63;
    addr_8666_54:
    rbp51 = rax55;
    *reinterpret_cast<int32_t*>(&r12_64) = 0;
    *reinterpret_cast<int32_t*>(&r12_64 + 4) = 0;
    do {
        r15_65 = *reinterpret_cast<void***>(rbx52 + 8);
        if (!r15_65 || (eax66 = *reinterpret_cast<unsigned char*>(r15_65 + 12), *reinterpret_cast<signed char*>(&eax66) == 0)) {
            rdi67 = *reinterpret_cast<void***>(rbx52);
            rax68 = stream_open(rdi67, "r", rdx21, rdi67, "r", rdx21);
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            *reinterpret_cast<void***>(rbp51) = rax68;
            if (!rax68) 
                goto addr_8727_56;
        } else {
            if (*reinterpret_cast<signed char*>(&eax66) == 1 && (r9d69 = *reinterpret_cast<void***>(r15_65 + 8), rdi70 = proctab, rsi71 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp56) + 16), rax72 = hash_remove(rdi70, rsi71, rdx21, rdi70, rsi71, rdx21), rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8), !!rax72)) {
                rax72->fc = 2;
                reap(r9d69, rsi71);
                rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            }
            rdi73 = r15_65 + 13;
            eax74 = fun_3cd0(rdi73, rdi73);
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
            if (eax74 < 0) 
                break;
            rdi75 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp56) + 16);
            eax76 = pipe_fork(rdi75, 9, rdx21, rdi75, 9, rdx21);
            if (reinterpret_cast<int1_t>(eax76 == 0xffffffff)) 
                goto addr_86f1_67;
            if (!eax76) 
                goto addr_87fa_69;
            *reinterpret_cast<void***>(r15_65 + 8) = eax76;
            register_proc(r15_65, 9, rdx21, r15_65, 9, rdx21);
            fun_3a40();
            fun_3a40();
            *reinterpret_cast<int32_t*>(&rdi77) = v78;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi77) + 4) = 0;
            rax79 = fun_3c20(rdi77, "r", rdx21, rdi77, "r", rdx21);
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            if (!rax79) 
                goto addr_889c_59;
            *reinterpret_cast<void***>(rbp51) = rax79;
        }
        ++r12_64;
        rbx52 = rbx52 + 16;
        rbp51 = rbp51 + 8;
    } while (r13_50 != r12_64);
    goto addr_8727_56;
    *reinterpret_cast<void***>(v57 + reinterpret_cast<unsigned char>(r12_64) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_56;
    addr_86f1_67:
    rax80 = fun_37d0();
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax80) == 24)) 
        goto addr_8861_58;
    fun_3a40();
    *reinterpret_cast<void***>(rax80) = reinterpret_cast<void**>(24);
    *reinterpret_cast<void***>(v57 + reinterpret_cast<unsigned char>(r12_64) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_56;
    addr_87fa_69:
    fun_3a40();
    if (eax74) {
        move_fd_part_0(eax74, eax74);
    }
    if (v81 != 1) {
        move_fd_part_0(v81, v81);
    }
    rdx21 = reinterpret_cast<void**>("-d");
    fun_3e20();
    rax82 = fun_37d0();
    edi83 = *reinterpret_cast<void***>(rax82);
    async_safe_die(edi83, "couldn't execute compress program (with -d)", edi83, "couldn't execute compress program (with -d)");
    goto addr_885c_57;
    addr_855f_32:
    if (!ecx41) {
        addr_8598_49:
        fun_3a40();
        rdi23 = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
        fun_3a40();
        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
        goto addr_8568_50;
    } else {
        tmp32_84 = nprocs + 1;
        nprocs = tmp32_84;
        goto addr_8568_50;
    }
    addr_85e4_34:
    fun_3a40();
    rdi23 = *reinterpret_cast<void***>(v26 + 4);
    *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
    fun_3a40();
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
    *reinterpret_cast<void***>(rbp10) = r8d40;
    goto addr_8568_50;
    addr_85e0_47:
    r8d40 = *reinterpret_cast<void***>(rbp10);
    goto addr_85e4_34;
    addr_7fd3_10:
    rax85 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v17) - reinterpret_cast<unsigned char>(g28));
    if (rax85) {
        fun_3950();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        goto addr_83ee_8;
    } else {
        return;
    }
    addr_80a8_14:
    rbp10 = keylist;
    r13_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r15_13) + 0xffffffffffffffff);
    if (!rbp10) {
        while (1) {
            rbx86 = r14_12;
            while (1) {
                eax87 = mbsnwidth(r14_12);
                rsp88 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                r15_13 = reinterpret_cast<void**>(static_cast<int64_t>(eax87));
                if (reinterpret_cast<unsigned char>(r14_12) < reinterpret_cast<unsigned char>(rbx86)) {
                    do {
                        ++r14_12;
                        *reinterpret_cast<int32_t*>(&rax89) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax89) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r14_12 + 0xffffffffffffffff) == 9);
                        r15_13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_13) + reinterpret_cast<uint64_t>(rax89));
                    } while (rbx86 != r14_12);
                }
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                rsi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_37) - reinterpret_cast<unsigned char>(rbx86));
                eax90 = mbsnwidth(rbx86);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp88) - 8 + 8);
                r14_12 = reinterpret_cast<void**>(static_cast<int64_t>(eax90));
                if (reinterpret_cast<unsigned char>(rbx86) < reinterpret_cast<unsigned char>(r13_37)) {
                    do {
                        ++rbx86;
                        *reinterpret_cast<int32_t*>(&rax91) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax91) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx86 + 0xffffffffffffffff) == 9);
                        r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rax91));
                    } while (r13_37 != rbx86);
                }
                rbx15 = r15_13 + 0xffffffffffffffff;
                if (r15_13) {
                    do {
                        rdi92 = stdout;
                        rax93 = *reinterpret_cast<void***>(rdi92 + 40);
                        if (reinterpret_cast<unsigned char>(rax93) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi92 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi8) = 32;
                            *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                            fun_39c0(rdi92, 32);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        } else {
                            *reinterpret_cast<void***>(rdi92 + 40) = rax93 + 1;
                            *reinterpret_cast<void***>(rax93) = reinterpret_cast<void**>(32);
                        }
                        cf94 = reinterpret_cast<unsigned char>(rbx15) < reinterpret_cast<unsigned char>(1);
                        --rbx15;
                    } while (!cf94);
                }
                if (!r14_12) {
                    *reinterpret_cast<int32_t*>(&rdx) = 5;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                    rax95 = fun_3920();
                    rsi8 = rax95;
                    fun_3c40(1, rsi8, 5);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
                } else {
                    do {
                        rdi96 = stdout;
                        rax97 = *reinterpret_cast<void***>(rdi96 + 40);
                        if (reinterpret_cast<unsigned char>(rax97) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi96 + 48))) {
                            *reinterpret_cast<uint32_t*>(&rsi8) = 95;
                            *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                            fun_39c0(rdi96, 95);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        } else {
                            *reinterpret_cast<void***>(rdi96 + 40) = rax97 + 1;
                            *reinterpret_cast<void***>(rax97) = reinterpret_cast<void**>(95);
                        }
                        --r14_12;
                    } while (r14_12);
                    rdi98 = stdout;
                    rax99 = *reinterpret_cast<void***>(rdi98 + 40);
                    if (reinterpret_cast<unsigned char>(rax99) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi98 + 48))) 
                        goto addr_83b2_103; else 
                        goto addr_82a7_104;
                }
                addr_82b2_105:
                if (!rbp10) 
                    goto addr_7fd3_10;
                rbp10 = *reinterpret_cast<void***>(rbp10 + 64);
                if (!rbp10) {
                    zf100 = unique == 0;
                    if (!zf100) 
                        goto addr_7fd3_10;
                    zf101 = stable == 0;
                    if (!zf101) 
                        goto addr_7fd3_10;
                    r14_12 = *reinterpret_cast<void***>(v14);
                    rbx86 = r14_12;
                    r13_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v14 + 8)) + 0xffffffffffffffff);
                    continue;
                }
                r14_12 = *reinterpret_cast<void***>(v14);
                r15_13 = *reinterpret_cast<void***>(v14 + 8);
                r13_37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r15_13) + 0xffffffffffffffff);
                if (!rbp10) 
                    break;
                addr_80c8_111:
                r10_102 = *reinterpret_cast<void***>(rbp10);
                if (r10_102 != 0xffffffffffffffff) 
                    goto addr_80d6_112;
                rbx86 = r14_12;
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp10 + 16) == 0xffffffffffffffff)) {
                    addr_80ee_114:
                    rdx = rbp10;
                    rsi8 = r15_13;
                    rax103 = limfield_isra_0(r14_12, rsi8, rdx);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    r13_37 = rax103;
                }
                if (!reinterpret_cast<int1_t>(r10_102 == 0xffffffffffffffff) || !*reinterpret_cast<void***>(rbp10 + 48)) {
                    addr_810b_117:
                    if (*reinterpret_cast<unsigned char*>(rbp10 + 54)) 
                        goto addr_8125_118;
                    if (!(0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp10 + 48)))) 
                        continue; else 
                        goto addr_8125_118;
                } else {
                    addr_8125_118:
                    r15d104 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_37));
                    *reinterpret_cast<void***>(r13_37) = reinterpret_cast<void**>(0);
                    *reinterpret_cast<uint32_t*>(&rax105) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx86));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d760) + reinterpret_cast<uint64_t>(rax105))) {
                        do {
                            *reinterpret_cast<uint32_t*>(&rax106) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx86 + 1));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
                            ++rbx86;
                        } while (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d760) + reinterpret_cast<uint64_t>(rax106)));
                    }
                }
                v107 = rbx86;
                if (reinterpret_cast<unsigned char>(rbx86) > reinterpret_cast<unsigned char>(r13_37)) 
                    goto addr_8310_123;
                if (*reinterpret_cast<unsigned char*>(rbp10 + 54)) {
                    getmonth(rbx86, reinterpret_cast<int64_t>(rsp11) + 24, rdx);
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                } else {
                    if (*reinterpret_cast<unsigned char*>(rbp10 + 52)) {
                        fun_3c80(rbx86, reinterpret_cast<int64_t>(rsp11) + 24, rdx);
                        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                        __asm__("fstp st0");
                    } else {
                        if (!(0xff0000ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp10 + 48)))) {
                            addr_8310_123:
                            v107 = r13_37;
                        } else {
                            rax108 = rbx86;
                            if (reinterpret_cast<unsigned char>(rbx86) < reinterpret_cast<unsigned char>(r13_37)) {
                                *reinterpret_cast<int32_t*>(&rax109) = 0;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax109) + 4) = 0;
                                *reinterpret_cast<unsigned char*>(&rax109) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx86) == 45);
                                rax108 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax109) + reinterpret_cast<unsigned char>(rbx86));
                            }
                            rdi110 = reinterpret_cast<struct s4**>(reinterpret_cast<int64_t>(rsp11) + 32);
                            al111 = traverse_raw_number(rdi110, rsi8, rdi110, rsi8);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                            if (al111 > 47) {
                                rax112 = rax108;
                                *reinterpret_cast<uint32_t*>(&rcx113) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax112));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx113) + 4) = 0;
                                if (*reinterpret_cast<unsigned char*>(rbp10 + 53)) {
                                    rax112 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax112) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax112) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(0x15360 + rcx113) < 1)))))));
                                }
                                v107 = rax112;
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(r13_37) = *reinterpret_cast<void***>(&r15d104);
                r13_37 = v107;
                continue;
                addr_80d6_112:
                rdx = rbp10;
                rsi8 = r15_13;
                rax114 = begfield_isra_0(r14_12, rsi8, rdx);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                rbx86 = rax114;
                if (*reinterpret_cast<void***>(rbp10 + 16) == 0xffffffffffffffff) 
                    goto addr_810b_117; else 
                    goto addr_80ee_114;
                addr_83b2_103:
                *reinterpret_cast<uint32_t*>(&rsi8) = 10;
                *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
                fun_39c0(rdi98, 10);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                goto addr_82b2_105;
                addr_82a7_104:
                rdx = rax99 + 1;
                *reinterpret_cast<void***>(rdi98 + 40) = rdx;
                *reinterpret_cast<void***>(rax99) = reinterpret_cast<void**>(10);
                goto addr_82b2_105;
            }
        }
    } else {
        goto addr_80c8_111;
    }
    addr_809c_137:
    r14_12 = *reinterpret_cast<void***>(v14);
    r15_13 = *reinterpret_cast<void***>(v14 + 8);
    goto addr_80a8_14;
    while (1) {
        addr_8090_138:
        ecx115 = 10;
        eax116 = 10;
        goto addr_8020_139;
        addr_806e_140:
        rax117 = fun_3920();
        *reinterpret_cast<uint32_t*>(&rsi8) = 0;
        *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0;
        sort_die(rax117, 0, 5);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
        continue;
        while (*reinterpret_cast<uint32_t*>(&rsi8) = *reinterpret_cast<unsigned char*>(&ecx115), *reinterpret_cast<int32_t*>(&rsi8 + 4) = 0, eax118 = fun_39c0(r12_9, rsi8), rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8), eax118 != -1) {
            while (1) {
                if (rbx15 == r14_12) 
                    goto addr_809c_137;
                addr_803c_12:
                eax116 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_12));
                ++r14_12;
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax116) == 9)) {
                    ecx115 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&eax116));
                    if (rbx15 == r14_12) 
                        goto addr_8090_138;
                    addr_8020_139:
                    rdx = *reinterpret_cast<void***>(r12_9 + 40);
                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_9 + 48))) 
                        break;
                } else {
                    rdx = *reinterpret_cast<void***>(r12_9 + 40);
                    ecx115 = 62;
                    eax116 = 62;
                    if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_9 + 48))) 
                        break;
                }
                *reinterpret_cast<void***>(r12_9 + 40) = rdx + 1;
                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax116);
            }
        }
        goto addr_806e_140;
    }
}

struct s18 {
    unsigned char f0;
    signed char f1;
    signed char f2;
};

uint64_t file_prefixlen(struct s18* rdi, uint64_t* rsi) {
    uint64_t r8_3;
    uint64_t r10_4;
    uint64_t rcx5;
    uint64_t rdx6;
    int64_t rax7;
    int32_t r9d8;
    int64_t rax9;
    int32_t ecx10;
    uint32_t eax11;

    r8_3 = *rsi;
    *reinterpret_cast<int32_t*>(&r10_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_4) + 4) = 0;
    while (1) {
        rcx5 = r10_4 + 1;
        rdx6 = r10_4;
        if (rcx5 >= r8_3) {
            addr_d1ea_3:
            if (reinterpret_cast<int64_t>(r8_3) >= reinterpret_cast<int64_t>(0)) {
                addr_d190_4:
                if (r8_3 == rdx6) 
                    break;
            } else {
                if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi) + rdx6)) 
                    break;
            }
        } else {
            while (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi) + rdx6) == 46) {
                *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi) + rdx6 + 1);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rax7) <= 90) {
                    if (*reinterpret_cast<signed char*>(&rax7) <= 64) 
                        goto addr_d184_10;
                } else {
                    r9d8 = static_cast<int32_t>(rax7 - 97);
                    if (*reinterpret_cast<unsigned char*>(&r9d8) <= 25) 
                        goto addr_d1c5_12;
                    if (*reinterpret_cast<signed char*>(&rax7) != 0x7e) 
                        goto addr_d184_10;
                }
                addr_d1c5_12:
                rdx6 = rdx6 + 2;
                if (r8_3 <= rdx6) {
                    addr_d1e1_14:
                    rcx5 = rdx6 + 1;
                    if (rcx5 < r8_3) 
                        continue; else 
                        break;
                } else {
                    do {
                        *reinterpret_cast<uint32_t*>(&rax9) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi) + rdx6);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rax9) > 90) {
                            ecx10 = static_cast<int32_t>(rax9 - 97);
                            if (*reinterpret_cast<unsigned char*>(&ecx10) > 25) {
                                if (*reinterpret_cast<signed char*>(&rax9) != 0x7e) 
                                    goto addr_d1e1_14;
                            }
                        } else {
                            if (*reinterpret_cast<signed char*>(&rax9) > 64) 
                                continue;
                            eax11 = *reinterpret_cast<uint32_t*>(&rax9) - 48;
                            if (*reinterpret_cast<unsigned char*>(&eax11) > 9) 
                                goto addr_d1e1_14;
                        }
                        ++rdx6;
                    } while (r8_3 > rdx6);
                }
                goto addr_d1e1_14;
            }
            goto addr_d1ea_3;
        }
        r10_4 = rcx5;
        continue;
        addr_d184_10:
        if (reinterpret_cast<int64_t>(r8_3) >= reinterpret_cast<int64_t>(0)) {
            goto addr_d190_4;
        }
    }
    *rsi = rdx6;
    return r10_4;
}

int64_t verrevcmp(struct s18* rdi, uint64_t rsi, struct s18* rdx, uint64_t rcx) {
    struct s18* r8_5;
    uint64_t rax6;
    struct s18* rdi7;
    uint64_t rdx8;
    int64_t rax9;
    int32_t r9d10;
    int64_t rax11;
    uint32_t ebx12;
    uint32_t r10d13;
    int64_t r9_14;
    uint32_t r10d15;
    int64_t r9_16;
    int64_t r11_17;
    int64_t r10_18;
    int32_t r11d19;
    int64_t r10_20;
    int64_t r11_21;
    int32_t ebp22;

    r8_5 = rdi;
    *reinterpret_cast<int32_t*>(&rax6) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rdi7 = rdx;
    *reinterpret_cast<int32_t*>(&rdx8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
    goto addr_d22c_2;
    addr_d476_3:
    *reinterpret_cast<int32_t*>(&rax9) = r9d10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
    addr_d480_4:
    return 0xffffffff;
    addr_d46f_5:
    addr_d470_6:
    r9d10 = 1;
    goto addr_d476_3;
    addr_d492_7:
    *reinterpret_cast<uint32_t*>(&rax11) = ebx12 - r10d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    return rax11;
    addr_d4b5_8:
    r10d13 = 0;
    goto addr_d492_7;
    addr_d48c_9:
    r10d13 = 0xffffffff;
    goto addr_d492_7;
    addr_d3a1_10:
    r9d10 = 0;
    goto addr_d476_3;
    while (1) {
        *reinterpret_cast<uint32_t*>(&r9_14) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_5) + rdx8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&r9_14) - 48 <= 9) {
            while (1) {
                if (reinterpret_cast<int64_t>(rax6) >= reinterpret_cast<int64_t>(rcx) || (r10d15 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi7) + rax6))), *reinterpret_cast<uint32_t*>(&r9_16) = r10d15, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_16) + 4) = 0, r10d15 - 48 <= 9)) {
                    if (reinterpret_cast<int64_t>(rdx8) < reinterpret_cast<int64_t>(rsi)) {
                        do {
                            if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_5) + rdx8) != 48) 
                                break;
                            ++rdx8;
                        } while (rsi != rdx8);
                        goto addr_d440_16;
                    } else {
                        goto addr_d440_16;
                    }
                } else {
                    if (rdx8 == rsi) {
                        *reinterpret_cast<uint32_t*>(&r11_17) = *reinterpret_cast<unsigned char*>(&r9_16);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_17) + 4) = 0;
                        ebx12 = 0xffffffff;
                        if (static_cast<uint32_t>(r11_17 - 48) > 9) 
                            goto addr_d29e_20; else 
                            goto addr_d4b5_8;
                    } else {
                        *reinterpret_cast<uint32_t*>(&r10_18) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_5) + rdx8);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_18) + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&r9_14) = *reinterpret_cast<uint32_t*>(&r10_18);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
                        if (static_cast<uint32_t>(r10_18 - 48) > 9) {
                            addr_d25c_22:
                            ebx12 = *reinterpret_cast<uint32_t*>(&r10_18);
                            if (*reinterpret_cast<signed char*>(&r9_14) > reinterpret_cast<signed char>(90)) {
                                r11d19 = static_cast<int32_t>(r9_14 - 97);
                                if (*reinterpret_cast<unsigned char*>(&r11d19) <= 25) {
                                    addr_d280_24:
                                    if (rax6 == rcx) 
                                        goto addr_d48c_9; else 
                                        goto addr_d289_25;
                                } else {
                                    goto addr_d26f_27;
                                }
                            } else {
                                if (*reinterpret_cast<signed char*>(&r9_14) > reinterpret_cast<signed char>(64)) 
                                    goto addr_d280_24; else 
                                    goto addr_d26f_27;
                            }
                        } else {
                            ebx12 = 0;
                            goto addr_d289_25;
                        }
                    }
                }
                if (reinterpret_cast<int64_t>(rax6) < reinterpret_cast<int64_t>(rcx)) {
                    do {
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi7) + rax6) != 48) 
                            break;
                        ++rax6;
                    } while (rcx != rax6);
                    goto addr_d450_33;
                } else {
                    goto addr_d450_33;
                }
                r9d10 = 0;
                if (reinterpret_cast<int64_t>(rdx8) >= reinterpret_cast<int64_t>(rsi)) {
                    addr_d4cf_36:
                    if (reinterpret_cast<int64_t>(rsi) <= reinterpret_cast<int64_t>(rdx8)) {
                        addr_d36b_37:
                        if (reinterpret_cast<int64_t>(rcx) <= reinterpret_cast<int64_t>(rax6)) 
                            goto addr_d383_38;
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi7) + rax6) - 48 <= 9) 
                            goto addr_d480_4;
                    } else {
                        goto addr_d45c_41;
                    }
                } else {
                    do {
                        if (reinterpret_cast<int64_t>(rcx) <= reinterpret_cast<int64_t>(rax6)) 
                            goto addr_d4cf_36;
                        *reinterpret_cast<int32_t*>(&r10_20) = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_5) + rdx8);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_20) + 4) = 0;
                        if (static_cast<uint32_t>(r10_20 - 48) > 9) 
                            goto addr_d36b_37;
                        *reinterpret_cast<int32_t*>(&r11_21) = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdi7) + rax6);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_21) + 4) = 0;
                        if (static_cast<uint32_t>(r11_21 - 48) > 9) 
                            goto addr_d470_6;
                        if (!r9d10) {
                            r9d10 = *reinterpret_cast<int32_t*>(&r10_20) - *reinterpret_cast<int32_t*>(&r11_21);
                        }
                        ++rdx8;
                        ++rax6;
                    } while (reinterpret_cast<int64_t>(rsi) > reinterpret_cast<int64_t>(rdx8));
                    goto addr_d439_48;
                }
                addr_d383_38:
                if (r9d10) 
                    goto addr_d476_3;
                if (reinterpret_cast<int64_t>(rdx8) < reinterpret_cast<int64_t>(rsi)) 
                    break;
                goto addr_d398_51;
                addr_d45c_41:
                if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_5) + rdx8) - 48 > 9) 
                    goto addr_d36b_37; else 
                    goto addr_d46f_5;
                addr_d439_48:
                goto addr_d36b_37;
                addr_d450_33:
                if (reinterpret_cast<int64_t>(rdx8) >= reinterpret_cast<int64_t>(rsi)) {
                    addr_d398_51:
                    if (reinterpret_cast<int64_t>(rax6) >= reinterpret_cast<int64_t>(rcx)) 
                        goto addr_d3a1_10;
                } else {
                    r9d10 = 0;
                    goto addr_d45c_41;
                }
                addr_d2d3_53:
                if (reinterpret_cast<int64_t>(rdx8) < reinterpret_cast<int64_t>(rsi)) 
                    break; else 
                    continue;
                addr_d440_16:
                if (reinterpret_cast<int64_t>(rax6) >= reinterpret_cast<int64_t>(rcx)) {
                    addr_d22c_2:
                    if (reinterpret_cast<int64_t>(rdx8) >= reinterpret_cast<int64_t>(rsi)) 
                        goto addr_d398_51; else 
                        break;
                }
                addr_d29e_20:
                r10d13 = *reinterpret_cast<uint32_t*>(&r11_17);
                if (*reinterpret_cast<signed char*>(&r9_16) > reinterpret_cast<signed char>(90)) {
                    ebp22 = static_cast<int32_t>(r9_16 - 97);
                    if (*reinterpret_cast<unsigned char*>(&ebp22) <= 25) {
                        addr_d2c2_56:
                        if (r10d13 != ebx12) 
                            goto addr_d492_7;
                    } else {
                        goto addr_d2b1_58;
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&r9_16) > reinterpret_cast<signed char>(64)) 
                        goto addr_d2c2_56; else 
                        goto addr_d2b1_58;
                }
                ++rdx8;
                ++rax6;
                goto addr_d2d3_53;
                addr_d2b1_58:
                if (*reinterpret_cast<unsigned char*>(&r9_16) == 0x7e) {
                    r10d13 = 0xfffffffe;
                    goto addr_d2c2_56;
                } else {
                    r10d13 = static_cast<uint32_t>(r11_17 + 0x100);
                    goto addr_d2c2_56;
                }
                addr_d289_25:
                *reinterpret_cast<uint32_t*>(&r9_16) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi7) + rax6);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_16) + 4) = 0;
                r10d13 = 0;
                *reinterpret_cast<uint32_t*>(&r11_17) = *reinterpret_cast<unsigned char*>(&r9_16);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_17) + 4) = 0;
                if (static_cast<uint32_t>(r11_17 - 48) <= 9) 
                    goto addr_d2c2_56; else 
                    goto addr_d29e_20;
                addr_d26f_27:
                if (*reinterpret_cast<unsigned char*>(&r9_14) == 0x7e) {
                    ebx12 = 0xfffffffe;
                    goto addr_d280_24;
                } else {
                    ebx12 = static_cast<uint32_t>(r10_18 + 0x100);
                    goto addr_d280_24;
                }
            }
        } else {
            *reinterpret_cast<uint32_t*>(&r10_18) = *reinterpret_cast<unsigned char*>(&r9_14);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_18) + 4) = 0;
            ebx12 = 0;
            if (static_cast<uint32_t>(r10_18 - 48) <= 9) 
                goto addr_d280_24; else 
                goto addr_d25c_22;
        }
    }
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0x17260);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0x17260);
    if (*reinterpret_cast<void***>(rdi + 40) != 0x17260) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x9a88]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0x17260);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x98ef]");
        if (1) 
            goto addr_da7c_6;
        __asm__("comiss xmm1, [rip+0x98e6]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x98d8]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_da5c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_da52_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_da5c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_da52_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_da52_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_da5c_13:
        return 0;
    } else {
        addr_da7c_6:
        return r8_3;
    }
}

struct s19 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s20 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_37c0();

int32_t transfer_entries(void** rdi, void** rsi, int32_t edx) {
    void** rdx3;
    void** r14_4;
    int32_t r12d5;
    void** rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rax12;
    struct s19* rax13;
    void** rax14;
    void** rsi15;
    void** rax16;
    struct s20* r13_17;
    void** rax18;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = *reinterpret_cast<void***>(rsi);
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8))) {
        do {
            addr_dae6_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_dad8_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_dae6_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = *reinterpret_cast<void***>(r14_4 + 16);
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rax12 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(r15_11, rsi10)), rsi10 = *reinterpret_cast<void***>(r14_4 + 16), reinterpret_cast<unsigned char>(rax12) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax13 = reinterpret_cast<struct s19*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax13->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax13->f8;
                            rax13->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_db5e_9;
                        } else {
                            rax13->f0 = r15_11;
                            rax14 = *reinterpret_cast<void***>(r14_4 + 72);
                            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14;
                            *reinterpret_cast<void***>(r14_4 + 72) = r13_9;
                            if (!rdx3) 
                                goto addr_db5e_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_3ea5_12;
                    addr_db5e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_dad8_3;
            }
            rsi15 = *reinterpret_cast<void***>(r14_4 + 16);
            rax16 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(r15_8, rsi15));
            if (reinterpret_cast<unsigned char>(rax16) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 16))) 
                goto addr_3ea5_12;
            r13_17 = reinterpret_cast<struct s20*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax16) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
            if (r13_17->f0) 
                goto addr_db98_16;
            r13_17->f0 = r15_8;
            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
            continue;
            addr_db98_16:
            rax18 = *reinterpret_cast<void***>(r14_4 + 72);
            if (!rax18) {
                rax18 = fun_3760(16, rsi15, rdx3);
                if (!rax18) 
                    goto addr_dc0a_19;
            } else {
                *reinterpret_cast<void***>(r14_4 + 72) = *reinterpret_cast<void***>(rax18 + 8);
            }
            rdx3 = r13_17->f8;
            *reinterpret_cast<void***>(rax18) = r15_8;
            *reinterpret_cast<void***>(rax18 + 8) = rdx3;
            r13_17->f8 = rax18;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            *reinterpret_cast<void***>(rbp6 + 24) = *reinterpret_cast<void***>(rbp6 + 24) - 1;
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_3ea5_12:
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    fun_37c0();
    addr_dc0a_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_d90f_25:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_d8b4_28;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_d920_32;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_d920_32;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_d90f_25;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_d8b4_28;
            }
        }
    }
    addr_d911_36:
    return rax11;
    addr_d8b4_28:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_d911_36;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_d920_32:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

uint64_t fun_3d40();

unsigned char malloc;

/* parse_omp_threads.part.0 */
uint64_t parse_omp_threads_part_0(void** rdi) {
    void** rax2;
    void** v3;
    int32_t eax4;
    uint64_t rax5;
    uint32_t ecx6;
    unsigned char* rdx7;
    void* rdx8;

    rax2 = g28;
    v3 = rax2;
    eax4 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi));
    if (!*reinterpret_cast<signed char*>(&eax4)) 
        goto addr_efa3_2;
    if (*reinterpret_cast<signed char*>(&eax4) <= 13) 
        goto addr_ef9f_4;
    while (*reinterpret_cast<signed char*>(&eax4) == 32) {
        do {
            eax4 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 1));
            ++rdi;
            if (!*reinterpret_cast<signed char*>(&eax4)) 
                goto addr_efa3_2;
            if (*reinterpret_cast<signed char*>(&eax4) > 13) 
                break;
            addr_ef9f_4:
        } while (*reinterpret_cast<signed char*>(&eax4) > 8);
        goto addr_efa3_2;
    }
    if (eax4 - 48 > 9 || (rax5 = fun_3d40(), 1)) {
        addr_efa3_2:
        *reinterpret_cast<int32_t*>(&rax5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        goto addr_efa5_9;
    } else {
        ecx6 = malloc;
        if (!*reinterpret_cast<signed char*>(&ecx6)) 
            goto addr_efa5_9;
        rdx7 = reinterpret_cast<unsigned char*>(1);
        if (*reinterpret_cast<signed char*>(&ecx6) <= 13) 
            goto addr_f00a_12;
        while (*reinterpret_cast<signed char*>(&ecx6) == 32) {
            do {
                ecx6 = *rdx7;
                ++rdx7;
                if (!*reinterpret_cast<signed char*>(&ecx6)) 
                    goto addr_efa5_9;
                if (*reinterpret_cast<signed char*>(&ecx6) > 13) 
                    break;
                addr_f00a_12:
            } while (*reinterpret_cast<signed char*>(&ecx6) > 8);
            goto addr_efa3_2;
        }
    }
    if (*reinterpret_cast<signed char*>(&ecx6) == 44) {
        addr_efa5_9:
        rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v3) - reinterpret_cast<unsigned char>(g28));
        if (rdx8) {
            fun_3950();
        } else {
            return rax5;
        }
    } else {
        goto addr_efa3_2;
    }
}

uint64_t fun_3930();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_3930();
    if (r8d > 10) {
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x17380 + rax11 * 4) + 0x17380;
    }
}

struct s21 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_3a20();

struct s22 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s21* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s22* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0x10caf;
    rax8 = fun_37d0();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
        fun_37c0();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x1d190) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xc341]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x10d3b;
            fun_3a20();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s22*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x1d8c0) {
                fun_3750(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x10dca);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_3950();
        } else {
            return r14_19;
        }
    }
}

struct s23 {
    int64_t f0;
    signed char** f8;
    uint64_t* f10;
    signed char[8] pad32;
    int64_t f20;
    signed char* f28;
    signed char* f30;
    signed char* f38;
    signed char[8] pad72;
    void* f48;
    signed char[24] pad104;
    unsigned char f68;
    signed char[15] pad120;
    int64_t f78;
    signed char** f80;
    signed char** f88;
    signed char** f90;
    signed char[8] pad160;
    void* fa0;
    signed char[24] pad192;
    unsigned char fc0;
    signed char[15] pad208;
    int64_t fd0;
    uint64_t* fd8;
    uint64_t* fe0;
    uint64_t* fe8;
    signed char[8] pad248;
    void* ff8;
    signed char[24] pad280;
    unsigned char f118;
};

void _obstack_newchunk(int64_t rdi, struct s23* rsi);

void save_token(struct s23* rdi, struct s23* rsi) {
    signed char* rax3;
    signed char* r12_4;
    signed char* rax5;
    signed char* rdx6;
    signed char** rax7;
    signed char** rdx8;
    uint64_t* rdx9;
    uint64_t* rax10;

    rax3 = rdi->f30;
    r12_4 = rdi->f28;
    if (rax3 == r12_4) {
        rdi->f68 = reinterpret_cast<unsigned char>(rdi->f68 | 2);
    }
    rax5 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rax3) + reinterpret_cast<int64_t>(rdi->f48) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rdi->f48)));
    rdx6 = rdi->f38;
    if (reinterpret_cast<uint64_t>(rax5) - rdi->f20 <= reinterpret_cast<uint64_t>(rdx6) - rdi->f20) {
        rdx6 = rax5;
    }
    rax7 = rdi->f90;
    rdi->f30 = rdx6;
    rdi->f28 = rdx6;
    rdx8 = rdi->f88;
    if (reinterpret_cast<uint64_t>(rax7) - reinterpret_cast<uint64_t>(rdx8) <= 7) {
        _obstack_newchunk(reinterpret_cast<int64_t>(rdi) + 0x70, 8);
        rdx8 = rdi->f88;
    }
    *rdx8 = r12_4;
    rdx9 = rdi->fe0;
    rax10 = rdi->fe8;
    rdi->f88 = rdi->f88 + 1;
    if (reinterpret_cast<uint64_t>(rax10) - reinterpret_cast<uint64_t>(rdx9) <= 7) {
        _obstack_newchunk(reinterpret_cast<int64_t>(rdi) + 0xc8, 8);
        rdx9 = rdi->fe0;
    }
    *rdx9 = reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(r12_4) - 1;
    rdi->fe0 = rdi->fe0 + 1;
    rdi->f0 = rdi->f0 + 1;
    return;
}

uint32_t fun_3bc0(void** rdi, void** rsi);

uint32_t strcoll_loop(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r13_6;
    void** r12_7;
    void** rbp8;
    void** rax9;
    void** r14_10;
    uint32_t eax11;
    void** rax12;
    void** rbx13;
    void** rax14;
    void** rax15;

    r15_5 = rdi;
    r13_6 = rsi;
    r12_7 = rcx;
    rbp8 = rdx;
    rax9 = fun_37d0();
    r14_10 = rax9;
    do {
        *reinterpret_cast<void***>(r14_10) = reinterpret_cast<void**>(0);
        eax11 = fun_3bc0(r15_5, rbp8);
        if (eax11) 
            break;
        rax12 = fun_3940(r15_5, r15_5);
        rbx13 = rax12 + 1;
        rax14 = fun_3940(rbp8, rbp8);
        r15_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_5) + reinterpret_cast<unsigned char>(rbx13));
        rax15 = rax14 + 1;
        rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<unsigned char>(rax15));
        r12_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_7) - reinterpret_cast<unsigned char>(rax15));
        r13_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_6) - reinterpret_cast<unsigned char>(rbx13));
        if (!r13_6) 
            goto addr_14810_4;
    } while (r12_7);
    goto addr_14828_6;
    return eax11;
    addr_14810_4:
    return *reinterpret_cast<uint32_t*>(&rax15) - (*reinterpret_cast<uint32_t*>(&rax15) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax15) < *reinterpret_cast<uint32_t*>(&rax15) + reinterpret_cast<uint1_t>(!!r12_7)));
    addr_14828_6:
    return 1;
}

/* xfopen.part.0 */
void xfopen_part_0(void** rdi, void** rsi, void** rdx, ...) {
    void** rax4;

    rax4 = fun_3920();
    sort_die(rax4, rdi, 5);
    fun_3980();
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x1d1a8;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s24 {
    unsigned char f0;
    unsigned char f1;
};

void* getmonth(void** rdi, void** rsi, void** rdx) {
    int64_t rax4;
    void** rbx5;
    int64_t rax6;
    uint64_t r10_7;
    uint64_t r11_8;
    void** rcx9;
    uint64_t r9_10;
    struct s24* rdx11;
    uint32_t eax12;
    int64_t rsi13;
    void* eax14;

    *reinterpret_cast<uint32_t*>(&rax4) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    rbx5 = rsi;
    if (*reinterpret_cast<signed char*>(0x1d760 + rax4)) {
        do {
            *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
            ++rdi;
        } while (*reinterpret_cast<signed char*>(0x1d760 + rax6));
    }
    *reinterpret_cast<int32_t*>(&r10_7) = 12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_7) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r11_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_8) + 4) = 0;
    do {
        rcx9 = rdi;
        r9_10 = r11_8 + r10_7 >> 1;
        rdx11 = *reinterpret_cast<struct s24**>(0x1d060 + (r9_10 << 4));
        eax12 = rdx11->f0;
        if (!*reinterpret_cast<unsigned char*>(&eax12)) 
            break;
        do {
            *reinterpret_cast<uint32_t*>(&rsi13) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx9));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(0x1d460 + rsi13) < *reinterpret_cast<unsigned char*>(&eax12)) 
                break;
            if (*reinterpret_cast<unsigned char*>(0x1d460 + rsi13) > *reinterpret_cast<unsigned char*>(&eax12)) 
                goto addr_6d88_8;
            eax12 = rdx11->f1;
            rdx11 = reinterpret_cast<struct s24*>(&rdx11->f1);
            ++rcx9;
        } while (*reinterpret_cast<unsigned char*>(&eax12));
        goto addr_6d70_10;
        r10_7 = r9_10;
        continue;
        addr_6d88_8:
        r11_8 = r9_10 + 1;
    } while (r11_8 < r10_7);
    goto addr_6d63_13;
    addr_6d70_10:
    if (rbx5) {
        *reinterpret_cast<void***>(rbx5) = rcx9;
    }
    eax14 = *reinterpret_cast<void**>(0x1d060 + (r9_10 << 4) + 8);
    addr_6d81_17:
    return eax14;
    addr_6d63_13:
    eax14 = reinterpret_cast<void*>(0);
    goto addr_6d81_17;
}

void async_safe_die(void** edi, void** rsi, ...) {
    void** r12d3;
    void** rax4;
    void** rdx5;
    void** rcx6;
    void* rsp7;
    int64_t rdi8;
    void** rcx9;
    void** rax10;
    void** rcx11;
    void** rax12;
    void** rcx13;
    void** rcx14;
    void** rcx15;
    void** r8_16;

    r12d3 = edi;
    rax4 = fun_3940(rsi);
    rdx5 = rax4;
    fun_38b0(2, rsi, rdx5, rcx6);
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 40 - 8 + 8 - 8 + 8);
    if (!r12d3) 
        goto addr_76ca_2;
    while (1) {
        *reinterpret_cast<void***>(&rdi8) = r12d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        rax10 = inttostr(rdi8, reinterpret_cast<int64_t>(rsp7) + 12, rdx5, rcx9);
        fun_38b0(2, ": errno ", 8, rcx11);
        rax12 = fun_3940(rax10, rax10);
        fun_38b0(2, rax10, rax12, rcx13);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        addr_76ca_2:
        *reinterpret_cast<int32_t*>(&rdx5) = 1;
        *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
        fun_38b0(2, "\n", 1, rcx14);
        fun_3800(2, "\n", 1, rcx15, r8_16);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8 - 8 + 8);
    }
}

int32_t fun_37e0(void** rdi, void** rsi, void** rdx, ...);

void zaptemp(void** rdi, void** rsi, void** rdx) {
    void** r12_4;
    void** rbx5;
    void* rsp6;
    void** rax7;
    void** v8;
    void** rbp9;
    void** r13d10;
    void** rdi11;
    void** rsi12;
    struct s3* rax13;
    void** r15_14;
    void** rsi15;
    void** r14_16;
    int32_t eax17;
    int32_t eax18;
    void** rax19;
    void** r8d20;
    void* rax21;
    void** rdx22;
    void** rcx23;
    int64_t v24;
    void** rdi25;

    r12_4 = rdi;
    rbx5 = reinterpret_cast<void**>(0x1d388);
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8);
    rax7 = g28;
    v8 = rax7;
    rbp9 = temphead;
    if (rdi != rbp9 + 13) {
        do {
            rbx5 = rbp9;
            rbp9 = *reinterpret_cast<void***>(rbp9);
        } while (r12_4 != rbp9 + 13);
    }
    if (*reinterpret_cast<unsigned char*>(rbp9 + 12) == 1 && (r13d10 = *reinterpret_cast<void***>(rbp9 + 8), rdi11 = proctab, rsi12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 16), rax13 = hash_remove(rdi11, rsi12, rdx), rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8), !!rax13)) {
        rax13->fc = 2;
        reap(r13d10, rsi12);
        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
    }
    r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 24);
    rsi15 = reinterpret_cast<void**>(0x1d3a0);
    r14_16 = *reinterpret_cast<void***>(rbp9);
    eax17 = fun_3c00();
    eax18 = fun_37e0(r12_4, 0x1d3a0, r15_14, r12_4, 0x1d3a0, r15_14);
    rax19 = fun_37d0();
    r8d20 = *reinterpret_cast<void***>(rax19);
    *reinterpret_cast<void***>(rbx5) = r14_16;
    if (eax17) {
        if (eax18) 
            goto addr_8d26_7; else 
            goto addr_8ccb_8;
    }
    rsi15 = r15_14;
    fun_3c00();
    r8d20 = r8d20;
    if (!eax18) {
        addr_8ccb_8:
        if (!r14_16) {
            addr_8d6c_10:
            temptail = rbx5;
            goto addr_8cd4_11;
        } else {
            addr_8cd4_11:
            fun_3750(rbp9, rsi15, rbp9, rsi15);
            rax21 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
            if (!rax21) {
                return;
            }
        }
    } else {
        addr_8d26_7:
        quotearg_n_style_colon();
        fun_3920();
        rsi15 = r8d20;
        *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
        fun_3c90();
        if (r14_16) 
            goto addr_8cd4_11; else 
            goto addr_8d6c_10;
    }
    fun_3950();
    rdx22 = *reinterpret_cast<void***>(rsi15 + 8);
    rcx23 = *reinterpret_cast<void***>(rsi15 + 16);
    if (!0) 
        goto addr_8ddb_15;
    if (rdx22 == *reinterpret_cast<void***>(rsi15 + 24) || *reinterpret_cast<void***>(rsi15 + 40)) {
        addr_8e1f_17:
        goto v24;
    }
    addr_8de0_19:
    fun_3e80(rbp9 + 8, rsi15, rdx22, rcx23);
    rdi25 = *reinterpret_cast<void***>(rbp9);
    heap_insert(rdi25, rsi15, rdx22, rcx23);
    *reinterpret_cast<unsigned char*>(rsi15 + 84) = 1;
    fun_3a90(rbp9 + 48, rsi15, rdx22, rcx23);
    addr_8ddb_15:
    if (rdx22 != *reinterpret_cast<void***>(rsi15 + 24)) 
        goto addr_8de0_19;
    if (!*reinterpret_cast<void***>(rsi15 + 48)) 
        goto addr_8de0_19; else 
        goto addr_8e1f_17;
}

void** open_input_files(void** rdi, void** rsi, void** rdx) {
    void** r13_4;
    void** rbp5;
    void** rbx6;
    void** rax7;
    void** v8;
    void** rax9;
    void* rsp10;
    void** v11;
    void** r12_12;
    void** r15_13;
    uint32_t eax14;
    void** rdi15;
    void** rax16;
    void** r9d17;
    void** rdi18;
    void** rsi19;
    struct s3* rax20;
    int32_t eax21;
    void** eax22;
    int64_t rdi23;
    int32_t v24;
    void** rax25;
    void* rax26;
    void** rsi27;
    void** rax28;
    void** rax29;
    void** rax30;
    void** r13d31;
    void** rax32;
    int32_t v33;
    void** rax34;
    void** edi35;

    r13_4 = rsi;
    rbp5 = rdx;
    rbx6 = rdi;
    rax7 = g28;
    v8 = rax7;
    rax9 = xnmalloc(r13_4, 8, rdx);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    v11 = rax9;
    *reinterpret_cast<void***>(rbp5) = rax9;
    if (!r13_4) {
        *reinterpret_cast<int32_t*>(&r12_12) = 0;
        *reinterpret_cast<int32_t*>(&r12_12 + 4) = 0;
    } else {
        rbp5 = rax9;
        *reinterpret_cast<int32_t*>(&r12_12) = 0;
        *reinterpret_cast<int32_t*>(&r12_12 + 4) = 0;
        do {
            r15_13 = *reinterpret_cast<void***>(rbx6 + 8);
            if (!r15_13 || (eax14 = *reinterpret_cast<unsigned char*>(r15_13 + 12), *reinterpret_cast<signed char*>(&eax14) == 0)) {
                rdi15 = *reinterpret_cast<void***>(rbx6);
                rax16 = stream_open(rdi15, "r", rdx);
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                *reinterpret_cast<void***>(rbp5) = rax16;
                if (!rax16) 
                    break;
            } else {
                if (*reinterpret_cast<signed char*>(&eax14) == 1 && (r9d17 = *reinterpret_cast<void***>(r15_13 + 8), rdi18 = proctab, rsi19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 16), rax20 = hash_remove(rdi18, rsi19, rdx), rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), !!rax20)) {
                    rax20->fc = 2;
                    reap(r9d17, rsi19);
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                }
                eax21 = fun_3cd0(r15_13 + 13);
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                if (eax21 < 0) 
                    goto addr_87a0_9;
                eax22 = pipe_fork(reinterpret_cast<int64_t>(rsp10) + 16, 9, rdx);
                if (reinterpret_cast<int1_t>(eax22 == 0xffffffff)) 
                    goto addr_86f1_11;
                if (!eax22) 
                    goto addr_87fa_13;
                *reinterpret_cast<void***>(r15_13 + 8) = eax22;
                register_proc(r15_13, 9, rdx);
                fun_3a40();
                fun_3a40();
                *reinterpret_cast<int32_t*>(&rdi23) = v24;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0;
                rax25 = fun_3c20(rdi23, "r", rdx);
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                if (!rax25) 
                    goto addr_889c_15;
                *reinterpret_cast<void***>(rbp5) = rax25;
            }
            ++r12_12;
            rbx6 = rbx6 + 16;
            rbp5 = rbp5 + 8;
        } while (r13_4 != r12_12);
    }
    addr_8727_18:
    while (rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28)), !!rax26) {
        addr_885c_19:
        fun_3950();
        addr_8861_20:
        rsi27 = compress_program;
        rax28 = quotearg_style(4, rsi27, rdx, 4, rsi27, rdx);
        r12_12 = rax28;
        rax29 = fun_3920();
        rdx = rax29;
        fun_3c90();
        addr_889c_15:
        rax30 = fun_37d0();
        r13d31 = *reinterpret_cast<void***>(rax30);
        fun_3a40();
        *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax30) = r13d31;
    }
    return r12_12;
    addr_87a0_9:
    *reinterpret_cast<void***>(v11 + reinterpret_cast<unsigned char>(r12_12) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_18;
    addr_86f1_11:
    rax32 = fun_37d0();
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax32) == 24)) 
        goto addr_8861_20;
    fun_3a40();
    *reinterpret_cast<void***>(rax32) = reinterpret_cast<void**>(24);
    *reinterpret_cast<void***>(v11 + reinterpret_cast<unsigned char>(r12_12) * 8) = reinterpret_cast<void**>(0);
    goto addr_8727_18;
    addr_87fa_13:
    fun_3a40();
    if (eax21) {
        move_fd_part_0(eax21);
    }
    if (v33 != 1) {
        move_fd_part_0(v33, v33);
    }
    rdx = reinterpret_cast<void**>("-d");
    fun_3e20();
    rax34 = fun_37d0();
    edi35 = *reinterpret_cast<void***>(rax34);
    async_safe_die(edi35, "couldn't execute compress program (with -d)", edi35, "couldn't execute compress program (with -d)");
    goto addr_885c_19;
}

struct s25 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s25* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s25* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x17311);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x1730c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x17315);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x17308);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t quotearg_n_style_mem();

void collate_error(void** edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    fun_3920();
    fun_3c90();
    fun_3920();
    fun_3c90();
    quotearg_n_style_mem();
    quotearg_n_style_mem();
    fun_3920();
}

int64_t __gmon_start__ = 0;

void fun_3003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g1cc18 = 0;

void fun_3033() {
    __asm__("cli ");
    goto g1cc18;
}

void fun_3043() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3053() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3063() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3073() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3083() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3093() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3103() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3113() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3123() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3133() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3143() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3153() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3163() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3173() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3183() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3193() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3203() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3213() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3223() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3233() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3243() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3253() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3263() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3273() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3283() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3293() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3303() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3313() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3323() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3333() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3343() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3353() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3363() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3373() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3383() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3393() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3403() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3413() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3423() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3433() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3443() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3453() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3463() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3473() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3483() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3493() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_34f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3503() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3513() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3523() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3533() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3543() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3553() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3563() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3573() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3583() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3593() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_35f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3603() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3613() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3623() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3633() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3643() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3653() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3663() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3673() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3683() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3693() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_36a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_36b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_36c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_36d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_36e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_36f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3703() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3713() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3723() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3733() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3743() {
    __asm__("cli ");
    goto 0x3020;
}

int64_t free = 0;

void fun_3753() {
    __asm__("cli ");
    goto free;
}

int64_t malloc = 0;

void fun_3763() {
    __asm__("cli ");
    goto malloc;
}

int64_t __cxa_finalize = 0;

void fun_3773() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x3030;

void fun_3783() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t getenv = 0x3040;

void fun_3793() {
    __asm__("cli ");
    goto getenv;
}

int64_t __snprintf_chk = 0x3050;

void fun_37a3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t raise = 0x3060;

void fun_37b3() {
    __asm__("cli ");
    goto raise;
}

int64_t abort = 0x3070;

void fun_37c3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x3080;

void fun_37d3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t unlink = 0x3090;

void fun_37e3() {
    __asm__("cli ");
    goto unlink;
}

int64_t strncmp = 0x30a0;

void fun_37f3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x30b0;

void fun_3803() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x30c0;

void fun_3813() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x30d0;

void fun_3823() {
    __asm__("cli ");
    goto __fpending;
}

int64_t ferror = 0x30e0;

void fun_3833() {
    __asm__("cli ");
    goto ferror;
}

int64_t qsort = 0x30f0;

void fun_3843() {
    __asm__("cli ");
    goto qsort;
}

int64_t sigaction = 0x3100;

void fun_3853() {
    __asm__("cli ");
    goto sigaction;
}

int64_t iswcntrl = 0x3110;

void fun_3863() {
    __asm__("cli ");
    goto iswcntrl;
}

int64_t reallocarray = 0x3120;

void fun_3873() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t localeconv = 0x3130;

void fun_3883() {
    __asm__("cli ");
    goto localeconv;
}

int64_t fcntl = 0x3140;

void fun_3893() {
    __asm__("cli ");
    goto fcntl;
}

int64_t clearerr_unlocked = 0x3150;

void fun_38a3() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t write = 0x3160;

void fun_38b3() {
    __asm__("cli ");
    goto write;
}

int64_t fread_unlocked = 0x3170;

void fun_38c3() {
    __asm__("cli ");
    goto fread_unlocked;
}

int64_t textdomain = 0x3180;

void fun_38d3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t pthread_cond_wait = 0x3190;

void fun_38e3() {
    __asm__("cli ");
    goto pthread_cond_wait;
}

int64_t fclose = 0x31a0;

void fun_38f3() {
    __asm__("cli ");
    goto fclose;
}

int64_t __sched_cpucount = 0x31b0;

void fun_3903() {
    __asm__("cli ");
    goto __sched_cpucount;
}

int64_t bindtextdomain = 0x31c0;

void fun_3913() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x31d0;

void fun_3923() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x31e0;

void fun_3933() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x31f0;

void fun_3943() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x3200;

void fun_3953() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x3210;

void fun_3963() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x3220;

void fun_3973() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x3230;

void fun_3983() {
    __asm__("cli ");
    goto dup2;
}

int64_t strchr = 0x3240;

void fun_3993() {
    __asm__("cli ");
    goto strchr;
}

int64_t pthread_mutex_destroy = 0x3250;

void fun_39a3() {
    __asm__("cli ");
    goto pthread_mutex_destroy;
}

int64_t nanosleep = 0x3260;

void fun_39b3() {
    __asm__("cli ");
    goto nanosleep;
}

int64_t __overflow = 0x3270;

void fun_39c3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x3280;

void fun_39d3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t ftruncate = 0x3290;

void fun_39e3() {
    __asm__("cli ");
    goto ftruncate;
}

int64_t mkostemp = 0x32a0;

void fun_39f3() {
    __asm__("cli ");
    goto mkostemp;
}

int64_t lseek = 0x32b0;

void fun_3a03() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x32c0;

void fun_3a13() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x32d0;

void fun_3a23() {
    __asm__("cli ");
    goto memset;
}

int64_t fgetc = 0x32e0;

void fun_3a33() {
    __asm__("cli ");
    goto fgetc;
}

int64_t close = 0x32f0;

void fun_3a43() {
    __asm__("cli ");
    goto close;
}

int64_t pipe = 0x3300;

void fun_3a53() {
    __asm__("cli ");
    goto pipe;
}

int64_t posix_fadvise = 0x3310;

void fun_3a63() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memchr = 0x3320;

void fun_3a73() {
    __asm__("cli ");
    goto memchr;
}

int64_t memcmp = 0x3330;

void fun_3a83() {
    __asm__("cli ");
    goto memcmp;
}

int64_t pthread_cond_signal = 0x3340;

void fun_3a93() {
    __asm__("cli ");
    goto pthread_cond_signal;
}

int64_t fputs_unlocked = 0x3350;

void fun_3aa3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x3360;

void fun_3ab3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strxfrm = 0x3370;

void fun_3ac3() {
    __asm__("cli ");
    goto strxfrm;
}

int64_t strcmp = 0x3380;

void fun_3ad3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t signal = 0x3390;

void fun_3ae3() {
    __asm__("cli ");
    goto signal;
}

int64_t fputc_unlocked = 0x33a0;

void fun_3af3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t sigemptyset = 0x33b0;

void fun_3b03() {
    __asm__("cli ");
    goto sigemptyset;
}

int64_t stat = 0x33c0;

void fun_3b13() {
    __asm__("cli ");
    goto stat;
}

int64_t strtol = 0x33d0;

void fun_3b23() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x33e0;

void fun_3b33() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x33f0;

void fun_3b43() {
    __asm__("cli ");
    goto fileno;
}

int64_t __stpcpy_chk = 0x3400;

void fun_3b53() {
    __asm__("cli ");
    goto __stpcpy_chk;
}

int64_t pthread_cond_init = 0x3410;

void fun_3b63() {
    __asm__("cli ");
    goto pthread_cond_init;
}

int64_t wcwidth = 0x3420;

void fun_3b73() {
    __asm__("cli ");
    goto wcwidth;
}

int64_t pthread_mutex_unlock = 0x3430;

void fun_3b83() {
    __asm__("cli ");
    goto pthread_mutex_unlock;
}

int64_t pause = 0x3440;

void fun_3b93() {
    __asm__("cli ");
    goto pause;
}

int64_t fflush = 0x3450;

void fun_3ba3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x3460;

void fun_3bb3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t strcoll = 0x3470;

void fun_3bc3() {
    __asm__("cli ");
    goto strcoll;
}

int64_t mkstemp = 0x3480;

void fun_3bd3() {
    __asm__("cli ");
    goto mkstemp;
}

int64_t __freading = 0x3490;

void fun_3be3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x34a0;

void fun_3bf3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t pthread_sigmask = 0x34b0;

void fun_3c03() {
    __asm__("cli ");
    goto pthread_sigmask;
}

int64_t realloc = 0x34c0;

void fun_3c13() {
    __asm__("cli ");
    goto realloc;
}

int64_t fdopen = 0x34d0;

void fun_3c23() {
    __asm__("cli ");
    goto fdopen;
}

int64_t setlocale = 0x34e0;

void fun_3c33() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x34f0;

void fun_3c43() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t setvbuf = 0x3500;

void fun_3c53() {
    __asm__("cli ");
    goto setvbuf;
}

int64_t pthread_create = 0x3510;

void fun_3c63() {
    __asm__("cli ");
    goto pthread_create;
}

int64_t memmove = 0x3520;

void fun_3c73() {
    __asm__("cli ");
    goto memmove;
}

int64_t strtold = 0x3530;

void fun_3c83() {
    __asm__("cli ");
    goto strtold;
}

int64_t error = 0x3540;

void fun_3c93() {
    __asm__("cli ");
    goto error;
}

int64_t waitpid = 0x3550;

void fun_3ca3() {
    __asm__("cli ");
    goto waitpid;
}

int64_t __explicit_bzero_chk = 0x3560;

void fun_3cb3() {
    __asm__("cli ");
    goto __explicit_bzero_chk;
}

int64_t pthread_cond_destroy = 0x3570;

void fun_3cc3() {
    __asm__("cli ");
    goto pthread_cond_destroy;
}

int64_t open = 0x3580;

void fun_3cd3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x3590;

void fun_3ce3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x35a0;

void fun_3cf3() {
    __asm__("cli ");
    goto fopen;
}

int64_t dcngettext = 0x35b0;

void fun_3d03() {
    __asm__("cli ");
    goto dcngettext;
}

int64_t sysconf = 0x35c0;

void fun_3d13() {
    __asm__("cli ");
    goto sysconf;
}

int64_t strtoumax = 0x35d0;

void fun_3d23() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t euidaccess = 0x35e0;

void fun_3d33() {
    __asm__("cli ");
    goto euidaccess;
}

int64_t strtoul = 0x35f0;

void fun_3d43() {
    __asm__("cli ");
    goto strtoul;
}

int64_t __cxa_atexit = 0x3600;

void fun_3d53() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t sysinfo = 0x3610;

void fun_3d63() {
    __asm__("cli ");
    goto sysinfo;
}

int64_t pipe2 = 0x3620;

void fun_3d73() {
    __asm__("cli ");
    goto pipe2;
}

int64_t sigismember = 0x3630;

void fun_3d83() {
    __asm__("cli ");
    goto sigismember;
}

int64_t exit = 0x3640;

void fun_3d93() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x3650;

void fun_3da3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x3660;

void fun_3db3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t getrlimit = 0x3670;

void fun_3dc3() {
    __asm__("cli ");
    goto getrlimit;
}

int64_t getrandom = 0x3680;

void fun_3dd3() {
    __asm__("cli ");
    goto getrandom;
}

int64_t sched_getaffinity = 0x3690;

void fun_3de3() {
    __asm__("cli ");
    goto sched_getaffinity;
}

int64_t fflush_unlocked = 0x36a0;

void fun_3df3() {
    __asm__("cli ");
    goto fflush_unlocked;
}

int64_t pthread_join = 0x36b0;

void fun_3e03() {
    __asm__("cli ");
    goto pthread_join;
}

int64_t mbsinit = 0x36c0;

void fun_3e13() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t execlp = 0x36d0;

void fun_3e23() {
    __asm__("cli ");
    goto execlp;
}

int64_t iswprint = 0x36e0;

void fun_3e33() {
    __asm__("cli ");
    goto iswprint;
}

int64_t pthread_mutex_init = 0x36f0;

void fun_3e43() {
    __asm__("cli ");
    goto pthread_mutex_init;
}

int64_t fstat = 0x3700;

void fun_3e53() {
    __asm__("cli ");
    goto fstat;
}

int64_t sigaddset = 0x3710;

void fun_3e63() {
    __asm__("cli ");
    goto sigaddset;
}

int64_t fork = 0x3720;

void fun_3e73() {
    __asm__("cli ");
    goto fork;
}

int64_t pthread_mutex_lock = 0x3730;

void fun_3e83() {
    __asm__("cli ");
    goto pthread_mutex_lock;
}

int64_t __ctype_b_loc = 0x3740;

void fun_3e93() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void** fun_3790(int64_t rdi, void** rsi, void** rdx);

int32_t posix2_version(int64_t rdi);

void set_program_name(void** rdi);

void** fun_3c30(int64_t rdi, ...);

void fun_3910(int64_t rdi, void** rsi);

void fun_38d0(int64_t rdi, void** rsi);

uint32_t exit_failure = 1;

int32_t hard_locale();

signed char hard_LC_TIME = 0;

struct s27 {
    unsigned char f0;
    signed char f1;
};

struct s26 {
    struct s27* f0;
    void** f8;
};

struct s26* fun_3880(void** rdi, void** rsi);

void** xcalloc(void** rdi, int64_t rsi, void** rdx);

int32_t fun_3d90();

uint32_t optind = 0;

void** optarg = reinterpret_cast<void**>(0);

signed char thousands_sep_ignored = 0;

void** fun_3d00();

void** umaxtostr(void** rdi, void* rsi, void** rdx);

void** fun_3b50(void* rdi, void** rsi, void** rdx);

void fun_3810(void** rdi, void** rsi, void** rdx);

void** quote_n();

void xstrtol_fatal(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** program_name = reinterpret_cast<void**>(0);

void** stderr = reinterpret_cast<void**>(0);

void fun_3db0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, void** a8, void** a9, void** a10, void** a11, void** a12);

void** uinttostr(int64_t rdi, void* rsi, void** rdx, void** rcx, void** r8);

uint32_t fun_3960(int64_t rdi);

void** Version = reinterpret_cast<void**>(24);

void version_etc(void** rdi);

struct s28 {
    unsigned char f0;
    signed char f1;
};

void usage();

void** argmatch_die = reinterpret_cast<void**>(0x90);

struct s29 {
    signed char[87136] pad87136;
    signed char f15460;
    signed char[5] pad87142;
    signed char f15466;
};

struct s29* __xargmatch_internal(int64_t rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9);

void physmem_total(void** rdi, void** rsi, void** rdx);

void readtokens0_init(void** rdi, void** rsi, void** rdx);

signed char readtokens0(void** rdi, void** rsi, void** rdx);

void** randread_new(void** rdi, int64_t rsi, void** rdx);

void randread(void** rdi, void** rsi, int64_t rdx);

int32_t randread_free(void** rdi, void** rsi, int64_t rdx);

void md5_init_ctx(int64_t rdi, void** rsi, int64_t rdx);

/* opts.7 */
signed char opts_7 = 88;

int32_t fun_3d30();

void** num_processors(int64_t rdi, void** rsi, void** rdx);

/* size_bound.0 */
void** size_bound_0 = reinterpret_cast<void**>(0);

int32_t fun_3dc0(void*** rdi, void** rsi, void** rdx, ...);

void physmem_available();

void** g5;

void** g9;

int32_t gd;

int32_t g11;

int64_t heap_alloc(int64_t rdi, void** rsi, void** rdx);

void fun_3b60(void** rdi);

void fun_39a0(void** rdi, void** rsi);

void heap_free(int64_t rdi, void** rsi);

void fun_3cc0(void** rdi, void** rsi);

/* opts.6 */
signed char opts_6 = 0;

void** fun_3e90(void** rdi, void** rsi, void** rdx, ...);

struct s30 {
    int32_t f0;
    signed char[36] pad40;
    int32_t f28;
};

struct s30** fun_3780(void** rdi, void** rsi);

signed char g1d76a = 0;

signed char g1d56a = 0;

signed char g1d66a = 0;

signed char g1d46a = 0;

struct s31 {
    void** f0;
    signed char[7] pad8;
    int32_t f8;
};

void** fun_3bb0(int64_t rdi);

void fun_3840(int64_t rdi, uint64_t rsi, int64_t rdx);

void fun_3b00(int64_t rdi, uint64_t rsi, int64_t rdx);

void fun_3850(int64_t rdi, ...);

void fun_3e60(int64_t rdi, int64_t rsi, void** rdx);

int32_t fun_3d80(int64_t rdi, int64_t rsi, void** rdx);

void fun_3ae0(int64_t rdi);

void atexit(int64_t rdi);

void fun_3f13(uint32_t edi, void** rsi, void** rdx) {
    void** r13_4;
    void** v5;
    void** rax6;
    void** v7;
    void** v8;
    int32_t eax9;
    void** rdi10;
    void** v11;
    void** rax12;
    void** rsi13;
    void** v14;
    int32_t eax15;
    void** rdi16;
    int32_t eax17;
    void** r15_18;
    struct s26* rax19;
    void* rsp20;
    struct s27* rcx21;
    uint32_t edx22;
    void** rdx23;
    uint32_t eax24;
    void** r14_25;
    int64_t rax26;
    uint32_t eax27;
    void** v28;
    void** rax29;
    void** rdi30;
    void* rsp31;
    void** rax32;
    void** v33;
    void* rsp34;
    void** rdi35;
    void** rax36;
    void** rcx37;
    void** r14_38;
    void** rax39;
    void* rsp40;
    void** rdi41;
    void** rax42;
    void** rdx43;
    void** rbp44;
    void** v45;
    void** rdx46;
    void* rsp47;
    int1_t zf48;
    void** rdi49;
    int64_t rax50;
    void** rax51;
    void** rax52;
    void** rsi53;
    uint32_t eax54;
    void** r12_55;
    void** rdi56;
    void** v57;
    int64_t v58;
    void** rax59;
    void** rax60;
    int64_t rax61;
    void** rax62;
    void** rax63;
    int64_t v64;
    void** rax65;
    void** v66;
    void** rbx67;
    void** rax68;
    void* rsp69;
    uint64_t rdx70;
    int1_t less_or_equal71;
    int1_t zf72;
    void** rax73;
    void* rsp74;
    signed char al75;
    uint64_t v76;
    int1_t zf77;
    int1_t zf78;
    void** r8_79;
    void** rax80;
    int1_t zf81;
    int1_t zf82;
    int1_t zf83;
    void** rax84;
    int1_t zf85;
    int1_t zf86;
    int1_t zf87;
    int1_t zf88;
    void** rax89;
    void** rax90;
    void** rax91;
    void** rax92;
    void* rsp93;
    void** r9_94;
    void** rdi95;
    void** r10_96;
    void** rax97;
    void** rdx98;
    void** rax99;
    void* rsp100;
    uint32_t ebx101;
    void** v102;
    void** v103;
    void** v104;
    void** rbp105;
    uint32_t r12d106;
    void** v107;
    void** rbx108;
    unsigned char v109;
    void** v110;
    uint32_t eax111;
    uint32_t edi112;
    int1_t zf113;
    uint32_t edi114;
    void** rbp115;
    void* r15_116;
    void** rax117;
    void* rsp118;
    void** rax119;
    void** rax120;
    void* rsp121;
    void** rax122;
    void* rsp123;
    void** rcx124;
    void** rax125;
    void* rdi126;
    void** rax127;
    void** rax128;
    uint32_t eax129;
    uint32_t eax130;
    void** rax131;
    uint32_t eax132;
    uint32_t edi133;
    int1_t zf134;
    uint32_t edi135;
    void** v136;
    int1_t zf137;
    uint32_t eax138;
    void** rax139;
    int1_t zf140;
    void** rdi141;
    void** rax142;
    void** rax143;
    void** rax144;
    void** rax145;
    void** rsi146;
    void* rsp147;
    void** rsi148;
    void** rax149;
    void* rsp150;
    void** rax151;
    void* rsp152;
    void** rax153;
    void** rax154;
    void** rsi155;
    void** rdi156;
    void** r11_157;
    void* rsp158;
    void** rax159;
    int1_t zf160;
    void** rax161;
    void** rax162;
    void** rax163;
    void* rsp164;
    void** rax165;
    void** rdx166;
    uint32_t ebx167;
    void** v168;
    void** rax169;
    uint32_t ebx170;
    uint32_t ebx171;
    void** rax172;
    void** rbp173;
    void** v174;
    void* rsp175;
    void** rsi176;
    signed char al177;
    void** rdi178;
    void** v179;
    void** rax180;
    void** r14_181;
    void** r13_182;
    void** rax183;
    void** rdx184;
    void** rsi185;
    void** v186;
    void** rax187;
    void** v188;
    void** rbp189;
    void** rax190;
    void** rax191;
    void** rdi192;
    int64_t v193;
    void** rax194;
    void** rsi195;
    void** rdi196;
    void** rsi197;
    void** rax198;
    void** rax199;
    void* rsp200;
    int64_t rdi201;
    void** rax202;
    void** rdi203;
    void** eax204;
    int64_t rdi205;
    int64_t rax206;
    void** rdi207;
    int32_t eax208;
    struct s28* v209;
    uint32_t eax210;
    void** rsi211;
    struct s29* rax212;
    void* rsp213;
    void** rax214;
    uint1_t below_or_equal215;
    void** rsi216;
    int1_t cf217;
    int64_t rdx218;
    void** rsi219;
    void** rax220;
    void** rax221;
    void** rdi222;
    int64_t v223;
    void** rax224;
    void** rax225;
    signed char al226;
    void** v227;
    void** v228;
    void** rdi229;
    signed char al230;
    unsigned char v231;
    uint32_t edx232;
    uint32_t edx233;
    int1_t zf234;
    uint32_t eax235;
    unsigned char v236;
    void** rax237;
    void* rsp238;
    int32_t eax239;
    int1_t zf240;
    uint32_t eax241;
    unsigned char v242;
    int1_t zf243;
    void** rax244;
    void** rdi245;
    void** rax246;
    int64_t rax247;
    void** rax248;
    void** rsi249;
    void** rax250;
    void** rax251;
    signed char al252;
    void** v253;
    void** v254;
    uint32_t eax255;
    unsigned char v256;
    uint32_t eax257;
    unsigned char v258;
    uint32_t eax259;
    unsigned char v260;
    uint32_t eax261;
    unsigned char v262;
    uint32_t eax263;
    unsigned char v264;
    uint32_t eax265;
    unsigned char v266;
    uint32_t eax267;
    unsigned char v268;
    uint32_t eax269;
    unsigned char v270;
    uint32_t eax271;
    unsigned char v272;
    uint32_t edi273;
    uint32_t eax274;
    int32_t eax275;
    void** v276;
    int32_t eax277;
    void** v278;
    void** rax279;
    void** rax280;
    void** rax281;
    void** rbx282;
    void** v283;
    void** v284;
    void** rsi285;
    void** rdi286;
    void** rax287;
    uint64_t rdx288;
    void** rax289;
    void** v290;
    int32_t eax291;
    unsigned char al292;
    void** eax293;
    void* rsp294;
    int32_t eax295;
    int32_t eax296;
    uint32_t v297;
    int1_t zf298;
    int1_t zf299;
    void** v300;
    void** rax301;
    void** rsi302;
    void** r13_303;
    int32_t eax304;
    int32_t eax305;
    int32_t eax306;
    uint1_t cf307;
    uint1_t zf308;
    uint1_t below_or_equal309;
    void** rax310;
    uint1_t below_or_equal311;
    void** rdx312;
    uint64_t rdx313;
    int64_t rax314;
    void** rax315;
    void** v316;
    void** v317;
    void** rax318;
    signed char al319;
    void* rsp320;
    void** rax321;
    void** rax322;
    void** v323;
    void* rsp324;
    int64_t rax325;
    void* rsp326;
    int64_t v327;
    void** rax328;
    void** rax329;
    void** rax330;
    void** rsi331;
    void* rsp332;
    void** rdi333;
    void** v334;
    uint64_t v335;
    uint32_t eax336;
    void** rax337;
    struct s30** rax338;
    void* rsp339;
    void** rdx340;
    struct s30** rdi341;
    uint64_t rsi342;
    uint32_t eax343;
    uint32_t eax344;
    uint32_t eax345;
    int32_t eax346;
    uint32_t edx347;
    int32_t r14d348;
    uint32_t eax349;
    int64_t rdx350;
    struct s30* rax351;
    int32_t eax352;
    void** v353;
    int64_t r14_354;
    struct s31* r13_355;
    int64_t rdi356;
    void** rax357;
    void** rax358;
    void** rbp359;
    void** rax360;
    void** rdi361;
    void** rdx362;
    int64_t rsi363;
    uint32_t ecx364;
    int32_t* rbp365;
    void* rsp366;
    void** rax367;
    int32_t r12d368;
    void** r15_369;
    int64_t rdi370;
    int64_t rsi371;
    int32_t* r12_372;
    int32_t r13d373;
    int64_t rsi374;
    int32_t eax375;
    int64_t rdi376;
    void** rax377;
    void** rax378;
    void** rax379;
    void** rdi380;
    void** rax381;
    void* rsp382;
    uint32_t eax383;
    int1_t zf384;
    void** rax385;
    void** rax386;

    __asm__("cli ");
    r13_4 = rsi;
    *reinterpret_cast<uint32_t*>(&v5) = edi;
    rax6 = fun_3790("POSIXLY_CORRECT", rsi, rdx);
    v7 = rax6;
    *reinterpret_cast<unsigned char*>(&v8) = reinterpret_cast<uint1_t>(!!rax6);
    eax9 = posix2_version("POSIXLY_CORRECT");
    rdi10 = *reinterpret_cast<void***>(r13_4);
    *reinterpret_cast<unsigned char*>(&v11) = reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(eax9 - 0x30db0) > 0x2b8);
    set_program_name(rdi10);
    rax12 = fun_3c30(6, 6);
    rsi13 = reinterpret_cast<void**>("/usr/local/share/locale");
    v14 = rax12;
    fun_3910("coreutils", "/usr/local/share/locale");
    fun_38d0("coreutils", "/usr/local/share/locale");
    exit_failure = 2;
    eax15 = hard_locale();
    *reinterpret_cast<int32_t*>(&rdi16) = 2;
    *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
    hard_LC_COLLATE = *reinterpret_cast<signed char*>(&eax15);
    eax17 = hard_locale();
    hard_LC_TIME = *reinterpret_cast<signed char*>(&eax17);
    *reinterpret_cast<int32_t*>(&r15_18) = eax17;
    rax19 = fun_3880(2, "/usr/local/share/locale");
    rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x408 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    rcx21 = rax19->f0;
    edx22 = rcx21->f0;
    decimal_point = *reinterpret_cast<unsigned char*>(&edx22);
    if (!*reinterpret_cast<unsigned char*>(&edx22) || rcx21->f1) {
        decimal_point = 46;
    }
    rdx23 = rax19->f8;
    eax24 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx23))));
    thousands_sep = eax24;
    if (!eax24) 
        goto addr_46b2_4;
    if (*reinterpret_cast<void***>(rdx23 + 1)) 
        goto addr_46ab_6; else 
        goto addr_4018_7;
    addr_68ec_8:
    goto addr_68f8_9;
    addr_6891_10:
    xfopen_part_0(r14_25, "r", rdx23);
    quote(r14_25, r14_25);
    fun_3920();
    fun_3c90();
    fun_3920();
    fun_3c90();
    goto addr_68ec_8;
    addr_44b7_13:
    *reinterpret_cast<uint32_t*>(&rax26) = eax27;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x150cc + rax26 * 4) + 0x150cc;
    while (1) {
        addr_690c_14:
        if (!v28) {
            v28 = reinterpret_cast<void**>("getrandom");
        }
        addr_68f8_9:
        rax29 = fun_3920();
        sort_die(rax29, v28, 5, rax29, v28, 5);
    }
    while (1) {
        addr_6937_17:
        rax32 = parse_field_count(rdi30 + 1, reinterpret_cast<int64_t>(rsp31) + 0xd8, "invalid number after '.'");
        rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
        rdi30 = rax32;
        goto addr_572b_18;
        addr_6926_19:
        rdi30 = v33;
        badfieldspec(rdi30, "stray character in field spec", 1);
        rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
        continue;
        while (1) {
            addr_6976_20:
            rsi13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0xc8);
            rax36 = parse_field_count(rdi35 + 1, rsi13, 0);
            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            rdi35 = rax36;
            if (1) 
                goto addr_56bb_21;
            goto addr_56af_23;
            addr_6957_24:
            rcx37 = reinterpret_cast<void**>("main");
            rdi35 = reinterpret_cast<void**>("s");
            fun_3a10("s", "src/sort.c", "ale", "main");
            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            continue;
            while (1) {
                addr_57de_25:
                rax39 = xcalloc(r14_38, 16, rdx23);
                rsp40 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                rdi41 = rax39;
                *reinterpret_cast<int32_t*>(&rax42) = 0;
                *reinterpret_cast<int32_t*>(&rax42 + 4) = 0;
                do {
                    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdi41) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax42) << 4)) = *reinterpret_cast<void***>(r15_18 + reinterpret_cast<unsigned char>(rax42) * 8);
                    rdx43 = rax42;
                    ++rax42;
                } while (rdx43 != rbp44);
                rcx37 = v45;
                rdx46 = r14_38;
                *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                merge(rdi41, 0, rdx46, rcx37);
                rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp40) - 8 + 8);
                while (1) {
                    zf48 = have_read_stdin == 0;
                    if (zf48 || (rdi49 = stdin, rax50 = rpl_fclose(rdi49, rsi13, rdx46), rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8), !!(*reinterpret_cast<int32_t*>(&rax50) + 1))) {
                        fun_3d90();
                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
                        rax51 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(optind)));
                        if (*reinterpret_cast<uint32_t*>(&rax51) != *reinterpret_cast<uint32_t*>(&v5)) 
                            goto addr_561b_31;
                    } else {
                        *reinterpret_cast<int32_t*>(&rdx23) = 5;
                        *reinterpret_cast<int32_t*>(&rdx23 + 4) = 0;
                        rax52 = fun_3920();
                        rsi53 = reinterpret_cast<void**>("-");
                        sort_die(rax52, "-", 5);
                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 8 + 8);
                        goto addr_6238_33;
                    }
                    *reinterpret_cast<uint32_t*>(&v33) = 0;
                    addr_5641_35:
                    *reinterpret_cast<unsigned char*>(&rax51) = reinterpret_cast<uint1_t>(v7 == 0);
                    eax54 = *reinterpret_cast<uint32_t*>(&rax51) & *reinterpret_cast<uint32_t*>(&v33);
                    *reinterpret_cast<unsigned char*>(&v11) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&v11) | *reinterpret_cast<unsigned char*>(&eax54));
                    if (!*reinterpret_cast<unsigned char*>(&v11)) {
                        addr_4c7e_36:
                        *reinterpret_cast<void***>(r15_18 + reinterpret_cast<unsigned char>(r12_55) * 8) = rdx46;
                        ++r12_55;
                        goto addr_4440_37;
                    } else {
                        rdi56 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0xc0);
                        *reinterpret_cast<uint32_t*>(&rcx37) = 18;
                        v57 = rdi56;
                        rsi13 = rdi56;
                        while (*reinterpret_cast<uint32_t*>(&rcx37)) {
                            *reinterpret_cast<uint32_t*>(&rcx37) = *reinterpret_cast<uint32_t*>(&rcx37) - 1;
                            rsi13 = rsi13 + 4;
                        }
                        v58 = -1;
                        rax59 = parse_field_count(rdx46 + 1, rsi13, 0);
                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                        rdi35 = rax59;
                        if (rax59) 
                            goto addr_5694_42;
                    }
                    if (!1) {
                        goto addr_6371_45;
                    }
                    addr_5694_42:
                    if (*reinterpret_cast<void***>(rax59) == 46) 
                        goto addr_6976_20;
                    if (!1) 
                        goto addr_56af_23;
                    addr_56c4_47:
                    rsi13 = v57;
                    *reinterpret_cast<uint32_t*>(&rdx46) = 0;
                    *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                    rax60 = set_ordering(rdi35, rsi13, 0);
                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                    if (*reinterpret_cast<void***>(rax60)) {
                        addr_6371_45:
                        rdx46 = optarg;
                        goto addr_4c7e_36;
                    } else {
                        if (!*reinterpret_cast<uint32_t*>(&v33)) {
                            addr_5767_49:
                            insertkey(v57, rsi13, rdx46);
                            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                            goto addr_4440_37;
                        } else {
                            rax61 = reinterpret_cast<int32_t>(optind);
                            rax62 = *reinterpret_cast<void***>(r13_4 + rax61 * 8);
                            optind = static_cast<uint32_t>(rax61 + 1);
                            v33 = rax62;
                            rax63 = parse_field_count(rax62 + 1, reinterpret_cast<int64_t>(rsp34) + 0xd0, "invalid number after '-'");
                            rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                            rdi30 = rax63;
                            if (!rax63) 
                                goto addr_6957_24;
                            if (*reinterpret_cast<void***>(rax63) == 46) 
                                goto addr_6937_17;
                            addr_572b_18:
                            if (v64) 
                                goto addr_574f_52;
                            if (v58) 
                                goto addr_5743_54;
                        }
                    }
                    addr_574f_52:
                    rsi13 = v57;
                    *reinterpret_cast<uint32_t*>(&rdx46) = 1;
                    *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                    rax65 = set_ordering(rdi30, rsi13, 1);
                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
                    if (*reinterpret_cast<void***>(rax65)) 
                        goto addr_6926_19; else 
                        goto addr_5767_49;
                    addr_5743_54:
                    --v58;
                    goto addr_574f_52;
                    addr_56af_23:
                    addr_56bb_21:
                    if (!rdi35) 
                        goto addr_6371_45; else 
                        goto addr_56c4_47;
                    addr_561b_31:
                    rax51 = *reinterpret_cast<void***>(r13_4 + reinterpret_cast<unsigned char>(rax51) * 8);
                    *reinterpret_cast<uint32_t*>(&v33) = 0;
                    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax51) == 45)) {
                        *reinterpret_cast<uint32_t*>(&rax51) = reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax51 + 1) - 48) <= 9);
                        *reinterpret_cast<uint32_t*>(&v33) = *reinterpret_cast<uint32_t*>(&rax51);
                        goto addr_5641_35;
                    }
                    addr_6238_33:
                    fun_3750(v66, rsi53, v66, rsi53);
                    rbx67 = temphead;
                    rax68 = xnmalloc(v5, 16, rdx23);
                    rsp69 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                    rbp44 = rax68;
                    while (rbx67) {
                        *reinterpret_cast<void***>(rax68 + 8) = rbx67;
                        rdx70 = reinterpret_cast<uint64_t>(rbx67 + 13);
                        rbx67 = *reinterpret_cast<void***>(rbx67);
                        rax68 = rax68 + 16;
                        *reinterpret_cast<uint64_t*>(rax68 + 0xfffffffffffffff0) = rdx70;
                    }
                    rsi13 = v5;
                    rcx37 = v45;
                    rdx46 = rsi13;
                    merge(rbp44, rsi13, rdx46, rcx37);
                    fun_3750(rbp44, rsi13);
                    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp69) - 8 + 8 - 8 + 8);
                    addr_55dc_59:
                    while (less_or_equal71 = reinterpret_cast<int32_t>(nprocs) <= reinterpret_cast<int32_t>(0), !less_or_equal71) {
                        reap(0xffffffff, rsi13);
                        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
                    }
                    continue;
                    addr_55c5_61:
                    fun_3750(v66, rsi13);
                    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                    goto addr_55dc_59;
                    while (1) {
                        addr_5964_62:
                        if (*reinterpret_cast<unsigned char*>(&v8) && (zf72 = thousands_sep_ignored == 0, !zf72)) {
                            rax73 = fun_3920();
                            *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                            rdx46 = rax73;
                            fun_3c90();
                            rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8 - 8 + 8);
                        }
                        while (1) {
                            do {
                                while (1) {
                                    al75 = default_key_compare(v11);
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8);
                                    *reinterpret_cast<uint32_t*>(&rbx67) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v76) + 7);
                                    *reinterpret_cast<int32_t*>(&rbx67 + 4) = 0;
                                    if (!al75) {
                                        zf77 = stable == 0;
                                        if (zf77 && (zf78 = unique == 0, zf78)) {
                                        }
                                        r13_4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0x3c0);
                                        key_to_opts(v11, r13_4, rdx46);
                                        fun_3940(r13_4, r13_4);
                                        *reinterpret_cast<int32_t*>(&r8_79) = 5;
                                        *reinterpret_cast<int32_t*>(&r8_79 + 4) = 0;
                                        rax80 = fun_3d00();
                                        rcx37 = r13_4;
                                        *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                        rdx46 = rax80;
                                        fun_3c90();
                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v76) + 7) = *reinterpret_cast<unsigned char*>(&rbx67);
                                        if (!*reinterpret_cast<unsigned char*>(&rbx67)) 
                                            goto addr_4621_68;
                                    } else {
                                        if (!*reinterpret_cast<unsigned char*>(&rbx67)) 
                                            goto addr_4621_68;
                                        zf81 = stable == 0;
                                        if (!zf81) 
                                            goto addr_62ec_72; else 
                                            goto addr_59a7_73;
                                    }
                                    addr_5a0d_74:
                                    zf82 = stable == 0;
                                    if (!zf82) 
                                        goto addr_4621_68; else 
                                        goto addr_5a1a_75;
                                    addr_62ec_72:
                                    zf83 = keylist == 0;
                                    if (zf83) 
                                        goto addr_4621_68;
                                    r13_4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0x3c0);
                                    key_to_opts(v11, r13_4, rdx46);
                                    fun_3940(r13_4, r13_4);
                                    *reinterpret_cast<int32_t*>(&r8_79) = 5;
                                    *reinterpret_cast<int32_t*>(&r8_79 + 4) = 0;
                                    rax84 = fun_3d00();
                                    rcx37 = r13_4;
                                    *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                    rdx46 = rax84;
                                    fun_3c90();
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v76) + 7) = 1;
                                    goto addr_5a0d_74;
                                    addr_59a7_73:
                                    zf85 = unique == 0;
                                    if (zf85 || (zf86 = keylist == 0, zf86)) {
                                        addr_5a1a_75:
                                        zf87 = unique == 0;
                                        if (!zf87) 
                                            goto addr_4621_68;
                                        zf88 = keylist == 0;
                                        if (zf88) 
                                            goto addr_4621_68;
                                    } else {
                                        r13_4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0x3c0);
                                        key_to_opts(v11, r13_4, rdx46);
                                        fun_3940(r13_4, r13_4);
                                        *reinterpret_cast<int32_t*>(&r8_79) = 5;
                                        *reinterpret_cast<int32_t*>(&r8_79 + 4) = 0;
                                        rax89 = fun_3d00();
                                        rcx37 = r13_4;
                                        *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                        rdx46 = rax89;
                                        fun_3c90();
                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v76) + 7) = *reinterpret_cast<unsigned char*>(&rbx67);
                                        goto addr_5a0d_74;
                                    }
                                    rax90 = fun_3920();
                                    *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                    rdx46 = rax90;
                                    fun_3c90();
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                    goto addr_4621_68;
                                    addr_5271_80:
                                    rbp44 = *reinterpret_cast<void***>(rbx67);
                                    addr_6574_81:
                                    rax91 = fun_3920();
                                    sort_die(rax91, rbp44, 5);
                                    rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                    goto addr_6586_82;
                                    addr_653d_83:
                                    rax92 = fun_3920();
                                    rcx37 = r13_4;
                                    *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                    rdx46 = rax92;
                                    fun_3c90();
                                    rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp93) - 8 + 8 - 8 + 8);
                                    goto addr_5954_84;
                                    addr_678e_85:
                                    *reinterpret_cast<signed char*>(r8_79 + 55) = 0;
                                    r9_94 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0x3c0);
                                    *reinterpret_cast<void***>(r8_79 + 48) = reinterpret_cast<void**>(0);
                                    key_to_opts(r8_79, r9_94, rdx46, r8_79, r9_94, rdx46);
                                    incompatible_options(r9_94, r9_94);
                                    rdi95 = optarg;
                                    badfieldspec(rdi95, "field number is zero", rdx46, rdi95, "field number is zero", rdx46);
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8);
                                    addr_67c8_87:
                                    fun_3920();
                                    fun_3c90();
                                    quote(r10_96, r10_96);
                                    r12_55 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_38) + (reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v57))) << 5));
                                    fun_3920();
                                    fun_3c90();
                                    rax97 = quote("2", "2");
                                    fun_3920();
                                    r8_79 = rax97;
                                    fun_3c90();
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    addr_6864_89:
                                    *reinterpret_cast<int32_t*>(&rdx98) = 5;
                                    *reinterpret_cast<int32_t*>(&rdx98 + 4) = 0;
                                    rax99 = fun_3920();
                                    sort_die(rax99, v45, 5, rax99, v45, 5);
                                    rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                    addr_6884_90:
                                    r14_38 = r15_18;
                                    ebx101 = 1;
                                    addr_5c81_91:
                                    *reinterpret_cast<uint32_t*>(&rbx67) = ebx101 ^ 1;
                                    *reinterpret_cast<int32_t*>(&rbx67 + 4) = 0;
                                    xfclose(v102, r14_38, rdx98, v102, r14_38, rdx98);
                                    fun_3750(v66, r14_38, v66, r14_38);
                                    fun_3750(v103, r14_38, v103, r14_38);
                                    fun_3d90();
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    goto addr_5cb3_92;
                                    addr_4e25_93:
                                    v104 = r12_55;
                                    rbp105 = keylist;
                                    rsi13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp74) + 0x110);
                                    v11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp74) + 0x160);
                                    *reinterpret_cast<uint32_t*>(&rcx37) = 18;
                                    r12d106 = *reinterpret_cast<unsigned char*>(&v7);
                                    *reinterpret_cast<int32_t*>(&r13_4) = 1;
                                    *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
                                    while (*reinterpret_cast<uint32_t*>(&rcx37)) {
                                        *reinterpret_cast<uint32_t*>(&rcx37) = *reinterpret_cast<uint32_t*>(&rcx37) - 1;
                                        rsi13 = rsi13 + 4;
                                    }
                                    v107 = rbx67;
                                    rbx108 = rbp105;
                                    *reinterpret_cast<unsigned char*>(&v14) = 0;
                                    *reinterpret_cast<unsigned char*>(&v5) = 0;
                                    *reinterpret_cast<unsigned char*>(&v102) = 0;
                                    *reinterpret_cast<unsigned char*>(&v8) = 0;
                                    v109 = *reinterpret_cast<unsigned char*>(&r14_38);
                                    v110 = r15_18;
                                    while (rbx108) {
                                        if (0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx108 + 48))) {
                                            eax111 = *reinterpret_cast<unsigned char*>(rbx108 + 52);
                                            edi112 = *reinterpret_cast<unsigned char*>(&v8);
                                            *reinterpret_cast<uint32_t*>(&rdx46) = 1;
                                            *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                            zf113 = *reinterpret_cast<signed char*>(&eax111) == 0;
                                            if (zf113) {
                                                edi112 = 1;
                                            }
                                            *reinterpret_cast<unsigned char*>(&v8) = *reinterpret_cast<unsigned char*>(&edi112);
                                            edi114 = *reinterpret_cast<unsigned char*>(&v102);
                                            if (!zf113) {
                                                edi114 = eax111;
                                            }
                                            *reinterpret_cast<unsigned char*>(&v102) = *reinterpret_cast<unsigned char*>(&edi114);
                                        }
                                        if (*reinterpret_cast<signed char*>(rbx108 + 57)) {
                                            rbp115 = *reinterpret_cast<void***>(rbx108);
                                            if (reinterpret_cast<int1_t>(rbp115 == 0xffffffffffffffff)) {
                                                *reinterpret_cast<int32_t*>(&rbp115) = 0;
                                                *reinterpret_cast<int32_t*>(&rbp115 + 4) = 0;
                                            }
                                            r15_116 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) + 0x370);
                                            v57 = *reinterpret_cast<void***>(rbx108 + 16);
                                            rax117 = umaxtostr(rbp115, r15_116, rdx46);
                                            rsp118 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8);
                                            rax119 = fun_3b50(reinterpret_cast<int64_t>(rsp118) + 0x391, rax117, 45);
                                            v7 = rax119;
                                            rax120 = umaxtostr(rbp115 + 1, r15_116, 45);
                                            rsp121 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp118) - 8 + 8 - 8 + 8);
                                            rax122 = fun_3b50(reinterpret_cast<int64_t>(rsp121) + 0x3c3, rax120, 44);
                                            rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp121) - 8 + 8);
                                            if (*reinterpret_cast<void***>(rbx108 + 16) != 0xffffffffffffffff) {
                                                rcx124 = v57 + 1;
                                                v57 = rcx124;
                                                rax125 = umaxtostr(rcx124, r15_116, 44);
                                                *reinterpret_cast<void***>(v7) = reinterpret_cast<void**>(0x2d20);
                                                fun_3810(v7 + 2, rax125, 44);
                                                *reinterpret_cast<int32_t*>(&rdi126) = 0;
                                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi126) + 4) = 0;
                                                *reinterpret_cast<unsigned char*>(&rdi126) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbx108 + 24) == 0xffffffffffffffff);
                                                rax127 = umaxtostr(reinterpret_cast<int64_t>(rdi126) + reinterpret_cast<unsigned char>(v57), r15_116, 44);
                                                *reinterpret_cast<void***>(rax122) = reinterpret_cast<void**>(44);
                                                fun_3810(rax122 + 1, rax127, 44);
                                                rsp123 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                            }
                                            rax128 = quote_n();
                                            quote_n();
                                            fun_3920();
                                            r8_79 = rax128;
                                            *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                            fun_3c90();
                                            rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp123) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                        }
                                        if (*reinterpret_cast<void***>(rbx108) == 0xffffffffffffffff || reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx108)) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx108 + 16))) {
                                            eax129 = 0;
                                        } else {
                                            fun_3920();
                                            *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                            fun_3c90();
                                            rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8 - 8 + 8);
                                            eax129 = 1;
                                        }
                                        *reinterpret_cast<uint32_t*>(&rcx37) = 1;
                                        if (!(0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx108 + 48)))) {
                                            *reinterpret_cast<uint32_t*>(&rcx37) = *reinterpret_cast<unsigned char*>(rbx108 + 54);
                                        }
                                        eax130 = eax129 | r12d106;
                                        if (!*reinterpret_cast<void***>(rbx108 + 16) && *reinterpret_cast<void***>(rbx108 + 24)) {
                                            if (!*reinterpret_cast<signed char*>(&eax130)) 
                                                goto addr_4e9f_117; else 
                                                goto addr_4e96_118;
                                        }
                                        if (*reinterpret_cast<signed char*>(&eax130)) {
                                            addr_4e96_118:
                                            if (!*reinterpret_cast<signed char*>(&r12d106)) {
                                                addr_50e8_120:
                                                if (0xffff00ff0000 & reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx108 + 48))) {
                                                    rax131 = *reinterpret_cast<void***>(rbx108) + 1;
                                                    if (!rax131) {
                                                        rax131 = reinterpret_cast<void**>(1);
                                                    }
                                                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx108 + 16)) >= reinterpret_cast<unsigned char>(rax131)) {
                                                        fun_3920();
                                                        *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                                        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                                        rcx37 = r13_4;
                                                        fun_3c90();
                                                        rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8 - 8 + 8);
                                                        eax132 = *reinterpret_cast<unsigned char*>(rbx108 + 52);
                                                        edi133 = *reinterpret_cast<unsigned char*>(&v5);
                                                        zf134 = *reinterpret_cast<signed char*>(&eax132) == 0;
                                                        if (zf134) {
                                                            edi133 = 1;
                                                        }
                                                        *reinterpret_cast<unsigned char*>(&v5) = *reinterpret_cast<unsigned char*>(&edi133);
                                                        edi135 = *reinterpret_cast<unsigned char*>(&v14);
                                                        if (!zf134) {
                                                            edi135 = eax132;
                                                        }
                                                        *reinterpret_cast<unsigned char*>(&v14) = *reinterpret_cast<unsigned char*>(&edi135);
                                                        goto addr_4e9f_117;
                                                    }
                                                }
                                            } else {
                                                addr_4e9f_117:
                                                if (v136 && v136 == *reinterpret_cast<void***>(rbx108 + 32)) {
                                                    v136 = reinterpret_cast<void**>(0);
                                                }
                                            }
                                        } else {
                                            zf137 = tab == 0x80;
                                            if (!zf137) 
                                                goto addr_50e8_120;
                                            if (*reinterpret_cast<void***>(rbx108 + 48)) 
                                                goto addr_614b_132;
                                            if (!*reinterpret_cast<uint32_t*>(&rcx37)) 
                                                goto addr_5a74_134;
                                            if (!*reinterpret_cast<void***>(rbx108 + 8)) 
                                                goto addr_614b_132; else 
                                                goto addr_5a74_134;
                                        }
                                        if (!1 && !*reinterpret_cast<void***>(rbx108 + 40)) {
                                        }
                                        ++r13_4;
                                        rdx46 = reinterpret_cast<void**>(0x101010101010101 ^ reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx108 + 48)));
                                        v76 = v76 & reinterpret_cast<unsigned char>(rdx46);
                                        rbx108 = *reinterpret_cast<void***>(rbx108 + 64);
                                        continue;
                                        addr_614b_132:
                                        if (*reinterpret_cast<signed char*>(rbx108 + 49)) 
                                            goto addr_50e8_120;
                                        if (!*reinterpret_cast<void***>(rbx108 + 24)) 
                                            goto addr_50e8_120;
                                        addr_5a74_134:
                                        fun_3920();
                                        rcx37 = r13_4;
                                        *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                        fun_3c90();
                                        rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8 - 8 + 8);
                                        goto addr_50e8_120;
                                    }
                                    *reinterpret_cast<uint32_t*>(&rbp44) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v102)) | *reinterpret_cast<uint32_t*>(&v8);
                                    *reinterpret_cast<uint32_t*>(&r14_38) = v109;
                                    *reinterpret_cast<int32_t*>(&r14_38 + 4) = 0;
                                    r12_55 = v104;
                                    r15_18 = v110;
                                    if (!*reinterpret_cast<unsigned char*>(&v5)) {
                                        if (!*reinterpret_cast<unsigned char*>(&v14)) 
                                            break;
                                    } else {
                                        eax138 = tab;
                                        *reinterpret_cast<uint32_t*>(&rdx46) = thousands_sep;
                                        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                        if (eax138 == 0x80) {
                                            if (*reinterpret_cast<uint32_t*>(&rdx46) == 0x80) {
                                                *reinterpret_cast<unsigned char*>(&v5) = 0;
                                                goto addr_5954_84;
                                            }
                                            rax139 = *reinterpret_cast<void***>(v107);
                                            *reinterpret_cast<uint32_t*>(&rcx37) = *reinterpret_cast<unsigned char*>(&rdx46);
                                            *reinterpret_cast<int32_t*>(&rcx37 + 4) = 0;
                                            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax139 + reinterpret_cast<unsigned char>(rcx37) * 2)) & 1) 
                                                goto addr_639d_147; else 
                                                goto addr_650d_148;
                                        } else {
                                            if (eax138 != *reinterpret_cast<uint32_t*>(&rdx46)) {
                                                *reinterpret_cast<unsigned char*>(&v5) = 0;
                                                goto addr_592c_151;
                                            }
                                        }
                                    }
                                    addr_63e0_152:
                                    eax138 = tab;
                                    if (eax138 != 0x80) {
                                        addr_592c_151:
                                        *reinterpret_cast<uint32_t*>(&rdx46) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(decimal_point)));
                                        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                        if (*reinterpret_cast<uint32_t*>(&rdx46) == eax138) 
                                            goto addr_6415_153;
                                    } else {
                                        zf140 = thousands_sep == 0x80;
                                        if (zf140) 
                                            goto addr_5954_84;
                                        rax139 = *reinterpret_cast<void***>(v107);
                                        goto addr_6404_156;
                                    }
                                    if (eax138 == 45) {
                                        addr_6586_82:
                                        rdi141 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp74) + 0x3c0);
                                        rax142 = quote(rdi141, rdi141);
                                        rsp93 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8);
                                        r13_4 = rax142;
                                        goto addr_653d_83;
                                    } else {
                                        if (eax138 != 43 || !*reinterpret_cast<unsigned char*>(&v14)) {
                                            addr_5954_84:
                                            if (!*reinterpret_cast<signed char*>(&rbp44)) 
                                                continue; else 
                                                goto addr_5959_159;
                                        } else {
                                            rax143 = quote(reinterpret_cast<int64_t>(rsp74) + 0x3c0);
                                            rsp93 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8);
                                            r13_4 = rax143;
                                            goto addr_653d_83;
                                        }
                                    }
                                    addr_6404_156:
                                    *reinterpret_cast<uint32_t*>(&rdx46) = decimal_point;
                                    *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax139 + reinterpret_cast<unsigned char>(rdx46) * 2)) & 1)) 
                                        goto addr_5954_84; else 
                                        goto addr_6415_153;
                                    addr_639d_147:
                                    rax144 = quote(reinterpret_cast<int64_t>(rsp74) + 0x3c0);
                                    r13_4 = rax144;
                                    rax145 = fun_3920();
                                    rcx37 = r13_4;
                                    *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                    rdx46 = rax145;
                                    fun_3c90();
                                    rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8 - 8 + 8 - 8 + 8);
                                    goto addr_63e0_152;
                                    addr_650d_148:
                                    *reinterpret_cast<unsigned char*>(&v5) = 0;
                                    goto addr_6404_156;
                                    addr_6784_161:
                                    xfopen_part_0(v102, rsi146, rdx46, v102, rsi146, rdx46);
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp147) - 8 + 8);
                                    goto addr_678e_85;
                                    addr_6750_162:
                                    quotearg_style(4, rsi148, rdx46, 4, rsi148, rdx46);
                                    rax149 = fun_3920();
                                    *reinterpret_cast<int32_t*>(&rsi146) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi146 + 4) = 0;
                                    rdx46 = rax149;
                                    fun_3c90();
                                    rsp147 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp150) - 8 + 8 - 8 + 8 - 8 + 8);
                                    goto addr_6784_161;
                                    addr_66d9_163:
                                    rax151 = quotearg_style(4, v102, rdx46, 4, v102, rdx46);
                                    r12_55 = rax151;
                                    fun_3920();
                                    fun_3c90();
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp152) - 8 + 8 - 8 + 8 - 8 + 8);
                                    addr_6712_164:
                                    quotearg_n_style_colon();
                                    rax153 = fun_3920();
                                    r8_79 = r12_55;
                                    *reinterpret_cast<int32_t*>(&rsi148) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi148 + 4) = 0;
                                    rdx46 = rax153;
                                    fun_3c90();
                                    rsp150 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8);
                                    goto addr_6750_162;
                                    addr_66a5_165:
                                    quotearg_style(4, rsi13, rdx46);
                                    rax154 = fun_3920();
                                    rdx46 = rax154;
                                    fun_3c90();
                                    rsp152 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8);
                                    goto addr_66d9_163;
                                    addr_4da0_166:
                                    *reinterpret_cast<int32_t*>(&rsi155) = *reinterpret_cast<int32_t*>(&v57);
                                    *reinterpret_cast<int32_t*>(&rsi155 + 4) = 0;
                                    r8_79 = r10_96;
                                    *reinterpret_cast<int32_t*>(&rdi156) = *reinterpret_cast<int32_t*>(&r11_157);
                                    *reinterpret_cast<int32_t*>(&rdi156 + 4) = 0;
                                    xstrtol_fatal(rdi156, rsi155, 0xffffff84, r14_38, r8_79);
                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp158) - 8 + 8);
                                    addr_4db7_167:
                                    if (!v14 || (rax159 = fun_3c30(3, 3), rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8), rax159 == 0)) {
                                        fun_3920();
                                        fun_3c90();
                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                    }
                                    zf160 = hard_LC_COLLATE == 0;
                                    if (zf160) {
                                        addr_5cb3_92:
                                        fun_3920();
                                        rdx46 = reinterpret_cast<void**>("%s");
                                        fun_3c90();
                                        rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                        goto addr_4e25_93;
                                    } else {
                                        rax161 = fun_3c30(3);
                                        quote(rax161, rax161);
                                        rax162 = fun_3920();
                                        rdx46 = rax162;
                                        fun_3c90();
                                        rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                        goto addr_4e25_93;
                                    }
                                    addr_6564_171:
                                    rbp44 = *reinterpret_cast<void***>(r15_18 + reinterpret_cast<unsigned char>(rbp44) * 8);
                                    goto addr_6574_81;
                                    addr_5ac9_172:
                                    r14_25 = *reinterpret_cast<void***>(r15_18);
                                    rax163 = stream_open(r14_25, "r", rdx23);
                                    rsp164 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                    v102 = rax163;
                                    if (!rax163) 
                                        goto addr_6891_10;
                                    rax165 = keylist;
                                    rdx166 = merge_buffer_size;
                                    r15_18 = r14_25;
                                    ebx167 = unique;
                                    v168 = rax165;
                                    rax169 = sort_size;
                                    ebx170 = ebx167 ^ 1;
                                    ebx171 = *reinterpret_cast<unsigned char*>(&ebx170);
                                    if (reinterpret_cast<unsigned char>(rax169) >= reinterpret_cast<unsigned char>(rdx166)) {
                                        rdx166 = rax169;
                                    }
                                    rax172 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp164) + 0x160);
                                    *reinterpret_cast<int32_t*>(&rbp173) = 0;
                                    *reinterpret_cast<int32_t*>(&rbp173 + 4) = 0;
                                    v174 = rax172;
                                    initbuf(rax172, 32, rdx166);
                                    rsp175 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp164) - 8 + 8);
                                    v103 = reinterpret_cast<void**>(0);
                                    v8 = reinterpret_cast<void**>(0);
                                    v28 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp175) + 0xa0);
                                    while (rsi176 = v102, rdx98 = r15_18, al177 = fillbuf(v174, rsi176, rdx98), rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp175) - 8 + 8), !!al177) {
                                        rdi178 = v66;
                                        v45 = v179;
                                        v5 = rdi178;
                                        r12_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi178) - reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v179) << 5));
                                        if (!rbp173) 
                                            goto addr_5ba7_178;
                                        rsi176 = v5 + 0xffffffffffffffe0;
                                        rax180 = compare(v28, rsi176, rdx98);
                                        rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8);
                                        if (reinterpret_cast<int32_t>(ebx171) <= *reinterpret_cast<int32_t*>(&rax180)) 
                                            goto addr_5cfb_180;
                                        addr_5ba7_178:
                                        r14_181 = v5 + 0xffffffffffffffe0;
                                        do {
                                            r13_182 = r14_181;
                                            if (reinterpret_cast<unsigned char>(r12_55) >= reinterpret_cast<unsigned char>(r14_181)) 
                                                break;
                                            r14_181 = r14_181 - 32;
                                            rsi176 = r14_181;
                                            rax183 = compare(r13_182, rsi176, rdx98);
                                            rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8);
                                        } while (reinterpret_cast<int32_t>(ebx171) > *reinterpret_cast<int32_t*>(&rax183));
                                        goto addr_5c6e_183;
                                        rdx184 = *reinterpret_cast<void***>(r14_181 + 8);
                                        v8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v8) + reinterpret_cast<unsigned char>(v45));
                                        if (reinterpret_cast<unsigned char>(rbp173) < reinterpret_cast<unsigned char>(rdx184)) {
                                            do {
                                                rbp173 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp173) + reinterpret_cast<unsigned char>(rbp173));
                                                if (!rbp173) 
                                                    break;
                                            } while (reinterpret_cast<unsigned char>(rdx184) > reinterpret_cast<unsigned char>(rbp173));
                                            goto addr_5c48_187;
                                        } else {
                                            addr_5be4_188:
                                            rsi185 = *reinterpret_cast<void***>(r14_181);
                                            fun_3b30(v103, rsi185, rdx184);
                                            rsp175 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8);
                                            v186 = *reinterpret_cast<void***>(r14_181 + 8);
                                            if (!v168) 
                                                continue; else 
                                                goto addr_5c0f_189;
                                        }
                                        rbp173 = rdx184;
                                        addr_5c48_187:
                                        fun_3750(v103, rsi176);
                                        rax187 = xmalloc(rbp173, rbp173);
                                        rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8 - 8 + 8);
                                        rdx184 = *reinterpret_cast<void***>(r14_181 + 8);
                                        v103 = rax187;
                                        goto addr_5be4_188;
                                        addr_5c0f_189:
                                    }
                                    goto addr_6884_90;
                                    addr_5cfb_180:
                                    r14_38 = r15_18;
                                    r15_18 = v5;
                                    addr_5c74_191:
                                    ebx101 = 0;
                                    if (*reinterpret_cast<unsigned char*>(&v188) == 99) {
                                        rbp189 = r15_18 + 0xffffffffffffffe0;
                                        rax190 = umaxtostr((reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(rbp189)) >> 5) + reinterpret_cast<unsigned char>(v8), reinterpret_cast<int64_t>(rsp100) + 0x3c0, rdx98);
                                        r15_18 = program_name;
                                        r12_55 = rax190;
                                        rax191 = fun_3920();
                                        r9_94 = r12_55;
                                        r8_79 = r14_38;
                                        rdi192 = stderr;
                                        fun_3db0(rdi192, 1, rax191, r15_18, r8_79, r9_94, v193, v5, v8, v188, v174, v102);
                                        rax194 = fun_3920();
                                        rsi195 = stderr;
                                        rdx98 = rax194;
                                        write_line(rbp189, rsi195, rdx98, r15_18);
                                        rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                        goto addr_5c81_91;
                                    }
                                    addr_5c6e_183:
                                    r14_38 = r15_18;
                                    r15_18 = r13_182;
                                    goto addr_5c74_191;
                                    while (1) {
                                        addr_61be_193:
                                        *reinterpret_cast<int32_t*>(&rdi196) = 1;
                                        *reinterpret_cast<int32_t*>(&rdi196 + 4) = 0;
                                        while (1) {
                                            addr_4d82_194:
                                            *reinterpret_cast<int32_t*>(&rsi197) = *reinterpret_cast<int32_t*>(&v57);
                                            *reinterpret_cast<int32_t*>(&rsi197 + 4) = 0;
                                            xstrtol_fatal(rdi196, rsi197, 83, r14_38, r9_94);
                                            rsp158 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                            if (*reinterpret_cast<int32_t*>(&r11_157) != 1) 
                                                goto addr_4da0_166;
                                            *reinterpret_cast<uint32_t*>(&v5) = *reinterpret_cast<uint32_t*>(&r9_94);
                                            rax198 = quote(r10_96, r10_96);
                                            r13_4 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r14_38) + (reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v57))) << 5));
                                            rax199 = fun_3920();
                                            fun_3c90();
                                            rsp200 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp158) - 8 + 8 - 8 + 8 - 8 + 8);
                                            *reinterpret_cast<uint32_t*>(&rdi201) = *reinterpret_cast<uint32_t*>(&v5);
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi201) + 4) = 0;
                                            rax202 = uinttostr(rdi201, reinterpret_cast<int64_t>(rsp200) + 0x3c0, rax199, r13_4, rax198);
                                            rbx67 = rax202;
                                            fun_3920();
                                            r8_79 = rbx67;
                                            rcx37 = r13_4;
                                            *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                            fun_3c90();
                                            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp200) - 8 + 8 - 8 + 8 - 8 + 8);
                                            rdi203 = v28;
                                            rdx46 = optarg;
                                            if (!rdi203) 
                                                goto addr_4abc_197;
                                            rsi13 = rdx46;
                                            v28 = rdx46;
                                            eax204 = fun_3ad0(rdi203, rsi13, rdx46);
                                            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                            rdx46 = v28;
                                            if (eax204) 
                                                goto addr_67c8_87;
                                            addr_4abc_197:
                                            v28 = rdx46;
                                            while (1) {
                                                while (1) {
                                                    addr_4440_37:
                                                    v103 = reinterpret_cast<void**>(0xffffffff);
                                                    if (*reinterpret_cast<uint32_t*>(&rbp44) == 0xffffffff) 
                                                        goto addr_4450_199;
                                                    if (!r12_55 || !*reinterpret_cast<unsigned char*>(&v8)) {
                                                        addr_4478_201:
                                                        *reinterpret_cast<uint32_t*>(&rdi205) = *reinterpret_cast<uint32_t*>(&v5);
                                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi205) + 4) = 0;
                                                        rcx37 = r14_38;
                                                        rsi13 = r13_4;
                                                        r8_79 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0xa0);
                                                        rdx46 = reinterpret_cast<void**>("-bcCdfghik:mMno:rRsS:t:T:uVy:z");
                                                        eax27 = fun_3960(rdi205);
                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                        *reinterpret_cast<uint32_t*>(&rbp44) = eax27;
                                                        if (eax27 == 0xffffffff) {
                                                            addr_4450_199:
                                                            rax206 = reinterpret_cast<int32_t>(optind);
                                                            goto addr_4420_202;
                                                        } else {
                                                            if (reinterpret_cast<int32_t>(eax27) > reinterpret_cast<int32_t>(0x87)) 
                                                                goto addr_4725_204;
                                                            if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax27) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax27 == 0))) 
                                                                goto addr_44ac_206;
                                                        }
                                                    } else {
                                                        rax206 = reinterpret_cast<int32_t>(optind);
                                                        if (*reinterpret_cast<unsigned char*>(&v11) != 1) 
                                                            goto addr_4420_202;
                                                        if (*reinterpret_cast<unsigned char*>(&v188)) 
                                                            goto addr_4420_202;
                                                        if (*reinterpret_cast<uint32_t*>(&v5) == *reinterpret_cast<uint32_t*>(&rax206)) 
                                                            goto addr_44f0_210; else 
                                                            goto addr_440b_211;
                                                    }
                                                    if (eax27 == 0xffffff7d) {
                                                        rdi207 = stdout;
                                                        r9_94 = reinterpret_cast<void**>("Paul Eggert");
                                                        rcx37 = Version;
                                                        r8_79 = reinterpret_cast<void**>("Mike Haertel");
                                                        rsi13 = reinterpret_cast<void**>("sort");
                                                        version_etc(rdi207);
                                                        *reinterpret_cast<int32_t*>(&rdi196) = 0;
                                                        *reinterpret_cast<int32_t*>(&rdi196 + 4) = 0;
                                                        eax208 = fun_3d90();
                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 - 8 - 8 + 8 - 8 + 8);
                                                        if (eax208 != 2) 
                                                            goto addr_4d82_194;
                                                        *reinterpret_cast<uint32_t*>(&rdx46) = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v209) - 1) - 48;
                                                        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                                        if (*reinterpret_cast<uint32_t*>(&rdx46) > 9) 
                                                            goto addr_4d82_194;
                                                        if (v209->f1) 
                                                            goto addr_4d82_194;
                                                        eax210 = v209->f0;
                                                        if (*reinterpret_cast<signed char*>(&eax210) != 37) 
                                                            goto addr_61dd_218;
                                                    } else {
                                                        if (eax27 != 0xffffff7e) {
                                                            addr_4725_204:
                                                            usage();
                                                            r9_94 = argmatch_die;
                                                            *reinterpret_cast<int32_t*>(&r8_79) = 1;
                                                            *reinterpret_cast<int32_t*>(&r8_79 + 4) = 0;
                                                            rsi211 = optarg;
                                                            rcx37 = reinterpret_cast<void**>("ghMnRVCCc");
                                                            rax212 = __xargmatch_internal("--sort", rsi211, 0x1c580, "ghMnRVCCc", 1, r9_94);
                                                            r11_157 = reinterpret_cast<void**>(1);
                                                            *reinterpret_cast<uint32_t*>(&rbp44) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>("ghMnRVCCc") + reinterpret_cast<uint64_t>(rax212))));
                                                            rsp213 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
                                                            rsi13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp213) + 0x110);
                                                            *reinterpret_cast<uint32_t*>(&rdx46) = 2;
                                                            *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                                            set_ordering(reinterpret_cast<int64_t>(rsp213) + 0x3c0, rsi13, 2);
                                                            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp213) - 8 + 8);
                                                            continue;
                                                        } else {
                                                            usage();
                                                            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                            goto addr_44f0_210;
                                                        }
                                                    }
                                                    addr_69f9_221:
                                                    physmem_total(rdi196, rsi13, rdx46);
                                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                    rax214 = v66;
                                                    r9_94 = r9_94;
                                                    __asm__("movapd xmm1, xmm0");
                                                    below_or_equal215 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax214 == 0)));
                                                    if (reinterpret_cast<signed char>(rax214) < reinterpret_cast<signed char>(0)) {
                                                        *reinterpret_cast<uint32_t*>(&rax214) = *reinterpret_cast<uint32_t*>(&rax214) & 1;
                                                        *reinterpret_cast<int32_t*>(&rax214 + 4) = 0;
                                                        __asm__("pxor xmm0, xmm0");
                                                        rdx46 = reinterpret_cast<void**>(static_cast<uint64_t>(reinterpret_cast<unsigned char>(rax214)));
                                                        below_or_equal215 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx46 == 0)));
                                                        __asm__("cvtsi2sd xmm0, rdx");
                                                        __asm__("addsd xmm0, xmm0");
                                                    } else {
                                                        __asm__("pxor xmm0, xmm0");
                                                        __asm__("cvtsi2sd xmm0, rax");
                                                    }
                                                    __asm__("mulsd xmm0, xmm1");
                                                    *reinterpret_cast<void***>(rdi196) = *reinterpret_cast<void***>(rsi13);
                                                    rsi216 = rsi13 + 4;
                                                    __asm__("divsd xmm0, [rip+0x106b6]");
                                                    __asm__("comisd xmm1, xmm0");
                                                    if (below_or_equal215) 
                                                        goto addr_61be_193;
                                                    *reinterpret_cast<void***>(rdi196 + 4) = *reinterpret_cast<void***>(rsi216);
                                                    rsi13 = rsi216 + 4;
                                                    __asm__("comisd xmm0, xmm1");
                                                    if (1) {
                                                        __asm__("subsd xmm0, xmm1");
                                                        __asm__("cvttsd2si rax, xmm0");
                                                        v66 = rax214;
                                                        __asm__("btc qword [rsp+0x160], 0x3f");
                                                    } else {
                                                        __asm__("cvttsd2si rax, xmm0");
                                                        v66 = rax214;
                                                        goto addr_4c44_234;
                                                    }
                                                    addr_61e5_235:
                                                    rax214 = v66;
                                                    addr_4c44_234:
                                                    cf217 = reinterpret_cast<unsigned char>(rax214) < reinterpret_cast<unsigned char>(sort_size);
                                                    if (cf217) 
                                                        continue;
                                                    *reinterpret_cast<uint32_t*>(&rdx218) = nmerge;
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx218) + 4) = 0;
                                                    rdx46 = reinterpret_cast<void**>(rdx218 * 34);
                                                    if (reinterpret_cast<unsigned char>(rdx46) < reinterpret_cast<unsigned char>(rax214)) {
                                                        rdx46 = rax214;
                                                    }
                                                    sort_size = rdx46;
                                                    continue;
                                                    addr_61dd_218:
                                                    if (*reinterpret_cast<signed char*>(&eax210) != 98) 
                                                        goto addr_4d82_194; else 
                                                        goto addr_61e5_235;
                                                    addr_44f0_210:
                                                    if (!v102) {
                                                        *reinterpret_cast<unsigned char*>(&v188) = 0;
                                                        goto addr_4501_240;
                                                    }
                                                    addr_46c1_241:
                                                    rsi219 = *reinterpret_cast<void***>(r15_18);
                                                    rax220 = quotearg_style(4, rsi219, rdx46);
                                                    r12_55 = rax220;
                                                    fun_3920();
                                                    fun_3c90();
                                                    rax221 = fun_3920();
                                                    rdi222 = stderr;
                                                    fun_3db0(rdi222, 1, "%s\n", rax221, r8_79, r9_94, v223, v5, v8, v188, v11, v102);
                                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                                    goto addr_4725_204;
                                                    addr_44ac_206:
                                                    if (eax27 > 0x87) 
                                                        goto addr_4725_204; else 
                                                        goto addr_44b7_13;
                                                    addr_440b_211:
                                                    rdx46 = *reinterpret_cast<void***>(r13_4 + *reinterpret_cast<int32_t*>(&rax206) * 8);
                                                    if (*reinterpret_cast<void***>(rdx46) != 45) {
                                                        goto addr_4420_202;
                                                    }
                                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx46 + 1) == 0x6f)) 
                                                        goto addr_4420_202;
                                                    if (*reinterpret_cast<void***>(rdx46 + 2)) 
                                                        goto addr_4478_201;
                                                    *reinterpret_cast<uint32_t*>(&rdx46) = static_cast<uint32_t>(rax206 + 1);
                                                    *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                                    if (*reinterpret_cast<uint32_t*>(&rdx46) == *reinterpret_cast<uint32_t*>(&v5)) {
                                                        addr_4420_202:
                                                        if (*reinterpret_cast<int32_t*>(&v5) > *reinterpret_cast<int32_t*>(&rax206)) {
                                                            *reinterpret_cast<uint32_t*>(&rdx46) = static_cast<uint32_t>(rax206 + 1);
                                                            *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                                            rax224 = *reinterpret_cast<void***>(r13_4 + rax206 * 8);
                                                            optind = *reinterpret_cast<uint32_t*>(&rdx46);
                                                            *reinterpret_cast<void***>(r15_18 + reinterpret_cast<unsigned char>(r12_55) * 8) = rax224;
                                                            ++r12_55;
                                                            continue;
                                                        }
                                                    } else {
                                                        goto addr_4478_201;
                                                    }
                                                    while (1) {
                                                        if (v102) {
                                                            if (r12_55) 
                                                                goto addr_46c1_241;
                                                            rsi146 = reinterpret_cast<void**>("r");
                                                            rax225 = stream_open(v102, "r", rdx46);
                                                            rsp147 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                            if (!rax225) 
                                                                goto addr_6784_161;
                                                            r13_4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp147) + 0x1b0);
                                                            readtokens0_init(r13_4, "r", rdx46);
                                                            al226 = readtokens0(rax225, r13_4, rdx46);
                                                            rsp150 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp147) - 8 + 8 - 8 + 8);
                                                            rsi148 = v102;
                                                            if (!al226) 
                                                                goto addr_6750_162;
                                                            xfclose(rax225, rsi148, rdx46);
                                                            rsp152 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp150) - 8 + 8);
                                                            rbp44 = v227;
                                                            if (!rbp44) 
                                                                goto addr_66d9_163;
                                                            fun_3750(r15_18, rsi148, r15_18, rsi148);
                                                            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp152) - 8 + 8);
                                                            r15_18 = v228;
                                                            do {
                                                                rsi13 = *reinterpret_cast<void***>(r15_18 + reinterpret_cast<unsigned char>(r12_55) * 8);
                                                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi13) == 45)) 
                                                                    goto addr_6680_255;
                                                                if (!*reinterpret_cast<void***>(rsi13 + 1)) 
                                                                    goto addr_66a5_165;
                                                                addr_6680_255:
                                                                ++r12_55;
                                                                if (!*reinterpret_cast<void***>(rsi13)) 
                                                                    goto addr_6712_164;
                                                            } while (rbp44 != r12_55);
                                                        }
                                                        addr_4501_240:
                                                        r8_79 = keylist;
                                                        if (!r8_79) {
                                                            rdi229 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0x110);
                                                            al230 = default_key_compare(rdi229, rdi229);
                                                            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                            if (!al230) {
                                                                insertkey(rdi229, rsi13, rdx46);
                                                                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                                r8_79 = keylist;
                                                                *reinterpret_cast<unsigned char*>(&v7) = 1;
                                                                *reinterpret_cast<uint32_t*>(&r14_38) = v231;
                                                                *reinterpret_cast<int32_t*>(&r14_38 + 4) = 0;
                                                                if (r8_79) {
                                                                    do {
                                                                        addr_45d0_260:
                                                                        edx232 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8_79 + 56));
                                                                        *reinterpret_cast<unsigned char*>(&edx232) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx232) | *reinterpret_cast<unsigned char*>(r8_79 + 51));
                                                                        *reinterpret_cast<unsigned char*>(&rcx37) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(r8_79 + 32));
                                                                        edx233 = edx232 | *reinterpret_cast<uint32_t*>(&rcx37);
                                                                        *reinterpret_cast<uint32_t*>(&rdx46) = *reinterpret_cast<unsigned char*>(&edx233);
                                                                        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                                                        if (reinterpret_cast<int32_t>(*reinterpret_cast<unsigned char*>(r8_79 + 50) + *reinterpret_cast<unsigned char*>(r8_79 + 52) + *reinterpret_cast<unsigned char*>(r8_79 + 53) + *reinterpret_cast<unsigned char*>(r8_79 + 54) + *reinterpret_cast<uint32_t*>(&rdx46)) > reinterpret_cast<int32_t>(1)) 
                                                                            goto addr_678e_85;
                                                                        r8_79 = *reinterpret_cast<void***>(r8_79 + 64);
                                                                    } while (r8_79);
                                                                }
                                                                zf234 = debug == 0;
                                                                if (!zf234) 
                                                                    break;
                                                                addr_4621_68:
                                                                eax235 = v236;
                                                                reverse = *reinterpret_cast<signed char*>(&eax235);
                                                                if (*reinterpret_cast<unsigned char*>(&r14_38)) {
                                                                    rax237 = randread_new(v28, 16, rdx46);
                                                                    rsp238 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                                    rbp44 = rax237;
                                                                    if (!rax237) 
                                                                        goto addr_690c_14;
                                                                    r13_4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp238) + 0x3c0);
                                                                    randread(rax237, r13_4, 16);
                                                                    eax239 = randread_free(rbp44, r13_4, 16);
                                                                    rsp238 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp238) - 8 + 8 - 8 + 8);
                                                                    if (eax239) 
                                                                        goto addr_68ec_8;
                                                                    r14_38 = reinterpret_cast<void**>(0x1d2e0);
                                                                    md5_init_ctx(0x1d2e0, r13_4, 16);
                                                                    rdx46 = reinterpret_cast<void**>(0x1d2e0);
                                                                    *reinterpret_cast<uint32_t*>(&rsi13) = 16;
                                                                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                                                    md5_process_bytes(r13_4, 16, 0x1d2e0);
                                                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp238) - 8 + 8 - 8 + 8);
                                                                }
                                                            } else {
                                                                zf240 = debug == 0;
                                                                if (!zf240) 
                                                                    goto addr_481f_268;
                                                                eax241 = v242;
                                                                reverse = *reinterpret_cast<signed char*>(&eax241);
                                                            }
                                                            zf243 = temp_dir_count == 0;
                                                            if (zf243) {
                                                                rax244 = fun_3790("TMPDIR", rsi13, rdx46);
                                                                rdi245 = rax244;
                                                                if (!rax244) {
                                                                    rdi245 = reinterpret_cast<void**>("/tmp");
                                                                }
                                                                add_temp_dir(rdi245, rdi245);
                                                                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                                            }
                                                            if (!r12_55) {
                                                                *reinterpret_cast<int32_t*>(&r12_55) = 1;
                                                                *reinterpret_cast<int32_t*>(&r12_55 + 4) = 0;
                                                                fun_3750(r15_18, rsi13, r15_18, rsi13);
                                                                rax246 = xmalloc(8, 8);
                                                                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                                                r15_18 = rax246;
                                                                *reinterpret_cast<void***>(r15_18) = reinterpret_cast<void**>("-");
                                                            }
                                                            rdx23 = sort_size;
                                                            if (rdx23) {
                                                                *reinterpret_cast<uint32_t*>(&rax247) = nmerge;
                                                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax247) + 4) = 0;
                                                                rax248 = reinterpret_cast<void**>(rax247 * 34);
                                                                if (reinterpret_cast<unsigned char>(rax248) < reinterpret_cast<unsigned char>(rdx23)) {
                                                                    rax248 = rdx23;
                                                                }
                                                                sort_size = rax248;
                                                            }
                                                            if (!*reinterpret_cast<unsigned char*>(&v188)) 
                                                                goto addr_523e_281;
                                                            --r12_55;
                                                            if (!r12_55) 
                                                                goto addr_4688_283;
                                                            rsi249 = *reinterpret_cast<void***>(r15_18 + 8);
                                                            rax250 = quotearg_style(4, rsi249, rdx23);
                                                            r12_55 = rax250;
                                                            rax251 = fun_3920();
                                                            *reinterpret_cast<int32_t*>(&r8_79) = *reinterpret_cast<signed char*>(&v188);
                                                            *reinterpret_cast<int32_t*>(&r8_79 + 4) = 0;
                                                            rcx37 = r12_55;
                                                            *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                                                            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                                            rdx46 = rax251;
                                                            fun_3c90();
                                                            incompatible_options("cC", "cC");
                                                            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                                        } else {
                                                            rcx37 = r8_79;
                                                            *reinterpret_cast<uint32_t*>(&r14_38) = 0;
                                                            *reinterpret_cast<int32_t*>(&r14_38 + 4) = 0;
                                                            do {
                                                                al252 = default_key_compare(rcx37, rcx37);
                                                                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                                if (al252 && !*reinterpret_cast<signed char*>(rcx37 + 55)) {
                                                                    *reinterpret_cast<void***>(rcx37 + 32) = v253;
                                                                    *reinterpret_cast<void***>(rcx37 + 40) = v254;
                                                                    eax255 = v256;
                                                                    *reinterpret_cast<void***>(rcx37 + 48) = *reinterpret_cast<void***>(&eax255);
                                                                    eax257 = v258;
                                                                    *reinterpret_cast<signed char*>(rcx37 + 49) = *reinterpret_cast<signed char*>(&eax257);
                                                                    eax259 = v260;
                                                                    *reinterpret_cast<unsigned char*>(rcx37 + 54) = *reinterpret_cast<unsigned char*>(&eax259);
                                                                    eax261 = v262;
                                                                    *reinterpret_cast<unsigned char*>(rcx37 + 50) = *reinterpret_cast<unsigned char*>(&eax261);
                                                                    eax263 = v264;
                                                                    *reinterpret_cast<unsigned char*>(rcx37 + 52) = *reinterpret_cast<unsigned char*>(&eax263);
                                                                    eax265 = v266;
                                                                    *reinterpret_cast<unsigned char*>(rcx37 + 53) = *reinterpret_cast<unsigned char*>(&eax265);
                                                                    eax267 = v268;
                                                                    *reinterpret_cast<void***>(rcx37 + 56) = *reinterpret_cast<void***>(&eax267);
                                                                    eax269 = v270;
                                                                    *reinterpret_cast<unsigned char*>(rcx37 + 51) = *reinterpret_cast<unsigned char*>(&eax269);
                                                                    eax271 = v272;
                                                                    *reinterpret_cast<signed char*>(rcx37 + 55) = *reinterpret_cast<signed char*>(&eax271);
                                                                }
                                                                *reinterpret_cast<unsigned char*>(&r14_38) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r14_38) | *reinterpret_cast<unsigned char*>(rcx37 + 51));
                                                                rcx37 = *reinterpret_cast<void***>(rcx37 + 64);
                                                            } while (rcx37);
                                                            *reinterpret_cast<unsigned char*>(&v7) = 0;
                                                            goto addr_45d0_260;
                                                        }
                                                    }
                                                    addr_4827_291:
                                                    edi273 = *reinterpret_cast<unsigned char*>(&v188);
                                                    *reinterpret_cast<unsigned char*>(&rdx46) = reinterpret_cast<uint1_t>(!!v45);
                                                    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(&edi273))) | *reinterpret_cast<unsigned char*>(&rdx46))) 
                                                        goto addr_4db7_167;
                                                    if (!*reinterpret_cast<signed char*>(&edi273)) {
                                                        *reinterpret_cast<unsigned char*>(&v188) = 0x6f;
                                                    }
                                                    eax274 = *reinterpret_cast<unsigned char*>(&v188);
                                                    opts_7 = *reinterpret_cast<signed char*>(&eax274);
                                                    incompatible_options("X --debug", "X --debug");
                                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                    stable = 1;
                                                    continue;
                                                    addr_523e_281:
                                                    rbx67 = r15_18;
                                                    *reinterpret_cast<uint32_t*>(&rbp44) = 0;
                                                    *reinterpret_cast<int32_t*>(&rbp44 + 4) = 0;
                                                    while (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx67)) == 45 && !*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx67) + 1) || (*reinterpret_cast<uint32_t*>(&rsi13) = 4, *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0, eax275 = fun_3d30(), rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8), eax275 == 0)) {
                                                        r14_38 = rbp44 + 1;
                                                        rbx67 = rbx67 + 8;
                                                        if (r12_55 == r14_38) 
                                                            goto addr_5285_297;
                                                        rbp44 = r14_38;
                                                    }
                                                    goto addr_5271_80;
                                                    addr_5285_297:
                                                    if (!v45) {
                                                        addr_52bc_299:
                                                        if (*reinterpret_cast<signed char*>(&v276)) 
                                                            goto addr_57de_25;
                                                    } else {
                                                        *reinterpret_cast<int32_t*>(&rdx23) = 0x1b6;
                                                        *reinterpret_cast<int32_t*>(&rdx23 + 4) = 0;
                                                        *reinterpret_cast<uint32_t*>(&rsi13) = 0x80041;
                                                        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                                        eax277 = fun_3cd0(v45, v45);
                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                        if (eax277 < 0) 
                                                            goto addr_6864_89;
                                                        if (eax277 == 1) 
                                                            goto addr_52bc_299; else 
                                                            goto addr_52b2_302;
                                                    }
                                                    if (!v278) {
                                                        rax279 = num_processors(2, rsi13, rdx23);
                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                        r8_79 = rax279;
                                                        *reinterpret_cast<int32_t*>(&rax280) = 8;
                                                        *reinterpret_cast<int32_t*>(&rax280 + 4) = 0;
                                                        if (reinterpret_cast<unsigned char>(r8_79) <= reinterpret_cast<unsigned char>(8)) {
                                                            rax280 = r8_79;
                                                        }
                                                        v278 = rax280;
                                                    }
                                                    v57 = r14_38;
                                                    rax281 = reinterpret_cast<void**>(0xffffffffffffff);
                                                    v5 = reinterpret_cast<void**>(0);
                                                    if (reinterpret_cast<unsigned char>(v278) <= reinterpret_cast<unsigned char>(0xffffffffffffff)) {
                                                        rax281 = v278;
                                                    }
                                                    v11 = rax281;
                                                    rbx282 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax281) + reinterpret_cast<unsigned char>(rax281));
                                                    v102 = rbx282;
                                                    rbx67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax281) << 8);
                                                    v283 = rbx282 - 1;
                                                    v284 = rbx67;
                                                    while (rsi285 = reinterpret_cast<void**>("r"), rdi286 = *reinterpret_cast<void***>(r15_18), v8 = *reinterpret_cast<void***>(r15_18), rax287 = stream_open(rdi286, "r", rdx23), rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8), v188 = rax287, !!rax287) {
                                                        if (reinterpret_cast<unsigned char>(v278) > reinterpret_cast<unsigned char>(1)) {
                                                            *reinterpret_cast<int32_t*>(&rdx288) = 1;
                                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx288) + 4) = 0;
                                                            *reinterpret_cast<int32_t*>(&rax289) = 1;
                                                            *reinterpret_cast<int32_t*>(&rax289 + 4) = 0;
                                                            while (reinterpret_cast<unsigned char>(v11) > reinterpret_cast<unsigned char>(rax289)) {
                                                                rax289 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax289) + reinterpret_cast<unsigned char>(rax289));
                                                                ++rdx288;
                                                            }
                                                            rdx23 = reinterpret_cast<void**>(rdx288 << 5);
                                                            v290 = rdx23;
                                                        } else {
                                                            v290 = reinterpret_cast<void**>(48);
                                                        }
                                                        rbp44 = reinterpret_cast<void**>(0);
                                                        v28 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0x160);
                                                        if (1) {
                                                            rbx67 = v290 + 1;
                                                            r12_55 = v290 + 2;
                                                            do {
                                                                if (!rbp44) {
                                                                    fun_3b40(v188, rsi285, rdx23);
                                                                    eax291 = fun_3e50();
                                                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                                                    al292 = reinterpret_cast<uint1_t>(!!eax291);
                                                                } else {
                                                                    r13_4 = *reinterpret_cast<void***>(r15_18 + reinterpret_cast<unsigned char>(rbp44) * 8);
                                                                    eax293 = fun_3ad0(r13_4, "-", rdx23);
                                                                    rsp294 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                                    if (eax293) {
                                                                        eax295 = fun_3b10(r13_4, v168, rdx23);
                                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp294) - 8 + 8);
                                                                        al292 = reinterpret_cast<uint1_t>(!!eax295);
                                                                    } else {
                                                                        eax296 = fun_3e50();
                                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp294) - 8 + 8);
                                                                        al292 = reinterpret_cast<uint1_t>(!!eax296);
                                                                    }
                                                                }
                                                                if (al292) 
                                                                    goto addr_6564_171;
                                                                if ((v297 & 0xf000) != 0x8000) {
                                                                    rsi285 = sort_size;
                                                                    if (rsi285) 
                                                                        break;
                                                                    zf298 = size_bound_0 == 0;
                                                                    *reinterpret_cast<uint32_t*>(&r14_38) = 0x20000;
                                                                    *reinterpret_cast<int32_t*>(&r14_38 + 4) = 0;
                                                                    if (zf298) 
                                                                        goto addr_5e95_328; else 
                                                                        goto addr_5dd0_329;
                                                                }
                                                                zf299 = size_bound_0 == 0;
                                                                r14_38 = v300;
                                                                if (!zf299 || (rax301 = sort_size, size_bound_0 = rax301, !!rax301)) {
                                                                    addr_5dd0_329:
                                                                    rsi285 = size_bound_0;
                                                                    rcx37 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx67) * reinterpret_cast<unsigned char>(r14_38) + 1);
                                                                    rdx23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx37) % reinterpret_cast<unsigned char>(rbx67));
                                                                    if (!reinterpret_cast<int1_t>(r14_38 == reinterpret_cast<unsigned char>(rcx37) / reinterpret_cast<unsigned char>(rbx67))) 
                                                                        break;
                                                                    if (reinterpret_cast<unsigned char>(rcx37) >= reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rsi285) - reinterpret_cast<unsigned char>(r12_55))) 
                                                                        break; else 
                                                                        continue;
                                                                } else {
                                                                    addr_5e95_328:
                                                                    rsi302 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0xa0);
                                                                    r13_303 = reinterpret_cast<void**>(0xffffffffffffffff);
                                                                    eax304 = fun_3dc0(2, rsi302, rdx23);
                                                                    if (!eax304) {
                                                                        r13_303 = v103;
                                                                    }
                                                                }
                                                                v28 = rsi302;
                                                                eax305 = fun_3dc0(9, rsi302, rdx23);
                                                                if (!eax305 && reinterpret_cast<unsigned char>(r13_303) > reinterpret_cast<unsigned char>(v103)) {
                                                                    r13_303 = v103;
                                                                }
                                                                r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_303) >> 1);
                                                                eax306 = fun_3dc0(5, v28, rdx23);
                                                                cf307 = 0;
                                                                zf308 = reinterpret_cast<uint1_t>(eax306 == 0);
                                                                below_or_equal309 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(zf308));
                                                                if (zf308 && (rax310 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(v103) >> 4) * 15), cf307 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r13_4) < reinterpret_cast<unsigned char>(rax310)), below_or_equal309 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r13_4) <= reinterpret_cast<unsigned char>(rax310)), !below_or_equal309)) {
                                                                    r13_4 = rax310;
                                                                }
                                                                physmem_available();
                                                                g5 = v103;
                                                                physmem_total(9, v28 + 4, rdx23);
                                                                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                                                g9 = (&v103)[4];
                                                                gd = *reinterpret_cast<int32_t*>(&v186);
                                                                __asm__("mulsd xmm2, xmm0");
                                                                __asm__("mulsd xmm0, [rip+0x111d6]");
                                                                __asm__("maxsd xmm1, xmm2");
                                                                __asm__("pxor xmm2, xmm2");
                                                                __asm__("cvtsi2sd xmm2, r13");
                                                                __asm__("comisd xmm2, xmm0");
                                                                if (below_or_equal309) {
                                                                    addr_5f64_347:
                                                                    below_or_equal311 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(r13_4 == 0)));
                                                                    if (reinterpret_cast<signed char>(r13_4) < reinterpret_cast<signed char>(0)) {
                                                                        rdx312 = r13_4;
                                                                        __asm__("pxor xmm0, xmm0");
                                                                        *reinterpret_cast<uint32_t*>(&rdx313) = *reinterpret_cast<uint32_t*>(&rdx312) & 1;
                                                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx313) + 4) = 0;
                                                                        below_or_equal311 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r13_4) >> 1) | rdx313) == 0)));
                                                                        __asm__("cvtsi2sd xmm0, rax");
                                                                        __asm__("addsd xmm0, xmm0");
                                                                    } else {
                                                                        __asm__("pxor xmm0, xmm0");
                                                                        __asm__("cvtsi2sd xmm0, r13");
                                                                    }
                                                                } else {
                                                                    g11 = *reinterpret_cast<int32_t*>(&v186 + 4);
                                                                    __asm__("comisd xmm0, xmm2");
                                                                    if (!cf307) 
                                                                        goto addr_619d_354; else 
                                                                        goto addr_5f5f_355;
                                                                }
                                                                __asm__("comisd xmm0, xmm1");
                                                                if (!below_or_equal311) {
                                                                    __asm__("comisd xmm1, [rip+0x1117c]");
                                                                    if (1) {
                                                                        __asm__("subsd xmm1, [rip+0x10f72]");
                                                                        __asm__("cvttsd2si r13, xmm1");
                                                                        __asm__("btc r13, 0x3f");
                                                                    } else {
                                                                        __asm__("cvttsd2si r13, xmm1");
                                                                    }
                                                                }
                                                                *reinterpret_cast<uint32_t*>(&rax314) = nmerge;
                                                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax314) + 4) = 0;
                                                                rax315 = reinterpret_cast<void**>(rax314 * 34);
                                                                if (reinterpret_cast<unsigned char>(rax315) < reinterpret_cast<unsigned char>(r13_4)) {
                                                                    rax315 = r13_4;
                                                                }
                                                                size_bound_0 = rax315;
                                                                goto addr_5dd0_329;
                                                                addr_619d_354:
                                                                __asm__("subsd xmm0, xmm2");
                                                                __asm__("cvttsd2si r13, xmm0");
                                                                __asm__("btc r13, 0x3f");
                                                                goto addr_5f64_347;
                                                                addr_5f5f_355:
                                                                __asm__("cvttsd2si r13, xmm0");
                                                                goto addr_5f64_347;
                                                                r12_55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_55) + reinterpret_cast<unsigned char>(rcx37));
                                                                ++rbp44;
                                                            } while (reinterpret_cast<unsigned char>(rbp44) < reinterpret_cast<unsigned char>(v57));
                                                            goto addr_5fe1_364;
                                                        } else {
                                                            addr_539c_365:
                                                            --v57;
                                                            *reinterpret_cast<unsigned char*>(&v76) = 0;
                                                            v316 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0xa0);
                                                            v317 = r15_18;
                                                            goto addr_53c0_366;
                                                        }
                                                        r12_55 = rsi285;
                                                        addr_5fe1_364:
                                                        rax318 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0x160);
                                                        v28 = rax318;
                                                        initbuf(rax318, v290, r12_55);
                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                        goto addr_539c_365;
                                                        addr_53c0_366:
                                                        while (rdx23 = v8, al319 = fillbuf(v28, v188, rdx23), rsp320 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8), !!al319) {
                                                            *reinterpret_cast<uint32_t*>(&rbx67) = 0;
                                                            *reinterpret_cast<int32_t*>(&rbx67 + 4) = 0;
                                                            r12_55 = reinterpret_cast<void**>(0);
                                                            if (!1) {
                                                                if (!v57) {
                                                                    r12_55 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v66)));
                                                                    saved_line = 0;
                                                                    if (reinterpret_cast<unsigned char>(v5) | reinterpret_cast<unsigned char>(v136)) {
                                                                        addr_543e_371:
                                                                        ++v5;
                                                                        *reinterpret_cast<uint32_t*>(&rbx67) = 0;
                                                                        *reinterpret_cast<int32_t*>(&rbx67 + 4) = 0;
                                                                        rax321 = maybe_create_temp(v316, 0, rdx23);
                                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp320) - 8 + 8);
                                                                        r13_4 = rax321 + 13;
                                                                    } else {
                                                                        xfclose(v188, v8, rdx23);
                                                                        rax322 = stream_open(v45, "w", rdx23);
                                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp320) - 8 + 8 - 8 + 8);
                                                                        if (!rax322) 
                                                                            goto addr_6864_89;
                                                                        v103 = rax322;
                                                                        r13_4 = v45;
                                                                        v5 = reinterpret_cast<void**>(0);
                                                                    }
                                                                    r14_38 = v323;
                                                                    if (reinterpret_cast<unsigned char>(r14_38) <= reinterpret_cast<unsigned char>(1)) {
                                                                        rdx46 = r13_4;
                                                                        write_unique(r12_55 + 0xffffffffffffffe0, v103, rdx46);
                                                                        rsp324 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                                    } else {
                                                                        r15_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp34) + 0x300);
                                                                        rax325 = heap_alloc(0x78c0, v102, rdx23);
                                                                        rsp326 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                                        v327 = rax325;
                                                                        rax328 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp326) + 0x2d8);
                                                                        v276 = rax328;
                                                                        fun_3e40(rax328);
                                                                        fun_3b60(r15_18);
                                                                        rax329 = xmalloc(v284, v284);
                                                                        *reinterpret_cast<void***>(rax329 + 48) = r14_38;
                                                                        r11_157 = rax329 + 88;
                                                                        rbp44 = rax329;
                                                                        *reinterpret_cast<void***>(rax329 + 40) = r14_38;
                                                                        *reinterpret_cast<void***>(rax329 + 24) = reinterpret_cast<void**>(0);
                                                                        *reinterpret_cast<void***>(rax329 + 16) = reinterpret_cast<void**>(0);
                                                                        *reinterpret_cast<void***>(rax329 + 8) = reinterpret_cast<void**>(0);
                                                                        *reinterpret_cast<void***>(rax329) = reinterpret_cast<void**>(0);
                                                                        *reinterpret_cast<void***>(rax329 + 32) = reinterpret_cast<void**>(0);
                                                                        *reinterpret_cast<void***>(rax329 + 56) = reinterpret_cast<void**>(0);
                                                                        *reinterpret_cast<uint32_t*>(rax329 + 80) = 0;
                                                                        *reinterpret_cast<unsigned char*>(rax329 + 84) = 0;
                                                                        v14 = r11_157;
                                                                        fun_3e40(r11_157);
                                                                        r10_96 = rbp44 + 0x80;
                                                                        v7 = r10_96;
                                                                        rax330 = init_node(rbp44, r10_96, r12_55, v11, r14_38);
                                                                        r9_94 = v103;
                                                                        r8_79 = v168;
                                                                        rsi331 = v11;
                                                                        sortlines(r12_55, rsi331, r14_38, v7, r8_79, r9_94, r13_4);
                                                                        rdx46 = r13_4;
                                                                        rcx37 = rax330;
                                                                        rsp332 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp326) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
                                                                        r12_55 = v14;
                                                                        r14_38 = v283;
                                                                        if (v102) {
                                                                            do {
                                                                                rdi333 = r12_55;
                                                                                --r14_38;
                                                                                r12_55 = r12_55 - 0xffffffffffffff80;
                                                                                fun_39a0(rdi333, rsi331);
                                                                                rsp332 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp332) - 8 + 8);
                                                                            } while (!reinterpret_cast<int1_t>(r14_38 == 0xffffffffffffffff));
                                                                        }
                                                                        fun_3750(rbp44, rsi331);
                                                                        heap_free(v327, rsi331);
                                                                        fun_3cc0(r15_18, rsi331);
                                                                        fun_39a0(v276, rsi331);
                                                                        rsp324 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp332) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                                                    }
                                                                    rsi13 = r13_4;
                                                                    xfclose(v103, rsi13, rdx46);
                                                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp324) - 8 + 8);
                                                                    if (1) 
                                                                        continue; else 
                                                                        goto addr_55c5_61;
                                                                } else {
                                                                    rdx23 = v334;
                                                                    rcx37 = v290 + 1;
                                                                    rbx67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v290) * v335);
                                                                    if (reinterpret_cast<unsigned char>(rcx37) < reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(-static_cast<int64_t>(reinterpret_cast<unsigned char>(rdx23))) - reinterpret_cast<unsigned char>(rbx67))) 
                                                                        goto addr_61f2_382;
                                                                }
                                                            }
                                                            saved_line = 0;
                                                            r12_55 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(v66)));
                                                            goto addr_543e_371;
                                                        }
                                                        rsi53 = v8;
                                                        r15_18 = v317;
                                                        xfclose(v188, rsi53, rdx23);
                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp320) - 8 + 8);
                                                        if (!v57) 
                                                            goto addr_6238_33;
                                                        addr_5ac0_385:
                                                        r15_18 = r15_18 + 8;
                                                        continue;
                                                        addr_61f2_382:
                                                        v136 = rdx23;
                                                        r15_18 = v317;
                                                        xfclose(v188, v8, rdx23);
                                                        rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp320) - 8 + 8);
                                                        goto addr_5ac0_385;
                                                    }
                                                    rdi196 = v8;
                                                    xfopen_part_0(rdi196, "r", rdx23);
                                                    r8_79 = r10_96;
                                                    rcx37 = r14_38;
                                                    *reinterpret_cast<uint32_t*>(&rdx46) = 0xffffff87;
                                                    *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                                    *reinterpret_cast<uint32_t*>(&rsi13) = *reinterpret_cast<uint32_t*>(&r9_94);
                                                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                                    xstrtol_fatal(rdi196, rsi13, 0xffffff87, rcx37, r8_79);
                                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8 - 8 + 8);
                                                    goto addr_69f9_221;
                                                    addr_52b2_302:
                                                    *reinterpret_cast<uint32_t*>(&rsi13) = 1;
                                                    *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                                    move_fd_part_0(eax277, eax277);
                                                    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                    goto addr_52bc_299;
                                                    addr_481f_268:
                                                    *reinterpret_cast<unsigned char*>(&v7) = 0;
                                                    *reinterpret_cast<uint32_t*>(&r14_38) = 0;
                                                    *reinterpret_cast<int32_t*>(&r14_38 + 4) = 0;
                                                    goto addr_4827_291;
                                                }
                                                addr_4688_283:
                                                if (!v45) 
                                                    goto addr_5ac9_172;
                                                eax336 = *reinterpret_cast<unsigned char*>(&v188);
                                                rdi16 = reinterpret_cast<void**>(0x1d010);
                                                opts_6 = *reinterpret_cast<signed char*>(&eax336);
                                                incompatible_options(0x1d010, 0x1d010);
                                                rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
                                                addr_46ab_6:
                                                thousands_sep_ignored = 1;
                                                addr_46b2_4:
                                                thousands_sep = 0x80;
                                                addr_4018_7:
                                                have_read_stdin = 0;
                                                rax337 = fun_3e90(rdi16, rsi13, rdx23);
                                                rbx67 = rax337;
                                                rax338 = fun_3780(rdi16, rsi13);
                                                rsp339 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp20) - 8 + 8 - 8 + 8);
                                                *reinterpret_cast<int32_t*>(&rdx340) = 0;
                                                *reinterpret_cast<int32_t*>(&rdx340 + 4) = 0;
                                                r11_157 = reinterpret_cast<void**>(0x1d75f);
                                                r10_96 = reinterpret_cast<void**>(0x1d65f);
                                                rdi341 = rax338;
                                                r9_94 = reinterpret_cast<void**>(0x1d55f);
                                                r8_79 = reinterpret_cast<void**>(0x1d45f);
                                                while (1) {
                                                    rsi342 = reinterpret_cast<unsigned char>(rdx340) * 4;
                                                    rcx37 = rdx340 + 1;
                                                    eax343 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx67) + reinterpret_cast<unsigned char>(rdx340) * 2));
                                                    if (!(*reinterpret_cast<unsigned char*>(&eax343) & 1)) {
                                                        if (rdx340 == 10) {
                                                            eax344 = *reinterpret_cast<uint16_t*>(*reinterpret_cast<void***>(rbx67) + 20);
                                                            g1d76a = 1;
                                                            g1d56a = 0;
                                                            *reinterpret_cast<uint16_t*>(&eax344) = reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&eax344) >> 14);
                                                            eax345 = (eax344 ^ 1) & 1;
                                                            g1d66a = *reinterpret_cast<signed char*>(&eax345);
                                                            eax346 = (*rdi341)->f28;
                                                            g1d46a = *reinterpret_cast<signed char*>(&eax346);
                                                        } else {
                                                            edx347 = eax343;
                                                            *reinterpret_cast<uint16_t*>(&eax343) = reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&eax343) >> 3);
                                                            r14d348 = 0;
                                                            *reinterpret_cast<uint16_t*>(&edx347) = reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&edx347) >> 14);
                                                            eax349 = (eax343 ^ 1) & 1;
                                                            *reinterpret_cast<uint32_t*>(&rdx350) = (edx347 ^ 1) & 1;
                                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx350) + 4) = 0;
                                                            goto addr_407f_393;
                                                        }
                                                    } else {
                                                        *reinterpret_cast<uint16_t*>(&eax343) = reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&eax343) >> 14);
                                                        r14d348 = 1;
                                                        eax349 = 0;
                                                        *reinterpret_cast<uint32_t*>(&rdx350) = (eax343 ^ 1) & 1;
                                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx350) + 4) = 0;
                                                        goto addr_407f_393;
                                                    }
                                                    addr_409e_395:
                                                    rdx340 = rcx37;
                                                    continue;
                                                    addr_407f_393:
                                                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d55f) + reinterpret_cast<unsigned char>(rcx37)) = *reinterpret_cast<signed char*>(&eax349);
                                                    rax351 = *rdi341;
                                                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d75f) + reinterpret_cast<unsigned char>(rcx37)) = *reinterpret_cast<signed char*>(&r14d348);
                                                    eax352 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rax351) + rsi342);
                                                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d65f) + reinterpret_cast<unsigned char>(rcx37)) = *reinterpret_cast<signed char*>(&rdx350);
                                                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x1d45f) + reinterpret_cast<unsigned char>(rcx37)) = *reinterpret_cast<signed char*>(&eax352);
                                                    if (rcx37 == 0x100) 
                                                        break; else 
                                                        goto addr_409e_395;
                                                }
                                                if (*reinterpret_cast<signed char*>(&r15_18)) {
                                                    v353 = r13_4;
                                                    *reinterpret_cast<int32_t*>(&r14_354) = 1;
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_354) + 4) = 0;
                                                    r13_355 = reinterpret_cast<struct s31*>(0x1d060);
                                                    do {
                                                        *reinterpret_cast<int32_t*>(&rdi356) = static_cast<int32_t>(r14_354 + 0x2000d);
                                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi356) + 4) = 0;
                                                        rax357 = fun_3bb0(rdi356);
                                                        rax358 = fun_3940(rax357);
                                                        rbp359 = rax358;
                                                        rax360 = xmalloc(rax358 + 1);
                                                        rsp339 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp339) - 8 + 8 - 8 + 8 - 8 + 8);
                                                        r13_355->f8 = *reinterpret_cast<int32_t*>(&r14_354);
                                                        r13_355->f0 = rax360;
                                                        if (rbp359) {
                                                            r9_94 = *reinterpret_cast<void***>(rbx67);
                                                            rdi361 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax357) + reinterpret_cast<unsigned char>(rbp359));
                                                            rdx362 = rax357;
                                                            *reinterpret_cast<int32_t*>(&rbp359) = 0;
                                                            *reinterpret_cast<int32_t*>(&rbp359 + 4) = 0;
                                                            do {
                                                                *reinterpret_cast<uint32_t*>(&rsi363) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx362));
                                                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi363) + 4) = 0;
                                                                if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r9_94) + reinterpret_cast<uint64_t>(rsi363 * 2))) & 1)) {
                                                                    ecx364 = *reinterpret_cast<unsigned char*>(0x1d460 + rsi363);
                                                                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax360) + reinterpret_cast<unsigned char>(rbp359)) = *reinterpret_cast<signed char*>(&ecx364);
                                                                    ++rbp359;
                                                                }
                                                                ++rdx362;
                                                            } while (rdi361 != rdx362);
                                                        }
                                                        ++r14_354;
                                                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax360) + reinterpret_cast<unsigned char>(rbp359)) = 0;
                                                        r13_355 = reinterpret_cast<struct s31*>(reinterpret_cast<int64_t>(r13_355) + 16);
                                                    } while (r14_354 != 13);
                                                    *reinterpret_cast<uint32_t*>(&rdx350) = 16;
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx350) + 4) = 0;
                                                    rcx37 = reinterpret_cast<void**>(0x6fb0);
                                                    *reinterpret_cast<int32_t*>(&rsi342) = 12;
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi342) + 4) = 0;
                                                    r13_4 = v353;
                                                    fun_3840(0x1d060, 12, 16);
                                                    rsp339 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp339) - 8 + 8);
                                                }
                                                rbp365 = reinterpret_cast<int32_t*>(0x15324);
                                                fun_3b00(0x1d3a0, rsi342, rdx350);
                                                rsp366 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp339) - 8 + 8);
                                                rax367 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp366) + 0x2d0);
                                                r12d368 = 14;
                                                v168 = rax367;
                                                r15_369 = rax367;
                                                while (1) {
                                                    rdx46 = r15_369;
                                                    *reinterpret_cast<int32_t*>(&rdi370) = r12d368;
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi370) + 4) = 0;
                                                    fun_3850(rdi370, rdi370);
                                                    rsp366 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp366) - 8 + 8);
                                                    if (v327 != 1) {
                                                        *reinterpret_cast<int32_t*>(&rsi371) = r12d368;
                                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi371) + 4) = 0;
                                                        fun_3e60(0x1d3a0, rsi371, rdx46);
                                                        rsp366 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp366) - 8 + 8);
                                                    }
                                                    if (reinterpret_cast<int1_t>(0x1534c == rbp365)) 
                                                        break;
                                                    r12d368 = *rbp365;
                                                    ++rbp365;
                                                }
                                                __asm__("movdqa xmm3, [rip+0x19151]");
                                                __asm__("movdqa xmm4, [rip+0x19159]");
                                                __asm__("movdqa xmm5, [rip+0x19154]");
                                                __asm__("movdqa xmm6, [rip+0x1915c]");
                                                v327 = 0x7f10;
                                                __asm__("movdqa xmm7, [rip+0x1915c]");
                                                __asm__("movdqa xmm2, [rip+0x19164]");
                                                __asm__("movups [rsp+0x2d8], xmm3");
                                                __asm__("movdqa xmm1, [rip+0x19164]");
                                                r12_372 = reinterpret_cast<int32_t*>(0x15324);
                                                __asm__("movdqa xmm3, [rip+0x1915c]");
                                                v188 = r13_4;
                                                r13d373 = 14;
                                                __asm__("movups [rsp+0x2e8], xmm4");
                                                __asm__("movups [rsp+0x2f8], xmm5");
                                                __asm__("movups [rsp+0x308], xmm6");
                                                __asm__("movups [rsp+0x318], xmm7");
                                                __asm__("movups [rsp+0x328], xmm2");
                                                __asm__("movups [rsp+0x338], xmm1");
                                                __asm__("movups [rsp+0x348], xmm3");
                                                while (1) {
                                                    *reinterpret_cast<int32_t*>(&rsi374) = r13d373;
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi374) + 4) = 0;
                                                    eax375 = fun_3d80(0x1d3a0, rsi374, rdx46);
                                                    rsp366 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp366) - 8 + 8);
                                                    if (!eax375) {
                                                        if (rbp365 == r12_372) 
                                                            break;
                                                    } else {
                                                        *reinterpret_cast<uint32_t*>(&rdx46) = 0;
                                                        *reinterpret_cast<int32_t*>(&rdx46 + 4) = 0;
                                                        *reinterpret_cast<int32_t*>(&rdi376) = r13d373;
                                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi376) + 4) = 0;
                                                        fun_3850(rdi376, rdi376);
                                                        rsp366 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp366) - 8 + 8);
                                                        if (rbp365 == r12_372) 
                                                            break;
                                                    }
                                                    r13d373 = *r12_372;
                                                    ++r12_372;
                                                }
                                                r13_4 = v188;
                                                *reinterpret_cast<int32_t*>(&r12_55) = 0;
                                                *reinterpret_cast<int32_t*>(&r12_55 + 4) = 0;
                                                fun_3ae0(17);
                                                *reinterpret_cast<uint32_t*>(&rbp44) = 0;
                                                r14_38 = reinterpret_cast<void**>(0x1c5e0);
                                                atexit(0x8b70);
                                                __asm__("pxor xmm0, xmm0");
                                                *reinterpret_cast<uint32_t*>(&rsi13) = 8;
                                                *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                                                __asm__("movups [rsp+0x118], xmm0");
                                                __asm__("movups [rsp+0x128], xmm0");
                                                __asm__("movups [rsp+0x138], xmm0");
                                                __asm__("movups [rsp+0x148], xmm0");
                                                rax377 = xnmalloc(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v5)), 8, rdx46);
                                                rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp366) - 8 + 8 - 8 + 8 - 8 + 8);
                                                *reinterpret_cast<signed char*>(&v276) = 0;
                                                v45 = reinterpret_cast<void**>(0);
                                                r15_18 = rax377;
                                                v102 = reinterpret_cast<void**>(0);
                                                v278 = reinterpret_cast<void**>(0);
                                                v28 = reinterpret_cast<void**>(0);
                                                *reinterpret_cast<unsigned char*>(&v188) = 0;
                                            }
                                        }
                                    }
                                }
                            } while (!*reinterpret_cast<signed char*>(&rbp44));
                            goto addr_5d1c_417;
                            addr_6415_153:
                            rax378 = quote(reinterpret_cast<int64_t>(rsp74) + 0x3c0);
                            r13_4 = rax378;
                            rax379 = fun_3920();
                            *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                            rcx37 = r13_4;
                            rdx46 = rax379;
                            fun_3c90();
                            rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8 - 8 + 8 - 8 + 8);
                            if (*reinterpret_cast<signed char*>(&rbp44)) 
                                goto addr_5964_62;
                        }
                        addr_5959_159:
                        if (*reinterpret_cast<unsigned char*>(&v5)) 
                            continue;
                        addr_5d1c_417:
                        rdi380 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp74) + 0x3c0);
                        r13_4 = reinterpret_cast<void**>(0x16c1d);
                        rax381 = quote(rdi380, rdi380);
                        rsp382 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8);
                        eax383 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(decimal_point)));
                        zf384 = eax383 == tab;
                        if (!zf384) {
                            rax385 = fun_3920();
                            rsp382 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp382) - 8 + 8);
                            r13_4 = rax385;
                        }
                        rax386 = fun_3920();
                        r8_79 = rax381;
                        rcx37 = r13_4;
                        *reinterpret_cast<uint32_t*>(&rsi13) = 0;
                        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                        rdx46 = rax386;
                        fun_3c90();
                        rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp382) - 8 + 8 - 8 + 8);
                    }
                }
            }
        }
    }
}

int64_t __libc_start_main = 0;

void fun_6b03() {
    __asm__("cli ");
    __libc_start_main(0x3f10, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x1d008;

void fun_3770(int64_t rdi);

int64_t fun_6ba3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_3770(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_6be3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s32 {
    signed char[8] pad8;
    int32_t f8;
};

uint64_t fun_6bf3(struct s32* rdi, uint64_t rsi) {
    __asm__("cli ");
    return static_cast<int64_t>(rdi->f8) % rsi;
}

struct s33 {
    signed char[8] pad8;
    int32_t f8;
};

struct s34 {
    signed char[8] pad8;
    int32_t f8;
};

int64_t fun_6c13(struct s33* rdi, struct s34* rsi) {
    int64_t rax3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f8 == *reinterpret_cast<int32_t*>(&rax3));
    return rax3;
}

void fun_6fb3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto fun_3ad0;
}

struct s35 {
    signed char[40] pad40;
    int64_t f28;
    int64_t f30;
    signed char[24] pad80;
    uint32_t f50;
};

struct s36 {
    signed char[40] pad40;
    int64_t f28;
    int64_t f30;
    signed char[24] pad80;
    uint32_t f50;
};

int64_t fun_78c3(struct s35* rdi, struct s36* rsi) {
    int64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    if (rdi->f50 == rsi->f50) {
        *reinterpret_cast<uint32_t*>(&rax3) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rdi->f30 + rdi->f28) < reinterpret_cast<uint64_t>(rsi->f30 + rsi->f28));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    } else {
        *reinterpret_cast<uint32_t*>(&rax4) = reinterpret_cast<uint1_t>(rdi->f50 < rsi->f50);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        return rax4;
    }
}

void fun_7f13(int32_t edi, void** rsi, void** rdx) {
    int32_t ebp4;
    void** rbx5;
    void** rdi6;
    int64_t rdi7;

    __asm__("cli ");
    ebp4 = edi;
    rbx5 = temphead;
    if (rbx5) {
        do {
            rdi6 = rbx5 + 13;
            fun_37e0(rdi6, rsi, rdx, rdi6, rsi, rdx);
            rbx5 = *reinterpret_cast<void***>(rbx5);
        } while (rbx5);
    }
    temphead = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi7) = ebp4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
    fun_3ae0(rdi7);
}

void fun_8b73() {
    void** rax1;
    void** v2;
    void** rax3;
    void** rdx4;
    int32_t eax5;
    void** rbx6;
    uint32_t eax7;
    unsigned char v8;
    void* rax9;

    __asm__("cli ");
    rax1 = g28;
    v2 = rax1;
    rax3 = temphead;
    if (rax3) {
        rdx4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x98 + 8);
        eax5 = fun_3c00();
        rbx6 = temphead;
        *reinterpret_cast<unsigned char*>(&eax7) = reinterpret_cast<uint1_t>(eax5 == 0);
        v8 = *reinterpret_cast<unsigned char*>(&eax7);
        if (rbx6) {
            do {
                fun_37e0(rbx6 + 13, 0x1d3a0, rdx4);
                rbx6 = *reinterpret_cast<void***>(rbx6);
            } while (rbx6);
            eax7 = v8;
        }
        temphead = reinterpret_cast<void**>(0);
        if (*reinterpret_cast<unsigned char*>(&eax7)) {
            fun_3c00();
        }
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v2) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_3950();
    }
}

struct s37 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    void** f28;
    signed char[7] pad48;
    void** f30;
};

int64_t fun_b883(struct s37* rdi) {
    void** rcx2;
    void** rdx3;
    void** rsi4;
    void** r9_5;
    void** v6;
    void** r8_7;
    void** rdi8;

    __asm__("cli ");
    rcx2 = rdi->f18;
    rdx3 = rdi->f10;
    rsi4 = rdi->f8;
    r9_5 = rdi->f28;
    v6 = rdi->f30;
    r8_7 = rdi->f20;
    rdi8 = rdi->f0;
    sortlines(rdi8, rsi4, rdx3, rcx2, r8_7, r9_5, v6);
    return 0;
}

void fun_3aa0(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_37f0(void** rdi, void** rsi, void** rdx, ...);

void fun_b8b3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** r12_6;
    void** rax7;
    void** r12_8;
    void** rax9;
    void** r12_10;
    void** rax11;
    void** r12_12;
    void** rax13;
    void** r12_14;
    void** rax15;
    void** r12_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** r12_22;
    void** rax23;
    void** r12_24;
    void** rax25;
    void** r12_26;
    void** rax27;
    void** r12_28;
    void** rax29;
    void** r12_30;
    void** rax31;
    void** r12_32;
    void** rax33;
    void** r12_34;
    void** rax35;
    void** rax36;
    void** r12_37;
    void** rax38;
    void** r12_39;
    void** rax40;
    void** r12_41;
    void** rax42;
    void** r12_43;
    void** rax44;
    void** r12_45;
    void** rax46;
    void** eax47;
    void** r13_48;
    void** rax49;
    void** rax50;
    int32_t eax51;
    void** rax52;
    void** rax53;
    void** rax54;
    int32_t eax55;
    void** rax56;
    void** r15_57;
    void** rax58;
    void** rax59;
    void** rax60;
    void** rdi61;
    void** r8_62;
    void** r9_63;
    int64_t v64;
    void** v65;
    void** v66;
    void** v67;
    void** v68;
    void** v69;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_3920();
            fun_3c40(1, rax5, r12_2, 1, rax5, r12_2);
            r12_6 = stdout;
            rax7 = fun_3920();
            fun_3aa0(rax7, r12_6, 5, r12_2);
            r12_8 = stdout;
            rax9 = fun_3920();
            fun_3aa0(rax9, r12_8, 5, r12_2);
            r12_10 = stdout;
            rax11 = fun_3920();
            fun_3aa0(rax11, r12_10, 5, r12_2);
            r12_12 = stdout;
            rax13 = fun_3920();
            fun_3aa0(rax13, r12_12, 5, r12_2);
            r12_14 = stdout;
            rax15 = fun_3920();
            fun_3aa0(rax15, r12_14, 5, r12_2);
            r12_16 = stdout;
            rax17 = fun_3920();
            fun_3aa0(rax17, r12_16, 5, r12_2);
            r12_18 = stdout;
            rax19 = fun_3920();
            fun_3aa0(rax19, r12_18, 5, r12_2);
            r12_20 = stdout;
            rax21 = fun_3920();
            fun_3aa0(rax21, r12_20, 5, r12_2);
            r12_22 = stdout;
            rax23 = fun_3920();
            fun_3aa0(rax23, r12_22, 5, r12_2);
            r12_24 = stdout;
            rax25 = fun_3920();
            fun_3aa0(rax25, r12_24, 5, r12_2);
            r12_26 = stdout;
            rax27 = fun_3920();
            fun_3aa0(rax27, r12_26, 5, r12_2);
            r12_28 = stdout;
            rax29 = fun_3920();
            fun_3aa0(rax29, r12_28, 5, r12_2);
            r12_30 = stdout;
            rax31 = fun_3920();
            fun_3aa0(rax31, r12_30, 5, r12_2);
            r12_32 = stdout;
            rax33 = fun_3920();
            fun_3aa0(rax33, r12_32, 5, r12_2);
            r12_34 = stdout;
            rax35 = fun_3920();
            fun_3aa0(rax35, r12_34, 5, r12_2);
            rax36 = fun_3920();
            fun_3c40(1, rax36, "/tmp", 1, rax36, "/tmp");
            r12_37 = stdout;
            rax38 = fun_3920();
            fun_3aa0(rax38, r12_37, 5, r12_2);
            r12_39 = stdout;
            rax40 = fun_3920();
            fun_3aa0(rax40, r12_39, 5, r12_2);
            r12_41 = stdout;
            rax42 = fun_3920();
            fun_3aa0(rax42, r12_41, 5, r12_2);
            r12_43 = stdout;
            rax44 = fun_3920();
            fun_3aa0(rax44, r12_43, 5, r12_2);
            r12_45 = stdout;
            rax46 = fun_3920();
            fun_3aa0(rax46, r12_45, 5, r12_2);
            do {
                if (1) 
                    break;
                eax47 = fun_3ad0("sort", 0, 5);
            } while (eax47);
            r13_48 = v4;
            if (!r13_48) {
                rax49 = fun_3920();
                fun_3c40(1, rax49, "GNU coreutils", 1, rax49, "GNU coreutils");
                rax50 = fun_3c30(5);
                if (!rax50 || (eax51 = fun_37f0(rax50, "en_", 3, rax50, "en_", 3), !eax51)) {
                    rax52 = fun_3920();
                    r13_48 = reinterpret_cast<void**>("sort");
                    fun_3c40(1, rax52, "https://www.gnu.org/software/coreutils/", 1, rax52, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_48 = reinterpret_cast<void**>("sort");
                    goto addr_be60_9;
                }
            } else {
                rax53 = fun_3920();
                fun_3c40(1, rax53, "GNU coreutils", 1, rax53, "GNU coreutils");
                rax54 = fun_3c30(5);
                if (!rax54 || (eax55 = fun_37f0(rax54, "en_", 3, rax54, "en_", 3), !eax55)) {
                    addr_bd66_11:
                    rax56 = fun_3920();
                    fun_3c40(1, rax56, "https://www.gnu.org/software/coreutils/", 1, rax56, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_48 == "sort")) {
                        r12_2 = reinterpret_cast<void**>(0x16c1d);
                    }
                } else {
                    addr_be60_9:
                    r15_57 = stdout;
                    rax58 = fun_3920();
                    fun_3aa0(rax58, r15_57, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_bd66_11;
                }
            }
            rax59 = fun_3920();
            fun_3c40(1, rax59, r13_48, 1, rax59, r13_48);
            addr_b90e_14:
            fun_3d90();
        }
    } else {
        rax60 = fun_3920();
        rdi61 = stderr;
        fun_3db0(rdi61, 1, rax60, r12_2, r8_62, r9_63, v64, v65, v66, v67, v68, v69);
        goto addr_b90e_14;
    }
}

void fun_be93() {
    __asm__("cli ");
    goto usage;
}

int64_t fun_bea3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    void** rax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_3940(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_37f0(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_bf23_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_bf70_6; else 
                    continue;
            } else {
                rax18 = fun_3940(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_bfa0_8;
                if (v16 == -1) 
                    goto addr_bf5e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_bf23_5;
            } else {
                rax19 = fun_3a80(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (*reinterpret_cast<int32_t*>(&rax19)) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_bf23_5;
            }
            addr_bf5e_10:
            v16 = rbx15;
            goto addr_bf23_5;
        }
    }
    addr_bf85_16:
    return v12;
    addr_bf70_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_bf85_16;
    addr_bfa0_8:
    v12 = rbx15;
    goto addr_bf85_16;
}

int64_t fun_bfb3(void** rdi, void*** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void*** rbp6;
    int64_t rbx7;
    void** eax8;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *rsi;
    if (!rdi5) {
        addr_bff8_2:
        return -1;
    } else {
        rbp6 = rsi;
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
        do {
            eax8 = fun_3ad0(rdi5, r12_4, rdx);
            if (!eax8) 
                break;
            ++rbx7;
            rdi5 = rbp6[rbx7 * 8];
        } while (rdi5);
        goto addr_bff8_2;
    }
    return rbx7;
}

void fun_c013(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_3920();
    } else {
        fun_3920();
    }
    quote_n();
    quotearg_n_style();
    goto fun_3c90;
}

void fun_c0a3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** v8;
    void** r12_9;
    void** r12_10;
    void** v11;
    void** rbp12;
    void** rbp13;
    void** v14;
    void** rbx15;
    void** r14_16;
    void** v17;
    void** rax18;
    void** r15_19;
    int64_t rbx20;
    void** rax21;
    void** rax22;
    void** rdi23;
    int64_t v24;
    void** v25;
    void** rax26;
    void** rdi27;
    int64_t v28;
    void** v29;
    void** rdi30;
    void** rax31;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    v11 = rbp12;
    rbp13 = rsi;
    v14 = rbx15;
    r14_16 = stderr;
    v17 = rdi;
    rax18 = fun_3920();
    fun_3aa0(rax18, r14_16, 5, rcx);
    r15_19 = *reinterpret_cast<void***>(rdi);
    *reinterpret_cast<int32_t*>(&rbx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx20) + 4) = 0;
    if (r15_19) {
        do {
            if (!rbx20 || (rax21 = fun_3a80(r13_7, rbp13, r12_10, r13_7, rbp13, r12_10), !!*reinterpret_cast<int32_t*>(&rax21))) {
                r13_7 = rbp13;
                rax22 = quote(r15_19, r15_19);
                rdi23 = stderr;
                fun_3db0(rdi23, 1, "\n  - %s", rax22, r8, r9, v24, v17, v25, v14, v11, v8);
            } else {
                rax26 = quote(r15_19, r15_19);
                rdi27 = stderr;
                fun_3db0(rdi27, 1, ", %s", rax26, r8, r9, v28, v17, v29, v14, v11, v8);
            }
            ++rbx20;
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<unsigned char>(r12_10));
            r15_19 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v17) + reinterpret_cast<uint64_t>(rbx20 * 8));
        } while (r15_19);
    }
    rdi30 = stderr;
    rax31 = *reinterpret_cast<void***>(rdi30 + 40);
    if (reinterpret_cast<unsigned char>(rax31) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi30 + 48))) {
        goto fun_39c0;
    } else {
        *reinterpret_cast<void***>(rdi30 + 40) = rax31 + 1;
        *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(void** rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_c1d3(int64_t rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    int64_t r13_10;
    int64_t r12_11;
    void** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    void** eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_c217_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_c28e_4;
        } else {
            addr_c28e_4:
            return rax14;
        }
    }
    rdi15 = *reinterpret_cast<void***>(rdx);
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_3ad0(rdi15, r14_9, rdx);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = *reinterpret_cast<void***>(rbp12 + rbx16 * 8);
        } while (rdi15);
        goto addr_c210_8;
    } else {
        goto addr_c210_8;
    }
    return rbx16;
    addr_c210_8:
    rax14 = -1;
    goto addr_c217_3;
}

struct s38 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_c2a3(void** rdi, struct s38* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    void** rax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            rax10 = fun_3a80(r12_6, rbp9, r13_7);
            if (!*reinterpret_cast<int32_t*>(&rax10)) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t file_name = 0;

void fun_c303(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_c313(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

void fun_c323() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    void** rsi11;
    void** r8_12;
    void** rcx13;
    void** rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_37d0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_3920();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_c3b3_5;
        rax10 = quotearg_colon();
        rsi11 = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<void**>("%s: %s");
        fun_3c90();
    }
    while (1) {
        *reinterpret_cast<uint32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_3800(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_c3b3_5:
        rsi11 = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<void**>("%s");
        fun_3c90();
    }
}

struct s39 {
    int64_t f0;
    int64_t f8;
    int64_t f10;
    int32_t f18;
};

void fun_c3d3(struct s39* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0xefcdab8967452301;
    rdi->f8 = 0x1032547698badcfe;
    rdi->f18 = 0;
    return;
}

struct s40 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
};

struct s41 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
};

struct s40* fun_c403(struct s41* rdi, struct s40* rsi) {
    __asm__("cli ");
    rsi->f0 = rdi->f0;
    rsi->f4 = rdi->f4;
    rsi->f8 = rdi->f8;
    rsi->fc = rdi->fc;
    return rsi;
}

struct s42 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    int32_t f18;
    int32_t f1c;
    int32_t f20;
    int32_t f24;
    int32_t f28;
    int32_t f2c;
    int32_t f30;
    int32_t f34;
    int32_t f38;
    int32_t f3c;
};

struct s43 {
    uint32_t f0;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
    uint32_t f10;
    int32_t f14;
};

void fun_c423(struct s42* rdi, uint64_t rsi, struct s43* rdx) {
    struct s42* rbx4;
    int64_t r11_5;
    struct s42* rcx6;
    uint32_t r10d7;
    uint32_t v8;
    uint32_t eax9;
    struct s43* v10;
    uint32_t v11;
    int32_t eax12;
    uint32_t tmp32_13;
    uint64_t rsi14;
    int32_t esi15;
    struct s42* v16;
    int64_t r15_17;
    int64_t r14_18;
    int64_t rdi19;
    int64_t rax20;
    int64_t rsi21;
    int64_t r8_22;
    int64_t r10_23;
    int64_t rbp24;
    int64_t r13_25;
    int64_t r12_26;
    int64_t r8_27;
    int64_t rax28;
    int64_t rcx29;
    int64_t rdx30;
    int64_t r9_31;
    int64_t rcx32;
    int64_t r9_33;
    int64_t rsi34;
    int64_t r10_35;
    int64_t rax36;
    int64_t rdx37;
    int64_t rbp38;
    int64_t rcx39;
    int64_t rbp40;
    int64_t rsi41;
    int64_t r12_42;
    int64_t rax43;
    int64_t rdi44;
    int64_t rdx45;
    int64_t r8_46;
    int64_t rcx47;
    int64_t rsi48;
    int64_t rax49;
    int64_t rdx50;
    int64_t rcx51;
    int64_t rdi52;
    int64_t rsi53;
    int64_t rax54;
    int64_t rdi55;
    int64_t rdx56;
    int64_t rcx57;
    int64_t rdi58;
    int64_t rsi59;
    int64_t rdi60;
    int64_t rax61;
    int64_t rdx62;
    int64_t rdi63;
    int64_t r10_64;
    int64_t rcx65;
    int64_t r10_66;
    int64_t rsi67;
    int64_t rax68;
    int64_t rdi69;
    int64_t rdx70;
    int64_t rcx71;
    int64_t rdi72;
    int64_t rsi73;
    int64_t rax74;
    int64_t rdx75;
    int64_t rsi76;
    uint32_t ecx77;
    int64_t r10_78;
    int64_t rdi79;
    int64_t rax80;
    int64_t rcx81;
    int64_t rax82;
    int64_t rdx83;
    int64_t rsi84;
    int64_t rdi85;
    int64_t rax86;
    int64_t rdi87;
    int64_t rcx88;
    int64_t rdi89;
    int64_t rsi90;
    int64_t rdx91;
    int64_t rsi92;
    int64_t rcx93;
    int64_t rdi94;
    int64_t rax95;
    int64_t rdx96;
    int64_t rdi97;
    int64_t rdx98;
    int64_t rsi99;
    int64_t rdx100;
    int64_t r10_101;
    int64_t rcx102;
    int64_t rax103;
    int64_t rdi104;
    int64_t r14_105;
    int64_t rax106;
    int64_t rdx107;
    int64_t rcx108;
    int64_t r10_109;
    int64_t rsi110;
    int64_t rbp111;
    int64_t rax112;
    int64_t rdx113;
    int64_t rdi114;
    int64_t rcx115;
    int64_t r14_116;
    int64_t rsi117;
    int64_t r14_118;
    int64_t rax119;
    int64_t r9_120;
    int64_t rdx121;
    int64_t r10_122;
    int64_t rcx123;
    int64_t rsi124;
    int64_t rdi125;
    uint32_t eax126;
    int64_t rcx127;
    uint32_t edx128;
    uint32_t ecx129;

    __asm__("cli ");
    rbx4 = rdi;
    *reinterpret_cast<uint32_t*>(&r11_5) = rdx->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
    rcx6 = reinterpret_cast<struct s42*>(reinterpret_cast<uint64_t>(rbx4) + (rsi & 0xfffffffffffffffc));
    r10d7 = rdx->f8;
    v8 = rdx->f0;
    eax9 = rdx->fc;
    v10 = rdx;
    v11 = eax9;
    eax12 = 0;
    tmp32_13 = *reinterpret_cast<uint32_t*>(&rsi) + rdx->f10;
    *reinterpret_cast<unsigned char*>(&eax12) = reinterpret_cast<uint1_t>(tmp32_13 < *reinterpret_cast<uint32_t*>(&rsi));
    rsi14 = rsi >> 32;
    esi15 = *reinterpret_cast<int32_t*>(&rsi14) + rdx->f14;
    v16 = rcx6;
    rdx->f10 = tmp32_13;
    rdx->f14 = esi15 + eax12;
    if (reinterpret_cast<uint64_t>(rbx4) < reinterpret_cast<uint64_t>(rcx6)) {
        *reinterpret_cast<uint32_t*>(&r15_17) = r10d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_17) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&r14_18) = rbx4->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_18) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi19) = v11;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax20) = v8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax20) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi21) = rbx4->f4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_22) = rbx4->fc;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_22) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r10_23) = rbx4->f14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_23) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbp24) = rbx4->f18;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp24) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r13_25) = rbx4->f1c;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_25) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_26) = rbx4->f24;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_26) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_27) = rbx4->f2c;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_27) + 4) = 0;
            __asm__("rol eax, 0x7");
            *reinterpret_cast<uint32_t*>(&rax28) = ((*reinterpret_cast<uint32_t*>(&rdi19) ^ *reinterpret_cast<uint32_t*>(&r15_17)) & *reinterpret_cast<uint32_t*>(&r11_5) ^ *reinterpret_cast<uint32_t*>(&rdi19)) + static_cast<int32_t>(r14_18 + rax20 - 0x28955b88) + *reinterpret_cast<uint32_t*>(&r11_5);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx29) = rbx4->f8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx29) + 4) = 0;
            __asm__("rol edx, 0xc");
            *reinterpret_cast<uint32_t*>(&rdx30) = ((*reinterpret_cast<uint32_t*>(&r11_5) ^ *reinterpret_cast<uint32_t*>(&r15_17)) & *reinterpret_cast<uint32_t*>(&rax28) ^ *reinterpret_cast<uint32_t*>(&r15_17)) + static_cast<int32_t>(rsi21 + rdi19 - 0x173848aa) + *reinterpret_cast<uint32_t*>(&rax28);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx30) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_31) = rbx4->f10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_31) + 4) = 0;
            __asm__("ror ecx, 0xf");
            *reinterpret_cast<uint32_t*>(&rcx32) = ((*reinterpret_cast<uint32_t*>(&rax28) ^ *reinterpret_cast<uint32_t*>(&r11_5)) & *reinterpret_cast<uint32_t*>(&rdx30) ^ *reinterpret_cast<uint32_t*>(&r11_5)) + static_cast<int32_t>(rcx29 + r15_17 + 0x242070db) + *reinterpret_cast<uint32_t*>(&rdx30);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx32) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_33) = rbx4->f3c;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_33) + 4) = 0;
            __asm__("ror esi, 0xa");
            *reinterpret_cast<uint32_t*>(&rsi34) = ((*reinterpret_cast<uint32_t*>(&rax28) ^ *reinterpret_cast<uint32_t*>(&rdx30)) & *reinterpret_cast<uint32_t*>(&rcx32) ^ *reinterpret_cast<uint32_t*>(&rax28)) + static_cast<int32_t>(r8_22 + r11_5 - 0x3e423112) + *reinterpret_cast<uint32_t*>(&rcx32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi34) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r10_35) = *reinterpret_cast<int32_t*>(&rbp24);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_35) + 4) = 0;
            __asm__("rol eax, 0x7");
            *reinterpret_cast<uint32_t*>(&rax36) = ((*reinterpret_cast<uint32_t*>(&rdx30) ^ *reinterpret_cast<uint32_t*>(&rcx32)) & *reinterpret_cast<uint32_t*>(&rsi34) ^ *reinterpret_cast<uint32_t*>(&rdx30)) + static_cast<int32_t>(r9_31 + rax28 - 0xa83f051) + *reinterpret_cast<uint32_t*>(&rsi34);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax36) + 4) = 0;
            __asm__("rol edx, 0xc");
            *reinterpret_cast<uint32_t*>(&rdx37) = ((*reinterpret_cast<uint32_t*>(&rcx32) ^ *reinterpret_cast<uint32_t*>(&rsi34)) & *reinterpret_cast<uint32_t*>(&rax36) ^ *reinterpret_cast<uint32_t*>(&rcx32)) + static_cast<int32_t>(r10_23 + rdx30 + 0x4787c62a) + *reinterpret_cast<uint32_t*>(&rax36);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx37) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbp38) = rbx4->f20;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp38) + 4) = 0;
            __asm__("ror ecx, 0xf");
            *reinterpret_cast<uint32_t*>(&rcx39) = ((*reinterpret_cast<uint32_t*>(&rsi34) ^ *reinterpret_cast<uint32_t*>(&rax36)) & *reinterpret_cast<uint32_t*>(&rdx37) ^ *reinterpret_cast<uint32_t*>(&rsi34)) + static_cast<int32_t>(rbp24 + rcx32 - 0x57cfb9ed) + *reinterpret_cast<uint32_t*>(&rdx37);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx39) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbp40) = rbx4->f30;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp40) + 4) = 0;
            __asm__("ror esi, 0xa");
            *reinterpret_cast<uint32_t*>(&rsi41) = ((*reinterpret_cast<uint32_t*>(&rax36) ^ *reinterpret_cast<uint32_t*>(&rdx37)) & *reinterpret_cast<uint32_t*>(&rcx39) ^ *reinterpret_cast<uint32_t*>(&rax36)) + static_cast<int32_t>(r13_25 + rsi34 - 0x2b96aff) + *reinterpret_cast<uint32_t*>(&rcx39);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_42) = rbx4->f38;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_42) + 4) = 0;
            __asm__("rol eax, 0x7");
            *reinterpret_cast<uint32_t*>(&rax43) = ((*reinterpret_cast<uint32_t*>(&rdx37) ^ *reinterpret_cast<uint32_t*>(&rcx39)) & *reinterpret_cast<uint32_t*>(&rsi41) ^ *reinterpret_cast<uint32_t*>(&rdx37)) + static_cast<int32_t>(rbp38 + rax36 + 0x698098d8) + *reinterpret_cast<uint32_t*>(&rsi41);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi44) = rbx4->f28;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi44) + 4) = 0;
            __asm__("rol edx, 0xc");
            *reinterpret_cast<uint32_t*>(&rdx45) = ((*reinterpret_cast<uint32_t*>(&rcx39) ^ *reinterpret_cast<uint32_t*>(&rsi41)) & *reinterpret_cast<uint32_t*>(&rax43) ^ *reinterpret_cast<uint32_t*>(&rcx39)) + static_cast<int32_t>(r12_26 + rdx37 - 0x74bb0851) + *reinterpret_cast<uint32_t*>(&rax43);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx45) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_46) = rbx4->f34;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_46) + 4) = 0;
            __asm__("ror ecx, 0xf");
            ++rbx4;
            *reinterpret_cast<uint32_t*>(&rcx47) = ((*reinterpret_cast<uint32_t*>(&rsi41) ^ *reinterpret_cast<uint32_t*>(&rax43)) & *reinterpret_cast<uint32_t*>(&rdx45) ^ *reinterpret_cast<uint32_t*>(&rsi41)) + static_cast<int32_t>(rdi44 + rcx39 - 0xa44f) + *reinterpret_cast<uint32_t*>(&rdx45);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx47) + 4) = 0;
            __asm__("ror esi, 0xa");
            *reinterpret_cast<uint32_t*>(&rsi48) = ((*reinterpret_cast<uint32_t*>(&rax43) ^ *reinterpret_cast<uint32_t*>(&rdx45)) & *reinterpret_cast<uint32_t*>(&rcx47) ^ *reinterpret_cast<uint32_t*>(&rax43)) + static_cast<int32_t>(r8_27 + rsi41 - 0x76a32842) + *reinterpret_cast<uint32_t*>(&rcx47);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi48) + 4) = 0;
            __asm__("rol eax, 0x7");
            *reinterpret_cast<uint32_t*>(&rax49) = ((*reinterpret_cast<uint32_t*>(&rdx45) ^ *reinterpret_cast<uint32_t*>(&rcx47)) & *reinterpret_cast<uint32_t*>(&rsi48) ^ *reinterpret_cast<uint32_t*>(&rdx45)) + static_cast<int32_t>(rbp40 + rax43 + 0x6b901122) + *reinterpret_cast<uint32_t*>(&rsi48);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax49) + 4) = 0;
            __asm__("rol edx, 0xc");
            *reinterpret_cast<uint32_t*>(&rdx50) = ((*reinterpret_cast<uint32_t*>(&rcx47) ^ *reinterpret_cast<uint32_t*>(&rsi48)) & *reinterpret_cast<uint32_t*>(&rax49) ^ *reinterpret_cast<uint32_t*>(&rcx47)) + static_cast<int32_t>(r8_46 + rdx45 - 0x2678e6d) + *reinterpret_cast<uint32_t*>(&rax49);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx50) + 4) = 0;
            __asm__("ror ecx, 0xf");
            *reinterpret_cast<uint32_t*>(&rcx51) = ((*reinterpret_cast<uint32_t*>(&rsi48) ^ *reinterpret_cast<uint32_t*>(&rax49)) & *reinterpret_cast<uint32_t*>(&rdx50) ^ *reinterpret_cast<uint32_t*>(&rsi48)) + static_cast<int32_t>(r12_42 + rcx47 - 0x5986bc72) + *reinterpret_cast<uint32_t*>(&rdx50);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx51) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi52) = *reinterpret_cast<int32_t*>(&rsi21);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi52) + 4) = 0;
            __asm__("ror esi, 0xa");
            *reinterpret_cast<uint32_t*>(&rsi53) = ((*reinterpret_cast<uint32_t*>(&rax49) ^ *reinterpret_cast<uint32_t*>(&rdx50)) & *reinterpret_cast<uint32_t*>(&rcx51) ^ *reinterpret_cast<uint32_t*>(&rax49)) + static_cast<int32_t>(r9_33 + rsi48 + 0x49b40821) + *reinterpret_cast<uint32_t*>(&rcx51);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi53) + 4) = 0;
            __asm__("rol eax, 0x5");
            *reinterpret_cast<uint32_t*>(&rax54) = ((*reinterpret_cast<uint32_t*>(&rcx51) ^ *reinterpret_cast<uint32_t*>(&rsi53)) & *reinterpret_cast<uint32_t*>(&rdx50) ^ *reinterpret_cast<uint32_t*>(&rcx51)) + static_cast<int32_t>(rdi52 + rax49 - 0x9e1da9e) + *reinterpret_cast<uint32_t*>(&rsi53);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax54) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi55) = *reinterpret_cast<int32_t*>(&r8_27);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
            __asm__("rol edx, 0x9");
            *reinterpret_cast<uint32_t*>(&rdx56) = ((*reinterpret_cast<uint32_t*>(&rsi53) ^ *reinterpret_cast<uint32_t*>(&rax54)) & *reinterpret_cast<uint32_t*>(&rcx51) ^ *reinterpret_cast<uint32_t*>(&rsi53)) + static_cast<int32_t>(r10_35 + rdx50 - 0x3fbf4cc0) + *reinterpret_cast<uint32_t*>(&rax54);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx56) + 4) = 0;
            __asm__("rol ecx, 0xe");
            *reinterpret_cast<uint32_t*>(&rcx57) = ((*reinterpret_cast<uint32_t*>(&rax54) ^ *reinterpret_cast<uint32_t*>(&rdx56)) & *reinterpret_cast<uint32_t*>(&rsi53) ^ *reinterpret_cast<uint32_t*>(&rax54)) + static_cast<int32_t>(rdi55 + rcx51 + 0x265e5a51) + *reinterpret_cast<uint32_t*>(&rdx56);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx57) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi58) = *reinterpret_cast<int32_t*>(&r10_23);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi58) + 4) = 0;
            __asm__("ror esi, 0xc");
            *reinterpret_cast<uint32_t*>(&rsi59) = ((*reinterpret_cast<uint32_t*>(&rdx56) ^ *reinterpret_cast<uint32_t*>(&rcx57)) & *reinterpret_cast<uint32_t*>(&rax54) ^ *reinterpret_cast<uint32_t*>(&rdx56)) + static_cast<int32_t>(r14_18 + rsi53 - 0x16493856) + *reinterpret_cast<uint32_t*>(&rcx57);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi59) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi60) = *reinterpret_cast<int32_t*>(&rdi44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi60) + 4) = 0;
            __asm__("rol eax, 0x5");
            *reinterpret_cast<uint32_t*>(&rax61) = ((*reinterpret_cast<uint32_t*>(&rcx57) ^ *reinterpret_cast<uint32_t*>(&rsi59)) & *reinterpret_cast<uint32_t*>(&rdx56) ^ *reinterpret_cast<uint32_t*>(&rcx57)) + static_cast<int32_t>(rdi58 + rax54 - 0x29d0efa3) + *reinterpret_cast<uint32_t*>(&rsi59);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax61) + 4) = 0;
            __asm__("rol edx, 0x9");
            *reinterpret_cast<uint32_t*>(&rdx62) = ((*reinterpret_cast<uint32_t*>(&rsi59) ^ *reinterpret_cast<uint32_t*>(&rax61)) & *reinterpret_cast<uint32_t*>(&rcx57) ^ *reinterpret_cast<uint32_t*>(&rsi59)) + static_cast<int32_t>(rdi60 + rdx56 + 0x2441453) + *reinterpret_cast<uint32_t*>(&rax61);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx62) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi63) = *reinterpret_cast<int32_t*>(&r9_31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi63) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r10_64) = *reinterpret_cast<int32_t*>(&r12_26);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_64) + 4) = 0;
            __asm__("rol ecx, 0xe");
            *reinterpret_cast<uint32_t*>(&rcx65) = ((*reinterpret_cast<uint32_t*>(&rax61) ^ *reinterpret_cast<uint32_t*>(&rdx62)) & *reinterpret_cast<uint32_t*>(&rsi59) ^ *reinterpret_cast<uint32_t*>(&rax61)) + static_cast<int32_t>(r9_33 + rcx57 - 0x275e197f) + *reinterpret_cast<uint32_t*>(&rdx62);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx65) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r10_66) = *reinterpret_cast<int32_t*>(&rbp38);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_66) + 4) = 0;
            __asm__("ror esi, 0xc");
            *reinterpret_cast<uint32_t*>(&rsi67) = ((*reinterpret_cast<uint32_t*>(&rdx62) ^ *reinterpret_cast<uint32_t*>(&rcx65)) & *reinterpret_cast<uint32_t*>(&rax61) ^ *reinterpret_cast<uint32_t*>(&rdx62)) + static_cast<int32_t>(rdi63 + rsi59 - 0x182c0438) + *reinterpret_cast<uint32_t*>(&rcx65);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi67) + 4) = 0;
            __asm__("rol eax, 0x5");
            *reinterpret_cast<uint32_t*>(&rax68) = ((*reinterpret_cast<uint32_t*>(&rcx65) ^ *reinterpret_cast<uint32_t*>(&rsi67)) & *reinterpret_cast<uint32_t*>(&rdx62) ^ *reinterpret_cast<uint32_t*>(&rcx65)) + static_cast<int32_t>(r10_64 + rax61 + 0x21e1cde6) + *reinterpret_cast<uint32_t*>(&rsi67);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax68) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi69) = *reinterpret_cast<int32_t*>(&r8_22);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi69) + 4) = 0;
            __asm__("rol edx, 0x9");
            *reinterpret_cast<uint32_t*>(&rdx70) = ((*reinterpret_cast<uint32_t*>(&rsi67) ^ *reinterpret_cast<uint32_t*>(&rax68)) & *reinterpret_cast<uint32_t*>(&rcx65) ^ *reinterpret_cast<uint32_t*>(&rsi67)) + static_cast<int32_t>(r12_42 + rdx62 - 0x3cc8f82a) + *reinterpret_cast<uint32_t*>(&rax68);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx70) + 4) = 0;
            __asm__("rol ecx, 0xe");
            *reinterpret_cast<uint32_t*>(&rcx71) = ((*reinterpret_cast<uint32_t*>(&rax68) ^ *reinterpret_cast<uint32_t*>(&rdx70)) & *reinterpret_cast<uint32_t*>(&rsi67) ^ *reinterpret_cast<uint32_t*>(&rax68)) + static_cast<int32_t>(rdi69 + rcx65 - 0xb2af279) + *reinterpret_cast<uint32_t*>(&rdx70);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx71) + 4) = 0;
            __asm__("ror edi, 0xc");
            *reinterpret_cast<uint32_t*>(&rdi72) = ((*reinterpret_cast<uint32_t*>(&rdx70) ^ *reinterpret_cast<uint32_t*>(&rcx71)) & *reinterpret_cast<uint32_t*>(&rax68) ^ *reinterpret_cast<uint32_t*>(&rdx70)) + static_cast<int32_t>(r10_66 + rsi67 + 0x455a14ed) + *reinterpret_cast<uint32_t*>(&rcx71);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi72) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi73) = *reinterpret_cast<int32_t*>(&rcx29);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi73) + 4) = 0;
            __asm__("rol eax, 0x5");
            *reinterpret_cast<uint32_t*>(&rax74) = ((*reinterpret_cast<uint32_t*>(&rcx71) ^ *reinterpret_cast<uint32_t*>(&rdi72)) & *reinterpret_cast<uint32_t*>(&rdx70) ^ *reinterpret_cast<uint32_t*>(&rcx71)) + static_cast<int32_t>(r8_46 + rax68 - 0x561c16fb) + *reinterpret_cast<uint32_t*>(&rdi72);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
            __asm__("rol edx, 0x9");
            *reinterpret_cast<uint32_t*>(&rdx75) = ((*reinterpret_cast<uint32_t*>(&rdi72) ^ *reinterpret_cast<uint32_t*>(&rax74)) & *reinterpret_cast<uint32_t*>(&rcx71) ^ *reinterpret_cast<uint32_t*>(&rdi72)) + static_cast<int32_t>(rsi73 + rdx70 - 0x3105c08) + *reinterpret_cast<uint32_t*>(&rax74);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            __asm__("rol esi, 0xe");
            *reinterpret_cast<uint32_t*>(&rsi76) = ((*reinterpret_cast<uint32_t*>(&rax74) ^ *reinterpret_cast<uint32_t*>(&rdx75)) & *reinterpret_cast<uint32_t*>(&rdi72) ^ *reinterpret_cast<uint32_t*>(&rax74)) + static_cast<int32_t>(r13_25 + rcx71 + 0x676f02d9) + *reinterpret_cast<uint32_t*>(&rdx75);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi76) + 4) = 0;
            ecx77 = *reinterpret_cast<uint32_t*>(&rdx75) ^ *reinterpret_cast<uint32_t*>(&rsi76);
            *reinterpret_cast<int32_t*>(&r10_78) = *reinterpret_cast<int32_t*>(&r10_23);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_78) + 4) = 0;
            __asm__("ror edi, 0xc");
            *reinterpret_cast<uint32_t*>(&rdi79) = (ecx77 & *reinterpret_cast<uint32_t*>(&rax74) ^ *reinterpret_cast<uint32_t*>(&rdx75)) + static_cast<int32_t>(rbp40 + rdi72 - 0x72d5b376) + *reinterpret_cast<uint32_t*>(&rsi76);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi79) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rax80) = *reinterpret_cast<int32_t*>(&rbp38);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax80) + 4) = 0;
            __asm__("rol ecx, 0x4");
            *reinterpret_cast<uint32_t*>(&rcx81) = (ecx77 ^ *reinterpret_cast<uint32_t*>(&rdi79)) + static_cast<int32_t>(r10_78 + rax74 - 0x5c6be) + *reinterpret_cast<uint32_t*>(&rdi79);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx81) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rax82) = *reinterpret_cast<int32_t*>(&r8_27);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax82) + 4) = 0;
            __asm__("rol edx, 0xb");
            *reinterpret_cast<uint32_t*>(&rdx83) = (*reinterpret_cast<uint32_t*>(&rsi76) ^ *reinterpret_cast<uint32_t*>(&rdi79) ^ *reinterpret_cast<uint32_t*>(&rcx81)) + static_cast<int32_t>(rax80 + rdx75 - 0x788e097f) + *reinterpret_cast<uint32_t*>(&rcx81);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx83) + 4) = 0;
            __asm__("rol esi, 0x10");
            *reinterpret_cast<uint32_t*>(&rsi84) = (*reinterpret_cast<uint32_t*>(&rdi79) ^ *reinterpret_cast<uint32_t*>(&rcx81) ^ *reinterpret_cast<uint32_t*>(&rdx83)) + static_cast<int32_t>(rax82 + rsi76 + 0x6d9d6122) + *reinterpret_cast<uint32_t*>(&rdx83);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi84) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi85) = *reinterpret_cast<int32_t*>(&rsi21);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi85) + 4) = 0;
            __asm__("ror eax, 0x9");
            *reinterpret_cast<uint32_t*>(&rax86) = (*reinterpret_cast<uint32_t*>(&rcx81) ^ *reinterpret_cast<uint32_t*>(&rdx83) ^ *reinterpret_cast<uint32_t*>(&rsi84)) + static_cast<int32_t>(r12_42 + rdi79 - 0x21ac7f4) + *reinterpret_cast<uint32_t*>(&rsi84);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax86) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi87) = *reinterpret_cast<int32_t*>(&r9_31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi87) + 4) = 0;
            __asm__("rol ecx, 0x4");
            *reinterpret_cast<uint32_t*>(&rcx88) = (*reinterpret_cast<uint32_t*>(&rdx83) ^ *reinterpret_cast<uint32_t*>(&rsi84) ^ *reinterpret_cast<uint32_t*>(&rax86)) + static_cast<int32_t>(rdi85 + rcx81 - 0x5b4115bc) + *reinterpret_cast<uint32_t*>(&rax86);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx88) + 4) = 0;
            __asm__("rol edi, 0xb");
            *reinterpret_cast<uint32_t*>(&rdi89) = (*reinterpret_cast<uint32_t*>(&rsi84) ^ *reinterpret_cast<uint32_t*>(&rax86) ^ *reinterpret_cast<uint32_t*>(&rcx88)) + static_cast<int32_t>(rdi87 + rdx83 + 0x4bdecfa9) + *reinterpret_cast<uint32_t*>(&rcx88);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi89) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi90) = *reinterpret_cast<int32_t*>(&rdi44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi90) + 4) = 0;
            __asm__("rol edx, 0x10");
            *reinterpret_cast<uint32_t*>(&rdx91) = (*reinterpret_cast<uint32_t*>(&rax86) ^ *reinterpret_cast<uint32_t*>(&rcx88) ^ *reinterpret_cast<uint32_t*>(&rdi89)) + static_cast<int32_t>(r13_25 + rsi84 - 0x944b4a0) + *reinterpret_cast<uint32_t*>(&rdi89);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx91) + 4) = 0;
            __asm__("ror esi, 0x9");
            *reinterpret_cast<uint32_t*>(&rsi92) = (*reinterpret_cast<uint32_t*>(&rcx88) ^ *reinterpret_cast<uint32_t*>(&rdi89) ^ *reinterpret_cast<uint32_t*>(&rdx91)) + static_cast<int32_t>(rsi90 + rax86 - 0x41404390) + *reinterpret_cast<uint32_t*>(&rdx91);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi92) + 4) = 0;
            __asm__("rol ecx, 0x4");
            *reinterpret_cast<uint32_t*>(&rcx93) = (*reinterpret_cast<uint32_t*>(&rdi89) ^ *reinterpret_cast<uint32_t*>(&rdx91) ^ *reinterpret_cast<uint32_t*>(&rsi92)) + static_cast<int32_t>(r8_46 + rcx88 + 0x289b7ec6) + *reinterpret_cast<uint32_t*>(&rsi92);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx93) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi94) = *reinterpret_cast<int32_t*>(&r8_22);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi94) + 4) = 0;
            __asm__("rol eax, 0xb");
            *reinterpret_cast<uint32_t*>(&rax95) = (*reinterpret_cast<uint32_t*>(&rdx91) ^ *reinterpret_cast<uint32_t*>(&rsi92) ^ *reinterpret_cast<uint32_t*>(&rcx93)) + static_cast<int32_t>(r14_18 + rdi89 - 0x155ed806) + *reinterpret_cast<uint32_t*>(&rcx93);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx96) = *reinterpret_cast<int32_t*>(&r10_35);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx96) + 4) = 0;
            __asm__("rol edi, 0x10");
            *reinterpret_cast<uint32_t*>(&rdi97) = (*reinterpret_cast<uint32_t*>(&rsi92) ^ *reinterpret_cast<uint32_t*>(&rcx93) ^ *reinterpret_cast<uint32_t*>(&rax95)) + static_cast<int32_t>(rdi94 + rdx91 - 0x2b10cf7b) + *reinterpret_cast<uint32_t*>(&rax95);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi97) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx98) = *reinterpret_cast<int32_t*>(&r12_26);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx98) + 4) = 0;
            __asm__("ror esi, 0x9");
            *reinterpret_cast<uint32_t*>(&rsi99) = (*reinterpret_cast<uint32_t*>(&rcx93) ^ *reinterpret_cast<uint32_t*>(&rax95) ^ *reinterpret_cast<uint32_t*>(&rdi97)) + static_cast<int32_t>(rdx96 + rsi92 + 0x4881d05) + *reinterpret_cast<uint32_t*>(&rdi97);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi99) + 4) = 0;
            __asm__("rol edx, 0x4");
            *reinterpret_cast<uint32_t*>(&rdx100) = (*reinterpret_cast<uint32_t*>(&rax95) ^ *reinterpret_cast<uint32_t*>(&rdi97) ^ *reinterpret_cast<uint32_t*>(&rsi99)) + static_cast<int32_t>(rdx98 + rcx93 - 0x262b2fc7) + *reinterpret_cast<uint32_t*>(&rsi99);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx100) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r10_101) = *reinterpret_cast<int32_t*>(&r10_23);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_101) + 4) = 0;
            __asm__("rol ecx, 0xb");
            *reinterpret_cast<uint32_t*>(&rcx102) = (*reinterpret_cast<uint32_t*>(&rdi97) ^ *reinterpret_cast<uint32_t*>(&rsi99) ^ *reinterpret_cast<uint32_t*>(&rdx100)) + static_cast<int32_t>(rbp40 + rax95 - 0x1924661b) + *reinterpret_cast<uint32_t*>(&rdx100);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx102) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rax103) = *reinterpret_cast<int32_t*>(&rcx29);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
            __asm__("rol edi, 0x10");
            *reinterpret_cast<uint32_t*>(&rdi104) = (*reinterpret_cast<uint32_t*>(&rsi99) ^ *reinterpret_cast<uint32_t*>(&rdx100) ^ *reinterpret_cast<uint32_t*>(&rcx102)) + static_cast<int32_t>(r9_33 + rdi97 + 0x1fa27cf8) + *reinterpret_cast<uint32_t*>(&rcx102);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi104) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_105) = *reinterpret_cast<int32_t*>(&rsi21);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_105) + 4) = 0;
            __asm__("ror eax, 0x9");
            *reinterpret_cast<uint32_t*>(&rax106) = (*reinterpret_cast<uint32_t*>(&rdx100) ^ *reinterpret_cast<uint32_t*>(&rcx102) ^ *reinterpret_cast<uint32_t*>(&rdi104)) + static_cast<int32_t>(rax103 + rsi99 - 0x3b53a99b) + *reinterpret_cast<uint32_t*>(&rdi104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
            __asm__("rol edx, 0x6");
            *reinterpret_cast<uint32_t*>(&rdx107) = ((~*reinterpret_cast<uint32_t*>(&rcx102) | *reinterpret_cast<uint32_t*>(&rax106)) ^ *reinterpret_cast<uint32_t*>(&rdi104)) + static_cast<int32_t>(r14_18 + rdx100 - 0xbd6ddbc) + *reinterpret_cast<uint32_t*>(&rax106);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx107) + 4) = 0;
            __asm__("rol ecx, 0xa");
            *reinterpret_cast<uint32_t*>(&rcx108) = ((~*reinterpret_cast<uint32_t*>(&rdi104) | *reinterpret_cast<uint32_t*>(&rdx107)) ^ *reinterpret_cast<uint32_t*>(&rax106)) + static_cast<int32_t>(r13_25 + rcx102 + 0x432aff97) + *reinterpret_cast<uint32_t*>(&rdx107);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx108) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r10_109) = *reinterpret_cast<int32_t*>(&r10_35);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_109) + 4) = 0;
            __asm__("rol esi, 0xf");
            *reinterpret_cast<uint32_t*>(&rsi110) = ((~*reinterpret_cast<uint32_t*>(&rax106) | *reinterpret_cast<uint32_t*>(&rcx108)) ^ *reinterpret_cast<uint32_t*>(&rdx107)) + static_cast<int32_t>(r12_42 + rdi104 - 0x546bdc59) + *reinterpret_cast<uint32_t*>(&rcx108);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi110) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbp111) = *reinterpret_cast<int32_t*>(&r8_22);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp111) + 4) = 0;
            __asm__("ror eax, 0xb");
            *reinterpret_cast<uint32_t*>(&rax112) = ((~*reinterpret_cast<uint32_t*>(&rdx107) | *reinterpret_cast<uint32_t*>(&rsi110)) ^ *reinterpret_cast<uint32_t*>(&rcx108)) + static_cast<int32_t>(r10_101 + rax106 - 0x36c5fc7) + *reinterpret_cast<uint32_t*>(&rsi110);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax112) + 4) = 0;
            __asm__("rol edx, 0x6");
            *reinterpret_cast<uint32_t*>(&rdx113) = ((~*reinterpret_cast<uint32_t*>(&rcx108) | *reinterpret_cast<uint32_t*>(&rax112)) ^ *reinterpret_cast<uint32_t*>(&rsi110)) + static_cast<int32_t>(rbp40 + rdx107 + 0x655b59c3) + *reinterpret_cast<uint32_t*>(&rax112);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx113) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi114) = *reinterpret_cast<int32_t*>(&rdi44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi114) + 4) = 0;
            __asm__("rol ecx, 0xa");
            *reinterpret_cast<uint32_t*>(&rcx115) = ((~*reinterpret_cast<uint32_t*>(&rsi110) | *reinterpret_cast<uint32_t*>(&rdx113)) ^ *reinterpret_cast<uint32_t*>(&rax112)) + static_cast<int32_t>(rbp111 + rcx108 - 0x70f3336e) + *reinterpret_cast<uint32_t*>(&rdx113);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx115) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r14_116) = *reinterpret_cast<int32_t*>(&rbp38);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_116) + 4) = 0;
            __asm__("rol esi, 0xf");
            *reinterpret_cast<uint32_t*>(&rsi117) = ((~*reinterpret_cast<uint32_t*>(&rax112) | *reinterpret_cast<uint32_t*>(&rcx115)) ^ *reinterpret_cast<uint32_t*>(&rdx113)) + static_cast<int32_t>(rdi114 + rsi110 - 0x100b83) + *reinterpret_cast<uint32_t*>(&rcx115);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi117) + 4) = 0;
            __asm__("ror eax, 0xb");
            *reinterpret_cast<int32_t*>(&r14_118) = *reinterpret_cast<int32_t*>(&r8_27);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_118) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rax119) = ((~*reinterpret_cast<uint32_t*>(&rdx113) | *reinterpret_cast<uint32_t*>(&rsi117)) ^ *reinterpret_cast<uint32_t*>(&rcx115)) + static_cast<int32_t>(r14_105 + rax112 - 0x7a7ba22f) + *reinterpret_cast<uint32_t*>(&rsi117);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax119) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_120) = *reinterpret_cast<int32_t*>(&r9_31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_120) + 4) = 0;
            __asm__("rol edx, 0x6");
            *reinterpret_cast<uint32_t*>(&rdx121) = ((~*reinterpret_cast<uint32_t*>(&rcx115) | *reinterpret_cast<uint32_t*>(&rax119)) ^ *reinterpret_cast<uint32_t*>(&rsi117)) + static_cast<int32_t>(r14_116 + rdx113 + 0x6fa87e4f) + *reinterpret_cast<uint32_t*>(&rax119);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx121) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r10_122) = *reinterpret_cast<int32_t*>(&r12_26);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_122) + 4) = 0;
            __asm__("rol ecx, 0xa");
            *reinterpret_cast<uint32_t*>(&rcx123) = ((~*reinterpret_cast<uint32_t*>(&rsi117) | *reinterpret_cast<uint32_t*>(&rdx121)) ^ *reinterpret_cast<uint32_t*>(&rax119)) + static_cast<int32_t>(r9_33 + rcx115 - 0x1d31920) + *reinterpret_cast<uint32_t*>(&rdx121);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx123) + 4) = 0;
            __asm__("rol esi, 0xf");
            *reinterpret_cast<uint32_t*>(&rsi124) = ((~*reinterpret_cast<uint32_t*>(&rax119) | *reinterpret_cast<uint32_t*>(&rcx123)) ^ *reinterpret_cast<uint32_t*>(&rdx121)) + static_cast<int32_t>(r10_109 + rsi117 - 0x5cfebcec) + *reinterpret_cast<uint32_t*>(&rcx123);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi124) + 4) = 0;
            __asm__("ror edi, 0xb");
            *reinterpret_cast<uint32_t*>(&rdi125) = ((~*reinterpret_cast<uint32_t*>(&rdx121) | *reinterpret_cast<uint32_t*>(&rsi124)) ^ *reinterpret_cast<uint32_t*>(&rcx123)) + static_cast<int32_t>(r8_46 + rax119 + 0x4e0811a1) + *reinterpret_cast<uint32_t*>(&rsi124);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi125) + 4) = 0;
            __asm__("rol eax, 0x6");
            eax126 = ((~*reinterpret_cast<uint32_t*>(&rcx123) | *reinterpret_cast<uint32_t*>(&rdi125)) ^ *reinterpret_cast<uint32_t*>(&rsi124)) + static_cast<int32_t>(r9_120 + rdx121 - 0x8ac817e) + *reinterpret_cast<uint32_t*>(&rdi125);
            *reinterpret_cast<int32_t*>(&rcx127) = *reinterpret_cast<int32_t*>(&rcx29);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx127) + 4) = 0;
            __asm__("rol edx, 0xa");
            edx128 = ((~*reinterpret_cast<uint32_t*>(&rsi124) | eax126) ^ *reinterpret_cast<uint32_t*>(&rdi125)) + static_cast<int32_t>(r14_118 + rcx123 - 0x42c50dcb) + eax126;
            __asm__("rol ecx, 0xf");
            ecx129 = ((~*reinterpret_cast<uint32_t*>(&rdi125) | edx128) ^ eax126) + static_cast<int32_t>(rcx127 + rsi124 + 0x2ad7d2bb) + edx128;
            __asm__("ror esi, 0xb");
            v8 = v8 + eax126;
            *reinterpret_cast<uint32_t*>(&r15_17) = *reinterpret_cast<uint32_t*>(&r15_17) + ecx129;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_17) + 4) = 0;
            v11 = v11 + edx128;
            *reinterpret_cast<uint32_t*>(&r11_5) = *reinterpret_cast<uint32_t*>(&r11_5) + (((~eax126 | ecx129) ^ edx128) + static_cast<int32_t>(r10_122 + rdi125 - 0x14792c6f) + ecx129);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        } while (reinterpret_cast<uint64_t>(v16) > reinterpret_cast<uint64_t>(rbx4));
        r10d7 = *reinterpret_cast<uint32_t*>(&r15_17);
    }
    v10->f0 = v8;
    v10->f4 = *reinterpret_cast<uint32_t*>(&r11_5);
    v10->f8 = r10d7;
    v10->fc = v11;
    return;
}

struct s44 {
    signed char[16] pad16;
    uint32_t f10;
    uint32_t f14;
    uint32_t f18;
    void** f1c;
};

signed char fillbuf = 0x80;

void** md5_process_block(void** rdi, ...);

void fun_cba3(struct s44* rdi, uint64_t rsi) {
    void* rdx3;
    uint32_t ecx4;
    uint32_t tmp32_5;
    int64_t rdi6;
    void* rax7;
    int64_t rax8;
    int64_t rax9;
    uint64_t rax10;
    int64_t rax11;
    int64_t r9_12;
    int64_t r9_13;
    uint64_t r9_14;
    int64_t r9_15;
    void** r9_16;
    signed char* rcx17;
    signed char rdx18;
    int64_t* rdi19;
    void* rcx20;
    int64_t* rsi21;
    uint64_t rcx22;
    signed char edx23;
    uint32_t edx24;
    uint32_t edx25;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rdx3) = rdi->f18;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    ecx4 = rdi->f14;
    tmp32_5 = *reinterpret_cast<uint32_t*>(&rdx3) + rdi->f10;
    *reinterpret_cast<uint32_t*>(&rdi6) = tmp32_5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
    rdi->f10 = *reinterpret_cast<uint32_t*>(&rdi6);
    if (tmp32_5 < *reinterpret_cast<uint32_t*>(&rdx3)) {
        ++ecx4;
        rdi->f14 = ecx4;
    }
    rax7 = reinterpret_cast<void*>((rax8 - (rax9 + static_cast<uint64_t>(reinterpret_cast<uint1_t>(rax10 < rax11 + static_cast<uint64_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx3) < 56))))) & 0xffffffffffffffc0) + 0x78 - reinterpret_cast<uint64_t>(rdx3));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + ((r9_12 - (r9_13 + static_cast<uint64_t>(reinterpret_cast<uint1_t>(r9_14 < r9_15 + static_cast<uint64_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx3) < 56))))) & 0xfffffffffffffff0) + 30) * 4 + 28) = static_cast<int32_t>(rdi6 * 8);
    r9_16 = reinterpret_cast<void**>(&rdi->f1c);
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + ((rsi - (rsi + reinterpret_cast<uint1_t>(rsi < rsi + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx3) < 56))) & 0xfffffffffffffff0) + 31) * 4 + 28) = ecx4 << 3 | *reinterpret_cast<uint32_t*>(&rdi6) >> 29;
    rcx17 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r9_16) + reinterpret_cast<uint64_t>(rdx3));
    if (reinterpret_cast<uint64_t>(rax7) >= 8) {
        rdx18 = fillbuf;
        rdi19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rcx17 + 8) & 0xfffffffffffffff8);
        *rcx17 = rdx18;
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rcx17) + reinterpret_cast<uint64_t>(rax7) - 8) = *reinterpret_cast<int64_t*>(0x171a0 + reinterpret_cast<uint64_t>(rax7) - 8);
        rcx20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx17) - reinterpret_cast<uint64_t>(rdi19));
        rsi21 = reinterpret_cast<int64_t*>(0x171a0 - reinterpret_cast<uint64_t>(rcx20));
        rcx22 = reinterpret_cast<uint64_t>(rcx20) + reinterpret_cast<uint64_t>(rax7) >> 3;
        while (rcx22) {
            --rcx22;
            *rdi19 = *rsi21;
            ++rdi19;
            ++rsi21;
        }
    } else {
        if (*reinterpret_cast<unsigned char*>(&rax7) & 4) {
            edx23 = fillbuf;
            *rcx17 = edx23;
            *reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(rcx17) + reinterpret_cast<uint64_t>(rax7) - 4) = *reinterpret_cast<int32_t*>(0x171a0 + reinterpret_cast<uint64_t>(rax7) - 4);
        } else {
            if (rax7 && (edx24 = *reinterpret_cast<unsigned char*>(&fillbuf), *rcx17 = *reinterpret_cast<signed char*>(&edx24), !!(*reinterpret_cast<unsigned char*>(&rax7) & 2))) {
                edx25 = *reinterpret_cast<uint16_t*>(0x171a0 + reinterpret_cast<uint64_t>(rax7) - 2);
                *reinterpret_cast<int16_t*>(reinterpret_cast<uint64_t>(rcx17) + reinterpret_cast<uint64_t>(rax7) - 2) = *reinterpret_cast<int16_t*>(&edx25);
            }
        }
    }
    md5_process_block(r9_16, r9_16);
}

struct s45 {
    signed char[24] pad24;
    uint32_t f18;
    void** f1c;
    signed char[63] pad92;
    void** f5c;
};

void** fun_ccc3(void** rdi, void** rsi, struct s45* rdx) {
    struct s45* r14_4;
    void** rbp5;
    void** rbx6;
    void** rax7;
    void* r12_8;
    void** rcx9;
    void** rax10;
    void** r13_11;
    uint32_t r15d12;
    uint32_t esi13;
    void* rdx14;
    void* rdi15;
    void* rcx16;
    void* rdx17;
    uint32_t edx18;
    uint32_t edx19;
    uint32_t ecx20;
    void* rsi21;
    void* rdx22;
    uint32_t edx23;
    void* rdx24;
    void** r12_25;
    void** r13_26;
    void** r12_27;
    void** rdi28;
    uint64_t rax29;
    uint64_t r15_30;
    void* rbx31;
    void* rdx32;
    void*** rcx33;
    void* rdi34;
    void* rax35;
    void* rcx36;
    void* rdx37;
    uint32_t edx38;
    uint32_t edx39;
    uint32_t ecx40;
    void* rsi41;
    void** rbx42;

    __asm__("cli ");
    r14_4 = rdx;
    rbp5 = rsi;
    rbx6 = rdi;
    *reinterpret_cast<uint32_t*>(&rax7) = rdx->f18;
    *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7)) {
        *reinterpret_cast<uint32_t*>(&r12_8) = *reinterpret_cast<uint32_t*>(&rax7);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
        rcx9 = reinterpret_cast<void**>(&rdx->f1c);
        rax10 = reinterpret_cast<void**>(0x80 - reinterpret_cast<uint64_t>(r12_8));
        if (reinterpret_cast<unsigned char>(rax10) > reinterpret_cast<unsigned char>(rsi)) {
            rax10 = rsi;
        }
        r13_11 = rax10;
        rax7 = fun_3b30(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(r12_8), rbx6, rax10);
        r15d12 = r14_4->f18 + *reinterpret_cast<int32_t*>(&r13_11);
        r14_4->f18 = r15d12;
        if (r15d12 > 64) {
            md5_process_block(rcx9);
            esi13 = r15d12 & 63;
            r14_4->f18 = esi13;
            rax7 = reinterpret_cast<void**>((reinterpret_cast<uint64_t>(r12_8) + reinterpret_cast<unsigned char>(r13_11) & 0xffffffffffffffc0) + reinterpret_cast<unsigned char>(rcx9));
            if (esi13 >= 8) {
                *reinterpret_cast<void***>(&r14_4->f1c) = *reinterpret_cast<void***>(rax7);
                *reinterpret_cast<uint32_t*>(&rdx14) = esi13;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
                *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx14) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(rdx14) - 8);
                rdi15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r14_4) + 36 & 0xfffffffffffffff8);
                rcx16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx9) - reinterpret_cast<uint64_t>(rdi15));
                rdx17 = rcx16;
                rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<uint64_t>(rcx16));
                edx18 = *reinterpret_cast<int32_t*>(&rdx17) + esi13 & 0xfffffff8;
                if (edx18 >= 8) {
                    edx19 = edx18 & 0xfffffff8;
                    ecx20 = 0;
                    do {
                        *reinterpret_cast<uint32_t*>(&rsi21) = ecx20;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
                        ecx20 = ecx20 + 8;
                        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi15) + reinterpret_cast<uint64_t>(rsi21)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(rsi21));
                    } while (ecx20 < edx19);
                }
            } else {
                if (*reinterpret_cast<unsigned char*>(&r15d12) & 4) {
                    *reinterpret_cast<void***>(&r14_4->f1c) = *reinterpret_cast<void***>(rax7);
                    *reinterpret_cast<uint32_t*>(&rdx22) = esi13;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(rdx22) - 4);
                    *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx22) - 4) = *reinterpret_cast<uint32_t*>(&rax7);
                } else {
                    if (esi13 && (edx23 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax7)), *reinterpret_cast<void***>(&r14_4->f1c) = *reinterpret_cast<void***>(&edx23), !!(r15d12 & 2))) {
                        *reinterpret_cast<uint32_t*>(&rdx24) = esi13;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(rdx24) - 2);
                        *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
                        *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx24) - 2) = *reinterpret_cast<int16_t*>(&rax7);
                    }
                }
            }
        }
        rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<unsigned char>(r13_11));
        rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) - reinterpret_cast<unsigned char>(r13_11));
    }
    if (reinterpret_cast<unsigned char>(rbp5) <= reinterpret_cast<unsigned char>(63)) {
        addr_ce03_16:
        if (rbp5) {
            r12_25 = reinterpret_cast<void**>(&r14_4->f1c);
            r13_26 = rbx6;
        } else {
            return rax7;
        }
    } else {
        if (!(*reinterpret_cast<unsigned char*>(&rbx6) & 3)) {
            r12_27 = rbp5;
            rdi28 = rbx6;
            *reinterpret_cast<uint32_t*>(&rbp5) = *reinterpret_cast<uint32_t*>(&rbp5) & 63;
            *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
            rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) + (reinterpret_cast<unsigned char>(r12_27) & 0xffffffffffffffc0));
            rax7 = md5_process_block(rdi28, rdi28);
            goto addr_ce03_16;
        } else {
            r12_25 = reinterpret_cast<void**>(&r14_4->f1c);
            if (rbp5 == 64) {
                r13_26 = rbx6;
            } else {
                rax29 = reinterpret_cast<uint64_t>(rbp5 + 0xffffffffffffffbf) >> 6;
                r15_30 = rax29;
                r13_26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) + (rax29 + 1 << 6));
                do {
                    __asm__("movdqu xmm0, [rbx]");
                    rbx6 = rbx6 + 64;
                    __asm__("movups [r12], xmm0");
                    __asm__("movdqu xmm1, [rbx-0x30]");
                    __asm__("movups [r12+0x10], xmm1");
                    __asm__("movdqu xmm2, [rbx-0x20]");
                    __asm__("movups [r12+0x20], xmm2");
                    __asm__("movdqu xmm3, [rbx-0x10]");
                    __asm__("movups [r12+0x30], xmm3");
                    md5_process_block(r12_25, r12_25);
                } while (rbx6 != r13_26);
                rax7 = reinterpret_cast<void**>(-r15_30 << 6);
                rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(rax7) + 0xffffffffffffffc0);
            }
        }
    }
    *reinterpret_cast<uint32_t*>(&rbx31) = r14_4->f18;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx31) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdx32) = *reinterpret_cast<uint32_t*>(&rbp5);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
    rcx33 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_25) + reinterpret_cast<uint64_t>(rbx31));
    if (*reinterpret_cast<uint32_t*>(&rbp5) >= 8) {
        rdi34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx33 + 8) & 0xfffffffffffffff8);
        *rcx33 = *reinterpret_cast<void***>(r13_26);
        *reinterpret_cast<uint32_t*>(&rax35) = *reinterpret_cast<uint32_t*>(&rbp5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax35) + 4) = 0;
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rcx33) + reinterpret_cast<uint64_t>(rax35) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r13_26) + reinterpret_cast<uint64_t>(rax35) - 8);
        rcx36 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx33) - reinterpret_cast<uint64_t>(rdi34));
        rdx37 = rcx36;
        rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_26) - reinterpret_cast<uint64_t>(rcx36));
        edx38 = *reinterpret_cast<int32_t*>(&rdx37) + *reinterpret_cast<uint32_t*>(&rbp5) & 0xfffffff8;
        if (edx38 >= 8) {
            edx39 = edx38 & 0xfffffff8;
            ecx40 = 0;
            do {
                *reinterpret_cast<uint32_t*>(&rsi41) = ecx40;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
                ecx40 = ecx40 + 8;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi34) + reinterpret_cast<uint64_t>(rsi41)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(rsi41));
            } while (ecx40 < edx39);
        }
    } else {
        if (*reinterpret_cast<unsigned char*>(&rbp5) & 4) {
            *rcx33 = *reinterpret_cast<void***>(r13_26);
            *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(r13_26) + reinterpret_cast<uint64_t>(rdx32) - 4);
            *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
            *reinterpret_cast<uint32_t*>(reinterpret_cast<uint64_t>(rcx33) + reinterpret_cast<uint64_t>(rdx32) - 4) = *reinterpret_cast<uint32_t*>(&rax7);
        } else {
            if (*reinterpret_cast<uint32_t*>(&rdx32) && (*reinterpret_cast<uint32_t*>(&rax7) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_26)), *reinterpret_cast<int32_t*>(&rax7 + 4) = 0, *rcx33 = rax7, !!(*reinterpret_cast<unsigned char*>(&rdx32) & 2))) {
                *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r13_26) + reinterpret_cast<uint64_t>(rdx32) - 2);
                *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
                *reinterpret_cast<int16_t*>(reinterpret_cast<uint64_t>(rcx33) + reinterpret_cast<uint64_t>(rdx32) - 2) = *reinterpret_cast<int16_t*>(&rax7);
            }
        }
    }
    rbx42 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx31) + reinterpret_cast<unsigned char>(rbp5));
    if (reinterpret_cast<unsigned char>(rbx42) > reinterpret_cast<unsigned char>(63)) {
        rbx42 = rbx42 - 64;
        md5_process_block(r12_25, r12_25);
        rax7 = fun_3b30(r12_25, &r14_4->f5c, rbx42);
    }
    r14_4->f18 = *reinterpret_cast<uint32_t*>(&rbx42);
    return rax7;
}

void fun_cfc3(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** r12_5;
    void* rdx6;

    __asm__("cli ");
    rax4 = g28;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0xb8);
    md5_process_bytes(rdi, rsi, r12_5);
    md5_finish_ctx(r12_5, rdx, r12_5);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_3950();
    } else {
        return;
    }
}

void fun_d053() {
    __asm__("cli ");
}

void fun_d063(void** rdi, void** rsi, void** rdx) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_3b40(rdi, rsi, rdx);
        goto 0x3a60;
    }
}

int32_t fun_3be0(void** rdi);

int64_t fun_3a00(int64_t rdi, ...);

int32_t rpl_fflush(void** rdi);

int64_t fun_38f0(void** rdi);

int64_t fun_d093(void** rdi, void** rsi, void** rdx) {
    uint32_t eax4;
    int32_t eax5;
    uint32_t eax6;
    int64_t rdi7;
    int64_t rax8;
    int32_t eax9;
    void** rax10;
    void** r12d11;
    int64_t rax12;

    __asm__("cli ");
    eax4 = fun_3b40(rdi, rsi, rdx);
    if (reinterpret_cast<int32_t>(eax4) >= reinterpret_cast<int32_t>(0)) {
        eax5 = fun_3be0(rdi);
        if (!(eax5 && (eax6 = fun_3b40(rdi, rsi, rdx), *reinterpret_cast<uint32_t*>(&rdi7) = eax6, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_3a00(rdi7), rax8 == -1) || (eax9 = rpl_fflush(rdi), eax9 == 0))) {
            rax10 = fun_37d0();
            r12d11 = *reinterpret_cast<void***>(rax10);
            rax12 = fun_38f0(rdi);
            if (r12d11) {
                *reinterpret_cast<void***>(rax10) = r12d11;
                *reinterpret_cast<int32_t*>(&rax12) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
            }
            return rax12;
        }
    }
    goto fun_38f0;
}

void rpl_fseeko(void** rdi);

void fun_d123(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_3be0(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_d4e3(struct s18* rdi, uint64_t rsi, struct s18* rdx, uint64_t rcx) {
    uint64_t* rsp5;
    unsigned char al6;
    unsigned char cl7;
    int64_t rcx8;
    int64_t rax9;
    int64_t rax10;
    uint32_t ecx11;
    unsigned char al12;
    uint32_t eax13;
    uint64_t rax14;
    uint64_t rax15;
    struct s18* rdx16;
    uint64_t rcx17;

    __asm__("cli ");
    rsp5 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16);
    if (reinterpret_cast<int64_t>(rsi) < reinterpret_cast<int64_t>(0)) {
        al6 = reinterpret_cast<uint1_t>(rdi->f0 == 0);
    } else {
        al6 = reinterpret_cast<uint1_t>(rsi == 0);
    }
    cl7 = reinterpret_cast<uint1_t>(rcx == 0);
    if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
        cl7 = reinterpret_cast<uint1_t>(rdx->f0 == 0);
    }
    if (al6) {
        addr_d5b0_7:
        *reinterpret_cast<uint32_t*>(&rcx8) = cl7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rax9) = static_cast<int32_t>(rcx8 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    } else {
        *reinterpret_cast<uint32_t*>(&rax10) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        if (cl7) 
            goto addr_d590_9;
        ecx11 = rdx->f0;
        if (rdi->f0 != 46) 
            goto addr_d53f_11;
    }
    if (*reinterpret_cast<signed char*>(&ecx11) != 46) 
        goto addr_d631_13;
    al12 = reinterpret_cast<uint1_t>(rsi == 1);
    if (reinterpret_cast<int64_t>(rsi) < reinterpret_cast<int64_t>(0)) {
        al12 = reinterpret_cast<uint1_t>(rdi->f1 == 0);
    }
    cl7 = reinterpret_cast<uint1_t>(rcx == 1);
    if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
        cl7 = reinterpret_cast<uint1_t>(rdx->f1 == 0);
    }
    if (al12) 
        goto addr_d5b0_7;
    *reinterpret_cast<uint32_t*>(&rax10) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
    if (cl7) 
        goto addr_d590_9;
    if (rdi->f1 == 46) {
        if (reinterpret_cast<int64_t>(rsi) < reinterpret_cast<int64_t>(0)) {
            if (!rdi->f2) {
                addr_d64b_23:
                if (rdx->f1 != 46) 
                    goto addr_d631_13;
            } else {
                goto addr_d610_25;
            }
        } else {
            if (rsi != 2) 
                goto addr_d610_25; else 
                goto addr_d64b_23;
        }
    } else {
        addr_d610_25:
        if (rdx->f1 != 46) 
            goto addr_d544_27; else 
            goto addr_d61c_28;
    }
    eax13 = 1;
    addr_d61e_30:
    if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
        if (rdx->f2) {
            addr_d629_32:
            if (!eax13) {
                addr_d544_27:
                rax14 = file_prefixlen(rdi, rsp5 + 1);
                rax15 = file_prefixlen(rdx, rsp5 - 1 + 1);
                rdx16 = rdx;
                rcx17 = rax15;
                if (rsi != rax14 || rcx != rax15) {
                    rax10 = verrevcmp(rdi, rax14, rdx16, rcx17);
                    if (*reinterpret_cast<uint32_t*>(&rax10)) {
                        addr_d590_9:
                        return rax10;
                    } else {
                        rcx17 = rcx;
                        rdx16 = rdx;
                    }
                }
            } else {
                addr_d631_13:
                *reinterpret_cast<uint32_t*>(&rax10) = 0xffffffff;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
                goto addr_d590_9;
            }
        } else {
            addr_d668_36:
            *reinterpret_cast<uint32_t*>(&rax10) = eax13 ^ 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            goto addr_d590_9;
        }
        rax10 = verrevcmp(rdi, rsi, rdx16, rcx17);
        goto addr_d590_9;
    } else {
        if (rcx == 2) 
            goto addr_d668_36; else 
            goto addr_d629_32;
    }
    addr_d61c_28:
    eax13 = 0;
    goto addr_d61e_30;
    addr_d53f_11:
    if (*reinterpret_cast<signed char*>(&ecx11) == 46) 
        goto addr_d590_9; else 
        goto addr_d544_27;
}

void fun_d683() {
    __asm__("cli ");
    goto filenvercmp;
}

int64_t fun_d6a3(void** rdi, void** rsi, void** rdx) {
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<void***>(rdi + 72)))) {
        eax4 = fun_3b40(rdi, rsi, rdx);
        *reinterpret_cast<uint32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_3a00(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

int32_t setlocale_null_r();

int64_t fun_d723() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_3950();
    } else {
        return rax3;
    }
}

uint64_t fun_d7a3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_d7c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s46 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_dc23(struct s46* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s47 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_dc33(struct s47* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s48 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_dc43(struct s48* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s51 {
    signed char[8] pad8;
    struct s51* f8;
};

struct s50 {
    int64_t f0;
    struct s51* f8;
};

struct s49 {
    struct s50* f0;
    struct s50* f8;
};

uint64_t fun_dc53(struct s49* rdi) {
    struct s50* rcx2;
    struct s50* rsi3;
    uint64_t r8_4;
    struct s51* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s54 {
    signed char[8] pad8;
    struct s54* f8;
};

struct s53 {
    int64_t f0;
    struct s54* f8;
};

struct s52 {
    struct s53* f0;
    struct s53* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_dcb3(struct s52* rdi) {
    struct s53* rcx2;
    struct s53* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s54* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s57 {
    signed char[8] pad8;
    struct s57* f8;
};

struct s56 {
    int64_t f0;
    struct s57* f8;
};

struct s55 {
    struct s56* f0;
    struct s56* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_dd23(struct s55* rdi, void** rsi) {
    void** v3;
    void** v4;
    void** r13_5;
    void** v6;
    void** r12_7;
    uint64_t r12_8;
    void** v9;
    void** rbp10;
    void** rbp11;
    void** v12;
    void** rbx13;
    struct s56* rcx14;
    struct s56* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s57* rax19;
    uint64_t rdx20;
    void** r9_21;
    int64_t v22;
    void** r9_23;
    int64_t v24;
    void** r9_25;
    int64_t v26;

    v3 = reinterpret_cast<void**>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_3db0(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_3db0(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x929c]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_ddda_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_de59_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_3db0(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_3db0;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x931b]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_de59_14; else 
            goto addr_ddda_13;
    }
}

struct s58 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s59 {
    int64_t f0;
    struct s59* f8;
};

int64_t fun_de83(struct s58* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s58* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s59* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x3eaa;
    rbx7 = reinterpret_cast<struct s59*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_dee8_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_dedb_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_dedb_7;
    }
    addr_deeb_10:
    return r12_3;
    addr_dee8_5:
    r12_3 = rbx7->f0;
    goto addr_deeb_10;
    addr_dedb_7:
    return 0;
}

struct s60 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_df03(struct s60* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x3eaf;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_df3f_7;
    return *rax2;
    addr_df3f_7:
    goto 0x3eaf;
}

struct s62 {
    int64_t f0;
    struct s62* f8;
};

struct s61 {
    int64_t* f0;
    struct s62* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_df53(struct s61* rdi, int64_t rsi) {
    struct s61* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s62* rax7;
    struct s62* rdx8;
    struct s62* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x3eb5;
    rax7 = reinterpret_cast<struct s62*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_df9e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_df9e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_dfbc_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_dfbc_10:
    return r8_10;
}

struct s64 {
    int64_t f0;
    struct s64* f8;
};

struct s63 {
    struct s64* f0;
    struct s64* f8;
};

void fun_dfe3(struct s63* rdi, int64_t rsi, uint64_t rdx) {
    struct s64* r9_4;
    uint64_t rax5;
    struct s64* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_e022_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_e022_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s66 {
    int64_t f0;
    struct s66* f8;
};

struct s65 {
    struct s66* f0;
    struct s66* f8;
};

int64_t fun_e033(struct s65* rdi, int64_t rsi, int64_t rdx) {
    struct s66* r14_4;
    int64_t r12_5;
    struct s65* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s66* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_e05f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_e0a1_10;
            }
            addr_e05f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_e069_11:
    return r12_5;
    addr_e0a1_10:
    goto addr_e069_11;
}

uint64_t fun_e0b3(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s67 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_e0f3(struct s67* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_3ab0(void** rdi, void** rsi);

void** fun_e123(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    void** rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    void** rax12;
    void** rax13;
    void** rdi14;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0xd7a0);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<void**>(0xd7c0);
    }
    rax9 = fun_3760(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0x17260);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((*reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax12 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&rsi)), *reinterpret_cast<void***>(r12_10 + 16) = rax12, rax12 == 0) || (*reinterpret_cast<uint32_t*>(&rsi) = 16, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax13 = fun_3ab0(rax12, 16), *reinterpret_cast<void***>(r12_10) = rax13, rax13 == 0))) {
            rdi14 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_3750(rdi14, rsi);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<void***>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s70 {
    int64_t f0;
    struct s70* f8;
};

struct s69 {
    int64_t f0;
    struct s70* f8;
};

struct s68 {
    struct s69* f0;
    struct s69* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s70* f48;
};

void fun_e223(struct s68* rdi) {
    struct s68* rbp2;
    struct s69* r12_3;
    struct s70* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s70* rax7;
    struct s70* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s71 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_e2d3(struct s71* rdi, void** rsi) {
    struct s71* r12_3;
    void** r13_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rdi9;
    void** rbx10;
    void** rbx11;
    void** rdi12;
    void** rdi13;

    __asm__("cli ");
    r12_3 = rdi;
    r13_4 = rdi->f0;
    rax5 = rdi->f8;
    rbp6 = r13_4;
    if (!rdi->f40 || !rdi->f20) {
        addr_e343_2:
        if (reinterpret_cast<unsigned char>(rax5) > reinterpret_cast<unsigned char>(rbp6)) {
            do {
                rbx7 = *reinterpret_cast<void***>(rbp6 + 8);
                if (rbx7) {
                    do {
                        rdi8 = rbx7;
                        rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
                        fun_3750(rdi8, rsi);
                    } while (rbx7);
                }
                rbp6 = rbp6 + 16;
            } while (reinterpret_cast<unsigned char>(r12_3->f8) > reinterpret_cast<unsigned char>(rbp6));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) < reinterpret_cast<unsigned char>(rax5)) {
            while (1) {
                rdi9 = *reinterpret_cast<void***>(r13_4);
                if (!rdi9) {
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                } else {
                    rbx10 = r13_4;
                    while (r12_3->f40(rdi9), rbx10 = *reinterpret_cast<void***>(rbx10 + 8), !!rbx10) {
                        rdi9 = *reinterpret_cast<void***>(rbx10);
                    }
                    rax5 = r12_3->f8;
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                }
            }
            rbp6 = r12_3->f0;
            goto addr_e343_2;
        }
    }
    rbx11 = r12_3->f48;
    if (rbx11) {
        do {
            rdi12 = rbx11;
            rbx11 = *reinterpret_cast<void***>(rbx11 + 8);
            fun_3750(rdi12, rsi);
        } while (rbx11);
    }
    rdi13 = r12_3->f0;
    fun_3750(rdi13, rsi);
    goto fun_3750;
}

int64_t fun_e3c3(void** rdi, uint64_t rsi) {
    void** r12_3;
    void** rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    void** r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi + 40);
    rax4 = g28;
    esi5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3 + 16));
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_e500_2;
    if (*reinterpret_cast<void***>(rdi + 16) == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_3ab0(rax6, 16);
        if (!rax8) {
            addr_e500_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8) - 8 + 8);
            v10 = *reinterpret_cast<void***>(rdi + 72);
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = *reinterpret_cast<void***>(rdi);
                fun_3750(rdi12, rdi);
                *reinterpret_cast<void***>(rdi) = rax8;
                *reinterpret_cast<void***>(rdi + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                *reinterpret_cast<void***>(rdi + 16) = rax6;
                *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rdi + 72) = v10;
            } else {
                *reinterpret_cast<void***>(rdi + 72) = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x3eba;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x3eba;
                fun_3750(rax8, r13_9);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        fun_3950();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s72 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_e553(void** rdi, void** rsi, void*** rdx) {
    void** rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s72* v17;
    int32_t r8d18;
    void** rax19;
    void* rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x3ebf;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_e66e_5; else 
                goto addr_e5df_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_e5df_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_e66e_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x8bc1]");
            if (!cf14) 
                goto addr_e6c5_12;
            __asm__("comiss xmm4, [rip+0x8b71]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x8b30]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_e6c5_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x3ebf;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_3760(16, rsi, rdx6);
                if (!rax19) {
                    addr_e6c5_12:
                    r8d18 = -1;
                } else {
                    goto addr_e622_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_e622_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_e59e_28:
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_3950();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_e622_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_e59e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_e773() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_3950();
    } else {
        return rax3;
    }
}

void** fun_e7d3(void** rdi, void** rsi) {
    void** rbx3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_e860_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_e916_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x89de]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x8948]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_3750(rdi15, rsi, rdi15, rsi);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_e916_5; else 
                goto addr_e860_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (rax16) {
        fun_3950();
    } else {
        return r12_7;
    }
}

void fun_e963() {
    __asm__("cli ");
    goto hash_remove;
}

int64_t fun_e973() {
    __asm__("cli ");
    return 0;
}

void** fun_e983(void** rdi, void** rsi, void** rdx) {
    void** rbp4;
    void** rbx5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rbp4 = rdi;
    rbx5 = rsi;
    rax6 = xmalloc(32);
    if (!rbx5) {
        rbx5 = reinterpret_cast<void**>(1);
    }
    rax7 = xnmalloc(rbx5, 8, rdx);
    *reinterpret_cast<void***>(rax6 + 8) = rbx5;
    *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rax6) = rax7;
    *reinterpret_cast<void***>(rax6 + 16) = reinterpret_cast<void**>(0);
    if (!rbp4) {
        rbp4 = reinterpret_cast<void**>(0xe970);
    }
    *reinterpret_cast<void***>(rax6 + 24) = rbp4;
    return rax6;
}

void fun_e9f3(void*** rdi, void** rsi) {
    void** rdi3;

    __asm__("cli ");
    rdi3 = *rdi;
    fun_3750(rdi3, rsi);
    goto fun_3750;
}

struct s73 {
    void** f0;
    signed char[7] pad8;
    void* f8;
    void*** f10;
    int64_t f18;
};

int64_t fun_ea13(struct s73* rdi, void** rsi) {
    void*** rbx3;
    void** rdi4;
    void*** rdx5;
    void** rax6;
    void*** rbx7;
    void** r15_8;
    int64_t r14_9;
    void** r13_10;
    void*** rbp11;
    void*** r12_12;
    void** rdi13;
    int32_t eax14;
    void* rax15;

    __asm__("cli ");
    rbx3 = rdi->f10;
    rdi4 = rdi->f0;
    rdx5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rdi->f8) - 1);
    if (reinterpret_cast<uint64_t>(rdx5) <= reinterpret_cast<uint64_t>(rbx3)) {
        *reinterpret_cast<int32_t*>(&rdx5) = 8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
        rax6 = x2nrealloc();
        rbx3 = rdi->f10;
        rdi->f0 = rax6;
        rdi4 = rax6;
    }
    rbx7 = rbx3 + 1;
    rdi->f10 = rbx7;
    *reinterpret_cast<void***>(rdi4 + reinterpret_cast<uint64_t>(rbx7) * 8) = rsi;
    r15_8 = rdi->f0;
    r14_9 = rdi->f18;
    r13_10 = *reinterpret_cast<void***>(r15_8 + reinterpret_cast<uint64_t>(rbx7) * 8);
    if (!reinterpret_cast<int1_t>(rbx7 == 1)) {
        do {
            rbp11 = rbx7;
            rbx7 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7) >> 1);
            r12_12 = reinterpret_cast<void***>(r15_8 + reinterpret_cast<uint64_t>(rbx7) * 8);
            rdi13 = *r12_12;
            eax14 = reinterpret_cast<int32_t>(r14_9(rdi13, r13_10, rdx5));
            rax15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp11) * 8);
            rdx5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r15_8) + reinterpret_cast<uint64_t>(rax15));
            if (!(reinterpret_cast<uint1_t>(eax14 < 0) | reinterpret_cast<uint1_t>(eax14 == 0))) 
                break;
            *rdx5 = *r12_12;
        } while (rbx7 != 1);
        goto addr_eab0_6;
    } else {
        goto addr_eab0_6;
    }
    addr_ea95_8:
    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r15_8) + reinterpret_cast<uint64_t>(rax15)) = r13_10;
    return 0;
    addr_eab0_6:
    *reinterpret_cast<int32_t*>(&rax15) = 8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_ea95_8;
}

struct s75 {
    int64_t f0;
    int64_t f8;
};

struct s74 {
    struct s75* f0;
    signed char[8] pad16;
    uint64_t f10;
    int64_t f18;
};

int64_t fun_eae3(struct s74* rdi) {
    uint64_t rax2;
    int64_t v3;
    struct s75* rdx4;
    uint64_t rcx5;
    int64_t rax6;
    struct s75* rbx7;
    uint64_t v8;
    uint64_t rcx9;
    int64_t rbp10;
    uint64_t v11;
    int64_t r13_12;
    int64_t* rax13;
    uint64_t r14_14;
    uint64_t r12_15;
    int64_t* r15_16;
    uint64_t r9_17;
    int64_t* r10_18;
    int64_t rsi19;
    int64_t rdi20;
    int32_t eax21;
    int1_t sf22;
    int64_t rdi23;
    int32_t eax24;

    __asm__("cli ");
    rax2 = rdi->f10;
    v3 = 0;
    if (!rax2) {
        addr_ebd6_2:
        return v3;
    } else {
        rdx4 = rdi->f0;
        v3 = rdx4->f8;
        rcx5 = rax2 - 1;
        rax6 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx4) + rax2 * 8);
        rdi->f10 = rcx5;
        rdx4->f8 = rax6;
        rbx7 = rdi->f0;
        v8 = rcx5;
        rcx9 = rcx5 >> 1;
        rbp10 = rdi->f18;
        v11 = rcx9;
        r13_12 = rbx7->f8;
        if (!rcx9) {
            rax13 = &rbx7->f8;
        } else {
            *reinterpret_cast<int32_t*>(&r14_14) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_14) + 4) = 0;
            while (1) {
                r12_15 = r14_14 + r14_14;
                if (v8 <= r12_15) {
                    r15_16 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rbx7) + (r14_14 << 4));
                } else {
                    r9_17 = r12_15 + 1;
                    r10_18 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rbx7) + r9_17 * 8);
                    r15_16 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rbx7) + (r14_14 << 4));
                    rsi19 = *r10_18;
                    rdi20 = *r15_16;
                    eax21 = reinterpret_cast<int32_t>(rbp10(rdi20, rsi19));
                    sf22 = eax21 < 0;
                    if (sf22) {
                        r15_16 = r10_18;
                    }
                    if (sf22) {
                        r12_15 = r9_17;
                    }
                }
                rdi23 = *r15_16;
                eax24 = reinterpret_cast<int32_t>(rbp10(rdi23, r13_12));
                rax13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rbx7) + r14_14 * 8);
                if (reinterpret_cast<uint1_t>(eax24 < 0) | reinterpret_cast<uint1_t>(eax24 == 0)) 
                    break;
                *rax13 = *r15_16;
                if (r12_15 > v11) 
                    goto addr_ebd0_15;
                r14_14 = r12_15;
            }
        }
    }
    addr_ebd3_17:
    *rax13 = r13_12;
    goto addr_ebd6_2;
    addr_ebd0_15:
    rax13 = r15_16;
    goto addr_ebd3_17;
}

struct s76 {
    signed char[11] pad11;
    signed char fb;
};

signed char* fun_ebf3(int32_t edi, struct s76* rsi) {
    signed char* r8_3;
    int32_t eax4;
    signed char* rsi5;
    int64_t rdx6;
    int64_t rdx7;
    int64_t rcx8;
    int32_t ecx9;
    int64_t rdx10;
    uint64_t rdx11;
    int32_t ecx12;
    int32_t eax13;

    __asm__("cli ");
    rsi->fb = 0;
    r8_3 = &rsi->fb;
    eax4 = edi;
    if (edi < 0) {
        do {
            rsi5 = r8_3;
            --r8_3;
            rdx6 = eax4 * 0x66666667 >> 34;
            *reinterpret_cast<int32_t*>(&rdx7) = *reinterpret_cast<int32_t*>(&rdx6) - (eax4 >> 31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx8) = static_cast<int32_t>(rdx7 + rdx7 * 4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
            ecx9 = static_cast<int32_t>(48 + rcx8 * 2) - eax4;
            eax4 = *reinterpret_cast<int32_t*>(&rdx7);
            *r8_3 = *reinterpret_cast<signed char*>(&ecx9);
        } while (*reinterpret_cast<int32_t*>(&rdx7));
        *(r8_3 - 1) = 45;
        return rsi5 - 2;
    } else {
        do {
            *reinterpret_cast<int32_t*>(&rdx10) = eax4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
            --r8_3;
            rdx11 = reinterpret_cast<uint64_t>(rdx10 * 0xcccccccd) >> 35;
            ecx12 = static_cast<int32_t>(rdx11 + rdx11 * 4);
            eax13 = eax4 - (ecx12 + ecx12) + 48;
            *r8_3 = *reinterpret_cast<signed char*>(&eax13);
            eax4 = *reinterpret_cast<int32_t*>(&rdx11);
        } while (*reinterpret_cast<int32_t*>(&rdx11));
        return r8_3;
    }
}

struct s77 {
    signed char[10] pad10;
    signed char fa;
};

struct s78 {
    signed char[10] pad10;
    signed char fa;
};

signed char* fun_ec83() {
    struct s77* rsi1;
    signed char* r8_2;
    struct s78* rsi3;
    int64_t rax4;
    uint32_t edi5;
    uint64_t rax6;
    int32_t ecx7;
    uint32_t edx8;
    uint32_t edx9;

    __asm__("cli ");
    rsi1->fa = 0;
    r8_2 = &rsi3->fa;
    do {
        *reinterpret_cast<uint32_t*>(&rax4) = edi5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        --r8_2;
        rax6 = reinterpret_cast<uint64_t>(rax4 * 0xcccccccd) >> 35;
        ecx7 = static_cast<int32_t>(rax6 + rax6 * 4);
        edx8 = edi5 - (ecx7 + ecx7) + 48;
        *r8_2 = *reinterpret_cast<signed char*>(&edx8);
        edx9 = edi5;
        edi5 = *reinterpret_cast<uint32_t*>(&rax6);
    } while (edx9 > 9);
    return r8_2;
}

struct s79 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_ecd3(uint64_t rdi, struct s79* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_3b70(int64_t rdi, void** rsi);

int32_t fun_3860(int64_t rdi, void** rsi);

uint32_t fun_3e10(void* rdi, void** rsi);

int64_t fun_ed33(void** rdi, void** rsi, void** rdx) {
    void** r15_4;
    void** rbp5;
    uint32_t v6;
    void** rax7;
    void** v8;
    uint64_t rax9;
    void* rsp10;
    int32_t r12d11;
    void* rax12;
    int64_t rax13;
    void** rax14;
    void** rdx15;
    uint32_t ecx16;
    int64_t rax17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    uint32_t v21;
    uint32_t eax22;
    uint32_t eax23;
    void** rsi24;
    void** rax25;
    int64_t rdi26;
    int32_t v27;
    void** rbx28;
    int32_t eax29;
    int64_t rdi30;
    int32_t v31;
    int32_t eax32;
    uint32_t eax33;
    uint32_t eax34;

    __asm__("cli ");
    r15_4 = rdi;
    rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
    v6 = *reinterpret_cast<uint32_t*>(&rdx);
    rax7 = g28;
    v8 = rax7;
    rax9 = fun_3930();
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8);
    if (rax9 <= 1) {
        r12d11 = 0;
        if (reinterpret_cast<unsigned char>(r15_4) >= reinterpret_cast<unsigned char>(rbp5)) {
            addr_ee78_3:
            rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
            if (rax12) {
                fun_3950();
            } else {
                *reinterpret_cast<int32_t*>(&rax13) = r12d11;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
                return rax13;
            }
        } else {
            rax14 = fun_3e90(rdi, rsi, rdx);
            r12d11 = 0;
            rdx15 = *reinterpret_cast<void***>(rax14);
            ecx16 = v6 & 2;
            do {
                *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_4));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                ++r15_4;
                eax18 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx15) + reinterpret_cast<uint64_t>(rax17 * 2)));
                if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax18) + 1) & 64) {
                    addr_eee9_8:
                    if (r12d11 == 0x7fffffff) 
                        goto addr_ee78_3;
                } else {
                    if (ecx16) 
                        goto addr_ef18_10;
                    if (*reinterpret_cast<unsigned char*>(&eax18) & 2) 
                        continue; else 
                        goto addr_eee9_8;
                }
                ++r12d11;
            } while (rbp5 != r15_4);
        }
    } else {
        r12d11 = 0;
        if (reinterpret_cast<unsigned char>(r15_4) >= reinterpret_cast<unsigned char>(rbp5)) 
            goto addr_ee78_3;
        r13_19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) + 32);
        r14_20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) + 28);
        v21 = v6 & 2;
        do {
            eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_4));
            if (*reinterpret_cast<signed char*>(&eax22) > 95) {
                eax23 = eax22 - 97;
                if (*reinterpret_cast<unsigned char*>(&eax23) <= 29) {
                    addr_ee63_18:
                    ++r15_4;
                    ++r12d11;
                    continue;
                }
            } else {
                if (*reinterpret_cast<signed char*>(&eax22) > 64) 
                    goto addr_ee63_18;
                if (*reinterpret_cast<signed char*>(&eax22) > 35) 
                    goto addr_ee58_22; else 
                    goto addr_edac_23;
            }
            addr_edb4_24:
            do {
                rsi24 = r15_4;
                rax25 = rpl_mbrtowc(r14_20, rsi24);
                if (rax25 == 0xffffffffffffffff) 
                    break;
                if (rax25 == 0xfffffffffffffffe) 
                    goto addr_ef28_27;
                *reinterpret_cast<int32_t*>(&rdi26) = v27;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rbx28) = 1;
                *reinterpret_cast<int32_t*>(&rbx28 + 4) = 0;
                if (rax25) {
                    rbx28 = rax25;
                }
                eax29 = fun_3b70(rdi26, rsi24);
                if (eax29 >= 0) {
                    if (0x7fffffff - r12d11 < eax29) 
                        goto addr_ef40_32;
                    r12d11 = r12d11 + eax29;
                    continue;
                } else {
                    if (v21) 
                        goto addr_ef18_10;
                    *reinterpret_cast<int32_t*>(&rdi30) = v31;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi30) + 4) = 0;
                    eax32 = fun_3860(rdi30, rsi24);
                    if (eax32) 
                        continue;
                }
                if (r12d11 == 0x7fffffff) 
                    goto addr_ef40_32;
                ++r12d11;
                r15_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_4) + reinterpret_cast<unsigned char>(rbx28));
                eax33 = fun_3e10(r13_19, rsi24);
            } while (!eax33);
            continue;
            if (!(*reinterpret_cast<unsigned char*>(&v6) & 1)) 
                goto addr_ee63_18; else 
                goto addr_ef13_40;
            addr_ef28_27:
            if (*reinterpret_cast<unsigned char*>(&v6) & 1) 
                goto addr_ef18_10;
            ++r12d11;
            r15_4 = rbp5;
            continue;
            addr_ee58_22:
            eax34 = eax22 - 37;
            if (*reinterpret_cast<unsigned char*>(&eax34) > 26) 
                goto addr_edb4_24; else 
                goto addr_ee63_18;
            addr_edac_23:
            if (*reinterpret_cast<signed char*>(&eax22) > 31) 
                goto addr_ee63_18; else 
                goto addr_edb4_24;
        } while (reinterpret_cast<unsigned char>(r15_4) < reinterpret_cast<unsigned char>(rbp5));
        goto addr_ee74_43;
    }
    goto addr_ee78_3;
    addr_ef18_10:
    r12d11 = -1;
    goto addr_ee78_3;
    addr_ee74_43:
    goto addr_ee78_3;
    addr_ef40_32:
    r12d11 = 0x7fffffff;
    goto addr_ee78_3;
    addr_ef13_40:
    goto addr_ef18_10;
}

void fun_ef53(void** rdi, int32_t esi) {
    __asm__("cli ");
    fun_3940(rdi);
    goto mbsnwidth;
}

int32_t fun_3de0();

int32_t fun_3900(int64_t rdi, void** rsi, void** rdx);

uint64_t fun_3d10(void*** rdi, void** rsi, void** rdx);

uint64_t fun_f043(int32_t edi, void** rsi, void** rdx) {
    void** rsp4;
    void** rax5;
    void** rax6;
    void* rsp7;
    void** rax8;
    uint64_t rax9;
    uint64_t r12_10;
    void** rbp11;
    void** rsi12;
    int32_t eax13;
    int32_t eax14;
    uint64_t rax15;
    uint64_t rax16;
    uint64_t rax17;
    void** rax18;
    uint64_t rax19;
    uint64_t rax20;
    uint64_t rbx21;
    void** rbp22;
    int32_t eax23;
    int32_t eax24;
    uint64_t rax25;
    void* rax26;

    __asm__("cli ");
    rsp4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x90);
    rax5 = g28;
    if (edi == 2) {
        rax6 = fun_3790("OMP_NUM_THREADS", rsi, rdx);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8);
        if (!rax6) {
            rax8 = fun_3790("OMP_THREAD_LIMIT", rsi, rdx);
            rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp7) - 8 + 8);
            if (!rax8 || (rax9 = parse_omp_threads_part_0(rax8), rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8), r12_10 = rax9, !rax9)) {
                addr_f205_4:
                r12_10 = 0xffffffffffffffff;
                goto addr_f11f_5;
            } else {
                addr_f11f_5:
                rbp11 = rsp4;
                *reinterpret_cast<int32_t*>(&rsi12) = 0x80;
                *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
                eax13 = fun_3de0();
                if (eax13 || (rsi12 = rbp11, eax14 = fun_3900(0x80, rsi12, rbp11), rax15 = reinterpret_cast<uint64_t>(static_cast<int64_t>(eax14)), rax15 == 0)) {
                    rax16 = fun_3d10(84, rsi12, rbp11);
                    if (rax16 <= r12_10) {
                        r12_10 = rax16;
                    }
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rax16) < reinterpret_cast<int64_t>(0)) | reinterpret_cast<uint1_t>(rax16 == 0)) {
                        r12_10 = 1;
                    }
                    goto addr_f09b_11;
                } else {
                    if (r12_10 > rax15) {
                        r12_10 = rax15;
                    }
                    goto addr_f09b_11;
                }
            }
        } else {
            rax17 = parse_omp_threads_part_0(rax6);
            rax18 = fun_3790("OMP_THREAD_LIMIT", rsi, rdx);
            rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp7) - 8 + 8 - 8 + 8);
            if (!rax18) {
                r12_10 = 0xffffffffffffffff;
            } else {
                rax19 = parse_omp_threads_part_0(rax18);
                rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8);
                r12_10 = rax19;
                if (!rax19) {
                    r12_10 = 0xffffffffffffffff;
                }
            }
            if (!rax17) 
                goto addr_f11f_5;
            if (r12_10 > rax17) {
                r12_10 = rax17;
            }
            goto addr_f09b_11;
        }
    }
    if (edi == 1) 
        goto addr_f205_4;
    rax20 = fun_3d10(83, rsi, rdx);
    rsp4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp4 - 8) + 8);
    rbx21 = rax20;
    r12_10 = rax20;
    if (rax20 + 0xffffffffffffffff > 1) 
        goto addr_f08e_25;
    rbp22 = rsp4;
    eax23 = fun_3de0();
    if (eax23 || ((eax24 = fun_3900(0x80, rbp22, rbp22), rax25 = reinterpret_cast<uint64_t>(static_cast<int64_t>(eax24)), rax25 == 0) || rbx21 >= rax25)) {
        addr_f09b_11:
        rax26 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
        if (rax26) {
            fun_3950();
        } else {
            return r12_10;
        }
    } else {
        rbx21 = rax25;
    }
    addr_f08e_25:
    *reinterpret_cast<int32_t*>(&r12_10) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_10) + 4) = 0;
    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rbx21) < reinterpret_cast<int64_t>(0)) | reinterpret_cast<uint1_t>(rbx21 == 0))) {
        r12_10 = rbx21;
        goto addr_f09b_11;
    }
}

int32_t g1e;

struct s80 {
    signed char[4] pad4;
    void** f4;
};

void** g22;

int32_t fun_3d60(void** rdi, void** rsi);

void fun_f233() {
    void** rax1;
    void** rsi2;
    void** rdx3;
    int32_t* rsi4;
    void** rsi5;
    struct s80* rsi6;
    void** rdx7;
    int32_t eax8;
    int64_t v9;
    void* rax10;

    __asm__("cli ");
    rax1 = g28;
    fun_3d10(85, rsi2, rdx3);
    __asm__("pxor xmm0, xmm0");
    __asm__("cvtsi2sd xmm0, rax");
    g1e = *rsi4;
    rsi5 = reinterpret_cast<void**>(&rsi6->f4);
    fun_3d10(34, rsi5, rdx7);
    g22 = *reinterpret_cast<void***>(rsi5);
    __asm__("pxor xmm1, xmm1");
    __asm__("comisd xmm0, xmm1");
    if (static_cast<int1_t>(!1)) {
        eax8 = fun_3d60(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x98 - 8 + 8 - 8 + 8 + 16, rsi5 + 4);
        if (eax8) 
            goto addr_f2a6_12;
    } else {
        __asm__("mulsd xmm0, xmm2");
        goto addr_f2a6_12;
    }
    if (v9 < 0) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rdx");
        __asm__("addsd xmm0, xmm0");
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
    }
    __asm__("pxor xmm1, xmm1");
    __asm__("cvtsi2sd xmm1, rax");
    __asm__("mulsd xmm0, xmm1");
    addr_f2a6_12:
    rax10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rax10) {
        fun_3950();
    } else {
        return;
    }
}

struct s81 {
    signed char[4] pad4;
    void** f4;
};

void fun_f323() {
    void** rax1;
    void** rsi2;
    void** rdx3;
    int32_t* rsi4;
    void** rsi5;
    struct s81* rsi6;
    void** rdx7;
    void** rsi8;
    void** rdi9;
    int32_t eax10;
    void** rdx11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    void* rax15;

    __asm__("cli ");
    rax1 = g28;
    fun_3d10(86, rsi2, rdx3);
    __asm__("pxor xmm0, xmm0");
    __asm__("cvtsi2sd xmm0, rax");
    g1e = *rsi4;
    rsi5 = reinterpret_cast<void**>(&rsi6->f4);
    fun_3d10(34, rsi5, rdx7);
    g22 = *reinterpret_cast<void***>(rsi5);
    rsi8 = rsi5 + 4;
    __asm__("pxor xmm1, xmm1");
    __asm__("comisd xmm0, xmm1");
    if (static_cast<int1_t>(!1)) {
        rdi9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x98 - 8 + 8 - 8 + 8 + 16);
        eax10 = fun_3d60(rdi9, rsi8);
        if (eax10) {
            physmem_total(rdi9, rsi8, rdx11);
            __asm__("mulsd xmm0, [rip+0x7ceb]");
        } else {
            if (v12 < 0) {
                __asm__("pxor xmm0, xmm0");
                __asm__("cvtsi2sd xmm0, rdx");
                __asm__("addsd xmm0, xmm0");
                if (v13 >= 0) {
                    addr_f3b3_12:
                    __asm__("pxor xmm1, xmm1");
                    __asm__("cvtsi2sd xmm1, rax");
                } else {
                    addr_f433_13:
                    __asm__("pxor xmm1, xmm1");
                    __asm__("cvtsi2sd xmm1, rdx");
                    __asm__("addsd xmm1, xmm1");
                }
                __asm__("addsd xmm0, xmm1");
                __asm__("pxor xmm1, xmm1");
                __asm__("cvtsi2sd xmm1, rax");
                __asm__("mulsd xmm0, xmm1");
            } else {
                __asm__("pxor xmm0, xmm0");
                __asm__("cvtsi2sd xmm0, rax");
                if (v14 < 0) 
                    goto addr_f433_13; else 
                    goto addr_f3b3_12;
            }
        }
    } else {
        __asm__("mulsd xmm0, xmm2");
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        fun_3950();
    } else {
        return;
    }
}

struct s82 {
    int32_t f0;
    int32_t f4;
};

/* have_pipe2_really.0 */
int32_t have_pipe2_really_0 = 0;

int32_t fun_3d70();

int32_t fun_3a50(struct s82* rdi);

int32_t rpl_fcntl(int64_t rdi, int64_t rsi, ...);

int64_t fun_f463(struct s82* rdi, uint32_t esi) {
    int32_t eax3;
    int32_t r13_4;
    int32_t eax5;
    void** rax6;
    int64_t rax7;
    uint32_t r12d8;
    void** rax9;
    int32_t eax10;
    int64_t rdi11;
    int32_t eax12;
    int64_t rdi13;
    int32_t eax14;
    int64_t rdi15;
    int32_t eax16;
    int64_t rdi17;
    int32_t eax18;
    int64_t rdi19;
    int32_t eax20;
    int64_t rdi21;
    int32_t eax22;
    int64_t rdi23;
    int32_t eax24;
    int64_t rdi25;
    int32_t eax26;
    int64_t rax27;
    int64_t rax28;
    void** rax29;
    void** r12d30;

    __asm__("cli ");
    eax3 = have_pipe2_really_0;
    r13_4 = rdi->f0;
    if (eax3 >= 0) {
        eax5 = fun_3d70();
        if (eax5 >= 0 || (rax6 = fun_37d0(), *reinterpret_cast<void***>(rax6) != 38)) {
            have_pipe2_really_0 = 1;
            *reinterpret_cast<int32_t*>(&rax7) = eax5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
            return rax7;
        } else {
            have_pipe2_really_0 = -1;
        }
    }
    r12d8 = esi & 0xfff7f7ff;
    if (r12d8) {
        rax9 = fun_37d0();
        r12d8 = 0xffffffff;
        *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(22);
        goto addr_f4b8_7;
    }
    eax10 = fun_3a50(rdi);
    if (eax10 < 0) {
        r12d8 = 0xffffffff;
        goto addr_f4b8_7;
    }
    if (!(esi & 0x800)) 
        goto addr_f4ac_11;
    *reinterpret_cast<int32_t*>(&rdi11) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
    eax12 = rpl_fcntl(rdi11, 3);
    if (eax12 >= 0 && ((*reinterpret_cast<int32_t*>(&rdi13) = rdi->f4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0, eax14 = rpl_fcntl(rdi13, 4, rdi13, 4), eax14 != -1) && (*reinterpret_cast<int32_t*>(&rdi15) = rdi->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0, eax16 = rpl_fcntl(rdi15, 3), eax16 >= 0))) {
        *reinterpret_cast<int32_t*>(&rdi17) = rdi->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
        eax18 = rpl_fcntl(rdi17, 4, rdi17, 4);
        if (eax18 != -1) {
            addr_f4ac_11:
            if (esi & 0x80000) {
                *reinterpret_cast<int32_t*>(&rdi19) = rdi->f4;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
                eax20 = rpl_fcntl(rdi19, 1);
                if (eax20 >= 0 && ((*reinterpret_cast<int32_t*>(&rdi21) = rdi->f4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0, eax22 = rpl_fcntl(rdi21, 2, rdi21, 2), eax22 != -1) && ((*reinterpret_cast<int32_t*>(&rdi23) = rdi->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0, eax24 = rpl_fcntl(rdi23, 1), eax24 >= 0) && (*reinterpret_cast<int32_t*>(&rdi25) = rdi->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0, eax26 = rpl_fcntl(rdi25, 2, rdi25, 2), eax26 != -1)))) {
                    *reinterpret_cast<uint32_t*>(&rax27) = r12d8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
                    return rax27;
                }
            } else {
                addr_f4b8_7:
                *reinterpret_cast<uint32_t*>(&rax28) = r12d8;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
                return rax28;
            }
        }
    }
    rax29 = fun_37d0();
    r12d30 = *reinterpret_cast<void***>(rax29);
    fun_3a40();
    fun_3a40();
    rdi->f0 = r13_4;
    *reinterpret_cast<void***>(rax29) = r12d30;
    r12d8 = 0xffffffff;
    goto addr_f4b8_7;
}

int64_t fun_3b20(void** rdi, signed char** rsi, int64_t rdx);

int64_t fun_f643() {
    int64_t r12_1;
    void** rax2;
    void** rsi3;
    void** rdx4;
    void** rax5;
    int64_t rax6;
    signed char* v7;
    void* rax8;
    int64_t rax9;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_1) = 0x31069;
    rax2 = g28;
    rax5 = fun_3790("_POSIX2_VERSION", rsi3, rdx4);
    if (rax5 && (*reinterpret_cast<void***>(rax5) && (rax6 = fun_3b20(rax5, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16 - 8 + 8, 10), !*v7))) {
        if (rax6 < 0xffffffff80000000) {
            *reinterpret_cast<int32_t*>(&r12_1) = 0x80000000;
        } else {
            *reinterpret_cast<int32_t*>(&r12_1) = 0x7fffffff;
            if (rax6 <= 0x7fffffff) {
                r12_1 = rax6;
            }
        }
    }
    rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax8) {
        fun_3950();
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = *reinterpret_cast<int32_t*>(&r12_1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

void fun_3da0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s83 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s83* fun_39d0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_f6d3(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s83* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_3da0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_37c0();
    } else {
        rbx3 = rdi;
        rax4 = fun_39d0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_37f0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 2) == 45))) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void** fun_10e73(void** rdi) {
    void** rbp2;
    void** rax3;
    void** r12d4;
    void** rax5;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_37d0();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = reinterpret_cast<void**>(0x1d9c0);
    }
    rax5 = xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return rax5;
}

int64_t fun_10eb3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x1d9c0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_10ed3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x1d9c0);
    }
    *rdi = esi;
    return 0x1d9c0;
}

int64_t fun_10ef3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x1d9c0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s84 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_10f33(struct s84* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s84*>(0x1d9c0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s85 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s85* fun_10f53(struct s85* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s85*>(0x1d9c0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x3ece;
    if (!rdx) 
        goto 0x3ece;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x1d9c0;
}

struct s86 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_10f93(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s86* r8) {
    struct s86* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s86*>(0x1d9c0);
    }
    rax7 = fun_37d0();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x10fc6);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s87 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_11013(int64_t rdi, int64_t rsi, void*** rdx, struct s87* rcx) {
    struct s87* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s87*>(0x1d9c0);
    }
    rax6 = fun_37d0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x11041);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x1109c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_11103() {
    __asm__("cli ");
}

void** g1d198 = reinterpret_cast<void**>(0xc0);

int64_t slotvec0 = 0x100;

void fun_11113() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void** rbx4;
    void** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = r12_2 + 24;
        rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *reinterpret_cast<void***>(rbx4);
            rbx4 = rbx4 + 16;
            fun_3750(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0x1d8c0) {
        fun_3750(rdi8, rsi9);
        g1d198 = reinterpret_cast<void**>(0x1d8c0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x1d190) {
        fun_3750(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0x1d190);
    }
    nslots = 1;
    return;
}

void fun_111b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_111d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_111e3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_11203(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_11223(int32_t edi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s21* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3ed4;
    rcx5 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_3950();
    } else {
        return rax6;
    }
}

void** fun_112b3(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s21* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x3ed9;
    rcx6 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_3950();
    } else {
        return rax7;
    }
}

void** fun_11343(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s21* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x3ede;
    rcx4 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_3950();
    } else {
        return rax5;
    }
}

void** fun_113d3(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s21* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x3ee3;
    rcx5 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_3950();
    } else {
        return rax6;
    }
}

void** fun_11463(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s21* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xc550]");
    __asm__("movdqa xmm1, [rip+0xc558]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xc541]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_3950();
    } else {
        return rax10;
    }
}

void** fun_11503(int64_t rdi, uint32_t esi) {
    struct s21* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xc4b0]");
    __asm__("movdqa xmm1, [rip+0xc4b8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xc4a1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_3950();
    } else {
        return rax9;
    }
}

void** fun_115a3(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc410]");
    __asm__("movdqa xmm1, [rip+0xc418]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xc3f9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_3950();
    } else {
        return rax3;
    }
}

void** fun_11633(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc380]");
    __asm__("movdqa xmm1, [rip+0xc388]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xc376]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_3950();
    } else {
        return rax4;
    }
}

void** fun_116c3(int32_t edi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s21* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x3ee8;
    rcx5 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_3950();
    } else {
        return rax6;
    }
}

void** fun_11763(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s21* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc24a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xc242]");
    __asm__("movdqa xmm2, [rip+0xc24a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3eed;
    if (!rdx) 
        goto 0x3eed;
    rcx6 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_3950();
    } else {
        return rax7;
    }
}

void** fun_11803(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s21* rcx7;
    void** rax8;
    void* rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc1aa]");
    __asm__("movdqa xmm1, [rip+0xc1b2]");
    __asm__("movdqa xmm2, [rip+0xc1ba]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x3ef2;
    if (!rdx) 
        goto 0x3ef2;
    rcx7 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_3950();
    } else {
        return rax8;
    }
}

void** fun_118b3(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s21* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc0fa]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xc0f2]");
    __asm__("movdqa xmm2, [rip+0xc0fa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3ef7;
    if (!rsi) 
        goto 0x3ef7;
    rcx5 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_3950();
    } else {
        return rax6;
    }
}

void** fun_11953(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s21* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xc05a]");
    __asm__("movdqa xmm1, [rip+0xc062]");
    __asm__("movdqa xmm2, [rip+0xc06a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x3efc;
    if (!rsi) 
        goto 0x3efc;
    rcx6 = reinterpret_cast<struct s21*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_3950();
    } else {
        return rax7;
    }
}

void fun_119f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_11a03(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_11a23() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_11a43(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_11a63(void** rdi) {
    void** rax2;

    __asm__("cli ");
    if (!rdi) 
        goto 0x3f01;
    quote(rdi);
    rax2 = fun_37d0();
    if (*reinterpret_cast<void***>(rax2)) {
        fun_3920();
    } else {
        fun_3920();
    }
    fun_3c90();
    goto 0x3f01;
}

void** fopen_safer();

void fun_3c50(void** rdi, void** rsi);

void* fun_3dd0(void*** rdi);

void isaac_seed(void*** rdi);

void fun_3cb0(void** rdi, void** rsi, void** rdx);

void** fun_11ad3(void** rdi, void* rsi) {
    void** rax3;
    void** r12_4;
    void* rbp5;
    void** rax6;
    void*** r13_7;
    void*** rbx8;
    void*** rbp9;
    void** rax10;
    void** rax11;
    void* rax12;
    void** rax13;
    void** r15d14;
    void** rbp15;

    __asm__("cli ");
    if (!rsi) {
        rax3 = xmalloc(0x1038);
        *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(0);
        r12_4 = rax3;
        *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x11a60);
        *reinterpret_cast<void***>(r12_4 + 16) = reinterpret_cast<void**>(0);
    } else {
        rbp5 = rsi;
        if (!rdi) {
            rax6 = xmalloc(0x1038);
            r12_4 = rax6;
            *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x11a60);
            r13_7 = reinterpret_cast<void***>(r12_4 + 32);
            *reinterpret_cast<void***>(r12_4 + 16) = reinterpret_cast<void**>(0);
            rbx8 = r13_7;
            *reinterpret_cast<void***>(r12_4 + 24) = reinterpret_cast<void**>(0);
            if (reinterpret_cast<uint64_t>(rbp5) > 0x800) {
                rbp5 = reinterpret_cast<void*>(0x800);
            }
            rbp9 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbp5) + reinterpret_cast<uint64_t>(r13_7));
            if (reinterpret_cast<uint64_t>(r13_7) < reinterpret_cast<uint64_t>(rbp9)) 
                goto addr_11bb8_7; else 
                goto addr_11bae_8;
        } else {
            rax10 = fopen_safer();
            if (!rax10) {
                *reinterpret_cast<int32_t*>(&r12_4) = 0;
                *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
            } else {
                rax11 = xmalloc(0x1038, 0x1038);
                r12_4 = rax11;
                *reinterpret_cast<void***>(rax11) = rax10;
                *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0x11a60);
                *reinterpret_cast<void***>(r12_4 + 16) = rdi;
                if (reinterpret_cast<uint64_t>(rbp5) <= 0x1000) {
                }
                fun_3c50(rax10, r12_4 + 24);
            }
        }
    }
    addr_11b4d_14:
    return r12_4;
    do {
        addr_11bb8_7:
        rax12 = fun_3dd0(rbx8);
        if (reinterpret_cast<int64_t>(rax12) >= reinterpret_cast<int64_t>(0)) {
            rbx8 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx8) + reinterpret_cast<uint64_t>(rax12));
        } else {
            rax13 = fun_37d0();
            r15d14 = *reinterpret_cast<void***>(rax13);
            if (r15d14 != 4) 
                goto addr_11bde_17;
        }
    } while (reinterpret_cast<uint64_t>(rbp9) > reinterpret_cast<uint64_t>(rbx8));
    addr_11c18_19:
    isaac_seed(r13_7);
    goto addr_11b4d_14;
    addr_11bde_17:
    rbp15 = *reinterpret_cast<void***>(r12_4);
    fun_3cb0(r12_4, 0x1038, 0x1038);
    fun_3750(r12_4, 0x1038, r12_4, 0x1038);
    if (rbp15) {
        rpl_fclose(rbp15, 0x1038, 0x1038);
    }
    *reinterpret_cast<void***>(rax13) = r15d14;
    *reinterpret_cast<int32_t*>(&r12_4) = 0;
    *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
    goto addr_11b4d_14;
    addr_11bae_8:
    goto addr_11c18_19;
}

struct s88 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_11c73(struct s88* rdi, int64_t rsi) {
    __asm__("cli ");
    rdi->f8 = rsi;
    return;
}

struct s89 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_11c83(struct s89* rdi, int64_t rsi) {
    __asm__("cli ");
    rdi->f10 = rsi;
    return;
}

struct s90 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
    int64_t f10;
    void** f18;
    signed char[2079] pad2104;
    void** f838;
};

void** isaac_refill(int64_t rdi, void** rsi, void** rdx);

void** fun_11c93(struct s90* rdi, void** rsi, void** rdx) {
    void** r14_4;
    struct s90* rbp5;
    void** rbx6;
    int1_t zf7;
    void** v8;
    void** rcx9;
    int64_t r12_10;
    void** rax11;
    void** v12;
    void** r13_13;
    void** rdx14;
    void** rax15;
    void** rcx16;
    void** r12_17;
    void** rax18;
    void** edx19;
    int64_t rdi20;
    void** r13_21;
    void** r15_22;
    void* rbx23;
    void** rax24;

    __asm__("cli ");
    r14_4 = rsi;
    rbp5 = rdi;
    rbx6 = rdx;
    zf7 = rdi->f0 == 0;
    v8 = rdi->f0;
    if (zf7) {
        rcx9 = rdi->f18;
        r12_10 = reinterpret_cast<int64_t>(rdi) + 32;
        rax11 = reinterpret_cast<void**>(&rdi->f838);
        v12 = rax11;
        if (reinterpret_cast<unsigned char>(rdx) <= reinterpret_cast<unsigned char>(rcx9)) {
            r13_13 = rdx;
            v12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax11) - reinterpret_cast<unsigned char>(rcx9) + 0x800);
        } else {
            do {
                rdx14 = rcx9;
                fun_3b30(r14_4, reinterpret_cast<uint64_t>(0x800 - reinterpret_cast<unsigned char>(rcx9)) + reinterpret_cast<unsigned char>(v12), rdx14);
                r14_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<unsigned char>(rcx9));
                rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(rcx9));
                if (!(*reinterpret_cast<unsigned char*>(&r14_4) & 7)) 
                    goto addr_11d88_5;
                isaac_refill(r12_10, v12, rdx14);
                *reinterpret_cast<int32_t*>(&rcx9) = 0x800;
                *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            } while (reinterpret_cast<unsigned char>(rbx6) > reinterpret_cast<unsigned char>(0x800));
            goto addr_11d7f_7;
        }
    } else {
        rax15 = fun_37d0();
        rcx16 = v8;
        r12_17 = rax15;
        while (rax18 = fun_38c0(r14_4, 1, rbx6, rcx16), edx19 = *reinterpret_cast<void***>(r12_17), r14_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<unsigned char>(rax18)), rbx6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx6) - reinterpret_cast<unsigned char>(rax18)), !!rbx6) {
            rdi20 = rbp5->f10;
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5->f0)) & 32)) {
                edx19 = reinterpret_cast<void**>(0);
            }
            *reinterpret_cast<void***>(r12_17) = edx19;
            rbp5->f8(rdi20, 1);
            rcx16 = rbp5->f0;
        }
        goto addr_11d06_13;
    }
    addr_11dd0_14:
    rax18 = fun_3b30(r14_4, v12, r13_13);
    rbp5->f18 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx9) - reinterpret_cast<unsigned char>(r13_13));
    addr_11d06_13:
    return rax18;
    addr_11d88_5:
    r13_21 = rbx6;
    r15_22 = rbx6;
    rbx23 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_4) + reinterpret_cast<unsigned char>(rbx6));
    *reinterpret_cast<uint32_t*>(&r13_13) = *reinterpret_cast<uint32_t*>(&r13_21) & 0x7ff;
    *reinterpret_cast<int32_t*>(&r13_13 + 4) = 0;
    do {
        r14_4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx23) - reinterpret_cast<unsigned char>(r15_22));
        if (r15_22 == r13_13) 
            break;
        rax24 = isaac_refill(r12_10, r14_4, rdx14);
        r15_22 = r15_22 - 0x800;
    } while (r15_22);
    goto addr_11df8_17;
    isaac_refill(r12_10, v12, rdx14);
    *reinterpret_cast<int32_t*>(&rcx9) = 0x800;
    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
    goto addr_11dd0_14;
    addr_11df8_17:
    rbp5->f18 = reinterpret_cast<void**>(0);
    return rax24;
    addr_11d7f_7:
    r13_13 = rbx6;
    goto addr_11dd0_14;
}

int64_t fun_11e33(void** rdi) {
    void** r12_2;

    __asm__("cli ");
    r12_2 = *reinterpret_cast<void***>(rdi);
    fun_3cb0(rdi, 0x1038, 0xffffffffffffffff);
    fun_3750(rdi, 0x1038, rdi, 0x1038);
    if (!r12_2) {
        return 0;
    } else {
        goto rpl_fclose;
    }
}

struct s91 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    int64_t f18;
    signed char[992] pad1024;
    int64_t f400;
    int64_t f408;
    int64_t f410;
    int64_t f418;
    signed char[992] pad2048;
    uint64_t f800;
    int64_t f808;
    int64_t f810;
};

struct s92 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    signed char[1000] pad1024;
    uint64_t f400;
};

struct s93 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
};

void fun_11e83(struct s91* rdi, struct s92* rsi) {
    struct s91* rdx3;
    struct s92* r8_4;
    uint64_t rcx5;
    struct s91* r9_6;
    int64_t r11_7;
    struct s91* rax8;
    int64_t r11_9;
    struct s92* rdi10;
    uint64_t r10_11;
    uint64_t rsi12;
    int64_t rsi13;
    uint64_t rcx14;
    uint64_t rsi15;
    uint64_t rsi16;
    int64_t rsi17;
    uint64_t r10_18;
    uint64_t rbx19;
    uint64_t rcx20;
    uint64_t rsi21;
    int64_t rsi22;
    uint64_t rsi23;
    uint64_t rsi24;
    int64_t rsi25;
    uint64_t rbx26;
    uint64_t r11_27;
    uint64_t r10_28;
    uint64_t rcx29;
    int64_t rcx30;
    uint64_t rsi31;
    uint64_t rsi32;
    int64_t rsi33;
    uint64_t r11_34;
    int64_t r10_35;
    int64_t rsi36;
    int64_t rsi37;
    uint64_t rsi38;
    uint64_t rsi39;
    int64_t rsi40;
    struct s93* r8_41;
    struct s91* rdi42;
    uint64_t r9_43;
    uint64_t rsi44;
    int64_t rsi45;
    uint64_t rcx46;
    uint64_t rsi47;
    uint64_t rsi48;
    int64_t rsi49;
    uint64_t r9_50;
    uint64_t r11_51;
    uint64_t rcx52;
    uint64_t rsi53;
    int64_t rsi54;
    uint64_t rsi55;
    uint64_t rsi56;
    int64_t rsi57;
    uint64_t r11_58;
    uint64_t r10_59;
    uint64_t r9_60;
    uint64_t rcx61;
    int64_t rcx62;
    uint64_t rsi63;
    uint64_t rsi64;
    int64_t rsi65;
    uint64_t r10_66;
    int64_t r11_67;
    int64_t rsi68;
    int64_t rsi69;
    uint64_t rsi70;
    uint64_t rsi71;
    int64_t rsi72;

    __asm__("cli ");
    rdx3 = rdi;
    r8_4 = rsi;
    rcx5 = rdi->f800;
    r9_6 = reinterpret_cast<struct s91*>(&rdi->f400);
    r11_7 = rdi->f810 + 1;
    rax8 = rdx3;
    rdi->f810 = r11_7;
    r11_9 = r11_7 + rdi->f808;
    rdi10 = rsi;
    do {
        r10_11 = rax8->f0;
        rsi12 = r10_11;
        *reinterpret_cast<uint32_t*>(&rsi13) = *reinterpret_cast<uint32_t*>(&rsi12) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
        rcx14 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(~(rcx5 ^ rcx5 << 21)) + rax8->f400);
        rsi15 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi13) + rcx14 + r11_9;
        rax8->f0 = rsi15;
        rsi16 = rsi15 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi17) = *reinterpret_cast<uint32_t*>(&rsi16) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        r10_18 = r10_11 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi17);
        rdi10->f0 = r10_18;
        rbx19 = rax8->f8;
        rcx20 = (rcx14 ^ rcx14 >> 5) + rax8->f408;
        rsi21 = rbx19;
        *reinterpret_cast<uint32_t*>(&rsi22) = *reinterpret_cast<uint32_t*>(&rsi21) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi22) + 4) = 0;
        rsi23 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi22) + rcx20 + r10_18;
        rax8->f8 = rsi23;
        rsi24 = rsi23 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&rsi24) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi25) + 4) = 0;
        rbx26 = rbx19 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi25);
        rdi10->f8 = rbx26;
        r11_27 = rax8->f10;
        r10_28 = (rcx20 << 12 ^ rcx20) + rax8->f410;
        rcx29 = r11_27;
        *reinterpret_cast<uint32_t*>(&rcx30) = *reinterpret_cast<uint32_t*>(&rcx29) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx30) + 4) = 0;
        rsi31 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rcx30) + r10_28 + rbx26;
        rax8->f10 = rsi31;
        rsi32 = rsi31 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi33) = *reinterpret_cast<uint32_t*>(&rsi32) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi33) + 4) = 0;
        r11_34 = r11_27 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi33);
        rdi10->f10 = r11_34;
        r10_35 = rax8->f18;
        rcx5 = (r10_28 >> 33 ^ r10_28) + rax8->f418;
        rsi36 = r10_35;
        *reinterpret_cast<uint32_t*>(&rsi37) = *reinterpret_cast<uint32_t*>(&rsi36) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi37) + 4) = 0;
        rsi38 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi37) + rcx5 + r11_34;
        rax8 = reinterpret_cast<struct s91*>(&rax8->pad1024);
        rdi10 = reinterpret_cast<struct s92*>(reinterpret_cast<int64_t>(rdi10) + 32);
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax8) - 8) = rsi38;
        rsi39 = rsi38 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi40) = *reinterpret_cast<uint32_t*>(&rsi39) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi40) + 4) = 0;
        r11_9 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi40) + r10_35;
        *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdi10) - 8) = r11_9;
    } while (rax8 != r9_6);
    r8_41 = reinterpret_cast<struct s93*>(&r8_4->f400);
    rdi42 = reinterpret_cast<struct s91*>(&rdx3->f800);
    do {
        r9_43 = rax8->f0;
        rsi44 = r9_43;
        *reinterpret_cast<uint32_t*>(&rsi45) = *reinterpret_cast<uint32_t*>(&rsi44) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi45) + 4) = 0;
        rcx46 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(~(rcx5 ^ rcx5 << 21)) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x400));
        rsi47 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi45) + rcx46 + r11_9;
        rax8->f0 = rsi47;
        rsi48 = rsi47 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi49) = *reinterpret_cast<uint32_t*>(&rsi48) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi49) + 4) = 0;
        r9_50 = r9_43 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi49);
        r8_41->f0 = r9_50;
        r11_51 = rax8->f8;
        rcx52 = (rcx46 ^ rcx46 >> 5) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3f8);
        rsi53 = r11_51;
        *reinterpret_cast<uint32_t*>(&rsi54) = *reinterpret_cast<uint32_t*>(&rsi53) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi54) + 4) = 0;
        rsi55 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi54) + rcx52 + r9_50;
        rax8->f8 = rsi55;
        rsi56 = rsi55 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi57) = *reinterpret_cast<uint32_t*>(&rsi56) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi57) + 4) = 0;
        r11_58 = r11_51 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi57);
        r8_41->f8 = r11_58;
        r10_59 = rax8->f10;
        r9_60 = (rcx52 << 12 ^ rcx52) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3f0);
        rcx61 = r10_59;
        *reinterpret_cast<uint32_t*>(&rcx62) = *reinterpret_cast<uint32_t*>(&rcx61) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx62) + 4) = 0;
        rsi63 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rcx62) + r9_60 + r11_58;
        rax8->f10 = rsi63;
        rsi64 = rsi63 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi65) = *reinterpret_cast<uint32_t*>(&rsi64) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi65) + 4) = 0;
        r10_66 = r10_59 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi65);
        r8_41->f10 = r10_66;
        r11_67 = rax8->f18;
        rcx5 = (r9_60 >> 33 ^ r9_60) + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rax8) - 0x3e8);
        rsi68 = r11_67;
        *reinterpret_cast<uint32_t*>(&rsi69) = *reinterpret_cast<uint32_t*>(&rsi68) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi69) + 4) = 0;
        rsi70 = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi69) + rcx5 + r10_66;
        rax8 = reinterpret_cast<struct s91*>(&rax8->pad1024);
        r8_41 = reinterpret_cast<struct s93*>(reinterpret_cast<int64_t>(r8_41) + 32);
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax8) - 8) = rsi70;
        rsi71 = rsi70 >> 8;
        *reinterpret_cast<uint32_t*>(&rsi72) = *reinterpret_cast<uint32_t*>(&rsi71) & 0x7f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi72) + 4) = 0;
        r11_9 = r11_67 + *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rdx3) + rsi72);
        *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r8_41) - 8) = r11_9;
    } while (rax8 != rdi42);
    rdx3->f800 = rcx5;
    rdx3->f808 = r11_9;
    return;
}

struct s94 {
    uint64_t f0;
    uint64_t f8;
    uint64_t f10;
    uint64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    signed char[1984] pad2048;
    int64_t f800;
    int64_t f808;
    int64_t f810;
};

void fun_12113(struct s94* rdi) {
    struct s94* rdx2;
    struct s94* rax3;
    uint64_t rcx4;
    uint64_t r11_5;
    uint64_t rsi6;
    uint64_t r10_7;
    uint64_t r9_8;
    uint64_t r8_9;
    uint64_t r12_10;
    struct s94* rbp11;
    struct s94* rbx12;
    uint64_t rdi13;
    uint64_t rsi14;
    uint64_t rcx15;
    uint64_t r13_16;
    uint64_t r11_17;
    uint64_t r12_18;
    uint64_t rdi19;
    uint64_t rcx20;
    uint64_t r13_21;
    uint64_t r14_22;
    uint64_t rsi23;
    uint64_t r11_24;
    uint64_t r12_25;
    uint64_t rsi26;
    uint64_t rcx27;
    uint64_t rax28;
    uint64_t r11_29;
    uint64_t r12_30;
    uint64_t rdi31;
    uint64_t r8_32;
    uint64_t rcx33;
    uint64_t rax34;
    uint64_t rsi35;
    uint64_t r11_36;
    uint64_t r12_37;

    __asm__("cli ");
    rdx2 = rdi;
    rax3 = rdi;
    rcx4 = 0x98f5704f6c44c0ab;
    r11_5 = 0x48fe4a0fa5a09315;
    rsi6 = 0x82f053db8355e0ce;
    r10_7 = 0xb29b2e824a595524;
    r9_8 = 0x8c0ea5053d4712a0;
    r8_9 = 0xb9f8b322c73ac862;
    r12_10 = 0xae985bf2cbfc89ed;
    rbp11 = rdi;
    rbx12 = reinterpret_cast<struct s94*>(&rdi->f800);
    rdi13 = 0x647c4677a2884b7c;
    do {
        rsi14 = rsi6 + rax3->f20;
        rcx15 = rcx4 + rax3->f38;
        r13_16 = rax3->f0 - rsi14 + rdi13;
        r11_17 = r11_5 + rax3->f28 ^ rcx15 >> 9;
        r12_18 = r12_10 + rax3->f30 ^ r13_16 << 9;
        rdi19 = rax3->f8 - r11_17 + r8_9;
        rcx20 = rdi19 >> 23 ^ rcx15 + r13_16;
        r13_21 = rax3->f10 - r12_18 + r9_8;
        r14_22 = rax3->f18 - rcx20 + r10_7;
        rdi13 = r13_21 << 15 ^ r13_16 + rdi19;
        rsi23 = rsi14 - rdi13;
        rax3->f0 = rdi13;
        r8_9 = r14_22 >> 14 ^ rdi19 + r13_21;
        r11_24 = r11_17 - r8_9;
        rax3->f8 = r8_9;
        r9_8 = rsi23 << 20 ^ r13_21 + r14_22;
        r12_25 = r12_18 - r9_8;
        rax3->f10 = r9_8;
        r10_7 = r11_24 >> 17 ^ r14_22 + rsi23;
        r11_5 = r11_24 + r12_25;
        rcx4 = rcx20 - r10_7;
        rax3->f18 = r10_7;
        rax3 = reinterpret_cast<struct s94*>(&rax3->pad2048);
        rsi6 = r12_25 << 14 ^ rsi23 + r11_24;
        r12_10 = r12_25 + rcx4;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 24) = r11_5;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 32) = rsi6;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 16) = r12_10;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rax3) - 8) = rcx4;
    } while (rbx12 != rax3);
    do {
        rsi26 = rsi6 + rdx2->f20;
        rcx27 = rcx4 + rdx2->f38;
        rax28 = rdx2->f0 - rsi26 + rdi13;
        r11_29 = r11_5 + rdx2->f28 ^ rcx27 >> 9;
        r12_30 = r12_10 + rdx2->f30 ^ rax28 << 9;
        rdi31 = rdx2->f8 - r11_29 + r8_9;
        r8_32 = rdx2->f10 - r12_30 + r9_8;
        rcx33 = rdi31 >> 23 ^ rcx27 + rax28;
        rax34 = rdx2->f18 - rcx33 + r10_7;
        rdi13 = r8_32 << 15 ^ rax28 + rdi31;
        rsi35 = rsi26 - rdi13;
        rdx2->f0 = rdi13;
        r8_9 = rax34 >> 14 ^ rdi31 + r8_32;
        r11_36 = r11_29 - r8_9;
        rdx2->f8 = r8_9;
        r9_8 = rsi35 << 20 ^ r8_32 + rax34;
        r12_37 = r12_30 - r9_8;
        rdx2->f10 = r9_8;
        r10_7 = r11_36 >> 17 ^ rax34 + rsi35;
        r11_5 = r11_36 + r12_37;
        rcx4 = rcx33 - r10_7;
        rdx2->f18 = r10_7;
        rdx2 = reinterpret_cast<struct s94*>(&rdx2->pad2048);
        rsi6 = r12_37 << 14 ^ rsi35 + r11_36;
        r12_10 = r12_37 + rcx4;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 24) = r11_5;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 32) = rsi6;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 16) = r12_10;
        *reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rdx2) - 8) = rcx4;
    } while (rbx12 != rdx2);
    rbp11->f810 = 0;
    rbp11->f808 = 0;
    rbp11->f800 = 0;
    return;
}

void _obstack_begin(int64_t rdi);

void fun_12463(int64_t rdi) {
    int64_t rdi2;

    __asm__("cli ");
    rdi2 = rdi + 24;
    *reinterpret_cast<int64_t*>(rdi2 - 24) = 0;
    *reinterpret_cast<int64_t*>(rdi2 - 16) = 0;
    *reinterpret_cast<int64_t*>(rdi2 - 8) = 0;
    _obstack_begin(rdi2);
    _obstack_begin(rdi + 0x70);
    goto _obstack_begin;
}

void _obstack_free(int64_t rdi);

void fun_124e3(int64_t rdi) {
    __asm__("cli ");
    _obstack_free(rdi + 24);
    _obstack_free(rdi + 0x70);
    goto _obstack_free;
}

int32_t fun_3a30(int64_t rdi, struct s23* rsi);

int32_t fun_3830(int64_t rdi);

int32_t fun_12513(int64_t rdi, struct s23* rsi) {
    int64_t r13_3;
    int64_t r12_4;
    struct s23* rbp5;
    int32_t eax6;
    int32_t ebx7;
    signed char* rax8;
    signed char* rax9;
    signed char** rdx10;
    signed char** rcx11;
    signed char** rax12;
    void* rdx13;
    uint64_t* rcx14;
    signed char** rax15;
    signed char** rdx16;
    uint64_t* rax17;
    void* rdx18;
    uint64_t* rax19;
    uint64_t* rdx20;
    int32_t eax21;

    __asm__("cli ");
    r13_3 = reinterpret_cast<int64_t>(&rsi->pad32);
    r12_4 = rdi;
    rbp5 = rsi;
    while (eax6 = fun_3a30(r12_4, rsi), ebx7 = eax6, rax8 = rbp5->f30, ebx7 != -1) {
        if (rbp5->f38 != rax8) {
            rbp5->f30 = rax8 + 1;
            *rax8 = *reinterpret_cast<signed char*>(&ebx7);
            if (ebx7) 
                continue;
        } else {
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi) + 4) = 0;
            _obstack_newchunk(r13_3, 1);
            rax9 = rbp5->f30;
            rbp5->f30 = rax9 + 1;
            *rax9 = *reinterpret_cast<signed char*>(&ebx7);
            if (ebx7) 
                continue;
        }
        save_token(rbp5, rsi);
    }
    if (rax8 != rbp5->f28) {
        if (rax8 == rbp5->f38) {
            *reinterpret_cast<int32_t*>(&rsi) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi) + 4) = 0;
            _obstack_newchunk(&rbp5->pad32, 1);
            rax8 = rbp5->f30;
        }
        rbp5->f30 = rax8 + 1;
        *rax8 = 0;
        save_token(rbp5, rsi);
    }
    rdx10 = rbp5->f88;
    if (reinterpret_cast<uint64_t>(rbp5->f90) - reinterpret_cast<uint64_t>(rdx10) <= 7) {
        _obstack_newchunk(reinterpret_cast<int64_t>(rbp5) + 0x70, 8);
        rdx10 = rbp5->f88;
    }
    *rdx10 = reinterpret_cast<signed char*>(0);
    rcx11 = rbp5->f80;
    rax12 = rbp5->f88 + 1;
    if (rax12 == rcx11) {
        rbp5->fc0 = reinterpret_cast<unsigned char>(rbp5->fc0 | 2);
    }
    rdx13 = rbp5->fa0;
    rbp5->f8 = rcx11;
    rcx14 = rbp5->fd8;
    rax15 = reinterpret_cast<signed char**>(reinterpret_cast<uint64_t>(rax12) + reinterpret_cast<int64_t>(rdx13) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rdx13)));
    rdx16 = rbp5->f90;
    if (reinterpret_cast<uint64_t>(rax15) - rbp5->f78 <= reinterpret_cast<uint64_t>(rdx16) - rbp5->f78) {
        rdx16 = rax15;
    }
    rax17 = rbp5->fe0;
    rbp5->f88 = rdx16;
    rbp5->f80 = rdx16;
    if (rax17 == rcx14) {
        rbp5->f118 = reinterpret_cast<unsigned char>(rbp5->f118 | 2);
    }
    rdx18 = rbp5->ff8;
    rbp5->f10 = rcx14;
    rax19 = reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rax17) + reinterpret_cast<int64_t>(rdx18) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rdx18)));
    rdx20 = rbp5->fe8;
    if (reinterpret_cast<uint64_t>(rax19) - rbp5->fd0 <= reinterpret_cast<uint64_t>(rdx20) - rbp5->fd0) {
        rdx20 = rax19;
    }
    rbp5->fe0 = rdx20;
    rbp5->fd8 = rdx20;
    eax21 = fun_3830(r12_4);
    *reinterpret_cast<unsigned char*>(&eax21) = reinterpret_cast<uint1_t>(eax21 == 0);
    return eax21;
}

int64_t fun_12703(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_3c30(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_3940(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_3b30(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_3b30(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_127b3() {
    __asm__("cli ");
    goto fun_3c30;
}

int32_t fun_3bd0();

void fun_127c3() {
    __asm__("cli ");
    fun_3bd0();
}

int32_t fun_39f0();

void fun_127e3() {
    __asm__("cli ");
    fun_39f0();
}

struct s95 {
    unsigned char f0;
    unsigned char f1;
};

struct s96 {
    unsigned char f0;
    unsigned char f1;
};

struct s97 {
    unsigned char f0;
    unsigned char f1;
};

int64_t fun_12803(struct s95* rdi, struct s96* rsi, uint32_t edx, uint32_t ecx) {
    struct s96* rax5;
    uint32_t esi6;
    uint32_t r11d7;
    uint32_t edx8;
    uint32_t ecx9;
    uint32_t esi10;
    int64_t r8_11;
    int64_t r9_12;
    uint32_t ecx13;
    uint32_t ecx14;
    int64_t r10_15;
    uint32_t ecx16;
    uint32_t r9d17;
    uint64_t rsi18;
    uint32_t ecx19;
    uint64_t rdi20;
    uint32_t ecx21;
    uint32_t edx22;
    int64_t r9_23;
    int64_t rdx24;
    uint32_t esi25;
    uint32_t ecx26;
    int64_t rax27;
    int32_t eax28;
    uint32_t edx29;
    uint32_t esi30;
    uint32_t r8d31;
    uint32_t ecx32;
    int32_t r9d33;
    int32_t r9d34;
    uint32_t r9d35;
    int32_t r9d36;
    int64_t r8_37;
    uint32_t ecx38;
    uint32_t r9d39;
    uint64_t r10_40;
    uint32_t ecx41;
    uint64_t rsi42;
    uint32_t ecx43;
    int64_t rax44;
    int64_t rax45;
    uint32_t edx46;
    int64_t r9_47;
    uint32_t ecx48;
    uint32_t esi49;
    struct s95* rax50;
    struct s96* rdx51;
    int64_t r10_52;
    uint32_t r8d53;
    struct s97* rax54;
    uint32_t r9d55;
    uint32_t esi56;
    int64_t rax57;

    __asm__("cli ");
    rax5 = rsi;
    esi6 = rdi->f0;
    r11d7 = edx;
    edx8 = ecx;
    ecx9 = rax5->f0;
    if (*reinterpret_cast<unsigned char*>(&esi6) == 45) {
        addr_12900_2:
        esi10 = rdi->f1;
        rdi = reinterpret_cast<struct s95*>(&rdi->f1);
        if (*reinterpret_cast<unsigned char*>(&esi10) == 48) 
            goto addr_12900_2;
        *reinterpret_cast<uint32_t*>(&r8_11) = *reinterpret_cast<unsigned char*>(&esi10);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_11) + 4) = 0;
        if (edx8 == *reinterpret_cast<uint32_t*>(&r8_11)) 
            goto addr_12900_2;
        if (*reinterpret_cast<unsigned char*>(&ecx9) != 45) 
            goto addr_1291c_5;
    } else {
        if (*reinterpret_cast<unsigned char*>(&ecx9) != 45) {
            while (*reinterpret_cast<unsigned char*>(&esi6) == 48 || (*reinterpret_cast<uint32_t*>(&r9_12) = *reinterpret_cast<unsigned char*>(&esi6), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_12) + 4) = 0, edx8 == *reinterpret_cast<uint32_t*>(&r9_12))) {
                esi6 = rdi->f1;
                rdi = reinterpret_cast<struct s95*>(&rdi->f1);
            }
            if (*reinterpret_cast<unsigned char*>(&ecx9) != 48) 
                goto addr_1288d_10; else 
                goto addr_1287c_11;
        } else {
            addr_12828_13:
            ecx13 = rax5->f1;
            rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
            if (*reinterpret_cast<signed char*>(&ecx13) == 48) 
                goto addr_12828_13;
            if (edx8 == ecx13) 
                goto addr_12828_13;
            if (r11d7 == ecx13) 
                goto addr_12a10_16; else 
                goto addr_12842_17;
        }
    }
    addr_12970_18:
    ecx14 = rax5->f1;
    rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
    if (*reinterpret_cast<unsigned char*>(&ecx14) == 48) 
        goto addr_12970_18;
    *reinterpret_cast<uint32_t*>(&r10_15) = *reinterpret_cast<unsigned char*>(&ecx14);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_15) + 4) = 0;
    if (edx8 == *reinterpret_cast<uint32_t*>(&r10_15)) 
        goto addr_12970_18;
    addr_12990_21:
    while (*reinterpret_cast<unsigned char*>(&esi10) == *reinterpret_cast<unsigned char*>(&ecx14) && static_cast<uint32_t>(r10_15 - 48) <= 9) {
        do {
            *reinterpret_cast<uint32_t*>(&r8_11) = rdi->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_11) + 4) = 0;
            rdi = reinterpret_cast<struct s95*>(&rdi->f1);
            esi10 = *reinterpret_cast<uint32_t*>(&r8_11);
        } while (edx8 == *reinterpret_cast<uint32_t*>(&r8_11));
        do {
            *reinterpret_cast<uint32_t*>(&r10_15) = rax5->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_15) + 4) = 0;
            rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
            ecx14 = *reinterpret_cast<uint32_t*>(&r10_15);
            if (edx8 != *reinterpret_cast<uint32_t*>(&r10_15)) 
                goto addr_12990_21;
            *reinterpret_cast<uint32_t*>(&r10_15) = rax5->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_15) + 4) = 0;
            rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
            ecx14 = *reinterpret_cast<uint32_t*>(&r10_15);
        } while (edx8 == *reinterpret_cast<uint32_t*>(&r10_15));
    }
    if (r11d7 != *reinterpret_cast<uint32_t*>(&r8_11)) 
        goto addr_12b9b_29;
    if (static_cast<uint32_t>(r10_15 - 48) > 9) 
        goto addr_12c61_31;
    addr_12b9b_29:
    ecx16 = static_cast<uint32_t>(r8_11 - 48);
    if (r11d7 == *reinterpret_cast<uint32_t*>(&r10_15)) {
        r9d17 = r11d7 - *reinterpret_cast<uint32_t*>(&r8_11);
        if (ecx16 <= 9) {
            addr_12bb7_33:
            *reinterpret_cast<int32_t*>(&rsi18) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0;
        } else {
            goto addr_12c61_31;
        }
    } else {
        r9d17 = *reinterpret_cast<uint32_t*>(&r10_15) - *reinterpret_cast<uint32_t*>(&r8_11);
        if (ecx16 <= 9) 
            goto addr_12bb7_33;
        *reinterpret_cast<int32_t*>(&rsi18) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r10_15) - 48 <= 9) 
            goto addr_12be2_37; else 
            goto addr_12c4c_38;
    }
    addr_12bc0_39:
    ecx19 = rdi->f1;
    rdi = reinterpret_cast<struct s95*>(&rdi->f1);
    if (edx8 == ecx19) 
        goto addr_12bc0_39;
    ++rsi18;
    if (ecx19 - 48 <= 9) 
        goto addr_12bc0_39;
    if (*reinterpret_cast<uint32_t*>(&r10_15) - 48 > 9) {
        r9d17 = r9d17 - (r9d17 + reinterpret_cast<uint1_t>(r9d17 < r9d17 + reinterpret_cast<uint1_t>(!!rsi18)));
        goto addr_12962_43;
    } else {
        addr_12be2_37:
        *reinterpret_cast<int32_t*>(&rdi20) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
    }
    addr_12be8_44:
    ecx21 = rax5->f1;
    rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
    if (edx8 == ecx21) 
        goto addr_12be8_44;
    ++rdi20;
    if (ecx21 - 48 <= 9) 
        goto addr_12be8_44;
    if (rdi20 == rsi18) {
        if (!rdi20) {
            r9d17 = 0;
        }
        goto addr_12962_43;
    } else {
        r9d17 = (r9d17 - (r9d17 + reinterpret_cast<uint1_t>(r9d17 < r9d17 + reinterpret_cast<uint1_t>(rsi18 < rdi20))) & 2) - 1;
        goto addr_12962_43;
    }
    addr_12c61_31:
    edx22 = rdi->f0;
    if (*reinterpret_cast<unsigned char*>(&r11d7) != rax5->f0) {
        r9d17 = 0;
        if (*reinterpret_cast<unsigned char*>(&r11d7) != *reinterpret_cast<unsigned char*>(&edx22)) 
            goto addr_12962_43; else 
            goto addr_12c79_52;
    }
    if (*reinterpret_cast<unsigned char*>(&r11d7) != *reinterpret_cast<unsigned char*>(&edx22)) 
        goto addr_12d27_54;
    do {
        *reinterpret_cast<int32_t*>(&r9_23) = reinterpret_cast<signed char>(rax5->f1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_23) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx24) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rdi->f1)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx24) + 4) = 0;
        rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
        rdi = reinterpret_cast<struct s95*>(&rdi->f1);
        esi25 = static_cast<uint32_t>(r9_23 - 48);
        if (*reinterpret_cast<signed char*>(&r9_23) != *reinterpret_cast<signed char*>(&rdx24)) 
            break;
    } while (esi25 <= 9);
    goto addr_12c4c_38;
    ecx26 = static_cast<uint32_t>(rdx24 - 48);
    if (esi25 > 9) {
        r9d17 = 0;
        if (ecx26 > 9) {
            addr_12962_43:
            *reinterpret_cast<uint32_t*>(&rax27) = r9d17;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            return rax27;
        } else {
            while (eax28 = reinterpret_cast<signed char>(rdi->f0), *reinterpret_cast<signed char*>(&eax28) == 48) {
                addr_12c79_52:
                rdi = reinterpret_cast<struct s95*>(&rdi->f1);
            }
        }
    } else {
        r9d17 = *reinterpret_cast<int32_t*>(&r9_23) - *reinterpret_cast<uint32_t*>(&rdx24);
        if (ecx26 > 9) {
            while (edx29 = rax5->f0, *reinterpret_cast<signed char*>(&edx29) == 48) {
                addr_12d27_54:
                rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
            }
            goto addr_12cc9_65;
        } else {
            goto addr_12962_43;
        }
    }
    addr_12b76_67:
    r9d17 = -static_cast<uint32_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(eax28 - 48 < 10))));
    goto addr_12962_43;
    addr_12cc9_65:
    r9d17 = 0;
    *reinterpret_cast<unsigned char*>(&r9d17) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&edx29) - 48 <= 9);
    goto addr_12962_43;
    addr_12c4c_38:
    r9d17 = 0;
    goto addr_12962_43;
    addr_1291c_5:
    if (r11d7 == *reinterpret_cast<uint32_t*>(&r8_11)) {
        do {
            esi30 = rdi->f1;
            rdi = reinterpret_cast<struct s95*>(&rdi->f1);
        } while (*reinterpret_cast<unsigned char*>(&esi30) == 48);
        *reinterpret_cast<uint32_t*>(&r8_11) = *reinterpret_cast<unsigned char*>(&esi30);
    }
    if (*reinterpret_cast<uint32_t*>(&r8_11) - 48 <= 9) {
        r9d17 = 0xffffffff;
        goto addr_12962_43;
    }
    while (*reinterpret_cast<unsigned char*>(&ecx9) == 48 || (r8d31 = *reinterpret_cast<unsigned char*>(&ecx9), edx8 == r8d31)) {
        ecx9 = rax5->f1;
        rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
    }
    if (r11d7 != r8d31) 
        goto addr_12957_76;
    do {
        ecx32 = rax5->f1;
        rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
    } while (*reinterpret_cast<unsigned char*>(&ecx32) == 48);
    r8d31 = *reinterpret_cast<unsigned char*>(&ecx32);
    addr_12957_76:
    r9d17 = r9d33 - (r9d34 + reinterpret_cast<uint1_t>(r9d35 < r9d36 + reinterpret_cast<uint1_t>(r8d31 - 48 < 10)));
    goto addr_12962_43;
    addr_1288d_10:
    while (*reinterpret_cast<uint32_t*>(&r8_37) = *reinterpret_cast<unsigned char*>(&ecx9), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_37) + 4) = 0, edx8 == *reinterpret_cast<uint32_t*>(&r8_37)) {
        do {
            addr_12880_79:
            ecx9 = rax5->f1;
            rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
        } while (*reinterpret_cast<unsigned char*>(&ecx9) == 48);
    }
    addr_128a0_81:
    while (*reinterpret_cast<unsigned char*>(&esi6) == *reinterpret_cast<unsigned char*>(&ecx9) && static_cast<uint32_t>(r8_37 - 48) <= 9) {
        do {
            *reinterpret_cast<uint32_t*>(&r9_12) = rdi->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_12) + 4) = 0;
            rdi = reinterpret_cast<struct s95*>(&rdi->f1);
            esi6 = *reinterpret_cast<uint32_t*>(&r9_12);
        } while (edx8 == *reinterpret_cast<uint32_t*>(&r9_12));
        do {
            *reinterpret_cast<uint32_t*>(&r8_37) = rax5->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_37) + 4) = 0;
            rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
            ecx9 = *reinterpret_cast<uint32_t*>(&r8_37);
            if (edx8 != *reinterpret_cast<uint32_t*>(&r8_37)) 
                goto addr_128a0_81;
            *reinterpret_cast<uint32_t*>(&r8_37) = rax5->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_37) + 4) = 0;
            rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
            ecx9 = *reinterpret_cast<uint32_t*>(&r8_37);
        } while (edx8 == *reinterpret_cast<uint32_t*>(&r8_37));
    }
    if (r11d7 != *reinterpret_cast<uint32_t*>(&r9_12)) 
        goto addr_12a99_89;
    if (static_cast<uint32_t>(r8_37 - 48) > 9) 
        goto addr_12b44_91;
    addr_12a99_89:
    ecx38 = static_cast<uint32_t>(r9_12 - 48);
    if (r11d7 == *reinterpret_cast<uint32_t*>(&r8_37)) {
        r9d39 = *reinterpret_cast<uint32_t*>(&r9_12) - r11d7;
        if (ecx38 <= 9) {
            addr_12ab2_93:
            *reinterpret_cast<int32_t*>(&r10_40) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_40) + 4) = 0;
        } else {
            goto addr_12b44_91;
        }
    } else {
        r9d39 = *reinterpret_cast<uint32_t*>(&r9_12) - *reinterpret_cast<uint32_t*>(&r8_37);
        if (ecx38 <= 9) 
            goto addr_12ab2_93;
        *reinterpret_cast<int32_t*>(&r10_40) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_40) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r8_37) - 48 <= 9) 
            goto addr_12ade_97; else 
            goto addr_12cab_98;
    }
    addr_12ab8_99:
    ecx41 = rdi->f1;
    rdi = reinterpret_cast<struct s95*>(&rdi->f1);
    if (edx8 == ecx41) 
        goto addr_12ab8_99;
    ++r10_40;
    if (ecx41 - 48 <= 9) 
        goto addr_12ab8_99;
    if (*reinterpret_cast<uint32_t*>(&r8_37) - 48 > 9) {
        r9d17 = 0;
        *reinterpret_cast<unsigned char*>(&r9d17) = reinterpret_cast<uint1_t>(!!r10_40);
        goto addr_12962_43;
    }
    addr_12ade_97:
    *reinterpret_cast<int32_t*>(&rsi42) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi42) + 4) = 0;
    addr_12ae0_103:
    ecx43 = rax5->f1;
    rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
    if (edx8 == ecx43) 
        goto addr_12ae0_103;
    ++rsi42;
    if (ecx43 - 48 <= 9) 
        goto addr_12ae0_103;
    if (rsi42 == r10_40) {
        if (!rsi42) {
            r9d39 = 0;
        }
        *reinterpret_cast<uint32_t*>(&rax44) = r9d39;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
        return rax44;
    } else {
        *reinterpret_cast<uint32_t*>(&rax45) = r9d39 - (r9d39 + reinterpret_cast<uint1_t>(r9d39 < r9d39 + reinterpret_cast<uint1_t>(r10_40 < rsi42))) | 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax45) + 4) = 0;
        return rax45;
    }
    addr_12b44_91:
    edx46 = rax5->f0;
    if (*reinterpret_cast<unsigned char*>(&r11d7) == rdi->f0) {
        if (*reinterpret_cast<unsigned char*>(&r11d7) == *reinterpret_cast<unsigned char*>(&edx46)) {
            do {
                *reinterpret_cast<uint32_t*>(&r9_47) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rdi->f1)));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_47) + 4) = 0;
                ecx48 = rax5->f1;
                rdi = reinterpret_cast<struct s95*>(&rdi->f1);
                rax5 = reinterpret_cast<struct s96*>(&rax5->f1);
                edx29 = *reinterpret_cast<uint32_t*>(&r9_47);
                esi49 = static_cast<uint32_t>(r9_47 - 48);
                if (*reinterpret_cast<signed char*>(&r9_47) != *reinterpret_cast<signed char*>(&ecx48)) 
                    break;
            } while (esi49 <= 9);
            goto addr_12c4c_38;
        } else {
            edx29 = rdi->f1;
            rax50 = reinterpret_cast<struct s95*>(&rdi->f1);
            goto addr_12cc4_114;
        }
    } else {
        r9d17 = 0;
        if (*reinterpret_cast<unsigned char*>(&r11d7) != *reinterpret_cast<unsigned char*>(&edx46)) 
            goto addr_12962_43;
        ecx48 = rax5->f1;
        rdx51 = reinterpret_cast<struct s96*>(&rax5->f1);
        goto addr_12b6e_117;
    }
    *reinterpret_cast<uint32_t*>(&r10_52) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&ecx48)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_52) + 4) = 0;
    r8d53 = static_cast<uint32_t>(r10_52 - 48);
    if (esi49 > 9) {
        r9d17 = 0;
        rdx51 = rax5;
        if (r8d53 > 9) 
            goto addr_12962_43;
    } else {
        rax50 = rdi;
        r9d17 = *reinterpret_cast<uint32_t*>(&r9_47) - *reinterpret_cast<uint32_t*>(&r10_52);
        if (r8d53 <= 9) {
            goto addr_12962_43;
        }
    }
    addr_12b6e_117:
    while (*reinterpret_cast<signed char*>(&ecx48) == 48) {
        ecx48 = rdx51->f1;
        rdx51 = reinterpret_cast<struct s96*>(&rdx51->f1);
    }
    eax28 = *reinterpret_cast<signed char*>(&ecx48);
    goto addr_12b76_67;
    addr_12cc4_114:
    while (*reinterpret_cast<signed char*>(&edx29) == 48) {
        edx29 = rax50->f1;
        rax50 = reinterpret_cast<struct s95*>(&rax50->f1);
    }
    goto addr_12cc9_65;
    addr_12cab_98:
    goto addr_12c4c_38;
    addr_1287c_11:
    goto addr_12880_79;
    do {
        addr_12a10_16:
        ecx13 = rax5->f1;
        rax54 = reinterpret_cast<struct s97*>(&rax5->f1);
        if (*reinterpret_cast<signed char*>(&ecx13) != 48) 
            break;
        ecx13 = rax54->f1;
        rax5 = reinterpret_cast<struct s96*>(&rax54->f1);
    } while (*reinterpret_cast<signed char*>(&ecx13) == 48);
    goto addr_12a2e_127;
    addr_12842_17:
    if (ecx13 - 48 <= 9) {
        return 1;
    }
    while (*reinterpret_cast<unsigned char*>(&esi6) == 48 || (r9d55 = *reinterpret_cast<unsigned char*>(&esi6), edx8 == r9d55)) {
        esi6 = rdi->f1;
        rdi = reinterpret_cast<struct s95*>(&rdi->f1);
    }
    if (r11d7 != r9d55) 
        goto addr_12a58_132;
    do {
        esi56 = rdi->f1;
        rdi = reinterpret_cast<struct s95*>(&rdi->f1);
    } while (*reinterpret_cast<unsigned char*>(&esi56) == 48);
    r9d55 = *reinterpret_cast<unsigned char*>(&esi56);
    addr_12a58_132:
    *reinterpret_cast<uint32_t*>(&rax57) = reinterpret_cast<uint1_t>(r9d55 - 48 <= 9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax57) + 4) = 0;
    return rax57;
    addr_12a2e_127:
    goto addr_12842_17;
}

int32_t dup_safer(int64_t rdi);

int64_t fun_12db3(int64_t rdi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (*reinterpret_cast<uint32_t*>(&rdi) <= 2) {
        eax2 = dup_safer(rdi);
        rax3 = fun_37d0();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_3a40();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<uint32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s98 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    void** f20;
    signed char[7] pad40;
    void** f28;
    signed char[7] pad48;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    void** f40;
};

void fun_3af0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_12e13(void** rdi, void** rsi, void** rdx, void** rcx, struct s98* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    void** v9;
    void** v10;
    void** v11;
    void** v12;
    void** v13;
    int64_t v14;
    void** v15;
    void** v16;
    void** v17;
    void** v18;
    void** v19;
    void** rax20;
    int64_t v21;
    void** v22;
    void** v23;
    void** v24;
    void** v25;
    void** v26;
    void** rax27;
    int64_t v28;
    void** v29;
    void** v30;
    void** v31;
    void** v32;
    void** v33;
    void** r10_34;
    void** r9_35;
    void** r8_36;
    void** rcx37;
    int64_t r15_38;
    void** v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_3db0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_3db0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_3920();
    fun_3db0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_3af0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_3920();
    fun_3db0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_3af0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_3920();
        fun_3db0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x17a08 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x17a08;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_13283() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s99 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_132a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s99* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s99* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_3950();
    } else {
        return;
    }
}

void fun_13343(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_133e6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_133f0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_3950();
    } else {
        return;
    }
    addr_133e6_5:
    goto addr_133f0_7;
}

void fun_13423() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rsi1 = stdout;
    fun_3af0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_3920();
    fun_3c40(1, rax6, "bug-coreutils@gnu.org");
    rax7 = fun_3920();
    fun_3c40(1, rax7, "GNU coreutils", 1, rax7, "GNU coreutils");
    fun_3920();
    goto fun_3c40;
}

int64_t fun_3870();

void fun_134c3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3870();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_13503(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3760(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_13523(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3760(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_13543(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3760(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_3c10();

void fun_13563(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_3c10();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_13593() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_3c10();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_135c3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3870();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_13603() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_3870();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_13643(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_3870();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_13673(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_3870();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_136c3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_3870();
            if (rax5) 
                break;
            addr_1370d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_1370d_5;
        rax8 = fun_3870();
        if (rax8) 
            goto addr_136f6_9;
        if (rbx4) 
            goto addr_1370d_5;
        addr_136f6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_13753(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_3870();
            if (rax8) 
                break;
            addr_1379a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_1379a_5;
        rax11 = fun_3870();
        if (rax11) 
            goto addr_13782_9;
        if (!rbx6) 
            goto addr_13782_9;
        if (r12_4) 
            goto addr_1379a_5;
        addr_13782_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_137e3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_1388d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_138a0_10:
                *r12_8 = 0;
            }
            addr_13840_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_13866_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_138b4_14;
            if (rcx10 <= rsi9) 
                goto addr_1385d_16;
            if (rsi9 >= 0) 
                goto addr_138b4_14;
            addr_1385d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_138b4_14;
            addr_13866_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_3c10();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_138b4_14;
            if (!rbp13) 
                break;
            addr_138b4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_1388d_9;
        } else {
            if (!r13_6) 
                goto addr_138a0_10;
            goto addr_13840_11;
        }
    }
}

void fun_138e3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_3ab0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_13913(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_3ab0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_13943(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3ab0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_13963(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3ab0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_13983(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3760(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_3b30;
    }
}

void fun_139c3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3760(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_3b30;
    }
}

void fun_13a03(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_3760(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_3b30;
    }
}

void fun_13a43(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_3940(rdi);
    rax5 = fun_3760(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_3b30;
    }
}

void fun_13a83() {
    __asm__("cli ");
    fun_3920();
    fun_3c90();
    fun_37c0();
}

int32_t memcoll();

int64_t fun_13b93(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int32_t eax5;
    void** rax6;
    void** edi7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    eax5 = memcoll();
    rax6 = fun_37d0();
    edi7 = *reinterpret_cast<void***>(rax6);
    if (edi7) {
        collate_error(edi7, rdi, rsi, rdx, rcx);
        *reinterpret_cast<int32_t*>(&rax8) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

int32_t memcoll0();

int64_t fun_13bf3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int32_t eax5;
    void** rax6;
    void** edi7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    eax5 = memcoll0();
    rax6 = fun_37d0();
    edi7 = *reinterpret_cast<void***>(rax6);
    if (edi7) {
        collate_error(edi7, rdi, rsi - 1, rdx, rcx - 1);
        *reinterpret_cast<int32_t*>(&rax8) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

void fun_3b90();

int64_t dtotimespec();

int64_t rpl_nanosleep(void* rdi, void* rsi);

int64_t fun_13c53() {
    void** rax1;
    void** v2;
    void** rax3;
    void* rsp4;
    void** rbx5;
    void* rbp6;
    int64_t rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movd rbp, xmm0");
    rax1 = g28;
    v2 = rax1;
    rax3 = fun_37d0();
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 40 - 8 + 8);
    __asm__("movd xmm1, rbp");
    __asm__("comisd xmm1, [rip+0x347f]");
    rbx5 = rax3;
    if (1) {
        do {
            fun_3b90();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx5) == 4)) 
                goto addr_13c86_3;
            fun_3b90();
            rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
        } while (*reinterpret_cast<void***>(rbx5) == 4);
    } else {
        addr_13c86_3:
        __asm__("movd xmm0, rbp");
        rbp6 = rsp4;
        dtotimespec();
        goto addr_13ca8_5;
    }
    goto addr_13c86_3;
    do {
        addr_13ca8_5:
        *reinterpret_cast<void***>(rbx5) = reinterpret_cast<void**>(0);
        rax7 = rpl_nanosleep(rbp6, rbp6);
        if (!*reinterpret_cast<int32_t*>(&rax7)) 
            break;
    } while (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx5)) & 0xfffffffb));
    goto addr_13cf0_8;
    addr_13cbd_9:
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v2) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_3950();
    } else {
        return rax7;
    }
    addr_13cf0_8:
    *reinterpret_cast<int32_t*>(&rax7) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    goto addr_13cbd_9;
}

void fun_13d03(uint32_t edi, int32_t esi) {
    uint32_t ebp3;

    __asm__("cli ");
    ebp3 = exit_failure;
    if (edi > 3) {
        while (1) {
            if (edi != 4) 
                goto 0x3f06;
            if (esi >= 0) {
                addr_13d49_4:
            } else {
                addr_13d9f_5:
            }
            fun_3920();
            esi = 0;
            edi = ebp3;
            fun_3c90();
            fun_37c0();
        }
    } else {
        if (edi <= 1) {
            if (edi != 1) {
                goto 0x3f06;
            }
        }
        if (esi < 0) 
            goto addr_13d9f_5; else 
            goto addr_13d49_4;
    }
}

uint64_t fun_3d20(void** rdi);

int64_t fun_3990(void** rdi);

int64_t fun_13dd3(void** rdi, void** rsi, void** rdx, uint64_t* rcx, void** r8) {
    uint64_t* v6;
    void** rax7;
    void** v8;
    void** rcx9;
    uint64_t rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    uint64_t rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void**>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_3a10("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax");
        do {
            fun_3950();
            while (1) {
                rbx10 = 0xffffffffffffffff;
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_14144_6;
                    rbx10 = rbx10 * reinterpret_cast<unsigned char>(rcx9);
                } while (!__intrinsic());
            }
            addr_14144_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_13e8d_12:
            *v6 = rbx10;
            addr_13e95_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_37d0();
    *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_3e90(rdi, rsi, rdx);
    rcx9 = *reinterpret_cast<void***>(rax21);
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx23 * 2) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_13ecb_21;
    rsi = r15_14;
    rax24 = fun_3d20(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx10) + 4) = 0, rax26 = fun_3990(r13_18), r8 = r8, rax26 == 0))) {
            addr_13ecb_21:
            r12d11 = 4;
            goto addr_13e95_13;
        } else {
            addr_13f09_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_3990(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = reinterpret_cast<int32_t>("}");
                    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void**>("}");
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x17b90 + rbp33 * 4) + 0x17b90;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_20)) {
            r12d11 = 1;
            if (*reinterpret_cast<void***>(r12_20) != 34) 
                goto addr_13ecb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_13e8d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_13e8d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_3990(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_13f09_24;
    }
    r12d11 = r12d11 | 2;
    *v6 = rbx10;
    goto addr_13e95_13;
}

int64_t fun_3820();

int64_t fun_14203(void** rdi, void** rsi, void** rdx) {
    int64_t rax4;
    uint32_t ebx5;
    int64_t rax6;
    void** rax7;
    void** rax8;

    __asm__("cli ");
    rax4 = fun_3820();
    ebx5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax6 = rpl_fclose(rdi, rsi, rdx);
    if (ebx5) {
        if (*reinterpret_cast<int32_t*>(&rax6)) {
            addr_1425e_3:
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            rax7 = fun_37d0();
            *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax6)) {
            if (rax4) 
                goto addr_1425e_3;
            rax8 = fun_37d0();
            *reinterpret_cast<int32_t*>(&rax6) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax8) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        }
    }
    return rax6;
}

int64_t fun_14273(int32_t* rdi, int32_t* rsi) {
    int64_t r8_3;
    int64_t rcx4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    r8_3 = 0x8000000000000000;
    __asm__("comisd xmm0, [rip+0x39f0]");
    if (1) {
        addr_14313_2:
        return r8_3;
    } else {
        *rdi = *rsi;
        r8_3 = 0x7fffffffffffffff;
        __asm__("comisd xmm1, xmm0");
        if (1) 
            goto addr_14313_2;
        __asm__("cvttsd2si r8, xmm0");
        __asm__("pxor xmm1, xmm1");
        __asm__("cvtsi2sd xmm1, r8");
        __asm__("subsd xmm0, xmm1");
        __asm__("mulsd xmm0, [rip+0x39b9]");
        __asm__("pxor xmm1, xmm1");
        __asm__("cvttsd2si rax, xmm0");
        __asm__("cvtsi2sd xmm1, rax");
        __asm__("comisd xmm0, xmm1");
        rcx4 = rax5;
        rax6 = (__intrinsic() >> 26) - (rcx4 >> 63);
        r8_3 = 0x7fffffffffffffff + rax6;
        if (rcx4 - rax6 * 0x3b9aca00 >= 0) 
            goto addr_14313_2;
    }
    return r8_3 - 1;
}

uint32_t fun_3890(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_14333(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_3890(rdi);
        r12d10 = eax9;
        goto addr_14434_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_3890(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_14434_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_3950();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_144e9_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_3890(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_3890(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_37d0();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_3a40();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_14434_3;
                }
            }
        } else {
            eax22 = fun_3890(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_37d0(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_14434_3;
            } else {
                eax25 = fun_3890(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_14434_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_144e9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_14399_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_1439d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_1439d_18:
            if (0) {
            }
        } else {
            addr_143e5_23:
            eax28 = fun_3890(rdi);
            r12d10 = eax28;
            goto addr_14434_3;
        }
        eax29 = fun_3890(rdi);
        r12d10 = eax29;
        goto addr_14434_3;
    }
    if (0) {
    }
    eax30 = fun_3890(rdi);
    r12d10 = eax30;
    goto addr_14434_3;
    addr_14399_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_1439d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_143e5_23;
        goto addr_1439d_18;
    }
}

int32_t dup_safer_flag();

int64_t fun_145a3(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer_flag();
        rax3 = fun_37d0();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_3a40();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

void fun_14603() {
    uint32_t esi1;

    __asm__("cli ");
    if (esi1 & 0x80000) {
    }
    goto rpl_fcntl;
}

void** fun_3cf0();

void** fun_14623() {
    void** rax1;
    void** r12_2;
    void** rsi3;
    void** rdx4;
    uint32_t eax5;
    int64_t rdi6;
    int32_t eax7;
    void** rax8;
    void** rdi9;
    void** r13d10;
    void** rsi11;
    void** rdx12;
    void** rsi13;
    void** rdx14;
    int64_t rax15;
    int64_t rdi16;
    void** rsi17;
    void** rdx18;
    void** rax19;
    void** rax20;
    void** r12d21;

    __asm__("cli ");
    rax1 = fun_3cf0();
    r12_2 = rax1;
    if (!rax1) 
        goto addr_14646_2;
    eax5 = fun_3b40(rax1, rsi3, rdx4);
    if (eax5 > 2) 
        goto addr_14646_2;
    *reinterpret_cast<uint32_t*>(&rdi6) = eax5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
    eax7 = dup_safer(rdi6);
    if (eax7 < 0) {
        rax8 = fun_37d0();
        rdi9 = r12_2;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        r13d10 = *reinterpret_cast<void***>(rax8);
        rpl_fclose(rdi9, rsi11, rdx12);
        *reinterpret_cast<void***>(rax8) = r13d10;
        goto addr_14646_2;
    } else {
        rax15 = rpl_fclose(r12_2, rsi13, rdx14);
        if (!*reinterpret_cast<int32_t*>(&rax15)) {
            *reinterpret_cast<int32_t*>(&rdi16) = eax7;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
            rax19 = fun_3c20(rdi16, rsi17, rdx18);
            r12_2 = rax19;
            if (rax19) {
                addr_14646_2:
                return r12_2;
            }
        }
        rax20 = fun_37d0();
        r12d21 = *reinterpret_cast<void***>(rax20);
        fun_3a40();
        *reinterpret_cast<void***>(rax20) = r12d21;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        goto addr_14646_2;
    }
}

void** fun_146c3() {
    void** rax1;

    __asm__("cli ");
    rax1 = fun_3bb0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*reinterpret_cast<void***>(rax1)) {
            rax1 = reinterpret_cast<void**>("ASCII");
        }
        return rax1;
    }
}

uint64_t fun_3970(uint32_t* rdi);

uint64_t fun_14703(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    int32_t eax8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_3970(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (eax8 = hard_locale(), !*reinterpret_cast<signed char*>(&eax8)))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_3950();
    } else {
        return r12_7;
    }
}

int64_t fun_14843(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    unsigned char* r15_6;
    unsigned char* r14_7;
    uint32_t r9d8;
    uint32_t r8d9;
    uint32_t eax10;
    uint32_t r9d11;
    uint32_t r8d12;
    uint32_t r10d13;
    void** rax14;
    int64_t rax15;

    __asm__("cli ");
    if (rsi != rcx || (rax5 = fun_3a80(rdi, rdx, rsi), !!*reinterpret_cast<uint32_t*>(&rax5))) {
        r15_6 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
        r14_7 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<unsigned char>(rcx));
        r9d8 = *r15_6;
        r8d9 = *r14_7;
        *r15_6 = 0;
        *r14_7 = 0;
        eax10 = strcoll_loop(rdi, rsi + 1, rdx, rcx + 1);
        r9d11 = *reinterpret_cast<unsigned char*>(&r9d8);
        r8d12 = *reinterpret_cast<unsigned char*>(&r8d9);
        r10d13 = eax10;
        *r15_6 = *reinterpret_cast<unsigned char*>(&r9d11);
        *r14_7 = *reinterpret_cast<unsigned char*>(&r8d12);
    } else {
        rax14 = fun_37d0();
        r10d13 = *reinterpret_cast<uint32_t*>(&rax5);
        *reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(0);
    }
    *reinterpret_cast<uint32_t*>(&rax15) = r10d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    return rax15;
}

int64_t fun_148f3(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rax5;
    void** rax6;

    __asm__("cli ");
    if (rsi != rcx || (rax5 = fun_3a80(rdi, rdx, rsi), !!*reinterpret_cast<int32_t*>(&rax5))) {
        goto strcoll_loop;
    } else {
        rax6 = fun_37d0();
        *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(0);
        return 0;
    }
}

struct s100 {
    int64_t f0;
    uint64_t f8;
};

int64_t fun_39b0(void* rdi, int64_t* rsi);

int64_t fun_14963(struct s100* rdi, int64_t* rsi) {
    void** rax3;
    void** v4;
    void** rax5;
    int64_t rax6;
    int64_t rbx7;
    int64_t* rbp8;
    void* r12_9;
    void* rdx10;

    __asm__("cli ");
    rax3 = g28;
    v4 = rax3;
    if (rdi->f8 > 0x3b9ac9ff) {
        rax5 = fun_37d0();
        *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(22);
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rbx7 = rdi->f0;
        rbp8 = rsi;
        r12_9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 32);
        if (rbx7 > 0x1fa400) {
            do {
                rbx7 = rbx7 - 0x1fa400;
                rax6 = fun_39b0(r12_9, rbp8);
                if (*reinterpret_cast<int32_t*>(&rax6)) 
                    goto addr_149d8_5;
            } while (rbx7 > 0x1fa400);
            goto addr_149e8_7;
        } else {
            goto addr_149e8_7;
        }
    }
    addr_149f7_9:
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_3950();
    } else {
        return rax6;
    }
    addr_149d8_5:
    if (rbp8) {
        *rbp8 = *rbp8 + rbx7;
        goto addr_149f7_9;
    }
    addr_149e8_7:
    rax6 = fun_39b0(r12_9, rbp8);
    goto addr_149f7_9;
}

struct s102 {
    int64_t f0;
    int64_t f8;
};

struct s101 {
    int64_t f0;
    struct s102* f8;
    uint64_t f10;
    uint64_t f18;
    int64_t f20;
    signed char[8] pad48;
    void** f30;
    signed char[7] pad56;
    int64_t f38;
    signed char[8] pad72;
    int64_t f48;
    unsigned char f50;
};

int64_t obstack_alloc_failed_handler = 0x14a30;

void fun_14a33() {
    void** rax1;
    void** rdi2;
    int64_t rsi3;
    void** r8_4;
    void** r9_5;
    int64_t rax6;
    void** v7;
    void** v8;
    void** v9;
    void** v10;
    struct s101* rdi11;
    void** r12_12;
    void** rbp13;
    int64_t rax14;
    int64_t rdi15;
    struct s102* rax16;
    uint64_t rdx17;
    int64_t rdx18;

    __asm__("cli ");
    rax1 = fun_3920();
    rdi2 = stderr;
    *reinterpret_cast<int32_t*>(&rsi3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi3) + 4) = 0;
    fun_3db0(rdi2, 1, "%s\n", rax1, r8_4, r9_5, rax6, __return_address(), v7, v8, v9, v10);
    *reinterpret_cast<uint32_t*>(&rdi11) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
    fun_3d90();
    if (0) {
        *reinterpret_cast<int32_t*>(&r12_12) = 15;
        *reinterpret_cast<int32_t*>(&r12_12 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp13) = 16;
        *reinterpret_cast<int32_t*>(&rbp13 + 4) = 0;
    } else {
        rbp13 = reinterpret_cast<void**>("%s\n");
        r12_12 = reinterpret_cast<void**>(" %s\n");
    }
    rdi11->f30 = r12_12;
    if (0) {
        rsi3 = 0xfe0;
    }
    rax14 = rdi11->f38;
    rdi11->f0 = rsi3;
    if (!(rdi11->f50 & 1)) {
        rdi15 = rsi3;
        rax16 = reinterpret_cast<struct s102*>(rax14(rdi15));
    } else {
        rdi15 = rdi11->f48;
        rax16 = reinterpret_cast<struct s102*>(rax14(rdi15));
    }
    rdi11->f8 = rax16;
    if (!rax16) {
        obstack_alloc_failed_handler(rdi15);
    } else {
        rdx17 = reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rax16) + reinterpret_cast<unsigned char>(r12_12)) + 16 & reinterpret_cast<uint64_t>(-reinterpret_cast<unsigned char>(rbp13));
        rdi11->f10 = rdx17;
        rdi11->f18 = rdx17;
        rdx18 = rdi11->f0 + reinterpret_cast<int64_t>(rax16);
        rax16->f0 = rdx18;
        rdi11->f20 = rdx18;
        rax16->f8 = 0;
        rdi11->f50 = reinterpret_cast<unsigned char>(rdi11->f50 & 0xf9);
        goto rax6;
    }
}

struct s103 {
    signed char[56] pad56;
    int64_t f38;
    int64_t f40;
    signed char[8] pad80;
    unsigned char f50;
};

void fun_14b23(struct s103* rdi) {
    int64_t rcx2;
    int64_t r8_3;

    __asm__("cli ");
    rdi->f50 = reinterpret_cast<unsigned char>(rdi->f50 & 0xfe);
    rdi->f38 = rcx2;
    rdi->f40 = r8_3;
    goto 0x14a80;
}

struct s104 {
    signed char[56] pad56;
    int64_t f38;
    int64_t f40;
    int64_t f48;
    unsigned char f50;
};

void fun_14b43(struct s104* rdi) {
    int64_t rcx2;
    int64_t r8_3;
    int64_t r9_4;

    __asm__("cli ");
    rdi->f50 = reinterpret_cast<unsigned char>(rdi->f50 | 1);
    rdi->f38 = rcx2;
    rdi->f40 = r8_3;
    rdi->f48 = r9_4;
    goto 0x14a80;
}

struct s106 {
    void* f0;
    struct s106* f8;
};

struct s105 {
    struct s105* f0;
    struct s106* f8;
    void** f10;
    signed char[7] pad24;
    void* f18;
    void* f20;
    signed char[8] pad48;
    void* f30;
    int64_t f38;
    int64_t f40;
    struct s105* f48;
    unsigned char f50;
};

void** fun_14b63(struct s105* rdi, struct s105* rsi) {
    int64_t rcx3;
    struct s105* rbx4;
    void** r13_5;
    struct s105* tmp64_6;
    struct s106* rbp7;
    struct s105* tmp64_8;
    struct s105* rsi9;
    struct s105* rax10;
    int64_t rax11;
    struct s106* rax12;
    struct s106* r12_13;
    struct s106* rax14;
    void* rax15;
    void** rsi16;
    void** r14_17;
    void** rax18;
    uint32_t edx19;
    int64_t rax20;
    struct s105* rdi21;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rcx3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    rbx4 = rdi;
    r13_5 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi->f18) - reinterpret_cast<unsigned char>(rdi->f10));
    tmp64_6 = reinterpret_cast<struct s105*>(reinterpret_cast<uint64_t>(rsi) + reinterpret_cast<unsigned char>(r13_5));
    rbp7 = rdi->f8;
    *reinterpret_cast<unsigned char*>(&rcx3) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_6) < reinterpret_cast<uint64_t>(rsi));
    tmp64_8 = reinterpret_cast<struct s105*>(reinterpret_cast<uint64_t>(tmp64_6) + reinterpret_cast<int64_t>(rdi->f30));
    rsi9 = tmp64_8;
    *reinterpret_cast<unsigned char*>(&rdi) = reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(tmp64_8) < reinterpret_cast<uint64_t>(tmp64_6));
    rax10 = reinterpret_cast<struct s105*>(reinterpret_cast<uint64_t>(rsi9) + (reinterpret_cast<unsigned char>(r13_5) >> 3) + 100);
    if (reinterpret_cast<uint64_t>(rsi9) < reinterpret_cast<uint64_t>(rbx4->f0)) {
        rsi9 = rbx4->f0;
    }
    if (reinterpret_cast<uint64_t>(rax10) >= reinterpret_cast<uint64_t>(rsi9)) {
        rsi9 = rax10;
    }
    if (!rcx3 && (*reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<unsigned char*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0, !rdi)) {
        rax11 = rbx4->f38;
        if (rbx4->f50 & 1) {
            rdi = rbx4->f48;
            rax12 = reinterpret_cast<struct s106*>(rax11(rdi));
            r12_13 = rax12;
        } else {
            rdi = rsi9;
            rax14 = reinterpret_cast<struct s106*>(rax11(rdi));
            r12_13 = rax14;
        }
        if (r12_13) {
            rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<uint64_t>(rsi9));
            rbx4->f8 = r12_13;
            rsi16 = rbx4->f10;
            r12_13->f8 = rbp7;
            rbx4->f20 = rax15;
            r12_13->f0 = rax15;
            r14_17 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_13) + reinterpret_cast<int64_t>(rbx4->f30) + 16) & reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rbx4->f30)));
            rax18 = fun_3b30(r14_17, rsi16, r13_5);
            edx19 = rbx4->f50;
            if (!(*reinterpret_cast<unsigned char*>(&edx19) & 2) && (rax18 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(~reinterpret_cast<int64_t>(rbx4->f30)) & reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp7) + reinterpret_cast<int64_t>(rbx4->f30) + 16)), rbx4->f10 == rax18)) {
                r12_13->f8 = rbp7->f8;
                rax20 = rbx4->f40;
                if (!(edx19 & 1)) {
                    rax18 = reinterpret_cast<void**>(rax20(rbp7, rsi16));
                } else {
                    rdi21 = rbx4->f48;
                    rax18 = reinterpret_cast<void**>(rax20(rdi21, rbp7));
                }
            }
            rbx4->f10 = r14_17;
            rbx4->f18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r14_17) + reinterpret_cast<unsigned char>(r13_5));
            rbx4->f50 = reinterpret_cast<unsigned char>(rbx4->f50 & 0xfd);
            return rax18;
        }
    }
    obstack_alloc_failed_handler(rdi);
}

struct s107 {
    struct s107* f0;
    struct s107* f8;
};

struct s108 {
    signed char[8] pad8;
    struct s107* f8;
};

struct s107* fun_14c93(struct s108* rdi, struct s107* rsi) {
    struct s107* rax3;

    __asm__("cli ");
    rax3 = rdi->f8;
    if (!rax3) {
        return rax3;
    }
    do {
        if (reinterpret_cast<uint64_t>(rsi) <= reinterpret_cast<uint64_t>(rax3)) 
            continue;
        if (reinterpret_cast<uint64_t>(rax3->f0) >= reinterpret_cast<uint64_t>(rsi)) 
            break;
        rax3 = rax3->f8;
    } while (rax3);
    goto addr_14cb3_7;
    return 1;
    addr_14cb3_7:
    return 0;
}

struct s110 {
    struct s110* f0;
    struct s110* f8;
};

struct s109 {
    signed char[8] pad8;
    struct s110* f8;
    struct s110* f10;
    struct s110* f18;
    struct s110* f20;
    signed char[24] pad64;
    int64_t f40;
    int64_t f48;
    unsigned char f50;
};

void fun_14cd3(struct s109* rdi, struct s110* rsi) {
    struct s110* r12_3;
    struct s110* rsi4;
    struct s109* rbx5;
    struct s110* rax6;
    struct s110* rbp7;
    int64_t rax8;
    int64_t rdi9;

    __asm__("cli ");
    r12_3 = rsi;
    rsi4 = rdi->f8;
    rbx5 = rdi;
    if (rsi4) {
        while (reinterpret_cast<uint64_t>(rsi4) >= reinterpret_cast<uint64_t>(r12_3) || (rax6 = rsi4->f0, reinterpret_cast<uint64_t>(rax6) < reinterpret_cast<uint64_t>(r12_3))) {
            rbp7 = rsi4->f8;
            rax8 = rbx5->f40;
            if (rbx5->f50 & 1) {
                rdi9 = rbx5->f48;
                rax8(rdi9);
                rbx5->f50 = reinterpret_cast<unsigned char>(rbx5->f50 | 2);
                if (!rbp7) 
                    goto addr_14d2b_5;
            } else {
                rax8(rsi4);
                rbx5->f50 = reinterpret_cast<unsigned char>(rbx5->f50 | 2);
                if (!rbp7) 
                    goto addr_14d2b_5;
            }
            rsi4 = rbp7;
        }
    } else {
        goto addr_14d2b_5;
    }
    rbx5->f18 = r12_3;
    rbx5->f10 = r12_3;
    rbx5->f20 = rax6;
    rbx5->f8 = rsi4;
    return;
    addr_14d2b_5:
    if (r12_3) 
        goto 0x3f0b;
    return;
}

struct s112 {
    int64_t f0;
    struct s112* f8;
};

struct s111 {
    signed char[8] pad8;
    struct s112* f8;
};

int64_t fun_14d63(struct s111* rdi) {
    struct s112* rax2;
    int64_t r8_3;
    int64_t rdx4;

    __asm__("cli ");
    rax2 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_3) + 4) = 0;
    if (rax2) {
        do {
            rdx4 = rax2->f0 - reinterpret_cast<int64_t>(rax2);
            rax2 = rax2->f8;
            r8_3 = r8_3 + rdx4;
        } while (rax2);
    }
    return r8_3;
}

void fun_14d93() {
    __asm__("cli ");
    goto rpl_fcntl;
}

void fun_14db3() {
    __asm__("cli ");
}

void fun_14dc7() {
    __asm__("cli ");
    return;
}

void fun_486f() {
    void** rdi1;
    uint32_t eax2;
    void** rdx3;
    void** eax4;
    uint32_t edx5;

    rdi1 = optarg;
    eax2 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi1))));
    if (!*reinterpret_cast<signed char*>(&eax2)) 
        goto 0x68c8;
    if (*reinterpret_cast<void***>(rdi1 + 1)) {
        eax4 = fun_3ad0(rdi1, "\\0", rdx3);
        if (eax4) 
            goto 0x6899;
        eax2 = 0;
    }
    edx5 = tab;
    if (edx5 == 0x80 || edx5 == eax2) {
        tab = eax2;
        goto 0x4440;
    } else {
        fun_3920();
        fun_3c90();
        fun_3920();
        fun_3c90();
    }
}

void fun_48c8() {
    unique = 1;
    goto 0x4440;
}

void fun_4913() {
    eolchar = 0;
    goto 0x4440;
}

void fun_4958() {
    debug = 1;
    goto 0x4440;
}

void fun_4ac6() {
    void* rsp1;
    void** rdi2;
    int32_t ecx3;
    void** v4;
    void** rsi5;
    void** rdi6;
    void** rax7;
    void* rsp8;
    void** rdi9;
    int64_t v10;
    void** rax11;
    void** rdx12;
    void* v13;
    void** v14;
    void** rdi15;
    void** rsi16;
    void** rdx17;
    void** rax18;
    void* rsp19;
    void** rax20;
    void** rdi21;
    void** rax22;
    void** rdi23;

    rsp1 = __zero_stack_offset();
    rdi2 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp1) + 0xc0);
    ecx3 = 18;
    v4 = rdi2;
    rsi5 = rdi2;
    while (ecx3) {
        --ecx3;
        rsi5 = rsi5 + 4;
    }
    rdi6 = optarg;
    rax7 = parse_field_count(rdi6, rsi5, "invalid number at field start");
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp1) - 8 + 8);
    rdi9 = rax7;
    if (!v10) 
        goto 0x67b5;
    if (*reinterpret_cast<void***>(rdi9) == 46 && (rax11 = parse_field_count(rdi9 + 1, reinterpret_cast<int64_t>(rsp8) + 0xc8, "invalid number after '.'"), rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8), rdi9 = rax11, rdx12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v13) + 0xffffffffffffffff), v14 = rdx12, !v13)) {
        rdi15 = optarg;
        badfieldspec(rdi15, "character offset is zero", rdx12);
    }
    if (!(reinterpret_cast<uint64_t>(v10 - 1) | reinterpret_cast<unsigned char>(v14))) {
    }
    rsi16 = v4;
    *reinterpret_cast<uint32_t*>(&rdx17) = 0;
    *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
    rax18 = set_ordering(rdi9, rsi16, 0);
    rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
    if (*reinterpret_cast<void***>(rax18) != 44) 
        goto addr_4b5a_10;
    rax20 = parse_field_count(rax18 + 1, reinterpret_cast<int64_t>(rsp19) + 0xd0, "invalid number after ','");
    rdi21 = rax20;
    if (0) 
        goto 0x67b5;
    if (*reinterpret_cast<void***>(rdi21) != 46) 
        goto addr_6072_13;
    rax22 = parse_field_count(rdi21 + 1, reinterpret_cast<int64_t>(rsp19) - 8 + 8 + 0xd8, "invalid number after '.'");
    rdi21 = rax22;
    addr_6072_13:
    rsi16 = v4;
    *reinterpret_cast<uint32_t*>(&rdx17) = 1;
    *reinterpret_cast<int32_t*>(&rdx17 + 4) = 0;
    rax18 = set_ordering(rdi21, rsi16, 1);
    addr_4b72_15:
    if (*reinterpret_cast<void***>(rax18)) {
        rdi23 = optarg;
        badfieldspec(rdi23, "stray character in field spec", rdx17);
    } else {
        insertkey(v4, rsi16, rdx17);
        goto 0x4440;
    }
    addr_4b5a_10:
    goto addr_4b72_15;
}

void fun_4b8a() {
    void** rdx1;
    void** v2;
    void** eax3;

    rdx1 = optarg;
    if (!v2 || (eax3 = fun_3ad0(v2, rdx1, rdx1), !eax3)) {
        goto 0x4440;
    } else {
        fun_3920();
        fun_3c90();
    }
}

void fun_4bbf() {
    void** r9_1;
    uint32_t eax2;
    int64_t v3;
    uint64_t v4;

    r9_1 = optarg;
    eax2 = xstrtoumax(r9_1, r9_1);
    if (eax2) 
        goto 0x4d65;
    if (*reinterpret_cast<signed char*>(v3 - 1) - 48 > 9) 
        goto 0x4c44;
    if (v4 > 0x3fffffffffffff) 
        goto 0x61be;
}

void fun_4c6e() {
    void** rdx1;

    rdx1 = optarg;
    if (*reinterpret_cast<void***>(rdx1) == 43) 
        goto 0x560a; else 
        goto "???";
}

void fun_4c8b() {
    void** r10_1;
    uint32_t eax2;
    int64_t v3;
    void** rdi4;

    r10_1 = optarg;
    eax2 = xstrtoumax(r10_1);
    if (eax2 == 1) {
        goto 0x4440;
    } else {
        if (eax2) 
            goto 0x69e6;
        if (v3) 
            goto 0x4440;
        fun_3920();
        fun_3c90();
        rdi4 = optarg;
        add_temp_dir(rdi4, rdi4);
        goto 0x4440;
    }
}

void fun_577e() {
    goto 0x4440;
}

struct s113 {
    signed char[55] pad55;
    signed char f37;
};

struct s114 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_6ee8() {
    struct s113* rsi1;
    uint32_t eax2;
    struct s114* r8_3;

    rsi1->f37 = 1;
    eax2 = r8_3->f1;
    if (*reinterpret_cast<signed char*>(&eax2)) 
        goto 0x6ed0; else 
        goto "???";
}

struct s115 {
    signed char[50] pad50;
    signed char f32;
};

void fun_6f08() {
    struct s115* rsi1;

    rsi1->f32 = 1;
    goto 0x6ef0;
}

struct s116 {
    signed char[53] pad53;
    signed char f35;
};

void fun_6f20() {
    struct s116* rsi1;

    rsi1->f35 = 1;
    goto 0x6ef0;
}

struct s117 {
    signed char[56] pad56;
    signed char f38;
};

void fun_6f80() {
    struct s117* rsi1;

    rsi1->f38 = 1;
    goto 0x6ef0;
}

void fun_7098() {
    int64_t* rbx1;
    int64_t v2;

    *rbx1 = v2;
}

void fun_70c0() {
    int64_t* rbx1;

    *rbx1 = -1;
    goto 0x70a0;
}

int32_t fun_3e30(int64_t rdi, void** rsi);

void fun_f905() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    uint32_t ecx44;
    unsigned char al45;
    void** v46;
    uint64_t v47;
    void* v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    void** rcx84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    uint32_t eax90;
    void* rdx91;
    void* rcx92;
    void* v93;
    void** rax94;
    uint1_t zf95;
    int32_t ecx96;
    int32_t ecx97;
    uint32_t ecx98;
    uint32_t edi99;
    int32_t ecx100;
    uint32_t edi101;
    uint32_t edi102;
    uint64_t rax103;
    uint32_t eax104;
    uint32_t r12d105;
    uint64_t rax106;
    uint64_t rax107;
    uint32_t r12d108;
    uint32_t ecx109;
    void** v110;
    void** rdx111;
    void* rax112;
    void* v113;
    uint64_t rax114;
    int64_t v115;
    int64_t rax116;
    int64_t rax117;
    int64_t rax118;
    int64_t v119;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_3920();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_3920();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_3940(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_fc03_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_fc03_22; else 
                            goto addr_fffd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_100bd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_10410_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_fc00_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_fc00_30; else 
                                goto addr_10429_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_3940(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_10410_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        rax24 = fun_3a80(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_10410_28; else 
                            goto addr_faac_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_10570_39:
                    ecx44 = 0x7d;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_103f0_40:
                        if (r11_27 == 1) {
                            addr_ff7d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_10538_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = ecx44;
                                goto addr_fbb7_44;
                            }
                        } else {
                            goto addr_10400_46;
                        }
                    } else {
                        addr_1057f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_ff7d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        ecx44 = 0x7b;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_fc03_22:
                                if (v47 != 1) {
                                    addr_10159_52:
                                    v48 = reinterpret_cast<void*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_3940(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_101a4_54;
                                    }
                                } else {
                                    goto addr_fc10_56;
                                }
                            } else {
                                addr_fbb5_57:
                                ebp36 = 0;
                                goto addr_fbb7_44;
                            }
                        } else {
                            addr_103e4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_1057f_47; else 
                                goto addr_103ee_59;
                        }
                    } else {
                        ecx44 = 0x7e;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_ff7d_41;
                        if (v47 == 1) 
                            goto addr_fc10_56; else 
                            goto addr_10159_52;
                    }
                }
                addr_fc71_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_fb08_63:
                    if (!1 && (edx51 = ecx44, *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<signed char*>(&ecx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_fb2d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_fe30_65;
                    } else {
                        addr_fc99_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_104e8_67;
                    }
                } else {
                    goto addr_fc90_69;
                }
                addr_fb41_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_fb8c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&ecx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_104e8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_fb8c_81;
                }
                addr_fc90_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_fb2d_64; else 
                    goto addr_fc99_66;
                addr_fbb7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_fc6f_91;
                if (v22) 
                    goto addr_fbcf_93;
                addr_fc6f_91:
                ecx44 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_fc71_62;
                addr_101a4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_1092b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_1099b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_1079f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_3e30(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_3e10(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_1029e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_fc5c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_102a8_112;
                    }
                } else {
                    addr_102a8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx84 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_10379_114;
                }
                addr_fc68_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_fc6f_91;
                while (1) {
                    addr_10379_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax85 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_10887_117;
                        eax86 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax86)) 
                            goto addr_102e6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx84)) 
                            goto addr_10895_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_10367_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_10367_128;
                        }
                    }
                    addr_10315_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax87 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax87) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax87) >> 6);
                        eax88 = eax87 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax88);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax89 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax89) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax89) >> 3);
                        eax90 = (eax89 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax90);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx84)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_10367_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_102e6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax86;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_10315_134;
                }
                ebp36 = v79;
                ecx44 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_fb8c_81;
                addr_10895_125:
                ebp36 = v79;
                ecx44 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_104e8_67;
                addr_1092b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_1029e_109;
                addr_1099b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx91 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx92 = v93;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx92) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx91 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx91) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx91));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx91;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_1029e_109;
                addr_fc10_56:
                rax94 = fun_3e90(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf95 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*reinterpret_cast<void***>(rax94) + reinterpret_cast<unsigned char>(rax24) * 2) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf95);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf95) & reinterpret_cast<unsigned char>(v32));
                goto addr_fc5c_110;
                addr_103ee_59:
                goto addr_103f0_40;
                addr_100bd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_fc03_22;
                ecx96 = static_cast<int32_t>(rbx43 - 65);
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx96));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_fc68_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_fbb5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_fc03_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_10102_160;
                if (!v22) 
                    goto addr_104d7_162; else 
                    goto addr_106e3_163;
                addr_10102_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_104d7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    ecx44 = 92;
                    goto addr_104e8_67;
                } else {
                    ecx44 = 92;
                    if (!v32) 
                        goto addr_ffab_166;
                }
                ecx44 = *reinterpret_cast<uint32_t*>(&rbx43);
                ebp36 = 0;
                addr_fe13_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_fb41_70; else 
                    goto addr_fe27_169;
                addr_ffab_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_fb08_63;
                goto addr_fc90_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        ecx44 = 0x7d;
                        r8d42 = 0;
                        goto addr_103e4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_1051f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_fc00_30;
                    ecx97 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx97));
                    ecx98 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_faf8_178; else 
                        goto addr_104a2_179;
                }
                ecx44 = 0x7b;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_103e4_58;
                ecx44 = 0x7c;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_fc03_22;
                }
                addr_1051f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_fc00_30:
                    r8d42 = 0;
                    goto addr_fc03_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        ecx44 = 0x7e;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_fc71_62;
                    } else {
                        ecx44 = 0x7e;
                        goto addr_10538_42;
                    }
                }
                addr_faf8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx98;
                ecx44 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_fb08_63;
                addr_104a2_179:
                ecx44 = *reinterpret_cast<uint32_t*>(&rbx43);
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_10400_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_fc71_62;
                } else {
                    addr_104b2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_fc03_22;
                }
                edi99 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi99))) 
                    goto addr_10c62_188;
                if (v28) 
                    goto addr_104d7_162;
                addr_10c62_188:
                ecx44 = 92;
                ebp36 = 0;
                goto addr_fe13_168;
                addr_faac_37:
                if (v22) 
                    goto addr_10aa3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_fac3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_10570_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_105fb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_fc03_22;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx98 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_faf8_178; else 
                        goto addr_105d7_199;
                }
                ecx44 = 0x7b;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_103e4_58;
                ecx44 = 0x7c;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_fc03_22;
                }
                addr_105fb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_fc03_22;
                }
                addr_105d7_199:
                ecx44 = *reinterpret_cast<uint32_t*>(&rbx43);
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_10400_46;
                goto addr_104b2_186;
                addr_fac3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_fc03_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_fc03_22; else 
                    goto addr_fad4_206;
            }
            edi101 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<uint32_t*>(&rdx10) = edi101 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(r15_16 == 0)) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_10bae_208;
            edi102 = edi101 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi102;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi102));
            if (!rax24) 
                goto addr_10a34_210;
            if (1) 
                goto addr_10a32_212;
            if (!v29) 
                goto addr_1066e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax103 = fun_3930();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax103;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_10ba1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_fe30_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax104 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax104)) 
            goto addr_fbeb_219; else 
            goto addr_fe4a_220;
        addr_fbcf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax85 = reinterpret_cast<unsigned char>(v32);
        addr_fbe3_221:
        if (*reinterpret_cast<signed char*>(&eax85)) 
            goto addr_fe4a_220; else 
            goto addr_fbeb_219;
        addr_1079f_103:
        r12d105 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d105)) {
            addr_fe4a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax106 = fun_3930();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax106;
        } else {
            addr_107bd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax107 = fun_3930();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax107;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_10c30_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_10696_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_10887_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_fbe3_221;
        addr_106e3_163:
        eax85 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_fbe3_221;
        addr_fe27_169:
        goto addr_fe30_65;
        addr_10bae_208:
        r14_35 = r12_21;
        r12d108 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d108)) 
            goto addr_fe4a_220;
        goto addr_107bd_222;
        addr_10a34_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (ecx109 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), !!*reinterpret_cast<signed char*>(&ecx109)))) {
            rsi30 = v110;
            rdx111 = r15_16;
            rax112 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx111)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx111)) = *reinterpret_cast<signed char*>(&ecx109);
                }
                ++rdx111;
                ecx109 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax112) + reinterpret_cast<unsigned char>(rdx111));
            } while (*reinterpret_cast<signed char*>(&ecx109));
            r15_16 = rdx111;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v113) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax114 = reinterpret_cast<uint64_t>(v115 - reinterpret_cast<unsigned char>(g28));
        if (!rax114) 
            goto addr_10a8e_236;
        fun_3950();
        rsp25 = rsp25 - 8 + 8;
        goto addr_10c30_225;
        addr_10a32_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_10a34_210;
        addr_1066e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_10a34_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_10696_226;
        }
        addr_10ba1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_fffd_24:
    *reinterpret_cast<uint32_t*>(&rax116) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax116) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x174ac + rax116 * 4) + 0x174ac;
    addr_10429_32:
    *reinterpret_cast<uint32_t*>(&rax117) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax117) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x175ac + rax117 * 4) + 0x175ac;
    addr_10aa3_190:
    addr_fbeb_219:
    goto 0xf8d0;
    addr_fad4_206:
    *reinterpret_cast<uint32_t*>(&rax118) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax118) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x173ac + rax118 * 4) + 0x173ac;
    addr_10a8e_236:
    goto v119;
}

void fun_faf0() {
}

void fun_fca8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0xf9a2;
}

void fun_fd01() {
    goto 0xf9a2;
}

void fun_fdee() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0xfc71;
    }
    if (v2) 
        goto 0x106e3;
    if (!r10_3) 
        goto addr_1084e_5;
    if (!v4) 
        goto addr_1071e_7;
    addr_1084e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_1071e_7:
    goto 0xfb24;
}

void fun_fe0c() {
}

void fun_feb7() {
    signed char v1;

    if (v1) {
        goto 0xfe3f;
    } else {
        goto 0xfb7a;
    }
}

void fun_fed1() {
    signed char v1;

    if (!v1) 
        goto 0xfeca; else 
        goto "???";
}

void fun_fef8() {
    goto 0xfe13;
}

void fun_ff78() {
}

void fun_ff90() {
}

void fun_ffbf() {
    goto 0xfe13;
}

void fun_10011() {
    goto 0xffa0;
}

void fun_10040() {
    goto 0xffa0;
}

void fun_10073() {
    goto 0xffa0;
}

void fun_10440() {
    goto 0xfaf8;
}

void fun_1073e() {
    signed char v1;

    if (v1) 
        goto 0x106e3;
    goto 0xfb24;
}

void fun_107e5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0xfb24;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0xfb08;
        goto 0xfb24;
    }
}

void fun_10c02() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0xfe70;
    } else {
        goto 0xf9a2;
    }
}

void fun_12ee8() {
    fun_3920();
}

void fun_13f7c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x13f8b;
}

void fun_1404c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x14059;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x13f8b;
}

void fun_14070() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_1409c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x13f8b;
}

void fun_140bd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x13f8b;
}

void fun_140e1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x13f8b;
}

void fun_14105() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x14094;
}

void fun_14129() {
}

void fun_14149() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x14094;
}

void fun_14165() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x14094;
}

void fun_479b(int64_t rdi) {
    void** rsi2;
    int32_t ebp3;
    void** r9_4;
    struct s29* rax5;
    int32_t eax6;
    signed char v7;

    rsi2 = optarg;
    ebp3 = 99;
    if (rsi2) {
        r9_4 = argmatch_die;
        rax5 = __xargmatch_internal("--check", rsi2, 0x1c5c0, "CCc", 1, r9_4);
        ebp3 = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>("CCc") + reinterpret_cast<uint64_t>(rax5));
    }
    eax6 = v7;
    if (*reinterpret_cast<signed char*>(&eax6)) {
        if (eax6 != ebp3) 
            goto 0x65ec;
    }
    goto 0x4440;
}

void fun_48d4() {
    int64_t rcx1;
    void** rdx2;
    int64_t rax3;
    int64_t r13_4;
    int32_t ecx5;
    int32_t esi6;

    rcx1 = reinterpret_cast<int32_t>(optind);
    rdx2 = optarg;
    rax3 = rcx1;
    if (*reinterpret_cast<void***>(r13_4 + rcx1 * 8 - 8) != rdx2) 
        goto 0x4440;
    while (ecx5 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx2)), esi6 = ecx5, ecx5 - 48 <= 9) {
        ++rdx2;
    }
    optind = *reinterpret_cast<int32_t*>(&rax3) - 1 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&esi6) < 1);
    goto 0x4440;
}

void fun_491f() {
    void** rdi1;
    void** rdx2;
    void** eax3;

    rdi1 = compress_program;
    rdx2 = optarg;
    if (rdi1) {
        eax3 = fun_3ad0(rdi1, rdx2, rdx2);
        rdx2 = rdx2;
        if (eax3) 
            goto 0x6a9b;
    }
    compress_program = rdx2;
    goto 0x4440;
}

void fun_4964() {
    goto 0x4440;
}

struct s118 {
    signed char[32] pad32;
    int64_t f20;
};

struct s119 {
    signed char[32] pad32;
    int64_t f20;
};

void fun_6f10() {
    struct s118* rsi1;
    struct s119* rsi2;
    int64_t r11_3;

    if (rsi1->f20) 
        goto 0x6ef0;
    rsi2->f20 = r11_3;
    goto 0x6ef0;
}

struct s120 {
    signed char[52] pad52;
    signed char f34;
};

void fun_6f30() {
    struct s120* rsi1;

    rsi1->f34 = 1;
    goto 0x6ef0;
}

struct s121 {
    signed char[51] pad51;
    signed char f33;
};

void fun_6f90() {
    struct s121* rsi1;

    rsi1->f33 = 1;
    goto 0x6ef0;
}

void fun_70d0() {
    int64_t r12_1;

    if (r12_1) 
        goto 0x70de;
    goto 0x70a5;
}

void fun_fd2e() {
    goto 0xf9a2;
}

void fun_ff04() {
    goto 0xfebc;
}

void fun_ffcb() {
    goto 0xfaf8;
}

void fun_1001d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0xffa0;
    goto 0xfbcf;
}

void fun_1004f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0xffab;
        goto 0xf9d0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0xfe4a;
        goto 0xfbeb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x107e8;
    if (r10_8 > r15_9) 
        goto addr_ff35_9;
    addr_ff3a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x107f3;
    goto 0xfb24;
    addr_ff35_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_ff3a_10;
}

void fun_10082() {
    goto 0xfbb7;
}

void fun_10450() {
    goto 0xfbb7;
}

void fun_10bef() {
    int32_t ebx1;

    if (ebx1) {
        goto 0xfd0c;
    } else {
        goto 0xfe70;
    }
}

void fun_12fa0() {
}

void fun_1401f() {
    if (__intrinsic()) 
        goto 0x14059; else 
        goto "???";
}

void fun_4975() {
    void** r10_1;
    uint32_t eax2;
    void** rsi3;
    int32_t eax4;
    uint32_t r9d5;
    int64_t rax6;
    int32_t v7;
    int64_t rdx8;
    int64_t v9;
    int64_t rax10;

    r10_1 = optarg;
    eax2 = xstrtoumax(r10_1);
    rsi3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8 + 0x1b0);
    eax4 = fun_3dc0(7, rsi3, 10, 7, rsi3, 10);
    r9d5 = 17;
    if (!eax4) {
        *reinterpret_cast<int32_t*>(&rax6) = v7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        r9d5 = static_cast<uint32_t>(rax6 - 3);
    }
    if (eax2) 
        goto 0x4d96;
    rdx8 = v9;
    *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<uint32_t*>(&rdx8);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
    nmerge = *reinterpret_cast<uint32_t*>(&rdx8);
    if (rax10 != rdx8) 
        goto 0x4a0f;
    if (*reinterpret_cast<uint32_t*>(&rax10) <= 1) 
        goto 0x67ec;
    if (*reinterpret_cast<uint32_t*>(&rax10) <= r9d5) 
        goto 0x4440; else 
        goto "???";
}

struct s122 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6f40() {
    struct s122* rsi1;
    int64_t r10_2;

    rsi1->f28 = r10_2;
    goto 0x6ef0;
}

struct s123 {
    signed char[54] pad54;
    signed char f36;
};

void fun_6fa0() {
    struct s123* rsi1;

    rsi1->f36 = 1;
    goto 0x6ef0;
}

void fun_1008c() {
    goto 0x10027;
}

void fun_1045a() {
    goto 0xff7d;
}

void fun_13000() {
    fun_3920();
    goto fun_3db0;
}

struct s124 {
    signed char[32] pad32;
    int64_t f20;
};

void fun_6f50() {
    struct s124* rsi1;
    int64_t r9_2;

    rsi1->f20 = r9_2;
    goto 0x6ef0;
}

void fun_fd5d() {
    goto 0xf9a2;
}

void fun_10098() {
    goto 0x10027;
}

void fun_10467() {
    goto 0xffce;
}

void fun_13040() {
    fun_3920();
    goto fun_3db0;
}

struct s125 {
    signed char[48] pad48;
    signed char f30;
    signed char f31;
};

void fun_6f60(int32_t edi, struct s125* rsi, uint32_t edx) {
    if (!edi) {
        rsi->f30 = 1;
    }
    if (edx > 1) 
        goto 0x6ef0;
    rsi->f31 = 1;
    goto 0x6ef0;
}

void fun_fd8a() {
    goto 0xf9a2;
}

void fun_100a4() {
    goto 0xffa0;
}

void fun_13080() {
    fun_3920();
    goto fun_3db0;
}

void fun_fdac() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x10740;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0xfc71;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0xfc71;
    }
    if (v11) 
        goto 0x10aa3;
    if (r10_12 > r15_13) 
        goto addr_10af3_8;
    addr_10af8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x10831;
    addr_10af3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_10af8_9;
}

struct s126 {
    signed char[24] pad24;
    int64_t f18;
};

struct s127 {
    signed char[16] pad16;
    void** f10;
};

struct s128 {
    signed char[8] pad8;
    void** f8;
};

void fun_130d0() {
    int64_t r15_1;
    struct s126* rbx2;
    void** r14_3;
    struct s127* rbx4;
    void** r13_5;
    struct s128* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    void** v11;
    void** v12;
    void** v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_3920();
    fun_3db0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x130f2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_13128() {
    fun_3920();
    goto 0x130f9;
}

struct s129 {
    signed char[32] pad32;
    void** f20;
};

struct s130 {
    signed char[24] pad24;
    int64_t f18;
};

struct s131 {
    signed char[16] pad16;
    void** f10;
};

struct s132 {
    signed char[8] pad8;
    void** f8;
};

struct s133 {
    signed char[40] pad40;
    void** f28;
};

void fun_13160() {
    void** rcx1;
    struct s129* rbx2;
    int64_t r15_3;
    struct s130* rbx4;
    void** r14_5;
    struct s131* rbx6;
    void** r13_7;
    struct s132* rbx8;
    void** r12_9;
    void*** rbx10;
    void** v11;
    struct s133* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_3920();
    fun_3db0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x13194, __return_address(), rcx1);
    goto v15;
}

void fun_131d8() {
    fun_3920();
    goto 0x1319b;
}
