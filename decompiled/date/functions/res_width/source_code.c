res_width (long int res)
{
  int i = 9;
  for (long long int r = 1; (r *= 10) <= res; )
    i--;
  return i;
}