batch_convert (char const *input_filename, char const *format,
               timezone_t tz, char const *tzstring)
{
  bool ok;
  FILE *in_stream;
  char *line;
  size_t buflen;
  struct timespec when;

  if (STREQ (input_filename, "-"))
    {
      input_filename = _("standard input");
      in_stream = stdin;
    }
  else
    {
      in_stream = fopen (input_filename, "r");
      if (in_stream == NULL)
        {
          die (EXIT_FAILURE, errno, "%s", quotef (input_filename));
        }
    }

  line = NULL;
  buflen = 0;
  ok = true;
  while (true)
    {
      ssize_t line_length = getline (&line, &buflen, in_stream);
      if (line_length < 0)
        {
          /* FIXME: detect/handle error here.  */
          break;
        }

      if (! parse_datetime2 (&when, line, NULL,
                             parse_datetime_flags, tz, tzstring))
        {
          if (line[line_length - 1] == '\n')
            line[line_length - 1] = '\0';
          error (0, 0, _("invalid date %s"), quote (line));
          ok = false;
        }
      else
        {
          ok &= show_date (format, when, tz);
        }
    }

  if (fclose (in_stream) == EOF)
    die (EXIT_FAILURE, errno, "%s", quotef (input_filename));

  free (line);

  return ok;
}