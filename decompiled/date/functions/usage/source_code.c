usage (int status)
{
  if (status != EXIT_SUCCESS)
    emit_try_help ();
  else
    {
      printf (_("\
Usage: %s [OPTION]... [+FORMAT]\n\
  or:  %s [-u|--utc|--universal] [MMDDhhmm[[CC]YY][.ss]]\n\
"),
              program_name, program_name);
      fputs (_("\
Display date and time in the given FORMAT.\n\
With -s, or with [MMDDhhmm[[CC]YY][.ss]], set the date and time.\n\
"), stdout);

      emit_mandatory_arg_note ();

      fputs (_("\
  -d, --date=STRING          display time described by STRING, not 'now'\n\
"), stdout);
      fputs (_("\
      --debug                annotate the parsed date,\n\
                              and warn about questionable usage to stderr\n\
"), stdout);
      fputs (_("\
  -f, --file=DATEFILE        like --date; once for each line of DATEFILE\n\
"), stdout);
      fputs (_("\
  -I[FMT], --iso-8601[=FMT]  output date/time in ISO 8601 format.\n\
                               FMT='date' for date only (the default),\n\
                               'hours', 'minutes', 'seconds', or 'ns'\n\
                               for date and time to the indicated precision.\n\
                               Example: 2006-08-14T02:34:56-06:00\n\
"), stdout);
      fputs (_("\
  --resolution               output the available resolution of timestamps\n\
                               Example: 0.000000001\n\
"), stdout);
      fputs (_("\
  -R, --rfc-email            output date and time in RFC 5322 format.\n\
                               Example: Mon, 14 Aug 2006 02:34:56 -0600\n\
"), stdout);
      fputs (_("\
      --rfc-3339=FMT         output date/time in RFC 3339 format.\n\
                               FMT='date', 'seconds', or 'ns'\n\
                               for date and time to the indicated precision.\n\
                               Example: 2006-08-14 02:34:56-06:00\n\
"), stdout);
      fputs (_("\
  -r, --reference=FILE       display the last modification time of FILE\n\
"), stdout);
      fputs (_("\
  -s, --set=STRING           set time described by STRING\n\
  -u, --utc, --universal     print or set Coordinated Universal Time (UTC)\n\
"), stdout);
      fputs (HELP_OPTION_DESCRIPTION, stdout);
      fputs (VERSION_OPTION_DESCRIPTION, stdout);
      fputs (_("\
\n\
FORMAT controls the output.  Interpreted sequences are:\n\
\n\
  %%   a literal %\n\
  %a   locale's abbreviated weekday name (e.g., Sun)\n\
"), stdout);
      fputs (_("\
  %A   locale's full weekday name (e.g., Sunday)\n\
  %b   locale's abbreviated month name (e.g., Jan)\n\
  %B   locale's full month name (e.g., January)\n\
  %c   locale's date and time (e.g., Thu Mar  3 23:05:25 2005)\n\
"), stdout);
      fputs (_("\
  %C   century; like %Y, except omit last two digits (e.g., 20)\n\
  %d   day of month (e.g., 01)\n\
  %D   date; same as %m/%d/%y\n\
  %e   day of month, space padded; same as %_d\n\
"), stdout);
      fputs (_("\
  %F   full date; like %+4Y-%m-%d\n\
  %g   last two digits of year of ISO week number (see %G)\n\
  %G   year of ISO week number (see %V); normally useful only with %V\n\
"), stdout);
      fputs (_("\
  %h   same as %b\n\
  %H   hour (00..23)\n\
  %I   hour (01..12)\n\
  %j   day of year (001..366)\n\
"), stdout);
      fputs (_("\
  %k   hour, space padded ( 0..23); same as %_H\n\
  %l   hour, space padded ( 1..12); same as %_I\n\
  %m   month (01..12)\n\
  %M   minute (00..59)\n\
"), stdout);
      fputs (_("\
  %n   a newline\n\
  %N   nanoseconds (000000000..999999999)\n\
  %p   locale's equivalent of either AM or PM; blank if not known\n\
  %P   like %p, but lower case\n\
  %q   quarter of year (1..4)\n\
  %r   locale's 12-hour clock time (e.g., 11:11:04 PM)\n\
  %R   24-hour hour and minute; same as %H:%M\n\
  %s   seconds since the Epoch (1970-01-01 00:00 UTC)\n\
"), stdout);
      fputs (_("\
  %S   second (00..60)\n\
  %t   a tab\n\
  %T   time; same as %H:%M:%S\n\
  %u   day of week (1..7); 1 is Monday\n\
"), stdout);
      fputs (_("\
  %U   week number of year, with Sunday as first day of week (00..53)\n\
  %V   ISO week number, with Monday as first day of week (01..53)\n\
  %w   day of week (0..6); 0 is Sunday\n\
  %W   week number of year, with Monday as first day of week (00..53)\n\
"), stdout);
      fputs (_("\
  %x   locale's date representation (e.g., 12/31/99)\n\
  %X   locale's time representation (e.g., 23:13:48)\n\
  %y   last two digits of year (00..99)\n\
  %Y   year\n\
"), stdout);
      fputs (_("\
  %z   +hhmm numeric time zone (e.g., -0400)\n\
  %:z  +hh:mm numeric time zone (e.g., -04:00)\n\
  %::z  +hh:mm:ss numeric time zone (e.g., -04:00:00)\n\
  %:::z  numeric time zone with : to necessary precision (e.g., -04, +05:30)\n\
  %Z   alphabetic time zone abbreviation (e.g., EDT)\n\
\n\
By default, date pads numeric fields with zeroes.\n\
"), stdout);
      fputs (_("\
The following optional flags may follow '%':\n\
\n\
  -  (hyphen) do not pad the field\n\
  _  (underscore) pad with spaces\n\
  0  (zero) pad with zeros\n\
  +  pad with zeros, and put '+' before future years with >4 digits\n\
  ^  use upper case if possible\n\
  #  use opposite case if possible\n\
"), stdout);
      fputs (_("\
\n\
After any flags comes an optional field width, as a decimal number;\n\
then an optional modifier, which is either\n\
E to use the locale's alternate representations if available, or\n\
O to use the locale's alternate numeric symbols if available.\n\
"), stdout);
      fputs (_("\
\n\
Examples:\n\
Convert seconds since the Epoch (1970-01-01 UTC) to a date\n\
  $ date --date='@2147483647'\n\
\n\
Show the time on the west coast of the US (use tzselect(1) to find TZ)\n\
  $ TZ='America/Los_Angeles' date\n\
\n\
Show the local time for 9AM next Friday on the west coast of the US\n\
  $ date --date='TZ=\"America/Los_Angeles\" 09:00 next Fri'\n\
"), stdout);
      emit_ancillary_info (PROGRAM_NAME);
    }
  exit (status);
}