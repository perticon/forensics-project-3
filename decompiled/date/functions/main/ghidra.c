void main(int param_1,undefined8 *param_2)

{
  char cVar1;
  byte bVar2;
  byte bVar3;
  int iVar4;
  long lVar5;
  char *pcVar6;
  ulong uVar7;
  char *pcVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  __ssize_t _Var11;
  long lVar12;
  FILE *__stream;
  undefined8 uVar13;
  int *piVar14;
  undefined8 *puVar15;
  undefined1 *puVar16;
  char *pcVar17;
  uint uVar18;
  long in_FS_OFFSET;
  bool bVar19;
  undefined auVar20 [16];
  char *local_128;
  undefined8 local_120;
  char *local_118;
  char *local_110;
  byte local_102;
  byte local_101;
  char *local_100;
  char *local_f8;
  size_t local_f0;
  long local_e8;
  long local_e0;
  stat local_d8;
  undefined8 local_40;
  
  uVar18 = 0x11afc0;
  puVar16 = short_options;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  local_128 = (char *)0x0;
  local_118 = (char *)0x0;
  local_101 = 0;
  local_102 = 0;
  local_110 = (char *)0x0;
  local_120 = (char *)0x0;
  pcVar8 = (char *)0x0;
LAB_001039a8:
  puVar15 = param_2;
  iVar4 = getopt_long(param_1,param_2,puVar16,uVar18,0);
  if (iVar4 != -1) {
    if (0x82 < iVar4) goto switchD_001039e1_caseD_4a;
    if (iVar4 < 0x49) {
      if (iVar4 == -0x83) {
        version_etc(stdout,"date","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar4 == -0x82) {
        usage(0);
        goto LAB_00103a01;
      }
      goto switchD_001039e1_caseD_4a;
    }
    switch(iVar4) {
    case 0x49:
      uVar7 = 0;
      if (optarg != (char *)0x0) {
        lVar5 = __xargmatch_internal
                          ("--iso-8601",optarg,time_spec_string,time_spec,4,argmatch_die,1,0);
        uVar7 = (ulong)*(uint *)(time_spec + lVar5 * 4);
      }
      pcVar6 = iso_8601_format_0 + uVar7 * 0x20;
      break;
    default:
      goto switchD_001039e1_caseD_4a;
    case 0x52:
      pcVar6 = "%a, %d %b %Y %H:%M:%S %z";
      break;
    case 100:
      local_120 = optarg;
      goto LAB_001039a8;
    case 0x66:
      local_128 = optarg;
      goto LAB_001039a8;
    case 0x72:
      local_118 = optarg;
      goto LAB_001039a8;
    case 0x73:
      local_110 = optarg;
      local_102 = 1;
      goto LAB_001039a8;
    case 0x75:
      goto switchD_001039e1_caseD_75;
    case 0x80:
      parse_datetime_flags = parse_datetime_flags | 1;
      goto LAB_001039a8;
    case 0x81:
      local_101 = 1;
      goto LAB_001039a8;
    case 0x82:
      goto switchD_001039e1_caseD_82;
    }
LAB_00103aaf:
    bVar19 = pcVar8 != (char *)0x0;
    pcVar8 = pcVar6;
    if (bVar19) {
      uVar13 = dcgettext(0,"multiple output formats specified",5);
      error(1,0,uVar13);
      goto LAB_0010410e;
    }
    goto LAB_001039a8;
  }
LAB_00103a01:
  uVar18 = ((((uint)(local_120 != (char *)0x0) - (uint)(local_128 == (char *)0x0)) + 2) -
           (uint)(local_118 == (char *)0x0)) + (uint)local_101;
  if ((int)uVar18 < 2) {
    if ((local_102 & (byte)uVar18) == 0) {
      if (optind < param_1) {
        if (optind + 1 < param_1) {
          puVar16 = (undefined1 *)quote(param_2[(long)optind + 1]);
          pcVar6 = "extra operand %s";
        }
        else {
          if (*(char *)param_2[optind] == '+') {
            if (pcVar8 == (char *)0x0) {
              pcVar8 = (char *)param_2[optind] + 1;
              optind = optind + 1;
              goto LAB_00103c24;
            }
            goto LAB_0010416f;
          }
          if (((uVar18 & 1) == 0) && (local_102 == 0)) goto LAB_00103c1b;
          puVar16 = (undefined1 *)quote();
          pcVar6 = 
          "the argument %s lacks a leading \'+\';\nwhen using an option to specify date(s), any non-option\nargument must be a format string beginning with \'+\'"
          ;
        }
        uVar13 = dcgettext(0,pcVar6,5);
        puVar15 = (undefined8 *)0x0;
        error(0,0,uVar13,puVar16);
        goto switchD_001039e1_caseD_4a;
      }
LAB_00103c1b:
      if (((pcVar8 == (char *)0x0) && (pcVar8 = "%s.%N", local_101 == 0)) &&
         (pcVar8 = nl_langinfo(0x2006c), *pcVar8 == '\0')) {
        pcVar8 = "%a %b %e %H:%M:%S %Z %Y";
      }
LAB_00103c24:
      local_100 = (char *)0x0;
      pcVar6 = pcVar8;
      while (*pcVar6 != '\0') {
        pcVar17 = pcVar6;
        if (*pcVar6 == '%') {
          if ((pcVar6[1] == '-') && (pcVar6[2] == 'N')) {
            if (local_100 == (char *)0x0) {
              local_100 = (char *)xstrdup(pcVar8);
            }
            lVar12 = gettime_res();
            cVar1 = '\t';
            lVar5 = 1;
            while (SBORROW8(lVar12,lVar5 * 10) == lVar12 + lVar5 * -10 < 0) {
              cVar1 = cVar1 + -1;
              lVar5 = lVar5 * 10;
            }
            pcVar17 = pcVar6 + 2;
            local_100[(long)(pcVar6 + (1 - (long)pcVar8))] = cVar1 + '0';
          }
          else {
            pcVar17 = pcVar6 + (pcVar6[1] == '%');
          }
        }
        pcVar6 = pcVar17 + 1;
      }
      if (local_100 != (char *)0x0) {
        pcVar8 = local_100;
      }
      local_100 = pcVar8;
      pcVar8 = getenv("TZ");
      uVar13 = tzalloc(pcVar8);
      if (local_128 == (char *)0x0) {
        bVar2 = (local_102 ^ 1) & ((byte)uVar18 ^ 1);
        if (bVar2 == 0) {
          if (local_118 == (char *)0x0) {
            if (local_101 == 0) {
              pcVar6 = local_110;
              if (local_110 == (char *)0x0) {
                pcVar6 = local_120;
              }
              local_120 = pcVar6;
              cVar1 = parse_datetime2(&local_e8,pcVar6,0,parse_datetime_flags,uVar13,pcVar8);
              if (cVar1 == '\0') goto LAB_00104055;
            }
            else {
              local_e0 = gettime_res();
              local_e8 = local_e0 / 1000000000;
              local_e0 = local_e0 % 1000000000;
            }
          }
          else {
            iVar4 = stat(local_118,&local_d8);
            if (iVar4 != 0) goto LAB_0010413e;
            local_e8 = local_d8.st_mtim.tv_sec;
            local_e0 = local_d8.st_mtim.tv_nsec;
          }
          if (local_102 != 0) {
LAB_00103e90:
            iVar4 = settime(&local_e8);
            if (iVar4 != 0) goto LAB_00104089;
          }
          bVar2 = 1;
        }
        else if (optind < param_1) {
          local_120 = (char *)param_2[optind];
          cVar1 = posixtime(&local_e8,local_120,7);
          local_e0 = 0;
          if (cVar1 != '\0') goto LAB_00103e90;
LAB_00104055:
          uVar13 = quote(local_120);
          uVar9 = dcgettext(0,"invalid date %s",5);
          error(1,0,uVar9,uVar13);
LAB_00104089:
          bVar2 = 0;
          uVar9 = dcgettext(0,"cannot set date",5);
          piVar14 = __errno_location();
          error(0,*piVar14,uVar9);
        }
        else {
          gettime(&local_e8);
        }
        bVar3 = show_date(local_100,local_e8,local_e0,uVar13);
        local_120 = (char *)((ulong)local_120 & 0xffffffffffffff00 | (ulong)(bVar2 & bVar3));
LAB_00103e26:
                    /* WARNING: Subroutine does not return */
        exit((uint)((byte)local_120 ^ 1));
      }
      iVar4 = strcmp(local_128,"-");
      if (iVar4 == 0) {
        local_128 = (char *)dcgettext(0,"standard input",5);
        __stream = stdin;
      }
      else {
        __stream = fopen(local_128,"r");
        if (__stream == (FILE *)0x0) {
          uVar13 = quotearg_n_style_colon(0,3,local_128);
          piVar14 = __errno_location();
          error(1,*piVar14,&DAT_00115968,uVar13);
LAB_00103f4a:
                    /* WARNING: Subroutine does not return */
          xalloc_die();
        }
      }
      local_120 = (char *)CONCAT71(local_120._1_7_,1);
      local_f8 = (char *)0x0;
      local_f0 = 0;
      while (_Var11 = __getdelim(&local_f8,&local_f0,10,__stream), -1 < _Var11) {
        cVar1 = parse_datetime2(&local_e8,local_f8,0,parse_datetime_flags,uVar13,pcVar8);
        if (cVar1 == '\0') {
          if (local_f8[_Var11 + -1] == '\n') {
            local_f8[_Var11 + -1] = '\0';
          }
          local_120 = (char *)((ulong)local_120 & 0xffffffffffffff00);
          uVar9 = quote(local_f8);
          uVar10 = dcgettext(0,"invalid date %s",5);
          error(0,0,uVar10,uVar9);
        }
        else {
          bVar2 = show_date(local_100,local_e8,local_e0,uVar13);
          local_120._0_1_ = (byte)local_120 & bVar2;
        }
        local_120 = (char *)((ulong)local_120 & 0xffffffffffffff00 | (ulong)(byte)local_120);
      }
      iVar4 = rpl_fclose(__stream);
      if (iVar4 != -1) {
        free(local_f8);
        goto LAB_00103e26;
      }
LAB_0010410e:
      uVar13 = quotearg_n_style_colon(0,3,local_128);
      piVar14 = __errno_location();
      error(1,*piVar14,&DAT_00115968,uVar13);
LAB_0010413e:
      uVar13 = quotearg_n_style_colon(0,3,local_118);
      piVar14 = __errno_location();
      error(1,*piVar14,&DAT_00115968,uVar13);
LAB_0010416f:
      uVar13 = dcgettext(0,"multiple output formats specified",5);
      auVar20 = error(1,0,uVar13);
      pcVar8 = local_128;
      local_128 = SUB168(auVar20,0);
      (*(code *)PTR___libc_start_main_0011bfb0)
                (main,pcVar8,&local_120,0,0,SUB168(auVar20 >> 0x40,0),&local_128);
      do {
                    /* WARNING: Do nothing block with infinite loop */
      } while( true );
    }
    pcVar6 = "the options to print and set the time may not be used together";
  }
  else {
    pcVar6 = "the options to specify dates for printing are mutually exclusive";
  }
  uVar13 = dcgettext(0,pcVar6,5);
  puVar15 = (undefined8 *)0x0;
  error(0,0,uVar13);
switchD_001039e1_caseD_4a:
  usage(1);
switchD_001039e1_caseD_82:
  lVar5 = __xargmatch_internal("--rfc-3339",optarg,0x11b1f0,0x1158f8,4,argmatch_die,1,puVar15);
  pcVar6 = rfc_3339_format_1 + (ulong)*(uint *)(time_spec + lVar5 * 4 + 8) * 0x20;
  goto LAB_00103aaf;
switchD_001039e1_caseD_75:
  iVar4 = putenv("TZ=UTC0");
  if (iVar4 != 0) goto LAB_00103f4a;
  goto LAB_001039a8;
}