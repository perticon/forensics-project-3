uint32_t show_date(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r12_7;
    void** rsp8;
    void** v9;
    struct s0* rax10;
    int1_t zf11;
    void** rax12;
    void** r13_13;
    int64_t rax14;
    void** rsi15;
    void** rax16;
    uint32_t eax17;
    int64_t r8_18;
    void** rdi19;
    int64_t r8_20;
    void** rdi21;
    void** rdi22;
    void** rax23;
    void* rdx24;

    r12_7 = rcx;
    rsp8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x70);
    v9 = rdx;
    rax10 = g28;
    zf11 = (*reinterpret_cast<unsigned char*>(&parse_datetime_flags) & 1) == 0;
    if (!zf11) {
        rax12 = quote(rdi, rsi);
        fun_3580();
        rcx = rax12;
        fun_37d0();
        rsp8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp8 - 8) + 8 - 8 + 8 - 8 + 8);
    }
    r13_13 = rsp8 + 16;
    rax14 = localtime_rz(r12_7, rsp8, r13_13, rcx, r8, r9);
    if (!rax14) {
        rsi15 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp8 - 8) + 8 + 80);
        rax16 = imaxtostr(rsi, rsi15, r13_13, rcx);
        quote(rax16, rsi15, rax16, rsi15);
        fun_3580();
        fun_37d0();
        eax17 = 0;
    } else {
        if (rdi == "%a, %d %b %Y %H:%M:%S %z") {
            fun_37a0(2, 2);
            *reinterpret_cast<int32_t*>(&r8_18) = *reinterpret_cast<int32_t*>(&v9);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_18) + 4) = 0;
            rdi19 = stdout;
            fprintftime(rdi19, rdi, r13_13, r12_7, r8_18);
            fun_37a0(2, 2);
        } else {
            *reinterpret_cast<int32_t*>(&r8_20) = *reinterpret_cast<int32_t*>(&v9);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_20) + 4) = 0;
            rdi21 = stdout;
            fprintftime(rdi21, rdi, r13_13, r12_7, r8_20);
        }
        rdi22 = stdout;
        rax23 = *reinterpret_cast<void***>(rdi22 + 40);
        if (reinterpret_cast<unsigned char>(rax23) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi22 + 48))) {
            fun_3600();
            eax17 = 1;
        } else {
            *reinterpret_cast<void***>(rdi22 + 40) = rax23 + 1;
            *reinterpret_cast<void***>(rax23) = reinterpret_cast<void**>(10);
            eax17 = 1;
        }
    }
    rdx24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax10) - reinterpret_cast<int64_t>(g28));
    if (rdx24) {
        fun_35b0();
    } else {
        return eax17;
    }
}