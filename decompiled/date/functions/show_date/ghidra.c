undefined8 show_date(char *param_1,undefined8 param_2,ulong param_3,undefined8 param_4)

{
  char *pcVar1;
  long lVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  long in_FS_OFFSET;
  undefined8 local_88;
  ulong local_80;
  undefined local_78 [64];
  undefined local_38 [24];
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  local_88 = param_2;
  local_80 = param_3;
  if (((byte)parse_datetime_flags & 1) != 0) {
    uVar3 = quote();
    uVar4 = dcgettext(0,"output format: %s",5);
    error(0,0,uVar4,uVar3);
  }
  lVar2 = localtime_rz(param_4,&local_88,local_78);
  if (lVar2 == 0) {
    uVar3 = imaxtostr(local_88,local_38);
    uVar3 = quote(uVar3);
    uVar4 = dcgettext(0,"time %s is out of range",5);
    error(0,0,uVar4,uVar3);
    uVar3 = 0;
  }
  else {
    if (param_1 == "%a, %d %b %Y %H:%M:%S %z") {
      setlocale(2,"C");
      fprintftime(stdout,"%a, %d %b %Y %H:%M:%S %z",local_78,param_4,local_80 & 0xffffffff);
      setlocale(2,"");
    }
    else {
      fprintftime(stdout,param_1,local_78,param_4,local_80 & 0xffffffff);
    }
    pcVar1 = stdout->_IO_write_ptr;
    if (pcVar1 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar1 + 1;
      *pcVar1 = '\n';
      uVar3 = 1;
    }
    else {
      __overflow(stdout,10);
      uVar3 = 1;
    }
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar3;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}