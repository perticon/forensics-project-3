show_date (char const *format, struct timespec when, timezone_t tz)
{
  struct tm tm;

  if (parse_datetime_flags & PARSE_DATETIME_DEBUG)
    error (0, 0, _("output format: %s"), quote (format));

  if (localtime_rz (tz, &when.tv_sec, &tm))
    {
      if (format == rfc_email_format)
        setlocale (LC_TIME, "C");
      fprintftime (stdout, format, &tm, tz, when.tv_nsec);
      if (format == rfc_email_format)
        setlocale (LC_TIME, "");
      fputc ('\n', stdout);
      return true;
    }
  else
    {
      char buf[INT_BUFSIZE_BOUND (intmax_t)];
      error (0, 0, _("time %s is out of range"),
             quote (timetostr (when.tv_sec, buf)));
      return false;
    }
}