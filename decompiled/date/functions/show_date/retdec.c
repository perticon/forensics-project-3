bool show_date(char * format, struct timespec when, int32_t * tz) {
    int64_t v1 = when.e0; // bp-136, 0x429f
    int64_t v2 = __readfsqword(40); // 0x42a8
    if (*(char *)&parse_datetime_flags % 2 != 0) {
        // 0x4350
        quote(format);
        function_3580();
        function_37d0();
    }
    // 0x42c5
    int64_t v3; // 0x4290
    int64_t v4; // bp-120, 0x4290
    if (localtime_rz(tz, &v1, (struct tm *)&v4) == NULL) {
        // 0x4388
        int64_t v5; // bp-56, 0x4290
        char * v6 = imaxtostr(v1, (char *)&v5); // 0x4391
        quote(v6);
        function_3580();
        function_37d0();
        v3 = 0;
    } else {
        int64_t v7; // 0x4290
        if (format == "%a, %d %b %Y %H:%M:%S %z") {
            // 0x43d0
            function_37a0();
            fprintftime(g34, "%a, %d %b %Y %H:%M:%S %z", (struct tm *)&v4, tz, (int32_t)v7);
            function_37a0();
        } else {
            // 0x42f1
            fprintftime(g34, format, (struct tm *)&v4, tz, (int32_t)v7);
        }
        int64_t v8 = (int64_t)g34; // 0x430b
        int64_t * v9 = (int64_t *)(v8 + 40); // 0x4312
        uint64_t v10 = *v9; // 0x4312
        if (v10 >= *(int64_t *)(v8 + 48)) {
            // 0x4418
            function_3600();
            v3 = 1;
        } else {
            // 0x4320
            *v9 = v10 + 1;
            *(char *)v10 = 10;
            v3 = 1;
        }
    }
    // 0x4330
    if (v2 != __readfsqword(40)) {
        // 0x442c
        return function_35b0() % 2 != 0;
    }
    // 0x4344
    return v3 != 0;
}