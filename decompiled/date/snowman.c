
struct s0 {
    signed char f0;
    signed char f1;
    signed char f2;
};

struct s0* g28;

uint32_t parse_datetime_flags = 0;

void** quote(void** rdi, void** rsi, ...);

void** fun_3580();

void fun_37d0();

int64_t localtime_rz(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, ...);

void** imaxtostr(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_37a0(int64_t rdi, ...);

void** stdout = reinterpret_cast<void**>(0);

void fprintftime(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void fun_3600();

void fun_35b0();

uint32_t show_date(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r12_7;
    void** rsp8;
    void** v9;
    struct s0* rax10;
    int1_t zf11;
    void** rax12;
    void** r13_13;
    int64_t rax14;
    void** rsi15;
    void** rax16;
    uint32_t eax17;
    int64_t r8_18;
    void** rdi19;
    int64_t r8_20;
    void** rdi21;
    void** rdi22;
    void** rax23;
    void* rdx24;

    r12_7 = rcx;
    rsp8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x70);
    v9 = rdx;
    rax10 = g28;
    zf11 = (*reinterpret_cast<unsigned char*>(&parse_datetime_flags) & 1) == 0;
    if (!zf11) {
        rax12 = quote(rdi, rsi);
        fun_3580();
        rcx = rax12;
        fun_37d0();
        rsp8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp8 - 8) + 8 - 8 + 8 - 8 + 8);
    }
    r13_13 = rsp8 + 16;
    rax14 = localtime_rz(r12_7, rsp8, r13_13, rcx, r8, r9);
    if (!rax14) {
        rsi15 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp8 - 8) + 8 + 80);
        rax16 = imaxtostr(rsi, rsi15, r13_13, rcx);
        quote(rax16, rsi15, rax16, rsi15);
        fun_3580();
        fun_37d0();
        eax17 = 0;
    } else {
        if (rdi == "%a, %d %b %Y %H:%M:%S %z") {
            fun_37a0(2, 2);
            *reinterpret_cast<int32_t*>(&r8_18) = *reinterpret_cast<int32_t*>(&v9);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_18) + 4) = 0;
            rdi19 = stdout;
            fprintftime(rdi19, rdi, r13_13, r12_7, r8_18);
            fun_37a0(2, 2);
        } else {
            *reinterpret_cast<int32_t*>(&r8_20) = *reinterpret_cast<int32_t*>(&v9);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_20) + 4) = 0;
            rdi21 = stdout;
            fprintftime(rdi21, rdi, r13_13, r12_7, r8_20);
        }
        rdi22 = stdout;
        rax23 = *reinterpret_cast<void***>(rdi22 + 40);
        if (reinterpret_cast<unsigned char>(rax23) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi22 + 48))) {
            fun_3600();
            eax17 = 1;
        } else {
            *reinterpret_cast<void***>(rdi22 + 40) = rax23 + 1;
            *reinterpret_cast<void***>(rax23) = reinterpret_cast<void**>(10);
            eax17 = 1;
        }
    }
    rdx24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax10) - reinterpret_cast<int64_t>(g28));
    if (rdx24) {
        fun_35b0();
    } else {
        return eax17;
    }
}

void*** fun_34e0();

void fun_3650(void** rdi, void** rsi, ...);

struct s1 {
    signed char[1] pad1;
    void** f1;
};

uint64_t fun_3830(void** rdi, ...);

uint32_t** fun_3480();

/* __strftime_internal.isra.0 */
void* __strftime_internal_isra_0(void** rdi, void** rsi, void** rdx, unsigned char cl, uint32_t r8d, int32_t r9d, int64_t a7, int64_t a8, int64_t a9) {
    void** r14_10;
    void** r13_11;
    void** r12_12;
    void** rbx13;
    unsigned char v14;
    struct s0* rax15;
    struct s0* v16;
    void*** rax17;
    void* rsp18;
    void** rdi19;
    void** ecx20;
    void*** v21;
    void** eax22;
    void** v23;
    uint32_t eax24;
    void* r15_25;
    void* rax26;
    int32_t ebp27;
    void** rax28;
    unsigned char* rbp29;
    unsigned char* r13_30;
    void** rdi31;
    uint32_t eax32;
    void** v33;
    int64_t rdx34;
    int32_t ecx35;
    uint32_t esi36;
    uint64_t rax37;
    int64_t rdi38;
    struct s1* rax39;
    struct s1* v40;
    void** rbp41;
    void** v42;
    void** rax43;
    void* r13_44;
    uint32_t** rax45;
    uint32_t** r13_46;
    void** rbp47;
    int64_t rdx48;
    void** rdi49;
    void** v50;
    void* rbx51;
    void* rbx52;
    void* rbp53;
    void** rdi54;
    uint32_t v55;
    uint32_t** rax56;
    unsigned char* rdx57;
    int32_t v58;
    unsigned char* r12_59;
    unsigned char* rbx60;
    uint32_t** rbp61;
    void** r13_62;
    int32_t r14d63;
    int64_t rsi64;
    unsigned char v65;
    void** rdi66;
    void* r12_67;
    int64_t rax68;

    r14_10 = rdi;
    r13_11 = reinterpret_cast<void**>(static_cast<int64_t>(r9d));
    r12_12 = rsi;
    rbx13 = rdx;
    v14 = cl;
    rax15 = g28;
    v16 = rax15;
    rax17 = fun_34e0();
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x4c8 - 8 + 8);
    rdi19 = *reinterpret_cast<void***>(rbx13 + 48);
    ecx20 = *reinterpret_cast<void***>(rbx13 + 8);
    v21 = rax17;
    eax22 = *rax17;
    v23 = eax22;
    if (rdi19) {
    }
    if (reinterpret_cast<signed char>(ecx20) <= reinterpret_cast<signed char>(12)) {
        if (ecx20) {
        }
    }
    eax24 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_12));
    *reinterpret_cast<int32_t*>(&r15_25) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_25) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax24)) 
        goto addr_51b5_9;
    while (1) {
        addr_5212_10:
        *v21 = v23;
        while (rax26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v16) - reinterpret_cast<int64_t>(g28)), !!rax26) {
            fun_35b0();
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
            if (!r8d) {
                r8d = 48;
            }
            if (~reinterpret_cast<uint64_t>(r15_25) <= 0) 
                goto addr_5228_17;
            r15_25 = r15_25;
            if (!r14_10) {
                addr_598c_19:
                if (r8d == 45 || (ebp27 = *reinterpret_cast<int32_t*>(&r13_11) - *reinterpret_cast<int32_t*>(&r12_12), ebp27 < 0)) {
                    addr_521f_20:
                    if (!reinterpret_cast<int1_t>(r15_25 == 0xffffffffffffffff)) {
                        addr_51ff_21:
                        while (eax24 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx13 + 1)), r12_12 = rbx13 + 1, r13_11 = reinterpret_cast<void**>(0xffffffffffffffff), !!*reinterpret_cast<signed char*>(&eax24)) {
                            addr_51b5_9:
                            if (*reinterpret_cast<signed char*>(&eax24) != 37) {
                                *reinterpret_cast<int32_t*>(&rax28) = 0;
                                *reinterpret_cast<int32_t*>(&rax28 + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rbx13) = 1;
                                *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
                                if (*reinterpret_cast<int32_t*>(&r13_11) >= 0) {
                                    rax28 = r13_11;
                                }
                                if (rax28) {
                                    rbx13 = rax28;
                                }
                                if (reinterpret_cast<unsigned char>(rbx13) >= reinterpret_cast<unsigned char>(~reinterpret_cast<uint64_t>(r15_25))) 
                                    goto addr_5228_17;
                                if (r14_10) {
                                    if (*reinterpret_cast<int32_t*>(&r13_11) > 1) {
                                        rbp29 = reinterpret_cast<unsigned char*>(rax28 + 0xffffffffffffffff);
                                        *reinterpret_cast<int32_t*>(&r13_30) = 0;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_30) + 4) = 0;
                                        do {
                                            ++r13_30;
                                            fun_3650(32, r14_10, 32, r14_10);
                                            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                        } while (reinterpret_cast<uint64_t>(rbp29) > reinterpret_cast<uint64_t>(r13_30));
                                    }
                                    *reinterpret_cast<int32_t*>(&rdi31) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_12));
                                    *reinterpret_cast<int32_t*>(&rdi31 + 4) = 0;
                                    fun_3650(rdi31, r14_10, rdi31, r14_10);
                                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                }
                                r15_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_25) + reinterpret_cast<unsigned char>(rbx13));
                                rbx13 = r12_12;
                                continue;
                            }
                            eax32 = v14;
                            rbx13 = r12_12;
                            r8d = 0;
                            *reinterpret_cast<signed char*>(&v33) = *reinterpret_cast<signed char*>(&eax32);
                            while (*reinterpret_cast<uint32_t*>(&rdx34) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx13 + 1)))), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx34) + 4) = 0, ++rbx13, ecx35 = static_cast<int32_t>(rdx34 - 35), esi36 = *reinterpret_cast<uint32_t*>(&rdx34), *reinterpret_cast<unsigned char*>(&ecx35) <= 60) {
                                rax37 = 1 << *reinterpret_cast<unsigned char*>(&ecx35);
                                if (rax37 & 0x1000000000002500) {
                                    r8d = *reinterpret_cast<uint32_t*>(&rdx34);
                                } else {
                                    if (*reinterpret_cast<unsigned char*>(&ecx35) == 59) {
                                        *reinterpret_cast<signed char*>(&v33) = 1;
                                    } else {
                                        if (!(*reinterpret_cast<uint32_t*>(&rax37) & 1)) 
                                            break;
                                    }
                                }
                            }
                            if (*reinterpret_cast<uint32_t*>(&rdx34) - 48 > 9) 
                                goto addr_530e_43;
                            *reinterpret_cast<int32_t*>(&r13_11) = 0;
                            do {
                                if (__intrinsic() || (*reinterpret_cast<int32_t*>(&r13_11) = *reinterpret_cast<int32_t*>(&r13_11) * 10 + reinterpret_cast<int32_t>(*reinterpret_cast<void***>(rbx13) - 48), __intrinsic())) {
                                    *reinterpret_cast<int32_t*>(&r13_11) = 0x7fffffff;
                                }
                                *reinterpret_cast<uint32_t*>(&rdi38) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx13 + 1))));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi38) + 4) = 0;
                                ++rbx13;
                                esi36 = *reinterpret_cast<uint32_t*>(&rdi38);
                            } while (static_cast<uint32_t>(rdi38 - 48) <= 9);
                            addr_530e_43:
                            if (*reinterpret_cast<unsigned char*>(&esi36) == 69 || *reinterpret_cast<unsigned char*>(&esi36) == 79) {
                                esi36 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx13 + 1));
                                ++rbx13;
                            }
                            if (*reinterpret_cast<unsigned char*>(&esi36) <= 0x7a) 
                                goto addr_532a_51;
                            rax39 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(rbx13) - reinterpret_cast<unsigned char>(r12_12));
                            v40 = rax39;
                            rbp41 = reinterpret_cast<void**>(&rax39->f1);
                            if (r8d == 45 || *reinterpret_cast<int32_t*>(&r13_11) < 0) {
                                v42 = rbp41;
                                *reinterpret_cast<int32_t*>(&r13_11) = 0;
                                *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
                            } else {
                                r13_11 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r13_11)));
                                rax43 = r13_11;
                                if (reinterpret_cast<unsigned char>(rbp41) >= reinterpret_cast<unsigned char>(r13_11)) {
                                    rax43 = rbp41;
                                }
                                v42 = rax43;
                            }
                            if (reinterpret_cast<unsigned char>(~reinterpret_cast<uint64_t>(r15_25)) <= reinterpret_cast<unsigned char>(v42)) 
                                goto addr_5228_17;
                            if (!r14_10) {
                                addr_542d_59:
                                r15_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_25) + reinterpret_cast<unsigned char>(v42));
                                continue;
                            } else {
                                if (reinterpret_cast<unsigned char>(rbp41) >= reinterpret_cast<unsigned char>(r13_11)) 
                                    goto addr_53eb_61;
                                r13_44 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r13_11) - reinterpret_cast<unsigned char>(rbp41));
                                if (r8d == 48) 
                                    goto addr_65d9_63;
                                if (r8d != 43) 
                                    goto addr_53be_65;
                            }
                            addr_65d9_63:
                            if (!r13_44) {
                                addr_53eb_61:
                                if (!*reinterpret_cast<signed char*>(&v33)) {
                                    fun_3830(r12_12, r12_12);
                                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                    goto addr_542d_59;
                                } else {
                                    if (rbp41) {
                                        rax45 = fun_3480();
                                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                        r13_46 = rax45;
                                        rbp47 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_12) + reinterpret_cast<uint64_t>(v40) + 1);
                                        do {
                                            *reinterpret_cast<uint32_t*>(&rdx48) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_12));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx48) + 4) = 0;
                                            ++r12_12;
                                            *reinterpret_cast<uint32_t*>(&rdi49) = (*r13_46)[rdx48];
                                            *reinterpret_cast<int32_t*>(&rdi49 + 4) = 0;
                                            fun_3650(rdi49, r14_10);
                                            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                        } while (r12_12 != rbp47);
                                        goto addr_542d_59;
                                    }
                                }
                            } else {
                                v50 = rbx13;
                                rbx51 = reinterpret_cast<void*>(0);
                                do {
                                    rbx51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx51) + 1);
                                    fun_3650(48, r14_10);
                                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                                } while (r13_44 != rbx51);
                            }
                            addr_53e6_73:
                            rbx13 = v50;
                            goto addr_53eb_61;
                            addr_53be_65:
                            if (!r13_44) 
                                goto addr_53eb_61;
                            v50 = rbx13;
                            rbx52 = reinterpret_cast<void*>(0);
                            do {
                                rbx52 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx52) + 1);
                                fun_3650(32, r14_10);
                                rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                            } while (r13_44 != rbx52);
                            goto addr_53e6_73;
                        }
                        goto addr_5212_10;
                    } else {
                        goto addr_5228_17;
                    }
                } else {
                    rbp53 = reinterpret_cast<void*>(static_cast<int64_t>(ebp27));
                    if (reinterpret_cast<uint64_t>(rbp53) >= ~reinterpret_cast<uint64_t>(r15_25)) {
                        addr_5228_17:
                        *v21 = reinterpret_cast<void**>(34);
                    } else {
                        if (!r14_10) {
                            r15_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_25) + reinterpret_cast<uint64_t>(rbp53));
                            goto addr_51ff_21;
                        }
                    }
                }
            } else {
                if (!*reinterpret_cast<signed char*>(&v33)) {
                    *reinterpret_cast<uint32_t*>(&v33) = r8d;
                    rdi54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp18) + 0xb0);
                    fun_3830(rdi54, rdi54);
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    r8d = *reinterpret_cast<uint32_t*>(&v33);
                    goto addr_598c_19;
                } else {
                    if (1) {
                        if (r8d == 45) 
                            goto addr_521f_20;
                        rbp53 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r13_11) - *reinterpret_cast<int32_t*>(&r12_12)));
                        if (~reinterpret_cast<uint64_t>(r15_25) <= reinterpret_cast<uint64_t>(rbp53)) {
                            goto addr_5228_17;
                        }
                    } else {
                        v55 = r8d;
                        rax56 = fun_3480();
                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                        rdx57 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp18) + 0xb0);
                        v33 = rbx13;
                        v58 = *reinterpret_cast<int32_t*>(&r12_12);
                        r12_59 = rdx57;
                        rbx60 = rdx57;
                        rbp61 = rax56;
                        r13_62 = r14_10;
                        r14d63 = *reinterpret_cast<int32_t*>(&r13_11);
                        do {
                            *reinterpret_cast<uint32_t*>(&rsi64) = v65;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi64) + 4) = 0;
                            ++r12_59;
                            *reinterpret_cast<uint32_t*>(&rdi66) = (*rbp61)[rsi64];
                            *reinterpret_cast<int32_t*>(&rdi66 + 4) = 0;
                            fun_3650(rdi66, r13_62);
                            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                        } while (rbx60 != r12_59);
                        rbx13 = v33;
                        *reinterpret_cast<int32_t*>(&r12_12) = v58;
                        r14_10 = r13_62;
                        r8d = v55;
                        *reinterpret_cast<int32_t*>(&r13_11) = r14d63;
                        goto addr_598c_19;
                    }
                }
            }
            *reinterpret_cast<int32_t*>(&r15_25) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_25) + 4) = 0;
            continue;
            if (rbp53) {
                if (r8d == 48 || (*reinterpret_cast<int32_t*>(&r12_67) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_67) + 4) = 0, r8d == 43)) {
                    *reinterpret_cast<int32_t*>(&r12_67) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_67) + 4) = 0;
                    do {
                        r12_67 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_67) + 1);
                        fun_3650(48, r14_10, 48, r14_10);
                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    } while (r12_67 != rbp53);
                } else {
                    do {
                        r12_67 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_67) + 1);
                        fun_3650(32, r14_10, 32, r14_10);
                        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8);
                    } while (r12_67 != rbp53);
                }
                r15_25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_25) + reinterpret_cast<uint64_t>(r12_67));
                goto addr_51ff_21;
            }
        }
        break;
    }
    return r15_25;
    addr_532a_51:
    *reinterpret_cast<uint32_t*>(&rax68) = *reinterpret_cast<unsigned char*>(&esi36);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax68) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x15998 + rax68 * 4) + 0x15998;
}

struct s2 {
    signed char[88423] pad88423;
    void** f15967;
};

uint32_t fun_35f0(void** rdi, int64_t rsi, int64_t rdx);

uint32_t fun_34a0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...);

/* str_days.constprop.0 */
void** str_days_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, ...) {
    int64_t rdx6;
    void** rdi7;
    struct s2* rcx8;
    void** rsi9;
    uint32_t eax10;
    void** r8_11;

    if (!*reinterpret_cast<signed char*>(rdi + 0xe8)) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        rdx6 = *reinterpret_cast<int32_t*>(rdi + 16);
        if (*reinterpret_cast<uint32_t*>(&rdx6) > 6) {
            addr_751c_3:
            return rsi;
        } else {
            rdi7 = rsi;
            *reinterpret_cast<int32_t*>(&rcx8) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi9) = 100;
            *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 8) + 1) <= reinterpret_cast<unsigned char>(13)) {
            eax10 = fun_35f0(rsi, 100, "%s");
        } else {
            eax10 = fun_34a0(rsi, 100, 1, 0xffffffffffffffff, "%ld");
        }
        rdx6 = *reinterpret_cast<int32_t*>(rdi + 16);
        if (*reinterpret_cast<uint32_t*>(&rdx6) > 6) 
            goto addr_751c_3;
        if (eax10 > 99) 
            goto addr_751c_3;
        *reinterpret_cast<int32_t*>(&rcx8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rcx8) = reinterpret_cast<uint1_t>(eax10 == 0);
        rsi9 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(100 - eax10)));
        rdi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax10))));
    }
    r8_11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(" %s") + reinterpret_cast<uint64_t>(rcx8));
    fun_34a0(rdi7, rsi9, 1, 0xffffffffffffffff, r8_11, rdi7, rsi9, 1, 0xffffffffffffffff, r8_11);
    return rsi;
}

void dbg_printf(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, ...);

void** stderr = reinterpret_cast<void**>(0);

void fun_3840(void** rdi, ...);

struct s3 {
    signed char[89034] pad89034;
    void** f15bca;
};

struct s4 {
    signed char[89124] pad89124;
    void** f15c24;
};

void** time_zone_str(void** edi, void** rsi, ...);

/* debug_print_current_time.part.0 */
void debug_print_current_time_part_0(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    struct s0* rax8;
    void** rax9;
    void** rsi10;
    void** rdi11;
    void** v12;
    void** rsp13;
    void** rdx14;
    uint32_t eax15;
    void** rsi16;
    uint32_t eax17;
    void** rsi18;
    void** rbp19;
    void** rax20;
    int1_t zf21;
    void** r12_22;
    uint32_t eax23;
    void** r12_24;
    void** rsi25;
    void** rax26;
    uint32_t edx27;
    struct s3* r12_28;
    void** r12_29;
    uint32_t edx30;
    struct s4* rdx31;
    int1_t zf32;
    void** edi33;
    void** rax34;
    int1_t zf35;
    void* rax36;
    void** rdi37;
    uint32_t edx38;
    void* rdi39;
    uint32_t ecx40;
    void*** rdi41;
    void** rcx42;
    void** rdx43;
    void*** rax44;
    void*** rcx45;
    int64_t v46;

    rax8 = g28;
    rax9 = fun_3580();
    rsi10 = rdi;
    rdi11 = rax9;
    dbg_printf(rdi11, rsi10, 5, rcx, r8, r9, v12);
    rsp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x78 - 8 + 8 - 8 + 8);
    if (*reinterpret_cast<int64_t*>(rsi + 0xa8) && !*reinterpret_cast<signed char*>(rsi + 0xe2)) {
        rcx = *reinterpret_cast<void***>(rsi + 40);
        *reinterpret_cast<int32_t*>(&rsi10) = 1;
        r8 = *reinterpret_cast<void***>(rsi + 56);
        rdi11 = stderr;
        fun_3840(rdi11);
        rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
        *reinterpret_cast<signed char*>(rsi + 0xe2) = 1;
        *reinterpret_cast<uint32_t*>(&rdx14) = 1;
        *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
        eax15 = *reinterpret_cast<unsigned char*>(rsi + 0xe7);
        if (*reinterpret_cast<unsigned char*>(rsi + 0xe0) == *reinterpret_cast<unsigned char*>(&eax15)) 
            goto addr_767a_3;
        rsi16 = stderr;
        fun_3650(32, rsi16);
        rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
        goto addr_7638_5;
    }
    eax17 = *reinterpret_cast<unsigned char*>(rsi + 0xe7);
    *reinterpret_cast<uint32_t*>(&rdx14) = 0;
    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
    if (*reinterpret_cast<unsigned char*>(rsi + 0xe0) == *reinterpret_cast<unsigned char*>(&eax17)) {
        addr_767a_3:
        if (!*reinterpret_cast<int64_t*>(rsi + 0xd0) || *reinterpret_cast<signed char*>(rsi + 0xe5)) {
            if (*reinterpret_cast<int64_t*>(rsi + 0xb0) && !*reinterpret_cast<unsigned char*>(rsi + 0xe3)) {
                if (*reinterpret_cast<signed char*>(&rdx14)) {
                    addr_78f7_9:
                    rsi18 = stderr;
                    fun_3650(32, rsi18);
                    rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
                    goto addr_76ac_10;
                } else {
                    addr_76ac_10:
                    rbp19 = *reinterpret_cast<void***>(rsi + 8);
                    rax20 = str_days_constprop_0(rsi, rsp13, rdx14, rcx, r8, rsi, rsp13, rdx14, rcx, r8);
                    fun_3580();
                    r8 = rbp19;
                    rcx = rax20;
                    rdi11 = stderr;
                    *reinterpret_cast<int32_t*>(&rsi10) = 1;
                    fun_3840(rdi11);
                    rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8 - 8 + 8 - 8 + 8);
                    *reinterpret_cast<unsigned char*>(rsi + 0xe3) = 1;
                    *reinterpret_cast<uint32_t*>(&rdx14) = 1;
                    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
                }
            }
        } else {
            *reinterpret_cast<int32_t*>(&rsi10) = 1;
            r8 = *reinterpret_cast<void***>(rsi + 80);
            rdi11 = stderr;
            fun_3840(rdi11);
            rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
            rcx = *reinterpret_cast<void***>(rsi + 96);
            if (rcx) {
                rdi11 = stderr;
                *reinterpret_cast<int32_t*>(&rsi10) = 1;
                fun_3840(rdi11);
                rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
            }
            if (*reinterpret_cast<int32_t*>(rsi + 28) == 1) {
                rcx = stderr;
                *reinterpret_cast<int32_t*>(&rsi10) = 1;
                rdi11 = reinterpret_cast<void**>("pm");
                fun_3830("pm");
                rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
            }
            zf21 = *reinterpret_cast<int64_t*>(rsi + 0xb0) == 0;
            *reinterpret_cast<signed char*>(rsi + 0xe5) = 1;
            *reinterpret_cast<uint32_t*>(&rdx14) = 1;
            *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
            if (zf21) 
                goto addr_7700_16;
            *reinterpret_cast<uint32_t*>(&rdx14) = *reinterpret_cast<unsigned char*>(rsi + 0xe3);
            *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
            if (!*reinterpret_cast<signed char*>(&rdx14)) 
                goto addr_78f7_9;
        }
    } else {
        addr_7638_5:
        r12_22 = *reinterpret_cast<void***>(rsi + 40);
        fun_3580();
        rdi11 = stderr;
        *reinterpret_cast<int32_t*>(&rsi10) = 1;
        rcx = r12_22;
        fun_3840(rdi11);
        rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8 - 8 + 8);
        eax23 = *reinterpret_cast<unsigned char*>(rsi + 0xe0);
        *reinterpret_cast<uint32_t*>(&rdx14) = 1;
        *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
        *reinterpret_cast<unsigned char*>(rsi + 0xe7) = *reinterpret_cast<unsigned char*>(&eax23);
        goto addr_767a_3;
    }
    addr_7700_16:
    if (!*reinterpret_cast<int64_t*>(rsi + 0xc0) || *reinterpret_cast<signed char*>(rsi + 0xe4)) {
        if (!*reinterpret_cast<int64_t*>(rsi + 0xd8) || *reinterpret_cast<signed char*>(rsi + 0xe6)) {
            if (*reinterpret_cast<signed char*>(rsi + 0xa0)) {
                r12_24 = *reinterpret_cast<void***>(rsi + 88);
                if (*reinterpret_cast<signed char*>(&rdx14)) {
                    addr_7786_21:
                    rsi25 = stderr;
                    fun_3650(32, rsi25);
                    goto addr_77c8_22;
                } else {
                    addr_77c8_22:
                    rax26 = fun_3580();
                    rdi11 = stderr;
                    rcx = r12_24;
                    *reinterpret_cast<int32_t*>(&rsi10) = 1;
                    rdx14 = rax26;
                    fun_3840(rdi11);
                    goto addr_77f4_23;
                }
            }
        } else {
            edx27 = *reinterpret_cast<uint32_t*>(&rdx14) ^ 1;
            *reinterpret_cast<uint32_t*>(&r12_28) = *reinterpret_cast<unsigned char*>(&edx27);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_28) + 4) = 0;
            r12_29 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r12_28) + reinterpret_cast<uint64_t>(" UTC%s"));
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(rsi + 20);
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        r8 = reinterpret_cast<void**>(" DST");
        if (!*reinterpret_cast<int64_t*>(rsi + 0xc8)) {
            r8 = reinterpret_cast<void**>(0x15e39);
        }
        edx30 = *reinterpret_cast<uint32_t*>(&rdx14) ^ 1;
        rdi11 = stderr;
        *reinterpret_cast<uint32_t*>(&rdx31) = *reinterpret_cast<unsigned char*>(&edx30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx31) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rsi10) = 1;
        rdx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx31) + reinterpret_cast<uint64_t>(" isdst=%d%s"));
        fun_3840(rdi11);
        rsp13 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp13 - 8) + 8);
        zf32 = *reinterpret_cast<int64_t*>(rsi + 0xd8) == 0;
        *reinterpret_cast<signed char*>(rsi + 0xe4) = 1;
        if (zf32) 
            goto addr_7779_28;
        if (!*reinterpret_cast<signed char*>(rsi + 0xe6)) 
            goto addr_7980_30; else 
            goto addr_7779_28;
    }
    addr_7921_31:
    edi33 = *reinterpret_cast<void***>(rsi + 24);
    rax34 = time_zone_str(edi33, rsp13, edi33, rsp13);
    rdi11 = stderr;
    rdx14 = r12_29;
    *reinterpret_cast<int32_t*>(&rsi10) = 1;
    rcx = rax34;
    fun_3840(rdi11);
    zf35 = *reinterpret_cast<signed char*>(rsi + 0xa0) == 0;
    *reinterpret_cast<signed char*>(rsi + 0xe6) = 1;
    if (zf35) {
        addr_77f4_23:
        rax36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax8) - reinterpret_cast<int64_t>(g28));
        if (!rax36) {
        }
    } else {
        goto addr_7782_35;
    }
    fun_35b0();
    rdi37 = rdx14;
    if (reinterpret_cast<signed char>(rcx) <= reinterpret_cast<signed char>(2)) 
        goto addr_79cc_38;
    if (reinterpret_cast<signed char>(r8) >= reinterpret_cast<signed char>(0)) {
        addr_79d5_40:
        edx38 = 0;
        rdi39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdi37) * 60);
        *reinterpret_cast<unsigned char*>(&edx38) = __intrinsic();
        if (*reinterpret_cast<signed char*>(&rsi10)) {
            ecx40 = 0;
            rdi41 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi39) - reinterpret_cast<unsigned char>(r8));
            *reinterpret_cast<unsigned char*>(&ecx40) = __intrinsic();
        } else {
            ecx40 = 0;
            rdi41 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rdi39) + reinterpret_cast<unsigned char>(r8));
            *reinterpret_cast<unsigned char*>(&ecx40) = __intrinsic();
        }
    } else {
        rcx42 = rdi37;
        rdx43 = reinterpret_cast<void**>((reinterpret_cast<int64_t>(__intrinsic()) + reinterpret_cast<unsigned char>(rdi37) >> 6) - reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rdi37) >> 63));
        rax44 = reinterpret_cast<void***>(rdx43 + reinterpret_cast<unsigned char>(rdx43) * 4);
        rdi37 = rdx43;
        rcx45 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rcx42) - (reinterpret_cast<uint64_t>(rax44 + reinterpret_cast<uint64_t>(rax44) * 4) << 2));
        goto addr_7a62_44;
    }
    if (edx38 | ecx40) {
        addr_7a0f_46:
        goto v46;
    } else {
        addr_79f1_47:
        if (reinterpret_cast<uint64_t>(rdi41 + 0x5a0) <= 0xb40) {
            *reinterpret_cast<void***>(rdi11 + 24) = reinterpret_cast<void**>(*reinterpret_cast<int32_t*>(&rdi41) * 60);
            goto addr_7a0f_46;
        }
    }
    addr_7a62_44:
    rdi41 = rcx45 + (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdi37) << 4) - reinterpret_cast<unsigned char>(rdi37)) * 4;
    goto addr_79f1_47;
    addr_79cc_38:
    if (reinterpret_cast<signed char>(r8) >= reinterpret_cast<signed char>(0)) 
        goto addr_79d5_40;
    *reinterpret_cast<int32_t*>(&rcx45) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx45) + 4) = 0;
    goto addr_7a62_44;
    addr_7782_35:
    r12_24 = *reinterpret_cast<void***>(rsi + 88);
    goto addr_7786_21;
    addr_7779_28:
    if (!*reinterpret_cast<signed char*>(rsi + 0xa0)) 
        goto addr_77f4_23; else 
        goto addr_7782_35;
    addr_7980_30:
    r12_29 = reinterpret_cast<void**>(" UTC%s");
    goto addr_7921_31;
}

struct s5 {
    signed char[104] pad104;
    int64_t f68;
    int64_t f70;
    int64_t f78;
    int64_t f80;
    int64_t f88;
    int64_t f90;
    int32_t f98;
};

/* debug_print_relative_time.part.0 */
void debug_print_relative_time_part_0(void** rdi, struct s5* rsi, int64_t rdx, void** rcx, void** r8, void** r9, int64_t a7) {
    void** rax8;
    void** v9;
    void** rdi10;
    void** rbp11;

    rax8 = fun_3580();
    dbg_printf(rax8, rdi, 5, rcx, r8, r9, v9);
    if (rsi->f68) {
        rdi10 = stderr;
        fun_3840(rdi10, rdi10);
        if (!rsi->f70) {
            addr_7aea_3:
            rbp11 = stderr;
            if (!rsi->f78) {
                addr_7b2e_4:
                if (rsi->f80) {
                    fun_3840(rbp11, rbp11);
                    rbp11 = stderr;
                }
            } else {
                addr_7aff_6:
                fun_3840(rbp11, rbp11);
                rbp11 = stderr;
                goto addr_7b2e_4;
            }
        } else {
            rbp11 = stderr;
            goto addr_7ad4_8;
        }
    } else {
        rbp11 = stderr;
        if (!rsi->f70) {
            if (rsi->f78) 
                goto addr_7aff_6;
            if (!rsi->f80 && (!rsi->f88 && (!rsi->f90 && !rsi->f98))) {
                fun_3580();
            }
        } else {
            goto addr_7ad4_8;
        }
    }
    if (rsi->f88) {
        fun_3840(rbp11, rbp11);
        rbp11 = stderr;
    }
    if (rsi->f90) {
        fun_3840(rbp11, rbp11);
        rbp11 = stderr;
    }
    if (rsi->f98) {
        fun_3840(rbp11, rbp11);
    }
    addr_7ad4_8:
    fun_3840(rbp11, rbp11);
    goto addr_7aea_3;
}

void rpl_vfprintf();

struct s6 {
    signed char[16] pad16;
    void** f10;
};

int32_t fun_36b0(void** rdi, void** rsi, ...);

struct s7 {
    void** f0;
    signed char[7] pad8;
    uint32_t f8;
    signed char[4] pad16;
    void** f10;
};

void dbg_printf(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, ...) {
    signed char al8;
    struct s0* rax9;
    void** rdi10;
    void* rax11;
    struct s6* r12_12;
    void** rbp13;
    void** rbx14;
    void** rsi15;
    int32_t eax16;
    void** rsi17;
    struct s7* r12_18;
    int32_t eax19;
    void** rsi20;
    int32_t eax21;

    if (al8) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax9 = g28;
    fun_3830("date: ", "date: ");
    rdi10 = stderr;
    rpl_vfprintf();
    rax11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax9) - reinterpret_cast<int64_t>(g28));
    if (!rax11) {
        return;
    }
    fun_35b0();
    r12_12 = reinterpret_cast<struct s6*>(0x1b6e0);
    rbp13 = rdi10;
    rbx14 = rdi;
    rsi15 = reinterpret_cast<void**>("GMT");
    do {
        eax16 = fun_36b0(rbx14, rsi15, rbx14, rsi15);
        if (!eax16) 
            break;
        rsi15 = r12_12->f10;
        r12_12 = reinterpret_cast<struct s6*>(&r12_12->f10);
    } while (rsi15);
    goto addr_7388_9;
    addr_737a_10:
    goto 0x3000000008;
    addr_7388_9:
    rsi17 = *reinterpret_cast<void***>(rbp13 + 0xf0);
    r12_18 = reinterpret_cast<struct s7*>(rbp13 + 0xf0);
    if (rsi17) {
        do {
            eax19 = fun_36b0(rbx14, rsi17, rbx14, rsi17);
            if (!eax19) 
                break;
            rsi17 = r12_18->f10;
            r12_18 = reinterpret_cast<struct s7*>(&r12_18->f10);
        } while (rsi17);
        goto addr_73a0_13;
    } else {
        goto addr_73a0_13;
    }
    goto 0x3000000008;
    addr_73a0_13:
    r12_12 = reinterpret_cast<struct s6*>(0x1b3e0);
    rsi20 = reinterpret_cast<void**>("WET");
    do {
        eax21 = fun_36b0(rbx14, rsi20, rbx14, rsi20);
        if (!eax21) 
            break;
        rsi20 = r12_12->f10;
        r12_12 = reinterpret_cast<struct s6*>(&r12_12->f10);
    } while (rsi20);
    goto addr_73f8_18;
    goto addr_737a_10;
    addr_73f8_18:
    goto 0x3000000008;
}

struct s7* lookup_zone(void** rdi, void** rsi) {
    struct s7* r12_3;
    void** rbp4;
    void** rbx5;
    void** rsi6;
    int32_t eax7;
    void** rsi8;
    struct s7* r12_9;
    int32_t eax10;
    void** rsi11;
    int32_t eax12;

    r12_3 = reinterpret_cast<struct s7*>(0x1b6e0);
    rbp4 = rdi;
    rbx5 = rsi;
    rsi6 = reinterpret_cast<void**>("GMT");
    do {
        eax7 = fun_36b0(rbx5, rsi6);
        if (!eax7) 
            break;
        rsi6 = r12_3->f10;
        r12_3 = reinterpret_cast<struct s7*>(&r12_3->f10);
    } while (rsi6);
    goto addr_7388_4;
    addr_737a_5:
    return r12_3;
    addr_7388_4:
    rsi8 = *reinterpret_cast<void***>(rbp4 + 0xf0);
    r12_9 = reinterpret_cast<struct s7*>(rbp4 + 0xf0);
    if (rsi8) {
        do {
            eax10 = fun_36b0(rbx5, rsi8);
            if (!eax10) 
                break;
            rsi8 = r12_9->f10;
            r12_9 = reinterpret_cast<struct s7*>(&r12_9->f10);
        } while (rsi8);
        goto addr_73a0_8;
    } else {
        goto addr_73a0_8;
    }
    return r12_9;
    addr_73a0_8:
    r12_3 = reinterpret_cast<struct s7*>(0x1b3e0);
    rsi11 = reinterpret_cast<void**>("WET");
    do {
        eax12 = fun_36b0(rbx5, rsi11);
        if (!eax12) 
            break;
        rsi11 = r12_3->f10;
        r12_3 = reinterpret_cast<struct s7*>(&r12_3->f10);
    } while (rsi11);
    goto addr_73f8_13;
    goto addr_737a_5;
    addr_73f8_13:
    return 0;
}

unsigned char mktime_ok(void** rdi, void** rsi) {
    int32_t eax3;

    eax3 = 0;
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rsi + 24)) >= reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<unsigned char*>(&eax3) = reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) ^ reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi)) | reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(rdi + 4)) ^ reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(rsi + 4)) | reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 8)) ^ reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8)) | *reinterpret_cast<uint32_t*>(rdi + 12) ^ *reinterpret_cast<uint32_t*>(rsi + 12) | *reinterpret_cast<uint32_t*>(rdi + 16) ^ *reinterpret_cast<uint32_t*>(rsi + 16) | *reinterpret_cast<uint32_t*>(rdi + 20) ^ *reinterpret_cast<uint32_t*>(rsi + 20)) == 0);
    }
    return *reinterpret_cast<unsigned char*>(&eax3);
}

int64_t nstrftime();

struct s8 {
    signed char[88423] pad88423;
    void** f15967;
};

/* debug_strfdatetime.constprop.0 */
void** debug_strfdatetime_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, ...) {
    void** rsi5;
    void** rdi6;
    struct s0* rax7;
    int64_t rax8;
    int64_t rbx9;
    void** edi10;
    void** rsi11;
    void* rax12;
    int64_t rdx13;
    int64_t v14;
    void** rdi15;
    struct s8* rcx16;
    void** rsi17;
    void** r8_18;
    int64_t v19;
    uint32_t eax20;

    *reinterpret_cast<int32_t*>(&rsi5) = 100;
    *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
    rdi6 = rdx;
    rax7 = g28;
    rax8 = nstrftime();
    if (rsi && ((rbx9 = rax8, *reinterpret_cast<int32_t*>(&rax8) <= 99) && *reinterpret_cast<int64_t*>(rsi + 0xd8))) {
        edi10 = *reinterpret_cast<void***>(rsi + 24);
        rsi11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 48 - 8 + 8);
        time_zone_str(edi10, rsi11, edi10, rsi11);
        rdi6 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx9)) + reinterpret_cast<unsigned char>(rdx));
        rsi5 = reinterpret_cast<void**>(static_cast<int64_t>(100 - *reinterpret_cast<int32_t*>(&rbx9)));
        fun_34a0(rdi6, rsi5, 1, 0xffffffffffffffff, " TZ=%s", rdi6, rsi5, 1, 0xffffffffffffffff, " TZ=%s");
    }
    rax12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax7) - reinterpret_cast<int64_t>(g28));
    if (!rax12) {
        return rdx;
    }
    fun_35b0();
    if (*reinterpret_cast<signed char*>(rdi6 + 0xe8)) 
        goto addr_74d6_7;
    *reinterpret_cast<void***>(rsi5) = reinterpret_cast<void**>(0);
    rdx13 = *reinterpret_cast<int32_t*>(rdi6 + 16);
    if (*reinterpret_cast<uint32_t*>(&rdx13) > 6) {
        addr_751c_9:
        goto v14;
    } else {
        rdi15 = rsi5;
        *reinterpret_cast<int32_t*>(&rcx16) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rsi17) = 100;
        *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
    }
    addr_7549_11:
    r8_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(" %s") + reinterpret_cast<uint64_t>(rcx16));
    fun_34a0(rdi15, rsi17, 1, 0xffffffffffffffff, r8_18, rdi15, rsi17, 1, 0xffffffffffffffff, r8_18);
    goto v19;
    addr_74d6_7:
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi6 + 8) + 1) <= reinterpret_cast<unsigned char>(13)) {
        eax20 = fun_35f0(rsi5, 100, "%s");
    } else {
        eax20 = fun_34a0(rsi5, 100, 1, 0xffffffffffffffff, "%ld");
    }
    rdx13 = *reinterpret_cast<int32_t*>(rdi6 + 16);
    if (*reinterpret_cast<uint32_t*>(&rdx13) > 6) 
        goto addr_751c_9;
    if (eax20 > 99) 
        goto addr_751c_9;
    *reinterpret_cast<int32_t*>(&rcx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx16) = reinterpret_cast<uint1_t>(eax20 == 0);
    rsi17 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(100 - eax20)));
    rdi15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi5) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax20))));
    goto addr_7549_11;
}

int32_t fun_3890(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

struct s9 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
};

void** time_zone_str(void** edi, void** rsi, ...) {
    int64_t rbx3;
    int64_t rbp4;
    int64_t rax5;
    int64_t r8_6;
    uint64_t rbx7;
    int32_t ebx8;
    int64_t r9_9;
    int32_t eax10;
    int32_t ebp11;
    struct s9* rax12;
    int32_t ecx13;
    int64_t rdx14;
    uint64_t rdx15;
    int64_t rsi16;
    uint64_t rsi17;
    int32_t r9d18;
    int32_t esi19;
    signed char* rsi20;
    int32_t edx21;
    int32_t ecx22;
    int64_t rdx23;
    uint64_t rdx24;
    int32_t esi25;
    int32_t edx26;
    int32_t ecx27;

    rbx3 = reinterpret_cast<int32_t>(edi);
    rbp4 = rbx3;
    *reinterpret_cast<uint32_t*>(&rax5) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rbx3) >> 31) & 2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_6) = static_cast<int32_t>(rax5 + 43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_6) + 4) = 0;
    rbx7 = rbx3 * 0xffffffff91a2b3c5 >> 32;
    ebx8 = (*reinterpret_cast<int32_t*>(&rbx7) + *reinterpret_cast<int32_t*>(&rbp4) >> 11) - (*reinterpret_cast<int32_t*>(&rbp4) >> 31);
    *reinterpret_cast<int32_t*>(&r9_9) = -ebx8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_9) + 4) = 0;
    if (__intrinsic()) {
        *reinterpret_cast<int32_t*>(&r9_9) = ebx8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_9) + 4) = 0;
    }
    eax10 = fun_3890(rsi, 1, -1, "%c%02d", r8_6, r9_9);
    ebp11 = *reinterpret_cast<int32_t*>(&rbp4) - ebx8 * reinterpret_cast<int32_t>("ko");
    if (ebp11) {
        rax12 = reinterpret_cast<struct s9*>(static_cast<int64_t>(eax10) + reinterpret_cast<unsigned char>(rsi));
        ecx13 = -ebp11;
        if (__intrinsic()) {
            ecx13 = ebp11;
        }
        rax12->f0 = 58;
        *reinterpret_cast<int32_t*>(&rdx14) = ecx13;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
        rdx15 = reinterpret_cast<uint64_t>(rdx14 * 0x88888889) >> 37;
        *reinterpret_cast<int32_t*>(&rsi16) = *reinterpret_cast<int32_t*>(&rdx15);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi16) + 4) = 0;
        rsi17 = reinterpret_cast<uint64_t>(rsi16 * 0xcccccccd) >> 35;
        r9d18 = static_cast<int32_t>(rsi17 + 48);
        esi19 = static_cast<int32_t>(rsi17 + rsi17 * 4);
        rax12->f1 = *reinterpret_cast<signed char*>(&r9d18);
        rsi20 = &rax12->f3;
        edx21 = *reinterpret_cast<int32_t*>(&rdx15) - (esi19 + esi19) + 48;
        ecx22 = ecx13 - *reinterpret_cast<int32_t*>(&rdx15) * 60;
        rax12->f2 = *reinterpret_cast<signed char*>(&edx21);
        if (ecx22) {
            *reinterpret_cast<int32_t*>(&rdx23) = ecx22;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0;
            rax12->f3 = 58;
            rdx24 = reinterpret_cast<uint64_t>(rdx23 * 0xcccccccd) >> 35;
            esi25 = static_cast<int32_t>(rdx24 + 48);
            edx26 = static_cast<int32_t>(rdx24 + rdx24 * 4);
            rax12->f4 = *reinterpret_cast<signed char*>(&esi25);
            rsi20 = &rax12->f6;
            ecx27 = ecx22 - (edx26 + edx26) + 48;
            rax12->f5 = *reinterpret_cast<signed char*>(&ecx27);
        }
        *rsi20 = 0;
    }
    return rsi;
}

void** tm_year_str(int32_t edi, void** rsi, ...) {
    int64_t rax3;
    int32_t eax4;
    int32_t eax5;
    int32_t edx6;
    int64_t rcx7;
    int64_t r9_8;
    int64_t r8_9;

    rax3 = edi * 0x51eb851f >> 37;
    eax4 = *reinterpret_cast<int32_t*>(&rax3) - (edi >> 31);
    eax5 = eax4 + 19;
    edx6 = edi - eax4 * 100;
    *reinterpret_cast<int32_t*>(&rcx7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx7) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx7) = reinterpret_cast<uint1_t>(edi >= 0xfffff894);
    *reinterpret_cast<int32_t*>(&r9_8) = -edx6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_8) + 4) = 0;
    if (__intrinsic()) {
        *reinterpret_cast<int32_t*>(&r9_8) = edx6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_8) + 4) = 0;
    }
    *reinterpret_cast<int32_t*>(&r8_9) = -eax5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_9) + 4) = 0;
    if (__intrinsic()) {
        *reinterpret_cast<int32_t*>(&r8_9) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_9) + 4) = 0;
    }
    fun_3890(rsi, 1, -1, rcx7 + reinterpret_cast<int64_t>("-%02d%02d"), r8_9, r9_8);
    return rsi;
}

void** fun_35a0(void** rdi, ...);

void gettime(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_34f0(void** rdi, void** rsi, void** rdx, ...);

void fun_34b0(void** rdi, ...);

int64_t fun_3460(void** rdi, void** rsi, void** rdx);

void tzfree(void** rdi, void** rsi, ...);

int32_t yyparse(void** rdi, void** rsi, void** rdx);

void** mktime_z(void** rdi, void** rsi, ...);

void** tzalloc(void** rdi, void** rsi, ...);

void** fun_3730(void** rdi, void** rsi, ...);

struct s10 {
    unsigned char f0;
    void** f1;
};

uint32_t parse_datetime_body(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rbp9;
    void** v10;
    uint32_t v11;
    void** v12;
    struct s0* rax13;
    struct s0* v14;
    void** rax15;
    void* rsp16;
    void** v17;
    void** rbx18;
    void** r14_19;
    void** v20;
    uint32_t eax21;
    int32_t eax22;
    void*** rsp23;
    void** r15_24;
    void** rsi25;
    int64_t rax26;
    void** v27;
    void** r13_28;
    void** rdx29;
    uint32_t eax30;
    void** rdi31;
    void** rax32;
    uint32_t edx33;
    void* rax34;
    void** rax35;
    void** rax36;
    int32_t v37;
    void** rdx38;
    int64_t rax39;
    int32_t v40;
    void** rax41;
    unsigned char v42;
    void** rsi43;
    void** rax44;
    void* rsp45;
    int32_t eax46;
    void** rax47;
    void** rbp48;
    void** rax49;
    void** rdx50;
    void* rsp51;
    void** rbp52;
    int64_t rax53;
    void* rsp54;
    void** rdx55;
    void** rax56;
    void** rax57;
    int64_t rax58;
    void** rsi59;
    void** v60;
    void** rax61;
    void* rsp62;
    void** rdx63;
    void** rax64;
    void** rax65;
    int1_t zf66;
    uint32_t eax67;
    void** v68;
    void** v69;
    int32_t v70;
    void** v71;
    void** r14_72;
    int64_t v73;
    int32_t v74;
    int64_t v75;
    int32_t v76;
    void** v77;
    int32_t v78;
    void** v79;
    int32_t v80;
    void** v81;
    int32_t v82;
    int16_t v83;
    int32_t v84;
    void** rdx85;
    void** v86;
    void** v87;
    int32_t v88;
    void** v89;
    int32_t r13d90;
    int64_t rax91;
    void** v92;
    int32_t v93;
    int32_t eax94;
    int32_t eax95;
    void** rax96;
    void* rsp97;
    void** rax98;
    void* rsp99;
    void** rdi100;
    void** rax101;
    int32_t eax102;
    void** rdi103;
    void* rsp104;
    void** rdi105;
    void** rdi106;
    void** rsi107;
    void** v108;
    void** rax109;
    void** rdi110;
    int32_t v111;
    uint64_t rax112;
    uint32_t edx113;
    uint32_t v114;
    void* rbx115;
    void** rax116;
    void** rsi117;
    uint32_t edx118;
    int64_t v119;
    void** rax120;
    int32_t edx121;
    int64_t rax122;
    int64_t rax123;
    int64_t v124;
    void** rdx125;
    void** rax126;
    void** r13_127;
    void** r10_128;
    int64_t v129;
    uint32_t r11d130;
    uint32_t edi131;
    void** rbx132;
    void* rsp133;
    void** rax134;
    void* rsp135;
    void** rdi136;
    void** rax137;
    int32_t eax138;
    int32_t v139;
    int64_t v140;
    void** r13_141;
    void** rdx142;
    int64_t v143;
    int64_t v144;
    void** rax145;
    unsigned char al146;
    void** rsi147;
    void** v148;
    void** rax149;
    void** rax150;
    unsigned char al151;
    uint32_t eax152;
    void** r8_153;
    uint32_t v154;
    uint32_t v155;
    uint32_t eax156;
    void** rax157;
    void* rsp158;
    void** rax159;
    void** rax160;
    void** rax161;
    void** rax162;
    void* rsp163;
    void** rax164;
    void* rsp165;
    void** rax166;
    void** rax167;
    void** rax168;
    void** rax169;
    void** rax170;
    void* rsp171;
    void** rax172;
    void** rax173;
    void** rax174;
    void** rax175;
    int32_t r13d176;
    void** r9_177;
    int64_t rax178;
    uint32_t eax179;
    uint32_t eax180;
    void** rbx181;
    int64_t rax182;
    void** rdi183;
    int64_t rax184;
    int32_t eax185;
    int32_t eax186;
    uint32_t esi187;
    int64_t rcx188;
    int64_t r9_189;
    int64_t r8_190;
    void** r11_191;
    void** r10_192;
    void** rax193;
    void** rax194;
    void** rax195;
    void** rdx196;
    void** rax197;
    void** rax198;
    uint32_t eax199;
    void** r9_200;
    uint32_t ecx201;
    int64_t rax202;
    void** rdx203;
    int32_t v204;
    uint64_t rdi205;
    uint64_t v206;
    void** rdx207;
    void** rax208;
    void** rax209;
    void** rax210;
    void** rax211;
    int32_t edx212;
    void** rax213;
    int32_t v214;
    int64_t rcx215;
    int64_t v216;
    int64_t rax217;
    int32_t v218;
    int32_t eax219;
    int32_t v220;
    uint64_t rdx221;
    int64_t rdx222;
    void** rax223;
    void** rdx224;
    uint32_t eax225;
    void* rsp226;
    void** rax227;
    void* rsp228;
    void** rax229;
    void* rax230;
    void** rax231;
    void** rax232;
    void* rsp233;
    void** rax234;
    void* rsp235;
    void** rax236;
    void** rsi237;
    void** rax238;
    void** rax239;
    void** rax240;
    void** v241;
    void** rax242;
    void** rax243;
    void** rdx244;
    void** rax245;
    void** rax246;
    void* rsp247;
    void** rax248;
    void** rax249;
    void** rax250;
    void** rax251;
    void* rsp252;
    void** rax253;
    void** rax254;
    void** rcx255;
    void** rdx256;
    int64_t rax257;
    void** rax258;
    void** rax259;
    void** rdx260;
    uint64_t rdi261;
    int32_t v262;
    uint64_t v263;
    void** rdx264;
    void** rax265;
    void* rsp266;
    void** v267;
    void** rax268;
    void** rax269;
    uint32_t v270;
    uint64_t rax271;
    uint32_t edx272;
    void** rax273;
    void** rdx274;
    void** rax275;
    void* rsp276;
    void** rax277;
    void** rax278;
    void** rax279;
    void** rax280;
    void** rdx281;
    void* rax282;
    struct s10* rax283;
    void** rax284;
    uint32_t eax285;
    int64_t rax286;

    r13_7 = rdx;
    r12_8 = r8;
    rbp9 = rsi;
    v10 = rdi;
    v11 = *reinterpret_cast<uint32_t*>(&rcx);
    v12 = r9;
    rax13 = g28;
    v14 = rax13;
    rax15 = fun_35a0(rsi);
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x458 - 8 + 8);
    v17 = rax15;
    if (!r13_7) {
        r13_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp16) + 96);
        gettime(r13_7, rsi, rdx, rcx, r8, r9);
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
    }
    rbx18 = *reinterpret_cast<void***>(r13_7);
    r14_19 = rbp9;
    v20 = *reinterpret_cast<void***>(r13_7 + 8);
    while (1) {
        eax21 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_19));
        if (*reinterpret_cast<signed char*>(&eax21) > 13) {
            if (*reinterpret_cast<signed char*>(&eax21) != 32) 
                break;
        } else {
            if (*reinterpret_cast<signed char*>(&eax21) <= 8) 
                break;
        }
        ++r14_19;
    }
    eax22 = fun_34f0(r14_19, "TZ=\"", 4);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
    if (eax22 || (*reinterpret_cast<uint32_t*>(&r15_24) = reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(r14_19 + 4)), *reinterpret_cast<int32_t*>(&r15_24 + 4) = 0, r8 = r14_19 + 4, *reinterpret_cast<signed char*>(&r15_24) == 0)) {
        addr_9b08_9:
        rsi25 = r13_7;
        rax26 = localtime_rz(r12_8, rsi25, rsp23 + 0xf0, rcx, r8, r9);
        rsp23 = rsp23 - 8 + 8;
        if (!rax26) {
            addr_a520_10:
            v27 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_28) = 0;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        } else {
            v27 = reinterpret_cast<void**>(0);
            r15_24 = r12_8;
            goto addr_9b2f_12;
        }
    } else {
        rdx29 = r8;
        eax30 = *reinterpret_cast<uint32_t*>(&r15_24);
        *reinterpret_cast<int32_t*>(&rdi31) = 1;
        *reinterpret_cast<int32_t*>(&rdi31 + 4) = 0;
        do {
            if (*reinterpret_cast<signed char*>(&eax30) != 92) {
                if (*reinterpret_cast<signed char*>(&eax30) == 34) 
                    goto addr_a338_16;
                rax32 = rdx29;
            } else {
                rax32 = rdx29 + 1;
                edx33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx29 + 1));
                if (*reinterpret_cast<signed char*>(&edx33) == 92) 
                    continue;
                if (*reinterpret_cast<signed char*>(&edx33) != 34) 
                    goto addr_9b01_20;
            }
            rdx29 = rax32 + 1;
            eax30 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax32 + 1));
            ++rdi31;
        } while (*reinterpret_cast<signed char*>(&eax30));
        goto addr_9b08_9;
    }
    addr_a298_22:
    while (fun_34b0(v27, v27), rax34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v14) - reinterpret_cast<int64_t>(g28)), !!rax34) {
        fun_35b0();
        rsp23 = rsp23 - 8 + 8 - 8 + 8;
        addr_b9d3_24:
        rax35 = fun_3580();
        rcx = rcx;
        *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<uint32_t*>(&rbx18);
        *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
        dbg_printf(rax35, r14_19, r13_28, rcx, r8, r9, v27, rax35, r14_19, r13_28, rcx, r8, r9, v27);
        rax36 = fun_3580();
        rsi25 = rbp9;
        dbg_printf(rax36, rsi25, 5, rcx, r8, r9, v27, rax36, rsi25, 5, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        if (v37 != -1 && ((rsi25 = v10, rdx38 = reinterpret_cast<void**>(rsp23 + 0x170), rax39 = localtime_rz(r15_24, rsi25, rdx38, rcx, r8, r9, r15_24, rsi25, rdx38, rcx, r8, r9), rsp23 = rsp23 - 8 + 8, !!rax39) && v37 != v40)) {
            rsi25 = reinterpret_cast<void**>("warning: daylight saving time changed after time adjustment\n");
            rax41 = fun_3580();
            dbg_printf(rax41, "warning: daylight saving time changed after time adjustment\n", 5, rcx, r8, r9, v27, rax41, "warning: daylight saving time changed after time adjustment\n", 5, rcx, r8, r9, v27);
            rsp23 = rsp23 - 8 + 8 - 8 + 8;
        }
        addr_b09b_26:
        *reinterpret_cast<uint32_t*>(&r13_28) = v42;
        *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        addr_a5e7_27:
        if (!*reinterpret_cast<signed char*>(&r13_28)) {
            *reinterpret_cast<uint32_t*>(&r13_28) = 1;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        } else {
            rsi43 = reinterpret_cast<void**>("timezone: system default\n");
            if (!v12) {
                addr_a629_30:
                rax44 = fun_3580();
                dbg_printf(rax44, rsi43, 5, rcx, r8, r9, v27, rax44, rsi43, 5, rcx, r8, r9, v27);
                rsp45 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
            } else {
                eax46 = fun_36b0(v12, "UTC0", v12, "UTC0");
                rsp23 = rsp23 - 8 + 8;
                if (eax46) {
                    rax47 = fun_3580();
                    dbg_printf(rax47, v12, 5, rcx, r8, r9, v27, rax47, v12, 5, rcx, r8, r9, v27);
                    rsp45 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
                } else {
                    rsi43 = reinterpret_cast<void**>("timezone: Universal Time\n");
                    goto addr_a629_30;
                }
            }
            rbx18 = v10;
            rbp48 = *reinterpret_cast<void***>(rbx18);
            r14_19 = *reinterpret_cast<void***>(rbx18 + 8);
            *reinterpret_cast<int32_t*>(&r14_19 + 4) = 0;
            rax49 = fun_3580();
            rdx50 = r14_19;
            *reinterpret_cast<int32_t*>(&rdx50 + 4) = 0;
            dbg_printf(rax49, rbp48, rdx50, rcx, r8, r9, v27, rax49, rbp48, rdx50, rcx, r8, r9, v27);
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8 - 8 + 8);
            rbp52 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x130);
            rax53 = fun_3460(rbx18, rbp52, rdx50);
            rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
            if (rax53) {
                rdx55 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 0x300);
                rax56 = debug_strfdatetime_constprop_0(rbp52, 0, rdx55, rcx, rbp52, 0, rdx55, rcx);
                rax57 = fun_3580();
                dbg_printf(rax57, rax56, 5, rcx, r8, r9, v27, rax57, rax56, 5, rcx, r8, r9, v27);
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8 - 8 + 8 - 8 + 8);
            }
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp54) + 0x170);
            rsi25 = v10;
            rax58 = localtime_rz(r15_24, rsi25, rbp9, rcx, r8, r9, r15_24, rsi25, rbp9, rcx, r8, r9);
            rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            if (rax58) {
                rsi59 = reinterpret_cast<void**>(rsp23 + 0x2e0);
                rax61 = time_zone_str(v60, rsi59, v60, rsi59);
                rsp62 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                rdx63 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp62) + 0x300);
                r14_19 = rax61;
                rax64 = debug_strfdatetime_constprop_0(rbp9, 0, rdx63, rcx, rbp9, 0, rdx63, rcx);
                rbp9 = rax64;
                rax65 = fun_3580();
                rsi25 = rbp9;
                dbg_printf(rax65, rsi25, r14_19, rcx, r8, r9, v27, rax65, rsi25, r14_19, rcx, r8, r9, v27);
                rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp62) - 8 + 8 - 8 + 8 - 8 + 8);
            }
        }
        addr_a28b_38:
        if (r15_24 == r12_8) 
            continue;
        tzfree(r15_24, rsi25, r15_24, rsi25);
        rsp23 = rsp23 - 8 + 8;
    }
    return *reinterpret_cast<uint32_t*>(&r13_28);
    addr_9b2f_12:
    zf66 = *reinterpret_cast<void***>(r14_19) == 0;
    if (zf66) {
        r14_19 = reinterpret_cast<void**>("0");
    }
    eax67 = *reinterpret_cast<unsigned char*>(&v11);
    v68 = r14_19;
    v42 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax67) & 1);
    v69 = reinterpret_cast<void**>(v70 + 0x76c);
    v71 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v20)));
    r14_72 = reinterpret_cast<void**>(rsp23 + 0x170);
    v73 = v74 + 1;
    v75 = v76;
    v77 = reinterpret_cast<void**>(static_cast<int64_t>(v78));
    v79 = reinterpret_cast<void**>(static_cast<int64_t>(v80));
    v81 = reinterpret_cast<void**>(static_cast<int64_t>(v82));
    v83 = 0;
    v37 = v84;
    rdx85 = v86;
    v87 = rdx85;
    v88 = v84;
    v89 = reinterpret_cast<void**>(0);
    r13d90 = 0x76a700;
    do {
        if (__intrinsic()) 
            break;
        rsi25 = reinterpret_cast<void**>(rsp23 + 0x130);
        rdx85 = r14_72;
        rax91 = localtime_rz(r15_24, rsi25, rdx85, rcx, r8, r9, r15_24, rsi25, rdx85, rcx, r8, r9);
        rsp23 = rsp23 - 8 + 8;
        if (!rax91) 
            continue;
        if (!v92) 
            continue;
        *reinterpret_cast<int32_t*>(&rdx85) = v93;
        *reinterpret_cast<int32_t*>(&rdx85 + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx85) != v88) 
            goto addr_a9d4_47;
        r13d90 = r13d90 + 0x76a700;
    } while (r13d90 != 0x1da9c00);
    addr_9d65_49:
    if (v87 && ((rsi25 = v89, !!rsi25) && (eax94 = fun_36b0(v87, rsi25, v87, rsi25), rsp23 = rsp23 - 8 + 8, eax94 == 0))) {
    }
    r14_19 = reinterpret_cast<void**>(rsp23 + 0x1b0);
    eax95 = yyparse(r14_19, rsi25, rdx85);
    rsp23 = rsp23 - 8 + 8;
    if (eax95) {
        *reinterpret_cast<uint32_t*>(&r13_28) = v42;
        *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&r13_28)) 
            goto addr_a28b_38;
        rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(v17));
        if (reinterpret_cast<unsigned char>(v68) >= reinterpret_cast<unsigned char>(rbp9)) 
            goto addr_a311_54;
    } else {
        if (v42) {
            rax96 = fun_3580();
            dbg_printf(rax96, "input timezone: ", 5, rcx, r8, r9, v27, rax96, "input timezone: ", 5, rcx, r8, r9, v27);
            rsp97 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
            if (0) 
                goto addr_a540_57;
            if (0) 
                goto addr_a540_57;
            rbx18 = v12;
            if (!rbx18) 
                goto addr_a530_60; else 
                goto addr_a490_61;
        } else {
            *reinterpret_cast<uint32_t*>(&r13_28) = 0;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
            if (0) {
                __asm__("movdqu xmm0, [rsp+0x208]");
                __asm__("movups [rax], xmm0");
                goto addr_a28b_38;
            }
            if (0) 
                goto addr_a288_65; else 
                goto addr_9e09_66;
        }
    }
    rax98 = fun_3580();
    rsp99 = reinterpret_cast<void*>(rsp23 - 8 + 8);
    rdi100 = rax98;
    addr_a322_68:
    rsi25 = v68;
    *reinterpret_cast<uint32_t*>(&r13_28) = 0;
    *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
    dbg_printf(rdi100, rsi25, 5, rcx, r8, r9, v27, rdi100, rsi25, 5, rcx, r8, r9, v27);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp99) - 8 + 8);
    goto addr_a28b_38;
    addr_a311_54:
    rax101 = fun_3580();
    rsp99 = reinterpret_cast<void*>(rsp23 - 8 + 8);
    rdi100 = rax101;
    goto addr_a322_68;
    addr_a530_60:
    goto addr_a540_57;
    addr_a490_61:
    if (r12_8 == r15_24) {
        eax102 = fun_36b0(v12, "UTC0", v12, "UTC0");
        rsp97 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8);
        if (!eax102) {
            addr_a540_57:
            fun_3580();
            rdi103 = stderr;
            fun_3840(rdi103, rdi103);
            rsp104 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8 - 8 + 8);
        } else {
            fun_3580();
            rcx = v12;
            rdi105 = stderr;
            fun_3840(rdi105, rdi105);
            rsp104 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8 - 8 + 8);
        }
    } else {
        fun_3580();
        rdi106 = stderr;
        rcx = rbx18;
        fun_3840(rdi106, rdi106);
        rsp104 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp97) - 8 + 8 - 8 + 8);
    }
    if (1) {
        addr_a760_73:
        if (0) {
            addr_a57d_74:
            rsi107 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp104) + 0x2e0);
            rax109 = time_zone_str(v108, rsi107, v108, rsi107);
            rdi110 = stderr;
            rcx = rax109;
            fun_3840(rdi110, rdi110);
            rsp104 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp104) - 8 + 8 - 8 + 8);
            rsi25 = stderr;
        } else {
            rsi25 = stderr;
        }
    } else {
        if (!1) 
            goto addr_a57d_74;
        rsi25 = stderr;
        if (!(reinterpret_cast<uint1_t>(v111 < 0) | reinterpret_cast<uint1_t>(v111 == 0))) 
            goto addr_a743_78;
    }
    fun_3650(10, rsi25, 10, rsi25);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp104) - 8 + 8);
    *reinterpret_cast<uint32_t*>(&r13_28) = v42;
    *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
    if (!1) {
        __asm__("movdqu xmm1, [rsp+0x208]");
        __asm__("movups [rax], xmm1");
        goto addr_a5e7_27;
    }
    rsi25 = reinterpret_cast<void**>(0);
    rcx = reinterpret_cast<void**>(0);
    if (!1) {
        if (*reinterpret_cast<signed char*>(&r13_28)) {
            if (0) {
                dbg_printf("error: seen multiple time parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple time parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
                rcx = reinterpret_cast<void**>(0);
            }
            if (0) {
                dbg_printf("error: seen multiple date parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple date parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
            }
            if (0) {
                dbg_printf("error: seen multiple days parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple days parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
            }
            if (0) {
                dbg_printf("error: seen multiple daylight-saving parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple daylight-saving parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
            }
            if (!1) {
                dbg_printf("error: seen multiple time-zone parts\n", 0, 0, 0, r8, r9, v27, "error: seen multiple time-zone parts\n", 0, 0, 0, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8;
                goto addr_a288_65;
            }
        }
    }
    addr_9e09_66:
    rbp9 = v69;
    if (reinterpret_cast<signed char>(rbp9) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<uint32_t*>(&rcx) = 0;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rax112 = reinterpret_cast<uint64_t>(0xfffffffffffff894 - reinterpret_cast<unsigned char>(rbp9));
        *reinterpret_cast<unsigned char*>(&rcx) = __intrinsic();
        edx113 = 1;
        if (rax112 == static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax112))) {
            edx113 = *reinterpret_cast<uint32_t*>(&rcx);
        }
        v114 = edx113;
    } else {
        if (0) {
            *reinterpret_cast<int32_t*>(&rbx115) = 0x7d0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx115) + 4) = 0;
            if (reinterpret_cast<signed char>(rbp9) >= reinterpret_cast<signed char>(69)) {
                rbx115 = reinterpret_cast<void*>(0x76c);
            }
            rbx18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx115) + reinterpret_cast<unsigned char>(rbp9));
            if (*reinterpret_cast<signed char*>(&r13_28)) {
                rax116 = fun_3580();
                rsi117 = rbp9;
                rbp9 = rbx18;
                dbg_printf(rax116, rsi117, rbx18, rcx, r8, r9, v27, rax116, rsi117, rbx18, rcx, r8, r9, v27);
                rsp23 = rsp23 - 8 + 8 - 8 + 8;
            } else {
                rbp9 = rbx18;
            }
        }
        edx118 = 0;
        rax112 = reinterpret_cast<uint64_t>(rbp9 + 0xfffffffffffff894);
        if (reinterpret_cast<unsigned char>(rbp9) >= reinterpret_cast<unsigned char>(0x76c)) 
            goto addr_a8c6_103; else 
            goto addr_9e3f_104;
    }
    addr_9e5f_105:
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v119) + 4) = *reinterpret_cast<uint32_t*>(&rax112);
    if (*reinterpret_cast<unsigned char*>(&v114)) {
        if (*reinterpret_cast<signed char*>(&r13_28)) {
            rax120 = fun_3580();
            dbg_printf(rax120, rbp9, 5, rcx, r8, r9, v27, rax120, rbp9, 5, rcx, r8, r9, v27);
            rsp23 = rsp23 - 8 + 8 - 8 + 8;
        }
    } else {
        edx121 = 0;
        *reinterpret_cast<uint32_t*>(&rsi25) = v42;
        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
        rax122 = v73 - 1;
        *reinterpret_cast<unsigned char*>(&edx121) = __intrinsic();
        *reinterpret_cast<uint32_t*>(&v119) = *reinterpret_cast<uint32_t*>(&rax122);
        if (rax122 != static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax122))) {
            edx121 = 1;
        }
        *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(&rax122);
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        if (edx121) 
            goto addr_a97e_111;
        rax123 = v75;
        *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v124) + 4) = *reinterpret_cast<uint32_t*>(&rax123);
        *reinterpret_cast<uint32_t*>(&rdx125) = *reinterpret_cast<uint32_t*>(&rax123);
        *reinterpret_cast<int32_t*>(&rdx125 + 4) = 0;
        if (rax123 == static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax123))) 
            goto addr_9ec3_113;
    }
    addr_a97e_111:
    rsi25 = reinterpret_cast<void**>("error: year, month, or day overflow\n");
    if (!v42) {
        addr_a288_65:
        *reinterpret_cast<uint32_t*>(&r13_28) = 0;
        *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        goto addr_a28b_38;
    } else {
        addr_a998_114:
        rax126 = fun_3580();
        dbg_printf(rax126, rsi25, 5, rcx, r8, r9, v27, rax126, rsi25, 5, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8 - 8 + 8;
        goto addr_a288_65;
    }
    addr_9ec3_113:
    if (0) {
        addr_ac03_115:
        *reinterpret_cast<uint32_t*>(&r9) = 2;
        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        rbp9 = v77;
        if (0) {
            if (reinterpret_cast<uint64_t>(rbp9 + 0xffffffffffffffff) <= 10) {
                addr_ac30_117:
                *reinterpret_cast<uint32_t*>(&r9) = *reinterpret_cast<uint32_t*>(&rbp9);
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                goto addr_ac33_118;
            } else {
                r13_127 = reinterpret_cast<void**>("am");
                if (rbp9 == 12) {
                    addr_ac33_118:
                    r8 = v79;
                    r10_128 = v81;
                    *reinterpret_cast<uint32_t*>(&v124) = *reinterpret_cast<uint32_t*>(&r9);
                    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v129) + 4) = *reinterpret_cast<uint32_t*>(&r8);
                    r11d130 = *reinterpret_cast<uint32_t*>(&r8);
                    edi131 = *reinterpret_cast<uint32_t*>(&r10_128);
                    *reinterpret_cast<uint32_t*>(&v129) = *reinterpret_cast<uint32_t*>(&r10_128);
                    if (*reinterpret_cast<unsigned char*>(&rsi25)) {
                        rbx132 = reinterpret_cast<void**>(rsp23 + 0x300);
                        fun_34a0(rbx132, 100, 1, 100, "%02d:%02d:%02d", rbx132, 100, 1, 100, "%02d:%02d:%02d");
                        r8 = r8;
                        rsp133 = reinterpret_cast<void*>(rsp23 - 8 - 8 - 8 + 8 + 8 + 8);
                        if (1) {
                            rax134 = fun_3580();
                            rsp135 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp133) - 8 + 8);
                            rdi136 = rax134;
                        } else {
                            rax137 = fun_3580();
                            rsp135 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp133) - 8 + 8);
                            rdi136 = rax137;
                        }
                        dbg_printf(rdi136, rbx132, 5, 100, r8, r10_128, v27, rdi136, rbx132, 5, 100, r8, r10_128, v27);
                        rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp135) - 8 + 8);
                    } else {
                        addr_ac61_124:
                        if (1) {
                            addr_9f39_125:
                            if (0) {
                                eax138 = v139;
                                v37 = eax138;
                                goto addr_9f4f_127;
                            } else {
                                eax138 = v37;
                                goto addr_9f4f_127;
                            }
                        } else {
                            goto addr_9f2e_130;
                        }
                    }
                } else {
                    goto addr_b4a5_132;
                }
            }
        } else {
            if (0) {
                *reinterpret_cast<uint32_t*>(&r9) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rbp9 + 12));
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                if (reinterpret_cast<uint64_t>(rbp9 + 0xffffffffffffffff) <= 10) 
                    goto addr_ac33_118;
                *reinterpret_cast<uint32_t*>(&r9) = 12;
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                r13_127 = reinterpret_cast<void**>("pm");
                if (rbp9 == 12) 
                    goto addr_ac33_118; else 
                    goto addr_b4a5_132;
            } else {
                if (reinterpret_cast<unsigned char>(rbp9) <= reinterpret_cast<unsigned char>(23)) 
                    goto addr_ac30_117;
                r13_127 = reinterpret_cast<void**>(0x15e39);
                goto addr_b4a5_132;
            }
        }
    } else {
        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&v83) + 1)) {
            v129 = 0;
            *reinterpret_cast<uint32_t*>(&v124) = 0;
            v71 = reinterpret_cast<void**>(0);
            if (!*reinterpret_cast<unsigned char*>(&rsi25)) {
                *reinterpret_cast<uint32_t*>(&r9) = 0;
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                r11d130 = 0;
                edi131 = 0;
                goto addr_ac61_124;
            }
        }
        if (1) 
            goto addr_ac03_115; else 
            goto addr_9f00_142;
    }
    addr_b59e_143:
    edi131 = *reinterpret_cast<uint32_t*>(&v129);
    r11d130 = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v129) + 4);
    *reinterpret_cast<uint32_t*>(&r9) = *reinterpret_cast<uint32_t*>(&v124);
    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdx125) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v124) + 4);
    *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(&v119);
    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
    goto addr_ac61_124;
    addr_9f4f_127:
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v140) + 4) = *reinterpret_cast<uint32_t*>(&rdx125);
    r13_141 = reinterpret_cast<void**>(rsp23 + 0x70);
    *reinterpret_cast<uint32_t*>(&rdx142) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v119) + 4);
    *reinterpret_cast<int32_t*>(&rdx142 + 4) = 0;
    rbx18 = reinterpret_cast<void**>(rsp23 + 0xb0);
    *reinterpret_cast<uint32_t*>(&v143) = edi131;
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v143) + 4) = r11d130;
    *reinterpret_cast<uint32_t*>(&v140) = *reinterpret_cast<uint32_t*>(&r9);
    *reinterpret_cast<uint32_t*>(&v144) = *reinterpret_cast<uint32_t*>(&rcx);
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v144) + 4) = *reinterpret_cast<uint32_t*>(&rdx142);
    rax145 = mktime_z(r15_24, r13_141);
    rsi25 = r13_141;
    rbp9 = rax145;
    al146 = mktime_ok(rbx18, rsi25);
    rsp23 = rsp23 - 8 + 8 - 8 + 8;
    if (al146) 
        goto addr_aac0_144;
    if (!0) 
        goto addr_9fd8_146;
    rsi147 = reinterpret_cast<void**>(rsp23 + 0x3e3);
    rbp9 = reinterpret_cast<void**>(rsp23 + 0x3e0);
    time_zone_str(v148, rsi147, v148, rsi147);
    rax149 = tzalloc(rbp9, rsi147, rbp9, rsi147);
    rsp23 = rsp23 - 8 + 8 - 8 + 8;
    r8 = rax149;
    if (!rax149) {
        rsi25 = reinterpret_cast<void**>("error: tzalloc (\"%s\") failed\n");
        if (!v42) 
            goto addr_a288_65;
    } else {
        v129 = v143;
        v124 = v140;
        v119 = v144;
        v37 = eax138;
        rax150 = mktime_z(r8, r13_141);
        rsi25 = r13_141;
        rbp9 = rax150;
        al151 = mktime_ok(rbx18, rsi25);
        tzfree(r8, rsi25);
        rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
        eax152 = al151;
        if (!*reinterpret_cast<signed char*>(&eax152)) {
            addr_9fd8_146:
            *reinterpret_cast<uint32_t*>(&r9) = *reinterpret_cast<uint32_t*>(&v143);
            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r8_153) = *reinterpret_cast<uint32_t*>(&v129);
            *reinterpret_cast<int32_t*>(&r8_153 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v143) + 4);
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            *reinterpret_cast<uint32_t*>(&v12) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v140) + 4);
            v154 = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v124) + 4);
            v155 = *reinterpret_cast<uint32_t*>(&v144);
            *reinterpret_cast<uint32_t*>(&v10) = *reinterpret_cast<uint32_t*>(&v119);
            eax156 = v42;
            if (*reinterpret_cast<uint32_t*>(&r9) != *reinterpret_cast<uint32_t*>(&r8_153) || *reinterpret_cast<uint32_t*>(&rcx) != *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v129) + 4)) {
                if (!*reinterpret_cast<signed char*>(&eax156)) 
                    goto addr_a288_65;
                rax157 = fun_3580();
                rsp158 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                rbp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp158) + 0x3e0);
                dbg_printf(rax157, "error: invalid date/time value:\n", 5, rcx, r8_153, r9, v27, rax157, "error: invalid date/time value:\n", 5, rcx, r8_153, r9, v27);
                rax159 = debug_strfdatetime_constprop_0(rbx18, r14_19, rbp9, rcx);
                rbx18 = rax159;
                rax160 = fun_3580();
                dbg_printf(rax160, rbx18, 5, rcx, r8_153, r9, v27, rax160, rbx18, 5, rcx, r8_153, r9, v27);
                rax161 = debug_strfdatetime_constprop_0(r13_141, r14_19, rbp9, rcx);
                rax162 = fun_3580();
                dbg_printf(rax162, rax161, 5, rcx, r8_153, r9, v27, rax162, rax161, 5, rcx, r8_153, r9, v27);
                rsp163 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp158) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                if (*reinterpret_cast<uint32_t*>(&r9) == *reinterpret_cast<uint32_t*>(&r8_153)) 
                    goto addr_ba91_153; else 
                    goto addr_b884_154;
            } else {
                *reinterpret_cast<uint32_t*>(&rsi25) = v154;
                *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&v12) != *reinterpret_cast<uint32_t*>(&rsi25) || ((*reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&v124), *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0, *reinterpret_cast<uint32_t*>(&v140) == *reinterpret_cast<uint32_t*>(&rsi25)) || ((*reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&v10), *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0, v155 != *reinterpret_cast<uint32_t*>(&rsi25)) || (*reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v119) + 4), *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0, *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v144) + 4) != *reinterpret_cast<uint32_t*>(&rsi25))))) {
                    if (!*reinterpret_cast<signed char*>(&eax156)) 
                        goto addr_a288_65;
                    rax164 = fun_3580();
                    rsp165 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                    rbp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp165) + 0x3e0);
                    dbg_printf(rax164, "error: invalid date/time value:\n", 5, rcx, r8_153, r9, v27, rax164, "error: invalid date/time value:\n", 5, rcx, r8_153, r9, v27);
                    rax166 = debug_strfdatetime_constprop_0(rbx18, r14_19, rbp9, rcx);
                    rbx18 = rax166;
                    rax167 = fun_3580();
                    dbg_printf(rax167, rbx18, 5, rcx, r8_153, r9, v27, rax167, rbx18, 5, rcx, r8_153, r9, v27);
                    rax168 = debug_strfdatetime_constprop_0(r13_141, r14_19, rbp9, rcx);
                    rax169 = fun_3580();
                    dbg_printf(rax169, rax168, 5, rcx, r8_153, r9, v27, rax169, rax168, 5, rcx, r8_153, r9, v27);
                    rsp163 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp165) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_b89d_158;
                } else {
                    if (!*reinterpret_cast<signed char*>(&eax156)) 
                        goto addr_a288_65;
                    rax170 = fun_3580();
                    rsp171 = reinterpret_cast<void*>(rsp23 - 8 + 8);
                    rbp9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp171) + 0x3e0);
                    dbg_printf(rax170, "error: invalid date/time value:\n", 5, rcx, r8_153, r9, v27, rax170, "error: invalid date/time value:\n", 5, rcx, r8_153, r9, v27);
                    rax172 = debug_strfdatetime_constprop_0(rbx18, r14_19, rbp9, rcx);
                    rbx18 = rax172;
                    rax173 = fun_3580();
                    dbg_printf(rax173, rbx18, 5, rcx, r8_153, r9, v27, rax173, rbx18, 5, rcx, r8_153, r9, v27);
                    rax174 = debug_strfdatetime_constprop_0(r13_141, r14_19, rbp9, rcx);
                    rax175 = fun_3580();
                    r13d176 = 1;
                    dbg_printf(rax175, rax174, 5, rcx, r8_153, r9, v27, rax175, rax174, 5, rcx, r8_153, r9, v27);
                    rsp163 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp171) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_a13b_161;
                }
            }
        } else {
            addr_aac0_144:
            if (1) {
                addr_ad1b_162:
                if (!v42) {
                    addr_b0e3_163:
                    rcx = reinterpret_cast<void**>(0);
                    if (1) {
                        if (1) {
                            addr_b135_165:
                            r14_19 = reinterpret_cast<void**>(0);
                            r9 = v71;
                            *reinterpret_cast<uint32_t*>(&rbx18) = 0;
                            r8 = reinterpret_cast<void**>(0);
                            if (__intrinsic()) 
                                goto addr_a288_65;
                            r9_177 = r9;
                            rsi25 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r9_177 - ((__intrinsic() >> 26) - (reinterpret_cast<signed char>(r9_177) >> 63)) * 0x3b9aca00) + 0x3b9aca00 - (__intrinsic() >> 11) * 0x3b9aca00);
                            r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r9_177) - reinterpret_cast<unsigned char>(rsi25)) >> 63);
                            rax178 = __intrinsic() >> 26;
                            eax179 = *reinterpret_cast<int32_t*>(&rax178) - *reinterpret_cast<uint32_t*>(&r9);
                            rcx = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp9)));
                            if (__intrinsic()) 
                                goto addr_a288_65;
                        } else {
                            eax180 = 0;
                            goto addr_b41f_168;
                        }
                        r13_28 = reinterpret_cast<void**>(0);
                        if (__intrinsic()) {
                            goto addr_a288_65;
                        }
                    }
                } else {
                    if (0) {
                        if (0) {
                            addr_b50e_173:
                            rbx181 = reinterpret_cast<void**>(rsp23 + 0x300);
                            goto addr_b516_174;
                        } else {
                            rbx181 = reinterpret_cast<void**>(rsp23 + 0x300);
                            goto addr_ad51_176;
                        }
                    }
                    rbx181 = reinterpret_cast<void**>(rsp23 + 0x300);
                    if (1) {
                        *reinterpret_cast<uint32_t*>(&rax182) = *reinterpret_cast<uint32_t*>(&v119);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax182) + 4) = 0;
                        rdi183 = reinterpret_cast<void**>(rsp23 + 0x2d3);
                        rax184 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v119) + 4) * 0x51eb851f >> 37;
                        eax185 = *reinterpret_cast<int32_t*>(&rax184) - (*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v119) + 4) >> 31);
                        eax186 = eax185 + 19;
                        esi187 = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v119) + 4) - eax185 * 100;
                        *reinterpret_cast<int32_t*>(&rcx188) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx188) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rcx188) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v119) + 4) >= reinterpret_cast<int32_t>(0xfffff894));
                        *reinterpret_cast<uint32_t*>(&r9_189) = -esi187;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_189) + 4) = 0;
                        if (__intrinsic()) {
                            *reinterpret_cast<uint32_t*>(&r9_189) = esi187;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_189) + 4) = 0;
                        }
                        *reinterpret_cast<int32_t*>(&r8_190) = -eax186;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_190) + 4) = 0;
                        if (__intrinsic()) {
                            *reinterpret_cast<int32_t*>(&r8_190) = eax186;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_190) + 4) = 0;
                        }
                        fun_3890(rdi183, 1, 13, rcx188 + reinterpret_cast<int64_t>("-%02d%02d"), r8_190, r9_189);
                        *reinterpret_cast<uint32_t*>(&r11_191) = *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v124) + 4);
                        *reinterpret_cast<int32_t*>(&r11_191 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r10_192) = static_cast<int32_t>(rax182 + 1);
                        *reinterpret_cast<int32_t*>(&r10_192 + 4) = 0;
                        r8 = reinterpret_cast<void**>("(Y-M-D) %s-%02d-%02d");
                        r9 = rdi183;
                        fun_34a0(rbx181, 100, 1, 100, "(Y-M-D) %s-%02d-%02d", rbx181, 100, 1, 100, "(Y-M-D) %s-%02d-%02d");
                        rax193 = fun_3580();
                        dbg_printf(rax193, rbx181, 5, 100, "(Y-M-D) %s-%02d-%02d", r9, r10_192, rax193, rbx181, 5, 100, "(Y-M-D) %s-%02d-%02d", r9, r10_192);
                        rdx142 = r10_192;
                        rcx = r11_191;
                        rsp23 = rsp23 - 8 + 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 + 8 + 8;
                        if (1) 
                            goto addr_ad51_176; else 
                            goto addr_b756_183;
                    } else {
                        addr_ad51_176:
                        rax194 = debug_strfdatetime_constprop_0(r13_141, r14_19, rbx181, rcx, r13_141, r14_19, rbx181, rcx);
                        rax195 = fun_3580();
                        dbg_printf(rax195, rax194, 5, rcx, r8, r9, v27, rax195, rax194, 5, rcx, r8, r9, v27);
                        rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
                        rcx = reinterpret_cast<void**>(0);
                        rsi25 = reinterpret_cast<void**>(0);
                        eax180 = v42;
                        rbx18 = reinterpret_cast<void**>(0);
                        if (1) {
                            addr_b410_184:
                            if (1) {
                                addr_af42_185:
                                if (*reinterpret_cast<signed char*>(&eax180)) {
                                    rdx196 = reinterpret_cast<void**>(rsp23 + 0x300);
                                    rax197 = debug_strfdatetime_constprop_0(r13_141, r14_19, rdx196, rcx, r13_141, r14_19, rdx196, rcx);
                                    rax198 = fun_3580();
                                    rsi25 = rax197;
                                    dbg_printf(rax198, rsi25, rbp9, rcx, r8, r9, v27, rax198, rsi25, rbp9, rcx, r8, r9, v27);
                                    rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
                                    r14_19 = reinterpret_cast<void**>(0);
                                    r9 = v71;
                                    *reinterpret_cast<uint32_t*>(&rbx18) = 0;
                                    r8 = reinterpret_cast<void**>(0);
                                    if (__intrinsic()) {
                                        eax199 = v42;
                                        *reinterpret_cast<unsigned char*>(&v114) = *reinterpret_cast<unsigned char*>(&eax199);
                                        goto addr_b574_188;
                                    } else {
                                        r9_200 = r9;
                                        rsi25 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r9_200 - ((__intrinsic() >> 26) - (reinterpret_cast<signed char>(r9_200) >> 63)) * 0x3b9aca00) + 0x3b9aca00 - (__intrinsic() >> 11) * 0x3b9aca00);
                                        r9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r9_200) - reinterpret_cast<unsigned char>(rsi25)) >> 63);
                                        ecx201 = v42;
                                        *reinterpret_cast<unsigned char*>(&v114) = *reinterpret_cast<unsigned char*>(&ecx201);
                                        rax202 = __intrinsic() >> 26;
                                        eax179 = *reinterpret_cast<int32_t*>(&rax202) - *reinterpret_cast<uint32_t*>(&r9);
                                        rcx = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp9)));
                                        if (__intrinsic() || (r13_28 = reinterpret_cast<void**>(0), __intrinsic())) {
                                            addr_b574_188:
                                            if (!*reinterpret_cast<unsigned char*>(&v114)) 
                                                goto addr_a288_65;
                                        } else {
                                            rcx = rcx;
                                            rbp9 = rcx;
                                            if (!__intrinsic() && ((rcx = reinterpret_cast<void**>(0), rbp9 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp9))), !__intrinsic()) && (rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax179)))), !__intrinsic()))) {
                                                *reinterpret_cast<void***>(v10) = rbp9;
                                                *reinterpret_cast<void***>(v10 + 8) = rsi25;
                                                if (!*reinterpret_cast<unsigned char*>(&v114)) 
                                                    goto addr_b09b_26;
                                                if (0) 
                                                    goto addr_b9d3_24; else 
                                                    goto addr_b09b_26;
                                            }
                                        }
                                        rsi25 = reinterpret_cast<void**>("error: adding relative time caused an overflow\n");
                                        goto addr_a998_114;
                                    }
                                }
                            } else {
                                addr_b41f_168:
                                rdx203 = reinterpret_cast<void**>(static_cast<int64_t>(v204));
                                *reinterpret_cast<int32_t*>(&rdi205) = 0;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi205) + 4) = 0;
                                rbx18 = rdx203;
                                *reinterpret_cast<unsigned char*>(&rdi205) = __intrinsic();
                                *reinterpret_cast<uint32_t*>(&rsi25) = 0;
                                *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) - (reinterpret_cast<unsigned char>(rdx203) - v206));
                                *reinterpret_cast<unsigned char*>(&rsi25) = __intrinsic();
                                rdx207 = rcx;
                                if (rdi205 | reinterpret_cast<unsigned char>(rsi25)) {
                                    if (*reinterpret_cast<signed char*>(&eax180)) {
                                        rax208 = fun_3580();
                                        *reinterpret_cast<uint32_t*>(&rsi25) = *reinterpret_cast<uint32_t*>(&rbx18);
                                        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                                        dbg_printf(rax208, rsi25, 5, rcx, r8, r9, v27, rax208, rsi25, 5, rcx, r8, r9, v27);
                                        rsp23 = rsp23 - 8 + 8 - 8 + 8;
                                        goto addr_a288_65;
                                    }
                                }
                            }
                        } else {
                            if (!*reinterpret_cast<signed char*>(&eax180)) {
                                rax209 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v119) + 4)))));
                                rsi25 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax209)));
                                *reinterpret_cast<uint32_t*>(&rcx) = __intrinsic();
                                *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                                if (rax209 != rsi25) 
                                    goto addr_a288_65;
                                if (*reinterpret_cast<uint32_t*>(&rcx)) 
                                    goto addr_a288_65; else 
                                    goto addr_b12c_199;
                            } else {
                                if (1) {
                                    addr_b63e_201:
                                    if (*reinterpret_cast<uint32_t*>(&v124) != 12) {
                                        rsi25 = reinterpret_cast<void**>("warning: when adding relative days, it is recommended to specify noon\n");
                                        rax210 = fun_3580();
                                        dbg_printf(rax210, "warning: when adding relative days, it is recommended to specify noon\n", 5, 0, r8, r9, v27, rax210, "warning: when adding relative days, it is recommended to specify noon\n", 5, 0, r8, r9, v27);
                                        rsp23 = rsp23 - 8 + 8 - 8 + 8;
                                    }
                                } else {
                                    if (*reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v124) + 4) != 15) {
                                        rax211 = fun_3580();
                                        dbg_printf(rax211, "warning: when adding relative months/years, it is recommended to specify the 15th of the months\n", 5, 0, r8, r9, v27, rax211, "warning: when adding relative months/years, it is recommended to specify the 15th of the months\n", 5, 0, r8, r9, v27);
                                        rsp23 = rsp23 - 8 + 8 - 8 + 8;
                                        rsi25 = reinterpret_cast<void**>(0);
                                    }
                                    if (0) 
                                        goto addr_b63e_201;
                                }
                                edx212 = 0;
                                rax213 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v119) + 4)))));
                                rcx = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax213)));
                                *reinterpret_cast<unsigned char*>(&edx212) = __intrinsic();
                                if (rax213 != rcx) 
                                    goto addr_b1f9_207;
                                if (edx212) 
                                    goto addr_b1f9_207;
                                v214 = *reinterpret_cast<int32_t*>(&rax213);
                                goto addr_ae2f_210;
                            }
                        }
                    }
                }
            } else {
                if (0) {
                    if (!v42) 
                        goto addr_b0e3_163; else 
                        goto addr_b50e_173;
                }
                rcx215 = v216;
                if (!(reinterpret_cast<uint1_t>(rcx215 < 0) | reinterpret_cast<uint1_t>(rcx215 == 0))) {
                    *reinterpret_cast<uint32_t*>(&rax217) = reinterpret_cast<uint1_t>(-1 != v218);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax217) + 4) = 0;
                    rcx215 = rcx215 - rax217;
                }
                rcx = reinterpret_cast<void**>(rcx215 * 7);
                if (__intrinsic()) 
                    goto addr_ab78_216;
                eax219 = v220 + 1 + 7;
                rdx221 = eax219 * 0xffffffff92492493 >> 32;
                *reinterpret_cast<uint32_t*>(&rdx222) = (*reinterpret_cast<int32_t*>(&rdx221) + eax219 >> 2) - reinterpret_cast<uint32_t>(eax219 >> 31);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx222) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rsi25) = static_cast<int32_t>(rdx222 * 8) - *reinterpret_cast<uint32_t*>(&rdx222);
                *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                if (__intrinsic()) 
                    goto addr_ab78_216; else 
                    goto addr_ab50_218;
            }
        }
    }
    addr_b7b6_219:
    rax223 = fun_3580();
    rsi25 = rbp9;
    dbg_printf(rax223, rsi25, 5, rcx, r8, r9, v27, rax223, rsi25, 5, rcx, r8, r9, v27);
    rsp23 = rsp23 - 8 + 8 - 8 + 8;
    goto addr_a288_65;
    addr_ba91_153:
    addr_b8a0_220:
    if (*reinterpret_cast<uint32_t*>(&v140) != *reinterpret_cast<uint32_t*>(&v124)) {
    }
    r13d176 = 0;
    if (*reinterpret_cast<uint32_t*>(&v12) != v154) {
        addr_a142_223:
        if (v155 == *reinterpret_cast<uint32_t*>(&v10)) {
        }
    } else {
        goto addr_a13b_161;
    }
    r9 = reinterpret_cast<void**>("----");
    if (*reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v144) + 4) == *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v119) + 4)) {
        r9 = reinterpret_cast<void**>(0x15e39);
    }
    *reinterpret_cast<int32_t*>(&rdx224) = 1;
    *reinterpret_cast<int32_t*>(&rdx224 + 4) = 0;
    eax225 = fun_34a0(rbp9, 100, 1, 100, "                                 %4s %2s %2s %2s %2s %2s", rbp9, 100, 1, 100, "                                 %4s %2s %2s %2s %2s %2s");
    rsp226 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp163) - 8 - 8 - 8 - 8 - 8 - 8 - 8 + 8 + 48);
    if (reinterpret_cast<int32_t>(eax225) < reinterpret_cast<int32_t>(0)) {
        addr_a1d0_229:
        dbg_printf("%s\n", rbp9, rdx224, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, "%s\n", rbp9, rdx224, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
        rax227 = fun_3580();
        dbg_printf(rax227, "     possible reasons:\n", 5, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, rax227, "     possible reasons:\n", 5, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
        rsp228 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp226) - 8 + 8 - 8 + 8 - 8 + 8);
        if (r13d176) {
            rax229 = fun_3580();
            dbg_printf(rax229, "       non-existing due to daylight-saving time;\n", 5, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, rax229, "       non-existing due to daylight-saving time;\n", 5, 100, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
            rsp228 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp228) - 8 + 8 - 8 + 8);
        }
    } else {
        if (reinterpret_cast<int32_t>(eax225) > reinterpret_cast<int32_t>(99)) {
            eax225 = 99;
        }
        rax230 = reinterpret_cast<void*>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax225)));
        do {
            rdx224 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax230)));
            if (!rax230) 
                goto addr_a1c6_235;
            rax230 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax230) - 1);
        } while (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<uint64_t>(rax230)) == 32);
        goto addr_a1c8_237;
    }
    *reinterpret_cast<uint32_t*>(&rcx) = v154;
    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&v12) != *reinterpret_cast<uint32_t*>(&rcx) && (*reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(&v10), *reinterpret_cast<int32_t*>(&rcx + 4) = 0, v155 != *reinterpret_cast<uint32_t*>(&rcx))) {
        rax231 = fun_3580();
        dbg_printf(rax231, "       invalid day/month combination;\n", 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, rax231, "       invalid day/month combination;\n", 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
        rsp228 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp228) - 8 + 8 - 8 + 8);
    }
    rax232 = fun_3580();
    dbg_printf(rax232, "       numeric values overflow;\n", 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, rax232, "       numeric values overflow;\n", 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
    rsp233 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp228) - 8 + 8 - 8 + 8);
    if (1) {
        rax234 = fun_3580();
        rsp235 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp233) - 8 + 8);
        rsi25 = rax234;
    } else {
        rax236 = fun_3580();
        rsp235 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp233) - 8 + 8);
        rsi25 = rax236;
    }
    dbg_printf("       %s\n", rsi25, 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27, "       %s\n", rsi25, 5, rcx, "                                 %4s %2s %2s %2s %2s %2s", r9, v27);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp235) - 8 + 8);
    goto addr_a288_65;
    addr_a1c6_235:
    *reinterpret_cast<int32_t*>(&rdx224) = 0;
    *reinterpret_cast<int32_t*>(&rdx224 + 4) = 0;
    addr_a1c8_237:
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp226) + reinterpret_cast<unsigned char>(rdx224) + 0x3e0) = 0;
    goto addr_a1d0_229;
    addr_a13b_161:
    goto addr_a142_223;
    addr_b884_154:
    if (*reinterpret_cast<uint32_t*>(&rcx) == *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v129) + 4)) {
        goto addr_b8a0_220;
    } else {
        addr_b89d_158:
        goto addr_b8a0_220;
    }
    addr_b756_183:
    if (0) {
        addr_b516_174:
        rsi237 = reinterpret_cast<void**>(rsp23 + 0x3e0);
        rax238 = str_days_constprop_0(r14_19, rsi237, rdx142, rcx, r8, r14_19, rsi237, rdx142, rcx, r8);
        rax239 = fun_3580();
        dbg_printf(rax239, rax238, 5, rcx, r8, r9, v27, rax239, rax238, 5, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8 - 8 + 8 - 8 + 8;
        goto addr_ad51_176;
    } else {
        goto addr_ad51_176;
    }
    addr_af3f_246:
    rbp9 = rdx207;
    goto addr_af42_185;
    addr_b12c_199:
    v214 = *reinterpret_cast<int32_t*>(&rax209);
    addr_ae2f_210:
    rax240 = reinterpret_cast<void**>(static_cast<uint64_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&v119)))));
    rcx = rax240;
    v241 = rax240;
    if (!reinterpret_cast<int1_t>(rcx == static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rcx))) || (__intrinsic() || ((rbx18 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v124) + 4))))), !reinterpret_cast<int1_t>(rbx18 == static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbx18)))) || __intrinsic()))) {
        addr_b1f9_207:
        if (v42) {
            rax242 = fun_3580();
            rsi25 = reinterpret_cast<void**>("parse-datetime.y");
            dbg_printf(rax242, "parse-datetime.y", 0x863, rcx, r8, r9, v27, rax242, "parse-datetime.y", 0x863, rcx, r8, r9, v27);
            rsp23 = rsp23 - 8 + 8 - 8 + 8;
            goto addr_a288_65;
        }
    } else {
        rsi25 = r13_141;
        v37 = eax138;
        rax243 = mktime_z(r15_24, rsi25, r15_24, rsi25);
        rsp23 = rsp23 - 8 + 8;
        rbp9 = rax243;
        if (1) {
            if (!v42) 
                goto addr_a288_65;
            rdx244 = reinterpret_cast<void**>(rsp23 + 0x300);
            rax245 = debug_strfdatetime_constprop_0(r13_141, r14_19, rdx244, rcx, r13_141, r14_19, rdx244, rcx);
            rsp23 = rsp23 - 8 + 8;
            rbp9 = rax245;
            goto addr_b7b6_219;
        } else {
            eax180 = v42;
            if (*reinterpret_cast<signed char*>(&eax180)) {
                r9 = reinterpret_cast<void**>(0);
                r8 = reinterpret_cast<void**>(0);
                rax246 = fun_3580();
                rcx = reinterpret_cast<void**>(0);
                dbg_printf(rax246, 0, 0, 0, 0, 0, v27, rax246, 0, 0, 0, 0, 0, v27);
                rsp247 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
                rax248 = debug_strfdatetime_constprop_0(r13_141, r14_19, reinterpret_cast<int64_t>(rsp247) + 0x300, 0);
                rax249 = fun_3580();
                rsi25 = rax248;
                dbg_printf(rax249, rsi25, 5, 0, 0, 0, v27, rax249, rsi25, 5, 0, 0, 0, v27);
                rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp247) - 8 + 8 - 8 + 8 - 8 + 8);
                if (eax138 != -1 && eax138 != v37) {
                    rsi25 = reinterpret_cast<void**>("warning: daylight saving time changed after date adjustment\n");
                    rax250 = fun_3580();
                    dbg_printf(rax250, "warning: daylight saving time changed after date adjustment\n", 5, 0, 0, 0, v27, rax250, "warning: daylight saving time changed after date adjustment\n", 5, 0, 0, 0, v27);
                    rsp23 = rsp23 - 8 + 8 - 8 + 8;
                }
                if (!0 && (*reinterpret_cast<uint32_t*>(&rbx18) != *reinterpret_cast<uint32_t*>(&rbx18) || !0 && *reinterpret_cast<int32_t*>(&v241) != *reinterpret_cast<int32_t*>(&v241))) {
                    rax251 = fun_3580();
                    dbg_printf(rax251, "warning: month/year adjustment resulted in shifted dates:\n", 5, 0, 0, 0, v27, rax251, "warning: month/year adjustment resulted in shifted dates:\n", 5, 0, 0, 0, v27);
                    rsp252 = reinterpret_cast<void*>(rsp23 - 8 + 8 - 8 + 8);
                    r8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp252) + 0x2d3);
                    rax253 = tm_year_str(v214, r8, v214, r8);
                    rax254 = fun_3580();
                    *reinterpret_cast<uint32_t*>(&rcx255) = *reinterpret_cast<uint32_t*>(&rbx18);
                    *reinterpret_cast<int32_t*>(&rcx255 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdx256) = *reinterpret_cast<int32_t*>(&v241) + 1;
                    *reinterpret_cast<int32_t*>(&rdx256 + 4) = 0;
                    dbg_printf(rax254, rax253, rdx256, rcx255, r8, 0, v27, rax254, rax253, rdx256, rcx255, r8, 0, v27);
                    *reinterpret_cast<int32_t*>(&rax257) = *reinterpret_cast<int32_t*>(&v241);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax257) + 4) = 0;
                    rax258 = tm_year_str(v214, r8);
                    rax259 = fun_3580();
                    *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<uint32_t*>(&rbx18);
                    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                    rsi25 = rax258;
                    *reinterpret_cast<int32_t*>(&rdx260) = static_cast<int32_t>(rax257 + 1);
                    *reinterpret_cast<int32_t*>(&rdx260 + 4) = 0;
                    dbg_printf(rax259, rsi25, rdx260, rcx, r8, 0, v27, rax259, rsi25, rdx260, rcx, r8, 0, v27);
                    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp252) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                }
                eax180 = v42;
                goto addr_b410_184;
            } else {
                if (1) 
                    goto addr_b135_165;
                *reinterpret_cast<int32_t*>(&rdi261) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi261) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdi261) = __intrinsic();
                *reinterpret_cast<uint32_t*>(&rsi25) = 0;
                *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
                rcx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) - (v262 - v263));
                *reinterpret_cast<unsigned char*>(&rsi25) = __intrinsic();
                rdx207 = rcx;
                if (rdi261 | reinterpret_cast<unsigned char>(rsi25)) 
                    goto addr_a288_65; else 
                    goto addr_af3f_246;
            }
        }
    }
    addr_ab78_216:
    if (v42) {
        rdx264 = reinterpret_cast<void**>(rsp23 + 0x300);
        rax265 = debug_strfdatetime_constprop_0(r13_141, r14_19, rdx264, rcx, r13_141, r14_19, rdx264, rcx);
        rsp266 = reinterpret_cast<void*>(rsp23 - 8 + 8);
        r9 = v267;
        rbx18 = rax265;
        v12 = r9;
        rax268 = str_days_constprop_0(r14_19, reinterpret_cast<int64_t>(rsp266) + 0x3e0, rdx264, rcx, r8);
        rbp9 = rax268;
        rax269 = fun_3580();
        *reinterpret_cast<uint32_t*>(&rcx) = v270;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rsi25 = rbp9;
        dbg_printf(rax269, rsi25, v12, rcx, rbx18, r9, v27, rax269, rsi25, v12, rcx, rbx18, r9, v27);
        rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp266) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_a288_65;
    }
    addr_ab50_218:
    rax271 = static_cast<int64_t>(reinterpret_cast<int32_t>(eax219 - *reinterpret_cast<uint32_t*>(&rsi25))) + reinterpret_cast<unsigned char>(rcx) + reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v124) + 4)));
    *reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(&v124) + 4) = *reinterpret_cast<uint32_t*>(&rax271);
    edx272 = __intrinsic();
    *reinterpret_cast<uint32_t*>(&rcx) = 1;
    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
    if (rax271 != static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax271))) {
        edx272 = 1;
    }
    if (edx272) 
        goto addr_ab78_216;
    v37 = -1;
    rax273 = mktime_z(r15_24, r13_141);
    rsp23 = rsp23 - 8 + 8;
    *reinterpret_cast<uint32_t*>(&rsi25) = 0xffffffff;
    *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
    rbp9 = rax273;
    if (1) 
        goto addr_ab78_216;
    if (!v42) 
        goto addr_b0e3_163;
    rdx274 = reinterpret_cast<void**>(rsp23 + 0x300);
    rax275 = debug_strfdatetime_constprop_0(r13_141, r14_19, rdx274, 1, r13_141, r14_19, rdx274, 1);
    rsp276 = reinterpret_cast<void*>(rsp23 - 8 + 8);
    rbx18 = rax275;
    rax277 = str_days_constprop_0(r14_19, reinterpret_cast<int64_t>(rsp276) + 0x3e0, rdx274, 1, r8);
    rax278 = fun_3580();
    rsi25 = rax277;
    rdx142 = rbx18;
    dbg_printf(rax278, rsi25, rdx142, 1, r8, r9, v27, rax278, rsi25, rdx142, 1, r8, r9, v27);
    rsp23 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp276) - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_ad1b_162;
    addr_9f2e_130:
    v37 = -1;
    goto addr_9f39_125;
    addr_b4a5_132:
    if (*reinterpret_cast<unsigned char*>(&rsi25)) {
        rax279 = fun_3580();
        rsi25 = rbp9;
        dbg_printf(rax279, rsi25, r13_127, rcx, r8, r9, v27, rax279, rsi25, r13_127, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8 - 8 + 8;
        goto addr_a288_65;
    }
    addr_9f00_142:
    v129 = 0;
    *reinterpret_cast<uint32_t*>(&v124) = 0;
    v71 = reinterpret_cast<void**>(0);
    if (*reinterpret_cast<unsigned char*>(&rsi25)) {
        dbg_printf("warning: using midnight as starting time: 00:00:00\n", rsi25, rdx125, rcx, r8, r9, v27, "warning: using midnight as starting time: 00:00:00\n", rsi25, rdx125, rcx, r8, r9, v27);
        rsp23 = rsp23 - 8 + 8;
        goto addr_b59e_143;
    } else {
        *reinterpret_cast<uint32_t*>(&r9) = 0;
        *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        r11d130 = 0;
        edi131 = 0;
        goto addr_9f2e_130;
    }
    addr_a8c6_103:
    if (reinterpret_cast<int64_t>(rax112) >= reinterpret_cast<int64_t>(0)) {
        addr_9e48_268:
        if (rax112 != static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax112))) {
            edx118 = 1;
        }
    } else {
        addr_a8cf_270:
        edx118 = 1;
        goto addr_9e48_268;
    }
    *reinterpret_cast<uint32_t*>(&rcx) = edx118 & 1;
    *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
    *reinterpret_cast<unsigned char*>(&v114) = *reinterpret_cast<unsigned char*>(&rcx);
    goto addr_9e5f_105;
    addr_9e3f_104:
    if (reinterpret_cast<int64_t>(rax112) >= reinterpret_cast<int64_t>(0)) 
        goto addr_a8cf_270; else 
        goto addr_9e48_268;
    addr_a743_78:
    rcx = rsi25;
    fun_3830(", dst", ", dst");
    rsp104 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp104) - 8 + 8);
    goto addr_a760_73;
    addr_a9d4_47:
    v89 = v92;
    goto addr_9d65_49;
    addr_a338_16:
    v27 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(rsp23 + 0x370);
    if (reinterpret_cast<signed char>(rdi31) > reinterpret_cast<signed char>(100)) {
        v12 = r8;
        rax280 = fun_3730(rdi31, "TZ=\"");
        rsp23 = rsp23 - 8 + 8;
        r8 = v12;
        v27 = rax280;
        if (!rax280) 
            goto addr_a520_10;
        v12 = v27;
    }
    rdx281 = v12;
    if (*reinterpret_cast<signed char*>(&r15_24) != 34) {
        do {
            *reinterpret_cast<int32_t*>(&rax282) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax282) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rax282) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&r15_24) == 92);
            ++rdx281;
            rax283 = reinterpret_cast<struct s10*>(reinterpret_cast<int64_t>(rax282) + reinterpret_cast<unsigned char>(r8));
            *reinterpret_cast<uint32_t*>(&rcx) = rax283->f0;
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r15_24) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax283->f1));
            r8 = reinterpret_cast<void**>(&rax283->f1);
            *reinterpret_cast<unsigned char*>(rdx281 + 0xffffffffffffffff) = *reinterpret_cast<unsigned char*>(&rcx);
        } while (*reinterpret_cast<signed char*>(&r15_24) != 34);
    }
    *reinterpret_cast<void***>(rdx281) = reinterpret_cast<void**>(0);
    rax284 = tzalloc(v12, "TZ=\"");
    rsp23 = rsp23 - 8 + 8;
    r15_24 = rax284;
    if (!rax284) {
        *reinterpret_cast<uint32_t*>(&r13_28) = 0;
        *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
        goto addr_a298_22;
    } else {
        r8 = r8;
        r14_19 = r8 + 1;
        while (1) {
            eax285 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_19));
            if (*reinterpret_cast<signed char*>(&eax285) > 13) {
                if (*reinterpret_cast<signed char*>(&eax285) != 32) 
                    break;
            } else {
                if (*reinterpret_cast<signed char*>(&eax285) <= 8) 
                    break;
            }
            ++r14_19;
        }
        rsi25 = r13_7;
        rax286 = localtime_rz(r15_24, rsi25, rsp23 + 0xf0, rcx, r8, r9);
        rsp23 = rsp23 - 8 + 8;
        if (rax286) 
            goto addr_9b2f_12;
    }
    goto addr_a288_65;
    addr_9b01_20:
    goto addr_9b08_9;
}

struct s11 {
    signed char[20] pad20;
    void** f14;
};

struct s12 {
    signed char[20] pad20;
    void** f14;
};

int64_t fun_3700();

struct s12* fun_34c0(void* rdi);

signed char year(struct s11* rdi, void** rsi, int64_t rdx, uint32_t ecx) {
    struct s0* rax5;
    void** edx6;
    struct s12* rax7;
    void* rdx8;

    rax5 = g28;
    if (rdx == 1) {
        edx6 = *reinterpret_cast<void***>(rsi);
        *reinterpret_cast<int32_t*>(&rax7) = 1;
        rdi->f14 = edx6;
        if (reinterpret_cast<signed char>(edx6) <= reinterpret_cast<signed char>(68)) {
            if (ecx & 8) {
                addr_bbd0_4:
                *reinterpret_cast<int32_t*>(&rax7) = 0;
            } else {
                rdi->f14 = edx6 + 100;
            }
        }
    } else {
        if (rdx == 2) {
            if (!(ecx & 2)) 
                goto addr_bbd0_4;
            rdi->f14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi)) * 100 + reinterpret_cast<unsigned char>(*reinterpret_cast<void**>(rsi + 4)) - 0x76c);
            *reinterpret_cast<int32_t*>(&rax7) = 1;
        } else {
            fun_3700();
            rax7 = fun_34c0(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16 - 8 + 8);
            if (rax7) {
                rdi->f14 = rax7->f14;
                *reinterpret_cast<int32_t*>(&rax7) = 1;
            }
        }
    }
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<int64_t>(g28));
    if (rdx8) {
        fun_35b0();
    } else {
        return *reinterpret_cast<signed char*>(&rax7);
    }
}

int64_t fun_3590();

int64_t fun_34d0(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_3590();
    if (r8d > 10) {
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x17080 + rax11 * 4) + 0x17080;
    }
}

struct s13 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_3640(void** rdi, ...);

struct s14 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s13* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    void*** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    void** rdi15;
    int64_t rax16;
    uint32_t r8d17;
    struct s14* rbx18;
    uint32_t r15d19;
    void** rsi20;
    void** r14_21;
    int64_t v22;
    int64_t v23;
    uint32_t r15d24;
    void** rax25;
    void** rsi26;
    void** rax27;
    uint32_t r8d28;
    int64_t v29;
    int64_t v30;
    void* rax31;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0xd49f;
    rax8 = fun_34e0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
        fun_34d0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x1c090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xea51]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            rdi15 = reinterpret_cast<void**>((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            v7 = 0xd52b;
            fun_3640(rdi15, rdi15);
            rax16 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax16);
        }
        r8d17 = rcx->f0;
        rbx18 = reinterpret_cast<struct s14*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d19 = rcx->f4;
        rsi20 = rbx18->f0;
        r14_21 = rbx18->f8;
        v22 = rcx->f30;
        v23 = rcx->f28;
        r15d24 = r15d19 | 1;
        rax25 = quotearg_buffer_restyled(r14_21, rsi20, rsi, rdx, r8d17, r15d24, &rcx->f8, v23, v22, v7);
        if (reinterpret_cast<unsigned char>(rsi20) <= reinterpret_cast<unsigned char>(rax25)) {
            rsi26 = rax25 + 1;
            rbx18->f0 = rsi26;
            if (r14_21 != 0x1c120) {
                fun_34b0(r14_21, r14_21);
                rsi26 = rsi26;
            }
            rax27 = xcharalloc(rsi26, rsi26);
            r8d28 = rcx->f0;
            rbx18->f8 = rax27;
            v29 = rcx->f30;
            r14_21 = rax27;
            v30 = rcx->f28;
            quotearg_buffer_restyled(rax27, rsi26, rsi, rdx, r8d28, r15d24, rsi26, v30, v29, 0xd5ba);
        }
        *rax8 = v10;
        rax31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax6) - reinterpret_cast<int64_t>(g28));
        if (rax31) {
            fun_35b0();
        } else {
            return r14_21;
        }
    }
}

void** fun_3490(int64_t rdi);

int32_t fun_3800(int64_t rdi, void** rsi);

int32_t fun_3540(int64_t rdi, void** rsi, int64_t rdx);

void fun_36f0(int64_t rdi, void** rsi, int64_t rdx);

void** set_tz(void** rdi) {
    void** rax2;
    void** rsi3;
    int32_t eax4;
    void** rax5;
    void** r12_6;
    int32_t eax7;
    int32_t eax8;
    void*** rax9;
    void** ebx10;
    void*** rbp11;
    void** rdi12;

    rax2 = fun_3490("TZ");
    if (!rax2) {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            return 1;
        }
    } else {
        if (*reinterpret_cast<void***>(rdi + 8) && (rsi3 = rax2, eax4 = fun_36b0(rdi + 9, rsi3), !eax4)) {
            return 1;
        }
    }
    rax5 = tzalloc(rax2, rsi3);
    r12_6 = rax5;
    if (!rax5) {
        addr_e5a1_7:
        return r12_6;
    } else {
        if (!*reinterpret_cast<void***>(rdi + 8)) {
            eax7 = fun_3800("TZ", rsi3);
            if (eax7) 
                goto addr_e60d_10; else 
                goto addr_e59c_11;
        }
        rsi3 = rdi + 9;
        eax8 = fun_3540("TZ", rsi3, 1);
        if (!eax8) {
            addr_e59c_11:
            fun_36f0("TZ", rsi3, 1);
            goto addr_e5a1_7;
        } else {
            addr_e60d_10:
            rax9 = fun_34e0();
            ebx10 = *rax9;
            rbp11 = rax9;
            if (r12_6 != 1) {
                do {
                    rdi12 = r12_6;
                    r12_6 = *reinterpret_cast<void***>(r12_6);
                    fun_34b0(rdi12, rdi12);
                } while (r12_6);
            }
        }
    }
    *rbp11 = ebx10;
    return 0;
}

void** fun_36e0(void** rdi, void** rsi, void** rdx, ...);

signed char save_abbr(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void** r13_5;
    int32_t eax6;
    void** rbx7;
    void** rsi8;
    int32_t eax9;
    void** rax10;
    int32_t eax11;
    void** rax12;
    void** rdx13;
    void** rax14;

    r12_3 = *reinterpret_cast<void***>(rsi + 48);
    if (!r12_3) {
        return 1;
    }
    rbp4 = rdi;
    r13_5 = rsi;
    if (reinterpret_cast<unsigned char>(rsi) > reinterpret_cast<unsigned char>(r12_3) || (eax6 = 1, reinterpret_cast<unsigned char>(r12_3) >= reinterpret_cast<unsigned char>(rsi + 56))) {
        rbx7 = rbp4 + 9;
        if (!*reinterpret_cast<void***>(r12_3)) {
            rbx7 = reinterpret_cast<void**>(0x15e39);
        } else {
            while (rsi8 = r12_3, eax9 = fun_36b0(rbx7, rsi8), !!eax9) {
                do {
                    if (*reinterpret_cast<void***>(rbx7)) 
                        goto addr_e483_9;
                    if (rbx7 != rbp4 + 9) 
                        goto addr_e4f0_11;
                    if (!*reinterpret_cast<void***>(rbp4 + 8)) 
                        goto addr_e4f0_11;
                    addr_e483_9:
                    rax10 = fun_35a0(rbx7, rbx7);
                    rbx7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<unsigned char>(rax10) + 1);
                    if (*reinterpret_cast<void***>(rbx7)) 
                        break;
                    if (!*reinterpret_cast<void***>(rbp4)) 
                        break;
                    rbx7 = *reinterpret_cast<void***>(rbp4) + 9;
                    rsi8 = r12_3;
                    rbp4 = *reinterpret_cast<void***>(rbp4);
                    eax11 = fun_36b0(rbx7, rsi8);
                } while (eax11);
                goto addr_e4b4_15;
            }
        }
    } else {
        addr_e4c1_16:
        return *reinterpret_cast<signed char*>(&eax6);
    }
    addr_e4b8_17:
    *reinterpret_cast<void***>(r13_5 + 48) = rbx7;
    eax6 = 1;
    goto addr_e4c1_16;
    addr_e4f0_11:
    rax12 = fun_35a0(r12_3, r12_3);
    rdx13 = rax12 + 1;
    if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbp4 + 0x80) - reinterpret_cast<unsigned char>(rbx7)) <= reinterpret_cast<signed char>(rdx13)) {
        rax14 = tzalloc(r12_3, rsi8);
        *reinterpret_cast<void***>(rbp4) = rax14;
        if (!rax14) {
            eax6 = 0;
            goto addr_e4c1_16;
        } else {
            *reinterpret_cast<void***>(rax14 + 8) = reinterpret_cast<void**>(0);
            rbx7 = rax14 + 9;
            goto addr_e4b8_17;
        }
    } else {
        fun_36e0(rbx7, r12_3, rdx13);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx7) + reinterpret_cast<unsigned char>(rax12) + 1) = 0;
        goto addr_e4b8_17;
    }
    addr_e4b4_15:
    goto addr_e4b8_17;
}

/* revert_tz.part.0 */
signed char revert_tz_part_0(void** rdi, void** rsi) {
    void** rbx3;
    void*** rax4;
    void** r12d5;
    void*** rbp6;
    int32_t eax7;
    int32_t r13d8;
    void** rdi9;
    int32_t eax10;
    int32_t eax11;

    rbx3 = rdi;
    rax4 = fun_34e0();
    r12d5 = *rax4;
    rbp6 = rax4;
    if (*reinterpret_cast<void***>(rbx3 + 8)) {
        rsi = rbx3 + 9;
        eax7 = fun_3540("TZ", rsi, 1);
        if (eax7) {
            addr_e31e_3:
            r12d5 = *rbp6;
            r13d8 = 0;
        } else {
            addr_e369_4:
            fun_36f0("TZ", rsi, 1);
            r13d8 = 1;
        }
        do {
            rdi9 = rbx3;
            rbx3 = *reinterpret_cast<void***>(rbx3);
            fun_34b0(rdi9, rdi9);
        } while (rbx3);
        *rbp6 = r12d5;
        eax10 = r13d8;
        return *reinterpret_cast<signed char*>(&eax10);
    } else {
        eax11 = fun_3800("TZ", rsi);
        if (!eax11) 
            goto addr_e369_4; else 
            goto addr_e31e_3;
    }
}

uint64_t ydhms_diff(int64_t rdi, int64_t rsi, int32_t edx, uint32_t ecx, int32_t r8d, int32_t r9d, int32_t a7, int32_t a8, int32_t a9, int32_t a10) {
    int64_t rcx11;
    uint64_t rcx12;
    int64_t rax13;
    int64_t rbx14;
    int64_t rdx15;
    int64_t rax16;
    int64_t r9_17;
    int64_t r9_18;
    int64_t rbp19;
    int64_t rdi20;
    int32_t ecx21;
    int64_t rdx22;
    uint32_t edx23;
    int64_t rbp24;
    int32_t r12d25;
    int64_t rdx26;
    int64_t rcx27;
    uint32_t ecx28;
    int64_t rcx29;
    int64_t rdx30;
    int64_t rax31;
    int64_t rax32;
    int64_t rax33;

    rcx11 = rdi;
    *reinterpret_cast<uint32_t*>(&rcx12) = *reinterpret_cast<uint32_t*>(&rcx11) & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx12) + 4) = 0;
    rax13 = rdi >> 2;
    rbx14 = r9d;
    rdx15 = rbx14;
    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<uint32_t*>(&rax13) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax13) < 0x1db - reinterpret_cast<uint1_t>(rcx12 < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
    r9_17 = rbx14 >> 2;
    *reinterpret_cast<uint32_t*>(&r9_18) = *reinterpret_cast<uint32_t*>(&r9_17) - (0x1db - reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_17) < 0x1db - reinterpret_cast<uint1_t>((*reinterpret_cast<uint32_t*>(&rdx15) & 3) < 1)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_18) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbp19) = *reinterpret_cast<uint32_t*>(&rax16) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp19) + 4) = 0;
    rdi20 = rdi - rbx14;
    ecx21 = static_cast<int32_t>(rbp19 + rax16);
    rdx22 = ecx21 * 0x51eb851f >> 35;
    edx23 = *reinterpret_cast<int32_t*>(&rdx22) - (ecx21 >> 31) - *reinterpret_cast<uint32_t*>(&rbp19);
    *reinterpret_cast<uint32_t*>(&rbp24) = *reinterpret_cast<uint32_t*>(&r9_18) >> 31;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp24) + 4) = 0;
    r12d25 = static_cast<int32_t>(rbp24 + r9_18);
    rdx26 = static_cast<int64_t>(reinterpret_cast<int32_t>(edx23)) >> 2;
    rcx27 = r12d25 * 0x51eb851f >> 35;
    ecx28 = *reinterpret_cast<int32_t*>(&rcx27) - (r12d25 >> 31) - *reinterpret_cast<uint32_t*>(&rbp24);
    rcx29 = static_cast<int64_t>(reinterpret_cast<int32_t>(ecx28)) >> 2;
    rdx30 = rdi20 + (rdi20 + rdi20 * 8) * 8;
    rax31 = reinterpret_cast<int32_t>(*reinterpret_cast<uint32_t*>(&rax16) - *reinterpret_cast<uint32_t*>(&r9_18) - (edx23 - ecx28) + (*reinterpret_cast<int32_t*>(&rdx26) - *reinterpret_cast<uint32_t*>(&rcx29))) + (rdx30 + rdx30 * 4 + rsi - a7);
    rax32 = edx + (rax31 + rax31 * 2) * 8 - a8;
    rax33 = reinterpret_cast<int32_t>(ecx) + ((rax32 << 4) - rax32) * 4 - a9;
    return r8d + ((rax33 << 4) - rax33) * 4 - reinterpret_cast<uint64_t>(static_cast<int64_t>(a10));
}

struct s15 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    int32_t f18;
    int32_t f1c;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
    int64_t f30;
};

struct s15* ranged_convert(int64_t rdi, uint64_t* rsi, struct s15* rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t rbx7;
    uint64_t r12_8;
    uint64_t* v9;
    void* rbp10;
    struct s0* rax11;
    struct s0* v12;
    struct s15* rax13;
    struct s15* v14;
    void*** rax15;
    int1_t zf16;
    void*** v17;
    uint64_t rcx18;
    int64_t rcx19;
    uint64_t r13_20;
    void* rax21;
    int32_t v22;
    uint64_t r14_23;
    uint64_t r12_24;
    uint64_t r13_25;
    struct s15* r15_26;
    int64_t rax27;
    int32_t v28;
    int32_t v29;
    int32_t v30;
    int32_t v31;
    int32_t v32;
    int32_t v33;
    int32_t v34;
    int32_t v35;
    int64_t v36;
    int64_t v37;
    uint64_t rax38;
    uint64_t rax39;

    rbx7 = rdi;
    r12_8 = *rsi;
    v9 = rsi;
    rbp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68 + 80);
    rax11 = g28;
    v12 = rax11;
    rax13 = reinterpret_cast<struct s15*>(rbx7(rbp10, rdx));
    v14 = rax13;
    if (!rax13) {
        rax15 = fun_34e0();
        zf16 = reinterpret_cast<int1_t>(*rax15 == 75);
        v17 = rax15;
        if (!zf16 || ((rcx18 = r12_8, *reinterpret_cast<uint32_t*>(&rcx19) = *reinterpret_cast<uint32_t*>(&rcx18) & 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0, r13_20 = rcx19 + reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_8) >> 1), r12_8 == r13_20) || !r13_20)) {
            addr_f6ae_3:
            rax21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v12) - reinterpret_cast<int64_t>(g28));
            if (rax21) {
                fun_35b0();
            } else {
                return v14;
            }
        } else {
            v22 = -1;
            r14_23 = r12_8;
            r12_24 = r13_20;
            r13_25 = 0;
            r15_26 = rdx;
            do {
                rax27 = reinterpret_cast<int64_t>(rbx7(rbp10, r15_26));
                if (rax27) {
                    r13_25 = r12_24;
                    v22 = r15_26->f0;
                    v28 = r15_26->f4;
                    v29 = r15_26->f8;
                    v30 = r15_26->fc;
                    v31 = r15_26->f10;
                    v32 = r15_26->f14;
                    v33 = r15_26->f18;
                    v34 = r15_26->f1c;
                    v35 = r15_26->f20;
                    v36 = r15_26->f28;
                    v37 = r15_26->f30;
                } else {
                    if (!reinterpret_cast<int1_t>(*v17 == 75)) 
                        goto addr_f6ae_3;
                    r14_23 = r12_24;
                }
                rax38 = r13_25 | r14_23;
                *reinterpret_cast<uint32_t*>(&rax39) = *reinterpret_cast<uint32_t*>(&rax38) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
                r12_24 = (reinterpret_cast<int64_t>(r13_25) >> 1) + (reinterpret_cast<int64_t>(r14_23) >> 1) + rax39;
            } while (r12_24 != r13_25 && r12_24 != r14_23);
        }
        if (v22 >= 0) {
            v14 = r15_26;
            *v9 = r13_25;
            r15_26->f0 = v22;
            r15_26->f4 = v28;
            r15_26->f8 = v29;
            r15_26->fc = v30;
            r15_26->f10 = v31;
            r15_26->f14 = v32;
            r15_26->f18 = v33;
            r15_26->f1c = v34;
            r15_26->f20 = v35;
            r15_26->f28 = v36;
            r15_26->f30 = v37;
            goto addr_f6ae_3;
        }
    } else {
        *rsi = r12_8;
        goto addr_f6ae_3;
    }
}

void* fun_37c0(void** rdi, ...);

struct s16 {
    signed char[1] pad1;
    void** f1;
};

uint32_t** fun_3870(void** rdi, ...);

/* __strftime_internal.isra.0 */
void** __strftime_internal_isra_0(void** rdi, void** rsi, void** rdx, void** rcx, unsigned char r8b, void** r9d, int32_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11) {
    void** r14_12;
    void** rbp13;
    void** rbx14;
    void** v15;
    void** r15_16;
    void** v17;
    unsigned char v18;
    struct s0* rax19;
    struct s0* v20;
    void*** rax21;
    void* rsp22;
    void** r11_23;
    void** r10d24;
    void*** v25;
    void** v26;
    uint32_t eax27;
    void** r13_28;
    void* rax29;
    uint32_t edx30;
    unsigned char v31;
    void** rdi32;
    void** r8_33;
    int64_t rax34;
    uint64_t rax35;
    int32_t eax36;
    void** rbp37;
    void* rax38;
    void* rcx39;
    uint32_t ecx40;
    int64_t r9_41;
    void** rcx42;
    int64_t r9_43;
    void** rbp44;
    void** v45;
    unsigned char* rbp46;
    uint32_t** rax47;
    void** r8_48;
    int1_t cf49;
    void** rax50;
    void** rcx51;
    void** rbx52;
    uint32_t eax53;
    uint32_t eax54;
    int64_t rdx55;
    int32_t ecx56;
    uint64_t rax57;
    struct s16* rcx58;
    void** r8_59;
    void** r9_60;
    void** r15_61;
    struct s16* r15_62;
    uint32_t** rax63;
    int64_t rcx64;
    int1_t cf65;
    void** r8_66;
    void** v67;
    void* rbp68;
    uint32_t** rax69;
    int64_t rcx70;
    int1_t cf71;
    void* rbp72;
    uint32_t** rax73;
    int64_t rcx74;
    int1_t cf75;
    int64_t rax76;

    r14_12 = rdi;
    rbp13 = rdx;
    rbx14 = rcx;
    v15 = rsi;
    r15_16 = reinterpret_cast<void**>(static_cast<int64_t>(a7));
    v17 = r9d;
    v18 = r8b;
    rax19 = g28;
    v20 = rax19;
    rax21 = fun_34e0();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x4c8 - 8 + 8);
    r11_23 = *reinterpret_cast<void***>(rbx14 + 48);
    r10d24 = *reinterpret_cast<void***>(rbx14 + 8);
    v25 = rax21;
    v26 = *rax21;
    if (!r11_23) {
    }
    if (reinterpret_cast<signed char>(r10d24) <= reinterpret_cast<signed char>(12)) {
        if (!r10d24) {
            r10d24 = reinterpret_cast<void**>(12);
        }
    } else {
        r10d24 = r10d24 - 12;
    }
    eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp13));
    *reinterpret_cast<int32_t*>(&r13_28) = 0;
    *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax27)) {
        goto addr_1011d_10;
    }
    while (1) {
        addr_1017b_11:
        if (r14_12 && v15) {
            *reinterpret_cast<void***>(r14_12) = reinterpret_cast<void**>(0);
        }
        rdi = v26;
        *v25 = rdi;
        while (rax29 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v20) - reinterpret_cast<int64_t>(g28)), !!rax29) {
            fun_35b0();
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            edx30 = *reinterpret_cast<uint32_t*>(&rdx) + 100;
            if (!r10d24) {
                if (v17 == 43) {
                    addr_117bf_18:
                } else {
                    r10d24 = v17;
                    goto addr_106ec_20;
                }
            } else {
                if (!reinterpret_cast<int1_t>(r10d24 == 43)) 
                    goto addr_106ec_20; else 
                    goto addr_117bf_18;
            }
            r10d24 = reinterpret_cast<void**>(43);
            v31 = reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r15_16) > reinterpret_cast<signed char>(2));
            addr_10703_24:
            if (static_cast<int1_t>(!reinterpret_cast<int1_t>(rdi == 79))) {
                if (0) {
                    edx30 = -edx30;
                }
                rdi32 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xc7);
                r8_33 = rdi32;
                while (1) {
                    rsi = r8_33;
                    if (!1) {
                        --rsi;
                    }
                    *reinterpret_cast<uint32_t*>(&rax34) = edx30;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax34) + 4) = 0;
                    r8_33 = rsi + 0xffffffffffffffff;
                    rax35 = reinterpret_cast<uint64_t>(rax34 * 0xcccccccd) >> 35;
                    if (edx30 > 9) 
                        goto addr_10f63_33;
                    if (1) 
                        break;
                    addr_10f63_33:
                    edx30 = *reinterpret_cast<uint32_t*>(&rax35);
                }
                if (!r10d24) {
                    eax36 = 1;
                    r10d24 = reinterpret_cast<void**>(48);
                } else {
                    *reinterpret_cast<unsigned char*>(&eax36) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(r10d24 == 45));
                }
                if (reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0)) {
                    r15_16 = reinterpret_cast<void**>(2);
                    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
                }
                rdi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi32) - reinterpret_cast<unsigned char>(r8_33));
                rbp37 = rdi;
                if (!0) 
                    goto addr_10b3a_41;
            } else {
                rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xab);
                rdi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb0);
                rax38 = fun_37c0(rdi);
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                rcx39 = rax38;
                if (!rax38) 
                    goto addr_10168_45; else 
                    goto addr_10499_46;
            }
            ecx40 = 45;
            addr_11455_48:
            rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<uint64_t>(r15_16 + 0xffffffffffffffff)));
            *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(rsi) - *reinterpret_cast<uint32_t*>(&rbp37);
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx) == 0) || !*reinterpret_cast<unsigned char*>(&eax36)) {
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
            }
            if (r10d24 == 95) {
                *reinterpret_cast<void**>(&r9_41) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_16) - *reinterpret_cast<uint32_t*>(&rdx));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_41) + 4) = 0;
                rdx = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx)));
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(rdx));
                r15_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28));
                if (!r14_12) {
                    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_101a0_53;
                    ++r13_28;
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(r9_41 - 1));
                    rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp37)));
                    goto addr_1149a_55;
                } else {
                    rdi = r14_12;
                    fun_3640(rdi);
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    rdx = rdx;
                    r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(1)) 
                        goto addr_101a0_53;
                    *reinterpret_cast<void**>(&r9_43) = *reinterpret_cast<void**>(&r9_41);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_43) + 4) = 0;
                    r8_33 = r8_33;
                    r10d24 = r10d24;
                    ecx40 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ecx40));
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(r9_43 - 1));
                }
            } else {
                if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= 1) 
                    goto addr_101a0_53;
                if (!r14_12) 
                    goto addr_1148d_60;
            }
            *reinterpret_cast<void***>(r14_12) = *reinterpret_cast<void***>(&ecx40);
            ++r14_12;
            addr_1148d_60:
            ++r13_28;
            rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp37)));
            if (r10d24 == 45) {
                addr_114ce_62:
                r15_16 = rcx42;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                goto addr_10b66_63;
            } else {
                addr_1149a_55:
                r15_16 = rcx42;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                if (reinterpret_cast<signed char>(rsi) < reinterpret_cast<signed char>(0)) {
                    addr_10b66_63:
                    if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r15_16)) {
                        addr_101a0_53:
                        *v25 = reinterpret_cast<void**>(34);
                    } else {
                        if (r14_12) {
                            if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rcx42)) {
                                rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(rcx42));
                                rbp44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                                if (r10d24 == 48 || r10d24 == 43) {
                                    rdi = r14_12;
                                    r14_12 = rbp44;
                                    fun_3640(rdi);
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    rcx42 = rcx42;
                                    r8_33 = r8_33;
                                } else {
                                    rdi = r14_12;
                                    r14_12 = rbp44;
                                    fun_3640(rdi);
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    r8_33 = r8_33;
                                    rcx42 = rcx42;
                                }
                            }
                            if (!*reinterpret_cast<signed char*>(&v45)) {
                                rdx = rcx42;
                                rdi = r14_12;
                                v45 = rcx42;
                                fun_36e0(rdi, r8_33, rdx);
                                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                rcx42 = v45;
                            } else {
                                v45 = r8_33;
                                rbp46 = reinterpret_cast<unsigned char*>(rcx42 + 0xffffffffffffffff);
                                if (rcx42) {
                                    rax47 = fun_3480();
                                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                                    rcx42 = rcx42;
                                    r8_48 = v45;
                                    do {
                                        rsi = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r8_48) + reinterpret_cast<uint64_t>(rbp46))));
                                        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                                        *reinterpret_cast<uint32_t*>(&rdx) = (*rax47)[reinterpret_cast<unsigned char>(rsi)];
                                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp46)) = *reinterpret_cast<signed char*>(&rdx);
                                        cf49 = reinterpret_cast<uint64_t>(rbp46) < 1;
                                        --rbp46;
                                    } while (!cf49);
                                }
                            }
                            r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rcx42));
                            goto addr_10577_75;
                        }
                    }
                } else {
                    r15_16 = rsi;
                    goto addr_10b59_77;
                }
            }
            *reinterpret_cast<int32_t*>(&r13_28) = 0;
            *reinterpret_cast<int32_t*>(&r13_28 + 4) = 0;
            continue;
            addr_10577_75:
            r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(r15_16));
            addr_10168_45:
            while (eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1)), rbp13 = rbx14 + 1, r15_16 = reinterpret_cast<void**>(0xffffffffffffffff), !!*reinterpret_cast<signed char*>(&eax27)) {
                addr_1011d_10:
                if (*reinterpret_cast<signed char*>(&eax27) != 37) {
                    *reinterpret_cast<int32_t*>(&rax50) = 0;
                    *reinterpret_cast<int32_t*>(&rax50 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rcx51) = 1;
                    *reinterpret_cast<int32_t*>(&rcx51 + 4) = 0;
                    if (reinterpret_cast<signed char>(r15_16) >= reinterpret_cast<signed char>(0)) {
                        rax50 = r15_16;
                    }
                    if (rax50) {
                        rcx51 = rax50;
                    }
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28));
                    if (reinterpret_cast<unsigned char>(rcx51) >= reinterpret_cast<unsigned char>(rdx)) 
                        goto addr_101a0_53;
                    if (r14_12) {
                        if (reinterpret_cast<signed char>(r15_16) > reinterpret_cast<signed char>(1)) {
                            rbx52 = rax50 + 0xffffffffffffffff;
                            rdi = r14_12;
                            v45 = rcx51;
                            rdx = rbx52;
                            r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rbx52));
                            fun_3640(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rcx51 = v45;
                        }
                        eax53 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp13));
                        ++r14_12;
                        *reinterpret_cast<unsigned char*>(r14_12 + 0xffffffffffffffff) = *reinterpret_cast<unsigned char*>(&eax53);
                    }
                    r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(rcx51));
                    rbx14 = rbp13;
                    continue;
                }
                eax54 = v18;
                rbx14 = rbp13;
                r10d24 = reinterpret_cast<void**>(0);
                *reinterpret_cast<signed char*>(&v45) = *reinterpret_cast<signed char*>(&eax54);
                while (*reinterpret_cast<void***>(&rdx55) = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx14 + 1)))), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, ++rbx14, ecx56 = static_cast<int32_t>(rdx55 - 35), rsi = *reinterpret_cast<void***>(&rdx55), rdi = *reinterpret_cast<void***>(&rdx55), *reinterpret_cast<unsigned char*>(&ecx56) <= 60) {
                    rax57 = 1 << *reinterpret_cast<unsigned char*>(&ecx56);
                    if (rax57 & 0x1000000000002500) {
                        r10d24 = *reinterpret_cast<void***>(&rdx55);
                    } else {
                        if (*reinterpret_cast<unsigned char*>(&ecx56) == 59) {
                            *reinterpret_cast<signed char*>(&v45) = 1;
                        } else {
                            if (!(*reinterpret_cast<uint32_t*>(&rax57) & 1)) 
                                break;
                        }
                    }
                }
                if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rdx55) - 48) > 9) 
                    goto addr_1027f_98;
                r15_16 = reinterpret_cast<void**>(0);
                do {
                    if (__intrinsic() || (r15_16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_16) * 10 + reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48)), *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0, __intrinsic())) {
                        r15_16 = reinterpret_cast<void**>(0x7fffffff);
                        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
                    }
                    rdi = reinterpret_cast<void**>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx14 + 1))));
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    ++rbx14;
                    rsi = rdi;
                } while (static_cast<uint32_t>(reinterpret_cast<uint64_t>(rdi + 0xffffffffffffffd0)) <= 9);
                addr_1027f_98:
                if (*reinterpret_cast<unsigned char*>(&rsi) == 69 || *reinterpret_cast<unsigned char*>(&rsi) == 79) {
                    rsi = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1))));
                    ++rbx14;
                } else {
                    rdi = reinterpret_cast<void**>(0);
                }
                if (*reinterpret_cast<unsigned char*>(&rsi) <= 0x7a) 
                    goto addr_1029b_106;
                rcx58 = reinterpret_cast<struct s16*>(reinterpret_cast<unsigned char>(rbx14) - reinterpret_cast<unsigned char>(rbp13));
                r8_59 = reinterpret_cast<void**>(&rcx58->f1);
                if (reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0) || r10d24 == 45) {
                    r9_60 = r8_59;
                    *reinterpret_cast<uint32_t*>(&rdx) = 0;
                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                } else {
                    rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
                    r9_60 = rdx;
                    if (reinterpret_cast<unsigned char>(r8_59) >= reinterpret_cast<unsigned char>(rdx)) {
                        r9_60 = r8_59;
                    }
                }
                if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r9_60)) 
                    goto addr_101a0_53;
                if (r14_12) {
                    if (reinterpret_cast<unsigned char>(r8_59) < reinterpret_cast<unsigned char>(rdx)) {
                        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_59));
                        r15_61 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                        if (r10d24 == 48 || r10d24 == 43) {
                            rdi = r14_12;
                            r14_12 = r15_61;
                            fun_3640(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            rcx58 = rcx58;
                            r8_59 = r8_59;
                            r9_60 = r9_60;
                        } else {
                            rdi = r14_12;
                            r14_12 = r15_61;
                            fun_3640(rdi);
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r9_60 = r9_60;
                            r8_59 = r8_59;
                            rcx58 = rcx58;
                        }
                    }
                    if (!*reinterpret_cast<signed char*>(&v45)) {
                        rdx = r8_59;
                        rdi = r14_12;
                        v45 = r8_59;
                        fun_36e0(rdi, rbp13, rdx);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r9_60 = r9_60;
                        r8_59 = v45;
                    } else {
                        r15_62 = rcx58;
                        if (r8_59) {
                            v45 = r8_59;
                            rax63 = fun_3480();
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r8_59 = v45;
                            r9_60 = r9_60;
                            do {
                                *reinterpret_cast<uint32_t*>(&rcx64) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp13) + reinterpret_cast<uint64_t>(r15_62));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx64) + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx) = (*rax63)[rcx64];
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(r15_62)) = *reinterpret_cast<signed char*>(&rdx);
                                cf65 = reinterpret_cast<uint64_t>(r15_62) < 1;
                                r15_62 = reinterpret_cast<struct s16*>(reinterpret_cast<uint64_t>(r15_62) - 1);
                            } while (!cf65);
                        }
                    }
                    r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r8_59));
                }
                r13_28 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_28) + reinterpret_cast<unsigned char>(r9_60));
            }
            goto addr_1017b_11;
            addr_10b59_77:
            rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
            r15_16 = rcx42;
            if (reinterpret_cast<unsigned char>(rdx) >= reinterpret_cast<unsigned char>(rcx42)) {
                r15_16 = rdx;
                goto addr_10b66_63;
            }
            addr_10b3a_41:
            if (v31) {
                ecx40 = 43;
                goto addr_11455_48;
            } else {
                rcx42 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(rdi)));
                if (reinterpret_cast<signed char>(r15_16) <= reinterpret_cast<signed char>(rdi)) 
                    goto addr_114c4_129;
                if (*reinterpret_cast<unsigned char*>(&eax36)) 
                    goto addr_10b59_77;
                addr_114c4_129:
                if (!reinterpret_cast<int1_t>(r10d24 == 45)) 
                    goto addr_10b59_77; else 
                    goto addr_114ce_62;
            }
            addr_10499_46:
            r10d24 = r10d24;
            r8_66 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax38) + 0xffffffffffffffff);
            if (r10d24 == 45 || reinterpret_cast<signed char>(r15_16) < reinterpret_cast<signed char>(0)) {
                r15_16 = r8_66;
                *reinterpret_cast<uint32_t*>(&rdx) = 0;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            } else {
                rdx = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r15_16)));
                r15_16 = rdx;
                if (reinterpret_cast<unsigned char>(r8_66) >= reinterpret_cast<unsigned char>(rdx)) {
                    r15_16 = r8_66;
                }
            }
            if (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(r13_28)) <= reinterpret_cast<unsigned char>(r15_16)) 
                goto addr_101a0_53;
            if (r14_12) {
                if (reinterpret_cast<unsigned char>(r8_66) < reinterpret_cast<unsigned char>(rdx)) {
                    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx) - reinterpret_cast<unsigned char>(r8_66));
                    v67 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(rdx));
                    if (r10d24 == 48 || r10d24 == 43) {
                        rdi = r14_12;
                        fun_3640(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r14_12 = v67;
                        rcx39 = rcx39;
                        r8_66 = r8_66;
                    } else {
                        rdi = r14_12;
                        fun_3640(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r14_12 = v67;
                        r8_66 = r8_66;
                        rcx39 = rcx39;
                    }
                }
                if (0) {
                    rbp68 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx39) + 0xfffffffffffffffe);
                    if (r8_66) {
                        v45 = r8_66;
                        rax69 = fun_3870(rdi);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r8_66 = v45;
                        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb1);
                        do {
                            *reinterpret_cast<uint32_t*>(&rcx70) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(rbp68));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx70) + 4) = 0;
                            *reinterpret_cast<uint32_t*>(&rdx) = (*rax69)[rcx70];
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp68)) = *reinterpret_cast<signed char*>(&rdx);
                            cf71 = reinterpret_cast<uint64_t>(rbp68) < 1;
                            rbp68 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp68) - 1);
                        } while (!cf71);
                    }
                } else {
                    if (!*reinterpret_cast<signed char*>(&v45)) {
                        rdx = r8_66;
                        rdi = r14_12;
                        v45 = r8_66;
                        fun_36e0(rdi, reinterpret_cast<int64_t>(rsp22) + 0xb1, rdx);
                        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        r8_66 = v45;
                    } else {
                        rbp72 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx39) + 0xfffffffffffffffe);
                        if (r8_66) {
                            v45 = r8_66;
                            rax73 = fun_3480();
                            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            r8_66 = v45;
                            rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xb1);
                            do {
                                *reinterpret_cast<uint32_t*>(&rcx74) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(rbp72));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx74) + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx) = (*rax73)[rcx74];
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<uint64_t>(rbp72)) = *reinterpret_cast<signed char*>(&rdx);
                                cf75 = reinterpret_cast<uint64_t>(rbp72) < 1;
                                rbp72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp72) - 1);
                            } while (!cf75);
                        }
                    }
                }
                r14_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_12) + reinterpret_cast<unsigned char>(r8_66));
                goto addr_10577_75;
            }
            addr_106ec_20:
            v31 = 0;
            goto addr_10703_24;
        }
        break;
    }
    return r13_28;
    addr_1029b_106:
    *reinterpret_cast<uint32_t*>(&rax76) = *reinterpret_cast<unsigned char*>(&rsi);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax76) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x1779c + rax76 * 4) + 0x1779c;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x1c0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s17 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s17* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s17* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x17021);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x1701c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x17025);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x17018);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

struct s18 {
    signed char[28] pad28;
    int32_t f1c;
    signed char[8] pad40;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
    int64_t f48;
    int64_t f50;
    int64_t f58;
    int64_t f60;
    signed char[57] pad161;
    signed char fa1;
    signed char[6] pad168;
    int64_t fa8;
    signed char[32] pad208;
    int64_t fd0;
    signed char[8] pad224;
    signed char fe0;
};

int64_t digits_to_date_time(struct s18* rdi) {
    int64_t r8_2;
    int64_t v3;
    int64_t v4;
    int64_t r10_5;
    int64_t rsi6;
    int64_t rax7;
    int64_t rdi8;
    int64_t rax9;
    int64_t v10;
    int64_t rax11;
    int64_t rdx12;
    int64_t rax13;
    int64_t rax14;

    r8_2 = v3;
    if (!rdi->fa8 || (rdi->f30 || rdi->fa1)) {
        if (v4 > 4) {
            rdi->fa8 = rdi->fa8 + 1;
            r10_5 = r8_2 >> 63;
            rdi->f30 = v4 - 4;
            rsi6 = (__intrinsic() + r8_2 >> 6) - r10_5;
            rax7 = rsi6 + rsi6 * 4;
            rdi->f40 = r8_2 - (rax7 + rax7 * 4 << 2);
            rdi8 = (__intrinsic() + rsi6 >> 6) - (rsi6 >> 63);
            rax9 = rdi8 + rdi8 * 4;
            rdi->f38 = rsi6 - (rax9 + rax9 * 4 << 2);
            rdi->f28 = (__intrinsic() >> 11) - r10_5;
            return r8_2 * 0x346dc5d63886594b;
        }
        rdi->fd0 = rdi->fd0 + 1;
        if (v4 > 2) 
            goto addr_703e_5;
    } else {
        if (rdi->fd0 || v4 > 2) {
            __asm__("movdqu xmm0, [rsp+0x8]");
            rdi->fe0 = 1;
            rdi->f30 = v10;
            __asm__("movups [rcx+0x20], xmm0");
            return v10;
        } else {
            rdi->fd0 = 1;
        }
    }
    *reinterpret_cast<int32_t*>(&rax11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    addr_7074_10:
    rdi->f48 = r8_2;
    rdi->f50 = rax11;
    rdi->f58 = 0;
    rdi->f60 = 0;
    rdi->f1c = 2;
    return rax11;
    addr_703e_5:
    rdx12 = (__intrinsic() + r8_2 >> 6) - (r8_2 >> 63);
    rax13 = rdx12 + rdx12 * 4;
    rax14 = r8_2;
    r8_2 = rdx12;
    rax11 = rax14 - (rax13 + rax13 * 4 << 2);
    goto addr_7074_10;
}

struct s19 {
    signed char[24] pad24;
    int32_t f18;
};

/* time_zone_hhmm.isra.0 */
signed char time_zone_hhmm_isra_0(struct s19* rdi, signed char sil, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rdi6;
    uint32_t edx7;
    int64_t rdi8;
    uint32_t ecx9;
    int64_t rdi10;
    int64_t rcx11;
    int64_t rdx12;
    int64_t rax13;
    int64_t rcx14;
    int32_t eax15;

    rdi6 = rdx;
    if (rcx > 2) {
        if (r8 >= 0) {
            addr_79d5_3:
            edx7 = 0;
            rdi8 = rdi6 * 60;
            *reinterpret_cast<unsigned char*>(&edx7) = __intrinsic();
            if (sil) {
                ecx9 = 0;
                rdi10 = rdi8 - r8;
                *reinterpret_cast<unsigned char*>(&ecx9) = __intrinsic();
            } else {
                ecx9 = 0;
                rdi10 = rdi8 + r8;
                *reinterpret_cast<unsigned char*>(&ecx9) = __intrinsic();
            }
        } else {
            rcx11 = rdi6;
            rdx12 = (__intrinsic() + rdi6 >> 6) - (rdi6 >> 63);
            rax13 = rdx12 + rdx12 * 4;
            rdi6 = rdx12;
            rcx14 = rcx11 - (rax13 + rax13 * 4 << 2);
            goto addr_7a62_7;
        }
    } else {
        if (r8 >= 0) 
            goto addr_79d5_3;
        *reinterpret_cast<int32_t*>(&rcx14) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx14) + 4) = 0;
        goto addr_7a62_7;
    }
    eax15 = 0;
    if (edx7 | ecx9) {
        addr_7a0f_11:
        return *reinterpret_cast<signed char*>(&eax15);
    } else {
        addr_79f1_12:
        eax15 = 0;
        if (reinterpret_cast<uint64_t>(rdi10 + 0x5a0) <= 0xb40) {
            eax15 = 1;
            rdi->f18 = *reinterpret_cast<int32_t*>(&rdi10) * 60;
            goto addr_7a0f_11;
        }
    }
    addr_7a62_7:
    rdi10 = rcx14 + ((rdi6 << 4) - rdi6) * 4;
    goto addr_79f1_12;
}

int64_t __gmon_start__ = 0;

void fun_3003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g1bd98 = 0;

void fun_3033() {
    __asm__("cli ");
    goto g1bd98;
}

void fun_3043() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3053() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3063() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3073() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3083() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3093() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_30f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3103() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3113() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3123() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3133() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3143() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3153() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3163() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3173() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3183() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3193() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_31f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3203() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3213() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3223() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3233() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3243() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3253() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3263() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3273() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3283() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3293() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_32f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3303() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3313() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3323() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3333() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3343() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3353() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3363() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3373() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3383() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3393() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33a3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33b3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33c3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33d3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33e3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_33f3() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3403() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3413() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3423() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3433() {
    __asm__("cli ");
    goto 0x3020;
}

void fun_3443() {
    __asm__("cli ");
    goto 0x3020;
}

int64_t localtime_r = 0;

void fun_3453() {
    __asm__("cli ");
    goto localtime_r;
}

int64_t gmtime_r = 0;

void fun_3463() {
    __asm__("cli ");
    goto gmtime_r;
}

int64_t __cxa_finalize = 0;

void fun_3473() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x3030;

void fun_3483() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t getenv = 0x3040;

void fun_3493() {
    __asm__("cli ");
    goto getenv;
}

int64_t __snprintf_chk = 0x3050;

void fun_34a3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x3060;

void fun_34b3() {
    __asm__("cli ");
    goto free;
}

int64_t localtime = 0x3070;

void fun_34c3() {
    __asm__("cli ");
    goto localtime;
}

int64_t abort = 0x3080;

void fun_34d3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x3090;

void fun_34e3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x30a0;

void fun_34f3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x30b0;

void fun_3503() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x30c0;

void fun_3513() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x30d0;

void fun_3523() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t clock_gettime = 0x30e0;

void fun_3533() {
    __asm__("cli ");
    goto clock_gettime;
}

int64_t setenv = 0x30f0;

void fun_3543() {
    __asm__("cli ");
    goto setenv;
}

int64_t textdomain = 0x3100;

void fun_3553() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x3110;

void fun_3563() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x3120;

void fun_3573() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x3130;

void fun_3583() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x3140;

void fun_3593() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x3150;

void fun_35a3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x3160;

void fun_35b3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x3170;

void fun_35c3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x3180;

void fun_35d3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x3190;

void fun_35e3() {
    __asm__("cli ");
    goto strchr;
}

int64_t snprintf = 0x31a0;

void fun_35f3() {
    __asm__("cli ");
    goto snprintf;
}

int64_t __overflow = 0x31b0;

void fun_3603() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x31c0;

void fun_3613() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x31d0;

void fun_3623() {
    __asm__("cli ");
    goto lseek;
}

int64_t fputs = 0x31e0;

void fun_3633() {
    __asm__("cli ");
    goto fputs;
}

int64_t memset = 0x31f0;

void fun_3643() {
    __asm__("cli ");
    goto memset;
}

int64_t fputc = 0x3200;

void fun_3653() {
    __asm__("cli ");
    goto fputc;
}

int64_t memcmp = 0x3210;

void fun_3663() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x3220;

void fun_3673() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x3230;

void fun_3683() {
    __asm__("cli ");
    goto calloc;
}

int64_t putenv = 0x3240;

void fun_3693() {
    __asm__("cli ");
    goto putenv;
}

int64_t __getdelim = 0x3250;

void fun_36a3() {
    __asm__("cli ");
    goto __getdelim;
}

int64_t strcmp = 0x3260;

void fun_36b3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x3270;

void fun_36c3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x3280;

void fun_36d3() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x3290;

void fun_36e3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t tzset = 0x32a0;

void fun_36f3() {
    __asm__("cli ");
    goto tzset;
}

int64_t time = 0x32b0;

void fun_3703() {
    __asm__("cli ");
    goto time;
}

int64_t fileno = 0x32c0;

void fun_3713() {
    __asm__("cli ");
    goto fileno;
}

int64_t settimeofday = 0x32d0;

void fun_3723() {
    __asm__("cli ");
    goto settimeofday;
}

int64_t malloc = 0x32e0;

void fun_3733() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x32f0;

void fun_3743() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x3300;

void fun_3753() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t clock_getres = 0x3310;

void fun_3763() {
    __asm__("cli ");
    goto clock_getres;
}

int64_t clock_settime = 0x3320;

void fun_3773() {
    __asm__("cli ");
    goto clock_settime;
}

int64_t __freading = 0x3330;

void fun_3783() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x3340;

void fun_3793() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x3350;

void fun_37a3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x3360;

void fun_37b3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t strftime = 0x3370;

void fun_37c3() {
    __asm__("cli ");
    goto strftime;
}

int64_t error = 0x3380;

void fun_37d3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x3390;

void fun_37e3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x33a0;

void fun_37f3() {
    __asm__("cli ");
    goto fopen;
}

int64_t unsetenv = 0x33b0;

void fun_3803() {
    __asm__("cli ");
    goto unsetenv;
}

int64_t __cxa_atexit = 0x33c0;

void fun_3813() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x33d0;

void fun_3823() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x33e0;

void fun_3833() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x33f0;

void fun_3843() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x3400;

void fun_3853() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x3410;

void fun_3863() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_tolower_loc = 0x3420;

void fun_3873() {
    __asm__("cli ");
    goto __ctype_tolower_loc;
}

int64_t __ctype_b_loc = 0x3430;

void fun_3883() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x3440;

void fun_3893() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void fun_3570(int64_t rdi, int64_t rsi);

void fun_3550(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_35c0(void** rdi);

uint32_t optind = 0;

int64_t Version = 0x15904;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

void fun_3820();

void usage();

void** argmatch_die = reinterpret_cast<void**>(0);

int64_t optarg = 0;

int64_t __xargmatch_internal(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, void** r9);

void** quotearg_n_style_colon();

void** fun_3750(void** rdi);

void** xstrdup(void** rdi);

int64_t gettime_res(void** rdi, void** rsi, ...);

int32_t parse_datetime2(void* rdi, void** rsi);

int32_t fun_36d0();

signed char posixtime(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t settime(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_37f0(int64_t rdi, int64_t rsi);

void xalloc_die();

void** stdin = reinterpret_cast<void**>(0);

void* fun_36a0(void* rdi, void** rsi, void** rdx, void** rcx);

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_38f3(uint32_t edi, void** rsi) {
    void** r15_3;
    void* r14_4;
    void** r13_5;
    void** rbx6;
    void** rdi7;
    void* rsp8;
    void** v9;
    void** r8_10;
    void** rcx11;
    void** rsi12;
    void** rdi13;
    int32_t eax14;
    void* rsp15;
    void** rdx16;
    int64_t rax17;
    void** rdi18;
    void* rsp19;
    void** rdi20;
    int64_t rcx21;
    void** r9_22;
    void** rax23;
    int64_t rsi24;
    int64_t rax25;
    int64_t rax26;
    int64_t rax27;
    void** rax28;
    void** v29;
    void** r12_30;
    uint32_t eax31;
    uint32_t eax32;
    void* rax33;
    void** rax34;
    int64_t rax35;
    int32_t edx36;
    int64_t rcx37;
    int64_t rax38;
    void** rax39;
    void** v40;
    void** rax41;
    void** rbx42;
    void** rax43;
    void* rsp44;
    void** r12_45;
    uint32_t eax46;
    uint32_t r15d47;
    int64_t rax48;
    void* rsp49;
    void** v50;
    void** v51;
    void** rax52;
    int32_t eax53;
    int32_t eax54;
    void** v55;
    void** v56;
    int64_t rax57;
    signed char al58;
    void** rax59;
    int32_t eax60;
    void** rax61;
    uint32_t eax62;
    void* rsp63;
    int32_t eax64;
    void* rsp65;
    void** rax66;
    void* rsp67;
    void** rax68;
    void*** rax69;
    void* rax70;
    void* rsp71;
    int32_t eax72;
    void* rsp73;
    void** r8_74;
    signed char* rax75;
    void** v76;
    void** v77;
    uint32_t eax78;
    int64_t rax79;

    __asm__("cli ");
    r15_3 = reinterpret_cast<void**>(0x1afc0);
    *reinterpret_cast<uint32_t*>(&r14_4) = edi;
    r13_5 = rsi;
    *reinterpret_cast<int32_t*>(&rbx6) = 0;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    rdi7 = *reinterpret_cast<void***>(rsi);
    set_program_name(rdi7);
    fun_37a0(6, 6);
    fun_3570("coreutils", "/usr/local/share/locale");
    fun_3550("coreutils", "/usr/local/share/locale");
    atexit(0x4f90, "/usr/local/share/locale");
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xf8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v9 = reinterpret_cast<void**>(0);
    while (1) {
        *reinterpret_cast<uint32_t*>(&r8_10) = 0;
        *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
        rcx11 = r15_3;
        rsi12 = r13_5;
        *reinterpret_cast<uint32_t*>(&rdi13) = *reinterpret_cast<uint32_t*>(&r14_4);
        *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
        eax14 = fun_35c0(rdi13);
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        if (eax14 == -1) {
            addr_3a01_3:
            *reinterpret_cast<uint32_t*>(&r15_3) = 2;
            *reinterpret_cast<int32_t*>(&r15_3 + 4) = 0;
            if (0) {
                *reinterpret_cast<uint32_t*>(&rdx16) = 0;
                *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                if (!0) {
                    rax17 = reinterpret_cast<int32_t>(optind);
                    if (*reinterpret_cast<int32_t*>(&rax17) >= *reinterpret_cast<int32_t*>(&r14_4)) 
                        goto addr_3c1b_7;
                    *reinterpret_cast<uint32_t*>(&rcx11) = static_cast<uint32_t>(rax17 + 1);
                    *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0;
                    if (*reinterpret_cast<int32_t*>(&rcx11) < *reinterpret_cast<int32_t*>(&r14_4)) 
                        goto addr_3f9e_9; else 
                        goto addr_3bfa_10;
                }
            }
        } else {
            if (eax14 > 0x82) 
                goto addr_3a54_13;
            if (eax14 <= 72) 
                goto addr_39e8_15; else 
                goto addr_39d1_16;
        }
        fun_3580();
        fun_37d0();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8 - 8 + 8);
        goto addr_3a54_13;
        addr_3f9e_9:
        rdi18 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r13_5 + rax17 * 8) + 8);
        quote(rdi18, rsi12);
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        addr_3fb7_18:
        fun_3580();
        fun_37d0();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp19) - 8 + 8 - 8 + 8);
        goto addr_3a54_13;
        addr_3bfa_10:
        rdi13 = *reinterpret_cast<void***>(r13_5 + rax17 * 8);
        if (*reinterpret_cast<void***>(rdi13) == 43) 
            goto addr_3fd4_19;
        if (0) 
            goto addr_3ffb_21;
        if (!0) 
            goto addr_3c1b_7;
        addr_3ffb_21:
        quote(rdi13, rsi12);
        rsp19 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
        goto addr_3fb7_18;
        addr_39e8_15:
        if (eax14 == 0xffffff7d) {
            rdi20 = stdout;
            rcx21 = Version;
            *reinterpret_cast<int32_t*>(&r9_22) = 0;
            *reinterpret_cast<int32_t*>(&r9_22 + 4) = 0;
            version_etc(rdi20, "date", "GNU coreutils", rcx21, "David MacKenzie");
            fun_3820();
            rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8 - 8 + 8);
            rax23 = reinterpret_cast<void**>("%a, %d %b %Y %H:%M:%S %z");
        } else {
            if (eax14 != 0xffffff7e) {
                addr_3a54_13:
                usage();
                r9_22 = argmatch_die;
                rsi24 = optarg;
                rax25 = __xargmatch_internal("--rfc-3339", rsi24, 0x1b1f0, 0x158f8, 4, r9_22);
                *reinterpret_cast<int32_t*>(&rax26) = *reinterpret_cast<int32_t*>(0x158f0 + rax25 * 4 + 8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
                rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
                rax23 = reinterpret_cast<void**>((rax26 << 5) + reinterpret_cast<uint64_t>("%Y-%m-%d"));
            } else {
                *reinterpret_cast<uint32_t*>(&rdi13) = 0;
                *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
                usage();
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                goto addr_3a01_3;
            }
        }
        if (rbx6) 
            break;
        rbx6 = rax23;
        continue;
        addr_39d1_16:
        *reinterpret_cast<uint32_t*>(&rax27) = eax14 - 73;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rax27) > 57) 
            goto addr_3a54_13; else 
            goto addr_39d9_28;
    }
    fun_3580();
    fun_37d0();
    quotearg_n_style_colon();
    fun_34e0();
    fun_37d0();
    addr_413e_31:
    quotearg_n_style_colon();
    fun_34e0();
    fun_37d0();
    goto addr_416f_32;
    addr_3c1b_7:
    if (!rbx6 && ((rbx6 = reinterpret_cast<void**>("%s.%N"), !0) && (*reinterpret_cast<uint32_t*>(&rdi13) = 0x2006c, *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0, rax28 = fun_3750(0x2006c), rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8), rbx6 = rax28, !*reinterpret_cast<void***>(rax28)))) {
        rbx6 = reinterpret_cast<void**>("%a %b %e %H:%M:%S %Z %Y");
    }
    addr_3c24_34:
    v29 = reinterpret_cast<void**>(0);
    r12_30 = rbx6;
    while (eax31 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_30)), !!*reinterpret_cast<signed char*>(&eax31)) {
        if (*reinterpret_cast<signed char*>(&eax31) == 37) {
            eax32 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_30 + 1));
            if (*reinterpret_cast<signed char*>(&eax32) != 45 || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_30 + 2) == 78)) {
                *reinterpret_cast<uint32_t*>(&rax33) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax32) == 37);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
                r12_30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_30) + reinterpret_cast<uint64_t>(rax33));
            } else {
                if (!v29) {
                    rdi13 = rbx6;
                    rax34 = xstrdup(rdi13);
                    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                    v29 = rax34;
                }
                rax35 = gettime_res(rdi13, rsi12);
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8);
                edx36 = 9;
                rcx37 = rax35;
                *reinterpret_cast<int32_t*>(&rax38) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax38) + 4) = 0;
                while (rax38 = rax38 * 10, rcx37 >= rax38) {
                    --edx36;
                }
                rax39 = r12_30;
                rcx11 = v29;
                *reinterpret_cast<uint32_t*>(&rdx16) = reinterpret_cast<uint32_t>(edx36 + 48);
                *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                r12_30 = r12_30 + 2;
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax39) - reinterpret_cast<unsigned char>(rbx6)) + reinterpret_cast<unsigned char>(rcx11) + 1) = *reinterpret_cast<signed char*>(&rdx16);
            }
        }
        ++r12_30;
    }
    if (v29) {
        rbx6 = v29;
    }
    v40 = rbx6;
    rax41 = fun_3490("TZ");
    rbx42 = rax41;
    rax43 = tzalloc(rax41, rsi12, rax41, rsi12);
    rsp44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp15) - 8 + 8 - 8 + 8);
    r12_45 = rax43;
    if (1) {
        while (1) {
            eax46 = *reinterpret_cast<uint32_t*>(&r15_3) ^ 1;
            r15d47 = 1;
            *reinterpret_cast<unsigned char*>(&r15d47) = reinterpret_cast<unsigned char>(1 & *reinterpret_cast<unsigned char*>(&eax46));
            if (!*reinterpret_cast<unsigned char*>(&r15d47)) {
                addr_3f4f_50:
                if (1) {
                    if (0) {
                        rax48 = gettime_res(0, rsi12, 0, rsi12);
                        rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
                        *reinterpret_cast<uint32_t*>(&rcx11) = 0x3b9aca00;
                        *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0;
                        rdx16 = reinterpret_cast<void**>(rax48 % reinterpret_cast<signed char>(0x3b9aca00));
                        v50 = reinterpret_cast<void**>(rax48 / reinterpret_cast<signed char>(0x3b9aca00));
                        v51 = rdx16;
                    } else {
                        rax52 = reinterpret_cast<void**>(0);
                        r9_22 = rbx42;
                        r8_10 = r12_45;
                        *reinterpret_cast<uint32_t*>(&rcx11) = parse_datetime_flags;
                        *reinterpret_cast<int32_t*>(&rcx11 + 4) = 0;
                        if (1) {
                            rax52 = v9;
                        }
                        *reinterpret_cast<uint32_t*>(&rdx16) = 0;
                        *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                        rsi12 = rax52;
                        v9 = rax52;
                        eax53 = parse_datetime2(reinterpret_cast<int64_t>(rsp44) + 64, rsi12);
                        rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
                        if (!*reinterpret_cast<signed char*>(&eax53)) 
                            goto addr_4055_56;
                    }
                } else {
                    rsi12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp44) + 80);
                    eax54 = fun_36d0();
                    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
                    if (eax54) 
                        goto addr_413e_31;
                    v50 = v55;
                    v51 = v56;
                }
                r13_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp49) + 64);
                if (1) 
                    goto addr_3ea0_60;
                goto addr_3e90_62;
            } else {
                rax57 = reinterpret_cast<int32_t>(optind);
                if (*reinterpret_cast<int32_t*>(&rax57) >= *reinterpret_cast<int32_t*>(&r14_4)) {
                    gettime(reinterpret_cast<int64_t>(rsp44) + 64, rsi12, rdx16, rcx11, r8_10, r9_22);
                    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
                } else {
                    r13_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp44) + 64);
                    *reinterpret_cast<uint32_t*>(&rdx16) = 7;
                    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
                    rsi12 = *reinterpret_cast<void***>(r13_5 + rax57 * 8);
                    v9 = *reinterpret_cast<void***>(r13_5 + rax57 * 8);
                    al58 = posixtime(r13_5, rsi12, 7, rcx11, r8_10, r9_22);
                    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
                    v51 = reinterpret_cast<void**>(0);
                    if (!al58) {
                        addr_4055_56:
                        rax59 = quote(v9, rsi12, v9, rsi12);
                        r12_45 = rax59;
                        fun_3580();
                        fun_37d0();
                        rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8 - 8 + 8 - 8 + 8);
                        goto addr_4089_66;
                    } else {
                        addr_3e90_62:
                        eax60 = settime(r13_5, rsi12, rdx16, rcx11, r8_10, r9_22);
                        rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
                        if (eax60) {
                            addr_4089_66:
                            r15d47 = 0;
                            rax61 = fun_3580();
                            r13_5 = rax61;
                            fun_34e0();
                            fun_37d0();
                            rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8 - 8 + 8 - 8 + 8);
                        } else {
                            addr_3ea0_60:
                            r15d47 = 1;
                        }
                    }
                }
                rsi12 = v50;
                rdx16 = v51;
                rcx11 = r12_45;
                eax62 = show_date(v40, rsi12, rdx16, rcx11, r8_10, r9_22);
                rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&r15_3) = r15d47 & eax62;
                *reinterpret_cast<unsigned char*>(&v9) = *reinterpret_cast<unsigned char*>(&r15_3);
                addr_3e26_68:
                fun_3820();
                rsp44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
            }
        }
    } else {
        eax64 = fun_36b0(0, "-", 0, "-");
        rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
        if (eax64) {
            rax66 = fun_37f0(0, "r");
            rsp67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8);
            r13_5 = rax66;
            if (!rax66) {
                rax68 = quotearg_n_style_colon();
                r12_45 = rax68;
                rax69 = fun_34e0();
                rcx11 = r12_45;
                rdx16 = reinterpret_cast<void**>("%s");
                rsi12 = *rax69;
                *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
                fun_37d0();
                xalloc_die();
                rsp44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_3f4f_50;
            }
        } else {
            fun_3580();
            rsp67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8);
            r13_5 = stdin;
        }
        *reinterpret_cast<unsigned char*>(&v9) = 1;
        r15_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp67) + 56);
        r14_4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) + 48);
        while (rcx11 = r13_5, *reinterpret_cast<uint32_t*>(&rdx16) = 10, *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0, rsi12 = r15_3, rax70 = fun_36a0(r14_4, rsi12, 10, rcx11), rsp71 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp67) - 8 + 8), reinterpret_cast<int64_t>(rax70) >= reinterpret_cast<int64_t>(0)) {
            r9_22 = rbx42;
            eax72 = parse_datetime2(reinterpret_cast<int64_t>(rsp71) + 64, 0);
            rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8);
            *reinterpret_cast<int32_t*>(&r8_74) = eax72;
            *reinterpret_cast<int32_t*>(&r8_74 + 4) = 0;
            if (!*reinterpret_cast<signed char*>(&eax72)) {
                rax75 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rax70) + 0xffffffffffffffff);
                if (*rax75 == 10) {
                    *rax75 = 0;
                }
                quote(0, 0, 0, 0);
                fun_3580();
                fun_37d0();
                rsp67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8 - 8 + 8 - 8 + 8);
                *reinterpret_cast<uint32_t*>(&r8_10) = *reinterpret_cast<unsigned char*>(&r8_74);
                *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
            } else {
                eax78 = show_date(v40, v76, v77, r12_45, r8_74, r9_22);
                rsp67 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&r8_10) = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&v9)) & eax78;
                *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
            }
            *reinterpret_cast<unsigned char*>(&v9) = *reinterpret_cast<unsigned char*>(&r8_10);
        }
        rax79 = rpl_fclose(r13_5, rsi12, 10, rcx11);
        if (*reinterpret_cast<int32_t*>(&rax79) + 1) {
            fun_34b0(0, 0);
            rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp71) - 8 + 8 - 8 + 8);
            goto addr_3e26_68;
        }
    }
    addr_3fd4_19:
    if (rbx6) {
        addr_416f_32:
        fun_3580();
        fun_37d0();
    } else {
        optind = *reinterpret_cast<uint32_t*>(&rcx11);
        rbx6 = rdi13 + 1;
        goto addr_3c24_34;
    }
    addr_39d9_28:
    goto *reinterpret_cast<int32_t*>(0x156c0 + rax27 * 4) + 0x156c0;
}

int64_t __libc_start_main = 0;

void fun_41a3() {
    __asm__("cli ");
    __libc_start_main(0x38f0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x1c008;

void fun_3470(int64_t rdi);

int64_t fun_4243() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_3470(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_4283() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_37b0(int64_t rdi, void** rsi, struct s0* rdx, struct s0* rcx);

void fun_3670(void** rdi, void** rsi, int64_t rdx, struct s0* rcx);

void fun_4443(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    void** rax5;
    void** r12_6;
    void** rax7;
    void** r12_8;
    void** rax9;
    void** r12_10;
    void** rax11;
    void** r12_12;
    void** rax13;
    void** r12_14;
    void** rax15;
    void** r12_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** r12_22;
    void** rax23;
    void** r12_24;
    void** rax25;
    void** r12_26;
    void** rax27;
    void** r12_28;
    void** rax29;
    void** r12_30;
    void** rax31;
    void** r12_32;
    void** rax33;
    void** r12_34;
    void** rax35;
    void** r12_36;
    void** rax37;
    void** r12_38;
    void** rax39;
    void** r12_40;
    void** rax41;
    void** r12_42;
    void** rax43;
    void** r12_44;
    void** rax45;
    void** r12_46;
    void** rax47;
    void** r12_48;
    void** rax49;
    void** r12_50;
    void** rax51;
    void** r12_52;
    void** rax53;
    void** r12_54;
    void** rax55;
    void** r12_56;
    void** rax57;
    void** r12_58;
    void** rax59;
    int32_t eax60;
    struct s0* r13_61;
    void** rax62;
    void** rax63;
    int32_t eax64;
    void** rax65;
    void** rax66;
    void** rax67;
    int32_t eax68;
    void** rax69;
    void** r15_70;
    void** rax71;
    void** rax72;
    void** rdi73;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_3580();
            fun_37b0(1, rax5, r12_2, r12_2);
            r12_6 = stdout;
            rax7 = fun_3580();
            fun_3670(rax7, r12_6, 5, r12_2);
            r12_8 = stdout;
            rax9 = fun_3580();
            fun_3670(rax9, r12_8, 5, r12_2);
            r12_10 = stdout;
            rax11 = fun_3580();
            fun_3670(rax11, r12_10, 5, r12_2);
            r12_12 = stdout;
            rax13 = fun_3580();
            fun_3670(rax13, r12_12, 5, r12_2);
            r12_14 = stdout;
            rax15 = fun_3580();
            fun_3670(rax15, r12_14, 5, r12_2);
            r12_16 = stdout;
            rax17 = fun_3580();
            fun_3670(rax17, r12_16, 5, r12_2);
            r12_18 = stdout;
            rax19 = fun_3580();
            fun_3670(rax19, r12_18, 5, r12_2);
            r12_20 = stdout;
            rax21 = fun_3580();
            fun_3670(rax21, r12_20, 5, r12_2);
            r12_22 = stdout;
            rax23 = fun_3580();
            fun_3670(rax23, r12_22, 5, r12_2);
            r12_24 = stdout;
            rax25 = fun_3580();
            fun_3670(rax25, r12_24, 5, r12_2);
            r12_26 = stdout;
            rax27 = fun_3580();
            fun_3670(rax27, r12_26, 5, r12_2);
            r12_28 = stdout;
            rax29 = fun_3580();
            fun_3670(rax29, r12_28, 5, r12_2);
            r12_30 = stdout;
            rax31 = fun_3580();
            fun_3670(rax31, r12_30, 5, r12_2);
            r12_32 = stdout;
            rax33 = fun_3580();
            fun_3670(rax33, r12_32, 5, r12_2);
            r12_34 = stdout;
            rax35 = fun_3580();
            fun_3670(rax35, r12_34, 5, r12_2);
            r12_36 = stdout;
            rax37 = fun_3580();
            fun_3670(rax37, r12_36, 5, r12_2);
            r12_38 = stdout;
            rax39 = fun_3580();
            fun_3670(rax39, r12_38, 5, r12_2);
            r12_40 = stdout;
            rax41 = fun_3580();
            fun_3670(rax41, r12_40, 5, r12_2);
            r12_42 = stdout;
            rax43 = fun_3580();
            fun_3670(rax43, r12_42, 5, r12_2);
            r12_44 = stdout;
            rax45 = fun_3580();
            fun_3670(rax45, r12_44, 5, r12_2);
            r12_46 = stdout;
            rax47 = fun_3580();
            fun_3670(rax47, r12_46, 5, r12_2);
            r12_48 = stdout;
            rax49 = fun_3580();
            fun_3670(rax49, r12_48, 5, r12_2);
            r12_50 = stdout;
            rax51 = fun_3580();
            fun_3670(rax51, r12_50, 5, r12_2);
            r12_52 = stdout;
            rax53 = fun_3580();
            fun_3670(rax53, r12_52, 5, r12_2);
            r12_54 = stdout;
            rax55 = fun_3580();
            fun_3670(rax55, r12_54, 5, r12_2);
            r12_56 = stdout;
            rax57 = fun_3580();
            fun_3670(rax57, r12_56, 5, r12_2);
            r12_58 = stdout;
            rax59 = fun_3580();
            fun_3670(rax59, r12_58, 5, r12_2);
            do {
                if (1) 
                    break;
                eax60 = fun_36b0("date", 0, "date", 0);
            } while (eax60);
            r13_61 = v4;
            if (!r13_61) {
                rax62 = fun_3580();
                fun_37b0(1, rax62, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax63 = fun_37a0(5);
                if (!rax63 || (eax64 = fun_34f0(rax63, "en_", 3, rax63, "en_", 3), !eax64)) {
                    rax65 = fun_3580();
                    r13_61 = reinterpret_cast<struct s0*>("date");
                    fun_37b0(1, rax65, "https://www.gnu.org/software/coreutils/", "date");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_61 = reinterpret_cast<struct s0*>("date");
                    goto addr_4ac8_9;
                }
            } else {
                rax66 = fun_3580();
                fun_37b0(1, rax66, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax67 = fun_37a0(5);
                if (!rax67 || (eax68 = fun_34f0(rax67, "en_", 3, rax67, "en_", 3), !eax68)) {
                    addr_49ce_11:
                    rax69 = fun_3580();
                    fun_37b0(1, rax69, "https://www.gnu.org/software/coreutils/", "date");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_61 == "date")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x15e39);
                    }
                } else {
                    addr_4ac8_9:
                    r15_70 = stdout;
                    rax71 = fun_3580();
                    fun_3670(rax71, r15_70, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_49ce_11;
                }
            }
            rax72 = fun_3580();
            fun_37b0(1, rax72, r13_61, r12_2);
            addr_449e_14:
            fun_3820();
        }
    } else {
        fun_3580();
        rdi73 = stderr;
        fun_3840(rdi73, rdi73);
        goto addr_449e_14;
    }
}

void fun_4b03() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_3660(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_4b13(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_35a0(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_34f0(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_4b93_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_4be0_6; else 
                    continue;
            } else {
                rax18 = fun_35a0(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_4c10_8;
                if (v16 == -1) 
                    goto addr_4bce_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_4b93_5;
            } else {
                eax19 = fun_3660(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_4b93_5;
            }
            addr_4bce_10:
            v16 = rbx15;
            goto addr_4b93_5;
        }
    }
    addr_4bf5_16:
    return v12;
    addr_4be0_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_4bf5_16;
    addr_4c10_8:
    v12 = rbx15;
    goto addr_4bf5_16;
}

int64_t fun_4c23(void** rdi, void*** rsi) {
    void** r12_3;
    void** rdi4;
    void*** rbp5;
    int64_t rbx6;
    int32_t eax7;

    __asm__("cli ");
    r12_3 = rdi;
    rdi4 = *rsi;
    if (!rdi4) {
        addr_4c68_2:
        return -1;
    } else {
        rbp5 = rsi;
        *reinterpret_cast<int32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
        do {
            eax7 = fun_36b0(rdi4, r12_3);
            if (!eax7) 
                break;
            ++rbx6;
            rdi4 = rbp5[rbx6 * 8];
        } while (rdi4);
        goto addr_4c68_2;
    }
    return rbx6;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_4c83(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_3580();
    } else {
        fun_3580();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_37d0;
}

void fun_4d13(void*** rdi, void** rsi, void** rdx, struct s0* rcx) {
    void** r13_5;
    void** r12_6;
    void** rbp7;
    void** r14_8;
    void*** v9;
    void** rax10;
    void** rsi11;
    void** r15_12;
    int64_t rbx13;
    uint32_t eax14;
    void** rdi15;
    void** rdi16;
    void** rdi17;
    void** rax18;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_5) = 0;
    *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0;
    r12_6 = rdx;
    rbp7 = rsi;
    r14_8 = stderr;
    v9 = rdi;
    rax10 = fun_3580();
    rsi11 = r14_8;
    fun_3670(rax10, rsi11, 5, rcx);
    r15_12 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx13) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx13) + 4) = 0;
    if (r15_12) {
        do {
            if (!rbx13 || (rsi11 = rbp7, eax14 = fun_3660(r13_5, rsi11, r12_6, r13_5, rsi11, r12_6), !!eax14)) {
                r13_5 = rbp7;
                quote(r15_12, rsi11, r15_12, rsi11);
                rdi15 = stderr;
                *reinterpret_cast<int32_t*>(&rsi11) = 1;
                *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                fun_3840(rdi15, rdi15);
            } else {
                quote(r15_12, rsi11, r15_12, rsi11);
                rdi16 = stderr;
                *reinterpret_cast<int32_t*>(&rsi11) = 1;
                *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                fun_3840(rdi16, rdi16);
            }
            ++rbx13;
            rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r12_6));
            r15_12 = v9[rbx13 * 8];
        } while (r15_12);
    }
    rdi17 = stderr;
    rax18 = *reinterpret_cast<void***>(rdi17 + 40);
    if (reinterpret_cast<unsigned char>(rax18) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi17 + 48))) {
        goto fun_3600;
    } else {
        *reinterpret_cast<void***>(rdi17 + 40) = rax18 + 1;
        *reinterpret_cast<void***>(rax18) = reinterpret_cast<void**>(10);
        return;
    }
}

int64_t argmatch(void** rdi, void*** rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(void*** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_4e43(int64_t rdi, void** rsi, void*** rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    int64_t r13_10;
    int64_t r12_11;
    void*** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int32_t eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_4e87_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_4efe_4;
        } else {
            addr_4efe_4:
            return rax14;
        }
    }
    rdi15 = *rdx;
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_36b0(rdi15, r14_9);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = rbp12[rbx16 * 8];
        } while (rdi15);
        goto addr_4e80_8;
    } else {
        goto addr_4e80_8;
    }
    return rbx16;
    addr_4e80_8:
    rax14 = -1;
    goto addr_4e87_3;
}

struct s20 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_4f13(void** rdi, struct s20* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_3660(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t file_name = 0;

void fun_4f73(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4f83(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_3500(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4f93() {
    void** rdi1;
    int32_t eax2;
    void*** rax3;
    int1_t zf4;
    void*** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_34e0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*rax3 == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_3580();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_5023_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_37d0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_3500(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_5023_5:
        *reinterpret_cast<void***>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_37d0();
    }
}

struct s21 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_3710(struct s21* rdi);

int32_t fun_3780(struct s21* rdi);

int64_t fun_3620(int64_t rdi, ...);

int32_t rpl_fflush(struct s21* rdi);

int64_t fun_3560(struct s21* rdi);

int64_t fun_5043(struct s21* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void*** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_3710(rdi);
    if (eax2 >= 0) {
        eax3 = fun_3780(rdi);
        if (!(eax3 && (eax4 = fun_3710(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_3620(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_34e0();
            r12d9 = *rax8;
            rax10 = fun_3560(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_3560;
}

void rpl_fseeko(struct s21* rdi);

void fun_50d3(struct s21* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_3780(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

void* fun_6ca3(void** rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8) {
    int64_t v6;
    void* rax7;

    __asm__("cli ");
    rax7 = __strftime_internal_isra_0(rdi, rsi, rdx, 0, 0, -1, rcx, r8, v6);
    return rax7;
}

int64_t fun_6cc3(struct s21* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_3710(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_3620(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_3530();

void fun_6d43(int64_t rdi) {
    __asm__("cli ");
    goto fun_3530;
}

int64_t fun_6d53() {
    struct s0* rax1;
    void* rcx2;
    int64_t v3;

    __asm__("cli ");
    rax1 = g28;
    fun_3530();
    rcx2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax1) - reinterpret_cast<int64_t>(g28));
    if (rcx2) {
        fun_35b0();
    } else {
        return v3;
    }
}

void fun_3760();

int64_t current_timespec();

int64_t fun_6da3() {
    struct s0* rax1;
    struct s0* v2;
    int64_t r13_3;
    int64_t v4;
    void* rax5;
    int64_t r12_6;
    int64_t rbp7;
    int32_t ebx8;
    int64_t rax9;
    int64_t rsi10;
    int64_t rcx11;
    int64_t rdx12;
    int64_t rax13;

    __asm__("cli ");
    rax1 = g28;
    v2 = rax1;
    fun_3760();
    r13_3 = v4;
    if (reinterpret_cast<uint1_t>(r13_3 < 0) | reinterpret_cast<uint1_t>(r13_3 == 0)) {
        *reinterpret_cast<int32_t*>(&r13_3) = 0x3b9aca00;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_3) + 4) = 0;
        goto addr_6ddc_3;
    } else {
        if (r13_3 == 1) {
            addr_6e28_5:
            rax5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v2) - reinterpret_cast<int64_t>(g28));
            if (rax5) {
                fun_35b0();
            } else {
                return r13_3;
            }
        } else {
            addr_6ddc_3:
            r12_6 = -1;
            *reinterpret_cast<int32_t*>(&rbp7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp7) + 4) = 0;
            ebx8 = 0;
        }
        while (1) {
            rax9 = current_timespec();
            rsi10 = rax9;
            rcx11 = rdx12;
            if (rbp7 != rax9 || r12_6 != rdx12) {
                if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(rcx11 < 0) | reinterpret_cast<uint1_t>(rcx11 == 0))) {
                    rdx12 = rcx11;
                    rax13 = r13_3;
                    do {
                        r13_3 = rdx12;
                        rdx12 = rax13 % r13_3;
                        rax13 = r13_3;
                    } while (rdx12);
                }
                ++ebx8;
                if (r13_3 <= 1) 
                    goto addr_6e28_5;
                if (ebx8 > 31) 
                    goto addr_6e28_5;
                r12_6 = rcx11;
                rbp7 = rsi10;
            } else {
                ebx8 = 32;
            }
        }
    }
}

struct s22 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_6e93(uint64_t rdi, struct s22* rsi) {
    signed char* r8_3;
    uint64_t rcx4;
    signed char* rsi5;
    uint64_t rdx6;
    int32_t eax7;
    uint64_t rdx8;
    uint64_t rax9;
    uint64_t rcx10;
    int32_t ecx11;

    __asm__("cli ");
    rsi->f14 = 0;
    r8_3 = &rsi->f14;
    rcx4 = rdi;
    if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
        do {
            rsi5 = r8_3;
            --r8_3;
            rdx6 = (__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rcx4) >> 63);
            eax7 = static_cast<int32_t>(48 + (rdx6 + rdx6 * 4) * 2) - *reinterpret_cast<int32_t*>(&rcx4);
            rcx4 = rdx6;
            *r8_3 = *reinterpret_cast<signed char*>(&eax7);
        } while (rdx6);
        *(r8_3 - 1) = 45;
        return rsi5 - 2;
    } else {
        do {
            --r8_3;
            rdx8 = __intrinsic() >> 3;
            rax9 = rdx8 + rdx8 * 4;
            rcx10 = rcx4 - (rax9 + rax9);
            ecx11 = *reinterpret_cast<int32_t*>(&rcx10) + 48;
            *r8_3 = *reinterpret_cast<signed char*>(&ecx11);
            rcx4 = rdx8;
        } while (rdx8);
        return r8_3;
    }
}

struct s23 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    void** f18;
    signed char[7] pad32;
    int64_t f20;
    int64_t f28;
    int32_t f30;
};

struct s24 {
    void** f0;
    signed char[55] pad56;
    void** f38;
    signed char[7] pad64;
    void** f40;
    signed char[7] pad72;
    int64_t f48;
    void** f50;
    signed char[7] pad88;
    int64_t f58;
    int64_t f60;
    int32_t f68;
};

struct s25 {
    signed char[93568] pad93568;
    signed char f16d80;
    signed char[31] pad93600;
    signed char f16da0;
};

struct s26 {
    signed char[8] pad8;
    uint32_t f8;
    signed char[4] pad16;
    void** f10;
};

struct s27 {
    signed char[8] pad8;
    uint32_t f8;
    signed char[4] pad16;
    void** f10;
};

struct s28 {
    signed char[8] pad8;
    uint32_t f8;
    signed char[4] pad16;
    void** f10;
};

int64_t fun_7cf3(void** rdi) {
    uint32_t r15d2;
    void** r9_3;
    void** rcx4;
    void** r12_5;
    void* rsp6;
    struct s0* rax7;
    struct s0* v8;
    void** r11_9;
    void** rbx10;
    uint32_t v11;
    void** r13_12;
    void** r14_13;
    int64_t rax14;
    int64_t rax15;
    void* rdx16;
    int64_t rdx17;
    int32_t eax18;
    int64_t r10_19;
    uint64_t rcx20;
    int64_t rdx21;
    struct s23* rdx22;
    void** v23;
    int64_t v24;
    void** v25;
    int64_t v26;
    int64_t v27;
    void** v28;
    struct s24* rax29;
    void** rdx30;
    uint32_t edi31;
    uint32_t esi32;
    struct s25* rax33;
    uint32_t ecx34;
    uint32_t eax35;
    uint32_t r10d36;
    void** rsi37;
    int64_t rdx38;
    void** r8_39;
    int32_t eax40;
    int64_t rdx41;
    void** rdi42;
    uint32_t eax43;
    int64_t rax44;
    int64_t rax45;
    uint32_t eax46;
    uint64_t rdx47;
    void** v48;
    uint32_t edx49;
    void** r10_50;
    uint32_t edi51;
    int64_t rdx52;
    uint32_t edx53;
    void** r10_54;
    int32_t edi55;
    uint32_t r15d56;
    uint32_t edx57;
    uint32_t esi58;
    int64_t r10_59;
    int64_t v60;
    uint32_t esi61;
    void** rsi62;
    void** rax63;
    int64_t rdi64;
    int32_t edx65;
    uint32_t eax66;
    unsigned char v67;
    int64_t rdx68;
    uint32_t v69;
    void** rsi70;
    struct s26* rbx71;
    void** r15_72;
    uint32_t v73;
    int32_t eax74;
    struct s26* r15_75;
    uint32_t v76;
    void** rax77;
    uint32_t eax78;
    signed char v79;
    void** rsi80;
    int32_t eax81;
    unsigned char dl82;
    void** r11_83;
    struct s7* rax84;
    uint32_t edx85;
    struct s7* rsi86;
    int32_t eax87;
    uint32_t r10d88;
    void** rsi89;
    uint32_t edx90;
    struct s27* rbx91;
    uint32_t r15d92;
    void** v93;
    void** r12_94;
    uint32_t v95;
    unsigned char v96;
    int32_t eax97;
    uint32_t r9d98;
    void** rbx99;
    uint32_t r10d100;
    void** r12_101;
    struct s28* r15_102;
    void** rsi103;
    int32_t eax104;
    void** rsi105;
    int32_t eax106;
    uint32_t eax107;
    unsigned char v108;
    void** rdi109;
    void** rdi110;
    void** rsi111;
    struct s7* rax112;
    void** rax113;
    void** v114;
    uint1_t zf115;

    __asm__("cli ");
    r15d2 = 0xfffffffe;
    *reinterpret_cast<uint32_t*>(&r9_3) = 0;
    *reinterpret_cast<uint32_t*>(&rcx4) = 0xfffffff2;
    *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
    r12_5 = rdi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x588);
    rax7 = g28;
    v8 = rax7;
    r11_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0x540);
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0xe0);
    v11 = 0;
    r13_12 = r11_9;
    r14_13 = r11_9;
    goto addr_7d61_2;
    addr_827c_3:
    return rax14;
    addr_7fa4_4:
    goto *reinterpret_cast<int32_t*>(0x16980 + rax15 * 4) + 0x16980;
    while (1) {
        addr_94a4_5:
        *reinterpret_cast<int32_t*>(&rax14) = 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        while (1) {
            while (rdx16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v8) - reinterpret_cast<int64_t>(g28)), !!rdx16) {
                fun_35b0();
                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                goto addr_9a02_8;
                addr_8260_9:
                *reinterpret_cast<int32_t*>(&rax14) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                continue;
                while (1) {
                    addr_7e50_10:
                    rdx17 = *reinterpret_cast<int32_t*>(&r9_3);
                    eax18 = *reinterpret_cast<signed char*>(0x16dc0 + rdx17);
                    if (eax18) {
                        while (1) {
                            r10_19 = eax18;
                            *reinterpret_cast<uint32_t*>(&rax15) = eax18 - 4;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                            rcx20 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(0x16bc0 + r10_19)));
                            rdx21 = 1 - *reinterpret_cast<int32_t*>(&rcx20);
                            rdx22 = reinterpret_cast<struct s23*>(reinterpret_cast<unsigned char>(rbx10) + reinterpret_cast<uint64_t>((rdx21 * 8 - rdx21) * 8));
                            r11_9 = rdx22->f0;
                            v23 = rdx22->f8;
                            v24 = rdx22->f10;
                            v25 = rdx22->f18;
                            v26 = rdx22->f20;
                            v27 = rdx22->f28;
                            *reinterpret_cast<int32_t*>(&v28) = rdx22->f30;
                            if (*reinterpret_cast<uint32_t*>(&rax15) <= 88) 
                                goto addr_7fa4_4;
                            rax29 = reinterpret_cast<struct s24*>(reinterpret_cast<unsigned char>(rbx10) - (rcx20 * 8 - rcx20 << 3));
                            rax29->f40 = v23;
                            rbx10 = reinterpret_cast<void**>(&rax29->f38);
                            rdx30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_12) - rcx20);
                            *reinterpret_cast<void***>(&rax29->f38) = r11_9;
                            rax29->f48 = v24;
                            rax29->f50 = v25;
                            rax29->f58 = v26;
                            rax29->f60 = v27;
                            rax29->f68 = *reinterpret_cast<int32_t*>(&v28);
                            edi31 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx30))));
                            esi32 = edi31;
                            rax33 = reinterpret_cast<struct s25*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(0x16c20 + r10_19) - 29));
                            ecx34 = rax33->f16da0 + edi31;
                            if (ecx34 > 0x72 || (rcx4 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(ecx34))), *reinterpret_cast<signed char*>(&esi32) != *reinterpret_cast<signed char*>(rcx4 + 0x16c80))) {
                                rcx4 = reinterpret_cast<void**>(0x16d80);
                                *reinterpret_cast<uint32_t*>(&r9_3) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x16d80) + reinterpret_cast<uint64_t>(rax33))));
                                eax35 = *reinterpret_cast<uint32_t*>(&r9_3);
                            } else {
                                *reinterpret_cast<uint32_t*>(&r9_3) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(rcx4 + 0x16d00)));
                                eax35 = *reinterpret_cast<uint32_t*>(&r9_3);
                            }
                            while (1) {
                                *reinterpret_cast<void***>(rdx30 + 1) = *reinterpret_cast<void***>(&eax35);
                                r13_12 = rdx30 + 1;
                                if (reinterpret_cast<unsigned char>(r13_12) >= reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(rsp6) + 0x553)) 
                                    goto addr_94a4_5;
                                if (*reinterpret_cast<uint32_t*>(&r9_3) == 12) 
                                    goto addr_94ae_17;
                                *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x16e40 + *reinterpret_cast<int32_t*>(&r9_3))));
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                addr_7d61_2:
                                r10d36 = *reinterpret_cast<uint32_t*>(&rcx4);
                                if (*reinterpret_cast<uint32_t*>(&rcx4) == 0xffffffa5) 
                                    goto addr_7e50_10;
                                if (r15d2 == 0xfffffffe) {
                                    rsi37 = *reinterpret_cast<void***>(r12_5);
                                    *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                    do {
                                        addr_82a7_21:
                                        if (*reinterpret_cast<signed char*>(&rdx38) <= reinterpret_cast<signed char>(13)) {
                                            if (*reinterpret_cast<signed char*>(&rdx38) > reinterpret_cast<signed char>(8)) 
                                                goto addr_8411_23; else 
                                                goto addr_82b9_24;
                                        }
                                        if (*reinterpret_cast<unsigned char*>(&rdx38) != 32) {
                                            addr_82b9_24:
                                            *reinterpret_cast<uint32_t*>(&r8_39) = static_cast<uint32_t>(rdx38 - 43) & 0xfffffffd;
                                            *reinterpret_cast<int32_t*>(&r8_39 + 4) = 0;
                                            if (*reinterpret_cast<unsigned char*>(&rdx38) - 48 <= 9) {
                                                if (*reinterpret_cast<signed char*>(&r8_39)) 
                                                    break;
                                            } else {
                                                if (*reinterpret_cast<signed char*>(&r8_39)) {
                                                    if (*reinterpret_cast<signed char*>(&rdx38) > reinterpret_cast<signed char>(90)) {
                                                        eax40 = static_cast<int32_t>(rdx38 - 97);
                                                        if (*reinterpret_cast<unsigned char*>(&eax40) <= 25) 
                                                            goto addr_854b_30;
                                                    } else {
                                                        if (*reinterpret_cast<signed char*>(&rdx38) > reinterpret_cast<signed char>(64)) 
                                                            goto addr_854b_30;
                                                    }
                                                    if (*reinterpret_cast<unsigned char*>(&rdx38) != 40) 
                                                        goto addr_98b0_33;
                                                    *reinterpret_cast<int32_t*>(&rdx41) = 0;
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx41) + 4) = 0;
                                                    do {
                                                        rdi42 = rsi37;
                                                        ++rsi37;
                                                        *reinterpret_cast<void***>(r12_5) = rsi37;
                                                        eax43 = *reinterpret_cast<unsigned char*>(rsi37 + 0xffffffffffffffff);
                                                        if (!*reinterpret_cast<signed char*>(&eax43)) 
                                                            goto addr_80c8_36;
                                                        if (*reinterpret_cast<signed char*>(&eax43) != 40) {
                                                            *reinterpret_cast<uint32_t*>(&rax44) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax43) == 41);
                                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
                                                            rdx41 = rdx41 - rax44;
                                                        } else {
                                                            ++rdx41;
                                                        }
                                                    } while (rdx41);
                                                    *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi42 + 1));
                                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                                    goto addr_82a7_21;
                                                }
                                            }
                                        } else {
                                            addr_8411_23:
                                            ++rsi37;
                                            *reinterpret_cast<void***>(r12_5) = rsi37;
                                            *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                            goto addr_82a7_21;
                                        }
                                        *reinterpret_cast<int32_t*>(&rax45) = 0;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax45) + 4) = 0;
                                        *reinterpret_cast<unsigned char*>(&rax45) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rdx38) != 45);
                                        eax46 = static_cast<uint32_t>(rax45 + rax45 - 1);
                                        while (1) {
                                            ++rsi37;
                                            *reinterpret_cast<void***>(r12_5) = rsi37;
                                            *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37));
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                            if (*reinterpret_cast<signed char*>(&rdx38) > reinterpret_cast<signed char>(13)) {
                                                if (*reinterpret_cast<unsigned char*>(&rdx38) != 32) 
                                                    break;
                                            } else {
                                                if (*reinterpret_cast<signed char*>(&rdx38) <= reinterpret_cast<signed char>(8)) 
                                                    goto addr_8301_46;
                                            }
                                        }
                                        addr_8301_46:
                                    } while (*reinterpret_cast<unsigned char*>(&rdx38) - 48 > 9);
                                    goto addr_830c_48;
                                } else {
                                    addr_7d77_49:
                                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(r15d2) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(r15d2 == 0)) {
                                        addr_80c8_36:
                                        *reinterpret_cast<uint32_t*>(&rdx47) = 0;
                                        r15d2 = 0;
                                        goto addr_7da3_50;
                                    } else {
                                        if (r15d2 == 0x100) {
                                            r15d2 = 0x101;
                                            addr_7ea3_53:
                                            while (*reinterpret_cast<uint32_t*>(&rcx4) == 0xffffffa5 || ((*reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4) + 1, *reinterpret_cast<uint32_t*>(&rcx4) > 0x72) || ((rcx4 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rcx4))), *reinterpret_cast<signed char*>(rcx4 + 0x16c80) != 1) || (*reinterpret_cast<uint32_t*>(&r9_3) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(rcx4 + 0x16d00))), eax35 = *reinterpret_cast<uint32_t*>(&r9_3), reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_3) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_3) == 0))))) {
                                                if (r13_12 == r14_13) 
                                                    goto addr_8260_9;
                                                --r13_12;
                                                rbx10 = rbx10 - 56;
                                                *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x16e40 + *reinterpret_cast<signed char*>(r13_12 + 0xffffffffffffffff))));
                                            }
                                            rbx10 = rbx10 + 56;
                                            v11 = 3;
                                            __asm__("movdqa xmm5, [rsp+0xc0]");
                                            __asm__("movdqa xmm3, [rsp+0xa0]");
                                            *reinterpret_cast<void***>(rbx10 + 48) = v48;
                                            rdx30 = r13_12;
                                            __asm__("movdqa xmm4, [rsp+0xb0]");
                                            __asm__("movups [rbx], xmm3");
                                            __asm__("movups [rbx+0x10], xmm4");
                                            __asm__("movups [rbx+0x20], xmm5");
                                            continue;
                                        }
                                        if (reinterpret_cast<int32_t>(r15d2) <= reinterpret_cast<int32_t>(0x115)) 
                                            goto addr_8460_58; else 
                                            goto addr_7d9a_59;
                                    }
                                }
                                eax46 = 0;
                                addr_830c_48:
                                *reinterpret_cast<uint32_t*>(&r8_39) = 0;
                                *reinterpret_cast<int32_t*>(&r8_39 + 4) = 0;
                                while (1) {
                                    edx49 = *reinterpret_cast<uint32_t*>(&rdx38) - 48;
                                    if (eax46 == 0xffffffff) {
                                        edx49 = 48 - *reinterpret_cast<uint32_t*>(&rdx38);
                                    }
                                    r8_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_39) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(edx49))));
                                    if (__intrinsic()) 
                                        break;
                                    *reinterpret_cast<uint32_t*>(&rdx38) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37 + 1));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx38) + 4) = 0;
                                    r10_50 = rsi37 + 1;
                                    *reinterpret_cast<uint32_t*>(&r11_9) = static_cast<uint32_t>(rdx38 - 48);
                                    *reinterpret_cast<int32_t*>(&r11_9 + 4) = 0;
                                    if (*reinterpret_cast<uint32_t*>(&r11_9) > 9) 
                                        goto addr_8347_65;
                                    r8_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_39) * 10);
                                    if (__intrinsic()) 
                                        break;
                                    rsi37 = r10_50;
                                }
                                r15d2 = 63;
                                goto addr_8460_58;
                                addr_8347_65:
                                edi51 = *reinterpret_cast<uint32_t*>(&rdx38) & 0xfffffffd;
                                if (*reinterpret_cast<signed char*>(&edi51) != 44 || (*reinterpret_cast<uint32_t*>(&rdx52) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r10_50 + 1) - 48), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx52) > 9)) {
                                    *reinterpret_cast<void***>(r12_5) = r10_50;
                                    edx53 = eax46 >> 31;
                                    *reinterpret_cast<uint32_t*>(&rdx47) = edx53 - (edx53 + reinterpret_cast<uint1_t>(edx53 < edx53 + reinterpret_cast<uint1_t>(!!eax46))) + 20;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                    r15d2 = 48 - (48 + reinterpret_cast<uint1_t>(48 < 48 + reinterpret_cast<uint1_t>(!!eax46))) + 0x113;
                                } else {
                                    *reinterpret_cast<uint32_t*>(&r11_9) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rsi37 + 3))));
                                    *reinterpret_cast<int32_t*>(&r11_9 + 4) = 0;
                                    r10_54 = rsi37 + 3;
                                    edi55 = 8;
                                    r15d56 = *reinterpret_cast<uint32_t*>(&r11_9);
                                    do {
                                        edx57 = static_cast<uint32_t>(rdx52 + rdx52 * 4);
                                        esi58 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(r11_9 + 0xffffffffffffffd0));
                                        *reinterpret_cast<uint32_t*>(&rdx52) = edx57 + edx57;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
                                        if (esi58 <= 9) {
                                            *reinterpret_cast<uint32_t*>(&r11_9) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r10_54 + 1))));
                                            *reinterpret_cast<int32_t*>(&r11_9 + 4) = 0;
                                            *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx52) + esi58;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
                                            ++r10_54;
                                            r15d56 = *reinterpret_cast<uint32_t*>(&r11_9);
                                            esi58 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(r11_9 + 0xffffffffffffffd0));
                                        }
                                        --edi55;
                                    } while (edi55);
                                    if (eax46 == 0xffffffff) 
                                        goto addr_88d6_75; else 
                                        goto addr_83ae_76;
                                }
                                addr_846e_77:
                                r10d36 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rcx4) + rdx47);
                                addr_7da3_50:
                                if (r10d36 > 0x72) 
                                    goto addr_7e50_10;
                                r10_59 = reinterpret_cast<int32_t>(r10d36);
                                if (static_cast<int32_t>(*reinterpret_cast<signed char*>(0x16c80 + r10_59)) != *reinterpret_cast<uint32_t*>(&rdx47)) 
                                    goto addr_7e50_10;
                                *reinterpret_cast<uint32_t*>(&r9_3) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x16d00 + r10_59)));
                                eax35 = *reinterpret_cast<uint32_t*>(&r9_3);
                                if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_3) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r9_3) == 0)) 
                                    break;
                                r15d2 = 0xfffffffe;
                                __asm__("movdqa xmm2, [rsp+0xc0]");
                                *reinterpret_cast<int64_t*>(rbx10 + 0x68) = v60;
                                rdx30 = r13_12;
                                __asm__("movups [rbx+0x58], xmm2");
                                rbx10 = rbx10 + 56;
                                v11 = v11 - 1 + reinterpret_cast<uint1_t>(v11 < 1);
                                __asm__("movdqa xmm0, [rsp+0xa0]");
                                __asm__("movdqa xmm1, [rsp+0xb0]");
                                __asm__("movups [rbx], xmm0");
                                __asm__("movups [rbx+0x10], xmm1");
                                continue;
                                addr_88d6_75:
                                if (esi58 > 9) {
                                    addr_88f9_81:
                                    if (*reinterpret_cast<uint32_t*>(&rdx52)) {
                                        addr_83d4_82:
                                        --r8_39;
                                        if (__intrinsic()) {
                                            addr_9509_83:
                                            *reinterpret_cast<uint32_t*>(&rdx47) = 2;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                            r15d2 = 63;
                                            goto addr_846e_77;
                                        } else {
                                            *reinterpret_cast<void***>(r12_5) = r10_54;
                                            r15d2 = 0x114;
                                            *reinterpret_cast<uint32_t*>(&rdx47) = 21;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                            goto addr_846e_77;
                                        }
                                    } else {
                                        *reinterpret_cast<void***>(r12_5) = r10_54;
                                        *reinterpret_cast<uint32_t*>(&rdx47) = 21;
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                        r15d2 = 0x114;
                                        goto addr_846e_77;
                                    }
                                } else {
                                    do {
                                        if (*reinterpret_cast<signed char*>(&r15d56) != 48) 
                                            break;
                                        esi61 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r10_54 + 1))));
                                        ++r10_54;
                                        r15d56 = esi61;
                                    } while (esi61 - 48 <= 9);
                                    goto addr_88f9_81;
                                }
                                *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx52) + 1;
                                esi58 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r10_54) - 48);
                                addr_83ae_76:
                                if (esi58 <= 9) {
                                    do {
                                        ++r10_54;
                                    } while (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(r10_54 + 1) - 48) <= 9);
                                }
                                if (reinterpret_cast<int32_t>(eax46) >= reinterpret_cast<int32_t>(0)) 
                                    goto addr_8930_91;
                                if (*reinterpret_cast<uint32_t*>(&rdx52)) 
                                    goto addr_83d4_82;
                                addr_8930_91:
                                *reinterpret_cast<void***>(r12_5) = r10_54;
                                *reinterpret_cast<uint32_t*>(&rdx47) = *reinterpret_cast<uint32_t*>(&rdx52) - (*reinterpret_cast<uint32_t*>(&rdx52) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx52) < *reinterpret_cast<uint32_t*>(&rdx52) + reinterpret_cast<uint1_t>(!!eax46))) + 22;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                r15d2 = r15d56 - (r15d56 + reinterpret_cast<uint1_t>(r15d56 < r15d56 + reinterpret_cast<uint1_t>(!!eax46))) + 0x115;
                                goto addr_846e_77;
                                addr_854b_30:
                                r11_9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0x560);
                                rsi62 = rsi37 + 1;
                                r8_39 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 0x573);
                                rax63 = r11_9;
                                while (1) {
                                    if (reinterpret_cast<unsigned char>(rax63) < reinterpret_cast<unsigned char>(r8_39)) {
                                        ++rax63;
                                    }
                                    *reinterpret_cast<void***>(r12_5) = rsi62;
                                    *reinterpret_cast<uint32_t*>(&rdi64) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi62));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi64) + 4) = 0;
                                    if (*reinterpret_cast<signed char*>(&rdi64) <= 90) {
                                        if (*reinterpret_cast<signed char*>(&rdi64) > 64) {
                                            addr_8574_97:
                                            ++rsi62;
                                        } else {
                                            addr_856e_98:
                                            if (*reinterpret_cast<signed char*>(&rdi64) != 46) 
                                                break; else 
                                                goto addr_8574_97;
                                        }
                                    } else {
                                        edx65 = static_cast<int32_t>(rdi64 - 97);
                                        if (*reinterpret_cast<unsigned char*>(&edx65) <= 25) 
                                            goto addr_8574_97;
                                        goto addr_856e_98;
                                    }
                                }
                                eax66 = v67;
                                if (*reinterpret_cast<unsigned char*>(&eax66)) {
                                    do {
                                        *reinterpret_cast<uint32_t*>(&rdx68) = *reinterpret_cast<unsigned char*>(&eax66);
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx68) + 4) = 0;
                                        if (static_cast<uint32_t>(rdx68 - 97) < 26) {
                                        }
                                        eax66 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v8) + 6);
                                    } while (*reinterpret_cast<unsigned char*>(&eax66));
                                }
                                v69 = *reinterpret_cast<uint32_t*>(&rcx4);
                                rsi70 = reinterpret_cast<void**>("AM");
                                v25 = rbx10;
                                rbx71 = reinterpret_cast<struct s26*>(0x1bae0);
                                r15_72 = r11_9;
                                *reinterpret_cast<uint32_t*>(&v23) = *reinterpret_cast<uint32_t*>(&r9_3);
                                v73 = r10d36;
                                do {
                                    eax74 = fun_36b0(r15_72, rsi70);
                                    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    if (!eax74) 
                                        break;
                                    rsi70 = rbx71->f10;
                                    rbx71 = reinterpret_cast<struct s26*>(&rbx71->f10);
                                } while (rsi70);
                                goto addr_93b0_109;
                                r15_75 = rbx71;
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&v23);
                                r10d36 = v73;
                                rbx10 = v25;
                                *reinterpret_cast<uint32_t*>(&rcx4) = v69;
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                addr_8643_111:
                                r15d2 = r15_75->f8;
                                addr_864b_112:
                                goto addr_7d77_49;
                                addr_93b0_109:
                                rbx10 = v25;
                                v76 = *reinterpret_cast<uint32_t*>(&v23);
                                rax77 = fun_35a0(r15_72);
                                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&v23) = 1;
                                *reinterpret_cast<uint32_t*>(&rcx4) = v69;
                                v28 = rax77;
                                r10d36 = v73;
                                *reinterpret_cast<uint32_t*>(&r9_3) = v76;
                                if (rax77 != 3 && (*reinterpret_cast<uint32_t*>(&v23) = 0, reinterpret_cast<int1_t>(rax77 == 4))) {
                                    eax78 = 0;
                                    *reinterpret_cast<unsigned char*>(&eax78) = reinterpret_cast<uint1_t>(v79 == 46);
                                    *reinterpret_cast<uint32_t*>(&v23) = eax78;
                                }
                                v25 = r15_72;
                                r15_75 = reinterpret_cast<struct s26*>(0x1b940);
                                rsi80 = reinterpret_cast<void**>("JANUARY");
                                do {
                                    *reinterpret_cast<uint32_t*>(&r11_9) = *reinterpret_cast<uint32_t*>(&v23);
                                    *reinterpret_cast<int32_t*>(&r11_9 + 4) = 0;
                                    if (*reinterpret_cast<uint32_t*>(&r11_9)) {
                                        eax81 = fun_34f0(v25, rsi80, 3);
                                        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    } else {
                                        eax81 = fun_36b0(v25, rsi80);
                                        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    }
                                    *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                    r10d36 = r10d36;
                                    dl82 = reinterpret_cast<uint1_t>(eax81 == 0);
                                    *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4);
                                    *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                    if (dl82) 
                                        goto addr_8643_111;
                                    rsi80 = r15_75->f10;
                                    r15_75 = reinterpret_cast<struct s26*>(&r15_75->f10);
                                } while (rsi80);
                                r11_83 = v25;
                                *reinterpret_cast<uint32_t*>(&v25) = *reinterpret_cast<uint32_t*>(&rcx4);
                                v23 = r11_83;
                                rax84 = lookup_zone(r12_5, r11_83);
                                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                r11_9 = v23;
                                *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&v25);
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                edx85 = dl82;
                                r10d36 = r10d36;
                                rsi86 = rax84;
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                if (!rax84) {
                                    *reinterpret_cast<uint32_t*>(&v25) = r10d36;
                                    v23 = r11_9;
                                    eax87 = fun_36b0(r11_9, "DST");
                                    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    r11_9 = v23;
                                    r10d88 = *reinterpret_cast<uint32_t*>(&v25);
                                    rsi89 = reinterpret_cast<void**>("YEAR");
                                    edx90 = *reinterpret_cast<unsigned char*>(&edx85);
                                    *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4);
                                    *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                    *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                    if (!eax87) {
                                        r15d2 = 0x103;
                                        goto addr_8460_58;
                                    } else {
                                        v25 = rbx10;
                                        rbx91 = reinterpret_cast<struct s27*>(0x1b880);
                                        r15d92 = *reinterpret_cast<uint32_t*>(&rcx4);
                                        v93 = r12_5;
                                        r12_94 = r11_9;
                                        *reinterpret_cast<uint32_t*>(&v23) = *reinterpret_cast<uint32_t*>(&r9_3);
                                        v95 = r10d88;
                                        v96 = *reinterpret_cast<unsigned char*>(&edx90);
                                        do {
                                            eax97 = fun_36b0(r12_94, rsi89);
                                            rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                            if (!eax97) 
                                                break;
                                            rsi89 = rbx91->f10;
                                            rbx91 = reinterpret_cast<struct s27*>(&rbx91->f10);
                                        } while (rsi89);
                                        goto addr_96db_126;
                                    }
                                } else {
                                    addr_94fc_127:
                                    r15d2 = rsi86->f8;
                                    goto addr_864b_112;
                                }
                                *reinterpret_cast<uint32_t*>(&rcx4) = r15d92;
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&v23);
                                r10d36 = v95;
                                r12_5 = v93;
                                rbx10 = v25;
                                r15d2 = rbx91->f8;
                                goto addr_864b_112;
                                addr_96db_126:
                                r11_9 = r12_94;
                                r9d98 = *reinterpret_cast<uint32_t*>(&v23);
                                *reinterpret_cast<uint32_t*>(&rcx4) = r15d92;
                                rbx99 = v25;
                                r10d100 = v95;
                                r8_39 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r11_9) + reinterpret_cast<unsigned char>(v28) + 0xffffffffffffffff);
                                *reinterpret_cast<uint32_t*>(&rdx16) = v96;
                                r12_101 = v93;
                                if (*reinterpret_cast<void***>(r8_39) == 83) {
                                    v23 = r12_101;
                                    r15_102 = reinterpret_cast<struct s28*>(0x1b880);
                                    r12_5 = rbx99;
                                    *reinterpret_cast<uint32_t*>(&rbx10) = r9d98;
                                    v25 = r14_13;
                                    rsi103 = reinterpret_cast<void**>("YEAR");
                                    *reinterpret_cast<uint32_t*>(&r14_13) = r10d100;
                                    *reinterpret_cast<void***>(r8_39) = reinterpret_cast<void**>(0);
                                    do {
                                        eax104 = fun_36b0(r11_9, rsi103);
                                        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                        r11_9 = r11_9;
                                        *reinterpret_cast<uint32_t*>(&rdx16) = *reinterpret_cast<unsigned char*>(&rdx16);
                                        r8_39 = r8_39;
                                        *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4);
                                        *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                        if (!eax104) 
                                            break;
                                        rsi103 = r15_102->f10;
                                        r15_102 = reinterpret_cast<struct s28*>(&r15_102->f10);
                                    } while (rsi103);
                                    goto addr_9a02_8;
                                } else {
                                    addr_970e_132:
                                    v23 = r12_101;
                                    r15_102 = reinterpret_cast<struct s28*>(0x1b720);
                                    r12_5 = rbx99;
                                    *reinterpret_cast<uint32_t*>(&rbx10) = r9d98;
                                    v25 = r14_13;
                                    rsi105 = reinterpret_cast<void**>("TOMORROW");
                                    *reinterpret_cast<uint32_t*>(&r14_13) = r10d100;
                                    goto addr_9742_133;
                                }
                                addr_9769_135:
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&rbx10);
                                r10d36 = *reinterpret_cast<uint32_t*>(&r14_13);
                                rbx10 = r12_5;
                                r14_13 = v25;
                                r12_5 = v23;
                                r15d2 = r15_102->f8;
                                goto addr_864b_112;
                                addr_9a02_8:
                                r9d98 = *reinterpret_cast<uint32_t*>(&rbx10);
                                r10d100 = *reinterpret_cast<uint32_t*>(&r14_13);
                                rbx99 = r12_5;
                                *reinterpret_cast<void***>(r8_39) = reinterpret_cast<void**>(83);
                                r12_101 = v23;
                                r14_13 = v25;
                                goto addr_970e_132;
                                do {
                                    addr_9742_133:
                                    eax106 = fun_36b0(r11_9, rsi105);
                                    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                    r11_9 = r11_9;
                                    *reinterpret_cast<uint32_t*>(&rdx16) = *reinterpret_cast<unsigned char*>(&rdx16);
                                    *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&rcx4);
                                    *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                    if (!eax106) 
                                        goto addr_9769_135;
                                    rsi105 = r15_102->f10;
                                    r15_102 = reinterpret_cast<struct s28*>(&r15_102->f10);
                                } while (rsi105);
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&rbx10);
                                *reinterpret_cast<int32_t*>(&r9_3 + 4) = 0;
                                r10d36 = *reinterpret_cast<uint32_t*>(&r14_13);
                                rbx10 = r12_5;
                                r14_13 = v25;
                                r12_5 = v23;
                                eax107 = v108;
                                if (v28 == 1) {
                                    rsi86 = reinterpret_cast<struct s7*>(0x1b220);
                                    rdi109 = reinterpret_cast<void**>("A");
                                    do {
                                        if (*reinterpret_cast<void***>(rdi109) == *reinterpret_cast<void***>(&eax107)) 
                                            break;
                                        rdi109 = rsi86->f10;
                                        rsi86 = reinterpret_cast<struct s7*>(&rsi86->f10);
                                    } while (rdi109);
                                    goto addr_9902_141;
                                } else {
                                    addr_9902_141:
                                    rdi110 = r11_9;
                                    rsi111 = r11_9;
                                    if (*reinterpret_cast<void***>(&eax107)) {
                                        do {
                                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax107) == 46)) {
                                                ++rsi111;
                                            } else {
                                                *reinterpret_cast<uint32_t*>(&rdx16) = 1;
                                            }
                                            eax107 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi110 + 1));
                                            ++rdi110;
                                            *reinterpret_cast<void***>(rsi111) = *reinterpret_cast<void***>(&eax107);
                                        } while (*reinterpret_cast<void***>(&eax107));
                                        if (!*reinterpret_cast<unsigned char*>(&rdx16)) 
                                            goto addr_992d_147;
                                        *reinterpret_cast<uint32_t*>(&v25) = *reinterpret_cast<uint32_t*>(&rcx4);
                                        v23 = r11_9;
                                        rax112 = lookup_zone(r12_5, r11_9);
                                        rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8);
                                        r11_9 = v23;
                                        *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&v25);
                                        *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                        *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                        *reinterpret_cast<int32_t*>(&r9_3 + 4) = 0;
                                        if (!rax112) 
                                            goto addr_992d_147; else 
                                            goto addr_99c1_149;
                                    } else {
                                        goto addr_992d_147;
                                    }
                                }
                                goto addr_94fc_127;
                                addr_992d_147:
                                if (!*reinterpret_cast<signed char*>(r12_5 + 0xe1)) 
                                    goto addr_9509_83;
                                *reinterpret_cast<uint32_t*>(&v25) = *reinterpret_cast<uint32_t*>(&rcx4);
                                r15d2 = 63;
                                v23 = r11_9;
                                rax113 = fun_3580();
                                dbg_printf(rax113, v23, 5, rcx4, r8_39, r9_3, v114);
                                rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) - 8 + 8 - 8 + 8);
                                *reinterpret_cast<uint32_t*>(&rcx4) = *reinterpret_cast<uint32_t*>(&v25);
                                *reinterpret_cast<int32_t*>(&rcx4 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&r9_3) = *reinterpret_cast<uint32_t*>(&r9_3);
                                *reinterpret_cast<uint32_t*>(&rdx47) = 2;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                goto addr_846e_77;
                                addr_99c1_149:
                                r15d2 = rax112->f8;
                                r10d36 = r10d36;
                                goto addr_864b_112;
                                addr_98b0_33:
                                *reinterpret_cast<uint32_t*>(&rdx47) = 0;
                                *reinterpret_cast<void***>(r12_5) = rsi37 + 1;
                                r15d2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi37));
                                if (r15d2) {
                                    addr_8460_58:
                                    *reinterpret_cast<uint32_t*>(&rdx47) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x16ec0 + r15d2)));
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx47) + 4) = 0;
                                    goto addr_846e_77;
                                } else {
                                    goto addr_7da3_50;
                                }
                                addr_7d9a_59:
                                r10d36 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rcx4 + 2));
                                *reinterpret_cast<uint32_t*>(&rdx47) = 2;
                                goto addr_7da3_50;
                            }
                            eax18 = reinterpret_cast<int32_t>(-eax35);
                        }
                    } else {
                        if (v11 == 3) {
                            zf115 = reinterpret_cast<uint1_t>(r15d2 == 0);
                            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(r15d2) < reinterpret_cast<int32_t>(0)) | zf115)) {
                                *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x16e40 + rdx17)));
                                r15d2 = 0xfffffffe;
                                goto addr_7ea3_53;
                            } else {
                                if (zf115) 
                                    goto addr_8260_9;
                            }
                        }
                        *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(0x16e40 + rdx17)));
                        goto addr_7ea3_53;
                    }
                }
            }
            goto addr_827c_3;
            addr_94ae_17:
            *reinterpret_cast<int32_t*>(&rax14) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        }
    }
}

void fun_bab3() {
    __asm__("cli ");
    goto parse_datetime_body;
}

int64_t fun_bac3(void** rdi, void** rsi, void** rdx) {
    uint32_t r12d4;
    void** rax5;
    void** rax6;
    uint32_t eax7;
    int64_t rax8;

    __asm__("cli ");
    r12d4 = 0;
    rax5 = fun_3490("TZ");
    rax6 = tzalloc(rax5, rsi);
    if (rax6) {
        eax7 = parse_datetime_body(rdi, rsi, rdx, 0, rax6, rax5);
        r12d4 = eax7;
        tzfree(rax6, rsi, rax6, rsi);
    }
    *reinterpret_cast<uint32_t*>(&rax8) = r12d4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
}

struct s29 {
    signed char[1] pad1;
    signed char f1;
    signed char f2;
};

struct s29* fun_35e0(void** rdi, int64_t rsi);

struct s30 {
    int32_t f0;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
};

int64_t rpl_mktime(void** rdi, void** rsi, int64_t rdx, int64_t rcx);

int64_t fun_bbe3(int64_t* rdi, void** rsi, uint32_t edx) {
    uint32_t r12d4;
    void** rbp5;
    int64_t* v6;
    struct s0* rax7;
    struct s0* v8;
    void** rax9;
    void* rsp10;
    void** rbx11;
    void** r14_12;
    struct s29* r13_13;
    struct s29* rax14;
    void** rax15;
    void** rdx16;
    int64_t r14_17;
    int64_t rax18;
    void** rbx19;
    int64_t rdx20;
    int64_t rcx21;
    int64_t rdx22;
    int64_t rdx23;
    int64_t rcx24;
    void** rsi25;
    signed char al26;
    struct s30* rax27;
    uint32_t v28;
    uint32_t v29;
    uint32_t eax30;
    uint32_t v31;
    uint32_t v32;
    int32_t v33;
    uint32_t v34;
    uint32_t v35;
    uint32_t v36;
    signed char al37;
    uint32_t r15d38;
    int64_t rax39;
    int64_t rax40;
    int64_t v41;
    uint32_t r14d42;
    uint32_t r13d43;
    uint32_t r12d44;
    void** v45;
    uint32_t ebp46;
    uint32_t ebx47;
    uint32_t v48;
    int64_t rax49;
    int64_t rax50;
    int64_t rax51;
    int64_t rax52;
    void* rdx53;

    __asm__("cli ");
    r12d4 = edx;
    rbp5 = rsi;
    v6 = rdi;
    rax7 = g28;
    v8 = rax7;
    rax9 = fun_35a0(rsi);
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8 - 8 + 8);
    rbx11 = rax9;
    r14_12 = rax9;
    if (!(*reinterpret_cast<unsigned char*>(&r12d4) & 4)) {
        *reinterpret_cast<int32_t*>(&r13_13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_13) + 4) = 0;
    } else {
        rax14 = fun_35e0(rbp5, 46);
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        r13_13 = rax14;
        if (rax14) {
            rax15 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax14) - reinterpret_cast<unsigned char>(rbp5));
            if (reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rax15) == 3) {
                rbx11 = rax15;
                r14_12 = rax15;
            } else {
                goto addr_bc50_7;
            }
        }
    }
    if (reinterpret_cast<uint64_t>(rbx11 + 0xfffffffffffffff8) > 4) 
        goto addr_bc50_7;
    if (*reinterpret_cast<uint32_t*>(&rbx11) & 1) 
        goto addr_bc50_7;
    *reinterpret_cast<int32_t*>(&rdx16) = 0;
    *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
    do {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(rdx16)) - 48 > 9) 
            goto addr_bc50_7;
        ++rdx16;
    } while (reinterpret_cast<signed char>(r14_12) > reinterpret_cast<signed char>(rdx16));
    r14_17 = reinterpret_cast<signed char>(r14_12) >> 1;
    *reinterpret_cast<int32_t*>(&rax18) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
    rbx19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 0xa0);
    do {
        *reinterpret_cast<int32_t*>(&rdx20) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp5 + rax18 * 2));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rcx21) = static_cast<int32_t>(rdx20 + rdx20 * 4 - 0xf0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx21) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx22) = *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbp5 + rax18 * 2) + 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
        *reinterpret_cast<void***>(rbx19 + rax18 * 4) = reinterpret_cast<void**>(static_cast<uint32_t>(rdx22 + rcx21 * 2 - 48));
        ++rax18;
    } while (r14_17 != rax18);
    rdx23 = r14_17 - 4;
    if (!(*reinterpret_cast<unsigned char*>(&r12d4) & 1)) {
        *reinterpret_cast<uint32_t*>(&rcx24) = r12d4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
        rsi25 = rbx19;
        al26 = year(reinterpret_cast<int64_t>(rsp10) + 32, rsi25, rdx23, *reinterpret_cast<uint32_t*>(&rcx24));
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        if (!al26) 
            goto addr_bc50_7;
        rax27 = reinterpret_cast<struct s30*>(reinterpret_cast<uint64_t>(rbx19 + r14_17 * 4) - 16);
        v28 = reinterpret_cast<uint32_t>(rax27->f0 - 1);
        v29 = rax27->f4;
        *reinterpret_cast<uint32_t*>(&rdx23) = rax27->f8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0;
        eax30 = rax27->fc;
        v31 = *reinterpret_cast<uint32_t*>(&rdx23);
        v32 = eax30;
    } else {
        rsi25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 0xb0);
        *reinterpret_cast<uint32_t*>(&rcx24) = r12d4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
        v28 = reinterpret_cast<uint32_t>(v33 - 1);
        v29 = v34;
        v31 = v35;
        v32 = v36;
        al37 = year(reinterpret_cast<int64_t>(rsp10) + 32, rsi25, rdx23, *reinterpret_cast<uint32_t*>(&rcx24));
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        if (!al37) 
            goto addr_bc50_7;
    }
    if (!r13_13) {
        r15d38 = 0;
    } else {
        *reinterpret_cast<uint32_t*>(&rax39) = r13_13->f1 - 48;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax39) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rax39) > 9) 
            goto addr_bc50_7;
        *reinterpret_cast<uint32_t*>(&rdx23) = reinterpret_cast<uint32_t>(static_cast<int32_t>(r13_13->f2));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx24) = static_cast<uint32_t>(rdx23 - 48);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rcx24) > 9) 
            goto addr_bc50_7;
        *reinterpret_cast<int32_t*>(&rax40) = static_cast<int32_t>(rax39 + rax39 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax40) + 4) = 0;
        r15d38 = static_cast<uint32_t>(rdx23 + rax40 * 2 - 48);
    }
    *reinterpret_cast<signed char*>(&v41) = 0;
    r14d42 = v32;
    r13d43 = v31;
    r12d44 = v29;
    v45 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp10) + 96);
    ebp46 = v28;
    ebx47 = v48;
    while (rax49 = rpl_mktime(v45, rsi25, rdx23, rcx24), rcx24 = rax49, 0) {
        *reinterpret_cast<uint32_t*>(&rsi25) = r15d38 ^ r15d38;
        *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
        if (!(ebx47 ^ ebx47 | ebp46 ^ ebp46 | r12d44 ^ r12d44 | r13d43 ^ r13d43 | r14d42 ^ r14d42 | *reinterpret_cast<uint32_t*>(&rsi25))) 
            goto addr_be50_27;
        if (r15d38 != 60) 
            goto addr_bc50_7;
        *reinterpret_cast<signed char*>(&v41) = 1;
        r15d38 = 59;
    }
    goto addr_bc50_7;
    addr_be50_27:
    rax50 = v41;
    *reinterpret_cast<uint32_t*>(&rax51) = *reinterpret_cast<uint32_t*>(&rax50) & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
    if (__intrinsic()) {
        addr_bc50_7:
        *reinterpret_cast<int32_t*>(&rax52) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax52) + 4) = 0;
    } else {
        *v6 = rax51 + rcx24;
        *reinterpret_cast<int32_t*>(&rax52) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax52) + 4) = 0;
    }
    rdx53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v8) - reinterpret_cast<int64_t>(g28));
    if (rdx53) {
        fun_35b0();
    } else {
        return rax52;
    }
}

struct s31 {
    signed char[1] pad1;
    signed char f1;
    signed char[2] pad4;
    signed char f4;
};

struct s31* fun_3610();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_bec3(struct s0* rdi) {
    struct s0* rbx2;
    struct s31* rax3;
    struct s0* r12_4;
    int32_t eax5;

    __asm__("cli ");
    if (!rdi) {
        fun_3830("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
        fun_34d0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx2 = rdi;
        rax3 = fun_3610();
        if (rax3 && ((r12_4 = reinterpret_cast<struct s0*>(&rax3->f1), reinterpret_cast<int64_t>(r12_4) - reinterpret_cast<int64_t>(rbx2) > 6) && (eax5 = fun_34f0(reinterpret_cast<int64_t>(rax3) + 0xfffffffffffffffa, "/.libs/", 7), !eax5))) {
            if (rax3->f1 != 0x6c || (r12_4->f1 != 0x74 || r12_4->f2 != 45)) {
                rbx2 = r12_4;
            } else {
                rbx2 = reinterpret_cast<struct s0*>(&rax3->f4);
                __progname = rbx2;
            }
        }
        program_name = rbx2;
        __progname_full = rbx2;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_d663(int64_t rdi) {
    int64_t rbp2;
    void*** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_34e0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0x1c220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_d6a3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x1c220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_d6c3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x1c220);
    }
    *rdi = esi;
    return 0x1c220;
}

int64_t fun_d6e3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x1c220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s32 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_d723(struct s32* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s32*>(0x1c220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s33 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s33* fun_d743(struct s33* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s33*>(0x1c220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x38aa;
    if (!rdx) 
        goto 0x38aa;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x1c220;
}

struct s34 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_d783(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s34* r8) {
    struct s34* rbx6;
    void*** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s34*>(0x1c220);
    }
    rax7 = fun_34e0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0xd7b6);
    *rax7 = r15d8;
    return rax13;
}

struct s35 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_d803(int64_t rdi, int64_t rsi, void*** rdx, struct s35* rcx) {
    struct s35* rbx5;
    void*** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s35*>(0x1c220);
    }
    rax6 = fun_34e0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0xd831);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0xd88c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_d8f3() {
    __asm__("cli ");
}

void** g1c098 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_d903() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_34b0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x1c120) {
        fun_34b0(rdi7);
        g1c098 = reinterpret_cast<void**>(0x1c120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x1c090) {
        fun_34b0(r12_2);
        slotvec = reinterpret_cast<void**>(0x1c090);
    }
    nslots = 1;
    return;
}

void fun_d9a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_d9c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_d9d3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_d9f3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_da13(void** rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s13* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x38b0;
    rcx5 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx4) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_35b0();
    } else {
        return rax6;
    }
}

void** fun_daa3(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s13* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x38b5;
    rcx6 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx5) - reinterpret_cast<int64_t>(g28));
    if (rdx8) {
        fun_35b0();
    } else {
        return rax7;
    }
}

void** fun_db33(int32_t edi, int64_t rsi) {
    struct s0* rax3;
    struct s13* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x38ba;
    rcx4 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax3) - reinterpret_cast<int64_t>(g28));
    if (rdx6) {
        fun_35b0();
    } else {
        return rax5;
    }
}

void** fun_dbc3(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s13* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x38bf;
    rcx5 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_35b0();
    } else {
        return rax6;
    }
}

void** fun_dc53(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s13* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xe5c0]");
    __asm__("movdqa xmm1, [rip+0xe5c8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xe5b1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<int64_t>(g28));
    if (rdx11) {
        fun_35b0();
    } else {
        return rax10;
    }
}

void** fun_dcf3(int64_t rdi, uint32_t esi) {
    struct s13* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xe520]");
    __asm__("movdqa xmm1, [rip+0xe528]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xe511]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<int64_t>(g28));
    if (rdx10) {
        fun_35b0();
    } else {
        return rax9;
    }
}

void** fun_dd93(int64_t rdi) {
    struct s0* rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xe480]");
    __asm__("movdqa xmm1, [rip+0xe488]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xe469]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax2) - reinterpret_cast<int64_t>(g28));
    if (rdx4) {
        fun_35b0();
    } else {
        return rax3;
    }
}

void** fun_de23(int64_t rdi, int64_t rsi) {
    struct s0* rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xe3f0]");
    __asm__("movdqa xmm1, [rip+0xe3f8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xe3e6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax3) - reinterpret_cast<int64_t>(g28));
    if (rdx5) {
        fun_35b0();
    } else {
        return rax4;
    }
}

void** fun_deb3(void** rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s13* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x38c4;
    rcx5 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx4) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_35b0();
    } else {
        return rax6;
    }
}

void** fun_df53(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s13* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xe2ba]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xe2b2]");
    __asm__("movdqa xmm2, [rip+0xe2ba]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x38c9;
    if (!rdx) 
        goto 0x38c9;
    rcx6 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx5) - reinterpret_cast<int64_t>(g28));
    if (rdx8) {
        fun_35b0();
    } else {
        return rax7;
    }
}

void** fun_dff3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s0* rcx6;
    struct s13* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xe21a]");
    __asm__("movdqa xmm1, [rip+0xe222]");
    __asm__("movdqa xmm2, [rip+0xe22a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x38ce;
    if (!rdx) 
        goto 0x38ce;
    rcx7 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx6) - reinterpret_cast<int64_t>(g28));
    if (rdx10) {
        fun_35b0();
    } else {
        return rax9;
    }
}

void** fun_e0a3(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s0* rdx4;
    struct s13* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xe16a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xe162]");
    __asm__("movdqa xmm2, [rip+0xe16a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x38d3;
    if (!rsi) 
        goto 0x38d3;
    rcx5 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx4) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_35b0();
    } else {
        return rax6;
    }
}

void** fun_e143(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s13* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xe0ca]");
    __asm__("movdqa xmm1, [rip+0xe0d2]");
    __asm__("movdqa xmm2, [rip+0xe0da]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x38d8;
    if (!rsi) 
        goto 0x38d8;
    rcx6 = reinterpret_cast<struct s13*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rcx5) - reinterpret_cast<int64_t>(g28));
    if (rdx8) {
        fun_35b0();
    } else {
        return rax7;
    }
}

void fun_e1e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_e1f3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_e213() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_e233(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int32_t fun_3770();

int32_t fun_3720(void* rdi);

int64_t fun_e253(int64_t rdi) {
    struct s0* rax2;
    int32_t eax3;
    int32_t r12d4;
    void*** rax5;
    int32_t eax6;
    void* rax7;
    int64_t rax8;

    __asm__("cli ");
    rax2 = g28;
    eax3 = fun_3770();
    r12d4 = eax3;
    if (eax3 && (rax5 = fun_34e0(), *rax5 != 1)) {
        eax6 = fun_3720(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 40 - 8 + 8 - 8 + 8);
        r12d4 = eax6;
    }
    rax7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax2) - reinterpret_cast<int64_t>(g28));
    if (rax7) {
        fun_35b0();
    } else {
        *reinterpret_cast<int32_t*>(&rax8) = r12d4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    }
}

void** fun_e383(void** rdi, void** rsi) {
    void** rax3;
    void** r12_4;
    void** rax5;
    void** rbx6;
    void** rax7;
    void** rax8;

    __asm__("cli ");
    if (!rdi) {
        rax3 = fun_3730(0x80, rsi);
        r12_4 = rax3;
        if (rax3) {
            *reinterpret_cast<void***>(r12_4) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(0);
            return r12_4;
        }
    } else {
        rax5 = fun_35a0(rdi);
        rbx6 = rax5 + 1;
        *reinterpret_cast<int32_t*>(&rax7) = 0x76;
        *reinterpret_cast<int32_t*>(&rax7 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rbx6) >= reinterpret_cast<unsigned char>(0x76)) {
            rax7 = rbx6;
        }
        rax8 = fun_3730(reinterpret_cast<uint64_t>(rax7 + 17) & 0xfffffffffffffff8, rsi);
        r12_4 = rax8;
        if (rax8) {
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_4 + 8) = reinterpret_cast<void**>(1);
            fun_36e0(r12_4 + 9, rdi, rbx6);
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_4) + reinterpret_cast<unsigned char>(rbx6) + 9) = 0;
        }
    }
    return r12_4;
}

void fun_e653(void** rdi) {
    void** rbx2;
    void** rdi3;

    __asm__("cli ");
    if (rdi == 1) {
        return;
    } else {
        rbx2 = rdi;
        if (rdi) {
            do {
                rdi3 = rbx2;
                rbx2 = *reinterpret_cast<void***>(rbx2);
                fun_34b0(rdi3);
            } while (rbx2);
        }
        return;
    }
}

int64_t fun_3450(int64_t rdi, void** rsi);

void** fun_e693(void** rdi, int64_t rsi, void** rdx) {
    void** rax4;
    void** rsi5;
    int64_t rax6;
    signed char al7;
    signed char al8;

    __asm__("cli ");
    if (!rdi) {
        goto fun_3460;
    } else {
        rax4 = set_tz(rdi);
        if (rax4) {
            rsi5 = rdx;
            rax6 = fun_3450(rsi, rsi5);
            if (!rax6 || (rsi5 = rdx, al7 = save_abbr(rdi, rsi5), al7 == 0)) {
                if (rax4 != 1) {
                    revert_tz_part_0(rax4, rsi5);
                }
            } else {
                if (reinterpret_cast<int1_t>(rax4 == 1) || (al8 = revert_tz_part_0(rax4, rsi5), !!al8)) {
                    return rdx;
                }
            }
        }
        return 0;
    }
}

int64_t fun_e743(void** rdi, void** rsi, int64_t rdx, int64_t rcx) {
    void** rbp5;
    struct s0* rax6;
    void* rax7;
    void** rax8;
    int64_t r13_9;
    void** r15_10;
    int64_t rax11;
    signed char al12;
    signed char al13;
    void** v14;
    void* rax15;

    __asm__("cli ");
    rbp5 = rsi;
    rax6 = g28;
    if (!rdi) {
        rax7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax6) - reinterpret_cast<int64_t>(g28));
        if (rax7) {
            fun_35b0();
        }
    }
    rax8 = set_tz(rdi);
    if (!rax8) {
        addr_e7d0_7:
        r13_9 = -1;
    } else {
        r15_10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 64) - 8 + 8);
        rax11 = rpl_mktime(r15_10, rsi, rdx, rcx);
        r13_9 = rax11;
        if (!0 || (rsi = r15_10, al12 = save_abbr(rdi, rsi), al12 == 0)) {
            if (rax8 != 1) {
                revert_tz_part_0(rax8, rsi);
                goto addr_e7d0_7;
            }
        } else {
            if (reinterpret_cast<int1_t>(rax8 == 1) || (al13 = revert_tz_part_0(rax8, rsi), !!al13)) {
                __asm__("movdqa xmm0, [rsp]");
                __asm__("movdqa xmm1, [rsp+0x10]");
                __asm__("movdqa xmm2, [rsp+0x20]");
                __asm__("movups [rbp+0x0], xmm0");
                *reinterpret_cast<void***>(rbp5 + 48) = v14;
                __asm__("movups [rbp+0x10], xmm1");
                __asm__("movups [rbp+0x20], xmm2");
            } else {
                goto addr_e7d0_7;
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax6) - reinterpret_cast<int64_t>(g28));
    if (!rax15) {
        return r13_9;
    }
}

struct s36 {
    signed char[32] pad32;
    int32_t f20;
};

void fun_e893(struct s36* rdi) {
    __asm__("cli ");
    rdi->f20 = 0;
    goto 0xf860;
}

void fun_36c0(int64_t rdi, void** rsi, void** rdx, void** rcx, int64_t r8, uint64_t r9);

void fun_e8b3(void** rdi, int64_t rsi, int64_t rdx, uint64_t rcx, int64_t r8, uint64_t r9) {
    uint64_t r12_7;
    void** rax8;
    void** rax9;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_3840(rdi, rdi);
    } else {
        r9 = rcx;
        fun_3840(rdi, rdi);
    }
    rax8 = fun_3580();
    fun_3840(rdi, rdi);
    fun_36c0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_3580();
    fun_3840(rdi, rdi);
    fun_36c0(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (r12_7 > 9) {
        fun_3580();
        fun_3840(rdi, rdi);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x176e8 + r12_7 * 4) + 0x176e8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_ed23() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s37 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_ed43(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s37* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s37* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v10) - reinterpret_cast<int64_t>(g28));
    if (rax18) {
        fun_35b0();
    } else {
        return;
    }
}

void fun_ede3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_ee86_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_ee90_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v16) - reinterpret_cast<int64_t>(g28));
    if (rax20) {
        fun_35b0();
    } else {
        return;
    }
    addr_ee86_5:
    goto addr_ee90_7;
}

void fun_eec3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    int64_t r8_4;
    uint64_t r9_5;
    void** rax6;
    struct s0* rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_36c0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_3580();
    fun_37b0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_3580();
    fun_37b0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_3580();
    goto fun_37b0;
}

int64_t fun_3520();

void fun_ef63(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3520();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_efa3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3730(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_efc3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3730(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_efe3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3730(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_3790(void** rdi, void** rsi, ...);

void fun_f003(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3790(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_f033(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_3790(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_f063(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_3520();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_f0a3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_3520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_f0e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_3520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_f113(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_3520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_f163(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_3520();
            if (rax5) 
                break;
            addr_f1ad_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_f1ad_5;
        rax8 = fun_3520();
        if (rax8) 
            goto addr_f196_9;
        if (rbx4) 
            goto addr_f1ad_5;
        addr_f196_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_f1f3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_3520();
            if (rax8) 
                break;
            addr_f23a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_f23a_5;
        rax11 = fun_3520();
        if (rax11) 
            goto addr_f222_9;
        if (!rbx6) 
            goto addr_f222_9;
        if (r12_4) 
            goto addr_f23a_5;
        addr_f222_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_f283(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_f32d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_f340_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_f2e0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_f306_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_f354_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_f2fd_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_f354_14;
            addr_f2fd_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_f354_14;
            addr_f306_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_3790(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_f354_14;
            if (!rbp13) 
                break;
            addr_f354_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_f32d_9;
        } else {
            if (!r13_6) 
                goto addr_f340_10;
            goto addr_f2e0_11;
        }
    }
}

int64_t fun_3680();

void fun_f383() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_3680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_f3b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_3680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_f3e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_3680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_f403() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_3680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_f423(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3730(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_36e0;
    }
}

void fun_f463(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3730(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_36e0;
    }
}

void fun_f4a3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_3730(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_36e0;
    }
}

void fun_f4e3(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_35a0(rdi);
    rax4 = fun_3730(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_36e0;
    }
}

void fun_f523() {
    void** rdi1;

    __asm__("cli ");
    fun_3580();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_37d0();
    fun_34d0(rdi1);
}

struct s38 {
    int32_t f0;
    uint32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int32_t f14;
    signed char[8] pad32;
    uint32_t f20;
    signed char[12] pad48;
    int64_t f30;
};

uint64_t fun_f863(struct s38* rdi, int64_t rsi, uint64_t* rdx) {
    int64_t rcx4;
    struct s38* v5;
    int64_t v6;
    int64_t rsi7;
    uint64_t* v8;
    struct s0* rax9;
    struct s0* v10;
    int32_t v11;
    uint32_t v12;
    int64_t rdi13;
    int32_t v14;
    int64_t rcx15;
    uint32_t v16;
    int64_t rcx17;
    uint32_t edi18;
    int64_t r15_19;
    int64_t rdx20;
    int64_t rdx21;
    int64_t rdx22;
    uint64_t r12_23;
    int64_t r9_24;
    int64_t rsi25;
    uint64_t rax26;
    int64_t v27;
    int64_t r8_28;
    uint64_t v29;
    int64_t rax30;
    int32_t v31;
    int64_t v32;
    int64_t rcx33;
    uint64_t rax34;
    void* rsp35;
    uint64_t v36;
    uint64_t rbp37;
    struct s15* r13_38;
    uint64_t* r14_39;
    uint64_t v40;
    uint64_t v41;
    int32_t v42;
    uint32_t v43;
    struct s15* rax44;
    int64_t rbx45;
    int32_t v46;
    int64_t v47;
    int64_t rax48;
    int32_t v49;
    int64_t v50;
    int64_t rax51;
    int32_t v52;
    int64_t v53;
    int64_t rax54;
    int32_t v55;
    int64_t v56;
    int32_t v57;
    uint64_t rax58;
    uint64_t r10_59;
    int32_t v60;
    unsigned char dl61;
    uint32_t eax62;
    int32_t v63;
    unsigned char v64;
    uint32_t edi65;
    unsigned char v66;
    int32_t v67;
    int64_t rcx68;
    int32_t v69;
    uint64_t rbp70;
    int64_t r12_71;
    struct s15* v72;
    int64_t r14_73;
    uint64_t* v74;
    void* v75;
    int64_t v76;
    struct s15* v77;
    uint64_t* rax78;
    uint64_t rdx79;
    int64_t rax80;
    int64_t v81;
    void* rax82;
    uint64_t rax83;
    uint64_t v84;
    int32_t v85;
    struct s15* rax86;
    int32_t v87;
    int64_t rax88;
    int32_t v89;
    int64_t v90;
    int64_t rax91;
    int32_t v92;
    int64_t v93;
    int64_t rax94;
    int32_t v95;
    int64_t v96;
    int64_t rax97;
    int32_t v98;
    int64_t v99;
    int32_t v100;
    int64_t rdx101;
    int64_t rax102;
    void*** rax103;
    int32_t ebx104;
    int32_t r15d105;
    int32_t r13d106;
    int64_t rax107;
    void*** rax108;
    int32_t v109;

    __asm__("cli ");
    rcx4 = rdi->f10;
    v5 = rdi;
    v6 = rsi;
    rsi7 = rdi->fc;
    v8 = rdx;
    rax9 = g28;
    v10 = rax9;
    v11 = rdi->f0;
    v12 = rdi->f4;
    rdi13 = rcx4;
    v14 = rdi->f8;
    rcx15 = rcx4 * 0x2aaaaaab >> 33;
    v16 = rdi->f20;
    *reinterpret_cast<int32_t*>(&rcx17) = *reinterpret_cast<int32_t*>(&rcx15) - (*reinterpret_cast<int32_t*>(&rdi13) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx17) + 4) = 0;
    edi18 = *reinterpret_cast<int32_t*>(&rdi13) - (static_cast<uint32_t>(rcx17 + rcx17 * 2) << 2);
    r15_19 = reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rcx17) - (edi18 >> 31)) + static_cast<int64_t>(rdi->f14);
    *reinterpret_cast<uint32_t*>(&rdx20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&r15_19) & 3) && (*reinterpret_cast<uint32_t*>(&rdx20) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0, reinterpret_cast<uint64_t>(0x8f5c28f5c28f5c29 * r15_19 + 0x51eb851eb851eb8) <= 0x28f5c28f5c28f5c)) {
        rdx21 = (__intrinsic() + r15_19 >> 6) - (r15_19 >> 63);
        *reinterpret_cast<uint32_t*>(&rdx22) = *reinterpret_cast<uint32_t*>(&rdx21) & 3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx20) = reinterpret_cast<uint1_t>(rdx22 == 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
    }
    *reinterpret_cast<int32_t*>(&r12_23) = 59;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_23) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r9_24) = 70;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
    rsi25 = rsi7 + reinterpret_cast<int32_t>(*reinterpret_cast<uint16_t*>(0x17760 + ((reinterpret_cast<uint32_t>(reinterpret_cast<int32_t>(edi18) >> 31) & 12) + edi18 + (rdx20 + (rdx20 + rdx20 * 2) * 4)) * 2) - 1);
    rax26 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v11));
    v27 = rsi25;
    if (*reinterpret_cast<int32_t*>(&rax26) <= 59) {
        r12_23 = rax26;
    }
    if (*reinterpret_cast<int32_t*>(&r12_23) < 0) {
        r12_23 = 0;
    }
    *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
    v29 = *v8;
    *reinterpret_cast<int32_t*>(&rax30) = -*reinterpret_cast<int32_t*>(&v29);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
    v31 = *reinterpret_cast<int32_t*>(&rax30);
    v32 = rax30;
    *reinterpret_cast<uint32_t*>(&rcx33) = v12;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
    rax34 = ydhms_diff(r15_19, rsi25, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), 70, 0, 0, 0, *reinterpret_cast<int32_t*>(&v32));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x128 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
    v36 = rax34;
    rbp37 = rax34;
    r13_38 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(rsp35) + 0xa0);
    r14_39 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x88);
    v40 = rax34;
    v41 = rax34;
    v42 = 6;
    v43 = 0;
    while (rax44 = ranged_convert(v6, r14_39, r13_38, rcx33, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax44) {
        *reinterpret_cast<int32_t*>(&rbx45) = v46;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx45) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_28) = *reinterpret_cast<int32_t*>(&r12_23);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
        v47 = rbx45;
        *reinterpret_cast<int32_t*>(&rax48) = v49;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
        v50 = rax48;
        *reinterpret_cast<int32_t*>(&rax51) = v52;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax51) + 4) = 0;
        v53 = rax51;
        *reinterpret_cast<int32_t*>(&rax54) = v55;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax54) + 4) = 0;
        v56 = rax54;
        *reinterpret_cast<int32_t*>(&r9_24) = v57;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rcx33) = v12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
        rax58 = ydhms_diff(r15_19, v27, v14, *reinterpret_cast<uint32_t*>(&rcx33), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v56), *reinterpret_cast<int32_t*>(&v53), *reinterpret_cast<int32_t*>(&v50), *reinterpret_cast<int32_t*>(&v47));
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        r10_59 = v40;
        if (!rax58) 
            goto addr_fc00_10;
        if (rbp37 == r10_59 || v41 != r10_59) {
            addr_fa60_12:
            --v42;
            if (!v42) 
                goto addr_fb70_13;
        } else {
            if (v60 < 0) 
                goto addr_fac0_15;
            *reinterpret_cast<uint32_t*>(&rcx33) = v16;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
            dl61 = reinterpret_cast<uint1_t>(!!v60);
            if (*reinterpret_cast<int32_t*>(&rcx33) < reinterpret_cast<int32_t>(0)) 
                goto addr_fbe8_17; else 
                goto addr_fab2_18;
        }
        v41 = rbp37;
        rbp37 = r10_59;
        v40 = rax58 + r10_59;
        eax62 = 0;
        *reinterpret_cast<unsigned char*>(&eax62) = reinterpret_cast<uint1_t>(!!v63);
        v43 = eax62;
        continue;
        addr_fbe8_17:
        if (reinterpret_cast<int32_t>(static_cast<uint32_t>(dl61)) < reinterpret_cast<int32_t>(v43)) 
            goto addr_fa60_12; else 
            goto addr_fbf5_20;
        addr_fab2_18:
        *reinterpret_cast<unsigned char*>(&rcx33) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<uint32_t*>(&rcx33));
        if (*reinterpret_cast<unsigned char*>(&rcx33) == dl61) 
            goto addr_fa60_12; else 
            goto addr_fab9_21;
    }
    goto addr_fb7b_22;
    addr_fc00_10:
    v64 = reinterpret_cast<uint1_t>(v16 == 0);
    edi65 = v64;
    v66 = reinterpret_cast<uint1_t>(v67 == 0);
    *reinterpret_cast<uint32_t*>(&rcx68) = v66;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx68) != *reinterpret_cast<signed char*>(&edi65) && !__intrinsic()) {
        v69 = *reinterpret_cast<int32_t*>(&r12_23);
        rbp70 = r10_59;
        r12_71 = v6;
        v72 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(rsp35) + 0xe0);
        *reinterpret_cast<int32_t*>(&r14_73) = 0x92c70;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
        v74 = reinterpret_cast<uint64_t*>(reinterpret_cast<int64_t>(rsp35) + 0x90);
        v75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) + 0x98);
        v76 = r15_19;
        v77 = r13_38;
        goto addr_fc94_24;
    }
    addr_fac0_15:
    rax78 = v8;
    *rax78 = r10_59 - (v31 + v36);
    if (v11 == *reinterpret_cast<int32_t*>(&rbx45)) 
        goto addr_fb2f_25;
    *reinterpret_cast<unsigned char*>(&rax78) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rbx45) == 60);
    *reinterpret_cast<int32_t*>(&rdx79) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx79) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rdx79) = reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(v11 < 0) | reinterpret_cast<uint1_t>(v11 == 0));
    if (!__intrinsic()) {
        rax80 = reinterpret_cast<int64_t>(v6(reinterpret_cast<int64_t>(rsp35) + 0xe0, r13_38));
        r10_59 = v11 + ((rdx79 & reinterpret_cast<uint64_t>(rax78)) - r12_23) + r10_59;
        if (!rax80) {
            addr_fb7b_22:
            r10_59 = 0xffffffffffffffff;
        } else {
            addr_fb2f_25:
            __asm__("movdqa xmm0, [rsp+0xa0]");
            __asm__("movdqa xmm1, [rsp+0xb0]");
            __asm__("movdqa xmm2, [rsp+0xc0]");
            __asm__("movups [rcx], xmm0");
            v5->f30 = v81;
            __asm__("movups [rcx+0x10], xmm1");
            __asm__("movups [rcx+0x20], xmm2");
        }
        rax82 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v10) - reinterpret_cast<int64_t>(g28));
        if (rax82) {
            fun_35b0();
        } else {
            return r10_59;
        }
    }
    addr_fbf5_20:
    goto addr_fac0_15;
    addr_fab9_21:
    goto addr_fac0_15;
    addr_fdf2_31:
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    r13_38 = v77;
    r10_59 = rax83 + v84;
    *reinterpret_cast<int32_t*>(&rbx45) = v85;
    goto addr_fac0_15;
    addr_fd8b_32:
    goto addr_fb7b_22;
    while (rax86 = ranged_convert(r12_71, v74, v72, rcx68, r8_28, r9_24), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8), !!rax86) {
        if (v64 == static_cast<unsigned char>(reinterpret_cast<uint1_t>(v87 == 0)) || v87 < 0) {
            *reinterpret_cast<int32_t*>(&rax88) = v89;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax88) + 4) = 0;
            v90 = rax88;
            *reinterpret_cast<int32_t*>(&rax91) = v92;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
            v93 = rax91;
            *reinterpret_cast<int32_t*>(&rax94) = v95;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax94) + 4) = 0;
            v96 = rax94;
            *reinterpret_cast<int32_t*>(&rax97) = v98;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
            v99 = rax97;
            *reinterpret_cast<int32_t*>(&r9_24) = v100;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_24) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r8_28) = v69;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_28) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx68) = v12;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx68) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx101) = v14;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx101) + 4) = 0;
            rax83 = ydhms_diff(v76, v27, *reinterpret_cast<int32_t*>(&rdx101), *reinterpret_cast<uint32_t*>(&rcx68), *reinterpret_cast<int32_t*>(&r8_28), *reinterpret_cast<int32_t*>(&r9_24), *reinterpret_cast<int32_t*>(&v99), *reinterpret_cast<int32_t*>(&v96), *reinterpret_cast<int32_t*>(&v93), *reinterpret_cast<int32_t*>(&v90));
            rax102 = reinterpret_cast<int64_t>(r12_71(v75, v77, rdx101, rcx68, r8_28, r9_24));
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
            if (rax102) 
                goto addr_fdf2_31;
            rax103 = fun_34e0();
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            if (*rax103 != 75) 
                goto addr_fd8b_32;
        }
        while (1) {
            do {
                ebx104 = ebx104 + r15d105;
                if (r13d106 == 1) 
                    break;
                r13d106 = 1;
                v84 = ebx104 + rbp70;
            } while (__intrinsic());
            break;
            *reinterpret_cast<int32_t*>(&r14_73) = *reinterpret_cast<int32_t*>(&r14_73) + 0x92c70;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_73) + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&r14_73) == 0xdb04f20) 
                goto addr_fd90_40;
            addr_fc94_24:
            r15d105 = static_cast<int32_t>(r14_73 + r14_73);
            r13d106 = 2;
            ebx104 = -*reinterpret_cast<int32_t*>(&r14_73);
            v84 = ebx104 + rbp70;
            if (!__intrinsic()) 
                break;
        }
    }
    goto addr_fb7b_22;
    addr_fd90_40:
    r13_38 = v77;
    r12_23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(v69));
    rax107 = reinterpret_cast<int64_t>(v6(v72, r13_38));
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    if (!rax107) {
        addr_fb70_13:
        rax108 = fun_34e0();
        *rax108 = reinterpret_cast<void**>(75);
        goto addr_fb7b_22;
    } else {
        *reinterpret_cast<int32_t*>(&rbx45) = v109;
        r10_59 = rbp70 + static_cast<int64_t>(reinterpret_cast<int32_t>((v64 - v66) * reinterpret_cast<uint32_t>("ko")));
        goto addr_fac0_15;
    }
}

void fun_fe13(int64_t rdi, void** rsi, int64_t rdx) {
    __asm__("cli ");
    fun_36f0(rdi, rsi, rdx);
    goto 0xf860;
}

void** vasnprintf(void** rdi, void* rsi, uint64_t rdx, int64_t rcx);

void fseterr(int64_t rdi, void* rsi, uint64_t rdx, int64_t rcx);

int64_t fun_fe43(int64_t rdi, uint64_t rsi, int64_t rdx) {
    int64_t rcx4;
    uint64_t rdx5;
    void* rsp6;
    struct s0* rax7;
    void** r13_8;
    void* rsi9;
    void** rax10;
    int64_t rax11;
    uint64_t rax12;
    void* rdx13;
    void*** rax14;

    __asm__("cli ");
    rcx4 = rdx;
    rdx5 = rsi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x808);
    rax7 = g28;
    r13_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
    rsi9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) + 24);
    rax10 = vasnprintf(r13_8, rsi9, rdx5, rcx4);
    if (!rax10) {
        addr_ff17_2:
        fseterr(rdi, rsi9, rdx5, rcx4);
        *reinterpret_cast<int32_t*>(&rax11) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    } else {
        rcx4 = rdi;
        rdx5 = 0x7d0;
        *reinterpret_cast<int32_t*>(&rsi9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        rax12 = fun_3830(rax10, rax10);
        if (rax12 < 0x7d0) {
            *reinterpret_cast<int32_t*>(&rax11) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            if (rax10 != r13_8) {
                fun_34b0(rax10, rax10);
                *reinterpret_cast<int32_t*>(&rax11) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            }
        } else {
            if (rax10 != r13_8) {
                fun_34b0(rax10, rax10);
            }
            if (0) 
                goto addr_ff0c_9; else 
                goto addr_feca_10;
        }
    }
    addr_fecc_11:
    rdx13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax7) - reinterpret_cast<int64_t>(g28));
    if (rdx13) {
        fun_35b0();
    } else {
        return rax11;
    }
    addr_ff0c_9:
    rax14 = fun_34e0();
    *rax14 = reinterpret_cast<void**>(75);
    goto addr_ff17_2;
    addr_feca_10:
    *reinterpret_cast<int32_t*>(&rax11) = 0x7d0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    goto addr_fecc_11;
}

int64_t fun_3510();

int64_t fun_ff33(void** rdi, void** rsi, void** rdx, void** rcx) {
    int64_t rax5;
    uint32_t ebx6;
    int64_t rax7;
    void*** rax8;
    void*** rax9;

    __asm__("cli ");
    rax5 = fun_3510();
    ebx6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax7 = rpl_fclose(rdi, rsi, rdx, rcx);
    if (ebx6) {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            addr_ff8e_3:
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rax8 = fun_34e0();
            *rax8 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            if (rax5) 
                goto addr_ff8e_3;
            rax9 = fun_34e0();
            *reinterpret_cast<int32_t*>(&rax7) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*rax9 == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    }
    return rax7;
}

void fun_ffa3(uint32_t* rdi) {
    __asm__("cli ");
    *rdi = *rdi | 32;
    return;
}

void** fun_ffb3() {
    void** rax1;

    __asm__("cli ");
    rax1 = fun_3750(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*reinterpret_cast<void***>(rax1)) {
            rax1 = reinterpret_cast<void**>("ASCII");
        }
        return rax1;
    }
}

uint64_t fun_35d0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_fff3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_35d0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<int64_t>(g28));
    if (rax9) {
        fun_35b0();
    } else {
        return r12_7;
    }
}

void** fun_11bf3(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9) {
    int64_t v7;
    int64_t v8;
    void** rax9;

    __asm__("cli ");
    rax9 = __strftime_internal_isra_0(rdi, rsi, rdx, rcx, 0, 0, 0xff, r8, r9, v7, v8);
    return rax9;
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s39 {
    signed char[7] pad7;
    void** f7;
};

struct s40 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s41 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_11c13(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void*** v11;
    struct s0* rax12;
    struct s0* v13;
    int32_t eax14;
    void* rsp15;
    void** r10_16;
    void* rax17;
    int64_t* rsp18;
    void** rbx19;
    int64_t* rsp20;
    void** rax21;
    void** v22;
    int64_t* rsp23;
    void** v24;
    int64_t* rsp25;
    void** v26;
    int64_t* rsp27;
    void** v28;
    int64_t* rsp29;
    void*** rax30;
    void** r15_31;
    void*** v32;
    int64_t* rsp33;
    int64_t* rsp34;
    void** v35;
    int64_t* rsp36;
    void** v37;
    int64_t* rsp38;
    void** rsi39;
    int32_t eax40;
    void*** rax41;
    void** rax42;
    struct s39* v43;
    void** tmp64_44;
    void* v45;
    void** r8_46;
    void** tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    void** v53;
    void** rax54;
    void*** rax55;
    struct s40* r14_56;
    struct s40* v57;
    void** r9_58;
    void** r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    void** tmp64_63;
    void** r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    void** rax67;
    int64_t* rsp68;
    void** rax69;
    int64_t* rsp70;
    void** rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;
    void** rax75;
    int64_t* rsp76;
    void** rax77;
    void** tmp64_78;
    int64_t* rsp79;
    void** rax80;
    int64_t* rsp81;
    void** rax82;
    int64_t* rsp83;
    void** rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    void** rsi89;
    void** rax90;
    int64_t* rsp91;
    void** rsi92;
    void** rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s41* rcx98;
    int64_t rax99;
    void** tmp64_100;
    void** r15_101;
    void*** rax102;
    int64_t rax103;
    int64_t* rsp104;
    void** rax105;
    int64_t* rsp106;
    void*** rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    void** rax110;
    int64_t* rsp111;
    void*** rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v13) - reinterpret_cast<int64_t>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0x12b4a;
                fun_35b0();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_12b4a_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_12b54_6;
                addr_12a3e_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0x12a63, rax21 = fun_3790(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0x12a89;
                    fun_34b0(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0x12aaf;
                    fun_34b0(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0x12ad5;
                    fun_34b0(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_12b54_6:
            addr_12738_16:
            v28 = r10_16;
            addr_1273f_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0x12744;
            rax30 = fun_34e0();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_12752_18:
            *v32 = reinterpret_cast<void**>(12);
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0x122f1;
                fun_34b0(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0x12309;
                fun_34b0(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_11f48_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0x11f60;
                fun_34b0(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0x11f78;
            fun_34b0(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_34e0();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *rax41 = reinterpret_cast<void**>(22);
        goto addr_11f48_23;
    }
    rax42 = reinterpret_cast<void**>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_11f3d_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = tmp64_44 + 6;
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_11f3d_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>("loneTable")) {
        rax49 = reinterpret_cast<void*>(tmp64_44 + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 0x1000);
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<void**>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_3730(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_11f3d_33:
            rax55 = fun_34e0();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *rax55 = reinterpret_cast<void**>(12);
            goto addr_11f48_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_11d3c_46;
    while (1) {
        addr_12694_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_11dff_50;
            if (r14_56->f50 != -1) 
                goto 0x38dd;
            r9_58 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_1266f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_12738_16;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_12738_16;
                if (r10_16 == v10) 
                    goto addr_12984_64; else 
                    goto addr_12643_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s40*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_12694_47;
            addr_11d3c_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_127f0_73;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_127f0_73;
                if (r15_31 == v10) 
                    goto addr_12780_79; else 
                    goto addr_11d97_80;
            }
            addr_11dbc_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0x11dd2;
            fun_36e0(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_12780_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0x12788;
            rax67 = fun_3730(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_127f0_73;
            if (!r9_58) 
                goto addr_11dbc_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0x127c7;
            rax69 = fun_36e0(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_11dbc_81;
            addr_11d97_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0x11da2;
            rax71 = fun_3790(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_127f0_73; else 
                goto addr_11dbc_81;
            addr_12984_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0x1299a;
            rax73 = fun_3730(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_12b59_84;
            if (r12_9) 
                goto addr_129ba_86;
            r10_16 = rax73;
            goto addr_1266f_55;
            addr_129ba_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0x129cf;
            rax75 = fun_36e0(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_1266f_55;
            addr_12643_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0x1265c;
            rax77 = fun_3790(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_1273f_17;
            r10_16 = rax77;
            goto addr_1266f_55;
        }
        break;
    }
    tmp64_78 = r12_9 + 1;
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_12b4a_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_12a3e_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_12738_16;
        rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_12afd_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_12afd_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_12738_16; else 
                goto addr_12b07_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_12a13_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0x12b1e;
        rax80 = fun_3730(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_12a3e_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0x12b3d;
                rax82 = fun_36e0(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_12a3e_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0x12a32;
        rax84 = fun_3790(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_1273f_17; else 
            goto addr_12a3e_7;
    }
    addr_12b07_96:
    rbx19 = r13_8;
    goto addr_12a13_98;
    addr_11dff_50:
    if (r14_56->f50 == -1) 
        goto 0x38dd;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x38e2;
        goto *reinterpret_cast<int32_t*>(0x179f8 + r13_87 * 4) + 0x179f8;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<void***>(v53) = reinterpret_cast<void**>(37);
    r13_8 = v53 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        *reinterpret_cast<void***>(v53 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v53 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0x11ecf;
        fun_36e0(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0x11f09;
        fun_36e0(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0x17988 + rax95 * 4) + 0x17988;
    }
    eax96 = r14_56->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x38dd;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s41*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x38dd;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_12042_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_12738_16;
    }
    addr_12042_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_12738_16;
            rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_12063_142; else 
                goto addr_128f2_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_128f2_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_12738_16; else 
                    goto addr_128fc_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_12063_142;
            }
        }
    }
    addr_12095_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0x1209f;
    rax102 = fun_34e0();
    *rax102 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x38e2;
    goto *reinterpret_cast<int32_t*>(0x179b0 + rax103 * 4) + 0x179b0;
    addr_12063_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0x12958;
        rax105 = fun_3730(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_12b59_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0x12b5e;
            rax107 = fun_34e0();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_12752_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0x1297f;
                fun_36e0(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_12095_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0x12082;
        rax110 = fun_3790(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_12738_16; else 
            goto addr_12095_147;
    }
    addr_128fc_145:
    rbx19 = tmp64_100;
    goto addr_12063_142;
    addr_127f0_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0x127f5;
    rax112 = fun_34e0();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_12752_18;
}

int32_t setlocale_null_r();

int64_t fun_12b93() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax1) - reinterpret_cast<int64_t>(g28));
    if (rdx7) {
        fun_35b0();
    } else {
        return rax3;
    }
}

int64_t fun_12c13(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_37a0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_35a0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_36e0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_36e0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_12cc3() {
    __asm__("cli ");
    goto fun_37a0;
}

struct s42 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_12cd3(int64_t rdi, struct s42* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_12d09_5;
    return 0xffffffff;
    addr_12d09_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x17a20 + rdx3 * 4) + 0x17a20;
}

struct s43 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s44 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_12f03(void** rdi, struct s43* rsi, struct s44* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s44* r15_7;
    struct s43* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    void** rax23;
    uint64_t rdi24;
    int32_t edx25;
    void** rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    void** rbx65;
    void* rsi66;
    int32_t eax67;
    void** rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rax88;
    void** rax89;
    void*** rax90;
    void*** rax91;
    void** rdi92;
    void*** rax93;
    void** rbx94;
    void* rdi95;
    int32_t eax96;
    void** rcx97;
    void* rax98;
    void* rdx99;
    void* rdx100;
    void* tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_12fb8_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_12fa5_7:
    return rax15;
    addr_12fb8_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<uint32_t*>(r12_16 + 16) = 0;
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 80) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_13029_11;
    } else {
        addr_13029_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<uint32_t*>(r12_16 + 16) = *reinterpret_cast<uint32_t*>(r12_16 + 16) | 1;
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_13040_14;
        } else {
            goto addr_13040_14;
        }
    }
    rax23 = rax5 + 2;
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = rax23 + 0xffffffffffffffff;
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax23)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_13508_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            ++rax23;
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        ++rax23;
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_13508_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx26 + 2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = rcx26 + 2;
    goto addr_13029_11;
    addr_13040_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x17a9c + rax33 * 4) + 0x17a9c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_13187_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_13889_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_1388e_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_13884_42;
            }
        } else {
            addr_1306d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_13288_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_13077_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 2));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_13a74_56; else 
                        goto addr_132c5_57;
                }
            } else {
                addr_13077_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_13098_60;
                } else {
                    goto addr_13098_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_1360b_65;
    addr_13187_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_13508_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_131ac_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_1322a_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_13b2b_75; else 
            goto addr_131d3_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_1350c_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_13077_52;
        goto addr_13288_44;
    }
    addr_1388e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_1306d_43;
    addr_13b2b_75:
    if (v11 != rdx53) {
        fun_34b0(rdx53);
        r10_4 = r10_4;
        goto addr_1393a_84;
    } else {
        goto addr_1393a_84;
    }
    addr_131d3_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_3730(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_3790(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_13b2b_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_36e0(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_1322a_68;
    addr_1360b_65:
    rbx65 = rbx14 + 2;
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = rbx65 + 0xffffffffffffffff;
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx65)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_13508_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            ++rbx65;
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        ++rbx65;
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_13508_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = rdx68 + 2;
    goto addr_131ac_67;
    label_41:
    addr_13884_42:
    goto addr_13889_36;
    addr_13a74_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_13a9a_104;
    addr_132c5_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_13508_22:
            r8_54 = r15_7->f8;
            goto addr_1350c_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_132d4_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_13982_111;
    } else {
        addr_132e1_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_13317_116;
        }
    }
    rdx53 = r8_54;
    goto addr_13b2b_75;
    addr_13982_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_3730(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_1393a_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_34b0(rdi86);
            }
        } else {
            addr_13bc0_120:
            rdx87 = r15_7->f0;
            rax88 = fun_36e0(rdi84, r8_85, reinterpret_cast<unsigned char>(rdx87) << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_139df_121;
        }
    } else {
        rax89 = fun_3790(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_13b2b_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_13bc0_120;
            }
        }
    }
    rax90 = fun_34e0();
    *rax90 = reinterpret_cast<void**>(12);
    return 0xffffffff;
    addr_139df_121:
    r15_7->f8 = r8_54;
    goto addr_132e1_112;
    addr_13317_116:
    rax91 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_1350c_80:
            if (v11 != r8_54) {
                fun_34b0(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_13077_52;
        }
    } else {
        *rax91 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_13077_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_34b0(rdi92, rdi92);
    }
    rax93 = fun_34e0();
    *rax93 = reinterpret_cast<void**>(22);
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_12fa5_7;
    addr_13a9a_104:
    rbx94 = rbx14 + 3;
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = rbx94 + 0xffffffffffffffff;
        rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (reinterpret_cast<uint64_t>(rdi95) > 0x1999999999999999) {
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi95) + reinterpret_cast<uint64_t>(rdi95) * 4);
            rdx99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx94)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx99) + reinterpret_cast<uint64_t>(rax98)), rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_101) < reinterpret_cast<uint64_t>(rdx99)) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_13508_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            ++rbx94;
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        ++rbx94;
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_101) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_13508_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = rcx97 + 2;
    goto addr_132d4_107;
    addr_13098_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x17c00 + rax105 * 4) + 0x17c00;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x17b44 + rax106 * 4) + 0x17b44;
    }
}

void fun_13c93() {
    __asm__("cli ");
}

void fun_13ca7() {
    __asm__("cli ");
    return;
}

void fun_3ac0() {
    parse_datetime_flags = parse_datetime_flags | 1;
    goto 0x3ab8;
}

void fun_3ae5() {
    goto 0x3ab8;
}

void fun_533f() {
}

void fun_5800(int32_t edi, signed char sil) {
    uint32_t eax3;
    unsigned char v4;
    signed char bpl5;
    uint32_t ebp6;
    signed char v7;
    void** r12_8;
    void** rdi9;
    void* rax10;
    void* rsp11;
    void* rbp12;
    void** rax13;
    void** v14;
    int32_t r8d15;
    int32_t r13d16;
    void** r13_17;
    void** v18;
    int32_t r13d19;
    int64_t r15_20;
    int64_t r14_21;
    void* r13_22;
    uint32_t** rax23;
    unsigned char* rbp24;
    unsigned char* r13_25;
    uint32_t** r12_26;
    int64_t rdx27;
    unsigned char v28;
    void** rdi29;
    void** r14_30;
    void** rdi31;
    uint32_t** rax32;
    unsigned char* rbp33;
    unsigned char* r13_34;
    uint32_t** r12_35;
    int64_t rdx36;
    unsigned char v37;
    void** rdi38;
    void** r14_39;
    void* rbx40;
    void** r14_41;
    void* rbx42;
    void** r14_43;

    eax3 = v4;
    if (bpl5) {
        eax3 = ebp6;
    }
    v7 = *reinterpret_cast<signed char*>(&eax3);
    if (edi == 69) {
        goto 0x5348;
    }
    if (!edi) 
        goto addr_54db_7;
    addr_54e3_9:
    r12_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) + 0xb0);
    rdi9 = r12_8;
    rax10 = fun_37c0(rdi9, rdi9);
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8);
    rbp12 = rax10;
    if (!rax10) 
        goto 0x51ff;
    rax13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax10) + 0xffffffffffffffff);
    v14 = rax13;
    if (r8d15 == 45 || r13d16 < 0) {
        *reinterpret_cast<int32_t*>(&r13_17) = 0;
        *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
        v18 = v14;
    } else {
        r13_17 = reinterpret_cast<void**>(static_cast<int64_t>(r13d19));
        if (reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(r13_17)) {
            rax13 = r13_17;
        }
        v18 = rax13;
    }
    if (reinterpret_cast<unsigned char>(~r15_20) <= reinterpret_cast<unsigned char>(v18)) 
        goto 0x5228;
    if (!r14_21) {
        addr_5610_17:
        goto 0x51ff;
    } else {
        if (reinterpret_cast<unsigned char>(v14) >= reinterpret_cast<unsigned char>(r13_17)) 
            goto addr_55bb_19;
        r13_22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r13_17) - reinterpret_cast<unsigned char>(v14));
        if (r8d15 == 48) 
            goto addr_6689_21;
        if (r8d15 != 43) 
            goto addr_5587_23;
    }
    addr_6689_21:
    if (!r13_22) {
        addr_55bb_19:
        if (0) {
            if (v14) {
                rax23 = fun_3870(rdi9, rdi9);
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                rbp24 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rbp12) + reinterpret_cast<unsigned char>(r12_8));
                r13_25 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp11) + 0xb1);
                r12_26 = rax23;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx27) = v28;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
                    ++r13_25;
                    *reinterpret_cast<uint32_t*>(&rdi29) = (*r12_26)[rdx27];
                    *reinterpret_cast<int32_t*>(&rdi29 + 4) = 0;
                    fun_3650(rdi29, r14_30, rdi29, r14_30);
                } while (r13_25 != rbp24);
                goto addr_5610_17;
            }
        } else {
            if (!v7) {
                rdi31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp11) + 0xb1);
                fun_3830(rdi31, rdi31);
                goto addr_5610_17;
            } else {
                if (v14) {
                    rax32 = fun_3480();
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    rbp33 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rbp12) + reinterpret_cast<unsigned char>(r12_8));
                    r13_34 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp11) + 0xb1);
                    r12_35 = rax32;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx36) = v37;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx36) + 4) = 0;
                        ++r13_34;
                        *reinterpret_cast<uint32_t*>(&rdi38) = (*r12_35)[rdx36];
                        *reinterpret_cast<int32_t*>(&rdi38 + 4) = 0;
                        fun_3650(rdi38, r14_39, rdi38, r14_39);
                    } while (r13_34 != rbp33);
                    goto addr_5610_17;
                }
            }
        }
    } else {
        rbx40 = reinterpret_cast<void*>(0);
        do {
            *reinterpret_cast<int32_t*>(&rdi9) = 48;
            *reinterpret_cast<int32_t*>(&rdi9 + 4) = 0;
            rbx40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx40) + 1);
            fun_3650(48, r14_41, 48, r14_41);
            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        } while (r13_22 != rbx40);
    }
    addr_55b6_37:
    goto addr_55bb_19;
    addr_5587_23:
    if (!r13_22) 
        goto addr_55bb_19;
    rbx42 = reinterpret_cast<void*>(0);
    do {
        *reinterpret_cast<int32_t*>(&rdi9) = 32;
        *reinterpret_cast<int32_t*>(&rdi9 + 4) = 0;
        rbx42 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx42) + 1);
        fun_3650(32, r14_43, 32, r14_43);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
    } while (r13_22 != rbx42);
    goto addr_55b6_37;
    addr_54db_7:
    goto addr_54e3_9;
}

void fun_581d(int32_t edi) {
    void* rsp2;
    int32_t edx3;
    int32_t v4;
    int64_t r12_5;
    int32_t r13d6;
    int32_t r13d7;
    int64_t rax8;
    int64_t rcx9;
    int64_t rcx10;
    int32_t esi11;
    int64_t rax12;
    int64_t rsi13;
    signed char* rcx14;
    int64_t rdi15;
    signed char* rsi16;
    int64_t rax17;
    int64_t rax18;
    int32_t edi19;
    int32_t edx20;

    rsp2 = __zero_stack_offset();
    if (edi == 69) 
        goto 0x5348;
    edx3 = v4;
    *reinterpret_cast<int32_t*>(&r12_5) = 9;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    if (reinterpret_cast<uint1_t>(r13d6 < 0) | reinterpret_cast<uint1_t>(r13d7 == 0)) {
    }
    while (1) {
        rax8 = edx3;
        if (*reinterpret_cast<int32_t*>(&r12_5) <= 9) {
            rcx9 = rax8 * 0x66666667 >> 34;
            *reinterpret_cast<int32_t*>(&rcx10) = *reinterpret_cast<int32_t*>(&rcx9) - (edx3 >> 31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            esi11 = static_cast<int32_t>(rcx10 + rcx10 * 4);
            if (*reinterpret_cast<int32_t*>(&r12_5) == 1) 
                break;
            if (edx3 - (esi11 + esi11)) 
                goto addr_5891_8;
        }
        *reinterpret_cast<int32_t*>(&r12_5) = *reinterpret_cast<int32_t*>(&r12_5) - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        rax12 = rax8 * 0x66666667 >> 34;
        edx3 = *reinterpret_cast<int32_t*>(&rax12) - (edx3 >> 31);
    }
    *reinterpret_cast<int32_t*>(&rsi13) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    addr_58a0_11:
    rcx14 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + rsi13 + 0xb0);
    *reinterpret_cast<int32_t*>(&rdi15) = static_cast<int32_t>(r12_5 - 1);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
    rsi16 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + rsi13 + 0xaf - rdi15);
    while (1) {
        --rcx14;
        rax17 = rax8 * 0x66666667 >> 34;
        *reinterpret_cast<int32_t*>(&rax18) = *reinterpret_cast<int32_t*>(&rax17) - (edx3 >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
        edi19 = static_cast<int32_t>(rax18 + rax18 * 4);
        edx20 = edx3 - (edi19 + edi19) + 48;
        *rcx14 = *reinterpret_cast<signed char*>(&edx20);
        edx3 = *reinterpret_cast<int32_t*>(&rax18);
        if (rcx14 == rsi16) 
            goto "???";
        rax8 = *reinterpret_cast<int32_t*>(&rax18);
    }
    addr_5891_8:
    rsi13 = *reinterpret_cast<int32_t*>(&r12_5);
    if (!*reinterpret_cast<int32_t*>(&r12_5)) 
        goto 0x6c2e; else 
        goto addr_58a0_11;
}

void fun_5a00(int32_t edi) {
    uint32_t edx2;
    uint32_t* v3;
    uint32_t r10d4;
    uint32_t eax5;
    void** rdi6;
    void** r9_7;
    void** rsi8;
    int64_t rax9;
    uint64_t rax10;
    int32_t r8d11;
    int32_t eax12;
    int32_t r8d13;
    int32_t r13d14;
    int64_t r13_15;
    void* rdi16;
    void* r12_17;
    signed char v18;
    int32_t ecx19;
    int32_t ebp20;
    uint64_t v21;
    int64_t r14_22;
    void** v23;
    int64_t rbx24;
    int32_t v25;
    int64_t rbp26;
    void** r14_27;
    uint64_t r15_28;
    int64_t r15_29;
    int64_t r14_30;
    void** rdi31;
    void** r14_32;
    void* rbp33;
    uint64_t r15_34;
    void* rax35;
    void* r12_36;
    int64_t r14_37;
    void* rax38;
    void** v39;
    void* rbx40;
    void* r13_41;
    void** r14_42;
    void* rbx43;
    void* r13_44;
    void** r14_45;
    signed char v46;
    uint32_t** rax47;
    uint32_t** rbx48;
    void** r13_49;
    void** rbp50;
    int64_t rdx51;
    void** v52;
    void** rdi53;
    void** r14_54;

    if (edi == 69) 
        goto 0x5348;
    edx2 = *v3;
    r10d4 = edx2 >> 31;
    eax5 = ~edx2 >> 31;
    if (edi == 79 && *reinterpret_cast<signed char*>(&eax5)) {
    }
    if (*reinterpret_cast<signed char*>(&r10d4)) {
        edx2 = -edx2;
    }
    rdi6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) + 0xc7);
    r9_7 = rdi6;
    while (1) {
        rsi8 = r9_7;
        if (!1) {
            --rsi8;
        }
        *reinterpret_cast<uint32_t*>(&rax9) = edx2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        r9_7 = rsi8 + 0xffffffffffffffff;
        rax10 = reinterpret_cast<uint64_t>(rax9 * 0xcccccccd) >> 35;
        if (edx2 > 9) 
            goto addr_5e38_13;
        if (1) 
            break;
        addr_5e38_13:
        edx2 = *reinterpret_cast<uint32_t*>(&rax10);
    }
    if (!r8d11) {
        eax12 = 1;
    } else {
        *reinterpret_cast<unsigned char*>(&eax12) = reinterpret_cast<uint1_t>(r8d13 != 45);
    }
    if (r13d14 < 0) {
        *reinterpret_cast<int32_t*>(&r13_15) = 2;
    }
    rdi16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdi6) - reinterpret_cast<unsigned char>(r9_7));
    r12_17 = rdi16;
    if (!*reinterpret_cast<signed char*>(&r10d4)) 
        goto addr_5c08_21;
    v18 = 45;
    addr_6515_23:
    ecx19 = 1 - *reinterpret_cast<int32_t*>(&r12_17);
    ebp20 = ecx19;
    if (reinterpret_cast<uint1_t>(ecx19 < 0) | reinterpret_cast<uint1_t>(ecx19 == 0) || !*reinterpret_cast<unsigned char*>(&eax12)) {
        ebp20 = 0;
    }
    if (0) {
        v21 = reinterpret_cast<uint64_t>(static_cast<int64_t>(ebp20));
        if (r14_22 && ebp20) {
            v23 = r9_7;
            rbx24 = ebp20;
            v25 = ebp20;
            rbp26 = 0;
            do {
                ++rbp26;
                fun_3650(32, r14_27);
            } while (rbp26 != rbx24);
            r9_7 = v23;
            ebp20 = v25;
        }
        r15_28 = r15_29 + v21;
        *reinterpret_cast<int32_t*>(&r13_15) = 2 - ebp20;
    }
    if (r15_28 > 0xfffffffffffffffd) 
        goto 0x5228;
    if (r14_30) {
        *reinterpret_cast<int32_t*>(&rdi31) = v18;
        *reinterpret_cast<int32_t*>(&rdi31 + 4) = 0;
        fun_3650(rdi31, r14_32);
        r9_7 = r9_7;
    }
    rbp33 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r12_17)));
    r15_34 = r15_28 + 1;
    *reinterpret_cast<int32_t*>(&rax35) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax35) + 4) = 0;
    r12_36 = rbp33;
    if (*reinterpret_cast<int32_t*>(&r13_15) = *reinterpret_cast<int32_t*>(&r13_15) - 1, *reinterpret_cast<int32_t*>(&r13_15) < 0) {
        addr_5c34_35:
        if (~r15_34 <= reinterpret_cast<uint64_t>(r12_36)) 
            goto 0x5228;
    } else {
        goto addr_5c27_37;
    }
    if (!r14_37) 
        goto 0x5cf0;
    if (reinterpret_cast<uint64_t>(rax35) > reinterpret_cast<uint64_t>(rbp33)) {
        rax38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax35) - reinterpret_cast<uint64_t>(rbp33));
        if (1) {
            v39 = r9_7;
            rbx40 = reinterpret_cast<void*>(0);
            r13_41 = rax38;
            do {
                rbx40 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx40) + 1);
                fun_3650(48, r14_42);
            } while (r13_41 != rbx40);
        } else {
            v39 = r9_7;
            rbx43 = reinterpret_cast<void*>(0);
            r13_44 = rax38;
            do {
                rbx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx43) + 1);
                fun_3650(32, r14_45);
            } while (r13_44 != rbx43);
        }
        r9_7 = v39;
    }
    if (!v46) {
        fun_3830(r9_7, r9_7);
        goto 0x5cf0;
    } else {
        if (!rbp33) 
            goto 0x5cf0;
        rax47 = fun_3480();
        rbx48 = rax47;
        r13_49 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp33) + reinterpret_cast<unsigned char>(r9_7));
        rbp50 = r9_7;
        do {
            *reinterpret_cast<uint32_t*>(&rdx51) = reinterpret_cast<unsigned char>(v52);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx51) + 4) = 0;
            ++rbp50;
            *reinterpret_cast<uint32_t*>(&rdi53) = (*rbx48)[rdx51];
            *reinterpret_cast<int32_t*>(&rdi53 + 4) = 0;
            fun_3650(rdi53, r14_54);
        } while (rbp50 != r13_49);
    }
    addr_5c27_37:
    rax35 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r13_15)));
    r12_36 = rbp33;
    if (reinterpret_cast<uint64_t>(rax35) >= reinterpret_cast<uint64_t>(rbp33)) {
        r12_36 = rax35;
        goto addr_5c34_35;
    }
    addr_5c08_21:
    if (0) {
        v18 = 43;
        goto addr_6515_23;
    } else {
        rbp33 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdi16)));
        if ((2 <= *reinterpret_cast<int32_t*>(&rdi16) || !*reinterpret_cast<unsigned char*>(&eax12)) && (r12_36 = rbp33, *reinterpret_cast<int32_t*>(&rax35) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax35) + 4) = 0, !1)) {
            goto addr_5c34_35;
        }
    }
}

void fun_5a3c(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5a20;
}

void fun_5a53(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5a20;
}

void fun_5a6e(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5a20;
}

void fun_5a89(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5a20;
}

void fun_5af3() {
    void** v1;
    void** rax2;
    void** rsi3;
    int32_t* v4;
    void** rcx5;

    __asm__("movdqu xmm4, [rax+0x20]");
    __asm__("movdqu xmm0, [rax]");
    __asm__("movdqu xmm2, [rax+0x10]");
    __asm__("movaps [rsp+0x70], xmm0");
    __asm__("movaps [rsp+0x80], xmm2");
    __asm__("movaps [rsp+0x40], xmm4");
    __asm__("movaps [rsp+0x90], xmm4");
    rax2 = mktime_z(v1, reinterpret_cast<int64_t>(__zero_stack_offset()) + 0x70);
    rsi3 = rax2;
    if (1) {
        *v4 = 75;
        goto 0x5233;
    } else {
        rcx5 = rsi3;
        do {
            rcx5 = reinterpret_cast<void**>((__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rcx5) >> 63));
            if (reinterpret_cast<signed char>(rsi3) < reinterpret_cast<signed char>(0)) {
            }
        } while (rcx5);
    }
}

void fun_5d0d() {
    int32_t r8d1;
    int32_t r13d2;
    uint64_t r15_3;
    int64_t r14_4;
    uint64_t r12_5;
    int32_t r13d6;
    uint64_t rbp7;
    int64_t r15_8;
    int64_t r14_9;
    int32_t r13d10;
    uint64_t r12_11;
    int32_t r8d12;
    uint64_t r13_13;
    int32_t r8d14;
    uint64_t r13_15;
    void** r14_16;
    void** r14_17;
    void** r14_18;

    if (r8d1 == 45 || r13d2 < 0) {
        if (r15_3 > 0xfffffffffffffffd) 
            goto 0x5228;
        if (!r14_4) 
            goto addr_6787_4;
    } else {
        r12_5 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r13d6));
        *reinterpret_cast<int32_t*>(&rbp7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp7) + 4) = 0;
        if (r12_5) {
            rbp7 = r12_5;
        }
        if (rbp7 >= reinterpret_cast<uint64_t>(~r15_8)) 
            goto 0x5228;
        if (!r14_9) 
            goto 0x613a;
        if (r13d10 > 1) {
            r12_11 = r12_5 - 1;
            if (r8d12 == 48 || (*reinterpret_cast<int32_t*>(&r13_13) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_13) + 4) = 0, r8d14 == 43)) {
                *reinterpret_cast<int32_t*>(&r13_15) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_15) + 4) = 0;
                do {
                    ++r13_15;
                    fun_3650(48, r14_16);
                } while (r12_11 != r13_15);
            } else {
                do {
                    ++r13_13;
                    fun_3650(32, r14_17);
                } while (r12_11 != r13_13);
            }
        }
    }
    fun_3650(9, r14_18);
    goto 0x613a;
    addr_6787_4:
    goto 0x613a;
}

void fun_5d90(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5a20;
}

void fun_5dae(int32_t edi) {
    if (edi == 79) 
        goto 0x57a1;
}

void fun_5e3c() {
    goto 0x5a20;
}

struct s45 {
    signed char[20] pad20;
    uint32_t f14;
};

void fun_5e9e(int32_t edi) {
    int64_t rcx2;
    struct s45* v3;
    uint32_t eax4;
    uint32_t eax5;
    int64_t rdx6;
    int32_t r8d7;
    int32_t v8;
    int32_t r8d9;

    if (edi == 69) 
        goto 0x57a1;
    *reinterpret_cast<uint32_t*>(&rcx2) = v3->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx2) + 4) = 0;
    eax4 = static_cast<uint32_t>(rcx2 + 0x76c);
    eax5 = (eax4 - (eax4 + reinterpret_cast<uint1_t>(eax4 < eax4 + reinterpret_cast<uint1_t>(eax4 < 0x76c))) & 0xffffff9d) + *reinterpret_cast<uint32_t*>(&rcx2);
    rdx6 = reinterpret_cast<int32_t>(eax5) * 0x51eb851f >> 37;
    if (!r8d7) {
        if (v8 == 43) 
            goto addr_6934_4; else 
            goto addr_5f03_5;
    }
    if (r8d9 != 43) {
        addr_5f03_5:
    } else {
        addr_6934_4:
        goto addr_6910_7;
    }
    addr_578d_8:
    addr_6910_7:
    if (99 >= reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rdx6) - (reinterpret_cast<int32_t>(eax5) >> 31) + 19)) {
        goto addr_578d_8;
    } else {
        goto addr_578d_8;
    }
}

void fun_5f15(int32_t edi) {
    int64_t v2;
    int64_t rax3;
    int32_t v4;
    uint32_t r12d5;
    unsigned char v6;
    uint32_t ecx7;
    void** v8;
    uint32_t r8d9;
    int64_t v10;
    void* rax11;
    uint32_t r8d12;
    uint32_t r8d13;
    int32_t r13d14;
    void* v15;
    void* r13_16;
    int32_t r13d17;
    void* rsi18;
    int64_t r15_19;
    int64_t r14_20;
    void* rax21;
    uint32_t v22;
    void* rbx23;
    void* rbp24;
    void** r14_25;
    void* rbx26;
    void* rbp27;
    void** r14_28;
    int64_t rax29;
    int32_t v30;
    uint32_t ecx31;
    void** r14_32;
    void** v33;
    int64_t v34;

    v2 = reinterpret_cast<int64_t>(__return_address());
    if (edi) 
        goto 0x5348;
    *reinterpret_cast<int32_t*>(&rax3) = v4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    r12d5 = v6;
    ecx7 = r12d5;
    rax11 = __strftime_internal_isra_0(0, "%m/%d/%y", v8, *reinterpret_cast<unsigned char*>(&ecx7), r8d9, -1, v10, rax3, v2);
    r8d12 = r8d13;
    if (r8d12 == 45 || r13d14 < 0) {
        v15 = rax11;
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_16) + 4) = 0;
    } else {
        r13_16 = reinterpret_cast<void*>(static_cast<int64_t>(r13d17));
        rsi18 = r13_16;
        if (reinterpret_cast<uint64_t>(rax11) >= reinterpret_cast<uint64_t>(r13_16)) {
            rsi18 = rax11;
        }
        v15 = rsi18;
    }
    if (reinterpret_cast<uint64_t>(~r15_19) <= reinterpret_cast<uint64_t>(v15)) 
        goto 0x5228;
    if (r14_20) {
        if (reinterpret_cast<uint64_t>(r13_16) > reinterpret_cast<uint64_t>(rax11)) {
            rax21 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_16) - reinterpret_cast<uint64_t>(rax11));
            if (r8d12 == 48 || r8d12 == 43) {
                v22 = r8d12;
                rbx23 = rax21;
                rbp24 = reinterpret_cast<void*>(0);
                do {
                    rbp24 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp24) + 1);
                    fun_3650(48, r14_25, 48, r14_25);
                } while (rbx23 != rbp24);
            } else {
                v22 = r8d12;
                rbx26 = rax21;
                rbp27 = reinterpret_cast<void*>(0);
                do {
                    rbp27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp27) + 1);
                    fun_3650(32, r14_28, 32, r14_28);
                } while (rbx26 != rbp27);
            }
            r8d12 = v22;
        }
        *reinterpret_cast<int32_t*>(&rax29) = v30;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        ecx31 = r12d5;
        __strftime_internal_isra_0(r14_32, "%m/%d/%y", v33, *reinterpret_cast<unsigned char*>(&ecx31), r8d12, -1, v34, rax29, v2);
    }
    goto 0x51ff;
}

void fun_6067(int32_t edi) {
    int32_t r8d2;

    if (edi == 69) 
        goto 0x5348;
    if (!r8d2) {
    }
    goto 0x5a20;
}

void fun_608d(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5790;
}

void fun_60c0() {
    int32_t r8d1;
    int32_t r13d2;
    uint64_t r15_3;
    int64_t r14_4;
    uint64_t r12_5;
    int32_t r13d6;
    uint64_t rbp7;
    int64_t r15_8;
    int64_t r14_9;
    int32_t r13d10;
    uint64_t r12_11;
    int32_t r8d12;
    uint64_t r13_13;
    int32_t r8d14;
    uint64_t r13_15;
    void** r14_16;
    void** r14_17;
    void** r14_18;

    if (r8d1 == 45 || r13d2 < 0) {
        if (r15_3 > 0xfffffffffffffffd) 
            goto 0x5228;
        if (!r14_4) 
            goto addr_676a_4;
    } else {
        r12_5 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r13d6));
        *reinterpret_cast<int32_t*>(&rbp7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp7) + 4) = 0;
        if (r12_5) {
            rbp7 = r12_5;
        }
        if (rbp7 >= reinterpret_cast<uint64_t>(~r15_8)) 
            goto 0x5228;
        if (!r14_9) 
            goto 0x613a;
        if (r13d10 > 1) {
            r12_11 = r12_5 - 1;
            if (r8d12 == 48 || (*reinterpret_cast<int32_t*>(&r13_13) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_13) + 4) = 0, r8d14 == 43)) {
                *reinterpret_cast<int32_t*>(&r13_15) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_15) + 4) = 0;
                do {
                    ++r13_15;
                    fun_3650(48, r14_16);
                } while (r12_11 != r13_15);
            } else {
                do {
                    ++r13_13;
                    fun_3650(32, r14_17);
                } while (r12_11 != r13_13);
            }
        }
    }
    fun_3650(10, r14_18);
    addr_676a_4:
    goto 0x613a;
}

void fun_615e(int32_t edi) {
    int32_t r8d2;
    int32_t r8d3;
    int32_t v4;

    if (edi == 69) 
        goto 0x57a1;
    if (edi == 79) 
        goto 0x5348;
    if (r8d2) {
        if (r8d3 == 43) 
            goto "???";
        goto 0x578d;
    } else {
        if (v4 == 43) {
            goto 0x6910;
        }
    }
}

void fun_61bf() {
    uint32_t esi1;
    unsigned char v2;
    void** rdi3;
    void** v4;
    signed char bpl5;
    signed char v6;
    void** rax7;
    void** v8;
    int32_t r13d9;
    int32_t r8d10;
    void** r12_11;
    void** v12;
    int32_t r13d13;
    int64_t r15_14;
    int64_t r14_15;
    void* r12_16;
    void* r13_17;
    void** r14_18;
    void* r13_19;
    void** r14_20;
    signed char bpl21;
    uint32_t** rax22;
    unsigned char* rbp23;
    unsigned char* v24;
    uint32_t** r12_25;
    unsigned char* r13_26;
    int64_t rdx27;
    void** rdi28;
    void** r14_29;
    void** v30;
    uint32_t** rax31;
    unsigned char* rbp32;
    unsigned char* v33;
    uint32_t** r12_34;
    unsigned char* r13_35;
    int64_t rdx36;
    void** rdi37;
    void** r14_38;

    esi1 = v2;
    rdi3 = v4;
    if (bpl5) {
        esi1 = 0;
    }
    v6 = *reinterpret_cast<signed char*>(&esi1);
    rax7 = fun_35a0(rdi3);
    v8 = rax7;
    if (r13d9 < 0 || r8d10 == 45) {
        *reinterpret_cast<int32_t*>(&r12_11) = 0;
        *reinterpret_cast<int32_t*>(&r12_11 + 4) = 0;
        v12 = v8;
    } else {
        r12_11 = reinterpret_cast<void**>(static_cast<int64_t>(r13d13));
        if (reinterpret_cast<unsigned char>(rax7) < reinterpret_cast<unsigned char>(r12_11)) {
            rax7 = r12_11;
        }
        v12 = rax7;
    }
    if (reinterpret_cast<unsigned char>(~r15_14) <= reinterpret_cast<unsigned char>(v12)) 
        goto 0x5228;
    if (!r14_15) 
        goto 0x5610;
    if (reinterpret_cast<unsigned char>(v8) < reinterpret_cast<unsigned char>(r12_11)) {
        r12_16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_11) - reinterpret_cast<unsigned char>(v8));
        if (r8d10 == 48 || r8d10 == 43) {
            if (r12_16) {
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_17) + 4) = 0;
                do {
                    *reinterpret_cast<int32_t*>(&rdi3) = 48;
                    *reinterpret_cast<int32_t*>(&rdi3 + 4) = 0;
                    r13_17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_17) + 1);
                    fun_3650(48, r14_18);
                } while (r12_16 != r13_17);
            }
        } else {
            *reinterpret_cast<int32_t*>(&r13_19) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_19) + 4) = 0;
            if (r12_16) {
                do {
                    *reinterpret_cast<int32_t*>(&rdi3) = 32;
                    *reinterpret_cast<int32_t*>(&rdi3 + 4) = 0;
                    r13_19 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_19) + 1);
                    fun_3650(32, r14_20);
                } while (r12_16 != r13_19);
            }
        }
    }
    if (bpl21) {
        if (!v8) 
            goto 0x5610;
        rax22 = fun_3870(rdi3, rdi3);
        rbp23 = v24;
        r12_25 = rax22;
        r13_26 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(v8) + reinterpret_cast<uint64_t>(rbp23));
        do {
            *reinterpret_cast<uint32_t*>(&rdx27) = *rbp23;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
            ++rbp23;
            *reinterpret_cast<uint32_t*>(&rdi28) = (*r12_25)[rdx27];
            *reinterpret_cast<int32_t*>(&rdi28 + 4) = 0;
            fun_3650(rdi28, r14_29);
        } while (rbp23 != r13_26);
        goto 0x5610;
    } else {
        if (!v6) {
            fun_3830(v30, v30);
            goto 0x5610;
        } else {
            if (!v8) 
                goto 0x5610;
            rax31 = fun_3480();
            rbp32 = v33;
            r12_34 = rax31;
            r13_35 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(v8) + reinterpret_cast<uint64_t>(rbp32));
            do {
                *reinterpret_cast<uint32_t*>(&rdx36) = *rbp32;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx36) + 4) = 0;
                ++rbp32;
                *reinterpret_cast<uint32_t*>(&rdi37) = (*r12_34)[rdx36];
                *reinterpret_cast<int32_t*>(&rdi37 + 4) = 0;
                fun_3650(rdi37, r14_38);
            } while (rbp32 != r13_35);
            goto 0x5610;
        }
    }
}

struct s46 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_62ba(int32_t edi) {
    int64_t rdx2;
    struct s46* v3;
    int64_t rcx4;
    int64_t rdx5;
    int32_t r8d6;
    int32_t r8d7;
    int32_t v8;

    if (edi == 69) 
        goto 0x57a1;
    rdx2 = v3->f14;
    rcx4 = rdx2;
    rdx5 = rdx2 * 0x51eb851f >> 37;
    if (*reinterpret_cast<int32_t*>(&rcx4) - (*reinterpret_cast<int32_t*>(&rdx5) - (*reinterpret_cast<int32_t*>(&rcx4) >> 31)) * 100 < 0) {
        if (*reinterpret_cast<int32_t*>(&rcx4) <= 0xfffff893) {
        }
    }
    if (r8d6) {
        if (r8d7 != 43) {
            addr_5778_8:
        } else {
            addr_68c4_9:
        }
    } else {
        if (v8 == 43) 
            goto addr_68c4_9; else 
            goto addr_5778_8;
    }
}

struct s47 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s48 {
    signed char[32] pad32;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
};

void fun_6303() {
    uint32_t eax1;
    struct s47* rbx2;
    uint64_t rcx3;
    int64_t rbx4;
    struct s48* v5;
    int64_t rdx6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t r11_9;

    eax1 = rbx2->f1;
    *reinterpret_cast<int32_t*>(&rcx3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax1) == 58) {
        while (++rcx3, eax1 = *reinterpret_cast<unsigned char*>(rbx4 + rcx3), *reinterpret_cast<signed char*>(&eax1) == 58) {
        }
    }
    if (*reinterpret_cast<signed char*>(&eax1) != 0x7a) 
        goto 0x5348;
    if (v5->f20 < 0) 
        goto 0x51ff;
    rdx6 = v5->f28;
    if (*reinterpret_cast<int32_t*>(&rdx6) >= 0 && !*reinterpret_cast<int32_t*>(&rdx6)) {
    }
    rax7 = *reinterpret_cast<int32_t*>(&rdx6) * 0xffffffff88888889 >> 32;
    eax8 = (*reinterpret_cast<int32_t*>(&rax7) + *reinterpret_cast<int32_t*>(&rdx6) >> 5) - (*reinterpret_cast<int32_t*>(&rdx6) >> 31);
    r11_9 = eax8 * 0xffffffff88888889 >> 32;
    if (rcx3 == 2) {
        addr_6a4f_10:
        goto 0x5790;
    } else {
        if (rcx3 > 2) {
            if (rcx3 != 3) 
                goto 0x5348;
            if (*reinterpret_cast<int32_t*>(&rdx6) - eax8 * 60) 
                goto addr_6a4f_10;
        } else {
            if (!rcx3) {
                goto 0x5790;
            }
        }
    }
    if (!(eax8 - ((*reinterpret_cast<int32_t*>(&r11_9) + eax8 >> 5) - (eax8 >> 31)) * 60)) {
        goto 0x5790;
    }
    goto 0x5790;
}

void fun_63f7(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5a20;
}

void fun_6415(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5790;
}

void fun_6448(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x6074;
}

void fun_645e(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x6074;
}

void fun_6474() {
    goto 0x6323;
}

void fun_64c0() {
    int1_t zf1;
    signed char bpl2;

    zf1 = bpl2 == 0;
    if (!zf1) {
    }
    if (!zf1) {
    }
    goto 0x54c6;
}

void fun_7fb5() {
}

void fun_8658() {
    goto 0x7ff0;
}

void fun_87ab() {
    goto 0x7ff0;
}

void fun_89b1() {
    goto 0x7ff0;
}

void fun_8a3f() {
    goto 0x7fc5;
}

void fun_8bc1() {
    goto 0x87af;
}

struct s49 {
    signed char[192] pad192;
    int64_t fc0;
};

struct s50 {
    signed char[192] pad192;
    int64_t fc0;
};

struct s51 {
    signed char[225] pad225;
    signed char fe1;
};

void fun_8d1b() {
    struct s49* r12_1;
    struct s50* r12_2;
    void** rax3;
    struct s51* r12_4;
    void** r12_5;
    void** rcx6;
    void** r8_7;
    signed char r9b8;

    r12_1->fc0 = r12_2->fc0 + 1;
    rax3 = fun_3580();
    if (!r12_4->fe1) 
        goto 0x7ff0;
    debug_print_current_time_part_0(rax3, r12_5, 5, rcx6, r8_7, static_cast<int64_t>(r9b8), __return_address());
    goto 0x7ff0;
}

void fun_8ecf() {
    goto 0x89f9;
}

struct s52 {
    signed char[24] pad24;
    int32_t f18;
};

struct s53 {
    signed char[152] pad152;
    int32_t f98;
};

struct s54 {
    signed char[48] pad48;
    int32_t f30;
};

struct s55 {
    signed char[152] pad152;
    int32_t f98;
};

struct s56 {
    signed char[144] pad144;
    int64_t f90;
};

struct s57 {
    signed char[144] pad144;
    int64_t f90;
};

struct s58 {
    signed char[40] pad40;
    int64_t f28;
};

struct s59 {
    signed char[136] pad136;
    int64_t f88;
};

struct s60 {
    signed char[136] pad136;
    int64_t f88;
};

struct s61 {
    signed char[32] pad32;
    int64_t f20;
};

struct s62 {
    signed char[128] pad128;
    int64_t f80;
};

struct s63 {
    signed char[128] pad128;
    int64_t f80;
};

struct s64 {
    signed char[24] pad24;
    int64_t f18;
};

struct s65 {
    signed char[120] pad120;
    int64_t f78;
};

struct s66 {
    signed char[120] pad120;
    int64_t f78;
};

struct s67 {
    signed char[16] pad16;
    int64_t f10;
};

struct s68 {
    signed char[112] pad112;
    int64_t f70;
};

struct s69 {
    signed char[112] pad112;
    int64_t f70;
};

struct s70 {
    signed char[8] pad8;
    int64_t f8;
};

struct s71 {
    signed char[104] pad104;
    int64_t f68;
};

struct s72 {
    signed char[104] pad104;
    int64_t f68;
};

struct s73 {
    signed char[161] pad161;
    signed char fa1;
};

struct s74 {
    signed char[225] pad225;
    signed char fe1;
};

void fun_9020() {
    struct s52* r12_1;
    void** r8_2;
    struct s53* r12_3;
    struct s54* rbx4;
    struct s55* r12_5;
    uint32_t edi6;
    struct s56* r12_7;
    struct s57* r12_8;
    struct s58* rbx9;
    struct s59* r12_10;
    struct s60* r12_11;
    struct s61* rbx12;
    uint32_t esi13;
    struct s62* r12_14;
    struct s63* r12_15;
    struct s64* rbx16;
    uint32_t r11d17;
    struct s65* r12_18;
    struct s66* r12_19;
    struct s67* rbx20;
    struct s68* r12_21;
    struct s69* r12_22;
    struct s70* rbx23;
    void** rcx24;
    struct s71* r12_25;
    struct s72* r12_26;
    int64_t* rbx27;
    uint32_t eax28;
    struct s73* r12_29;
    void** rax30;
    struct s74* r12_31;
    struct s5* r12_32;
    signed char r9b33;

    r12_1->f18 = 0xffff9d90;
    *reinterpret_cast<uint32_t*>(&r8_2) = 0;
    *reinterpret_cast<int32_t*>(&r8_2 + 4) = 0;
    r12_3->f98 = rbx4->f30 + r12_5->f98;
    *reinterpret_cast<unsigned char*>(&r8_2) = __intrinsic();
    edi6 = 0;
    r12_7->f90 = r12_8->f90 + rbx9->f28;
    *reinterpret_cast<unsigned char*>(&edi6) = __intrinsic();
    r12_10->f88 = r12_11->f88 + rbx12->f20;
    esi13 = 0;
    *reinterpret_cast<unsigned char*>(&esi13) = __intrinsic();
    r12_14->f80 = r12_15->f80 + rbx16->f18;
    r11d17 = 0;
    *reinterpret_cast<unsigned char*>(&r11d17) = __intrinsic();
    r12_18->f78 = r12_19->f78 + rbx20->f10;
    r12_21->f70 = r12_22->f70 + rbx23->f8;
    *reinterpret_cast<int32_t*>(&rcx24) = 0;
    *reinterpret_cast<int32_t*>(&rcx24 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx24) = __intrinsic();
    r12_25->f68 = r12_26->f68 + *rbx27;
    eax28 = static_cast<uint32_t>(static_cast<unsigned char>(__intrinsic())) | *reinterpret_cast<uint32_t*>(&r8_2) | edi6 | esi13 | r11d17;
    if (*reinterpret_cast<unsigned char*>(&eax28) | static_cast<unsigned char>(__intrinsic())) 
        goto 0x8260;
    if (rcx24) 
        goto 0x8260;
    r12_29->fa1 = 1;
    rax30 = fun_3580();
    if (!r12_31->fe1) 
        goto 0x7ff0;
    debug_print_relative_time_part_0(rax30, r12_32, 5, rcx24, r8_2, static_cast<int64_t>(r9b33), __return_address());
    goto 0x7ff0;
}

struct s75 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_9115() {
    int64_t rax1;
    int64_t rbx2;
    struct s75* r12_3;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 56);
    r12_3->f18 = *reinterpret_cast<int32_t*>(&rax1);
    goto 0x9038;
}

struct s76 {
    signed char[48] pad48;
    int64_t f30;
};

struct s77 {
    signed char[56] pad56;
    int64_t f38;
};

struct s78 {
    signed char[8] pad8;
    int64_t f8;
};

struct s79 {
    signed char[64] pad64;
    int64_t f40;
};

void fun_91b0() {
    struct s76* r12_1;
    int64_t rbx2;
    int64_t rax3;
    int64_t rbx4;
    struct s77* r12_5;
    int64_t rax6;
    struct s78* rbx7;
    struct s79* r12_8;

    __asm__("movdqu xmm7, [rbx-0x70]");
    r12_1->f30 = *reinterpret_cast<int64_t*>(rbx2 - 96);
    rax3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    __asm__("movups [r12+0x20], xmm7");
    r12_5->f38 = -rax3;
    if (rax3 == 0x8000000000000000) 
        goto 0x8260;
    rax6 = rbx7->f8;
    r12_8->f40 = -rax6;
    if (rax6 != 0x8000000000000000) 
        goto 0x7ff0;
    goto 0x8260;
}

struct s80 {
    signed char[64] pad64;
    int64_t f40;
};

struct s81 {
    signed char[56] pad56;
    int64_t f38;
};

struct s82 {
    signed char[8] pad8;
    int64_t f8;
};

struct s83 {
    signed char[40] pad40;
    int64_t f28;
};

struct s84 {
    signed char[48] pad48;
    int64_t f30;
};

struct s85 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_9208() {
    struct s80* r12_1;
    int64_t rbx2;
    struct s81* r12_3;
    int64_t rbx4;
    int64_t rax5;
    struct s82* rbx6;
    struct s83* r12_7;
    struct s84* r12_8;
    struct s85* rbx9;

    r12_1->f40 = *reinterpret_cast<int64_t*>(rbx2 - 0x68);
    r12_3->f38 = *reinterpret_cast<int64_t*>(rbx4 - 56);
    rax5 = rbx6->f8;
    r12_7->f28 = -rax5;
    if (rax5 == 0x8000000000000000) 
        goto 0x8260;
    r12_8->f30 = rbx9->f10;
    goto 0x7ff0;
}

struct s86 {
    signed char[64] pad64;
    int64_t f40;
};

struct s87 {
    signed char[56] pad56;
    int64_t f38;
};

struct s88 {
    signed char[48] pad48;
    int64_t f30;
};

struct s89 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_924a() {
    struct s86* r12_1;
    int64_t rbx2;
    struct s87* r12_3;
    int64_t rbx4;
    struct s88* r12_5;
    struct s89* rbx6;

    __asm__("movdqu xmm6, [rbx]");
    r12_1->f40 = *reinterpret_cast<int64_t*>(rbx2 - 0x68);
    __asm__("movups [r12+0x20], xmm6");
    r12_3->f38 = *reinterpret_cast<int64_t*>(rbx4 - 56);
    r12_5->f30 = rbx6->f10;
    goto 0x7ff0;
}

void fun_9370() {
    goto 0x7ff0;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_3860(int64_t rdi, void** rsi);

uint32_t fun_3850(void** rdi, void** rsi);

void** fun_3880(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_c0f5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_3580();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_3580();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_35a0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_c3f3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_c3f3_22; else 
                            goto addr_c7ed_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_c8ad_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_cc00_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_c3f0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_c3f0_30; else 
                                goto addr_cc19_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_35a0(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_cc00_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_3660(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_cc00_28; else 
                            goto addr_c29c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_cd60_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_cbe0_40:
                        if (r11_27 == 1) {
                            addr_c76d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_cd28_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_c3a7_44;
                            }
                        } else {
                            goto addr_cbf0_46;
                        }
                    } else {
                        addr_cd6f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_c76d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_c3f3_22:
                                if (v47 != 1) {
                                    addr_c949_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_35a0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_c994_54;
                                    }
                                } else {
                                    goto addr_c400_56;
                                }
                            } else {
                                addr_c3a5_57:
                                ebp36 = 0;
                                goto addr_c3a7_44;
                            }
                        } else {
                            addr_cbd4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_cd6f_47; else 
                                goto addr_cbde_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_c76d_41;
                        if (v47 == 1) 
                            goto addr_c400_56; else 
                            goto addr_c949_52;
                    }
                }
                addr_c461_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_c2f8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_c31d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_c620_65;
                    } else {
                        addr_c489_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_ccd8_67;
                    }
                } else {
                    goto addr_c480_69;
                }
                addr_c331_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_c37c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_ccd8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_c37c_81;
                }
                addr_c480_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_c31d_64; else 
                    goto addr_c489_66;
                addr_c3a7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_c45f_91;
                if (v22) 
                    goto addr_c3bf_93;
                addr_c45f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_c461_62;
                addr_c994_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_d11b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_d18b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_cf8f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_3860(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_3850(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_ca8e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_c44c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_ca98_112;
                    }
                } else {
                    addr_ca98_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_cb69_114;
                }
                addr_c458_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_c45f_91;
                while (1) {
                    addr_cb69_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_d077_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_cad6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_d085_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_cb57_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_cb57_128;
                        }
                    }
                    addr_cb05_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_cb57_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_cad6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_cb05_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_c37c_81;
                addr_d085_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_ccd8_67;
                addr_d11b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_ca8e_109;
                addr_d18b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_ca8e_109;
                addr_c400_56:
                rax93 = fun_3880(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_c44c_110;
                addr_cbde_59:
                goto addr_cbe0_40;
                addr_c8ad_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_c3f3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_c458_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_c3a5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_c3f3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_c8f2_160;
                if (!v22) 
                    goto addr_ccc7_162; else 
                    goto addr_ced3_163;
                addr_c8f2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_ccc7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_ccd8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_c79b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_c603_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_c331_70; else 
                    goto addr_c617_169;
                addr_c79b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_c2f8_63;
                goto addr_c480_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_cbd4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_cd0f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_c3f0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_c2e8_178; else 
                        goto addr_cc92_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_cbd4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_c3f3_22;
                }
                addr_cd0f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_c3f0_30:
                    r8d42 = 0;
                    goto addr_c3f3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_c461_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_cd28_42;
                    }
                }
                addr_c2e8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_c2f8_63;
                addr_cc92_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_cbf0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_c461_62;
                } else {
                    addr_cca2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_c3f3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_d452_188;
                if (v28) 
                    goto addr_ccc7_162;
                addr_d452_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_c603_168;
                addr_c29c_37:
                if (v22) 
                    goto addr_d293_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_c2b3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_cd60_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_cdeb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_c3f3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_c2e8_178; else 
                        goto addr_cdc7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_cbd4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_c3f3_22;
                }
                addr_cdeb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_c3f3_22;
                }
                addr_cdc7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_cbf0_46;
                goto addr_cca2_186;
                addr_c2b3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_c3f3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_c3f3_22; else 
                    goto addr_c2c4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_d39e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_d224_210;
            if (1) 
                goto addr_d222_212;
            if (!v29) 
                goto addr_ce5e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_3590();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_d391_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_c620_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_c3db_219; else 
            goto addr_c63a_220;
        addr_c3bf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_c3d3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_c63a_220; else 
            goto addr_c3db_219;
        addr_cf8f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_c63a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_3590();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_cfad_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_3590();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_d420_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_ce86_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_d077_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_c3d3_221;
        addr_ced3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_c3d3_221;
        addr_c617_169:
        goto addr_c620_65;
        addr_d39e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_c63a_220;
        goto addr_cfad_222;
        addr_d224_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - reinterpret_cast<int64_t>(g28);
        if (!rax111) 
            goto addr_d27e_236;
        fun_35b0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_d420_225;
        addr_d222_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_d224_210;
        addr_ce5e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_d224_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_ce86_226;
        }
        addr_d391_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_c7ed_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x171ac + rax113 * 4) + 0x171ac;
    addr_cc19_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x172ac + rax114 * 4) + 0x172ac;
    addr_d293_190:
    addr_c3db_219:
    goto 0xc0c0;
    addr_c2c4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x170ac + rax115 * 4) + 0x170ac;
    addr_d27e_236:
    goto v116;
}

void fun_c2e0() {
}

void fun_c498() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0xc192;
}

void fun_c4f1() {
    goto 0xc192;
}

void fun_c5de() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0xc461;
    }
    if (v2) 
        goto 0xced3;
    if (!r10_3) 
        goto addr_d03e_5;
    if (!v4) 
        goto addr_cf0e_7;
    addr_d03e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_cf0e_7:
    goto 0xc314;
}

void fun_c5fc() {
}

void fun_c6a7() {
    signed char v1;

    if (v1) {
        goto 0xc62f;
    } else {
        goto 0xc36a;
    }
}

void fun_c6c1() {
    signed char v1;

    if (!v1) 
        goto 0xc6ba; else 
        goto "???";
}

void fun_c6e8() {
    goto 0xc603;
}

void fun_c768() {
}

void fun_c780() {
}

void fun_c7af() {
    goto 0xc603;
}

void fun_c801() {
    goto 0xc790;
}

void fun_c830() {
    goto 0xc790;
}

void fun_c863() {
    goto 0xc790;
}

void fun_cc30() {
    goto 0xc2e8;
}

void fun_cf2e() {
    signed char v1;

    if (v1) 
        goto 0xced3;
    goto 0xc314;
}

void fun_cfd5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0xc314;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0xc2f8;
        goto 0xc314;
    }
}

void fun_d3f2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0xc660;
    } else {
        goto 0xc192;
    }
}

void fun_e988() {
    fun_3580();
}

void fun_102b0(int32_t edi) {
    if (edi != 79) {
        if (edi) 
            goto 0x10730;
    }
}

void fun_10779(int32_t edi) {
    void* rsp2;
    int64_t rbp3;
    int32_t edx4;
    int32_t v5;
    int32_t r15d6;
    int32_t r15d7;
    int64_t rax8;
    int64_t rcx9;
    int64_t rcx10;
    int32_t esi11;
    int64_t rax12;
    void* rsi13;
    void* rdi14;
    signed char* rcx15;
    int64_t r8_16;
    signed char* rdi17;
    int32_t r10d18;
    void* v19;
    uint64_t r13_20;
    int64_t r14_21;
    signed char v22;
    void* rdx23;
    int64_t r14_24;
    int64_t* r14_25;
    int64_t v26;
    void* r14_27;
    void* rax28;
    void* r14_29;
    void* rdi30;
    uint64_t rax31;
    void* rax32;
    void* rcx33;
    int32_t* r14_34;
    int32_t v35;
    void* r14_36;
    uint32_t eax37;
    unsigned char v38;
    signed char* r14_39;
    uint32_t eax40;
    void* r14_41;
    uint32_t** rax42;
    void* rdx43;
    void* rdi44;
    uint32_t** rcx45;
    int64_t r8_46;
    uint32_t eax47;
    void* r14_48;
    int1_t cf49;
    void** r14_50;
    void* r14_51;
    uint64_t r13_52;
    int64_t r13_53;
    int32_t edx54;
    uint64_t v55;
    uint64_t rdx56;
    int64_t v57;
    int64_t rax58;
    int64_t rax59;
    int32_t r8d60;
    int32_t edx61;

    rsp2 = __zero_stack_offset();
    if (edi == 69) 
        goto 0x102c0;
    *reinterpret_cast<int32_t*>(&rbp3) = 9;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
    edx4 = v5;
    if (reinterpret_cast<uint1_t>(r15d6 < 0) | reinterpret_cast<uint1_t>(r15d7 == 0)) {
    }
    while (1) {
        rax8 = edx4;
        if (*reinterpret_cast<int32_t*>(&rbp3) <= 9) {
            rcx9 = rax8 * 0x66666667 >> 34;
            *reinterpret_cast<int32_t*>(&rcx10) = *reinterpret_cast<int32_t*>(&rcx9) - (edx4 >> 31);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            esi11 = static_cast<int32_t>(rcx10 + rcx10 * 4);
            if (*reinterpret_cast<int32_t*>(&rbp3) == 1) 
                break;
            if (edx4 - (esi11 + esi11)) 
                goto addr_107e7_8;
        }
        *reinterpret_cast<int32_t*>(&rbp3) = *reinterpret_cast<int32_t*>(&rbp3) - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp3) + 4) = 0;
        rax12 = rax8 * 0x66666667 >> 34;
        edx4 = *reinterpret_cast<int32_t*>(&rax12) - (edx4 >> 31);
    }
    *reinterpret_cast<int32_t*>(&rsi13) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdi14) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
    goto addr_107f5_11;
    addr_107e7_8:
    rsi13 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rbp3)));
    rdi14 = rsi13;
    if (!*reinterpret_cast<int32_t*>(&rbp3)) {
        *reinterpret_cast<int32_t*>(&rsi13) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0;
    } else {
        addr_107f5_11:
        rcx15 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rdi14) + 0xb0);
        *reinterpret_cast<int32_t*>(&r8_16) = static_cast<int32_t>(rbp3 - 1);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_16) + 4) = 0;
        rdi17 = reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rdi14) + 0xaf - r8_16);
        goto addr_10812_13;
    }
    addr_10841_14:
    if (!r10d18) {
    }
    if (reinterpret_cast<int64_t>(v19) - r13_20 <= reinterpret_cast<uint64_t>(rsi13)) 
        goto 0x101a0;
    if (r14_21) {
        if (!v22) {
            if (reinterpret_cast<uint64_t>(rsi13) >= 8) {
                rdx23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_24 + 8) & 0xfffffffffffffff8);
                *r14_25 = v26;
                *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r14_27) + reinterpret_cast<uint64_t>(rsi13) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xa8);
                rax28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r14_29) - reinterpret_cast<uint64_t>(rdx23));
                rdi30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xb0 - reinterpret_cast<uint64_t>(rax28));
                rax31 = reinterpret_cast<uint64_t>(rax28) + reinterpret_cast<uint64_t>(rsi13) & 0xfffffffffffffff8;
                if (rax31 >= 8) {
                    rax32 = reinterpret_cast<void*>(rax31 & 0xfffffffffffffff8);
                    *reinterpret_cast<int32_t*>(&rcx33) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx33) + 4) = 0;
                    do {
                        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdx23) + reinterpret_cast<uint64_t>(rcx33)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi30) + reinterpret_cast<uint64_t>(rcx33));
                        rcx33 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx33) + 8);
                    } while (reinterpret_cast<uint64_t>(rcx33) < reinterpret_cast<uint64_t>(rax32));
                }
            } else {
                if (*reinterpret_cast<unsigned char*>(&rsi13) & 4) {
                    *r14_34 = v35;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(r14_36) + reinterpret_cast<uint64_t>(rsi13) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xac);
                } else {
                    if (rsi13 && (eax37 = v38, *r14_39 = *reinterpret_cast<signed char*>(&eax37), !!(*reinterpret_cast<unsigned char*>(&rsi13) & 2))) {
                        eax40 = *reinterpret_cast<uint16_t*>(reinterpret_cast<int64_t>(rsp2) + reinterpret_cast<uint64_t>(rsi13) + 0xae);
                        *reinterpret_cast<int16_t*>(reinterpret_cast<int64_t>(r14_41) + reinterpret_cast<uint64_t>(rsi13) - 2) = *reinterpret_cast<int16_t*>(&eax40);
                    }
                }
            }
        } else {
            if (rsi13) {
                rax42 = fun_3480();
                rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
                rdx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi13) + 0xffffffffffffffff);
                rdi44 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) + 0xb0);
                rsi13 = rsi13;
                rcx45 = rax42;
                do {
                    *reinterpret_cast<uint32_t*>(&r8_46) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi44) + reinterpret_cast<uint64_t>(rdx43));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_46) + 4) = 0;
                    eax47 = (*rcx45)[r8_46];
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_48) + reinterpret_cast<uint64_t>(rdx43)) = *reinterpret_cast<signed char*>(&eax47);
                    cf49 = reinterpret_cast<uint64_t>(rdx43) < 1;
                    rdx43 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx43) - 1);
                } while (!cf49);
            }
        }
        r14_50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_51) + reinterpret_cast<uint64_t>(rsi13));
    }
    r13_52 = r13_53 + reinterpret_cast<uint64_t>(rsi13);
    if (edx54 = 9 - *reinterpret_cast<int32_t*>(&rbp3), edx54 < 0) {
        if (v55 != r13_52) 
            goto 0x10168; else 
            goto "???";
    }
    rdx56 = reinterpret_cast<uint64_t>(static_cast<int64_t>(edx54));
    if (rdx56 >= v57 - r13_52) 
        goto 0x101a0;
    if (r14_50) 
        goto addr_108f3_36;
    goto 0x10168;
    addr_108f3_36:
    if (!rdx56) 
        goto 0x10168;
    if (1) 
        goto addr_11b09_39;
    if (!0) 
        goto addr_10917_41;
    addr_11b09_39:
    fun_3640(r14_50, r14_50);
    goto 0x10168;
    addr_10917_41:
    fun_3640(r14_50, r14_50);
    goto 0x10168;
    addr_10812_13:
    while (--rcx15, rax58 = rax8 * 0x66666667 >> 34, *reinterpret_cast<int32_t*>(&rax59) = *reinterpret_cast<int32_t*>(&rax58) - (edx4 >> 31), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax59) + 4) = 0, r8d60 = static_cast<int32_t>(rax59 + rax59 * 4), edx61 = edx4 - (r8d60 + r8d60) + 48, *rcx15 = *reinterpret_cast<signed char*>(&edx61), edx4 = *reinterpret_cast<int32_t*>(&rax59), rcx15 != rdi17) {
        rax8 = *reinterpret_cast<int32_t*>(&rax59);
    }
    goto addr_10841_14;
}

void fun_10930(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10710;
}

void fun_1096d(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10950;
}

void fun_10984(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10950;
}

void fun_1099f(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10950;
}

void fun_109ba(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10950;
}

void fun_10a28() {
    void** v1;
    void** rax2;
    void** rsi3;
    int32_t* v4;
    void** rcx5;

    __asm__("movdqu xmm4, [rax+0x20]");
    __asm__("movdqu xmm0, [rax]");
    __asm__("movdqu xmm2, [rax+0x10]");
    __asm__("movaps [rsp+0x70], xmm0");
    __asm__("movaps [rsp+0x80], xmm2");
    __asm__("movaps [rsp+0x40], xmm4");
    __asm__("movaps [rsp+0x90], xmm4");
    rax2 = mktime_z(v1, reinterpret_cast<int64_t>(__zero_stack_offset()) + 0x70);
    rsi3 = rax2;
    if (1) {
        *v4 = 75;
        goto 0x101ab;
    } else {
        rcx5 = rsi3;
        do {
            rcx5 = reinterpret_cast<void**>((__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(rcx5) >> 63));
            if (reinterpret_cast<signed char>(rsi3) < reinterpret_cast<signed char>(0)) {
            }
        } while (rcx5);
    }
}

void fun_10c15() {
    goto 0x102c0;
}

void fun_10c33() {
    uint64_t rax1;
    int64_t v2;
    uint64_t r13_3;
    int32_t r10d4;
    int32_t r15d5;
    int64_t r14_6;
    uint64_t rdx7;
    int32_t r15d8;
    uint64_t rcx9;
    int64_t r14_10;
    signed char* r14_11;
    int32_t r15d12;
    signed char* r15_13;
    int64_t r14_14;
    int32_t r10d15;
    int32_t r10d16;
    void** r14_17;
    void** r14_18;

    rax1 = v2 - r13_3;
    if (r10d4 == 45 || r15d5 < 0) {
        if (rax1 <= 1) 
            goto 0x101a0;
        if (!r14_6) 
            goto addr_116a8_4;
    } else {
        rdx7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d8));
        *reinterpret_cast<int32_t*>(&rcx9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        if (rdx7) {
            rcx9 = rdx7;
        }
        if (rcx9 >= rax1) 
            goto 0x101a0;
        if (!r14_10) 
            goto addr_10ca8_9; else 
            goto addr_10c6e_10;
    }
    addr_10ca0_11:
    *r14_11 = 9;
    addr_10ca8_9:
    goto 0x10168;
    addr_116a8_4:
    goto addr_10ca8_9;
    addr_10c6e_10:
    if (r15d12 > 1) {
        r15_13 = reinterpret_cast<signed char*>(r14_14 + (rdx7 - 1));
        if (r10d15 == 48 || r10d16 == 43) {
            r14_11 = r15_13;
            fun_3640(r14_17, r14_17);
            goto addr_10ca0_11;
        } else {
            r14_11 = r15_13;
            fun_3640(r14_18, r14_18);
            goto addr_10ca0_11;
        }
    }
}

struct s90 {
    signed char[20] pad20;
    uint32_t f14;
};

void fun_10cd0(int32_t edi) {
    int64_t rcx2;
    struct s90* v3;
    uint32_t eax4;
    uint32_t eax5;
    int64_t rdx6;
    int32_t r10d7;
    int32_t r10d8;
    int32_t v9;

    if (edi == 69) 
        goto 0x10721;
    *reinterpret_cast<uint32_t*>(&rcx2) = v3->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx2) + 4) = 0;
    eax4 = static_cast<uint32_t>(rcx2 + 0x76c);
    eax5 = (eax4 - (eax4 + reinterpret_cast<uint1_t>(eax4 < eax4 + reinterpret_cast<uint1_t>(eax4 < 0x76c))) & 0xffffff9d) + *reinterpret_cast<uint32_t*>(&rcx2);
    rdx6 = reinterpret_cast<int32_t>(eax5) * 0x51eb851f >> 37;
    if (r10d7) {
        if (r10d8 != 43) {
            addr_10d36_4:
            goto 0x10703;
        } else {
            addr_118c2_5:
        }
        if (99 >= reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rdx6) - (reinterpret_cast<int32_t>(eax5) >> 31) + 19)) 
            goto 0x117cc;
        goto 0x10703;
    } else {
        if (v9 == 43) 
            goto addr_118c2_5; else 
            goto addr_10d36_4;
    }
}

void fun_10d48(int32_t edi) {
    uint32_t r8d2;
    unsigned char v3;
    int64_t rax4;
    int32_t v5;
    void** v6;
    void** r10d7;
    int64_t v8;
    int64_t v9;
    void** rax10;
    void** r10d11;
    void** r10d12;
    uint32_t r8d13;
    int32_t r15d14;
    void** v15;
    void** rdx16;
    int32_t r15d17;
    void** rax18;
    void** r15_19;
    void* v20;
    uint64_t r13_21;
    int64_t r14_22;
    void** v23;
    void* r14_24;
    int64_t v25;
    void** r14_26;
    void** r14_27;
    void** r14_28;
    int64_t rax29;
    int32_t v30;
    void** v31;
    int64_t v32;

    if (edi) 
        goto 0x102c0;
    r8d2 = v3;
    *reinterpret_cast<int32_t*>(&rax4) = v5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    rax10 = __strftime_internal_isra_0(0, 0xffffffffffffffff, "%m/%d/%y", v6, *reinterpret_cast<unsigned char*>(&r8d2), r10d7, -1, v8, rax4, v9, __return_address());
    r10d11 = r10d12;
    r8d13 = r8d2;
    if (r10d11 == 45 || r15d14 < 0) {
        v15 = rax10;
        *reinterpret_cast<int32_t*>(&rdx16) = 0;
        *reinterpret_cast<int32_t*>(&rdx16 + 4) = 0;
    } else {
        rdx16 = reinterpret_cast<void**>(static_cast<int64_t>(r15d17));
        rax18 = rdx16;
        if (reinterpret_cast<unsigned char>(rax10) >= reinterpret_cast<unsigned char>(rdx16)) {
            rax18 = rax10;
        }
        v15 = rax18;
    }
    r15_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v20) - r13_21);
    if (reinterpret_cast<unsigned char>(r15_19) <= reinterpret_cast<unsigned char>(v15)) 
        goto 0x101a0;
    if (r14_22) {
        if (reinterpret_cast<unsigned char>(rdx16) > reinterpret_cast<unsigned char>(rax10)) {
            v23 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_24) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx16) - reinterpret_cast<unsigned char>(rax10)));
            if (r10d11 == 48 || r10d11 == 43) {
                v25 = 0x116fe;
                fun_3640(r14_26, r14_26);
                r14_27 = v23;
                r8d13 = r8d13;
                r10d11 = r10d11;
            } else {
                v25 = 0x10e3a;
                fun_3640(r14_28, r14_28);
                r14_27 = v23;
                r10d11 = r10d11;
                r8d13 = r8d13;
            }
        }
        *reinterpret_cast<int32_t*>(&rax29) = v30;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax29) + 4) = 0;
        __strftime_internal_isra_0(r14_27, r15_19, "%m/%d/%y", v31, *reinterpret_cast<unsigned char*>(&r8d13), r10d11, -1, v32, rax29, v25, __return_address());
    }
    goto 0x10168;
}

void fun_10ebb(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10950;
}

void fun_10ed9(int32_t edi) {
    if (edi == 79) 
        goto 0x10721;
}

void fun_10f67() {
    goto 0x10950;
}

void fun_10fd1(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10710;
}

void fun_11005() {
    uint64_t rax1;
    int64_t v2;
    uint64_t r13_3;
    int32_t r10d4;
    int32_t r15d5;
    int64_t r14_6;
    uint64_t rdx7;
    int32_t r15d8;
    uint64_t rcx9;
    int64_t r14_10;
    int32_t r15d11;
    signed char* r15_12;
    int64_t r14_13;
    int32_t r10d14;
    int32_t r10d15;
    signed char* r14_16;
    void** r14_17;
    void** r14_18;

    rax1 = v2 - r13_3;
    if (r10d4 == 45 || r15d5 < 0) {
        if (rax1 <= 1) 
            goto 0x101a0;
        if (!r14_6) 
            goto addr_116c5_4;
    } else {
        rdx7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d8));
        *reinterpret_cast<int32_t*>(&rcx9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        if (rdx7) {
            rcx9 = rdx7;
        }
        if (rcx9 >= rax1) 
            goto 0x101a0;
        if (!r14_10) 
            goto 0x10ca8;
        if (r15d11 > 1) {
            r15_12 = reinterpret_cast<signed char*>(r14_13 + (rdx7 - 1));
            if (r10d14 == 48 || r10d15 == 43) {
                r14_16 = r15_12;
                fun_3640(r14_17, r14_17);
            } else {
                r14_16 = r15_12;
                fun_3640(r14_18, r14_18);
            }
        }
    }
    *r14_16 = 10;
    goto 0x10ca8;
    addr_116c5_4:
    goto 0x10ca8;
}

void fun_1109f(int32_t edi) {
    int32_t r10d2;
    int32_t r10d3;
    int32_t v4;

    if (edi == 69) 
        goto 0x10721;
    if (edi == 79) 
        goto 0x102c0;
    if (r10d2) {
        if (r10d3 == 43) 
            goto "???";
        goto 0x10703;
    } else {
        if (v4 == 43) {
            goto 0x1180f;
        }
    }
}

void fun_11109() {
    uint32_t edi1;
    unsigned char v2;
    signed char r8b3;
    void** rdi4;
    void** r12_5;
    void** rax6;
    uint32_t r8d7;
    unsigned char r8b8;
    int32_t r10d9;
    int32_t r15d10;
    void** v11;
    void** r15_12;
    int32_t r15d13;
    void** rax14;
    void* v15;
    uint64_t r13_16;
    int64_t r14_17;
    void** r15_18;
    void* r14_19;
    void** r14_20;
    void** r14_21;
    void** r14_22;
    unsigned char* r15_23;
    uint32_t** rax24;
    uint32_t** rsi25;
    int64_t rdx26;
    void* r12_27;
    uint32_t eax28;
    int1_t cf29;
    void** r12_30;
    unsigned char* r15_31;
    uint32_t** rax32;
    uint32_t** rsi33;
    int64_t rdx34;
    void* r12_35;
    uint32_t eax36;
    int1_t cf37;

    edi1 = v2;
    if (r8b3) {
        edi1 = 0;
    }
    rdi4 = r12_5;
    rax6 = fun_35a0(rdi4);
    r8d7 = r8b8;
    if (r10d9 == 45 || r15d10 < 0) {
        v11 = rax6;
        *reinterpret_cast<int32_t*>(&r15_12) = 0;
        *reinterpret_cast<int32_t*>(&r15_12 + 4) = 0;
    } else {
        r15_12 = reinterpret_cast<void**>(static_cast<int64_t>(r15d13));
        rax14 = r15_12;
        if (reinterpret_cast<unsigned char>(rax6) >= reinterpret_cast<unsigned char>(r15_12)) {
            rax14 = rax6;
        }
        v11 = rax14;
    }
    if (reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(v15) - r13_16) <= reinterpret_cast<unsigned char>(v11)) 
        goto 0x101a0;
    if (r14_17) {
        if (reinterpret_cast<unsigned char>(rax6) < reinterpret_cast<unsigned char>(r15_12)) {
            r15_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r14_19) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_12) - reinterpret_cast<unsigned char>(rax6)));
            if (r10d9 == 48 || r10d9 == 43) {
                rdi4 = r14_20;
                r14_21 = r15_18;
                fun_3640(rdi4, rdi4);
                r8d7 = *reinterpret_cast<unsigned char*>(&r8d7);
            } else {
                rdi4 = r14_22;
                r14_21 = r15_18;
                fun_3640(rdi4, rdi4);
                r8d7 = *reinterpret_cast<unsigned char*>(&r8d7);
            }
        }
        if (*reinterpret_cast<unsigned char*>(&r8d7)) {
            r15_23 = reinterpret_cast<unsigned char*>(rax6 + 0xffffffffffffffff);
            if (rax6) {
                rax24 = fun_3870(rdi4, rdi4);
                rsi25 = rax24;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx26) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_27) + reinterpret_cast<uint64_t>(r15_23));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx26) + 4) = 0;
                    eax28 = (*rsi25)[rdx26];
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<uint64_t>(r15_23)) = *reinterpret_cast<signed char*>(&eax28);
                    cf29 = reinterpret_cast<uint64_t>(r15_23) < 1;
                    --r15_23;
                } while (!cf29);
            }
        } else {
            if (!*reinterpret_cast<signed char*>(&edi1)) {
                fun_36e0(r14_21, r12_30, rax6);
            } else {
                r15_31 = reinterpret_cast<unsigned char*>(rax6 + 0xffffffffffffffff);
                if (rax6) {
                    rax32 = fun_3480();
                    rsi33 = rax32;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx34) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_35) + reinterpret_cast<uint64_t>(r15_31));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx34) + 4) = 0;
                        eax36 = (*rsi33)[rdx34];
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r14_21) + reinterpret_cast<uint64_t>(r15_31)) = *reinterpret_cast<signed char*>(&eax36);
                        cf37 = reinterpret_cast<uint64_t>(r15_31) < 1;
                        --r15_31;
                    } while (!cf37);
                }
            }
        }
    }
    goto 0x10168;
}

void fun_1125d(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10950;
}

void fun_1127b(int32_t edi) {
    int32_t r10d2;

    if (edi == 69) 
        goto 0x102c0;
    if (!r10d2) {
    }
    goto 0x10950;
}

void fun_11291(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10710;
}

void fun_112c5(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10fb8;
}

struct s91 {
    signed char[1] pad1;
    unsigned char f1;
};

struct s92 {
    signed char[32] pad32;
    int32_t f20;
    signed char[4] pad40;
    int64_t f28;
};

void fun_112db() {
    uint32_t eax1;
    struct s91* rbx2;
    uint64_t r11_3;
    int64_t rbx4;
    struct s92* v5;
    int64_t rdx6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t r9_9;

    eax1 = rbx2->f1;
    *reinterpret_cast<int32_t*>(&r11_3) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_3) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax1) == 58) {
        while (++r11_3, eax1 = *reinterpret_cast<unsigned char*>(rbx4 + r11_3), *reinterpret_cast<signed char*>(&eax1) == 58) {
        }
    }
    if (*reinterpret_cast<signed char*>(&eax1) != 0x7a) 
        goto 0x102c0;
    if (v5->f20 < 0) 
        goto 0x10168;
    rdx6 = v5->f28;
    if (*reinterpret_cast<int32_t*>(&rdx6) >= 0 && !*reinterpret_cast<int32_t*>(&rdx6)) {
    }
    rax7 = *reinterpret_cast<int32_t*>(&rdx6) * 0xffffffff88888889 >> 32;
    eax8 = (*reinterpret_cast<int32_t*>(&rax7) + *reinterpret_cast<int32_t*>(&rdx6) >> 5) - (*reinterpret_cast<int32_t*>(&rdx6) >> 31);
    r9_9 = eax8 * 0xffffffff88888889 >> 32;
    if (r11_3 == 2) {
        addr_119a6_10:
        goto 0x10710;
    } else {
        if (r11_3 > 2) {
            if (r11_3 != 3) 
                goto 0x102c0;
            if (*reinterpret_cast<int32_t*>(&rdx6) - eax8 * 60) 
                goto addr_119a6_10;
        } else {
            if (!r11_3) {
                goto 0x10710;
            }
        }
    }
    if (!(eax8 - ((*reinterpret_cast<int32_t*>(&r9_9) + eax8 >> 5) - (eax8 >> 31)) * 60)) {
        goto 0x10710;
    }
    goto 0x10710;
}

void fun_113c9() {
    goto 0x112fc;
}

void fun_113f5() {
    goto 0x10d5f;
}

struct s93 {
    signed char[1] pad1;
    signed char f1;
};

struct s94 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_12108() {
    void** rdi1;
    void* r15_2;
    void* r12_3;
    int64_t rbp4;
    void** r8_5;
    int64_t rbp6;
    int64_t rbp7;
    int64_t rsi8;
    void** rsi9;
    int64_t rsi10;
    uint32_t eax11;
    int64_t rdx12;
    int64_t rbp13;
    void** rsi14;
    int64_t rbp15;
    void** r8_16;
    int64_t rbp17;
    int64_t rbp18;
    int64_t rsi19;
    void** rsi20;
    int64_t rsi21;
    int64_t rbp22;
    void** r8_23;
    int64_t rbp24;
    int64_t rbp25;
    int64_t rsi26;
    void** rsi27;
    int64_t rbp28;
    int64_t rbp29;
    void** rcx30;
    uint64_t r15_31;
    int64_t r12_32;
    void** rax33;
    int64_t rbp34;
    int64_t rbp35;
    int64_t rbp36;
    uint64_t r13_37;
    int64_t rbp38;
    int64_t rbx39;
    int64_t rbx40;
    void** rax41;
    void** rcx42;
    void* rbx43;
    void* rbx44;
    void** tmp64_45;
    void* r12_46;
    void** rax47;
    void** rbx48;
    int64_t r15_49;
    int64_t rbp50;
    void** r15_51;
    void** rax52;
    void** rax53;
    int64_t r12_54;
    void** r15_55;
    void** r12_56;
    int64_t rbp57;
    int64_t rbp58;
    uint32_t eax59;
    struct s94* r14_60;
    int32_t eax61;
    int64_t rbp62;

    rdi1 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r15_2) + reinterpret_cast<uint64_t>(r12_3));
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        r8_5 = *reinterpret_cast<void***>(rbp6 - 0x3e0);
        *reinterpret_cast<int64_t*>(rbp7 - 0x418) = rsi8;
        eax11 = fun_34a0(rdi1, rsi9, 1, 0xffffffffffffffff, r8_5, rdi1, rsi10, 1, 0xffffffffffffffff, r8_5);
        *reinterpret_cast<uint32_t*>(&rdx12) = *reinterpret_cast<uint32_t*>(rbp13 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
        rsi14 = *reinterpret_cast<void***>(rbp15 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx12) < reinterpret_cast<int32_t>(0)) 
            goto addr_12293_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            r8_16 = *reinterpret_cast<void***>(rbp17 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp18 - 0x418) = rsi19;
            eax11 = fun_34a0(rdi1, rsi20, 1, 0xffffffffffffffff, r8_16, rdi1, rsi21, 1, 0xffffffffffffffff, r8_16);
            rsi14 = *reinterpret_cast<void***>(rbp22 - 0x418);
        } else {
            r8_23 = *reinterpret_cast<void***>(rbp24 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp25 - 0x418) = rsi26;
            eax11 = fun_34a0(rdi1, rsi27, 1, 0xffffffffffffffff, r8_23);
            rsi14 = *reinterpret_cast<void***>(rbp28 - 0x418);
        }
        *reinterpret_cast<uint32_t*>(&rdx12) = *reinterpret_cast<uint32_t*>(rbp29 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx12) < reinterpret_cast<int32_t>(0)) 
            goto addr_12293_5;
    }
    rcx30 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx12)));
    if (reinterpret_cast<unsigned char>(rcx30) < reinterpret_cast<unsigned char>(rsi14)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx30) + r15_31 + r12_32)) 
            goto 0x38dd;
    }
    if (*reinterpret_cast<int32_t*>(&rdx12) >= reinterpret_cast<int32_t>(eax11)) {
        addr_1219d_16:
        *reinterpret_cast<int32_t*>(&rax33) = static_cast<int32_t>(rdx12 + 1);
        *reinterpret_cast<int32_t*>(&rax33 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax33) < reinterpret_cast<unsigned char>(rsi14)) {
            **reinterpret_cast<int32_t**>(rbp34 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp35 - 0x40c);
            goto 0x12677;
        }
    } else {
        addr_12195_18:
        *reinterpret_cast<uint32_t*>(rbp36 - 0x3bc) = eax11;
        *reinterpret_cast<uint32_t*>(&rdx12) = eax11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
        goto addr_1219d_16;
    }
    if (r13_37 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp38 - 0x3d0) = 75;
        goto 0x122e0;
    }
    if (rbx39 < 0) {
        if (rbx40 == -1) 
            goto 0x120b8;
        goto 0x12752;
    }
    *reinterpret_cast<int32_t*>(&rax41) = static_cast<int32_t>(rdx12 + 2);
    *reinterpret_cast<int32_t*>(&rax41 + 4) = 0;
    rcx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx43) + reinterpret_cast<uint64_t>(rbx44));
    tmp64_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax41) + reinterpret_cast<uint64_t>(r12_46));
    rax47 = tmp64_45;
    if (reinterpret_cast<unsigned char>(tmp64_45) < reinterpret_cast<unsigned char>(rax41)) 
        goto 0x12752;
    if (reinterpret_cast<unsigned char>(rax47) >= reinterpret_cast<unsigned char>(rcx42)) 
        goto addr_121d6_26;
    rax47 = rcx42;
    addr_121d6_26:
    if (reinterpret_cast<unsigned char>(rbx48) >= reinterpret_cast<unsigned char>(rax47)) 
        goto 0x120b8;
    if (reinterpret_cast<unsigned char>(rcx42) >= reinterpret_cast<unsigned char>(rax47)) {
        rax47 = rcx42;
    }
    if (rax47 == 0xffffffffffffffff) 
        goto 0x12752;
    if (r15_49 != *reinterpret_cast<int64_t*>(rbp50 - 0x3e8)) {
        rax52 = fun_3790(r15_51, rax47);
        if (!rax52) 
            goto 0x12752;
        goto 0x120b8;
    }
    rax53 = fun_3730(rax47, rsi14);
    if (!rax53) 
        goto 0x12752;
    if (r12_54) 
        goto addr_1259a_36;
    goto 0x120b8;
    addr_1259a_36:
    fun_36e0(rax53, r15_55, r12_56, rax53, r15_55, r12_56);
    goto 0x120b8;
    addr_12293_5:
    if ((*reinterpret_cast<struct s93**>(rbp57 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s93**>(rbp57 - 0x3f0))->f1 = 0;
        goto 0x120b8;
    }
    if (reinterpret_cast<int32_t>(eax11) >= reinterpret_cast<int32_t>(0)) 
        goto addr_12195_18;
    if (**reinterpret_cast<int32_t**>(rbp58 - 0x3d0)) 
        goto 0x122e0;
    eax59 = static_cast<uint32_t>(r14_60->f48) & 0xffffffef;
    eax61 = 22;
    if (*reinterpret_cast<signed char*>(&eax59) != 99) 
        goto addr_122d7_42;
    eax61 = 84;
    addr_122d7_42:
    **reinterpret_cast<int32_t**>(rbp62 - 0x3d0) = eax61;
}

void fun_12220() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_12310() {
    void** rdi1;
    void* r15_2;
    void* r12_3;
    int64_t rbp4;
    void** r8_5;
    int64_t rbp6;
    int64_t rbp7;
    int64_t rsi8;
    void** rsi9;
    int64_t rsi10;

    rdi1 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r15_2) + reinterpret_cast<uint64_t>(r12_3));
    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x12455;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            r8_5 = *reinterpret_cast<void***>(rbp6 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp7 - 0x418) = rsi8;
            __asm__("fstp tword [rsp+0x8]");
            fun_34a0(rdi1, rsi9, 1, 0xffffffffffffffff, r8_5, rdi1, rsi10, 1, 0xffffffffffffffff, r8_5);
            goto 0x1216d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x12143;
        }
    }
}

struct s95 {
    int32_t f0;
    void** f4;
};

struct s96 {
    signed char[4] pad4;
    void** f4;
};

void fun_12358() {
    struct s95* rdi1;
    void* r15_2;
    void* r12_3;
    int32_t* rsi4;
    void** rdi5;
    void** rsi6;
    struct s96* rsi7;
    int64_t rbp8;
    void** r8_9;
    int64_t rbp10;
    int64_t rbp11;
    void** r8_12;
    int64_t rbp13;
    int64_t rbp14;

    rdi1 = reinterpret_cast<struct s95*>(reinterpret_cast<int64_t>(r15_2) + reinterpret_cast<int64_t>(r12_3));
    rdi1->f0 = *rsi4;
    rdi5 = reinterpret_cast<void**>(&rdi1->f4);
    rsi6 = reinterpret_cast<void**>(&rsi7->f4);
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            r8_9 = *reinterpret_cast<void***>(rbp10 - 0x3e0);
            *reinterpret_cast<void***>(rbp11 - 0x418) = rsi6;
            fun_34a0(rdi5, rsi6, 1, 0xffffffffffffffff, r8_9, rdi5, rsi6, 1, 0xffffffffffffffff, r8_9);
            goto 0x1216d;
        }
    }
    r8_12 = *reinterpret_cast<void***>(rbp13 - 0x3e0);
    *reinterpret_cast<void***>(rbp14 - 0x418) = rsi6;
    fun_34a0(rdi5, rsi6, 1, 0xffffffffffffffff, r8_12, rdi5, rsi6, 1, 0xffffffffffffffff, r8_12);
    goto 0x1216d;
}

void fun_123c0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x12246;
}

void fun_12410() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x123f0;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x1224f;
}

void fun_124c0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x12246;
    goto 0x123f0;
}

void fun_12553() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x11fc2;
}

void fun_126f0() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x12677;
}

struct s97 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s98 {
    signed char[8] pad8;
    int64_t f8;
};

struct s99 {
    signed char[16] pad16;
    int64_t f10;
};

struct s100 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_12d18() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s97* rcx3;
    struct s98* rcx4;
    int64_t r11_5;
    struct s99* rcx6;
    uint32_t* rcx7;
    struct s100* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x12d00; else 
        goto "???";
}

struct s101 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s102 {
    signed char[8] pad8;
    int64_t f8;
};

struct s103 {
    signed char[16] pad16;
    int64_t f10;
};

struct s104 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_12d50() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s101* rcx3;
    struct s102* rcx4;
    int64_t r11_5;
    struct s103* rcx6;
    uint32_t* rcx7;
    struct s104* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x12d36;
}

struct s105 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s106 {
    signed char[8] pad8;
    int64_t f8;
};

struct s107 {
    signed char[16] pad16;
    int64_t f10;
};

struct s108 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_12d70() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s105* rcx3;
    struct s106* rcx4;
    int64_t r11_5;
    struct s107* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s108* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x12d36;
}

struct s109 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s110 {
    signed char[8] pad8;
    int64_t f8;
};

struct s111 {
    signed char[16] pad16;
    int64_t f10;
};

struct s112 {
    signed char[16] pad16;
    signed char f10;
};

void fun_12d90() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s109* rcx3;
    struct s110* rcx4;
    int64_t r11_5;
    struct s111* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s112* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x12d36;
}

struct s113 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s114 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_12e10() {
    struct s113* rcx1;
    struct s114* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x12d36;
}

struct s115 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s116 {
    signed char[8] pad8;
    int64_t f8;
};

struct s117 {
    signed char[16] pad16;
    int64_t f10;
};

struct s118 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_12e60() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s115* rcx3;
    struct s116* rcx4;
    int64_t r11_5;
    struct s117* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s118* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x12d36;
}

void fun_130ac() {
}

void fun_130db() {
    goto 0x130b8;
}

void fun_13130() {
}

void fun_13340() {
    goto 0x13133;
}

struct s119 {
    signed char[8] pad8;
    void** f8;
};

struct s120 {
    signed char[8] pad8;
    int64_t f8;
};

struct s121 {
    signed char[8] pad8;
    void** f8;
};

struct s122 {
    signed char[8] pad8;
    void** f8;
};

void fun_133e8() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s119* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s120* r15_14;
    void** rax15;
    struct s121* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s122* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x13b27;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x13b27;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_3730(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x13948; else 
                goto "???";
        }
    } else {
        rax15 = fun_3790(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x13b27;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_13c7a_9; else 
            goto addr_1345e_10;
    }
    addr_135b4_11:
    rax19 = fun_36e0(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_1345e_10:
    r14_20->f8 = rcx12;
    goto 0x12f79;
    addr_13c7a_9:
    r13_18 = *r14_21;
    goto addr_135b4_11;
}

struct s123 {
    signed char[11] pad11;
    void** fb;
};

struct s124 {
    signed char[80] pad80;
    int64_t f50;
};

struct s125 {
    signed char[80] pad80;
    int64_t f50;
};

struct s126 {
    signed char[8] pad8;
    void** f8;
};

struct s127 {
    signed char[8] pad8;
    void** f8;
};

struct s128 {
    signed char[8] pad8;
    void** f8;
};

struct s129 {
    signed char[72] pad72;
    signed char f48;
};

struct s130 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_13671() {
    void** ecx1;
    int32_t edx2;
    struct s123* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s124* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s125* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s126* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rax32;
    struct s127* r15_33;
    void** rax34;
    uint64_t r11_35;
    void** v36;
    struct s128* r15_37;
    void*** r13_38;
    struct s129* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s130* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s123*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x13508;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x13c5c;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_137cd_12;
    } else {
        addr_13374_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_133af_17;
        }
    }
    rax25 = fun_3730(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x1393a;
    addr_138e0_19:
    rdx30 = *r15_31;
    rax32 = fun_36e0(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_13816_20:
    r15_33->f8 = r8_12;
    goto addr_13374_13;
    addr_137cd_12:
    rax34 = fun_3790(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x13b27;
    if (v36 != r15_37->f8) 
        goto addr_13816_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_138e0_19;
    addr_133af_17:
    r13_38 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x1350c;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x133f0;
    goto 0x12f79;
}

void fun_1368f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x13358;
    if (dl2 & 4) 
        goto 0x13358;
    if (edx3 > 7) 
        goto 0x13358;
    if (dl4 & 2) 
        goto 0x13358;
    goto 0x13358;
}

void fun_136d8() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x13358;
    if (dl2 & 4) 
        goto 0x13358;
    if (edx3 > 7) 
        goto 0x13358;
    if (dl4 & 2) 
        goto 0x13358;
    goto 0x13358;
}

void fun_13720() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x13358;
    if (dl2 & 4) 
        goto 0x13358;
    if (edx3 > 7) 
        goto 0x13358;
    if (dl4 & 2) 
        goto 0x13358;
    goto 0x13358;
}

void fun_13768() {
    goto 0x13358;
}

int32_t fun_3690(int64_t rdi);

void fun_3acc() {
    int32_t eax1;

    eax1 = fun_3690("TZ=UTC0");
    if (eax1) 
        goto 0x3f4a;
    goto 0x3ab8;
}

void fun_3afb() {
    goto 0x3ab8;
}

void fun_3bc4() {
    goto 0x3ab8;
}

void fun_57ca(int32_t edi) {
    signed char bpl2;

    if (edi) 
        goto 0x5348;
    if (bpl2) {
    }
    goto 0x54e3;
}

struct s131 {
    signed char[20] pad20;
    int32_t f14;
    uint32_t f18;
    int32_t f1c;
};

void fun_561a(int32_t edi, signed char sil) {
    int64_t r9_3;
    struct s131* v4;
    uint32_t ecx5;
    int64_t rax6;
    int32_t eax7;
    uint64_t rdx8;
    int64_t rdx9;
    int64_t rbp10;
    int64_t rdx11;
    int32_t eax12;
    int64_t rax13;
    uint32_t edx14;
    int32_t edx15;
    uint32_t edx16;
    uint32_t r10d17;
    uint32_t edx18;
    int64_t r10_19;
    uint64_t rax20;
    int64_t rax21;
    int64_t r11_22;
    int32_t eax23;
    int32_t r8d24;
    int32_t v25;
    int64_t rdx26;
    int32_t ecx27;
    int64_t rdx28;
    int32_t r8d29;
    int32_t v30;
    int32_t r8d31;

    if (edi == 69) 
        goto 0x5348;
    *reinterpret_cast<int32_t*>(&r9_3) = v4->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_3) + 4) = 0;
    ecx5 = v4->f1c - v4->f18 + 0x17e;
    *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&r9_3) >> 31) & 0x190;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    eax7 = static_cast<int32_t>(r9_3 + rax6 - 100);
    rdx8 = reinterpret_cast<int32_t>(ecx5) * 0xffffffff92492493 >> 32;
    *reinterpret_cast<int32_t*>(&rdx9) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rdx8) + ecx5) >> 2) - (reinterpret_cast<int32_t>(ecx5) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbp10) = static_cast<int32_t>(rdx9 * 8) - *reinterpret_cast<int32_t*>(&rdx9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdx11) = v4->f1c - ecx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
    if (static_cast<int32_t>(rdx11 + rbp10 + 3) < 0) {
        eax12 = eax7 - 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax12) & 3) && reinterpret_cast<uint32_t>(eax12 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28) {
            __asm__("cdq ");
        }
        *reinterpret_cast<int32_t*>(&rax13) = -1;
    } else {
        edx14 = 0x16d;
        if (!(*reinterpret_cast<unsigned char*>(&eax7) & 3) && (edx14 = 0x16e, reinterpret_cast<uint32_t>(eax7 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28)) {
            __asm__("cdq ");
            edx15 = eax7 % 0x190;
            edx16 = reinterpret_cast<uint32_t>(-edx15);
            edx14 = edx16 - (edx16 + reinterpret_cast<uint1_t>(edx16 < edx16 + reinterpret_cast<uint1_t>(!!edx15))) + 0x16e;
        }
        r10d17 = v4->f1c - edx14;
        edx18 = r10d17 - v4->f18 + 0x17e;
        *reinterpret_cast<uint32_t*>(&r10_19) = r10d17 - edx18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_19) + 4) = 0;
        rax20 = reinterpret_cast<int32_t>(edx18) * 0xffffffff92492493 >> 32;
        *reinterpret_cast<int32_t*>(&rax21) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax20) + edx18) >> 2) - (reinterpret_cast<int32_t>(edx18) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r11_22) = static_cast<int32_t>(rax21 * 8) - *reinterpret_cast<int32_t*>(&rax21);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_22) + 4) = 0;
        eax23 = static_cast<int32_t>(r10_19 + r11_22 + 3);
        if (eax23 >= 0) {
        }
        *reinterpret_cast<int32_t*>(&rax13) = (eax23 >> 31) + 1;
    }
    if (sil == 71) {
        if (r8d24) 
            goto 0x68f9;
        if (v25 == 43) 
            goto 0x6b85;
        goto 0x578d;
    }
    if (sil != 0x67) {
        goto 0x5a20;
    }
    rdx26 = *reinterpret_cast<int32_t*>(&r9_3) * 0x51eb851f >> 37;
    ecx27 = *reinterpret_cast<int32_t*>(&r9_3) - (*reinterpret_cast<int32_t*>(&rdx26) - (*reinterpret_cast<int32_t*>(&r9_3) >> 31)) * 100 + *reinterpret_cast<int32_t*>(&rax13);
    rdx28 = ecx27 * 0x51eb851f >> 37;
    if (ecx27 - (*reinterpret_cast<int32_t*>(&rdx28) - (ecx27 >> 31)) * 100 >= 0) 
        goto "???";
    if (*reinterpret_cast<int32_t*>(&r9_3) < 0xfffff894 - *reinterpret_cast<int32_t*>(&rax13)) 
        goto addr_68af_19;
    if (r8d29) 
        goto 0x68ba;
    if (v30 == 43) 
        goto 0x68c4;
    goto 0x5778;
    addr_68af_19:
    if (!r8d31) 
        goto 0x5769; else 
        goto "???";
}

void fun_6142(int32_t edi) {
    if (edi == 69) 
        goto 0x5348;
    goto 0x5ace;
}

void fun_5cf8() {
    int64_t rbx1;
    int64_t r12_2;
    int32_t r8d3;
    int32_t r13d4;
    uint64_t r15_5;
    int64_t r14_6;
    uint64_t r12_7;
    int32_t r13d8;
    uint64_t rbp9;
    int64_t r15_10;
    int64_t r14_11;
    int32_t r13d12;
    uint64_t r12_13;
    int32_t r8d14;
    uint64_t r13_15;
    int32_t r8d16;
    uint64_t r13_17;
    void** r14_18;
    void** r14_19;
    void** rdi20;
    signed char* rbx21;
    void** r14_22;

    if (rbx1 - 1 != r12_2) {
        goto 0x5348;
    }
    if (r8d3 == 45 || r13d4 < 0) {
        if (r15_5 > 0xfffffffffffffffd) 
            goto 0x5228;
        if (!r14_6) 
            goto addr_69dc_6;
    } else {
        r12_7 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r13d8));
        *reinterpret_cast<int32_t*>(&rbp9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp9) + 4) = 0;
        if (r12_7) {
            rbp9 = r12_7;
        }
        if (rbp9 >= reinterpret_cast<uint64_t>(~r15_10)) 
            goto 0x5228;
        if (!r14_11) 
            goto 0x613a;
        if (r13d12 > 1) {
            r12_13 = r12_7 - 1;
            if (r8d14 == 48 || (*reinterpret_cast<int32_t*>(&r13_15) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_15) + 4) = 0, r8d16 == 43)) {
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_17) + 4) = 0;
                do {
                    ++r13_17;
                    fun_3650(48, r14_18);
                } while (r12_13 != r13_17);
            } else {
                do {
                    ++r13_15;
                    fun_3650(32, r14_19);
                } while (r12_13 != r13_15);
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi20) = *rbx21;
    *reinterpret_cast<int32_t*>(&rdi20 + 4) = 0;
    fun_3650(rdi20, r14_22);
    goto 0x613a;
    addr_69dc_6:
    goto 0x613a;
}

void fun_5e7c(int32_t edi) {
    signed char bpl2;

    if (edi == 69) 
        goto 0x5348;
    if (bpl2) {
    }
    goto 0x54c6;
}

void fun_6035(int32_t edi) {
    int32_t r13d2;
    int32_t r8d3;
    int64_t r13_4;
    int64_t rax5;
    int32_t v6;
    uint32_t ecx7;
    unsigned char v8;
    void** v9;
    int64_t v10;

    if (edi) 
        goto 0x5348;
    if (r13d2 >= 0 || r8d3) {
        if (static_cast<int32_t>(r13_4 - 6) < 0) {
        }
        goto 0x5f2e;
    } else {
        *reinterpret_cast<int32_t*>(&rax5) = v6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        ecx7 = v8;
        __strftime_internal_isra_0(0, "%Y-%m-%d", v9, *reinterpret_cast<unsigned char*>(&ecx7), 43, 4, v10, rax5, __return_address());
        goto 0x5f88;
    }
}

void fun_647b() {
    goto 0x5f2e;
}

void fun_64c7() {
    goto 0x5f2e;
}

struct s132 {
    signed char[112] pad112;
    int64_t f70;
};

struct s133 {
    signed char[104] pad104;
    int64_t f68;
};

struct s134 {
    signed char[152] pad152;
    int32_t f98;
};

struct s135 {
    signed char[144] pad144;
    int64_t f90;
};

struct s136 {
    signed char[128] pad128;
    int64_t f80;
};

struct s137 {
    signed char[136] pad136;
    int64_t f88;
};

struct s138 {
    signed char[120] pad120;
    int64_t f78;
};

struct s139 {
    signed char[152] pad152;
    int32_t f98;
};

struct s140 {
    signed char[144] pad144;
    int64_t f90;
};

struct s141 {
    signed char[128] pad128;
    int64_t f80;
};

struct s142 {
    signed char[136] pad136;
    int64_t f88;
};

struct s143 {
    signed char[120] pad120;
    int64_t f78;
};

struct s144 {
    signed char[104] pad104;
    int64_t f68;
};

struct s145 {
    signed char[112] pad112;
    int64_t f70;
};

struct s146 {
    signed char[161] pad161;
    signed char fa1;
};

void fun_80d2() {
    int64_t rax1;
    int64_t rbx2;
    int64_t rdi3;
    int64_t rbx4;
    int64_t rsi5;
    int64_t rbx6;
    int64_t rdx7;
    int64_t rbx8;
    int64_t rcx9;
    int64_t rbx10;
    int64_t v11;
    int64_t rbx12;
    int64_t r8_13;
    struct s132* r12_14;
    int32_t eax15;
    int64_t rbx16;
    int64_t r8_17;
    struct s133* r12_18;
    int32_t edi19;
    struct s134* r12_20;
    int32_t r8d21;
    int32_t* rbx22;
    int64_t rsi23;
    struct s135* r12_24;
    int64_t rdx25;
    struct s136* r12_26;
    int64_t rcx27;
    struct s137* r12_28;
    int64_t rax29;
    struct s138* r12_30;
    int32_t r8d31;
    int32_t edi32;
    int32_t v33;
    int64_t r8_34;
    int64_t rsi35;
    int64_t v36;
    int64_t r8_37;
    int64_t rcx38;
    int64_t v39;
    int64_t r8_40;
    int64_t v41;
    int64_t v42;
    int64_t r8_43;
    int64_t v44;
    int64_t rax45;
    int64_t v46;
    int64_t v47;
    int64_t v48;
    uint32_t eax49;
    int64_t rdx50;
    uint32_t r8d51;
    uint32_t r8d52;
    int32_t r8d53;
    int32_t v54;
    int64_t r8_55;
    int64_t v56;
    int64_t r8_57;
    int64_t v58;
    int64_t r8_59;
    int64_t v60;
    int64_t r8_61;
    uint32_t eax62;
    int64_t v63;
    int64_t rdx64;
    int64_t v65;
    uint32_t r8d66;
    uint32_t r8d67;
    struct s139* r12_68;
    struct s140* r12_69;
    struct s141* r12_70;
    struct s142* r12_71;
    struct s143* r12_72;
    struct s144* r12_73;
    struct s145* r12_74;
    struct s146* r12_75;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 56);
    rdi3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    rsi5 = *reinterpret_cast<int64_t*>(rbx6 - 40);
    rdx7 = *reinterpret_cast<int64_t*>(rbx8 - 32);
    rcx9 = *reinterpret_cast<int64_t*>(rbx10 - 24);
    v11 = *reinterpret_cast<int64_t*>(rbx12 - 16);
    r8_13 = r12_14->f70;
    eax15 = *reinterpret_cast<int32_t*>(rbx16 - 8);
    r8_17 = r12_18->f68;
    edi19 = r12_20->f98;
    r8d21 = *rbx22;
    rsi23 = r12_24->f90;
    rdx25 = r12_26->f80;
    rcx27 = r12_28->f88;
    rax29 = r12_30->f78;
    if (r8d21 < 0) {
        r8d31 = 0;
        edi32 = edi19 - eax15;
        *reinterpret_cast<unsigned char*>(&r8d31) = __intrinsic();
        v33 = r8d31;
        *reinterpret_cast<int32_t*>(&r8_34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_34) + 4) = 0;
        rsi35 = rsi23 - v11;
        *reinterpret_cast<unsigned char*>(&r8_34) = __intrinsic();
        v36 = r8_34;
        *reinterpret_cast<int32_t*>(&r8_37) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_37) + 4) = 0;
        rcx38 = rcx27 - rcx9;
        *reinterpret_cast<unsigned char*>(&r8_37) = __intrinsic();
        v39 = r8_37;
        *reinterpret_cast<int32_t*>(&r8_40) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_40) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r8_40) = __intrinsic();
        v41 = rdx25 - rdx7;
        v42 = r8_40;
        *reinterpret_cast<int32_t*>(&r8_43) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_43) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r8_43) = __intrinsic();
        v44 = rax29 - rsi5;
        *reinterpret_cast<int32_t*>(&rax45) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax45) + 4) = 0;
        v46 = r8_43;
        *reinterpret_cast<unsigned char*>(&rax45) = __intrinsic();
        v47 = r8_13 - rdi3;
        v48 = rax45;
        eax49 = 0;
        *reinterpret_cast<unsigned char*>(&eax49) = __intrinsic();
        rdx50 = r8_17 - rax1;
        r8d51 = *reinterpret_cast<unsigned char*>(&v36);
        *reinterpret_cast<unsigned char*>(&r8d51) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d51) | *reinterpret_cast<unsigned char*>(&v33)) | *reinterpret_cast<unsigned char*>(&v39)) | *reinterpret_cast<unsigned char*>(&v42)) | *reinterpret_cast<unsigned char*>(&v46)) | *reinterpret_cast<unsigned char*>(&v48));
        r8d52 = r8d51 | eax49;
    } else {
        r8d53 = 0;
        edi32 = edi19 + eax15;
        *reinterpret_cast<unsigned char*>(&r8d53) = __intrinsic();
        v54 = r8d53;
        *reinterpret_cast<int32_t*>(&r8_55) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_55) + 4) = 0;
        rsi35 = rsi23 + v11;
        *reinterpret_cast<unsigned char*>(&r8_55) = __intrinsic();
        v56 = r8_55;
        *reinterpret_cast<int32_t*>(&r8_57) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_57) + 4) = 0;
        rcx38 = rcx27 + rcx9;
        *reinterpret_cast<unsigned char*>(&r8_57) = __intrinsic();
        v58 = r8_57;
        *reinterpret_cast<int32_t*>(&r8_59) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_59) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r8_59) = __intrinsic();
        v41 = rdx25 + rdx7;
        v60 = r8_59;
        *reinterpret_cast<int32_t*>(&r8_61) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_61) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&r8_61) = __intrinsic();
        v44 = rax29 + rsi5;
        eax62 = 0;
        v63 = r8_61;
        *reinterpret_cast<unsigned char*>(&eax62) = __intrinsic();
        v47 = r8_13 + rdi3;
        *reinterpret_cast<int32_t*>(&rdx64) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx64) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx64) = __intrinsic();
        v65 = rdx64;
        rdx50 = r8_17 + rax1;
        r8d66 = *reinterpret_cast<unsigned char*>(&v56);
        *reinterpret_cast<unsigned char*>(&r8d66) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d66) | *reinterpret_cast<unsigned char*>(&v54)) | *reinterpret_cast<unsigned char*>(&v58)) | *reinterpret_cast<unsigned char*>(&v60)) | *reinterpret_cast<unsigned char*>(&v63));
        r8d67 = r8d66 | eax62;
        *reinterpret_cast<unsigned char*>(&r8d52) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r8d67) | *reinterpret_cast<unsigned char*>(&v65));
    }
    r12_68->f98 = edi32;
    r12_69->f90 = rsi35;
    r12_70->f80 = v41;
    r12_71->f88 = rcx38;
    r12_72->f78 = v44;
    r12_73->f68 = rdx50;
    r12_74->f70 = v47;
    if (!*reinterpret_cast<unsigned char*>(&r8d52)) {
        r12_75->fa1 = 1;
        goto 0x7ff0;
    }
}

struct s147 {
    signed char[225] pad225;
    unsigned char fe1;
};

struct s148 {
    signed char[56] pad56;
    void** f38;
};

struct s149 {
    signed char[64] pad64;
    int64_t f40;
};

struct s150 {
    signed char[48] pad48;
    int64_t f30;
};

struct s151 {
    signed char[16] pad16;
    int64_t f10;
};

struct s152 {
    signed char[48] pad48;
    int64_t f30;
};

struct s153 {
    signed char[56] pad56;
    int64_t f38;
};

struct s154 {
    signed char[64] pad64;
    int64_t f40;
};

struct s155 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_895e() {
    uint32_t eax1;
    struct s147* r12_2;
    void** r8_3;
    int64_t rbx4;
    void** v5;
    int64_t rbx6;
    void** rax7;
    void** rcx8;
    void** r9_9;
    struct s148* r12_10;
    struct s149* r12_11;
    int64_t rbx12;
    struct s150* r12_13;
    struct s151* rbx14;
    void** rcx15;
    int64_t rbx16;
    void** rax17;
    void** r9_18;
    struct s152* r12_19;
    int64_t rbx20;
    struct s153* r12_21;
    int64_t rbx22;
    struct s154* r12_23;
    struct s155* rbx24;

    eax1 = r12_2->fe1;
    r8_3 = *reinterpret_cast<void***>(rbx4 - 0xd0);
    if (reinterpret_cast<signed char>(r8_3) <= reinterpret_cast<signed char>(3)) {
        v5 = *reinterpret_cast<void***>(rbx6 - 0xd8);
        if (*reinterpret_cast<signed char*>(&eax1)) {
            rax7 = fun_3580();
            dbg_printf(rax7, v5, 5, rcx8, r8_3, r9_9, __return_address());
        }
        __asm__("movdqu xmm6, [rbx]");
        r12_10->f38 = v5;
        __asm__("movups [r12+0x20], xmm6");
        r12_11->f40 = *reinterpret_cast<int64_t*>(rbx12 - 0x68);
        r12_13->f30 = rbx14->f10;
        goto 0x7ff0;
    } else {
        if (*reinterpret_cast<signed char*>(&eax1)) {
            rcx15 = *reinterpret_cast<void***>(rbx16 - 0xd8);
            rax17 = fun_3580();
            dbg_printf(rax17, rcx15, r8_3, rcx15, r8_3, r9_18, __return_address());
        }
        __asm__("movdqu xmm7, [rbx-0xe0]");
        r12_19->f30 = *reinterpret_cast<int64_t*>(rbx20 - 0xd0);
        __asm__("movups [r12+0x20], xmm7");
        r12_21->f38 = *reinterpret_cast<int64_t*>(rbx22 - 0x68);
        r12_23->f40 = rbx24->f8;
        goto 0x7ff0;
    }
}

void fun_8694() {
    goto 0x7ff0;
}

void fun_87e8() {
    goto 0x7ff0;
}

void fun_89ba() {
    if (__intrinsic()) 
        goto 0x8260;
    goto 0x7ff0;
}

void fun_8a48() {
    goto 0x7ff0;
}

void fun_8bca() {
    goto 0x7ff0;
}

struct s156 {
    signed char[80] pad80;
    int64_t f50;
};

struct s157 {
    signed char[88] pad88;
    int64_t f58;
};

struct s158 {
    signed char[72] pad72;
    int64_t f48;
};

struct s159 {
    signed char[96] pad96;
    int64_t f60;
};

struct s160 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_8d44() {
    struct s156* r12_1;
    int64_t rax2;
    int64_t rbx3;
    struct s157* r12_4;
    struct s158* r12_5;
    struct s159* r12_6;
    struct s160* r12_7;

    r12_1->f50 = 0;
    rax2 = *reinterpret_cast<int64_t*>(rbx3 - 48);
    r12_4->f58 = 0;
    r12_5->f48 = rax2;
    r12_6->f60 = 0;
    r12_7->f1c = 2;
    goto 0x7ff0;
}

void fun_8eef() {
    goto 0x8e89;
}

struct s161 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_9132() {
    struct s161* r12_1;

    r12_1->f18 = 0xffff9d90;
    goto 0x7ff0;
}

struct s162 {
    signed char[64] pad64;
    int64_t f40;
};

struct s163 {
    signed char[56] pad56;
    int64_t f38;
};

void fun_9274() {
    struct s162* r12_1;
    int64_t rbx2;
    struct s163* r12_3;
    int64_t* rbx4;

    r12_1->f40 = *reinterpret_cast<int64_t*>(rbx2 - 48);
    r12_3->f38 = *rbx4;
    goto 0x7ff0;
}

void fun_c51e() {
    goto 0xc192;
}

void fun_c6f4() {
    goto 0xc6ac;
}

void fun_c7bb() {
    goto 0xc2e8;
}

void fun_c80d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0xc790;
    goto 0xc3bf;
}

void fun_c83f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0xc79b;
        goto 0xc1c0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0xc63a;
        goto 0xc3db;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0xcfd8;
    if (r10_8 > r15_9) 
        goto addr_c725_9;
    addr_c72a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0xcfe3;
    goto 0xc314;
    addr_c725_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_c72a_10;
}

void fun_c872() {
    goto 0xc3a7;
}

void fun_cc40() {
    goto 0xc3a7;
}

void fun_d3df() {
    int32_t ebx1;

    if (ebx1) {
        goto 0xc4fc;
    } else {
        goto 0xc660;
    }
}

void fun_ea40() {
}

void fun_10428(int32_t edi) {
    signed char r8b2;

    if (r8b2) {
    }
    if (edi == 69) 
        goto 0x102c0; else 
        goto "???";
}

void fun_10745(int32_t edi) {
    signed char r8b2;

    if (edi) 
        goto 0x102c0;
    if (r8b2) {
    }
    goto 0x10462;
}

struct s164 {
    signed char[20] pad20;
    int32_t f14;
    uint32_t f18;
    int32_t f1c;
};

void fun_1057f(int32_t edi, signed char sil) {
    int64_t rcx3;
    struct s164* v4;
    uint32_t edx5;
    int64_t rax6;
    int32_t r9d7;
    uint64_t rax8;
    int64_t rax9;
    int64_t r8_10;
    int64_t rax11;
    int32_t r9d12;
    int64_t rax13;
    uint32_t eax14;
    uint32_t eax15;
    uint32_t r11d16;
    uint32_t edx17;
    int64_t r11_18;
    uint64_t rax19;
    int64_t rax20;
    int64_t r9_21;
    int32_t eax22;
    int32_t r10d23;
    int32_t v24;
    int64_t rdx25;
    int32_t r8d26;
    int64_t rdx27;
    int32_t r10d28;
    int32_t r10d29;
    int32_t v30;

    if (edi == 69) 
        goto 0x102c0;
    *reinterpret_cast<int32_t*>(&rcx3) = v4->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    edx5 = v4->f1c - v4->f18 + 0x17e;
    *reinterpret_cast<uint32_t*>(&rax6) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rcx3) >> 31) & 0x190;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    r9d7 = static_cast<int32_t>(rcx3 + rax6 - 100);
    rax8 = reinterpret_cast<int32_t>(edx5) * 0xffffffff92492493 >> 32;
    *reinterpret_cast<int32_t*>(&rax9) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax8) + edx5) >> 2) - (reinterpret_cast<int32_t>(edx5) >> 31);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_10) = static_cast<int32_t>(rax9 * 8) - *reinterpret_cast<int32_t*>(&rax9);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax11) = v4->f1c - edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    if (static_cast<int32_t>(rax11 + r8_10 + 3) < 0) {
        r9d12 = r9d7 - 1;
        if (!(*reinterpret_cast<unsigned char*>(&r9d12) & 3) && reinterpret_cast<uint32_t>(r9d12 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28) {
            __asm__("cdq ");
        }
        *reinterpret_cast<int32_t*>(&rax13) = -1;
    } else {
        eax14 = 0x16d;
        if (!(*reinterpret_cast<unsigned char*>(&r9d7) & 3) && (eax14 = 0x16e, reinterpret_cast<uint32_t>(r9d7 * 0xc28f5c29 + 0x51eb850) <= 0x28f5c28)) {
            __asm__("cdq ");
            eax15 = reinterpret_cast<uint32_t>(r9d7 / 0x190);
            eax14 = eax15 - (eax15 + reinterpret_cast<uint1_t>(eax15 < eax15 + reinterpret_cast<uint1_t>(!!(r9d7 % 0x190)))) + 0x16e;
        }
        r11d16 = v4->f1c - eax14;
        edx17 = r11d16 - v4->f18 + 0x17e;
        *reinterpret_cast<uint32_t*>(&r11_18) = r11d16 - edx17;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_18) + 4) = 0;
        rax19 = reinterpret_cast<int32_t>(edx17) * 0xffffffff92492493 >> 32;
        *reinterpret_cast<int32_t*>(&rax20) = (reinterpret_cast<int32_t>(*reinterpret_cast<int32_t*>(&rax19) + edx17) >> 2) - (reinterpret_cast<int32_t>(edx17) >> 31);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax20) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r9_21) = static_cast<int32_t>(rax20 * 8) - *reinterpret_cast<int32_t*>(&rax20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_21) + 4) = 0;
        eax22 = static_cast<int32_t>(r11_18 + r9_21 + 3);
        if (eax22 >= 0) {
        }
        *reinterpret_cast<int32_t*>(&rax13) = (eax22 >> 31) + 1;
    }
    if (sil == 71) {
        if (r10d23) 
            goto 0x117f8;
        if (v24 == 43) 
            goto 0x11a94;
        goto 0x10703;
    } else {
        if (sil != 0x67) {
            goto 0x10950;
        } else {
            rdx25 = *reinterpret_cast<int32_t*>(&rcx3) * 0x51eb851f >> 37;
            r8d26 = *reinterpret_cast<int32_t*>(&rcx3) - (*reinterpret_cast<int32_t*>(&rdx25) - (*reinterpret_cast<int32_t*>(&rcx3) >> 31)) * 100 + *reinterpret_cast<int32_t*>(&rax13);
            rdx27 = r8d26 * 0x51eb851f >> 37;
            if (r8d26 - (*reinterpret_cast<int32_t*>(&rdx27) - (r8d26 >> 31)) * 100 < 0) {
                if (*reinterpret_cast<int32_t*>(&rcx3) >= 0xfffff894 - *reinterpret_cast<int32_t*>(&rax13)) 
                    goto 0x11b9d;
                if (!r10d28) 
                    goto 0x11248; else 
                    goto "???";
            } else {
                if (r10d29) 
                    goto 0x117b5;
                if (v30 == 43) 
                    goto 0x117bf; else 
                    goto "???";
            }
        }
    }
}

void fun_10c1e() {
    int64_t rbx1;
    int64_t rbp2;
    uint64_t rax3;
    int64_t v4;
    uint64_t r13_5;
    int32_t r10d6;
    int32_t r15d7;
    int64_t r14_8;
    uint64_t rdx9;
    int32_t r15d10;
    uint64_t rcx11;
    int64_t r14_12;
    int32_t r15d13;
    uint64_t r15_14;
    int64_t r14_15;
    int32_t r10d16;
    int32_t r10d17;
    uint64_t r14_18;
    void** r14_19;
    void** r14_20;
    uint32_t eax21;
    unsigned char* rbx22;

    if (rbx1 - 1 != rbp2) {
        goto 0x102c0;
    }
    rax3 = v4 - r13_5;
    if (r10d6 == 45 || r15d7 < 0) {
        if (rax3 <= 1) 
            goto 0x101a0;
        if (!r14_8) 
            goto addr_1195f_6;
    } else {
        rdx9 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r15d10));
        *reinterpret_cast<int32_t*>(&rcx11) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
        if (rdx9) {
            rcx11 = rdx9;
        }
        if (rcx11 >= rax3) 
            goto 0x101a0;
        if (!r14_12) 
            goto 0x10ca8;
        if (r15d13 > 1) {
            r15_14 = r14_15 + (rdx9 - 1);
            if (r10d16 == 48 || r10d17 == 43) {
                r14_18 = r15_14;
                fun_3640(r14_19, r14_19);
            } else {
                r14_18 = r15_14;
                fun_3640(r14_20, r14_20);
            }
        }
    }
    eax21 = *rbx22;
    *reinterpret_cast<signed char*>(r14_18 + 1 - 1) = *reinterpret_cast<signed char*>(&eax21);
    goto 0x10ca8;
    addr_1195f_6:
    goto 0x10ca8;
}

void fun_10cb0(int32_t edi) {
    signed char r8b2;

    if (edi == 69) 
        goto 0x102c0;
    if (r8b2) {
    }
    goto 0x10443;
}

void fun_10e8a(int32_t edi, int64_t rsi) {
    int32_t r15d3;
    int32_t r10d4;
    int64_t r15_5;
    uint32_t r8d6;
    unsigned char v7;
    int64_t rax8;
    int32_t v9;
    void** v10;
    int64_t v11;
    int64_t rax12;

    if (edi) 
        goto 0x102c0;
    if (r15d3 >= 0 || r10d4) {
        if (static_cast<int32_t>(r15_5 - 6) >= 0) {
        }
        goto 0x10d5f;
    } else {
        r8d6 = v7;
        *reinterpret_cast<int32_t*>(&rax8) = v9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        __strftime_internal_isra_0(0, 0xffffffffffffffff, "%Y-%m-%d", v10, *reinterpret_cast<unsigned char*>(&r8d6), 43, 4, v11, rax8, rax12, __return_address());
        goto 0x10ddd;
    }
}

void fun_10fab(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
}

void fun_11083(int32_t edi) {
    if (edi == 69) 
        goto 0x102c0;
    goto 0x10a03;
}

struct s165 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_11200(int32_t edi) {
    int64_t rdx2;
    struct s165* v3;
    int64_t rax4;
    int64_t rdx5;
    int32_t r10d6;

    if (edi == 69) 
        goto 0x10721;
    rdx2 = v3->f14;
    rax4 = rdx2;
    rdx5 = rdx2 * 0x51eb851f >> 37;
    if (*reinterpret_cast<int32_t*>(&rax4) - (*reinterpret_cast<int32_t*>(&rdx5) - (*reinterpret_cast<int32_t*>(&rax4) >> 31)) * 100 < 0 && *reinterpret_cast<int32_t*>(&rax4) <= 0xfffff893) {
    }
    if (r10d6) 
        goto 0x117b5; else 
        goto "???";
}

void fun_113d1() {
    int1_t zf1;
    signed char r8b2;

    zf1 = r8b2 == 0;
    if (!zf1) {
    }
    if (!zf1) {
    }
    goto 0x10443;
}

void fun_11409() {
    goto 0x113d3;
}

struct s166 {
    signed char[1] pad1;
    signed char f1;
};

void fun_11fb0() {
    signed char* r13_1;
    struct s166* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_126fe() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x12677;
}

struct s167 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s168 {
    signed char[8] pad8;
    int64_t f8;
};

struct s169 {
    signed char[16] pad16;
    int64_t f10;
};

struct s170 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_12e30() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s167* rcx3;
    struct s168* rcx4;
    int64_t r11_5;
    struct s169* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s170* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x12d36;
}

void fun_130e5() {
    goto 0x130b8;
}

void fun_13a34() {
    goto 0x13358;
}

void fun_13348() {
}

void fun_13778() {
    goto 0x13358;
}

void fun_3b0c() {
    goto 0x3ab8;
}

void fun_54b8(int32_t edi) {
    if (edi == 79) 
        goto 0x5348; else 
        goto "???";
}

void fun_6491() {
}

struct s171 {
    signed char[152] pad152;
    int32_t f98;
};

struct s172 {
    signed char[48] pad48;
    int32_t f30;
};

struct s173 {
    signed char[152] pad152;
    int32_t f98;
};

struct s174 {
    signed char[144] pad144;
    int64_t f90;
};

struct s175 {
    signed char[144] pad144;
    int64_t f90;
};

struct s176 {
    signed char[40] pad40;
    int64_t f28;
};

struct s177 {
    signed char[136] pad136;
    int64_t f88;
};

struct s178 {
    signed char[136] pad136;
    int64_t f88;
};

struct s179 {
    signed char[32] pad32;
    int64_t f20;
};

struct s180 {
    signed char[128] pad128;
    int64_t f80;
};

struct s181 {
    signed char[128] pad128;
    int64_t f80;
};

struct s182 {
    signed char[24] pad24;
    int64_t f18;
};

struct s183 {
    signed char[120] pad120;
    int64_t f78;
};

struct s184 {
    signed char[120] pad120;
    int64_t f78;
};

struct s185 {
    signed char[16] pad16;
    int64_t f10;
};

struct s186 {
    signed char[112] pad112;
    int64_t f70;
};

struct s187 {
    signed char[112] pad112;
    int64_t f70;
};

struct s188 {
    signed char[8] pad8;
    int64_t f8;
};

struct s189 {
    signed char[104] pad104;
    int64_t f68;
};

struct s190 {
    signed char[104] pad104;
    int64_t f68;
};

void fun_86d2() {
    uint32_t r8d1;
    struct s171* r12_2;
    struct s172* rbx3;
    struct s173* r12_4;
    uint32_t edi5;
    struct s174* r12_6;
    struct s175* r12_7;
    struct s176* rbx8;
    struct s177* r12_9;
    struct s178* r12_10;
    struct s179* rbx11;
    uint32_t esi12;
    struct s180* r12_13;
    struct s181* r12_14;
    struct s182* rbx15;
    int64_t rcx16;
    struct s183* r12_17;
    struct s184* r12_18;
    struct s185* rbx19;
    int64_t rdx20;
    struct s186* r12_21;
    int64_t v22;
    int64_t rcx23;
    struct s187* r12_24;
    struct s188* rbx25;
    int64_t v26;
    int64_t rcx27;
    struct s189* r12_28;
    struct s190* r12_29;
    int64_t* rbx30;
    uint32_t eax31;

    r8d1 = 0;
    r12_2->f98 = rbx3->f30 + r12_4->f98;
    *reinterpret_cast<unsigned char*>(&r8d1) = __intrinsic();
    edi5 = 0;
    r12_6->f90 = r12_7->f90 + rbx8->f28;
    *reinterpret_cast<unsigned char*>(&edi5) = __intrinsic();
    r12_9->f88 = r12_10->f88 + rbx11->f20;
    esi12 = 0;
    *reinterpret_cast<unsigned char*>(&esi12) = __intrinsic();
    r12_13->f80 = r12_14->f80 + rbx15->f18;
    *reinterpret_cast<int32_t*>(&rcx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx16) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx16) = __intrinsic();
    r12_17->f78 = r12_18->f78 + rbx19->f10;
    rdx20 = r12_21->f70;
    v22 = rcx16;
    *reinterpret_cast<int32_t*>(&rcx23) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx23) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx23) = __intrinsic();
    r12_24->f70 = rdx20 + rbx25->f8;
    v26 = rcx23;
    *reinterpret_cast<int32_t*>(&rcx27) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx27) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx27) = __intrinsic();
    r12_28->f68 = r12_29->f68 + *rbx30;
    eax31 = static_cast<uint32_t>(static_cast<unsigned char>(__intrinsic())) | r8d1 | edi5 | esi12;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax31) | *reinterpret_cast<unsigned char*>(&v22)) | *reinterpret_cast<unsigned char*>(&v26)) 
        goto 0x8260;
    if (rcx27) 
        goto 0x8260; else 
        goto "???";
}

void fun_8825() {
    goto 0x7ff0;
}

void fun_888a() {
}

void fun_89c3() {
    goto 0x8698;
}

void fun_884d() {
}

void fun_8a85() {
    goto 0x7ff0;
}

struct s191 {
    signed char[208] pad208;
    int64_t fd0;
};

struct s192 {
    signed char[208] pad208;
    int64_t fd0;
};

void fun_8bd6() {
    struct s191* r12_1;
    struct s192* r12_2;

    r12_1->fd0 = r12_2->fd0 + 1;
    goto 0x89f9;
}

struct s193 {
    signed char[160] pad160;
    signed char fa0;
};

void fun_8d76() {
    struct s193* r12_1;

    __asm__("movdqu xmm6, [rbx]");
    r12_1->fa0 = 1;
    __asm__("movups [r12+0x58], xmm6");
    goto 0x89f9;
}

struct s194 {
    signed char[176] pad176;
    int64_t fb0;
};

struct s195 {
    signed char[176] pad176;
    int64_t fb0;
};

void fun_8f0f() {
    struct s194* r12_1;
    struct s195* r12_2;

    r12_1->fb0 = r12_2->fb0 + 1;
    goto 0x89f9;
}

struct s196 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_9140() {
    int64_t rax1;
    int64_t* rbx2;
    struct s196* r12_3;

    rax1 = *rbx2;
    r12_3->f18 = *reinterpret_cast<int32_t*>(&rax1);
    goto 0x7ff0;
}

struct s197 {
    signed char[56] pad56;
    int64_t f38;
};

struct s198 {
    signed char[64] pad64;
    int64_t f40;
};

void fun_928a() {
    struct s197* r12_1;
    int64_t rbx2;
    struct s198* r12_3;
    int64_t rbx4;

    __asm__("movdqu xmm7, [rbx]");
    r12_1->f38 = *reinterpret_cast<int64_t*>(rbx2 - 0xa8);
    __asm__("movups [r12+0x20], xmm7");
    r12_3->f40 = *reinterpret_cast<int64_t*>(rbx4 - 0x68);
    goto 0x923c;
}

void fun_c87c() {
    goto 0xc817;
}

void fun_cc4a() {
    goto 0xc76d;
}

void fun_eaa0() {
    fun_3580();
    goto fun_3840;
}

void fun_11410() {
    goto 0x10d5f;
}

void fun_12490() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x12246;
    goto 0x123f0;
}

void fun_1270c() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x12677;
}

struct s199 {
    int32_t f0;
    int32_t f4;
};

struct s200 {
    int32_t f0;
    int32_t f4;
};

struct s201 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s202 {
    signed char[8] pad8;
    int64_t f8;
};

struct s203 {
    signed char[8] pad8;
    int64_t f8;
};

struct s204 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_12de0(struct s199* rdi, struct s200* rsi) {
    struct s201* rcx3;
    struct s202* rcx4;
    struct s203* rcx5;
    struct s204* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x12d36;
}

void fun_130ef() {
    goto 0x130b8;
}

void fun_13a3e() {
    goto 0x13358;
}

void fun_3b1c() {
    goto 0x3ab8;
}

void fun_8837() {
}

struct s205 {
    signed char[208] pad208;
    int64_t fd0;
};

struct s206 {
    signed char[208] pad208;
    int64_t fd0;
};

struct s207 {
    signed char[168] pad168;
    int64_t fa8;
};

struct s208 {
    signed char[168] pad168;
    int64_t fa8;
};

void fun_89cc() {
    struct s205* r12_1;
    struct s206* r12_2;
    struct s207* r12_3;
    struct s208* r12_4;

    r12_1->fd0 = r12_2->fd0 + 1;
    r12_3->fa8 = r12_4->fa8 + 1;
}

struct s209 {
    signed char[152] pad152;
    int32_t f98;
};

struct s210 {
    signed char[48] pad48;
    int32_t f30;
};

struct s211 {
    signed char[152] pad152;
    int32_t f98;
};

struct s212 {
    signed char[144] pad144;
    int64_t f90;
};

struct s213 {
    signed char[144] pad144;
    int64_t f90;
};

struct s214 {
    signed char[40] pad40;
    int64_t f28;
};

struct s215 {
    signed char[136] pad136;
    int64_t f88;
};

struct s216 {
    signed char[136] pad136;
    int64_t f88;
};

struct s217 {
    signed char[32] pad32;
    int64_t f20;
};

struct s218 {
    signed char[128] pad128;
    int64_t f80;
};

struct s219 {
    signed char[128] pad128;
    int64_t f80;
};

struct s220 {
    signed char[24] pad24;
    int64_t f18;
};

struct s221 {
    signed char[120] pad120;
    int64_t f78;
};

struct s222 {
    signed char[120] pad120;
    int64_t f78;
};

struct s223 {
    signed char[16] pad16;
    int64_t f10;
};

struct s224 {
    signed char[112] pad112;
    int64_t f70;
};

struct s225 {
    signed char[112] pad112;
    int64_t f70;
};

struct s226 {
    signed char[8] pad8;
    int64_t f8;
};

struct s227 {
    signed char[104] pad104;
    int64_t f68;
};

struct s228 {
    signed char[104] pad104;
    int64_t f68;
};

struct s229 {
    signed char[161] pad161;
    signed char fa1;
};

void fun_8ac2() {
    struct s18* r12_1;
    uint32_t r8d2;
    struct s209* r12_3;
    struct s210* rbx4;
    struct s211* r12_5;
    uint32_t edi6;
    struct s212* r12_7;
    struct s213* r12_8;
    struct s214* rbx9;
    struct s215* r12_10;
    struct s216* r12_11;
    struct s217* rbx12;
    uint32_t esi13;
    struct s218* r12_14;
    struct s219* r12_15;
    struct s220* rbx16;
    uint32_t r10d17;
    struct s221* r12_18;
    struct s222* r12_19;
    struct s223* rbx20;
    struct s224* r12_21;
    struct s225* r12_22;
    struct s226* rbx23;
    int64_t rcx24;
    struct s227* r12_25;
    struct s228* r12_26;
    int64_t* rbx27;
    uint32_t eax28;
    struct s229* r12_29;

    digits_to_date_time(r12_1);
    r8d2 = 0;
    r12_3->f98 = rbx4->f30 + r12_5->f98;
    *reinterpret_cast<unsigned char*>(&r8d2) = __intrinsic();
    edi6 = 0;
    r12_7->f90 = r12_8->f90 + rbx9->f28;
    *reinterpret_cast<unsigned char*>(&edi6) = __intrinsic();
    r12_10->f88 = r12_11->f88 + rbx12->f20;
    esi13 = 0;
    *reinterpret_cast<unsigned char*>(&esi13) = __intrinsic();
    r12_14->f80 = r12_15->f80 + rbx16->f18;
    r10d17 = 0;
    *reinterpret_cast<unsigned char*>(&r10d17) = __intrinsic();
    r12_18->f78 = r12_19->f78 + rbx20->f10;
    r12_21->f70 = r12_22->f70 + rbx23->f8;
    *reinterpret_cast<int32_t*>(&rcx24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx24) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rcx24) = __intrinsic();
    r12_25->f68 = r12_26->f68 + *rbx27;
    eax28 = static_cast<uint32_t>(static_cast<unsigned char>(__intrinsic())) | r8d2 | edi6 | esi13 | r10d17;
    if (*reinterpret_cast<unsigned char*>(&eax28) | static_cast<unsigned char>(__intrinsic())) 
        goto 0x8260;
    if (rcx24) 
        goto 0x8260;
    r12_29->fa1 = 1;
    goto 0x7ff0;
}

struct s230 {
    signed char[28] pad28;
    int32_t f1c;
};

struct s231 {
    signed char[88] pad88;
    int64_t f58;
};

struct s232 {
    signed char[72] pad72;
    int64_t f48;
};

struct s233 {
    signed char[80] pad80;
    int64_t f50;
};

struct s234 {
    signed char[96] pad96;
    int64_t f60;
};

void fun_8bff() {
    int64_t rax1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    struct s230* r12_5;
    int64_t rcx6;
    int64_t rbx7;
    struct s231* r12_8;
    int64_t rax9;
    int64_t rbx10;
    struct s232* r12_11;
    struct s233* r12_12;
    struct s234* r12_13;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 56);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 0xa0);
    r12_5->f1c = 2;
    rcx6 = *reinterpret_cast<int64_t*>(rbx7 - 0x110);
    r12_8->f58 = rax1;
    rax9 = *reinterpret_cast<int32_t*>(rbx10 - 48);
    r12_11->f48 = rcx6;
    r12_12->f50 = rdx3;
    r12_13->f60 = rax9;
    goto 0x7ff0;
}

struct s235 {
    signed char[168] pad168;
    int64_t fa8;
};

struct s236 {
    signed char[168] pad168;
    int64_t fa8;
};

void fun_8da9() {
    struct s235* r12_1;
    struct s236* r12_2;

    r12_1->fa8 = r12_2->fa8 + 1;
    goto 0x89f9;
}

struct s237 {
    signed char[232] pad232;
    signed char fe8;
};

struct s238 {
    signed char[8] pad8;
    int64_t f8;
};

struct s239 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8f38() {
    struct s237* r12_1;
    struct s238* r12_2;
    int64_t rbx3;
    int64_t rax4;
    int64_t* rbx5;
    struct s239* r12_6;

    r12_1->fe8 = 1;
    r12_2->f8 = *reinterpret_cast<int64_t*>(rbx3 - 48);
    rax4 = *rbx5;
    r12_6->f10 = *reinterpret_cast<int32_t*>(&rax4);
    goto 0x7ff0;
}

struct s240 {
    signed char[200] pad200;
    int64_t fc8;
};

struct s241 {
    signed char[200] pad200;
    int64_t fc8;
};

struct s242 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_914d() {
    struct s240* r12_1;
    struct s241* r12_2;
    struct s242* r12_3;

    r12_1->fc8 = r12_2->fc8 + 1;
    r12_3->f14 = 1;
    goto 0x7ff0;
}

struct s243 {
    signed char[56] pad56;
    int64_t f38;
};

struct s244 {
    signed char[64] pad64;
    int64_t f40;
};

struct s245 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_92ab() {
    struct s243* r12_1;
    int64_t rbx2;
    struct s244* r12_3;
    struct s245* rbx4;

    r12_1->f38 = *reinterpret_cast<int64_t*>(rbx2 - 56);
    r12_3->f40 = rbx4->f8;
    goto 0x7ff0;
}

void fun_c54d() {
    goto 0xc192;
}

void fun_c888() {
    goto 0xc817;
}

void fun_cc57() {
    goto 0xc7be;
}

void fun_eae0() {
    fun_3580();
    goto fun_3840;
}

void fun_1271b() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x12677;
}

void fun_130f9() {
    goto 0x130b8;
}

void fun_3b2d() {
    int64_t rsi1;
    void** r9_2;

    rsi1 = optarg;
    if (rsi1) {
        r9_2 = argmatch_die;
        __xargmatch_internal("--iso-8601", rsi1, 0x1b1e0, 0x158f0, 4, r9_2);
    }
    goto 0x3aaf;
}

struct s246 {
    signed char[88] pad88;
    int64_t f58;
};

struct s247 {
    signed char[96] pad96;
    int64_t f60;
};

struct s248 {
    signed char[72] pad72;
    int64_t f48;
};

struct s249 {
    signed char[80] pad80;
    int64_t f50;
};

struct s250 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_8c37() {
    int64_t rax1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    struct s246* r12_5;
    struct s247* r12_6;
    struct s248* r12_7;
    struct s249* r12_8;
    struct s250* r12_9;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 48);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 0xa0);
    r12_5->f58 = 0;
    r12_6->f60 = 0;
    r12_7->f48 = rdx3;
    r12_8->f50 = rax1;
    r12_9->f1c = 2;
    goto 0x7ff0;
}

struct s251 {
    signed char[88] pad88;
    int64_t f58;
};

struct s252 {
    signed char[72] pad72;
    int64_t f48;
};

struct s253 {
    signed char[96] pad96;
    int64_t f60;
};

struct s254 {
    signed char[80] pad80;
    int64_t f50;
};

struct s255 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_8dd2() {
    int64_t rdx1;
    int64_t rbx2;
    int64_t rcx3;
    int64_t rbx4;
    struct s251* r12_5;
    int64_t rbx6;
    int64_t rax7;
    int64_t rbx8;
    struct s252* r12_9;
    struct s253* r12_10;
    int64_t rax11;
    int64_t* rbx12;
    struct s254* r12_13;
    struct s255* r12_14;

    rdx1 = *reinterpret_cast<int64_t*>(rbx2 - 0xa0);
    rcx3 = *reinterpret_cast<int64_t*>(rbx4 - 0x110);
    r12_5->f58 = *reinterpret_cast<int64_t*>(rbx6 - 56);
    rax7 = *reinterpret_cast<int32_t*>(rbx8 - 48);
    r12_9->f48 = rcx3;
    r12_10->f60 = rax7;
    rax11 = *rbx12;
    r12_13->f50 = rdx1;
    r12_14->f1c = *reinterpret_cast<int32_t*>(&rax11);
    goto 0x7ff0;
}

struct s256 {
    signed char[232] pad232;
    signed char fe8;
};

struct s257 {
    signed char[8] pad8;
    int64_t f8;
};

struct s258 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8f57() {
    struct s256* r12_1;
    struct s257* r12_2;
    int64_t rbx3;
    int64_t rax4;
    int64_t* rbx5;
    struct s258* r12_6;

    r12_1->fe8 = 1;
    r12_2->f8 = *reinterpret_cast<int64_t*>(rbx3 - 56);
    rax4 = *rbx5;
    r12_6->f10 = *reinterpret_cast<int32_t*>(&rax4);
    goto 0x7ff0;
}

struct s259 {
    signed char[20] pad20;
    int32_t f14;
};

void fun_9164() {
    int64_t rax1;
    int64_t* rbx2;
    struct s259* r12_3;

    rax1 = *rbx2;
    r12_3->f14 = *reinterpret_cast<int32_t*>(&rax1);
    goto 0x7ff0;
}

void fun_92c2() {
    goto 0x7fc5;
}

void fun_c57a() {
    goto 0xc192;
}

void fun_c894() {
    goto 0xc790;
}

void fun_eb20() {
    fun_3580();
    goto fun_3840;
}

void fun_13103() {
    goto 0x130b8;
}

void fun_8c6c() {
    struct s18* r12_1;

    digits_to_date_time(r12_1);
    goto 0x7ff0;
}

struct s260 {
    signed char[88] pad88;
    int64_t f58;
};

struct s261 {
    signed char[96] pad96;
    int64_t f60;
};

struct s262 {
    signed char[80] pad80;
    int64_t f50;
};

struct s263 {
    signed char[72] pad72;
    int64_t f48;
};

struct s264 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_8e09() {
    int64_t rax1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    struct s260* r12_5;
    struct s261* r12_6;
    struct s262* r12_7;
    int64_t rax8;
    int64_t* rbx9;
    struct s263* r12_10;
    struct s264* r12_11;

    rax1 = *reinterpret_cast<int64_t*>(rbx2 - 48);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 0xa0);
    r12_5->f58 = 0;
    r12_6->f60 = 0;
    r12_7->f50 = rax1;
    rax8 = *rbx9;
    r12_10->f48 = rdx3;
    r12_11->f1c = *reinterpret_cast<int32_t*>(&rax8);
    goto 0x7ff0;
}

struct s265 {
    signed char[8] pad8;
    int64_t f8;
};

struct s266 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8f76() {
    struct s265* r12_1;
    int64_t rax2;
    int64_t rbx3;
    struct s266* r12_4;

    r12_1->f8 = 0;
    rax2 = *reinterpret_cast<int64_t*>(rbx3 - 56);
    r12_4->f10 = *reinterpret_cast<int32_t*>(&rax2);
    goto 0x7ff0;
}

struct s267 {
    signed char[216] pad216;
    int64_t fd8;
};

struct s268 {
    signed char[216] pad216;
    int64_t fd8;
};

void fun_9171() {
    int64_t rcx1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    struct s267* r12_5;
    struct s268* r12_6;
    uint32_t esi7;
    int64_t rbx8;
    int64_t r8_9;
    int64_t* rbx10;
    struct s19* r12_11;
    signed char al12;

    rcx1 = *reinterpret_cast<int64_t*>(rbx2 - 40);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    r12_5->fd8 = r12_6->fd8 + 1;
    esi7 = *reinterpret_cast<unsigned char*>(rbx8 - 56);
    r8_9 = *rbx10;
    al12 = time_zone_hhmm_isra_0(r12_11, *reinterpret_cast<signed char*>(&esi7), rdx3, rcx1, r8_9);
    if (al12) 
        goto 0x7ff0;
    goto 0x8260;
}

void fun_92d8() {
    goto 0x7ff0;
}

void fun_c59c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0xcf30;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0xc461;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0xc461;
    }
    if (v11) 
        goto 0xd293;
    if (r10_12 > r15_13) 
        goto addr_d2e3_8;
    addr_d2e8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0xd021;
    addr_d2e3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_d2e8_9;
}

void fun_eb70() {
    void** rbp1;
    int64_t v2;

    fun_3580();
    fun_3840(rbp1, rbp1);
    goto v2;
}

void fun_8c9e() {
    goto 0x87ec;
}

struct s269 {
    signed char[80] pad80;
    int64_t f50;
};

struct s270 {
    signed char[88] pad88;
    int64_t f58;
};

struct s271 {
    signed char[72] pad72;
    int64_t f48;
};

struct s272 {
    signed char[96] pad96;
    int64_t f60;
};

struct s273 {
    signed char[28] pad28;
    int32_t f1c;
};

void fun_8e3d() {
    struct s269* r12_1;
    int64_t rax2;
    int64_t rbx3;
    struct s270* r12_4;
    struct s271* r12_5;
    int64_t rax6;
    int64_t* rbx7;
    struct s272* r12_8;
    struct s273* r12_9;

    r12_1->f50 = 0;
    rax2 = *reinterpret_cast<int64_t*>(rbx3 - 48);
    r12_4->f58 = 0;
    r12_5->f48 = rax2;
    rax6 = *rbx7;
    r12_8->f60 = 0;
    r12_9->f1c = *reinterpret_cast<int32_t*>(&rax6);
    goto 0x7ff0;
}

struct s274 {
    signed char[8] pad8;
    int64_t f8;
};

struct s275 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8f8d() {
    struct s274* r12_1;
    int64_t rax2;
    int64_t* rbx3;
    struct s275* r12_4;

    r12_1->f8 = 0;
    rax2 = *rbx3;
    r12_4->f10 = *reinterpret_cast<int32_t*>(&rax2);
    goto 0x7ff0;
}

void fun_9315() {
    goto 0x7ff0;
}

void fun_ebc8() {
    fun_3580();
    goto 0xeb99;
}

struct s276 {
    signed char[216] pad216;
    int64_t fd8;
};

struct s277 {
    signed char[216] pad216;
    int64_t fd8;
};

void fun_8ca7() {
    struct s276* r12_1;
    struct s277* r12_2;

    r12_1->fd8 = r12_2->fd8 + 1;
    goto 0x89f9;
}

void fun_8e6e() {
}

struct s278 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_8fa3() {
    struct s278* r12_1;
    int64_t rbx2;

    r12_1->f18 = *reinterpret_cast<int32_t*>(rbx2 - 56) + reinterpret_cast<int32_t>("ko");
    goto 0x7ff0;
}

struct s279 {
    signed char[56] pad56;
    int64_t f38;
};

struct s280 {
    signed char[64] pad64;
    int64_t f40;
};

struct s281 {
    signed char[8] pad8;
    int64_t f8;
};

struct s282 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_931e() {
    struct s279* r12_1;
    int64_t rbx2;
    int64_t rax3;
    int64_t rbx4;
    struct s280* r12_5;
    int64_t rax6;
    struct s281* rbx7;
    struct s282* r12_8;

    r12_1->f38 = *reinterpret_cast<int64_t*>(rbx2 - 0x70);
    rax3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    r12_5->f40 = -rax3;
    if (rax3 == 0x8000000000000000) 
        goto 0x8260;
    rax6 = rbx7->f8;
    r12_8->f28 = -rax6;
    if (rax6 != 0x8000000000000000) 
        goto 0x923c;
    goto 0x8260;
}

void fun_ec00() {
    void** rbp1;
    int64_t v2;

    fun_3580();
    fun_3840(rbp1, rbp1);
    goto v2;
}

struct s283 {
    signed char[184] pad184;
    int64_t fb8;
};

struct s284 {
    signed char[184] pad184;
    int64_t fb8;
};

struct s285 {
    signed char[225] pad225;
    signed char fe1;
};

void fun_8cd0() {
    struct s283* r12_1;
    struct s284* r12_2;
    struct s285* r12_3;
    void** r12_4;
    int64_t rdx5;
    void** rcx6;
    void** r8_7;
    void** r9_8;

    r12_1->fb8 = r12_2->fb8 + 1;
    if (!r12_3->fe1) 
        goto 0x7ff0;
    debug_print_current_time_part_0("J", r12_4, rdx5, rcx6, r8_7, r9_8, __return_address());
    goto 0x7ff0;
}

struct s286 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_8fb5() {
    struct s286* r12_1;
    int32_t* rbx2;

    r12_1->f18 = *rbx2 + reinterpret_cast<int32_t>("ko");
    goto 0x7ff0;
}

void fun_ec78() {
    fun_3580();
    goto 0xec3b;
}

struct s287 {
    signed char[24] pad24;
    int32_t f18;
};

struct s288 {
    signed char[24] pad24;
    int32_t f18;
};

void fun_8fc6() {
    int64_t rcx1;
    int64_t rbx2;
    int64_t rdx3;
    int64_t rbx4;
    uint32_t esi5;
    int64_t rbx6;
    int64_t r8_7;
    int64_t* rbx8;
    struct s19* r12_9;
    signed char al10;
    int32_t edx11;
    int64_t rax12;
    struct s287* r12_13;
    int64_t rbx14;
    struct s288* r12_15;

    rcx1 = *reinterpret_cast<int64_t*>(rbx2 - 40);
    rdx3 = *reinterpret_cast<int64_t*>(rbx4 - 48);
    esi5 = *reinterpret_cast<unsigned char*>(rbx6 - 56);
    r8_7 = *rbx8;
    al10 = time_zone_hhmm_isra_0(r12_9, *reinterpret_cast<signed char*>(&esi5), rdx3, rcx1, r8_7);
    if (!al10) 
        goto 0x8260;
    edx11 = 0;
    rax12 = r12_13->f18 + *reinterpret_cast<int64_t*>(rbx14 - 0x70);
    *reinterpret_cast<unsigned char*>(&edx11) = __intrinsic();
    r12_15->f18 = *reinterpret_cast<int32_t*>(&rax12);
    if (rax12 != static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax12))) {
        edx11 = 1;
    }
    if (!edx11) 
        goto 0x7ff0;
    goto 0x8260;
}
