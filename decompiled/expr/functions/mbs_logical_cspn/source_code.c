mbs_logical_cspn (char const *s, char const *accept)
{
  size_t idx = 0;

  if (accept[0] == '\0')
    return 0;

  /* General case.  */
  if (MB_CUR_MAX > 1)
    {
      mbui_iterator_t iter;

      for (mbui_init (iter, s); mbui_avail (iter); mbui_advance (iter))
        {
          ++idx;
          if (mb_len (mbui_cur (iter)) == 1)
            {
              if (mbschr (accept, *mbui_cur_ptr (iter)))
                return idx;
            }
          else
            {
              mbui_iterator_t aiter;

              for (mbui_init (aiter, accept);
                   mbui_avail (aiter);
                   mbui_advance (aiter))
                if (mb_equal (mbui_cur (aiter), mbui_cur (iter)))
                  return idx;
            }
        }

      /* not found */
      return 0;
    }
  else
    {
      /* single-byte locale,
         convert returned byte offset to 1-based index or zero if not found. */
      size_t i = strcspn (s, accept);
      return (s[i] ? i + 1 : 0);
    }
}