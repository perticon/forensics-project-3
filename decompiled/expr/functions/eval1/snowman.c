void** eval1(uint32_t edi, void** rsi, void** rdx) {
    uint32_t ebx4;
    void** rax5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    uint32_t edi9;
    int64_t rax10;
    uint1_t zf11;
    uint32_t eax12;
    uint32_t eax13;
    void** rax14;
    uint32_t eax15;
    uint32_t eax16;
    void** rdi17;
    void** rdi18;
    void** rax19;
    void** rdi20;

    ebx4 = edi;
    rax5 = eval2(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&edi)), rsi, rdx);
    rdx6 = args;
    rbp7 = rax5;
    rax8 = *reinterpret_cast<void***>(rdx6);
    if (rax8) {
        do {
            edi9 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax8) - 38);
            if (!edi9) {
                edi9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax8 + 1));
            }
            *reinterpret_cast<int32_t*>(&rax10) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            zf11 = reinterpret_cast<uint1_t>(edi9 == 0);
            *reinterpret_cast<unsigned char*>(&rax10) = zf11;
            args = rdx6 + rax10 * 8;
            if (!zf11) 
                break;
            if (*reinterpret_cast<signed char*>(&ebx4)) {
                eax12 = null(rbp7);
                eax13 = eax12 ^ 1;
                edi9 = *reinterpret_cast<unsigned char*>(&eax13);
            }
            rax14 = eval2(edi9, rsi, rdx6);
            eax15 = null(rbp7);
            if (*reinterpret_cast<signed char*>(&eax15) || (eax16 = null(rax14), !!*reinterpret_cast<signed char*>(&eax16))) {
                if (*reinterpret_cast<void***>(rbp7) == 1) {
                    rdi17 = *reinterpret_cast<void***>(rbp7 + 8);
                    fun_24c0(rdi17);
                } else {
                    fun_2780(rbp7 + 8);
                }
                fun_24c0(rbp7);
                if (*reinterpret_cast<void***>(rax14) == 1) {
                    rdi18 = *reinterpret_cast<void***>(rax14 + 8);
                    fun_24c0(rdi18);
                } else {
                    fun_2780(rax14 + 8);
                }
                fun_24c0(rax14);
                rax19 = xmalloc(24);
                *reinterpret_cast<int32_t*>(&rsi) = 0;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
                rbp7 = rax19;
                fun_27f0(rax19 + 8);
            } else {
                if (*reinterpret_cast<void***>(rax14) == 1) {
                    rdi20 = *reinterpret_cast<void***>(rax14 + 8);
                    fun_24c0(rdi20);
                } else {
                    fun_2780(rax14 + 8);
                }
                fun_24c0(rax14);
            }
            rdx6 = args;
            rax8 = *reinterpret_cast<void***>(rdx6);
        } while (rax8);
    }
    return rbp7;
}