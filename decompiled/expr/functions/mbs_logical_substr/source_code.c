mbs_logical_substr (char const *s, size_t pos, size_t len)
{
  char *v, *vlim;

  size_t blen = strlen (s); /* byte length */
  size_t llen = (MB_CUR_MAX > 1) ? mbslen (s) : blen; /* logical length */

  if (llen < pos || pos == 0 || len == 0 || len == SIZE_MAX)
    return xstrdup ("");

  /* characters to copy */
  size_t vlen = MIN (len, llen - pos + 1);

  if (MB_CUR_MAX == 1)
    {
      /* Single-byte case */
      v = xmalloc (vlen + 1);
      vlim = mempcpy (v, s + pos - 1, vlen);
    }
  else
    {
      /* Multibyte case */

      /* FIXME: this is wasteful. Some memory can be saved by counting
         how many bytes the matching characters occupy. */
      vlim = v = xmalloc (blen + 1);

      mbui_iterator_t iter;
      size_t idx=1;
      for (mbui_init (iter, s);
           mbui_avail (iter) && vlen > 0;
           mbui_advance (iter), ++idx)
        {
          /* Skip until we reach the starting position */
          if (idx < pos)
            continue;

          /* Copy one character */
          --vlen;
          vlim = mempcpy (vlim, mbui_cur_ptr (iter), mb_len (mbui_cur (iter)));
        }
    }
  *vlim = '\0';
  return v;
}