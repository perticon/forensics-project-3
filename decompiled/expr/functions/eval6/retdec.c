int64_t eval6(uint64_t a1) {
    int64_t v1 = a1 % 256; // 0x3706
    int64_t v2 = __readfsqword(40); // 0x371e
    int64_t result; // 0x3700
    int64_t v3; // 0x3700
    int64_t v4; // 0x3700
    int64_t v5; // 0x3700
    int64_t v6; // bp-136, 0x3700
    int64_t v7; // 0x3700
    int32_t v8; // 0x3700
    char v9; // 0x3700
    if (args == NULL) {
        goto lab_0x3c40;
    } else {
        int64_t v10 = (int64_t)args; // 0x3717
        int32_t v11 = (int32_t)(int64_t)args % 256 - 43; // 0x3740
        int32_t v12 = v11 == 0 ? (int32_t)*(char *)&g36 : v11;
        int64_t v13 = 8 * (int64_t)(v12 == 0) + v10; // 0x374c
        *(int64_t *)&args = v13;
        if (v12 != 0) {
            // 0x37d0
            if (*(int64_t *)v13 == 0) {
                goto lab_0x3c40;
            } else {
                int64_t v14 = function_26e0(); // 0x37e8
                int64_t v15 = 8 * (int64_t)((int32_t)v14 == 0) + v13; // 0x37f4
                *(int64_t *)&args = v15;
                if ((int32_t)v14 != 0) {
                    // 0x3870
                    if (*(int64_t *)v15 == 0) {
                        goto lab_0x3c40;
                    } else {
                        int64_t v16 = function_26e0(); // 0x3884
                        int64_t v17 = (int32_t)v16 == 0; // 0x388d
                        int64_t v18 = 8 * v17 + v15; // 0x3890
                        *(int64_t *)&args = v18;
                        if ((int32_t)v16 != 0) {
                            // 0x38d0
                            if (*(int64_t *)v18 == 0) {
                                goto lab_0x3c40;
                            } else {
                                int64_t v19 = function_26e0(); // 0x38e4
                                *(int64_t *)&args = 8 * (int64_t)((int32_t)v19 == 0) + v18;
                                if ((int32_t)v19 != 0) {
                                    // 0x39f0
                                } else {
                                    int64_t v20 = eval6(v1); // 0x3905
                                    int64_t v21 = eval6(v1); // 0x3910
                                    int32_t * v22 = (int32_t *)v20; // 0x3918
                                    int32_t v23 = *v22; // 0x3918
                                    if (v23 == 0) {
                                        int64_t v24 = function_25c0(); // 0x3dde
                                        function_2780();
                                        *(int64_t *)(v20 + 8) = v24;
                                        *v22 = 1;
                                    } else {
                                        if (v23 != 1) {
                                            // 0x416b
                                            return eval6_cold();
                                        }
                                    }
                                    int32_t * v25 = (int32_t *)v21; // 0x392c
                                    int32_t v26 = *v25; // 0x392c
                                    int64_t v27; // 0x3700
                                    if (v26 == 0) {
                                        int64_t v28 = function_25c0(); // 0x3da7
                                        function_2780();
                                        *(int64_t *)(v21 + 8) = v28;
                                        *v25 = 1;
                                        v27 = v28;
                                    } else {
                                        if (v26 != 1) {
                                            // 0x41d3
                                            return eval6_cold();
                                        }
                                        // 0x3941
                                        v27 = *(int64_t *)(v21 + 8);
                                    }
                                    char * v29 = (char *)v27; // 0x3946
                                    if (*v29 != 0) {
                                        // 0x3e00
                                        if (function_25a0() < 2) {
                                            // 0x4090
                                            function_2680();
                                        } else {
                                            char v30 = 0; // bp-200, 0x3e0f
                                            mbuiter_multi_next_part_0(&v30);
                                            if (v8 == 0 != v9 != 0) {
                                                int64_t v31 = *(int64_t *)(v20 + 8); // 0x3700
                                                while (true) {
                                                    int64_t v32 = v31;
                                                    int64_t v33; // 0x3700
                                                    if (v33 != 1) {
                                                        // 0x3fa0
                                                        v6 = 0;
                                                        while (true) {
                                                          lab_0x3ffb:
                                                            // 0x3ffb
                                                            mbuiter_multi_next_part_0((char *)&v6);
                                                            if (v9 != 0) {
                                                                if (v8 == 0) {
                                                                    // break -> 0x3e8f
                                                                    break;
                                                                }
                                                                if (v9 == 0) {
                                                                    goto lab_0x4012;
                                                                } else {
                                                                    // break (via goto) -> 0x3957
                                                                    goto lab_0x3957_2;
                                                                    goto lab_0x3ffb;
                                                                }
                                                            } else {
                                                                goto lab_0x4012;
                                                            }
                                                        }
                                                    } else {
                                                        // 0x3e67
                                                        if (mbschr(v29, (int32_t)*(char *)v32) != NULL) {
                                                            // break -> 0x3957
                                                            break;
                                                        }
                                                    }
                                                    // 0x3e8f
                                                    mbuiter_multi_next_part_0(&v30);
                                                    v31 = v32 + v33;
                                                }
                                            }
                                        }
                                    }
                                  lab_0x3957_2:;
                                    int64_t v34 = xmalloc(); // 0x395c
                                    *(int32_t *)v34 = 0;
                                    function_27f0();
                                    if (*v22 == 1) {
                                        // 0x3f90
                                        function_24c0();
                                    } else {
                                        // 0x3980
                                        function_2780();
                                    }
                                    // 0x3989
                                    function_24c0();
                                    if (*v25 == 1) {
                                        // 0x3f78
                                        function_24c0();
                                    } else {
                                        // 0x399c
                                        function_2780();
                                    }
                                    // 0x39a6
                                    function_24c0();
                                    result = v34;
                                    goto lab_0x3792;
                                }
                            }
                        } else {
                            int64_t v35 = eval6(v1); // 0x38a1
                            int64_t v36 = eval6(v1); // 0x38ac
                            int64_t v37 = v35; // 0x38b6
                            if ((char)a1 != 0) {
                                // 0x3f00
                                v37 = docolon(v35, v36, v17, v10);
                                if (*(int32_t *)v35 == 1) {
                                    // 0x40b0
                                    function_24c0();
                                } else {
                                    // 0x3f18
                                    function_2780();
                                }
                                // 0x3f21
                                function_24c0();
                            }
                            // 0x38bc
                            v3 = v37;
                            v4 = v37;
                            if (*(int32_t *)v36 != 1) {
                                goto lab_0x3854;
                            } else {
                                goto lab_0x38c2;
                            }
                        }
                    }
                } else {
                    int64_t v38 = eval6(v1); // 0x3805
                    int32_t * v39 = (int32_t *)v38; // 0x380d
                    int32_t v40 = *v39; // 0x380d
                    int64_t v41; // 0x3700
                    if (v40 == 0) {
                        int64_t v42 = function_25c0(); // 0x39c6
                        function_2780();
                        *(int64_t *)(v38 + 8) = v42;
                        *v39 = 1;
                        v41 = v42;
                    } else {
                        if (v40 != 1) {
                            // 0x4166
                            return eval6_cold();
                        }
                        // 0x3820
                        v41 = *(int64_t *)(v38 + 8);
                    }
                    // 0x3824
                    mbslen((char *)v41);
                    int64_t v43 = xmalloc(); // 0x3834
                    *(int32_t *)v43 = 0;
                    function_27f0();
                    v3 = v43;
                    v4 = v43;
                    if (*v39 == 1) {
                        goto lab_0x38c2;
                    } else {
                        goto lab_0x3854;
                    }
                }
            }
        } else {
            // 0x3759
            require_more_args();
            v5 = (int64_t)args;
            v7 = &ignore_EPIPE;
            goto lab_0x3773;
        }
    }
  lab_0x4012:
    // 0x4029
    if ((int32_t)function_26a0() == 0) {
        // break (via goto) -> 0x3957
        goto lab_0x3957_2;
    }
    goto lab_0x3ffb;
  lab_0x3c40:
    // 0x3c40
    require_more_args();
    int64_t v44 = (int64_t)args;
    int64_t v45 = v44; // 0x3c52
    int64_t v46 = v44; // 0x3c52
    int64_t v47; // 0x3700
    int64_t v48; // 0x3700
    int64_t v49; // 0x3cc9
    int64_t v50; // 0x3cdf
    if (args == NULL) {
        goto lab_0x3f61;
    } else {
        int32_t v51 = (int32_t)v44 % 256 - 40; // 0x3c5c
        int32_t v52 = v51 == 0 ? (int32_t)*(char *)&g36 : v51;
        int64_t v53 = 8 * (int64_t)(v52 == 0) + v44; // 0x3c6c
        *(int64_t *)&args = v53;
        if (v52 != 0) {
            int64_t v54 = *(int64_t *)v53; // 0x3f31
            v45 = v53;
            v46 = 0;
            if (v54 != 0) {
                int32_t v55 = (int32_t)*(char *)v54 - 41; // 0x3f3d
                int32_t v56 = v55; // 0x3f40
                if (v55 == 0) {
                    // 0x3f42
                    v56 = (int32_t)*(char *)(v54 + 1);
                }
                int64_t v57 = 8 * (int64_t)(v56 == 0) + v53; // 0x3f4d
                *(int64_t *)&args = v57;
                if (v56 == 0) {
                    // 0x4170
                    function_2590();
                    function_2830();
                    // 0x4194
                    return eval6_cold();
                }
                // 0x3f5e
                v45 = v57;
                v46 = *(int64_t *)v57;
            }
            goto lab_0x3f61;
        } else {
            int32_t * v58 = eval(a1 % 2 != 0); // 0x3c80
            if (args == NULL) {
                // 0x4199
                quotearg_n_style();
                function_2590();
                function_2830();
                // 0x41d3
                return eval6_cold();
            }
            int64_t v59 = (int64_t)args;
            int32_t v60 = (int32_t)v59 % 256 - 41; // 0x3c9e
            int32_t v61 = v60 != 0 ? v60 : (int32_t)*(char *)&g36;
            *(int64_t *)&args = 8 * (int64_t)(v61 == 0) + v59;
            result = (int64_t)v58;
            if (v61 == 0) {
                goto lab_0x3792;
            } else {
                // 0x3cbf
                v49 = quotearg_n_style();
                v50 = function_2590();
                v47 = function_2830();
                v48 = (int32_t)"syntax error: expecting ')' instead of %s" ^ (int32_t)"syntax error: expecting ')' instead of %s";
                goto lab_0x3d00;
            }
        }
    }
  lab_0x3f61:
    // 0x3f61
    v5 = v46;
    v7 = v45 + 8;
    goto lab_0x3773;
  lab_0x3773:
    // 0x3773
    *(int64_t *)&args = v7;
    int64_t v62 = xmalloc(); // 0x3778
    *(int32_t *)v62 = 1;
    *(int64_t *)(v62 + 8) = (int64_t)xstrdup((char *)v5);
    result = v62;
    goto lab_0x3792;
  lab_0x3792:
    // 0x3792
    if (v2 == __readfsqword(40)) {
        // 0x37a9
        return result;
    }
    // 0x4161
    function_25d0();
    // 0x4166
    return eval6_cold();
  lab_0x3d00:
    // 0x3d00
    if ((int32_t)v47 != 1) {
        eval6_cold();
    }
    int64_t v63 = toarith_part_0(v49, v48, v50); // 0x3d0c
    int64_t v64 = v44; // 0x3d13
    int64_t v65 = v49; // 0x3d13
    int64_t v66 = v1; // 0x3d13
    if ((char)v63 != 0) {
        goto lab_0x3a61;
    } else {
        goto lab_0x3d19;
    }
  lab_0x38c2:
    // 0x38c2
    function_24c0();
    int64_t v67 = v4; // 0x38cb
    goto lab_0x385d;
  lab_0x3854:
    // 0x3854
    function_2780();
    v67 = v3;
    goto lab_0x385d;
  lab_0x3a61:;
    int32_t v96 = *(int32_t *)v1; // 0x3a61
    if (v96 != 0) {
        // 0x3ee0
        if (v96 != 1) {
            eval6_cold();
        }
        int64_t v97 = toarith_part_0(v1, v48, v50); // 0x3eec
        v64 = v44;
        v65 = v49;
        v66 = v1;
        if ((char)v97 == 0) {
            goto lab_0x3d19;
        } else {
            goto lab_0x3a6d;
        }
    } else {
        goto lab_0x3a6d;
    }
  lab_0x3d19:;
    int64_t v98 = xmalloc(); // 0x3d1e
    *(int32_t *)v98 = 1;
    *(int64_t *)(v98 + 8) = (int64_t)xstrdup((char *)&g23);
    int64_t v92 = v64; // 0x3d38
    int64_t v93 = v65; // 0x3d38
    int64_t v94 = v66; // 0x3d38
    int64_t v95 = v98; // 0x3d38
    goto lab_0x3d3c;
  lab_0x385d:
    // 0x385d
    function_24c0();
    result = v67;
    goto lab_0x3792;
  lab_0x3a6d:;
    uint32_t v68 = *(int32_t *)(v49 + 12); // 0x3a6d
    char * v69 = (char *)-2; // 0x3a74
    int64_t v70 = -1; // 0x3a74
    if (v68 >= 0) {
        // 0x3a7a
        v69 = (char *)-3;
        v70 = -2;
        if (v68 <= 1) {
            // 0x3a83
            v69 = (char *)-1;
            v70 = 0;
            if (v68 != 0) {
                int64_t v71 = *(int64_t *)*(int64_t *)(v49 + 16); // 0x3a90
                int64_t v72 = v71 == -1 ? -2 : v71; // 0x3a9d
                v69 = (char *)(v72 - 1);
                v70 = v72;
            }
        }
    }
    uint32_t v73 = *(int32_t *)(v1 + 12); // 0x3aaf
    int64_t v74 = -1; // 0x3ab5
    if (v73 >= 0) {
        // 0x3abb
        v74 = -2;
        if (v73 <= 1) {
            // 0x3ac7
            v74 = 0;
            if (v73 != 0) {
                int64_t v75 = *(int64_t *)*(int64_t *)(v1 + 16); // 0x3ad2
                v74 = v75 == -1 ? -2 : v75;
            }
        }
    }
    uint64_t v76 = v74;
    int64_t v77 = function_25b0(); // 0x3b06
    if (function_25a0() >= 2) {
        // 0x3b08
        v77 = mbslen((char *)*(int64_t *)(v44 + 8));
    }
    // 0x3b10
    char * v78; // 0x3700
    if (v76 < 0xffffffffffffffff == v77 > (int64_t)v69) {
        int64_t v79 = function_25a0(); // 0x3b39
        int64_t v80 = xmalloc();
        char * v81; // 0x3700
        char * v82; // 0x3700
        if (v79 == 1) {
            // 0x4136
            v81 = (char *)v80;
            v82 = (char *)function_2810();
        } else {
            uint64_t v83 = v77 - v70 + 1; // 0x3b2e
            int64_t v84 = v83 > v76 ? v76 : v83; // 0x3b35
            char * v85 = (char *)v80; // 0x3b68
            v6 = 0;
            mbuiter_multi_next_part_0((char *)&v6);
            int64_t v86 = 1; // 0x3bed
            v81 = v85;
            v82 = v85;
            if ((v8 != 0 || v9 == 0) == (v84 != 0)) {
                char * v87 = v85; // 0x3b9d
                int64_t v88 = v84; // 0x3b9d
                if (v86 >= v70) {
                    // 0x3b9f
                    v87 = (char *)function_2810();
                    v88 = v84 - 1;
                }
                // 0x3bc9
                mbuiter_multi_next_part_0((char *)&v6);
                v86++;
                char * v89 = v87; // 0x3bed
                v81 = v85;
                v82 = v87;
                while (v88 != 0) {
                    int64_t v90 = v88;
                    v87 = v89;
                    v88 = v90;
                    if (v86 >= v70) {
                        // 0x3b9f
                        v87 = (char *)function_2810();
                        v88 = v90 - 1;
                    }
                    // 0x3bc9
                    mbuiter_multi_next_part_0((char *)&v6);
                    v86++;
                    v89 = v87;
                    v81 = v85;
                    v82 = v87;
                }
            }
        }
        // 0x3bff
        *v82 = 0;
        v78 = v81;
    } else {
        // 0x40cf
        v78 = xstrdup((char *)&g23);
    }
    int64_t v91 = xmalloc(); // 0x3c0c
    *(int32_t *)v91 = 1;
    *(int64_t *)(v91 + 8) = (int64_t)xstrdup(v78);
    function_24c0();
    v92 = v44;
    v93 = v49;
    v94 = v1;
    v95 = v91;
    goto lab_0x3d3c;
  lab_0x3d3c:
    // 0x3d3c
    if (*(int32_t *)v92 == 1) {
        // 0x4080
        function_24c0();
    } else {
        // 0x3d46
        function_2780();
    }
    // 0x3d4f
    function_24c0();
    if (*(int32_t *)v93 == 1) {
        // 0x4070
        function_24c0();
    } else {
        // 0x3d62
        function_2780();
    }
    // 0x3d6c
    function_24c0();
    if (*(int32_t *)v94 == 1) {
        // 0x4058
        function_24c0();
    } else {
        // 0x3d7f
        function_2780();
    }
    // 0x3d88
    function_24c0();
    result = v95;
    goto lab_0x3792;
}