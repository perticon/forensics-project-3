int * eval6(char param_1)

{
  byte **ppbVar1;
  char *pcVar2;
  char cVar3;
  uint uVar4;
  int iVar5;
  int *piVar6;
  undefined8 uVar7;
  int *piVar8;
  int *piVar9;
  int *__ptr;
  size_t sVar10;
  size_t sVar11;
  undefined8 uVar12;
  char *pcVar13;
  size_t sVar14;
  long lVar15;
  char *pcVar16;
  byte *pbVar17;
  long lVar18;
  ulong __n;
  ulong uVar19;
  long in_FS_OFFSET;
  ulong local_f0;
  undefined *local_e8;
  undefined *local_d0;
  undefined local_c8 [4];
  undefined8 local_c4;
  undefined local_bc;
  char *local_b8;
  size_t local_b0;
  char local_a8;
  int local_a4;
  undefined local_88 [4];
  undefined8 local_84;
  undefined local_7c;
  char *local_78;
  size_t local_70;
  char local_68;
  int local_64;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  pbVar17 = *args;
  if (pbVar17 == (byte *)0x0) goto LAB_00103c40;
  uVar4 = *pbVar17 - 0x2b;
  if (uVar4 == 0) {
    uVar4 = (uint)pbVar17[1];
  }
  ppbVar1 = args + (uVar4 == 0);
  args = ppbVar1;
  if (uVar4 == 0) {
    require_more_args();
    pbVar17 = *args;
  }
  else {
    if (*ppbVar1 == (byte *)0x0) goto LAB_00103c40;
    iVar5 = strcmp((char *)*ppbVar1,"length");
    ppbVar1 = ppbVar1 + (iVar5 == 0);
    args = ppbVar1;
    if (iVar5 == 0) {
      piVar8 = (int *)eval6(param_1);
      if (*piVar8 == 0) {
        uVar7 = __gmpz_get_str(0,10,piVar8 + 2);
        __gmpz_clear(piVar8 + 2);
        *(undefined8 *)(piVar8 + 2) = uVar7;
        *piVar8 = 1;
      }
      else {
        if (*piVar8 != 1) {
          piVar8 = (int *)eval6_cold();
          return piVar8;
        }
        uVar7 = *(undefined8 *)(piVar8 + 2);
      }
      uVar7 = mbslen(uVar7);
      piVar6 = (int *)xmalloc(0x18);
      *piVar6 = 0;
      __gmpz_init_set_ui(piVar6 + 2,uVar7);
      iVar5 = *piVar8;
joined_r0x001038c0:
      if (iVar5 == 1) {
        free(*(void **)(piVar8 + 2));
      }
      else {
        __gmpz_clear(piVar8 + 2);
      }
      free(piVar8);
      goto LAB_00103792;
    }
    if (*ppbVar1 == (byte *)0x0) goto LAB_00103c40;
    iVar5 = strcmp((char *)*ppbVar1,"match");
    ppbVar1 = ppbVar1 + (iVar5 == 0);
    args = ppbVar1;
    if (iVar5 == 0) {
      piVar9 = (int *)eval6(param_1);
      piVar8 = (int *)eval6(param_1);
      piVar6 = piVar9;
      if (param_1 != '\0') {
        piVar6 = (int *)docolon(piVar9,piVar8);
        if (*piVar9 == 1) {
          free(*(void **)(piVar9 + 2));
        }
        else {
          __gmpz_clear(piVar9 + 2);
        }
        free(piVar9);
      }
      iVar5 = *piVar8;
      goto joined_r0x001038c0;
    }
    if (*ppbVar1 != (byte *)0x0) {
      iVar5 = strcmp((char *)*ppbVar1,"index");
      ppbVar1 = ppbVar1 + (iVar5 == 0);
      args = ppbVar1;
      if (iVar5 == 0) {
        piVar8 = (int *)eval6(param_1);
        piVar9 = (int *)eval6(param_1);
        if (*piVar8 == 0) {
          uVar7 = __gmpz_get_str(0,10,piVar8 + 2);
          __gmpz_clear(piVar8 + 2);
          *(undefined8 *)(piVar8 + 2) = uVar7;
          *piVar8 = 1;
        }
        else if (*piVar8 != 1) {
          piVar8 = (int *)eval6_cold();
          return piVar8;
        }
        if (*piVar9 == 0) {
          pcVar13 = (char *)__gmpz_get_str(0,10,piVar9 + 2);
          __gmpz_clear(piVar9 + 2);
          *(char **)(piVar9 + 2) = pcVar13;
          *piVar9 = 1;
        }
        else {
          if (*piVar9 != 1) {
            piVar8 = (int *)eval6_cold();
            return piVar8;
          }
          pcVar13 = *(char **)(piVar9 + 2);
        }
        pcVar16 = *(char **)(piVar8 + 2);
        if (*pcVar13 == '\0') {
LAB_00103954:
          lVar18 = 0;
        }
        else {
          sVar14 = __ctype_get_mb_cur_max();
          if (1 < sVar14) {
            local_c8[0] = 0;
            lVar18 = 0;
            local_c4 = 0;
            local_b8 = pcVar16;
            do {
              local_bc = 0;
              mbuiter_multi_next_part_0(local_c8);
              sVar14 = local_b0;
              pcVar16 = local_b8;
              if ((local_a8 != '\0') && (local_a4 == 0)) goto LAB_00103954;
              lVar18 = lVar18 + 1;
              if (local_b0 != 1) {
                local_88[0] = 0;
                local_84 = 0;
                local_78 = pcVar13;
                do {
                  local_7c = 0;
                  mbuiter_multi_next_part_0(local_88);
                  sVar10 = local_70;
                  pcVar2 = local_78;
                  if (local_68 == '\0') {
LAB_00104012:
                    if ((local_70 == local_b0) &&
                       (iVar5 = memcmp(local_78,local_b8,local_70), iVar5 == 0)) goto LAB_00103957;
                  }
                  else {
                    sVar14 = local_b0;
                    pcVar16 = local_b8;
                    if (local_64 == 0) goto LAB_00103e8f;
                    if (local_a8 == '\0') goto LAB_00104012;
                    if (local_a4 == local_64) goto LAB_00103957;
                  }
                  local_78 = pcVar2 + sVar10;
                } while( true );
              }
              lVar15 = mbschr(pcVar13,(int)*local_b8);
              if (lVar15 != 0) goto LAB_00103957;
LAB_00103e8f:
              local_b8 = pcVar16 + sVar14;
            } while( true );
          }
          sVar14 = strcspn(pcVar16,pcVar13);
          if (pcVar16[sVar14] == '\0') goto LAB_00103954;
          lVar18 = sVar14 + 1;
        }
LAB_00103957:
        piVar6 = (int *)xmalloc(0x18);
        *piVar6 = 0;
        __gmpz_init_set_ui(piVar6 + 2,lVar18);
        if (*piVar8 == 1) {
          free(*(void **)(piVar8 + 2));
        }
        else {
          __gmpz_clear(piVar8 + 2);
        }
        free(piVar8);
        if (*piVar9 == 1) {
          free(*(void **)(piVar9 + 2));
        }
        else {
          __gmpz_clear(piVar9 + 2);
        }
        free(piVar9);
        goto LAB_00103792;
      }
      if (*ppbVar1 != (byte *)0x0) {
        iVar5 = strcmp((char *)*ppbVar1,"substr");
        args = ppbVar1 + (iVar5 == 0);
        if (iVar5 == 0) {
          piVar8 = (int *)eval6(param_1);
          piVar9 = (int *)eval6(param_1);
          __ptr = (int *)eval6(param_1);
          if (*piVar8 == 0) {
            uVar7 = __gmpz_get_str(0,10,piVar8 + 2);
            __gmpz_clear(piVar8 + 2);
            *(undefined8 *)(piVar8 + 2) = uVar7;
            *piVar8 = 1;
          }
          else if (*piVar8 != 1) {
            piVar8 = (int *)eval6_cold();
            return piVar8;
          }
          if (*piVar9 == 0) {
LAB_00103a61:
            if (*__ptr != 0) {
              if (*__ptr != 1) {
code_r0x0010292a:
                    /* WARNING: Subroutine does not return */
                abort();
              }
              cVar3 = toarith_part_0(__ptr);
              if (cVar3 == '\0') goto LAB_00103d19;
            }
            iVar5 = piVar9[3];
            if (iVar5 < 0) {
              local_e8 = (undefined *)0xfffffffffffffffe;
              local_f0 = 0xffffffffffffffff;
            }
            else if (iVar5 < 2) {
              if (iVar5 == 0) {
                local_e8 = (undefined *)0xffffffffffffffff;
                local_f0 = 0;
              }
              else {
                local_f0 = 0xfffffffffffffffe;
                if (**(ulong **)(piVar9 + 4) != 0xffffffffffffffff) {
                  local_f0 = **(ulong **)(piVar9 + 4);
                }
                local_e8 = (undefined *)(local_f0 - 1);
              }
            }
            else {
              local_e8 = (undefined *)0xfffffffffffffffd;
              local_f0 = 0xfffffffffffffffe;
            }
            iVar5 = __ptr[3];
            if (iVar5 < 0) {
              uVar19 = 0xffffffffffffffff;
            }
            else {
              uVar19 = 0xfffffffffffffffe;
              if (((iVar5 < 2) && (uVar19 = 0, iVar5 != 0)) &&
                 (uVar19 = **(ulong **)(__ptr + 4), uVar19 == 0xffffffffffffffff)) {
                uVar19 = 0xfffffffffffffffe;
              }
            }
            pcVar13 = *(char **)(piVar8 + 2);
            sVar10 = strlen(pcVar13);
            sVar11 = __ctype_get_mb_cur_max();
            sVar14 = sVar10;
            if (1 < sVar11) {
              sVar14 = mbslen(pcVar13);
            }
            if ((local_e8 < sVar14) && (uVar19 - 1 < 0xfffffffffffffffe)) {
              __n = (sVar14 - local_f0) + 1;
              if (uVar19 < __n) {
                __n = uVar19;
              }
              sVar14 = __ctype_get_mb_cur_max();
              if (sVar14 == 1) {
                local_d0 = (undefined *)xmalloc(__n + 1);
                local_e8 = (undefined *)mempcpy(local_d0,pcVar13 + (long)local_e8,__n);
              }
              else {
                local_d0 = (undefined *)xmalloc(sVar10 + 1);
                uVar19 = 1;
                local_88[0] = 0;
                local_84 = 0;
                local_e8 = local_d0;
                local_78 = pcVar13;
                while( true ) {
                  local_7c = 0;
                  mbuiter_multi_next_part_0(local_88);
                  sVar14 = local_70;
                  pcVar13 = local_78;
                  if (((local_68 != '\0') && (local_64 == 0)) || (__n == 0)) break;
                  if (local_f0 <= uVar19) {
                    __n = __n - 1;
                    local_e8 = (undefined *)mempcpy(local_e8,local_78,local_70);
                  }
                  local_78 = pcVar13 + sVar14;
                  uVar19 = uVar19 + 1;
                }
              }
              *local_e8 = 0;
            }
            else {
              local_d0 = (undefined *)xstrdup("");
            }
            piVar6 = (int *)xmalloc(0x18);
            *piVar6 = 1;
            uVar7 = xstrdup(local_d0);
            *(undefined8 *)(piVar6 + 2) = uVar7;
            free(local_d0);
          }
          else {
            if (*piVar9 != 1) goto code_r0x0010292a;
            cVar3 = toarith_part_0(piVar9);
            if (cVar3 != '\0') goto LAB_00103a61;
LAB_00103d19:
            piVar6 = (int *)xmalloc(0x18);
            *piVar6 = 1;
            uVar7 = xstrdup("");
            *(undefined8 *)(piVar6 + 2) = uVar7;
          }
          if (*piVar8 == 1) {
            free(*(void **)(piVar8 + 2));
          }
          else {
            __gmpz_clear(piVar8 + 2);
          }
          free(piVar8);
          if (*piVar9 == 1) {
            free(*(void **)(piVar9 + 2));
          }
          else {
            __gmpz_clear(piVar9 + 2);
          }
          free(piVar9);
          if (*__ptr == 1) {
            free(*(void **)(__ptr + 2));
          }
          else {
            __gmpz_clear(__ptr + 2);
          }
          free(__ptr);
          goto LAB_00103792;
        }
      }
    }
LAB_00103c40:
    require_more_args();
    pbVar17 = *args;
    if (pbVar17 != (byte *)0x0) {
      uVar4 = *pbVar17 - 0x28;
      if (uVar4 == 0) {
        uVar4 = (uint)pbVar17[1];
      }
      args = args + (uVar4 == 0);
      if (uVar4 == 0) {
        piVar6 = (int *)eval(param_1);
        pbVar17 = *args;
        if (pbVar17 == (byte *)0x0) {
          uVar7 = quotearg_n_style(0,8,args[-1]);
          uVar12 = dcgettext(0,"syntax error: expecting \')\' after %s",5);
                    /* WARNING: Subroutine does not return */
          error(2,0,uVar12,uVar7);
        }
        uVar4 = *pbVar17 - 0x29;
        if (uVar4 == 0) {
          uVar4 = (uint)pbVar17[1];
        }
        args = args + (uVar4 == 0);
        if (uVar4 != 0) {
          uVar7 = quotearg_n_style(0,8,*args);
          uVar12 = dcgettext(0,"syntax error: expecting \')\' instead of %s",5);
                    /* WARNING: Subroutine does not return */
          error(2,0,uVar12,uVar7);
        }
        goto LAB_00103792;
      }
      pbVar17 = *args;
      if (pbVar17 != (byte *)0x0) {
        uVar4 = *pbVar17 - 0x29;
        if (uVar4 == 0) {
          uVar4 = (uint)pbVar17[1];
        }
        args = args + (uVar4 == 0);
        if (uVar4 == 0) {
          uVar7 = dcgettext(0,"syntax error: unexpected \')\'",5);
                    /* WARNING: Subroutine does not return */
          error(2,0,uVar7);
        }
        pbVar17 = *args;
      }
    }
  }
  args = args + 1;
  piVar6 = (int *)xmalloc(0x18);
  *piVar6 = 1;
  uVar7 = xstrdup(pbVar17);
  *(undefined8 *)(piVar6 + 2) = uVar7;
LAB_00103792:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return piVar6;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}