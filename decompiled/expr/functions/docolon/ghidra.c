undefined4 * docolon(int *param_1,int *param_2)

{
  long lVar1;
  undefined8 uVar2;
  char *pcVar3;
  size_t sVar4;
  long lVar5;
  ulong uVar6;
  undefined4 *puVar7;
  int *piVar8;
  int iVar9;
  ulong uVar10;
  long in_FS_OFFSET;
  long local_1e8;
  void *local_1e0;
  void *local_1d8;
  undefined8 local_1c8;
  undefined8 local_1c0;
  undefined *local_1a8;
  undefined8 local_1a0;
  ulong local_198;
  byte local_190;
  undefined local_188 [4];
  undefined8 local_184;
  undefined local_17c;
  long local_178;
  long local_170;
  char local_168;
  int local_164;
  undefined local_148 [264];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (*param_1 == 0) {
    uVar2 = __gmpz_get_str(0,10,param_1 + 2);
    __gmpz_clear(param_1 + 2);
    *(undefined8 *)(param_1 + 2) = uVar2;
    *param_1 = 1;
  }
  else if (*param_1 != 1) {
    puVar7 = (undefined4 *)docolon_cold();
    return puVar7;
  }
  if (*param_2 == 0) {
    pcVar3 = (char *)__gmpz_get_str(0,10,param_2 + 2);
    __gmpz_clear(param_2 + 2);
    *(char **)(param_2 + 2) = pcVar3;
    *param_2 = 1;
  }
  else {
    if (*param_2 != 1) {
      puVar7 = (undefined4 *)docolon_cold();
      return puVar7;
    }
    pcVar3 = *(char **)(param_2 + 2);
  }
  local_1a8 = local_148;
  local_1e8 = 0;
  local_1e0 = (void *)0x0;
  local_1d8 = (void *)0x0;
  local_1c8 = 0;
  local_1c0 = 0;
  local_1a0 = 0;
  rpl_re_syntax_options = 0x2c6;
  sVar4 = strlen(pcVar3);
  lVar5 = rpl_re_compile_pattern(pcVar3,sVar4,&local_1c8);
  if (lVar5 != 0) {
                    /* WARNING: Subroutine does not return */
    error(2,0,"%s",lVar5);
  }
  pcVar3 = *(char **)(param_1 + 2);
  local_190 = local_190 & 0x7f;
  sVar4 = strlen(pcVar3);
  uVar6 = rpl_re_match(&local_1c8,pcVar3,sVar4,0,&local_1e8);
  if ((long)uVar6 < 0) {
    if (uVar6 != 0xffffffffffffffff) {
      uVar2 = dcgettext(0,"error in regular expression matcher",5);
      iVar9 = 0x4b;
      if (uVar6 == 0xfffffffffffffffe) {
        piVar8 = __errno_location();
        iVar9 = *piVar8;
      }
                    /* WARNING: Subroutine does not return */
      error(3,iVar9,uVar2);
    }
    if (local_198 == 0) {
      puVar7 = (undefined4 *)xmalloc(0x18);
      *puVar7 = 0;
      __gmpz_init_set_ui(puVar7 + 2,0);
      goto joined_r0x001035bd;
    }
  }
  else {
    if (local_198 == 0) {
      sVar4 = __ctype_get_mb_cur_max();
      uVar10 = uVar6;
      if (sVar4 != 1) {
        lVar5 = *(long *)(param_1 + 2);
        local_188[0] = 0;
        local_184 = 0;
        local_178 = lVar5;
        while( true ) {
          local_17c = 0;
          mbuiter_multi_next_part_0(local_188);
          uVar10 = local_198;
          if (((local_168 != '\0') && (local_164 == 0)) || (uVar6 <= (ulong)(local_178 - lVar5)))
          break;
          local_178 = local_178 + local_170;
          local_198 = local_198 + 1;
        }
      }
      puVar7 = (undefined4 *)xmalloc(0x18);
      *puVar7 = 0;
      __gmpz_init_set_ui(puVar7 + 2,uVar10);
      goto joined_r0x001035bd;
    }
    if (-1 < *(long *)((long)local_1d8 + 8)) {
      *(undefined *)(*(long *)(param_1 + 2) + *(long *)((long)local_1d8 + 8)) = 0;
      lVar5 = *(long *)((long)local_1e0 + 8);
      lVar1 = *(long *)(param_1 + 2);
      puVar7 = (undefined4 *)xmalloc(0x18);
      *puVar7 = 1;
      uVar2 = xstrdup(lVar5 + lVar1);
      *(undefined8 *)(puVar7 + 2) = uVar2;
      goto joined_r0x001035bd;
    }
  }
  puVar7 = (undefined4 *)xmalloc(0x18);
  *puVar7 = 1;
  uVar2 = xstrdup("");
  *(undefined8 *)(puVar7 + 2) = uVar2;
joined_r0x001035bd:
  if (local_1e8 != 0) {
    free(local_1e0);
    free(local_1d8);
  }
  local_1a8 = (undefined *)0x0;
  rpl_regfree(&local_1c8);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return puVar7;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}