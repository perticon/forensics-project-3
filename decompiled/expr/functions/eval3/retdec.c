int64_t eval3(uint64_t a1) {
    uint64_t v1 = a1 % 256; // 0x44cb
    int64_t result = eval4(v1); // 0x44d1
    if (args == NULL) {
        // 0x4630
        return result;
    }
    int64_t v2 = result + 8;
    int64_t v3; // 0x44c0
    int64_t v4; // 0x44c0
    int64_t v5; // 0x44c0
    int64_t v6; // 0x44c0
    int64_t v7; // 0x4530
    while (true) {
      lab_0x4504:
        // 0x4504
        v5 = v4;
        int64_t v8 = (int64_t)args;
        int32_t v9 = (int32_t)v8 % 256 - 43; // 0x4507
        int32_t v10 = v9 != 0 ? v9 : (int32_t)*(char *)&g36;
        int64_t v11 = 8 * (int64_t)(v10 == 0) + v8; // 0x451d
        *(int64_t *)&args = v11;
        int64_t v12 = v11; // 0x4528
        if (v10 != 0) {
            int64_t v13 = *(int64_t *)v11; // 0x45f8
            if (v13 == 0) {
                // break -> 0x4630
                break;
            }
            int32_t v14 = (int32_t)*(char *)v13 - 45; // 0x4603
            int32_t v15 = v14; // 0x4606
            if (v14 == 0) {
                // 0x4608
                v15 = (int32_t)*(char *)(v13 + 1);
            }
            int64_t v16 = 8 * (int64_t)(v15 == 0) + v11; // 0x4613
            *(int64_t *)&args = v16;
            v12 = v16;
            if (v15 != 0) {
                // break -> 0x4630
                break;
            }
        }
        // 0x452e
        v7 = eval4(v1);
        v6 = v5;
        if ((char)a1 == 0) {
            goto lab_0x456a;
        } else {
            // 0x453d
            v3 = v12;
            int32_t v17 = *(int32_t *)result; // 0x453d
            if (v17 != 0) {
                if (v17 != 1) {
                    eval3_cold();
                }
                // 0x4591
                if ((char)toarith_part_0(result, v5, v3) != 0) {
                    goto lab_0x4545;
                } else {
                    goto lab_0x459d;
                }
            } else {
                goto lab_0x4545;
            }
        }
    }
    // 0x4630
    return result;
  lab_0x456a:
    // 0x456a
    if (*(int32_t *)v7 != 1) {
        // 0x44e0
        function_2780();
    } else {
        // 0x4574
        function_24c0();
    }
    // 0x44e9
    function_24c0();
    v4 = v6;
    if (args == NULL) {
        return result;
    }
    goto lab_0x4504;
  lab_0x4545:;
    uint32_t v18 = *(int32_t *)v7; // 0x4545
    int64_t v19 = v18; // 0x454a
    int64_t v20 = v3; // 0x454a
    int64_t v21 = v5; // 0x454a
    if (v18 != 0) {
        goto lab_0x45c8;
    } else {
        // 0x455c
        __gmpz_sub();
        v6 = v2;
        goto lab_0x456a;
    }
  lab_0x459d:;
    int64_t v22 = function_2590(); // 0x45ab
    v19 = function_2830();
    v20 = v22;
    v21 = (int32_t)"non-integer argument" ^ (int32_t)"non-integer argument";
    goto lab_0x45c8;
  lab_0x45c8:
    // 0x45c8
    if ((int32_t)v19 != 1) {
        eval3_cold();
    }
    // 0x45d1
    if ((char)toarith_part_0(v7, v21, v20) == 0) {
        goto lab_0x459d;
    } else {
        // 0x455c
        __gmpz_sub();
        v6 = v2;
        goto lab_0x456a;
    }
}