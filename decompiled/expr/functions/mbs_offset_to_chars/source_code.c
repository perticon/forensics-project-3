mbs_offset_to_chars (char const *s, size_t ofs)
{
  mbui_iterator_t iter;
  size_t c = 0;
  for (mbui_init (iter, s); mbui_avail (iter); mbui_advance (iter))
    {
      ptrdiff_t d = mbui_cur_ptr (iter) - s;
      if (d >= ofs)
        break;
      ++c;
    }
  return c;
}