int64_t eval4(uint64_t a1) {
    int32_t * v1 = eval5(a1 % 2 != 0); // 0x42d1
    int64_t result = (int64_t)v1; // 0x42d1
    if (args == NULL) {
        // 0x4488
        return result;
    }
    int64_t v2 = result + 8;
    int32_t v3; // 0x42c0
    int64_t v4; // 0x42c0
    int64_t v5; // 0x42c0
    int64_t v6; // 0x42c0
    int64_t v7; // 0x42c0
    int64_t v8; // 0x4330
    int32_t * v9; // 0x4330
    while (true) {
      lab_0x4304:
        // 0x4304
        v6 = v5;
        int64_t v10 = (int64_t)args;
        int32_t v11 = (int32_t)v10 % 256 - 42; // 0x4307
        int32_t v12 = v11 != 0 ? v11 : (int32_t)*(char *)&g36;
        int64_t v13 = 8 * (int64_t)(v12 == 0) + v10; // 0x431d
        *(int64_t *)&args = v13;
        int64_t v14 = v13; // 0x4328
        int32_t v15 = 0; // 0x4328
        if (v12 != 0) {
            int64_t v16 = *(int64_t *)v13; // 0x4410
            if (v16 == 0) {
                // break -> 0x4488
                break;
            }
            int32_t v17 = (int32_t)*(char *)v16 - 47; // 0x441b
            int32_t v18 = v17; // 0x441e
            if (v17 == 0) {
                // 0x4420
                v18 = (int32_t)*(char *)(v16 + 1);
            }
            int64_t v19 = 8 * (int64_t)(v18 == 0) + v13; // 0x4431
            *(int64_t *)&args = v19;
            v14 = v19;
            v15 = 1;
            if (v18 != 0) {
                int64_t v20 = *(int64_t *)v19; // 0x4442
                if (v20 == 0) {
                    // break -> 0x4488
                    break;
                }
                int32_t v21 = (int32_t)*(char *)v20 - 37; // 0x444d
                int32_t v22 = v21; // 0x4450
                if (v21 == 0) {
                    // 0x4452
                    v22 = (int32_t)*(char *)(v20 + 1);
                }
                int64_t v23 = 8 * (int64_t)(v22 == 0) + v19; // 0x445d
                *(int64_t *)&args = v23;
                v14 = v23;
                v15 = 2;
                if (v22 != 0) {
                    // break -> 0x4488
                    break;
                }
            }
        }
        // 0x432e
        v9 = eval5(a1 % 2 != 0);
        v7 = v6;
        if ((char)a1 == 0) {
            goto lab_0x4383;
        } else {
            // 0x433d
            v3 = v15;
            v4 = v14;
            v8 = (int64_t)v9;
            int32_t v24 = *v1; // 0x433d
            if (v24 != 0) {
                if (v24 != 1) {
                    eval4_cold();
                }
                // 0x43a9
                if ((char)toarith_part_0(result, v6, v4) != 0) {
                    goto lab_0x4345;
                } else {
                    goto lab_0x43b5;
                }
            } else {
                goto lab_0x4345;
            }
        }
    }
    // 0x4488
    return result;
  lab_0x4383:
    // 0x4383
    if (*v9 != 1) {
        // 0x42e0
        function_2780();
    } else {
        // 0x438d
        function_24c0();
    }
    // 0x42e9
    function_24c0();
    v5 = v7;
    if (args == NULL) {
        return result;
    }
    goto lab_0x4304;
  lab_0x4345:;
    uint32_t v25 = *v9; // 0x4345
    int64_t v26 = v25; // 0x434a
    int64_t v27 = v4; // 0x434a
    int64_t v28 = v6; // 0x434a
    if (v25 != 0) {
        goto lab_0x43e0;
    } else {
        if (v3 == 0) {
            // 0x4375
            __gmpz_tdiv_r();
            v7 = v2;
            goto lab_0x4383;
        } else {
            goto lab_0x4359;
        }
    }
  lab_0x43b5:;
    int64_t v29 = function_2590(); // 0x43c3
    v26 = function_2830();
    v27 = v29;
    v28 = (int32_t)"non-integer argument" ^ (int32_t)"non-integer argument";
    goto lab_0x43e0;
  lab_0x43e0:
    // 0x43e0
    if ((int32_t)v26 != 1) {
        eval4_cold();
    }
    // 0x43e9
    if ((char)toarith_part_0(v8, v28, v27) == 0) {
        goto lab_0x43b5;
    } else {
        if (v3 != 0) {
            goto lab_0x4359;
        } else {
            // 0x4375
            __gmpz_tdiv_r();
            v7 = v2;
            goto lab_0x4383;
        }
    }
  lab_0x4359:
    // 0x4359
    if (*(int32_t *)(v8 + 12) == 0) {
        // 0x4494
        function_2590();
        return function_2830();
    }
    // 0x4375
    __gmpz_tdiv_r();
    v7 = v2;
    goto lab_0x4383;
}