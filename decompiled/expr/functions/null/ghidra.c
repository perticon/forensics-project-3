ulong null(int *param_1)

{
  char *pcVar1;
  char cVar2;
  ulong in_R8;
  ulong uVar3;
  
  if (*param_1 == 0) {
    uVar3 = in_R8 & 0xffffffffffffff00 | (ulong)(param_1[3] == 0);
  }
  else {
    if (*param_1 != 1) {
      uVar3 = null_cold();
      return uVar3;
    }
    uVar3 = 1;
    cVar2 = **(char **)(param_1 + 2);
    if (cVar2 != '\0') {
      pcVar1 = *(char **)(param_1 + 2) + (cVar2 == '-');
      cVar2 = *pcVar1;
      do {
        if (cVar2 != '0') {
          return 0;
        }
        cVar2 = pcVar1[1];
        pcVar1 = pcVar1 + 1;
      } while (cVar2 != '\0');
      return 1;
    }
  }
  return uVar3 & 0xffffffff;
}