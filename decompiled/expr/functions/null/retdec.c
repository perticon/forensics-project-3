int64_t null(int64_t a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5) {
    int32_t v1 = a1;
    if (v1 == 0) {
        // 0x331f
        return a5 & 0xffffff00 | (int64_t)(*(int32_t *)(a1 + 12) == 0);
    }
    if (v1 != 1) {
        // 0x337a
        return null_cold();
    }
    int64_t v2 = *(int64_t *)(a1 + 8); // 0x332d
    char v3 = *(char *)v2; // 0x3337
    if (v3 == 0) {
        // 0x331f
        return 1;
    }
    int64_t v4 = v2 + (int64_t)(v3 == 45); // 0x3347
    int64_t v5 = v4; // 0x334d
    char v6 = *(char *)v4; // 0x334d
    int64_t result = 0; // 0x335f
    while (v6 == 48) {
        // 0x3350
        v5++;
        v6 = *(char *)v5;
        result = 1;
        if (v6 == 0) {
            // break -> 0x331f
            break;
        }
        result = 0;
    }
    // 0x331f
    return result;
}