void** eval(uint32_t edi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, ...) {
    uint32_t ebx11;
    void** rax12;
    void** rdx13;
    void** r12_14;
    void** rax15;
    uint32_t edi16;
    int64_t rax17;
    uint1_t zf18;
    uint32_t eax19;
    void** rax20;
    uint32_t eax21;
    void** rdi22;
    void** rdi23;
    uint32_t eax24;
    void** rdi25;
    void** rax26;
    void** rdi27;

    ebx11 = edi;
    rax12 = eval1(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&edi)), rsi, rdx);
    rdx13 = args;
    r12_14 = rax12;
    rax15 = *reinterpret_cast<void***>(rdx13);
    if (rax15) {
        while (1) {
            edi16 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rax15) - 0x7c);
            if (!edi16) {
                edi16 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax15 + 1));
            }
            *reinterpret_cast<int32_t*>(&rax17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            zf18 = reinterpret_cast<uint1_t>(edi16 == 0);
            *reinterpret_cast<unsigned char*>(&rax17) = zf18;
            args = rdx13 + rax17 * 8;
            if (!zf18) 
                break;
            if (*reinterpret_cast<signed char*>(&ebx11)) {
                eax19 = null(r12_14);
                edi16 = *reinterpret_cast<unsigned char*>(&eax19);
            }
            rax20 = eval1(edi16, rsi, rdx13);
            eax21 = null(r12_14);
            if (*reinterpret_cast<signed char*>(&eax21)) {
                if (*reinterpret_cast<void***>(r12_14) == 1) {
                    rdi22 = *reinterpret_cast<void***>(r12_14 + 8);
                    fun_24c0(rdi22);
                } else {
                    fun_2780(r12_14 + 8);
                }
                rdi23 = r12_14;
                r12_14 = rax20;
                fun_24c0(rdi23);
                eax24 = null(rax20);
                if (*reinterpret_cast<signed char*>(&eax24)) {
                    if (*reinterpret_cast<void***>(rax20) == 1) {
                        rdi25 = *reinterpret_cast<void***>(rax20 + 8);
                        fun_24c0(rdi25);
                    } else {
                        fun_2780(rax20 + 8);
                    }
                    fun_24c0(rax20);
                    rax26 = xmalloc(24);
                    *reinterpret_cast<int32_t*>(&rsi) = 0;
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    *reinterpret_cast<void***>(rax26) = reinterpret_cast<void**>(0);
                    r12_14 = rax26;
                    fun_27f0(rax26 + 8);
                }
                rdx13 = args;
                rax15 = *reinterpret_cast<void***>(rdx13);
                if (!rax15) 
                    break;
            } else {
                if (*reinterpret_cast<void***>(rax20) == 1) {
                    rdi27 = *reinterpret_cast<void***>(rax20 + 8);
                    fun_24c0(rdi27);
                } else {
                    fun_2780(rax20 + 8);
                }
                fun_24c0(rax20);
                rdx13 = args;
                rax15 = *reinterpret_cast<void***>(rdx13);
                if (!rax15) 
                    break;
            }
        }
    }
    return r12_14;
}