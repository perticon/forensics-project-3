void** eval5(uint32_t edi, void** rsi) {
    uint32_t r13d3;
    uint32_t ebx4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rdx8;
    uint32_t eax9;
    int64_t rdx10;
    uint1_t zf11;
    void** rax12;
    void** rax13;
    void** rdi14;
    void** rdi15;
    void** rdi16;
    void** rdi17;
    void** rdi18;

    r13d3 = edi;
    ebx4 = *reinterpret_cast<unsigned char*>(&edi);
    rax5 = eval6(ebx4, rsi);
    rcx6 = args;
    r12_7 = rax5;
    rdx8 = *reinterpret_cast<void***>(rcx6);
    if (rdx8) {
        do {
            eax9 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx8) - 58);
            if (!eax9) {
                eax9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx8 + 1));
            }
            *reinterpret_cast<int32_t*>(&rdx10) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
            zf11 = reinterpret_cast<uint1_t>(eax9 == 0);
            *reinterpret_cast<unsigned char*>(&rdx10) = zf11;
            args = rcx6 + rdx10 * 8;
            if (!zf11) 
                break;
            rax12 = eval6(ebx4, rsi);
            if (*reinterpret_cast<signed char*>(&r13d3)) {
                rsi = rax12;
                rax13 = docolon(r12_7, rsi);
                if (*reinterpret_cast<void***>(r12_7) == 1) {
                    rdi14 = *reinterpret_cast<void***>(r12_7 + 8);
                    fun_24c0(rdi14, rdi14);
                } else {
                    rdi15 = r12_7 + 8;
                    fun_2780(rdi15, rdi15);
                }
                rdi16 = r12_7;
                r12_7 = rax13;
                fun_24c0(rdi16, rdi16);
            }
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax12) == 1)) {
                rdi17 = rax12 + 8;
                fun_2780(rdi17, rdi17);
            } else {
                rdi18 = *reinterpret_cast<void***>(rax12 + 8);
                fun_24c0(rdi18, rdi18);
            }
            fun_24c0(rax12, rax12);
            rcx6 = args;
            rdx8 = *reinterpret_cast<void***>(rcx6);
        } while (rdx8);
    }
    return r12_7;
}