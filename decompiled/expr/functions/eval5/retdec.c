int32_t * eval5(bool evaluate) {
    int64_t v1 = evaluate ? 255 : 0; // 0x41eb
    int64_t v2 = eval6(v1); // 0x41f1
    if (args == NULL) {
        // 0x42a0
        return (int32_t *)v2;
    }
    int64_t v3 = v2;
    int64_t v4 = (int64_t)args;
    int32_t v5 = (int32_t)v4 % 256 - 58; // 0x4233
    int32_t v6 = v5 != 0 ? v5 : (int32_t)*(char *)&g36;
    int64_t v7 = 8 * (int64_t)(v6 == 0) + v4; // 0x4243
    *(int64_t *)&args = v7;
    int64_t v8 = v3; // 0x424e
    while (v6 == 0) {
        int64_t v9 = eval6(v1); // 0x4252
        int64_t v10 = v3; // 0x425d
        if (evaluate) {
            // 0x4270
            v10 = docolon(v3, v9, v7, v4);
            if (*(int32_t *)v3 == 1) {
                // 0x42b0
                function_24c0();
            } else {
                // 0x4285
                function_2780();
            }
            // 0x428f
            function_24c0();
        }
        // 0x425f
        if (*(int32_t *)v9 != 1) {
            // 0x4210
            function_2780();
        } else {
            // 0x4265
            function_24c0();
        }
        // 0x4219
        function_24c0();
        v8 = v10;
        if (args == NULL) {
            // break -> 0x42a0
            break;
        }
        v3 = v10;
        v4 = (int64_t)args;
        v5 = (int32_t)v4 % 256 - 58;
        v6 = v5 != 0 ? v5 : (int32_t)*(char *)&g36;
        v7 = 8 * (int64_t)(v6 == 0) + v4;
        *(int64_t *)&args = v7;
        v8 = v3;
    }
    // 0x42a0
    return (int32_t *)v8;
}