void require_more_args() {
    void** rax1;
    int64_t v2;
    int64_t rbx3;
    int1_t zf4;
    uint64_t rax5;
    void** r12_6;
    void** rax7;
    int64_t r12_8;
    void** rdi9;
    void** rax10;
    void** rax11;
    int32_t eax12;
    uint32_t eax13;
    void** rdx14;
    uint32_t ecx15;
    uint32_t eax16;
    int64_t rax17;
    uint32_t eax18;
    uint32_t eax19;
    int32_t eax20;
    void* rdx21;
    void** rbp22;
    struct s1* rdx23;
    int32_t eax24;
    int32_t eax25;

    rax1 = args;
    if (*reinterpret_cast<void***>(rax1)) {
        return;
    }
    quotearg_n_style();
    fun_2590();
    fun_2830();
    v2 = rbx3;
    zf4 = *reinterpret_cast<signed char*>(&__cxa_finalize + 2) == 0;
    if (zf4) 
        goto addr_2cb0_5;
    addr_2d07_6:
    rax5 = fun_25a0();
    r12_6 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&g10) + 2);
    strnlen1(r12_6, rax5, r12_6, rax5);
    rax7 = rpl_mbrtowc();
    g1a = rax7;
    if (rax7 == 0xffffffffffffffff) {
        g1a = reinterpret_cast<void**>(1);
        g22 = 0;
        *reinterpret_cast<signed char*>(&g8 + 6) = 1;
        goto r12_8;
    }
    if (rax7 == 0xfffffffffffffffe) {
        rdi9 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&g10) + 2);
        rax10 = fun_25b0(rdi9, rdi9);
        g22 = 0;
        g1a = rax10;
        *reinterpret_cast<signed char*>(&g8 + 6) = 1;
        goto r12_8;
    }
    if (rax7) 
        goto addr_2d5a_11;
    rax11 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&g10) + 2);
    g1a = reinterpret_cast<void**>(1);
    if (*reinterpret_cast<void***>(rax11)) {
        addr_2dd1_13:
        fun_2660("*iter->cur.ptr == '\\0'", "./lib/mbuiter.h", 0xab, "mbuiter_multi_next");
        goto addr_2df0_14;
    } else {
        eax12 = *reinterpret_cast<int32_t*>(&g26);
        if (eax12) {
            fun_2660("iter->cur.wc == 0", "./lib/mbuiter.h", 0xac, "mbuiter_multi_next");
            goto addr_2dd1_13;
        } else {
            addr_2d5a_11:
            g22 = 1;
            eax13 = fun_28c0(6, r12_6, 6, r12_6);
            if (eax13) {
                *reinterpret_cast<signed char*>(&__cxa_finalize + 2) = 0;
                *reinterpret_cast<signed char*>(&g8 + 6) = 1;
                goto r12_8;
            }
        }
    }
    addr_2ce1_18:
    *reinterpret_cast<signed char*>(&g8 + 6) = 1;
    goto r12_8;
    addr_2cb0_5:
    rdx14 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(&g10) + 2);
    ecx15 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx14));
    eax16 = ecx15;
    *reinterpret_cast<unsigned char*>(&eax16) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax16) >> 5);
    *reinterpret_cast<uint32_t*>(&rax17) = eax16 & 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
    eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0x18ba0) + reinterpret_cast<uint64_t>(rax17 * 4))) >> *reinterpret_cast<signed char*>(&ecx15);
    if (!(*reinterpret_cast<unsigned char*>(&eax18) & 1)) {
        eax19 = fun_28c0(6, 0x18ba0);
        if (!eax19) {
            addr_2df0_14:
            fun_2660("mbsinit (&iter->state)", "./lib/mbuiter.h", 0x8f, "mbuiter_multi_next");
        } else {
            *reinterpret_cast<signed char*>(&__cxa_finalize + 2) = 1;
            goto addr_2d07_6;
        }
    } else {
        g1a = reinterpret_cast<void**>(1);
        eax20 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx14));
        g22 = 1;
        *reinterpret_cast<int32_t*>(&g26) = eax20;
        goto addr_2ce1_18;
    }
    *reinterpret_cast<int32_t*>(&rdx21) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx21) + 4) = 0;
    rbp22 = g1899d;
    *reinterpret_cast<unsigned char*>(&rdx21) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(rbp22) == 45);
    rdx23 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rdx21) + reinterpret_cast<unsigned char>(rbp22));
    eax24 = rdx23->f0;
    do {
        if (eax24 - 48 > 9) 
            break;
        eax24 = rdx23->f1;
        rdx23 = reinterpret_cast<struct s1*>(&rdx23->f1);
    } while (*reinterpret_cast<signed char*>(&eax24));
    goto addr_2e50_25;
    goto v2;
    addr_2e50_25:
    eax25 = fun_2870("(&iter->state)", rbp22, 10, "mbuiter_multi_next");
    if (eax25) {
        fun_2830();
    } else {
        fun_24c0(rbp22, rbp22);
        g18995 = 0;
        goto v2;
    }
}