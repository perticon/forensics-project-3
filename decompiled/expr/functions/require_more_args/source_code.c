require_more_args (void)
{
  if (nomoreargs ())
    die (EXPR_INVALID, 0, _("syntax error: missing argument after %s"),
         quotearg_n_style (0, locale_quoting_style, *(args - 1)));
}