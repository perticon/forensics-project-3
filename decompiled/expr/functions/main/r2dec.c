int32_t main (int32_t argc, char ** argv) {
    rdi = argc;
    rsi = argv;
    r12 = 0x00018a54;
    rdi = *(rsi);
    rbx = rsi;
    set_program_name (*(rdi), rsi, rdx);
    setlocale (6, 0x00019709);
    bindtextdomain (r12, "/usr/local/share/locale");
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    *(obj.exit_failure) = 3;
    atexit ();
    rax = "Paul Eggert";
    rax = "James Youngman";
    rax = "Mike Parker";
    eax = 0;
    parse_long_options (ebp, rbx, "expr", "GNU coreutils", "9.1.17-a351f", dbg.usage);
    if (ebp <= 1) {
        goto label_1;
    }
    eax = strcmp (*((rbx + 8)), 0x00018b26);
    if (eax == 0) {
        if (ebp == 2) {
            goto label_1;
        }
        rbx += 8;
    }
    rbx += 8;
    *(obj.args) = rbx;
    rax = eval (1, rsi);
    rax = args;
    rdx = *(rax);
    if (rdx != 0) {
        goto label_2;
    }
    eax = *(rbp);
    if (eax == 0) {
        goto label_3;
    }
    eax--;
    if (eax != 0) {
        goto label_4;
    }
    puts (*((rbp + 8)));
    do {
label_0:
        rdi = rbp;
        al = null ();
        edi = (int32_t) al;
        exit (rdi);
label_3:
        rdi = stdout;
        rdx = rbp + 8;
        esi = 0xa;
        gmpz_out_str ();
        rdi = stdout;
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_5;
        }
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
    } while (1);
label_1:
    edx = 5;
    rax = dcgettext (0, "missing operand");
    eax = 0;
    error (0, 0, rax);
    usage (2, rsi, rdx, rcx, r8, r9);
label_5:
    esi = 0xa;
    overflow ();
    goto label_0;
label_2:
    esi = 8;
    edi = 0;
    rax = quotearg_n_style ();
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "syntax error: unexpected argument %s");
    rcx = r12;
    eax = 0;
    error (2, 0, rax);
label_4:
    return main_cold ();
}