void main(uint param_1,undefined8 *param_2)

{
  char *pcVar1;
  byte bVar2;
  int iVar3;
  int *piVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  exit_failure = 3;
  atexit(close_stdout);
  parse_long_options(param_1,param_2,&DAT_001189d5,"GNU coreutils","9.1.17-a351f",usage,
                     "Mike Parker","James Youngman","Paul Eggert",0);
  if (1 < param_1) {
    iVar3 = strcmp((char *)param_2[1],"--");
    if (iVar3 == 0) {
      if (param_1 == 2) goto LAB_00102ae1;
      param_2 = param_2 + 1;
    }
    args = param_2 + 1;
    piVar4 = (int *)eval(1);
    if (*args != 0) {
      uVar5 = quotearg_n_style(0,8);
      uVar6 = dcgettext(0,"syntax error: unexpected argument %s",5);
                    /* WARNING: Subroutine does not return */
      error(2,0,uVar6,uVar5);
    }
    if (*piVar4 == 0) {
      __gmpz_out_str(stdout,10,piVar4 + 2);
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = '\n';
      }
      else {
        __overflow(stdout,10);
      }
    }
    else {
      if (*piVar4 != 1) {
        main_cold();
        return;
      }
      puts(*(char **)(piVar4 + 2));
    }
    bVar2 = null();
                    /* WARNING: Subroutine does not return */
    exit((uint)bVar2);
  }
LAB_00102ae1:
  uVar5 = dcgettext(0,"missing operand",5);
                    /* WARNING: Subroutine does not return */
  error(0,0,uVar5);
}