main (int argc, char **argv)
{
  VALUE *v;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  initialize_exit_failure (EXPR_FAILURE);
  atexit (close_stdout);

  parse_long_options (argc, argv, PROGRAM_NAME, PACKAGE_NAME, VERSION,
                      usage, AUTHORS, (char const *) NULL);

  /* The above handles --help and --version.
     Since there is no other invocation of getopt, handle '--' here.  */
  unsigned int u_argc = argc;
  if (1 < u_argc && STREQ (argv[1], "--"))
    {
      --u_argc;
      ++argv;
    }

  if (u_argc <= 1)
    {
      error (0, 0, _("missing operand"));
      usage (EXPR_INVALID);
    }

  args = argv + 1;

  v = eval (true);
  if (!nomoreargs ())
    die (EXPR_INVALID, 0, _("syntax error: unexpected argument %s"),
         quotearg_n_style (0, locale_quoting_style, *args));

  printv (v);

  main_exit (null (v));
}