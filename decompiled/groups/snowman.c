
struct s0 {
    struct s0* f0;
    signed char f1;
    signed char f2;
    signed char[1] pad4;
    struct s0* f4;
    signed char[3] pad8;
    struct s0* f8;
    signed char[15] pad24;
    struct s0* f18;
};

int64_t fun_2430();

int64_t fun_2380(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2430();
    if (r8d > 10) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x74a0 + rax11 * 4) + 0x74a0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* g28;

struct s0* fun_2390();

struct s0* slotvec = reinterpret_cast<struct s0*>(0x70);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_24d0();

struct s2 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_2370(struct s0* rdi, struct s0* rsi, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

void fun_2450();

struct s0* quotearg_n_options(struct s0* rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    struct s0* rax8;
    struct s0* r15_9;
    struct s0* v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x47ef;
    rax8 = fun_2390();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<struct s0**>(&rax8->f0);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x66e1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x487b;
            fun_24d0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb0e0) {
                fun_2370(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x490a);
        }
        *reinterpret_cast<struct s0**>(&rax8->f0) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2450();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x7443);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0x743c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x7447);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0x7438);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2363() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_2373() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2040;

void fun_2383() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_2393() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_23a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_23b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_23c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x2090;

void fun_23d3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20a0;

void fun_23e3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20b0;

void fun_23f3() {
    __asm__("cli ");
    goto fclose;
}

int64_t getpwuid = 0x20c0;

void fun_2403() {
    __asm__("cli ");
    goto getpwuid;
}

int64_t bindtextdomain = 0x20d0;

void fun_2413() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20e0;

void fun_2423() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x20f0;

void fun_2433() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2100;

void fun_2443() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2110;

void fun_2453() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getuid = 0x2120;

void fun_2463() {
    __asm__("cli ");
    goto getuid;
}

int64_t getopt_long = 0x2130;

void fun_2473() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2483() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t getgrgid = 0x2150;

void fun_2493() {
    __asm__("cli ");
    goto getgrgid;
}

int64_t __overflow = 0x2160;

void fun_24a3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2170;

void fun_24b3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2180;

void fun_24c3() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2190;

void fun_24d3() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x21a0;

void fun_24e3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21b0;

void fun_24f3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21c0;

void fun_2503() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21d0;

void fun_2513() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21e0;

void fun_2523() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t getpwnam = 0x21f0;

void fun_2533() {
    __asm__("cli ");
    goto getpwnam;
}

int64_t memcpy = 0x2200;

void fun_2543() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2210;

void fun_2553() {
    __asm__("cli ");
    goto fileno;
}

int64_t getgid = 0x2220;

void fun_2563() {
    __asm__("cli ");
    goto getgid;
}

int64_t getgroups = 0x2230;

void fun_2573() {
    __asm__("cli ");
    goto getgroups;
}

int64_t malloc = 0x2240;

void fun_2583() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_2593() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_25a3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t getegid = 0x2270;

void fun_25b3() {
    __asm__("cli ");
    goto getegid;
}

int64_t __freading = 0x2280;

void fun_25c3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2290;

void fun_25d3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22a0;

void fun_25e3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22b0;

void fun_25f3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22c0;

void fun_2603() {
    __asm__("cli ");
    goto error;
}

int64_t getgrouplist = 0x22d0;

void fun_2613() {
    __asm__("cli ");
    goto getgrouplist;
}

int64_t fseeko = 0x22e0;

void fun_2623() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x22f0;

void fun_2633() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2300;

void fun_2643() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2310;

void fun_2653() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2320;

void fun_2663() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2330;

void fun_2673() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2340;

void fun_2683() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2350;

void fun_2693() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s0* fun_25e0(int64_t rdi, ...);

void fun_2410(int64_t rdi, int64_t rsi);

void fun_23e0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2470(int64_t rdi, struct s0* rsi, struct s0* rdx);

int32_t optind = 0;

int32_t fun_2460(int64_t rdi, struct s0* rsi);

struct s0* fun_2420();

void fun_2600();

uint32_t fun_25b0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9);

int32_t fun_2560(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9);

struct s4 {
    signed char[16] pad16;
    struct s0* f10;
    signed char[3] pad20;
    int32_t f14;
};

struct s4* fun_2530(struct s0* rdi, struct s0* rsi);

void fun_25f0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18, int64_t a19, int64_t a20);

uint32_t print_group_list();

struct s5 {
    signed char[40] pad40;
    signed char* f28;
    signed char* f30;
};

struct s5* stdout = reinterpret_cast<struct s5*>(0);

void fun_24a0(struct s5* rdi, struct s0* rsi, ...);

struct s0* quote(struct s0* rdi, struct s0* rsi, void* rdx, struct s0* rcx, int64_t r8, int64_t r9);

struct s0* Version = reinterpret_cast<struct s0*>(0xd5);

void version_etc(struct s5* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_2640();

void usage();

int64_t fun_26e3(int32_t edi, struct s0* rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r15_5;
    int64_t v6;
    int64_t r14_7;
    int64_t v8;
    int64_t r13_9;
    int64_t v10;
    int64_t r12_11;
    struct s0* r12_12;
    int64_t v13;
    int64_t rbp14;
    int32_t ebp15;
    int64_t v16;
    int64_t rbx17;
    struct s0* rbx18;
    struct s0* rdi19;
    int64_t r8_20;
    struct s0* rdx21;
    struct s0* rsi22;
    struct s0* rcx23;
    int64_t rdi24;
    int32_t eax25;
    struct s0* rax26;
    int32_t eax27;
    uint32_t eax28;
    int32_t eax29;
    struct s0* rax30;
    void* rdx31;
    struct s0* rdi32;
    struct s4* rax33;
    int32_t r15d34;
    struct s0* r14d35;
    int64_t rax36;
    struct s0* rdx37;
    int64_t r9_38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;
    uint32_t eax46;
    struct s5* rdi47;
    signed char* rax48;
    int64_t rax49;
    struct s0* rdi50;
    struct s0* rax51;
    int64_t rax52;
    struct s5* rdi53;
    struct s5* rdi54;
    uint32_t eax55;
    signed char* rax56;
    uint32_t r12d57;
    int64_t rax58;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r15_5;
    v6 = r14_7;
    v8 = r13_9;
    v10 = r12_11;
    r12_12 = reinterpret_cast<struct s0*>(0x7861);
    v13 = rbp14;
    ebp15 = edi;
    v16 = rbx17;
    rbx18 = rsi;
    rdi19 = *reinterpret_cast<struct s0**>(&rsi->f0);
    set_program_name(rdi19);
    fun_25e0(6, 6);
    fun_2410("coreutils", "/usr/local/share/locale");
    fun_23e0("coreutils", "/usr/local/share/locale");
    atexit(0x3100, "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&r8_20) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_20) + 4) = 0;
    rdx21 = reinterpret_cast<struct s0*>(0x7861);
    rsi22 = rbx18;
    rcx23 = reinterpret_cast<struct s0*>("=q");
    *reinterpret_cast<int32_t*>(&rdi24) = ebp15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    eax25 = fun_2470(rdi24, rsi22, 0x7861);
    if (eax25 == -1) {
        addr_27b0_2:
        *reinterpret_cast<int32_t*>(&rdx21) = optind;
        *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx21) == ebp15) {
            rax26 = fun_2390();
            *reinterpret_cast<struct s0**>(&rax26->f0) = reinterpret_cast<struct s0*>(0);
            rbx18 = rax26;
            eax27 = fun_2460(rdi24, rsi22);
            if (eax27 == -1) {
                addr_296a_4:
                if (*reinterpret_cast<struct s0**>(&rbx18->f0)) {
                    fun_2420();
                    fun_2600();
                    goto addr_2997_6;
                }
            } else {
                *reinterpret_cast<struct s0**>(&rbx18->f0) = reinterpret_cast<struct s0*>(0);
                eax28 = fun_25b0(rdi24, rsi22, rdx21, rcx23, r8_20, "James Youngman");
                *reinterpret_cast<uint32_t*>(&r12_12) = eax28;
                if (eax28 != 0xffffffff) 
                    goto addr_28ef_8;
            }
            while (!*reinterpret_cast<struct s0**>(&rbx18->f0)) {
                addr_28ef_8:
                *reinterpret_cast<struct s0**>(&rbx18->f0) = reinterpret_cast<struct s0*>(0);
                eax29 = fun_2560(rdi24, rsi22, rdx21, rcx23, r8_20, "James Youngman");
                if (eax29 != -1) 
                    goto addr_2905_10;
                addr_2997_6:
                if (!*reinterpret_cast<struct s0**>(&rbx18->f0)) 
                    goto addr_2905_10;
                rax30 = fun_2420();
                rsi22 = *reinterpret_cast<struct s0**>(&rbx18->f0);
                *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rdi24) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
                rdx21 = rax30;
                fun_2600();
            }
        } else {
            *reinterpret_cast<uint32_t*>(&r12_12) = 1;
            if (*reinterpret_cast<int32_t*>(&rdx21) < ebp15) {
                do {
                    rdx31 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx21)));
                    rdi32 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbx18) + reinterpret_cast<int64_t>(rdx31) * 8);
                    rax33 = fun_2530(rdi32, rsi22);
                    if (rax33) {
                        r15d34 = rax33->f14;
                        r14d35 = rax33->f10;
                        rax36 = optind;
                        rdx37 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbx18) + rax36 * 8);
                        fun_25f0(1, "%s : ", rdx37, rcx23, r8_20, r9_38, v39, v16, v13, v10, v8, v6, v4, v3, v40, v41, v42, v43, v44, v45);
                        *reinterpret_cast<int32_t*>(&rcx23) = r15d34;
                        *reinterpret_cast<int32_t*>(&rcx23 + 4) = 0;
                        rsi22 = r14d35;
                        *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r9_38) = 32;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_38) + 4) = 0;
                        *reinterpret_cast<int32_t*>(&r8_20) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_20) + 4) = 0;
                        eax46 = print_group_list();
                        rdi47 = stdout;
                        if (!*reinterpret_cast<signed char*>(&eax46)) {
                            *reinterpret_cast<uint32_t*>(&r12_12) = 0;
                        }
                        rax48 = rdi47->f28;
                        if (reinterpret_cast<uint64_t>(rax48) >= reinterpret_cast<uint64_t>(rdi47->f30)) {
                            rsi22 = reinterpret_cast<struct s0*>(10);
                            *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                            fun_24a0(rdi47, 10, rdi47, 10);
                        } else {
                            rdi47->f28 = rax48 + 1;
                            *rax48 = 10;
                        }
                    } else {
                        rax49 = optind;
                        rdi50 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbx18) + rax49 * 8);
                        rax51 = quote(rdi50, rsi22, rdx31, rcx23, r8_20, r9_38);
                        fun_2420();
                        rcx23 = rax51;
                        rsi22 = reinterpret_cast<struct s0*>(0);
                        *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&r12_12) = 0;
                        fun_2600();
                    }
                    *reinterpret_cast<int32_t*>(&rax52) = optind;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax52) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdx21) = static_cast<int32_t>(rax52 + 1);
                    optind = *reinterpret_cast<int32_t*>(&rdx21);
                } while (*reinterpret_cast<int32_t*>(&rdx21) < ebp15);
                goto addr_2940_21;
            } else {
                goto addr_2940_21;
            }
        }
    } else {
        if (eax25 == 0xffffff7d) {
            rdi53 = stdout;
            r9_38 = reinterpret_cast<int64_t>("James Youngman");
            rcx23 = Version;
            r8_20 = reinterpret_cast<int64_t>("David MacKenzie");
            rdx21 = reinterpret_cast<struct s0*>("GNU coreutils");
            rsi22 = reinterpret_cast<struct s0*>("groups");
            version_etc(rdi53, "groups", "GNU coreutils", rcx23, "David MacKenzie", "James Youngman", 0, "=q");
            eax25 = fun_2640();
        }
        if (eax25 == 0xffffff7e) 
            goto addr_2963_26; else 
            goto addr_27a6_27;
    }
    fun_2420();
    *reinterpret_cast<int32_t*>(&rdi54) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
    fun_2600();
    goto addr_29f1_29;
    addr_2905_10:
    eax55 = print_group_list();
    rdi54 = stdout;
    *reinterpret_cast<uint32_t*>(&r12_12) = eax55;
    rax56 = rdi54->f28;
    if (reinterpret_cast<uint64_t>(rax56) >= reinterpret_cast<uint64_t>(rdi54->f30)) {
        addr_29f1_29:
        fun_24a0(rdi54, 10, rdi54, 10);
    } else {
        rdi54->f28 = rax56 + 1;
        *rax56 = 10;
    }
    addr_2940_21:
    r12d57 = *reinterpret_cast<uint32_t*>(&r12_12) ^ 1;
    *reinterpret_cast<uint32_t*>(&rax58) = *reinterpret_cast<unsigned char*>(&r12d57);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax58) + 4) = 0;
    return rax58;
    addr_2963_26:
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    usage();
    goto addr_296a_4;
    addr_27a6_27:
    *reinterpret_cast<int32_t*>(&rdi24) = 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    usage();
    goto addr_27b0_2;
}

int64_t __libc_start_main = 0;

void fun_2a03() {
    __asm__("cli ");
    __libc_start_main(0x26e0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_2360(int64_t rdi);

int64_t fun_2aa3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2360(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2ae3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_24f0(struct s0* rdi, struct s5* rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_2510(int64_t rdi);

int32_t fun_23a0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s5* stderr = reinterpret_cast<struct s5*>(0);

void fun_2660(struct s5* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2af3(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    int64_t r8_7;
    int64_t r9_8;
    struct s5* r12_9;
    struct s0* rax10;
    struct s5* r12_11;
    struct s0* rax12;
    struct s5* r12_13;
    struct s0* rax14;
    int32_t eax15;
    struct s0* r13_16;
    struct s0* rax17;
    int64_t r8_18;
    int64_t r9_19;
    struct s0* rax20;
    int32_t eax21;
    struct s0* rax22;
    int64_t r8_23;
    int64_t r9_24;
    struct s0* rax25;
    int64_t r8_26;
    int64_t r9_27;
    struct s0* rax28;
    int32_t eax29;
    struct s0* rax30;
    int64_t r8_31;
    int64_t r9_32;
    struct s5* r15_33;
    struct s0* rax34;
    struct s0* rax35;
    int64_t r8_36;
    int64_t r9_37;
    struct s0* rax38;
    struct s5* rdi39;
    struct s0* r8_40;
    struct s0* r9_41;
    int64_t v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;
    int64_t v46;
    int64_t v47;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2420();
            fun_25f0(1, rax5, r12_2, rcx6, r8_7, r9_8, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_9 = stdout;
            rax10 = fun_2420();
            fun_24f0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2420();
            fun_24f0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2420();
            fun_24f0(rax14, r12_13, 5, rcx6);
            do {
                if (1) 
                    break;
                eax15 = fun_2510("groups");
            } while (eax15);
            r13_16 = v4;
            if (!r13_16) {
                rax17 = fun_2420();
                fun_25f0(1, rax17, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_18, r9_19, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax20 = fun_25e0(5);
                if (!rax20 || (eax21 = fun_23a0(rax20, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax21)) {
                    rax22 = fun_2420();
                    r13_16 = reinterpret_cast<struct s0*>("groups");
                    fun_25f0(1, rax22, "https://www.gnu.org/software/coreutils/", "groups", r8_23, r9_24, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_16 = reinterpret_cast<struct s0*>("groups");
                    goto addr_2e00_9;
                }
            } else {
                rax25 = fun_2420();
                fun_25f0(1, rax25, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_26, r9_27, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax28 = fun_25e0(5);
                if (!rax28 || (eax29 = fun_23a0(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax29)) {
                    addr_2d06_11:
                    rax30 = fun_2420();
                    fun_25f0(1, rax30, "https://www.gnu.org/software/coreutils/", "groups", r8_31, r9_32, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_16 == "groups")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x7861);
                    }
                } else {
                    addr_2e00_9:
                    r15_33 = stdout;
                    rax34 = fun_2420();
                    fun_24f0(rax34, r15_33, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_2d06_11;
                }
            }
            rax35 = fun_2420();
            rcx6 = r12_2;
            fun_25f0(1, rax35, r13_16, rcx6, r8_36, r9_37, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            addr_2b4e_14:
            fun_2640();
        }
    } else {
        rax38 = fun_2420();
        rdi39 = stderr;
        rcx6 = r12_2;
        fun_2660(rdi39, 1, rax38, rcx6, r8_40, r9_41, v42, v43, v44, v45, v46, v47);
        goto addr_2b4e_14;
    }
}

struct s0* umaxtostr(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

struct s0** fun_2490(int64_t rdi);

int64_t fun_2e33(int32_t edi, int32_t esi) {
    int32_t r12d3;
    struct s0* rbp4;
    struct s0* rdx5;
    struct s0* rcx6;
    struct s0* rax7;
    struct s0* rdi8;
    int64_t rdi9;
    struct s0** rax10;
    struct s0* rax11;
    struct s5* rsi12;
    int64_t rax13;

    __asm__("cli ");
    r12d3 = 1;
    *reinterpret_cast<int32_t*>(&rbp4) = edi;
    *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
    if (!*reinterpret_cast<signed char*>(&esi)) {
        addr_2e90_2:
        rax7 = umaxtostr(rbp4, 0xb0b0, rdx5, rcx6);
        rdi8 = rax7;
    } else {
        *reinterpret_cast<int32_t*>(&rdi9) = *reinterpret_cast<int32_t*>(&rbp4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
        rax10 = fun_2490(rdi9);
        if (!rax10) {
            r12d3 = 0;
            rax11 = fun_2420();
            rcx6 = rbp4;
            rdx5 = rax11;
            fun_2600();
            goto addr_2e90_2;
        } else {
            rdi8 = *rax10;
            r12d3 = esi;
        }
    }
    rsi12 = stdout;
    fun_24f0(rdi8, rsi12, rdx5, rcx6);
    *reinterpret_cast<int32_t*>(&rax13) = r12d3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    return rax13;
}

struct s6 {
    signed char[20] pad20;
    uint32_t f14;
};

struct s6* fun_2400(int64_t rdi);

signed char print_group(int64_t rdi, struct s0* rsi);

int32_t xgetgroups(struct s0* rdi, struct s0* rsi, void* rdx);

int64_t fun_2ec3(struct s0* rdi, int32_t esi, uint32_t edx, struct s0* rcx, int64_t r8, int64_t r9) {
    int32_t r12d7;
    uint32_t ebp8;
    uint32_t ebx9;
    void* rsp10;
    int32_t v11;
    unsigned char v12;
    struct s0* rax13;
    struct s0* v14;
    struct s6* r15_15;
    int32_t r13d16;
    int64_t rdi17;
    struct s6* rax18;
    uint32_t r12d19;
    int64_t rdi20;
    struct s0* rsi21;
    signed char al22;
    void* rsp23;
    struct s5* rdi24;
    signed char* rax25;
    struct s0* rsi26;
    uint32_t edx27;
    struct s0* rsi28;
    int64_t rdi29;
    signed char al30;
    struct s0* rsi31;
    void* rdx32;
    int32_t eax33;
    struct s0* rdi34;
    struct s0* v35;
    void* r15_36;
    void* r14_37;
    uint32_t v38;
    struct s5* rdi39;
    signed char* rax40;
    struct s0* rsi41;
    uint32_t ecx42;
    int64_t rdi43;
    void* v44;
    signed char al45;
    struct s0* v46;
    void* rax47;
    int64_t rax48;

    __asm__("cli ");
    r12d7 = *reinterpret_cast<int32_t*>(&r8);
    ebp8 = *reinterpret_cast<uint32_t*>(&rcx);
    ebx9 = edx;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40);
    v11 = *reinterpret_cast<int32_t*>(&r9);
    v12 = *reinterpret_cast<unsigned char*>(&r9);
    rax13 = g28;
    v14 = rax13;
    if (!rdi) {
        *reinterpret_cast<int32_t*>(&r15_15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        r13d16 = 1;
    } else {
        *reinterpret_cast<int32_t*>(&rdi17) = esi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
        rax18 = fun_2400(rdi17);
        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
        r15_15 = rax18;
        *reinterpret_cast<unsigned char*>(&r13d16) = reinterpret_cast<uint1_t>(!!rax18);
    }
    r12d19 = *reinterpret_cast<unsigned char*>(&r12d7);
    *reinterpret_cast<uint32_t*>(&rdi20) = ebx9;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rsi21) = r12d19;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    al22 = print_group(rdi20, rsi21);
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
    if (!al22) {
        r13d16 = 0;
    }
    if (ebx9 != ebp8) {
        rdi24 = stdout;
        rax25 = rdi24->f28;
        if (reinterpret_cast<uint64_t>(rax25) >= reinterpret_cast<uint64_t>(rdi24->f30)) {
            *reinterpret_cast<uint32_t*>(&rsi26) = *reinterpret_cast<unsigned char*>(&v11);
            *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
            fun_24a0(rdi24, rsi26);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        } else {
            rdi24->f28 = rax25 + 1;
            edx27 = *reinterpret_cast<unsigned char*>(&v11);
            *rax25 = *reinterpret_cast<signed char*>(&edx27);
        }
        *reinterpret_cast<uint32_t*>(&rsi28) = r12d19;
        *reinterpret_cast<int32_t*>(&rsi28 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdi29) = ebp8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
        al30 = print_group(rdi29, rsi28);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        if (!al30) {
            r13d16 = 0;
        }
    }
    *reinterpret_cast<uint32_t*>(&rsi31) = ebp8;
    *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
    if (r15_15) {
        *reinterpret_cast<uint32_t*>(&rsi31) = r15_15->f14;
        *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
    }
    rdx32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) + 16);
    eax33 = xgetgroups(rdi, rsi31, rdx32);
    if (eax33 < 0) {
        fun_2390();
        if (!rdi) {
            fun_2420();
            fun_2600();
        } else {
            quote(rdi, rsi31, rdx32, rcx, r8, r9);
            fun_2420();
            fun_2600();
        }
        r13d16 = 0;
    } else {
        rdi34 = v35;
        *reinterpret_cast<int32_t*>(&r15_36) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_36) + 4) = 0;
        r14_37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(static_cast<int64_t>(eax33)) << 2);
        v38 = v12;
        if (eax33) {
            do {
                if (*reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(rdi34) + reinterpret_cast<uint64_t>(r15_36)) != ebx9 && *reinterpret_cast<uint32_t*>(reinterpret_cast<unsigned char>(rdi34) + reinterpret_cast<uint64_t>(r15_36)) != ebp8) {
                    rdi39 = stdout;
                    rax40 = rdi39->f28;
                    if (reinterpret_cast<uint64_t>(rax40) >= reinterpret_cast<uint64_t>(rdi39->f30)) {
                        *reinterpret_cast<uint32_t*>(&rsi41) = v38;
                        *reinterpret_cast<int32_t*>(&rsi41 + 4) = 0;
                        fun_24a0(rdi39, rsi41);
                    } else {
                        rdi39->f28 = rax40 + 1;
                        ecx42 = v12;
                        *rax40 = *reinterpret_cast<signed char*>(&ecx42);
                    }
                    *reinterpret_cast<uint32_t*>(&rsi31) = r12d19;
                    *reinterpret_cast<int32_t*>(&rsi31 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&rdi43) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(v44) + reinterpret_cast<uint64_t>(r15_36));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi43) + 4) = 0;
                    al45 = print_group(rdi43, rsi31);
                    rdi34 = v46;
                    if (!al45) {
                        r13d16 = 0;
                    }
                }
                r15_36 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_36) + 4);
            } while (r14_37 != r15_36);
        }
        fun_2370(rdi34, rsi31);
    }
    rax47 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v14) - reinterpret_cast<unsigned char>(g28));
    if (rax47) {
        fun_2450();
    } else {
        *reinterpret_cast<int32_t*>(&rax48) = r13d16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
        return rax48;
    }
}

int64_t file_name = 0;

void fun_30e3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_30f3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s5* rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_23b0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_3103() {
    struct s5* rdi1;
    int32_t eax2;
    struct s0* rax3;
    int1_t zf4;
    struct s0* rbx5;
    struct s5* rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2390(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax3->f0) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2420();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3193_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<struct s0**>(&rsi11) = *reinterpret_cast<struct s0**>(&rbx5->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2600();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3193_5:
        *reinterpret_cast<struct s0**>(&rsi11) = *reinterpret_cast<struct s0**>(&rbx5->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2600();
    }
}

struct s7 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_31b3(uint64_t rdi, struct s7* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

void fun_2650(struct s0* rdi, int64_t rsi, int64_t rdx, struct s5* rcx);

struct s8 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s8* fun_24b0();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_3213(struct s0* rdi) {
    struct s5* rcx2;
    struct s0* rbx3;
    struct s8* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2650("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2380("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24b0();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_49b3(int64_t rdi) {
    int64_t rbp2;
    struct s0* rax3;
    struct s0* r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2390();
    r12d4 = *reinterpret_cast<struct s0**>(&rax3->f0);
    if (!rbp2) {
        rbp2 = 0xb1e0;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<struct s0**>(&rax3->f0) = r12d4;
    return;
}

int64_t fun_49f3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb1e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4a13(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb1e0);
    }
    *rdi = esi;
    return 0xb1e0;
}

int64_t fun_4a33(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb1e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s9 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_4a73(struct s9* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xb1e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s10 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s10* fun_4a93(struct s10* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s10*>(0xb1e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x26aa;
    if (!rdx) 
        goto 0x26aa;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb1e0;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_4ad3(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    struct s11* rbx6;
    struct s0* rax7;
    struct s0* r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s11*>(0xb1e0);
    }
    rax7 = fun_2390();
    r15d8 = *reinterpret_cast<struct s0**>(&rax7->f0);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x4b06);
    *reinterpret_cast<struct s0**>(&rax7->f0) = r15d8;
    return rax13;
}

struct s12 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_4b53(int64_t rdi, int64_t rsi, struct s0** rdx, struct s12* rcx) {
    struct s12* rbx5;
    struct s0* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    struct s0* v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s12*>(0xb1e0);
    }
    rax6 = fun_2390();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<struct s0**>(&rax6->f0);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x4b81);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x4bdc);
    *reinterpret_cast<struct s0**>(&rax6->f0) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_4c43() {
    __asm__("cli ");
}

struct s0* gb078 = reinterpret_cast<struct s0*>(0xe0);

int64_t slotvec0 = 0x100;

void fun_4c53() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rsi7;
    struct s0* rdi8;
    struct s0* rsi9;
    struct s0* rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2370(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = r12_2->f8;
    if (rdi8 != 0xb0e0) {
        fun_2370(rdi8, rsi9);
        gb078 = reinterpret_cast<struct s0*>(0xb0e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb070) {
        fun_2370(r12_2, rsi10);
        slotvec = reinterpret_cast<struct s0*>(0xb070);
    }
    nslots = 1;
    return;
}

void fun_4cf3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4d13() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4d23(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4d43(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_4d63(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26b0;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_4df3(struct s0* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x26b5;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s0* fun_4e83(int32_t edi, int64_t rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x26ba;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2450();
    } else {
        return rax5;
    }
}

struct s0* fun_4f13(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26bf;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_4fa3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6230]");
    __asm__("movdqa xmm1, [rip+0x6238]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6221]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2450();
    } else {
        return rax10;
    }
}

struct s0* fun_5043(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6190]");
    __asm__("movdqa xmm1, [rip+0x6198]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6181]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s0* fun_50e3(int64_t rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x60f0]");
    __asm__("movdqa xmm1, [rip+0x60f8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x60d9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2450();
    } else {
        return rax3;
    }
}

struct s0* fun_5173(int64_t rdi, int64_t rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6060]");
    __asm__("movdqa xmm1, [rip+0x6068]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6056]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2450();
    } else {
        return rax4;
    }
}

struct s0* fun_5203(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26c4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_52a3(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5f2a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5f22]");
    __asm__("movdqa xmm2, [rip+0x5f2a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26c9;
    if (!rdx) 
        goto 0x26c9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

struct s0* fun_5343(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5e8a]");
    __asm__("movdqa xmm1, [rip+0x5e92]");
    __asm__("movdqa xmm2, [rip+0x5e9a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ce;
    if (!rdx) 
        goto 0x26ce;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2450();
    } else {
        return rax9;
    }
}

struct s0* fun_53f3(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5dda]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5dd2]");
    __asm__("movdqa xmm2, [rip+0x5dda]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26d3;
    if (!rsi) 
        goto 0x26d3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax6;
    }
}

struct s0* fun_5493(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5d3a]");
    __asm__("movdqa xmm1, [rip+0x5d42]");
    __asm__("movdqa xmm2, [rip+0x5d4a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26d8;
    if (!rsi) 
        goto 0x26d8;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2450();
    } else {
        return rax7;
    }
}

void fun_5533() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5543(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5563() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5583(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s13 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2520(int64_t rdi, struct s5* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_55a3(struct s5* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s13* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2660(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2660(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2420();
    fun_2660(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2520(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2420();
    fun_2660(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2520(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2420();
        fun_2660(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7b08 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7b08;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_5a13() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s14 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_5a33(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s14* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s14* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2450();
    } else {
        return;
    }
}

void fun_5ad3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_5b76_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_5b80_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2450();
    } else {
        return;
    }
    addr_5b76_5:
    goto addr_5b80_7;
}

void fun_5bb3() {
    struct s5* rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    int64_t r8_8;
    int64_t r9_9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    int64_t v20;
    int64_t v21;
    int64_t v22;
    struct s0* rax23;
    int64_t r8_24;
    int64_t r9_25;
    int64_t v26;
    int64_t v27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;
    int64_t v38;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2520(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2420();
    fun_25f0(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22);
    rax23 = fun_2420();
    fun_25f0(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_24, r9_25, v26, __return_address(), v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
    fun_2420();
    goto fun_25f0;
}

int64_t fun_23d0();

void xalloc_die();

void fun_5c53(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

struct s0* fun_2580(signed char* rdi);

void fun_5c93(signed char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_2580(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5cb3(signed char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_2580(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5cd3(signed char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_2580(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

struct s0* fun_25d0(struct s0* rdi);

void fun_5cf3(struct s0* rdi, int64_t rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_25d0(rdi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5d23(struct s0* rdi, int64_t rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_25d0(rdi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5d53(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5d93() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5dd3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5e03(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5e53(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23d0();
            if (rax5) 
                break;
            addr_5e9d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_5e9d_5;
        rax8 = fun_23d0();
        if (rax8) 
            goto addr_5e86_9;
        if (rbx4) 
            goto addr_5e9d_5;
        addr_5e86_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_5ee3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23d0();
            if (rax8) 
                break;
            addr_5f2a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_5f2a_5;
        rax11 = fun_23d0();
        if (rax11) 
            goto addr_5f12_9;
        if (!rbx6) 
            goto addr_5f12_9;
        if (r12_4) 
            goto addr_5f2a_5;
        addr_5f12_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_5f73(struct s0* rdi, void** rsi, struct s0* rdx, void* rcx, uint64_t r8) {
    struct s0* r13_6;
    struct s0* rdi7;
    void** r12_8;
    void* rsi9;
    void* rcx10;
    void* rbx11;
    void* rax12;
    void* rbp13;
    void* rbp14;
    struct s0* rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void*>((reinterpret_cast<int64_t>(rcx10) >> 1) + reinterpret_cast<uint64_t>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<int64_t>(rbx11) <= reinterpret_cast<int64_t>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_601d_9:
            rbx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_6030_10:
                *r12_8 = reinterpret_cast<void*>(0);
            }
            addr_5fd0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbx11) - reinterpret_cast<uint64_t>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_5ff6_12;
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6044_14;
            if (reinterpret_cast<int64_t>(rcx10) <= reinterpret_cast<int64_t>(rsi9)) 
                goto addr_5fed_16;
            if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) 
                goto addr_6044_14;
            addr_5fed_16:
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6044_14;
            addr_5ff6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25d0(rdi7);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6044_14;
            if (!rbp13) 
                break;
            addr_6044_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<int64_t>(rbp13) <= reinterpret_cast<int64_t>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_601d_9;
        } else {
            if (!r13_6) 
                goto addr_6030_10;
            goto addr_5fd0_11;
        }
    }
}

int64_t fun_2500();

void fun_6073() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_60a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_60d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_60f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2540(signed char* rdi, struct s0* rsi, signed char* rdx);

void fun_6113(int64_t rdi, signed char* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2580(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2540;
    }
}

void fun_6153(int64_t rdi, signed char* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2580(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2540;
    }
}

struct s15 {
    signed char[1] pad1;
    signed char f1;
};

void fun_6193(int64_t rdi, struct s15* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2580(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<uint64_t>(rsi)) = 0;
        goto fun_2540;
    }
}

struct s0* fun_2440(struct s0* rdi, ...);

void fun_61d3(struct s0* rdi) {
    struct s0* rax2;
    struct s0* rax3;

    __asm__("cli ");
    rax2 = fun_2440(rdi);
    rax3 = fun_2580(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2540;
    }
}

void fun_6213() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2420();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2600();
    fun_2380(rdi1);
}

int32_t mgetgroups();

int64_t fun_6253() {
    int32_t eax1;
    struct s0* rax2;
    int64_t rax3;

    __asm__("cli ");
    eax1 = mgetgroups();
    if (eax1 != -1 || (rax2 = fun_2390(), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax2->f0) == 12))) {
        *reinterpret_cast<int32_t*>(&rax3) = eax1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    } else {
        xalloc_die();
    }
}

int64_t fun_23c0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_6283(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    struct s0* rax5;
    struct s0* rax6;

    __asm__("cli ");
    rax2 = fun_23c0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_62de_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2390();
            *reinterpret_cast<struct s0**>(&rax5->f0) = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_62de_3;
            rax6 = fun_2390();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax6->f0) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s16 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2550(struct s16* rdi);

int32_t fun_25c0(struct s16* rdi);

int64_t fun_24c0(int64_t rdi, ...);

int32_t rpl_fflush(struct s16* rdi);

int64_t fun_23f0(struct s16* rdi);

int64_t fun_62f3(struct s16* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    struct s0* rax8;
    struct s0* r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2550(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25c0(rdi);
        if (!(eax3 && (eax4 = fun_2550(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24c0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2390();
            r12d9 = *reinterpret_cast<struct s0**>(&rax8->f0);
            rax10 = fun_23f0(rdi);
            if (r12d9) {
                *reinterpret_cast<struct s0**>(&rax8->f0) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_23f0;
}

void rpl_fseeko(struct s16* rdi);

void fun_6383(struct s16* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25c0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_63d3(struct s16* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2550(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24c0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_25a0(int64_t rdi);

signed char* fun_6453() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25a0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2480(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6493(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2480(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2450();
    } else {
        return r12_7;
    }
}

int64_t fun_2570();

int32_t fun_2610(int64_t rdi, int64_t rsi, struct s0* rdx, void* rcx);

int64_t fun_6523(int64_t rdi, struct s0* esi, struct s0** rdx) {
    struct s0* ebp4;
    struct s0** v5;
    struct s0* rax6;
    struct s0* v7;
    int64_t rax8;
    int64_t r12_9;
    struct s0* rax10;
    struct s0* rax11;
    int32_t r12d12;
    struct s0* rax13;
    struct s0* r13_14;
    struct s0* rax15;
    int64_t rbx16;
    int32_t v17;
    struct s0* rax18;
    struct s0* r14_19;
    void* r13_20;
    int64_t rsi21;
    int32_t eax22;
    uint64_t rsi23;
    struct s0* rsi24;
    int64_t rax25;
    struct s0* rax26;
    void* rax27;
    int64_t rax28;
    struct s0* rsi29;
    int64_t rax30;
    struct s0* esi31;
    struct s0** rcx32;
    struct s0** rax33;
    int64_t rax34;
    struct s0* rax35;

    __asm__("cli ");
    ebp4 = esi;
    v5 = rdx;
    rax6 = g28;
    v7 = rax6;
    if (!rdi) {
        rax8 = fun_2570();
        *reinterpret_cast<int32_t*>(&r12_9) = *reinterpret_cast<int32_t*>(&rax8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_9) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax8) < 0) {
            rax10 = fun_2390();
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax10->f0) == 38) || (rax11 = fun_2580(4), rax11 == 0)) {
                addr_6764_4:
                r12d12 = -1;
            } else {
                r12d12 = 0;
                *reinterpret_cast<struct s0**>(&rax11->f0) = ebp4;
                *reinterpret_cast<unsigned char*>(&r12d12) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(ebp4 == 0xffffffff));
                *v5 = rax11;
            }
        } else {
            if (!*reinterpret_cast<int32_t*>(&rax8) || ebp4 != 0xffffffff) {
                rax13 = fun_2580(reinterpret_cast<uint64_t>(static_cast<int64_t>(static_cast<int32_t>(r12_9 + 1))) << 2);
                r13_14 = rax13;
                if (!rax13) 
                    goto addr_6764_4;
                if (ebp4 == 0xffffffff) 
                    goto addr_676f_9; else 
                    goto addr_668d_10;
            } else {
                rax15 = fun_2580(reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax8))) << 2);
                r13_14 = rax15;
                if (!rax15) 
                    goto addr_6764_4; else 
                    goto addr_6746_12;
            }
        }
    } else {
        rbx16 = rdi;
        v17 = 10;
        rax18 = fun_2580(40);
        r14_19 = rax18;
        if (!rax18) 
            goto addr_6764_4;
        r12d12 = 10;
        r13_20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8 + 20);
        while (1) {
            *reinterpret_cast<struct s0**>(&rsi21) = ebp4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi21) + 4) = 0;
            eax22 = fun_2610(rbx16, rsi21, r14_19, r13_20);
            if (eax22 >= 0) {
                r12d12 = v17;
            } else {
                if (v17 == r12d12) {
                    r12d12 = r12d12 + r12d12;
                    v17 = r12d12;
                } else {
                    r12d12 = v17;
                }
            }
            rsi23 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r12d12));
            rsi24 = reinterpret_cast<struct s0*>(rsi23 << 2);
            *reinterpret_cast<uint32_t*>(&rax25) = reinterpret_cast<uint1_t>(!!(rsi23 >> 62));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax25) + 4) = 0;
            if (__undefined()) 
                goto addr_65e0_21;
            if (rax25) 
                goto addr_65e0_21;
            rax26 = fun_25d0(r14_19);
            if (!rax26) 
                goto addr_65eb_24;
            if (eax22 >= 0) 
                goto addr_6630_26;
            r14_19 = rax26;
        }
    }
    addr_65f9_28:
    rax27 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (rax27) {
        fun_2450();
    } else {
        *reinterpret_cast<int32_t*>(&rax28) = r12d12;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
        return rax28;
    }
    addr_676f_9:
    addr_6746_12:
    rsi29 = r13_14;
    rax30 = fun_2570();
    r12d12 = *reinterpret_cast<int32_t*>(&rax30);
    if (*reinterpret_cast<int32_t*>(&rax30) >= 0) {
        addr_66a9_31:
        *v5 = r13_14;
        if (r12d12 > 1) {
            esi31 = *reinterpret_cast<struct s0**>(&r13_14->f0);
            rcx32 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r13_14) + r12d12 * 4);
            rax33 = &r13_14->f4;
            if (reinterpret_cast<uint64_t>(rcx32) > reinterpret_cast<uint64_t>(rax33)) {
                do {
                    if (*rax33 == esi31 || *rax33 == *reinterpret_cast<struct s0**>(&r13_14->f0)) {
                        --r12d12;
                    } else {
                        r13_14->f4 = *rax33;
                        r13_14 = reinterpret_cast<struct s0*>(&r13_14->f4);
                    }
                    rax33 = rax33 + 4;
                } while (reinterpret_cast<uint64_t>(rcx32) > reinterpret_cast<uint64_t>(rax33));
                goto addr_65f9_28;
            } else {
                goto addr_65f9_28;
            }
        }
    } else {
        addr_675c_38:
        fun_2370(r13_14, rsi29);
        goto addr_6764_4;
    }
    addr_668d_10:
    rsi29 = reinterpret_cast<struct s0*>(&rax13->f4);
    rax34 = fun_2570();
    if (*reinterpret_cast<int32_t*>(&rax34) < 0) 
        goto addr_675c_38;
    *reinterpret_cast<struct s0**>(&r13_14->f0) = ebp4;
    r12d12 = static_cast<int32_t>(rax34 + 1);
    goto addr_66a9_31;
    addr_65e0_21:
    rax35 = fun_2390();
    *reinterpret_cast<struct s0**>(&rax35->f0) = reinterpret_cast<struct s0*>(12);
    addr_65eb_24:
    r12d12 = -1;
    fun_2370(r14_19, rsi24, r14_19, rsi24);
    goto addr_65f9_28;
    addr_6630_26:
    *v5 = rax26;
    goto addr_65f9_28;
}

int32_t setlocale_null_r();

int64_t fun_6783() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2450();
    } else {
        return rax3;
    }
}

int64_t fun_6803(int64_t rdi, signed char* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25e0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2440(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2540(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2540(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_68b3() {
    __asm__("cli ");
    goto fun_25e0;
}

void fun_68c3() {
    __asm__("cli ");
}

void fun_68d7() {
    __asm__("cli ");
    return;
}

uint32_t fun_24e0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_2680(int64_t rdi, struct s0* rsi);

uint32_t fun_2670(struct s0* rdi, struct s0* rsi);

void** fun_2690(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_3445() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2420();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2420();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2440(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3743_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3743_22; else 
                            goto addr_3b3d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_3bfd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_3f50_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3740_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3740_30; else 
                                goto addr_3f69_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2440(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_3f50_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24e0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_3f50_28; else 
                            goto addr_35ec_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_40b0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_3f30_40:
                        if (r11_27 == 1) {
                            addr_3abd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4078_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_36f7_44;
                            }
                        } else {
                            goto addr_3f40_46;
                        }
                    } else {
                        addr_40bf_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_3abd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3743_22:
                                if (v47 != 1) {
                                    addr_3c99_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2440(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_3ce4_54;
                                    }
                                } else {
                                    goto addr_3750_56;
                                }
                            } else {
                                addr_36f5_57:
                                ebp36 = 0;
                                goto addr_36f7_44;
                            }
                        } else {
                            addr_3f24_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_40bf_47; else 
                                goto addr_3f2e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_3abd_41;
                        if (v47 == 1) 
                            goto addr_3750_56; else 
                            goto addr_3c99_52;
                    }
                }
                addr_37b1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3648_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_366d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_3970_65;
                    } else {
                        addr_37d9_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4028_67;
                    }
                } else {
                    goto addr_37d0_69;
                }
                addr_3681_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad4);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_36cc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4028_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_36cc_81;
                }
                addr_37d0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_366d_64; else 
                    goto addr_37d9_66;
                addr_36f7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_37af_91;
                if (v22) 
                    goto addr_370f_93;
                addr_37af_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_37b1_62;
                addr_3ce4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_446b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_44db_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_42df_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2680(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2670(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_3dde_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_379c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_3de8_112;
                    }
                } else {
                    addr_3de8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_3eb9_114;
                }
                addr_37a8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_37af_91;
                while (1) {
                    addr_3eb9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_43c7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_3e26_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_43d5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_3ea7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_3ea7_128;
                        }
                    }
                    addr_3e55_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad4);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_3ea7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_3e26_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad4);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3e55_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_36cc_81;
                addr_43d5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4028_67;
                addr_446b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_3dde_109;
                addr_44db_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_3dde_109;
                addr_3750_56:
                rax93 = fun_2690(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_379c_110;
                addr_3f2e_59:
                goto addr_3f30_40;
                addr_3bfd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3743_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_37a8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_36f5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3743_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_3c42_160;
                if (!v22) 
                    goto addr_4017_162; else 
                    goto addr_4223_163;
                addr_3c42_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4017_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4028_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_3aeb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_3953_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3681_70; else 
                    goto addr_3967_169;
                addr_3aeb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3648_63;
                goto addr_37d0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_3f24_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_405f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3740_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3638_178; else 
                        goto addr_3fe2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3f24_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3743_22;
                }
                addr_405f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3740_30:
                    r8d42 = 0;
                    goto addr_3743_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_37b1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4078_42;
                    }
                }
                addr_3638_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3648_63;
                addr_3fe2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_3f40_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_37b1_62;
                } else {
                    addr_3ff2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3743_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_47a2_188;
                if (v28) 
                    goto addr_4017_162;
                addr_47a2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_3953_168;
                addr_35ec_37:
                if (v22) 
                    goto addr_45e3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3603_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_40b0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_413b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3743_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3638_178; else 
                        goto addr_4117_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3f24_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3743_22;
                }
                addr_413b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3743_22;
                }
                addr_4117_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3f40_46;
                goto addr_3ff2_186;
                addr_3603_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3743_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3743_22; else 
                    goto addr_3614_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_46ee_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4574_210;
            if (1) 
                goto addr_4572_212;
            if (!v29) 
                goto addr_41ae_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_46e1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_3970_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_372b_219; else 
            goto addr_398a_220;
        addr_370f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_3723_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_398a_220; else 
            goto addr_372b_219;
        addr_42df_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_398a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_42fd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2430();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_4770_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_41d6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_43c7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3723_221;
        addr_4223_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3723_221;
        addr_3967_169:
        goto addr_3970_65;
        addr_46ee_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_398a_220;
        goto addr_42fd_222;
        addr_4574_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_45ce_236;
        fun_2450();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4770_225;
        addr_4572_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4574_210;
        addr_41ae_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4574_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_41d6_226;
        }
        addr_46e1_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_3b3d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x75cc + rax113 * 4) + 0x75cc;
    addr_3f69_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x76cc + rax114 * 4) + 0x76cc;
    addr_45e3_190:
    addr_372b_219:
    goto 0x3410;
    addr_3614_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x74cc + rax115 * 4) + 0x74cc;
    addr_45ce_236:
    goto v116;
}

void fun_3630() {
}

void fun_37e8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x34e2;
}

void fun_3841() {
    goto 0x34e2;
}

void fun_392e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x37b1;
    }
    if (v2) 
        goto 0x4223;
    if (!r10_3) 
        goto addr_438e_5;
    if (!v4) 
        goto addr_425e_7;
    addr_438e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_425e_7:
    goto 0x3664;
}

void fun_394c() {
}

void fun_39f7() {
    signed char v1;

    if (v1) {
        goto 0x397f;
    } else {
        goto 0x36ba;
    }
}

void fun_3a11() {
    signed char v1;

    if (!v1) 
        goto 0x3a0a; else 
        goto "???";
}

void fun_3a38() {
    goto 0x3953;
}

void fun_3ab8() {
}

void fun_3ad0() {
}

void fun_3aff() {
    goto 0x3953;
}

void fun_3b51() {
    goto 0x3ae0;
}

void fun_3b80() {
    goto 0x3ae0;
}

void fun_3bb3() {
    goto 0x3ae0;
}

void fun_3f80() {
    goto 0x3638;
}

void fun_427e() {
    signed char v1;

    if (v1) 
        goto 0x4223;
    goto 0x3664;
}

void fun_4325() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3664;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3648;
        goto 0x3664;
    }
}

void fun_4742() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x39b0;
    } else {
        goto 0x34e2;
    }
}

void fun_5678() {
    fun_2420();
}

void fun_386e() {
    goto 0x34e2;
}

void fun_3a44() {
    goto 0x39fc;
}

void fun_3b0b() {
    goto 0x3638;
}

void fun_3b5d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x3ae0;
    goto 0x370f;
}

void fun_3b8f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x3aeb;
        goto 0x3510;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x398a;
        goto 0x372b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4328;
    if (r10_8 > r15_9) 
        goto addr_3a75_9;
    addr_3a7a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4333;
    goto 0x3664;
    addr_3a75_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_3a7a_10;
}

void fun_3bc2() {
    goto 0x36f7;
}

void fun_3f90() {
    goto 0x36f7;
}

void fun_472f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x384c;
    } else {
        goto 0x39b0;
    }
}

void fun_5730() {
}

void fun_3bcc() {
    goto 0x3b67;
}

void fun_3f9a() {
    goto 0x3abd;
}

void fun_5790() {
    fun_2420();
    goto fun_2660;
}

void fun_389d() {
    goto 0x34e2;
}

void fun_3bd8() {
    goto 0x3b67;
}

void fun_3fa7() {
    goto 0x3b0e;
}

void fun_57d0() {
    fun_2420();
    goto fun_2660;
}

void fun_38ca() {
    goto 0x34e2;
}

void fun_3be4() {
    goto 0x3ae0;
}

void fun_5810() {
    fun_2420();
    goto fun_2660;
}

void fun_38ec() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4280;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x37b1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x37b1;
    }
    if (v11) 
        goto 0x45e3;
    if (r10_12 > r15_13) 
        goto addr_4633_8;
    addr_4638_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4371;
    addr_4633_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4638_9;
}

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s19 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_5860() {
    int64_t r15_1;
    struct s17* rbx2;
    struct s0* r14_3;
    struct s18* rbx4;
    struct s0* r13_5;
    struct s19* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    struct s5* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2420();
    fun_2660(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x5882, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_58b8() {
    fun_2420();
    goto 0x5889;
}

struct s20 {
    signed char[32] pad32;
    int64_t f20;
};

struct s21 {
    signed char[24] pad24;
    int64_t f18;
};

struct s22 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s23 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s24 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_58f0() {
    int64_t rcx1;
    struct s20* rbx2;
    int64_t r15_3;
    struct s21* rbx4;
    struct s0* r14_5;
    struct s22* rbx6;
    struct s0* r13_7;
    struct s23* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s24* rbx12;
    struct s0* rax13;
    struct s5* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2420();
    fun_2660(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x5924, __return_address(), rcx1);
    goto v15;
}

void fun_5968() {
    fun_2420();
    goto 0x592b;
}
