undefined  [16] main(__uid_t param_1,undefined8 *param_2)

{
  __uid_t _Var1;
  char *pcVar2;
  char cVar3;
  int iVar4;
  uint uVar5;
  __gid_t _Var6;
  passwd *ppVar7;
  undefined8 uVar8;
  undefined1 *puVar9;
  ulong uVar10;
  ulong extraout_RDX;
  _IO_FILE *p_Var11;
  undefined8 uStack56;
  
  uVar5 = 0x107861;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  puVar9 = longopts;
  iVar4 = getopt_long(param_1,param_2,"",longopts,0);
  if (iVar4 == -1) {
LAB_001027b0:
    if (optind != param_1) {
      uVar5 = 1;
      for (; (int)optind < (int)param_1; optind = optind + 1) {
        ppVar7 = getpwnam((char *)param_2[(int)optind]);
        if (ppVar7 == (passwd *)0x0) {
          quote(param_2[(int)optind]);
          uVar8 = dcgettext(0,"%s: no such user",5);
          uVar5 = 0;
          error(0,0,uVar8);
        }
        else {
          _Var6 = ppVar7->pw_gid;
          _Var1 = ppVar7->pw_uid;
          __printf_chk(1,"%s : ",param_2[(int)optind]);
          cVar3 = print_group_list(param_2[(int)optind],_Var1,_Var6,_Var6,1,0x20);
          if (cVar3 == '\0') {
            uVar5 = 0;
          }
          pcVar2 = stdout->_IO_write_ptr;
          if (pcVar2 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar2 + 1;
            *pcVar2 = '\n';
          }
          else {
            __overflow(stdout,10);
          }
        }
      }
      goto LAB_00102940;
    }
    param_2 = (undefined8 *)__errno_location();
    *(undefined4 *)param_2 = 0;
    param_1 = getuid();
    if (param_1 == 0xffffffff) goto LAB_0010296a;
  }
  else {
    if (iVar4 == -0x83) {
      version_etc(stdout,"groups","GNU coreutils",Version,"David MacKenzie","James Youngman",0,
                  puVar9);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar4 != -0x82) {
      usage(1);
      goto LAB_001027b0;
    }
    usage(0);
LAB_0010296a:
    if (*(int *)param_2 != 0) {
      uVar8 = dcgettext(0,"cannot get real UID",5);
      error(1,*(int *)param_2,uVar8);
      uVar10 = extraout_RDX;
      goto LAB_00102997;
    }
  }
  *(int *)param_2 = 0;
  uVar5 = getegid();
  if (uVar5 == 0xffffffff) goto LAB_001029c4;
  do {
    *(int *)param_2 = 0;
    _Var6 = getgid();
    uVar10 = (ulong)_Var6;
    if (_Var6 != 0xffffffff) {
LAB_00102905:
      uVar5 = print_group_list(0,param_1,uVar10,uVar5,1,0x20);
      pcVar2 = stdout->_IO_write_ptr;
      p_Var11 = stdout;
      if (stdout->_IO_write_end <= pcVar2) goto LAB_001029f1;
      stdout->_IO_write_ptr = pcVar2 + 1;
      *pcVar2 = '\n';
      goto LAB_00102940;
    }
LAB_00102997:
    if (*(int *)param_2 == 0) goto LAB_00102905;
    uVar8 = dcgettext(0,"cannot get real GID",5);
    error(1,*(int *)param_2,uVar8);
LAB_001029c4:
  } while (*(int *)param_2 == 0);
  uVar8 = dcgettext(0,"cannot get effective GID",5);
  p_Var11 = (_IO_FILE *)0x1;
  error(1,*(int *)param_2,uVar8);
LAB_001029f1:
  __overflow(p_Var11,10);
LAB_00102940:
  return CONCAT88(uStack56,(ulong)((uVar5 ^ 1) & 0xff));
}