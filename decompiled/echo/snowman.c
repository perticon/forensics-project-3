
int64_t hextobin(int64_t rdi) {
    int32_t eax2;
    int64_t rdi3;
    int64_t rax4;
    int64_t rax5;

    eax2 = static_cast<int32_t>(rdi - 65);
    if (*reinterpret_cast<unsigned char*>(&eax2) > 37) {
        *reinterpret_cast<uint32_t*>(&rdi3) = *reinterpret_cast<unsigned char*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi3) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rax4) = static_cast<int32_t>(rdi3 - 48);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        return rax4;
    } else {
        *reinterpret_cast<uint32_t*>(&rax5) = *reinterpret_cast<unsigned char*>(&eax2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x7004 + rax5 * 4) + 0x7004;
    }
}

struct s0 {
    void* f0;
    signed char[4] pad8;
    struct s0* f8;
    struct s0* f10;
    struct s0* f18;
    signed char* f20;
    signed char* f28;
    signed char* f30;
    signed char[16] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int64_t fun_23c0();

int64_t fun_2320(struct s0* rdi, ...);

void* quotearg_buffer_restyled(struct s0* rdi, void* rsi, struct s0* rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_23c0();
    if (r8d > 10) {
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x78c0 + rax11 * 4) + 0x78c0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

int64_t g28;

int32_t* fun_2330(struct s0* rdi);

struct s0* slotvec = reinterpret_cast<struct s0*>(0xb070);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_2440();

struct s2 {
    void* f0;
    signed char[4] pad8;
    struct s0* f8;
};

void fun_2310(struct s0* rdi);

struct s0* xcharalloc(void* rdi, ...);

void fun_23e0();

struct s0* quotearg_n_options(struct s0* rdi, struct s0* rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    void* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void* rax23;
    void* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    int64_t rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x489f;
    rax8 = fun_2330(rdi);
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
        fun_2320(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6631]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x492b;
            fun_2440();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<int64_t>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->pad40, v21, v20, v7);
        if (reinterpret_cast<uint32_t>(rsi18) <= reinterpret_cast<uint32_t>(rax23)) {
            rsi24 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rax23) + 1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb0e0) {
                fun_2310(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x49ba);
        }
        *rax8 = v10;
        rax29 = rax6 - g28;
        if (rax29) {
            fun_23e0();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
};

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s3* gettext_quote_part_0(struct s3* rdi, int32_t esi, struct s3* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s3* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s3* rax10;
    struct s3* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s3*>(0x785b);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax7 = reinterpret_cast<struct s3*>(0x7854);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s3*>(0x785f);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax10 = reinterpret_cast<struct s3*>(0x7850);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s3*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s3*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae58 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae58;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_22f3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_2303() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2040;

void fun_2313() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2323() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2333() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2343() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2353() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_2363() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_2373() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20b0;

void fun_2383() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_2393() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_23a3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20e0;

void fun_23b3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x20f0;

void fun_23c3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2100;

void fun_23d3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2110;

void fun_23e3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t mbrtowc = 0x2120;

void fun_23f3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x2130;

void fun_2403() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2140;

void fun_2413() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2150;

void fun_2423() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2160;

void fun_2433() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2170;

void fun_2443() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x2180;

void fun_2453() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2190;

void fun_2463() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21a0;

void fun_2473() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21b0;

void fun_2483() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21c0;

void fun_2493() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x21d0;

void fun_24a3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x21e0;

void fun_24b3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x21f0;

void fun_24c3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2200;

void fun_24d3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2210;

void fun_24e3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2220;

void fun_24f3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2230;

void fun_2503() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2240;

void fun_2513() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2250;

void fun_2523() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2260;

void fun_2533() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2270;

void fun_2543() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x2280;

void fun_2553() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2290;

void fun_2563() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x22a0;

void fun_2573() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x22b0;

void fun_2583() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x22c0;

void fun_2593() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x22d0;

void fun_25a3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x22e0;

void fun_25b3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

struct s5 {
    int64_t f0;
    struct s3* f8;
};

int64_t fun_2300(int64_t rdi);

void set_program_name(int64_t rdi, struct s5* rsi);

struct s3* fun_2510(int64_t rdi, ...);

void fun_23a0(int64_t rdi, int64_t rsi);

void fun_2380(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2480(struct s3* rdi, struct s5* rsi, int64_t rdx);

void usage();

int64_t Version = 0x77eb;

struct s0* stdout = reinterpret_cast<struct s0*>(0);

void version_etc(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void fun_2400();

void fun_2460(struct s3* rdi, struct s0* rsi, ...);

int64_t fun_2603(int32_t edi, struct s5* rsi, int64_t rdx) {
    struct s5* r14_4;
    int64_t rbp5;
    int64_t rax6;
    int1_t zf7;
    unsigned char bl8;
    int64_t rdi9;
    struct s3* rbp10;
    int32_t eax11;
    int64_t r9_12;
    struct s3** r14_13;
    struct s3* rdi14;
    int32_t eax15;
    int64_t rdi16;
    int32_t ebp17;
    int64_t rdi18;
    int32_t eax19;
    int64_t rcx20;
    struct s0* rdi21;
    uint32_t r8d22;
    uint32_t esi23;
    unsigned char* rdi24;
    unsigned char* rax25;
    uint32_t ecx26;
    uint32_t ecx27;
    uint64_t rdx28;
    uint32_t edx29;
    struct s0* rdi30;
    signed char* rax31;
    int64_t rax32;
    struct s3** r12_33;
    int64_t rax34;
    struct s3** rbx35;
    struct s3* r13_36;
    uint32_t edx37;
    struct s3* rax38;
    int64_t rcx39;
    int32_t eax40;
    struct s3* r8_41;
    struct s0* rdi42;
    signed char* rax43;
    struct s0* rdi44;
    signed char* rax45;
    struct s0* rdi46;
    signed char* rax47;
    int64_t rax48;
    struct s0* rsi49;
    struct s3* rdi50;
    struct s0* rdi51;
    signed char* rax52;

    __asm__("cli ");
    r14_4 = rsi;
    *reinterpret_cast<int32_t*>(&rbp5) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp5) + 4) = 0;
    rax6 = fun_2300("POSIXLY_CORRECT");
    zf7 = rax6 == 0;
    bl8 = reinterpret_cast<uint1_t>(!zf7);
    if (zf7) {
        rdi9 = r14_4->f0;
        set_program_name(rdi9, rsi);
        fun_2510(6, 6);
        fun_23a0("coreutils", "/usr/local/share/locale");
        fun_2380("coreutils", "/usr/local/share/locale");
        atexit(0x3210, "/usr/local/share/locale");
        if (*reinterpret_cast<int32_t*>(&rbp5) == 2) {
            addr_269f_3:
            rbp10 = r14_4->f8;
            eax11 = fun_2480(rbp10, "--help", rdx);
            if (!eax11) {
                usage();
            }
        } else {
            *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(rbp5 - 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_12) + 4) = 0;
            r14_13 = &r14_4->f8;
            if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_12) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_12) == 0))) {
                goto addr_276d_7;
            }
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rbp5) <= 1 || (rdi14 = r14_4->f8, rsi = reinterpret_cast<struct s5*>("-n"), eax15 = fun_2480(rdi14, "-n", rdx), !!eax15)) {
            rdi16 = r14_4->f0;
            r14_13 = &r14_4->f8;
            set_program_name(rdi16, rsi);
            fun_2510(6, 6);
            fun_23a0("coreutils", "/usr/local/share/locale");
            fun_2380("coreutils", "/usr/local/share/locale");
            atexit(0x3210, "/usr/local/share/locale");
            *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(rbp5 - 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_12) + 4) = 0;
            ebp17 = 1;
            if (!rax6) 
                goto addr_2740_10; else 
                goto addr_27f7_11;
        } else {
            rdi18 = r14_4->f0;
            set_program_name(rdi18, "-n");
            fun_2510(6, 6);
            fun_23a0("coreutils", "/usr/local/share/locale");
            fun_2380("coreutils", "/usr/local/share/locale");
            atexit(0x3210, "/usr/local/share/locale");
            if (*reinterpret_cast<int32_t*>(&rbp5) == 2) 
                goto addr_269f_3;
            *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(rbp5 - 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_12) + 4) = 0;
            r14_13 = &r14_4->f8;
            goto addr_26db_14;
        }
    }
    eax19 = fun_2480(rbp10, "--version", rdx);
    if (!eax19) {
        rcx20 = Version;
        rdi21 = stdout;
        version_etc(rdi21, "echo", "GNU coreutils", rcx20, "Brian Fox", "Chet Ramey");
        goto addr_278d_17;
    } else {
        r14_13 = &r14_4->f8;
        *reinterpret_cast<int32_t*>(&r9_12) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_12) + 4) = 0;
    }
    addr_26db_14:
    r8d22 = 0;
    ebp17 = 1;
    do {
        if (*reinterpret_cast<unsigned char*>(&(*r14_13)->f0) != 45) 
            break;
        esi23 = (*r14_13)->f1;
        rdi24 = &(*r14_13)->f1;
        if (!*reinterpret_cast<signed char*>(&esi23)) 
            break;
        rax25 = &(*r14_13)->f2;
        ecx26 = esi23;
        do {
            ecx27 = ecx26 - 69;
            if (*reinterpret_cast<unsigned char*>(&ecx27) > 41) 
                goto addr_272f_23;
            rdx28 = 0x20100000001 >> *reinterpret_cast<unsigned char*>(&ecx27);
            edx29 = *reinterpret_cast<uint32_t*>(&rdx28) & 1;
            if (!edx29) 
                goto addr_272f_23;
            ecx26 = *rax25;
            ++rax25;
        } while (*reinterpret_cast<signed char*>(&ecx26));
        do {
            addr_2a18_27:
            ++rdi24;
            if (*reinterpret_cast<signed char*>(&esi23) == 0x65) {
                r8d22 = edx29;
            } else {
                if (*reinterpret_cast<signed char*>(&esi23) != 0x6e) {
                    if (*reinterpret_cast<signed char*>(&esi23) == 69) {
                        r8d22 = 0;
                    }
                } else {
                    esi23 = *rdi24;
                    ebp17 = 0;
                    if (*reinterpret_cast<signed char*>(&esi23)) 
                        goto addr_2a18_27; else 
                        break;
                }
            }
            esi23 = *rdi24;
        } while (*reinterpret_cast<signed char*>(&esi23));
        r14_13 = r14_13 + 8;
        *reinterpret_cast<int32_t*>(&r9_12) = *reinterpret_cast<int32_t*>(&r9_12) - 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_12) + 4) = 0;
    } while (*reinterpret_cast<int32_t*>(&r9_12));
    goto addr_2a44_35;
    if (bl8) {
        addr_27f7_11:
        if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_12) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_12) == 0)) {
            addr_2768_37:
            if (!*reinterpret_cast<signed char*>(&ebp17)) {
                addr_278d_17:
                return 0;
            } else {
                addr_276d_7:
                rdi30 = stdout;
                rax31 = rdi30->f28;
                if (reinterpret_cast<uint64_t>(rax31) >= reinterpret_cast<uint64_t>(rdi30->f30)) {
                    fun_2400();
                    goto addr_278d_17;
                } else {
                    rdi30->f28 = rax31 + 1;
                    *rax31 = 10;
                    goto addr_278d_17;
                }
            }
        } else {
            addr_2800_40:
            *reinterpret_cast<int32_t*>(&rax32) = static_cast<int32_t>(r9_12 - 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax32) + 4) = 0;
            r12_33 = r14_13 + rax32 * 8;
        }
    } else {
        if (!*reinterpret_cast<signed char*>(&r8d22)) {
            addr_2745_42:
            *reinterpret_cast<int32_t*>(&rax34) = static_cast<int32_t>(r9_12 - 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax34) + 4) = 0;
            rbx35 = r14_13 + rax34 * 8;
            goto addr_2750_43;
        } else {
            goto addr_27f7_11;
        }
    }
    while (1) {
        r13_36 = *r14_13;
        while (edx37 = *reinterpret_cast<unsigned char*>(&r13_36->f0), rax38 = reinterpret_cast<struct s3*>(&r13_36->f1), !!*reinterpret_cast<signed char*>(&edx37)) {
            do {
                if (*reinterpret_cast<signed char*>(&edx37) != 92) {
                    r13_36 = rax38;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx39) = r13_36->f1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx39) + 4) = 0;
                    if (!*reinterpret_cast<unsigned char*>(&rcx39)) {
                        r13_36 = rax38;
                    } else {
                        eax40 = static_cast<int32_t>(rcx39 - 48);
                        r8_41 = reinterpret_cast<struct s3*>(&r13_36->f2);
                        if (*reinterpret_cast<unsigned char*>(&eax40) <= 72) 
                            goto addr_286e_52;
                        rdi42 = stdout;
                        rax43 = rdi42->f28;
                        if (reinterpret_cast<uint64_t>(rdi42->f30) <= reinterpret_cast<uint64_t>(rax43)) 
                            goto addr_2c0c_54; else 
                            goto addr_2894_55;
                    }
                }
                addr_281f_56:
                rdi44 = stdout;
                rax45 = rdi44->f28;
                if (reinterpret_cast<uint64_t>(rax45) >= reinterpret_cast<uint64_t>(rdi44->f30)) 
                    continue; else 
                    break;
                addr_2c0c_54:
                fun_2400();
                edx37 = *reinterpret_cast<unsigned char*>(&rcx39);
                r13_36 = r8_41;
                goto addr_281f_56;
                addr_2894_55:
                r13_36 = r8_41;
                rdi42->f28 = rax43 + 1;
                edx37 = *reinterpret_cast<uint32_t*>(&rcx39);
                *rax43 = 92;
                goto addr_281f_56;
                fun_2400();
                edx37 = *reinterpret_cast<unsigned char*>(&r13_36->f0);
                rax38 = reinterpret_cast<struct s3*>(&r13_36->f1);
            } while (*reinterpret_cast<signed char*>(&edx37));
            goto addr_2a79_58;
            rdi44->f28 = rax45 + 1;
            *rax45 = *reinterpret_cast<signed char*>(&edx37);
        }
        addr_2a80_60:
        if (r12_33 == r14_13) 
            goto addr_2768_37;
        rdi46 = stdout;
        rax47 = rdi46->f28;
        if (reinterpret_cast<uint64_t>(rax47) >= reinterpret_cast<uint64_t>(rdi46->f30)) {
            fun_2400();
        } else {
            rdi46->f28 = rax47 + 1;
            *rax47 = 32;
        }
        r14_13 = r14_13 + 8;
        continue;
        addr_2a79_58:
        goto addr_2a80_60;
    }
    addr_286e_52:
    *reinterpret_cast<uint32_t*>(&rax48) = *reinterpret_cast<unsigned char*>(&eax40);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x709c + rax48 * 4) + 0x709c;
    addr_2750_43:
    while (rsi49 = stdout, rdi50 = *r14_13, fun_2460(rdi50, rsi49), rbx35 != r14_13) {
        rdi51 = stdout;
        rax52 = rdi51->f28;
        if (reinterpret_cast<uint64_t>(rax52) >= reinterpret_cast<uint64_t>(rdi51->f30)) {
            fun_2400();
        } else {
            rdi51->f28 = rax52 + 1;
            *rax52 = 32;
        }
        r14_13 = r14_13 + 8;
    }
    goto addr_2768_37;
    addr_272f_23:
    if (bl8) 
        goto addr_2800_40;
    if (*reinterpret_cast<signed char*>(&r8d22)) 
        goto addr_2800_40;
    addr_2740_10:
    if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_12) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r9_12) == 0)) 
        goto addr_2768_37; else 
        goto addr_2745_42;
    addr_2a44_35:
    goto addr_2768_37;
}

int64_t __libc_start_main = 0;

void fun_2c63() {
    __asm__("cli ");
    __libc_start_main(0x2600, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_22f0(int64_t rdi);

int64_t fun_2d03() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_22f0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2d43() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_2430(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s6 {
    signed char f0;
    signed char f1;
    signed char f2;
};

struct s6* program_name = reinterpret_cast<struct s6*>(0);

struct s3* fun_23b0();

void fun_2520(int64_t rdi, struct s3* rsi, struct s6* rdx, struct s6* rcx);

int32_t fun_2340(struct s3* rdi, int64_t rsi, int64_t rdx, struct s6* rcx);

void fun_2560();

void fun_2de3(int32_t edi) {
    struct s6* r12_2;
    struct s3* rax3;
    struct s0* r12_4;
    struct s3* rax5;
    struct s0* r12_6;
    struct s3* rax7;
    struct s0* r12_8;
    struct s3* rax9;
    struct s0* r12_10;
    struct s3* rax11;
    struct s0* r12_12;
    struct s3* rax13;
    struct s0* r12_14;
    struct s3* rax15;
    struct s0* r12_16;
    struct s3* rax17;
    struct s6* r12_18;
    struct s3* rax19;
    struct s0* r13_20;
    struct s3* rax21;
    int32_t eax22;
    struct s6* r13_23;
    struct s3* rax24;
    struct s3* rax25;
    int32_t eax26;
    struct s3* rax27;
    struct s0* rbp28;
    struct s3* rax29;
    struct s3* rax30;
    int1_t zf31;
    struct s3* rax32;
    struct s3* rax33;
    struct s3* rax34;
    int32_t eax35;

    __asm__("cli ");
    if (edi) {
        fun_2430("status == EXIT_SUCCESS", "src/echo.c", 40, "usage");
    }
    r12_2 = program_name;
    rax3 = fun_23b0();
    fun_2520(1, rax3, r12_2, r12_2);
    r12_4 = stdout;
    rax5 = fun_23b0();
    fun_2460(rax5, r12_4, rax5, r12_4);
    r12_6 = stdout;
    rax7 = fun_23b0();
    fun_2460(rax7, r12_6, rax7, r12_6);
    r12_8 = stdout;
    rax9 = fun_23b0();
    fun_2460(rax9, r12_8, rax9, r12_8);
    r12_10 = stdout;
    rax11 = fun_23b0();
    fun_2460(rax11, r12_10, rax11, r12_10);
    r12_12 = stdout;
    rax13 = fun_23b0();
    fun_2460(rax13, r12_12, rax13, r12_12);
    r12_14 = stdout;
    rax15 = fun_23b0();
    fun_2460(rax15, r12_14, rax15, r12_14);
    r12_16 = stdout;
    rax17 = fun_23b0();
    r12_18 = reinterpret_cast<struct s6*>("echo");
    fun_2460(rax17, r12_16, rax17, r12_16);
    rax19 = fun_23b0();
    fun_2520(1, rax19, "echo", r12_2);
    r13_20 = stdout;
    rax21 = fun_23b0();
    fun_2460(rax21, r13_20, rax21, r13_20);
    do {
        if (1) 
            break;
        eax22 = fun_2480("echo", 0, 5);
    } while (eax22);
    r13_23 = reinterpret_cast<struct s6*>(0);
    if (1) {
        while (1) {
            rax24 = fun_23b0();
            fun_2520(1, rax24, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
            rax25 = fun_2510(5);
            if (!rax25 || (eax26 = fun_2340(rax25, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax26)) {
                rax27 = fun_23b0();
                r13_23 = reinterpret_cast<struct s6*>("echo");
                fun_2520(1, rax27, "https://www.gnu.org/software/coreutils/", r12_18);
                r12_18 = reinterpret_cast<struct s6*>(" invocation");
            } else {
                r13_23 = reinterpret_cast<struct s6*>("echo");
                addr_31ba_10:
                rbp28 = stdout;
                rax29 = fun_23b0();
                fun_2460(rax29, rbp28, rax29, rbp28);
                addr_30be_11:
                rax30 = fun_23b0();
                fun_2520(1, rax30, "https://www.gnu.org/software/coreutils/", r12_18);
                zf31 = r13_23 == r12_18;
                r12_18 = reinterpret_cast<struct s6*>(" invocation");
                if (!zf31) 
                    goto label_12;
            }
            addr_30fb_13:
            rax32 = fun_23b0();
            fun_2520(1, rax32, r13_23, r12_18);
            fun_2560();
            continue;
            label_12:
            r12_18 = reinterpret_cast<struct s6*>(0x7c81);
            goto addr_30fb_13;
        }
    } else {
        rax33 = fun_23b0();
        fun_2520(1, rax33, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
        rax34 = fun_2510(5);
        if (!rax34) 
            goto addr_30be_11;
        eax35 = fun_2340(rax34, "en_", 3, "https://www.gnu.org/software/coreutils/");
        if (eax35) 
            goto addr_31ba_10; else 
            goto addr_30be_11;
    }
}

int64_t file_name = 0;

void fun_31f3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3203(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s0* rdi);

struct s0* stderr = reinterpret_cast<struct s0*>(0);

struct s3* quotearg_colon();

void fun_2530();

int32_t exit_failure = 1;

struct s3* fun_2350(int64_t rdi, int64_t rsi, int64_t rdx, struct s3* rcx, struct s3* r8);

void fun_3213() {
    struct s0* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s0* rdi6;
    int32_t eax7;
    struct s3* rax8;
    int64_t rdi9;
    struct s3* rax10;
    int64_t rsi11;
    struct s3* r8_12;
    struct s3* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2330(rdi1), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_23b0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_32a3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2530();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2350(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_32a3_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2530();
    }
}

void fun_2570(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s7 {
    signed char[1] pad1;
    signed char f1;
    signed char[2] pad4;
    signed char f4;
};

struct s7* fun_2410();

struct s6* __progname = reinterpret_cast<struct s6*>(0);

struct s6* __progname_full = reinterpret_cast<struct s6*>(0);

void fun_32c3(struct s6* rdi) {
    struct s0* rcx2;
    struct s6* rbx3;
    struct s7* rax4;
    struct s6* r12_5;
    struct s6* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2570("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2320("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2410();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s6*>(&rax4->f1), reinterpret_cast<int64_t>(r12_5) - reinterpret_cast<int64_t>(rbx3) > 6) && (eax7 = fun_2340(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (rax4->f1 != 0x6c || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s6*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(struct s0* rdi, int64_t rsi);

void fun_4a63(struct s0* rdi) {
    struct s0* rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2330(rdi);
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = reinterpret_cast<struct s0*>(0xb1e0);
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_4aa3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb1e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4ac3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb1e0);
    }
    *rdi = esi;
    return 0xb1e0;
}

int64_t fun_4ae3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb1e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s8 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_4b23(struct s8* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xb1e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s9 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* fun_4b43(struct s9* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xb1e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x25ca;
    if (!rdx) 
        goto 0x25ca;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb1e0;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void* fun_4b83(struct s0* rdi, void* rsi, struct s0* rdx, int64_t rcx, struct s10* r8) {
    struct s10* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s10*>(0xb1e0);
    }
    rax7 = fun_2330(rdi);
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->pad40, v12, v10, 0x4bb6);
    *rax7 = r15d8;
    return rax13;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_4c03(struct s0* rdi, int64_t rsi, void** rdx, struct s11* rcx) {
    struct s11* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void* rax14;
    void* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s11*>(0xb1e0);
    }
    rax6 = fun_2330(rdi);
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void*>(&rbx5->pad40);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x4c31);
    rsi15 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rax14) + 1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x4c8c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_4cf3() {
    __asm__("cli ");
}

struct s0* gb078 = reinterpret_cast<struct s0*>(0xb0e0);

int64_t slotvec0 = 0x100;

void fun_4d03() {
    uint32_t eax1;
    struct s0* r12_2;
    int64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 2;
            fun_2310(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xb0e0) {
        fun_2310(rdi7);
        gb078 = reinterpret_cast<struct s0*>(0xb0e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb070) {
        fun_2310(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xb070);
    }
    nslots = 1;
    return;
}

void fun_4da3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4dc3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4dd3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4df3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_4e13(struct s0* rdi, int32_t esi, struct s0* rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x25d0;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_23e0();
    } else {
        return rax6;
    }
}

struct s0* fun_4ea3(struct s0* rdi, int32_t esi, struct s0* rdx, int64_t rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x25d5;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_23e0();
    } else {
        return rax7;
    }
}

struct s0* fun_4f33(int32_t edi, struct s0* rsi) {
    int64_t rax3;
    struct s1* rcx4;
    struct s0* rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x25da;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_23e0();
    } else {
        return rax5;
    }
}

struct s0* fun_4fc3(int32_t edi, struct s0* rsi, int64_t rdx) {
    int64_t rax4;
    struct s1* rcx5;
    struct s0* rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x25df;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_23e0();
    } else {
        return rax6;
    }
}

struct s0* fun_5053(struct s0* rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6180]");
    __asm__("movdqa xmm1, [rip+0x6188]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6171]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_23e0();
    } else {
        return rax10;
    }
}

struct s0* fun_50f3(struct s0* rdi, uint32_t esi) {
    struct s1* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x60e0]");
    __asm__("movdqa xmm1, [rip+0x60e8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x60d1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_23e0();
    } else {
        return rax9;
    }
}

struct s0* fun_5193(struct s0* rdi) {
    int64_t rax2;
    struct s0* rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6040]");
    __asm__("movdqa xmm1, [rip+0x6048]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6029]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_23e0();
    } else {
        return rax3;
    }
}

struct s0* fun_5223(struct s0* rdi, int64_t rsi) {
    int64_t rax3;
    struct s0* rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5fb0]");
    __asm__("movdqa xmm1, [rip+0x5fb8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x5fa6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_23e0();
    } else {
        return rax4;
    }
}

struct s0* fun_52b3(struct s0* rdi, int32_t esi, struct s0* rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x25e4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_23e0();
    } else {
        return rax6;
    }
}

struct s0* fun_5353(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5e7a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5e72]");
    __asm__("movdqa xmm2, [rip+0x5e7a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x25e9;
    if (!rdx) 
        goto 0x25e9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_23e0();
    } else {
        return rax7;
    }
}

struct s0* fun_53f3(int32_t edi, int64_t rsi, int64_t rdx, struct s0* rcx, int64_t r8) {
    int64_t rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    int64_t rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5dda]");
    __asm__("movdqa xmm1, [rip+0x5de2]");
    __asm__("movdqa xmm2, [rip+0x5dea]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x25ee;
    if (!rdx) 
        goto 0x25ee;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = rcx6 - g28;
    if (rdx10) {
        fun_23e0();
    } else {
        return rax9;
    }
}

struct s0* fun_54a3(int64_t rdi, int64_t rsi, struct s0* rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5d2a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5d22]");
    __asm__("movdqa xmm2, [rip+0x5d2a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x25f3;
    if (!rsi) 
        goto 0x25f3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_23e0();
    } else {
        return rax6;
    }
}

struct s0* fun_5543(int64_t rdi, int64_t rsi, struct s0* rdx, int64_t rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5c8a]");
    __asm__("movdqa xmm1, [rip+0x5c92]");
    __asm__("movdqa xmm2, [rip+0x5c9a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x25f8;
    if (!rsi) 
        goto 0x25f8;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_23e0();
    } else {
        return rax7;
    }
}

void fun_55e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_55f3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5613() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5633(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s12 {
    struct s3* f0;
    signed char[7] pad8;
    struct s3* f8;
    signed char[7] pad16;
    struct s3* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2580(struct s0* rdi, int64_t rsi, struct s3* rdx, struct s3* rcx, struct s3* r8, struct s3* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2490(int64_t rdi, struct s0* rsi, struct s3* rdx, struct s3* rcx, struct s3* r8, struct s3* r9);

void fun_5653(struct s0* rdi, struct s3* rsi, struct s3* rdx, struct s3* rcx, struct s12* r8, struct s3* r9) {
    struct s3* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s3* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s3* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s3* r14_40;
    struct s3* r13_41;
    struct s3* r12_42;
    struct s3* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2580(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2580(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_23b0();
    fun_2580(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2490(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_23b0();
    fun_2580(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2490(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_23b0();
        fun_2580(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7f28 + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_7) * 4)) + 0x7f28;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_5ac3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s13 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_5ae3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s13* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s13* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_23e0();
    } else {
        return;
    }
}

void fun_5b83(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_5c26_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_5c30_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_23e0();
    } else {
        return;
    }
    addr_5c26_5:
    goto addr_5c30_7;
}

void fun_5c63() {
    struct s0* rsi1;
    struct s3* rdx2;
    struct s3* rcx3;
    struct s3* r8_4;
    struct s3* r9_5;
    struct s3* rax6;
    struct s6* rcx7;
    struct s3* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2490(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_23b0();
    fun_2520(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_23b0();
    fun_2520(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_23b0();
    goto fun_2520;
}

int64_t fun_2370();

void xalloc_die();

void fun_5d03(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2370();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_24c0(unsigned char* rdi);

void fun_5d43(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_24c0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5d63(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_24c0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5d83(unsigned char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_24c0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2500();

void fun_5da3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2500();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5dd3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2500();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5e03(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2370();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5e43() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2370();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5e83(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2370();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5eb3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2370();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5f03(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2370();
            if (rax5) 
                break;
            addr_5f4d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_5f4d_5;
        rax8 = fun_2370();
        if (rax8) 
            goto addr_5f36_9;
        if (rbx4) 
            goto addr_5f4d_5;
        addr_5f36_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_5f93(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2370();
            if (rax8) 
                break;
            addr_5fda_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_5fda_5;
        rax11 = fun_2370();
        if (rax11) 
            goto addr_5fc2_9;
        if (!rbx6) 
            goto addr_5fc2_9;
        if (r12_4) 
            goto addr_5fda_5;
        addr_5fc2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6023(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_60cd_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_60e0_10:
                *r12_8 = 0;
            }
            addr_6080_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_60a6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_60f4_14;
            if (rcx10 <= rsi9) 
                goto addr_609d_16;
            if (rsi9 >= 0) 
                goto addr_60f4_14;
            addr_609d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_60f4_14;
            addr_60a6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2500();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_60f4_14;
            if (!rbp13) 
                break;
            addr_60f4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_60cd_9;
        } else {
            if (!r13_6) 
                goto addr_60e0_10;
            goto addr_6080_11;
        }
    }
}

int64_t fun_2470();

void fun_6123() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2470();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6153() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2470();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6183() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2470();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_61a3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2470();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_24a0(signed char* rdi, struct s3* rsi, unsigned char* rdx);

void fun_61c3(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_24c0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_24a0;
    }
}

void fun_6203(int64_t rdi, unsigned char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_24c0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_24a0;
    }
}

struct s14 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_6243(int64_t rdi, struct s14* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_24c0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_24a0;
    }
}

struct s3* fun_23d0(struct s3* rdi, ...);

void fun_6283(struct s3* rdi) {
    struct s3* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_23d0(rdi);
    rax3 = fun_24c0(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_24a0;
    }
}

void fun_62c3() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_23b0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    fun_2530();
    fun_2320(rdi1);
}

int64_t fun_2360();

int64_t rpl_fclose(struct s0* rdi);

int64_t fun_6303(struct s0* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_2360();
    ebx3 = reinterpret_cast<uint32_t>(rdi->f0) & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_635e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2330(rdi);
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_635e_3;
            rax6 = fun_2330(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_24b0(struct s0* rdi);

int32_t fun_24f0(struct s0* rdi);

int64_t fun_2420(int64_t rdi, ...);

int32_t rpl_fflush(struct s0* rdi);

int64_t fun_2390(struct s0* rdi);

int64_t fun_6373(struct s0* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_24b0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_24f0(rdi);
        if (!(eax3 && (eax4 = fun_24b0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2420(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2330(rdi);
            r12d9 = *rax8;
            rax10 = fun_2390(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2390;
}

void rpl_fseeko(struct s0* rdi);

void fun_6403(struct s0* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_24f0(rdi), !eax2) || !(reinterpret_cast<uint32_t>(rdi->f0) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6453(struct s0* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_24b0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2420(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = reinterpret_cast<void*>(reinterpret_cast<uint32_t>(rdi->f0) & 0xffffffef);
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_24e0(int64_t rdi);

signed char* fun_64d3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_24e0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_23f0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6513(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_23f0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_23e0();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_65a3() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_23e0();
    } else {
        return rax3;
    }
}

int64_t fun_6623(int64_t rdi, signed char* rsi, struct s3* rdx) {
    struct s3* rax4;
    int32_t r13d5;
    struct s3* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2510(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_23d0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_24a0(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_24a0(rsi, rax4, reinterpret_cast<unsigned char>(rdx) - 1);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_66d3() {
    __asm__("cli ");
    goto fun_2510;
}

void fun_66e3() {
    __asm__("cli ");
}

void fun_66f7() {
    __asm__("cli ");
    return;
}

struct s15 {
    signed char[2] pad2;
    unsigned char f2;
};

struct s16 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_28a9() {
    int64_t rax1;
    struct s15* r13_2;
    unsigned char* r8_3;
    struct s16* r8_4;
    int64_t rax5;
    int32_t ecx6;

    *reinterpret_cast<uint32_t*>(&rax1) = r13_2->f2 - 48;
    if (*reinterpret_cast<unsigned char*>(&rax1) <= 7) {
        r8_3 = &r8_4->f1;
    }
    *reinterpret_cast<uint32_t*>(&rax5) = *r8_3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    ecx6 = static_cast<int32_t>(rax5 - 48);
    if (*reinterpret_cast<unsigned char*>(&ecx6) <= 7) {
        goto 0x281f;
    } else {
        goto 0x281f;
    }
}

struct s17 {
    signed char[2] pad2;
    unsigned char f2;
};

void fun_28db() {
    int64_t rsi1;
    struct s17* r13_2;
    int32_t eax3;

    *reinterpret_cast<uint32_t*>(&rsi1) = r13_2->f2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi1) + 4) = 0;
    eax3 = static_cast<int32_t>(rsi1 - 48);
    if (*reinterpret_cast<unsigned char*>(&eax3) <= 7) {
        goto 0x28ae;
    } else {
        goto 0x281f;
    }
}

void fun_28f8() {
    goto 0x281f;
}

void fun_29d5() {
    goto 0x281f;
}

uint32_t fun_2450(struct s3* rdi, struct s3* rsi, struct s3* rdx, struct s3* rcx);

struct s3* rpl_mbrtowc(void* rdi, struct s3* rsi);

int32_t fun_25a0(int64_t rdi, struct s3* rsi);

uint32_t fun_2590(struct s3* rdi, struct s3* rsi);

void** fun_25b0(struct s3* rdi, struct s3* rsi, struct s3* rdx, struct s3* rcx);

void fun_34f5() {
    struct s3** rsp1;
    int32_t ebp2;
    struct s3* rax3;
    struct s3** rsp4;
    struct s3* r11_5;
    struct s3* r11_6;
    struct s3* v7;
    int32_t ebp8;
    struct s3* rax9;
    struct s3* rdx10;
    struct s3* rax11;
    struct s3* r11_12;
    struct s3* v13;
    int32_t ebp14;
    struct s3* rax15;
    struct s3* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s3* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s3* v22;
    int32_t ebx23;
    struct s3* rax24;
    struct s3** rsp25;
    struct s3* v26;
    struct s3* r11_27;
    struct s3* v28;
    struct s3* v29;
    struct s3* rsi30;
    struct s3* v31;
    struct s3* v32;
    struct s3* r10_33;
    struct s3* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s3* r9_37;
    struct s3* v38;
    struct s3* rdi39;
    struct s3* v40;
    struct s3* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s3* rcx44;
    unsigned char al45;
    struct s3* v46;
    int64_t v47;
    struct s3* v48;
    struct s3* v49;
    struct s3* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s3* v58;
    unsigned char v59;
    struct s3* v60;
    struct s3* v61;
    struct s3* v62;
    signed char* v63;
    struct s3* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s3* r13_69;
    struct s3* rsi70;
    void* v71;
    struct s3* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s3* v107;
    struct s3* rdx108;
    void* rax109;
    void* v110;
    int64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s3**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_23b0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_23b0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = *reinterpret_cast<unsigned char*>(&rdx10->f0), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s3*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s3*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_23d0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s3*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s3*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s3**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s3*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s3*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_37f3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_37f3_22; else 
                            goto addr_3bed_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_3cad_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4000_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_37f0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_37f0_30; else 
                                goto addr_4019_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s3*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_23d0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4000_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2450(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4000_28; else 
                            goto addr_369c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4160_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_3fe0_40:
                        if (r11_27 == 1) {
                            addr_3b6d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4128_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_37a7_44;
                            }
                        } else {
                            goto addr_3ff0_46;
                        }
                    } else {
                        addr_416f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_3b6d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_37f3_22:
                                if (v47 != 1) {
                                    addr_3d49_52:
                                    v48 = reinterpret_cast<struct s3*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_23d0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_3d94_54;
                                    }
                                } else {
                                    goto addr_3800_56;
                                }
                            } else {
                                addr_37a5_57:
                                ebp36 = 0;
                                goto addr_37a7_44;
                            }
                        } else {
                            addr_3fd4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_416f_47; else 
                                goto addr_3fde_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_3b6d_41;
                        if (v47 == 1) 
                            goto addr_3800_56; else 
                            goto addr_3d49_52;
                    }
                }
                addr_3861_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s3*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_36f8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_371d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_3a20_65;
                    } else {
                        addr_3889_66:
                        r9_37 = reinterpret_cast<struct s3*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_40d8_67;
                    }
                } else {
                    goto addr_3880_69;
                }
                addr_3731_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s3*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s3*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s3*>(&r9_37->f1);
                addr_377c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s3*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_40d8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s3*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_377c_81;
                }
                addr_3880_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_371d_64; else 
                    goto addr_3889_66;
                addr_37a7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_385f_91;
                if (v22) 
                    goto addr_37bf_93;
                addr_385f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3861_62;
                addr_3d94_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s3*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_451b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_458b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69)) + 1);
                        rsi70 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = *reinterpret_cast<unsigned char*>(&rdx10->f0) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_438f_103;
                            rdx10 = reinterpret_cast<struct s3*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_25a0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2590(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_3e8e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_384c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_3e98_112;
                    }
                } else {
                    addr_3e98_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_3f69_114;
                }
                addr_3858_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_385f_91;
                while (1) {
                    addr_3f69_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4477_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_3ed6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s3*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s3*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4485_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_3f57_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s3*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_3f57_128;
                        }
                    }
                    addr_3f05_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s3*>(&r9_37->f1);
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_3f57_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s3*>(&r15_16->f1);
                    continue;
                    addr_3ed6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3f05_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_377c_81;
                addr_4485_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_40d8_67;
                addr_451b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_3e8e_109;
                addr_458b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s3*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_3e8e_109;
                addr_3800_56:
                rax93 = fun_25b0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax24) * 2) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_384c_110;
                addr_3fde_59:
                goto addr_3fe0_40;
                addr_3cad_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_37f3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s3*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s3*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3858_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_37a5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_37f3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_3cf2_160;
                if (!v22) 
                    goto addr_40c7_162; else 
                    goto addr_42d3_163;
                addr_3cf2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_40c7_162:
                    r9_37 = reinterpret_cast<struct s3*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_40d8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_3b9b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_3a03_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3731_70; else 
                    goto addr_3a17_169;
                addr_3b9b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_36f8_63;
                goto addr_3880_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_3fd4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_410f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_37f0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s3*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s3*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_36e8_178; else 
                        goto addr_4092_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3fd4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_37f3_22;
                }
                addr_410f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_37f0_30:
                    r8d42 = 0;
                    goto addr_37f3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3861_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4128_42;
                    }
                }
                addr_36e8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_36f8_63;
                addr_4092_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_3ff0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3861_62;
                } else {
                    addr_40a2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_37f3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4852_188;
                if (v28) 
                    goto addr_40c7_162;
                addr_4852_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_3a03_168;
                addr_369c_37:
                if (v22) 
                    goto addr_4693_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_36b3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4160_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_41eb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_37f3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s3*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s3*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_36e8_178; else 
                        goto addr_41c7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3fd4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_37f3_22;
                }
                addr_41eb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_37f3_22;
                }
                addr_41c7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3ff0_46;
                goto addr_40a2_186;
                addr_36b3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_37f3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_37f3_22; else 
                    goto addr_36c4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s3*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_479e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s3*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4624_210;
            if (1) 
                goto addr_4622_212;
            if (!v29) 
                goto addr_425e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_23c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s3*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s3*>("\"");
            if (!0) 
                goto addr_4791_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s3*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s3*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_3a20_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_37db_219; else 
            goto addr_3a3a_220;
        addr_37bf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_37d3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_3a3a_220; else 
            goto addr_37db_219;
        addr_438f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_3a3a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_23c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_43ad_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_23c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s3*>("'");
        v29 = reinterpret_cast<struct s3*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s3*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s3*>(1);
        v22 = reinterpret_cast<struct s3*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s3*>(0);
            continue;
        }
        addr_4820_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4286_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s3*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s3*>(0);
        v22 = reinterpret_cast<struct s3*>(0);
        v28 = reinterpret_cast<struct s3*>(1);
        v26 = reinterpret_cast<struct s3*>("'");
        continue;
        addr_4477_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_37d3_221;
        addr_42d3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_37d3_221;
        addr_3a17_169:
        goto addr_3a20_65;
        addr_479e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_3a3a_220;
        goto addr_43ad_222;
        addr_4624_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(&v26->f0), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s3*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - g28;
        if (!rax111) 
            goto addr_467e_236;
        fun_23e0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4820_225;
        addr_4622_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4624_210;
        addr_425e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4624_210;
        } else {
            rdx10 = reinterpret_cast<struct s3*>(0);
            goto addr_4286_226;
        }
        addr_4791_216:
        r13_34 = reinterpret_cast<struct s3*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s3*>("\"");
        v29 = reinterpret_cast<struct s3*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s3*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s3*>(1);
        v22 = reinterpret_cast<struct s3*>(0);
        v31 = reinterpret_cast<struct s3*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_3bed_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x79ec + rax113 * 4) + 0x79ec;
    addr_4019_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x7aec + rax114 * 4) + 0x7aec;
    addr_4693_190:
    addr_37db_219:
    goto 0x34c0;
    addr_36c4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x78ec + rax115 * 4) + 0x78ec;
    addr_467e_236:
    goto v116;
}

void fun_36e0() {
}

void fun_3898() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3592;
}

void fun_38f1() {
    goto 0x3592;
}

void fun_39de() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3861;
    }
    if (v2) 
        goto 0x42d3;
    if (!r10_3) 
        goto addr_443e_5;
    if (!v4) 
        goto addr_430e_7;
    addr_443e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_430e_7:
    goto 0x3714;
}

void fun_39fc() {
}

void fun_3aa7() {
    signed char v1;

    if (v1) {
        goto 0x3a2f;
    } else {
        goto 0x376a;
    }
}

void fun_3ac1() {
    signed char v1;

    if (!v1) 
        goto 0x3aba; else 
        goto "???";
}

void fun_3ae8() {
    goto 0x3a03;
}

void fun_3b68() {
}

void fun_3b80() {
}

void fun_3baf() {
    goto 0x3a03;
}

void fun_3c01() {
    goto 0x3b90;
}

void fun_3c30() {
    goto 0x3b90;
}

void fun_3c63() {
    goto 0x3b90;
}

void fun_4030() {
    goto 0x36e8;
}

void fun_432e() {
    signed char v1;

    if (v1) 
        goto 0x42d3;
    goto 0x3714;
}

void fun_43d5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3714;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x36f8;
        goto 0x3714;
    }
}

void fun_47f2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x3a60;
    } else {
        goto 0x3592;
    }
}

void fun_5728() {
    fun_23b0();
}

void fun_290b() {
    goto 0x281f;
}

void fun_2b7e() {
    goto 0x281f;
}

void fun_391e() {
    goto 0x3592;
}

void fun_3af4() {
    goto 0x3aac;
}

void fun_3bbb() {
    goto 0x36e8;
}

void fun_3c0d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x3b90;
    goto 0x37bf;
}

void fun_3c3f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x3b9b;
        goto 0x35c0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x3a3a;
        goto 0x37db;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x43d8;
    if (r10_8 > r15_9) 
        goto addr_3b25_9;
    addr_3b2a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x43e3;
    goto 0x3714;
    addr_3b25_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_3b2a_10;
}

void fun_3c72() {
    goto 0x37a7;
}

void fun_4040() {
    goto 0x37a7;
}

void fun_47df() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x38fc;
    } else {
        goto 0x3a60;
    }
}

void fun_57e0() {
}

void fun_291e() {
    goto 0x281f;
}

int64_t fun_2d80() {
    return 12;
}

void fun_2b8c() {
    goto 0x281f;
}

void fun_3c7c() {
    goto 0x3c17;
}

void fun_404a() {
    goto 0x3b6d;
}

void fun_5840() {
    fun_23b0();
    goto fun_2580;
}

void fun_2931() {
    goto 0x281f;
}

int64_t fun_2d90() {
    return 13;
}

void fun_394d() {
    goto 0x3592;
}

void fun_3c88() {
    goto 0x3c17;
}

void fun_4057() {
    goto 0x3bbe;
}

void fun_5880() {
    fun_23b0();
    goto fun_2580;
}

void fun_2944() {
    goto 0x281f;
}

int64_t fun_2da0() {
    return 14;
}

void fun_397a() {
    goto 0x3592;
}

void fun_3c94() {
    goto 0x3b90;
}

void fun_58c0() {
    fun_23b0();
    goto fun_2580;
}

void fun_2957() {
    goto 0x281f;
}

int64_t fun_2db0() {
    return 15;
}

void fun_399c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4330;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3861;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3861;
    }
    if (v11) 
        goto 0x4693;
    if (r10_12 > r15_13) 
        goto addr_46e3_8;
    addr_46e8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4421;
    addr_46e3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_46e8_9;
}

struct s18 {
    signed char[24] pad24;
    int64_t f18;
};

struct s19 {
    signed char[16] pad16;
    struct s3* f10;
};

struct s20 {
    signed char[8] pad8;
    struct s3* f8;
};

void fun_5910() {
    int64_t r15_1;
    struct s18* rbx2;
    struct s3* r14_3;
    struct s19* rbx4;
    struct s3* r13_5;
    struct s20* rbx6;
    struct s3* r12_7;
    struct s3** rbx8;
    struct s3* rax9;
    struct s0* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_23b0();
    fun_2580(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x5932, __return_address(), v11, v12, v13);
    goto v14;
}

struct s21 {
    signed char[2] pad2;
    unsigned char f2;
};

struct s22 {
    signed char[3] pad3;
    unsigned char f3;
};

void fun_296a(struct s3* rdi, struct s3* rsi, struct s3* rdx, struct s3* rcx, int64_t r8) {
    uint32_t r15d6;
    struct s21* r13_7;
    void** rax8;
    void* rsi9;
    int64_t rax10;
    int64_t rdi11;
    int64_t rdi12;
    struct s22* r13_13;
    int64_t rdi14;

    r15d6 = r13_7->f2;
    rax8 = fun_25b0(rdi, rsi, rdx, rcx);
    rsi9 = *rax8;
    *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<unsigned char*>(&r15d6);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi9) + rax10 * 2 + 1) & 16)) 
        goto 0x287b;
    *reinterpret_cast<uint32_t*>(&rdi11) = *reinterpret_cast<unsigned char*>(&r15d6);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
    hextobin(rdi11);
    *reinterpret_cast<uint32_t*>(&rdi12) = r13_13->f3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi9) + rdi12 * 2 + 1) & 16)) {
        goto 0x281f;
    } else {
        *reinterpret_cast<uint32_t*>(&rdi14) = *reinterpret_cast<unsigned char*>(&rdi12);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
        hextobin(rdi14);
        goto 0x281f;
    }
}

int64_t fun_2dc0() {
    return 10;
}

void fun_5968() {
    fun_23b0();
    goto 0x5939;
}

int64_t fun_2dd0() {
    return 11;
}

struct s23 {
    signed char[32] pad32;
    int64_t f20;
};

struct s24 {
    signed char[24] pad24;
    int64_t f18;
};

struct s25 {
    signed char[16] pad16;
    struct s3* f10;
};

struct s26 {
    signed char[8] pad8;
    struct s3* f8;
};

struct s27 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_59a0() {
    int64_t rcx1;
    struct s23* rbx2;
    int64_t r15_3;
    struct s24* rbx4;
    struct s3* r14_5;
    struct s25* rbx6;
    struct s3* r13_7;
    struct s26* rbx8;
    struct s3* r12_9;
    struct s3** rbx10;
    int64_t v11;
    struct s27* rbx12;
    struct s3* rax13;
    struct s0* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_23b0();
    fun_2580(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x59d4, __return_address(), rcx1);
    goto v15;
}

void fun_5a18() {
    fun_23b0();
    goto 0x59db;
}
