undefined8 main(int param_1,undefined8 *param_2)

{
  byte **ppbVar1;
  byte bVar2;
  bool bVar3;
  bool bVar4;
  byte bVar5;
  char cVar6;
  int iVar7;
  char *pcVar8;
  byte *pbVar9;
  ushort **ppuVar10;
  byte bVar11;
  undefined8 extraout_RDX;
  ulong uVar12;
  ushort *puVar13;
  byte *pbVar14;
  uint uVar15;
  byte **ppbVar16;
  uint uVar17;
  
  pcVar8 = getenv("POSIXLY_CORRECT");
  bVar3 = pcVar8 != (char *)0x0;
  if (pcVar8 == (char *)0x0) {
    set_program_name(*param_2);
    setlocale(6,"");
    bindtextdomain("coreutils");
    textdomain("coreutils");
    atexit(close_stdout);
    if (param_1 == 2) {
LAB_0010269f:
      pcVar8 = (char *)param_2[1];
      iVar7 = strcmp(pcVar8,"--help");
      if (iVar7 == 0) {
                    /* WARNING: Subroutine does not return */
        usage(0);
      }
      iVar7 = strcmp(pcVar8,"--version");
      if (iVar7 == 0) {
        version_etc(stdout,&DAT_001071c6,"GNU coreutils",Version,"Brian Fox","Chet Ramey",0,
                    extraout_RDX);
        return 0;
      }
      param_1 = 1;
    }
    else {
      param_1 = param_1 + -1;
      if (param_1 < 1) goto LAB_0010276d;
    }
LAB_001026db:
    ppbVar16 = (byte **)(param_2 + 1);
    bVar4 = true;
    uVar17 = 0;
    do {
      pbVar9 = *ppbVar16;
      if (*pbVar9 != 0x2d) {
LAB_00102bf6:
        if ((bVar3) || ((char)uVar17 != '\0')) goto LAB_001027f7;
        goto LAB_00102745;
      }
      bVar5 = pbVar9[1];
      pbVar14 = pbVar9 + 1;
      if (bVar5 == 0) goto LAB_00102bf6;
      pbVar9 = pbVar9 + 2;
      bVar11 = bVar5;
      do {
        if ((0x29 < (byte)(bVar11 + 0xbb)) ||
           (uVar12 = 0x20100000001 >> (bVar11 + 0xbb & 0x3f), (uVar12 & 1) == 0)) {
          if ((bVar3) || ((char)uVar17 != '\0')) goto LAB_00102800;
          goto LAB_00102740;
        }
        bVar11 = *pbVar9;
        pbVar9 = pbVar9 + 1;
      } while (bVar11 != 0);
      do {
        while (pbVar14 = pbVar14 + 1, uVar15 = (uint)uVar12 & 1, bVar5 == 0x65) {
LAB_00102a10:
          bVar5 = *pbVar14;
          uVar17 = uVar15;
          if (bVar5 == 0) goto LAB_00102a36;
        }
        uVar15 = uVar17;
        if (bVar5 != 0x6e) {
          if (bVar5 == 0x45) {
            uVar15 = 0;
          }
          goto LAB_00102a10;
        }
        bVar5 = *pbVar14;
        bVar4 = false;
      } while (bVar5 != 0);
LAB_00102a36:
      ppbVar16 = ppbVar16 + 1;
      param_1 = param_1 + -1;
      uVar17 = uVar15;
    } while (param_1 != 0);
  }
  else {
    if ((1 < param_1) && (iVar7 = strcmp((char *)param_2[1],"-n"), iVar7 == 0)) {
      set_program_name(*param_2);
      setlocale(6,"");
      bindtextdomain("coreutils");
      textdomain("coreutils");
      atexit(close_stdout);
      if (param_1 == 2) goto LAB_0010269f;
      param_1 = param_1 + -1;
      goto LAB_001026db;
    }
    ppbVar16 = (byte **)(param_2 + 1);
    set_program_name(*param_2);
    setlocale(6,"");
    bindtextdomain("coreutils");
    textdomain("coreutils");
    atexit(close_stdout);
    param_1 = param_1 + -1;
    bVar4 = true;
    if (pcVar8 == (char *)0x0) {
LAB_00102740:
      if (0 < param_1) {
LAB_00102745:
        ppbVar1 = ppbVar16 + (param_1 - 1);
        for (; fputs_unlocked((char *)*ppbVar16,stdout), ppbVar1 != ppbVar16;
            ppbVar16 = ppbVar16 + 1) {
          pcVar8 = stdout->_IO_write_ptr;
          if (pcVar8 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar8 + 1;
            *pcVar8 = ' ';
          }
          else {
            __overflow(stdout,0x20);
          }
        }
      }
    }
    else {
LAB_001027f7:
      if (0 < param_1) {
LAB_00102800:
        ppbVar1 = ppbVar16 + (param_1 - 1);
        do {
          pbVar9 = *ppbVar16;
LAB_0010283e:
          bVar5 = *pbVar9;
          pbVar14 = pbVar9;
          if (bVar5 != 0) {
            do {
              pbVar9 = pbVar14 + 1;
              uVar17 = (uint)bVar5;
              if (bVar5 == 0x5c) {
                bVar11 = pbVar14[1];
                if (bVar11 == 0) {
                  uVar17 = 0x5c;
                }
                else {
                  pbVar9 = pbVar14 + 2;
                  switch(bVar11) {
                  case 0x30:
                    bVar11 = pbVar14[2];
                    if ((byte)(bVar11 - 0x30) < 8) {
                      bVar2 = pbVar14[3];
                      pbVar9 = pbVar14 + 3;
                      goto LAB_001028ae;
                    }
                    uVar17 = 0;
                    bVar5 = 0;
                    break;
                  case 0x31:
                  case 0x32:
                  case 0x33:
                  case 0x34:
                  case 0x35:
                  case 0x36:
                  case 0x37:
                    bVar2 = pbVar14[2];
LAB_001028ae:
                    bVar5 = bVar11 - 0x30;
                    if ((byte)(bVar2 - 0x30) < 8) {
                      pbVar9 = pbVar9 + 1;
                      bVar5 = (bVar2 - 0x30) + bVar5 * '\b';
                    }
                    if ((byte)(*pbVar9 - 0x30) < 8) {
                      bVar5 = (*pbVar9 - 0x30) + bVar5 * '\b';
                      pbVar9 = pbVar9 + 1;
                      uVar17 = (uint)bVar5;
                    }
                    else {
                      uVar17 = (uint)bVar5;
                    }
                    break;
                  default:
switchD_00102878_caseD_38:
                    uVar17 = (uint)bVar11;
                    pcVar8 = stdout->_IO_write_ptr;
                    bVar5 = bVar11;
                    if (stdout->_IO_write_end < pcVar8 || stdout->_IO_write_end == pcVar8) {
                      __overflow(stdout,0x5c);
                    }
                    else {
                      stdout->_IO_write_ptr = pcVar8 + 1;
                      *pcVar8 = '\\';
                    }
                    break;
                  case 0x5c:
                    uVar17 = 0x5c;
                    break;
                  case 0x61:
                    uVar17 = 7;
                    bVar5 = 7;
                    break;
                  case 0x62:
                    uVar17 = 8;
                    bVar5 = 8;
                    break;
                  case 99:
                    return 0;
                  case 0x65:
                    uVar17 = 0x1b;
                    bVar5 = 0x1b;
                    break;
                  case 0x66:
                    uVar17 = 0xc;
                    bVar5 = 0xc;
                    break;
                  case 0x6e:
                    uVar17 = 10;
                    bVar5 = 10;
                    break;
                  case 0x72:
                    uVar17 = 0xd;
                    bVar5 = 0xd;
                    break;
                  case 0x74:
                    uVar17 = 9;
                    bVar5 = 9;
                    break;
                  case 0x76:
                    uVar17 = 0xb;
                    bVar5 = 0xb;
                    break;
                  case 0x78:
                    bVar5 = pbVar14[2];
                    ppuVar10 = __ctype_b_loc();
                    puVar13 = *ppuVar10;
                    if ((*(byte *)((long)puVar13 + (ulong)bVar5 * 2 + 1) & 0x10) == 0)
                    goto switchD_00102878_caseD_38;
                    bVar5 = hextobin();
                    if ((*(byte *)((long)puVar13 + (ulong)pbVar14[3] * 2 + 1) & 0x10) == 0) {
                      pbVar9 = pbVar14 + 3;
                      uVar17 = (uint)bVar5;
                    }
                    else {
                      pbVar9 = pbVar14 + 4;
                      cVar6 = hextobin(pbVar14[3]);
                      bVar5 = bVar5 * '\x10' + cVar6;
                      uVar17 = (uint)bVar5;
                    }
                  }
                }
              }
              pbVar14 = (byte *)stdout->_IO_write_ptr;
              if (pbVar14 < stdout->_IO_write_end) goto code_r0x00102834;
              __overflow(stdout,uVar17);
              bVar5 = *pbVar9;
              pbVar14 = pbVar9;
              if (bVar5 == 0) break;
            } while( true );
          }
          if (ppbVar1 == ppbVar16) break;
          pcVar8 = stdout->_IO_write_ptr;
          if (pcVar8 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar8 + 1;
            *pcVar8 = ' ';
          }
          else {
            __overflow(stdout,0x20);
          }
          ppbVar16 = ppbVar16 + 1;
        } while( true );
      }
    }
  }
  if (!bVar4) {
    return 0;
  }
LAB_0010276d:
  pcVar8 = stdout->_IO_write_ptr;
  if (pcVar8 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar8 + 1;
    *pcVar8 = '\n';
  }
  else {
    __overflow(stdout,10);
  }
  return 0;
code_r0x00102834:
  stdout->_IO_write_ptr = (char *)(pbVar14 + 1);
  *pbVar14 = bVar5;
  goto LAB_0010283e;
}