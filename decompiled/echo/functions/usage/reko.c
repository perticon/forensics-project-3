void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
		fn0000000000002430("usage", 0x28, "src/echo.c", "status == EXIT_SUCCESS");
	fn0000000000002520(fn00000000000023B0(0x05, "Usage: %s [SHORT-OPTION]... [STRING]...\n  or:  %s LONG-OPTION\n", null), 0x01);
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "Echo the STRING(s) to standard output.\n\n  -n             do not output the trailing newline\n", null));
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "  -e             enable interpretation of backslash escapes\n  -E             disable interpretation of backslash escapes (default)\n", null));
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "\nIf -e is in effect, the following sequences are recognized:\n\n", null));
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n", null));
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "  \\0NNN   byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n", null));
	fn0000000000002520(fn00000000000023B0(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "\nNOTE: printf(1) is a preferred alternative,\nwhich does not have issues outputting option-like strings.\n", null));
	struct Eq_1036 * rbx_203 = fp + ~0xA7 + 16;
	do
	{
		char * rsi_205 = rbx_203->qw0000;
		++rbx_203;
	} while (rsi_205 != null && fn0000000000002480(rsi_205, &g_u71C6) != 0x00);
	ptr64 r13_218 = rbx_203->qw0008;
	if (r13_218 != 0x00)
	{
		fn0000000000002520(fn00000000000023B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_247 = fn0000000000002510(null, 0x05);
		if (rax_247 == 0x00 || fn0000000000002340(0x03, "en_", rax_247) == 0x00)
			goto l00000000000030BE;
	}
	else
	{
		fn0000000000002520(fn00000000000023B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_334 = fn0000000000002510(null, 0x05);
		if (rax_334 == 0x00 || fn0000000000002340(0x03, "en_", rax_334) == 0x00)
		{
			fn0000000000002520(fn00000000000023B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l00000000000030FB:
			fn0000000000002520(fn00000000000023B0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
			fn0000000000002560(0x00);
		}
		r13_218 = 29126;
	}
	fn0000000000002460(stdout, fn00000000000023B0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000030BE:
	fn0000000000002520(fn00000000000023B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l00000000000030FB;
}