void usage(int param_1)

{
  FILE *pFVar1;
  undefined **ppuVar2;
  int iVar3;
  undefined8 uVar4;
  char *pcVar5;
  undefined8 uVar6;
  undefined *puVar7;
  long in_FS_OFFSET;
  undefined *local_a8;
  char *local_a0;
  char *local_98 [5];
  char *local_70;
  char *local_68;
  char *local_60;
  char *local_58;
  char *local_50;
  undefined8 local_48;
  undefined8 local_40;
  undefined8 local_30;
  
  uVar6 = program_name;
  local_30 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  if (param_1 != 0) {
                    /* WARNING: Subroutine does not return */
    __assert_fail("status == EXIT_SUCCESS","src/echo.c",0x28,"usage");
  }
  uVar4 = dcgettext(0,"Usage: %s [SHORT-OPTION]... [STRING]...\n  or:  %s LONG-OPTION\n",5);
  __printf_chk(1,uVar4,uVar6,uVar6);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "Echo the STRING(s) to standard output.\n\n  -n             do not output the trailing newline\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "  -e             enable interpretation of backslash escapes\n  -E             disable interpretation of backslash escapes (default)\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,"      --help        display this help and exit\n",5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,"      --version     output version information and exit\n",5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,"\nIf -e is in effect, the following sequences are recognized:\n\n",5
                            );
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "  \\0NNN   byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  uVar6 = dcgettext(0,
                    "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell\'s documentation\nfor details about the options it supports.\n"
                    ,5);
  __printf_chk(1,uVar6,&DAT_001071c6);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "\nNOTE: printf(1) is a preferred alternative,\nwhich does not have issues outputting option-like strings.\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  local_48 = 0;
  local_a8 = &DAT_001071ed;
  local_a0 = "test invocation";
  local_98[0] = "coreutils";
  local_98[1] = "Multi-call invocation";
  local_98[4] = "sha256sum";
  local_98[2] = "sha224sum";
  local_68 = "sha384sum";
  local_98[3] = "sha2 utilities";
  local_70 = "sha2 utilities";
  local_60 = "sha2 utilities";
  local_58 = "sha512sum";
  local_50 = "sha2 utilities";
  local_40 = 0;
  ppuVar2 = &local_a8;
  do {
    puVar7 = (undefined *)ppuVar2;
    if (*(char **)(puVar7 + 0x10) == (char *)0x0) break;
    iVar3 = strcmp("echo",*(char **)(puVar7 + 0x10));
    ppuVar2 = (undefined **)(puVar7 + 0x10);
  } while (iVar3 != 0);
  puVar7 = *(undefined **)(puVar7 + 0x18);
  if (puVar7 == (undefined *)0x0) {
    uVar6 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar6,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar5 = setlocale(5,(char *)0x0);
    if (pcVar5 != (char *)0x0) {
      iVar3 = strncmp(pcVar5,"en_",3);
      if (iVar3 != 0) {
        puVar7 = &DAT_001071c6;
        goto LAB_001031ba;
      }
    }
    uVar6 = dcgettext(0,"Full documentation <%s%s>\n",5);
    puVar7 = &DAT_001071c6;
    __printf_chk(1,uVar6,"https://www.gnu.org/software/coreutils/",&DAT_001071c6);
    pcVar5 = " invocation";
  }
  else {
    uVar6 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar6,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar5 = setlocale(5,(char *)0x0);
    if (pcVar5 != (char *)0x0) {
      iVar3 = strncmp(pcVar5,"en_",3);
      if (iVar3 != 0) {
LAB_001031ba:
        pFVar1 = stdout;
        pcVar5 = (char *)dcgettext(0,
                                   "Report any translation bugs to <https://translationproject.org/team/>\n"
                                   ,5);
        fputs_unlocked(pcVar5,pFVar1);
      }
    }
    uVar6 = dcgettext(0,"Full documentation <%s%s>\n",5);
    __printf_chk(1,uVar6,"https://www.gnu.org/software/coreutils/",&DAT_001071c6);
    pcVar5 = " invocation";
    if (puVar7 != &DAT_001071c6) {
      pcVar5 = "";
    }
  }
  uVar6 = dcgettext(0,"or available locally via: info \'(coreutils) %s%s\'\n",5);
  __printf_chk(1,uVar6,puVar7,pcVar5);
                    /* WARNING: Subroutine does not return */
  exit(0);
}