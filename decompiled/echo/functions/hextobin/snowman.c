int64_t hextobin(int64_t rdi) {
    int32_t eax2;
    int64_t rdi3;
    int64_t rax4;
    int64_t rax5;

    eax2 = static_cast<int32_t>(rdi - 65);
    if (*reinterpret_cast<unsigned char*>(&eax2) > 37) {
        *reinterpret_cast<uint32_t*>(&rdi3) = *reinterpret_cast<unsigned char*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi3) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rax4) = static_cast<int32_t>(rdi3 - 48);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        return rax4;
    } else {
        *reinterpret_cast<uint32_t*>(&rax5) = *reinterpret_cast<unsigned char*>(&eax2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x7004 + rax5 * 4) + 0x7004;
    }
}