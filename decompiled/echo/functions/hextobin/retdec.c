int32_t hextobin(unsigned char c) {
    int64_t v1 = (int64_t)(char)c + 0xffffffbf; // 0x2d50
    if ((char)v1 >= 38) {
        // 0x2d70
        return (int32_t)c - 48;
    }
    int32_t v2 = *(int32_t *)((4 * v1 & 1020) + (int64_t)&g1); // 0x2d61
    return v2 + (int32_t)&g1;
}