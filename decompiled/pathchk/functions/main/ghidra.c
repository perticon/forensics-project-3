byte main(int param_1,undefined8 *param_2)

{
  char *__s;
  bool bVar1;
  bool bVar2;
  int iVar3;
  size_t sVar4;
  size_t sVar5;
  int *piVar6;
  ulong uVar7;
  char *pcVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  bool bVar11;
  ulong uVar12;
  char *pcVar13;
  ulong uVar14;
  ulong uVar15;
  char cVar16;
  long in_FS_OFFSET;
  byte local_f7;
  stat local_d8;
  long local_40;
  
  bVar1 = false;
  bVar2 = false;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  while (iVar3 = getopt_long(param_1,param_2,&DAT_001070f2,longopts), iVar3 != -1) {
    if (iVar3 == 0x50) {
LAB_00102770:
      bVar1 = true;
    }
    else {
      if (iVar3 < 0x51) {
        if (iVar3 == -0x83) {
          version_etc(stdout,"pathchk","GNU coreutils",Version,"Paul Eggert","David MacKenzie",
                      "Jim Meyering",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar3 == -0x82) {
          usage();
          goto LAB_00102770;
        }
        goto LAB_00102cd0;
      }
      if (iVar3 != 0x70) {
        if (iVar3 != 0x80) goto LAB_00102cd0;
        bVar1 = true;
      }
      bVar2 = true;
    }
  }
  if (optind == param_1) {
    uVar9 = dcgettext(0,"missing operand",5);
    error(0,0,uVar9);
LAB_00102cd0:
    usage(1);
  }
  else {
    local_f7 = 1;
    for (; optind < param_1; optind = optind + 1) {
      __s = (char *)param_2[optind];
      sVar4 = strlen(__s);
      pcVar8 = __s;
      if (bVar1) {
        while (pcVar8 = strchr(pcVar8,0x2d), pcVar8 != (char *)0x0) {
          if ((__s == pcVar8) || (pcVar8[-1] == '/')) {
            uVar9 = quotearg_style(4,__s);
            uVar10 = dcgettext(0,"leading \'-\' in a component of file name %s",5);
            error(0,0,uVar10,uVar9);
            bVar11 = false;
            goto LAB_00102808;
          }
          pcVar8 = pcVar8 + 1;
        }
        if (sVar4 != 0) {
          if (bVar2) goto LAB_0010285b;
          goto LAB_001027a9;
        }
LAB_00102b2b:
        uVar9 = dcgettext(0,"empty file name",5);
        error(0,0,uVar9);
        bVar11 = false;
      }
      else if (bVar2) {
        if (sVar4 == 0) goto LAB_00102b2b;
LAB_0010285b:
        sVar5 = strspn(__s,"/ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._-");
        pcVar8 = __s + sVar5;
        if (*pcVar8 != '\0') {
          local_d8.st_dev = 0;
          uVar14 = rpl_mbrlen(pcVar8,sVar4 - sVar5,&local_d8);
          quotearg_n_style(0,4,__s);
          if (0x10 < uVar14) {
            uVar14 = 1;
          }
          uVar9 = quotearg_n_style_mem(1,8,pcVar8,uVar14);
          uVar10 = dcgettext(0,"nonportable character %s in file name %s",5);
          error(0,0,uVar10,uVar9);
          bVar11 = false;
          goto LAB_00102808;
        }
        if (0xff < sVar4) {
          uVar14 = 0x100;
LAB_00102b91:
          uVar9 = quotearg_style(4,__s);
          uVar10 = dcgettext(0,"limit %lu exceeded by length %lu of file name %s",5);
          error(0,0,uVar10,uVar14 - 1,sVar4,uVar9);
          bVar11 = false;
          goto LAB_00102808;
        }
        uVar14 = 0xe;
        if (!bVar2) goto LAB_001029f7;
LAB_00102898:
        pcVar8 = __s;
        uVar15 = 0xe;
        do {
          while (pcVar13 = pcVar8, cVar16 = *pcVar13, cVar16 == '/') {
            pcVar8 = pcVar13 + 1;
          }
          if (cVar16 == '\0') goto LAB_001029f7;
          uVar7 = uVar14;
          if (uVar14 == 0) {
            pcVar8 = ".";
            if (__s != pcVar13) {
              pcVar8 = __s;
            }
            piVar6 = __errno_location();
            *piVar6 = 0;
            *pcVar13 = '\0';
            uVar7 = pathconf(pcVar8,3);
            *pcVar13 = cVar16;
            if ((long)uVar7 < 0) {
              if (*piVar6 == 0) {
                uVar7 = 0xffffffffffffffff;
              }
              else {
                uVar14 = uVar15;
                uVar7 = uVar15;
                if (*piVar6 != 2) {
                  *pcVar13 = '\0';
                  uVar9 = quotearg_n_style_colon(0,3,pcVar8);
                  error(0,*piVar6,"%s",uVar9);
                  *pcVar13 = cVar16;
                  bVar11 = false;
                  goto LAB_00102808;
                }
              }
            }
          }
          cVar16 = pcVar13[1];
          if ((cVar16 == '/') || (cVar16 == '\0')) {
            pcVar8 = pcVar13 + 1;
            uVar12 = 1;
          }
          else {
            uVar12 = 1;
            do {
              uVar12 = uVar12 + 1;
              cVar16 = pcVar13[uVar12];
              pcVar8 = pcVar13 + uVar12;
              if (cVar16 == '/') break;
            } while (cVar16 != '\0');
          }
          uVar15 = uVar7;
        } while (uVar12 <= uVar7);
        *pcVar8 = '\0';
        uVar9 = quote(pcVar13);
        uVar10 = dcgettext(0,"limit %lu exceeded by length %lu of file name component %s",5);
        error(0,0,uVar10,uVar7,uVar12,uVar9);
        bVar11 = false;
        *pcVar8 = cVar16;
      }
      else {
LAB_001027a9:
        iVar3 = lstat(__s,&local_d8);
        if (iVar3 == 0) {
LAB_001029f7:
          bVar11 = true;
        }
        else {
          piVar6 = __errno_location();
          bVar11 = *piVar6 != 2 || sVar4 == 0;
          if (*piVar6 == 2 && sVar4 != 0) {
            pcVar8 = __s;
            if (0xff < sVar4) {
              cVar16 = *__s;
              *piVar6 = 0;
              pcVar13 = ".";
              if (cVar16 == '/') {
                pcVar13 = "/";
              }
              uVar14 = pathconf(pcVar13,4);
              if ((long)uVar14 < 0) {
                if (*piVar6 != 0) {
                  uVar9 = dcgettext(0,"%s: unable to determine maximum file name length",5);
                  error(0,*piVar6,uVar9,pcVar13);
                  goto LAB_00102808;
                }
              }
              else if (uVar14 <= sVar4) goto LAB_00102b91;
            }
            do {
              for (; *pcVar8 == '/'; pcVar8 = pcVar8 + 1) {
LAB_00102aa0:
              }
              if (*pcVar8 == '\0') goto LAB_001029f7;
              if ((pcVar8[1] == '/') || (pcVar8[1] == '\0')) goto LAB_00102aa0;
              uVar14 = 1;
              do {
                uVar14 = uVar14 + 1;
                if (pcVar8[uVar14] == '/') break;
              } while (pcVar8[uVar14] != '\0');
              pcVar8 = pcVar8 + uVar14;
            } while (uVar14 < 0xf);
            uVar14 = 0;
            goto LAB_00102898;
          }
          uVar9 = quotearg_n_style_colon(0,3,__s);
          error(0,*piVar6,"%s",uVar9);
          bVar11 = false;
        }
      }
LAB_00102808:
      local_f7 = local_f7 & bVar11;
    }
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return local_f7 ^ 1;
    }
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}