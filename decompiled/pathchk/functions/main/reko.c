void main(char * rsi[], int32 edi, struct Eq_366 * fs)
{
	ptr64 fp;
	word64 rax_36 = fs->qw0028;
	set_program_name(rsi[0]);
	fn0000000000002550("", 0x06);
	fn00000000000023C0("/usr/local/share/locale", "coreutils");
	fn0000000000002390("coreutils");
	atexit(&g_t3130);
	byte r13b_114 = 0x00;
	uip64 r14_1460 = 0x00;
	while (true)
	{
		byte r14b_116 = (byte) r14_1460;
		int32 eax_83 = fn0000000000002410(&g_tAB80, "+pP", rsi, edi, null);
		if (eax_83 == ~0x00)
			break;
		if (eax_83 != 0x50)
		{
			if (eax_83 <= 0x50)
			{
				if (eax_83 == ~0x82)
				{
					version_etc(0x7004, stdout, fs);
					fn00000000000025A0(0x00);
				}
				if (eax_83 == ~0x81)
					usage(0x00);
l0000000000002CD0:
				usage(0x01);
			}
			if (eax_83 != 0x70)
			{
				if (eax_83 != 0x80)
					goto l0000000000002CD0;
				r14_1460 = 0x01;
			}
			r13b_114 = 0x01;
		}
		else
			r14_1460 = 0x01;
	}
	int32 eax_123 = g_dwB090;
	if (eax_123 == edi)
	{
		fn0000000000002570(fn00000000000023D0(0x05, "missing operand", null), 0x00, 0x00);
		goto l0000000000002CD0;
	}
	if (eax_123 >= edi)
	{
l0000000000002AEE:
		if (rax_36 - fs->qw0028 == 0x00)
			return;
		fn0000000000002400();
	}
l0000000000002826:
	word32 * r13_325;
	Eq_26 rax_135;
	Eq_26 rbp_125 = rsi[(int64) eax_123];
	Eq_26 rax_127 = fn00000000000023F0(rbp_125);
	word32 eax_212 = (word32) rax_127;
	if (r14b_116 == 0x00)
	{
		if (r13b_114 != 0x00)
		{
			rax_135 = rax_127;
			if (rax_127 != 0x00)
				goto l000000000000285B;
			goto l0000000000002B2B;
		}
	}
	else
	{
		Eq_26 rdi_131 = rbp_125;
		while (true)
		{
			rax_135 = fn0000000000002430(0x2D, rdi_131);
			eax_212 = (word32) rax_135;
			if (rax_135 == 0x00)
				break;
			if (rbp_125 == rax_135 || *((word64) rax_135 - 1) == 0x2F)
			{
				quotearg_style(0x04, fs);
				fn0000000000002570(fn00000000000023D0(0x05, "leading '-' in a component of file name %s", null), 0x00, 0x00);
				goto l0000000000002808;
			}
			rdi_131 = (word64) rax_135 + 1;
		}
		if (rax_127 == 0x00)
		{
l0000000000002B2B:
			fn0000000000002570(fn00000000000023D0(0x05, "empty file name", null), 0x00, 0x00);
			goto l0000000000002808;
		}
		if (r13b_114 != 0x00)
		{
l000000000000285B:
			fn0000000000002470();
			word64 r12_405 = rbp_125 + rax_135;
			if (*r12_405 != 0x00)
			{
				up64 rax_419 = rpl_mbrlen(fp - 0xD8, rax_127 - rax_135, r12_405, fs);
				quotearg_n_style(0x04, 0x00, fs);
				quotearg_n_style_mem(0x08, 0x01, fs);
				fn0000000000002570(fn00000000000023D0(0x05, "nonportable character %s in file name %s", null), 0x00, 0x00);
				goto l0000000000002808;
			}
			if (rax_127 <= 0xFF)
			{
				r13_325 = (word32 *) 0x0E;
				if (r13b_114 == 0x00)
					goto l00000000000029F7;
				goto l0000000000002898;
			}
l0000000000002B91:
			quotearg_style(0x04, fs);
			fn0000000000002570(fn00000000000023D0(0x05, "limit %lu exceeded by length %lu of file name %s", null), 0x00, 0x00);
			goto l0000000000002808;
		}
	}
	fn0000000000002480();
	if (eax_212 == 0x00)
		goto l00000000000029F7;
	word32 * rax_218 = fn0000000000002340();
	int8 al_227 = (int8) (rax_127 == 0x00);
	Eq_26 rax_247 = SEQ(SLICE(rax_218, word56, 8), al_227);
	if (((int8) (*rax_218 != 0x02) | al_227) != 0x00)
	{
		quotearg_n_style_colon(0x03, 0x00, fs);
		fn0000000000002570(29928, *rax_218, 0x00);
		goto l0000000000002808;
	}
	if (rax_127 > 0xFF)
	{
		*rax_218 = 0x00;
		*rbp_125 != 0x2F;
		fn00000000000023A0();
		if (rax_247 >= 0x00)
		{
			if (rax_127 >= rax_247)
				goto l0000000000002B91;
		}
		else if (*rax_218 != 0x00)
		{
			fn0000000000002570(fn00000000000023D0(0x05, "%s: unable to determine maximum file name length", null), *rax_218, 0x00);
l0000000000002808:
			eax_123 = g_dwB090 + 0x01;
			g_dwB090 = eax_123;
			if (eax_123 >= edi)
				goto l0000000000002AEE;
			goto l0000000000002826;
		}
	}
	Eq_26 rax_284 = rbp_125;
	while (true)
	{
l0000000000002AA4:
		byte dl_289 = *rax_284;
		if (dl_289 != 0x2F)
		{
			if (dl_289 == 0x00)
				goto l00000000000029F7;
			byte dl_297 = *((word64) rax_284 + 1);
			if (dl_297 != 0x2F && dl_297 != 0x00)
			{
				Eq_793 rdx_307 = 0x01;
				do
				{
					rdx_307 = (word64) rdx_307 + 1;
					byte cl_315 = Mem920[rax_284 + rdx_307:byte];
					word64 rsi_314 = rax_284 + rdx_307;
				} while (cl_315 != 0x2F && cl_315 != 0x00);
				if (rdx_307 > 0x0E)
				{
					r13_325 = null;
l0000000000002898:
					Eq_26 rbx_580 = rbp_125;
					word32 * r14_582 = (word32 *) 0x0E;
					word32 * rbp_584 = r13_325;
					while (true)
					{
						while (true)
						{
							Eq_530 r12b_589 = *rbx_580;
							if (r12b_589 != 0x2F)
								break;
							rbx_580 = (word64) rbx_580 + 1;
						}
						if (r12b_589 == 0x00)
							break;
						if (rbp_584 == null)
						{
							word32 * rax_610 = fn0000000000002340();
							*rax_610 = 0x00;
							*rbx_580 = 0x00;
							fn00000000000023A0();
							*rbx_580 = r12b_589;
							if (rax_610 >= null)
								r14_582 = rax_610;
							else
							{
								word32 eax_626 = *rax_610;
								if (eax_626 != 0x00)
								{
									if (eax_626 != 0x02)
									{
										*rbx_580 = 0x00;
										quotearg_n_style_colon(0x03, 0x00, fs);
										fn0000000000002570(29928, *rax_610, 0x00);
										*rbx_580 = r12b_589;
										goto l0000000000002808;
									}
									rbp_584 = r14_582;
								}
								else
									r14_582 = (word32 *) ~0x00;
							}
						}
						else
							r14_582 = rbp_584;
						Eq_26 r10_696;
						word32 * r8_698;
						byte r15b_689 = *((word64) rbx_580 + 1);
						if (r15b_689 != 0x2F && r15b_689 != 0x00)
						{
							r8_698 = (word32 *) 0x01;
							do
							{
								r8_698 = (word32 *) ((char *) r8_698 + 1);
								r15b_689 = Mem686[rbx_580 + r8_698:byte];
								r10_696 = rbx_580 + r8_698;
							} while (r15b_689 != 0x2F && r15b_689 != 0x00);
						}
						else
						{
							r10_696 = (word64) rbx_580 + 1;
							r8_698 = (word32 *) 0x01;
						}
						if (r14_582 < r8_698)
						{
							*r10_696 = 0x00;
							quote(fs);
							fn0000000000002570(fn00000000000023D0(0x05, "limit %lu exceeded by length %lu of file name component %s", null), 0x00, 0x00);
							*r10_696 = r15b_689;
							goto l0000000000002808;
						}
						rbx_580 = r10_696;
					}
l00000000000029F7:
					goto l0000000000002808;
				}
				rax_284 = rsi_314;
				goto l0000000000002AA4;
			}
		}
		rax_284 = (word64) rax_284 + 1;
	}
}