no_leading_hyphen (char const *file)
{
  char const *p;

  for (p = file;  (p = strchr (p, '-'));  p++)
    if (p == file || p[-1] == '/')
      {
        error (0, 0, _("leading '-' in a component of file name %s"),
               quoteaf (file));
        return false;
      }

  return true;
}