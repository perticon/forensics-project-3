void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000025C0(fn00000000000023D0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002E2E;
	}
	fn0000000000002560(fn00000000000023D0(0x05, "Usage: %s [OPTION]... NAME...\n", null), 0x01);
	fn00000000000024A0(stdout, fn00000000000023D0(0x05, "Diagnose invalid or unportable file names.\n\n  -p                  check for most POSIX systems\n  -P                  check for empty names and leading \"-\"\n      --portability   check for all POSIX systems (equivalent to -p -P)\n", null));
	fn00000000000024A0(stdout, fn00000000000023D0(0x05, "      --help        display this help and exit\n", null));
	fn00000000000024A0(stdout, fn00000000000023D0(0x05, "      --version     output version information and exit\n", null));
	struct Eq_934 * rbx_139 = fp - 0xB8 + 16;
	do
	{
		char * rsi_141 = rbx_139->qw0000;
		++rbx_139;
	} while (rsi_141 != null && fn00000000000024C0(rsi_141, "pathchk") != 0x00);
	ptr64 r13_154 = rbx_139->qw0008;
	if (r13_154 != 0x00)
	{
		fn0000000000002560(fn00000000000023D0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_241 = fn0000000000002550(null, 0x05);
		if (rax_241 == 0x00 || fn0000000000002350(0x03, "en_", rax_241) == 0x00)
			goto l0000000000002FE6;
	}
	else
	{
		fn0000000000002560(fn00000000000023D0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_183 = fn0000000000002550(null, 0x05);
		if (rax_183 == 0x00 || fn0000000000002350(0x03, "en_", rax_183) == 0x00)
		{
			fn0000000000002560(fn00000000000023D0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003023:
			fn0000000000002560(fn00000000000023D0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002E2E:
			fn00000000000025A0(edi);
		}
		r13_154 = 0x7004;
	}
	fn00000000000024A0(stdout, fn00000000000023D0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002FE6:
	fn0000000000002560(fn00000000000023D0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003023;
}