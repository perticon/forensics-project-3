
int64_t fun_23e0();

int64_t fun_2330(void** rdi, ...);

void* quotearg_buffer_restyled(void** rdi, void* rsi, void** rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_23e0();
    if (r8d > 10) {
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x75a0 + rax11 * 4) + 0x75a0;
    }
}

struct s0 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void** g28;

int32_t* fun_2340(void** rdi, ...);

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_2460();

struct s1 {
    void* f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2320(void** rdi);

void** xcharalloc(void* rdi, ...);

void fun_2400();

void** quotearg_n_options(void** rdi, void** rsi, int64_t rdx, struct s0* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s1* rbx16;
    uint32_t r15d17;
    void* rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void* rax23;
    void* rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x487f;
    rax8 = fun_2340(rdi);
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
        fun_2330(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6651]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x490b;
            fun_2460();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s1*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->pad40, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax23) + 1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb0e0) {
                fun_2320(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x499a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2400();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s2 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s2* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s2* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x753b);
            if (!reinterpret_cast<int1_t>(*rdi == 96)) {
                rax7 = reinterpret_cast<void**>(0x7534);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x753f);
            if (!reinterpret_cast<int1_t>(*rdi == 96)) {
                rax10 = reinterpret_cast<void**>(0x7530);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae58 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae58;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2313() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_2323() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2040;

void fun_2333() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_2343() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_2353() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_2363() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_2373() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x2090;

void fun_2383() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20a0;

void fun_2393() {
    __asm__("cli ");
    goto textdomain;
}

int64_t pathconf = 0x20b0;

void fun_23a3() {
    __asm__("cli ");
    goto pathconf;
}

int64_t fclose = 0x20c0;

void fun_23b3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_23c3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20e0;

void fun_23d3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x20f0;

void fun_23e3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2100;

void fun_23f3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2110;

void fun_2403() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2120;

void fun_2413() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2130;

void fun_2423() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2140;

void fun_2433() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x2150;

void fun_2443() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2160;

void fun_2453() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2170;

void fun_2463() {
    __asm__("cli ");
    goto memset;
}

int64_t strspn = 0x2180;

void fun_2473() {
    __asm__("cli ");
    goto strspn;
}

int64_t lstat = 0x2190;

void fun_2483() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x21a0;

void fun_2493() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21b0;

void fun_24a3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21c0;

void fun_24b3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21d0;

void fun_24c3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x21e0;

void fun_24d3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x21f0;

void fun_24e3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2200;

void fun_24f3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2210;

void fun_2503() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2220;

void fun_2513() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2230;

void fun_2523() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2240;

void fun_2533() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2250;

void fun_2543() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2260;

void fun_2553() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2270;

void fun_2563() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2280;

void fun_2573() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2290;

void fun_2583() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x22a0;

void fun_2593() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22b0;

void fun_25a3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x22c0;

void fun_25b3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x22d0;

void fun_25c3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x22e0;

void fun_25d3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x22f0;

void fun_25e3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2300;

void fun_25f3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2550(int64_t rdi, ...);

void fun_23c0(int64_t rdi, int64_t rsi);

void fun_2390(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2410(int64_t rdi, void*** rsi, void** rdx, int64_t rcx);

void** stdout = reinterpret_cast<void**>(0);

int64_t Version = 0x74cb;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_25a0();

void usage();

int32_t optind = 0;

void** fun_23d0();

void fun_2570();

void** fun_23f0(void** rdi, ...);

void** fun_2430(void** rdi, int64_t rsi, void** rdx);

void* fun_2470(void** rdi, void** rsi, void** rdx);

uint64_t rpl_mbrlen(signed char* rdi);

int64_t quotearg_n_style();

int64_t quotearg_n_style_mem(int64_t rdi, int64_t rsi, signed char* rdx, uint64_t rcx);

int64_t quotearg_style(int64_t rdi, void** rsi, void** rdx);

void** fun_23a0(void** rdi, void** rsi, void** rdx);

int64_t quote(void** rdi, void** rsi, void** rdx);

int64_t quotearg_n_style_colon();

int32_t fun_2480(void** rdi, void* rsi, void** rdx);

int64_t fun_2643(int32_t edi, void*** rsi) {
    int32_t r14d3;
    int32_t r13d4;
    int32_t ebp5;
    void*** rbx6;
    void** rdi7;
    void** rax8;
    void** v9;
    void* rsp10;
    void** rdx11;
    int64_t rdi12;
    int32_t eax13;
    void** rdi14;
    int64_t rcx15;
    int32_t eax16;
    unsigned char v17;
    signed char v18;
    signed char v19;
    int32_t v20;
    void*** v21;
    void** rbp22;
    void** rax23;
    void* rsp24;
    void** r15_25;
    void** rdi26;
    void** rax27;
    void** rax28;
    uint32_t r8d29;
    void** rsi30;
    void** rdi31;
    void* rax32;
    signed char* r12_33;
    uint64_t rax34;
    uint64_t r15_35;
    void** rax36;
    void** r13_37;
    void** v38;
    void** rbx39;
    void** r14_40;
    void** rbp41;
    void** rax42;
    uint32_t r12d43;
    uint32_t r15d44;
    void** r10_45;
    void** r8_46;
    void** r15_47;
    int32_t* rax48;
    void** rax49;
    void** rax50;
    int32_t eax51;
    int32_t* rax52;
    unsigned char r8b53;
    int1_t zf54;
    void** r12_55;
    void** rax56;
    void** rax57;
    void** rax58;
    uint32_t edx59;
    uint32_t ecx60;
    void** rax61;
    int32_t eax62;
    uint32_t eax63;
    int64_t rax64;
    void* rdx65;

    __asm__("cli ");
    r14d3 = 0;
    r13d4 = 0;
    ebp5 = edi;
    rbx6 = rsi;
    rdi7 = *rsi;
    rax8 = g28;
    v9 = rax8;
    set_program_name(rdi7);
    fun_2550(6, 6);
    fun_23c0("coreutils", "/usr/local/share/locale");
    fun_2390("coreutils", "/usr/local/share/locale");
    atexit(0x3130, "/usr/local/share/locale");
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xc8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    while (rdx11 = reinterpret_cast<void**>("+pP"), *reinterpret_cast<int32_t*>(&rdi12) = ebp5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0, eax13 = fun_2410(rdi12, rbx6, "+pP", 0xab80), rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8), eax13 != -1) {
        if (eax13 != 80) {
            if (eax13 > 80) {
                if (eax13 != 0x70) {
                    if (eax13 != 0x80) 
                        goto addr_2cd0_7;
                    r14d3 = 1;
                }
                r13d4 = 1;
                continue;
            } else {
                if (eax13 == 0xffffff7d) {
                    rdi14 = stdout;
                    rcx15 = Version;
                    version_etc(rdi14, "pathchk", "GNU coreutils", rcx15, "Paul Eggert", "David MacKenzie", "Jim Meyering", 0);
                    eax13 = fun_25a0();
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 - 8 - 8 + 8 - 8 + 8);
                }
                if (eax13 != 0xffffff7e) 
                    goto addr_2cd0_7;
                usage();
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
            }
        }
        r14d3 = 1;
    }
    eax16 = optind;
    if (eax16 == ebp5) {
        fun_23d0();
        fun_2570();
    } else {
        v17 = 1;
        if (eax16 < ebp5) {
            v18 = *reinterpret_cast<signed char*>(&r13d4);
            v19 = *reinterpret_cast<signed char*>(&r14d3);
            v20 = ebp5;
            v21 = rbx6;
            do {
                rbp22 = v21[eax16 * 8];
                rax23 = fun_23f0(rbp22, rbp22);
                rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                r15_25 = rax23;
                if (v19) {
                    rdi26 = rbp22;
                    while (rax27 = fun_2430(rdi26, 45, rdx11), rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8), !!rax27) {
                        if (rbp22 == rax27) 
                            goto addr_2a02_23;
                        if (*reinterpret_cast<signed char*>(rax27 + 0xffffffffffffffff) == 47) 
                            goto addr_2a02_23;
                        rdi26 = rax27 + 1;
                    }
                } else {
                    if (v18) {
                        if (!rax23) 
                            goto addr_2b2b_28; else 
                            goto addr_285b_29;
                    }
                }
                if (!r15_25) {
                    addr_2b2b_28:
                    rax28 = fun_23d0();
                    rdx11 = rax28;
                    fun_2570();
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8);
                    r8d29 = 0;
                    continue;
                } else {
                    if (v18) {
                        addr_285b_29:
                        rsi30 = reinterpret_cast<void**>("/ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._-");
                        rdi31 = rbp22;
                        rax32 = fun_2470(rdi31, "/ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789._-", rdx11);
                        rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
                        r12_33 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp22) + reinterpret_cast<uint64_t>(rax32));
                        if (*r12_33) {
                            rax34 = rpl_mbrlen(r12_33);
                            r15_35 = rax34;
                            quotearg_n_style();
                            if (r15_35 > 16) {
                                *reinterpret_cast<int32_t*>(&r15_35) = 1;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_35) + 4) = 0;
                            }
                            quotearg_n_style_mem(1, 8, r12_33, r15_35);
                            rax36 = fun_23d0();
                            rdx11 = rax36;
                            fun_2570();
                            rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                            r8d29 = 0;
                            continue;
                        } else {
                            if (reinterpret_cast<unsigned char>(r15_25) <= reinterpret_cast<unsigned char>(0xff)) {
                                *reinterpret_cast<int32_t*>(&r13_37) = 14;
                                *reinterpret_cast<int32_t*>(&r13_37 + 4) = 0;
                                if (!v18) {
                                    addr_29f7_38:
                                    r8d29 = 1;
                                    continue;
                                } else {
                                    addr_2898_39:
                                    v38 = rbp22;
                                    rbx39 = rbp22;
                                    *reinterpret_cast<int32_t*>(&r14_40) = 14;
                                    *reinterpret_cast<int32_t*>(&r14_40 + 4) = 0;
                                    rbp41 = r13_37;
                                    goto addr_28b4_40;
                                }
                            }
                        }
                    } else {
                        goto addr_27a9_42;
                    }
                }
                addr_2b91_43:
                quotearg_style(4, rbp22, rdx11);
                rax42 = fun_23d0();
                rdx11 = rax42;
                fun_2570();
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8);
                r8d29 = 0;
                continue;
                while (1) {
                    addr_28b4_40:
                    r12d43 = reinterpret_cast<unsigned char>(*rbx39);
                    if (*reinterpret_cast<void**>(&r12d43) == 47) {
                        ++rbx39;
                        continue;
                    }
                    if (!*reinterpret_cast<void**>(&r12d43)) 
                        goto addr_29f7_38;
                    if (!rbp41) 
                        goto addr_28d0_47;
                    r14_40 = rbp41;
                    addr_290c_49:
                    r15d44 = reinterpret_cast<unsigned char>(rbx39[1]);
                    if (*reinterpret_cast<void**>(&r15d44) == 47 || !*reinterpret_cast<void**>(&r15d44)) {
                        r10_45 = rbx39 + 1;
                        *reinterpret_cast<int32_t*>(&r8_46) = 1;
                        *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
                    } else {
                        *reinterpret_cast<int32_t*>(&r8_46) = 1;
                        *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
                        do {
                            ++r8_46;
                            r15d44 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx39) + reinterpret_cast<unsigned char>(r8_46));
                            r10_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx39) + reinterpret_cast<unsigned char>(r8_46));
                            if (*reinterpret_cast<void**>(&r15d44) == 47) 
                                break;
                        } while (*reinterpret_cast<void**>(&r15d44));
                        goto addr_293a_54;
                    }
                    addr_2940_55:
                    if (reinterpret_cast<unsigned char>(r14_40) < reinterpret_cast<unsigned char>(r8_46)) 
                        break;
                    rbx39 = r10_45;
                    continue;
                    addr_293a_54:
                    goto addr_2940_55;
                    addr_28d0_47:
                    r15_47 = reinterpret_cast<void**>(".");
                    if (v38 != rbx39) {
                        r15_47 = v38;
                    }
                    rax48 = fun_2340(rdi31, rdi31);
                    *reinterpret_cast<int32_t*>(&rsi30) = 3;
                    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                    *rax48 = 0;
                    rdi31 = r15_47;
                    *rbx39 = reinterpret_cast<void*>(0);
                    rax49 = fun_23a0(rdi31, 3, rdx11);
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
                    *rbx39 = *reinterpret_cast<void**>(&r12d43);
                    if (reinterpret_cast<signed char>(rax49) < reinterpret_cast<signed char>(0)) {
                        if (!*rax48) {
                            r14_40 = reinterpret_cast<void**>(0xffffffffffffffff);
                            goto addr_290c_49;
                        } else {
                            if (*rax48 != 2) 
                                goto addr_297e_62;
                            rbp41 = r14_40;
                            goto addr_290c_49;
                        }
                    } else {
                        r14_40 = rax49;
                        goto addr_290c_49;
                    }
                }
                *r10_45 = reinterpret_cast<void*>(0);
                quote(rbx39, rsi30, rdx11);
                rax50 = fun_23d0();
                rdx11 = rax50;
                fun_2570();
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8);
                r8d29 = 0;
                *r10_45 = *reinterpret_cast<void**>(&r15d44);
                continue;
                addr_297e_62:
                *rbx39 = reinterpret_cast<void*>(0);
                quotearg_n_style_colon();
                rdx11 = reinterpret_cast<void**>("%s");
                fun_2570();
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
                *rbx39 = *reinterpret_cast<void**>(&r12d43);
                r8d29 = 0;
                continue;
                addr_27a9_42:
                rdi31 = rbp22;
                eax51 = fun_2480(rdi31, reinterpret_cast<int64_t>(rsp24) + 32, rdx11);
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8);
                if (!eax51) 
                    goto addr_29f7_38;
                rax52 = fun_2340(rdi31, rdi31);
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                r8b53 = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax52 != 2)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(r15_25 == 0)));
                if (!r8b53) {
                    if (reinterpret_cast<unsigned char>(r15_25) <= reinterpret_cast<unsigned char>(0xff)) 
                        goto addr_2a92_68;
                    zf54 = reinterpret_cast<int1_t>(*rbp22 == 47);
                    *rax52 = 0;
                    r12_55 = reinterpret_cast<void**>(".");
                    if (zf54) {
                        r12_55 = reinterpret_cast<void**>("/");
                    }
                    rdi31 = r12_55;
                    rax56 = fun_23a0(rdi31, 4, rdx11);
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
                    if (reinterpret_cast<signed char>(rax56) >= reinterpret_cast<signed char>(0)) 
                        goto addr_2a86_72;
                } else {
                    quotearg_n_style_colon();
                    rdx11 = reinterpret_cast<void**>("%s");
                    fun_2570();
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
                    r8d29 = 0;
                    continue;
                }
                if (!*rax52) {
                    addr_2a92_68:
                    rax57 = rbp22;
                } else {
                    rax58 = fun_23d0();
                    rdx11 = rax58;
                    fun_2570();
                    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8 - 8 + 8);
                    r8d29 = r8b53;
                    continue;
                }
                while (1) {
                    *reinterpret_cast<uint32_t*>(&rdx11) = reinterpret_cast<unsigned char>(*rax57);
                    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rdx11) == 47) {
                        addr_2aa0_77:
                        ++rax57;
                        continue;
                    } else {
                        if (!*reinterpret_cast<signed char*>(&rdx11)) 
                            goto addr_29f7_38;
                        edx59 = reinterpret_cast<unsigned char>(rax57[1]);
                        if (*reinterpret_cast<signed char*>(&edx59) == 47) 
                            goto addr_2aa0_77;
                        if (!*reinterpret_cast<signed char*>(&edx59)) 
                            goto addr_2aa0_77;
                    }
                    *reinterpret_cast<uint32_t*>(&rdx11) = 1;
                    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
                    do {
                        ++rdx11;
                        ecx60 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rax57) + reinterpret_cast<unsigned char>(rdx11));
                        rsi30 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax57) + reinterpret_cast<unsigned char>(rdx11));
                        if (*reinterpret_cast<signed char*>(&ecx60) == 47) 
                            break;
                    } while (*reinterpret_cast<signed char*>(&ecx60));
                    if (reinterpret_cast<unsigned char>(rdx11) > reinterpret_cast<unsigned char>(14)) 
                        break;
                    rax57 = rsi30;
                }
                *reinterpret_cast<int32_t*>(&r13_37) = 0;
                *reinterpret_cast<int32_t*>(&r13_37 + 4) = 0;
                goto addr_2898_39;
                addr_2a86_72:
                if (reinterpret_cast<unsigned char>(r15_25) >= reinterpret_cast<unsigned char>(rax56)) 
                    goto addr_2b91_43; else 
                    goto addr_2a92_68;
                addr_2a02_23:
                quotearg_style(4, rbp22, rdx11);
                rax61 = fun_23d0();
                rdx11 = rax61;
                fun_2570();
                rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8 - 8 + 8);
                r8d29 = 0;
                eax62 = optind;
                v17 = reinterpret_cast<unsigned char>(v17 & *reinterpret_cast<unsigned char*>(&r8d29));
                eax16 = eax62 + 1;
                optind = eax16;
            } while (eax16 < v20);
        }
        eax63 = static_cast<uint32_t>(v17) ^ 1;
        *reinterpret_cast<uint32_t*>(&rax64) = *reinterpret_cast<unsigned char*>(&eax63);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax64) + 4) = 0;
        rdx65 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
        if (!rdx65) {
            return rax64;
        }
    }
    addr_2cd0_7:
    usage();
    fun_2400();
}

int64_t __libc_start_main = 0;

void fun_2ce3() {
    __asm__("cli ");
    __libc_start_main(0x2640, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_2310(int64_t rdi);

int64_t fun_2d83() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2310(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2dc3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2560(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_24a0(void** rdi, void** rsi, int64_t rdx, void** rcx);

int32_t fun_24c0(int64_t rdi);

int32_t fun_2350(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_25c0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2dd3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    int32_t eax13;
    void** r13_14;
    void** rax15;
    void** rax16;
    int32_t eax17;
    void** rax18;
    void** rax19;
    void** rax20;
    int32_t eax21;
    void** rax22;
    void** r15_23;
    void** rax24;
    void** rax25;
    void** rax26;
    void** rdi27;
    void** r8_28;
    void** r9_29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_23d0();
            fun_2560(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_23d0();
            fun_24a0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_23d0();
            fun_24a0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_23d0();
            fun_24a0(rax12, r12_11, 5, rcx6);
            do {
                if (1) 
                    break;
                eax13 = fun_24c0("pathchk");
            } while (eax13);
            r13_14 = v4;
            if (!r13_14) {
                rax15 = fun_23d0();
                fun_2560(1, rax15, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax16 = fun_2550(5);
                if (!rax16 || (eax17 = fun_2350(rax16, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax17)) {
                    rax18 = fun_23d0();
                    r13_14 = reinterpret_cast<void**>("pathchk");
                    fun_2560(1, rax18, "https://www.gnu.org/software/coreutils/", "pathchk");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_14 = reinterpret_cast<void**>("pathchk");
                    goto addr_30e0_9;
                }
            } else {
                rax19 = fun_23d0();
                fun_2560(1, rax19, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax20 = fun_2550(5);
                if (!rax20 || (eax21 = fun_2350(rax20, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax21)) {
                    addr_2fe6_11:
                    rax22 = fun_23d0();
                    fun_2560(1, rax22, "https://www.gnu.org/software/coreutils/", "pathchk");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_14 == "pathchk")) {
                        r12_2 = reinterpret_cast<void**>(0x7961);
                    }
                } else {
                    addr_30e0_9:
                    r15_23 = stdout;
                    rax24 = fun_23d0();
                    fun_24a0(rax24, r15_23, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_2fe6_11;
                }
            }
            rax25 = fun_23d0();
            rcx6 = r12_2;
            fun_2560(1, rax25, r13_14, rcx6);
            addr_2e2e_14:
            fun_25a0();
        }
    } else {
        rax26 = fun_23d0();
        rdi27 = stderr;
        rcx6 = r12_2;
        fun_25c0(rdi27, 1, rax26, rcx6, r8_28, r9_29, v30, v31, v32, v33, v34, v35);
        goto addr_2e2e_14;
    }
}

int64_t file_name = 0;

void fun_3113(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3123(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2360(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_3133() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2340(rdi1), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_23d0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_31c3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2570();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2360(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_31c3_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2570();
    }
}

void** rpl_mbrtowc(void* rdi, void** rsi);

void fun_31e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdx) {
    }
    goto rpl_mbrtowc;
}

uint64_t fun_2420(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_3213(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2420(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2400();
    } else {
        return r12_7;
    }
}

void fun_25b0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s3 {
    signed char[1] pad1;
    void* f1;
    signed char[2] pad4;
    void* f4;
};

struct s3* fun_2440();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_32a3(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s3* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_25b0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2330("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2440();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2350(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void**>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(r12_5[1] == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(void** rdi, int64_t rsi);

void fun_4a43(void** rdi) {
    void** rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2340(rdi);
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = reinterpret_cast<void**>(0xb1e0);
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_4a83(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb1e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4aa3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb1e0);
    }
    *rdi = esi;
    return 0xb1e0;
}

int64_t fun_4ac3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb1e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s4 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_4b03(struct s4* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s4*>(0xb1e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s5 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s5* fun_4b23(struct s5* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s5*>(0xb1e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x260a;
    if (!rdx) 
        goto 0x260a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb1e0;
}

struct s6 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void* fun_4b63(void** rdi, void* rsi, void** rdx, int64_t rcx, struct s6* r8) {
    struct s6* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s6*>(0xb1e0);
    }
    rax7 = fun_2340(rdi);
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->pad40, v12, v10, 0x4b96);
    *rax7 = r15d8;
    return rax13;
}

struct s7 {
    uint32_t f0;
    uint32_t f4;
    signed char[32] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_4be3(void** rdi, int64_t rsi, void** rdx, struct s7* rcx) {
    struct s7* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void* rax14;
    void* rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s7*>(0xb1e0);
    }
    rax6 = fun_2340(rdi);
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void*>(&rbx5->pad40);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x4c11);
    rsi15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax14) + 1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x4c6c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_4cd3() {
    __asm__("cli ");
}

void** gb078 = reinterpret_cast<void**>(0xe0);

int64_t slotvec0 = 0x100;

void fun_4ce3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2320(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0xb0e0) {
        fun_2320(rdi7);
        gb078 = reinterpret_cast<void**>(0xb0e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb070) {
        fun_2320(r12_2);
        slotvec = reinterpret_cast<void**>(0xb070);
    }
    nslots = 1;
    return;
}

void fun_4d83() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4da3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4db3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4dd3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_4df3(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2610;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2400();
    } else {
        return rax6;
    }
}

void** fun_4e83(void** rdi, int32_t esi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2615;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2400();
    } else {
        return rax7;
    }
}

void** fun_4f13(int32_t edi, void** rsi) {
    void** rax3;
    struct s0* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x261a;
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2400();
    } else {
        return rax5;
    }
}

void** fun_4fa3(int32_t edi, void** rsi, int64_t rdx) {
    void** rax4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x261f;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2400();
    } else {
        return rax6;
    }
}

void** fun_5033(void** rdi, int64_t rsi, uint32_t edx) {
    struct s0* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x61a0]");
    __asm__("movdqa xmm1, [rip+0x61a8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6191]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2400();
    } else {
        return rax10;
    }
}

void** fun_50d3(void** rdi, uint32_t esi) {
    struct s0* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6100]");
    __asm__("movdqa xmm1, [rip+0x6108]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x60f1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2400();
    } else {
        return rax9;
    }
}

void** fun_5173(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6060]");
    __asm__("movdqa xmm1, [rip+0x6068]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6049]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2400();
    } else {
        return rax3;
    }
}

void** fun_5203(void** rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5fd0]");
    __asm__("movdqa xmm1, [rip+0x5fd8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x5fc6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2400();
    } else {
        return rax4;
    }
}

void** fun_5293(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2624;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2400();
    } else {
        return rax6;
    }
}

void** fun_5333(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5e9a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5e92]");
    __asm__("movdqa xmm2, [rip+0x5e9a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2629;
    if (!rdx) 
        goto 0x2629;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2400();
    } else {
        return rax7;
    }
}

void** fun_53d3(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, int64_t r8) {
    void** rcx6;
    struct s0* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5dfa]");
    __asm__("movdqa xmm1, [rip+0x5e02]");
    __asm__("movdqa xmm2, [rip+0x5e0a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x262e;
    if (!rdx) 
        goto 0x262e;
    rcx7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2400();
    } else {
        return rax9;
    }
}

void** fun_5483(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s0* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5d4a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5d42]");
    __asm__("movdqa xmm2, [rip+0x5d4a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2633;
    if (!rsi) 
        goto 0x2633;
    rcx5 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2400();
    } else {
        return rax6;
    }
}

void** fun_5523(int64_t rdi, int64_t rsi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s0* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5caa]");
    __asm__("movdqa xmm1, [rip+0x5cb2]");
    __asm__("movdqa xmm2, [rip+0x5cba]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2638;
    if (!rsi) 
        goto 0x2638;
    rcx6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2400();
    } else {
        return rax7;
    }
}

void fun_55c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_55d3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_55f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5613(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s8 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_24d0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_5633(void** rdi, void** rsi, void** rdx, void** rcx, struct s8* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_25c0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_25c0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_23d0();
    fun_25c0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_24d0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_23d0();
    fun_25c0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_24d0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_23d0();
        fun_25c0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7c08 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7c08;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_5aa3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s9 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_5ac3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s9* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s9* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2400();
    } else {
        return;
    }
}

void fun_5b63(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_5c06_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_5c10_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2400();
    } else {
        return;
    }
    addr_5c06_5:
    goto addr_5c10_7;
}

void fun_5c43() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_24d0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_23d0();
    fun_2560(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_23d0();
    fun_2560(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_23d0();
    goto fun_2560;
}

int64_t fun_2380();

void xalloc_die();

void fun_5ce3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2380();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2500(void** rdi);

void fun_5d23(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2500(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5d43(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2500(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5d63(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2500(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2540();

void fun_5d83(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2540();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5db3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5de3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2380();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_5e23() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2380();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5e63(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2380();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5e93(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2380();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5ee3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2380();
            if (rax5) 
                break;
            addr_5f2d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_5f2d_5;
        rax8 = fun_2380();
        if (rax8) 
            goto addr_5f16_9;
        if (rbx4) 
            goto addr_5f2d_5;
        addr_5f16_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_5f73(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2380();
            if (rax8) 
                break;
            addr_5fba_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_5fba_5;
        rax11 = fun_2380();
        if (rax11) 
            goto addr_5fa2_9;
        if (!rbx6) 
            goto addr_5fa2_9;
        if (r12_4) 
            goto addr_5fba_5;
        addr_5fa2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6003(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_60ad_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_60c0_10:
                *r12_8 = 0;
            }
            addr_6060_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6086_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_60d4_14;
            if (rcx10 <= rsi9) 
                goto addr_607d_16;
            if (rsi9 >= 0) 
                goto addr_60d4_14;
            addr_607d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_60d4_14;
            addr_6086_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2540();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_60d4_14;
            if (!rbp13) 
                break;
            addr_60d4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_60ad_9;
        } else {
            if (!r13_6) 
                goto addr_60c0_10;
            goto addr_6060_11;
        }
    }
}

int64_t fun_24b0();

void fun_6103() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6133() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6163() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6183() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_24b0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_24e0(signed char* rdi, void** rsi, void** rdx);

void fun_61a3(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2500(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_24e0;
    }
}

void fun_61e3(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2500(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_24e0;
    }
}

struct s10 {
    signed char[1] pad1;
    void* f1;
};

void fun_6223(int64_t rdi, struct s10* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2500(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_24e0;
    }
}

void fun_6263(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_23f0(rdi);
    rax3 = fun_2500(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_24e0;
    }
}

void fun_62a3() {
    void** rdi1;

    __asm__("cli ");
    fun_23d0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2570();
    fun_2330(rdi1);
}

int64_t fun_2370();

int64_t rpl_fclose(void** rdi);

int64_t fun_62e3(void** rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_2370();
    ebx3 = reinterpret_cast<unsigned char>(*rdi) & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_633e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2340(rdi);
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_633e_3;
            rax6 = fun_2340(rdi);
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_24f0(void** rdi);

int32_t fun_2530(void** rdi);

int64_t fun_2450(int64_t rdi, ...);

int32_t rpl_fflush(void** rdi);

int64_t fun_23b0(void** rdi);

int64_t fun_6353(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_24f0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2530(rdi);
        if (!(eax3 && (eax4 = fun_24f0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2450(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2340(rdi);
            r12d9 = *rax8;
            rax10 = fun_23b0(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_23b0;
}

void rpl_fseeko(void** rdi);

void fun_63e3(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2530(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*rdi) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6433(void** rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<int64_t*>(rdi + 40) != *reinterpret_cast<int64_t*>(rdi + 32) || *reinterpret_cast<int64_t*>(rdi + 72)))) {
        eax4 = fun_24f0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2450(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *rdi = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(*rdi) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

int32_t setlocale_null_r();

int64_t fun_64b3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2400();
    } else {
        return rax3;
    }
}

signed char* fun_2520(int64_t rdi);

signed char* fun_6533() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2520(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

int64_t fun_6573(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2550(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_23f0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_24e0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_24e0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6623() {
    __asm__("cli ");
    goto fun_2550;
}

void fun_6633() {
    __asm__("cli ");
}

void fun_6647() {
    __asm__("cli ");
    return;
}

uint32_t fun_2490(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_25e0(int64_t rdi, void** rsi);

uint32_t fun_25d0(void** rdi, void** rsi);

void** fun_25f0(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_34d5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_23d0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_23d0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*rdx10), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_23f0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*rbx41);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_37d3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_37d3_22; else 
                            goto addr_3bcd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_3c8d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_3fe0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*rbx41);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_37d0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_37d0_30; else 
                                goto addr_3ff9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_23f0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_3fe0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2490(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_3fe0_28; else 
                            goto addr_367c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4140_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_3fc0_40:
                        if (r11_27 == 1) {
                            addr_3b4d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4108_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3787_44;
                            }
                        } else {
                            goto addr_3fd0_46;
                        }
                    } else {
                        addr_414f_47:
                        rax24 = v46;
                        if (!rax24[1]) {
                            goto addr_3b4d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_37d3_22:
                                if (v47 != 1) {
                                    addr_3d29_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_23f0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_3d74_54;
                                    }
                                } else {
                                    goto addr_37e0_56;
                                }
                            } else {
                                addr_3785_57:
                                ebp36 = 0;
                                goto addr_3787_44;
                            }
                        } else {
                            addr_3fb4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_414f_47; else 
                                goto addr_3fbe_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_3b4d_41;
                        if (v47 == 1) 
                            goto addr_37e0_56; else 
                            goto addr_3d29_52;
                    }
                }
                addr_3841_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_36d8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_36fd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_3a00_65;
                    } else {
                        addr_3869_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_40b8_67;
                    }
                } else {
                    goto addr_3860_69;
                }
                addr_3711_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_375c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_40b8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_375c_81;
                }
                addr_3860_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_36fd_64; else 
                    goto addr_3869_66;
                addr_3787_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_383f_91;
                if (v22) 
                    goto addr_379f_93;
                addr_383f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3841_62;
                addr_3d74_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_44fb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_456b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*rdx10) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_436f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_25e0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_25d0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_3e6e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_382c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_3e78_112;
                    }
                } else {
                    addr_3e78_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_3f49_114;
                }
                addr_3838_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_383f_91;
                while (1) {
                    addr_3f49_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4457_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_3eb6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4465_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_3f37_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_3f37_128;
                        }
                    }
                    addr_3ee5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_3f37_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_3eb6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3ee5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_375c_81;
                addr_4465_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_40b8_67;
                addr_44fb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_3e6e_109;
                addr_456b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_3e6e_109;
                addr_37e0_56:
                rax93 = fun_25f0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_382c_110;
                addr_3fbe_59:
                goto addr_3fc0_40;
                addr_3c8d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_37d3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3838_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3785_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_37d3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_3cd2_160;
                if (!v22) 
                    goto addr_40a7_162; else 
                    goto addr_42b3_163;
                addr_3cd2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_40a7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_40b8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_3b7b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_39e3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3711_70; else 
                    goto addr_39f7_169;
                addr_3b7b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_36d8_63;
                goto addr_3860_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_3fb4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_40ef_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_37d0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_36c8_178; else 
                        goto addr_4072_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3fb4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_37d3_22;
                }
                addr_40ef_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_37d0_30:
                    r8d42 = 0;
                    goto addr_37d3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3841_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4108_42;
                    }
                }
                addr_36c8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_36d8_63;
                addr_4072_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_3fd0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3841_62;
                } else {
                    addr_4082_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_37d3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4832_188;
                if (v28) 
                    goto addr_40a7_162;
                addr_4832_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_39e3_168;
                addr_367c_37:
                if (v22) 
                    goto addr_4673_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*rbx41);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3693_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4140_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_41cb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_37d3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_36c8_178; else 
                        goto addr_41a7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_3fb4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_37d3_22;
                }
                addr_41cb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_37d3_22;
                }
                addr_41a7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3fd0_46;
                goto addr_4082_186;
                addr_3693_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_37d3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_37d3_22; else 
                    goto addr_36a4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_477e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4604_210;
            if (1) 
                goto addr_4602_212;
            if (!v29) 
                goto addr_423e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_23e0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_4771_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_3a00_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_37bb_219; else 
            goto addr_3a1a_220;
        addr_379f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_37b3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_3a1a_220; else 
            goto addr_37bb_219;
        addr_436f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_3a1a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_23e0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_438d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_23e0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_4800_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4266_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_4457_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_37b3_221;
        addr_42b3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_37b3_221;
        addr_39f7_169:
        goto addr_3a00_65;
        addr_477e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_3a1a_220;
        goto addr_438d_222;
        addr_4604_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*v26), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_465e_236;
        fun_2400();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4800_225;
        addr_4602_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4604_210;
        addr_423e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4604_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_4266_226;
        }
        addr_4771_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_3bcd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x76cc + rax113 * 4) + 0x76cc;
    addr_3ff9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x77cc + rax114 * 4) + 0x77cc;
    addr_4673_190:
    addr_37bb_219:
    goto 0x34a0;
    addr_36a4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x75cc + rax115 * 4) + 0x75cc;
    addr_465e_236:
    goto v116;
}

void fun_36c0() {
}

void fun_3878() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3572;
}

void fun_38d1() {
    goto 0x3572;
}

void fun_39be() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3841;
    }
    if (v2) 
        goto 0x42b3;
    if (!r10_3) 
        goto addr_441e_5;
    if (!v4) 
        goto addr_42ee_7;
    addr_441e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_42ee_7:
    goto 0x36f4;
}

void fun_39dc() {
}

void fun_3a87() {
    signed char v1;

    if (v1) {
        goto 0x3a0f;
    } else {
        goto 0x374a;
    }
}

void fun_3aa1() {
    signed char v1;

    if (!v1) 
        goto 0x3a9a; else 
        goto "???";
}

void fun_3ac8() {
    goto 0x39e3;
}

void fun_3b48() {
}

void fun_3b60() {
}

void fun_3b8f() {
    goto 0x39e3;
}

void fun_3be1() {
    goto 0x3b70;
}

void fun_3c10() {
    goto 0x3b70;
}

void fun_3c43() {
    goto 0x3b70;
}

void fun_4010() {
    goto 0x36c8;
}

void fun_430e() {
    signed char v1;

    if (v1) 
        goto 0x42b3;
    goto 0x36f4;
}

void fun_43b5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x36f4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x36d8;
        goto 0x36f4;
    }
}

void fun_47d2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x3a40;
    } else {
        goto 0x3572;
    }
}

void fun_5708() {
    fun_23d0();
}

void fun_38fe() {
    goto 0x3572;
}

void fun_3ad4() {
    goto 0x3a8c;
}

void fun_3b9b() {
    goto 0x36c8;
}

void fun_3bed() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x3b70;
    goto 0x379f;
}

void fun_3c1f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x3b7b;
        goto 0x35a0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x3a1a;
        goto 0x37bb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x43b8;
    if (r10_8 > r15_9) 
        goto addr_3b05_9;
    addr_3b0a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x43c3;
    goto 0x36f4;
    addr_3b05_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_3b0a_10;
}

void fun_3c52() {
    goto 0x3787;
}

void fun_4020() {
    goto 0x3787;
}

void fun_47bf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x38dc;
    } else {
        goto 0x3a40;
    }
}

void fun_57c0() {
}

void fun_3c5c() {
    goto 0x3bf7;
}

void fun_402a() {
    goto 0x3b4d;
}

void fun_5820() {
    fun_23d0();
    goto fun_25c0;
}

void fun_392d() {
    goto 0x3572;
}

void fun_3c68() {
    goto 0x3bf7;
}

void fun_4037() {
    goto 0x3b9e;
}

void fun_5860() {
    fun_23d0();
    goto fun_25c0;
}

void fun_395a() {
    goto 0x3572;
}

void fun_3c74() {
    goto 0x3b70;
}

void fun_58a0() {
    fun_23d0();
    goto fun_25c0;
}

void fun_397c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4310;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3841;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3841;
    }
    if (v11) 
        goto 0x4673;
    if (r10_12 > r15_13) 
        goto addr_46c3_8;
    addr_46c8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4401;
    addr_46c3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_46c8_9;
}

struct s11 {
    signed char[24] pad24;
    int64_t f18;
};

struct s12 {
    signed char[16] pad16;
    void** f10;
};

struct s13 {
    signed char[8] pad8;
    void** f8;
};

void fun_58f0() {
    int64_t r15_1;
    struct s11* rbx2;
    void** r14_3;
    struct s12* rbx4;
    void** r13_5;
    struct s13* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_23d0();
    fun_25c0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x5912, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_5948() {
    fun_23d0();
    goto 0x5919;
}

struct s14 {
    signed char[32] pad32;
    int64_t f20;
};

struct s15 {
    signed char[24] pad24;
    int64_t f18;
};

struct s16 {
    signed char[16] pad16;
    void** f10;
};

struct s17 {
    signed char[8] pad8;
    void** f8;
};

struct s18 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_5980() {
    int64_t rcx1;
    struct s14* rbx2;
    int64_t r15_3;
    struct s15* rbx4;
    void** r14_5;
    struct s16* rbx6;
    void** r13_7;
    struct s17* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s18* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_23d0();
    fun_25c0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x59b4, __return_address(), rcx1);
    goto v15;
}

void fun_59f8() {
    fun_23d0();
    goto 0x59bb;
}
