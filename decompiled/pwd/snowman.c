
struct s0 {
    struct s0* f0;
    signed char f1;
    signed char f2;
    signed char[5] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    struct s0* f18;
    signed char[8166] pad8191;
    signed char f1fff;
};

struct s0* xnmalloc(int64_t rdi, struct s0* rsi);

void fun_25c0(struct s0* rdi, struct s0* rsi, struct s0* rdx);

void fun_23c0(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, ...);

void file_name_prepend(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8) {
    signed char* rdx6;
    struct s0* rax7;
    signed char* r14_8;
    struct s0* rdx9;
    struct s0* rax10;
    struct s0* rbp11;
    void* rdi12;
    struct s0* rsi13;
    struct s0* rdx14;
    struct s0* v15;
    struct s0* rdi16;
    struct s0* rax17;

    rdx6 = &rdx->f1;
    rax7 = rdi->f10;
    r14_8 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0)));
    if (reinterpret_cast<uint64_t>(rdx6) > reinterpret_cast<uint64_t>(r14_8)) {
        rdx9 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdx6) + reinterpret_cast<unsigned char>(rdi->f8));
        rax10 = xnmalloc(2, rdx9);
        rbp11 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx9) + reinterpret_cast<unsigned char>(rdx9));
        rdi12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbp11) + reinterpret_cast<uint64_t>(r14_8) - reinterpret_cast<unsigned char>(rdi->f8));
        rsi13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0)) + reinterpret_cast<uint64_t>(r14_8));
        rdx14 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi->f8) - reinterpret_cast<uint64_t>(r14_8));
        v15 = *reinterpret_cast<struct s0**>(&rdi->f0);
        rdi16 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi12) + reinterpret_cast<unsigned char>(rax10));
        rdi->f10 = rdi16;
        fun_25c0(rdi16, rsi13, rdx14);
        fun_23c0(v15, rsi13, rdx14, rcx);
        *reinterpret_cast<struct s0**>(&rdi->f0) = rax10;
        rax7 = rdi->f10;
        rdi->f8 = rbp11;
    }
    rax17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rdx)));
    rdi->f10 = rax17;
    *reinterpret_cast<struct s0**>(&rax17->f0) = reinterpret_cast<struct s0*>(47);
}

struct s0* nth_parent(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8) {
    struct s0* rax6;
    struct s0* rdx7;
    struct s0* rcx8;

    rax6 = xnmalloc(3, rdi);
    if (!rdi) {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax6) + 0xffffffffffffffff) = 0;
        return rax6;
    } else {
        rdx7 = rax6;
        rcx8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rdi) * 2 + reinterpret_cast<unsigned char>(rax6));
        do {
            rdx7->f2 = 47;
            rdx7 = reinterpret_cast<struct s0*>(&rdx7->pad8);
            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rdx7) + 0xfffffffffffffffd) = 0x2e2e;
        } while (rdx7 != rcx8);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx8) + 0xffffffffffffffff) = 0;
        return rax6;
    }
}

int64_t fun_2490();

int64_t fun_23d0(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2490();
    if (r8d > 10) {
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x75a0 + rax11 * 4) + 0x75a0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* g28;

uint32_t* fun_23e0();

struct s0* slotvec = reinterpret_cast<struct s0*>(0x70);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_2510();

struct s2 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

struct s0* xcharalloc(struct s0* rdi, ...);

void fun_24c0();

struct s0* quotearg_n_options(struct s0* rdi, struct s0* rsi, uint32_t* rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    uint32_t* rax8;
    struct s0* r15_9;
    uint32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x4aaf;
    rax8 = fun_23e0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
        fun_23d0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6421]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x4b3b;
            fun_2510();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb0e0) {
                fun_23c0(r14_19, rsi24, rsi, rdx, r14_19, rsi24, rsi, rdx);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x4bca);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_24c0();
        } else {
            return r14_19;
        }
    }
}

struct s0* fun_23b0(struct s0* rdi);

struct s3 {
    signed char[1] pad1;
    struct s0* f1;
    unsigned char f2;
    unsigned char f3;
};

struct s3* fun_2700();

uint32_t fun_25b0(struct s0* rdi, struct s0* rsi, ...);

struct s0* logical_getcwd(int64_t rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx) {
    struct s0* rdi5;
    struct s0* rax6;
    struct s0* v7;
    struct s0* rax8;
    struct s0* rsp9;
    struct s0* r12_10;
    struct s3* rax11;
    uint32_t edx12;
    uint32_t edx13;
    uint32_t eax14;
    uint32_t eax15;
    int64_t v16;
    int64_t v17;
    struct s0* v18;
    struct s0* v19;
    void* rax20;
    struct s0* rax21;
    int64_t v22;
    struct s0* rdx23;
    struct s0* rcx24;
    int64_t v25;

    rdi5 = reinterpret_cast<struct s0*>("PWD");
    rax6 = g28;
    v7 = rax6;
    rax8 = fun_23b0("PWD");
    rsp9 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x138 - 8 + 8);
    if (!rax8) 
        goto addr_2ed0_2;
    r12_10 = rax8;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax8->f0) == 47)) 
        goto addr_2ed0_2;
    rdi5 = rax8;
    while (rax11 = fun_2700(), rsp9 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp9) - 8 + 8), !!rax11) {
        edx12 = rax11->f2;
        if (!*reinterpret_cast<signed char*>(&edx12)) 
            goto addr_2ed0_2;
        if (*reinterpret_cast<signed char*>(&edx12) == 47) 
            goto addr_2ed0_2;
        if (*reinterpret_cast<signed char*>(&edx12) == 46) {
            edx13 = rax11->f3;
            if (!*reinterpret_cast<signed char*>(&edx13)) 
                goto addr_2ed0_2;
            if (*reinterpret_cast<signed char*>(&edx13) == 47) 
                goto addr_2ec8_11;
        }
        rdi5 = reinterpret_cast<struct s0*>(&rax11->f1);
    }
    rdi5 = r12_10;
    eax14 = fun_25b0(rdi5, rsp9);
    rsp9 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp9) - 8 + 8);
    if (eax14 || ((rdi5 = reinterpret_cast<struct s0*>("."), eax15 = fun_25b0(".", reinterpret_cast<unsigned char>(rsp9) + 0x90), !!eax15) || (v16 != v17 || v18 != v19))) {
        addr_2ed0_2:
        *reinterpret_cast<int32_t*>(&r12_10) = 0;
        *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
    }
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (!rax20) {
        return r12_10;
    }
    fun_24c0();
    rax21 = xnmalloc(3, rdi5);
    if (rdi5) 
        goto addr_2f56_19;
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax21) + 0xffffffffffffffff) = 0;
    goto v22;
    addr_2f56_19:
    rdx23 = rax21;
    rcx24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi5) + reinterpret_cast<unsigned char>(rdi5) * 2 + reinterpret_cast<unsigned char>(rax21));
    do {
        rdx23->f2 = 47;
        rdx23 = reinterpret_cast<struct s0*>(&rdx23->pad8);
        *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rdx23) + 0xfffffffffffffffd) = 0x2e2e;
    } while (rdx23 != rcx24);
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx24) + 0xffffffffffffffff) = 0;
    goto v25;
    addr_2ec8_11:
    goto addr_2ed0_2;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x7543);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0x753c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x7547);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0x7538);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23a3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_23b3() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2040;

void fun_23c3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_23d3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_23e3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23f3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2403() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_2413() {
    __asm__("cli ");
    goto __fpending;
}

int64_t puts = 0x20a0;

void fun_2423() {
    __asm__("cli ");
    goto puts;
}

int64_t reallocarray = 0x20b0;

void fun_2433() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20c0;

void fun_2443() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2453() {
    __asm__("cli ");
    goto fclose;
}

int64_t opendir = 0x20e0;

void fun_2463() {
    __asm__("cli ");
    goto opendir;
}

int64_t bindtextdomain = 0x20f0;

void fun_2473() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2100;

void fun_2483() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2110;

void fun_2493() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2120;

void fun_24a3() {
    __asm__("cli ");
    goto strlen;
}

int64_t chdir = 0x2130;

void fun_24b3() {
    __asm__("cli ");
    goto chdir;
}

int64_t __stack_chk_fail = 0x2140;

void fun_24c3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2150;

void fun_24d3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2160;

void fun_24e3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strrchr = 0x2170;

void fun_24f3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2180;

void fun_2503() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2190;

void fun_2513() {
    __asm__("cli ");
    goto memset;
}

int64_t getcwd = 0x21a0;

void fun_2523() {
    __asm__("cli ");
    goto getcwd;
}

int64_t closedir = 0x21b0;

void fun_2533() {
    __asm__("cli ");
    goto closedir;
}

int64_t lstat = 0x21c0;

void fun_2543() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x21d0;

void fun_2553() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_2563() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2573() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2200;

void fun_2583() {
    __asm__("cli ");
    goto strcmp;
}

int64_t dirfd = 0x2210;

void fun_2593() {
    __asm__("cli ");
    goto dirfd;
}

int64_t fputc_unlocked = 0x2220;

void fun_25a3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x2230;

void fun_25b3() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2240;

void fun_25c3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2250;

void fun_25d3() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x2260;

void fun_25e3() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x2270;

void fun_25f3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2280;

void fun_2603() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2290;

void fun_2613() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22a0;

void fun_2623() {
    __asm__("cli ");
    goto __freading;
}

int64_t fchdir = 0x22b0;

void fun_2633() {
    __asm__("cli ");
    goto fchdir;
}

int64_t realloc = 0x22c0;

void fun_2643() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22d0;

void fun_2653() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22e0;

void fun_2663() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22f0;

void fun_2673() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2300;

void fun_2683() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x2310;

void fun_2693() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2320;

void fun_26a3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2330;

void fun_26b3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2340;

void fun_26c3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2350;

void fun_26d3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2360;

void fun_26e3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2370;

void fun_26f3() {
    __asm__("cli ");
    goto fstat;
}

int64_t strstr = 0x2380;

void fun_2703() {
    __asm__("cli ");
    goto strstr;
}

int64_t __ctype_b_loc = 0x2390;

void fun_2713() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s0* fun_2650(int64_t rdi, ...);

void fun_2470(int64_t rdi, int64_t rsi);

void fun_2440(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_24d0(int64_t rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx);

void usage();

int64_t stdout = 0;

uint32_t* Version = reinterpret_cast<uint32_t*>(0x74d1);

void version_etc(int64_t rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8);

int32_t fun_26a0();

int32_t optind = 0;

struct s0* fun_2480();

void fun_2670();

struct s0* xgetcwd(int64_t rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx);

void fun_2420(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, ...);

int32_t fun_24b0(int64_t rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8);

uint32_t* quote(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8);

int32_t fun_2530(uint32_t* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8);

struct s0* xmalloc(int64_t rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8);

struct s5 {
    uint32_t* f0;
    int64_t f8;
};

struct s5* get_root_dev_ino(void* rdi, struct s0* rsi);

uint32_t* fun_2460(int64_t rdi, struct s0* rsi, struct s0* rdx);

int32_t fun_2590(uint32_t* rdi, struct s0* rsi, struct s0* rdx);

int32_t fun_2630(int64_t rdi, struct s0* rsi, struct s0* rdx);

uint32_t fun_26f0(int64_t rdi, struct s0* rsi, struct s0* rdx);

struct s6 {
    int64_t f0;
    signed char[11] pad19;
    struct s0* f13;
    signed char f14;
};

struct s6* fun_25e0(uint32_t* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8);

int32_t fun_2540(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8);

struct s0* fun_24a0(struct s0* rdi, ...);

uint32_t* quotearg_style(int64_t rdi, int64_t rsi, struct s0* rdx, uint32_t* rcx, int64_t r8);

int64_t fun_2763(int32_t edi, struct s0* rsi) {
    uint32_t* r15_3;
    int32_t r12d4;
    struct s0* rbx5;
    struct s0* rax6;
    struct s0* v7;
    struct s0* rax8;
    struct s0* rdi9;
    int32_t r13d10;
    struct s0* r14_11;
    void* rsp12;
    int64_t r8_13;
    uint32_t* rcx14;
    struct s0* rdx15;
    struct s0* rsi16;
    int64_t rdi17;
    int32_t eax18;
    int64_t rdi19;
    int1_t less20;
    struct s0* rax21;
    struct s0* rax22;
    struct s0* rax23;
    void* rsp24;
    void* rsp25;
    int32_t eax26;
    struct s0** rsp27;
    void* rsp28;
    struct s0* rsi29;
    struct s0* v30;
    uint32_t eax31;
    void* rsp32;
    struct s0* rax33;
    uint32_t* rax34;
    struct s0* rax35;
    uint32_t* rax36;
    struct s0** rsp37;
    uint32_t* r12_38;
    void* rsp39;
    struct s0* rax40;
    uint32_t* rax41;
    struct s0* rax42;
    struct s0* rdi43;
    struct s0* v44;
    struct s0* rdi45;
    void* rax46;
    struct s0* rax47;
    struct s0* rax48;
    void* rsp49;
    struct s5* rax50;
    void* rsp51;
    struct s5* v52;
    uint32_t eax53;
    struct s0* v54;
    uint32_t* v55;
    int64_t v56;
    struct s0* v57;
    uint32_t* rax58;
    void* rsp59;
    int32_t eax60;
    int64_t rdi61;
    int32_t eax62;
    int64_t rdi63;
    uint32_t eax64;
    uint32_t* rax65;
    struct s0* v66;
    struct s0* v67;
    struct s0* v68;
    unsigned char v69;
    struct s6* rax70;
    struct s0* r13_71;
    void* rdx72;
    int32_t eax73;
    int64_t v74;
    int64_t v75;
    int64_t v76;
    int64_t v77;
    int64_t v78;
    struct s0* rax79;
    int32_t eax80;
    int32_t ecx81;
    uint32_t* rax82;
    struct s0* rax83;
    uint32_t* rax84;
    uint32_t* rax85;
    struct s0* rax86;
    struct s0* rax87;
    uint32_t* rax88;
    struct s0* rax89;
    int32_t eax90;
    struct s0* rax91;
    uint32_t* rax92;
    struct s0* rax93;
    struct s0* rax94;
    uint32_t* rax95;
    struct s0* rax96;

    __asm__("cli ");
    r15_3 = reinterpret_cast<uint32_t*>("xq");
    r12d4 = edi;
    rbx5 = rsi;
    rax6 = g28;
    v7 = rax6;
    rax8 = fun_23b0("POSIXLY_CORRECT");
    rdi9 = *reinterpret_cast<struct s0**>(&rbx5->f0);
    *reinterpret_cast<unsigned char*>(&r13d10) = reinterpret_cast<uint1_t>(!!rax8);
    set_program_name(rdi9);
    fun_2650(6, 6);
    fun_2470("coreutils", "/usr/local/share/locale");
    r14_11 = reinterpret_cast<struct s0*>("LP");
    fun_2440("coreutils", "/usr/local/share/locale");
    atexit(0x3420, "/usr/local/share/locale");
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x218 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    while (*reinterpret_cast<int32_t*>(&r8_13) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_13) + 4) = 0, rcx14 = reinterpret_cast<uint32_t*>("xq"), rdx15 = reinterpret_cast<struct s0*>("LP"), rsi16 = rbx5, *reinterpret_cast<int32_t*>(&rdi17) = r12d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0, eax18 = fun_24d0(rdi17, rsi16, "LP", "xq"), rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8), eax18 != -1) {
        if (eax18 == 76) {
            addr_2870_4:
            r13d10 = 1;
            continue;
        } else {
            if (eax18 > 76) {
                addr_2850_6:
                if (eax18 != 80) 
                    goto addr_2904_7;
            } else {
                if (eax18 != 0xffffff7d) {
                    if (eax18 != 0xffffff7e) 
                        goto addr_2904_7;
                    usage();
                    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                    goto addr_2870_4;
                } else {
                    rdi19 = stdout;
                    rcx14 = Version;
                    r8_13 = reinterpret_cast<int64_t>("Jim Meyering");
                    rdx15 = reinterpret_cast<struct s0*>("GNU coreutils");
                    rsi16 = reinterpret_cast<struct s0*>("pwd");
                    version_etc(rdi19, "pwd", "GNU coreutils", rcx14, "Jim Meyering");
                    eax18 = fun_26a0();
                    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
                    goto addr_2850_6;
                }
            }
        }
        r13d10 = 0;
    }
    less20 = optind < r12d4;
    if (less20) {
        rax21 = fun_2480();
        *reinterpret_cast<uint32_t*>(&rsi16) = 0;
        *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi17) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
        rdx15 = rax21;
        fun_2670();
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8);
    }
    if (!*reinterpret_cast<unsigned char*>(&r13d10) || (rax22 = logical_getcwd(rdi17, rsi16, rdx15, "xq"), rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8), rax22 == 0)) {
        rax23 = xgetcwd(rdi17, rsi16, rdx15, "xq");
        rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        if (!rax23) 
            goto addr_2913_17;
        fun_2420(rax23, rsi16, rdx15, "xq");
        fun_23c0(rax23, rsi16, rdx15, "xq");
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8);
        goto addr_28a6_19;
    } else {
        fun_2420(rax22, rsi16, rdx15, "xq");
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        goto addr_28a6_19;
    }
    addr_2904_7:
    usage();
    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    goto addr_290e_21;
    addr_28b9_22:
    return 0;
    while (1) {
        addr_2b62_23:
        eax26 = fun_24b0("..", rsi16, rdx15, rcx14, r8_13);
        rsp27 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
        if (eax26 >= 0) {
            rsi29 = v30;
            eax31 = fun_25b0(".", rsi29, ".", rsi29);
            rsp32 = reinterpret_cast<void*>(rsp27 - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rbx5) = eax31 >> 31;
            goto addr_2a17_25;
        }
        addr_2b8c_26:
        rax33 = nth_parent(r14_11, rsi16, rdx15, rcx14, r8_13);
        rax34 = quote(rax33, rsi16, rdx15, rcx14, r8_13);
        rax35 = fun_2480();
        rax36 = fun_23e0();
        rcx14 = rax34;
        rdx15 = rax35;
        *reinterpret_cast<uint32_t*>(&rsi29) = *rax36;
        *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
        fun_2670();
        rsp37 = rsp27 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        goto addr_2bce_27;
        addr_2c08_28:
        fun_2530(r12_38, rsi29, rdx15, rcx14, r8_13);
        rsp37 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp39) - 8 + 8);
        *r15_3 = *reinterpret_cast<uint32_t*>(&rbx5);
        goto addr_2b27_29;
        while (1) {
            addr_2ce0_30:
            rax40 = nth_parent(r14_11, rsi29, rdx15, rcx14, r8_13);
            rax41 = quote(rax40, rsi29, rdx15, rcx14, r8_13);
            rax42 = fun_2480();
            *reinterpret_cast<uint32_t*>(&rsi16) = *r15_3;
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            rcx14 = rax41;
            rdx15 = rax42;
            fun_2670();
            rsp37 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp39) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            while (1) {
                rdi43 = v44->f10;
                if (!*reinterpret_cast<struct s0**>(&rdi43->f0)) {
                    *reinterpret_cast<uint32_t*>(&rdx15) = 0;
                    *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
                    rsi16 = reinterpret_cast<struct s0*>(0x7025);
                    file_name_prepend(v44, 0x7025, 0, rcx14, r8_13);
                    rsp37 = rsp37 - 8 + 8;
                    rdi43 = v44->f10;
                }
                fun_2420(rdi43, rsi16, rdx15, rcx14, rdi43, rsi16, rdx15, rcx14);
                rdi45 = *reinterpret_cast<struct s0**>(&v44->f0);
                fun_23c0(rdi45, rsi16, rdx15, rcx14, rdi45, rsi16, rdx15, rcx14);
                fun_23c0(v44, rsi16, rdx15, rcx14, v44, rsi16, rdx15, rcx14);
                rsp25 = reinterpret_cast<void*>(rsp37 - 8 + 8 - 8 + 8 - 8 + 8);
                addr_28a6_19:
                rax46 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
                if (!rax46) 
                    goto addr_28b9_22;
                addr_290e_21:
                fun_24c0();
                rsp24 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                addr_2913_17:
                rax47 = xmalloc(24, rsi16, rdx15, rcx14, r8_13);
                rax47->f8 = reinterpret_cast<struct s0*>(0x2000);
                rax48 = xmalloc(0x2000, rsi16, rdx15, rcx14, r8_13);
                rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp24) - 8 + 8 - 8 + 8);
                rdx15 = reinterpret_cast<struct s0*>(&rax48->f1fff);
                *reinterpret_cast<struct s0**>(&rax47->f0) = rax48;
                rax47->f10 = rdx15;
                rax48->f1fff = 0;
                rax50 = get_root_dev_ino(reinterpret_cast<int64_t>(rsp49) + 64, rsi16);
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
                v52 = rax50;
                if (!rax50) 
                    break;
                *reinterpret_cast<int32_t*>(&r14_11) = 1;
                *reinterpret_cast<int32_t*>(&r14_11 + 4) = 0;
                rsi16 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp51) + 80);
                eax53 = fun_25b0(".", rsi16);
                rsp37 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                if (reinterpret_cast<int32_t>(eax53) < reinterpret_cast<int32_t>(0)) 
                    goto addr_2c5a_35;
                v44 = rax47;
                v30 = reinterpret_cast<struct s0*>(rsp37 + 0xe0);
                v54 = reinterpret_cast<struct s0*>(rsp37 + 0x170);
                while (1) {
                    rcx14 = v52->f0;
                    if (v55 != rcx14) 
                        goto addr_29c3_38;
                    if (v56 == v52->f8) 
                        break;
                    addr_29c3_38:
                    v57 = reinterpret_cast<struct s0*>(&r14_11->f1);
                    rax58 = fun_2460("..", rsi16, rdx15);
                    rsp59 = reinterpret_cast<void*>(rsp37 - 8 + 8);
                    r12_38 = rax58;
                    if (!rax58) 
                        goto addr_2c18_40;
                    eax60 = fun_2590(rax58, rsi16, rdx15);
                    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
                    if (eax60 < 0) 
                        goto addr_2b62_23;
                    *reinterpret_cast<int32_t*>(&rdi61) = eax60;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi61) + 4) = 0;
                    eax62 = fun_2630(rdi61, rsi16, rdx15);
                    rsp27 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
                    if (eax62 < 0) 
                        goto addr_2b8c_26;
                    rsi29 = v30;
                    *reinterpret_cast<int32_t*>(&rdi63) = eax60;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi63) + 4) = 0;
                    eax64 = fun_26f0(rdi63, rsi29, rdx15);
                    rsp32 = reinterpret_cast<void*>(rsp27 - 8 + 8);
                    *reinterpret_cast<uint32_t*>(&rbx5) = eax64 >> 31;
                    addr_2a17_25:
                    rax65 = fun_23e0();
                    rsp39 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                    r15_3 = rax65;
                    if (*reinterpret_cast<signed char*>(&rbx5)) 
                        goto addr_2ce0_30;
                    rbx5 = v66;
                    v67 = v68;
                    v69 = reinterpret_cast<uint1_t>(v68 != rbx5);
                    while (1) {
                        *r15_3 = 0;
                        do {
                            rax70 = fun_25e0(r12_38, rsi29, rdx15, rcx14, r8_13);
                            rsp39 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp39) - 8 + 8);
                            if (!rax70) 
                                goto addr_2b0c_47;
                            r13_71 = reinterpret_cast<struct s0*>(&rax70->f13);
                            if (*reinterpret_cast<struct s0**>(&rax70->f13) != 46) 
                                break;
                            *reinterpret_cast<int32_t*>(&rdx72) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx72) + 4) = 0;
                            *reinterpret_cast<unsigned char*>(&rdx72) = reinterpret_cast<uint1_t>(rax70->f14 == 46);
                            *reinterpret_cast<uint32_t*>(&rdx15) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rax70) + reinterpret_cast<int64_t>(rdx72) + 20);
                            *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
                        } while (!*reinterpret_cast<signed char*>(&rdx15) || *reinterpret_cast<signed char*>(&rdx15) == 47);
                        goto addr_2a92_50;
                        addr_2a63_51:
                        if (!rax70->f0 || v69) {
                            rsi29 = v54;
                            eax73 = fun_2540(r13_71, rsi29, rdx15, rcx14, r8_13);
                            rsp39 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp39) - 8 + 8);
                            if (eax73 < 0) 
                                continue;
                            if (v74 != v75) 
                                continue;
                            if (v67 == rbx5) 
                                break;
                            if (v76 != v77) 
                                continue; else 
                                break;
                        } else {
                            if (rax70->f0 != v78) 
                                continue; else 
                                goto addr_2a79_57;
                        }
                        addr_2a92_50:
                        goto addr_2a63_51;
                    }
                    addr_2ace_58:
                    rax79 = fun_24a0(r13_71, r13_71);
                    rsi29 = r13_71;
                    rdx15 = rax79;
                    file_name_prepend(v44, rsi29, rdx15, rcx14, r8_13);
                    eax80 = fun_2530(r12_38, rsi29, rdx15, rcx14, r8_13);
                    rsp37 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp39) - 8 + 8 - 8 + 8 - 8 + 8);
                    if (eax80) 
                        goto addr_2b27_29;
                    rsi16 = v30;
                    ecx81 = 36;
                    while (ecx81) {
                        --ecx81;
                        rsi16 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsi16) + 4);
                    }
                    r14_11 = v57;
                    continue;
                    addr_2a79_57:
                    goto addr_2ace_58;
                }
            }
            addr_2c9d_63:
            rax82 = quotearg_style(4, "/", rdx15, rcx14, r8_13);
            rax83 = fun_2480();
            rax84 = fun_23e0();
            rcx14 = rax82;
            rdx15 = rax83;
            *reinterpret_cast<uint32_t*>(&rsi29) = *rax84;
            *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
            fun_2670();
            rsp39 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            continue;
            addr_2c5a_35:
            rax85 = quotearg_style(4, ".", rdx15, rcx14, r8_13);
            rax86 = fun_2480();
            fun_23e0();
            rcx14 = rax85;
            rdx15 = rax86;
            fun_2670();
            rsp51 = reinterpret_cast<void*>(rsp37 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_2c9d_63;
            addr_2c18_40:
            rax87 = nth_parent(r14_11, rsi16, rdx15, rcx14, r8_13);
            rax88 = quote(rax87, rsi16, rdx15, rcx14, r8_13);
            rax89 = fun_2480();
            fun_23e0();
            rcx14 = rax88;
            rdx15 = rax89;
            fun_2670();
            rsp37 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_2c5a_35;
        }
        addr_2b0c_47:
        *reinterpret_cast<uint32_t*>(&rbx5) = *r15_3;
        if (!*reinterpret_cast<uint32_t*>(&rbx5)) {
            eax90 = fun_2530(r12_38, rsi29, rdx15, rcx14, r8_13);
            rsp37 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp39) - 8 + 8);
            if (!eax90) {
                addr_2bce_27:
                rax91 = nth_parent(r14_11, rsi29, rdx15, rcx14, r8_13);
                rax92 = quote(rax91, rsi29, rdx15, rcx14, r8_13);
                r12_38 = rax92;
                rax93 = fun_2480();
                rcx14 = r12_38;
                *reinterpret_cast<uint32_t*>(&rsi29) = 0;
                *reinterpret_cast<int32_t*>(&rsi29 + 4) = 0;
                rdx15 = rax93;
                fun_2670();
                rsp39 = reinterpret_cast<void*>(rsp37 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_2c08_28;
            } else {
                addr_2b27_29:
                rax94 = nth_parent(r14_11, rsi29, rdx15, rcx14, r8_13);
                rax95 = quote(rax94, rsi29, rdx15, rcx14, r8_13);
                r12_38 = rax95;
                rax96 = fun_2480();
                *reinterpret_cast<uint32_t*>(&rsi16) = *r15_3;
                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                rcx14 = r12_38;
                rdx15 = rax96;
                fun_2670();
                rsp28 = reinterpret_cast<void*>(rsp37 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            }
        }
    }
}

int64_t __libc_start_main = 0;

void fun_2d63() {
    __asm__("cli ");
    __libc_start_main(0x2760, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_23a0(int64_t rdi);

int64_t fun_2e03() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23a0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2e43() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_2660(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_2560(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int32_t fun_2580(int64_t rdi);

int32_t fun_23f0(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int64_t stderr = 0;

void fun_26c0(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3053(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    int64_t r12_7;
    struct s0* rax8;
    int64_t r12_9;
    struct s0* rax10;
    int64_t r12_11;
    struct s0* rax12;
    int64_t r12_13;
    struct s0* rax14;
    int64_t r12_15;
    struct s0* rax16;
    struct s0* rax17;
    int32_t eax18;
    struct s0* r13_19;
    struct s0* rax20;
    struct s0* rax21;
    int32_t eax22;
    struct s0* rax23;
    struct s0* rax24;
    struct s0* rax25;
    int32_t eax26;
    struct s0* rax27;
    int64_t r15_28;
    struct s0* rax29;
    struct s0* rax30;
    struct s0* rax31;
    int64_t rdi32;
    struct s0* r8_33;
    struct s0* r9_34;
    int64_t v35;
    int64_t v36;
    int64_t v37;
    int64_t v38;
    int64_t v39;
    int64_t v40;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2480();
            fun_2660(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2480();
            fun_2560(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2480();
            fun_2560(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2480();
            fun_2560(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2480();
            fun_2560(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2480();
            fun_2560(rax16, r12_15, 5, rcx6);
            rax17 = fun_2480();
            fun_2660(1, rax17, "pwd", rcx6);
            do {
                if (1) 
                    break;
                eax18 = fun_2580("pwd");
            } while (eax18);
            r13_19 = v4;
            if (!r13_19) {
                rax20 = fun_2480();
                fun_2660(1, rax20, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax21 = fun_2650(5);
                if (!rax21 || (eax22 = fun_23f0(rax21, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax22)) {
                    rax23 = fun_2480();
                    r13_19 = reinterpret_cast<struct s0*>("pwd");
                    fun_2660(1, rax23, "https://www.gnu.org/software/coreutils/", "pwd");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_19 = reinterpret_cast<struct s0*>("pwd");
                    goto addr_33d0_9;
                }
            } else {
                rax24 = fun_2480();
                fun_2660(1, rax24, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax25 = fun_2650(5);
                if (!rax25 || (eax26 = fun_23f0(rax25, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax26)) {
                    addr_32d6_11:
                    rax27 = fun_2480();
                    fun_2660(1, rax27, "https://www.gnu.org/software/coreutils/", "pwd");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_19 == "pwd")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x7025);
                    }
                } else {
                    addr_33d0_9:
                    r15_28 = stdout;
                    rax29 = fun_2480();
                    fun_2560(rax29, r15_28, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_32d6_11;
                }
            }
            rax30 = fun_2480();
            rcx6 = r12_2;
            fun_2660(1, rax30, r13_19, rcx6);
            addr_30ae_14:
            fun_26a0();
        }
    } else {
        rax31 = fun_2480();
        rdi32 = stderr;
        rcx6 = r12_2;
        fun_26c0(rdi32, 1, rax31, rcx6, r8_33, r9_34, v35, v36, v37, v38, v39, v40);
        goto addr_30ae_14;
    }
}

int64_t file_name = 0;

void fun_3403(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3413(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_2400(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_3423() {
    int64_t rdi1;
    int32_t eax2;
    uint32_t* rax3;
    int1_t zf4;
    uint32_t* rbx5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23e0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2480();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_34b3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<uint32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2670();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2400(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_34b3_5:
        *reinterpret_cast<uint32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2670();
    }
}

void fun_26b0(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s7 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s7* fun_24f0();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_34d3(struct s0* rdi) {
    int64_t rcx2;
    struct s0* rbx3;
    struct s7* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_26b0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23d0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24f0();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23f0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_4c73(int64_t rdi) {
    int64_t rbp2;
    uint32_t* rax3;
    uint32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23e0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xb1e0;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_4cb3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb1e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4cd3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb1e0);
    }
    *rdi = esi;
    return 0xb1e0;
}

int64_t fun_4cf3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb1e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s8 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_4d33(struct s8* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xb1e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s9 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* fun_4d53(struct s9* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xb1e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x272a;
    if (!rdx) 
        goto 0x272a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb1e0;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_4d93(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, struct s10* r8) {
    struct s10* rbx6;
    uint32_t* rax7;
    uint32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s10*>(0xb1e0);
    }
    rax7 = fun_23e0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x4dc6);
    *rax7 = r15d8;
    return rax13;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_4e13(struct s0* rdi, uint32_t* rsi, struct s0** rdx, struct s11* rcx) {
    struct s11* rbx5;
    uint32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    uint32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s11*>(0xb1e0);
    }
    rax6 = fun_23e0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x4e41);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x4e9c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_4f03() {
    __asm__("cli ");
}

struct s0* gb078 = reinterpret_cast<struct s0*>(0xe0);

int64_t slotvec0 = 0x100;

void fun_4f13() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rsi7;
    struct s0* rdx8;
    uint32_t* rcx9;
    struct s0* rdi10;
    struct s0* rsi11;
    struct s0* rdx12;
    uint32_t* rcx13;
    struct s0* rsi14;
    struct s0* rdx15;
    uint32_t* rcx16;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23c0(rdi6, rsi7, rdx8, rcx9);
        } while (rbx4 != rbp5);
    }
    rdi10 = r12_2->f8;
    if (rdi10 != 0xb0e0) {
        fun_23c0(rdi10, rsi11, rdx12, rcx13);
        gb078 = reinterpret_cast<struct s0*>(0xb0e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb070) {
        fun_23c0(r12_2, rsi14, rdx15, rcx16);
        slotvec = reinterpret_cast<struct s0*>(0xb070);
    }
    nslots = 1;
    return;
}

void fun_4fb3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4fd3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_4fe3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5003(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_5023(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2730;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

struct s0* fun_50b3(struct s0* rdi, int32_t esi, struct s0* rdx, uint32_t* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2735;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

struct s0* fun_5143(int32_t edi, struct s0* rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x273a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_24c0();
    } else {
        return rax5;
    }
}

struct s0* fun_51d3(int32_t edi, struct s0* rsi, uint32_t* rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x273f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

struct s0* fun_5263(struct s0* rdi, uint32_t* rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5f70]");
    __asm__("movdqa xmm1, [rip+0x5f78]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x5f61]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_24c0();
    } else {
        return rax10;
    }
}

struct s0* fun_5303(struct s0* rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5ed0]");
    __asm__("movdqa xmm1, [rip+0x5ed8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x5ec1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

struct s0* fun_53a3(struct s0* rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5e30]");
    __asm__("movdqa xmm1, [rip+0x5e38]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x5e19]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_24c0();
    } else {
        return rax3;
    }
}

struct s0* fun_5433(struct s0* rdi, uint32_t* rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5da0]");
    __asm__("movdqa xmm1, [rip+0x5da8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x5d96]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_24c0();
    } else {
        return rax4;
    }
}

struct s0* fun_54c3(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2744;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

struct s0* fun_5563(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5c6a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x5c62]");
    __asm__("movdqa xmm2, [rip+0x5c6a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2749;
    if (!rdx) 
        goto 0x2749;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

struct s0* fun_5603(int32_t edi, int64_t rsi, int64_t rdx, struct s0* rcx, uint32_t* r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5bca]");
    __asm__("movdqa xmm1, [rip+0x5bd2]");
    __asm__("movdqa xmm2, [rip+0x5bda]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x274e;
    if (!rdx) 
        goto 0x274e;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

struct s0* fun_56b3(int64_t rdi, int64_t rsi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5b1a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5b12]");
    __asm__("movdqa xmm2, [rip+0x5b1a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2753;
    if (!rsi) 
        goto 0x2753;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

struct s0* fun_5753(int64_t rdi, int64_t rsi, struct s0* rdx, uint32_t* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5a7a]");
    __asm__("movdqa xmm1, [rip+0x5a82]");
    __asm__("movdqa xmm2, [rip+0x5a8a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2758;
    if (!rsi) 
        goto 0x2758;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void fun_57f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5803(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5823() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5843(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s12 {
    int64_t f0;
    int64_t f8;
};

struct s12* fun_5863(struct s12* rdi) {
    struct s0* rax2;
    struct s0* rdx3;
    uint32_t* rcx4;
    int64_t r8_5;
    int32_t eax6;
    struct s12* rax7;
    int64_t v8;
    int64_t v9;
    void* rdx10;

    __asm__("cli ");
    rax2 = g28;
    eax6 = fun_2540("/", reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0, rdx3, rcx4, r8_5);
    if (eax6) {
        *reinterpret_cast<int32_t*>(&rax7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    } else {
        rdi->f0 = v8;
        rdi->f8 = v9;
        rax7 = rdi;
    }
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax7;
    }
}

struct s13 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_25a0(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_58e3(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s13* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_26c0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_26c0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2480();
    fun_26c0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_25a0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2480();
    fun_26c0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_25a0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2480();
        fun_26c0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7c08 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7c08;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_5d53() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s14 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_5d73(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s14* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s14* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_24c0();
    } else {
        return;
    }
}

void fun_5e13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_5eb6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_5ec0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_24c0();
    } else {
        return;
    }
    addr_5eb6_5:
    goto addr_5ec0_7;
}

void fun_5ef3() {
    int64_t rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_25a0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2480();
    fun_2660(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2480();
    fun_2660(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2480();
    goto fun_2660;
}

int64_t fun_2430();

void xalloc_die();

void fun_5f93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2430();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_25f0(signed char* rdi);

void fun_5fd3(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25f0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_5ff3(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25f0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6013(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_25f0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2640();

void fun_6033(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2640();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6063() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6093(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2430();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_60d3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2430();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6113(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2430();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6143(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2430();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6193(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2430();
            if (rax5) 
                break;
            addr_61dd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_61dd_5;
        rax8 = fun_2430();
        if (rax8) 
            goto addr_61c6_9;
        if (rbx4) 
            goto addr_61dd_5;
        addr_61c6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6223(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2430();
            if (rax8) 
                break;
            addr_626a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_626a_5;
        rax11 = fun_2430();
        if (rax11) 
            goto addr_6252_9;
        if (!rbx6) 
            goto addr_6252_9;
        if (r12_4) 
            goto addr_626a_5;
        addr_6252_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_62b3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_635d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_6370_10:
                *r12_8 = 0;
            }
            addr_6310_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6336_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6384_14;
            if (rcx10 <= rsi9) 
                goto addr_632d_16;
            if (rsi9 >= 0) 
                goto addr_6384_14;
            addr_632d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6384_14;
            addr_6336_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2640();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6384_14;
            if (!rbp13) 
                break;
            addr_6384_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_635d_9;
        } else {
            if (!r13_6) 
                goto addr_6370_10;
            goto addr_6310_11;
        }
    }
}

int64_t fun_2570();

void fun_63b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2570();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_63e3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2570();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6413() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2570();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6433() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2570();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6453(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25f0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25c0;
    }
}

void fun_6493(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25f0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25c0;
    }
}

struct s15 {
    signed char[1] pad1;
    signed char f1;
};

void fun_64d3(int64_t rdi, struct s15* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_25f0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_25c0;
    }
}

void fun_6513(struct s0* rdi) {
    struct s0* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_24a0(rdi);
    rax3 = fun_25f0(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25c0;
    }
}

void fun_6553() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2480();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2670();
    fun_23d0(rdi1);
}

int64_t fun_2520();

int64_t fun_6593() {
    int64_t rax1;
    uint32_t* rax2;

    __asm__("cli ");
    rax1 = fun_2520();
    if (rax1 || (rax2 = fun_23e0(), *rax2 != 12)) {
        return rax1;
    } else {
        xalloc_die();
    }
}

int64_t fun_2410();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_65c3(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    uint32_t* rax5;
    uint32_t* rax6;

    __asm__("cli ");
    rax2 = fun_2410();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_661e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23e0();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_661e_3;
            rax6 = fun_23e0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s16 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_25d0(struct s16* rdi);

int32_t fun_2620(struct s16* rdi);

int64_t fun_2500(int64_t rdi, ...);

int32_t rpl_fflush(struct s16* rdi);

int64_t fun_2450(struct s16* rdi);

int64_t fun_6633(struct s16* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    uint32_t* rax8;
    uint32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_25d0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2620(rdi);
        if (!(eax3 && (eax4 = fun_25d0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2500(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23e0();
            r12d9 = *rax8;
            rax10 = fun_2450(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2450;
}

void rpl_fseeko(struct s16* rdi);

void fun_66c3(struct s16* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2620(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6713(struct s16* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_25d0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2500(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2610(int64_t rdi);

signed char* fun_6793() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2610(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24e0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_67d3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24e0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_24c0();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_6863() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax3;
    }
}

int64_t fun_68e3(int64_t rdi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2650(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<struct s0**>(&rsi->f0) = reinterpret_cast<struct s0*>(0);
        }
    } else {
        rax6 = fun_24a0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_25c0(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_25c0(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6993() {
    __asm__("cli ");
    goto fun_2650;
}

void fun_69a3() {
    __asm__("cli ");
}

void fun_69b7() {
    __asm__("cli ");
    return;
}

uint32_t fun_2550(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_26e0(int64_t rdi, struct s0* rsi);

uint32_t fun_26d0(struct s0* rdi, struct s0* rsi);

void** fun_2710(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_3705() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2480();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2480();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24a0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3a03_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3a03_22; else 
                            goto addr_3dfd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_3ebd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4210_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3a00_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3a00_30; else 
                                goto addr_4229_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24a0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4210_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2550(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4210_28; else 
                            goto addr_38ac_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4370_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_41f0_40:
                        if (r11_27 == 1) {
                            addr_3d7d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4338_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_39b7_44;
                            }
                        } else {
                            goto addr_4200_46;
                        }
                    } else {
                        addr_437f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_3d7d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3a03_22:
                                if (v47 != 1) {
                                    addr_3f59_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_24a0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_3fa4_54;
                                    }
                                } else {
                                    goto addr_3a10_56;
                                }
                            } else {
                                addr_39b5_57:
                                ebp36 = 0;
                                goto addr_39b7_44;
                            }
                        } else {
                            addr_41e4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_437f_47; else 
                                goto addr_41ee_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_3d7d_41;
                        if (v47 == 1) 
                            goto addr_3a10_56; else 
                            goto addr_3f59_52;
                    }
                }
                addr_3a71_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3908_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_392d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_3c30_65;
                    } else {
                        addr_3a99_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_42e8_67;
                    }
                } else {
                    goto addr_3a90_69;
                }
                addr_3941_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_398c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_42e8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_398c_81;
                }
                addr_3a90_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_392d_64; else 
                    goto addr_3a99_66;
                addr_39b7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_3a6f_91;
                if (v22) 
                    goto addr_39cf_93;
                addr_3a6f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3a71_62;
                addr_3fa4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_472b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_479b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_459f_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26e0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_26d0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_409e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_3a5c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_40a8_112;
                    }
                } else {
                    addr_40a8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4179_114;
                }
                addr_3a68_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_3a6f_91;
                while (1) {
                    addr_4179_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4687_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_40e6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4695_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4167_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4167_128;
                        }
                    }
                    addr_4115_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4167_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_40e6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->pad8);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4115_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_398c_81;
                addr_4695_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_42e8_67;
                addr_472b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_409e_109;
                addr_479b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_409e_109;
                addr_3a10_56:
                rax93 = fun_2710(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_3a5c_110;
                addr_41ee_59:
                goto addr_41f0_40;
                addr_3ebd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3a03_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3a68_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_39b5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3a03_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_3f02_160;
                if (!v22) 
                    goto addr_42d7_162; else 
                    goto addr_44e3_163;
                addr_3f02_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_42d7_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_42e8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_3dab_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_3c13_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3941_70; else 
                    goto addr_3c27_169;
                addr_3dab_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3908_63;
                goto addr_3a90_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_41e4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_431f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3a00_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_38f8_178; else 
                        goto addr_42a2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_41e4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3a03_22;
                }
                addr_431f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3a00_30:
                    r8d42 = 0;
                    goto addr_3a03_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3a71_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4338_42;
                    }
                }
                addr_38f8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3908_63;
                addr_42a2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4200_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3a71_62;
                } else {
                    addr_42b2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3a03_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4a62_188;
                if (v28) 
                    goto addr_42d7_162;
                addr_4a62_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_3c13_168;
                addr_38ac_37:
                if (v22) 
                    goto addr_48a3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_38c3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4370_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_43fb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3a03_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_38f8_178; else 
                        goto addr_43d7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_41e4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3a03_22;
                }
                addr_43fb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3a03_22;
                }
                addr_43d7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4200_46;
                goto addr_42b2_186;
                addr_38c3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3a03_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3a03_22; else 
                    goto addr_38d4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_49ae_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4834_210;
            if (1) 
                goto addr_4832_212;
            if (!v29) 
                goto addr_446e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2490();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_49a1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_3c30_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_39eb_219; else 
            goto addr_3c4a_220;
        addr_39cf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_39e3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_3c4a_220; else 
            goto addr_39eb_219;
        addr_459f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_3c4a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2490();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_45bd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2490();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_4a30_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4496_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_4687_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_39e3_221;
        addr_44e3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_39e3_221;
        addr_3c27_169:
        goto addr_3c30_65;
        addr_49ae_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_3c4a_220;
        goto addr_45bd_222;
        addr_4834_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_488e_236;
        fun_24c0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4a30_225;
        addr_4832_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4834_210;
        addr_446e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4834_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_4496_226;
        }
        addr_49a1_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_3dfd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x76cc + rax113 * 4) + 0x76cc;
    addr_4229_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x77cc + rax114 * 4) + 0x77cc;
    addr_48a3_190:
    addr_39eb_219:
    goto 0x36d0;
    addr_38d4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x75cc + rax115 * 4) + 0x75cc;
    addr_488e_236:
    goto v116;
}

void fun_38f0() {
}

void fun_3aa8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x37a2;
}

void fun_3b01() {
    goto 0x37a2;
}

void fun_3bee() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3a71;
    }
    if (v2) 
        goto 0x44e3;
    if (!r10_3) 
        goto addr_464e_5;
    if (!v4) 
        goto addr_451e_7;
    addr_464e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_451e_7:
    goto 0x3924;
}

void fun_3c0c() {
}

void fun_3cb7() {
    signed char v1;

    if (v1) {
        goto 0x3c3f;
    } else {
        goto 0x397a;
    }
}

void fun_3cd1() {
    signed char v1;

    if (!v1) 
        goto 0x3cca; else 
        goto "???";
}

void fun_3cf8() {
    goto 0x3c13;
}

void fun_3d78() {
}

void fun_3d90() {
}

void fun_3dbf() {
    goto 0x3c13;
}

void fun_3e11() {
    goto 0x3da0;
}

void fun_3e40() {
    goto 0x3da0;
}

void fun_3e73() {
    goto 0x3da0;
}

void fun_4240() {
    goto 0x38f8;
}

void fun_453e() {
    signed char v1;

    if (v1) 
        goto 0x44e3;
    goto 0x3924;
}

void fun_45e5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3924;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3908;
        goto 0x3924;
    }
}

void fun_4a02() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x3c70;
    } else {
        goto 0x37a2;
    }
}

void fun_59b8() {
    fun_2480();
}

void fun_3b2e() {
    goto 0x37a2;
}

void fun_3d04() {
    goto 0x3cbc;
}

void fun_3dcb() {
    goto 0x38f8;
}

void fun_3e1d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x3da0;
    goto 0x39cf;
}

void fun_3e4f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x3dab;
        goto 0x37d0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x3c4a;
        goto 0x39eb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x45e8;
    if (r10_8 > r15_9) 
        goto addr_3d35_9;
    addr_3d3a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x45f3;
    goto 0x3924;
    addr_3d35_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_3d3a_10;
}

void fun_3e82() {
    goto 0x39b7;
}

void fun_4250() {
    goto 0x39b7;
}

void fun_49ef() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x3b0c;
    } else {
        goto 0x3c70;
    }
}

void fun_5a70() {
}

void fun_3e8c() {
    goto 0x3e27;
}

void fun_425a() {
    goto 0x3d7d;
}

void fun_5ad0() {
    fun_2480();
    goto fun_26c0;
}

void fun_3b5d() {
    goto 0x37a2;
}

void fun_3e98() {
    goto 0x3e27;
}

void fun_4267() {
    goto 0x3dce;
}

void fun_5b10() {
    fun_2480();
    goto fun_26c0;
}

void fun_3b8a() {
    goto 0x37a2;
}

void fun_3ea4() {
    goto 0x3da0;
}

void fun_5b50() {
    fun_2480();
    goto fun_26c0;
}

void fun_3bac() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4540;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3a71;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3a71;
    }
    if (v11) 
        goto 0x48a3;
    if (r10_12 > r15_13) 
        goto addr_48f3_8;
    addr_48f8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4631;
    addr_48f3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_48f8_9;
}

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s19 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_5ba0() {
    int64_t r15_1;
    struct s17* rbx2;
    struct s0* r14_3;
    struct s18* rbx4;
    struct s0* r13_5;
    struct s19* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2480();
    fun_26c0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x5bc2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_5bf8() {
    fun_2480();
    goto 0x5bc9;
}

struct s20 {
    signed char[32] pad32;
    int64_t f20;
};

struct s21 {
    signed char[24] pad24;
    int64_t f18;
};

struct s22 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s23 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s24 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_5c30() {
    int64_t rcx1;
    struct s20* rbx2;
    int64_t r15_3;
    struct s21* rbx4;
    struct s0* r14_5;
    struct s22* rbx6;
    struct s0* r13_7;
    struct s23* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s24* rbx12;
    struct s0* rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2480();
    fun_26c0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x5c64, __return_address(), rcx1);
    goto v15;
}

void fun_5ca8() {
    fun_2480();
    goto 0x5c6b;
}
