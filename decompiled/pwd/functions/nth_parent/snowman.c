struct s0* nth_parent(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8) {
    struct s0* rax6;
    struct s0* rdx7;
    struct s0* rcx8;

    rax6 = xnmalloc(3, rdi);
    if (!rdi) {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax6) + 0xffffffffffffffff) = 0;
        return rax6;
    } else {
        rdx7 = rax6;
        rcx8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rdi) * 2 + reinterpret_cast<unsigned char>(rax6));
        do {
            rdx7->f2 = 47;
            rdx7 = reinterpret_cast<struct s0*>(&rdx7->pad8);
            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rdx7) + 0xfffffffffffffffd) = 0x2e2e;
        } while (rdx7 != rcx8);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx8) + 0xffffffffffffffff) = 0;
        return rax6;
    }
}