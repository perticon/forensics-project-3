char * nth_parent(int64_t n) {
    char * result = xnmalloc(3, n); // 0x2f4c
    int64_t v1 = (int64_t)result; // 0x2f4c
    if (n == 0) {
        // 0x2f80
        *(char *)(v1 - 1) = 0;
        return result;
    }
    int64_t v2 = 3 * n + v1; // 0x2f5d
    *(char *)(v1 + 2) = 47;
    int64_t v3 = v1 + 3; // 0x2f69
    *(int16_t *)v1 = 0x2e2e;
    while (v3 != v2) {
        int64_t v4 = v3;
        *(char *)(v4 + 2) = 47;
        v3 = v4 + 3;
        *(int16_t *)v4 = 0x2e2e;
    }
    // 0x2f76
    *(char *)(v2 - 1) = 0;
    return result;
}