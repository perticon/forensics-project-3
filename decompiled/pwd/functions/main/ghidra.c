undefined8 main(int param_1,undefined8 *param_2)

{
  undefined *puVar1;
  undefined8 *puVar2;
  int iVar3;
  int iVar4;
  char *pcVar5;
  void **ppvVar6;
  void *pvVar7;
  __ino_t *p_Var8;
  DIR *__dirp;
  dirent *pdVar9;
  size_t sVar10;
  undefined8 uVar11;
  undefined8 uVar12;
  int *piVar13;
  long lVar14;
  stat *psVar15;
  stat *psVar16;
  undefined *puVar17;
  undefined1 *puVar18;
  long in_FS_OFFSET;
  bool bVar19;
  byte bVar20;
  void **local_240;
  undefined local_208 [16];
  stat local_1f8;
  stat local_168;
  stat local_d8;
  long local_40;
  
  bVar20 = 0;
  puVar18 = longopts;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  pcVar5 = getenv("POSIXLY_CORRECT");
  bVar19 = pcVar5 != (char *)0x0;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar17 = &DAT_001070f1;
  textdomain("coreutils");
  atexit(close_stdout);
LAB_001027f5:
  do {
    iVar3 = getopt_long(param_1,param_2,&DAT_001070f1,longopts,0);
    if (iVar3 == -1) goto LAB_0010287b;
    if (iVar3 != 0x4c) {
      if (iVar3 < 0x4d) {
        if (iVar3 == -0x83) {
          version_etc(stdout,&DAT_0010700b,"GNU coreutils",Version,"Jim Meyering",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar3 == -0x82) {
          usage();
          goto LAB_00102870;
        }
      }
      else if (iVar3 == 0x50) {
        bVar19 = false;
        goto LAB_001027f5;
      }
      usage(1);
      goto LAB_0010290e;
    }
LAB_00102870:
    bVar19 = true;
  } while( true );
code_r0x00102af2:
  psVar15 = &local_168;
  psVar16 = &local_1f8;
  for (lVar14 = 0x24; puVar17 = puVar1, lVar14 != 0; lVar14 = lVar14 + -1) {
    *(undefined4 *)&psVar16->st_dev = *(undefined4 *)&psVar15->st_dev;
    psVar15 = (stat *)((long)psVar15 + (ulong)bVar20 * -8 + 4);
    psVar16 = (stat *)((long)psVar16 + (ulong)bVar20 * -8 + 4);
  }
  goto LAB_001029a5;
LAB_0010287b:
  if (optind < param_1) {
    uVar11 = dcgettext(0,"ignoring non-option arguments",5);
    error(0,0,uVar11);
  }
  if ((bVar19) && (pcVar5 = (char *)logical_getcwd(), pcVar5 != (char *)0x0)) {
    puts(pcVar5);
  }
  else {
    pcVar5 = (char *)xgetcwd();
    if (pcVar5 == (char *)0x0) {
      ppvVar6 = (void **)xmalloc(0x18);
      ppvVar6[1] = (void *)0x2000;
      pvVar7 = (void *)xmalloc(0x2000);
      *ppvVar6 = pvVar7;
      ppvVar6[2] = (void *)((long)pvVar7 + 0x1fff);
      *(undefined *)((long)pvVar7 + 0x1fff) = 0;
      p_Var8 = (__ino_t *)get_root_dev_ino(local_208);
      if (p_Var8 != (__ino_t *)0x0) {
        puVar17 = (undefined *)0x1;
        iVar3 = stat(".",&local_1f8);
        if (-1 < iVar3) {
LAB_001029a5:
          local_240 = ppvVar6;
          if ((local_1f8.st_ino == *p_Var8) &&
             ((undefined8 *)local_1f8.st_dev == (undefined8 *)p_Var8[1])) goto LAB_00102d1b;
          puVar1 = puVar17 + 1;
          __dirp = opendir("..");
          if (__dirp != (DIR *)0x0) {
            iVar3 = dirfd(__dirp);
            if (iVar3 < 0) goto LAB_00102b62;
            iVar4 = fchdir(iVar3);
            if (iVar4 < 0) goto LAB_00102b8c;
            iVar3 = fstat(iVar3,&local_168);
            do {
              puVar18 = (undefined1 *)__errno_location();
              puVar2 = (undefined8 *)local_168.st_dev;
              param_2 = (undefined8 *)local_1f8.st_dev;
              if (iVar3 < 0) goto LAB_00102ce0;
              bVar19 = local_168.st_dev == local_1f8.st_dev;
              do {
                while( true ) {
                  *(uint *)puVar18 = 0;
                  do {
                    pdVar9 = readdir(__dirp);
                    if (pdVar9 == (dirent *)0x0) {
                      param_2 = (undefined8 *)(ulong)*(uint *)puVar18;
                      if (*(uint *)puVar18 != 0) goto LAB_00102c08;
                      iVar3 = closedir(__dirp);
                      if (iVar3 == 0) goto LAB_00102bce;
                      goto LAB_00102b27;
                    }
                    pcVar5 = pdVar9->d_name;
                  } while ((pdVar9->d_name[0] == '.') &&
                          ((pdVar9->d_name[(ulong)(pdVar9->d_name[1] == '.') + 1] == '\0' ||
                           (pdVar9->d_name[(ulong)(pdVar9->d_name[1] == '.') + 1] == '/'))));
                  if ((pdVar9->d_ino != 0) && (bVar19)) break;
                  iVar3 = lstat(pcVar5,&local_d8);
                  if (((-1 < iVar3) && (local_d8.st_ino == local_1f8.st_ino)) &&
                     ((puVar2 == param_2 || (local_d8.st_dev == local_1f8.st_dev))))
                  goto LAB_00102ace;
                }
              } while (pdVar9->d_ino != local_1f8.st_ino);
LAB_00102ace:
              sVar10 = strlen(pcVar5);
              file_name_prepend(ppvVar6,pcVar5,sVar10);
              iVar3 = closedir(__dirp);
              if (iVar3 == 0) goto code_r0x00102af2;
LAB_00102b27:
              while( true ) {
                uVar11 = nth_parent(puVar17);
                __dirp = (DIR *)quote(uVar11);
                uVar11 = dcgettext(0,"reading directory %s",5);
                error(1,*(uint *)puVar18,uVar11,__dirp);
LAB_00102b62:
                iVar3 = chdir("..");
                if (-1 < iVar3) break;
LAB_00102b8c:
                uVar11 = nth_parent(puVar17);
                uVar11 = quote(uVar11);
                uVar12 = dcgettext(0,"failed to chdir to %s",5);
                piVar13 = __errno_location();
                error(1,*piVar13,uVar12,uVar11);
LAB_00102bce:
                uVar11 = nth_parent(puVar17);
                __dirp = (DIR *)quote(uVar11);
                uVar11 = dcgettext(0,"couldn\'t find directory entry in %s with matching i-node",5);
                error(1,0,uVar11,__dirp);
LAB_00102c08:
                closedir(__dirp);
                *(uint *)puVar18 = (uint)param_2;
              }
              iVar3 = stat(".",&local_168);
            } while( true );
          }
          uVar11 = nth_parent(puVar17);
          uVar11 = quote(uVar11);
          uVar12 = dcgettext(0,"cannot open directory %s",5);
          piVar13 = __errno_location();
          error(1,*piVar13,uVar12,uVar11);
        }
        uVar11 = quotearg_style(4,&DAT_00107132);
        uVar12 = dcgettext(0,"failed to stat %s",5);
        piVar13 = __errno_location();
        error(1,*piVar13,uVar12,uVar11);
      }
      uVar11 = quotearg_style(4,"/");
      uVar12 = dcgettext(0,"failed to get attributes of %s",5);
      piVar13 = __errno_location();
      error(1,*piVar13,uVar12,uVar11);
LAB_00102ce0:
      uVar11 = nth_parent(puVar17);
      uVar11 = quote(uVar11);
      uVar12 = dcgettext(0,"failed to stat %s",5);
      error(1,*(uint *)puVar18,uVar12,uVar11);
LAB_00102d1b:
      pcVar5 = (char *)local_240[2];
      if (*pcVar5 == '\0') {
        file_name_prepend(local_240,"",0);
        pcVar5 = (char *)local_240[2];
      }
      puts(pcVar5);
      free(*local_240);
      free(local_240);
    }
    else {
      puts(pcVar5);
      free(pcVar5);
    }
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return 0;
  }
LAB_0010290e:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}