word64 file_name_prepend(Eq_48 rdx, Eq_17 rsi, Eq_17 rdi)
{
	Eq_17 rax_30 = *((word64) rdi + 16);
	uint64 r14_32 = rax_30 - *rdi;
	if ((word64) rdx + 1 > r14_32)
	{
		uint64 rdx_35 = (word64) *((word64) rdi + 8) + ((word64) rdx + 1);
		xnmalloc(rax_30);
		uint64 rdx_49 = *((word64) rdi + 8);
		Eq_17 r8_50 = *rdi;
		Eq_17 rdi_57 = (word64) rax_30 + ((rdx_35 * 0x02 + r14_32) - rdx_49);
		*((word64) rdi + 16) = rdi_57;
		fn00000000000025C0(rdx_49 - r14_32, (word64) r8_50 + r14_32, rdi_57);
		fn00000000000023C0(r8_50);
		*rdi = rax_30;
		rax_30 = *((word64) rdi + 16);
		*((word64) rdi + 8) = rdx_35 * 0x02;
	}
	word64 rax_71 = rax_30 + ~rdx;
	*((word64) rdi + 16) = rax_71;
	rax_71->u0 = 0x2F;
	Eq_17 rdi_76 = *((word64) rdi + 16);
	return fn00000000000025C0(rdx, rsi, (word64) rdi_76 + 1);
}