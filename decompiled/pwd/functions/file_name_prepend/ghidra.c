void file_name_prepend(void **param_1,void *param_2,ulong param_3)

{
  void *pvVar1;
  void *pvVar2;
  void *pvVar3;
  long lVar4;
  void *__dest;
  ulong uVar5;
  
  pvVar2 = param_1[2];
  uVar5 = (long)pvVar2 - (long)*param_1;
  if (uVar5 < param_3 + 1) {
    lVar4 = param_3 + 1 + (long)param_1[1];
    pvVar3 = (void *)xnmalloc(2,lVar4);
    pvVar1 = (void *)(lVar4 * 2);
    pvVar2 = *param_1;
    __dest = (void *)((long)pvVar1 + (uVar5 - (long)param_1[1]) + (long)pvVar3);
    param_1[2] = __dest;
    memcpy(__dest,(void *)((long)pvVar2 + uVar5),(long)param_1[1] - uVar5);
    free(pvVar2);
    *param_1 = pvVar3;
    pvVar2 = param_1[2];
    param_1[1] = pvVar1;
  }
  param_1[2] = (undefined *)((long)pvVar2 + ~param_3);
  *(undefined *)((long)pvVar2 + ~param_3) = 0x2f;
  memcpy((void *)((long)param_1[2] + 1),param_2,param_3);
  return;
}