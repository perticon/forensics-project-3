void file_name_prepend(struct s0* rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx, int64_t r8) {
    signed char* rdx6;
    struct s0* rax7;
    signed char* r14_8;
    struct s0* rdx9;
    struct s0* rax10;
    struct s0* rbp11;
    void* rdi12;
    struct s0* rsi13;
    struct s0* rdx14;
    struct s0* v15;
    struct s0* rdi16;
    struct s0* rax17;

    rdx6 = &rdx->f1;
    rax7 = rdi->f10;
    r14_8 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0)));
    if (reinterpret_cast<uint64_t>(rdx6) > reinterpret_cast<uint64_t>(r14_8)) {
        rdx9 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdx6) + reinterpret_cast<unsigned char>(rdi->f8));
        rax10 = xnmalloc(2, rdx9);
        rbp11 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdx9) + reinterpret_cast<unsigned char>(rdx9));
        rdi12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbp11) + reinterpret_cast<uint64_t>(r14_8) - reinterpret_cast<unsigned char>(rdi->f8));
        rsi13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi->f0)) + reinterpret_cast<uint64_t>(r14_8));
        rdx14 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi->f8) - reinterpret_cast<uint64_t>(r14_8));
        v15 = *reinterpret_cast<struct s0**>(&rdi->f0);
        rdi16 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi12) + reinterpret_cast<unsigned char>(rax10));
        rdi->f10 = rdi16;
        fun_25c0(rdi16, rsi13, rdx14);
        fun_23c0(v15, rsi13, rdx14, rcx);
        *reinterpret_cast<struct s0**>(&rdi->f0) = rax10;
        rax7 = rdi->f10;
        rdi->f8 = rbp11;
    }
    rax17 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rdx)));
    rdi->f10 = rax17;
    *reinterpret_cast<struct s0**>(&rax17->f0) = reinterpret_cast<struct s0*>(47);
}