void file_name_prepend(int32_t * p, char * s, int64_t s_len) {
    int64_t v1 = (int64_t)p;
    uint64_t v2 = s_len + 1; // 0x2f9e
    int64_t * v3 = (int64_t *)(v1 + 16); // 0x2fab
    int64_t v4 = *v3; // 0x2fab
    uint64_t v5 = v4 - v1; // 0x2fb2
    int64_t v6 = v4; // 0x2fb8
    if (v2 > v5) {
        int64_t * v7 = (int64_t *)(v1 + 8); // 0x2ff0
        int64_t v8 = *v7 + v2; // 0x2ff0
        int64_t v9 = (int64_t)xnmalloc(2, v8); // 0x2fff
        int64_t v10 = 2 * v8; // 0x3004
        *v3 = v10 + v5 - *v7 + v9;
        function_25c0();
        function_23c0();
        *(int64_t *)p = v9;
        v6 = *v3;
        *v7 = v10;
    }
    int64_t v11 = v6 + -1 - s_len; // 0x2fc3
    *v3 = v11;
    *(char *)v11 = 47;
    function_25c0();
}