char * logical_getcwd(struct Eq_426 * fs)
{
	ptr64 fp;
	word64 qwLocB0;
	word64 qwLoc0140;
	word64 qwLocB8;
	word64 qwLoc0148;
	char * r12_25;
	word64 rax_13 = fs->qw0028;
	char * rax_20 = fn00000000000023B0("PWD");
	if (rax_20 != null)
	{
		r12_25 = rax_20;
		if (*rax_20 == 0x2F)
		{
			char * rdi_27 = rax_20;
			while (true)
			{
				struct Eq_1110 * rax_32 = fn0000000000002700("/.", rdi_27);
				if (rax_32 == null)
					break;
				byte dl_59 = rax_32->b0002;
				if (dl_59 == 0x00 || dl_59 == 0x2F)
					goto l0000000000002ED0;
				if (dl_59 == 0x2E)
				{
					byte dl_69 = rax_32->b0003;
					if (dl_69 == 0x00 || dl_69 == 0x2F)
						goto l0000000000002ED0;
				}
				rdi_27 = (char *) rax_32 + 1;
			}
			if (fn00000000000025B0(fp - 0x0148, rax_20) == 0x00 && (fn00000000000025B0(fp - 0xB8, ".") == 0x00 && (qwLoc0140 == qwLocB0 && qwLoc0148 == qwLocB8)))
			{
l0000000000002ED3:
				if (rax_13 - fs->qw0028 == 0x00)
					return r12_25;
				fn00000000000024C0();
			}
		}
	}
l0000000000002ED0:
	r12_25 = null;
	goto l0000000000002ED3;
}