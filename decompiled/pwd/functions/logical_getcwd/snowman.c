struct s0* logical_getcwd(int64_t rdi, struct s0* rsi, struct s0* rdx, uint32_t* rcx) {
    struct s0* rdi5;
    struct s0* rax6;
    struct s0* v7;
    struct s0* rax8;
    struct s0* rsp9;
    struct s0* r12_10;
    struct s3* rax11;
    uint32_t edx12;
    uint32_t edx13;
    uint32_t eax14;
    uint32_t eax15;
    int64_t v16;
    int64_t v17;
    struct s0* v18;
    struct s0* v19;
    void* rax20;
    struct s0* rax21;
    int64_t v22;
    struct s0* rdx23;
    struct s0* rcx24;
    int64_t v25;

    rdi5 = reinterpret_cast<struct s0*>("PWD");
    rax6 = g28;
    v7 = rax6;
    rax8 = fun_23b0("PWD");
    rsp9 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x138 - 8 + 8);
    if (!rax8) 
        goto addr_2ed0_2;
    r12_10 = rax8;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax8->f0) == 47)) 
        goto addr_2ed0_2;
    rdi5 = rax8;
    while (rax11 = fun_2700(), rsp9 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp9) - 8 + 8), !!rax11) {
        edx12 = rax11->f2;
        if (!*reinterpret_cast<signed char*>(&edx12)) 
            goto addr_2ed0_2;
        if (*reinterpret_cast<signed char*>(&edx12) == 47) 
            goto addr_2ed0_2;
        if (*reinterpret_cast<signed char*>(&edx12) == 46) {
            edx13 = rax11->f3;
            if (!*reinterpret_cast<signed char*>(&edx13)) 
                goto addr_2ed0_2;
            if (*reinterpret_cast<signed char*>(&edx13) == 47) 
                goto addr_2ec8_11;
        }
        rdi5 = reinterpret_cast<struct s0*>(&rax11->f1);
    }
    rdi5 = r12_10;
    eax14 = fun_25b0(rdi5, rsp9);
    rsp9 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rsp9) - 8 + 8);
    if (eax14 || ((rdi5 = reinterpret_cast<struct s0*>("."), eax15 = fun_25b0(".", reinterpret_cast<unsigned char>(rsp9) + 0x90), !!eax15) || (v16 != v17 || v18 != v19))) {
        addr_2ed0_2:
        *reinterpret_cast<int32_t*>(&r12_10) = 0;
        *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
    }
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (!rax20) {
        return r12_10;
    }
    fun_24c0();
    rax21 = xnmalloc(3, rdi5);
    if (rdi5) 
        goto addr_2f56_19;
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax21) + 0xffffffffffffffff) = 0;
    goto v22;
    addr_2f56_19:
    rdx23 = rax21;
    rcx24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi5) + reinterpret_cast<unsigned char>(rdi5) * 2 + reinterpret_cast<unsigned char>(rax21));
    do {
        rdx23->f2 = 47;
        rdx23 = reinterpret_cast<struct s0*>(&rdx23->pad8);
        *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rdx23) + 0xfffffffffffffffd) = 0x2e2e;
    } while (rdx23 != rcx24);
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx24) + 0xffffffffffffffff) = 0;
    goto v25;
    addr_2ec8_11:
    goto addr_2ed0_2;
}