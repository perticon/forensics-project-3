char * logical_getcwd(void)

{
  char cVar1;
  int iVar2;
  char *__file;
  char *pcVar3;
  long in_FS_OFFSET;
  stat local_148;
  stat local_b8;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  __file = getenv("PWD");
  if ((__file != (char *)0x0) && (pcVar3 = __file, *__file == '/')) {
    while (pcVar3 = strstr(pcVar3,"/."), pcVar3 != (char *)0x0) {
      cVar1 = pcVar3[2];
      if (((cVar1 == '\0') || (cVar1 == '/')) ||
         ((cVar1 == '.' && ((pcVar3[3] == '\0' || (pcVar3[3] == '/')))))) goto LAB_00102ed0;
      pcVar3 = pcVar3 + 1;
    }
    iVar2 = stat(__file,&local_148);
    if ((((iVar2 == 0) && (iVar2 = stat(".",&local_b8), iVar2 == 0)) &&
        (local_148.st_ino == local_b8.st_ino)) && (local_148.st_dev == local_b8.st_dev))
    goto LAB_00102ed3;
  }
LAB_00102ed0:
  __file = (char *)0x0;
LAB_00102ed3:
  if (local_20 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return __file;
}