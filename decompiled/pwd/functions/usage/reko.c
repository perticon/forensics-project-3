void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000026C0(fn0000000000002480(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000030AE;
	}
	fn0000000000002660(fn0000000000002480(0x05, "Usage: %s [OPTION]...\n", null), 0x01);
	fn0000000000002560(stdout, fn0000000000002480(0x05, "Print the full filename of the current working directory.\n\n", null));
	fn0000000000002560(stdout, fn0000000000002480(0x05, "  -L, --logical   use PWD from environment, even if it contains symlinks\n  -P, --physical  avoid all symlinks\n", null));
	fn0000000000002560(stdout, fn0000000000002480(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002560(stdout, fn0000000000002480(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002560(stdout, fn0000000000002480(0x05, "\nIf no option is specified, -P is assumed.\n", null));
	fn0000000000002660(fn0000000000002480(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	struct Eq_1330 * rbx_184 = fp - 0xB8 + 16;
	do
	{
		char * rsi_186 = rbx_184->qw0000;
		++rbx_184;
	} while (rsi_186 != null && fn0000000000002580(rsi_186, "pwd") != 0x00);
	ptr64 r13_199 = rbx_184->qw0008;
	if (r13_199 != 0x00)
	{
		fn0000000000002660(fn0000000000002480(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_17 rax_286 = fn0000000000002650(null, 0x05);
		if (rax_286 == 0x00 || fn00000000000023F0(0x03, "en_", rax_286) == 0x00)
			goto l00000000000032D6;
	}
	else
	{
		fn0000000000002660(fn0000000000002480(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_17 rax_228 = fn0000000000002650(null, 0x05);
		if (rax_228 == 0x00 || fn00000000000023F0(0x03, "en_", rax_228) == 0x00)
		{
			fn0000000000002660(fn0000000000002480(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003313:
			fn0000000000002660(fn0000000000002480(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000030AE:
			fn00000000000026A0(edi);
		}
		r13_199 = 0x700B;
	}
	fn0000000000002560(stdout, fn0000000000002480(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000032D6:
	fn0000000000002660(fn0000000000002480(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003313;
}