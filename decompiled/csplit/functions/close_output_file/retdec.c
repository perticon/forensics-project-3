void close_output_file(void) {
    int64_t v1 = __readfsqword(40); // 0x3e64
    if (output_stream == NULL) {
        goto lab_0x3ebb;
    } else {
        // 0x3e7c
        if ((*(char *)&output_stream & 32) != 0) {
            // 0x3fda
            quotearg_style();
            function_2690();
            goto lab_0x3fc1;
        } else {
            // 0x3e85
            if (rpl_fclose((struct _IO_FILE *)output_stream) != 0) {
                // 0x3f9a
                quotearg_n_style_colon();
                function_25c0();
                goto lab_0x3fc1;
            } else {
                // 0x3e92
                if (bytes_written != 0) {
                    goto lab_0x3ea7;
                } else {
                    // 0x3e9e
                    if (*(char *)&elide_empty_files != 0) {
                        // 0x3f10
                        function_2560();
                        int64_t v2 = function_25d0(); // 0x3f2c
                        int32_t v3 = *(int32_t *)function_25c0(); // 0x3f42
                        files_created--;
                        function_2560();
                        if ((int32_t)v2 != 0 && v3 != 2) {
                            // 0x3f6b
                            quotearg_n_style_colon();
                            function_2920();
                        }
                        // 0x3eb0
                        *(int64_t *)&output_stream = 0;
                        goto lab_0x3ebb;
                    } else {
                        goto lab_0x3ea7;
                    }
                }
            }
        }
    }
  lab_0x3ebb:
    // 0x3ebb
    if (v1 == __readfsqword(40)) {
        // 0x3ed2
        return;
    }
    // 0x3f95
    function_26c0();
    // 0x3f9a
    quotearg_n_style_colon();
    function_25c0();
    goto lab_0x3fc1;
  lab_0x3fc1:
    // 0x3fc1
    function_2920();
    *(int64_t *)&output_stream = 0;
    cleanup_fatal();
    // 0x3fda
    quotearg_style();
    function_2690();
    goto lab_0x3fc1;
  lab_0x3ea7:
    // 0x3ea7
    if (*(char *)&suppress_count == 0) {
        // 0x3ee0
        int64_t v4; // bp-72, 0x3e50
        imaxtostr(bytes_written, (char *)&v4);
        function_29d0();
    }
    // 0x3eb0
    *(int64_t *)&output_stream = 0;
    goto lab_0x3ebb;
}