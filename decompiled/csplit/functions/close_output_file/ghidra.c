void close_output_file(void)

{
  int iVar1;
  int iVar2;
  undefined8 uVar3;
  int *piVar4;
  char *pcVar5;
  long in_FS_OFFSET;
  sigset_t sStack200;
  undefined local_48 [24];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if (output_stream != (byte *)0x0) {
    if ((*output_stream & 0x20) != 0) goto LAB_00103fda;
    iVar1 = rpl_fclose();
    if (iVar1 != 0) {
      uVar3 = quotearg_n_style_colon(0,3,output_filename);
      piVar4 = __errno_location();
      pcVar5 = "%s";
      iVar1 = *piVar4;
      do {
        error(0,iVar1,pcVar5,uVar3);
        output_stream = (byte *)0x0;
        cleanup_fatal();
LAB_00103fda:
        uVar3 = quotearg_style(4,output_filename);
        pcVar5 = (char *)dcgettext(0,"write error for %s",5);
        iVar1 = 0;
      } while( true );
    }
    if ((bytes_written == 0) && (elide_empty_files != '\0')) {
      sigprocmask(0,(sigset_t *)caught_signals,&sStack200);
      pcVar5 = output_filename;
      iVar2 = unlink(output_filename);
      piVar4 = __errno_location();
      iVar1 = *piVar4;
      files_created = files_created + -1;
      sigprocmask(2,&sStack200,(sigset_t *)0x0);
      if ((iVar2 != 0) && (iVar1 != 2)) {
        uVar3 = quotearg_n_style_colon(0,3,pcVar5);
        error(0,iVar1,"%s",uVar3);
      }
    }
    else if (suppress_count == '\0') {
      uVar3 = imaxtostr(bytes_written,local_48);
      __fprintf_chk(stdout,1,"%s\n",uVar3);
    }
    output_stream = (byte *)0x0;
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}