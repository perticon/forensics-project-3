void close_output_file(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    struct s1* rdi7;
    void** rax8;
    void* rax9;
    int64_t rax10;
    void** rsp11;
    void** rax12;
    void*** rax13;
    void** rcx14;
    void** rdx15;
    void** rsi16;
    void** rsi17;
    void** rax18;
    void** rax19;
    void** rdi20;
    int1_t zf21;
    int1_t zf22;
    void** rax23;
    void** rdi24;
    int64_t v25;
    int64_t v26;
    int64_t v27;
    void** v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    void** v34;
    void** r12_35;
    int32_t eax36;
    void*** rax37;
    void** r13d38;
    uint32_t eax39;

    rdi7 = output_stream;
    rax8 = g28;
    if (!rdi7) {
        addr_3ebb_2:
        rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
        if (rax9) {
            fun_26c0();
        } else {
            return;
        }
    } else {
        if (rdi7->f0 & 32) 
            goto addr_3fda_6;
        rax10 = rpl_fclose(rdi7, rsi);
        rsp11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0xa8) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax10)) 
            goto addr_3e92_8;
    }
    rax12 = quotearg_n_style_colon();
    rax13 = fun_25c0();
    rcx14 = rax12;
    rdx15 = reinterpret_cast<void**>("%s");
    rsi16 = *rax13;
    *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
    while (1) {
        fun_2920();
        output_stream = reinterpret_cast<struct s1*>(0);
        cleanup_fatal(0, rsi16, rdx15, rcx14, r8, r9);
        addr_3fda_6:
        rsi17 = output_filename;
        rax18 = quotearg_style(4, rsi17, 4, rsi17);
        rax19 = fun_2690();
        rcx14 = rax18;
        rsi16 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
        rdx15 = rax19;
    }
    addr_3e92_8:
    rdi20 = bytes_written;
    if (rdi20 || (zf21 = elide_empty_files == 0, zf21)) {
        zf22 = suppress_count == 0;
        if (zf22) {
            rax23 = imaxtostr(rdi20, rsp11 + 0x80);
            rdi24 = stdout;
            fun_29d0(rdi24, 1, "%s\n", rax23, r8, r9, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34);
        }
    } else {
        fun_2560();
        r12_35 = output_filename;
        eax36 = fun_25d0(r12_35, 0x1f120, rsp11, rcx);
        rax37 = fun_25c0();
        r13d38 = *rax37;
        eax39 = files_created;
        files_created = eax39 - 1;
        fun_2560();
        if (eax36 && r13d38 != 2) {
            quotearg_n_style_colon();
            fun_2920();
        }
    }
    output_stream = reinterpret_cast<struct s1*>(0);
    goto addr_3ebb_2;
}