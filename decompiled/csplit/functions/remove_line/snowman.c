void** remove_line(void** rdi, void** rsi, void** rdx, ...) {
    void** rdi4;
    void** rax5;
    int1_t zf6;
    signed char al7;
    void** rdx8;
    int1_t less_or_equal9;
    void** rcx10;
    void** rdx11;
    void** r8_12;
    void** rdx13;

    rdi4 = prev_buf_0;
    if (rdi4) {
        free_buffer(rdi4, rsi, rdx);
        prev_buf_0 = reinterpret_cast<void**>(0);
    }
    rax5 = head;
    if (!rax5) {
        zf6 = have_read_eof == 0;
        if (!zf6 || (al7 = load_buffer_part_0(rdi4), al7 == 0)) {
            return 0;
        } else {
            rax5 = head;
        }
    }
    rdx8 = *reinterpret_cast<void***>(rax5 + 24);
    less_or_equal9 = reinterpret_cast<signed char>(rdx8) <= reinterpret_cast<signed char>(current_line);
    if (!less_or_equal9) {
        current_line = rdx8;
    }
    rcx10 = *reinterpret_cast<void***>(rax5 + 56);
    *reinterpret_cast<void***>(rax5 + 24) = rdx8 + 1;
    rdx11 = *reinterpret_cast<void***>(rcx10 + 16) + 1;
    *reinterpret_cast<void***>(rcx10 + 16) = rdx11;
    r8_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx11) << 4) + 8);
    if (rdx11 != *reinterpret_cast<void***>(rcx10) || (rdx13 = *reinterpret_cast<void***>(rcx10 + 0x518), *reinterpret_cast<void***>(rax5 + 56) = rdx13, !!rdx13) && *reinterpret_cast<void***>(rdx13)) {
        return r8_12;
    } else {
        prev_buf_0 = rax5;
        head = *reinterpret_cast<void***>(rax5 + 64);
        return r8_12;
    }
}