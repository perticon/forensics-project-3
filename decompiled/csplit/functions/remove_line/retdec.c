int32_t * remove_line(void) {
    int32_t * v1 = g39; // 0x4764
    if (v1 != NULL) {
        // 0x4770
        free_buffer(v1);
        g39 = NULL;
    }
    int32_t * v2 = head; // 0x478a
    if (head == NULL) {
        // 0x47e0
        if (*(char *)&have_read_eof != 0 || (char)load_buffer_part_0() == 0) {
            // 0x4830
            return NULL;
        }
        // 0x47f2
        v2 = head;
    }
    int64_t v3 = (int64_t)v2;
    int64_t * v4 = (int64_t *)(v3 + 24); // 0x478c
    int64_t v5 = *v4; // 0x478c
    if (v5 > current_line) {
        // 0x47d0
        current_line = v5;
    }
    int64_t v6 = *(int64_t *)(v3 + 56); // 0x4799
    *v4 = v5 + 1;
    int64_t * v7 = (int64_t *)(v6 + 16); // 0x47a5
    int64_t v8 = *v7 + 1; // 0x47a9
    *v7 = v8;
    int64_t v9 = v6 + 8 + 16 * v8; // 0x47b8
    if (v8 != *(int64_t *)v6) {
        // 0x47c2
        return (int32_t *)v9;
    }
    int64_t v10 = *(int64_t *)(v6 + (int64_t)&g55); // 0x4800
    g43 = v10;
    if (v10 == 0) {
        // 0x4816
        g39 = v2;
        *(int64_t *)&head = g44;
        return (int32_t *)v9;
    }
    // 0x4810
    if (*(int64_t *)v10 != 0) {
        // 0x47c2
        return (int32_t *)v9;
    }
    // 0x4816
    g39 = v2;
    *(int64_t *)&head = g44;
    return (int32_t *)v9;
}