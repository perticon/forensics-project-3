long * remove_line(void)

{
  long lVar1;
  long *plVar2;
  long *plVar3;
  long lVar4;
  char cVar5;
  
  if (prev_buf_0 != 0) {
    free_buffer();
    prev_buf_0 = 0;
  }
  if ((head == 0) && ((have_read_eof != '\0' || (cVar5 = load_buffer_part_0(), cVar5 == '\0')))) {
    return (long *)0x0;
  }
  lVar4 = head;
  lVar1 = *(long *)(head + 0x18);
  if (current_line < lVar1) {
    current_line = lVar1;
  }
  plVar2 = *(long **)(head + 0x38);
  *(long *)(head + 0x18) = lVar1 + 1;
  lVar1 = plVar2[2] + 1;
  plVar2[2] = lVar1;
  if (lVar1 == *plVar2) {
    plVar3 = (long *)plVar2[0xa3];
    *(long **)(lVar4 + 0x38) = plVar3;
    if ((plVar3 == (long *)0x0) || (*plVar3 == 0)) {
      prev_buf_0 = lVar4;
      head = *(undefined8 *)(lVar4 + 0x40);
      return plVar2 + lVar1 * 2 + 1;
    }
  }
  return plVar2 + lVar1 * 2 + 1;
}