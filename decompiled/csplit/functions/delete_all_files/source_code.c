delete_all_files (bool in_signal_handler)
{
  if (! remove_files)
    return;

  for (int i = files_created; 0 <= --i; )
    {
      char const *name = make_filename (i);
      if (unlink (name) != 0 && errno != ENOENT && !in_signal_handler)
        error (0, errno, "%s", quotef (name));
    }

  files_created = 0;
}