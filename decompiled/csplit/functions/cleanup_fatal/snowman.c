void cleanup_fatal(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rsp7;
    void* rsp8;
    uint32_t eax9;
    struct s1* rdi10;
    void** rax11;
    void* rax12;
    int64_t v13;
    int64_t rax14;
    void** rsp15;
    void** rax16;
    void*** rax17;
    void** rcx18;
    void** rdx19;
    void** rsi20;
    void** rsi21;
    void** rax22;
    void** rax23;
    void** rdi24;
    int1_t zf25;
    int1_t zf26;
    void** rax27;
    void** rdi28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    void** v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;
    void** v38;
    void** r12_39;
    int32_t eax40;
    void*** rax41;
    void** r13d42;
    uint32_t eax43;

    rsp7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0x90);
    close_output_file(rdi, rsi, rdx, rcx, r8, r9);
    fun_2560();
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp7 - 8) + 8 - 8 + 8);
    eax9 = remove_files;
    if (*reinterpret_cast<signed char*>(&eax9)) {
        delete_all_files_part_0(0, 0x1f120, rsp7);
        rsp8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp8) - 8 + 8);
    }
    fun_2560();
    fun_29b0();
    rdi10 = output_stream;
    rax11 = g28;
    if (!rdi10) {
        addr_3ebb_5:
        rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax11) - reinterpret_cast<unsigned char>(g28));
        if (rax12) {
            fun_26c0();
        } else {
            goto v13;
        }
    } else {
        if (rdi10->f0 & 32) 
            goto addr_3fda_9;
        rax14 = rpl_fclose(rdi10, rsp7);
        rsp15 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(rsp8) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 0xa8) - 8 + 8);
        if (!*reinterpret_cast<int32_t*>(&rax14)) 
            goto addr_3e92_11;
    }
    rax16 = quotearg_n_style_colon();
    rax17 = fun_25c0();
    rcx18 = rax16;
    rdx19 = reinterpret_cast<void**>("%s");
    rsi20 = *rax17;
    *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
    while (1) {
        fun_2920();
        output_stream = reinterpret_cast<struct s1*>(0);
        cleanup_fatal(0, rsi20, rdx19, rcx18, r8, r9);
        addr_3fda_9:
        rsi21 = output_filename;
        rax22 = quotearg_style(4, rsi21, 4, rsi21);
        rax23 = fun_2690();
        rcx18 = rax22;
        rsi20 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
        rdx19 = rax23;
    }
    addr_3e92_11:
    rdi24 = bytes_written;
    if (rdi24 || (zf25 = elide_empty_files == 0, zf25)) {
        zf26 = suppress_count == 0;
        if (zf26) {
            rax27 = imaxtostr(rdi24, rsp15 + 0x80);
            rdi28 = stdout;
            fun_29d0(rdi28, 1, "%s\n", rax27, r8, r9, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
        }
    } else {
        fun_2560();
        r12_39 = output_filename;
        eax40 = fun_25d0(r12_39, 0x1f120, rsp15, rcx);
        rax41 = fun_25c0();
        r13d42 = *rax41;
        eax43 = files_created;
        files_created = eax43 - 1;
        fun_2560();
        if (eax40 && r13d42 != 2) {
            quotearg_n_style_colon();
            fun_2920();
        }
    }
    output_stream = reinterpret_cast<struct s1*>(0);
    goto addr_3ebb_5;
}