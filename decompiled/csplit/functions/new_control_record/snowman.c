void** new_control_record(void** rdi, void** rsi) {
    int64_t rax3;
    int1_t zf4;
    void** rdi5;
    void** rax6;
    void** rax7;

    rax3 = control_used;
    zf4 = rax3 == control_allocated_2;
    rdi5 = controls;
    if (zf4) {
        rax6 = xpalloc();
        controls = rax6;
        rdi5 = rax6;
        rax3 = control_used;
    }
    control_used = rax3 + 1;
    rax7 = reinterpret_cast<void**>((rax3 + rax3 * 2 << 5) + reinterpret_cast<unsigned char>(rdi5));
    *reinterpret_cast<signed char*>(rax7 + 30) = 0;
    *reinterpret_cast<void***>(rax7 + 16) = reinterpret_cast<void**>(0);
    *reinterpret_cast<signed char*>(rax7 + 28) = 0;
    *reinterpret_cast<void***>(rax7 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(0);
    return rax7;
}