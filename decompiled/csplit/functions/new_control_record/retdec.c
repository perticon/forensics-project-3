int32_t * new_control_record(void) {
    int64_t v1 = (int64_t)controls; // 0x3c09
    if (control_used == g40) {
        // 0x3c48
        v1 = (int64_t)xpalloc((char *)controls, &g40, 1, -1, 96);
        *(int64_t *)&controls = v1;
    }
    int64_t v2 = control_used;
    control_used = v2 + 1;
    int64_t v3 = 96 * v2 + v1; // 0x3c1e
    *(char *)(v3 + 30) = 0;
    *(int64_t *)(v3 + 16) = 0;
    *(char *)(v3 + 28) = 0;
    *(int64_t *)(v3 + 8) = 0;
    *(int64_t *)v3 = 0;
    return (int32_t *)v3;
}