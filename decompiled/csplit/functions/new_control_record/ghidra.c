void new_control_record(void)

{
  undefined8 *puVar1;
  
  if (control_used == control_allocated_2) {
    controls = xpalloc(controls,&control_allocated_2,1,0xffffffffffffffff,0x60);
  }
  puVar1 = (undefined8 *)(control_used * 0x60 + controls);
  control_used = control_used + 1;
  *(undefined *)((long)puVar1 + 0x1e) = 0;
  puVar1[2] = 0;
  *(undefined *)((long)puVar1 + 0x1c) = 0;
  puVar1[1] = 0;
  *puVar1 = 0;
  return;
}