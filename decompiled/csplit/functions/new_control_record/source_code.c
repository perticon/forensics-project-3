new_control_record (void)
{
  static idx_t control_allocated = 0; /* Total space allocated. */
  struct control *p;

  if (control_used == control_allocated)
    controls = xpalloc (controls, &control_allocated, 1, -1, sizeof *controls);
  p = &controls[control_used++];
  p->regexpr = false;
  p->repeat = 0;
  p->repeat_forever = false;
  p->lines_required = 0;
  p->offset = 0;
  return p;
}