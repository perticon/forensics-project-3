max_out (char *format)
{
  bool percent = false;

  for (char *f = format; *f; f++)
    if (*f == '%' && *++f != '%')
      {
        if (percent)
          die (EXIT_FAILURE, 0,
               _("too many %% conversion specifications in suffix"));
        percent = true;
        int flags;
        f += get_format_flags (f, &flags);
        while (ISDIGIT (*f))
          f++;
        if (*f == '.')
          while (ISDIGIT (*++f))
            continue;
        check_format_conv_type (f, flags);
      }

  if (! percent)
    die (EXIT_FAILURE, 0,
         _("missing %% conversion specification in suffix"));

  int maxlen = snprintf (NULL, 0, format, INT_MAX);
  if (maxlen < 0)
    xalloc_die ();
  return maxlen;
}