void free_buffer(void** rdi, void** rsi, void** rdx) {
    void** rbp4;
    void** rbx5;
    void** rdi6;
    void** rdi7;

    rbp4 = rdi;
    rbx5 = *reinterpret_cast<void***>(rdi + 48);
    if (rbx5) {
        do {
            rdi6 = rbx5;
            rbx5 = *reinterpret_cast<void***>(rbx5 + 0x518);
            fun_2590(rdi6);
        } while (rbx5);
    }
    rdi7 = *reinterpret_cast<void***>(rbp4 + 40);
    fun_2590(rdi7);
}