free_buffer (struct buffer_record *buf)
{
  for (struct line *l = buf->line_start; l;)
    {
      struct line *n = l->next;
      free (l);
      l = n;
    }
  free (buf->buffer);
  free (buf);
}