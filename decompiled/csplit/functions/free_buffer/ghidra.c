void free_buffer(void *param_1)

{
  void *pvVar1;
  void *__ptr;
  
  __ptr = *(void **)((long)param_1 + 0x30);
  while (__ptr != (void *)0x0) {
    pvVar1 = *(void **)((long)__ptr + 0x518);
    free(__ptr);
    __ptr = pvVar1;
  }
  free(*(void **)((long)param_1 + 0x28));
  free(param_1);
  return;
}