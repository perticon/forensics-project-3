parse_repeat_count (int argnum, struct control *p, char *str)
{
  char *end;

  end = str + strlen (str) - 1;
  if (*end != '}')
    die (EXIT_FAILURE, 0, _("%s: '}' is required in repeat count"),
         quote (str));
  *end = '\0';

  if (str + 1 == end - 1 && *(str + 1) == '*')
    p->repeat_forever = true;
  else
    {
      uintmax_t val;
      if (xstrtoumax (str + 1, NULL, 10, &val, "") != LONGINT_OK
          || INTMAX_MAX < val)
        {
          die (EXIT_FAILURE, 0,
               _("%s}: integer required between '{' and '}'"),
               quote (global_argv[argnum]));
        }
      p->repeat = val;
    }

  *end = '}';
}