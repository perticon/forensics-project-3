load_buffer (void)
{
  struct buffer_record *b;
  idx_t bytes_wanted = START_SIZE; /* Minimum buffer size. */
  idx_t bytes_avail;		/* Size of new buffer created. */
  idx_t lines_found;		/* Number of lines in this new buffer. */
  char *p;			/* Place to load into buffer. */

  if (have_read_eof)
    return false;

  /* We must make the buffer at least as large as the amount of data
     in the partial line left over from the last call,
     plus room for a sentinel '\n'. */
  if (bytes_wanted <= hold_count)
    bytes_wanted = hold_count + 1;

  while (true)
    {
      b = get_new_buffer (bytes_wanted);
      bytes_avail = b->bytes_alloc; /* Size of buffer returned. */
      p = b->buffer;

      /* First check the 'holding' area for a partial line. */
      if (hold_count)
        {
          memcpy (p, hold_area, hold_count);
          p += hold_count;
          b->bytes_used += hold_count;
          bytes_avail -= hold_count;
          hold_count = 0;
        }

      b->bytes_used += read_input (p, bytes_avail - 1);

      lines_found = record_line_starts (b);

      if (lines_found || have_read_eof)
        break;

      if (INT_MULTIPLY_WRAPV (b->bytes_alloc, 2, &bytes_wanted))
        xalloc_die ();
      free_buffer (b);
    }

  if (lines_found)
    save_buffer (b);
  else
    free_buffer (b);

  return lines_found != 0;
}