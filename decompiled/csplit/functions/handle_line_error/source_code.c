handle_line_error (const struct control *p, intmax_t repetition)
{
  char buf[INT_BUFSIZE_BOUND (intmax_t)];

  fprintf (stderr, _("%s: %s: line number out of range"),
           program_name, quote (imaxtostr (p->lines_required, buf)));
  if (repetition)
    fprintf (stderr, _(" on repetition %s\n"), imaxtostr (repetition, buf));
  else
    fprintf (stderr, "\n");

  cleanup_fatal ();
}