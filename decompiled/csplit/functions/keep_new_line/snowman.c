void keep_new_line(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rcx5;
    void** rdx6;
    void** rbx7;
    void** rax8;
    void** rax9;
    void** rcx10;
    struct s3* rax11;

    if (!*reinterpret_cast<void***>(rdi + 48)) {
        rax4 = xmalloc(0x520, rsi);
        *reinterpret_cast<int32_t*>(&rcx5) = 1;
        *reinterpret_cast<int32_t*>(&rcx5 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx6) = 1;
        *reinterpret_cast<int32_t*>(&rdx6 + 4) = 0;
        *reinterpret_cast<void***>(rax4 + 0x518) = reinterpret_cast<void**>(0);
        rbx7 = rax4;
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 8) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 16) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdi + 56) = rax4;
        *reinterpret_cast<void***>(rdi + 48) = rax4;
        *reinterpret_cast<int32_t*>(&rax8) = 0;
        *reinterpret_cast<int32_t*>(&rax8 + 4) = 0;
    } else {
        rbx7 = *reinterpret_cast<void***>(rdi + 56);
        if (*reinterpret_cast<void***>(rbx7) == 80) {
            rax9 = xmalloc(0x520, rsi);
            *reinterpret_cast<void***>(rax9 + 0x518) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax9 + 8) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax9 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rbx7 + 0x518) = rax9;
            rbx7 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 56) + 0x518);
            rcx10 = *reinterpret_cast<void***>(rbx7);
            rax8 = *reinterpret_cast<void***>(rbx7 + 8);
            *reinterpret_cast<void***>(rdi + 56) = rbx7;
            rdx6 = rcx10 + 1;
            rcx5 = rax8 + 1;
        } else {
            rax8 = *reinterpret_cast<void***>(rbx7 + 8);
            rdx6 = *reinterpret_cast<void***>(rbx7) + 1;
            rcx5 = rax8 + 1;
        }
    }
    rax11 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax8) << 4) + reinterpret_cast<unsigned char>(rbx7));
    rax11->f20 = rsi;
    rax11->f18 = rdx;
    *reinterpret_cast<void***>(rbx7) = rdx6;
    *reinterpret_cast<void***>(rbx7 + 8) = rcx5;
    return;
}