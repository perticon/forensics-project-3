void keep_new_line(int32_t * b, char * line_start, int64_t line_len) {
    int64_t v1 = (int64_t)b;
    int64_t * v2 = (int64_t *)(v1 + 48); // 0x42b3
    int64_t * v3; // 0x42a0
    int64_t * v4; // 0x42a0
    int64_t v5; // 0x42a0
    int64_t v6; // 0x42a0
    int64_t v7; // 0x42a0
    int64_t v8; // 0x42a0
    if (*v2 == 0) {
        int64_t v9 = xmalloc(); // 0x435d
        *(int64_t *)(v9 + (int64_t)&g55) = 0;
        int64_t * v10 = (int64_t *)v9;
        *v10 = 0;
        int64_t * v11 = (int64_t *)(v9 + 8);
        *v11 = 0;
        *(int64_t *)(v9 + 16) = 0;
        *(int64_t *)(v1 + 56) = v9;
        *v2 = v9;
        v3 = v11;
        v4 = v10;
        v5 = 0;
        v7 = 1;
        v8 = 1;
        v6 = v9;
    } else {
        int64_t * v12 = (int64_t *)(v1 + 56); // 0x42be
        int64_t v13 = *v12; // 0x42be
        int64_t * v14 = (int64_t *)v13;
        int64_t v15 = *v14; // 0x42c2
        if (v15 == 80) {
            int64_t v16 = xmalloc(); // 0x4305
            *(int64_t *)(v16 + (int64_t)&g55) = 0;
            *(int64_t *)v16 = 0;
            *(int64_t *)(v16 + 8) = 0;
            *(int64_t *)(v16 + 16) = 0;
            *(int64_t *)(v13 + (int64_t)&g55) = v16;
            int64_t v17 = *(int64_t *)(*v12 + (int64_t)&g55); // 0x4337
            int64_t * v18 = (int64_t *)v17;
            int64_t * v19 = (int64_t *)(v17 + 8);
            int64_t v20 = *v19; // 0x4341
            *v12 = v17;
            v3 = v19;
            v4 = v18;
            v5 = v20;
            v7 = v20 + 1;
            v8 = *v18 + 1;
            v6 = v17;
        } else {
            int64_t * v21 = (int64_t *)(v13 + 8);
            int64_t v22 = *v21; // 0x42cb
            v3 = v21;
            v4 = v14;
            v5 = v22;
            v7 = v22 + 1;
            v8 = v15 + 1;
            v6 = v13;
        }
    }
    int64_t v23 = v6 + 16 * v5; // 0x42db
    *(int64_t *)(v23 + 32) = (int64_t)line_start;
    *(int64_t *)(v23 + 24) = line_len;
    *v4 = v8;
    *v3 = v7;
}