undefined8 main(uint param_1,undefined8 *param_2)

{
  byte *pbVar1;
  sigaction *psVar2;
  sigaction **ppsVar3;
  bool bVar4;
  char cVar5;
  int iVar6;
  int iVar7;
  ushort **ppuVar8;
  char *pcVar9;
  ulong uVar10;
  size_t sVar11;
  int *piVar12;
  undefined8 uVar13;
  undefined8 uVar14;
  byte bVar15;
  byte bVar16;
  byte *pbVar17;
  uint uVar18;
  uint extraout_EDX;
  ulong uVar19;
  undefined1 *puVar20;
  undefined1 *unaff_RBP;
  undefined4 *puVar21;
  long lVar22;
  undefined4 *puVar23;
  long lVar24;
  uint uVar25;
  uint *puVar26;
  char *pcVar27;
  undefined1 *__oact;
  sigaction **__s;
  long in_FS_OFFSET;
  byte bVar28;
  undefined auVar29 [16];
  undefined8 uVar30;
  undefined8 uStack296;
  ulong local_120;
  char *local_118;
  uint local_10c;
  ulong local_100;
  undefined local_f8 [136];
  int local_70;
  long local_40;
  
  bVar28 = 0;
  __s = (sigaction **)0x119749;
  __oact = longopts;
  local_120 = local_120 & 0xffffffff00000000 | (ulong)param_1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar26 = &switchD_00102bc5::switchdataD_00118b40;
  textdomain("coreutils");
  atexit(close_stdout);
  remove_files = 1;
  controls = 0;
  control_used = 0;
  suppress_count = 0;
  suppress_matched = '\0';
  prefix = &DAT_00118136;
  global_argv = param_2;
  while( true ) {
    uVar30 = 0x102b95;
    iVar6 = getopt_long(local_120 & 0xffffffff,param_2,"f:b:kn:sqz",longopts,0);
    if (iVar6 == -1) break;
    if (0x80 < iVar6) goto switchD_00102bc5_caseD_63;
    if (iVar6 < 0x62) {
      if (iVar6 == -0x83) {
        version_etc(stdout,"csplit","GNU coreutils",Version,"Stuart Kemp","David MacKenzie",0,uVar30
                   );
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar6 != -0x82) goto switchD_00102bc5_caseD_63;
      usage(0);
      break;
    }
    switch(iVar6) {
    case 0x62:
      suffix = optarg;
      break;
    default:
      goto switchD_00102bc5_caseD_63;
    case 0x66:
      prefix = optarg;
      break;
    case 0x6b:
      remove_files = 0;
      break;
    case 0x6e:
      uVar30 = dcgettext(0,"invalid number",5);
      digits = xdectoimax(optarg,0,0x7fffffff,"",uVar30,0);
      break;
    case 0x71:
    case 0x73:
      suppress_count = 1;
      break;
    case 0x7a:
      elide_empty_files = 1;
      break;
    case 0x80:
      suppress_matched = '\x01';
    }
  }
  if ((int)((int)local_120 - optind) < 2) {
    if ((int)optind < (int)local_120) {
      unaff_RBP = (undefined1 *)(long)(int)local_120;
      puVar26 = (uint *)quote(param_2[(long)((long)unaff_RBP + -1)]);
      uVar30 = dcgettext(0,"missing operand after %s",5);
      error(0,0,uVar30,puVar26);
    }
    else {
      uVar30 = dcgettext(0,"missing operand",5);
      error(0,0,uVar30);
    }
switchD_00102bc5_caseD_63:
    usage(1);
  }
  else {
    puVar26 = (uint *)strlen((char *)prefix);
    if (suffix != (byte *)0x0) {
      bVar4 = false;
      lVar24 = 1;
      uVar10 = 0x100800001;
      bVar15 = *suffix;
      pbVar17 = suffix;
      if (bVar15 != 0) {
LAB_00102d48:
        pbVar1 = pbVar17 + 1;
        if ((bVar15 != 0x25) || (pbVar17 = pbVar17 + 1, *pbVar1 == 0x25)) goto LAB_00102d38;
        if (!bVar4) {
          uVar25 = 0;
          do {
            bVar15 = *pbVar17;
            if (bVar15 == 0x27) {
LAB_00102e33:
              uVar25 = uVar25 | 1;
            }
            else if ((char)bVar15 < '(') {
              if (bVar15 != 0x23) {
LAB_00102dbb:
                bVar15 = *pbVar17;
                __oact = (undefined1 *)(ulong)bVar15;
                if (bVar15 == 0) goto LAB_001039ea;
                bVar16 = bVar15 + 0xa8;
                if (bVar16 < 0x21) {
                  uVar19 = lVar24 << (bVar16 & 0x3f);
                  if ((uVar19 & uVar10) != 0) {
                    uVar18 = 2;
                    goto LAB_0010369c;
                  }
                  if ((uVar19 & 0x21000) != 0) {
                    uVar18 = 1;
                    goto LAB_0010369c;
                  }
                  if (bVar16 == 0x1d) goto LAB_00103694;
                }
                ppuVar8 = __ctype_b_loc();
                if ((*(byte *)((long)*ppuVar8 + (long)__oact * 2 + 1) & 0x40) == 0) {
                  uVar30 = dcgettext(0,"invalid conversion specifier in suffix: \\%.3o",5);
                  error(1,0,uVar30,(uint)bVar15);
                  goto LAB_001035cd;
                }
                uVar30 = dcgettext(0,"invalid conversion specifier in suffix: %c",5);
                pbVar17 = (byte *)(ulong)(uint)bVar15;
                uVar25 = 0;
                uVar10 = 1;
                error(1,0,uVar30);
                goto LAB_00102e33;
              }
              uVar25 = uVar25 | 2;
            }
            else if ((bVar15 != 0x2d) && (bVar15 != 0x30)) {
              while ((int)(char)bVar15 - 0x30U < 10) {
                pbVar1 = pbVar17 + 1;
                pbVar17 = pbVar17 + 1;
                bVar15 = *pbVar1;
              }
              if (bVar15 == 0x2e) {
                do {
                  pbVar1 = pbVar17 + 1;
                  pbVar17 = pbVar17 + 1;
                } while ((int)(char)*pbVar1 - 0x30U < 10);
              }
              goto LAB_00102dbb;
            }
            pbVar17 = pbVar17 + 1;
          } while( true );
        }
        goto LAB_00103a0e;
      }
      goto LAB_00103715;
    }
    lVar24 = 0xb;
    if (digits < 0xb) goto LAB_00102e4f;
  }
  lVar24 = (long)(int)digits;
LAB_00102e4f:
  lVar22 = lVar24 + 1;
  lVar24 = lVar22 + (long)puVar26;
  if (!SCARRY8(lVar22,(long)puVar26)) {
    filename_space = ximalloc();
    pcVar27 = (char *)param_2[(int)optind];
    uVar25 = optind + 1;
    optind = uVar25;
    iVar6 = strcmp(pcVar27,"-");
    if ((iVar6 != 0) && (iVar6 = fd_reopen(0,pcVar27,0,0), uVar25 = optind, iVar6 < 0)) {
      uVar30 = quotearg_style(4,pcVar27);
      uVar14 = dcgettext(0,"cannot open %s for reading",5);
      piVar12 = __errno_location();
      error(1,*piVar12,uVar14,uVar30);
      goto LAB_0010367f;
    }
    uVar10 = (ulong)uVar25;
    if ((int)uVar25 < (int)local_120) {
LAB_00102ea8:
      do {
        iVar6 = (int)uVar10;
        lVar24 = (long)iVar6;
        pcVar27 = (char *)(lVar24 * 8);
        __s = (sigaction **)(param_2 + lVar24);
        __oact = (undefined1 *)*__s;
        cVar5 = *(char *)(_union_1454 *)__oact;
        if ((cVar5 == '/') || (cVar5 == '%')) {
          iVar7 = (int)cVar5;
          __s = (sigaction **)((long)(_union_1454 *)__oact + 1);
          local_118 = (char *)((ulong)local_118 & 0xffffffffffffff00 | (ulong)(cVar5 == '%'));
          pcVar9 = strrchr((char *)__s,iVar7);
          if (pcVar9 == (char *)0x0) goto LAB_001037d9;
          local_10c = local_10c & 0xffffff00 | (uint)(byte)local_118;
          local_118 = pcVar9;
          unaff_RBP = (undefined1 *)new_control_record();
          *(int *)((long)unaff_RBP + 0x18) = iVar6;
          *(char *)((long)unaff_RBP + 0x1d) = (char)local_10c;
          *(char *)((long)unaff_RBP + 0x1e) = '\x01';
          *(undefined8 *)((long)unaff_RBP + 0x20) = 0;
          *(undefined8 *)((long)unaff_RBP + 0x28) = 0;
          uVar30 = xmalloc(0x100);
          *(undefined8 *)((long)unaff_RBP + 0x40) = uVar30;
          *(undefined8 *)((long)unaff_RBP + 0x48) = 0;
          rpl_re_syntax_options = 0x2c6;
          __s = (sigaction **)
                rpl_re_compile_pattern
                          (__s,local_118 + (-1 - (long)__oact),(int *)((long)unaff_RBP + 0x20));
          if (__s != (sigaction **)0x0) goto LAB_00103778;
          if ((local_118[1] == '\0') ||
             (iVar7 = xstrtoimax(local_118 + 1,0,10,unaff_RBP,""), iVar7 == 0)) goto LAB_00102f33;
          uVar10 = quote(__oact);
          uVar30 = dcgettext(0,"%s: integer expected after delimiter",5);
          error(1,0,uVar30,uVar10);
        }
        else {
          unaff_RBP = (undefined1 *)new_control_record();
          psVar2 = *__s;
          *(int *)((long)unaff_RBP + 0x18) = iVar6;
          iVar7 = xstrtoumax(psVar2,0,10,&local_100,"");
          if ((iVar7 != 0) || ((long)local_100 < 0)) goto LAB_00103739;
          if (local_100 == 0) goto LAB_001037af;
          if (local_100 < last_val_3) goto LAB_00103803;
          if (local_100 == last_val_3) {
            uVar30 = quote(*__s);
            uVar14 = dcgettext(0,"warning: line number %s is the same as preceding line number",5);
            error(0,0,uVar14,uVar30);
          }
          last_val_3 = local_100;
          *(ulong *)((long)unaff_RBP + 8) = local_100;
LAB_00102f33:
          if ((int)local_120 <= (int)(iVar6 + 1U)) break;
          __oact = pcVar27 + 8;
          pcVar27 = (char *)(param_2 + 1)[lVar24];
          if (*pcVar27 != '{') {
            uVar10 = (ulong)(iVar6 + 1U);
            goto LAB_00102ea8;
          }
        }
        sVar11 = strlen(pcVar27);
        pcVar9 = pcVar27 + (sVar11 - 1);
        if (*pcVar9 != '}') {
          uVar30 = quote(pcVar27);
          uVar14 = dcgettext(0,"%s: \'}\' is required in repeat count",5);
          error(1,0,uVar14,uVar30);
          uVar25 = extraout_EDX;
LAB_0010396b:
          uVar30 = dcgettext(0,"invalid flags in conversion specification: %%%c%c",5);
          error(1,0,uVar30,(-((uVar25 & 2) == 0) & 4U) + 0x23,(ulong)__oact & 0xffffffff);
          goto LAB_001039a6;
        }
        *pcVar9 = '\0';
        if ((pcVar27 + 1 == pcVar9 + -1) && (pcVar27[1] == '*')) {
          *(char *)((long)unaff_RBP + 0x1c) = '\x01';
        }
        else {
          iVar6 = xstrtoumax(pcVar27 + 1,0,10,&local_100);
          if (iVar6 != 0) goto LAB_00103889;
          if ((long)local_100 < 0) goto LAB_00103889;
          *(ulong *)((long)unaff_RBP + 0x10) = local_100;
        }
        *pcVar9 = '}';
        uVar25 = (int)uVar10 + 2;
        uVar10 = (ulong)uVar25;
      } while ((int)uVar25 < (int)local_120);
    }
    unaff_RBP = sig_4;
    __oact = local_f8;
    puVar20 = sig_4;
    sigemptyset((sigset_t *)caught_signals);
    do {
      iVar6 = *(int *)puVar20;
      sigaction(iVar6,(sigaction *)0x0,(sigaction *)__oact);
      if (local_f8._0_8_ != (code *)0x1) {
        sigaddset((sigset_t *)caught_signals,iVar6);
      }
      puVar20 = (undefined1 *)((long)puVar20 + 4);
    } while (puVar20 != "9.1.17-a351f");
    puVar21 = (undefined4 *)caught_signals;
    puVar23 = (undefined4 *)(local_f8 + 8);
    for (lVar24 = 0x20; lVar24 != 0; lVar24 = lVar24 + -1) {
      *puVar23 = *puVar21;
      puVar21 = puVar21 + (ulong)bVar28 * -2 + 1;
      puVar23 = puVar23 + (ulong)bVar28 * -2 + 1;
    }
    local_f8._0_8_ = interrupt_handler;
    local_70 = 0;
    do {
      iVar6 = *(int *)unaff_RBP;
      iVar7 = sigismember((sigset_t *)caught_signals,iVar6);
      if (iVar7 != 0) {
        sigaction(iVar6,(sigaction *)__oact,(sigaction *)0x0);
      }
      unaff_RBP = (undefined1 *)((long)unaff_RBP + 4);
    } while (unaff_RBP != "9.1.17-a351f");
    for (local_118 = (char *)0x0; (long)local_118 < control_used; local_118 = local_118 + 1) {
      local_120 = (long)local_118 * 0x60;
      __oact = (undefined1 *)0x0;
      psVar2 = (sigaction *)__oact;
      if (*(char *)(controls + 0x1e + local_120) == '\0') {
        while ((__oact = (undefined1 *)psVar2, lVar24 = controls + local_120,
               *(char *)(lVar24 + 0x1c) != '\0' || ((long)__oact <= *(long *)(lVar24 + 0x10)))) {
          lVar22 = *(long *)(lVar24 + 8);
          psVar2 = (sigaction *)((long)(_union_1454 *)__oact + 1);
          create_output_file();
          __s = (sigaction **)(lVar22 * (long)psVar2);
          lVar22 = find_line((char *)((long)current_line + 1));
          if ((lVar22 == 0) && (suppress_matched != '\0')) goto LAB_0010376b;
          if ((head == 0) &&
             ((have_read_eof != '\0' || (cVar5 = load_buffer_part_0(), cVar5 == '\0')))) {
            lVar24 = dcgettext(0,"input disappeared",5);
            piVar12 = __errno_location();
            error(1,*piVar12,lVar24);
LAB_0010345a:
            remove_line();
          }
          else {
            unaff_RBP = (undefined1 *)((long)__s + 1);
            lVar22 = *(long *)(head + 0x18);
            piVar12 = (int *)(lVar22 + 1);
            if ((long)__s - lVar22 != 0 && lVar22 <= (long)__s) {
              do {
                lVar22 = remove_line();
                if (lVar22 == 0) goto LAB_0010376b;
                save_line_to_file(lVar22);
                piVar12 = (int *)((long)piVar12 + 1);
              } while (piVar12 != (int *)unaff_RBP);
            }
            close_output_file();
            if (suppress_matched != '\0') goto LAB_0010345a;
          }
          lVar22 = find_line();
          if ((lVar22 == 0) && (suppress_matched == '\0')) goto LAB_0010376b;
        }
      }
      else {
        for (; (puVar26 = (uint *)(controls + local_120), *(char *)((long)puVar26 + 0x1c) != '\0' ||
               ((long)__oact <= *(long *)((long)puVar26 + 0x10)));
            __oact = (undefined1 *)((long)(_union_1454 *)__oact + 1)) {
          bVar28 = *(byte *)((long)puVar26 + 0x1d);
          unaff_RBP = (undefined1 *)(ulong)bVar28;
          if (bVar28 == 0) {
            create_output_file();
          }
          if (*(long *)puVar26 < 0) {
            do {
              current_line = (sigaction **)((long)current_line + 1);
              lVar24 = find_line();
              if (lVar24 == 0) {
                if (*(char *)((long)puVar26 + 0x1c) == '\0') goto LAB_001039d9;
                if (bVar28 == 0) goto LAB_0010332a;
                goto LAB_001032b5;
              }
              lVar24 = rpl_re_search();
              if (lVar24 == -2) goto LAB_001038c3;
            } while (lVar24 == -1);
          }
          else {
            while( true ) {
              current_line = (sigaction **)((long)current_line + 1);
              lVar24 = find_line();
              if (lVar24 == 0) {
                if (*(char *)((long)puVar26 + 0x1c) == '\0') goto LAB_001039d9;
                if (bVar28 == 0) goto LAB_00103583;
                goto LAB_001032b5;
              }
              lVar24 = rpl_re_search();
              if (lVar24 == -2) goto LAB_001038c3;
              if (lVar24 != -1) break;
              uVar30 = remove_line();
              if (bVar28 == 0) {
                save_line_to_file(uVar30);
              }
            }
          }
          __s = (sigaction **)((long)current_line + *(long *)puVar26);
          local_10c = *(uint *)((long)puVar26 + 0x18);
          if (head == 0) {
LAB_001035cd:
            if ((have_read_eof == '\0') && (cVar5 = load_buffer_part_0(), cVar5 != '\0'))
            goto LAB_00103489;
            uVar30 = dcgettext(0,"input disappeared",5);
            piVar12 = __errno_location();
            error(1,*piVar12,uVar30);
LAB_0010360f:
            remove_line();
          }
          else {
LAB_00103489:
            ppsVar3 = *(sigaction ***)(head + 0x18);
            if ((long)__s < (long)ppsVar3) goto LAB_001038e9;
            lVar24 = 0;
            if (__s != ppsVar3) {
              do {
                lVar22 = remove_line();
                if (lVar22 == 0) goto LAB_00103848;
                if ((char)unaff_RBP == '\0') {
                  save_line_to_file();
                }
                lVar24 = lVar24 + 1;
              } while ((long)__s - (long)ppsVar3 != lVar24);
            }
            if ((char)unaff_RBP == '\0') {
              close_output_file();
            }
            if (0 < *(long *)puVar26) {
              current_line = __s;
            }
            if (suppress_matched != '\0') goto LAB_0010360f;
          }
        }
      }
LAB_0010367f:
    }
    create_output_file();
    dump_rest_of_file();
    close_output_file();
    iVar6 = close(0);
    if (iVar6 != 0) {
      puVar26 = (uint *)dcgettext(0,"read error",5);
      piVar12 = __errno_location();
      error(0,*piVar12,puVar26);
      cleanup_fatal();
LAB_001039d9:
      regexp_error_isra_0(*(undefined4 *)((long)puVar26 + 0x18),__oact,(ulong)unaff_RBP & 0xff);
LAB_001039ea:
      uVar30 = dcgettext(0,"missing conversion specifier in suffix",5);
      error(1,0,uVar30);
LAB_00103a0e:
      uVar30 = dcgettext(0,"too many %% conversion specifications in suffix",5);
      auVar29 = error(1,0,uVar30);
      uVar30 = uStack296;
      uStack296 = SUB168(auVar29,0);
      (*(code *)PTR___libc_start_main_0011efc8)
                (main,uVar30,&local_120,0,0,SUB168(auVar29 >> 0x40,0),&uStack296);
      do {
                    /* WARNING: Do nothing block with infinite loop */
      } while( true );
    }
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return 0;
    }
LAB_001039a6:
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
LAB_00103504:
                    /* WARNING: Subroutine does not return */
  xalloc_die(lVar24);
LAB_00103694:
  *pbVar17 = 100;
  uVar18 = 1;
LAB_0010369c:
  uVar25 = ~uVar18 & uVar25;
  if (uVar25 != 0) goto LAB_0010396b;
  bVar4 = true;
LAB_00102d38:
  bVar15 = pbVar17[1];
  pbVar17 = pbVar17 + 1;
  if (bVar15 == 0) goto LAB_001034d6;
  goto LAB_00102d48;
LAB_001034d6:
  if (!bVar4) {
LAB_00103715:
    uVar30 = dcgettext(0,"missing %% conversion specification in suffix",5);
    error(1,0,uVar30);
LAB_00103739:
    lVar24 = quote(*__s);
    uVar30 = dcgettext(0,"%s: invalid pattern",5);
    error(1,0,uVar30,lVar24);
LAB_0010376b:
    handle_line_error_isra_0(*(undefined8 *)(lVar24 + 8),__oact);
LAB_00103778:
    uVar30 = quote(__oact);
    uVar14 = dcgettext(0,"%s: invalid regular expression: %s",5);
    error(0,0,uVar14,uVar30,__s);
    cleanup_fatal();
LAB_001037af:
    iVar7 = (int)unaff_RBP;
    psVar2 = *__s;
    uVar30 = dcgettext(0,"%s: line number must be greater than zero",5);
    error(1,0,uVar30,psVar2);
LAB_001037d9:
    uVar30 = dcgettext(0,"%s: closing delimiter \'%c\' missing",5);
    error(1,0,uVar30,__oact,iVar7);
LAB_00103803:
    uVar30 = imaxtostr();
    uVar14 = quote(*__s);
    uVar13 = dcgettext(0,"line number %s is smaller than preceding line number, %s",5);
    error(1,0,uVar13,uVar14,uVar30);
LAB_00103848:
    uVar30 = global_argv[(int)local_10c];
    do {
      uVar30 = quote(uVar30);
      uVar14 = dcgettext(0,"%s: line number out of range",5);
      error(0,0,uVar14,uVar30);
      cleanup_fatal();
LAB_00103889:
      uVar30 = quote(*(undefined8 *)((long)global_argv + (long)__oact));
      uVar14 = dcgettext(0,"%s}: integer required between \'{\' and \'}\'",5);
      error(1,0,uVar14,uVar30);
LAB_001038c3:
      uVar30 = dcgettext(0,"error in regular expression search",5);
      error(0,0,uVar30);
      cleanup_fatal();
LAB_001038e9:
      uVar30 = global_argv[(int)local_10c];
    } while( true );
  }
  iVar6 = __snprintf_chk(0,0,1,0xffffffffffffffff);
  lVar24 = (long)iVar6;
  if (iVar6 < 0) goto LAB_00103504;
  goto LAB_00102e4f;
LAB_0010332a:
  while (lVar24 = remove_line(), lVar24 != 0) {
    save_line_to_file();
  }
  goto LAB_00103337;
LAB_00103583:
  while (lVar24 = remove_line(), lVar24 != 0) {
    save_line_to_file();
  }
LAB_00103337:
  close_output_file();
LAB_001032b5:
                    /* WARNING: Subroutine does not return */
  exit(0);
}