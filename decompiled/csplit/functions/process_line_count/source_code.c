process_line_count (const struct control *p, intmax_t repetition)
{
  intmax_t linenum;
  intmax_t last_line_to_save = p->lines_required * (repetition + 1);

  create_output_file ();

  /* Ensure that the line number specified is not 1 greater than
     the number of lines in the file.
     When suppressing matched lines, check before the loop. */
  if (no_more_lines () && suppress_matched)
    handle_line_error (p, repetition);

  linenum = get_first_line_in_buffer ();
  while (linenum++ < last_line_to_save)
    {
      struct cstring *line = remove_line ();
      if (line == NULL)
        handle_line_error (p, repetition);
      save_line_to_file (line);
    }

  close_output_file ();

  if (suppress_matched)
    remove_line ();

  /* Ensure that the line number specified is not 1 greater than
     the number of lines in the file. */
  if (no_more_lines () && !suppress_matched)
    handle_line_error (p, repetition);
}