void** make_filename(uint32_t edi, void** rsi, void** rdx, void** rcx) {
    void** rsi5;
    void** rdi6;
    void** rax7;
    uint32_t r12d8;
    void** rbp9;
    void** rdi10;
    void** rax11;
    int64_t r9_12;
    int64_t r8_13;
    void** rax14;
    void** r12_15;
    void** rbp16;
    void** rdi17;
    void** rax18;
    int64_t r8_19;
    int64_t r9_20;
    void** rax21;

    rsi5 = prefix;
    rdi6 = filename_space;
    fun_2600(rdi6, rsi5);
    rax7 = suffix;
    if (!rax7) {
        r12d8 = digits;
        rbp9 = filename_space;
        rdi10 = prefix;
        rax11 = fun_26b0(rdi10, rdi10);
        *reinterpret_cast<uint32_t*>(&r9_12) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_12) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r8_13) = r12d8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_13) + 4) = 0;
        fun_2a50(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(rax11), 1, -1, "%0*d", r8_13, r9_12);
        rax14 = filename_space;
        return rax14;
    } else {
        r12_15 = suffix;
        rbp16 = filename_space;
        rdi17 = prefix;
        rax18 = fun_26b0(rdi17, rdi17);
        *reinterpret_cast<uint32_t*>(&r8_19) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_19) + 4) = 0;
        fun_2a50(reinterpret_cast<unsigned char>(rbp16) + reinterpret_cast<unsigned char>(rax18), 1, -1, r12_15, r8_19, r9_20);
        rax21 = filename_space;
        return rax21;
    }
}