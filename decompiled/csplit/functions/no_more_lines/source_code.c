no_more_lines (void)
{
  return find_line (current_line + 1) == NULL;
}