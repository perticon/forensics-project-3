void save_line_to_file(size_t *param_1)

{
  size_t sVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  int *piVar4;
  long lVar5;
  int iVar6;
  int iVar7;
  long in_FS_OFFSET;
  sigset_t sStack208;
  long lStack72;
  size_t *psStack56;
  
  sVar1 = fwrite_unlocked((void *)param_1[1],1,*param_1,output_stream);
  if (*param_1 == sVar1) {
    bytes_written = bytes_written + *param_1;
    return;
  }
  uVar2 = quotearg_style(4,output_filename);
  uVar3 = dcgettext(0,"write error for %s",5);
  piVar4 = __errno_location();
  error(0,*piVar4,uVar3,uVar2);
  output_stream = (FILE *)0x0;
  cleanup_fatal();
  iVar6 = files_created;
  iVar7 = 0x4b;
  lStack72 = *(long *)(in_FS_OFFSET + 0x28);
  psStack56 = param_1;
  uVar2 = make_filename();
  output_filename = uVar2;
  if (iVar6 != 0x7fffffff) goto LAB_00104110;
  do {
    uVar2 = output_filename;
    uVar3 = quotearg_n_style_colon(0,3,output_filename);
    error(0,iVar7,"%s",uVar3);
    cleanup_fatal();
LAB_00104110:
    sigprocmask(0,(sigset_t *)caught_signals,&sStack208);
    lVar5 = fopen_safer(uVar2,&DAT_0011801c);
    output_stream = (FILE *)lVar5;
    piVar4 = __errno_location();
    iVar6 = (iVar6 + 1) - (uint)(lVar5 == 0);
    iVar7 = *piVar4;
    files_created = iVar6;
    sigprocmask(2,&sStack208,(sigset_t *)0x0);
  } while (lVar5 == 0);
  bytes_written = 0;
  if (lStack72 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  bytes_written = 0;
  return;
}