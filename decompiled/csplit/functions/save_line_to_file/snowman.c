void save_line_to_file(void** rdi, ...) {
    struct s1* rcx2;
    int64_t v3;
    int64_t rbx4;
    void** rdi5;
    void** rdx6;
    void** rax7;
    void** r8_8;
    void** rsi9;
    void** rax10;
    void** rax11;
    void*** rax12;
    void** rcx13;
    void** rsi14;
    void** r9_15;
    void** r12d16;
    void** rax17;
    void** v18;
    uint32_t ebx19;
    void** rax20;
    void* rsp21;
    void** rbp22;
    struct s1* rax23;
    void*** rax24;
    void* rax25;
    void** rax26;
    void** rsi27;
    void** r9_28;
    void** tmp64_29;

    rcx2 = output_stream;
    v3 = rbx4;
    rdi5 = *reinterpret_cast<void***>(rdi + 8);
    rdx6 = *reinterpret_cast<void***>(rdi);
    rax7 = fun_28b0(rdi5, 1, rdx6, rcx2);
    r8_8 = rax7;
    if (*reinterpret_cast<void***>(rdi) != r8_8) {
        rsi9 = output_filename;
        rax10 = quotearg_style(4, rsi9, 4, rsi9);
        rax11 = fun_2690();
        rax12 = fun_25c0();
        rcx13 = rax10;
        rsi14 = *rax12;
        *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
        fun_2920();
        output_stream = reinterpret_cast<struct s1*>(0);
        cleanup_fatal(0, rsi14, rax11, rcx13, r8_8, r9_15);
        r12d16 = reinterpret_cast<void**>(75);
        rax17 = g28;
        v18 = rax17;
        ebx19 = files_created;
        rax20 = make_filename(ebx19, rsi14, rax11, rcx13);
        rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8);
        output_filename = rax20;
        rbp22 = rax20;
        if (ebx19 == 0x7fffffff) 
            goto addr_40e5_4;
        while (1) {
            fun_2560();
            rax23 = fopen_safer(rbp22, "w", rsp21, rcx13);
            output_stream = rax23;
            rax24 = fun_25c0();
            ebx19 = ebx19 - (1 - reinterpret_cast<uint1_t>(ebx19 < 1 - reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax23) < 1)));
            r12d16 = *rax24;
            files_created = ebx19;
            fun_2560();
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            if (rax23) {
                bytes_written = reinterpret_cast<void**>(0);
                rax25 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
                if (!rax25) 
                    break;
                fun_26c0();
                rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
            }
            rbp22 = output_filename;
            addr_40e5_4:
            rax26 = quotearg_n_style_colon();
            rsi27 = r12d16;
            *reinterpret_cast<int32_t*>(&rsi27 + 4) = 0;
            rcx13 = rax26;
            fun_2920();
            cleanup_fatal(0, rsi27, "%s", rcx13, r8_8, r9_28);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        goto v3;
    } else {
        tmp64_29 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(bytes_written) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)));
        bytes_written = tmp64_29;
        return;
    }
}