void save_line_to_file(int32_t * line) {
    int64_t v1 = *(int64_t *)((int64_t)line + 8); // 0x4024
    if (v1 == function_28b0()) {
        // 0x403b
        bytes_written += v1;
        return;
    }
    // 0x4048
    quotearg_style();
    function_2690();
    function_25c0();
    function_2920();
    *(int64_t *)&output_stream = 0;
    cleanup_fatal();
}