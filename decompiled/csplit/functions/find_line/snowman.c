struct s0* find_line(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbp7;
    void** rbx8;
    int1_t zf9;
    signed char al10;
    void** rdx11;
    void** rax12;
    int1_t zf13;
    signed char al14;
    void* rbp15;
    void** rax16;

    rbp7 = rdi;
    rbx8 = head;
    if (rbx8) 
        goto addr_4685_2;
    while ((zf9 = have_read_eof == 0, zf9) && (al10 = load_buffer_part_0(rdi, rdi), !!al10)) {
        rbx8 = head;
        addr_4685_2:
        rdx11 = *reinterpret_cast<void***>(rbx8 + 16);
        if (reinterpret_cast<signed char>(rdx11) > reinterpret_cast<signed char>(rbp7)) 
            goto addr_468e_5;
        while (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8 + 32)) + reinterpret_cast<unsigned char>(rdx11)) <= reinterpret_cast<signed char>(rbp7)) {
            rax12 = *reinterpret_cast<void***>(rbx8 + 64);
            if (!rax12) {
                zf13 = have_read_eof == 0;
                if (!zf13) 
                    goto addr_46f9_9;
                al14 = load_buffer_part_0(rdi, rdi);
                if (!al14) 
                    goto addr_46f9_9;
                rax12 = *reinterpret_cast<void***>(rbx8 + 64);
                if (!rax12) 
                    goto addr_46c7_12;
            }
            rdx11 = *reinterpret_cast<void***>(rax12 + 16);
            rbx8 = rax12;
        }
        goto addr_4720_14;
        addr_46c7_12:
        rdi = reinterpret_cast<void**>("b");
        fun_2740("b", "src/csplit.c", 0x247, "find_line");
    }
    addr_46f9_9:
    return 0;
    addr_468e_5:
    goto addr_46f9_9;
    addr_4720_14:
    rbp15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbp7) - reinterpret_cast<unsigned char>(rdx11));
    rax16 = *reinterpret_cast<void***>(rbx8 + 48);
    if (reinterpret_cast<int64_t>(rbp15) > reinterpret_cast<int64_t>(79)) {
        do {
            rbp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp15) - 80);
            rax16 = *reinterpret_cast<void***>(rax16 + 0x518);
        } while (reinterpret_cast<int64_t>(rbp15) > reinterpret_cast<int64_t>(79));
    }
    return reinterpret_cast<unsigned char>(rax16) + (reinterpret_cast<uint64_t>(rbp15) << 4) + 24;
}