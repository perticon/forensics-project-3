int32_t * find_line(int64_t linenum) {
    // 0x4670
    if (head == NULL) {
        goto lab_0x46f0;
    } else {
        goto lab_0x4685;
    }
  lab_0x46f0:
    // 0x46f0
    if (*(char *)&have_read_eof != 0 || (char)load_buffer_part_0() == 0) {
        // 0x46f9
        return NULL;
    }
    // 0x4711
    goto lab_0x4685;
  lab_0x4685:;
    int64_t v1 = (int64_t)head;
    int64_t v2 = *(int64_t *)(v1 + 16); // 0x4685
    if (v2 > linenum) {
        // 0x46f9
        return NULL;
    }
    int64_t v3 = v2; // 0x46a1
    int64_t v4 = v1; // 0x46a1
    if (*(int64_t *)(v1 + 32) + v2 <= linenum) {
        int64_t * v5 = (int64_t *)(v1 + 64); // 0x46a3
        int64_t v6 = *v5; // 0x46a3
        int64_t v7 = v6; // 0x46aa
        int64_t v8; // 0x46be
        if (v6 == 0) {
            // 0x46ac
            if (*(char *)&have_read_eof != 0 || (char)load_buffer_part_0() == 0) {
                // 0x46f9
                return NULL;
            }
            // 0x46be
            v8 = *v5;
            v7 = v8;
            if (v8 == 0) {
                // 0x46c7
                function_2740();
                goto lab_0x46f0;
            }
        }
        int64_t v9 = *(int64_t *)(v7 + 16); // 0x4690
        v3 = v9;
        v4 = v7;
        while (*(int64_t *)(v7 + 32) + v9 <= linenum) {
            // 0x46a3
            v5 = (int64_t *)(v7 + 64);
            v6 = *v5;
            v7 = v6;
            if (v6 == 0) {
                // 0x46ac
                if (*(char *)&have_read_eof != 0 || (char)load_buffer_part_0() == 0) {
                    // 0x46f9
                    return NULL;
                }
                // 0x46be
                v8 = *v5;
                v7 = v8;
                if (v8 == 0) {
                    // 0x46c7
                    function_2740();
                    goto lab_0x46f0;
                }
            }
            // 0x4690
            v9 = *(int64_t *)(v7 + 16);
            v3 = v9;
            v4 = v7;
        }
    }
    int64_t v10 = linenum - v3; // 0x4720
    int64_t v11 = *(int64_t *)(v4 + 48); // 0x4723
    if (v10 < 80) {
        // 0x4741
        return (int32_t *)(v11 + 24 + 16 * v10);
    }
    int64_t v12 = v10 - 80; // 0x4730
    int64_t v13 = *(int64_t *)(v11 + (int64_t)&g55); // 0x4734
    int64_t v14 = v13; // 0x473f
    int64_t v15 = v12; // 0x473f
    while (v12 > 79) {
        // 0x4730
        v12 = v15 - 80;
        v13 = *(int64_t *)(v14 + (int64_t)&g55);
        v14 = v13;
        v15 = v12;
    }
    // 0x4741
    return (int32_t *)(v13 + 24 + 16 * v12);
}