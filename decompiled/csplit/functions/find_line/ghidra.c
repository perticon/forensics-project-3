long find_line(long param_1)

{
  char cVar1;
  long lVar2;
  long lVar3;
  long lVar4;
  
  if (((head == 0) && ((have_read_eof != '\0' || (cVar1 = load_buffer_part_0(), cVar1 == '\0')))) ||
     (lVar3 = *(long *)(head + 0x10), lVar4 = head, param_1 < lVar3)) {
    return 0;
  }
  do {
    if (param_1 < *(long *)(lVar4 + 0x20) + lVar3) {
      lVar4 = *(long *)(lVar4 + 0x30);
      for (param_1 = param_1 - lVar3; 0x4f < param_1; param_1 = param_1 + -0x50) {
        lVar4 = *(long *)(lVar4 + 0x518);
      }
      return lVar4 + 0x18 + param_1 * 0x10;
    }
    lVar2 = *(long *)(lVar4 + 0x40);
    if (lVar2 == 0) {
      if (have_read_eof != '\0') {
        return 0;
      }
      cVar1 = load_buffer_part_0();
      if (cVar1 == '\0') {
        return 0;
      }
      lVar2 = *(long *)(lVar4 + 0x40);
      if (lVar2 == 0) {
                    /* WARNING: Subroutine does not return */
        __assert_fail("b","src/csplit.c",0x247,"find_line");
      }
    }
    lVar3 = *(long *)(lVar2 + 0x10);
    lVar4 = lVar2;
  } while( true );
}