check_format_conv_type (char *format, int flags)
{
  unsigned char ch = *format;
  int compatible_flags = FLAG_THOUSANDS;

  switch (ch)
    {
    case 'd':
    case 'i':
      break;

    case 'u':
      *format = 'd';
      break;

    case 'o':
    case 'x':
    case 'X':
      compatible_flags = FLAG_ALTERNATIVE;
      break;

    case 0:
      die (EXIT_FAILURE, 0, _("missing conversion specifier in suffix"));

    default:
      if (isprint (ch))
        die (EXIT_FAILURE, 0,
             _("invalid conversion specifier in suffix: %c"), ch);
      else
        die (EXIT_FAILURE, 0,
             _("invalid conversion specifier in suffix: \\%.3o"), ch);
    }

  if (flags & ~ compatible_flags)
    die (EXIT_FAILURE, 0,
         _("invalid flags in conversion specification: %%%c%c"),
         (flags & ~ compatible_flags & FLAG_ALTERNATIVE ? '#' : '\''), ch);
}