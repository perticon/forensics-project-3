void dump_rest_of_file(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rax7;

    while (rax7 = remove_line(rdi, rsi, rdx), rdi = rax7, !!rax7) {
        save_line_to_file(rdi);
    }
    return;
}