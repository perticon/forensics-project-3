int64_t dump_rest_of_file(void) {
    int32_t * v1 = remove_line(); // 0x4855
    if (v1 == NULL) {
        // 0x4862
        return (int64_t)v1;
    }
    save_line_to_file(v1);
    int32_t * v2 = remove_line(); // 0x4855
    while (v2 != NULL) {
        // 0x4850
        save_line_to_file(v2);
        v2 = remove_line();
    }
    // 0x4862
    return (int64_t)v2;
}