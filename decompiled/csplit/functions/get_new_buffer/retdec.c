int32_t * get_new_buffer(int64_t min_size) {
    int64_t v1 = xmalloc(); // 0x3c90
    int64_t * v2 = (int64_t *)v1; // 0x3ca0
    *v2 = 0;
    char * v3 = xpalloc(NULL, v2, min_size, -1, 1); // 0x3cb4
    *(int64_t *)(v1 + 8) = 0;
    *(int64_t *)(v1 + 40) = (int64_t)v3;
    *(int64_t *)(v1 + 32) = 0;
    *(int64_t *)(v1 + 56) = 0;
    int64_t v4 = last_line_number + 1; // 0x3ce0
    *(int64_t *)(v1 + 24) = v4;
    *(int64_t *)(v1 + 16) = v4;
    *(int64_t *)(v1 + 48) = 0;
    *(int64_t *)(v1 + 64) = 0;
    return (int32_t *)v1;
}