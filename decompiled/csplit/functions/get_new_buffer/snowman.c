void** get_new_buffer(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;
    struct s2* rax5;
    void** rax6;

    rax3 = xmalloc(72, rsi);
    *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(0);
    rax4 = xpalloc();
    *reinterpret_cast<void***>(rax3 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rax3 + 40) = rax4;
    rax5 = last_line_number;
    *reinterpret_cast<void***>(rax3 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rax3 + 56) = reinterpret_cast<void**>(0);
    rax6 = reinterpret_cast<void**>(&rax5->f1);
    *reinterpret_cast<void***>(rax3 + 24) = rax6;
    *reinterpret_cast<void***>(rax3 + 16) = rax6;
    *reinterpret_cast<void***>(rax3 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rax3 + 64) = reinterpret_cast<void**>(0);
    return rax3;
}