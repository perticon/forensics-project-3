parse_patterns (int argc, int start, char **argv)
{
  struct control *p;		/* New control record created. */
  static intmax_t last_val = 0;

  for (int i = start; i < argc; i++)
    {
      if (*argv[i] == '/' || *argv[i] == '%')
        {
          p = extract_regexp (i, *argv[i] == '%', argv[i]);
        }
      else
        {
          p = new_control_record ();
          p->argnum = i;

          uintmax_t val;
          if (xstrtoumax (argv[i], NULL, 10, &val, "") != LONGINT_OK
              || INTMAX_MAX < val)
            die (EXIT_FAILURE, 0, _("%s: invalid pattern"), quote (argv[i]));
          if (val == 0)
            die (EXIT_FAILURE, 0,
                 _("%s: line number must be greater than zero"), argv[i]);
          if (val < last_val)
            {
              char buf[INT_BUFSIZE_BOUND (intmax_t)];
              die (EXIT_FAILURE, 0,
               _("line number %s is smaller than preceding line number, %s"),
                   quote (argv[i]), imaxtostr (last_val, buf));
            }

          if (val == last_val)
            error (0, 0,
           _("warning: line number %s is the same as preceding line number"),
                   quote (argv[i]));

          last_val = val;

          p->lines_required = val;
        }

      if (i + 1 < argc && *argv[i + 1] == '{')
        {
          /* We have a repeat count. */
          i++;
          parse_repeat_count (i, p, argv[i]);
        }
    }
}