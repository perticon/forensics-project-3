get_first_line_in_buffer (void)
{
  if (head == NULL && !load_buffer ())
    die (EXIT_FAILURE, errno, _("input disappeared"));

  return head->first_available;
}