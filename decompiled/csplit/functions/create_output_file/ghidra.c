void create_output_file(void)

{
  undefined8 uVar1;
  undefined8 uVar2;
  long lVar3;
  int *piVar4;
  int iVar5;
  int iVar6;
  long in_FS_OFFSET;
  sigset_t sStack184;
  long local_30;
  
  iVar5 = files_created;
  iVar6 = 0x4b;
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar1 = make_filename();
  output_filename = uVar1;
  if (iVar5 != 0x7fffffff) goto LAB_00104110;
  do {
    uVar1 = output_filename;
    uVar2 = quotearg_n_style_colon(0,3,output_filename);
    error(0,iVar6,"%s",uVar2);
    cleanup_fatal();
LAB_00104110:
    sigprocmask(0,(sigset_t *)caught_signals,&sStack184);
    lVar3 = fopen_safer(uVar1,&DAT_0011801c);
    output_stream = lVar3;
    piVar4 = __errno_location();
    iVar5 = (iVar5 + 1) - (uint)(lVar3 == 0);
    iVar6 = *piVar4;
    files_created = iVar5;
    sigprocmask(2,&sStack184,(sigset_t *)0x0);
  } while (lVar3 == 0);
  bytes_written = 0;
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  bytes_written = 0;
  return;
}