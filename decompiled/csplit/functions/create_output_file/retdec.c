void create_output_file(void) {
    int64_t v1 = __readfsqword(40); // 0x40b3
    int64_t v2 = (int64_t)make_filename(files_created); // 0x40ce
    output_filename = v2;
    int64_t v3 = files_created; // 0x40e3
    int64_t v4 = v2; // 0x40e3
    if (files_created != 0x7fffffff) {
        goto lab_0x4110;
    } else {
        goto lab_0x40e5;
    }
  lab_0x4110:
    // 0x4110
    function_2560();
    struct _IO_FILE * v5 = fopen_safer((char *)v4, "w"); // 0x412e
    *(int64_t *)&output_stream = (int64_t)v5;
    function_25c0();
    int64_t v6 = v3 + (int64_t)(v5 != NULL); // 0x414e
    files_created = v6;
    function_2560();
    if (v5 != NULL) {
        // 0x4166
        bytes_written = 0;
        if (v1 == __readfsqword(40)) {
            // 0x4184
            return;
        }
        // 0x4192
        function_26c0();
    }
    // 0x4197
    v4 = output_filename;
    v3 = v6 & 0xffffffff;
    goto lab_0x40e5;
  lab_0x40e5:
    // 0x40e5
    quotearg_n_style_colon();
    function_2920();
    cleanup_fatal();
    goto lab_0x4110;
}