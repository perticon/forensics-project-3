void create_output_file(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r12d7;
    void** rax8;
    void** v9;
    uint32_t ebx10;
    void** rax11;
    void* rsp12;
    void** rbp13;
    struct s1* rax14;
    void*** rax15;
    void* rax16;
    void** rax17;
    void** rsi18;

    r12d7 = reinterpret_cast<void**>(75);
    rax8 = g28;
    v9 = rax8;
    ebx10 = files_created;
    rax11 = make_filename(ebx10, rsi, rdx, rcx);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x98 - 8 + 8);
    output_filename = rax11;
    rbp13 = rax11;
    if (ebx10 == 0x7fffffff) 
        goto addr_40e5_2;
    while (1) {
        fun_2560();
        rax14 = fopen_safer(rbp13, "w", rsp12, rcx);
        output_stream = rax14;
        rax15 = fun_25c0();
        ebx10 = ebx10 - (1 - reinterpret_cast<uint1_t>(ebx10 < 1 - reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rax14) < 1)));
        r12d7 = *rax15;
        files_created = ebx10;
        fun_2560();
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        if (rax14) {
            bytes_written = reinterpret_cast<void**>(0);
            rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
            if (!rax16) 
                break;
            fun_26c0();
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
        }
        rbp13 = output_filename;
        addr_40e5_2:
        rax17 = quotearg_n_style_colon();
        rsi18 = r12d7;
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
        rcx = rax17;
        fun_2920();
        cleanup_fatal(0, rsi18, "%s", rcx, r8, r9);
        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    return;
}