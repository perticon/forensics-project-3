undefined8 xalloc_die(void)

{
  undefined *puVar1;
  undefined8 uVar2;
  long *plVar3;
  undefined *puVar4;
  void *pvVar5;
  int *piVar6;
  long lVar7;
  undefined *__s;
  long lVar8;
  size_t sVar9;
  
  uVar2 = dcgettext(0,"memory exhausted",5);
  error(0,0,"%s",uVar2);
  cleanup_fatal();
  lVar7 = hold_count + 1;
  if ((long)hold_count < 0x1fff) {
    lVar7 = 0x1fff;
  }
  do {
    plVar3 = (long *)get_new_buffer(lVar7);
    sVar9 = hold_count;
    lVar7 = *plVar3;
    pvVar5 = (void *)plVar3[5];
    if (hold_count != 0) {
      lVar7 = lVar7 - hold_count;
      pvVar5 = memcpy(pvVar5,hold_area,hold_count);
      plVar3[1] = plVar3[1] + sVar9;
      hold_count = 0;
      pvVar5 = (void *)((long)pvVar5 + sVar9);
    }
    lVar7 = lVar7 + -1;
    if (lVar7 != 0) {
      lVar7 = safe_read(0,pvVar5);
      if (lVar7 == 0) {
        have_read_eof = '\x01';
      }
      else if (lVar7 == -1) {
        uVar2 = dcgettext(0,"read error",5);
        piVar6 = __errno_location();
        error(0,*piVar6,uVar2);
        cleanup_fatal();
        goto LAB_00104630;
      }
    }
    lVar7 = lVar7 + plVar3[1];
    plVar3[1] = lVar7;
    if (lVar7 != 0) {
      __s = (undefined *)plVar3[5];
      lVar8 = 0;
      puVar1 = __s + lVar7;
      *puVar1 = 10;
      while( true ) {
        puVar4 = (undefined *)rawmemchr(__s,10);
        sVar9 = (long)puVar4 - (long)__s;
        if (puVar1 == puVar4) break;
        lVar8 = lVar8 + 1;
        keep_new_line(plVar3,__s,sVar9 + 1);
        __s = puVar4 + 1;
      }
      if (sVar9 != 0) {
        if (have_read_eof != '\0') {
          keep_new_line(plVar3,__s,sVar9);
          plVar3[4] = lVar8 + 1;
          lVar7 = last_line_number + 1;
          last_line_number = lVar8 + 1 + last_line_number;
          plVar3[2] = lVar7;
          plVar3[3] = lVar7;
          goto LAB_001044fb;
        }
        pvVar5 = (void *)ximemdup(__s,sVar9);
        free(hold_area);
        hold_count = sVar9;
        hold_area = pvVar5;
      }
      plVar3[4] = lVar8;
      lVar7 = last_line_number + 1;
      last_line_number = last_line_number + lVar8;
      plVar3[2] = lVar7;
      plVar3[3] = lVar7;
      if (lVar8 != 0) {
LAB_001044fb:
        plVar3[8] = 0;
        plVar3[7] = plVar3[6];
        lVar7 = (long)head;
        if (head == (long *)0x0) {
          head = plVar3;
          return 1;
        }
        do {
          lVar8 = lVar7;
          lVar7 = *(long *)(lVar8 + 0x40);
        } while (lVar7 != 0);
        *(long **)(lVar8 + 0x40) = plVar3;
        return 1;
      }
    }
    if (have_read_eof != '\0') {
LAB_00104630:
      free_buffer(plVar3);
      return 0;
    }
    lVar7 = *plVar3 * 2;
    if (SEXT816(lVar7) != SEXT816(*plVar3) * SEXT816(2)) {
                    /* WARNING: Subroutine does not return */
      xalloc_die();
    }
    free_buffer(plVar3);
  } while( true );
}