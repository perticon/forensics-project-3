check_for_offset (struct control *p, char const *str, char const *num)
{
  if (xstrtoimax (num, NULL, 10, &p->offset, "") != LONGINT_OK)
    die (EXIT_FAILURE, 0, _("%s: integer expected after delimiter"),
         quote (str));
}