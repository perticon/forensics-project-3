void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002560(fn00000000000023A0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000028AE;
	}
	fn0000000000002500(fn00000000000023A0(0x05, "Usage: %s FILE\n  or:  %s OPTION\n", null), 0x01);
	fn0000000000002440(stdout, fn00000000000023A0(0x05, "Call the unlink function to remove the specified FILE.\n\n", null));
	fn0000000000002440(stdout, fn00000000000023A0(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002440(stdout, fn00000000000023A0(0x05, "      --version     output version information and exit\n", null));
	struct Eq_578 * rbx_140 = fp - 0xB8 + 16;
	do
	{
		char * rsi_142 = rbx_140->qw0000;
		++rbx_140;
	} while (rsi_142 != null && fn0000000000002460(rsi_142, "unlink") != 0x00);
	ptr64 r13_155 = rbx_140->qw0008;
	if (r13_155 != 0x00)
	{
		fn0000000000002500(fn00000000000023A0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_242 = fn00000000000024F0(null, 0x05);
		if (rax_242 == 0x00 || fn0000000000002330(0x03, "en_", rax_242) == 0x00)
			goto l0000000000002A66;
	}
	else
	{
		fn0000000000002500(fn00000000000023A0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_184 = fn00000000000024F0(null, 0x05);
		if (rax_184 == 0x00 || fn0000000000002330(0x03, "en_", rax_184) == 0x00)
		{
			fn0000000000002500(fn00000000000023A0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002AA3:
			fn0000000000002500(fn00000000000023A0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000028AE:
			fn0000000000002540(edi);
		}
		r13_155 = 0x7004;
	}
	fn0000000000002440(stdout, fn00000000000023A0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002A66:
	fn0000000000002500(fn00000000000023A0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002AA3;
}