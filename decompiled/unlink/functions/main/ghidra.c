undefined8 main(int param_1,undefined8 *param_2)

{
  int iVar1;
  undefined8 uVar2;
  int *piVar3;
  long lVar4;
  long extraout_RDX;
  undefined8 uVar5;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  uVar5 = 0x102636;
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,"unlink","GNU coreutils",Version,1,usage,"Michael Stone",0,uVar5);
  if (param_1 <= optind) {
    uVar5 = dcgettext(0,"missing operand",5);
    error(0,0,uVar5);
                    /* WARNING: Subroutine does not return */
    usage(1);
  }
  lVar4 = (long)optind;
  if (param_1 <= optind + 1) {
    iVar1 = unlink((char *)param_2[lVar4]);
    if (iVar1 == 0) {
      return 0;
    }
    uVar5 = quotearg_style(4,param_2[optind]);
    uVar2 = dcgettext(0,"cannot unlink %s",5);
    piVar3 = __errno_location();
    error(1,*piVar3,uVar2,uVar5);
    lVar4 = extraout_RDX;
  }
  uVar5 = quote(param_2[lVar4 + 1]);
  uVar2 = dcgettext(0,"extra operand %s",5);
  error(0,0,uVar2,uVar5);
                    /* WARNING: Subroutine does not return */
  usage(1);
}