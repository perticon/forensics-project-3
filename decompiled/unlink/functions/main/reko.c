void main(char * rsi[], int32 edi, struct Eq_353 * fs)
{
	set_program_name(rsi[0]);
	fn00000000000024F0("", 0x06);
	fn0000000000002390("/usr/local/share/locale", "coreutils");
	fn0000000000002370("coreutils");
	atexit(&g_t2BB0);
	parse_gnu_standard_options_only(0x00, 0x7004, rsi, (uint64) edi, 0x01, fs, &g_t2850);
	int32 eax_87 = g_dwB090;
	if (eax_87 >= edi)
	{
		fn0000000000002510(fn00000000000023A0(0x05, "missing operand", null), 0x00, 0x00);
		usage(0x01);
	}
	else
	{
		int64 rdx_112 = (int64) eax_87;
		if (eax_87 + 0x01 >= edi)
		{
			if (fn0000000000002320(rsi[rdx_112]) == 0x00)
				return;
			quotearg_style(0x04, fs);
			fn0000000000002510(fn00000000000023A0(0x05, "cannot unlink %s", null), *fn0000000000002310(), 0x01);
		}
		quote(fs);
		fn0000000000002510(fn00000000000023A0(0x05, "extra operand %s", null), 0x00, 0x00);
		usage(0x01);
	}
}