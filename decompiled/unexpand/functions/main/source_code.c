main (int argc, char **argv)
{
  bool have_tabval = false;
  uintmax_t tabval IF_LINT ( = 0);
  int c;

  /* If true, cancel the effect of any -a (explicit or implicit in -t),
     so that only leading blanks will be considered.  */
  bool convert_first_only = false;

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  atexit (close_stdout);

  while ((c = getopt_long (argc, argv, ",0123456789at:", longopts, NULL))
         != -1)
    {
      switch (c)
        {
        case '?':
          usage (EXIT_FAILURE);
        case 'a':
          convert_entire_line = true;
          break;
        case 't':
          convert_entire_line = true;
          parse_tab_stops (optarg);
          break;
        case CONVERT_FIRST_ONLY_OPTION:
          convert_first_only = true;
          break;
        case ',':
          if (have_tabval)
            add_tab_stop (tabval);
          have_tabval = false;
          break;
        case_GETOPT_HELP_CHAR;
        case_GETOPT_VERSION_CHAR (PROGRAM_NAME, AUTHORS);
        default:
          if (!have_tabval)
            {
              tabval = 0;
              have_tabval = true;
            }
          if (!DECIMAL_DIGIT_ACCUMULATE (tabval, c - '0', uintmax_t))
            die (EXIT_FAILURE, 0, _("tab stop value is too large"));
          break;
        }
    }

  if (convert_first_only)
    convert_entire_line = false;

  if (have_tabval)
    add_tab_stop (tabval);

  finalize_tab_stops ();

  set_file_list ((optind < argc) ? &argv[optind] : NULL);

  unexpand ();

  cleanup_file_list_stdin ();

  return exit_status;
}