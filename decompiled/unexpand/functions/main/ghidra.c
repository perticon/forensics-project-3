undefined4 main(int param_1,undefined8 *param_2)

{
  char *pcVar1;
  byte *pbVar2;
  bool bVar3;
  bool bVar4;
  int iVar5;
  ulong uVar6;
  _IO_FILE *p_Var7;
  undefined *__ptr;
  size_t sVar8;
  ushort **ppuVar9;
  ulong uVar10;
  int *piVar11;
  undefined8 uVar12;
  ushort uVar13;
  undefined8 *puVar14;
  ulong uVar15;
  ushort uVar16;
  uint uVar17;
  long in_FS_OFFSET;
  bool bVar18;
  byte local_62;
  byte local_61;
  char local_49;
  long local_48;
  long local_40;
  
  uVar16 = 0x70f4;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  bVar3 = false;
  textdomain("coreutils");
  atexit(close_stdout);
  bVar4 = false;
  uVar15 = 0;
LAB_00102748:
  iVar5 = getopt_long(param_1,param_2,",0123456789at:",longopts,0);
  if (iVar5 == -1) {
    if (bVar3) {
      convert_entire_line = 0;
    }
    if (bVar4) {
      add_tab_stop(uVar15);
    }
    finalize_tab_stops();
    puVar14 = (undefined8 *)0x0;
    if (optind < param_1) {
      puVar14 = param_2 + optind;
    }
    set_file_list(puVar14);
    p_Var7 = (_IO_FILE *)next_file(0);
    if (p_Var7 != (_IO_FILE *)0x0) {
      __ptr = (undefined *)xmalloc(max_column_width);
      do {
        local_62 = 1;
        uVar6 = 0;
        uVar15 = 0;
        uVar13 = 1;
        local_48 = 0;
        local_61 = 0;
LAB_001029b2:
        do {
          pbVar2 = (byte *)p_Var7->_IO_read_ptr;
          if (pbVar2 < p_Var7->_IO_read_end) {
            p_Var7->_IO_read_ptr = (char *)(pbVar2 + 1);
            uVar17 = (uint)*pbVar2;
LAB_001029cc:
            if ((char)uVar13 == '\0') goto LAB_00102988;
            ppuVar9 = __ctype_b_loc();
            uVar16 = (*ppuVar9)[(int)uVar17] & 1;
            if (((*ppuVar9)[(int)uVar17] & 1) == 0) {
              if (uVar17 == 8) {
                uVar15 = (uVar15 - 1) + (ulong)(uVar15 == 0);
                local_48 = local_48 + -1 + (ulong)(local_48 == 0);
                local_62 = (byte)uVar16;
                if (uVar6 != 0) goto LAB_00102933;
                uVar13 = convert_entire_line | uVar16;
                goto LAB_00102988;
              }
              goto LAB_0010291a;
            }
LAB_001029f1:
            uVar10 = get_next_tab_column(uVar15,&local_48,&local_49);
            if (local_49 != '\0') goto LAB_00102b1b;
            if (uVar10 < uVar15) goto LAB_00102c31;
            if (uVar17 == 9) {
              if (uVar6 != 0) {
                *__ptr = 9;
              }
LAB_00102a46:
              uVar6 = (ulong)local_61;
              uVar13 = 1;
              uVar17 = 9;
              uVar15 = uVar10;
              goto LAB_00102929;
            }
            uVar15 = uVar15 + 1;
            local_62 = local_62 ^ 1 | uVar10 != uVar15;
            uVar13 = (ushort)local_62;
            if (local_62 == 0) {
              *__ptr = 9;
              uVar10 = uVar15;
              goto LAB_00102a46;
            }
            if (uVar10 == uVar15) {
              local_61 = local_62;
            }
            __ptr[uVar6] = (char)uVar17;
            uVar6 = uVar6 + 1;
          }
          else {
            uVar17 = __uflow(p_Var7);
            if (-1 < (int)uVar17) goto LAB_001029cc;
            p_Var7 = (_IO_FILE *)next_file(p_Var7);
            if (p_Var7 != (_IO_FILE *)0x0) goto LAB_001029b2;
            if ((char)uVar13 == '\0') {
LAB_00102b36:
              free(__ptr);
              goto LAB_00102b40;
            }
            ppuVar9 = __ctype_b_loc();
            uVar16 = (*ppuVar9)[(int)uVar17] & 1;
            if (((*ppuVar9)[(int)uVar17] & 1) != 0) goto LAB_001029f1;
LAB_0010291a:
            uVar15 = uVar15 + 1;
            if (uVar15 == 0) goto LAB_00102be9;
            uVar13 = 1;
LAB_00102929:
            while( true ) {
              local_62 = (byte)uVar16;
              if (uVar6 != 0) {
LAB_00102933:
                if ((1 < uVar6) && (local_61 != 0)) {
                  *__ptr = 9;
                }
                sVar8 = fwrite_unlocked(__ptr,1,uVar6,stdout);
                if (sVar8 != uVar6) goto LAB_00102bbd;
                local_61 = 0;
              }
              uVar13 = uVar13 & (convert_entire_line | uVar16);
              if ((int)uVar17 < 0) goto LAB_00102b36;
              uVar6 = 0;
LAB_00102988:
              pcVar1 = stdout->_IO_write_ptr;
              if (pcVar1 < stdout->_IO_write_end) break;
              iVar5 = __overflow(stdout,uVar17 & 0xff);
              if (-1 < iVar5) goto LAB_001029a8;
              uVar15 = dcgettext(0,"write error",5);
              piVar11 = __errno_location();
              error(1,*piVar11,uVar15);
LAB_00102b1b:
              uVar13 = 0;
            }
            stdout->_IO_write_ptr = pcVar1 + 1;
            *pcVar1 = (char)uVar17;
          }
LAB_001029a8:
        } while (uVar17 != 10);
      } while( true );
    }
LAB_00102b40:
    cleanup_file_list_stdin();
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      return exit_status;
    }
LAB_00102c55:
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  if (iVar5 == 0x61) {
    convert_entire_line = 1;
    goto LAB_00102748;
  }
  if (iVar5 < 0x62) {
    if (iVar5 == 0x2c) {
LAB_00102840:
      if (bVar4) {
        add_tab_stop();
        bVar4 = false;
      }
      goto LAB_00102748;
    }
    if (iVar5 < 0x2d) {
      if (iVar5 == -0x83) {
        version_etc(stdout,"unexpand","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar5 == -0x82) {
        usage(0);
LAB_00102bbd:
        uVar12 = dcgettext(0,"write error",5);
        piVar11 = __errno_location();
        error(1,*piVar11,uVar12);
LAB_00102be9:
        uVar12 = dcgettext(0,"input line is too long",5);
        error(1,0,uVar12);
LAB_00102c0d:
        uVar12 = dcgettext(0,"tab stop value is too large",5);
        error(1,0,uVar12);
LAB_00102c31:
        uVar12 = dcgettext(0,"input line is too long",5);
        error(1,0,uVar12);
        goto LAB_00102c55;
      }
    }
    else if (iVar5 == 0x3f) {
      usage();
      goto LAB_00102840;
    }
  }
  else {
    if (iVar5 == 0x74) {
      convert_entire_line = 1;
      parse_tab_stops();
      goto LAB_00102748;
    }
    if (iVar5 == 0x80) {
      bVar3 = true;
      goto LAB_00102748;
    }
  }
  if (bVar4) {
    if ((0x1999999999999999 < uVar15) ||
       (uVar6 = (long)(iVar5 + -0x30) + uVar15 * 10, bVar18 = uVar6 < uVar15, uVar15 = uVar6, bVar18
       )) goto LAB_00102c0d;
  }
  else {
    bVar4 = true;
    uVar15 = (long)(iVar5 + -0x30);
  }
  goto LAB_00102748;
}