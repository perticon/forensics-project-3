void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002640(fn0000000000002420(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002DAE;
	}
	fn00000000000025D0(fn0000000000002420(0x05, "Usage: %s [OPTION]... [FILE]...\n", null), 0x01);
	fn0000000000002500(stdout, fn0000000000002420(0x05, "Convert blanks in each FILE to tabs, writing to standard output.\n", null));
	fn0000000000002500(stdout, fn0000000000002420(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002500(stdout, fn0000000000002420(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002500(stdout, fn0000000000002420(0x05, "  -a, --all        convert all blanks, instead of just initial blanks\n      --first-only  convert only leading sequences of blanks (overrides -a)\n  -t, --tabs=N     have tabs N characters apart instead of 8 (enables -a)\n", null));
	emit_tab_list_info();
	fn0000000000002500(stdout, fn0000000000002420(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002500(stdout, fn0000000000002420(0x05, "      --version     output version information and exit\n", null));
	struct Eq_1019 * rbx_186 = fp - 0xB8 + 16;
	do
	{
		char * rsi_188 = rbx_186->qw0000;
		++rbx_186;
	} while (rsi_188 != null && fn0000000000002520(rsi_188, "unexpand") != 0x00);
	ptr64 r13_201 = rbx_186->qw0008;
	if (r13_201 != 0x00)
	{
		fn00000000000025D0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_17 rax_288 = fn00000000000025C0(null, 0x05);
		if (rax_288 == 0x00 || fn00000000000023A0(0x03, "en_", rax_288) == 0x00)
			goto l0000000000002FD6;
	}
	else
	{
		fn00000000000025D0(fn0000000000002420(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_17 rax_230 = fn00000000000025C0(null, 0x05);
		if (rax_230 == 0x00 || fn00000000000023A0(0x03, "en_", rax_230) == 0x00)
		{
			fn00000000000025D0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003013:
			fn00000000000025D0(fn0000000000002420(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002DAE:
			fn0000000000002620(edi);
		}
		r13_201 = 0x7004;
	}
	fn0000000000002500(stdout, fn0000000000002420(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002FD6:
	fn00000000000025D0(fn0000000000002420(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003013;
}