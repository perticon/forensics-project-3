find_closing_delim (const struct E_string *es, size_t start_idx,
                    char pre_bracket_char, size_t *result_idx)
{
  for (size_t i = start_idx; i < es->len - 1; i++)
    if (es->s[i] == pre_bracket_char && es->s[i + 1] == ']'
        && !es->escaped[i] && !es->escaped[i + 1])
      {
        *result_idx = i;
        return true;
      }
  return false;
}