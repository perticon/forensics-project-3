int64_t get_next (int64_t arg_8h, int64_t arg_10h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rdx = 0x000092bc;
    rax = *((rdi + 0x10));
label_0:
    if (rsi != 0) {
        *(rsi) = 2;
    }
    if (rax == 0xfffffffffffffffe) {
        goto label_7;
    }
    r14 = *((rbp + 8));
label_2:
    if (r14 == 0) {
        goto label_8;
    }
    if (*(r14) > 4) {
        void (*0x26ca)() ();
    }
    eax = *(r14);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (5 cases) at 0x92bc */
    void (*rax)() ();
    rax = *((r14 + 8));
    r13d = *((r14 + 0x10));
    *((rbp + 0x10)) = 0xffffffffffffffff;
    *((rbp + 8)) = rax;
label_1:
    eax = r13d;
    r12 = rbx;
    r13 = rbx;
    r14 = rbx;
    return rax;
    rax = *((r14 + 0x18));
    if (rax != 0) {
        goto label_9;
    }
    rax = *((r14 + 8));
    *((rbp + 0x10)) = 0xffffffffffffffff;
    *((rbp + 8)) = rax;
    rax = 0xffffffffffffffff;
    goto label_0;
    r12d = *((r14 + 0x10));
    if (rsi != 0) {
        if (r12d == 6) {
            goto label_10;
        }
        if (r12d != 0xa) {
            goto label_5;
        }
        *(rsi) = 1;
        r12d = *((r14 + 0x10));
    }
label_5:
    rbx = *((rbp + 0x10));
    if (rbx == -1) {
        goto label_11;
    }
label_3:
    esi = (int32_t) bl;
    edi = r12d;
    al = is_char_class_member ();
    if (al == 0) {
        goto label_12;
    }
    r13d = ebx;
    ebx++;
    if (ebx <= 0xff) {
        goto label_13;
    }
    goto label_6;
    do {
        ebx++;
        if (ebx == 0x100) {
            goto label_6;
        }
label_13:
        esi = (int32_t) bl;
        edi = r12d;
        al = is_char_class_member ();
    } while (al == 0);
    rbx = (int64_t) ebx;
    eax = r13d;
    *((rbp + 0x10)) = rbx;
    return rax;
    rdx = *((rbp + 0x10));
    rax = rdx + 1;
    if (rdx == -1) {
        goto label_14;
    }
label_4:
    edx = *((r14 + 0x11));
    *((rbp + 0x10)) = rax;
    r13d = eax;
    if (rdx != rax) {
        goto label_1;
    }
label_6:
    rax = *((r14 + 8));
    *((rbp + 0x10)) = 0xffffffffffffffff;
    *((rbp + 8)) = rax;
    eax = r13d;
    return rax;
label_7:
    rax = *(rbp);
    r14 = *((rax + 8));
    *((rbp + 0x10)) = 0xffffffffffffffff;
    *((rbp + 8)) = r14;
    goto label_2;
label_11:
    ebx = 0;
    while (al == 0) {
        ebx++;
        if (ebx == 0x100) {
            goto label_15;
        }
        esi = ebx;
        edi = r12d;
        al = is_char_class_member ();
    }
    rbx = (int64_t) ebx;
    *((rbp + 0x10)) = rbx;
    goto label_3;
label_14:
    eax = *((r14 + 0x10));
    goto label_4;
label_10:
    *(rsi) = 0;
    r12d = *((r14 + 0x10));
    goto label_5;
label_8:
    r13d = 0xffffffff;
    goto label_1;
label_9:
    rcx = *((rbp + 0x10));
    r13d = *((r14 + 0x10));
    rdx = rcx + 1;
    ecx = 1;
    if (rcx == -1) {
        rdx = rcx;
    }
    *((rbp + 0x10)) = rdx;
    if (rax != rdx) {
        goto label_1;
    }
    goto label_6;
label_12:
    assert_fail ("is_char_class_member (p->u.char_class, s->state)", "src/tr.c", 0x43a, "get_next");
label_15:
    return assert_fail ("i < N_CHARS", "src/tr.c", 0x437, "get_next");
}