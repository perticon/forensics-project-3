int64_t get_next(int64_t a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5, int64_t a6, int64_t a7) {
    int64_t * v1 = (int64_t *)(a1 + 16); // 0x3b42
    if (a2 != 0) {
        // 0x3b4b
        *(int32_t *)a2 = 2;
    }
    int64_t * v2 = (int64_t *)(a1 + 8);
    int64_t v3 = *v2;
    if (*v1 == -2) {
        // 0x3ca8
        *v1 = -1;
        *v2 = v3;
    }
    if (v3 == 0) {
        // 0x3d20
        return 0xffffffff;
    }
    int32_t * v4 = (int32_t *)v3; // 0x3b68
    uint32_t v5 = *v4; // 0x3b68
    int32_t v6 = v5; // 0x3b6c
    if (v5 >= 5) {
        get_next_cold();
        v6 = *v4;
    }
    int32_t v7 = *(int32_t *)(4 * (int64_t)v6 + (int64_t)&g14); // 0x3b75
    return (int64_t)v7 + (int64_t)&g14;
}