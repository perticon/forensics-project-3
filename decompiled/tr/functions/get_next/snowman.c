int32_t get_next(void** rdi, void** rsi, ...) {
    unsigned char rax3;
    void** r14_4;
    int64_t rax5;

    rax3 = *reinterpret_cast<unsigned char*>(rdi + 16);
    if (rsi) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(2);
    }
    if (rax3 == 0xfffffffffffffffe) {
        r14_4 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi) + 8);
        *reinterpret_cast<unsigned char*>(rdi + 16) = reinterpret_cast<unsigned char>(0xffffffffffffffff);
        *reinterpret_cast<void***>(rdi + 8) = r14_4;
    } else {
        r14_4 = *reinterpret_cast<void***>(rdi + 8);
    }
    if (!r14_4) {
        return -1;
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)) > reinterpret_cast<unsigned char>(4)) {
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
        } else {
            *reinterpret_cast<void***>(&rax5) = *reinterpret_cast<void***>(r14_4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x92bc + rax5 * 4) + 0x92bc;
        }
    }
}