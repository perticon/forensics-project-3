uint64 get_next(Eq_15 rsi, struct Eq_469 * rdi, struct Eq_469 & rbpOut, union Eq_15 & r14Out)
{
	Eq_15 r14;
	struct Eq_469 * rbp;
	word64 rax_22 = rdi->qw0010;
	if (rsi != 0x00)
		rsi->u2 = 0x02;
	Eq_15 r14_34;
	if (rax_22 != ~0x01)
		r14_34 = rdi->t0008;
	else
	{
		r14_34 = rdi->ptr0000->t0008;
		rdi->qw0010 = ~0x00;
		rdi->t0008 = r14_34;
	}
	if (r14_34 == 0x00)
	{
		rbpOut = rbp;
		r14Out = r14;
		return 0xFFFFFFFF;
	}
	else if (*r14_34 > 0x04)
		get_next.cold();
	else
	{
		<anonymous> * rax_67 = (int64) g_a92BC[(uint64) *r14_34 * 0x04] + 0x92BC;
		uint64 rax_84;
		rax_67();
		rbpOut = rdi;
		r14Out = r14_34;
		return rax_84;
	}
}