ulong read_and_delete(long param_1,undefined8 param_2)

{
  ulong uVar1;
  undefined8 uVar2;
  int *piVar3;
  ulong uVar4;
  ulong uVar5;
  
  do {
    uVar1 = safe_read(0,param_1,param_2);
    if (uVar1 == 0xffffffffffffffff) {
      uVar2 = dcgettext(0,"read error",5);
      piVar3 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar3,uVar2);
    }
    if (uVar1 == 0) {
      return 0;
    }
    uVar4 = 0;
    do {
      uVar5 = uVar4;
      uVar4 = uVar5 + 1;
      if (in_delete_set[*(byte *)(param_1 + uVar5)] != '\0') goto joined_r0x00104115;
    } while (uVar1 != uVar4);
    uVar4 = uVar5 + 2;
    uVar5 = uVar1;
    if (uVar1 <= uVar4) {
      return uVar1;
    }
    do {
      if (in_delete_set[*(byte *)(param_1 + uVar4)] == '\0') {
        *(byte *)(param_1 + uVar5) = *(byte *)(param_1 + uVar4);
        uVar5 = uVar5 + 1;
      }
      uVar4 = uVar4 + 1;
joined_r0x00104115:
    } while (uVar4 < uVar1);
  } while (uVar5 == 0);
  return uVar5;
}