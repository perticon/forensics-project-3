Eq_15 read_and_delete(Eq_15 rax, Eq_15 rsi, Eq_202 rdi, struct Eq_435 * fs, ptr64 & r12Out)
{
	ptr64 r12;
	do
	{
		safe_read(rsi, rdi, 0x00);
		if (rax == ~0x00)
		{
			fn0000000000002600(fn0000000000002440(0x05, "read error", null), *fn00000000000023B0(), 0x01);
			ptr64 r12_67;
			word64 r13_285;
			word32 ebx_284;
			Eq_15 rax_60 = parse_str(0x01, fs, out ebx_284, out r12_67, out r13_285);
			r12Out = r12_67;
			return rax_60;
		}
		if (rax == 0x00)
			goto l0000000000004146;
		Eq_15 rsi_101 = 0x00;
		while (true)
		{
			Eq_15 rdx_106 = (word64) rsi_101 + 1;
			if (Mem49[0x000000000000E1E0<p64> + CONVERT(Mem49[rdi + rsi_101:byte], byte, uint64):byte] != 0x00)
				break;
			if (rax == rdx_106)
			{
				rdx_106 = (word64) rsi_101 + 2;
				if (rdx_106 >= rax)
					goto l0000000000004146;
				rsi_101 = rax;
				goto l0000000000004120;
			}
			rsi_101 = rdx_106;
		}
		if (rax > rdx_106)
		{
l0000000000004120:
			do
			{
				word64 rdi_109 = CONVERT(Mem107[rdi + rdx_106:byte], byte, uint64);
				byte dil_113 = (byte) rdi_109;
				if (Mem107[0x000000000000E1E0<p64> + rdi_109:byte] == 0x00)
				{
					Mem115[rdi + rsi_101:byte] = dil_113;
					rsi_101 = (word64) rsi_101 + 1;
				}
				rdx_106 = (word64) rdx_106 + 1;
			} while (rdx_106 < rax);
		}
	} while (rsi_101 == 0x00);
	rax = rsi_101;
l0000000000004146:
	r12Out = r12;
	return rax;
}