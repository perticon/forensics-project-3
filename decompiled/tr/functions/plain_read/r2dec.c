uint64_t plain_read (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = safe_read (0, rdi, rsi);
    if (rax != -1) {
        return rax;
    }
    plain_read_part_0 ();
}