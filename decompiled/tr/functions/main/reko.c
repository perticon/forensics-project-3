void main(char * rsi[], word32 edi, struct Eq_435 * fs)
{
	set_program_name(rsi[0]);
	fn00000000000025E0("", 0x06);
	fn0000000000002420("/usr/local/share/locale", "coreutils");
	fn0000000000002400("coreutils");
	atexit(&g_t4E80);
	int64 rbp_24 = (int64) edi;
	word32 ebp_1765 = (word32) rbp_24;
	while (true)
	{
		int32 eax_79 = fn0000000000002480(&g_tDA80, "+AcCdst", rsi, ebp_1765, null);
		if (eax_79 == ~0x00)
			break;
		if (eax_79 != 99)
		{
			if (eax_79 > 99)
			{
				if (eax_79 == 115)
				{
					g_t103E4.u0 = 0x01;
					continue;
				}
				if (eax_79 != 116)
				{
					if (eax_79 != 100)
						goto l0000000000002944;
					g_t103E3.u0 = 0x01;
					continue;
				}
				else
				{
					g_b103E1 = 0x01;
					continue;
				}
			}
			if (eax_79 == ~0x81)
				usage(0x00);
			if (eax_79 < ~0x80)
			{
				if (eax_79 != ~0x82)
					goto l0000000000002944;
				version_etc(0x908E, stdout, fs);
				fn0000000000002640(0x00);
			}
			if (eax_79 == 0x41)
			{
				fn00000000000025E0("C", 0x03);
				fn00000000000025E0("C", 0x00);
				continue;
			}
			if (eax_79 != 0x43)
				goto l0000000000002944;
		}
		g_b103E2 = 0x01;
	}
	char * rsi_1610;
	word32 r13d_1115;
	word32 ebx_248;
	struct Eq_469 * r12_231;
	struct Eq_470 * r13_132;
	struct Eq_471 * rsp_138;
	int32 edx_189;
	int32 esi_193;
	word32 eax_92 = g_dwE098;
	Eq_15 dl_98 = g_t103E3;
	Eq_15 cl_103 = g_t103E4;
	int32 ebx_95 = ebp_1765 - eax_92;
	if (ebx_95 == 0x02)
	{
		if (dl_98 == 0x00)
		{
			g_b103E0 = 0x01;
			edx_189 = 0x02;
			if (cl_103 != 0x00)
				goto l00000000000028D2;
l0000000000002A1D:
			((word64) xmalloc(0x20) + 8)->u0 = 0x00;
			rsp_138 = (struct Eq_471 *) <invalid>;
			word32 ebx_1892;
			word64 r12_1893;
			if ((byte) parse_str(rsi[(int64) g_dwE098], fs, out ebx_1892, out r12_1893, out r13_132) == 0x00)
				goto l0000000000002A5A;
			goto l0000000000002D04;
		}
		g_b103E0 = 0x00;
		if (cl_103 != 0x00)
			goto l0000000000002A1D;
	}
	else
	{
		g_b103E0 = 0x00;
		if (dl_98 == cl_103)
		{
			esi_193 = 0x02;
			edx_189 = 0x02;
l000000000000297C:
			if (ebx_95 < esi_193)
			{
				if (ebx_95 == 0x00)
				{
					fn0000000000002600(fn0000000000002440(0x05, "missing operand", null), 0x00, 0x00);
l0000000000002944:
					usage(0x01);
				}
				quote((rsi - 8)[rbp_24], fs);
				fn0000000000002600(fn0000000000002440(0x05, "missing operand after %s", null), 0x00, 0x00);
				rsi_1610 = (char *) "Two strings must be given when both deleting and squeezing repeats.";
				if (g_t103E4 == 0x00)
					rsi_1610 = (char *) "Two strings must be given when translating.";
l0000000000002920:
				fn0000000000002440(0x05, rsi_1610, null);
				fn0000000000002660(0xA8F0, 0x01, stderr);
				goto l0000000000002944;
			}
l00000000000028D2:
			if (ebx_95 > edx_189)
			{
				quote(rsi[(int64) (eax_92 + edx_189)], fs);
				fn0000000000002600(fn0000000000002440(0x05, "extra operand %s", null), 0x00, 0x00);
				rsi_1610 = (char *) "Only one string may be given when deleting without squeezing repeats.";
				if (ebx_95 != 0x02)
					goto l0000000000002944;
				goto l0000000000002920;
			}
			((word64) xmalloc(0x20) + 8)->u0 = 0x00;
			struct Eq_471 * rbx_235 = (struct Eq_471 *) <invalid>;
			rsp_138 = (struct Eq_471 *) <invalid>;
			ebx_248 = (word32) rbx_235;
			r13d_1115 = (word32) r13_132;
			word32 ebx_1894;
			if ((byte) parse_str(rsi[(int64) g_dwE098], fs, out ebx_1894, out r12_231, out r13_132) == 0x00)
			{
l0000000000002A5A:
				fn0000000000002640(0x01);
			}
			if (ebx_248 != 0x02)
				goto l0000000000002AA8;
l0000000000002D04:
			Eq_15 rax_255 = xmalloc(0x20);
			rsp_138->t00A8 = rax_255;
			rsp_138->t00A0 = rax_255;
			((word64) rax_255 + 8)->u0 = 0x00;
			struct Eq_471 * rbx_281 = (struct Eq_471 *) <invalid>;
			ebx_248 = (word32) rbx_281;
			r13d_1115 = (word32) r13_268;
			word64 r13_268;
			word32 ebx_1895;
			if ((byte) parse_str(r13_132->a0008[(int64) g_dwE098], fs, out ebx_1895, out r12_231, out r13_268) != 0x00)
			{
l0000000000002AA8:
				word64 r14_1896;
				struct Eq_469 * r12_300;
				struct Eq_469 * rbp_1011 = get_spec_stats(r12_231, out r12_300, out r14_1896);
				struct Eq_471 * rsp_1092 = (struct Eq_471 *) <invalid>;
				if (g_b103E2 != 0x00)
				{
					rsp_1092->qw0070 = ~0x01;
					word64 * rdi_309 = rsp_1092->a00E0;
					uint64 rcx_311;
					for (rcx_311 = 0x20; rcx_311 != 0x00; --rcx_311)
					{
						*rdi_309 = 0x00;
						++rdi_309;
					}
					word32 r13d_1774 = 0x0100;
					while (true)
					{
						rsp_1092 = (struct Eq_471 *) <invalid>;
						word64 r14_1907;
						word32 eax_339 = (word32) get_next(0x00, r12_300, out rbp_1011, out r14_1907);
						if (eax_339 == ~0x00)
							break;
						int64 rax_342 = (int64) eax_339;
						word32 edx_347 = (word32) rsp_1092->a00E0[rax_342];
						rsp_1092->a00E0[rax_342] = 0x01;
						r13d_1774 -= (word32) ((byte) edx_347 ^ 0x01);
					}
					Eq_15 r13_359 = (int64) r13d_1774;
					rsp_1092->t0078 = r13_359;
					r13d_1115 = (word32) r13_359;
				}
				Eq_706 r15_1185 = rsp_1092->t0080;
				if (r15_1185 != 0x00)
				{
l0000000000003423:
					fn0000000000002600(fn0000000000002440(0x05, "the [c*] repeat construct may not appear in string1", null), 0x00, 0x01);
					goto l0000000000003447;
				}
				Eq_15 rax_1181;
				if (rbp_1011 == null)
				{
l0000000000002B1D:
					fadvise(stdin);
					Eq_15 al_870 = g_t103E4;
					rsp_1092->t0010 = al_870;
					if (ebx_248 == 0x01 && al_870 != 0x00)
					{
						word64 r14_1897;
						set_initialize(g_aE2E0, (word32) g_b103E2, r12_300, out r14_1897);
						squeeze_filter.constprop.0(0x3DD0);
						goto l0000000000002CB9;
					}
					if (g_t103E3 != 0x00)
					{
						if (ebx_248 == 0x01)
						{
l0000000000003215:
							Eq_202 r12_944 = g_aE3E0;
							word64 r14_1898;
							Eq_15 rax_947 = set_initialize(g_aE1E0, (word32) g_b103E2, r12_300, out r14_1898);
							do
							{
								Eq_15 rax_969 = read_and_delete(rax_947, 0x2000, r12_944, fs, out r12_944);
								if (rax_969 == 0x00)
									goto l0000000000002CB9;
								rax_947 = fn00000000000025C0(stdout, rax_969, 0x01, r12_944);
							} while (rax_969 == rax_947);
l0000000000003364:
							fn0000000000002600(fn0000000000002440(0x05, "write error", null), *fn00000000000023B0(), 0x01);
							goto l0000000000003390;
						}
						if (rsp_1092->t0010 != 0x00)
						{
							word64 r14_1899;
							set_initialize(g_aE1E0, (word32) g_b103E2, r12_300, out r14_1899);
							word64 r14_1900;
							set_initialize(g_aE2E0, 0x00, rbp_1011, out r14_1900);
							squeeze_filter.constprop.0(0x40D0);
							goto l0000000000002CB9;
						}
					}
					if (g_b103E0 == 0x00)
						goto l0000000000002CB9;
					int64 rax_1886 = 0x00;
					if (g_b103E2 == 0x00)
					{
						do
						{
							byte al_1047 = (byte) rax_1909;
							0xE0E0 + rax_1909 = (byte *) al_1047;
							rax_1886 = SEQ(SLICE(rax_1909 + 0x01, word56, 8), al_1047 + 0x01);
							rax_1909 = rax_1886;
						} while (rax_1909 != 0xFF);
						rsp_1092->qw0070 = ~0x01;
						rsp_1092->t0008 = &rsp_1092->dw005C;
						word64 rax_1058 = ~0x01;
						Eq_15 r14_1059 = &rsp_1092->t0058;
						while (true)
						{
							rbp_1011->qw0010 = rax_1058;
							do
							{
								struct Eq_471 * rsp_1076 = (struct Eq_471 *) <invalid>;
								struct Eq_469 * rbp_1074;
								word64 r14_1901;
								int64 r13_1087 = (int64) (word32) get_next(r14_1059, r12_300, out rbp_1074, out r14_1901);
								Eq_15 rax_1089 = get_next(rsp_1076->t0008, rbp_1074, out rbp_1011, out r14_1059);
								word32 eax_1119 = (word32) rax_1089;
								rsp_1092 = (struct Eq_471 *) <invalid>;
								r13d_1115 = (word32) r13_1087;
								byte al_1504 = (byte) eax_1119;
								Eq_15 edx_1101 = rsp_1092->t0058;
								if (edx_1101 != 0x00)
								{
									if (edx_1101 == 0x01 && rsp_1092->dw005C == 0x00)
									{
										Eq_983 r13_1514 = 0x00;
										Eq_7479 r12_1518[] = *fn00000000000026A0();
										do
										{
											if ((r12_1518[r13_1514].b0001 & 0x01) != 0x00)
												Mem1531[0x000000000000E0E0<p64> + r13_1514:byte] = Mem1521[fn0000000000002690() + 0x00:word64][r13_1514 * 0x04];
											r13_1514 = (word64) r13_1514 + 1;
										} while (r13_1514 != 0x0100);
										break;
									}
								}
								else if (rsp_1092->dw005C == 0x01)
								{
									Eq_1164 r13_1481 = 0x00;
									Eq_7479 r12_1485[] = *fn00000000000026A0();
									do
									{
										if ((r12_1485[r13_1481].b0001 & 0x02) != 0x00)
											Mem1498[0x000000000000E0E0<p64> + r13_1481:byte] = Mem1488[fn0000000000002380() + 0x00:word64][r13_1481 * 0x04];
										r13_1481 = (word64) r13_1481 + 1;
									} while (r13_1481 != 0x0100);
									break;
								}
								rax_1181 = rax_1089;
								if (r13d_1115 == ~0x00)
									goto l0000000000002C71;
								if (eax_1119 == ~0x00)
									goto l00000000000030A1;
								g_aE0E0[r13_1087] = al_1504;
							} while (rsp_1092->dw005C == 0x02);
							rsp_1092->qw0070 = ~0x00;
							rsp_1092->t0068 = *((word64) rsp_1092->t0068 + 8);
							rbp_1011->t0008 = *((word64) rbp_1011->t0008 + 8);
							rax_1058 = ~0x00;
						}
					}
					ptr64 r14_1160;
					set_initialize(g_aE1E0, 0x00, r12_300, out r14_1160);
					rbp_1011->qw0010 = ~0x01;
					int64 rax_1889 = 0x00;
					do
					{
						byte al_1176 = (byte) rax_1908;
						0xE0E0 + rax_1908 = (byte *) al_1176;
						rax_1181 = rax_1908 + 0x01;
						rax_1889 = SEQ(SLICE(rax_1908 + 0x01, word56, 8), al_1176 + 0x01);
						rax_1908 = rax_1889;
					} while (rax_1908 != 0xFF);
					do
					{
						if (*((word64) r15_1185.u0 + r14_1160) == 0x00)
						{
							rax_1181 = get_next(0x00, rbp_1011, out rbp_1011, out r14_1160);
							rsp_1092 = (struct Eq_471 *) <invalid>;
							byte al_1217 = (byte) rax_1181;
							if ((word32) rax_1181 == ~0x00)
							{
								if (g_b103E1 != 0x00)
									break;
								fn00000000000024D0("main", 0x0730, "src/tr.c", "ch != -1 || truncate_set1");
							}
							Mem1219[0x000000000000E0E0<p64> + r15_1185:byte] = al_1217;
						}
						r15_1185 = (word64) r15_1185.u0 + 1;
					} while (r15_1185 != 0x0100);
l0000000000002C71:
					Eq_202 r12_1234 = g_aE3E0;
					if (rsp_1092->t0010 != 0x00)
					{
						word64 r14_1902;
						set_initialize(g_aE2E0, 0x00, rbp_1011, out r14_1902);
						squeeze_filter.constprop.0(0x4080);
l0000000000002CB9:
						if (fn00000000000024F0(0x00) == 0x00)
							fn0000000000002640(0x00);
						goto l0000000000003338;
					}
					do
					{
						Eq_15 rax_1250 = read_and_xlate(rax_1181, 0x2000, r12_1234, fs, out r12_1234);
						if (rax_1250 == 0x00)
							goto l0000000000002CB9;
						rax_1181 = fn00000000000025C0(stdout, rax_1250, 0x01, r12_1234);
					} while (rax_1250 == rax_1181);
					fn0000000000002600(fn0000000000002440(0x05, "write error", null), *fn00000000000023B0(), 0x01);
l00000000000033DB:
					fn0000000000002600(fn0000000000002440(0x05, "when not truncating set1, string2 must be non-empty", null), 0x00, 0x01);
					goto l00000000000033FF;
				}
				else
				{
					rsp_1092->t0018 = rsp_1092->t0078;
					Eq_15 r14_379;
					rbp_1011 = get_spec_stats(rbp_1011, out r12_300, out r14_379);
					rsp_1092 = (struct Eq_471 *) <invalid>;
					Eq_15 rax_389 = rbp_1011->t0018;
					if (r14_379 >= rax_389 && rbp_1011->qw0020 == 0x01)
					{
						Eq_15 rcx_394 = rsp_1092->t0018;
						rbp_1011->ptr0028->qw0018 = rcx_394 - rax_389;
						rbp_1011->t0018 = rcx_394;
						if (g_b103E0 == 0x00)
							goto l00000000000031F1;
					}
					else
					{
						up64 rax_404 = rbp_1011->qw0020;
						if (rax_404 > 0x01)
						{
l0000000000003447:
							Eq_15 rax_1454 = fn0000000000002440(0x05, "only one [c*] repeat construct may appear in string2", null);
							fn0000000000002600(rax_1454, 0x00, 0x01);
							_start(rax_1454, rsp_1092->t0000);
						}
						if (g_b103E0 == 0x00)
						{
							if (rax_404 != 0x00)
							{
l00000000000031F1:
								fn0000000000002600(fn0000000000002440(0x05, "the [c*] construct may appear in string2 only when translating", null), 0x00, 0x01);
								goto l0000000000003215;
							}
							goto l0000000000002B1D;
						}
					}
					if (rbp_1011->t0030 == 0x00)
					{
						if (rbp_1011->b0032 == 0x00)
						{
							rsp_1092->t0020 = rbp_1011->t0018;
							rsp_1092->t0030 = rsp_1092->t0068;
							rsp_1092->t0038 = rbp_1011->t0008;
							if (g_b103E2 == 0x00)
							{
								uint64 rax_431 = (uint64) rbp_1011->b0031;
								word32 r14d_477 = (word32) rax_431;
								Eq_15 r14b_491 = (byte) r14d_477;
								if ((byte) rax_431 != 0x00)
								{
									uint64 rdx_441;
									uint64 rsi_443 = 0x00;
									Eq_15 r8_445 = 0x00;
									Eq_7479 r9_449[] = *fn00000000000026A0();
									for (rdx_441 = 0x00; rdx_441 != 0x0100; ++rdx_441)
									{
										uint64 rax_454 = (uint64) r9_449[rdx_441].w0000;
										r8_445 = r8_445 - ~0x00 - (word64) (((word16) rax_454 & 0x0100) < 0x01);
										rsi_443 = rsi_443 - ~0x00 - (word64) (((word16) rax_454 & 0x0200) < 0x01);
									}
									rsp_1092->dw004C = ebx_248;
									rbp_1011->qw0010 = ~0x01;
									rsp_1092->t0008 = r14b_491;
									rsp_1092->t0040 = r15_1185;
									rsp_1092->qw0070 = ~0x01;
									rsp_1092->t0010 = rsi_443 - 1;
									rsp_1092->t0028 = r8_445;
									int8 r13b_1780 = (byte) r14d_477;
									Eq_15 r14_493 = &rsp_1092->t0058;
									do
									{
										Eq_15 rbp_509;
										word64 r14_1903;
										word32 ebx_603 = (word32) get_next(r14_493, r12_300, out rbp_509, out r14_1903);
										rsp_1092 = (struct Eq_471 *) <invalid>;
										word64 rbp_1904;
										word32 eax_613 = (word32) get_next(rbp_509, rbp_1011, out rbp_1904, out r14_493);
										word32 esi_542 = rsp_1092->dw005C;
										if (r13b_1780 == 0x00)
										{
											if (esi_542 == 0x02)
												goto l0000000000002F00;
											goto l0000000000002E95;
										}
										if (esi_542 != 0x02)
										{
											if (rsp_1092->t0008 == 0x00 || rsp_1092->t0058 == 0x02)
											{
												fn0000000000002600(fn0000000000002440(0x05, "misaligned [:upper:] and/or [:lower:] construct", null), 0x00, 0x01);
												goto l00000000000032F0;
											}
l0000000000002E95:
											Eq_15 rcx_553 = rsp_1092->t0068;
											rsp_1092->qw0070 = ~0x00;
											Eq_15 rdi_556 = rsp_1092->t0010;
											rsp_1092->t0068 = *((word64) rcx_553 + 8);
											Eq_15 rcx_561 = *((word64) rbp_1011->t0008 + 8);
											rbp_1011->qw0010 = ~0x00;
											rbp_1011->t0008 = rcx_561;
											Eq_15 rcx_564 = rsp_1092->t0078;
											if (rsp_1092->t0058 == 0x01)
												rdi_556 = rsp_1092->t0028 - 1;
											Eq_15 rdi_579 = rsp_1092->t0010;
											rsp_1092->t0078 = rcx_564 - rdi_556;
											Eq_15 rcx_582 = rbp_1011->t0018;
											if (esi_542 == 0x01)
												rdi_579 = rsp_1092->t0028 - 1;
											rbp_1011->t0018 = rcx_582 - rdi_579;
										}
l0000000000002F00:
										rsp_1092->t0008.u0 = (int8) (rsp_1092->qw0070 == ~0x00);
										r13b_1780 = (int8) (rbp_1011->qw0010 == ~0x00);
									} while (ebx_603 != 0x01 && eax_613 != 0x01);
									ebx_248 = rsp_1092->dw004C;
									r15_1185 = rsp_1092->t0040;
									if (rsp_1092->t0018 < rsp_1092->t0078 || rsp_1092->t0020 < rbp_1011->t0018)
									{
l0000000000003390:
										fn00000000000024D0("validate_case_classes", 1224, "src/tr.c", "old_s1_len >= s1->length && old_s2_len >= s2->length");
									}
									rsp_1092->t0068 = rsp_1092->t0030;
									rbp_1011->t0008 = rsp_1092->t0038;
									rsp_1092->t0020 = rbp_1011->t0018;
								}
								Eq_15 rax_643 = rsp_1092->t0078;
								rsp_1092->t0018 = rax_643;
								if (rax_643 <= rsp_1092->t0020 || g_b103E1 != 0x00)
									goto l0000000000002B1D;
							}
							else if (rsp_1092->t0018 <= rsp_1092->t0020 || g_b103E1 != 0x00)
								goto l0000000000003032;
							if (rsp_1092->t0020 == 0x00)
								goto l00000000000033DB;
							Eq_15 r14b_690;
							Eq_15 rax_655 = rbp_1011->t0008;
							up32 edx_658 = *rax_655;
							if (edx_658 != 0x01)
							{
								if (edx_658 > 0x01)
								{
									if (edx_658 == 0x02)
									{
l00000000000033FF:
										fn0000000000002600(fn0000000000002440(0x05, "when translating with string1 longer than string2,\nthe latter string must not end with a character class", null), 0x00, 0x01);
										goto l0000000000003423;
									}
									if (edx_658 != 0x04)
										main.cold();
								}
								r14b_690 = *((word64) rax_655 + 16);
							}
							else
								r14b_690 = *((word64) rax_655 + 0x0011);
							Eq_15 r13_678 = rsp_1092->t0018 - rsp_1092->t0020;
							Eq_15 rax_680 = xmalloc(0x20);
							Eq_15 rdx_687 = rbp_1011->t0008;
							((word64) rax_680 + 8)->u0 = 0x00;
							rax_680->u2 = 0x04;
							*((word64) rax_680 + 16) = r14b_690;
							*((word64) rax_680 + 24) = r13_678;
							r13d_1115 = (word32) r13_678;
							if (rdx_687 == 0x00)
								append_repeated_char.part.0();
							*((word64) rdx_687 + 8) = rax_680;
							rbp_1011->t0008 = rax_680;
							rbp_1011->t0018 = rsp_1092->t0018;
							if (g_b103E2 != 0x00)
							{
l0000000000003032:
								if (rsp_1092->b0091 != 0x00)
								{
									if (rbp_1011->t0018 == rsp_1092->t0018)
									{
										rbp_1011->qw0010 = ~0x01;
										word64 r14_1905;
										word32 eax_787 = (word32) get_next(0x00, rbp_1011, out rbp_1011, out r14_1905);
										rsp_1092 = (struct Eq_471 *) <invalid>;
										r13d_1115 = eax_787;
										if (eax_787 != ~0x00)
										{
											do
											{
												rsp_1092 = (struct Eq_471 *) <invalid>;
												word64 r14_1906;
												word32 eax_810 = (word32) get_next(0x00, rbp_1011, out rbp_1011, out r14_1906);
												if (eax_810 == ~0x00)
													goto l0000000000002B1D;
											} while (eax_787 == eax_810);
										}
									}
									fn0000000000002600(fn0000000000002440(0x05, "when translating with complemented character classes,\nstring2 must map all characters in the domain to one", null), 0x00, 0x01);
									rax_1181.u0 = 0x00;
l00000000000030A1:
									if (r13d_1115 != 0x01 && g_b103E1 == 0x00)
										fn00000000000024D0("main", 0x0765, "src/tr.c", "c1 == -1 || truncate_set1");
									goto l0000000000002C71;
								}
								goto l0000000000002B1D;
							}
							goto l0000000000002B1D;
						}
l00000000000032F0:
						fn0000000000002600(fn0000000000002440(0x05, "when translating, the only character classes that may appear in\nstring2 are 'upper' and 'lower'", null), 0x00, 0x01);
					}
					fn0000000000002600(fn0000000000002440(0x05, "[=c=] expressions may not appear in string2 when translating", null), 0x00, 0x01);
l0000000000003338:
					fn0000000000002600(fn0000000000002440(0x05, "standard input", null), *fn00000000000023B0(), 0x01);
					goto l0000000000003364;
				}
			}
			goto l0000000000002A5A;
		}
	}
	esi_193 = 0x01;
	edx_189 = 0x00 - (word32) (cl_103 < dl_98) + 0x02;
	goto l000000000000297C;
}