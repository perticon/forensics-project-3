int32_t main (int32_t argc, char ** argv) {
    uint32_t var_8h;
    uint32_t var_10h;
    uint32_t var_18h;
    uint32_t var_sp_20h;
    int64_t var_28h;
    void * var_sp_30h;
    void * var_38h;
    int64_t var_40h;
    int64_t var_4ch;
    uint32_t var_58h;
    uint32_t var_5ch;
    int64_t var_60h;
    void * var_68h;
    uint32_t var_70h;
    uint32_t var_78h;
    int64_t var_80h;
    uint32_t var_91h;
    int64_t var_a0h;
    void * var_a8h;
    int64_t var_e0h;
    int64_t var_1e8h;
    rdi = argc;
    rsi = argv;
    r14 = 0x0000914c;
    r13 = rsi;
    r12 = 0x0000910b;
    rbp = (int64_t) edi;
    rbx = "+AcCdst";
    rax = *(fs:0x28);
    *((rsp + 0x1e8)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0000a941);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = obj_long_options;
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    do {
label_0:
        r8d = 0;
        rcx = r12;
        rdx = rbx;
        rsi = r13;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_24;
        }
        if (eax == 0x63) {
            goto label_25;
        }
        if (eax > 0x63) {
            goto label_26;
        }
        if (eax == 0xffffff7e) {
            goto label_27;
        }
        if (eax < 0xffffff7f) {
            goto label_28;
        }
        if (eax != 0x41) {
            goto label_29;
        }
        setlocale (3, r14);
        eax = setlocale (0, r14);
    } while (1);
label_26:
    if (eax == 0x73) {
        goto label_30;
    }
    if (eax == 0x74) {
        *(obj.truncate_set1) = 1;
        goto label_0;
label_29:
        if (eax != 0x43) {
            goto label_1;
        }
label_25:
        *(obj.complement) = 1;
        goto label_0;
label_28:
        if (eax != 0xffffff7d) {
            goto label_1;
        }
        eax = 0;
        version_etc (*(obj.stdout), 0x0000908e, "GNU coreutils", *(obj.Version), "Jim Meyering", 0);
        eax = exit (0);
    }
    if (eax != 0x64) {
        goto label_1;
    }
    *(obj.delete) = 1;
    goto label_0;
label_24:
    eax = optind;
    ebx = ebp;
    edx = *(obj.delete);
    ecx = *(obj.squeeze_repeats);
    ebx -= eax;
    if (ebx != 2) {
        goto label_31;
    }
    if (dl != 0) {
        goto label_32;
    }
    *(obj.translating) = 1;
    edx = 2;
    if (cl == 0) {
        goto label_33;
    }
    do {
        if (ebx <= edx) {
            goto label_34;
        }
        eax += edx;
        rax = (int64_t) eax;
        rax = quote (*((r13 + rax*8)), rsi, rdx, rcx, r8);
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "extra operand %s");
        rcx = r12;
        eax = 0;
        error (0, 0, rax);
        edx = 5;
        rsi = "Only one string may be given when deleting without squeezing repeats.";
        if (ebx == 2) {
label_2:
            rax = dcgettext (0, rsi);
            rdi = stderr;
            esi = 1;
            rdx = 0x0000a8f0;
            rcx = rax;
            eax = 0;
            fprintf_chk ();
        }
label_1:
        usage (1, rsi, rdx, rcx, r8, r9);
label_30:
        *(obj.squeeze_repeats) = 1;
        goto label_0;
label_27:
        usage (0, rsi, rdx, rcx, r8, r9);
label_31:
        *(obj.translating) = 0;
        if (dl == cl) {
            goto label_35;
        }
label_4:
        esi = 1;
        edx -= edx;
        edx += 2;
label_3:
    } while (ebx >= esi);
    if (ebx == 0) {
        edx = 5;
        rax = dcgettext (0, "missing operand");
        eax = 0;
        error (0, 0, rax);
        goto label_1;
    }
    rax = quote (*((r13 + rbp*8 - 8)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "missing operand after %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    edx = 5;
    rsi = "Two strings must be given when both deleting and squeezing repeats.";
    rax = "Two strings must be given when translating.";
    if (*(obj.squeeze_repeats) == 0) {
        rsi = rax;
    }
    goto label_2;
label_35:
    esi = 2;
    edx = 2;
    goto label_3;
label_32:
    *(obj.translating) = 0;
    if (cl == 0) {
        goto label_4;
    }
label_33:
    r12 = rsp + 0x60;
    rax = xmalloc (0x20);
    *((rsp + 0x68)) = rax;
    *((rsp + 0x60)) = rax;
    *((rax + 8)) = 0;
    rax = *(obj.optind);
    al = parse_str (*((r13 + rax*8)), r12, rdx, rcx, r8, r9);
    if (al != 0) {
        goto label_36;
    }
    do {
label_9:
        exit (1);
label_34:
        r12 = rsp + 0x60;
        rax = xmalloc (0x20);
        *((rsp + 0x68)) = rax;
        *((rsp + 0x60)) = rax;
        *((rax + 8)) = 0;
        rax = *(obj.optind);
        al = parse_str (*((r13 + rax*8)), r12, rdx, rcx, r8, r9);
    } while (al == 0);
    ebp = 0;
    if (ebx == 2) {
        goto label_36;
    }
label_8:
    rdi = r12;
    get_spec_stats ();
    if (*(obj.complement) != 0) {
        goto label_37;
    }
label_10:
    r15 = *((rsp + 0x80));
    if (r15 != 0) {
        goto label_38;
    }
    if (rbp != 0) {
        r14 = *((rsp + 0x78));
        rdi = rbp;
        *((rsp + 0x18)) = r14;
        get_spec_stats ();
        rax = *((rbp + 0x18));
        if (r14 >= rax) {
            if (*((rbp + 0x20)) == 1) {
                goto label_39;
            }
        }
        rax = *((rbp + 0x20));
        if (rax > 1) {
            goto label_40;
        }
        if (*(obj.translating) != 0) {
            goto label_21;
        }
        if (rax != 0) {
            goto label_41;
        }
    }
label_13:
    fadvise (*(obj.stdin), 2);
    eax = *(obj.squeeze_repeats);
    *((rsp + 0x10)) = al;
    if (ebx == 1) {
        if (al != 0) {
            goto label_42;
        }
    }
    if (*(obj.delete) != 0) {
        ebx--;
        if (ebx == 0) {
            goto label_43;
        }
        if (*((rsp + 0x10)) != 0) {
            goto label_44;
        }
    }
    if (*(obj.translating) == 0) {
        goto label_18;
    }
    eax = 0;
    rbx = obj_xlate;
    if (*(obj.complement) != 0) {
        goto label_45;
    }
    do {
        *((rbx + rax)) = al;
        rax++;
    } while (rax != 0x100);
    rcx = rsp + 0x5c;
    rax = 0xfffffffffffffffe;
    r14 = rsp + 0x58;
    r15 = r12;
    *((rsp + 0x70)) = 0xfffffffffffffffe;
    *((rsp + 8)) = rcx;
label_6:
    *((rbp + 0x10)) = rax;
    while (edx == 0) {
        if (*((rsp + 0x5c)) == 1) {
            goto label_46;
        }
label_5:
        if (r13d == 0xffffffff) {
            goto label_15;
        }
        if (eax == 0xffffffff) {
            goto label_47;
        }
        *((rbx + r13)) = al;
        if (*((rsp + 0x5c)) != 2) {
            goto label_7;
        }
        rsi = r14;
        rdi = r15;
        eax = get_next ();
        rsi = *((rsp + 8));
        rdi = rbp;
        r13 = (int64_t) eax;
        get_next ();
        edx = *((rsp + 0x58));
    }
    edx--;
    if (edx != 0) {
        goto label_5;
    }
    if (*((rsp + 0x5c)) != 0) {
        goto label_5;
    }
    rax = ctype_b_loc ();
    r13d = 0;
    r12 = *(rax);
    do {
        if ((*((r12 + r13*2 + 1)) & 1) != 0) {
            rax = ctype_tolower_loc ();
            rax = *(rax);
            eax = *((rax + r13*4));
            *((rbx + r13)) = al;
        }
        r13++;
    } while (r13 != 0x100);
label_7:
    *((rsp + 0x70)) = 0xffffffffffffffff;
    rax = *((rsp + 0x68));
    rax = *((rax + 8));
    *((rsp + 0x68)) = rax;
    rax = *((rbp + 8));
    rax = *((rax + 8));
    *((rbp + 8)) = rax;
    rax |= 0xffffffffffffffff;
    goto label_6;
label_15:
    r12 = obj_io_buf;
    if (*((rsp + 0x10)) == 0) {
        goto label_48;
    }
    goto label_49;
    do {
        rcx = stdout;
        rdx = rax;
        esi = 1;
        rdi = r12;
        rax = fwrite_unlocked ();
        if (rbx != rax) {
            goto label_50;
        }
label_48:
        rax = read_and_xlate (r12, sym._init, rdx, rcx, r8, r9);
        rbx = rax;
    } while (rax != 0);
label_18:
    eax = close (0);
    if (eax != 0) {
        goto label_51;
    }
    exit (0);
label_46:
    rax = ctype_b_loc ();
    r13d = 0;
    r12 = *(rax);
    do {
        if ((*((r12 + r13*2 + 1)) & 2) != 0) {
            rax = ctype_toupper_loc ();
            rax = *(rax);
            eax = *((rax + r13*4));
            *((rbx + r13)) = al;
        }
        r13++;
    } while (r13 != 0x100);
    goto label_7;
label_36:
    rbp = rsp + 0xa0;
    rax = xmalloc (0x20);
    *((rsp + 0xa8)) = rax;
    *((rsp + 0xa0)) = rax;
    *((rax + 8)) = 0;
    rax = *(obj.optind);
    al = parse_str (*((r13 + rax*8 + 8)), rbp, rdx, rcx, r8, r9);
    if (al != 0) {
        goto label_8;
    }
    goto label_9;
label_37:
    rdi = rsp + 0xe0;
    ecx = 0x20;
    eax = 0;
    *((rsp + 0x70)) = 0xfffffffffffffffe;
    *(rdi) = rax;
    rcx--;
    rdi += 8;
    r13d = 0x100;
    while (eax != 0xffffffff) {
        rax = (int64_t) eax;
        edx = *((rsp + rax + 0xe0));
        *((rsp + rax + 0xe0)) = 1;
        edx ^= 1;
        edx = (int32_t) dl;
        r13d -= edx;
        esi = 0;
        rdi = r12;
        eax = get_next ();
    }
    r13 = (int64_t) r13d;
    *((rsp + 0x78)) = r13;
    goto label_10;
label_21:
    if (*((rbp + 0x30)) != 0) {
        goto label_52;
    }
    if (*((rbp + 0x32)) != 0) {
        goto label_53;
    }
    rax = *((rbp + 0x18));
    *((rsp + 0x20)) = rax;
    rax = *((rsp + 0x68));
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 8));
    *((rsp + 0x38)) = rax;
    if (*(obj.complement) != 0) {
        goto label_54;
    }
    eax = *((rbp + 0x31));
    r14d = eax;
    if (al == 0) {
        goto label_55;
    }
    rax = ctype_b_loc ();
    edx = 0;
    esi = 0;
    r8d = 0;
    r9 = *(rax);
    do {
        eax = *((r9 + rdx*2));
        ecx = eax;
        cx &= 0x100;
        r8 -= 0xffffffffffffffff;
        ax &= 0x200;
        rsi -= 0xffffffffffffffff;
        rdx++;
    } while (rdx != 0x100);
    rax = rsp + 0x5c;
    rcx = rsi - 1;
    r13d = r14d;
    *((rsp + 0x4c)) = ebx;
    *((rbp + 0x10)) = 0xfffffffffffffffe;
    *((rsp + 8)) = r14b;
    r14 = rsp + 0x58;
    *((rsp + 0x40)) = r15;
    r15 = rbp;
    *((rsp + 0x70)) = 0xfffffffffffffffe;
    *((rsp + 0x10)) = rcx;
    *((rsp + 0x28)) = r8;
    while (r13b != 0) {
        if (esi != 2) {
            if (*((rsp + 8)) == 0) {
                goto label_56;
            }
            if (*((rsp + 0x58)) == 2) {
                goto label_56;
            }
label_11:
            rcx = *((rsp + 0x68));
            *((rsp + 0x70)) = 0xffffffffffffffff;
            rdi = *((rsp + 0x10));
            rcx = *((rcx + 8));
            *((rsp + 0x68)) = rcx;
            rcx = *((r15 + 8));
            rcx = *((rcx + 8));
            *((r15 + 0x10)) = 0xffffffffffffffff;
            *((r15 + 8)) = rcx;
            rcx = *((rsp + 0x78));
            if (*((rsp + 0x58)) == 1) {
                rdx = *((rsp + 0x28));
                rdi = rdx - 1;
            }
            rcx -= rdi;
            esi--;
            rdi = *((rsp + 0x10));
            *((rsp + 0x78)) = rcx;
            rcx = *((r15 + 0x18));
            if (esi == 0) {
                rdx = *((rsp + 0x28));
                rdi = rdx - 1;
            }
            rcx -= rdi;
            *((r15 + 0x18)) = rcx;
        }
label_12:
        rsp + 8 = (*((rsp + 0x70)) == -1) ? 1 : 0;
        r13b = (*((r15 + 0x10)) == -1) ? 1 : 0;
        ebx++;
        if (ebx == 0) {
            goto label_57;
        }
        eax++;
        if (eax == 0) {
            goto label_57;
        }
        rsi = r14;
        rdi = r12;
        eax = get_next ();
        rsi = rbp;
        rdi = r15;
        ebx = eax;
        get_next ();
        esi = *((rsp + 0x5c));
    }
    if (esi != 2) {
        goto label_11;
    }
    goto label_12;
label_57:
    ebx = *((rsp + 0x4c));
    r15 = *((rsp + 0x40));
    rax = *((rsp + 0x18));
    if (rax < *((rsp + 0x78))) {
        goto label_58;
    }
    rax = *((rsp + 0x20));
    if (rax < *((rbp + 0x18))) {
        goto label_58;
    }
    rax = *((rsp + 0x30));
    *((rsp + 0x68)) = rax;
    rax = *((rsp + 0x38));
    *((rbp + 8)) = rax;
    rax = *((rbp + 0x18));
    *((rsp + 0x20)) = rax;
label_55:
    rax = *((rsp + 0x78));
    *((rsp + 0x18)) = rax;
    if (rax <= *((rsp + 0x20))) {
        goto label_13;
    }
    if (*(obj.truncate_set1) != 0) {
        goto label_13;
    }
label_20:
    if (*((rsp + 0x20)) == 0) {
        goto label_59;
    }
    rax = *((rbp + 8));
    edx = *(rax);
    if (edx == 1) {
        goto label_60;
    }
    if (edx > 1) {
        goto label_61;
    }
label_22:
    r14d = *((rax + 0x10));
label_23:
    r13 = *((rsp + 0x18));
    r13 -= *((rsp + 0x20));
    xmalloc (0x20);
    rdx = *((rbp + 8));
    *((rax + 8)) = 0;
    *(rax) = 4;
    *((rax + 0x10)) = r14b;
    *((rax + 0x18)) = r13;
    if (rdx == 0) {
        goto label_62;
    }
    *((rdx + 8)) = rax;
    *((rbp + 8)) = rax;
    rax = *((rsp + 0x18));
    *((rbp + 0x18)) = rax;
    if (*(obj.complement) == 0) {
        goto label_13;
    }
label_19:
    if (*((rsp + 0x91)) == 0) {
        goto label_13;
    }
    rax = *((rsp + 0x18));
    if (*((rbp + 0x18)) != rax) {
        goto label_63;
    }
    *((rbp + 0x10)) = 0xfffffffffffffffe;
    esi = 0;
    rdi = rbp;
    eax = get_next ();
    r13d = eax;
    if (eax == 0xffffffff) {
        goto label_63;
    }
label_14:
    esi = 0;
    rdi = rbp;
    eax = get_next ();
    if (eax == 0xffffffff) {
        goto label_13;
    }
    if (r13d == eax) {
        goto label_14;
    }
label_63:
    edx = 5;
    rax = dcgettext (0, "when translating with complemented character classes,\nstring2 must map all characters in the domain to one");
    eax = 0;
    error (1, 0, rax);
label_47:
    r15d = r13d;
    r15d++;
    if (r15d == 0) {
        goto label_15;
    }
    if (*(obj.truncate_set1) != 0) {
        goto label_15;
    }
    assert_fail ("c1 == -1 || truncate_set1", "src/tr.c", 0x765, "main");
label_45:
    r14 = obj_in_delete_set;
    esi = 0;
    rdi = r12;
    rdx = r14;
    eax = set_initialize ();
    *((rbp + 0x10)) = 0xfffffffffffffffe;
    eax = 0;
    do {
        *((rbx + rax)) = al;
        rax++;
    } while (rax != 0x100);
    goto label_64;
label_17:
    esi = 0;
    rdi = rbp;
    eax = get_next ();
    if (eax == 0xffffffff) {
        goto label_65;
    }
    *((rbx + r15)) = al;
label_16:
    r15++;
    if (r15 == 0x100) {
        goto label_15;
    }
label_64:
    if (*((r14 + r15)) != 0) {
        goto label_16;
    }
    goto label_17;
label_65:
    if (*(obj.truncate_set1) != 0) {
        goto label_15;
    }
    assert_fail ("ch != -1 || truncate_set1", "src/tr.c", 0x730, "main");
label_49:
    rdi = rbp;
    rdx = obj_in_squeeze_set;
    esi = 0;
    set_initialize ();
    rdi = dbg_read_and_xlate;
    squeeze_filter_constprop_0 ();
    goto label_18;
label_54:
    rcx = *((rsp + 0x20));
    if (*((rsp + 0x18)) <= rcx) {
        goto label_19;
    }
    if (*(obj.truncate_set1) != 0) {
        goto label_19;
    }
    goto label_20;
label_42:
    esi = *(obj.complement);
    rdi = r12;
    rdx = obj_in_squeeze_set;
    set_initialize ();
    rdi = sym_plain_read;
    rax = squeeze_filter_constprop_0 ();
    goto label_18;
label_39:
    rcx = *((rsp + 0x18));
    rdx = *((rbp + 0x28));
    rsi = rcx;
    rsi -= rax;
    *((rdx + 0x18)) = rsi;
    *((rbp + 0x18)) = rcx;
    if (*(obj.translating) != 0) {
        goto label_21;
    }
label_41:
    edx = 5;
    rax = dcgettext (0, "the [c*] construct may appear in string2 only when translating");
    eax = 0;
    error (1, 0, rax);
label_43:
    esi = *(obj.complement);
    rdi = r12;
    rdx = obj_in_delete_set;
    r12 = obj_io_buf;
    rax = set_initialize ();
    while (rax != 0) {
        rcx = stdout;
        rdx = rax;
        esi = 1;
        rdi = r12;
        rax = fwrite_unlocked ();
        if (rbx != rax) {
            goto label_66;
        }
        rax = read_and_delete (r12, sym._init, rdx, rcx, r8, r9);
        rbx = rax;
    }
    goto label_18;
label_61:
    if (edx == 2) {
        goto label_67;
    }
    if (edx == 4) {
        goto label_22;
    }
    void (*0x26cf)() ();
label_44:
    esi = *(obj.complement);
    rdi = r12;
    rdx = obj_in_delete_set;
    set_initialize ();
    rdi = rbp;
    rdx = obj_in_squeeze_set;
    esi = 0;
    set_initialize ();
    rdi = dbg_read_and_delete;
    squeeze_filter_constprop_0 ();
    goto label_18;
label_60:
    r14d = *((rax + 0x11));
    goto label_23;
label_62:
    append_repeated_char_part_0 ();
label_56:
    edx = 5;
    rax = dcgettext (0, "misaligned [:upper:] and/or [:lower:] construct");
    eax = 0;
    error (1, 0, rax);
label_53:
    edx = 5;
    rax = dcgettext (0, "when translating, the only character classes that may appear in\nstring2 are 'upper' and 'lower');
    eax = 0;
    error (1, 0, rax);
label_52:
    edx = 5;
    rax = dcgettext (0, "[=c=] expressions may not appear in string2 when translating");
    eax = 0;
    error (1, 0, rax);
label_51:
    edx = 5;
    rax = dcgettext (0, "standard input");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_66:
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    do {
        eax = 0;
    } while (rcx != 0);
    error (1, *(rax), r12);
label_58:
    assert_fail ("old_s1_len >= s1->length && old_s2_len >= s2->length", "src/tr.c", 0x4c8, "validate_case_classes");
label_50:
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_59:
    edx = 5;
    rax = dcgettext (0, "when not truncating set1, string2 must be non-empty");
    eax = 0;
    error (1, 0, rax);
label_67:
    edx = 5;
    rax = dcgettext (0, "when translating with string1 longer than string2,\nthe latter string must not end with a character class");
    eax = 0;
    error (1, 0, rax);
label_38:
    edx = 5;
    rax = dcgettext (0, "the [c*] repeat construct may not appear in string1");
    eax = 0;
    error (1, 0, rax);
label_40:
    edx = 5;
    rax = dcgettext (0, "only one [c*] repeat construct may appear in string2");
    eax = 0;
    error (1, 0, rax);
}