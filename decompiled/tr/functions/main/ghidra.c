void main(int param_1,undefined8 *param_2)

{
  uint uVar1;
  ushort *puVar2;
  long lVar3;
  uint *puVar4;
  ulong uVar5;
  char cVar6;
  int iVar7;
  int iVar8;
  undefined8 uVar9;
  undefined8 uVar10;
  long lVar11;
  ushort **ppuVar12;
  __int32_t **pp_Var13;
  size_t sVar14;
  size_t sVar15;
  undefined4 *puVar16;
  int *piVar17;
  byte bVar18;
  byte extraout_DL;
  byte bVar19;
  int iVar20;
  long lVar21;
  long *plVar22;
  int iVar23;
  long lVar24;
  undefined8 *puVar25;
  long lVar26;
  undefined uVar27;
  long in_FS_OFFSET;
  char cVar28;
  bool bVar29;
  byte bVar30;
  ulong local_210;
  ulong local_208;
  int local_1d0;
  int local_1cc;
  long local_1c8;
  long local_1c0;
  long local_1b8;
  ulong local_1b0;
  long local_1a8;
  char local_197;
  long local_188;
  long local_180;
  char acStack344 [16];
  undefined8 local_148 [33];
  undefined8 local_40;
  
  bVar30 = 0;
  iVar8 = 0x10915b;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
LAB_001027a6:
  bVar18 = 0x80;
  iVar7 = getopt_long(param_1,param_2,"+AcCdst");
  if (iVar7 != -1) {
    if (iVar7 == 99) {
LAB_00102829:
      complement = '\x01';
      goto LAB_001027a6;
    }
    if (iVar7 < 100) {
      if (iVar7 == -0x82) {
        iVar7 = usage(0);
        bVar19 = extraout_DL;
        goto LAB_00102961;
      }
      if (iVar7 < -0x81) {
        if (iVar7 == -0x83) {
          version_etc(stdout,&DAT_0010908e,"GNU coreutils",Version,"Jim Meyering",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
      }
      else {
        if (iVar7 == 0x41) {
          setlocale(3,"C");
          setlocale(0,"C");
          goto LAB_001027a6;
        }
        if (iVar7 == 0x43) goto LAB_00102829;
      }
LAB_00102944:
      usage();
LAB_0010294e:
      squeeze_repeats = 1;
      goto LAB_001027a6;
    }
    if (iVar7 == 0x73) goto LAB_0010294e;
    if (iVar7 == 0x74) {
      truncate_set1 = '\x01';
    }
    else {
      if (iVar7 != 100) goto LAB_00102944;
      delete = 1;
    }
    goto LAB_001027a6;
  }
  iVar8 = param_1 - optind;
  iVar7 = optind;
  bVar18 = squeeze_repeats;
  bVar19 = delete;
  if (iVar8 == 2) {
    if (delete == 0) {
      translating = '\x01';
      iVar20 = 2;
      if (squeeze_repeats != 0) goto LAB_001028d2;
    }
    else {
      translating = '\0';
      if (squeeze_repeats == 0) goto LAB_00102970;
    }
    local_1c8 = xmalloc(0x20);
    *(undefined8 *)(local_1c8 + 8) = 0;
    local_1c0 = local_1c8;
    cVar6 = parse_str(param_2[optind],&local_1c8);
    if (cVar6 == '\0') goto LAB_00102a5a;
LAB_00102d04:
    plVar22 = &local_188;
    local_188 = xmalloc(0x20);
    *(undefined8 *)(local_188 + 8) = 0;
    local_180 = local_188;
    cVar6 = parse_str(param_2[(long)optind + 1]);
    if (cVar6 == '\0') {
LAB_00102a5a:
                    /* WARNING: Subroutine does not return */
      exit(1);
    }
  }
  else {
LAB_00102961:
    if (bVar19 == bVar18) {
      iVar23 = 2;
      iVar20 = 2;
    }
    else {
LAB_00102970:
      iVar23 = 1;
      iVar20 = 2 - (uint)(bVar18 < bVar19);
    }
    translating = '\0';
    if (iVar8 < iVar23) {
      if (iVar8 != 0) {
        uVar9 = quote(param_2[(long)param_1 + -1]);
        uVar10 = dcgettext(0,"missing operand after %s",5);
                    /* WARNING: Subroutine does not return */
        error(0,0,uVar10,uVar9);
      }
      uVar9 = dcgettext(0,"missing operand",5);
                    /* WARNING: Subroutine does not return */
      error(0,0,uVar9);
    }
LAB_001028d2:
    if (iVar20 < iVar8) {
      uVar9 = quote(param_2[iVar7 + iVar20]);
      uVar10 = dcgettext(0,"extra operand %s",5);
                    /* WARNING: Subroutine does not return */
      error(0,0,uVar10,uVar9);
    }
    local_1c8 = xmalloc(0x20);
    *(undefined8 *)(local_1c8 + 8) = 0;
    local_1c0 = local_1c8;
    cVar6 = parse_str(param_2[optind]);
    if (cVar6 == '\0') goto LAB_00102a5a;
    plVar22 = (long *)0x0;
    if (iVar8 == 2) goto LAB_00102d04;
  }
  get_spec_stats(&local_1c8);
  if (complement != '\0') {
    local_1b8 = -2;
    puVar25 = local_148;
    for (lVar11 = 0x20; lVar11 != 0; lVar11 = lVar11 + -1) {
      *puVar25 = 0;
      puVar25 = puVar25 + (ulong)bVar30 * -2 + 1;
    }
    iVar7 = 0x100;
    while (iVar20 = get_next(&local_1c8), iVar20 != -1) {
      bVar30 = *(byte *)((long)local_148 + (long)iVar20);
      *(undefined *)((long)local_148 + (long)iVar20) = 1;
      iVar7 = iVar7 - (uint)(bVar30 ^ 1);
    }
    local_1b0 = (ulong)iVar7;
  }
  uVar5 = local_1b0;
  if (local_1a8 != 0) {
    uVar9 = dcgettext(0,"the [c*] repeat construct may not appear in string1",5);
                    /* WARNING: Subroutine does not return */
    error(1,0,uVar9);
  }
  if (plVar22 != (long *)0x0) {
    local_210 = local_1b0;
    get_spec_stats(plVar22);
    lVar11 = local_1c0;
    if ((uVar5 < (ulong)plVar22[3]) || (plVar22[4] != 1)) {
      if (1 < (ulong)plVar22[4]) {
        uVar9 = dcgettext(0,"only one [c*] repeat construct may appear in string2",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar9);
      }
      if (translating == '\0') {
        local_1c0 = lVar11;
        if (plVar22[4] != 0) goto LAB_001031f1;
        goto LAB_00102b1d;
      }
    }
    else {
      bVar29 = translating == '\0';
      *(ulong *)(plVar22[5] + 0x18) = uVar5 - plVar22[3];
      plVar22[3] = uVar5;
      if (bVar29) {
LAB_001031f1:
        uVar9 = dcgettext(0,"the [c*] construct may appear in string2 only when translating",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar9);
      }
    }
    if (*(char *)(plVar22 + 6) != '\0') {
      uVar9 = dcgettext(0,"[=c=] expressions may not appear in string2 when translating",5);
                    /* WARNING: Subroutine does not return */
      error(1,0,uVar9);
    }
    if (*(char *)((long)plVar22 + 0x32) != '\0') {
      uVar9 = dcgettext(0,
                        "when translating, the only character classes that may appear in\nstring2 are \'upper\' and \'lower\'"
                        ,5);
                    /* WARNING: Subroutine does not return */
      error(1,0,uVar9);
    }
    local_208 = plVar22[3];
    lVar3 = plVar22[1];
    if (complement == '\0') {
      cVar6 = *(char *)((long)plVar22 + 0x31);
      if (cVar6 != '\0') {
        ppuVar12 = __ctype_b_loc();
        lVar21 = 0;
        lVar24 = 0;
        lVar26 = 0;
        do {
          lVar26 = (lVar26 + 1) - (ulong)(((*ppuVar12)[lVar21] & 0x100) == 0);
          lVar24 = (lVar24 + 1) - (ulong)(((*ppuVar12)[lVar21] & 0x200) == 0);
          lVar21 = lVar21 + 1;
        } while (lVar21 != 0x100);
        plVar22[2] = -2;
        local_1b8 = -2;
        cVar28 = cVar6;
        do {
          iVar7 = get_next(&local_1c8,&local_1d0);
          iVar20 = get_next(plVar22);
          if (cVar6 == '\0') {
            if (local_1cc != 2) {
LAB_00102e95:
              local_1b8 = -1;
              local_1c0 = *(long *)(local_1c0 + 8);
              lVar21 = *(long *)(plVar22[1] + 8);
              plVar22[2] = -1;
              plVar22[1] = lVar21;
              lVar21 = lVar24 + -1;
              if (local_1d0 == 1) {
                lVar21 = lVar26 + -1;
              }
              local_1b0 = local_1b0 - lVar21;
              lVar21 = lVar24 + -1;
              if (local_1cc == 1) {
                lVar21 = lVar26 + -1;
              }
              plVar22[3] = plVar22[3] - lVar21;
            }
          }
          else if (local_1cc != 2) {
            if ((cVar28 != '\0') && (local_1d0 != 2)) goto LAB_00102e95;
            goto LAB_001032cc;
          }
          cVar28 = local_1b8 == -1;
          cVar6 = plVar22[2] == -1;
        } while ((iVar7 != -1) && (iVar20 != -1));
        if ((uVar5 < local_1b0) || (local_208 < (ulong)plVar22[3])) {
                    /* WARNING: Subroutine does not return */
          __assert_fail("old_s1_len >= s1->length && old_s2_len >= s2->length","src/tr.c",0x4c8,
                        "validate_case_classes");
        }
        plVar22[1] = lVar3;
        local_208 = plVar22[3];
      }
      local_210 = local_1b0;
      local_1c0 = lVar11;
      if ((local_208 < local_1b0) && (truncate_set1 == '\0')) {
LAB_00102fb7:
        if (local_208 == 0) {
          local_1c0 = lVar11;
          uVar9 = dcgettext(0,"when not truncating set1, string2 must be non-empty",5);
                    /* WARNING: Subroutine does not return */
          error(1,0,uVar9);
        }
        puVar4 = (uint *)plVar22[1];
        uVar1 = *puVar4;
        if (uVar1 == 1) {
          uVar27 = *(undefined *)((long)puVar4 + 0x11);
        }
        else {
          if (1 < uVar1) {
            if (uVar1 == 2) {
              local_1c0 = lVar11;
              uVar9 = dcgettext(0,
                                "when translating with string1 longer than string2,\nthe latter string must not end with a character class"
                                ,5);
                    /* WARNING: Subroutine does not return */
              error(1,0,uVar9);
            }
            if (uVar1 != 4) {
              local_1c0 = lVar11;
              main_cold();
              return;
            }
          }
          uVar27 = *(undefined *)(puVar4 + 4);
        }
        local_1c0 = lVar11;
        puVar16 = (undefined4 *)xmalloc(0x20);
        lVar11 = plVar22[1];
        *(undefined8 *)(puVar16 + 2) = 0;
        *puVar16 = 4;
        *(undefined *)(puVar16 + 4) = uVar27;
        *(ulong *)(puVar16 + 6) = local_210 - local_208;
        if (lVar11 == 0) {
          append_repeated_char_part_0();
LAB_001032cc:
          uVar9 = dcgettext(0,"misaligned [:upper:] and/or [:lower:] construct",5);
                    /* WARNING: Subroutine does not return */
          error(1,0,uVar9);
        }
        *(undefined4 **)(lVar11 + 8) = puVar16;
        plVar22[1] = (long)puVar16;
        plVar22[3] = local_210;
        if (complement != '\0') goto LAB_00103032;
      }
    }
    else {
      if ((local_208 < uVar5) && (truncate_set1 == '\0')) goto LAB_00102fb7;
LAB_00103032:
      if (local_197 != '\0') {
        if (plVar22[3] == local_210) {
          plVar22[2] = -2;
          iVar7 = get_next(plVar22,0);
          if (iVar7 != -1) {
            do {
              iVar20 = get_next(plVar22,0);
              if (iVar20 == -1) goto LAB_00102b1d;
            } while (iVar7 == iVar20);
          }
        }
        uVar9 = dcgettext(0,
                          "when translating with complemented character classes,\nstring2 must map all characters in the domain to one"
                          ,5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar9);
      }
    }
  }
LAB_00102b1d:
  fadvise(stdin,2);
  bVar30 = squeeze_repeats;
  if ((iVar8 == 1) && (squeeze_repeats != 0)) {
    set_initialize(&local_1c8,complement,in_squeeze_set);
    squeeze_filter_constprop_0();
    goto LAB_00102cb9;
  }
  if (delete != 0) {
    if (iVar8 == 1) {
      set_initialize(&local_1c8,complement,in_delete_set);
      while (sVar15 = read_and_delete(io_buf,0x2000), sVar15 != 0) {
        sVar14 = fwrite_unlocked(io_buf,1,sVar15,stdout);
        if (sVar15 != sVar14) {
          uVar9 = dcgettext(0,"write error",5);
          piVar17 = __errno_location();
                    /* WARNING: Subroutine does not return */
          error(1,*piVar17,uVar9);
        }
      }
      goto LAB_00102cb9;
    }
    if (squeeze_repeats != 0) {
      set_initialize(&local_1c8,complement,in_delete_set);
      set_initialize(plVar22,0,in_squeeze_set);
      squeeze_filter_constprop_0();
      goto LAB_00102cb9;
    }
  }
  if (translating != '\0') {
    lVar11 = 0;
    if (complement == '\0') {
      do {
        xlate[lVar11] = (char)lVar11;
        lVar11 = lVar11 + 1;
      } while (lVar11 != 0x100);
      lVar11 = -2;
      local_1b8 = -2;
      do {
        plVar22[2] = lVar11;
        while( true ) {
          iVar8 = get_next(&local_1c8,&local_1d0);
          iVar7 = get_next(plVar22,&local_1cc);
          if (local_1d0 == 0) break;
          if ((local_1d0 == 1) && (local_1cc == 0)) {
            ppuVar12 = __ctype_b_loc();
            lVar11 = 0;
            puVar2 = *ppuVar12;
            do {
              if ((*(byte *)((long)puVar2 + lVar11 * 2 + 1) & 1) != 0) {
                pp_Var13 = __ctype_tolower_loc();
                xlate[lVar11] = (char)(*pp_Var13)[lVar11];
              }
              lVar11 = lVar11 + 1;
            } while (lVar11 != 0x100);
            goto LAB_00102c45;
          }
LAB_00102bc8:
          if (iVar8 == -1) goto LAB_00102c71;
          if (iVar7 == -1) {
            if ((iVar8 != -1) && (truncate_set1 == '\0')) {
                    /* WARNING: Subroutine does not return */
              __assert_fail("c1 == -1 || truncate_set1","src/tr.c",0x765,
                            (char *)&__PRETTY_FUNCTION___10);
            }
            goto LAB_00102c71;
          }
          xlate[iVar8] = (char)iVar7;
          if (local_1cc != 2) goto LAB_00102c45;
        }
        if (local_1cc != 1) goto LAB_00102bc8;
        ppuVar12 = __ctype_b_loc();
        lVar11 = 0;
        puVar2 = *ppuVar12;
        do {
          if ((*(byte *)((long)puVar2 + lVar11 * 2 + 1) & 2) != 0) {
            pp_Var13 = __ctype_toupper_loc();
            xlate[lVar11] = (char)(*pp_Var13)[lVar11];
          }
          lVar11 = lVar11 + 1;
        } while (lVar11 != 0x100);
LAB_00102c45:
        local_1b8 = -1;
        local_1c0 = *(long *)(local_1c0 + 8);
        plVar22[1] = *(long *)(plVar22[1] + 8);
        lVar11 = -1;
      } while( true );
    }
    set_initialize(&local_1c8,0,in_delete_set);
    plVar22[2] = -2;
    lVar11 = 0;
    do {
      xlate[lVar11] = (char)lVar11;
      lVar11 = lVar11 + 1;
    } while (lVar11 != 0x100);
    do {
      if (in_delete_set[local_1a8] == '\0') {
        iVar8 = get_next(plVar22,0);
        if (iVar8 == -1) {
          if (truncate_set1 == '\0') {
                    /* WARNING: Subroutine does not return */
            __assert_fail("ch != -1 || truncate_set1","src/tr.c",0x730,
                          (char *)&__PRETTY_FUNCTION___10);
          }
          break;
        }
        xlate[local_1a8] = (char)iVar8;
      }
      local_1a8 = local_1a8 + 1;
    } while (local_1a8 != 0x100);
LAB_00102c71:
    if (bVar30 == 0) {
      while (sVar15 = read_and_xlate(io_buf,0x2000), sVar15 != 0) {
        sVar14 = fwrite_unlocked(io_buf,1,sVar15,stdout);
        if (sVar15 != sVar14) {
          uVar9 = dcgettext(0,"write error",5);
          piVar17 = __errno_location();
                    /* WARNING: Subroutine does not return */
          error(1,*piVar17,uVar9);
        }
      }
    }
    else {
      set_initialize(plVar22,0,in_squeeze_set);
      squeeze_filter_constprop_0();
    }
  }
LAB_00102cb9:
  iVar8 = close(0);
  if (iVar8 != 0) {
    uVar9 = dcgettext(0,"standard input",5);
    piVar17 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar17,uVar9);
  }
                    /* WARNING: Subroutine does not return */
  exit(0);
}