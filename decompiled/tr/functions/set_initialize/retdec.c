int64_t set_initialize(int64_t a1, int32_t a2, int64_t a3) {
    // 0x3e30
    *(int64_t *)(a1 + 16) = -2;
    int64_t v1 = get_next(a1, 0, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40); // 0x3e5b
    int64_t result = v1; // 0x3e63
    if ((int32_t)v1 != -1) {
        *(char *)((0x100000000 * v1 >> 32) + a3) = 1;
        int64_t v2 = get_next(a1, 0, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40); // 0x3e5b
        result = v2;
        while ((int32_t)v2 != -1) {
            // 0x3e50
            *(char *)((0x100000000 * v2 >> 32) + a3) = 1;
            v2 = get_next(a1, 0, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40, (int64_t)&g40);
            result = v2;
        }
    }
    // 0x3e65
    if ((char)a2 == 0) {
        // 0x3e84
        return result;
    }
    int64_t result2 = a3 + 256; // 0x3e6d
    int64_t v3 = a3; // 0x3e74
    char * v4 = (char *)v3; // 0x3e78
    *v4 = *v4 ^ 1;
    v3++;
    while (v3 != result2) {
        // 0x3e78
        v4 = (char *)v3;
        *v4 = *v4 ^ 1;
        v3++;
    }
    // 0x3e84
    return result2;
}