set_initialize (struct Spec_list *s, bool complement_this_set, bool *in_set)
{
  int c;

  s->state = BEGIN_STATE;
  while ((c = get_next (s, NULL)) != -1)
    in_set[c] = true;
  if (complement_this_set)
    for (size_t i = 0; i < N_CHARS; i++)
      in_set[i] = (!in_set[i]);
}