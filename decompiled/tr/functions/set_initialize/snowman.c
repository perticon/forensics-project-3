void set_initialize(void** rdi, uint32_t esi, void** rdx, ...) {
    uint32_t r12d4;
    void** rbp5;
    void** rbx6;
    int32_t eax7;
    void** rdx8;
    void** rax9;

    r12d4 = esi;
    rbp5 = rdi;
    rbx6 = rdx;
    *reinterpret_cast<unsigned char*>(rdi + 16) = reinterpret_cast<unsigned char>(0xfffffffffffffffe);
    while (eax7 = get_next(rbp5, 0), eax7 != -1) {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax7))) = 1;
    }
    if (*reinterpret_cast<signed char*>(&r12d4)) {
        rdx8 = rbx6;
        rax9 = rbx6 + 0x100;
        do {
            *reinterpret_cast<void***>(rdx8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx8)) ^ 1);
            ++rdx8;
        } while (rdx8 != rax9);
    }
    return;
}