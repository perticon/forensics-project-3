int64_t set_initialize (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12d = esi;
    rbx = rdx;
    *((rdi + 0x10)) = 0xfffffffffffffffe;
    while (eax != 0xffffffff) {
        rax = (int64_t) eax;
        *((rbx + rax)) = 1;
        esi = 0;
        rdi = rbp;
        eax = get_next ();
    }
    if (r12b == 0) {
        goto label_0;
    }
    rdx = rbx;
    rax = rbx + 0x100;
    do {
        *(rdx) ^= 1;
        rdx++;
    } while (rdx != rax);
label_0:
    return rax;
}