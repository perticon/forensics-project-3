Eq_15 set_initialize(struct Eq_822 * rdx, word32 esi, struct Eq_469 * rdi, ptr64 & r14Out)
{
	rdi->qw0010 = ~0x01;
	byte r12b_46 = (byte) esi;
	struct Eq_469 * rbp_14 = rdi;
	while (true)
	{
		ptr64 r14_28;
		Eq_15 rax_29 = get_next(0x00, rbp_14, out rbp_14, out r14_28);
		word32 eax_39 = (word32) rax_29;
		if (eax_39 == ~0x00)
			break;
		rdx[(int64) eax_39] = (struct Eq_822) 0x01;
	}
	if (r12b_46 != 0x00)
	{
		struct Eq_822 * rdx_50 = rdx;
		rax_29 = rdx + 0x0100;
		do
		{
			rdx_50->a0000[0] = (byte) (rdx_50->a0000[0] ^ 0x01);
			++rdx_50;
		} while (rdx_50 != rdx + 0x0100);
	}
	r14Out = r14_28;
	return rax_29;
}