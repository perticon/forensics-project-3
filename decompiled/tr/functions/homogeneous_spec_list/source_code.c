homogeneous_spec_list (struct Spec_list *s)
{
  int b, c;

  s->state = BEGIN_STATE;

  if ((b = get_next (s, NULL)) == -1)
    return false;

  while ((c = get_next (s, NULL)) != -1)
    if (c != b)
      return false;

  return true;
}