char * make_printable_str(byte *param_1,long param_2)

{
  byte bVar1;
  char *pcVar2;
  char *__dest;
  ushort **ppuVar3;
  byte *pbVar4;
  byte *__src;
  long in_FS_OFFSET;
  byte local_45;
  undefined local_44;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  pcVar2 = (char *)xnmalloc(param_2 + 1,4);
  if (param_2 != 0) {
    pbVar4 = param_1 + param_2;
    __dest = pcVar2;
    do {
      bVar1 = *param_1;
      if ((char)bVar1 < '\x0e') {
        if ((char)bVar1 < '\a') {
LAB_0010377d:
          ppuVar3 = __ctype_b_loc();
          if ((*(byte *)((long)*ppuVar3 + (ulong)bVar1 * 2 + 1) & 0x40) == 0) {
            __src = &local_45;
            __sprintf_chk(__src,1,5,"\\%03o");
          }
          else {
            __src = &local_45;
            local_44 = 0;
            local_45 = bVar1;
          }
        }
        else {
          __src = &DAT_0010901e;
          switch(bVar1) {
          case 8:
            __src = &DAT_0010900c;
            break;
          case 9:
            __src = &DAT_00109018;
            break;
          case 10:
            __src = &DAT_00109012;
            break;
          case 0xb:
            __src = &DAT_0010901b;
            break;
          case 0xc:
            __src = &DAT_0010900f;
            break;
          case 0xd:
            __src = &DAT_00109015;
          }
        }
      }
      else {
        __src = &DAT_0010900a;
        if (bVar1 != 0x5c) goto LAB_0010377d;
      }
      param_1 = param_1 + 1;
      __dest = stpcpy(__dest,(char *)__src);
    } while (pbVar4 != param_1);
  }
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return pcVar2;
}