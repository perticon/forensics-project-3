void** make_printable_str(void** rdi, void** rsi, ...) {
    void** r14_3;
    void** rdi4;
    int64_t rax5;
    int64_t v6;
    void** rax7;
    void*** rsp8;
    void** v9;
    int64_t rax10;
    void** rbx11;
    void** r15_12;
    uint32_t r8d13;
    void** rax14;
    void*** rax15;
    int64_t rax16;
    uint32_t r8d17;
    int64_t rdi18;
    int64_t r8_19;

    r14_3 = rdi;
    rdi4 = rsi + 1;
    rax5 = g28;
    v6 = rax5;
    rax7 = xnmalloc();
    rsp8 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8);
    v9 = rax7;
    if (!rsi) {
        addr_36f4_2:
        rax10 = v6 - g28;
        if (!rax10) {
            return v9;
        }
    } else {
        rbx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(r14_3));
        r15_12 = rax7;
        while (1) {
            r8d13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3));
            if (*reinterpret_cast<signed char*>(&r8d13) > reinterpret_cast<signed char>(13)) {
                if (*reinterpret_cast<unsigned char*>(&r8d13) == 92) {
                    addr_36e0_7:
                    rdi4 = r15_12;
                    ++r14_3;
                    rax14 = fun_2430();
                    rsp8 = rsp8 - 8 + 8;
                    r15_12 = rax14;
                    if (rbx11 == r14_3) 
                        goto addr_36f4_2;
                } else {
                    addr_377d_8:
                    rax15 = fun_26a0(rdi4, rdi4);
                    rsp8 = rsp8 - 8 + 8;
                    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<unsigned char*>(&r8d13);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(*rax15) + reinterpret_cast<uint64_t>(rax16 * 2) + 1) & 64)) {
                        fun_26b0(rsp8 + 19, 1, 5, "\\%03o", rax16);
                        rsp8 = rsp8 - 8 + 8;
                        goto addr_36e0_7;
                    } else {
                        goto addr_36e0_7;
                    }
                }
            } else {
                if (*reinterpret_cast<signed char*>(&r8d13) <= reinterpret_cast<signed char>(6)) 
                    goto addr_377d_8;
                r8d17 = r8d13 - 8;
                if (*reinterpret_cast<unsigned char*>(&r8d17) > 5) 
                    goto addr_36e0_7; else 
                    goto addr_36c5_13;
            }
        }
    }
    fun_2470();
    if (*reinterpret_cast<uint32_t*>(&rdi4) <= 11) 
        goto addr_37fa_16;
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    addr_37fa_16:
    *reinterpret_cast<uint32_t*>(&rdi18) = *reinterpret_cast<uint32_t*>(&rdi4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9278 + rdi18 * 4) + 0x9278;
    addr_36c5_13:
    *reinterpret_cast<uint32_t*>(&r8_19) = *reinterpret_cast<unsigned char*>(&r8d17);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_19) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9260 + r8_19 * 4) + 0x9260;
}