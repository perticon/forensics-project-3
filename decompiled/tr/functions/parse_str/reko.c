uint64 parse_str(Eq_15 rdi, struct Eq_435 * fs, union Eq_560 & ebxOut, byte & r12Out, union Eq_15 & r13Out)
{
	ptr64 fp;
	Eq_15 r13;
	byte * r12;
	byte bLoc78;
	word64 rax_28 = fs->qw0028;
	Eq_15 rax_35 = fn0000000000002460(rdi);
	Eq_15 rax_38 = xmalloc(rax_35);
	Eq_15 rax_48 = xcalloc(0x01, rax_35);
	struct Eq_471 * rsp_154 = fp - 0xB8;
	uint64 rsi_44 = 0x01;
	Eq_15 rdi_446 = rax_35;
	Eq_15 rbp_146 = rax_38;
	byte bl_1009 = *rdi;
	Eq_15 r14_234 = rax_48;
	if (bl_1009 != 0x00)
	{
		Eq_2609 rcx_104 = 0x00;
		word32 r15d_1006 = 0x00;
		word32 r11_458[] = g_a92D0;
		do
		{
			uint64 rax_71 = (uint64) r15d_1006;
			word32 ecx_440 = (word32) rcx_104;
			Eq_15 r15_73 = (uint64) (r15d_1006 + 0x01);
			byte * r12_75 = (word64) rax_38 + rax_71;
			if (bl_1009 == 0x5C)
			{
				Eq_2650 r10_1393 = (uint64) ((word32) rcx_104 + 0x01);
				word64 r9_87 = CONVERT(Mem85[rdi + r10_1393:byte], byte, uint64);
				byte * rbx_83 = (word64) rax_48 + rax_71;
				*rbx_83 = 0x01;
				word32 r9d_96 = (word32) r9_87;
				if ((byte) r9_87 != 0x00)
				{
					cu8 dl_99 = (byte) r9_87 - 0x30;
					bl_1009 = (byte) r9d_96;
					if (dl_99 <= 0x46)
					{
						<anonymous> * rax_985 = r11_458 + (int64) r11_458[(uint64) dl_99] /64 4;
						uint64 rax_998;
						word64 rcx_999;
						word64 r11_1000;
						word64 rdx_1001;
						word64 r9_1002;
						word64 r10_1003;
						word64 r8_1004;
						rax_985();
						ebxOut.u1 = <invalid>;
						r12Out = r12_75;
						r13Out = rdi;
						return rax_998;
					}
				}
				else
				{
					fn0000000000002600(fn0000000000002440(0x05, "warning: an unescaped backslash at end of string is not portable", null), 0x00, 0x00);
					*rbx_83 = 0x00;
					rsi_44 = 0x00;
					rdi_446.u0 = 0x00;
					r10_1393.u1 = (uint64) ecx_440;
					bl_1009 = 0x5C;
					r11_458 = g_a92D0;
				}
				rcx_104.u1 = (uint64) (word32) r10_1393;
			}
			*r12_75 = bl_1009;
			Eq_2609 rax_107 = (uint64) ((word32) rcx_104 + 0x01);
			r15d_1006 = (word32) r15_73;
			bl_1009 = Mem112[rdi + rax_107:byte];
			rcx_104 = rax_107;
		} while (bl_1009 != 0x00);
		Eq_15 r13_131;
		if (r15_73 > 0x02)
		{
			Eq_15 r13_135 = r15_73;
			Eq_15 r8_137 = 0x02;
			Eq_15 rcx_139 = 0x00;
			Eq_15 r15_145 = rax_48;
			do
			{
l0000000000004306:
				uint64 r14_153 = (uint64) *((word64) rcx_139 + ((word64) rbp_146 + 1));
				byte r12b_156 = Mem148[rbp_146 + rcx_139:byte];
				rsp_154->t0000 = (word64) rcx_139 + 1;
				Eq_15 r14b_167 = (byte) r14_153;
				if (r12b_156 == 0x5B)
				{
					byte al_162 = Mem155[r15_145 + rcx_139:byte];
					rsp_154->t0040 = al_162;
					if (al_162 == 0x00)
					{
						Eq_15 r12_378;
						Eq_15 rdx_511;
						char * rsi_394;
						if ((r14b_167 == 0x3A || r14b_167 == 0x3D) && Mem163[r15_145 + Mem163[rsp_154 + 0x00:word64]:byte] == 0x00)
						{
							Eq_15 rsi_180 = r13_135 - 1;
							if (r8_137 < rsi_180)
							{
								Eq_15 rax_185 = r8_137;
								do
								{
									Eq_15 rbx_191 = rax_185;
									rax_185 = (word64) rax_185 + 1;
									word32 r14d_228 = (word32) r14_153;
									if (Mem163[rbp_146 + rax_185:byte] == r14b_167 && (*((word64) rax_185 + ((word64) rbp_146 + 1)) == 0x5D && (Mem163[(r15_145 - 1) + rax_185:byte] == 0x00 && Mem163[(r15_145 + 1) + rax_185:byte] == 0x00)))
									{
										Mem219[rsp_154 + 24:word64] = rbp_146 + r8_137;
										word64 rax_216 = rax_185 - rcx_139;
										rsp_154->t0010 = rax_216 - 0x02;
										if (rax_216 == 0x02)
										{
											char * rsi_233 = "missing character class name '[::]'";
											r14_234 = r15_145;
											if ((byte) r14d_228 != 0x3A)
												rsi_233 = (char *) "missing equivalence class character '[==]'";
											fn0000000000002600(fn0000000000002440(0x05, rsi_233, null), 0x00, 0x00);
											goto l0000000000004689;
										}
										Eq_15 rdx_307;
										Eq_15 rax_299;
										if (r14b_167 != 0x3A)
										{
											rsp_154->t0020 = rcx_139;
											if (rsp_154->t0010 == 0x01)
											{
												rax_299 = xmalloc(0x20);
												Eq_15 rdi_406 = rsp_154->t0018;
												Eq_15 rsi_407 = rsp_154->t0008;
												((word64) rax_299 + 8)->u0 = 0x00;
												Eq_15 dl_412 = *rdi_406;
												rax_299->u2 = 0x03;
												*((word64) rax_299 + 16) = dl_412;
												rdx_307 = *((word64) rsi_407 + 8);
												if (rdx_307 != 0x00)
												{
l0000000000004882:
													Eq_15 rdi_420 = rsp_154->t0008;
													*((word64) rdx_307 + 8) = rax_299;
													*((word64) rdi_420 + 8) = rax_299;
													rcx_139 = (word64) rbx_191 + 2;
													goto l00000000000042F9;
												}
												else
													fn00000000000024D0("append_equiv_class", 0x02E7, "src/tr.c", "list->tail");
											}
											rsp_154->t0028 = r8_137;
											byte al_475 = star_digits_closebracket(r8_137, &rsp_154->dw005C + 1);
											r8_137 = rsp_154->t0028;
											rcx_139 = rsp_154->t0020;
											if (al_475 != 0x00)
												break;
											r12_378 = make_printable_str(rsp_154->t0010, rsp_154->t0018, fs, out rbp_146, out r14_234);
											rdx_511 = fn0000000000002440(0x05, "%s: equivalence class operand must be a single character", null);
										}
										else
										{
											rsp_154->t0020 = rbp_146;
											rsp_154->t0030 = r8_137;
											rsp_154->t0038 = rcx_139;
											rsp_154->t0028 = r12b_156;
											Eq_15 r12_271 = rsp_154->t0018;
											rsp_154->t0048 = rax_185;
											Eq_15 rbx_273 = rsp_154->t0010;
											rsp_154->t0047 = r14b_167;
											Eq_15 r14_275 = 0x00;
											do
											{
												Eq_15 rbp_283 = *((char *) g_aDB60 + r14_275 * 0x08);
												if (fn00000000000023C0((word32) rbx_273, rbp_283, r12_271) == 0x00 && rbx_273 == fn0000000000002460(rbp_283))
												{
													rsp_154->t0000 = r14_275;
													rbp_146 = rsp_154->t0020;
													rbx_191 = rsp_154->t0048;
													rax_299 = xmalloc(0x20);
													Eq_15 rdi_304 = rsp_154->t0008;
													Eq_15 r11d_309 = rsp_154->t0000;
													((word64) rax_299 + 8)->u0 = 0x00;
													rdx_307 = *((word64) rdi_304 + 8);
													rax_299->u2 = 0x02;
													*((word64) rax_299 + 16) = r11d_309;
													if (rdx_307 != 0x00)
														goto l0000000000004882;
													fn00000000000024D0("append_char_class", 0x02BD, "src/tr.c", "list->tail");
												}
												r14_275 = (word64) r14_275 + 1;
											} while (r14_275 != 0x0C);
											Eq_15 r8_320 = rsp_154->t0030;
											rbp_146 = rsp_154->t0020;
											r12b_156 = rsp_154->t0028;
											rsp_154->t0028 = rsp_154->t0038;
											r14b_167 = rsp_154->t0047;
											rsp_154->t0020 = r8_320;
											byte al_344 = star_digits_closebracket(r8_320, &rsp_154->dw005C + 1);
											r8_137 = rsp_154->t0020;
											rcx_139 = rsp_154->t0028;
											if (al_344 != 0x00)
												break;
											Eq_15 rax_359 = make_printable_str(rsp_154->t0010, rsp_154->t0018, fs, out rbp_146, out r14_234);
											quote(rax_359, fs);
											r12_378 = rax_359;
											rsi_394 = (char *) "invalid character class %s";
l0000000000004820:
											rdx_511 = fn0000000000002440(0x05, rsi_394, null);
										}
										fn0000000000002600(rdx_511, 0x00, 0x00);
										fn0000000000002390(r12_378);
										goto l0000000000004689;
									}
								} while (rax_185 < rsi_180);
							}
						}
						if (Mem516[rbp_146 + r8_137:byte] == 0x2A && Mem516[r15_145 + r8_137:byte] == 0x00)
						{
							Eq_15 rax_525 = (word64) rcx_139 + 3;
							Eq_15 rbx_526 = rax_525;
							if (r13_135 > rax_525)
							{
								while (Mem516[r15_145 + rbx_526:byte] == 0x00)
								{
									if (Mem516[rbp_146 + rbx_526:byte] == 0x5D)
									{
										Eq_15 rdx_555;
										if (rbx_526 - rsp_154->t0000 == (byte (*)[]) 0x02)
										{
											rsp_154->t0050.u0 = 0x00;
											rdx_555.u0 = 0x00;
											goto l00000000000043F1;
										}
										word64 rdi_560 = rbp_146 + rax_525;
										rsp_154->t0000 = rdi_560;
										uint64 rdx_575 = (uint64) (int8) (*rdi_560 != 0x30);
										struct Eq_471 * r14_582 = (struct Eq_471 *) <invalid>;
										rsp_154 = (struct Eq_471 *) <invalid>;
										Eq_15 r12_588;
										byte r14b_1397;
										word32 eax_600 = xstrtoumax(&rsp_154->t0050, (word32) rdx_575 + 0x08 + (word32) rdx_575, &rsp_154->t0058, rdi_560, null, r15_145, fs, out rbx_526, out rbp_146, out r12_588, out r13_135, out r14b_1397, out r15_145);
										r14b_167 = (byte) r14_582;
										Eq_15 rdi_599 = rsp_154->t0000;
										if (eax_600 == 0x00)
										{
											rdx_555 = rsp_154->t0050;
											if (rdx_555 != ~0x00 && rsp_154->t0058 == rdi_599)
											{
l00000000000043F1:
												rsp_154->t0000 = rdx_555;
												Eq_15 rax_682 = xmalloc(0x20);
												Eq_15 rdx_687 = rsp_154->t0000;
												Eq_15 rdi_688 = rsp_154->t0008;
												((word64) rax_682 + 8)->u0 = 0x00;
												*((word64) rax_682 + 24) = rdx_687;
												Eq_15 rdx_691 = *((word64) rdi_688 + 8);
												rax_682->u2 = 0x04;
												*((word64) rax_682 + 16) = r14b_167;
												if (rdx_691 == 0x00)
													append_repeated_char.part.0();
												else
												{
													Eq_15 rsi_704 = rsp_154->t0008;
													*((word64) rdx_691 + 8) = rax_682;
													rcx_139 = rbx_526 + 1;
													*((word64) rsi_704 + 8) = rax_682;
													r8_137 = rcx_139 + 2;
													if (r13_135 <=u rcx_139 + 2)
														goto l0000000000004449;
													goto l0000000000004306;
												}
											}
										}
										Eq_15 rax_616 = make_printable_str(r12_588, rdi_599, fs, out rbp_146, out r14_234);
										quote(rax_616, fs);
										r12_378 = rax_616;
										rsi_394 = (char *) "invalid repeat count %s in [c*n] construct";
										goto l0000000000004820;
									}
									++rbx_526;
									if (r13_135 <= rbx_526)
										break;
								}
							}
						}
					}
				}
				if (r14b_167 != 0x2D)
				{
l00000000000042BA:
					Eq_15 rax_816 = xmalloc(0x20);
					Eq_15 rdi_823 = rsp_154->t0008;
					((word64) rax_816 + 8)->u0 = 0x00;
					Eq_15 rdx_825 = *((word64) rdi_823 + 8);
					rax_816->u2 = 0x00;
					*((word64) rax_816 + 16) = r12b_156;
					if (rdx_825 == 0x00)
					{
l0000000000004A4D:
						fn00000000000024D0("append_normal_char", 0x0289, "src/tr.c", "list->tail");
					}
					Eq_15 rdi_832 = rsp_154->t0008;
					rcx_139 = rsp_154->t0000;
					*((word64) rdx_825 + 8) = rax_816;
					*((word64) rdi_832 + 8) = rax_816;
					goto l00000000000042F9;
				}
				word64 rax_721 = CONVERT(Mem717[r15_145 + Mem717[rsp_154 + 0x00:word64]:byte], byte, uint64);
				Eq_706 al_722 = (byte) rax_721;
				rsp_154->t0040 = al_722;
				if (al_722 != 0x00)
					goto l00000000000042BA;
				byte bl_732 = Mem723[rbp_146 + r8_137:byte];
				if (bl_732 < r12b_156)
				{
					make_printable_char((word32) r12b_156);
					make_printable_char((word32) bl_732);
					fn0000000000002600(fn0000000000002440(0x05, "range-endpoints of '%s-%s' are in reverse collating sequence order", null), 0x00, 0x00);
					fn0000000000002390(rax_721);
					fn0000000000002390(rax_721);
					r14_234 = r15_145;
					goto l0000000000004689;
				}
				rsp_154->t0000 = rcx_139;
				Eq_15 rax_794 = xmalloc(0x20);
				Eq_15 rdi_799 = rsp_154->t0008;
				Eq_15 rcx_800 = rsp_154->t0000;
				((word64) rax_794 + 8)->u0 = 0x00;
				Eq_15 rdx_802 = *((word64) rdi_799 + 8);
				rax_794->u2 = 0x01;
				*((word64) rax_794 + 16) = r12b_156;
				*((word64) rax_794 + 0x0011) = bl_732;
				if (rdx_802 == 0x00)
					fn00000000000024D0("append_range", 0x02A7, "src/tr.c", "list->tail");
				Eq_15 rdi_809 = rsp_154->t0008;
				*((word64) rdx_802 + 8) = rax_794;
				*((word64) rdi_809 + 8) = rax_794;
				rcx_139 = (word64) rcx_800 + 3;
l00000000000042F9:
				r8_137 = (word64) rcx_139 + 2;
			} while (r13_135 > r8_137);
l0000000000004449:
			r14_234 = r15_145;
			r15_73 = r13_135;
			r13_131 = rcx_139;
		}
		else
			r13_131.u0 = 0x00;
		word64 r12_863 = rbp_146 + r13_131;
		word64 rbx_865 = rbp_146 + r15_73;
		if (r15_73 > r13_131)
		{
			Eq_15 r13_874 = rsp_154->t0008;
			do
			{
				Eq_15 r15b_890 = *r12_863;
				Eq_15 rax_881 = xmalloc(0x20);
				Eq_15 rdx_887 = *((word64) r13_874 + 8);
				((word64) rax_881 + 8)->u0 = 0x00;
				rax_881->u2 = 0x00;
				*((word64) rax_881 + 16) = r15b_890;
				if (rdx_887 == 0x00)
					goto l0000000000004A4D;
				*((word64) rdx_887 + 8) = rax_881;
				*((word64) r13_874 + 8) = rax_881;
				++r12_863;
			} while (rbx_865 != r12_863);
		}
	}
	rsp_154->t0040.u1 = 0x01;
l0000000000004689:
	fn0000000000002390(rbp_146);
	fn0000000000002390(r14_234);
	if (rax_28 - fs->qw0028 != 0x00)
		fn0000000000002470();
	else
	{
		ebxOut.u1 = <invalid>;
		r12Out = r12;
		r13Out = r13;
		return (uint64) bLoc78;
	}
}