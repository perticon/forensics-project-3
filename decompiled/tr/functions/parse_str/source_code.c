parse_str (char const *s, struct Spec_list *spec_list)
{
  struct E_string es;
  bool ok = unquote (s, &es) && build_spec_list (&es, spec_list);
  es_free (&es);
  return ok;
}