signed char parse_str(void** rdi, void** rsi) {
    void** r13_3;
    void** v4;
    int64_t rax5;
    int64_t v6;
    void** rax7;
    void** rax8;
    void** rsi9;
    void** rbp10;
    void** rax11;
    void*** rsp12;
    uint32_t ebx13;
    void** r14_14;
    void** rcx15;
    void** r15_16;
    void** rdi17;
    void** r13_18;
    unsigned char* r12_19;
    unsigned char* rbx20;
    unsigned char v21;
    void** r13_22;
    uint32_t r15d23;
    void** rax24;
    void** rdx25;
    int64_t rax26;
    uint32_t eax27;
    void** rcx28;
    signed char* r12_29;
    void** rdx30;
    void** rax31;
    uint32_t ebx32;
    void** rax33;
    void** rax34;
    void** r12_35;
    void** rax36;
    void** r12_37;
    void** v38;
    void** v39;
    void** rax40;
    void** rax41;
    int64_t rax42;
    void** rax43;
    void** r8_44;
    void** v45;
    uint32_t eax46;
    uint32_t eax47;
    void** rax48;
    void** rax49;
    void** rbx50;
    void** rax51;
    void** rbx52;
    int32_t eax53;
    void* v54;
    void** rax55;
    void** rax56;
    void** v57;
    void** v58;
    void** v59;
    unsigned char v60;
    void** r12_61;
    void** v62;
    void** rbx63;
    unsigned char v64;
    void** r14_65;
    void** rbp66;
    int32_t eax67;
    void** rax68;
    void** rax69;
    uint32_t edx70;
    signed char al71;
    void** r11_72;
    struct s0* rdi73;
    signed char al74;
    void* rax75;
    void* rdx76;
    int64_t r9_77;
    void* r10_78;
    void** rax79;
    uint32_t r9d80;

    r13_3 = rdi;
    v4 = rsi;
    rax5 = g28;
    v6 = rax5;
    rax7 = fun_2460(rdi);
    rax8 = xmalloc(rax7);
    *reinterpret_cast<int32_t*>(&rsi9) = 1;
    *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
    rbp10 = rax8;
    rax11 = xcalloc(rax7, 1);
    rsp12 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88 - 8 + 8 - 8 + 8 - 8 + 8);
    ebx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_3));
    r14_14 = rax11;
    if (*reinterpret_cast<signed char*>(&ebx13)) {
        rcx15 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r15_16) = 0;
        goto addr_422c_3;
    }
    addr_4a0f_4:
    fun_24d0("list->tail", "src/tr.c", 0x2bd, "append_char_class", "list->tail", "src/tr.c", 0x2bd, "append_char_class");
    addr_4a2e_5:
    fun_24d0("list->tail", "src/tr.c", 0x2a7, "append_range", "list->tail", "src/tr.c", 0x2a7, "append_range");
    addr_4a4d_6:
    rdi17 = reinterpret_cast<void**>("list->tail");
    fun_24d0("list->tail", "src/tr.c", 0x289, "append_normal_char", "list->tail", "src/tr.c", 0x289, "append_normal_char");
    addr_4a6c_7:
    append_repeated_char_part_0(rdi17, rdi17);
    addr_4a02_9:
    *reinterpret_cast<int32_t*>(&r13_18) = 0;
    *reinterpret_cast<int32_t*>(&r13_18 + 4) = 0;
    addr_4452_10:
    r12_19 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r13_18));
    rbx20 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r15_16));
    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(r13_18)) {
        addr_44af_11:
        v21 = 1;
    } else {
        r13_22 = v4;
        do {
            r15d23 = *r12_19;
            rax24 = xmalloc(32, 32);
            rdx25 = *reinterpret_cast<void***>(r13_22 + 8);
            *reinterpret_cast<void***>(rax24 + 8) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<unsigned char*>(rax24 + 16) = *reinterpret_cast<unsigned char*>(&r15d23);
            if (!rdx25) 
                goto addr_4a4d_6;
            ++r12_19;
            *reinterpret_cast<void***>(rdx25 + 8) = rax24;
            *reinterpret_cast<void***>(r13_22 + 8) = rax24;
        } while (rbx20 != r12_19);
        goto addr_44af_11;
    }
    addr_4689_15:
    fun_2390(rbp10, rbp10);
    fun_2390(r14_14, r14_14);
    rax26 = v6 - g28;
    if (rax26) {
        fun_2470();
        goto addr_4a0f_4;
    } else {
        eax27 = v21;
        return *reinterpret_cast<signed char*>(&eax27);
    }
    addr_4449_18:
    r14_14 = r15_16;
    r15_16 = r13_3;
    r13_18 = rcx28;
    goto addr_4452_10;
    addr_4638_19:
    r14_14 = r15_16;
    rax31 = make_printable_char(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_29)), rsi9, rdx30);
    rax33 = make_printable_char(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ebx32)), rsi9, rdx30);
    fun_2440();
    fun_2600();
    fun_2390(rax31, rax31);
    fun_2390(rax33, rax33);
    goto addr_4689_15;
    addr_499f_20:
    r14_14 = r15_16;
    rax36 = make_printable_str(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rax34), r12_35, reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rax34), r12_35);
    r12_37 = rax36;
    quote(rax36, rax36);
    addr_4820_21:
    fun_2440();
    addr_461d_22:
    fun_2600();
    fun_2390(r12_37);
    goto addr_4689_15;
    addr_49e8_23:
    fun_2440();
    fun_2600();
    goto addr_4689_15;
    addr_47f4_24:
    r14_14 = r15_16;
    rax40 = make_printable_str(v38, v39, v38, v39);
    r12_37 = rax40;
    quote(rax40, rax40);
    goto addr_4820_21;
    addr_45ef_25:
    r14_14 = r15_16;
    rax41 = make_printable_str(v38, v39);
    r12_37 = rax41;
    fun_2440();
    goto addr_461d_22;
    addr_4265_26:
    *reinterpret_cast<uint32_t*>(&rax42) = *reinterpret_cast<unsigned char*>(&rdx30);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax42) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x92d0 + rax42 * 4) + 0x92d0;
    while (1) {
        *reinterpret_cast<int32_t*>(&rax43) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx15 + 1));
        *reinterpret_cast<int32_t*>(&rax43 + 4) = 0;
        *r12_29 = *reinterpret_cast<signed char*>(&ebx13);
        ebx13 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_3) + reinterpret_cast<unsigned char>(rax43));
        rcx15 = rax43;
        if (!*reinterpret_cast<signed char*>(&ebx13)) {
            if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(2)) 
                goto addr_4a02_9;
            r13_3 = r15_16;
            *reinterpret_cast<int32_t*>(&r8_44) = 2;
            *reinterpret_cast<int32_t*>(&r8_44 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx28) = 0;
            *reinterpret_cast<int32_t*>(&rcx28 + 4) = 0;
            r15_16 = r14_14;
            do {
                addr_4306_30:
                *reinterpret_cast<uint32_t*>(&r12_29) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rcx28));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_29) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r14_14) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rcx28) + 1);
                *reinterpret_cast<int32_t*>(&r14_14 + 4) = 0;
                v45 = rcx28 + 1;
                if (*reinterpret_cast<unsigned char*>(&r12_29) != 91) 
                    goto addr_42b0_31;
                eax46 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(rcx28));
                v21 = *reinterpret_cast<unsigned char*>(&eax46);
                if (*reinterpret_cast<unsigned char*>(&eax46)) 
                    goto addr_42b0_31;
                if (*reinterpret_cast<unsigned char*>(&r14_14) != 58 && *reinterpret_cast<unsigned char*>(&r14_14) != 61) {
                    goto addr_4348_35;
                }
                if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(v45)) || (rsi9 = r13_3 + 0xffffffffffffffff, reinterpret_cast<unsigned char>(r8_44) >= reinterpret_cast<unsigned char>(rsi9))) {
                    addr_4348_35:
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r8_44)) != 42 || *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(r8_44))) {
                        addr_42b0_31:
                        if (*reinterpret_cast<unsigned char*>(&r14_14) != 45 || (eax47 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(v45)), v21 = *reinterpret_cast<unsigned char*>(&eax47), !!*reinterpret_cast<unsigned char*>(&eax47))) {
                            rax48 = xmalloc(32, 32);
                            rsp12 = rsp12 - 8 + 8;
                            *reinterpret_cast<void***>(rax48 + 8) = reinterpret_cast<void**>(0);
                            rdx30 = *reinterpret_cast<void***>(v4 + 8);
                            *reinterpret_cast<void***>(rax48) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<unsigned char*>(rax48 + 16) = *reinterpret_cast<unsigned char*>(&r12_29);
                            if (!rdx30) 
                                goto addr_4a4d_6;
                            rcx28 = v45;
                            *reinterpret_cast<void***>(rdx30 + 8) = rax48;
                            *reinterpret_cast<void***>(v4 + 8) = rax48;
                            continue;
                        } else {
                            ebx32 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r8_44));
                            if (*reinterpret_cast<unsigned char*>(&ebx32) < *reinterpret_cast<unsigned char*>(&r12_29)) 
                                goto addr_4638_19;
                            rax49 = xmalloc(32, 32);
                            rsp12 = rsp12 - 8 + 8;
                            *reinterpret_cast<void***>(rax49 + 8) = reinterpret_cast<void**>(0);
                            rdx30 = *reinterpret_cast<void***>(v4 + 8);
                            *reinterpret_cast<void***>(rax49) = reinterpret_cast<void**>(1);
                            *reinterpret_cast<unsigned char*>(rax49 + 16) = *reinterpret_cast<unsigned char*>(&r12_29);
                            *reinterpret_cast<unsigned char*>(rax49 + 17) = *reinterpret_cast<unsigned char*>(&ebx32);
                            if (!rdx30) 
                                goto addr_4a2e_5;
                            *reinterpret_cast<void***>(rdx30 + 8) = rax49;
                            rcx28 = rcx28 + 3;
                            *reinterpret_cast<void***>(v4 + 8) = rax49;
                            continue;
                        }
                    } else {
                        rax34 = rcx28 + 3;
                        rbx50 = rax34;
                        if (reinterpret_cast<unsigned char>(r13_3) > reinterpret_cast<unsigned char>(rax34)) {
                            do {
                                if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(rbx50))) 
                                    goto addr_42b0_31;
                                if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rbx50)) == 93) 
                                    break;
                                ++rbx50;
                            } while (reinterpret_cast<unsigned char>(r13_3) > reinterpret_cast<unsigned char>(rbx50));
                            goto addr_42b0_31;
                        } else {
                            goto addr_42b0_31;
                        }
                    }
                } else {
                    rax51 = r8_44;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx30) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rax51));
                        *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
                        rbx52 = rax51;
                        ++rax51;
                        if (*reinterpret_cast<unsigned char*>(&rdx30) != *reinterpret_cast<unsigned char*>(&r14_14)) 
                            continue;
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rbx52) + 1) != 93) 
                            continue;
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(rax51) + 0xffffffffffffffff)) 
                            continue;
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(rbx52) + 1)) 
                            goto addr_4591_52;
                    } while (reinterpret_cast<unsigned char>(rax51) < reinterpret_cast<unsigned char>(rsi9));
                    goto addr_4348_35;
                }
                r12_35 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx50) - reinterpret_cast<unsigned char>(v45) - 2);
                if (r12_35) {
                    eax53 = xstrtoumax();
                    rsp12 = rsp12 - 8 + 8;
                    if (eax53) 
                        goto addr_499f_20;
                    if (0) 
                        goto addr_499f_20;
                    if (!reinterpret_cast<int1_t>(v54 == reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rax34)) + reinterpret_cast<unsigned char>(r12_35))) 
                        goto addr_499f_20;
                }
                rax55 = xmalloc(32, 32);
                rsp12 = rsp12 - 8 + 8;
                rdi17 = v4;
                *reinterpret_cast<void***>(rax55 + 8) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rax55 + 24) = reinterpret_cast<void**>(0);
                rdx30 = *reinterpret_cast<void***>(rdi17 + 8);
                *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(4);
                *reinterpret_cast<unsigned char*>(rax55 + 16) = *reinterpret_cast<unsigned char*>(&r14_14);
                if (!rdx30) 
                    goto addr_4a6c_7;
                rsi9 = v4;
                rcx28 = rbx50 + 1;
                *reinterpret_cast<void***>(rdx30 + 8) = rax55;
                r8_44 = rcx28 + 2;
                *reinterpret_cast<void***>(rsi9 + 8) = rax55;
                if (reinterpret_cast<unsigned char>(r13_3) > reinterpret_cast<unsigned char>(r8_44)) 
                    goto addr_4306_30; else 
                    goto addr_4449_18;
                addr_4591_52:
                v38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r8_44));
                rax56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx52) - reinterpret_cast<unsigned char>(rcx28) - 2);
                v39 = rax56;
                if (!rax56) 
                    goto addr_49c9_61;
                if (*reinterpret_cast<unsigned char*>(&r14_14) == 58) {
                    v57 = rbp10;
                    v58 = r8_44;
                    v59 = rcx28;
                    v60 = *reinterpret_cast<unsigned char*>(&r12_29);
                    r12_61 = v38;
                    v62 = rbx52;
                    rbx63 = v39;
                    v64 = *reinterpret_cast<unsigned char*>(&r14_14);
                    r14_65 = reinterpret_cast<void**>(0);
                    do {
                        rdx30 = rbx63;
                        rbp66 = *reinterpret_cast<void***>(0xdb60 + reinterpret_cast<unsigned char>(r14_65) * 8);
                        rsi9 = rbp66;
                        eax67 = fun_23c0(r12_61, rsi9, rdx30, rcx28);
                        rsp12 = rsp12 - 8 + 8;
                        if (eax67) 
                            continue;
                        rax68 = fun_2460(rbp66, rbp66);
                        rsp12 = rsp12 - 8 + 8;
                        if (rbx63 == rax68) 
                            break;
                        ++r14_65;
                    } while (!reinterpret_cast<int1_t>(r14_65 == 12));
                    goto addr_47b0_67;
                } else {
                    if (v39 == 1) {
                        rax69 = xmalloc(32);
                        rsp12 = rsp12 - 8 + 8;
                        rsi9 = v4;
                        *reinterpret_cast<void***>(rax69 + 8) = reinterpret_cast<void**>(0);
                        edx70 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v38));
                        *reinterpret_cast<void***>(rax69) = reinterpret_cast<void**>(3);
                        *reinterpret_cast<unsigned char*>(rax69 + 16) = *reinterpret_cast<unsigned char*>(&edx70);
                        rdx30 = *reinterpret_cast<void***>(rsi9 + 8);
                        if (rdx30) 
                            goto addr_4882_70; else 
                            break;
                    } else {
                        rsi9 = r8_44;
                        al71 = star_digits_closebracket(rsp12 + 96, rsi9);
                        rsp12 = rsp12 - 8 + 8;
                        r8_44 = r8_44;
                        rcx28 = rcx28;
                        if (al71) 
                            goto addr_4348_35; else 
                            goto addr_45ef_25;
                    }
                }
                rbp10 = v57;
                rbx52 = v62;
                rax69 = xmalloc(32, 32);
                rsp12 = rsp12 - 8 + 8;
                r11_72 = r14_65;
                *reinterpret_cast<void***>(rax69 + 8) = reinterpret_cast<void**>(0);
                rdx30 = *reinterpret_cast<void***>(v4 + 8);
                *reinterpret_cast<void***>(rax69) = reinterpret_cast<void**>(2);
                *reinterpret_cast<unsigned char*>(rax69 + 16) = *reinterpret_cast<unsigned char*>(&r11_72);
                if (!rdx30) 
                    goto addr_4a0f_4;
                addr_4882_70:
                *reinterpret_cast<void***>(rdx30 + 8) = rax69;
                rcx28 = rbx52 + 2;
                *reinterpret_cast<void***>(v4 + 8) = rax69;
                continue;
                addr_47b0_67:
                rdi73 = reinterpret_cast<struct s0*>(rsp12 + 96);
                rbp10 = v57;
                *reinterpret_cast<uint32_t*>(&r12_29) = v60;
                rsi9 = v58;
                *reinterpret_cast<uint32_t*>(&r14_14) = v64;
                al74 = star_digits_closebracket(rdi73, rsi9, rdi73, rsi9);
                rsp12 = rsp12 - 8 + 8;
                r8_44 = v58;
                rcx28 = v59;
                if (al74) 
                    goto addr_4348_35; else 
                    goto addr_47f4_24;
                r8_44 = rcx28 + 2;
            } while (reinterpret_cast<unsigned char>(r13_3) > reinterpret_cast<unsigned char>(r8_44));
            goto addr_4449_18;
            rcx15 = reinterpret_cast<void**>("append_equiv_class");
            fun_24d0("list->tail", "src/tr.c", 0x2e7, "append_equiv_class");
            rsp12 = rsp12 - 8 + 8;
        } else {
            addr_422c_3:
            *reinterpret_cast<int32_t*>(&rax75) = *reinterpret_cast<int32_t*>(&r15_16);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax75) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_16) = *reinterpret_cast<int32_t*>(&r15_16) + 1;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            r12_29 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<uint64_t>(rax75));
            if (*reinterpret_cast<signed char*>(&ebx13) != 92) 
                continue;
            *reinterpret_cast<int32_t*>(&rdx76) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx15 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx76) + 4) = 0;
            rbx52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_14) + reinterpret_cast<uint64_t>(rax75));
            *reinterpret_cast<uint32_t*>(&r9_77) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_3) + reinterpret_cast<uint64_t>(rdx76));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_77) + 4) = 0;
            *reinterpret_cast<void***>(rbx52) = reinterpret_cast<void**>(1);
            r10_78 = rdx76;
            if (*reinterpret_cast<signed char*>(&r9_77)) 
                goto addr_4259_76;
        }
        rax79 = fun_2440();
        *reinterpret_cast<int32_t*>(&rsi9) = 0;
        *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
        rdx30 = rax79;
        fun_2600();
        rsp12 = rsp12 - 8 + 8 - 8 + 8;
        *reinterpret_cast<void***>(rbx52) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(&r10_78) = rcx15;
        ebx13 = 92;
        addr_4280_78:
        rcx15 = *reinterpret_cast<void***>(&r10_78);
        *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
        continue;
        addr_4259_76:
        *reinterpret_cast<uint32_t*>(&rdx30) = static_cast<uint32_t>(r9_77 - 48);
        *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
        ebx13 = *reinterpret_cast<uint32_t*>(&r9_77);
        if (*reinterpret_cast<unsigned char*>(&rdx30) > 70) 
            goto addr_4280_78; else 
            goto addr_4265_26;
    }
    addr_49c9_61:
    r9d80 = *reinterpret_cast<uint32_t*>(&r14_14);
    r14_14 = r15_16;
    if (*reinterpret_cast<signed char*>(&r9d80) != 58) {
        goto addr_49e8_23;
    }
}