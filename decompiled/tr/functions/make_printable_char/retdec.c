char * make_printable_char(unsigned char c) {
    int64_t v1 = xmalloc(); // 0x35de
    int64_t v2 = *(int64_t *)function_26a0(); // 0x35ee
    if ((*(char *)(v2 + (2 * (int64_t)c | 1)) & 64) == 0) {
        // 0x3610
        function_26b0();
        return (char *)v1;
    }
    char * result = (char *)v1; // 0x35f8
    *result = c;
    *(char *)(v1 + 1) = 0;
    return result;
}