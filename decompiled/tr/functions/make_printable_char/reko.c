void make_printable_char(word32 edi)
{
	Eq_15 rax_17 = xmalloc(0x05);
	byte bl_24 = (byte) edi;
	if ((*((char *) *fn00000000000026A0() + ((uint64) bl_24 * 0x02 + 1)) & 0x40) == 0x00)
		fn00000000000026B0("\\%03o", 0x05, 0x01, rax_17);
	else
	{
		*rax_17 = bl_24;
		*((word64) rax_17 + 1) = 0x00;
	}
}