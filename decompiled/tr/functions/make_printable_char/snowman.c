void** make_printable_char(uint32_t edi, void** rsi, void** rdx) {
    uint32_t ebx4;
    void** rax5;
    void*** rax6;
    int64_t rdx7;
    int64_t r8_8;

    ebx4 = edi;
    rax5 = xmalloc(5);
    rax6 = fun_26a0(5);
    *reinterpret_cast<uint32_t*>(&rdx7) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ebx4));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(*rax6) + reinterpret_cast<uint64_t>(rdx7 * 2) + 1) & 64)) {
        *reinterpret_cast<uint32_t*>(&r8_8) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ebx4));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_8) + 4) = 0;
        fun_26b0(rax5, 1, 5, "\\%03o", r8_8);
        return rax5;
    } else {
        *reinterpret_cast<void***>(rax5) = *reinterpret_cast<void***>(&ebx4);
        *reinterpret_cast<void***>(rax5 + 1) = reinterpret_cast<void**>(0);
        return rax5;
    }
}