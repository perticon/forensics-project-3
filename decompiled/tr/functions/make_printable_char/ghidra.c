byte * make_printable_char(byte param_1)

{
  byte *pbVar1;
  ushort **ppuVar2;
  
  pbVar1 = (byte *)xmalloc(5);
  ppuVar2 = __ctype_b_loc();
  if ((*(byte *)((long)*ppuVar2 + (ulong)param_1 * 2 + 1) & 0x40) != 0) {
    *pbVar1 = param_1;
    pbVar1[1] = 0;
    return pbVar1;
  }
  __sprintf_chk(pbVar1,1,5,"\\%03o",param_1);
  return pbVar1;
}