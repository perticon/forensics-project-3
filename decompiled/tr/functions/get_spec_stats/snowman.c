void get_spec_stats(void** rdi) {
    void** rax2;
    void** rbp3;
    int64_t rax4;

    *reinterpret_cast<signed char*>(rdi + 48) = reinterpret_cast<signed char>(0);
    rax2 = *reinterpret_cast<void***>(rdi);
    rbp3 = *reinterpret_cast<void***>(rax2 + 8);
    *reinterpret_cast<uint64_t*>(rdi + 32) = 0;
    *reinterpret_cast<signed char*>(rdi + 50) = 0;
    if (!rbp3) {
        *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
        return;
    }
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3)) <= reinterpret_cast<unsigned char>(4)) 
        goto addr_39da_5;
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    addr_39da_5:
    *reinterpret_cast<void***>(&rax4) = *reinterpret_cast<void***>(rbp3);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x92a8 + rax4 * 4) + 0x92a8;
}