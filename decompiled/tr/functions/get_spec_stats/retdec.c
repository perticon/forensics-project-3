int64_t get_spec_stats(int64_t * a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5) {
    int64_t result = (int64_t)a1;
    *(int16_t *)(result + 48) = 0;
    int64_t v1 = *(int64_t *)(result + 8); // 0x39af
    *(int64_t *)(result + 32) = 0;
    *(char *)(result + 50) = 0;
    if (v1 == 0) {
        // 0x3a2c
        *(int64_t *)(result + 24) = 0;
        return result;
    }
    int32_t * v2 = (int32_t *)v1; // 0x39d0
    uint32_t v3 = *v2; // 0x39d0
    int32_t v4 = v3; // 0x39d4
    if (v3 >= 5) {
        get_spec_stats_cold();
        v4 = *v2;
    }
    int32_t v5 = *(int32_t *)(4 * (int64_t)v4 + (int64_t)&g13); // 0x39dd
    return (int64_t)v5 + (int64_t)&g13;
}