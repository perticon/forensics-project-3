int64_t get_spec_stats (int64_t arg_8h, int64_t arg_10h, int64_t arg_11h, int64_t arg_18h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    eax = 0;
    r14d = 0;
    *((rdi + 0x30)) = ax;
    rax = *(rdi);
    *((rsp + 8)) = rdi;
    rbp = *((rax + 8));
    *((rdi + 0x20)) = 0;
    *((rdi + 0x32)) = 0;
    if (rbp == 0) {
        goto label_3;
    }
    r12 = 0x000092a8;
label_0:
    if (*(rbp) > 4) {
        void (*0x26c5)() ();
    }
    eax = *(rbp);
    rax = *((r12 + rax*4));
    rax += r12;
    /* switch table (5 cases) at 0x92a8 */
    eax = void (*rax)() ();
    ecx = *((rbp + 0x10));
    eax = 0;
    ebx = 0;
    do {
        edx = 0;
        dl = (cl == al) ? 1 : 0;
        eax++;
        rbx += rdx;
    } while (eax != 0x100);
    rax = *((rsp + 8));
    *((rax + 0x30)) = 1;
label_1:
    rbx += r14;
    r14 = rbx;
    if (rbx < 0) {
        goto label_4;
    }
    if (rbx == -1) {
        goto label_4;
    }
label_2:
    rbp = *((rbp + 8));
    if (rbp != 0) {
        goto label_0;
    }
label_3:
    rax = *((rsp + 8));
    *((rax + 0x18)) = r14;
    return rax;
    rax = *((rsp + 8));
    r15d = *((rbp + 0x10));
    r13d = 0;
    ebx = 0;
    *((rax + 0x31)) = 1;
    do {
        esi = r13d;
        edi = r15d;
        al = is_char_class_member ();
        rbx -= 0xffffffffffffffff;
        r13d++;
    } while (r13d != 0x100);
    r15d -= 6;
    r15d &= 0xfffffffb;
    if (r15d == 0) {
        goto label_1;
    }
    rax = *((rsp + 8));
    *((rax + 0x32)) = 1;
    goto label_1;
    eax = *((rbp + 0x11));
    edx = *((rbp + 0x10));
    if (al < dl) {
        goto label_5;
    }
    ebx = (int32_t) al;
    eax = ebx;
    eax -= edx;
    ebx = rax + 1;
    rbx = (int64_t) ebx;
    goto label_1;
    ebx = 1;
    goto label_1;
    rbx = *((rbp + 0x18));
    if (rbx != 0) {
        goto label_1;
    }
    rax = *((rsp + 8));
    *((rax + 0x20))++;
    *((rax + 0x28)) = rbp;
    goto label_2;
label_4:
    edx = 5;
    rax = dcgettext (0, "too many characters in set");
    eax = 0;
    error (1, 0, rax);
label_5:
    return assert_fail ("p->u.range.last_char >= p->u.range.first_char", "src/tr.c", 0x4f1, "get_spec_stats");
}