Eq_15 get_spec_stats(struct Eq_469 * rdi, ptr64 & r12Out, union Eq_688 & r14Out)
{
	Eq_688 r14;
	ptr64 r12;
	Eq_15 rbp;
	rdi->t0030.u1 = 0x00;
	Eq_15 rbp_32 = rdi->ptr0000->t0008;
	rdi->qw0020 = 0x00;
	rdi->b0032 = 0x00;
	if (rbp_32 == 0x00)
	{
		rdi->t0018.u0 = 0x00;
		r12Out = r12;
		r14Out = r14;
		return rbp;
	}
	else if (*rbp_32 > 0x04)
		get_spec_stats.cold();
	else
	{
		<anonymous> * rax_60 = (int64) g_a92A8[(uint64) *rbp_32 * 0x04] + 37544;
		rax_60();
		r12Out = 37544;
		r14Out.u0 = 0x00;
		return rbp_32;
	}
}