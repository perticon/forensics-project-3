ulong read_and_xlate(byte *param_1,undefined8 param_2)

{
  ulong uVar1;
  undefined8 uVar2;
  int *piVar3;
  ulong uVar4;
  byte *pbVar5;
  ulong uVar6;
  long lVar7;
  byte *pbVar8;
  
  lVar7 = 0;
  pbVar5 = param_1;
  uVar1 = safe_read(0,param_1,param_2);
  if (uVar1 != 0xffffffffffffffff) {
    pbVar5 = param_1;
    if (uVar1 != 0) {
      do {
        pbVar8 = pbVar5 + 1;
        *pbVar5 = xlate[*pbVar5];
        pbVar5 = pbVar8;
      } while (param_1 + uVar1 != pbVar8);
    }
    return uVar1;
  }
  plain_read_part_0();
  while( true ) {
    uVar1 = safe_read(0,lVar7,pbVar5);
    if (uVar1 == 0xffffffffffffffff) {
      uVar2 = dcgettext(0,"read error",5);
      piVar3 = __errno_location();
                    /* WARNING: Subroutine does not return */
      error(1,*piVar3,uVar2);
    }
    if (uVar1 == 0) break;
    uVar4 = 0;
    do {
      uVar6 = uVar4;
      uVar4 = uVar6 + 1;
      if (in_delete_set[*(byte *)(lVar7 + uVar6)] != '\0') goto joined_r0x00104115;
    } while (uVar1 != uVar4);
    uVar4 = uVar6 + 2;
    uVar6 = uVar1;
    if (uVar1 <= uVar4) {
      return uVar1;
    }
    do {
      if (in_delete_set[*(byte *)(lVar7 + uVar4)] == '\0') {
        *(byte *)(lVar7 + uVar6) = *(byte *)(lVar7 + uVar4);
        uVar6 = uVar6 + 1;
      }
      uVar4 = uVar4 + 1;
joined_r0x00104115:
    } while (uVar4 < uVar1);
    if (uVar6 != 0) {
      return uVar6;
    }
  }
  return 0;
}