read_and_xlate (char *buf, size_t size)
{
  size_t bytes_read = plain_read (buf, size);

  for (size_t i = 0; i < bytes_read; i++)
    buf[i] = xlate[to_uchar (buf[i])];

  return bytes_read;
}