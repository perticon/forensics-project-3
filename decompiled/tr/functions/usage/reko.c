void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002660(fn0000000000002440(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000004AD9;
	}
	fn00000000000025F0(fn0000000000002440(0x05, "Usage: %s [OPTION]... STRING1 [STRING2]\n", null), 0x01);
	fn0000000000002530(stdout, fn0000000000002440(0x05, "Translate, squeeze, and/or delete characters from standard input,\nwriting to standard output.  STRING1 and STRING2 specify arrays of\ncharacters ARRAY1 and ARRAY2 that control the action.\n\n  -c, -C, --complement    use the complement of ARRAY1\n  -d, --delete            delete characters in ARRAY1, do not translate\n  -s, --squeeze-repeats   replace each sequence of a repeated character\n                            that is listed in the last specified ARRAY,\n                            with a single occurrence of that character\n  -t, --truncate-set1     first truncate ARRAY1 to length of ARRAY2\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "\nARRAYs are specified as strings of characters.  Most represent themselves.\nInterpreted sequences are:\n\n  \\NNN            character with octal value NNN (1 to 3 octal digits)\n  \\\\              backslash\n  \\a              audible BEL\n  \\b              backspace\n  \\f              form feed\n  \\n              new line\n  \\r              return\n  \\t              horizontal tab\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "  \\v              vertical tab\n  CHAR1-CHAR2     all characters from CHAR1 to CHAR2 in ascending order\n  [CHAR*]         in ARRAY2, copies of CHAR until length of ARRAY1\n  [CHAR*REPEAT]   REPEAT copies of CHAR, REPEAT octal if starting with 0\n  [:alnum:]       all letters and digits\n  [:alpha:]       all letters\n  [:blank:]       all horizontal whitespace\n  [:cntrl:]       all control characters\n  [:digit:]       all digits\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "  [:graph:]       all printable characters, not including space\n  [:lower:]       all lower case letters\n  [:print:]       all printable characters, including space\n  [:punct:]       all punctuation characters\n  [:space:]       all horizontal or vertical whitespace\n  [:upper:]       all upper case letters\n  [:xdigit:]      all hexadecimal digits\n  [=CHAR=]        all characters which are equivalent to CHAR\n", null));
	fn0000000000002530(stdout, fn0000000000002440(0x05, "\nTranslation occurs if -d is not given and both STRING1 and STRING2 appear.\n-t may be used only when translating.  ARRAY2 is extended to length of\nARRAY1 by repeating its last character as necessary.  Excess characters\nof ARRAY2 are ignored.  Character classes expand in unspecified order;\nwhile translating, [:lower:] and [:upper:] may be used in pairs to\nspecify case conversion.  Squeezing occurs after translation or deletion.\n", null));
	struct Eq_3485 * rdx_169 = fp + ~0xA7;
	do
	{
		struct Eq_3504 * rax_193 = rdx_169[1];
		++rdx_169;
	} while (rax_193 != null && ((word32) rax_193->b0000 != 116 || ((word32) rax_193->b0001 != 114 || rax_193->b0002 != 0x00)));
	ptr64 r12_214 = rdx_169->qw0008;
	if (r12_214 != 0x00)
	{
		fn00000000000025F0(fn0000000000002440(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_15 rax_301 = fn00000000000025E0(null, 0x05);
		if (rax_301 == 0x00 || fn00000000000023C0(0x03, 0x9115, rax_301) == 0x00)
			goto l0000000000004D36;
	}
	else
	{
		fn00000000000025F0(fn0000000000002440(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_15 rax_243 = fn00000000000025E0(null, 0x05);
		if (rax_243 == 0x00 || fn00000000000023C0(0x03, 0x9115, rax_243) == 0x00)
		{
			fn00000000000025F0(fn0000000000002440(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000004D73:
			fn00000000000025F0(fn0000000000002440(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000004AD9:
			fn0000000000002640(edi);
		}
		r12_214 = 0x908E;
	}
	fn0000000000002530(stdout, fn0000000000002440(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000004D36:
	fn00000000000025F0(fn0000000000002440(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000004D73;
}