signed char star_digits_closebracket(struct s0* rdi, void** rsi, ...) {
    void* rcx3;
    uint32_t r9d4;
    void* r8_5;
    void** rdi6;
    void** rsi7;
    uint32_t eax8;
    uint32_t edx9;
    uint32_t eax10;

    rcx3 = rdi->f0;
    r9d4 = 0;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx3) + reinterpret_cast<unsigned char>(rsi)) == 42) {
        r8_5 = rdi->f8;
        r9d4 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_5) + reinterpret_cast<unsigned char>(rsi));
        if (*reinterpret_cast<signed char*>(&r9d4)) {
            addr_35c0_3:
            return 0;
        } else {
            rdi6 = rdi->f10;
            rsi7 = rsi + 1;
            if (reinterpret_cast<unsigned char>(rsi7) < reinterpret_cast<unsigned char>(rdi6)) {
                do {
                    eax8 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rcx3) + reinterpret_cast<unsigned char>(rsi7));
                    edx9 = eax8;
                    if (eax8 - 48 > 9) 
                        break;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_5) + reinterpret_cast<unsigned char>(rsi7))) 
                        goto addr_356c_7;
                    ++rsi7;
                } while (reinterpret_cast<unsigned char>(rsi7) < reinterpret_cast<unsigned char>(rdi6));
                goto addr_35c0_3;
            } else {
                goto addr_356c_7;
            }
        }
    } else {
        addr_356c_7:
        eax10 = r9d4;
        return *reinterpret_cast<signed char*>(&eax10);
    }
    if (*reinterpret_cast<signed char*>(&edx9) == 93) {
        r9d4 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_5) + reinterpret_cast<unsigned char>(rsi7))) ^ 1;
        goto addr_356c_7;
    }
}