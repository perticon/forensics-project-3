bool star_digits_closebracket(int32_t * es, int64_t idx) {
    int64_t v1 = (int64_t)es;
    if (*(char *)(v1 + idx) != 42) {
        // 0x356c
        return false;
    }
    int64_t v2 = *(int64_t *)(v1 + 8); // 0x3570
    unsigned char v3 = *(char *)(v2 + idx); // 0x3574
    if (v3 != 0) {
        // 0x35c0
        return false;
    }
    int64_t v4 = v3; // 0x3574
    uint64_t v5 = *(int64_t *)(v1 + 16); // 0x357e
    int64_t v6 = idx + 1; // 0x3582
    if (v6 >= v5) {
        // 0x356c
        return v4 % 2 != 0;
    }
    int64_t v7 = v6;
    unsigned char v8 = *(char *)(v7 + v1); // 0x35a0
    while (v8 == 57 || (int32_t)v8 < 57) {
        // 0x3590
        if (*(char *)(v7 + v2) != 0) {
            // 0x356c
            return v4 % 2 != 0;
        }
        int64_t v9 = v7 + 1; // 0x3597
        if (v9 >= v5) {
            // 0x35c0
            return false;
        }
        v7 = v9;
        v8 = *(char *)(v7 + v1);
    }
    int64_t v10 = v4; // 0x35b1
    if (v8 == 93) {
        // 0x35b3
        v10 = (int64_t)(*(char *)(v7 + v2) ^ 1);
    }
    // 0x356c
    return v10 % 2 != 0;
}