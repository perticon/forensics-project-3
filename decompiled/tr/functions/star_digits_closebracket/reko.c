byte star_digits_closebracket(Eq_15 rsi, struct Eq_1907 * rdi)
{
	byte (* rcx_6)[] = rdi->ptr0000;
	uint64 r9_126 = 0x00;
	if (Mem0[rcx_6 + rsi:byte] != 0x2A)
		return (byte) r9_126;
	byte (* r8_12)[] = rdi->ptr0008;
	r9_126 = CONVERT(Mem0[r8_12 + rsi:byte], byte, uint64);
	if ((byte) r9_126 != 0x00)
		return 0x00;
	Eq_1930 rdi_19 = rdi->t0010;
	word64 rsi_20 = rsi + 0x01;
	if (rsi + 0x01 >=u rdi_19)
		return (byte) r9_126;
	do
	{
		word32 eax_28 = CONVERT(Mem0[rcx_6 + rsi_20:byte], byte, word32);
		byte dl_50 = (byte) eax_28;
		if (eax_28 > 0x39)
		{
			if (dl_50 == 0x5D)
				r9_126 = CONVERT(CONVERT(Mem0[r8_12 + rsi_20:byte], byte, word32) ^ 0x01, word32, uint64);
			return (byte) r9_126;
		}
		if (Mem0[r8_12 + rsi_20:byte] != 0x00)
			return (byte) r9_126;
		rsi_20 = (word64) rsi_20 + 1;
	} while (rsi_20 < rdi_19);
	return 0x00;
}