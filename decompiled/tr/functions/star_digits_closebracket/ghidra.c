byte star_digits_closebracket(long *param_1,long param_2)

{
  byte bVar1;
  long lVar2;
  ulong uVar3;
  
  if (*(char *)(*param_1 + param_2) != '*') {
    return 0;
  }
  lVar2 = param_1[1];
  if (*(char *)(lVar2 + param_2) == '\0') {
    uVar3 = param_2 + 1;
    if ((ulong)param_1[2] <= uVar3) {
      return 0;
    }
    do {
      bVar1 = *(byte *)(*param_1 + uVar3);
      if (9 < bVar1 - 0x30) {
        if (bVar1 != 0x5d) {
          return 0;
        }
        return *(byte *)(lVar2 + uVar3) ^ 1;
      }
      if (*(char *)(lVar2 + uVar3) != '\0') {
        return 0;
      }
      uVar3 = uVar3 + 1;
    } while (uVar3 < (ulong)param_1[2]);
  }
  return 0;
}