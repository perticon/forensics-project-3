find_bracketed_repeat (const struct E_string *es, size_t start_idx,
                       unsigned char *char_to_repeat, count *repeat_count,
                       size_t *closing_bracket_idx)
{
  assert (start_idx + 1 < es->len);
  if (!es_match (es, start_idx + 1, '*'))
    return -1;

  for (size_t i = start_idx + 2; i < es->len && !es->escaped[i]; i++)
    {
      if (es->s[i] == ']')
        {
          size_t digit_str_len = i - start_idx - 2;

          *char_to_repeat = es->s[start_idx];
          if (digit_str_len == 0)
            {
              /* We've matched [c*] -- no explicit repeat count.  */
              *repeat_count = 0;
            }
          else
            {
              /* Here, we have found [c*s] where s should be a string
                 of octal (if it starts with '0') or decimal digits.  */
              char const *digit_str = &es->s[start_idx + 2];
              char *d_end;
              if ((xstrtoumax (digit_str, &d_end, *digit_str == '0' ? 8 : 10,
                               repeat_count, NULL)
                   != LONGINT_OK)
                  || REPEAT_COUNT_MAXIMUM < *repeat_count
                  || digit_str + digit_str_len != d_end)
                {
                  char *tmp = make_printable_str (digit_str, digit_str_len);
                  error (0, 0,
                         _("invalid repeat count %s in [c*n] construct"),
                         quote (tmp));
                  free (tmp);
                  return -2;
                }
            }
          *closing_bracket_idx = i;
          return 0;
        }
    }
  return -1;			/* No bracket found.  */
}