int64_t is_char_class_member(int64_t a1, int64_t a2) {
    if ((uint32_t)(int32_t)a1 >= 12) {
        is_char_class_member_cold();
    }
    int32_t v1 = *(int32_t *)((4 * a1 & 0x3fffffffc) + (int64_t)&g12); // 0x3805
    return (int64_t)v1 + (int64_t)&g12;
}