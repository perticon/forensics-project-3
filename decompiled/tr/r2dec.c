#include <stdint.h>

/* /tmp/tmprlq2jj6i @ 0x3470 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmprlq2jj6i @ 0x3560 */
 
int32_t dbg_star_digits_closebracket (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* _Bool star_digits_closebracket(E_string const * es,size_t idx); */
    rcx = *(rdi);
    r9d = 0;
    while (1) {
label_0:
        eax = r9d;
        return eax;
        r8 = *((rdi + 8));
        r9d = *((r8 + rsi));
        if (r9b != 0) {
            goto label_2;
        }
        rdi = *((rdi + 0x10));
        rsi++;
        if (rsi < rdi) {
            goto label_3;
        }
    }
label_1:
    if (*((r8 + rsi)) != 0) {
        goto label_0;
    }
    rsi++;
    if (rsi >= rdi) {
        goto label_2;
    }
label_3:
    eax = *((rcx + rsi));
    edx = eax;
    eax -= 0x30;
    if (eax <= 9) {
        goto label_1;
    }
    if (dl != 0x5d) {
        goto label_0;
    }
    r9d = *((r8 + rsi));
    r9d ^= 1;
    goto label_0;
label_2:
    r9d = 0;
    eax = r9d;
    return eax;
}

/* /tmp/tmprlq2jj6i @ 0x35d0 */
 
uint64_t dbg_make_printable_char (int64_t arg1) {
    rdi = arg1;
    /* char * make_printable_char(unsigned char c); */
    ebx = edi;
    rax = xmalloc (5);
    r12 = rax;
    rax = ctype_b_loc ();
    edx = (int32_t) bl;
    rax = *(rax);
    if ((*((rax + rdx*2 + 1)) & 0x40) != 0) {
        *(r12) = bl;
        rax = r12;
        *((r12 + 1)) = 0;
        return rax;
    }
    r8d = (int32_t) bl;
    rdi = r12;
    rcx = "\\%03o";
    eax = 0;
    edx = 5;
    esi = 1;
    sprintf_chk ();
    rax = r12;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x7a60 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x2580 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmprlq2jj6i @ 0x7fe0 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000a4c0);
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x2440 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmprlq2jj6i @ 0x2600 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmprlq2jj6i @ 0x23a0 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmprlq2jj6i @ 0x25d0 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmprlq2jj6i @ 0x23f0 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmprlq2jj6i @ 0x26a0 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmprlq2jj6i @ 0x26b0 */
 
void sprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__sprintf_chk]");
}

/* /tmp/tmprlq2jj6i @ 0x3640 */
 
int64_t dbg_make_printable_str (uint32_t arg1, uint32_t arg2) {
    char[5] buf;
    int64_t canary;
    int64_t var_13h;
    int64_t var_14h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    /* char * make_printable_str(char const * s,size_t len); */
    r14 = rdi;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = xnmalloc (rsi + 1, 4);
    *(rsp) = rax;
    if (rbx == 0) {
        goto label_1;
    }
    rbx += r14;
    r15 = rax;
    r13 = 0x0000900a;
    r12 = 0x0000901e;
    rbp = 0x00009260;
    do {
        r8d = *(r14);
        if (r8b > 0xd) {
            goto label_2;
        }
        if (r8b <= 6) {
            goto label_3;
        }
        r8d -= 8;
        rsi = r12;
        if (r8b <= 5) {
            r8d = (int32_t) r8b;
            rax = *((rbp + r8*4));
            rax += rbp;
            /* switch table (6 cases) at 0x9260 */
            void (*rax)() ();
            rsi = 0x0000900c;
        }
label_0:
        rdi = r15;
        r14++;
        rax = stpcpy ();
        r15 = rax;
    } while (rbx != r14);
label_1:
    rax = *((rsp + 0x18));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_4;
    }
    rax = *(rsp);
    return rax;
    rsi = 0x0000900f;
    goto label_0;
    rsi = 0x00009015;
    goto label_0;
    rsi = 0x0000901b;
    goto label_0;
    rsi = 0x00009012;
    goto label_0;
    rsi = 0x00009018;
    goto label_0;
label_2:
    rsi = r13;
    if (r8b == 0x5c) {
        goto label_0;
    }
label_3:
    *((rsp + 8)) = r8b;
    rax = ctype_b_loc ();
    r11 = rax;
    eax = *((rsp + 8));
    rdx = *(r11);
    r8 = rax;
    if ((*((rdx + rax*2 + 1)) & 0x40) != 0) {
        *((rsp + 0x13)) = al;
        rsi = rsp + 0x13;
        *((rsp + 0x14)) = 0;
        goto label_0;
    }
    rdi = rsp + 0x13;
    esi = 1;
    rcx = "\\%03o";
    eax = 0;
    edx = 5;
    *((rsp + 8)) = rdi;
    sprintf_chk ();
    rsi = *((rsp + 8));
    goto label_0;
label_4:
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x37f0 */
 
int64_t is_char_class_member (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    if (edi > 0xb) {
        void (*0x26c0)() ();
    }
    rdx = 0x00009278;
    ebx = esi;
    rax = *((rdx + rdi*4));
    rax += rdx;
    /* switch table (12 cases) at 0x9278 */
    void (*rax)() ();
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 0x100;
    do {
label_0:
        al = (eax != 0) ? 1 : 0;
        return rax;
        rax = ctype_b_loc ();
        r8 = rax;
        eax = (int32_t) bl;
        rdx = *(r8);
        eax = *((rdx + rax*2));
        eax &= 0x1000;
    } while (1);
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 8;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 0x400;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 1;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 2;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 0x800;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 0x8000;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 0x200;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 0x4000;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= 4;
    goto label_0;
    rax = ctype_b_loc ();
    r8 = rax;
    eax = (int32_t) bl;
    rdx = *(r8);
    eax = *((rdx + rax*2));
    eax &= sym._init;
    goto label_0;
}

/* /tmp/tmprlq2jj6i @ 0x26c0 */
 
void is_char_class_member_cold (void) {
    /* [16] -r-x section size 24962 named .text */
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x3990 */
 
int64_t get_spec_stats (int64_t arg_8h, int64_t arg_10h, int64_t arg_11h, int64_t arg_18h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    eax = 0;
    r14d = 0;
    *((rdi + 0x30)) = ax;
    rax = *(rdi);
    *((rsp + 8)) = rdi;
    rbp = *((rax + 8));
    *((rdi + 0x20)) = 0;
    *((rdi + 0x32)) = 0;
    if (rbp == 0) {
        goto label_3;
    }
    r12 = 0x000092a8;
label_0:
    if (*(rbp) > 4) {
        void (*0x26c5)() ();
    }
    eax = *(rbp);
    rax = *((r12 + rax*4));
    rax += r12;
    /* switch table (5 cases) at 0x92a8 */
    eax = void (*rax)() ();
    ecx = *((rbp + 0x10));
    eax = 0;
    ebx = 0;
    do {
        edx = 0;
        dl = (cl == al) ? 1 : 0;
        eax++;
        rbx += rdx;
    } while (eax != 0x100);
    rax = *((rsp + 8));
    *((rax + 0x30)) = 1;
label_1:
    rbx += r14;
    r14 = rbx;
    if (rbx < 0) {
        goto label_4;
    }
    if (rbx == -1) {
        goto label_4;
    }
label_2:
    rbp = *((rbp + 8));
    if (rbp != 0) {
        goto label_0;
    }
label_3:
    rax = *((rsp + 8));
    *((rax + 0x18)) = r14;
    return rax;
    rax = *((rsp + 8));
    r15d = *((rbp + 0x10));
    r13d = 0;
    ebx = 0;
    *((rax + 0x31)) = 1;
    do {
        esi = r13d;
        edi = r15d;
        al = is_char_class_member ();
        rbx -= 0xffffffffffffffff;
        r13d++;
    } while (r13d != 0x100);
    r15d -= 6;
    r15d &= 0xfffffffb;
    if (r15d == 0) {
        goto label_1;
    }
    rax = *((rsp + 8));
    *((rax + 0x32)) = 1;
    goto label_1;
    eax = *((rbp + 0x11));
    edx = *((rbp + 0x10));
    if (al < dl) {
        goto label_5;
    }
    ebx = (int32_t) al;
    eax = ebx;
    eax -= edx;
    ebx = rax + 1;
    rbx = (int64_t) ebx;
    goto label_1;
    ebx = 1;
    goto label_1;
    rbx = *((rbp + 0x18));
    if (rbx != 0) {
        goto label_1;
    }
    rax = *((rsp + 8));
    *((rax + 0x20))++;
    *((rax + 0x28)) = rbp;
    goto label_2;
label_4:
    edx = 5;
    rax = dcgettext (0, "too many characters in set");
    eax = 0;
    error (1, 0, rax);
label_5:
    return assert_fail ("p->u.range.last_char >= p->u.range.first_char", "src/tr.c", 0x4f1, "get_spec_stats");
}

/* /tmp/tmprlq2jj6i @ 0x26c5 */
 
void get_spec_stats_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x3b30 */
 
int64_t get_next (int64_t arg_8h, int64_t arg_10h, int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rdx = 0x000092bc;
    rax = *((rdi + 0x10));
label_0:
    if (rsi != 0) {
        *(rsi) = 2;
    }
    if (rax == 0xfffffffffffffffe) {
        goto label_7;
    }
    r14 = *((rbp + 8));
label_2:
    if (r14 == 0) {
        goto label_8;
    }
    if (*(r14) > 4) {
        void (*0x26ca)() ();
    }
    eax = *(r14);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (5 cases) at 0x92bc */
    void (*rax)() ();
    rax = *((r14 + 8));
    r13d = *((r14 + 0x10));
    *((rbp + 0x10)) = 0xffffffffffffffff;
    *((rbp + 8)) = rax;
label_1:
    eax = r13d;
    r12 = rbx;
    r13 = rbx;
    r14 = rbx;
    return rax;
    rax = *((r14 + 0x18));
    if (rax != 0) {
        goto label_9;
    }
    rax = *((r14 + 8));
    *((rbp + 0x10)) = 0xffffffffffffffff;
    *((rbp + 8)) = rax;
    rax = 0xffffffffffffffff;
    goto label_0;
    r12d = *((r14 + 0x10));
    if (rsi != 0) {
        if (r12d == 6) {
            goto label_10;
        }
        if (r12d != 0xa) {
            goto label_5;
        }
        *(rsi) = 1;
        r12d = *((r14 + 0x10));
    }
label_5:
    rbx = *((rbp + 0x10));
    if (rbx == -1) {
        goto label_11;
    }
label_3:
    esi = (int32_t) bl;
    edi = r12d;
    al = is_char_class_member ();
    if (al == 0) {
        goto label_12;
    }
    r13d = ebx;
    ebx++;
    if (ebx <= 0xff) {
        goto label_13;
    }
    goto label_6;
    do {
        ebx++;
        if (ebx == 0x100) {
            goto label_6;
        }
label_13:
        esi = (int32_t) bl;
        edi = r12d;
        al = is_char_class_member ();
    } while (al == 0);
    rbx = (int64_t) ebx;
    eax = r13d;
    *((rbp + 0x10)) = rbx;
    return rax;
    rdx = *((rbp + 0x10));
    rax = rdx + 1;
    if (rdx == -1) {
        goto label_14;
    }
label_4:
    edx = *((r14 + 0x11));
    *((rbp + 0x10)) = rax;
    r13d = eax;
    if (rdx != rax) {
        goto label_1;
    }
label_6:
    rax = *((r14 + 8));
    *((rbp + 0x10)) = 0xffffffffffffffff;
    *((rbp + 8)) = rax;
    eax = r13d;
    return rax;
label_7:
    rax = *(rbp);
    r14 = *((rax + 8));
    *((rbp + 0x10)) = 0xffffffffffffffff;
    *((rbp + 8)) = r14;
    goto label_2;
label_11:
    ebx = 0;
    while (al == 0) {
        ebx++;
        if (ebx == 0x100) {
            goto label_15;
        }
        esi = ebx;
        edi = r12d;
        al = is_char_class_member ();
    }
    rbx = (int64_t) ebx;
    *((rbp + 0x10)) = rbx;
    goto label_3;
label_14:
    eax = *((r14 + 0x10));
    goto label_4;
label_10:
    *(rsi) = 0;
    r12d = *((r14 + 0x10));
    goto label_5;
label_8:
    r13d = 0xffffffff;
    goto label_1;
label_9:
    rcx = *((rbp + 0x10));
    r13d = *((r14 + 0x10));
    rdx = rcx + 1;
    ecx = 1;
    if (rcx == -1) {
        rdx = rcx;
    }
    *((rbp + 0x10)) = rdx;
    if (rax != rdx) {
        goto label_1;
    }
    goto label_6;
label_12:
    assert_fail ("is_char_class_member (p->u.char_class, s->state)", "src/tr.c", 0x43a, "get_next");
label_15:
    return assert_fail ("i < N_CHARS", "src/tr.c", 0x437, "get_next");
}

/* /tmp/tmprlq2jj6i @ 0x26ca */
 
void get_next_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x3da0 */
 
uint64_t plain_read_part_0 (void) {
    edx = 5;
    rax = dcgettext (0, "read error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
}

/* /tmp/tmprlq2jj6i @ 0x23b0 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmprlq2jj6i @ 0x7300 */
 
uint64_t dbg_safe_read (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* size_t safe_read(int fd,void * buf,size_t count); */
    r13d = edi;
    rbx = rdx;
    do {
label_0:
        rax = read (r13d, rbp, rbx);
        r12 = rax;
        if (rax >= 0) {
            goto label_1;
        }
        rax = errno_location ();
        eax = *(rax);
    } while (eax == 4);
    if (rbx > 0x7ff00000) {
        if (eax != 0x16) {
            goto label_1;
        }
        ebx = 0x7ff00000;
        goto label_0;
    }
label_1:
    rax = r12;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x24d0 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmprlq2jj6i @ 0x3dd0 */
 
uint64_t plain_read (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rax = safe_read (0, rdi, rsi);
    if (rax != -1) {
        return rax;
    }
    plain_read_part_0 ();
}

/* /tmp/tmprlq2jj6i @ 0x3e00 */
 
void append_repeated_char_part_0 (void) {
    return assert_fail ("list->tail", "src/tr.c", 0x2d1, "append_repeated_char");
}

/* /tmp/tmprlq2jj6i @ 0x3e30 */
 
int64_t set_initialize (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12d = esi;
    rbx = rdx;
    *((rdi + 0x10)) = 0xfffffffffffffffe;
    while (eax != 0xffffffff) {
        rax = (int64_t) eax;
        *((rbx + rax)) = 1;
        esi = 0;
        rdi = rbp;
        eax = get_next ();
    }
    if (r12b == 0) {
        goto label_0;
    }
    rdx = rbx;
    rax = rbx + 0x100;
    do {
        *(rdx) ^= 1;
        rdx++;
    } while (rdx != rax);
label_0:
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x3e90 */
 
uint64_t squeeze_filter_constprop_0 (int64_t arg_2h, int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    r15d = 0x7fffffff;
    r14 = obj_io_buf;
    r13 = obj_in_squeeze_set;
    r12 = rdi;
    ebp = 0;
    ebx = 0;
label_0:
    if (rbp < rbx) {
        goto label_9;
    }
    do {
label_4:
        esi = _init;
        rdi = r14;
        rax = void (*r12)() ();
        rbx = rax;
        if (rax == 0) {
            goto label_10;
        }
        if (r15d == 0x7fffffff) {
            goto label_11;
        }
        ebp = 0;
label_1:
        eax = *((r14 + rbp));
        if (eax == r15d) {
            goto label_12;
        }
        r15d = 0x7fffffff;
    } while (rbp >= rbx);
label_9:
    if (r15d == 0x7fffffff) {
        goto label_13;
    }
label_2:
    if (rbx <= rbp) {
        goto label_0;
    }
    goto label_1;
label_12:
    rbp++;
    if (rbp != rbx) {
        goto label_1;
    }
    goto label_0;
label_11:
    edi = 0;
    ebp = 0;
label_3:
    eax = *((r14 + rbp));
    if (*((r13 + rax)) == 0) {
        goto label_14;
    }
    if (rbx <= rbp) {
        goto label_15;
    }
label_6:
    rax = rbp;
    r15d = *((r14 + rbp));
    rax -= rdi;
    rdx = rax + 1;
    if (rbp != 0) {
        rsi = 0x0000e3df;
        if (r15b != *((rbp + rsi))) {
            rdx = rax;
            goto label_16;
        }
    }
label_16:
    rbp++;
    if (rdx == 0) {
        goto label_2;
    }
    rcx = stdout;
    rdi += r14;
    esi = 1;
    *((rsp + 8)) = rdx;
    rax = fwrite_unlocked ();
    rdx = *((rsp + 8));
    if (rax == rdx) {
        goto label_2;
    }
    do {
        edx = 5;
        rax = dcgettext (0, "write error");
        r12 = rax;
        rax = errno_location ();
        eax = 0;
        error (1, *(rax), r12);
label_14:
        r9 = rbp + 2;
        if (r9 >= rbx) {
            goto label_17;
        }
label_5:
        goto label_3;
label_13:
        rdi = rbp;
        goto label_3;
label_15:
        r15 = rbx;
        r9 = rbp;
        r15 -= rdi;
label_8:
        rdi += r14;
        rdx = r15;
        esi = 1;
        *((rsp + 8)) = r9;
        rcx = stdout;
        rax = fwrite_unlocked ();
        r9 = *((rsp + 8));
    } while (r15 != rax);
label_7:
    r15d = 0x7fffffff;
    if (rbx <= r9) {
        goto label_4;
    }
    rdi = r9;
    goto label_5;
label_10:
    return rax;
    if (rbx != r9) {
label_17:
        goto label_18;
    }
    rbp++;
    eax = *((r14 + rbp));
    if (*((r13 + rax)) == 0) {
        goto label_19;
    }
    if (rbp < rbx) {
        goto label_6;
    }
    r9 = rbp;
label_19:
    r15 = rbx;
    r15 -= rdi;
    if (r15 == 0) {
        goto label_7;
    }
    goto label_8;
label_18:
    r15 = rbx;
    r15 -= rdi;
    goto label_8;
}

/* /tmp/tmprlq2jj6i @ 0x4080 */
 
uint64_t dbg_read_and_xlate (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_47h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg_70h, int64_t arg_78h, int64_t arg1, int64_t arg2) {
    int64_t var_1h;
    int64_t var_8h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    int64_t var_78h_2;
    rdi = arg1;
    rsi = arg2;
    /* size_t read_and_xlate(char * buf,size_t size); */
    rbx = rdi;
    rax = safe_read (0, rdi, rsi);
    if (rax == -1) {
        goto label_0;
    }
    rdi = rbx;
    rsi = rbx + rax;
    rcx = obj_xlate;
    if (rax == 0) {
        goto label_1;
    }
    do {
        edx = *(rdi);
        rdi++;
        edx = *((rcx + rdx));
        *((rdi - 1)) = dl;
    } while (rsi != rdi);
label_1:
    return rax;
label_0:
    plain_read_part_0 ();
}

/* /tmp/tmprlq2jj6i @ 0x40d0 */
 
uint64_t dbg_read_and_delete (int64_t arg_1h, int64_t arg_8h_2, int64_t arg_8h, int64_t arg_10h, int64_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_47h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg_70h, int64_t arg_78h, int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_78h;
    int64_t var_78h_2;
    rdi = arg1;
    rsi = arg2;
    /* size_t read_and_delete(char * buf,size_t size); */
    r12 = obj_in_delete_set;
    rbx = rsi;
label_0:
    rax = safe_read (0, rbp, rbx);
    if (rax == -1) {
        goto label_3;
    }
    if (rax == 0) {
        goto label_4;
    }
    esi = 0;
label_1:
    ecx = *((rbp + rsi));
    rdx = rsi + 1;
    if (*((r12 + rcx)) == 0) {
        goto label_5;
    }
    if (rax <= rdx) {
        goto label_6;
    }
    do {
label_2:
        edi = *((rbp + rdx));
        if (*((r12 + rdi)) == 0) {
            *((rbp + rsi)) = dil;
            rsi++;
        }
        rdx++;
    } while (rdx < rax);
label_6:
    if (rsi == 0) {
        goto label_0;
    }
    rax = rsi;
    do {
label_4:
        return rax;
label_5:
        if (rax != rdx) {
            rsi = rdx;
            goto label_1;
        }
        rdx = rsi + 2;
    } while (rdx >= rax);
    rsi = rax;
    goto label_2;
label_3:
    edx = 5;
    rax = dcgettext (0, "read error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
}

/* /tmp/tmprlq2jj6i @ 0x41a0 */
 
int64_t dbg_parse_str (int64_t arg_1h, int64_t arg_8h_2, uint32_t arg_8h, uint32_t arg_10h, uint32_t arg_18h, int64_t arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_47h, int64_t arg_48h, int64_t arg_50h, uint32_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg_70h, int64_t arg_78h, int64_t arg1, int64_t arg2) {
    count repeat_count;
    char * d_end;
    E_string es;
    int64_t var_8h;
    void * var_60h;
    int64_t var_68h;
    int64_t var_78h;
    int64_t var_78h_2;
    rdi = arg1;
    rsi = arg2;
    /* _Bool parse_str(char const * s,Spec_list * spec_list); */
    r13 = rdi;
    *((rsp + 8)) = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = strlen (rdi);
    r12 = rax;
    rax = xmalloc (rax);
    esi = 1;
    rdi = r12;
    *((rsp + 0x60)) = rax;
    rax = xcalloc ();
    ebx = *(r13);
    *((rsp + 0x68)) = rax;
    r14 = rax;
    if (bl == 0) {
        goto label_14;
    }
    ecx = 0;
    r15d = 0;
    r11 = 0x000092d0;
    while (bl != 0x5c) {
label_0:
        eax = rcx + 1;
        *(r12) = bl;
        ebx = *((r13 + rax));
        rcx = rax;
        if (bl == 0) {
            goto label_15;
        }
        eax = r15d;
        r15d++;
        r12 = rbp + rax;
    }
    edx = rcx + 1;
    rbx = r14 + rax;
    r9d = *((r13 + rdx));
    *(rbx) = 1;
    r10 = rdx;
    if (r9b == 0) {
        goto label_16;
    }
    edx = r9 - 0x30;
    ebx = r9d;
    if (dl <= 0x46) {
        eax = (int32_t) dl;
        rax = *((r11 + rax*4));
        rax += r11;
        /* switch table (71 cases) at 0x92d0 */
        void (*rax)() ();
        ebx = 8;
    }
label_8:
    ecx = r10d;
    goto label_0;
label_15:
    *((rsp + 0x70)) = r15;
    if (r15 <= 2) {
        goto label_17;
    }
    r13 = r15;
    r8d = 2;
    ecx = 0;
    r15 = r14;
    while (r12b != 0x5b) {
label_1:
        if (r14b == 0x2d) {
            goto label_18;
        }
label_4:
        xmalloc (0x20);
        rdi = *((rsp + 8));
        *((rax + 8)) = 0;
        rdx = *((rdi + 8));
        *(rax) = 0;
        *((rax + 0x10)) = r12b;
        if (rdx == 0) {
            goto label_19;
        }
        rdi = *((rsp + 8));
        rcx = *(rsp);
        *((rdx + 8)) = rax;
        *((rdi + 8)) = rax;
label_5:
        r8 = rcx + 2;
        if (r13 <= r8) {
            goto label_20;
        }
label_3:
        r12d = *((rbp + rcx));
        rax = rcx + 1;
        r14d = *((rbp + rcx + 1));
        *(rsp) = rax;
    }
    eax = *((r15 + rcx));
    *((rsp + 0x40)) = al;
    if (al != 0) {
        goto label_1;
    }
    if (r14b == 0x3a) {
        goto label_21;
    }
    if (r14b == 0x3d) {
        goto label_21;
    }
label_6:
    if (*((rbp + r8)) != 0x2a) {
        goto label_1;
    }
    if (*((r15 + r8)) != 0) {
        goto label_1;
    }
    rax = rcx + 3;
    rbx = rax;
    if (r13 > rax) {
        goto label_22;
    }
    goto label_1;
label_2:
    rbx++;
    if (r13 <= rbx) {
        goto label_1;
    }
label_22:
    if (*((r15 + rbx)) != 0) {
        goto label_1;
    }
    if (*((rbp + rbx)) != 0x5d) {
        goto label_2;
    }
    r12 = rbx;
    r12 -= *(rsp);
    r12 -= 2;
    if (r12 == 0) {
        goto label_23;
    }
    edx = 0;
    *(rsp) = rdi;
    dl = (*(rdi) != 0x30) ? 1 : 0;
    eax = xstrtoumax (rbp + rax, rsp + 0x58, rdx + rdx + 8, rsp + 0x50, 0);
    rdi = *(rsp);
    if (eax != 0) {
        goto label_24;
    }
    rdx = *((rsp + 0x50));
    if (rdx == -1) {
        goto label_24;
    }
    rax = rdi + r12;
    if (*((rsp + 0x58)) != rax) {
        goto label_24;
    }
label_10:
    *(rsp) = rdx;
    xmalloc (0x20);
    rdx = *(rsp);
    rdi = *((rsp + 8));
    *((rax + 8)) = 0;
    *((rax + 0x18)) = rdx;
    rdx = *((rdi + 8));
    *(rax) = 4;
    *((rax + 0x10)) = r14b;
    if (rdx == 0) {
        goto label_25;
    }
    rsi = *((rsp + 8));
    rcx = rbx + 1;
    *((rdx + 8)) = rax;
    r8 = rcx + 2;
    *((rsi + 8)) = rax;
    if (r13 > r8) {
        goto label_3;
    }
label_20:
    r14 = r15;
    r15 = r13;
    r13 = rcx;
label_13:
    r12 = rbp + r13;
    rbx = rbp + r15;
    if (r15 <= r13) {
        goto label_14;
    }
    r13 = *((rsp + 8));
    do {
        r15d = *(r12);
        xmalloc (0x20);
        rdx = *((r13 + 8));
        *((rax + 8)) = 0;
        *(rax) = 0;
        *((rax + 0x10)) = r15b;
        if (rdx == 0) {
            goto label_19;
        }
        r12++;
        *((rdx + 8)) = rax;
        *((r13 + 8)) = rax;
    } while (rbx != r12);
label_14:
    *((rsp + 0x40)) = 1;
    goto label_12;
label_18:
    rax = *(rsp);
    eax = *((r15 + rax));
    *((rsp + 0x40)) = al;
    if (al != 0) {
        goto label_4;
    }
    ebx = *((rbp + r8));
    if (bl < r12b) {
        goto label_26;
    }
    *(rsp) = rcx;
    xmalloc (0x20);
    rdi = *((rsp + 8));
    rcx = *(rsp);
    *((rax + 8)) = 0;
    rdx = *((rdi + 8));
    *(rax) = 1;
    *((rax + 0x10)) = r12b;
    *((rax + 0x11)) = bl;
    if (rdx == 0) {
        goto label_27;
    }
    rdi = *((rsp + 8));
    *((rdx + 8)) = rax;
    rcx += 3;
    *((rdi + 8)) = rax;
    goto label_5;
label_21:
    rax = *(rsp);
    if (*((r15 + rax)) != 0) {
        goto label_6;
    }
    rsi = r13 - 1;
    if (r8 >= rsi) {
        goto label_6;
    }
    rax = r8;
    goto label_28;
label_7:
    if (rax >= rsi) {
        goto label_6;
    }
label_28:
    edx = *((rbp + rax));
    rbx = rax;
    rax = rax + 1;
    if (dl != r14b) {
        goto label_7;
    }
    if (*((rbp + rbx + 1)) != 0x5d) {
        goto label_7;
    }
    if (*((r15 + rax - 1)) != 0) {
        goto label_7;
    }
    if (*((r15 + rbx + 1)) != 0) {
        goto label_7;
    }
    rax = rbx;
    rdi = rbp + r8;
    rax -= rcx;
    *((rsp + 0x18)) = rdi;
    rax -= 2;
    *((rsp + 0x10)) = rax;
    if (rax == 0) {
        goto label_29;
    }
    if (r14b == 0x3a) {
        goto label_30;
    }
    *((rsp + 0x20)) = rcx;
    if (*((rsp + 0x10)) == 1) {
        goto label_31;
    }
    rsi = r8;
    *((rsp + 0x28)) = r8;
    al = star_digits_closebracket (rsp + 0x60, rsi);
    r8 = *((rsp + 0x28));
    rcx = *((rsp + 0x20));
    if (al != 0) {
        goto label_6;
    }
    r14 = r15;
    rax = make_printable_str (*((rsp + 0x18)), *((rsp + 0x10)));
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "%s: equivalence class operand must be a single character");
    rcx = r12;
label_9:
    eax = 0;
    error (0, 0, rax);
    free (r12);
    goto label_12;
label_26:
    edi = (int32_t) r12b;
    r14 = r15;
    rax = make_printable_char (rdi);
    edi = (int32_t) bl;
    r13 = rax;
    rax = make_printable_char (rdi);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "range-endpoints of '%s-%s' are in reverse collating sequence order");
    r8 = r12;
    rcx = r13;
    eax = 0;
    error (0, 0, rax);
    free (r13);
    free (r12);
label_12:
    free (rbp);
    free (r14);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_32;
    }
    eax = *((rsp + 0x40));
    return rax;
    eax = rcx + 2;
    ebx = edx;
    r8d = *((r13 + rax));
    eax = r8 - 0x30;
    if (eax > 7) {
        goto label_8;
    }
    ebx = rax + rbx*8;
    eax = rcx + 3;
    eax = *((r13 + rax));
    *(rsp) = eax;
    eax -= 0x30;
    if (eax > 7) {
        goto label_33;
    }
    edx = (int32_t) bl;
    edx = rax + rdx*8;
    if (edx > 0xff) {
        goto label_34;
    }
    ebx = rax + rbx*8;
    r10d = rcx + 3;
    goto label_8;
    ebx = 0xb;
    goto label_8;
    ebx = 9;
    goto label_8;
    ebx = 0xd;
    goto label_8;
    ebx = 0xa;
    goto label_8;
    ebx = 0xc;
    goto label_8;
label_30:
    *((rsp + 0x20)) = rbp;
    r11d = 0;
    *((rsp + 0x30)) = r8;
    *((rsp + 0x38)) = rcx;
    *((rsp + 0x28)) = r12b;
    r12 = *((rsp + 0x18));
    *((rsp + 0x48)) = rbx;
    rbx = *((rsp + 0x10));
    *((rsp + 0x47)) = r14b;
    r14 = r11;
    do {
        rax = obj_char_class_name;
        rbp = *((rax + r14*8));
        eax = strncmp (r12, *((rax + r14*8)), rbx);
        if (eax == 0) {
            rax = strlen (rbp);
            if (rbx == rax) {
                goto label_35;
            }
        }
        r14++;
    } while (r14 != 0xc);
    r8 = *((rsp + 0x30));
    rcx = *((rsp + 0x38));
    rbp = *((rsp + 0x20));
    r12d = *((rsp + 0x28));
    rsi = r8;
    *((rsp + 0x28)) = rcx;
    r14d = *((rsp + 0x47));
    *((rsp + 0x20)) = r8;
    al = star_digits_closebracket (rsp + 0x60, rsi);
    r8 = *((rsp + 0x20));
    rcx = *((rsp + 0x28));
    if (al != 0) {
        goto label_6;
    }
    r14 = r15;
    rax = make_printable_str (*((rsp + 0x18)), *((rsp + 0x10)));
    rdi = rax;
    r12 = rax;
    rax = quote (rdi, rsi, rdx, rcx, r8);
    edx = 5;
    r13 = rax;
label_11:
    rax = dcgettext (0, "invalid character class %s");
    rcx = r13;
    rdx = rax;
    goto label_9;
label_23:
    *((rsp + 0x50)) = 0;
    edx = 0;
    goto label_10;
label_35:
    *(rsp) = r14;
    rbp = *((rsp + 0x20));
    rbx = *((rsp + 0x48));
    xmalloc (0x20);
    rdi = *((rsp + 8));
    r11 = *(rsp);
    *((rax + 8)) = 0;
    rdx = *((rdi + 8));
    *(rax) = 2;
    *((rax + 0x10)) = r11d;
    if (rdx == 0) {
        goto label_36;
    }
    do {
        rdi = *((rsp + 8));
        *((rdx + 8)) = rax;
        rcx = rbx + 2;
        *((rdi + 8)) = rax;
        goto label_5;
label_31:
        xmalloc (0x20);
        rdi = *((rsp + 0x18));
        rsi = *((rsp + 8));
        *((rax + 8)) = 0;
        edx = *(rdi);
        *(rax) = 3;
        *((rax + 0x10)) = dl;
        rdx = *((rsi + 8));
    } while (rdx != 0);
    assert_fail ("list->tail", "src/tr.c", 0x2e7, "append_equiv_class");
label_16:
    edx = 5;
    *(rsp) = ecx;
    rax = dcgettext (0, "warning: an unescaped backslash at end of string is not portable");
    eax = 0;
    error (0, 0, rax);
    *(rbx) = 0;
    r10d = *(rsp);
    ebx = 0x5c;
    r11 = 0x000092d0;
    goto label_8;
    ebx = 7;
    goto label_8;
label_33:
    r10d++;
    goto label_8;
    ebx = 0x5c;
    goto label_8;
label_34:
    edx = 5;
    *((rsp + 0x18)) = r10d;
    *((rsp + 0x10)) = r9b;
    *((rsp + 0x40)) = r8d;
    rax = dcgettext (0, "warning: the ambiguous octal escape \\%c%c%c is being\n\tinterpreted as the 2-byte sequence \\0%c%c, %c");
    ecx = *((rsp + 0x10));
    r9d = *((rsp + 8));
    eax = 0;
    r8d = *((rsp + 0x50));
    error (0, 0, rax);
    r10d = *((rsp + 0x38));
    r11 = 0x000092d0;
    r10d++;
    goto label_8;
label_24:
    r14 = r15;
    rax = make_printable_str (rdi, r12);
    rdi = rax;
    r12 = rax;
    rax = quote (rdi, rsi, rdx, rcx, r8);
    edx = 5;
    rsi = "invalid repeat count %s in [c*n] construct";
    r13 = rax;
    goto label_11;
label_29:
    r9d = r14d;
    edx = 5;
    rsi = "missing character class name '[::]';
    r14 = r15;
    if (r9b != 0x3a) {
    }
    rax = dcgettext (0, "missing equivalence class character '[==]');
    eax = 0;
    error (0, 0, rax);
    goto label_12;
label_17:
    r13d = 0;
    goto label_13;
label_32:
    stack_chk_fail ();
label_36:
    assert_fail ("list->tail", "src/tr.c", 0x2bd, "append_char_class");
label_27:
    assert_fail ("list->tail", "src/tr.c", 0x2a7, "append_range");
label_19:
    assert_fail ("list->tail", "src/tr.c", 0x289, "append_normal_char");
label_25:
    return append_repeated_char_part_0 ();
}

/* /tmp/tmprlq2jj6i @ 0x26cf */
 
void main_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x5020 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000a517;
        rdx = 0x0000a508;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000a50f;
        rdx = 0x0000a511;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000a513;
    rdx = 0x0000a50c;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x8620 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x25a0 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmprlq2jj6i @ 0x5100 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x26d4)() ();
    }
    rdx = 0x0000a580;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xa580 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000a51b;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000a511;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000a5ac;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xa5ac */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000a50f;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000a511;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000a50f;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000a511;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000a6ac;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xa6ac */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000a7ac;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xa7ac */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000a511;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000a50f;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000a50f;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000a511;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmprlq2jj6i @ 0x26d4 */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x6520 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x26d9)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x26d9 */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x26de */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x26e4 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x26e9 */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x26ee */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x26f3 */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x26f8 */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x26fd */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x2702 */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x2707 */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x270c */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x34a0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x34d0 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x3510 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_00002370 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmprlq2jj6i @ 0x2370 */
 
void fcn_00002370 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmprlq2jj6i @ 0x3550 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmprlq2jj6i @ 0x8830 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmprlq2jj6i @ 0x6a70 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmprlq2jj6i @ 0x6da0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010510]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00010520]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x2470 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmprlq2jj6i @ 0x78a0 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x68b0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x7aa0 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x67d0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x8660 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x6ac0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x26e4)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x6830 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x4f70 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmprlq2jj6i @ 0x24b0 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmprlq2jj6i @ 0x23c0 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmprlq2jj6i @ 0x2650 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmprlq2jj6i @ 0x7000 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x00010510]");
    __asm ("movdqa xmm2, xmmword [0x00010520]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x26fd)() ();
    }
    if (rdx == 0) {
        void (*0x26fd)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x70a0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010510]");
    __asm ("movdqa xmm2, xmmword [0x00010520]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2702)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x2702)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x6be0 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x26ee)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x6f60 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x26f8)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x8844 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmprlq2jj6i @ 0x4f30 */
 
void fdadvise (void) {
    return posix_fadvise ();
}

/* /tmp/tmprlq2jj6i @ 0x7b20 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x7c20 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x7ac0 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x8550 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2590)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmprlq2jj6i @ 0x6aa0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprlq2jj6i @ 0x7fa0 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2560)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x2460 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmprlq2jj6i @ 0x7a80 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x77e0 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x7370)() ();
}

/* /tmp/tmprlq2jj6i @ 0x4e80 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000a4c0);
    } while (1);
}

/* /tmp/tmprlq2jj6i @ 0x8770 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmprlq2jj6i @ 0x7a20 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x86f0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x7150 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x00010510]");
    __asm ("movdqa xmm2, xmmword [0x00010520]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2707)() ();
    }
    if (rax == 0) {
        void (*0x2707)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x6ed0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010510]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00010520]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x7e70 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x2540 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmprlq2jj6i @ 0x6770 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x7290 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmprlq2jj6i @ 0x6710 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x7ee0 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2560)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x69b0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x4e70 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmprlq2jj6i @ 0x7af0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x7cb0 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x84c0 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x2410)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmprlq2jj6i @ 0x7bd0 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x72a0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprlq2jj6i @ 0x71f0 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010510]");
    __asm ("movdqa xmm2, xmmword [0x00010520]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x270c)() ();
    }
    if (rax == 0) {
        void (*0x270c)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x7f20 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2560)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x69a0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x68b0)() ();
}

/* /tmp/tmprlq2jj6i @ 0x6a80 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprlq2jj6i @ 0x2720 */
 
int32_t main (int32_t argc, char ** argv) {
    uint32_t var_8h;
    uint32_t var_10h;
    uint32_t var_18h;
    uint32_t var_sp_20h;
    int64_t var_28h;
    void * var_sp_30h;
    void * var_38h;
    int64_t var_40h;
    int64_t var_4ch;
    uint32_t var_58h;
    uint32_t var_5ch;
    int64_t var_60h;
    void * var_68h;
    uint32_t var_70h;
    uint32_t var_78h;
    int64_t var_80h;
    uint32_t var_91h;
    int64_t var_a0h;
    void * var_a8h;
    int64_t var_e0h;
    int64_t var_1e8h;
    rdi = argc;
    rsi = argv;
    r14 = 0x0000914c;
    r13 = rsi;
    r12 = 0x0000910b;
    rbp = (int64_t) edi;
    rbx = "+AcCdst";
    rax = *(fs:0x28);
    *((rsp + 0x1e8)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    setlocale (6, 0x0000a941);
    bindtextdomain (r12, "/usr/local/share/locale");
    r12 = obj_long_options;
    textdomain (r12, rsi);
    rdi = dbg_close_stdout;
    atexit ();
    do {
label_0:
        r8d = 0;
        rcx = r12;
        rdx = rbx;
        rsi = r13;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_24;
        }
        if (eax == 0x63) {
            goto label_25;
        }
        if (eax > 0x63) {
            goto label_26;
        }
        if (eax == 0xffffff7e) {
            goto label_27;
        }
        if (eax < 0xffffff7f) {
            goto label_28;
        }
        if (eax != 0x41) {
            goto label_29;
        }
        setlocale (3, r14);
        eax = setlocale (0, r14);
    } while (1);
label_26:
    if (eax == 0x73) {
        goto label_30;
    }
    if (eax == 0x74) {
        *(obj.truncate_set1) = 1;
        goto label_0;
label_29:
        if (eax != 0x43) {
            goto label_1;
        }
label_25:
        *(obj.complement) = 1;
        goto label_0;
label_28:
        if (eax != 0xffffff7d) {
            goto label_1;
        }
        eax = 0;
        version_etc (*(obj.stdout), 0x0000908e, "GNU coreutils", *(obj.Version), "Jim Meyering", 0);
        eax = exit (0);
    }
    if (eax != 0x64) {
        goto label_1;
    }
    *(obj.delete) = 1;
    goto label_0;
label_24:
    eax = optind;
    ebx = ebp;
    edx = *(obj.delete);
    ecx = *(obj.squeeze_repeats);
    ebx -= eax;
    if (ebx != 2) {
        goto label_31;
    }
    if (dl != 0) {
        goto label_32;
    }
    *(obj.translating) = 1;
    edx = 2;
    if (cl == 0) {
        goto label_33;
    }
    do {
        if (ebx <= edx) {
            goto label_34;
        }
        eax += edx;
        rax = (int64_t) eax;
        rax = quote (*((r13 + rax*8)), rsi, rdx, rcx, r8);
        edx = 5;
        r12 = rax;
        rax = dcgettext (0, "extra operand %s");
        rcx = r12;
        eax = 0;
        error (0, 0, rax);
        edx = 5;
        rsi = "Only one string may be given when deleting without squeezing repeats.";
        if (ebx == 2) {
label_2:
            rax = dcgettext (0, rsi);
            rdi = stderr;
            esi = 1;
            rdx = 0x0000a8f0;
            rcx = rax;
            eax = 0;
            fprintf_chk ();
        }
label_1:
        usage (1, rsi, rdx, rcx, r8, r9);
label_30:
        *(obj.squeeze_repeats) = 1;
        goto label_0;
label_27:
        usage (0, rsi, rdx, rcx, r8, r9);
label_31:
        *(obj.translating) = 0;
        if (dl == cl) {
            goto label_35;
        }
label_4:
        esi = 1;
        edx -= edx;
        edx += 2;
label_3:
    } while (ebx >= esi);
    if (ebx == 0) {
        edx = 5;
        rax = dcgettext (0, "missing operand");
        eax = 0;
        error (0, 0, rax);
        goto label_1;
    }
    rax = quote (*((r13 + rbp*8 - 8)), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "missing operand after %s");
    rcx = r12;
    eax = 0;
    error (0, 0, rax);
    edx = 5;
    rsi = "Two strings must be given when both deleting and squeezing repeats.";
    rax = "Two strings must be given when translating.";
    if (*(obj.squeeze_repeats) == 0) {
        rsi = rax;
    }
    goto label_2;
label_35:
    esi = 2;
    edx = 2;
    goto label_3;
label_32:
    *(obj.translating) = 0;
    if (cl == 0) {
        goto label_4;
    }
label_33:
    r12 = rsp + 0x60;
    rax = xmalloc (0x20);
    *((rsp + 0x68)) = rax;
    *((rsp + 0x60)) = rax;
    *((rax + 8)) = 0;
    rax = *(obj.optind);
    al = parse_str (*((r13 + rax*8)), r12, rdx, rcx, r8, r9);
    if (al != 0) {
        goto label_36;
    }
    do {
label_9:
        exit (1);
label_34:
        r12 = rsp + 0x60;
        rax = xmalloc (0x20);
        *((rsp + 0x68)) = rax;
        *((rsp + 0x60)) = rax;
        *((rax + 8)) = 0;
        rax = *(obj.optind);
        al = parse_str (*((r13 + rax*8)), r12, rdx, rcx, r8, r9);
    } while (al == 0);
    ebp = 0;
    if (ebx == 2) {
        goto label_36;
    }
label_8:
    rdi = r12;
    get_spec_stats ();
    if (*(obj.complement) != 0) {
        goto label_37;
    }
label_10:
    r15 = *((rsp + 0x80));
    if (r15 != 0) {
        goto label_38;
    }
    if (rbp != 0) {
        r14 = *((rsp + 0x78));
        rdi = rbp;
        *((rsp + 0x18)) = r14;
        get_spec_stats ();
        rax = *((rbp + 0x18));
        if (r14 >= rax) {
            if (*((rbp + 0x20)) == 1) {
                goto label_39;
            }
        }
        rax = *((rbp + 0x20));
        if (rax > 1) {
            goto label_40;
        }
        if (*(obj.translating) != 0) {
            goto label_21;
        }
        if (rax != 0) {
            goto label_41;
        }
    }
label_13:
    fadvise (*(obj.stdin), 2);
    eax = *(obj.squeeze_repeats);
    *((rsp + 0x10)) = al;
    if (ebx == 1) {
        if (al != 0) {
            goto label_42;
        }
    }
    if (*(obj.delete) != 0) {
        ebx--;
        if (ebx == 0) {
            goto label_43;
        }
        if (*((rsp + 0x10)) != 0) {
            goto label_44;
        }
    }
    if (*(obj.translating) == 0) {
        goto label_18;
    }
    eax = 0;
    rbx = obj_xlate;
    if (*(obj.complement) != 0) {
        goto label_45;
    }
    do {
        *((rbx + rax)) = al;
        rax++;
    } while (rax != 0x100);
    rcx = rsp + 0x5c;
    rax = 0xfffffffffffffffe;
    r14 = rsp + 0x58;
    r15 = r12;
    *((rsp + 0x70)) = 0xfffffffffffffffe;
    *((rsp + 8)) = rcx;
label_6:
    *((rbp + 0x10)) = rax;
    while (edx == 0) {
        if (*((rsp + 0x5c)) == 1) {
            goto label_46;
        }
label_5:
        if (r13d == 0xffffffff) {
            goto label_15;
        }
        if (eax == 0xffffffff) {
            goto label_47;
        }
        *((rbx + r13)) = al;
        if (*((rsp + 0x5c)) != 2) {
            goto label_7;
        }
        rsi = r14;
        rdi = r15;
        eax = get_next ();
        rsi = *((rsp + 8));
        rdi = rbp;
        r13 = (int64_t) eax;
        get_next ();
        edx = *((rsp + 0x58));
    }
    edx--;
    if (edx != 0) {
        goto label_5;
    }
    if (*((rsp + 0x5c)) != 0) {
        goto label_5;
    }
    rax = ctype_b_loc ();
    r13d = 0;
    r12 = *(rax);
    do {
        if ((*((r12 + r13*2 + 1)) & 1) != 0) {
            rax = ctype_tolower_loc ();
            rax = *(rax);
            eax = *((rax + r13*4));
            *((rbx + r13)) = al;
        }
        r13++;
    } while (r13 != 0x100);
label_7:
    *((rsp + 0x70)) = 0xffffffffffffffff;
    rax = *((rsp + 0x68));
    rax = *((rax + 8));
    *((rsp + 0x68)) = rax;
    rax = *((rbp + 8));
    rax = *((rax + 8));
    *((rbp + 8)) = rax;
    rax |= 0xffffffffffffffff;
    goto label_6;
label_15:
    r12 = obj_io_buf;
    if (*((rsp + 0x10)) == 0) {
        goto label_48;
    }
    goto label_49;
    do {
        rcx = stdout;
        rdx = rax;
        esi = 1;
        rdi = r12;
        rax = fwrite_unlocked ();
        if (rbx != rax) {
            goto label_50;
        }
label_48:
        rax = read_and_xlate (r12, sym._init, rdx, rcx, r8, r9);
        rbx = rax;
    } while (rax != 0);
label_18:
    eax = close (0);
    if (eax != 0) {
        goto label_51;
    }
    exit (0);
label_46:
    rax = ctype_b_loc ();
    r13d = 0;
    r12 = *(rax);
    do {
        if ((*((r12 + r13*2 + 1)) & 2) != 0) {
            rax = ctype_toupper_loc ();
            rax = *(rax);
            eax = *((rax + r13*4));
            *((rbx + r13)) = al;
        }
        r13++;
    } while (r13 != 0x100);
    goto label_7;
label_36:
    rbp = rsp + 0xa0;
    rax = xmalloc (0x20);
    *((rsp + 0xa8)) = rax;
    *((rsp + 0xa0)) = rax;
    *((rax + 8)) = 0;
    rax = *(obj.optind);
    al = parse_str (*((r13 + rax*8 + 8)), rbp, rdx, rcx, r8, r9);
    if (al != 0) {
        goto label_8;
    }
    goto label_9;
label_37:
    rdi = rsp + 0xe0;
    ecx = 0x20;
    eax = 0;
    *((rsp + 0x70)) = 0xfffffffffffffffe;
    *(rdi) = rax;
    rcx--;
    rdi += 8;
    r13d = 0x100;
    while (eax != 0xffffffff) {
        rax = (int64_t) eax;
        edx = *((rsp + rax + 0xe0));
        *((rsp + rax + 0xe0)) = 1;
        edx ^= 1;
        edx = (int32_t) dl;
        r13d -= edx;
        esi = 0;
        rdi = r12;
        eax = get_next ();
    }
    r13 = (int64_t) r13d;
    *((rsp + 0x78)) = r13;
    goto label_10;
label_21:
    if (*((rbp + 0x30)) != 0) {
        goto label_52;
    }
    if (*((rbp + 0x32)) != 0) {
        goto label_53;
    }
    rax = *((rbp + 0x18));
    *((rsp + 0x20)) = rax;
    rax = *((rsp + 0x68));
    *((rsp + 0x30)) = rax;
    rax = *((rbp + 8));
    *((rsp + 0x38)) = rax;
    if (*(obj.complement) != 0) {
        goto label_54;
    }
    eax = *((rbp + 0x31));
    r14d = eax;
    if (al == 0) {
        goto label_55;
    }
    rax = ctype_b_loc ();
    edx = 0;
    esi = 0;
    r8d = 0;
    r9 = *(rax);
    do {
        eax = *((r9 + rdx*2));
        ecx = eax;
        cx &= 0x100;
        r8 -= 0xffffffffffffffff;
        ax &= 0x200;
        rsi -= 0xffffffffffffffff;
        rdx++;
    } while (rdx != 0x100);
    rax = rsp + 0x5c;
    rcx = rsi - 1;
    r13d = r14d;
    *((rsp + 0x4c)) = ebx;
    *((rbp + 0x10)) = 0xfffffffffffffffe;
    *((rsp + 8)) = r14b;
    r14 = rsp + 0x58;
    *((rsp + 0x40)) = r15;
    r15 = rbp;
    *((rsp + 0x70)) = 0xfffffffffffffffe;
    *((rsp + 0x10)) = rcx;
    *((rsp + 0x28)) = r8;
    while (r13b != 0) {
        if (esi != 2) {
            if (*((rsp + 8)) == 0) {
                goto label_56;
            }
            if (*((rsp + 0x58)) == 2) {
                goto label_56;
            }
label_11:
            rcx = *((rsp + 0x68));
            *((rsp + 0x70)) = 0xffffffffffffffff;
            rdi = *((rsp + 0x10));
            rcx = *((rcx + 8));
            *((rsp + 0x68)) = rcx;
            rcx = *((r15 + 8));
            rcx = *((rcx + 8));
            *((r15 + 0x10)) = 0xffffffffffffffff;
            *((r15 + 8)) = rcx;
            rcx = *((rsp + 0x78));
            if (*((rsp + 0x58)) == 1) {
                rdx = *((rsp + 0x28));
                rdi = rdx - 1;
            }
            rcx -= rdi;
            esi--;
            rdi = *((rsp + 0x10));
            *((rsp + 0x78)) = rcx;
            rcx = *((r15 + 0x18));
            if (esi == 0) {
                rdx = *((rsp + 0x28));
                rdi = rdx - 1;
            }
            rcx -= rdi;
            *((r15 + 0x18)) = rcx;
        }
label_12:
        rsp + 8 = (*((rsp + 0x70)) == -1) ? 1 : 0;
        r13b = (*((r15 + 0x10)) == -1) ? 1 : 0;
        ebx++;
        if (ebx == 0) {
            goto label_57;
        }
        eax++;
        if (eax == 0) {
            goto label_57;
        }
        rsi = r14;
        rdi = r12;
        eax = get_next ();
        rsi = rbp;
        rdi = r15;
        ebx = eax;
        get_next ();
        esi = *((rsp + 0x5c));
    }
    if (esi != 2) {
        goto label_11;
    }
    goto label_12;
label_57:
    ebx = *((rsp + 0x4c));
    r15 = *((rsp + 0x40));
    rax = *((rsp + 0x18));
    if (rax < *((rsp + 0x78))) {
        goto label_58;
    }
    rax = *((rsp + 0x20));
    if (rax < *((rbp + 0x18))) {
        goto label_58;
    }
    rax = *((rsp + 0x30));
    *((rsp + 0x68)) = rax;
    rax = *((rsp + 0x38));
    *((rbp + 8)) = rax;
    rax = *((rbp + 0x18));
    *((rsp + 0x20)) = rax;
label_55:
    rax = *((rsp + 0x78));
    *((rsp + 0x18)) = rax;
    if (rax <= *((rsp + 0x20))) {
        goto label_13;
    }
    if (*(obj.truncate_set1) != 0) {
        goto label_13;
    }
label_20:
    if (*((rsp + 0x20)) == 0) {
        goto label_59;
    }
    rax = *((rbp + 8));
    edx = *(rax);
    if (edx == 1) {
        goto label_60;
    }
    if (edx > 1) {
        goto label_61;
    }
label_22:
    r14d = *((rax + 0x10));
label_23:
    r13 = *((rsp + 0x18));
    r13 -= *((rsp + 0x20));
    xmalloc (0x20);
    rdx = *((rbp + 8));
    *((rax + 8)) = 0;
    *(rax) = 4;
    *((rax + 0x10)) = r14b;
    *((rax + 0x18)) = r13;
    if (rdx == 0) {
        goto label_62;
    }
    *((rdx + 8)) = rax;
    *((rbp + 8)) = rax;
    rax = *((rsp + 0x18));
    *((rbp + 0x18)) = rax;
    if (*(obj.complement) == 0) {
        goto label_13;
    }
label_19:
    if (*((rsp + 0x91)) == 0) {
        goto label_13;
    }
    rax = *((rsp + 0x18));
    if (*((rbp + 0x18)) != rax) {
        goto label_63;
    }
    *((rbp + 0x10)) = 0xfffffffffffffffe;
    esi = 0;
    rdi = rbp;
    eax = get_next ();
    r13d = eax;
    if (eax == 0xffffffff) {
        goto label_63;
    }
label_14:
    esi = 0;
    rdi = rbp;
    eax = get_next ();
    if (eax == 0xffffffff) {
        goto label_13;
    }
    if (r13d == eax) {
        goto label_14;
    }
label_63:
    edx = 5;
    rax = dcgettext (0, "when translating with complemented character classes,\nstring2 must map all characters in the domain to one");
    eax = 0;
    error (1, 0, rax);
label_47:
    r15d = r13d;
    r15d++;
    if (r15d == 0) {
        goto label_15;
    }
    if (*(obj.truncate_set1) != 0) {
        goto label_15;
    }
    assert_fail ("c1 == -1 || truncate_set1", "src/tr.c", 0x765, "main");
label_45:
    r14 = obj_in_delete_set;
    esi = 0;
    rdi = r12;
    rdx = r14;
    eax = set_initialize ();
    *((rbp + 0x10)) = 0xfffffffffffffffe;
    eax = 0;
    do {
        *((rbx + rax)) = al;
        rax++;
    } while (rax != 0x100);
    goto label_64;
label_17:
    esi = 0;
    rdi = rbp;
    eax = get_next ();
    if (eax == 0xffffffff) {
        goto label_65;
    }
    *((rbx + r15)) = al;
label_16:
    r15++;
    if (r15 == 0x100) {
        goto label_15;
    }
label_64:
    if (*((r14 + r15)) != 0) {
        goto label_16;
    }
    goto label_17;
label_65:
    if (*(obj.truncate_set1) != 0) {
        goto label_15;
    }
    assert_fail ("ch != -1 || truncate_set1", "src/tr.c", 0x730, "main");
label_49:
    rdi = rbp;
    rdx = obj_in_squeeze_set;
    esi = 0;
    set_initialize ();
    rdi = dbg_read_and_xlate;
    squeeze_filter_constprop_0 ();
    goto label_18;
label_54:
    rcx = *((rsp + 0x20));
    if (*((rsp + 0x18)) <= rcx) {
        goto label_19;
    }
    if (*(obj.truncate_set1) != 0) {
        goto label_19;
    }
    goto label_20;
label_42:
    esi = *(obj.complement);
    rdi = r12;
    rdx = obj_in_squeeze_set;
    set_initialize ();
    rdi = sym_plain_read;
    rax = squeeze_filter_constprop_0 ();
    goto label_18;
label_39:
    rcx = *((rsp + 0x18));
    rdx = *((rbp + 0x28));
    rsi = rcx;
    rsi -= rax;
    *((rdx + 0x18)) = rsi;
    *((rbp + 0x18)) = rcx;
    if (*(obj.translating) != 0) {
        goto label_21;
    }
label_41:
    edx = 5;
    rax = dcgettext (0, "the [c*] construct may appear in string2 only when translating");
    eax = 0;
    error (1, 0, rax);
label_43:
    esi = *(obj.complement);
    rdi = r12;
    rdx = obj_in_delete_set;
    r12 = obj_io_buf;
    rax = set_initialize ();
    while (rax != 0) {
        rcx = stdout;
        rdx = rax;
        esi = 1;
        rdi = r12;
        rax = fwrite_unlocked ();
        if (rbx != rax) {
            goto label_66;
        }
        rax = read_and_delete (r12, sym._init, rdx, rcx, r8, r9);
        rbx = rax;
    }
    goto label_18;
label_61:
    if (edx == 2) {
        goto label_67;
    }
    if (edx == 4) {
        goto label_22;
    }
    void (*0x26cf)() ();
label_44:
    esi = *(obj.complement);
    rdi = r12;
    rdx = obj_in_delete_set;
    set_initialize ();
    rdi = rbp;
    rdx = obj_in_squeeze_set;
    esi = 0;
    set_initialize ();
    rdi = dbg_read_and_delete;
    squeeze_filter_constprop_0 ();
    goto label_18;
label_60:
    r14d = *((rax + 0x11));
    goto label_23;
label_62:
    append_repeated_char_part_0 ();
label_56:
    edx = 5;
    rax = dcgettext (0, "misaligned [:upper:] and/or [:lower:] construct");
    eax = 0;
    error (1, 0, rax);
label_53:
    edx = 5;
    rax = dcgettext (0, "when translating, the only character classes that may appear in\nstring2 are 'upper' and 'lower');
    eax = 0;
    error (1, 0, rax);
label_52:
    edx = 5;
    rax = dcgettext (0, "[=c=] expressions may not appear in string2 when translating");
    eax = 0;
    error (1, 0, rax);
label_51:
    edx = 5;
    rax = dcgettext (0, "standard input");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_66:
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    do {
        eax = 0;
    } while (rcx != 0);
    error (1, *(rax), r12);
label_58:
    assert_fail ("old_s1_len >= s1->length && old_s2_len >= s2->length", "src/tr.c", 0x4c8, "validate_case_classes");
label_50:
    edx = 5;
    rax = dcgettext (0, "write error");
    r12 = rax;
    rax = errno_location ();
    eax = 0;
    error (1, *(rax), r12);
label_59:
    edx = 5;
    rax = dcgettext (0, "when not truncating set1, string2 must be non-empty");
    eax = 0;
    error (1, 0, rax);
label_67:
    edx = 5;
    rax = dcgettext (0, "when translating with string1 longer than string2,\nthe latter string must not end with a character class");
    eax = 0;
    error (1, 0, rax);
label_38:
    edx = 5;
    rax = dcgettext (0, "the [c*] repeat construct may not appear in string1");
    eax = 0;
    error (1, 0, rax);
label_40:
    edx = 5;
    rax = dcgettext (0, "only one [c*] repeat construct may appear in string2");
    eax = 0;
    error (1, 0, rax);
}

/* /tmp/tmprlq2jj6i @ 0x4a80 */
 
int64_t dbg_usage (int64_t arg_8h, int64_t arg_10h, char * arg_18h, char * arg_20h, int64_t arg_28h, int64_t arg_30h, int64_t arg_38h, int64_t arg_40h, int64_t arg_48h, int64_t arg_50h, int64_t arg_58h, int64_t arg_60h, int64_t arg_68h, int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        exit (ebp);
    }
    rbx = "sha256sum";
    rax = dcgettext (0, "Usage: %s [OPTION]... STRING1 [STRING2]\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Translate, squeeze, and/or delete characters from standard input,\nwriting to standard output.  STRING1 and STRING2 specify arrays of\ncharacters ARRAY1 and ARRAY2 that control the action.\n\n  -c, -C, --complement    use the complement of ARRAY1\n  -d, --delete            delete characters in ARRAY1, do not translate\n  -s, --squeeze-repeats   replace each sequence of a repeated character\n                            that is listed in the last specified ARRAY,\n                            with a single occurrence of that character\n  -t, --truncate-set1     first truncate ARRAY1 to length of ARRAY2\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nARRAYs are specified as strings of characters.  Most represent themselves.\nInterpreted sequences are:\n\n  \\NNN            character with octal value NNN (1 to 3 octal digits)\n  \\\\              backslash\n  \\a              audible BEL\n  \\b              backspace\n  \\f              form feed\n  \\n              new line\n  \\r              return\n  \\t              horizontal tab\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  \\v              vertical tab\n  CHAR1-CHAR2     all characters from CHAR1 to CHAR2 in ascending order\n  [CHAR*]         in ARRAY2, copies of CHAR until length of ARRAY1\n  [CHAR*REPEAT]   REPEAT copies of CHAR, REPEAT octal if starting with 0\n  [:alnum:]       all letters and digits\n  [:alpha:]       all letters\n  [:blank:]       all horizontal whitespace\n  [:cntrl:]       all control characters\n  [:digit:]       all digits\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  [:graph:]       all printable characters, not including space\n  [:lower:]       all lower case letters\n  [:print:]       all printable characters, including space\n  [:punct:]       all punctuation characters\n  [:space:]       all horizontal or vertical whitespace\n  [:upper:]       all upper case letters\n  [:xdigit:]      all hexadecimal digits\n  [=CHAR=]        all characters which are equivalent to CHAR\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    edx = 5;
    r12 = stdout;
    rax = dcgettext (0, "\nTranslation occurs if -d is not given and both STRING1 and STRING2 appear.\n-t may be used only when translating.  ARRAY2 is extended to length of\nARRAY1 by repeating its last character as necessary.  Excess characters\nof ARRAY2 are ignored.  Character classes expand in unspecified order;\nwhile translating, [:lower:] and [:upper:] may be used in pairs to\nspecify case conversion.  Squeezing occurs after translation or deletion.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    rax = 0x00009091;
    *((rsp + 0x30)) = rbx;
    rbx = "sha384sum";
    *(rsp) = rax;
    rax = "test invocation";
    rdx = rsp;
    esi = 0x74;
    *((rsp + 8)) = rax;
    rax = 0x0000910b;
    edi = 0x72;
    *((rsp + 0x10)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x18)) = rax;
    rax = "sha224sum";
    *((rsp + 0x20)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x40)) = rbx;
    rbx = "sha512sum";
    *((rsp + 0x28)) = rax;
    *((rsp + 0x38)) = rax;
    *((rsp + 0x48)) = rax;
    *((rsp + 0x50)) = rbx;
    *((rsp + 0x58)) = rax;
    *((rsp + 0x60)) = 0;
    *((rsp + 0x68)) = 0;
    do {
label_0:
        rax = *((rdx + 0x10));
        rdx += 0x10;
        if (rax == 0) {
            goto label_3;
        }
        ecx = *(rax);
    } while (esi != ecx);
    ecx = *((rax + 1));
    if (edi != ecx) {
        goto label_0;
    }
    if (*((rax + 2)) != 0) {
        goto label_0;
    }
label_3:
    r12 = *((rdx + 8));
    rsi = "\n%s online help: <%s>\n";
    edx = 5;
    edi = 0;
    if (r12 == 0) {
        goto label_4;
    }
    rax = dcgettext (rdi, rsi);
    r13 = "https://www.gnu.org/software/coreutils/";
    rdx = "GNU coreutils";
    edi = 1;
    rsi = rax;
    rcx = r13;
    rbx = 0x0000908e;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x00009115, 3);
        if (eax != 0) {
            goto label_5;
        }
    }
label_2:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rdx = r13;
    rcx = rbx;
    edi = 1;
    rsi = rax;
    eax = 0;
    r13 = 0x000090ad;
    printf_chk ();
    rax = 0x0000a941;
    r13 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r13;
        rdx = r12;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_1;
label_4:
        rax = dcgettext (rdi, rsi);
        r13 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r13;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x00009115, 3);
            if (eax != 0) {
                goto label_6;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        r12 = 0x0000908e;
        rdx = r13;
        edi = 1;
        rsi = rax;
        rcx = r12;
        r13 = 0x000090ad;
        eax = 0;
        printf_chk ();
    }
label_6:
    rbx = 0x0000908e;
    r12 = rbx;
label_5:
    r14 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r14;
    fputs_unlocked ();
    goto label_2;
}

/* /tmp/tmprlq2jj6i @ 0x6750 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x6e40 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010510]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00010520]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x6c70 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x26f3)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x6b50 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x26e9)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x72e0 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmprlq2jj6i @ 0x8020 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = 0x400;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x0000ac98;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0xac98 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = 0x400;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmprlq2jj6i @ 0x67f0 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x26de)() ();
    }
    if (rdx == 0) {
        void (*0x26de)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x6d00 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x00010510]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00010520]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x7e40 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x7ec0 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x4f40 */
 
uint32_t dbg_fadvise (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void fadvise(FILE * fp,fadvice_t advice); */
    if (rdi != 0) {
        r12d = esi;
        eax = fileno (rdi);
        ecx = r12d;
        edx = 0;
        esi = 0;
        edi = eax;
        void (*0x2500)() ();
    }
    return eax;
}

/* /tmp/tmprlq2jj6i @ 0x2570 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmprlq2jj6i @ 0x6a50 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmprlq2jj6i @ 0x7b60 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x72c0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmprlq2jj6i @ 0x85a0 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x2610)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmprlq2jj6i @ 0x6790 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x7d40 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmprlq2jj6i @ 0x7980 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmprlq2jj6i @ 0x2550 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmprlq2jj6i @ 0x25f0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmprlq2jj6i @ 0x4e60 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmprlq2jj6i @ 0x8450 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmprlq2jj6i @ 0x23e0 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmprlq2jj6i @ 0x7370 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000a8e8;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000a8fb);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000abe8;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xabe8 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2660)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2660)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2660)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmprlq2jj6i @ 0x7800 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmprlq2jj6i @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmprlq2jj6i @ 0x7ea0 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x8820 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmprlq2jj6i @ 0x7ba0 */
 
uint64_t dbg_xnmalloc (uint32_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x7f60 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2560)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmprlq2jj6i @ 0x2380 */
 
void ctype_toupper_loc (void) {
    /* [15] -r-x section size 832 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmprlq2jj6i @ 0x2390 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmprlq2jj6i @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    /* [39] ---- section size 407 named .shstrtab */
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax)++;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rdi) += 0;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += eax;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += bpl;
    *(rdi) += ah;
    *(rcx) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(fp_stack--) = *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += bl;
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *((rax + rax)) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    if (*(rax) >= 0) {
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += dl;
        *(rax) += al;
        *(rax) += al;
        *(rax) += al;
        *(rax) += eax;
        *(rax) += al;
        *(0x000000f1) += al;
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *((rcx + 0x68)) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    __asm ("enter 0x32, 0");
    *(rax) += al;
    *(rax) += al;
    __asm ("enter 0x32, 0");
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmprlq2jj6i @ 0x23d0 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmprlq2jj6i @ 0x2400 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmprlq2jj6i @ 0x2410 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmprlq2jj6i @ 0x2420 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmprlq2jj6i @ 0x2430 */
 
void stpcpy (void) {
    __asm ("bnd jmp qword [reloc.stpcpy]");
}

/* /tmp/tmprlq2jj6i @ 0x2450 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmprlq2jj6i @ 0x2480 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmprlq2jj6i @ 0x2490 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmprlq2jj6i @ 0x24a0 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmprlq2jj6i @ 0x24c0 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmprlq2jj6i @ 0x24e0 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmprlq2jj6i @ 0x24f0 */
 
void close (void) {
    __asm ("bnd jmp qword [reloc.close]");
}

/* /tmp/tmprlq2jj6i @ 0x2500 */
 
void posix_fadvise (void) {
    __asm ("bnd jmp qword [reloc.posix_fadvise]");
}

/* /tmp/tmprlq2jj6i @ 0x2510 */
 
void read (void) {
    __asm ("bnd jmp qword [reloc.read]");
}

/* /tmp/tmprlq2jj6i @ 0x2520 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmprlq2jj6i @ 0x2530 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmprlq2jj6i @ 0x2560 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmprlq2jj6i @ 0x2590 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmprlq2jj6i @ 0x25b0 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmprlq2jj6i @ 0x25c0 */
 
void fwrite_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fwrite_unlocked]");
}

/* /tmp/tmprlq2jj6i @ 0x25e0 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmprlq2jj6i @ 0x2610 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmprlq2jj6i @ 0x2620 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmprlq2jj6i @ 0x2630 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmprlq2jj6i @ 0x2640 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmprlq2jj6i @ 0x2660 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmprlq2jj6i @ 0x2670 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmprlq2jj6i @ 0x2680 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmprlq2jj6i @ 0x2690 */
 
void ctype_tolower_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_tolower_loc]");
}

/* /tmp/tmprlq2jj6i @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 848 named .plt */
    __asm ("bnd jmp qword [0x0000de18]");
}

/* /tmp/tmprlq2jj6i @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmprlq2jj6i @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}
