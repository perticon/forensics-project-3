
int64_t g28;

void** fun_2460(void** rdi, ...);

void** xmalloc(void** rdi, ...);

void** xcalloc(void** rdi, void** rsi);

void fun_24d0(void** rdi, void*** rsi, int64_t rdx, void** rcx, ...);

/* append_repeated_char.part.0 */
void append_repeated_char_part_0(void** rdi, ...);

void fun_2390(void** rdi, ...);

void fun_2470();

void** make_printable_char(uint32_t edi, void** rsi, void** rdx);

void** fun_2440();

void fun_2600();

void** make_printable_str(void** rdi, void** rsi, ...);

void** quote(void** rdi, ...);

int32_t xstrtoumax();

int32_t fun_23c0(void** rdi, void** rsi, void** rdx, void** rcx);

struct s0 {
    void* f0;
    void* f8;
    void** f10;
};

signed char star_digits_closebracket(struct s0* rdi, void** rsi, ...);

signed char parse_str(void** rdi, void** rsi) {
    void** r13_3;
    void** v4;
    int64_t rax5;
    int64_t v6;
    void** rax7;
    void** rax8;
    void** rsi9;
    void** rbp10;
    void** rax11;
    void*** rsp12;
    uint32_t ebx13;
    void** r14_14;
    void** rcx15;
    void** r15_16;
    void** rdi17;
    void** r13_18;
    unsigned char* r12_19;
    unsigned char* rbx20;
    unsigned char v21;
    void** r13_22;
    uint32_t r15d23;
    void** rax24;
    void** rdx25;
    int64_t rax26;
    uint32_t eax27;
    void** rcx28;
    signed char* r12_29;
    void** rdx30;
    void** rax31;
    uint32_t ebx32;
    void** rax33;
    void** rax34;
    void** r12_35;
    void** rax36;
    void** r12_37;
    void** v38;
    void** v39;
    void** rax40;
    void** rax41;
    int64_t rax42;
    void** rax43;
    void** r8_44;
    void** v45;
    uint32_t eax46;
    uint32_t eax47;
    void** rax48;
    void** rax49;
    void** rbx50;
    void** rax51;
    void** rbx52;
    int32_t eax53;
    void* v54;
    void** rax55;
    void** rax56;
    void** v57;
    void** v58;
    void** v59;
    unsigned char v60;
    void** r12_61;
    void** v62;
    void** rbx63;
    unsigned char v64;
    void** r14_65;
    void** rbp66;
    int32_t eax67;
    void** rax68;
    void** rax69;
    uint32_t edx70;
    signed char al71;
    void** r11_72;
    struct s0* rdi73;
    signed char al74;
    void* rax75;
    void* rdx76;
    int64_t r9_77;
    void* r10_78;
    void** rax79;
    uint32_t r9d80;

    r13_3 = rdi;
    v4 = rsi;
    rax5 = g28;
    v6 = rax5;
    rax7 = fun_2460(rdi);
    rax8 = xmalloc(rax7);
    *reinterpret_cast<int32_t*>(&rsi9) = 1;
    *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
    rbp10 = rax8;
    rax11 = xcalloc(rax7, 1);
    rsp12 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x88 - 8 + 8 - 8 + 8 - 8 + 8);
    ebx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_3));
    r14_14 = rax11;
    if (*reinterpret_cast<signed char*>(&ebx13)) {
        rcx15 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r15_16) = 0;
        goto addr_422c_3;
    }
    addr_4a0f_4:
    fun_24d0("list->tail", "src/tr.c", 0x2bd, "append_char_class", "list->tail", "src/tr.c", 0x2bd, "append_char_class");
    addr_4a2e_5:
    fun_24d0("list->tail", "src/tr.c", 0x2a7, "append_range", "list->tail", "src/tr.c", 0x2a7, "append_range");
    addr_4a4d_6:
    rdi17 = reinterpret_cast<void**>("list->tail");
    fun_24d0("list->tail", "src/tr.c", 0x289, "append_normal_char", "list->tail", "src/tr.c", 0x289, "append_normal_char");
    addr_4a6c_7:
    append_repeated_char_part_0(rdi17, rdi17);
    addr_4a02_9:
    *reinterpret_cast<int32_t*>(&r13_18) = 0;
    *reinterpret_cast<int32_t*>(&r13_18 + 4) = 0;
    addr_4452_10:
    r12_19 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r13_18));
    rbx20 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r15_16));
    if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(r13_18)) {
        addr_44af_11:
        v21 = 1;
    } else {
        r13_22 = v4;
        do {
            r15d23 = *r12_19;
            rax24 = xmalloc(32, 32);
            rdx25 = *reinterpret_cast<void***>(r13_22 + 8);
            *reinterpret_cast<void***>(rax24 + 8) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<unsigned char*>(rax24 + 16) = *reinterpret_cast<unsigned char*>(&r15d23);
            if (!rdx25) 
                goto addr_4a4d_6;
            ++r12_19;
            *reinterpret_cast<void***>(rdx25 + 8) = rax24;
            *reinterpret_cast<void***>(r13_22 + 8) = rax24;
        } while (rbx20 != r12_19);
        goto addr_44af_11;
    }
    addr_4689_15:
    fun_2390(rbp10, rbp10);
    fun_2390(r14_14, r14_14);
    rax26 = v6 - g28;
    if (rax26) {
        fun_2470();
        goto addr_4a0f_4;
    } else {
        eax27 = v21;
        return *reinterpret_cast<signed char*>(&eax27);
    }
    addr_4449_18:
    r14_14 = r15_16;
    r15_16 = r13_3;
    r13_18 = rcx28;
    goto addr_4452_10;
    addr_4638_19:
    r14_14 = r15_16;
    rax31 = make_printable_char(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&r12_29)), rsi9, rdx30);
    rax33 = make_printable_char(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ebx32)), rsi9, rdx30);
    fun_2440();
    fun_2600();
    fun_2390(rax31, rax31);
    fun_2390(rax33, rax33);
    goto addr_4689_15;
    addr_499f_20:
    r14_14 = r15_16;
    rax36 = make_printable_str(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rax34), r12_35, reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rax34), r12_35);
    r12_37 = rax36;
    quote(rax36, rax36);
    addr_4820_21:
    fun_2440();
    addr_461d_22:
    fun_2600();
    fun_2390(r12_37);
    goto addr_4689_15;
    addr_49e8_23:
    fun_2440();
    fun_2600();
    goto addr_4689_15;
    addr_47f4_24:
    r14_14 = r15_16;
    rax40 = make_printable_str(v38, v39, v38, v39);
    r12_37 = rax40;
    quote(rax40, rax40);
    goto addr_4820_21;
    addr_45ef_25:
    r14_14 = r15_16;
    rax41 = make_printable_str(v38, v39);
    r12_37 = rax41;
    fun_2440();
    goto addr_461d_22;
    addr_4265_26:
    *reinterpret_cast<uint32_t*>(&rax42) = *reinterpret_cast<unsigned char*>(&rdx30);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax42) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x92d0 + rax42 * 4) + 0x92d0;
    while (1) {
        *reinterpret_cast<int32_t*>(&rax43) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx15 + 1));
        *reinterpret_cast<int32_t*>(&rax43 + 4) = 0;
        *r12_29 = *reinterpret_cast<signed char*>(&ebx13);
        ebx13 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_3) + reinterpret_cast<unsigned char>(rax43));
        rcx15 = rax43;
        if (!*reinterpret_cast<signed char*>(&ebx13)) {
            if (reinterpret_cast<unsigned char>(r15_16) <= reinterpret_cast<unsigned char>(2)) 
                goto addr_4a02_9;
            r13_3 = r15_16;
            *reinterpret_cast<int32_t*>(&r8_44) = 2;
            *reinterpret_cast<int32_t*>(&r8_44 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx28) = 0;
            *reinterpret_cast<int32_t*>(&rcx28 + 4) = 0;
            r15_16 = r14_14;
            do {
                addr_4306_30:
                *reinterpret_cast<uint32_t*>(&r12_29) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rcx28));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_29) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r14_14) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rcx28) + 1);
                *reinterpret_cast<int32_t*>(&r14_14 + 4) = 0;
                v45 = rcx28 + 1;
                if (*reinterpret_cast<unsigned char*>(&r12_29) != 91) 
                    goto addr_42b0_31;
                eax46 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(rcx28));
                v21 = *reinterpret_cast<unsigned char*>(&eax46);
                if (*reinterpret_cast<unsigned char*>(&eax46)) 
                    goto addr_42b0_31;
                if (*reinterpret_cast<unsigned char*>(&r14_14) != 58 && *reinterpret_cast<unsigned char*>(&r14_14) != 61) {
                    goto addr_4348_35;
                }
                if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(v45)) || (rsi9 = r13_3 + 0xffffffffffffffff, reinterpret_cast<unsigned char>(r8_44) >= reinterpret_cast<unsigned char>(rsi9))) {
                    addr_4348_35:
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r8_44)) != 42 || *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(r8_44))) {
                        addr_42b0_31:
                        if (*reinterpret_cast<unsigned char*>(&r14_14) != 45 || (eax47 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(v45)), v21 = *reinterpret_cast<unsigned char*>(&eax47), !!*reinterpret_cast<unsigned char*>(&eax47))) {
                            rax48 = xmalloc(32, 32);
                            rsp12 = rsp12 - 8 + 8;
                            *reinterpret_cast<void***>(rax48 + 8) = reinterpret_cast<void**>(0);
                            rdx30 = *reinterpret_cast<void***>(v4 + 8);
                            *reinterpret_cast<void***>(rax48) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<unsigned char*>(rax48 + 16) = *reinterpret_cast<unsigned char*>(&r12_29);
                            if (!rdx30) 
                                goto addr_4a4d_6;
                            rcx28 = v45;
                            *reinterpret_cast<void***>(rdx30 + 8) = rax48;
                            *reinterpret_cast<void***>(v4 + 8) = rax48;
                            continue;
                        } else {
                            ebx32 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r8_44));
                            if (*reinterpret_cast<unsigned char*>(&ebx32) < *reinterpret_cast<unsigned char*>(&r12_29)) 
                                goto addr_4638_19;
                            rax49 = xmalloc(32, 32);
                            rsp12 = rsp12 - 8 + 8;
                            *reinterpret_cast<void***>(rax49 + 8) = reinterpret_cast<void**>(0);
                            rdx30 = *reinterpret_cast<void***>(v4 + 8);
                            *reinterpret_cast<void***>(rax49) = reinterpret_cast<void**>(1);
                            *reinterpret_cast<unsigned char*>(rax49 + 16) = *reinterpret_cast<unsigned char*>(&r12_29);
                            *reinterpret_cast<unsigned char*>(rax49 + 17) = *reinterpret_cast<unsigned char*>(&ebx32);
                            if (!rdx30) 
                                goto addr_4a2e_5;
                            *reinterpret_cast<void***>(rdx30 + 8) = rax49;
                            rcx28 = rcx28 + 3;
                            *reinterpret_cast<void***>(v4 + 8) = rax49;
                            continue;
                        }
                    } else {
                        rax34 = rcx28 + 3;
                        rbx50 = rax34;
                        if (reinterpret_cast<unsigned char>(r13_3) > reinterpret_cast<unsigned char>(rax34)) {
                            do {
                                if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(rbx50))) 
                                    goto addr_42b0_31;
                                if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rbx50)) == 93) 
                                    break;
                                ++rbx50;
                            } while (reinterpret_cast<unsigned char>(r13_3) > reinterpret_cast<unsigned char>(rbx50));
                            goto addr_42b0_31;
                        } else {
                            goto addr_42b0_31;
                        }
                    }
                } else {
                    rax51 = r8_44;
                    do {
                        *reinterpret_cast<uint32_t*>(&rdx30) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rax51));
                        *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
                        rbx52 = rax51;
                        ++rax51;
                        if (*reinterpret_cast<unsigned char*>(&rdx30) != *reinterpret_cast<unsigned char*>(&r14_14)) 
                            continue;
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rbx52) + 1) != 93) 
                            continue;
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(rax51) + 0xffffffffffffffff)) 
                            continue;
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_16) + reinterpret_cast<unsigned char>(rbx52) + 1)) 
                            goto addr_4591_52;
                    } while (reinterpret_cast<unsigned char>(rax51) < reinterpret_cast<unsigned char>(rsi9));
                    goto addr_4348_35;
                }
                r12_35 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx50) - reinterpret_cast<unsigned char>(v45) - 2);
                if (r12_35) {
                    eax53 = xstrtoumax();
                    rsp12 = rsp12 - 8 + 8;
                    if (eax53) 
                        goto addr_499f_20;
                    if (0) 
                        goto addr_499f_20;
                    if (!reinterpret_cast<int1_t>(v54 == reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(rax34)) + reinterpret_cast<unsigned char>(r12_35))) 
                        goto addr_499f_20;
                }
                rax55 = xmalloc(32, 32);
                rsp12 = rsp12 - 8 + 8;
                rdi17 = v4;
                *reinterpret_cast<void***>(rax55 + 8) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rax55 + 24) = reinterpret_cast<void**>(0);
                rdx30 = *reinterpret_cast<void***>(rdi17 + 8);
                *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(4);
                *reinterpret_cast<unsigned char*>(rax55 + 16) = *reinterpret_cast<unsigned char*>(&r14_14);
                if (!rdx30) 
                    goto addr_4a6c_7;
                rsi9 = v4;
                rcx28 = rbx50 + 1;
                *reinterpret_cast<void***>(rdx30 + 8) = rax55;
                r8_44 = rcx28 + 2;
                *reinterpret_cast<void***>(rsi9 + 8) = rax55;
                if (reinterpret_cast<unsigned char>(r13_3) > reinterpret_cast<unsigned char>(r8_44)) 
                    goto addr_4306_30; else 
                    goto addr_4449_18;
                addr_4591_52:
                v38 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r8_44));
                rax56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx52) - reinterpret_cast<unsigned char>(rcx28) - 2);
                v39 = rax56;
                if (!rax56) 
                    goto addr_49c9_61;
                if (*reinterpret_cast<unsigned char*>(&r14_14) == 58) {
                    v57 = rbp10;
                    v58 = r8_44;
                    v59 = rcx28;
                    v60 = *reinterpret_cast<unsigned char*>(&r12_29);
                    r12_61 = v38;
                    v62 = rbx52;
                    rbx63 = v39;
                    v64 = *reinterpret_cast<unsigned char*>(&r14_14);
                    r14_65 = reinterpret_cast<void**>(0);
                    do {
                        rdx30 = rbx63;
                        rbp66 = *reinterpret_cast<void***>(0xdb60 + reinterpret_cast<unsigned char>(r14_65) * 8);
                        rsi9 = rbp66;
                        eax67 = fun_23c0(r12_61, rsi9, rdx30, rcx28);
                        rsp12 = rsp12 - 8 + 8;
                        if (eax67) 
                            continue;
                        rax68 = fun_2460(rbp66, rbp66);
                        rsp12 = rsp12 - 8 + 8;
                        if (rbx63 == rax68) 
                            break;
                        ++r14_65;
                    } while (!reinterpret_cast<int1_t>(r14_65 == 12));
                    goto addr_47b0_67;
                } else {
                    if (v39 == 1) {
                        rax69 = xmalloc(32);
                        rsp12 = rsp12 - 8 + 8;
                        rsi9 = v4;
                        *reinterpret_cast<void***>(rax69 + 8) = reinterpret_cast<void**>(0);
                        edx70 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v38));
                        *reinterpret_cast<void***>(rax69) = reinterpret_cast<void**>(3);
                        *reinterpret_cast<unsigned char*>(rax69 + 16) = *reinterpret_cast<unsigned char*>(&edx70);
                        rdx30 = *reinterpret_cast<void***>(rsi9 + 8);
                        if (rdx30) 
                            goto addr_4882_70; else 
                            break;
                    } else {
                        rsi9 = r8_44;
                        al71 = star_digits_closebracket(rsp12 + 96, rsi9);
                        rsp12 = rsp12 - 8 + 8;
                        r8_44 = r8_44;
                        rcx28 = rcx28;
                        if (al71) 
                            goto addr_4348_35; else 
                            goto addr_45ef_25;
                    }
                }
                rbp10 = v57;
                rbx52 = v62;
                rax69 = xmalloc(32, 32);
                rsp12 = rsp12 - 8 + 8;
                r11_72 = r14_65;
                *reinterpret_cast<void***>(rax69 + 8) = reinterpret_cast<void**>(0);
                rdx30 = *reinterpret_cast<void***>(v4 + 8);
                *reinterpret_cast<void***>(rax69) = reinterpret_cast<void**>(2);
                *reinterpret_cast<unsigned char*>(rax69 + 16) = *reinterpret_cast<unsigned char*>(&r11_72);
                if (!rdx30) 
                    goto addr_4a0f_4;
                addr_4882_70:
                *reinterpret_cast<void***>(rdx30 + 8) = rax69;
                rcx28 = rbx52 + 2;
                *reinterpret_cast<void***>(v4 + 8) = rax69;
                continue;
                addr_47b0_67:
                rdi73 = reinterpret_cast<struct s0*>(rsp12 + 96);
                rbp10 = v57;
                *reinterpret_cast<uint32_t*>(&r12_29) = v60;
                rsi9 = v58;
                *reinterpret_cast<uint32_t*>(&r14_14) = v64;
                al74 = star_digits_closebracket(rdi73, rsi9, rdi73, rsi9);
                rsp12 = rsp12 - 8 + 8;
                r8_44 = v58;
                rcx28 = v59;
                if (al74) 
                    goto addr_4348_35; else 
                    goto addr_47f4_24;
                r8_44 = rcx28 + 2;
            } while (reinterpret_cast<unsigned char>(r13_3) > reinterpret_cast<unsigned char>(r8_44));
            goto addr_4449_18;
            rcx15 = reinterpret_cast<void**>("append_equiv_class");
            fun_24d0("list->tail", "src/tr.c", 0x2e7, "append_equiv_class");
            rsp12 = rsp12 - 8 + 8;
        } else {
            addr_422c_3:
            *reinterpret_cast<int32_t*>(&rax75) = *reinterpret_cast<int32_t*>(&r15_16);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax75) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r15_16) = *reinterpret_cast<int32_t*>(&r15_16) + 1;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            r12_29 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<uint64_t>(rax75));
            if (*reinterpret_cast<signed char*>(&ebx13) != 92) 
                continue;
            *reinterpret_cast<int32_t*>(&rdx76) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx15 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx76) + 4) = 0;
            rbx52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_14) + reinterpret_cast<uint64_t>(rax75));
            *reinterpret_cast<uint32_t*>(&r9_77) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r13_3) + reinterpret_cast<uint64_t>(rdx76));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_77) + 4) = 0;
            *reinterpret_cast<void***>(rbx52) = reinterpret_cast<void**>(1);
            r10_78 = rdx76;
            if (*reinterpret_cast<signed char*>(&r9_77)) 
                goto addr_4259_76;
        }
        rax79 = fun_2440();
        *reinterpret_cast<int32_t*>(&rsi9) = 0;
        *reinterpret_cast<int32_t*>(&rsi9 + 4) = 0;
        rdx30 = rax79;
        fun_2600();
        rsp12 = rsp12 - 8 + 8 - 8 + 8;
        *reinterpret_cast<void***>(rbx52) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(&r10_78) = rcx15;
        ebx13 = 92;
        addr_4280_78:
        rcx15 = *reinterpret_cast<void***>(&r10_78);
        *reinterpret_cast<int32_t*>(&rcx15 + 4) = 0;
        continue;
        addr_4259_76:
        *reinterpret_cast<uint32_t*>(&rdx30) = static_cast<uint32_t>(r9_77 - 48);
        *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
        ebx13 = *reinterpret_cast<uint32_t*>(&r9_77);
        if (*reinterpret_cast<unsigned char*>(&rdx30) > 70) 
            goto addr_4280_78; else 
            goto addr_4265_26;
    }
    addr_49c9_61:
    r9d80 = *reinterpret_cast<uint32_t*>(&r14_14);
    r14_14 = r15_16;
    if (*reinterpret_cast<signed char*>(&r9d80) != 58) {
        goto addr_49e8_23;
    }
}

int64_t fun_23a0(void** rdi, ...);

void get_spec_stats(void** rdi) {
    void** rax2;
    void** rbp3;
    int64_t rax4;

    *reinterpret_cast<signed char*>(rdi + 48) = reinterpret_cast<signed char>(0);
    rax2 = *reinterpret_cast<void***>(rdi);
    rbp3 = *reinterpret_cast<void***>(rax2 + 8);
    *reinterpret_cast<uint64_t*>(rdi + 32) = 0;
    *reinterpret_cast<signed char*>(rdi + 50) = 0;
    if (!rbp3) {
        *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
        return;
    }
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3)) <= reinterpret_cast<unsigned char>(4)) 
        goto addr_39da_5;
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    fun_23a0(rdi);
    addr_39da_5:
    *reinterpret_cast<void***>(&rax4) = *reinterpret_cast<void***>(rbp3);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x92a8 + rax4 * 4) + 0x92a8;
}

int32_t get_next(void** rdi, void** rsi, ...) {
    unsigned char rax3;
    void** r14_4;
    int64_t rax5;

    rax3 = *reinterpret_cast<unsigned char*>(rdi + 16);
    if (rsi) {
        *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(2);
    }
    if (rax3 == 0xfffffffffffffffe) {
        r14_4 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi) + 8);
        *reinterpret_cast<unsigned char*>(rdi + 16) = reinterpret_cast<unsigned char>(0xffffffffffffffff);
        *reinterpret_cast<void***>(rdi + 8) = r14_4;
    } else {
        r14_4 = *reinterpret_cast<void***>(rdi + 8);
    }
    if (!r14_4) {
        return -1;
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)) > reinterpret_cast<unsigned char>(4)) {
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
            fun_23a0(rdi);
        } else {
            *reinterpret_cast<void***>(&rax5) = *reinterpret_cast<void***>(r14_4);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x92bc + rax5 * 4) + 0x92bc;
        }
    }
}

signed char is_char_class_member(void** rdi, uint32_t esi) {
    int64_t rdi3;

    if (*reinterpret_cast<uint32_t*>(&rdi) > 11) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rdi3) = *reinterpret_cast<uint32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi3) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x9278 + rdi3 * 4) + 0x9278;
    }
}

signed char star_digits_closebracket(struct s0* rdi, void** rsi, ...) {
    void* rcx3;
    uint32_t r9d4;
    void* r8_5;
    void** rdi6;
    void** rsi7;
    uint32_t eax8;
    uint32_t edx9;
    uint32_t eax10;

    rcx3 = rdi->f0;
    r9d4 = 0;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx3) + reinterpret_cast<unsigned char>(rsi)) == 42) {
        r8_5 = rdi->f8;
        r9d4 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_5) + reinterpret_cast<unsigned char>(rsi));
        if (*reinterpret_cast<signed char*>(&r9d4)) {
            addr_35c0_3:
            return 0;
        } else {
            rdi6 = rdi->f10;
            rsi7 = rsi + 1;
            if (reinterpret_cast<unsigned char>(rsi7) < reinterpret_cast<unsigned char>(rdi6)) {
                do {
                    eax8 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rcx3) + reinterpret_cast<unsigned char>(rsi7));
                    edx9 = eax8;
                    if (eax8 - 48 > 9) 
                        break;
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r8_5) + reinterpret_cast<unsigned char>(rsi7))) 
                        goto addr_356c_7;
                    ++rsi7;
                } while (reinterpret_cast<unsigned char>(rsi7) < reinterpret_cast<unsigned char>(rdi6));
                goto addr_35c0_3;
            } else {
                goto addr_356c_7;
            }
        }
    } else {
        addr_356c_7:
        eax10 = r9d4;
        return *reinterpret_cast<signed char*>(&eax10);
    }
    if (*reinterpret_cast<signed char*>(&edx9) == 93) {
        r9d4 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_5) + reinterpret_cast<unsigned char>(rsi7))) ^ 1;
        goto addr_356c_7;
    }
}

void** xnmalloc();

void** fun_2430();

void*** fun_26a0(void** rdi, ...);

void fun_26b0(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

void** make_printable_str(void** rdi, void** rsi, ...) {
    void** r14_3;
    void** rdi4;
    int64_t rax5;
    int64_t v6;
    void** rax7;
    void*** rsp8;
    void** v9;
    int64_t rax10;
    void** rbx11;
    void** r15_12;
    uint32_t r8d13;
    void** rax14;
    void*** rax15;
    int64_t rax16;
    uint32_t r8d17;
    int64_t rdi18;
    int64_t r8_19;

    r14_3 = rdi;
    rdi4 = rsi + 1;
    rax5 = g28;
    v6 = rax5;
    rax7 = xnmalloc();
    rsp8 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8);
    v9 = rax7;
    if (!rsi) {
        addr_36f4_2:
        rax10 = v6 - g28;
        if (!rax10) {
            return v9;
        }
    } else {
        rbx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(r14_3));
        r15_12 = rax7;
        while (1) {
            r8d13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3));
            if (*reinterpret_cast<signed char*>(&r8d13) > reinterpret_cast<signed char>(13)) {
                if (*reinterpret_cast<unsigned char*>(&r8d13) == 92) {
                    addr_36e0_7:
                    rdi4 = r15_12;
                    ++r14_3;
                    rax14 = fun_2430();
                    rsp8 = rsp8 - 8 + 8;
                    r15_12 = rax14;
                    if (rbx11 == r14_3) 
                        goto addr_36f4_2;
                } else {
                    addr_377d_8:
                    rax15 = fun_26a0(rdi4, rdi4);
                    rsp8 = rsp8 - 8 + 8;
                    *reinterpret_cast<uint32_t*>(&rax16) = *reinterpret_cast<unsigned char*>(&r8d13);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(*rax15) + reinterpret_cast<uint64_t>(rax16 * 2) + 1) & 64)) {
                        fun_26b0(rsp8 + 19, 1, 5, "\\%03o", rax16);
                        rsp8 = rsp8 - 8 + 8;
                        goto addr_36e0_7;
                    } else {
                        goto addr_36e0_7;
                    }
                }
            } else {
                if (*reinterpret_cast<signed char*>(&r8d13) <= reinterpret_cast<signed char>(6)) 
                    goto addr_377d_8;
                r8d17 = r8d13 - 8;
                if (*reinterpret_cast<unsigned char*>(&r8d17) > 5) 
                    goto addr_36e0_7; else 
                    goto addr_36c5_13;
            }
        }
    }
    fun_2470();
    if (*reinterpret_cast<uint32_t*>(&rdi4) <= 11) 
        goto addr_37fa_16;
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    fun_23a0(rdi4);
    addr_37fa_16:
    *reinterpret_cast<uint32_t*>(&rdi18) = *reinterpret_cast<uint32_t*>(&rdi4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9278 + rdi18 * 4) + 0x9278;
    addr_36c5_13:
    *reinterpret_cast<uint32_t*>(&r8_19) = *reinterpret_cast<unsigned char*>(&r8d17);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_19) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9260 + r8_19 * 4) + 0x9260;
}

int64_t fun_2450();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2450();
    if (r8d > 10) {
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
        fun_23a0(rdi, rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xa580 + rax11 * 4) + 0xa580;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

int32_t* fun_23b0();

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_24e0();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    int64_t rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    int64_t rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x654f;
    rax8 = fun_23b0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xe070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7981]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x65db;
            fun_24e0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x10400) {
                fun_2390(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x666a);
        }
        *rax8 = v10;
        rax29 = rax6 - g28;
        if (rax29) {
            fun_2470();
        } else {
            return r14_19;
        }
    }
}

void set_initialize(void** rdi, uint32_t esi, void** rdx, ...) {
    uint32_t r12d4;
    void** rbp5;
    void** rbx6;
    int32_t eax7;
    void** rdx8;
    void** rax9;

    r12d4 = esi;
    rbp5 = rdi;
    rbx6 = rdx;
    *reinterpret_cast<unsigned char*>(rdi + 16) = reinterpret_cast<unsigned char>(0xfffffffffffffffe);
    while (eax7 = get_next(rbp5, 0), eax7 != -1) {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(static_cast<int64_t>(eax7))) = 1;
    }
    if (*reinterpret_cast<signed char*>(&r12d4)) {
        rdx8 = rbx6;
        rax9 = rbx6 + 0x100;
        do {
            *reinterpret_cast<void***>(rdx8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx8)) ^ 1);
            ++rdx8;
        } while (rdx8 != rax9);
    }
    return;
}

struct s3 {
    signed char[58335] pad58335;
    signed char fe3df;
    unsigned char fe3e0;
};

void** stdout = reinterpret_cast<void**>(0);

void** fun_25c0(unsigned char* rdi, int64_t rsi, void** rdx, void** rcx);

/* squeeze_filter.constprop.0 */
void squeeze_filter_constprop_0(void** rdi, ...) {
    int32_t r15d2;
    void** r12_3;
    struct s3* rbp4;
    struct s3* rbx5;
    void** rdx6;
    void** rcx7;
    struct s3* rax8;
    struct s3* rdi9;
    struct s3* r9_10;
    void** r15_11;
    int64_t rax12;
    void** rax13;
    int64_t rax14;
    void** rax15;
    void** rax16;
    void** rax17;

    r15d2 = 0x7fffffff;
    r12_3 = rdi;
    *reinterpret_cast<int32_t*>(&rbp4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx5) + 4) = 0;
    goto addr_3eb9_2;
    addr_4030_3:
    return;
    addr_3ebe_4:
    while (rax8 = reinterpret_cast<struct s3*>(r12_3(0xe3e0, 0x2000, rdx6, rcx7)), rbx5 = rax8, !!rax8) {
        if (r15d2 == 0x7fffffff) {
            *reinterpret_cast<int32_t*>(&rdi9) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rbp4) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp4) + 4) = 0;
            goto addr_3f24_7;
        } else {
            *reinterpret_cast<int32_t*>(&rbp4) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp4) + 4) = 0;
            goto addr_3ee0_9;
        }
        while (1) {
            addr_3fc0_10:
            r9_10 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rbp4) + 2);
            if (reinterpret_cast<uint64_t>(r9_10) < reinterpret_cast<uint64_t>(rbx5)) 
                goto addr_3fc9_11;
            if (r9_10 != rbx5) {
                r15_11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx5) - reinterpret_cast<uint64_t>(rdi9));
            } else {
                rbp4 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rbp4) + 1);
                *reinterpret_cast<uint32_t*>(&rax12) = rbp4->fe3e0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                if (*reinterpret_cast<signed char*>(0xe2e0 + rax12)) {
                    if (reinterpret_cast<uint64_t>(rbp4) < reinterpret_cast<uint64_t>(rbx5)) 
                        goto addr_3f3e_16;
                    r9_10 = rbp4;
                }
                r15_11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx5) - reinterpret_cast<uint64_t>(rdi9));
                if (!r15_11) 
                    goto addr_4013_19;
            }
            while (rdx6 = r15_11, rcx7 = stdout, rax13 = fun_25c0(&rdi9->fe3e0, 1, rdx6, rcx7), r9_10 = r9_10, r15_11 == rax13) {
                addr_4013_19:
                r15d2 = 0x7fffffff;
                if (reinterpret_cast<uint64_t>(rbx5) <= reinterpret_cast<uint64_t>(r9_10)) 
                    goto addr_3ebe_4;
                rdi9 = r9_10;
                addr_3fc9_11:
                rbp4 = r9_10;
                while (1) {
                    addr_3f24_7:
                    *reinterpret_cast<uint32_t*>(&rax14) = rbp4->fe3e0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                    if (!*reinterpret_cast<signed char*>(0xe2e0 + rax14)) 
                        goto addr_3fc0_10;
                    if (reinterpret_cast<uint64_t>(rbx5) <= reinterpret_cast<uint64_t>(rbp4)) 
                        break;
                    addr_3f3e_16:
                    r15d2 = reinterpret_cast<signed char>(rbp4->fe3e0);
                    rax15 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp4) - reinterpret_cast<uint64_t>(rdi9));
                    rdx6 = rax15 + 1;
                    if (rbp4 && *reinterpret_cast<signed char*>(&r15d2) == rbp4->fe3df) {
                        rdx6 = rax15;
                    }
                    rbp4 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rbp4) + 1);
                    if (!rdx6) 
                        goto addr_3f02_26;
                    rcx7 = stdout;
                    rax16 = fun_25c0(&rdi9->fe3e0, 1, rdx6, rcx7);
                    rdx6 = rdx6;
                    if (rax16 != rdx6) 
                        goto addr_3f92_28;
                    do {
                        addr_3f02_26:
                        if (reinterpret_cast<uint64_t>(rbx5) <= reinterpret_cast<uint64_t>(rbp4)) {
                            addr_3eb9_2:
                            if (reinterpret_cast<uint64_t>(rbp4) < reinterpret_cast<uint64_t>(rbx5)) 
                                continue; else 
                                goto addr_3ebe_4;
                        } else {
                            do {
                                addr_3ee0_9:
                                if (static_cast<int32_t>(reinterpret_cast<signed char>(rbp4->fe3e0)) != r15d2) 
                                    break;
                                rbp4 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(rbp4) + 1);
                            } while (rbp4 != rbx5);
                            goto addr_3f19_31;
                        }
                        r15d2 = 0x7fffffff;
                        if (reinterpret_cast<uint64_t>(rbp4) >= reinterpret_cast<uint64_t>(rbx5)) 
                            goto addr_3ebe_4; else 
                            continue;
                        addr_3f19_31:
                        goto addr_3eb9_2;
                    } while (r15d2 != 0x7fffffff);
                    rdi9 = rbp4;
                }
                r9_10 = rbp4;
                r15_11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbx5) - reinterpret_cast<uint64_t>(rdi9));
            }
            addr_3f92_28:
            rax17 = fun_2440();
            r12_3 = rax17;
            fun_23b0();
            rdx6 = r12_3;
            *reinterpret_cast<int32_t*>(&rdi9) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
            fun_2600();
        }
    }
    goto addr_4030_3;
}

int64_t g906c = 0x6900726f72726520;

/* append_repeated_char.part.0 */
void append_repeated_char_part_0(void** rdi, ...) {
    int64_t v2;
    int64_t rax3;
    int32_t eax4;
    unsigned char* rdx5;

    v2 = rax3;
    fun_24d0("list->tail", "src/tr.c", 0x2d1, "append_repeated_char");
    g906c = -2;
    while (eax4 = get_next("list->tail", 0, "list->tail", 0), eax4 != -1) {
        *reinterpret_cast<signed char*>(0x2d1 + eax4) = 1;
    }
    if (!0) {
        rdx5 = reinterpret_cast<unsigned char*>(0x2d1);
        do {
            *rdx5 = reinterpret_cast<unsigned char>(*rdx5 ^ 1);
            ++rdx5;
        } while (!reinterpret_cast<int1_t>(rdx5 == 0x3d1));
    }
    goto v2;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xe080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

/* plain_read.part.0 */
void plain_read_part_0() {
    fun_2440();
    fun_23b0();
    fun_2600();
}

void** make_printable_char(uint32_t edi, void** rsi, void** rdx) {
    uint32_t ebx4;
    void** rax5;
    void*** rax6;
    int64_t rdx7;
    int64_t r8_8;

    ebx4 = edi;
    rax5 = xmalloc(5);
    rax6 = fun_26a0(5);
    *reinterpret_cast<uint32_t*>(&rdx7) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ebx4));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(*rax6) + reinterpret_cast<uint64_t>(rdx7 * 2) + 1) & 64)) {
        *reinterpret_cast<uint32_t*>(&r8_8) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&ebx4));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_8) + 4) = 0;
        fun_26b0(rax5, 1, 5, "\\%03o", r8_8);
        return rax5;
    } else {
        *reinterpret_cast<void***>(rax5) = *reinterpret_cast<void***>(&ebx4);
        *reinterpret_cast<void***>(rax5 + 1) = reinterpret_cast<void**>(0);
        return rax5;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, int64_t rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xa513);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xa50c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xa517);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xa508);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gde18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gde18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2373() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x2030;

void fun_2383() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t free = 0x2040;

void fun_2393() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_23a3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_23b3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23c3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23d3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23e3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_23f3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20b0;

void fun_2403() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_2413() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_2423() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t stpcpy = 0x20e0;

void fun_2433() {
    __asm__("cli ");
    goto stpcpy;
}

int64_t dcgettext = 0x20f0;

void fun_2443() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2453() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2463() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2473() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2483() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2493() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2150;

void fun_24a3() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x2160;

void fun_24b3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2170;

void fun_24c3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2180;

void fun_24d3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2190;

void fun_24e3() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x21a0;

void fun_24f3() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x21b0;

void fun_2503() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t read = 0x21c0;

void fun_2513() {
    __asm__("cli ");
    goto read;
}

int64_t memcmp = 0x21d0;

void fun_2523() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_2533() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2543() {
    __asm__("cli ");
    goto calloc;
}

int64_t fputc_unlocked = 0x2200;

void fun_2553() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2210;

void fun_2563() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2220;

void fun_2573() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2230;

void fun_2583() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2240;

void fun_2593() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2250;

void fun_25a3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2260;

void fun_25b3() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2270;

void fun_25c3() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2280;

void fun_25d3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2290;

void fun_25e3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22a0;

void fun_25f3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x22b0;

void fun_2603() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22c0;

void fun_2613() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x22d0;

void fun_2623() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x22e0;

void fun_2633() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x22f0;

void fun_2643() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2300;

void fun_2653() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2310;

void fun_2663() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2320;

void fun_2673() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2330;

void fun_2683() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_tolower_loc = 0x2340;

void fun_2693() {
    __asm__("cli ");
    goto __ctype_tolower_loc;
}

int64_t __ctype_b_loc = 0x2350;

void fun_26a3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x2360;

void fun_26b3() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_25e0();

void fun_2420(int64_t rdi, int64_t rsi);

void fun_2400(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2480(int64_t rdi, void** rsi);

uint32_t optind = 0;

unsigned char delete = 0;

unsigned char squeeze_repeats = 0;

signed char translating = 0;

unsigned char complement = 0;

signed char truncate_set1 = 0;

uint32_t usage();

void** stderr = reinterpret_cast<void**>(0);

void fun_2660(void** rdi, int64_t rsi, void** rdx, void** rcx, ...);

int64_t Version = 0xa4af;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

int32_t fun_2640();

int64_t stdin = 0;

void fadvise(int64_t rdi, void** rsi);

void** read_and_delete(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_24f0();

int32_t** fun_2380(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t** fun_2690(void** rdi, void** rsi);

void** read_and_xlate(int64_t rdi, void** rsi, void** rdx, void** rcx);

struct s5 {
    signed char[57568] pad57568;
    signed char fe0e0;
};

void fun_2723(int32_t edi, void** rsi) {
    void** r14_3;
    void** r13_4;
    int64_t rbp5;
    void** rbx6;
    void** rdi7;
    void** r12_8;
    void* rsp9;
    void** rcx10;
    void** rdx11;
    int64_t rdi12;
    int32_t eax13;
    uint32_t eax14;
    uint32_t esi15;
    void** rdi16;
    void** rax17;
    int1_t zf18;
    void** rdi19;
    void** rax20;
    void** rax21;
    void** rdi22;
    void** rdi23;
    int64_t rcx24;
    void** r12_25;
    void** rax26;
    int64_t rax27;
    void** rdi28;
    signed char al29;
    void* rsp30;
    void** rbp31;
    void** rax32;
    int64_t rax33;
    void** rdi34;
    signed char al35;
    void* rsp36;
    int1_t zf37;
    uint32_t r13d38;
    int32_t eax39;
    void* rax40;
    uint32_t edx41;
    uint32_t edx42;
    void** v43;
    void** r15_44;
    void** v45;
    void** v46;
    int1_t zf47;
    int1_t zf48;
    void** v49;
    void** v50;
    int1_t zf51;
    uint32_t eax52;
    int1_t zf53;
    void*** rax54;
    void* rsi55;
    void** r8_56;
    void** r9_57;
    uint32_t eax58;
    uint32_t ecx59;
    uint32_t r13d60;
    uint32_t v61;
    void** v62;
    void** v63;
    void** r15_64;
    void** rbp65;
    int64_t v66;
    void** v67;
    void** v68;
    int32_t eax69;
    int32_t eax70;
    int32_t v71;
    int32_t v72;
    void** rdi73;
    void** rcx74;
    int32_t v75;
    void** rdi76;
    void** rax77;
    int1_t zf78;
    int64_t rdi79;
    void** rsi80;
    void* rsp81;
    uint32_t eax82;
    int1_t zf83;
    signed char v84;
    int32_t eax85;
    void** rax86;
    int32_t eax87;
    uint32_t esi88;
    void** rax89;
    void** rax90;
    int32_t eax91;
    void** rdi92;
    void*** rax93;
    void* r13_94;
    int32_t** rax95;
    int32_t eax96;
    unsigned char rax97;
    int32_t eax98;
    int32_t eax99;
    uint32_t v100;
    int32_t v101;
    int32_t v102;
    int32_t v103;
    void*** rax104;
    void* r13_105;
    int32_t** rax106;
    int32_t eax107;
    int1_t zf108;
    void* rax109;
    int32_t eax110;
    int1_t zf111;
    void** rax112;
    void** rax113;
    uint32_t esi114;
    int1_t zf115;
    struct s5* rax116;
    int1_t zf117;
    int1_t zf118;
    void** rax119;
    int64_t rax120;
    void** rdi121;
    signed char al122;

    __asm__("cli ");
    r14_3 = reinterpret_cast<void**>("C");
    r13_4 = rsi;
    rbp5 = edi;
    rbx6 = reinterpret_cast<void**>("+AcCdst");
    rdi7 = *reinterpret_cast<void***>(rsi);
    set_program_name(rdi7);
    fun_25e0();
    fun_2420("coreutils", "/usr/local/share/locale");
    r12_8 = reinterpret_cast<void**>(0xda80);
    fun_2400("coreutils", "/usr/local/share/locale");
    atexit(0x4e80, "/usr/local/share/locale");
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1f8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    while (1) {
        rcx10 = r12_8;
        rdx11 = rbx6;
        *reinterpret_cast<int32_t*>(&rdi12) = *reinterpret_cast<int32_t*>(&rbp5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0;
        eax13 = fun_2480(rdi12, r13_4);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        if (eax13 == -1) {
            eax14 = optind;
            *reinterpret_cast<uint32_t*>(&rdx11) = delete;
            *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx10) = squeeze_repeats;
            *reinterpret_cast<int32_t*>(&rcx10 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rbx6) = *reinterpret_cast<int32_t*>(&rbp5) - eax14;
            *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rbx6) != 2) {
                addr_2961_4:
                translating = 0;
                if (*reinterpret_cast<unsigned char*>(&rdx11) == *reinterpret_cast<unsigned char*>(&rcx10)) {
                    esi15 = 2;
                    *reinterpret_cast<uint32_t*>(&rdx11) = 2;
                    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
                } else {
                    addr_2970_6:
                    esi15 = 1;
                    *reinterpret_cast<uint32_t*>(&rdx11) = *reinterpret_cast<uint32_t*>(&rdx11) - (*reinterpret_cast<uint32_t*>(&rdx11) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rdx11) < *reinterpret_cast<uint32_t*>(&rdx11) + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rcx10) < *reinterpret_cast<unsigned char*>(&rdx11)))) + 2;
                    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
                }
            } else {
                if (*reinterpret_cast<unsigned char*>(&rdx11)) {
                    translating = 0;
                    if (!*reinterpret_cast<unsigned char*>(&rcx10)) 
                        goto addr_2970_6; else 
                        goto addr_2a1d_9;
                } else {
                    translating = 1;
                    *reinterpret_cast<uint32_t*>(&rdx11) = 2;
                    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
                    if (!*reinterpret_cast<unsigned char*>(&rcx10)) 
                        goto addr_2a1d_9; else 
                        goto addr_28d2_11;
                }
            }
        } else {
            if (eax13 == 99) {
                addr_2829_13:
                complement = 1;
                continue;
            } else {
                if (eax13 > 99) {
                    if (eax13 == 0x73) {
                        addr_294e_16:
                        squeeze_repeats = 1;
                        continue;
                    } else {
                        if (eax13 == 0x74) {
                            truncate_set1 = 1;
                            continue;
                        }
                    }
                } else {
                    if (eax13 == 0xffffff7e) {
                        eax14 = usage();
                        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                        goto addr_2961_4;
                    }
                    if (eax13 < 0xffffff7f) 
                        goto addr_2838_22; else 
                        goto addr_27db_23;
                }
            }
        }
        if (*reinterpret_cast<int32_t*>(&rbx6) < reinterpret_cast<int32_t>(esi15)) {
            if (*reinterpret_cast<uint32_t*>(&rbx6)) {
                rdi16 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r13_4 + rbp5 * 8) - 8);
                rax17 = quote(rdi16);
                r12_8 = rax17;
                fun_2440();
                fun_2600();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8 - 8 + 8);
                zf18 = squeeze_repeats == 0;
                if (zf18) {
                }
                goto addr_2920_29;
            } else {
                fun_2440();
                fun_2600();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
                goto addr_2944_31;
            }
        }
        addr_28d2_11:
        if (*reinterpret_cast<int32_t*>(&rbx6) <= *reinterpret_cast<int32_t*>(&rdx11)) 
            break;
        rdi19 = *reinterpret_cast<void***>(r13_4 + (eax14 + *reinterpret_cast<uint32_t*>(&rdx11)) * 8);
        rax20 = quote(rdi19, rdi19);
        r12_8 = rax20;
        fun_2440();
        fun_2600();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8 - 8 + 8);
        if (*reinterpret_cast<uint32_t*>(&rbx6) != 2) {
            addr_2944_31:
            usage();
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            goto addr_294e_16;
        } else {
            addr_2920_29:
            rax21 = fun_2440();
            rdi22 = stderr;
            fun_2660(rdi22, 1, "%s\n", rax21);
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
            goto addr_2944_31;
        }
        addr_2880_33:
        if (eax13 == 100) {
            delete = 1;
            continue;
        }
        addr_2838_22:
        if (eax13 != 0xffffff7d) 
            goto addr_2944_31;
        rdi23 = stdout;
        rcx24 = Version;
        version_etc(rdi23, "tr", "GNU coreutils", rcx24, "Jim Meyering");
        eax13 = fun_2640();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
        goto addr_2880_33;
        addr_27db_23:
        if (eax13 == 65) {
            fun_25e0();
            fun_25e0();
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
            continue;
        }
        if (eax13 != 67) 
            goto addr_2944_31; else 
            goto addr_2829_13;
    }
    while (1) {
        r12_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 96);
        rax26 = xmalloc(32, 32);
        *reinterpret_cast<void***>(rax26 + 8) = reinterpret_cast<void**>(0);
        rax27 = reinterpret_cast<int32_t>(optind);
        rdi28 = *reinterpret_cast<void***>(r13_4 + rax27 * 8);
        al29 = parse_str(rdi28, r12_25);
        rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
        if (al29) {
            *reinterpret_cast<int32_t*>(&rbp31) = 0;
            *reinterpret_cast<int32_t*>(&rbp31 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rbx6) != 2) 
                break;
            addr_2d04_40:
            rbp31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp30) + 0xa0);
            rax32 = xmalloc(32, 32);
            *reinterpret_cast<void***>(rax32 + 8) = reinterpret_cast<void**>(0);
            rax33 = reinterpret_cast<int32_t>(optind);
            rdi34 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r13_4 + rax33 * 8) + 8);
            al35 = parse_str(rdi34, rbp31);
            rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8 - 8 + 8);
            if (al35) 
                break;
        }
        addr_2a5a_42:
        fun_2640();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
    }
    get_spec_stats(r12_25);
    rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
    zf37 = complement == 0;
    if (!zf37) {
        *reinterpret_cast<uint32_t*>(&rcx10) = 32;
        *reinterpret_cast<int32_t*>(&rcx10 + 4) = 0;
        while (rcx10) {
            --rcx10;
        }
        r13d38 = 0x100;
        while (eax39 = get_next(r12_25, 0), rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8), eax39 != -1) {
            rax40 = reinterpret_cast<void*>(static_cast<int64_t>(eax39));
            edx41 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsp36) + reinterpret_cast<int64_t>(rax40) + 0xe0);
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp36) + reinterpret_cast<int64_t>(rax40) + 0xe0) = 1;
            edx42 = edx41 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx11) = *reinterpret_cast<unsigned char*>(&edx42);
            *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
            r13d38 = r13d38 - *reinterpret_cast<uint32_t*>(&rdx11);
        }
        r13_4 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(r13d38)));
        v43 = r13_4;
    }
    r15_44 = v45;
    if (r15_44) {
        addr_3423_52:
        fun_2440();
        fun_2600();
        goto addr_3447_53;
    } else {
        if (!rbp31) 
            goto addr_2b1d_55;
        r14_3 = v43;
        v46 = r14_3;
        get_spec_stats(rbp31);
        rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
        if (reinterpret_cast<unsigned char>(r14_3) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp31 + 24))) 
            goto addr_2af9_57;
        if (*reinterpret_cast<uint64_t*>(rbp31 + 32) == 1) 
            goto addr_31cd_59;
    }
    addr_2af9_57:
    if (*reinterpret_cast<uint64_t*>(rbp31 + 32) > 1) {
        addr_3447_53:
        fun_2440();
        fun_2600();
    } else {
        zf47 = translating == 0;
        if (!zf47) {
            addr_2da9_61:
            if (*reinterpret_cast<signed char*>(rbp31 + 48)) {
                addr_3314_62:
                fun_2440();
                fun_2600();
            } else {
                if (*reinterpret_cast<signed char*>(rbp31 + 50)) {
                    addr_32f0_64:
                    fun_2440();
                    fun_2600();
                    goto addr_3314_62;
                } else {
                    zf48 = complement == 0;
                    v49 = *reinterpret_cast<void***>(rbp31 + 24);
                    v50 = *reinterpret_cast<void***>(rbp31 + 8);
                    if (!zf48) {
                        rcx10 = v49;
                        if (reinterpret_cast<unsigned char>(v46) <= reinterpret_cast<unsigned char>(rcx10)) 
                            goto addr_3032_67;
                        zf51 = truncate_set1 == 0;
                        if (!zf51) 
                            goto addr_3032_67;
                        goto addr_2fb7_70;
                    } else {
                        eax52 = *reinterpret_cast<unsigned char*>(rbp31 + 49);
                        *reinterpret_cast<uint32_t*>(&r14_3) = eax52;
                        *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
                        if (!*reinterpret_cast<signed char*>(&eax52)) {
                            addr_2f95_72:
                            v46 = v43;
                            if (reinterpret_cast<unsigned char>(v43) <= reinterpret_cast<unsigned char>(v49)) 
                                goto addr_2b1d_55;
                            zf53 = truncate_set1 == 0;
                            if (!zf53) 
                                goto addr_2b1d_55; else 
                                goto addr_2fb7_70;
                        } else {
                            rax54 = fun_26a0(rbp31);
                            rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
                            *reinterpret_cast<uint32_t*>(&rdx11) = 0;
                            *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rsi55) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi55) + 4) = 0;
                            *reinterpret_cast<int32_t*>(&r8_56) = 0;
                            *reinterpret_cast<int32_t*>(&r8_56 + 4) = 0;
                            r9_57 = *rax54;
                            do {
                                eax58 = reinterpret_cast<uint16_t>(*reinterpret_cast<void***>(r9_57 + reinterpret_cast<unsigned char>(rdx11) * 2));
                                ecx59 = eax58;
                                r8_56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_56) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r8_56) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&ecx59) & 0x100) < 1)))))));
                                rsi55 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi55) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint64_t>(rsi55) < 1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint16_t>(*reinterpret_cast<uint16_t*>(&eax58) & 0x200) < 1))))));
                                ++rdx11;
                            } while (!reinterpret_cast<int1_t>(rdx11 == 0x100));
                            rcx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsi55) + 0xffffffffffffffff);
                            r13d60 = *reinterpret_cast<uint32_t*>(&r14_3);
                            v61 = *reinterpret_cast<uint32_t*>(&rbx6);
                            *reinterpret_cast<unsigned char*>(rbp31 + 16) = reinterpret_cast<unsigned char>(0xfffffffffffffffe);
                            *reinterpret_cast<unsigned char*>(&v62) = *reinterpret_cast<unsigned char*>(&r14_3);
                            r14_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp36) + 88);
                            v63 = r15_44;
                            r15_64 = rbp31;
                            rbp65 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp36) + 92);
                            v66 = -2;
                            v67 = rcx10;
                            v68 = r8_56;
                            while (1) {
                                eax69 = get_next(r12_25, r14_3);
                                eax70 = get_next(r15_64, rbp65);
                                rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8 - 8 + 8);
                                if (*reinterpret_cast<unsigned char*>(&r13d60)) {
                                    if (v71 == 2) {
                                        addr_2f00_79:
                                        *reinterpret_cast<unsigned char*>(&v62) = reinterpret_cast<uint1_t>(v66 == -1);
                                        *reinterpret_cast<unsigned char*>(&r13d60) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(r15_64 + 16) == 0xffffffffffffffff);
                                        if (!(eax69 + 1)) 
                                            goto addr_2f4e_80;
                                        if (!(eax70 + 1)) 
                                            goto addr_2f4e_80; else 
                                            continue;
                                    } else {
                                        if (!*reinterpret_cast<unsigned char*>(&v62)) 
                                            goto addr_32cc_83;
                                        if (v72 == 2) 
                                            goto addr_32cc_83;
                                    }
                                } else {
                                    if (v71 == 2) {
                                        goto addr_2f00_79;
                                    }
                                }
                                v66 = -1;
                                rdi73 = v67;
                                rcx74 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r15_64 + 8) + 8);
                                *reinterpret_cast<unsigned char*>(r15_64 + 16) = reinterpret_cast<unsigned char>(0xffffffffffffffff);
                                *reinterpret_cast<void***>(r15_64 + 8) = rcx74;
                                if (v75 == 1) {
                                    rdx11 = v68;
                                    rdi73 = rdx11 + 0xffffffffffffffff;
                                }
                                rdi76 = v67;
                                v43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v43) - reinterpret_cast<unsigned char>(rdi73));
                                if (!(v71 - 1)) {
                                    rdx11 = v68;
                                    rdi76 = rdx11 + 0xffffffffffffffff;
                                }
                                rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_64 + 24)) - reinterpret_cast<unsigned char>(rdi76));
                                *reinterpret_cast<void***>(r15_64 + 24) = rcx10;
                                goto addr_2f00_79;
                            }
                        }
                    }
                }
            }
        } else {
            if (*reinterpret_cast<uint64_t*>(rbp31 + 32)) 
                goto addr_31f1_93; else 
                goto addr_2b1d_55;
        }
    }
    addr_3338_94:
    fun_2440();
    fun_23b0();
    fun_2600();
    addr_3364_95:
    fun_2440();
    fun_23b0();
    fun_2600();
    goto addr_3390_96;
    addr_2fb7_70:
    if (!v49) {
        addr_33db_97:
        fun_2440();
        fun_2600();
        goto addr_33ff_98;
    } else {
        if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp31 + 8)) == 1) {
            *reinterpret_cast<uint32_t*>(&r14_3) = *reinterpret_cast<unsigned char*>(*reinterpret_cast<void***>(rbp31 + 8) + 17);
            *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
        } else {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp31 + 8))) > reinterpret_cast<unsigned char>(1)) {
                if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp31 + 8)) == 2) {
                    addr_33ff_98:
                    fun_2440();
                    fun_2600();
                    goto addr_3423_52;
                } else {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp31 + 8)) != 4) {
                        goto 0x26cf;
                    }
                }
            } else {
                *reinterpret_cast<uint32_t*>(&r14_3) = *reinterpret_cast<unsigned char*>(*reinterpret_cast<void***>(rbp31 + 8) + 16);
                *reinterpret_cast<int32_t*>(&r14_3 + 4) = 0;
            }
        }
    }
    r13_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v46) - reinterpret_cast<unsigned char>(v49));
    rax77 = xmalloc(32);
    rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
    rdx11 = *reinterpret_cast<void***>(rbp31 + 8);
    *reinterpret_cast<void***>(rax77 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rax77) = reinterpret_cast<void**>(4);
    *reinterpret_cast<unsigned char*>(rax77 + 16) = *reinterpret_cast<unsigned char*>(&r14_3);
    *reinterpret_cast<void***>(rax77 + 24) = r13_4;
    if (!rdx11) {
        append_repeated_char_part_0(32);
    } else {
        *reinterpret_cast<void***>(rdx11 + 8) = rax77;
        zf78 = complement == 0;
        *reinterpret_cast<void***>(rbp31 + 8) = rax77;
        *reinterpret_cast<void***>(rbp31 + 24) = v46;
        if (zf78) {
            addr_2b1d_55:
            rdi79 = stdin;
            *reinterpret_cast<uint32_t*>(&rsi80) = 2;
            *reinterpret_cast<int32_t*>(&rsi80 + 4) = 0;
            fadvise(rdi79, 2);
            rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
            eax82 = squeeze_repeats;
            *reinterpret_cast<signed char*>(&v67) = *reinterpret_cast<signed char*>(&eax82);
            if (*reinterpret_cast<uint32_t*>(&rbx6) != 1 || !*reinterpret_cast<signed char*>(&eax82)) {
                zf83 = delete == 0;
                if (zf83) 
                    goto addr_2b63_110;
                *reinterpret_cast<uint32_t*>(&rbx6) = *reinterpret_cast<uint32_t*>(&rbx6) - 1;
                *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
                if (!*reinterpret_cast<uint32_t*>(&rbx6)) 
                    goto addr_3215_112; else 
                    goto addr_2b58_113;
            } else {
                *reinterpret_cast<uint32_t*>(&rsi80) = complement;
                *reinterpret_cast<int32_t*>(&rsi80 + 4) = 0;
                rdx11 = reinterpret_cast<void**>(0xe2e0);
                set_initialize(r12_25, *reinterpret_cast<uint32_t*>(&rsi80), 0xe2e0);
                squeeze_filter_constprop_0(0x3dd0, 0x3dd0);
                goto addr_2cb9_115;
            }
        } else {
            addr_3032_67:
            if (v84) {
                if (*reinterpret_cast<void***>(rbp31 + 24) != v46 || (*reinterpret_cast<unsigned char*>(rbp31 + 16) = reinterpret_cast<unsigned char>(0xfffffffffffffffe), eax85 = get_next(rbp31, 0), rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8), *reinterpret_cast<int32_t*>(&r13_4) = eax85, eax85 == -1)) {
                    addr_307d_117:
                    rax86 = fun_2440();
                    rdx11 = rax86;
                    fun_2600();
                    goto addr_30a1_118;
                } else {
                    do {
                        eax87 = get_next(rbp31, 0);
                        rsp36 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp36) - 8 + 8);
                        if (eax87 == -1) 
                            goto addr_2b1d_55;
                    } while (*reinterpret_cast<int32_t*>(&r13_4) == eax87);
                    goto addr_307d_117;
                }
            }
        }
    }
    addr_32cc_83:
    fun_2440();
    fun_2600();
    goto addr_32f0_64;
    addr_3215_112:
    esi88 = complement;
    rdx11 = reinterpret_cast<void**>(0xe1e0);
    set_initialize(r12_25, esi88, 0xe1e0);
    do {
        *reinterpret_cast<uint32_t*>(&rsi80) = 0x2000;
        *reinterpret_cast<int32_t*>(&rsi80 + 4) = 0;
        rax89 = read_and_delete(0xe3e0, 0x2000, rdx11, rcx10);
        rbx6 = rax89;
        if (!rax89) 
            break;
        rcx10 = stdout;
        rdx11 = rax89;
        rax90 = fun_25c0(0xe3e0, 1, rdx11, rcx10);
    } while (rbx6 == rax90);
    goto addr_3364_95;
    addr_2cb9_115:
    while (eax91 = fun_24f0(), !eax91) {
        *reinterpret_cast<int32_t*>(&rdi92) = 0;
        *reinterpret_cast<int32_t*>(&rdi92 + 4) = 0;
        fun_2640();
        while (1) {
            addr_2ccf_125:
            rax93 = fun_26a0(rdi92, rdi92);
            *reinterpret_cast<int32_t*>(&r13_94) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_94) + 4) = 0;
            r12_25 = *rax93;
            do {
                if (*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(r12_25 + reinterpret_cast<uint64_t>(r13_94) * 2) + 1) & 2) {
                    rax95 = fun_2380(rdi92, rsi80, rdx11, rcx10);
                    eax96 = (*rax95)[static_cast<uint64_t>(r13_94)];
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(r13_94)) = *reinterpret_cast<signed char*>(&eax96);
                }
                r13_94 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_94) + 1);
            } while (!reinterpret_cast<int1_t>(r13_94 == 0x100));
            while (1) {
                *reinterpret_cast<void***>(rbp31 + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rbp31 + 8) + 8);
                rax97 = reinterpret_cast<unsigned char>(0xffffffffffffffff);
                addr_2bb7_131:
                *reinterpret_cast<unsigned char*>(rbp31 + 16) = rax97;
                do {
                    eax98 = get_next(r15_44, r14_3, r15_44, r14_3);
                    rsi80 = v62;
                    rdi92 = rbp31;
                    r13_4 = reinterpret_cast<void**>(static_cast<int64_t>(eax98));
                    eax99 = get_next(rdi92, rsi80, rdi92, rsi80);
                    *reinterpret_cast<uint32_t*>(&rdx11) = v100;
                    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
                    if (!*reinterpret_cast<uint32_t*>(&rdx11)) {
                        if (v101 == 1) 
                            goto addr_2ccf_125;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rdx11) = *reinterpret_cast<uint32_t*>(&rdx11) - 1;
                        *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
                        if (*reinterpret_cast<uint32_t*>(&rdx11)) 
                            goto addr_2bc8_135;
                        if (!v102) 
                            break;
                    }
                    addr_2bc8_135:
                    if (*reinterpret_cast<int32_t*>(&r13_4) == -1) 
                        goto addr_2c71_137;
                    if (eax99 == -1) 
                        goto addr_30a1_118;
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<unsigned char>(r13_4)) = *reinterpret_cast<signed char*>(&eax99);
                } while (v103 == 2);
                continue;
                rax104 = fun_26a0(rdi92, rdi92);
                *reinterpret_cast<int32_t*>(&r13_105) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_105) + 4) = 0;
                r12_25 = *rax104;
                do {
                    if (*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(r12_25 + reinterpret_cast<uint64_t>(r13_105) * 2) + 1) & 1) {
                        rax106 = fun_2690(rdi92, rsi80);
                        eax107 = (*rax106)[static_cast<uint64_t>(r13_105)];
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(r13_105)) = *reinterpret_cast<signed char*>(&eax107);
                    }
                    r13_105 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r13_105) + 1);
                } while (!reinterpret_cast<int1_t>(r13_105 == 0x100));
            }
        }
        addr_30a1_118:
        *reinterpret_cast<int32_t*>(&r15_44) = *reinterpret_cast<int32_t*>(&r13_4) + 1;
        *reinterpret_cast<int32_t*>(&r15_44 + 4) = 0;
        if (!*reinterpret_cast<int32_t*>(&r15_44)) 
            goto addr_2c71_137;
        zf108 = truncate_set1 == 0;
        if (!zf108) 
            goto addr_2c71_137;
        rcx10 = reinterpret_cast<void**>("main");
        fun_24d0("c1 == -1 || truncate_set1", "src/tr.c", 0x765, "main");
        addr_30da_146:
        r14_3 = reinterpret_cast<void**>(0xe1e0);
        rdx11 = reinterpret_cast<void**>(0xe1e0);
        set_initialize(r12_25, 0, 0xe1e0, r12_25, 0, 0xe1e0);
        *reinterpret_cast<unsigned char*>(rbp31 + 16) = reinterpret_cast<unsigned char>(0xfffffffffffffffe);
        *reinterpret_cast<int32_t*>(&rax109) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax109) + 4) = 0;
        do {
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<uint64_t>(rax109)) = *reinterpret_cast<signed char*>(&rax109);
            rax109 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax109) + 1);
        } while (!reinterpret_cast<int1_t>(rax109 == 0x100));
        do {
            if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0xe1e0) + reinterpret_cast<unsigned char>(r15_44))) {
                eax110 = get_next(rbp31, 0, rbp31, 0);
                if (eax110 == -1) 
                    break;
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx6) + reinterpret_cast<unsigned char>(r15_44)) = *reinterpret_cast<signed char*>(&eax110);
            }
            ++r15_44;
        } while (r15_44 != 0x100);
        goto addr_2c71_137;
        zf111 = truncate_set1 == 0;
        if (!zf111) {
            addr_2c71_137:
            if (!*reinterpret_cast<signed char*>(&v67)) {
                do {
                    *reinterpret_cast<uint32_t*>(&rsi80) = 0x2000;
                    *reinterpret_cast<int32_t*>(&rsi80 + 4) = 0;
                    rax112 = read_and_xlate(0xe3e0, 0x2000, rdx11, rcx10);
                    rbx6 = rax112;
                    if (!rax112) 
                        goto addr_2cb9_115;
                    rcx10 = stdout;
                    rdx11 = rax112;
                    rax113 = fun_25c0(0xe3e0, 1, rdx11, rcx10);
                } while (rbx6 == rax113);
                goto addr_33af_157;
            }
        } else {
            rcx10 = reinterpret_cast<void**>("main");
            fun_24d0("ch != -1 || truncate_set1", "src/tr.c", 0x730, "main");
        }
        rdx11 = reinterpret_cast<void**>(0xe2e0);
        *reinterpret_cast<uint32_t*>(&rsi80) = 0;
        *reinterpret_cast<int32_t*>(&rsi80 + 4) = 0;
        set_initialize(rbp31, 0, 0xe2e0, rbp31, 0, 0xe2e0);
        squeeze_filter_constprop_0(read_and_xlate, read_and_xlate);
    }
    goto addr_3338_94;
    addr_33af_157:
    fun_2440();
    fun_23b0();
    fun_2600();
    goto addr_33db_97;
    addr_2b58_113:
    if (*reinterpret_cast<signed char*>(&v67)) {
        esi114 = complement;
        set_initialize(r12_25, esi114, 0xe1e0);
        rdx11 = reinterpret_cast<void**>(0xe2e0);
        *reinterpret_cast<uint32_t*>(&rsi80) = 0;
        *reinterpret_cast<int32_t*>(&rsi80 + 4) = 0;
        set_initialize(rbp31, 0, 0xe2e0);
        squeeze_filter_constprop_0(read_and_delete);
        goto addr_2cb9_115;
    } else {
        addr_2b63_110:
        zf115 = translating == 0;
        if (!zf115) {
            *reinterpret_cast<int32_t*>(&rax116) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax116) + 4) = 0;
            zf117 = complement == 0;
            rbx6 = reinterpret_cast<void**>(0xe0e0);
            if (!zf117) 
                goto addr_30da_146;
            do {
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0xe0e0) + reinterpret_cast<uint64_t>(rax116)) = *reinterpret_cast<signed char*>(&rax116);
                rax116 = reinterpret_cast<struct s5*>(reinterpret_cast<uint64_t>(rax116) + 1);
            } while (!reinterpret_cast<int1_t>(rax116 == 0x100));
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp81) + 92);
            rax97 = reinterpret_cast<unsigned char>(0xfffffffffffffffe);
            r14_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp81) + 88);
            r15_44 = r12_25;
            v62 = rcx10;
            goto addr_2bb7_131;
        }
    }
    addr_2f4e_80:
    rbp31 = r15_64;
    *reinterpret_cast<uint32_t*>(&rbx6) = v61;
    *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    r15_44 = v63;
    if (reinterpret_cast<unsigned char>(v46) < reinterpret_cast<unsigned char>(v43) || reinterpret_cast<unsigned char>(v49) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp31 + 24))) {
        addr_3390_96:
        fun_24d0("old_s1_len >= s1->length && old_s2_len >= s2->length", "src/tr.c", 0x4c8, "validate_case_classes");
        goto addr_33af_157;
    } else {
        *reinterpret_cast<void***>(rbp31 + 8) = v50;
        v49 = *reinterpret_cast<void***>(rbp31 + 24);
        goto addr_2f95_72;
    }
    addr_31f1_93:
    fun_2440();
    fun_2600();
    goto addr_3215_112;
    addr_31cd_59:
    rcx10 = v46;
    rdx11 = *reinterpret_cast<void***>(rbp31 + 40);
    zf118 = translating == 0;
    *reinterpret_cast<void***>(rdx11 + 24) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp31 + 24)));
    *reinterpret_cast<void***>(rbp31 + 24) = rcx10;
    if (!zf118) 
        goto addr_2da9_61; else 
        goto addr_31f1_93;
    addr_2a1d_9:
    r12_25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp9) + 96);
    rax119 = xmalloc(32, 32);
    *reinterpret_cast<void***>(rax119 + 8) = reinterpret_cast<void**>(0);
    rax120 = reinterpret_cast<int32_t>(optind);
    rdi121 = *reinterpret_cast<void***>(r13_4 + rax120 * 8);
    al122 = parse_str(rdi121, r12_25);
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
    if (al122) 
        goto addr_2d04_40; else 
        goto addr_2a5a_42;
}

int64_t __libc_start_main = 0;

void fun_3473() {
    __asm__("cli ");
    __libc_start_main(0x2720, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xe008;

void fun_2370(int64_t rdi);

int64_t fun_3513() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2370(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3553() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

uint64_t safe_read();

void fun_3dd3(int64_t rdi, int64_t rsi) {
    uint64_t rax3;

    __asm__("cli ");
    rax3 = safe_read();
    if (rax3 == 0xffffffffffffffff) {
        plain_read_part_0();
    } else {
        return;
    }
}

void fun_4083(unsigned char* rdi, int64_t rsi) {
    uint64_t rax3;
    unsigned char* rdi4;
    unsigned char* rsi5;
    int64_t rdx6;
    uint32_t edx7;

    __asm__("cli ");
    rax3 = safe_read();
    if (rax3 == 0xffffffffffffffff) {
        plain_read_part_0();
    } else {
        rdi4 = rdi;
        rsi5 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rdi) + rax3);
        if (rax3) {
            do {
                *reinterpret_cast<uint32_t*>(&rdx6) = *rdi4;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                ++rdi4;
                edx7 = *reinterpret_cast<unsigned char*>(0xe0e0 + rdx6);
                *reinterpret_cast<signed char*>(rdi4 - 1) = *reinterpret_cast<signed char*>(&edx7);
            } while (rsi5 != rdi4);
        }
        return;
    }
}

uint64_t fun_40d3(void* rdi, int64_t rsi) {
    void* rbp3;
    uint64_t rax4;
    uint64_t rsi5;
    int64_t rcx6;
    uint64_t rdx7;
    int64_t rdi8;

    __asm__("cli ");
    rbp3 = rdi;
    while (rax4 = safe_read(), rax4 != 0xffffffffffffffff) {
        if (!rax4) 
            goto addr_4146_4;
        *reinterpret_cast<int32_t*>(&rsi5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi5) + 4) = 0;
        while (*reinterpret_cast<uint32_t*>(&rcx6) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rbp3) + rsi5), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0, rdx7 = rsi5 + 1, *reinterpret_cast<signed char*>(0xe1e0 + rcx6) == 0) {
            if (rax4 == rdx7) 
                goto addr_415a_8;
            rsi5 = rdx7;
        }
        if (rax4 <= rdx7) {
            addr_413e_11:
            if (!rsi5) 
                continue; else 
                goto addr_4143_12;
        }
        do {
            addr_4120_14:
            *reinterpret_cast<uint32_t*>(&rdi8) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rbp3) + rdx7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
            if (!*reinterpret_cast<signed char*>(0xe1e0 + rdi8)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rbp3) + rsi5) = *reinterpret_cast<signed char*>(&rdi8);
                ++rsi5;
            }
            ++rdx7;
        } while (rdx7 < rax4);
        goto addr_413e_11;
        addr_415a_8:
        rdx7 = rsi5 + 2;
        if (rdx7 >= rax4) 
            goto addr_4146_4;
        rsi5 = rax4;
        goto addr_4120_14;
    }
    fun_2440();
    fun_23b0();
    fun_2600();
    addr_4146_4:
    return rax4;
    addr_4143_12:
    rax4 = rsi5;
    goto addr_4146_4;
}

void** program_name = reinterpret_cast<void**>(0);

void fun_25f0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_2530(void** rdi, void** rsi, int64_t rdx, void** rcx);

unsigned char __cxa_finalize;

unsigned char g1;

signed char g2;

void fun_4a83(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** rcx4;
    void** r12_5;
    void** rax6;
    void** r12_7;
    void** rax8;
    void** r12_9;
    void** rax10;
    void** r12_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r12_15;
    void** rax16;
    void** r12_17;
    void** rax18;
    uint32_t ecx19;
    uint32_t ecx20;
    int1_t zf21;
    void** rax22;
    void** rax23;
    int32_t eax24;
    void** rax25;
    void** r13_26;
    void** rax27;
    void** rax28;
    int32_t eax29;
    void** rax30;
    void** r14_31;
    void** rax32;
    void** rax33;
    void** rax34;
    void** rdi35;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_2440();
            fun_25f0(1, rax3, r12_2, rcx4);
            r12_5 = stdout;
            rax6 = fun_2440();
            fun_2530(rax6, r12_5, 5, rcx4);
            r12_7 = stdout;
            rax8 = fun_2440();
            fun_2530(rax8, r12_7, 5, rcx4);
            r12_9 = stdout;
            rax10 = fun_2440();
            fun_2530(rax10, r12_9, 5, rcx4);
            r12_11 = stdout;
            rax12 = fun_2440();
            fun_2530(rax12, r12_11, 5, rcx4);
            r12_13 = stdout;
            rax14 = fun_2440();
            fun_2530(rax14, r12_13, 5, rcx4);
            r12_15 = stdout;
            rax16 = fun_2440();
            fun_2530(rax16, r12_15, 5, rcx4);
            r12_17 = stdout;
            rax18 = fun_2440();
            fun_2530(rax18, r12_17, 5, rcx4);
            do {
                if (1) 
                    break;
                ecx19 = __cxa_finalize;
            } while (0x74 != ecx19 || ((ecx20 = g1, 0x72 != ecx20) || (zf21 = g2 == 0, !zf21)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax22 = fun_2440();
                fun_25f0(1, rax22, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax23 = fun_25e0();
                if (!rax23 || (eax24 = fun_23c0(rax23, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax24)) {
                    rax25 = fun_2440();
                    r12_2 = reinterpret_cast<void**>("tr");
                    r13_26 = reinterpret_cast<void**>(" invocation");
                    fun_25f0(1, rax25, "https://www.gnu.org/software/coreutils/", "tr");
                } else {
                    r12_2 = reinterpret_cast<void**>("tr");
                    goto addr_4e33_9;
                }
            } else {
                rax27 = fun_2440();
                fun_25f0(1, rax27, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax28 = fun_25e0();
                if (!rax28 || (eax29 = fun_23c0(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax29)) {
                    addr_4d36_11:
                    rax30 = fun_2440();
                    r13_26 = reinterpret_cast<void**>(" invocation");
                    fun_25f0(1, rax30, "https://www.gnu.org/software/coreutils/", "tr");
                    if (!reinterpret_cast<int1_t>(r12_2 == "tr")) {
                        r13_26 = reinterpret_cast<void**>(0xa941);
                    }
                } else {
                    addr_4e33_9:
                    r14_31 = stdout;
                    rax32 = fun_2440();
                    fun_2530(rax32, r14_31, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_4d36_11;
                }
            }
            rax33 = fun_2440();
            rcx4 = r13_26;
            fun_25f0(1, rax33, r12_2, rcx4);
            addr_4ad9_14:
            fun_2640();
        }
    } else {
        rax34 = fun_2440();
        rdi35 = stderr;
        rcx4 = r12_2;
        fun_2660(rdi35, 1, rax34, rcx4);
        goto addr_4ad9_14;
    }
}

int64_t file_name = 0;

void fun_4e63(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4e73(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_23d0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4e83() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23b0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2440();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4f13_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2600();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23d0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4f13_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2600();
    }
}

void fun_4f33() {
    __asm__("cli ");
}

struct s6 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2570(struct s6* rdi);

void fun_4f43(struct s6* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2570(rdi);
        goto 0x2500;
    }
}

void fun_2650(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s7 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s7* fun_24b0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4f73(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s7* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2650("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23a0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24b0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23c0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_6713(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23b0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0x10500;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_6753(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x10500);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_6773(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x10500);
    }
    *rdi = esi;
    return 0x10500;
}

int64_t fun_6793(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x10500);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s8 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_67d3(struct s8* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0x10500);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s9 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* fun_67f3(struct s9* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0x10500);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x26de;
    if (!rdx) 
        goto 0x26de;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x10500;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_6833(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s10* r8) {
    struct s10* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s10*>(0x10500);
    }
    rax7 = fun_23b0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x6866);
    *rax7 = r15d8;
    return rax13;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_68b3(int64_t rdi, int64_t rsi, void*** rdx, struct s11* rcx) {
    struct s11* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s11*>(0x10500);
    }
    rax6 = fun_23b0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x68e1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x693c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_69a3() {
    __asm__("cli ");
}

void** ge078 = reinterpret_cast<void**>(0);

int64_t slotvec0 = 0x100;

void fun_69b3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2390(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x10400) {
        fun_2390(rdi7);
        ge078 = reinterpret_cast<void**>(0x10400);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xe070) {
        fun_2390(r12_2);
        slotvec = reinterpret_cast<void**>(0xe070);
    }
    nslots = 1;
    return;
}

void fun_6a53() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6a73() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6a83(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6aa3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_6ac3(void** rdi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26e4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

void** fun_6b53(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x26e9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

void** fun_6be3(int32_t edi, int64_t rsi) {
    int64_t rax3;
    struct s1* rcx4;
    void** rax5;
    int64_t rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x26ee;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = rax3 - g28;
    if (rdx6) {
        fun_2470();
    } else {
        return rax5;
    }
}

void** fun_6c73(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t rax4;
    struct s1* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26f3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = rax4 - g28;
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

void** fun_6d03(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    int64_t rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    int64_t rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x97f0]");
    __asm__("movdqa xmm1, [rip+0x97f8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x97e1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = rax5 - g28;
    if (rdx11) {
        fun_2470();
    } else {
        return rax10;
    }
}

void** fun_6da3(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    int64_t rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x9750]");
    __asm__("movdqa xmm1, [rip+0x9758]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x9741]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = rax4 - g28;
    if (rdx10) {
        fun_2470();
    } else {
        return rax9;
    }
}

void** fun_6e43(int64_t rdi) {
    int64_t rax2;
    void** rax3;
    int64_t rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x96b0]");
    __asm__("movdqa xmm1, [rip+0x96b8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x9699]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = rax2 - g28;
    if (rdx4) {
        fun_2470();
    } else {
        return rax3;
    }
}

void** fun_6ed3(int64_t rdi, int64_t rsi) {
    int64_t rax3;
    void** rax4;
    int64_t rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x9620]");
    __asm__("movdqa xmm1, [rip+0x9628]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x9616]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = rax3 - g28;
    if (rdx5) {
        fun_2470();
    } else {
        return rax4;
    }
}

void** fun_6f63(void** rdi, int32_t esi, int64_t rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26f8;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

void** fun_7003(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x94ea]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x94e2]");
    __asm__("movdqa xmm2, [rip+0x94ea]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26fd;
    if (!rdx) 
        goto 0x26fd;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

void** fun_70a3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t rcx6;
    struct s1* rcx7;
    void** rdi8;
    void** rax9;
    int64_t rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x944a]");
    __asm__("movdqa xmm1, [rip+0x9452]");
    __asm__("movdqa xmm2, [rip+0x945a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2702;
    if (!rdx) 
        goto 0x2702;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = rcx6 - g28;
    if (rdx10) {
        fun_2470();
    } else {
        return rax9;
    }
}

void** fun_7153(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rdx4;
    struct s1* rcx5;
    void** rax6;
    int64_t rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x939a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x9392]");
    __asm__("movdqa xmm2, [rip+0x939a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2707;
    if (!rsi) 
        goto 0x2707;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = rdx4 - g28;
    if (rdx7) {
        fun_2470();
    } else {
        return rax6;
    }
}

void** fun_71f3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int64_t rcx5;
    struct s1* rcx6;
    void** rax7;
    int64_t rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x92fa]");
    __asm__("movdqa xmm1, [rip+0x9302]");
    __asm__("movdqa xmm2, [rip+0x930a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x270c;
    if (!rsi) 
        goto 0x270c;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = rcx5 - g28;
    if (rdx8) {
        fun_2470();
    } else {
        return rax7;
    }
}

void fun_7293() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_72a3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_72c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_72e3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int64_t fun_2510(int64_t rdi, int64_t rsi, uint64_t rdx);

int64_t fun_7303(int32_t edi, int64_t rsi, uint64_t rdx) {
    int32_t r13d4;
    int64_t rbp5;
    uint64_t rbx6;
    int64_t rdi7;
    int64_t rax8;
    int32_t* rax9;

    __asm__("cli ");
    r13d4 = edi;
    rbp5 = rsi;
    rbx6 = rdx;
    while (*reinterpret_cast<int32_t*>(&rdi7) = r13d4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0, rax8 = fun_2510(rdi7, rbp5, rbx6), rax8 < 0) {
        rax9 = fun_23b0();
        if (*rax9 == 4) 
            continue;
        if (rbx6 <= 0x7ff00000) 
            break;
        if (*rax9 != 22) 
            break;
        *reinterpret_cast<int32_t*>(&rbx6) = 0x7ff00000;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    }
    return rax8;
}

void fun_2550(int64_t rdi, void** rsi, void** rdx, void** rcx, int64_t r8, uint64_t r9);

void fun_7373(void** rdi, void** rsi, void** rdx, uint64_t rcx, void*** r8, uint64_t r9) {
    uint64_t r12_7;
    void** rax8;
    void** rax9;
    void** r12_10;
    void** rax11;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2660(rdi, 1, "%s %s\n", rdx, rdi, 1, "%s %s\n", rdx);
    } else {
        r9 = rcx;
        fun_2660(rdi, 1, "%s (%s) %s\n", rsi, rdi, 1, "%s (%s) %s\n", rsi);
    }
    rax8 = fun_2440();
    fun_2660(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8, rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax8);
    fun_2550(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_2440();
    fun_2660(rdi, 1, rax9, "https://gnu.org/licenses/gpl.html", rdi, 1, rax9, "https://gnu.org/licenses/gpl.html");
    fun_2550(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (r12_7 > 9) {
        r12_10 = *r8;
        rax11 = fun_2440();
        fun_2660(rdi, 1, rax11, r12_10, rdi, 1, rax11, r12_10);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xabe8 + r12_7 * 4) + 0xabe8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_77e3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s12 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_7803(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s12* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s12* rcx8;
    int64_t rax9;
    int64_t v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    int64_t rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = v10 - g28;
    if (rax18) {
        fun_2470();
    } else {
        return;
    }
}

void fun_78a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    int64_t rax15;
    int64_t v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    int64_t rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_7946_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7950_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = v16 - g28;
    if (rax20) {
        fun_2470();
    } else {
        return;
    }
    addr_7946_5:
    goto addr_7950_7;
}

void fun_7983() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    int64_t r8_4;
    uint64_t r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2550(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2440();
    fun_25f0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2440();
    fun_25f0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2440();
    goto fun_25f0;
}

int64_t fun_23f0();

void xalloc_die();

void fun_7a23(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2580(void** rdi);

void fun_7a63(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2580(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7a83(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2580(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7aa3(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2580(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25d0();

void fun_7ac3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25d0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7af3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7b23(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7b63() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7ba3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7bd3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7c23(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23f0();
            if (rax5) 
                break;
            addr_7c6d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_7c6d_5;
        rax8 = fun_23f0();
        if (rax8) 
            goto addr_7c56_9;
        if (rbx4) 
            goto addr_7c6d_5;
        addr_7c56_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7cb3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23f0();
            if (rax8) 
                break;
            addr_7cfa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_7cfa_5;
        rax11 = fun_23f0();
        if (rax11) 
            goto addr_7ce2_9;
        if (!rbx6) 
            goto addr_7ce2_9;
        if (r12_4) 
            goto addr_7cfa_5;
        addr_7ce2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7d43(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_7ded_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7e00_10:
                *r12_8 = 0;
            }
            addr_7da0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_7dc6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7e14_14;
            if (rcx10 <= rsi9) 
                goto addr_7dbd_16;
            if (rsi9 >= 0) 
                goto addr_7e14_14;
            addr_7dbd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7e14_14;
            addr_7dc6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25d0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7e14_14;
            if (!rbp13) 
                break;
            addr_7e14_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_7ded_9;
        } else {
            if (!r13_6) 
                goto addr_7e00_10;
            goto addr_7da0_11;
        }
    }
}

int64_t fun_2540();

void fun_7e43() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7e73() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7ea3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7ec3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2540();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2560(signed char* rdi, void** rsi, void** rdx);

void fun_7ee3(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2580(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2560;
    }
}

void fun_7f23(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2580(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2560;
    }
}

struct s13 {
    signed char[1] pad1;
    void** f1;
};

void fun_7f63(int64_t rdi, struct s13* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2580(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2560;
    }
}

void fun_7fa3(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2460(rdi);
    rax3 = fun_2580(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2560;
    }
}

void fun_7fe3() {
    void** rdi1;

    __asm__("cli ");
    fun_2440();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2600();
    fun_23a0(rdi1);
}

uint64_t fun_2620(void** rdi);

int64_t fun_24a0(void** rdi);

int64_t fun_8023(void** rdi, void*** rsi, uint32_t edx, uint64_t* rcx, void** r8) {
    uint64_t* v6;
    int64_t rax7;
    int64_t v8;
    void** rcx9;
    uint64_t rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void*** r15_14;
    int64_t rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    void*** rax21;
    void** rax22;
    int64_t rdx23;
    uint64_t rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (edx > 36) {
        rcx9 = reinterpret_cast<void**>("xstrtoumax");
        rsi = reinterpret_cast<void***>("lib/xstrtol.c");
        fun_24d0("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax");
        do {
            fun_2470();
            while (1) {
                rbx10 = 0xffffffffffffffff;
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_8394_6;
                    rbx10 = rbx10 * reinterpret_cast<unsigned char>(rcx9);
                } while (!__intrinsic());
            }
            addr_8394_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *r15_14 = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_80dd_12:
            *v6 = rbx10;
            addr_80e5_13:
            rax15 = v8 - g28;
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_23b0();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_26a0(rdi);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx23 * 2) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_811b_21;
    rsi = r15_14;
    rax24 = fun_2620(rbp17);
    r8 = *r15_14;
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx10) + 4) = 0, rax26 = fun_24a0(r13_18), r8 = r8, rax26 == 0))) {
            addr_811b_21:
            r12d11 = 4;
            goto addr_80e5_13;
        } else {
            addr_8159_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_24a0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void**>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xac98 + rbp33 * 4) + 0xac98;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_811b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_80dd_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_80dd_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_24a0(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_8159_24;
    }
    r12d11 = r12d11 | 2;
    *v6 = rbx10;
    goto addr_80e5_13;
}

int64_t fun_23e0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_8453(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23e0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_84ae_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23b0();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_84ae_3;
            rax6 = fun_23b0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int32_t fun_25b0(struct s6* rdi);

int64_t fun_24c0(int64_t rdi, ...);

int32_t rpl_fflush(struct s6* rdi);

int64_t fun_2410(struct s6* rdi);

int64_t fun_84c3(struct s6* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2570(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25b0(rdi);
        if (!(eax3 && (eax4 = fun_2570(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24c0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23b0();
            r12d9 = *rax8;
            rax10 = fun_2410(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2410;
}

void rpl_fseeko(struct s6* rdi);

void fun_8553(struct s6* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25b0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_85a3(struct s6* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2570(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24c0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_25a0(int64_t rdi);

signed char* fun_8623() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25a0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2490(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_8663(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    int64_t rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    int64_t rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2490(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = rax5 - g28;
    if (rax9) {
        fun_2470();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_86f3() {
    int64_t rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    int64_t rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = rax1 - g28;
    if (rdx7) {
        fun_2470();
    } else {
        return rax3;
    }
}

int64_t fun_8773() {
    void** rax1;
    int32_t r13d2;
    void** rdx3;
    signed char* rsi4;
    void** rax5;
    int64_t rax6;

    __asm__("cli ");
    rax1 = fun_25e0();
    if (!rax1) {
        r13d2 = 22;
        if (rdx3) {
            *rsi4 = 0;
        }
    } else {
        rax5 = fun_2460(rax1);
        if (reinterpret_cast<unsigned char>(rdx3) > reinterpret_cast<unsigned char>(rax5)) {
            fun_2560(rsi4, rax1, rax5 + 1);
            return 0;
        } else {
            r13d2 = 34;
            if (rdx3) {
                fun_2560(rsi4, rax1, rdx3 + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi4) + reinterpret_cast<unsigned char>(rdx3)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax6) = r13d2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    return rax6;
}

void fun_8823() {
    __asm__("cli ");
    goto fun_25e0;
}

void fun_8833() {
    __asm__("cli ");
}

void fun_8847() {
    __asm__("cli ");
    return;
}

void fun_36d8() {
}

void fun_3720() {
    goto 0x36e0;
}

void fun_3810(void** rdi) {
    int64_t v2;

    fun_26a0(rdi);
    goto v2;
}

struct s14 {
    signed char[16] pad16;
    unsigned char f10;
};

struct s15 {
    signed char[48] pad48;
    signed char f30;
};

struct s16 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_39e7() {
    uint32_t ecx1;
    struct s14* rbp2;
    int32_t eax3;
    uint64_t rbx4;
    int64_t rdx5;
    struct s15* v6;
    uint64_t tmp64_7;
    int64_t r14_8;
    struct s16* rbp9;

    ecx1 = rbp2->f10;
    eax3 = 0;
    *reinterpret_cast<int32_t*>(&rbx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx4) + 4) = 0;
    do {
        *reinterpret_cast<int32_t*>(&rdx5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx5) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rdx5) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&ecx1) == *reinterpret_cast<signed char*>(&eax3));
        ++eax3;
        rbx4 = rbx4 + rdx5;
    } while (eax3 != 0x100);
    v6->f30 = 1;
    tmp64_7 = rbx4 + r14_8;
    if (tmp64_7 < rbx4 || tmp64_7 == 0xffffffffffffffff) {
        fun_2440();
        fun_2600();
        fun_24d0("p->u.range.last_char >= p->u.range.first_char", "src/tr.c", 0x4f1, "get_spec_stats");
    } else {
        if (rbp9->f8) 
            goto 0x39d0; else 
            goto "???";
    }
}

struct s17 {
    signed char[16] pad16;
    int32_t f10;
};

struct s18 {
    signed char[49] pad49;
    signed char f31;
};

struct s19 {
    signed char[50] pad50;
    signed char f32;
};

void fun_3a44() {
    int32_t r15d1;
    struct s17* rbp2;
    uint32_t r13d3;
    struct s18* v4;
    void** rdi5;
    struct s19* v6;

    r15d1 = rbp2->f10;
    r13d3 = 0;
    v4->f31 = 1;
    do {
        *reinterpret_cast<int32_t*>(&rdi5) = r15d1;
        *reinterpret_cast<int32_t*>(&rdi5 + 4) = 0;
        is_char_class_member(rdi5, r13d3);
        ++r13d3;
    } while (r13d3 != 0x100);
    if (!(reinterpret_cast<uint32_t>(r15d1 - 6) & 0xfffffffb)) 
        goto 0x3a0d;
    v6->f32 = 1;
    goto 0x3a0d;
}

struct s20 {
    signed char[17] pad17;
    unsigned char f11;
};

struct s21 {
    signed char[16] pad16;
    unsigned char f10;
};

void fun_3a96() {
    uint32_t eax1;
    struct s20* rbp2;
    uint32_t edx3;
    struct s21* rbp4;

    eax1 = rbp2->f11;
    edx3 = rbp4->f10;
    if (*reinterpret_cast<unsigned char*>(&eax1) < *reinterpret_cast<unsigned char*>(&edx3)) 
        goto 0x3b02;
    goto 0x3a0d;
}

void fun_3ab4() {
    goto 0x3a0d;
}

struct s22 {
    signed char[8] pad8;
    int64_t f8;
};

struct s23 {
    signed char[16] pad16;
    int64_t f10;
};

struct s24 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_3b80() {
    int64_t rax1;
    struct s22* r14_2;
    struct s23* rbp3;
    struct s24* rbp4;

    rax1 = r14_2->f8;
    rbp3->f10 = -1;
    rbp4->f8 = rax1;
}

struct s25 {
    signed char[16] pad16;
    int32_t f10;
};

struct s26 {
    signed char[16] pad16;
    int32_t f10;
};

struct s27 {
    signed char[16] pad16;
    int32_t f10;
};

struct s28 {
    signed char[16] pad16;
    int64_t f10;
};

struct s29 {
    signed char[16] pad16;
    int64_t f10;
};

struct s30 {
    signed char[16] pad16;
    int64_t f10;
};

struct s31 {
    signed char[8] pad8;
    int64_t f8;
};

struct s32 {
    signed char[16] pad16;
    int64_t f10;
};

struct s33 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_3bd8() {
    int32_t r12d1;
    struct s25* r14_2;
    int64_t rsi3;
    int32_t* rsi4;
    struct s26* r14_5;
    int32_t* rsi6;
    struct s27* r14_7;
    int64_t rbx8;
    struct s28* rbp9;
    uint32_t ebx10;
    void** rdi11;
    signed char al12;
    void** rdi13;
    signed char al14;
    int32_t ebx15;
    void** rdi16;
    signed char al17;
    struct s29* rbp18;
    struct s30* rbp19;
    int64_t v20;
    int64_t rax21;
    struct s31* r14_22;
    struct s32* rbp23;
    struct s33* rbp24;
    int64_t v25;

    r12d1 = r14_2->f10;
    if (rsi3) {
        if (r12d1 == 6) {
            *rsi4 = 0;
            r12d1 = r14_5->f10;
        } else {
            if (r12d1 == 10) {
                *rsi6 = 1;
                r12d1 = r14_7->f10;
            }
        }
    }
    rbx8 = rbp9->f10;
    if (rbx8 == -1) {
        ebx10 = 0;
        do {
            *reinterpret_cast<int32_t*>(&rdi11) = r12d1;
            *reinterpret_cast<int32_t*>(&rdi11 + 4) = 0;
            al12 = is_char_class_member(rdi11, ebx10);
            if (al12) 
                break;
            ++ebx10;
        } while (ebx10 != 0x100);
        goto addr_3d76_10;
    } else {
        addr_3c09_11:
        *reinterpret_cast<int32_t*>(&rdi13) = r12d1;
        *reinterpret_cast<int32_t*>(&rdi13 + 4) = 0;
        al14 = is_char_class_member(rdi13, static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&rbx8)));
        if (!al14) {
            fun_24d0("is_char_class_member (p->u.char_class, s->state)", "src/tr.c", 0x43a, "get_next");
            goto addr_3d76_10;
        } else {
            ebx15 = *reinterpret_cast<int32_t*>(&rbx8) + 1;
            if (ebx15 <= 0xff) {
                do {
                    *reinterpret_cast<int32_t*>(&rdi16) = r12d1;
                    *reinterpret_cast<int32_t*>(&rdi16 + 4) = 0;
                    al17 = is_char_class_member(rdi16, static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ebx15)));
                    if (al17) 
                        goto addr_3c4a_15;
                    ++ebx15;
                } while (ebx15 != 0x100);
                goto addr_3c87_17;
            } else {
                goto addr_3c87_17;
            }
        }
    }
    rbx8 = reinterpret_cast<int32_t>(ebx10);
    rbp18->f10 = rbx8;
    goto addr_3c09_11;
    addr_3d76_10:
    fun_24d0("i < N_CHARS", "src/tr.c", 0x437, "get_next");
    addr_3c4a_15:
    rbp19->f10 = ebx15;
    goto v20;
    addr_3c87_17:
    rax21 = r14_22->f8;
    rbp23->f10 = -1;
    rbp24->f8 = rax21;
    goto v25;
}

struct s34 {
    signed char[16] pad16;
    int64_t f10;
};

struct s35 {
    signed char[16] pad16;
    unsigned char f10;
};

struct s36 {
    signed char[17] pad17;
    unsigned char f11;
};

struct s37 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_3c60() {
    int64_t rax1;
    struct s34* rbp2;
    struct s35* r14_3;
    int64_t rdx4;
    struct s36* r14_5;
    struct s37* rbp6;

    rax1 = rbp2->f10 + 1;
    if (rbp2->f10 == -1) {
        *reinterpret_cast<uint32_t*>(&rax1) = r14_3->f10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax1) + 4) = 0;
    }
    *reinterpret_cast<uint32_t*>(&rdx4) = r14_5->f11;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    rbp6->f10 = rax1;
    if (rdx4 != rax1) 
        goto 0x3b95; else 
        goto "???";
}

void fun_4272() {
}

void fun_46c4() {
    int64_t rax1;
    int64_t rcx2;
    int64_t rbx3;
    int32_t edx4;
    int64_t r8_5;
    int64_t r13_6;
    int64_t rax7;
    int64_t rbx8;
    int64_t rax9;
    int64_t rcx10;
    int32_t eax11;
    int64_t r13_12;
    int64_t rax13;
    int64_t rdx14;

    *reinterpret_cast<int32_t*>(&rax1) = static_cast<int32_t>(rcx2 + 2);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax1) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rbx3) = edx4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = *reinterpret_cast<signed char*>(r13_6 + rax1);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rax7) = static_cast<uint32_t>(r8_5 - 48);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax7) > 7) 
        goto 0x4280;
    *reinterpret_cast<int32_t*>(&rbx8) = static_cast<int32_t>(rax7 + rbx3 * 8);
    *reinterpret_cast<int32_t*>(&rax9) = static_cast<int32_t>(rcx10 + 3);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    eax11 = *reinterpret_cast<signed char*>(r13_12 + rax9);
    *reinterpret_cast<uint32_t*>(&rax13) = eax11 - 48;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax13) > 7) {
        goto 0x4280;
    } else {
        *reinterpret_cast<uint32_t*>(&rdx14) = *reinterpret_cast<unsigned char*>(&rbx8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx14) + 4) = 0;
        if (static_cast<int32_t>(rax13 + rdx14 * 8) > 0xff) {
            fun_2440();
            fun_2600();
            goto 0x4280;
        } else {
            goto 0x4280;
        }
    }
}

void fun_4715() {
    goto 0x4280;
}

uint32_t fun_2520(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, unsigned char* rsi);

int32_t fun_2680(int64_t rdi, unsigned char* rsi);

uint32_t fun_2670(void** rdi, unsigned char* rsi);

void fun_51a5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rax10;
    void** r11_11;
    void** v12;
    int32_t ebp13;
    void** rax14;
    void* r15_15;
    int32_t ebx16;
    void** rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void* rsi30;
    void* v31;
    void** v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rdx44;
    void** rcx45;
    uint32_t edx46;
    unsigned char al47;
    void** v48;
    int64_t v49;
    void** v50;
    void** v51;
    void** rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    void** v61;
    unsigned char v62;
    void* v63;
    void* v64;
    void** v65;
    signed char* v66;
    void** r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void* r14_71;
    void** r13_72;
    unsigned char* rsi73;
    void* v74;
    void** r15_75;
    unsigned char* rdx76;
    void* v77;
    int64_t rax78;
    int64_t rdi79;
    int32_t v80;
    int32_t eax81;
    void* rdi82;
    uint32_t edx83;
    unsigned char v84;
    void* rdi85;
    void* v86;
    uint32_t esi87;
    uint32_t ebp88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    uint32_t eax94;
    void* rdx95;
    void* rcx96;
    void* v97;
    void*** rax98;
    uint1_t zf99;
    int32_t ecx100;
    uint32_t ecx101;
    uint32_t edi102;
    int32_t ecx103;
    uint32_t edi104;
    uint32_t edx105;
    uint32_t edi106;
    uint32_t edx107;
    int64_t rax108;
    uint32_t eax109;
    uint32_t r12d110;
    int64_t rax111;
    int64_t rax112;
    uint32_t r12d113;
    void* v114;
    void* rdx115;
    void* rax116;
    void* v117;
    int64_t rax118;
    int64_t v119;
    int64_t rax120;
    int64_t rax121;
    int64_t rax122;
    int64_t v123;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2440();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_2440();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx17)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2460(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_54a3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_54a3_22; else 
                            goto addr_589d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_595d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5cb0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_54a0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_54a0_30; else 
                                goto addr_5cc9_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2460(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5cb0_28;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2520(rdi39, v26, v28, rcx45);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5cb0_28; else 
                            goto addr_534c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_5e10_39:
                    *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5c90_40:
                        if (r11_27 == 1) {
                            addr_581d_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_5dd8_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx45);
                                goto addr_5457_44;
                            }
                        } else {
                            goto addr_5ca0_46;
                        }
                    } else {
                        addr_5e1f_47:
                        rax24 = v48;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_581d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_54a3_22:
                                if (v49 != 1) {
                                    addr_59f9_52:
                                    v50 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_2460(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_5a44_54;
                                    }
                                } else {
                                    goto addr_54b0_56;
                                }
                            } else {
                                addr_5455_57:
                                ebp36 = 0;
                                goto addr_5457_44;
                            }
                        } else {
                            addr_5c84_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_5e1f_47; else 
                                goto addr_5c8e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_581d_41;
                        if (v49 == 1) 
                            goto addr_54b0_56; else 
                            goto addr_59f9_52;
                    }
                }
                addr_5511_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<void**>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_53a8_63:
                    if (!1 && (edx54 = *reinterpret_cast<uint32_t*>(&rcx45), *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<unsigned char*>(&rcx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_53cd_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_56d0_65;
                    } else {
                        addr_5539_66:
                        ++r9_37;
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5d88_67;
                    }
                } else {
                    goto addr_5530_69;
                }
                addr_53e1_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                ++r9_37;
                addr_542c_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5d88_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_542c_81;
                }
                addr_5530_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_53cd_64; else 
                    goto addr_5539_66;
                addr_5457_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_550f_91;
                if (v22) 
                    goto addr_546f_93;
                addr_550f_91:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_5511_62;
                addr_5a44_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx45 = r12_67;
                    r13_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_61cb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_623b_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72)) + 1);
                        rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = *rdx76 - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_603f_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    *reinterpret_cast<int32_t*>(&rdi79) = v80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi79) + 4) = 0;
                    eax81 = fun_2680(rdi79, rsi73);
                    if (!eax81) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2670(r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx83 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx83) & reinterpret_cast<unsigned char>(v32));
                addr_5b3e_109:
                if (reinterpret_cast<uint64_t>(rdi82) <= 1) {
                    addr_54fc_110:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        edx83 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_5b48_112;
                    }
                } else {
                    addr_5b48_112:
                    v84 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi85 = v86;
                    esi87 = 0;
                    ebp88 = reinterpret_cast<unsigned char>(v22);
                    rcx45 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi82) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_5c19_114;
                }
                addr_5508_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_550f_91;
                while (1) {
                    addr_5c19_114:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        *reinterpret_cast<unsigned char*>(&esi87) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax89 = esi87;
                        if (*reinterpret_cast<signed char*>(&ebp88)) 
                            goto addr_6127_117;
                        eax90 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax90) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax90) & *reinterpret_cast<unsigned char*>(&esi87));
                        if (*reinterpret_cast<unsigned char*>(&eax90)) 
                            goto addr_5b86_119;
                    } else {
                        eax57 = (esi87 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                            goto addr_6135_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_5c07_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_5c07_128;
                        }
                    }
                    addr_5bb5_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax91 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax91) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax91) >> 6);
                        eax92 = eax91 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax92);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax93 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax93) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax93) >> 3);
                        eax94 = (eax93 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax94);
                    }
                    ++r9_37;
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                        break;
                    esi87 = edx83;
                    addr_5c07_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi85) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_5b86_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax90;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_5bb5_134;
                }
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_542c_81;
                addr_6135_125:
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_5d88_67;
                addr_61cb_96:
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx83 = reinterpret_cast<unsigned char>(v32);
                goto addr_5b3e_109;
                addr_623b_98:
                r11_27 = v65;
                rdi82 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx95 = rdi82;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx96 = v97;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx96) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx95 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx95) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx95));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi82 = rdx95;
                }
                edx83 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_5b3e_109;
                addr_54b0_56:
                rax98 = fun_26a0(rdi39, rdi39);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi82) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf99 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax98 + reinterpret_cast<unsigned char>(rax24) * 2) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf99);
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf99) & reinterpret_cast<unsigned char>(v32));
                goto addr_54fc_110;
                addr_5c8e_59:
                goto addr_5c90_40;
                addr_595d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_54a3_22;
                *reinterpret_cast<uint32_t*>(&rcx45) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx45));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_5508_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5455_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_54a3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_59a2_160;
                if (!v22) 
                    goto addr_5d77_162; else 
                    goto addr_5f83_163;
                addr_59a2_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_5d77_162:
                    ++r9_37;
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    goto addr_5d88_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!v32) 
                        goto addr_584b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                addr_56b3_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_53e1_70; else 
                    goto addr_56c7_169;
                addr_584b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_53a8_63;
                goto addr_5530_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5c84_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_5dbf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_54a0_30;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx101 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_5398_178; else 
                        goto addr_5d42_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5c84_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_54a3_22;
                }
                addr_5dbf_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_54a0_30:
                    r8d42 = 0;
                    goto addr_54a3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_5511_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        goto addr_5dd8_42;
                    }
                }
                addr_5398_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx101;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_53a8_63;
                addr_5d42_179:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5ca0_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_5511_62;
                } else {
                    addr_5d52_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_54a3_22;
                }
                edi102 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi102))) 
                    goto addr_6502_188;
                if (v28) 
                    goto addr_5d77_162;
                addr_6502_188:
                *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                goto addr_56b3_168;
                addr_534c_37:
                if (v22) 
                    goto addr_6343_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_5363_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_5e10_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_5e9b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_54a3_22;
                    ecx103 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx103));
                    ecx101 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_5398_178; else 
                        goto addr_5e77_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5c84_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_54a3_22;
                }
                addr_5e9b_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_54a3_22;
                }
                addr_5e77_199:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5ca0_46;
                goto addr_5d52_186;
                addr_5363_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_54a3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_54a3_22; else 
                    goto addr_5374_206;
            }
            edi104 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx45) = reinterpret_cast<uint1_t>(r15_15 == 0);
            edx105 = edi104 & *reinterpret_cast<uint32_t*>(&rax24);
            if (*reinterpret_cast<unsigned char*>(&rcx45) & *reinterpret_cast<unsigned char*>(&edx105)) 
                goto addr_644e_208;
            edi106 = edi104 ^ 1;
            edx107 = edi106;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi106));
            if (!rax24) 
                goto addr_62d4_210;
            if (1) 
                goto addr_62d2_212;
            if (!v29) 
                goto addr_5f0e_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax108 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v49 = rax108;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_6441_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_56d0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax109 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax109)) 
            goto addr_548b_219; else 
            goto addr_56ea_220;
        addr_546f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax89 = reinterpret_cast<unsigned char>(v32);
        addr_5483_221:
        if (*reinterpret_cast<signed char*>(&eax89)) 
            goto addr_56ea_220; else 
            goto addr_548b_219;
        addr_603f_103:
        r12d110 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d110)) {
            addr_56ea_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax111 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax111;
        } else {
            addr_605d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax112 = fun_2450();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax112;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_64d0_225:
        v31 = r13_34;
        addr_5f36_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_6127_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_5483_221;
        addr_5f83_163:
        eax89 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_5483_221;
        addr_56c7_169:
        goto addr_56d0_65;
        addr_644e_208:
        r14_35 = r12_21;
        r12d113 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d113)) 
            goto addr_56ea_220;
        goto addr_605d_222;
        addr_62d4_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx107) && (*reinterpret_cast<uint32_t*>(&rcx45) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx45)))) {
            rsi30 = v114;
            rdx115 = r15_15;
            rax116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx115)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi30) + reinterpret_cast<uint64_t>(rdx115)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                rdx115 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx115) + 1);
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax116) + reinterpret_cast<uint64_t>(rdx115));
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx45));
            r15_15 = rdx115;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v117) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax118 = v119 - g28;
        if (!rax118) 
            goto addr_632e_236;
        fun_2470();
        rsp25 = rsp25 - 8 + 8;
        goto addr_64d0_225;
        addr_62d2_212:
        edx107 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_62d4_210;
        addr_5f0e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx107 = 0;
            goto addr_62d4_210;
        } else {
            goto addr_5f36_226;
        }
        addr_6441_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_589d_24:
    *reinterpret_cast<uint32_t*>(&rax120) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax120) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa6ac + rax120 * 4) + 0xa6ac;
    addr_5cc9_32:
    *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax121) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa7ac + rax121 * 4) + 0xa7ac;
    addr_6343_190:
    addr_548b_219:
    goto 0x5170;
    addr_5374_206:
    *reinterpret_cast<uint32_t*>(&rax122) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa5ac + rax122 * 4) + 0xa5ac;
    addr_632e_236:
    goto v123;
}

void fun_5390() {
}

void fun_5548() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x5242;
}

void fun_55a1() {
    goto 0x5242;
}

void fun_568e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x5511;
    }
    if (v2) 
        goto 0x5f83;
    if (!r10_3) 
        goto addr_60ee_5;
    if (!v4) 
        goto addr_5fbe_7;
    addr_60ee_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_5fbe_7:
    goto 0x53c4;
}

void fun_56ac() {
}

void fun_5757() {
    signed char v1;

    if (v1) {
        goto 0x56df;
    } else {
        goto 0x541a;
    }
}

void fun_5771() {
    signed char v1;

    if (!v1) 
        goto 0x576a; else 
        goto "???";
}

void fun_5798() {
    goto 0x56b3;
}

void fun_5818() {
}

void fun_5830() {
}

void fun_585f() {
    goto 0x56b3;
}

void fun_58b1() {
    goto 0x5840;
}

void fun_58e0() {
    goto 0x5840;
}

void fun_5913() {
    goto 0x5840;
}

void fun_5ce0() {
    goto 0x5398;
}

void fun_5fde() {
    signed char v1;

    if (v1) 
        goto 0x5f83;
    goto 0x53c4;
}

void fun_6085() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x53c4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x53a8;
        goto 0x53c4;
    }
}

void fun_64a2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x5710;
    } else {
        goto 0x5242;
    }
}

void fun_7448() {
    fun_2440();
}

void fun_81cc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x81db;
}

void fun_829c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x82a9;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x81db;
}

void fun_82c0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_82ec() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x81db;
}

void fun_830d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x81db;
}

void fun_8331() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x81db;
}

void fun_8355() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x82e4;
}

void fun_8379() {
}

void fun_8399() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x82e4;
}

void fun_83b5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x82e4;
}

void fun_3730() {
    goto 0x36e0;
}

void fun_3830(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

struct s38 {
    signed char[24] pad24;
    int64_t f18;
};

struct s39 {
    signed char[32] pad32;
    int64_t f20;
    int64_t f28;
};

void fun_3abe() {
    struct s38* rbp1;
    struct s39* v2;
    int64_t rbp3;

    if (rbp1->f18) 
        goto 0x3a0d;
    v2->f20 = v2->f20 + 1;
    v2->f28 = rbp3;
    goto 0x3a23;
}

struct s40 {
    signed char[24] pad24;
    int64_t f18;
};

struct s41 {
    signed char[16] pad16;
    int64_t f10;
};

struct s42 {
    signed char[16] pad16;
    int64_t f10;
};

struct s43 {
    signed char[8] pad8;
    int64_t f8;
};

struct s44 {
    signed char[16] pad16;
    int64_t f10;
};

struct s45 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_3ba8() {
    int64_t rax1;
    struct s40* r14_2;
    int64_t rdx3;
    struct s41* rbp4;
    struct s42* rbp5;
    int64_t rax6;
    struct s43* r14_7;
    struct s44* rbp8;
    struct s45* rbp9;

    rax1 = r14_2->f18;
    if (rax1) {
        rdx3 = rbp4->f10 + 1;
        if (rbp4->f10 == -1) {
            rdx3 = 1;
        }
        rbp5->f10 = rdx3;
        if (rax1 != rdx3) 
            goto 0x3b95;
        goto 0x3c87;
    } else {
        rax6 = r14_7->f8;
        rbp8->f10 = -1;
        rbp9->f8 = rax6;
        goto 0x3b46;
    }
}

void fun_4924() {
    goto 0x4280;
}

void fun_4937() {
    goto 0x4280;
}

void fun_471f() {
    goto 0x4280;
}

void fun_55ce() {
    goto 0x5242;
}

void fun_57a4() {
    goto 0x575c;
}

void fun_586b() {
    goto 0x5398;
}

void fun_58bd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x5840;
    goto 0x546f;
}

void fun_58ef() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x584b;
        goto 0x5270;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x56ea;
        goto 0x548b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x6088;
    if (r10_8 > r15_9) 
        goto addr_57d5_9;
    addr_57da_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x6093;
    goto 0x53c4;
    addr_57d5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_57da_10;
}

void fun_5922() {
    goto 0x5457;
}

void fun_5cf0() {
    goto 0x5457;
}

void fun_648f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x55ac;
    } else {
        goto 0x5710;
    }
}

void fun_7500() {
}

void fun_826f() {
    if (__intrinsic()) 
        goto 0x82a9; else 
        goto "???";
}

void fun_3740() {
    goto 0x36e0;
}

void fun_3850(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_4729() {
    goto 0x4280;
}

void fun_592c() {
    goto 0x58c7;
}

void fun_5cfa() {
    goto 0x581d;
}

void fun_7560() {
    fun_2440();
    goto fun_2660;
}

void fun_3750() {
    goto 0x36e0;
}

void fun_3870(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_4733() {
    goto 0x4280;
}

void fun_55fd() {
    goto 0x5242;
}

void fun_5938() {
    goto 0x58c7;
}

void fun_5d07() {
    goto 0x586e;
}

void fun_75a0() {
    fun_2440();
    goto fun_2660;
}

void fun_3760() {
    goto 0x36e0;
}

void fun_3890(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_473d() {
    goto 0x4280;
}

void fun_562a() {
    goto 0x5242;
}

void fun_5944() {
    goto 0x5840;
}

void fun_75e0() {
    fun_2440();
    goto fun_2660;
}

void fun_38b0(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_564c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5fe0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x5511;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x5511;
    }
    if (v11) 
        goto 0x6343;
    if (r10_12 > r15_13) 
        goto addr_6393_8;
    addr_6398_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x60d1;
    addr_6393_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_6398_9;
}

void fun_7630() {
    void** r12_1;
    void*** rbx2;
    void** rax3;
    void** rbp4;
    int64_t v5;

    r12_1 = *rbx2;
    rax3 = fun_2440();
    fun_2660(rbp4, 1, rax3, r12_1, rbp4, 1, rax3, r12_1);
    goto v5;
}

void fun_38d0(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_7688() {
    fun_2440();
    goto 0x7659;
}

void fun_38f0(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_76c0() {
    void** r12_1;
    void*** rbx2;
    void** rax3;
    void** rbp4;
    int64_t v5;

    r12_1 = *rbx2;
    rax3 = fun_2440();
    fun_2660(rbp4, 1, rax3, r12_1, rbp4, 1, rax3, r12_1);
    goto v5;
}

void fun_3910(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_7738() {
    fun_2440();
    goto 0x76fb;
}

void fun_3930(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_3950(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}

void fun_3970(void** rdi) {
    fun_26a0(rdi);
    goto 0x3827;
}
