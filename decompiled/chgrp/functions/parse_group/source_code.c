parse_group (char const *name)
{
  gid_t gid = -1;

  if (*name)
    {
      struct group *grp = getgrnam (name);
      if (grp)
        gid = grp->gr_gid;
      else
        {
          uintmax_t tmp;
          if (! (xstrtoumax (name, NULL, 10, &tmp, "") == LONGINT_OK
                 && tmp <= GID_T_MAX))
            die (EXIT_FAILURE, 0, _("invalid group: %s"),
                 quote (name));
          gid = tmp;
        }
      endgrent ();		/* Save a file descriptor. */
    }

  return gid;
}