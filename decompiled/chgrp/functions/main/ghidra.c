void main(int param_1,undefined8 *param_2)

{
  ulong uVar1;
  int iVar2;
  uint uVar3;
  undefined8 uVar4;
  group *pgVar5;
  undefined8 uVar6;
  int *piVar7;
  undefined8 extraout_RDX;
  char *__name;
  char *pcVar8;
  long in_FS_OFFSET;
  undefined auVar9 [16];
  undefined8 uStack296;
  uint local_120;
  char local_119;
  ulong local_110;
  undefined local_108 [4];
  char local_104;
  long local_100;
  undefined local_f8;
  char *local_e8;
  stat local_d8;
  undefined8 local_40;
  
  __name = (char *)(long)param_1;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  pcVar8 = &DAT_0010c9a0;
  textdomain("coreutils");
  atexit(close_stdout);
  chopt_init(local_108);
  uStack296 = CONCAT44(0xffffffff,(undefined4)uStack296);
  local_120 = 0x10;
  local_119 = '\0';
  do {
    iVar2 = getopt_long(param_1,param_2,"HLPRcfhv",long_options,0);
    if (iVar2 == -1) {
      if (local_104 == '\0') {
LAB_00102a79:
        local_120 = 0x10;
      }
      else if (local_120 == 0x10) {
        if (uStack296._4_4_ == 1) goto LAB_00102d9c;
        uStack296 = uStack296 & 0xffffffff;
      }
      pcVar8 = reference_file;
      local_f8 = uStack296._4_4_ != 0;
      if (reference_file == (char *)0x0) {
        if (1 < param_1 - optind) {
          __name = (char *)param_2[optind];
          optind = optind + 1;
          if (*__name != '\0') goto LAB_00102d25;
          goto LAB_00102c36;
        }
      }
      else if (0 < param_1 - optind) {
        iVar2 = stat(reference_file,&local_d8);
        if (iVar2 != 0) goto LAB_00102dc0;
        local_e8 = (char *)gid_to_name(local_d8.st_gid);
        break;
      }
      if (optind < param_1) {
        pcVar8 = (char *)quote(param_2[(long)(__name + -1)]);
        uVar4 = dcgettext(0,"missing operand after %s",5);
        error(0,0,uVar4,pcVar8);
      }
      else {
        uVar4 = dcgettext(0,"missing operand",5);
        error(0,0,uVar4);
      }
    }
    else if (iVar2 < 0x84) {
      if (iVar2 < 0x48) {
        if (iVar2 == -0x83) {
          version_etc(stdout,"chgrp","GNU coreutils",Version,"David MacKenzie","Jim Meyering",0,
                      extraout_RDX);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar2 == -0x82) {
          usage(0);
          goto LAB_00102a79;
        }
      }
      else if (iVar2 - 0x48U < 0x3c) {
                    /* WARNING: Could not recover jumptable at 0x00102a57. Too many branches */
                    /* WARNING: Treating indirect jump as call */
        (*(code *)(pcVar8 + *(int *)(pcVar8 + (ulong)(iVar2 - 0x48U) * 4)))();
        return;
      }
    }
    usage();
    reference_file = optarg;
  } while( true );
LAB_00102c5f:
  if ((local_104 == '\0') || (local_119 == '\0')) {
LAB_00102c6d:
    uVar3 = chown_files(param_2 + optind,local_120 | 0x400,0xffffffff,local_d8.st_gid,0xffffffff,
                        0xffffffff,local_108,(long)optind);
                    /* WARNING: Subroutine does not return */
    exit((uVar3 ^ 1) & 0xff);
  }
  local_100 = get_root_dev_ino(dev_ino_buf_0);
  if (local_100 != 0) goto LAB_00102c6d;
  uVar4 = quotearg_style(4,"/");
  uVar6 = dcgettext(0,"failed to get attributes of %s",5);
  piVar7 = __errno_location();
  error(1,*piVar7,uVar6,uVar4);
LAB_00102d25:
  pcVar8 = (char *)xstrdup(__name);
LAB_00102c36:
  local_d8.st_gid = 0xffffffff;
  local_e8 = pcVar8;
  if (*__name != '\0') {
    pgVar5 = getgrnam(__name);
    if (pgVar5 == (group *)0x0) {
      iVar2 = xstrtoumax(__name,0,10,&local_110,"");
      if (iVar2 != 0) {
LAB_00102d6a:
        pcVar8 = (char *)quote(__name);
        uVar4 = dcgettext(0,"invalid group: %s",5);
        error(1,0,uVar4,pcVar8);
LAB_00102d9c:
        uVar4 = dcgettext(0,"-R --dereference requires either -H or -L",5);
        error(1,0,uVar4);
LAB_00102dc0:
        uVar4 = quotearg_style(4,pcVar8);
        uVar6 = dcgettext(0,"failed to get attributes of %s",5);
        piVar7 = __errno_location();
        auVar9 = error(1,*piVar7,uVar6,uVar4);
        uVar1 = uStack296;
        uStack296 = SUB168(auVar9,0);
        (*(code *)PTR___libc_start_main_00111fd0)
                  (main,uVar1,&local_120,0,0,SUB168(auVar9 >> 0x40,0),&uStack296);
        do {
                    /* WARNING: Do nothing block with infinite loop */
        } while( true );
      }
      local_d8.st_gid = (__gid_t)local_110;
      if (0xffffffff < local_110) goto LAB_00102d6a;
    }
    else {
      local_d8.st_gid = pgVar5->gr_gid;
    }
    endgrent();
  }
  goto LAB_00102c5f;
}