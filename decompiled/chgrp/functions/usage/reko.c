void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002890(fn0000000000002590(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002F4E;
	}
	fn00000000000027C0(fn0000000000002590(0x05, "Usage: %s [OPTION]... GROUP FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n", null), 0x01);
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "Change the group of each FILE to GROUP.\nWith --reference, change the group of each FILE to that of RFILE.\n\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "                         (useful only on systems that can change the\n                         ownership of a symlink)\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "      --no-preserve-root  do not treat '/' specially (the default)\n      --preserve-root    fail to operate recursively on '/'\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "      --reference=RFILE  use RFILE's group rather than specifying a\n                         GROUP value\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "  -R, --recursive        operate on files and directories recursively\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "      --help        display this help and exit\n", null));
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "      --version     output version information and exit\n", null));
	fn00000000000027C0(fn0000000000002590(0x05, "\nExamples:\n  %s staff /u      Change the group of /u to \"staff\".\n  %s -hR staff /u  Change the group of /u and subfiles to \"staff\".\n", null), 0x01);
	struct Eq_1111 * rbx_252 = fp - 0xB8 + 16;
	do
	{
		char * rsi_254 = rbx_252->qw0000;
		++rbx_252;
	} while (rsi_254 != null && fn00000000000026D0(rsi_254, "chgrp") != 0x00);
	ptr64 r13_267 = rbx_252->qw0008;
	if (r13_267 != 0x00)
	{
		fn00000000000027C0(fn0000000000002590(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_8 rax_354 = fn00000000000027B0(null, 0x05);
		if (rax_354 == 0x00 || fn00000000000024D0(0x03, "en_", rax_354) == 0x00)
			goto l0000000000003236;
	}
	else
	{
		fn00000000000027C0(fn0000000000002590(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_8 rax_296 = fn00000000000027B0(null, 0x05);
		if (rax_296 == 0x00 || fn00000000000024D0(0x03, "en_", rax_296) == 0x00)
		{
			fn00000000000027C0(fn0000000000002590(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003273:
			fn00000000000027C0(fn0000000000002590(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002F4E:
			fn0000000000002870(edi);
		}
		r13_267 = 0xC004;
	}
	fn00000000000026B0(stdout, fn0000000000002590(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003236:
	fn00000000000027C0(fn0000000000002590(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003273;
}