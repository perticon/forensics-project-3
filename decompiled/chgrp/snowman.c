
void** fun_25b0(void** rdi, ...);

void** xmalloc(uint64_t rdi);

signed char* fun_2580(void** rdi, void** rsi);

void fun_24f0(int64_t rdi, void** rsi);

void** user_group_str(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r13_6;
    void** rax7;
    void** rax8;
    void** rax9;
    signed char* rax10;

    if (!rdi) {
        if (!rsi) {
            *reinterpret_cast<int32_t*>(&r13_6) = 0;
            *reinterpret_cast<int32_t*>(&r13_6 + 4) = 0;
        } else {
            goto addr_33c0_5;
        }
    } else {
        if (!rsi) {
            addr_33c0_5:
            goto addr_af30_7;
        } else {
            rax7 = fun_25b0(rdi);
            rax8 = fun_25b0(rsi);
            rax9 = xmalloc(reinterpret_cast<unsigned char>(rax7) + reinterpret_cast<unsigned char>(rax8) + 2);
            r13_6 = rax9;
            rax10 = fun_2580(rax9, rdi);
            *rax10 = 58;
            fun_24f0(rax10 + 1, rsi);
        }
    }
    return r13_6;
    addr_af30_7:
}

int64_t fun_24b0(void** rdi, ...);

int32_t i_ring_push(void*** rdi);

int32_t fun_2660();

int32_t cwd_advance_fd(void** rdi, void** esi, signed char dl) {
    int32_t eax4;
    int32_t eax5;

    if (*reinterpret_cast<void***>(rdi + 44) == esi && !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) {
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
    }
    if (dl) {
        eax4 = i_ring_push(rdi + 96);
        if (eax4 < 0) {
            addr_4a39_27:
            *reinterpret_cast<void***>(rdi + 44) = esi;
            return eax4;
        } else {
            eax5 = fun_2660();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_4a39_27;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_4a39_27;
        eax5 = fun_2660();
    }
    *reinterpret_cast<void***>(rdi + 44) = esi;
    return eax5;
}

void** fun_2750(void** rdi, void** rsi, void** rdx, ...);

void fun_2710(void** rdi, void** rsi, void** rdx);

void** fts_alloc(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    rax4 = fun_2750(reinterpret_cast<uint64_t>(rdx + 0x108) & 0xfffffffffffffff8, rsi, rdx);
    if (rax4) {
        fun_2710(rax4 + 0x100, rsi, rdx);
        rax5 = *reinterpret_cast<void***>(rdi + 32);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rdx) + 0x100) = 0;
        *reinterpret_cast<void***>(rax4 + 96) = rdx;
        *reinterpret_cast<void***>(rax4 + 80) = rdi;
        *reinterpret_cast<void***>(rax4 + 56) = rax5;
        *reinterpret_cast<void***>(rax4 + 64) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 24) = reinterpret_cast<void**>(0);
        *reinterpret_cast<unsigned char*>(rax4 + 0x6a) = reinterpret_cast<unsigned char>(0x30000);
        *reinterpret_cast<void***>(rax4 + 32) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 40) = reinterpret_cast<void**>(0);
    }
    return rax4;
}

void** g28;

int64_t free = 0;

void** hash_initialize(void** rdi);

struct s0 {
    signed char[8] pad8;
    void** f8;
};

struct s0* hash_lookup();

int32_t fun_2840();

void** hash_insert(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

uint32_t fun_25d0();

void fun_2480(void** rdi, void** rsi, ...);

void** filesystem_type(void** rdi, void** esi, ...) {
    void** rsi2;
    void** rsp3;
    void** r12_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** r13d8;
    int64_t r8_9;
    void** rax10;
    void** v11;
    struct s0* rax12;
    void** rax13;
    int32_t eax14;
    void** r12_15;
    void** v16;
    void** rax17;
    void** rax18;
    void** rax19;
    void* rdx20;
    int32_t eax21;
    void** v22;

    rsi2 = esi;
    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x98);
    r12_4 = *reinterpret_cast<void***>(rdi + 80);
    rax5 = g28;
    rbp6 = *reinterpret_cast<void***>(r12_4 + 80);
    if (!(*reinterpret_cast<unsigned char*>(r12_4 + 73) & 2)) 
        goto addr_4988_2;
    rbx7 = rdi;
    r13d8 = rsi2;
    if (!rbp6 && (r8_9 = free, rsi2 = reinterpret_cast<void**>(0), rdi = reinterpret_cast<void**>(13), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax10 = hash_initialize(13), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), *reinterpret_cast<void***>(r12_4 + 80) = rax10, rbp6 = rax10, !rax10) || (rsi2 = rsp3, rdi = rbp6, v11 = *reinterpret_cast<void***>(rbx7 + 0x70), rax12 = hash_lookup(), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), rax12 == 0)) {
        if (reinterpret_cast<signed char>(r13d8) < reinterpret_cast<signed char>(0)) {
            addr_4988_2:
            *reinterpret_cast<int32_t*>(&rax13) = 0;
            *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
        } else {
            rsi2 = rsp3 + 16;
            rdi = r13d8;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax14 = fun_2840();
            if (!eax14) {
                r12_15 = v16;
                if (!rbp6) {
                    addr_49f6_7:
                    rax13 = r12_15;
                } else {
                    rdi = reinterpret_cast<void**>(16);
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    rax17 = fun_2750(16, rsi2, 0x4540, 16, rsi2, 0x4540);
                    if (!rax17) 
                        goto addr_49f1_9;
                    rax18 = *reinterpret_cast<void***>(rbx7 + 0x70);
                    *reinterpret_cast<void***>(rax17 + 8) = r12_15;
                    rsi2 = rax17;
                    rdi = rbp6;
                    *reinterpret_cast<void***>(rax17) = rax18;
                    rax19 = hash_insert(rdi, rsi2, 0x4540, 0x4550, r8_9);
                    if (!rax19) 
                        goto addr_4a00_11; else 
                        goto addr_49e8_12;
                }
            } else {
                goto addr_4988_2;
            }
        }
    } else {
        rax13 = rax12->f8;
    }
    rdx20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (!rdx20) {
        return rax13;
    }
    fun_25d0();
    if (*reinterpret_cast<void***>(rdi + 44) != rsi2) 
        goto addr_4a2b_19;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) 
        goto addr_28fa_21;
    addr_4a2b_19:
    if (*reinterpret_cast<signed char*>(&rdx20)) {
        eax21 = i_ring_push(rdi + 96);
        if (eax21 < 0) {
            addr_4a39_23:
            *reinterpret_cast<void***>(rdi + 44) = rsi2;
            goto v11;
        } else {
            fun_2660();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_4a39_23;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_4a39_23;
        fun_2660();
    }
    *reinterpret_cast<void***>(rdi + 44) = rsi2;
    goto v11;
    addr_28fa_21:
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    fun_24b0(rdi);
    addr_4a00_11:
    rdi = rax17;
    fun_2480(rdi, rsi2, rdi, rsi2);
    goto addr_49f1_9;
    addr_49e8_12:
    if (rax17 != rax19) {
        fun_24b0(rdi);
        goto addr_28fa_21;
    } else {
        addr_49f1_9:
        r12_15 = v22;
        goto addr_49f6_7;
    }
}

void** fun_27a0(void** rdi);

void fun_2510();

void** fts_sort(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rsi9;
    void** r8_10;
    void** rax11;
    void** rdx12;
    void** r8_13;
    void** rax14;
    void** rdx15;
    void** rsi16;
    void** rcx17;
    void** rdx18;

    r12_5 = rdi;
    rbp6 = rdx;
    rbx7 = rsi;
    rdi8 = *reinterpret_cast<void***>(rdi + 16);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 56)) < reinterpret_cast<unsigned char>(rdx)) {
        rsi9 = rdx + 40;
        r8_10 = rdi8;
        *reinterpret_cast<void***>(r12_5 + 56) = rsi9;
        if (reinterpret_cast<unsigned char>(rsi9) >> 61) {
            addr_4802_3:
            fun_2480(r8_10, rsi9);
            *reinterpret_cast<void***>(r12_5 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_5 + 56) = reinterpret_cast<void**>(0);
            return rbx7;
        } else {
            rsi9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi9) << 3);
            rax11 = fun_27a0(rdi8);
            rdi8 = rax11;
            if (!rax11) {
                r8_10 = *reinterpret_cast<void***>(r12_5 + 16);
                goto addr_4802_3;
            } else {
                *reinterpret_cast<void***>(r12_5 + 16) = rax11;
            }
        }
    }
    rdx12 = rdi8;
    if (rbx7) {
        do {
            *reinterpret_cast<void***>(rdx12) = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdx12 = rdx12 + 8;
        } while (rbx7);
    }
    fun_2510();
    r8_13 = *reinterpret_cast<void***>(r12_5 + 16);
    rax14 = *reinterpret_cast<void***>(r8_13);
    rdx15 = r8_13;
    rsi16 = rax14;
    rcx17 = rbp6 - 1;
    if (rcx17) {
        while (rdx15 = rdx15 + 8, *reinterpret_cast<void***>(rsi16 + 16) = *reinterpret_cast<void***>(rdx15 + 8), --rcx17, !!rcx17) {
            rsi16 = *reinterpret_cast<void***>(rdx15);
        }
        rdx18 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_13 + reinterpret_cast<unsigned char>(rbp6) * 8) - 8);
    } else {
        rdx18 = rax14;
    }
    *reinterpret_cast<void***>(rdx18 + 16) = reinterpret_cast<void**>(0);
    return rax14;
}

int32_t fun_2790();

unsigned char i_ring_empty(void*** rdi, void** rsi, void** rdx);

void** i_ring_pop(void*** rdi, void** rsi, void** rdx);

uint32_t restore_initial_cwd(void** rdi, void** rsi) {
    void** eax3;
    uint32_t r12d4;
    int32_t eax5;
    void*** rbx6;
    unsigned char al7;
    void** eax8;

    eax3 = *reinterpret_cast<void***>(rdi + 72);
    r12d4 = reinterpret_cast<unsigned char>(eax3) & 4;
    if (r12d4) {
        r12d4 = 0;
    } else {
        if (!(*reinterpret_cast<unsigned char*>(&eax3 + 1) & 2)) {
            r12d4 = 0;
            eax5 = fun_2790();
            *reinterpret_cast<unsigned char*>(&r12d4) = reinterpret_cast<uint1_t>(!!eax5);
        } else {
            rsi = reinterpret_cast<void**>(0xffffff9c);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            cwd_advance_fd(rdi, 0xffffff9c, 1);
        }
    }
    rbx6 = reinterpret_cast<void***>(rdi + 96);
    while (al7 = i_ring_empty(rbx6, rsi, 1), al7 == 0) {
        eax8 = i_ring_pop(rbx6, rsi, 1);
        if (reinterpret_cast<signed char>(eax8) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_2660();
    }
    return r12d4;
}

void** fun_24c0();

unsigned char fts_palloc(void** rdi, void** rsi, void** rdx) {
    void** rsi4;
    void** rdi5;
    void** tmp64_6;
    void** rax7;
    void** rax8;
    void** rdi9;

    rsi4 = rsi + 0x100;
    rdi5 = *reinterpret_cast<void***>(rdi + 32);
    tmp64_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48)));
    if (reinterpret_cast<unsigned char>(tmp64_6) < reinterpret_cast<unsigned char>(rsi4)) {
        fun_2480(rdi5, tmp64_6);
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
        rax7 = fun_24c0();
        *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(36);
        return 0;
    } else {
        *reinterpret_cast<void***>(rdi + 48) = tmp64_6;
        rax8 = fun_27a0(rdi5);
        if (!rax8) {
            rdi9 = *reinterpret_cast<void***>(rdi + 32);
            fun_2480(rdi9, tmp64_6);
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
            return 0;
        } else {
            *reinterpret_cast<void***>(rdi + 32) = rax8;
            return 1;
        }
    }
}

void cycle_check_init(void** rdi);

unsigned char setup_dir(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rax4 = fun_2750(32, rsi, rdx);
        *reinterpret_cast<void***>(rdi + 88) = rax4;
        if (!rax4) {
            return 0;
        } else {
            cycle_check_init(rax4);
            return 1;
        }
    } else {
        rax5 = hash_initialize(31);
        *reinterpret_cast<void***>(rdi + 88) = rax5;
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rax5));
    }
}

void** hash_remove(void** rdi, void** rsi);

void leave_dir(void** rdi, void** rsi, ...) {
    void** rax3;
    void** rdi4;
    void** v5;
    void** rax6;
    void** rdx7;
    void** rax8;
    void* rax9;
    void** eax10;
    void*** rbx11;
    unsigned char al12;
    void** eax13;

    rax3 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102) {
        rdi4 = *reinterpret_cast<void***>(rdi + 88);
        v5 = *reinterpret_cast<void***>(rsi + 0x70);
        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 40);
        rax6 = hash_remove(rdi4, rsi);
        rdi = rax6;
        if (!rax6) {
            addr_28ff_3:
            fun_24b0(rdi, rdi);
        } else {
            fun_2480(rdi, rsi);
            goto addr_4c58_5;
        }
    } else {
        if (*reinterpret_cast<void***>(rsi + 8) && reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 88)) >= reinterpret_cast<signed char>(0)) {
            rdx7 = *reinterpret_cast<void***>(rdi + 88);
            if (!*reinterpret_cast<void***>(rdx7 + 16)) 
                goto addr_28ff_3;
            if (*reinterpret_cast<void***>(rdx7) == *reinterpret_cast<void***>(rsi + 0x78)) {
                if (*reinterpret_cast<void***>(rdx7 + 8) == *reinterpret_cast<void***>(rsi + 0x70)) {
                    rax8 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x78);
                    *reinterpret_cast<void***>(rdx7 + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x70);
                    *reinterpret_cast<void***>(rdx7) = rax8;
                    goto addr_4c58_5;
                }
            } else {
                goto addr_4c58_5;
            }
        }
    }
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    fun_24b0(rdi, rdi);
    addr_4c58_5:
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (!rax9) {
        return;
    }
    fun_25d0();
    eax10 = *reinterpret_cast<void***>(rdi + 72);
    if (!(reinterpret_cast<unsigned char>(eax10) & 4)) 
        goto addr_4cd6_36;
    addr_4cf3_38:
    rbx11 = reinterpret_cast<void***>(rdi + 96);
    while (al12 = i_ring_empty(rbx11, rsi, rdx7), al12 == 0) {
        eax13 = i_ring_pop(rbx11, rsi, rdx7);
        if (reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_2660();
    }
    goto v5;
    addr_4cd6_36:
    if (!(*reinterpret_cast<unsigned char*>(&eax10 + 1) & 2)) {
        fun_2790();
        goto addr_4cf3_38;
    } else {
        *reinterpret_cast<int32_t*>(&rdx7) = 1;
        *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
        rsi = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        cwd_advance_fd(rdi, 0xffffff9c, 1);
        goto addr_4cf3_38;
    }
}

int32_t fun_28c0();

void** openat_safer(int64_t rdi, void** rsi);

void** open_safer(void** rdi, int64_t rsi);

int32_t fts_safe_changedir(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r14d6;
    void** r12_7;
    void** rbx8;
    void** rax9;
    void** v10;
    uint32_t eax11;
    void** ebp12;
    unsigned char v13;
    uint32_t eax14;
    void** r13d15;
    void* rax16;
    uint32_t eax17;
    int32_t eax18;
    int32_t eax19;
    void** v20;
    void** v21;
    void** rax22;
    void** rax23;
    int64_t rdi24;
    void** eax25;
    int64_t rsi26;
    void** eax27;
    void*** r13_28;
    unsigned char al29;
    void** eax30;

    r15_5 = rdi;
    r14d6 = rdx;
    r12_7 = rsi;
    rbx8 = rcx;
    rax9 = g28;
    v10 = rax9;
    if (!rcx || ((eax11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)), eax11 != 46) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rcx + 1) == 46) || *reinterpret_cast<signed char*>(rcx + 2)))) {
        ebp12 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp12) & 4) {
            addr_4ed0_3:
            ebp12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp12) & 0x200);
            if (!ebp12 || reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
            } else {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                fun_2660();
            }
        } else {
            if (reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                v13 = 0;
                eax14 = reinterpret_cast<unsigned char>(ebp12) & 0x200;
                goto addr_4fec_8;
            } else {
                v13 = 0;
                r13d15 = r14d6;
                if (*reinterpret_cast<unsigned char*>(&ebp12) & 2) 
                    goto addr_4e98_10; else 
                    goto addr_4dba_11;
            }
        }
    } else {
        ebp12 = *reinterpret_cast<void***>(rdi + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp12) & 4) 
            goto addr_4ed0_3;
        if (reinterpret_cast<signed char>(rdx) >= reinterpret_cast<signed char>(0)) 
            goto addr_5020_14;
        if (reinterpret_cast<unsigned char>(ebp12) & 0x200) 
            goto addr_4f50_16; else 
            goto addr_4e55_17;
    }
    addr_4df0_18:
    while (rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28)), !!rax16) {
        eax11 = fun_25d0();
        addr_5020_14:
        v13 = 1;
        r13d15 = rdx;
        if (*reinterpret_cast<unsigned char*>(&ebp12) & 2) {
            goto addr_4e98_10;
        }
        addr_4dc2_21:
        if (eax11 != 46 || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx8 + 1) == 46)) {
            addr_4dcb_22:
            ebp12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp12) & 0x200);
            if (ebp12) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                eax17 = static_cast<uint32_t>(v13) ^ 1;
                rdx = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&eax17)));
                cwd_advance_fd(r15_5, r13d15, *reinterpret_cast<signed char*>(&rdx));
                continue;
            } else {
                eax18 = fun_2790();
                *reinterpret_cast<int32_t*>(&r12_7) = eax18;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) {
                    continue;
                }
            }
        } else {
            if (!*reinterpret_cast<signed char*>(rbx8 + 2)) {
                addr_4e98_10:
                eax19 = fun_28c0();
                if (eax19) {
                    addr_4ef3_27:
                    *reinterpret_cast<int32_t*>(&r12_7) = -1;
                    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                    if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) 
                        continue;
                } else {
                    if (*reinterpret_cast<void***>(r12_7 + 0x70) != v20 || *reinterpret_cast<void***>(r12_7 + 0x78) != v21) {
                        rax22 = fun_24c0();
                        *reinterpret_cast<void***>(rax22) = reinterpret_cast<void**>(2);
                        goto addr_4ef3_27;
                    } else {
                        ebp12 = *reinterpret_cast<void***>(r15_5 + 72);
                        goto addr_4dcb_22;
                    }
                }
            } else {
                goto addr_4dcb_22;
            }
        }
        rax23 = fun_24c0();
        ebp12 = *reinterpret_cast<void***>(rax23);
        rbx8 = rax23;
        fun_2660();
        *reinterpret_cast<void***>(rbx8) = ebp12;
    }
    return *reinterpret_cast<int32_t*>(&r12_7);
    addr_4fec_8:
    *reinterpret_cast<void***>(&rdi24) = *reinterpret_cast<void***>(r15_5 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp12) << 13 & 0x20000 | 0x90900);
    if (eax14) {
        addr_4f79_34:
        eax25 = openat_safer(rdi24, rbx8);
        r13d15 = eax25;
    } else {
        goto addr_4e6b_36;
    }
    addr_4e7a_37:
    if (reinterpret_cast<signed char>(r13d15) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<int32_t*>(&r12_7) = -1;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        goto addr_4df0_18;
    } else {
        ebp12 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp12) & 2) {
            goto addr_4e98_10;
        }
    }
    addr_4dba_11:
    if (!rbx8) 
        goto addr_4dcb_22;
    eax11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8));
    goto addr_4dc2_21;
    addr_4e6b_36:
    *reinterpret_cast<void***>(&rsi26) = rdx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi26) + 4) = 0;
    eax27 = open_safer(rbx8, rsi26);
    r13d15 = eax27;
    goto addr_4e7a_37;
    addr_4f50_16:
    r13_28 = reinterpret_cast<void***>(rdi + 96);
    al29 = i_ring_empty(r13_28, rsi, rdx);
    v13 = al29;
    if (!al29) {
        eax30 = i_ring_pop(r13_28, rsi, rdx);
        ebp12 = *reinterpret_cast<void***>(r15_5 + 72);
        r13d15 = eax30;
        if (reinterpret_cast<signed char>(eax30) < reinterpret_cast<signed char>(0)) {
            v13 = 1;
            eax14 = reinterpret_cast<unsigned char>(ebp12) & 0x200;
            goto addr_4fec_8;
        } else {
            v13 = 1;
            r14d6 = eax30;
            if (!(*reinterpret_cast<unsigned char*>(&ebp12) & 2)) 
                goto addr_4dcb_22;
            goto addr_4e98_10;
        }
    } else {
        *reinterpret_cast<void***>(&rdi24) = *reinterpret_cast<void***>(r15_5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp12) << 13 & 0x20000 | 0x90900);
        goto addr_4f79_34;
    }
    addr_4e55_17:
    v13 = 1;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp12) << 13 & 0x20000 | 0x90900);
    goto addr_4e6b_36;
}

void** opendirat(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

void** fun_26e0(void** rdi);

void fun_2680(void** rdi, void** rsi, ...);

void** fts_stat(void** rdi, void** rsi, signed char dl, ...);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void** rpl_fcntl();

struct s1 {
    signed char f0;
    void** f1;
};

struct s2 {
    void** f0;
    signed char[17] pad18;
    unsigned char f12;
    void** f13;
    signed char f14;
};

struct s2* fun_2740(void** rdi, void** rsi);

void fun_27d0(void** rdi, void** rsi, void** rdx, ...);

void** fts_build(void** rdi, void** rsi) {
    void** r14_3;
    void** rbp4;
    void** v5;
    void** rax6;
    void** v7;
    void** rax8;
    void** v9;
    void** eax10;
    int64_t rdx11;
    uint32_t edx12;
    int64_t rdi13;
    void** rcx14;
    void** rax15;
    void** r13_16;
    void** eax17;
    void** v18;
    void** rdi19;
    void** rax20;
    void** rax21;
    void* rax22;
    int64_t r8_23;
    uint64_t rax24;
    void* rax25;
    void** v26;
    void** rax27;
    void** edi28;
    void** v29;
    int32_t r12d30;
    int32_t ebx31;
    void** rax32;
    void** eax33;
    void** rdx34;
    int32_t eax35;
    void** rax36;
    void** rdi37;
    void** edx38;
    unsigned char v39;
    void** rcx40;
    void** v41;
    void** v42;
    void** v43;
    struct s1* rax44;
    void** r15_45;
    void** rax46;
    int1_t zf47;
    void** v48;
    void** v49;
    signed char v50;
    void** r12_51;
    void** rax52;
    void** rdi53;
    void** v54;
    void** rbx55;
    unsigned char v56;
    void** v57;
    void** v58;
    void** v59;
    struct s2* rax60;
    void** rax61;
    void** r14_62;
    void** rax63;
    void** rsi64;
    void** rax65;
    void** r15_66;
    uint32_t eax67;
    void** tmp64_68;
    void** rsi69;
    void** rax70;
    void** edx71;
    void** rdx72;
    void** eax73;
    int64_t rax74;
    int64_t rdx75;
    void** eax76;
    void** rax77;
    void** rax78;
    uint32_t eax79;
    int32_t eax80;
    void** rax81;
    void** rax82;
    uint32_t eax83;
    void** rbp84;
    void** rdi85;
    void** rbp86;
    void** rdi87;
    uint64_t rax88;
    uint32_t eax89;
    void** rdi90;
    void** rax91;
    void** rax92;
    void** rdx93;
    void** r13_94;
    void** r14_95;
    void** rbp96;
    void** ebx97;
    void** r12_98;
    void** rdi99;
    void** rdi100;
    void** r13_101;
    void** rbp102;
    void** r14_103;
    void** r12_104;
    void** rdi105;
    void** rdi106;
    void** v107;
    void** v108;

    r14_3 = rdi;
    rbp4 = *reinterpret_cast<void***>(rdi);
    v5 = rsi;
    rax6 = g28;
    v7 = rax6;
    rax8 = *reinterpret_cast<void***>(rbp4 + 24);
    v9 = rax8;
    if (!rax8) {
        eax10 = *reinterpret_cast<void***>(rdi + 72);
        *reinterpret_cast<uint32_t*>(&rdx11) = reinterpret_cast<unsigned char>(eax10) & 16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rdx11) && (*reinterpret_cast<uint32_t*>(&rdx11) = 0x20000, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(&eax10) & 1))) {
            edx12 = 0;
            *reinterpret_cast<unsigned char*>(&edx12) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbp4 + 88));
            *reinterpret_cast<uint32_t*>(&rdx11) = edx12 << 17;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        }
        rsi = *reinterpret_cast<void***>(rbp4 + 48);
        *reinterpret_cast<void***>(&rdi13) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        if ((reinterpret_cast<unsigned char>(eax10) & 0x204) == 0x200) {
            *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(r14_3 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        }
        rcx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78 + 100);
        rax15 = opendirat(rdi13, rsi, rdx11, rcx14);
        *reinterpret_cast<void***>(rbp4 + 24) = rax15;
        r13_16 = rax15;
        if (rax15) 
            goto addr_553a_7;
    } else {
        eax17 = fun_26e0(rax8);
        v18 = eax17;
        if (reinterpret_cast<signed char>(eax17) < reinterpret_cast<signed char>(0)) {
            rdi19 = *reinterpret_cast<void***>(rbp4 + 24);
            fun_2680(rdi19, rsi);
            *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
            if (reinterpret_cast<int1_t>(v5 == 3)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
                rax20 = fun_24c0();
                *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax20);
                goto addr_588a_11;
            }
        }
        if (!*reinterpret_cast<void***>(r14_3 + 64)) 
            goto addr_5930_13; else 
            goto addr_50a9_14;
    }
    if (!reinterpret_cast<int1_t>(v5 == 3)) {
        addr_588a_11:
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    } else {
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
        rax21 = fun_24c0();
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax21);
    }
    addr_56a1_17:
    rax22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (rax22) {
        fun_25d0();
    } else {
        return r13_16;
    }
    addr_553a_7:
    if (*reinterpret_cast<uint16_t*>(rbp4 + 0x68) == 11) {
        rsi = rbp4;
        rax15 = fts_stat(r14_3, rsi, 0, r14_3, rsi, 0);
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&rax15);
        goto addr_5550_22;
    }
    if (!(*reinterpret_cast<unsigned char*>(r14_3 + 73) & 1) || (leave_dir(r14_3, rbp4, r14_3, rbp4), fts_stat(r14_3, rbp4, 0, r14_3, rbp4, 0), rsi = rbp4, rax15 = enter_dir(r14_3, rsi, 0, rcx14, r8_23), !!*reinterpret_cast<signed char*>(&rax15))) {
        addr_5550_22:
        rax24 = reinterpret_cast<unsigned char>(rax15) - (reinterpret_cast<unsigned char>(rax15) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax15) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax15) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 64)) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax25) = *reinterpret_cast<uint32_t*>(&rax24) & 0x186a1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax25) + 4) = 0;
        v26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax25) - 1);
        if (v5 == 2) 
            goto addr_58e0_24;
    } else {
        rax27 = fun_24c0();
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
        *reinterpret_cast<void***>(rax27) = reinterpret_cast<void**>(12);
        goto addr_56a1_17;
    }
    edi28 = v29;
    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 56) != 24 || *reinterpret_cast<int64_t*>(rbp4 + 0x80) != 2) {
        addr_5585_27:
        r12d30 = 1;
        *reinterpret_cast<unsigned char*>(&ebx31) = reinterpret_cast<uint1_t>(v5 == 3);
    } else {
        rsi = edi28;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rax32 = filesystem_type(rbp4, rsi, rbp4, rsi);
        if (rax32 == 0x9fa0) 
            goto addr_5a9a_29;
        if (reinterpret_cast<signed char>(rax32) > reinterpret_cast<signed char>(0x9fa0)) 
            goto addr_5a84_31; else 
            goto addr_570c_32;
    }
    addr_5593_33:
    if (*reinterpret_cast<unsigned char*>(r14_3 + 73) & 2) {
        rsi = reinterpret_cast<void**>(0x406);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax33 = rpl_fcntl();
        v18 = eax33;
        edi28 = eax33;
    }
    if (reinterpret_cast<signed char>(edi28) < reinterpret_cast<signed char>(0) || (rdx34 = edi28, *reinterpret_cast<int32_t*>(&rdx34 + 4) = 0, rsi = rbp4, eax35 = fts_safe_changedir(r14_3, rsi, rdx34, 0), !!eax35)) {
        if (*reinterpret_cast<unsigned char*>(&ebx31) && *reinterpret_cast<signed char*>(&r12d30)) {
            rax36 = fun_24c0();
            *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax36);
        }
        *reinterpret_cast<unsigned char*>(rbp4 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(rbp4 + 0x6a) | 1);
        rdi37 = *reinterpret_cast<void***>(rbp4 + 24);
        fun_2680(rdi37, rsi, rdi37, rsi);
        edx38 = *reinterpret_cast<void***>(r14_3 + 72);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        if (*reinterpret_cast<unsigned char*>(&edx38 + 1) & 2 && reinterpret_cast<signed char>(v18) >= reinterpret_cast<signed char>(0)) {
            fun_2660();
            edx38 = *reinterpret_cast<void***>(r14_3 + 72);
        }
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        v39 = 0;
    } else {
        goto addr_50b2_42;
    }
    addr_50bb_43:
    rcx40 = *reinterpret_cast<void***>(rbp4 + 72);
    v41 = rcx40;
    v42 = rcx40 + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 56)) + reinterpret_cast<unsigned char>(rcx40) + 0xffffffffffffffff) != 47) {
        v42 = rcx40;
        v41 = rcx40 + 1;
    }
    v43 = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(edx38) & 4) {
        rax44 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 32)));
        rcx40 = reinterpret_cast<void**>(&rax44->f1);
        rax44->f0 = 47;
        v43 = rcx40;
    }
    r15_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 48)) - reinterpret_cast<unsigned char>(v41));
    rax46 = *reinterpret_cast<void***>(rbp4 + 88) + 1;
    zf47 = *reinterpret_cast<void***>(rbp4 + 24) == 0;
    v48 = *reinterpret_cast<void***>(rbp4 + 24);
    v49 = rax46;
    if (zf47) {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4)) {
            *reinterpret_cast<int32_t*>(&r13_16) = 0;
            *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
            if (!(v39 & static_cast<unsigned char>(reinterpret_cast<uint1_t>(v9 == 0)))) 
                goto addr_5855_50;
            v50 = 0;
            *reinterpret_cast<int32_t*>(&r12_51) = 0;
            *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        } else {
            v50 = 0;
            *reinterpret_cast<int32_t*>(&r13_16) = 0;
            *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_51) = 0;
            *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
            goto addr_57ab_53;
        }
    } else {
        rax52 = fun_24c0();
        rdi53 = v48;
        *reinterpret_cast<int32_t*>(&r12_51) = 0;
        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        v54 = rax52;
        rbx55 = r14_3;
        v50 = 0;
        v56 = 0;
        v57 = reinterpret_cast<void**>(0);
        v58 = rbp4;
        v59 = reinterpret_cast<void**>(0);
        while (*reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(0), rax60 = fun_2740(rdi53, rsi), !!rax60) {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 72)) & 32) 
                goto addr_51c2_57;
            if (*reinterpret_cast<void***>(&rax60->f13) != 46) 
                goto addr_51c2_57;
            if (!rax60->f14) {
                addr_5184_60:
                rax61 = v58;
                rdi53 = *reinterpret_cast<void***>(rax61 + 24);
                if (!rdi53) 
                    goto addr_52d8_61; else 
                    continue;
            } else {
                if (rax60->f14 != 46) {
                    addr_51c2_57:
                    r14_62 = reinterpret_cast<void**>(&rax60->f13);
                    rax63 = fun_25b0(r14_62, r14_62);
                    rsi64 = r14_62;
                    rax65 = fts_alloc(rbx55, rsi64, rax63);
                    if (!rax65) 
                        goto addr_5628_63;
                } else {
                    goto addr_5184_60;
                }
            }
            if (reinterpret_cast<unsigned char>(rax63) >= reinterpret_cast<unsigned char>(r15_45)) {
                r15_66 = *reinterpret_cast<void***>(rbx55 + 32);
                rsi64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(rax63) + 2);
                *reinterpret_cast<unsigned char*>(&eax67) = fts_palloc(rbx55, rsi64, rax63);
                if (!*reinterpret_cast<unsigned char*>(&eax67)) 
                    goto addr_5628_63;
                rsi64 = *reinterpret_cast<void***>(rbx55 + 32);
                if (rsi64 != r15_66) 
                    goto addr_53ab_68;
            } else {
                addr_51f4_69:
                tmp64_68 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax63) + reinterpret_cast<unsigned char>(v41));
                if (reinterpret_cast<unsigned char>(tmp64_68) < reinterpret_cast<unsigned char>(rax63)) 
                    goto addr_5aa3_70; else 
                    goto addr_5202_71;
            }
            eax67 = v56;
            addr_53bf_73:
            r15_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 48)) - reinterpret_cast<unsigned char>(v41));
            v56 = *reinterpret_cast<unsigned char*>(&eax67);
            goto addr_51f4_69;
            addr_53ab_68:
            rsi64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi64) + reinterpret_cast<unsigned char>(v41));
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 72)) & 4)) {
                rsi64 = v43;
            }
            v43 = rsi64;
            goto addr_53bf_73;
            addr_5202_71:
            rsi69 = rax65 + 0x100;
            *reinterpret_cast<void***>(rax65 + 88) = v49;
            rax70 = *reinterpret_cast<void***>(rbx55);
            *reinterpret_cast<void***>(rax65 + 72) = tmp64_68;
            edx71 = *reinterpret_cast<void***>(rbx55 + 72);
            *reinterpret_cast<void***>(rax65 + 8) = rax70;
            *reinterpret_cast<void***>(rax65 + 0x78) = rax60->f0;
            if (*reinterpret_cast<unsigned char*>(&edx71) & 4) {
                *reinterpret_cast<void***>(rax65 + 48) = *reinterpret_cast<void***>(rax65 + 56);
                rdx72 = *reinterpret_cast<void***>(rax65 + 96) + 1;
                fun_27d0(v43, rsi69, rdx72);
                edx71 = *reinterpret_cast<void***>(rbx55 + 72);
            } else {
                *reinterpret_cast<void***>(rax65 + 48) = rsi69;
            }
            if (!*reinterpret_cast<void***>(rbx55 + 64) || *reinterpret_cast<unsigned char*>(&edx71 + 1) & 4) {
                eax73 = reinterpret_cast<void**>(static_cast<uint32_t>(rax60->f12));
                rsi = eax73;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                *reinterpret_cast<void***>(&rax74) = eax73 - 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                if (!(*reinterpret_cast<unsigned char*>(&edx71) & 8) || !(*reinterpret_cast<unsigned char*>(&rsi) & 0xfb)) {
                    rsi = reinterpret_cast<void**>(11);
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax74)) <= reinterpret_cast<unsigned char>(11)) {
                        addr_53e8_81:
                        *reinterpret_cast<int32_t*>(&rdx75) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
                    } else {
                        eax76 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx75) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
                        goto addr_5278_83;
                    }
                } else {
                    if (!(reinterpret_cast<unsigned char>(edx71) & 16) && *reinterpret_cast<unsigned char*>(&rsi) == 10) {
                        *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                        goto addr_53e8_81;
                    }
                    *reinterpret_cast<int32_t*>(&rcx40) = 11;
                    *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax74)) <= reinterpret_cast<unsigned char>(11)) 
                        goto addr_5600_87; else 
                        goto addr_5492_88;
                }
            } else {
                rsi = rax65;
                rax77 = fts_stat(rbx55, rsi, 0);
                *reinterpret_cast<uint16_t*>(rax65 + 0x68) = *reinterpret_cast<uint16_t*>(&rax77);
                goto addr_5286_90;
            }
            addr_53ed_91:
            rcx40 = reinterpret_cast<void**>(0xcec0);
            eax76 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0xcec0) + reinterpret_cast<uint64_t>(rax74 * 4));
            addr_5278_83:
            *reinterpret_cast<void***>(rax65 + 0x88) = eax76;
            *reinterpret_cast<int64_t*>(rax65 + 0xa0) = rdx75;
            addr_5286_90:
            *reinterpret_cast<void***>(rax65 + 16) = reinterpret_cast<void**>(0);
            if (!v59) {
                v59 = rax65;
            } else {
                *reinterpret_cast<void***>(v57 + 16) = rax65;
            }
            if (!reinterpret_cast<int1_t>(r12_51 == fun_2710)) {
                ++r12_51;
                if (reinterpret_cast<unsigned char>(r12_51) >= reinterpret_cast<unsigned char>(v26)) 
                    goto addr_5898_96;
                v57 = rax65;
                goto addr_5184_60;
            } else {
                if (!*reinterpret_cast<void***>(rbx55 + 64)) {
                    rsi = v18;
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    rax78 = filesystem_type(v58, rsi);
                    if (rax78 == 0x1021994 || ((*reinterpret_cast<int32_t*>(&rcx40) = 0xff534d42, *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0, rax78 == 0xff534d42) || rax78 == 0x6969)) {
                        v57 = rax65;
                        *reinterpret_cast<int32_t*>(&r12_51) = 0x2711;
                        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
                        v50 = 0;
                        goto addr_5184_60;
                    } else {
                        v50 = 1;
                        goto addr_52bb_102;
                    }
                } else {
                    addr_52bb_102:
                    rax61 = v58;
                    v57 = rax65;
                    *reinterpret_cast<int32_t*>(&r12_51) = 0x2711;
                    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
                    rdi53 = *reinterpret_cast<void***>(rax61 + 24);
                    if (rdi53) 
                        continue; else 
                        goto addr_52d8_61;
                }
            }
            addr_5600_87:
            *reinterpret_cast<int32_t*>(&rdx75) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            goto addr_53ed_91;
            addr_5492_88:
            eax76 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx75) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            goto addr_5278_83;
        }
        goto addr_5740_103;
    }
    addr_5830_104:
    if (!*reinterpret_cast<void***>(rbp4 + 88)) {
        eax79 = restore_initial_cwd(r14_3, rsi);
        if (eax79) 
            goto addr_597d_106;
        goto addr_5850_108;
    }
    rsi = *reinterpret_cast<void***>(rbp4 + 8);
    rcx40 = reinterpret_cast<void**>("..");
    eax80 = fts_safe_changedir(r14_3, rsi, 0xffffffff, "..");
    if (!eax80) {
        addr_5850_108:
        if (r12_51) {
            addr_57dc_110:
            if (v50) {
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0x4560);
                rax81 = fts_sort(r14_3, r13_16, r12_51, rcx40);
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0);
                r13_16 = rax81;
                goto addr_56a1_17;
            } else {
                if (*reinterpret_cast<void***>(r14_3 + 64) && r12_51 != 1) {
                    rax82 = fts_sort(r14_3, r13_16, r12_51, rcx40);
                    r13_16 = rax82;
                    goto addr_56a1_17;
                }
            }
        } else {
            addr_5855_50:
            if (v5 == 3 && ((eax83 = *reinterpret_cast<uint16_t*>(rbp4 + 0x68), *reinterpret_cast<int16_t*>(&eax83) != 7) && *reinterpret_cast<int16_t*>(&eax83) != 4)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 6;
            }
        }
    } else {
        addr_597d_106:
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 7;
        *reinterpret_cast<void***>(r14_3 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) | 0x2000);
        if (r13_16) {
            do {
                rbp84 = r13_16;
                r13_16 = *reinterpret_cast<void***>(r13_16 + 16);
                rdi85 = *reinterpret_cast<void***>(rbp84 + 24);
                if (rdi85) {
                    fun_2680(rdi85, rsi, rdi85, rsi);
                }
                fun_2480(rbp84, rsi, rbp84, rsi);
            } while (r13_16);
            goto addr_588a_11;
        }
    }
    if (r13_16) {
        do {
            rbp86 = r13_16;
            r13_16 = *reinterpret_cast<void***>(r13_16 + 16);
            rdi87 = *reinterpret_cast<void***>(rbp86 + 24);
            if (rdi87) {
                fun_2680(rdi87, rsi, rdi87, rsi);
            }
            fun_2480(rbp86, rsi, rbp86, rsi);
        } while (r13_16);
        goto addr_588a_11;
    }
    addr_5740_103:
    rbp4 = v58;
    r14_3 = rbx55;
    r13_16 = v59;
    if (*reinterpret_cast<void***>(v54)) {
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(v54);
        rax88 = reinterpret_cast<unsigned char>(v9) | reinterpret_cast<unsigned char>(r12_51);
        eax89 = (*reinterpret_cast<uint32_t*>(&rax88) - (*reinterpret_cast<uint32_t*>(&rax88) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax88) < *reinterpret_cast<uint32_t*>(&rax88) + reinterpret_cast<uint1_t>(rax88 < 1))) & 0xfffffffd) + 7;
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&eax89);
    }
    rdi90 = *reinterpret_cast<void***>(rbp4 + 24);
    if (rdi90) {
        fun_2680(rdi90, rsi);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
    }
    addr_5789_128:
    if (v56) {
        addr_52ee_129:
        rax91 = *reinterpret_cast<void***>(r14_3 + 8);
        rcx40 = *reinterpret_cast<void***>(r14_3 + 32);
        if (rax91) {
            do {
                rsi = rax91 + 0x100;
                if (*reinterpret_cast<void***>(rax91 + 48) != rsi) {
                    *reinterpret_cast<void***>(rax91 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax91 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax91 + 56))) + reinterpret_cast<unsigned char>(rcx40));
                }
                *reinterpret_cast<void***>(rax91 + 56) = rcx40;
                rax91 = *reinterpret_cast<void***>(rax91 + 16);
            } while (rax91);
        }
    } else {
        addr_5794_134:
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4) {
            if (*reinterpret_cast<void***>(r14_3 + 48) == v41 || !r12_51) {
                addr_57ab_53:
                --v43;
                goto addr_57b1_136;
            } else {
                addr_57b1_136:
                *reinterpret_cast<void***>(v43) = reinterpret_cast<void**>(0);
                goto addr_57b9_137;
            }
        }
    }
    rax92 = r13_16;
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r13_16 + 88)) >= reinterpret_cast<signed char>(0)) {
        do {
            rsi = rax92 + 0x100;
            if (*reinterpret_cast<void***>(rax92 + 48) != rsi) {
                *reinterpret_cast<void***>(rax92 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax92 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax92 + 56))) + reinterpret_cast<unsigned char>(rcx40));
            }
            rdx93 = *reinterpret_cast<void***>(rax92 + 16);
            *reinterpret_cast<void***>(rax92 + 56) = rcx40;
            if (rdx93) {
                rax92 = rdx93;
            } else {
                rax92 = *reinterpret_cast<void***>(rax92 + 8);
            }
        } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax92 + 88)) >= reinterpret_cast<signed char>(0));
        goto addr_5794_134;
    } else {
        goto addr_5794_134;
    }
    addr_57b9_137:
    if (v9) 
        goto addr_5850_108;
    if (!v39) 
        goto addr_5850_108;
    if (v5 == 1) 
        goto addr_5830_104;
    if (!r12_51) 
        goto addr_5830_104; else 
        goto addr_57dc_110;
    addr_5628_63:
    r13_94 = v59;
    r14_95 = rbx55;
    rbp96 = v58;
    ebx97 = *reinterpret_cast<void***>(v54);
    fun_2480(rax65, rsi64, rax65, rsi64);
    if (r13_94) {
        do {
            r12_98 = r13_94;
            r13_94 = *reinterpret_cast<void***>(r13_94 + 16);
            rdi99 = *reinterpret_cast<void***>(r12_98 + 24);
            if (rdi99) {
                fun_2680(rdi99, rsi64, rdi99, rsi64);
            }
            fun_2480(r12_98, rsi64, r12_98, rsi64);
        } while (r13_94);
    }
    rdi100 = *reinterpret_cast<void***>(rbp96 + 24);
    *reinterpret_cast<int32_t*>(&r13_16) = 0;
    *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    fun_2680(rdi100, rsi64, rdi100, rsi64);
    *reinterpret_cast<void***>(rbp96 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp96 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_95 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_95 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v54) = ebx97;
    goto addr_56a1_17;
    addr_5aa3_70:
    r13_101 = v59;
    rbp102 = v58;
    r14_103 = rbx55;
    fun_2480(rax65, rsi64);
    if (r13_101) {
        do {
            r12_104 = r13_101;
            r13_101 = *reinterpret_cast<void***>(r13_101 + 16);
            rdi105 = *reinterpret_cast<void***>(r12_104 + 24);
            if (rdi105) {
                fun_2680(rdi105, rsi64);
            }
            fun_2480(r12_104, rsi64);
        } while (r13_101);
    }
    rdi106 = *reinterpret_cast<void***>(rbp102 + 24);
    *reinterpret_cast<int32_t*>(&r13_16) = 0;
    *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    fun_2680(rdi106, rsi64);
    *reinterpret_cast<void***>(rbp102 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp102 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_103 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_103 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(36);
    goto addr_56a1_17;
    addr_5898_96:
    rbp4 = v58;
    r13_16 = v59;
    r14_3 = rbx55;
    goto addr_5789_128;
    addr_52d8_61:
    r13_16 = v59;
    rbp4 = rax61;
    r14_3 = rbx55;
    if (!v56) 
        goto addr_5794_134; else 
        goto addr_52ee_129;
    addr_50b2_42:
    v39 = 1;
    edx38 = *reinterpret_cast<void***>(r14_3 + 72);
    goto addr_50bb_43;
    addr_5a84_31:
    if (rax32 == 0x5346414f || reinterpret_cast<int1_t>(rax32 == 0xff534d42)) {
        addr_5a9a_29:
        edi28 = v107;
        goto addr_5585_27;
    } else {
        addr_5721_158:
        if (!reinterpret_cast<int1_t>(v5 == 3)) {
            addr_58e0_24:
            v39 = 0;
            edx38 = *reinterpret_cast<void***>(r14_3 + 72);
            goto addr_50bb_43;
        } else {
            edi28 = v108;
            r12d30 = 0;
            ebx31 = 1;
            goto addr_5593_33;
        }
    }
    addr_570c_32:
    if (!rax32) 
        goto addr_5a9a_29;
    if (rax32 == 0x6969) 
        goto addr_5a9a_29; else 
        goto addr_5721_158;
    addr_5930_13:
    v26 = reinterpret_cast<void**>(0x186a0);
    edx38 = *reinterpret_cast<void***>(r14_3 + 72);
    v39 = 1;
    goto addr_50bb_43;
    addr_50a9_14:
    v26 = reinterpret_cast<void**>(0xffffffffffffffff);
    goto addr_50b2_42;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0xcf60);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0xcf60);
    if (*reinterpret_cast<void***>(rdi + 40) != 0xcf60) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x6458]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0xcf60);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x62bf]");
        if (1) 
            goto addr_6dac_6;
        __asm__("comiss xmm1, [rip+0x62b6]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x62a8]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_6d8c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_6d82_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_6d8c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_6d82_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_6d82_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_6d8c_13:
        return 0;
    } else {
        addr_6dac_6:
        return r8_3;
    }
}

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s4 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int32_t transfer_entries(void** rdi, void** rsi, int32_t edx) {
    void** rdx3;
    void** r14_4;
    int32_t r12d5;
    void** rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rdi12;
    void** rax13;
    struct s3* rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s4* r13_18;
    void** rax19;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = *reinterpret_cast<void***>(rsi);
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8))) {
        do {
            addr_6e16_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_6e08_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_6e16_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = *reinterpret_cast<void***>(r14_4 + 16);
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rdi12 = r15_11, rax13 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi10)), rsi10 = *reinterpret_cast<void***>(r14_4 + 16), reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax14 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax14->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14->f8;
                            rax14->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_6e8e_9;
                        } else {
                            rax14->f0 = r15_11;
                            rax15 = *reinterpret_cast<void***>(r14_4 + 72);
                            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            *reinterpret_cast<void***>(r14_4 + 72) = r13_9;
                            if (!rdx3) 
                                goto addr_6e8e_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_290e_12;
                    addr_6e8e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_6e08_3;
            }
            rsi16 = *reinterpret_cast<void***>(r14_4 + 16);
            rdi12 = r15_8;
            rax17 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 16))) 
                goto addr_290e_12;
            r13_18 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
            if (r13_18->f0) 
                goto addr_6ec8_16;
            r13_18->f0 = r15_8;
            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
            continue;
            addr_6ec8_16:
            rax19 = *reinterpret_cast<void***>(r14_4 + 72);
            if (!rax19) {
                rax19 = fun_2750(16, rsi16, rdx3);
                if (!rax19) 
                    goto addr_6f3a_19;
            } else {
                *reinterpret_cast<void***>(r14_4 + 72) = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx3 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx3;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            *reinterpret_cast<void***>(rbp6 + 24) = *reinterpret_cast<void***>(rbp6 + 24) - 1;
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_290e_12:
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    fun_24b0(rdi12, rdi12);
    addr_6f3a_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
        fun_24b0(r13_5, r13_5);
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_6c3f_23:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_6be4_26;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_6c50_30;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_6c50_30;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_6c3f_23;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_6be4_26;
            }
        }
    }
    addr_6c41_34:
    return rax11;
    addr_6be4_26:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_6c41_34;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_6c50_30:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_25a0();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_25a0();
    if (r8d > 10) {
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xd040 + rax11 * 4) + 0xd040;
    }
}

struct s5 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_2650();

struct s6 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s5* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s6* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x946f;
    rax8 = fun_24c0();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
        fun_24b0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x12070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x8a61]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x94fb;
            fun_2650();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s6*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x12100) {
                fun_2480(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x958a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_25d0();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x12080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

int32_t fun_28d0(int64_t rdi, void** rsi, void** rdx, ...);

void** fts_stat(void** rdi, void** rsi, signed char dl, ...) {
    void** rbp4;
    void** eax5;
    void** rsi6;
    int64_t rdi7;
    int32_t eax8;
    void** rax9;
    void** eax10;
    int64_t rdi11;
    int32_t eax12;
    uint32_t eax13;
    void** rax14;
    int64_t rax15;
    int64_t rdi16;
    int32_t eax17;
    void** rax18;
    int64_t* rdi19;
    int64_t rcx20;

    rbp4 = rsi + 0x70;
    eax5 = *reinterpret_cast<void***>(rdi + 72);
    if (*reinterpret_cast<unsigned char*>(&eax5) & 2) 
        goto addr_45b0_2;
    if (*reinterpret_cast<unsigned char*>(&eax5) & 1 && !*reinterpret_cast<void***>(rsi + 88)) {
        goto addr_45b0_2;
    }
    if (dl) {
        addr_45b0_2:
        rsi6 = *reinterpret_cast<void***>(rsi + 48);
        *reinterpret_cast<void***>(&rdi7) = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        eax8 = fun_28d0(rdi7, rsi6, rbp4);
        if (eax8 < 0) {
            rax9 = fun_24c0();
            eax10 = *reinterpret_cast<void***>(rax9);
            if (eax10 == 2) {
                rsi6 = *reinterpret_cast<void***>(rsi + 48);
                *reinterpret_cast<void***>(&rdi11) = *reinterpret_cast<void***>(rdi + 44);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
                eax12 = fun_28d0(rdi11, rsi6, rbp4, rdi11, rsi6, rbp4);
                if (eax12 < 0) {
                    eax10 = *reinterpret_cast<void***>(rax9);
                } else {
                    *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(0);
                    return 13;
                }
            }
        } else {
            addr_45c7_10:
            eax13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x88)) & 0xf000;
            if (eax13 == 0x4000) {
                *reinterpret_cast<uint32_t*>(&rax14) = 1;
                *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi + 0x100) == 46) && (!*reinterpret_cast<signed char*>(rsi + 0x101) || (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x100)) & 0xffff00) == 0x2e00)) {
                    *reinterpret_cast<uint32_t*>(&rax14) = (1 - (1 + reinterpret_cast<uint1_t>(1 < 1 + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 88)) < reinterpret_cast<unsigned char>(1)))) & 0xfffffffc) + 5;
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_45f7_13;
                }
            } else {
                if (eax13 == 0xa000) {
                    *reinterpret_cast<uint32_t*>(&rax14) = 12;
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_45f7_13;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = reinterpret_cast<uint1_t>(eax13 == 0x8000);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax14) = static_cast<uint32_t>(rax15 + rax15 * 4 + 3);
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_45f7_13;
                }
            }
        }
    } else {
        rsi6 = *reinterpret_cast<void***>(rsi + 48);
        *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
        eax17 = fun_28d0(rdi16, rsi6, rbp4, rdi16, rsi6, rbp4);
        if (eax17 >= 0) 
            goto addr_45c7_10;
        rax18 = fun_24c0();
        eax10 = *reinterpret_cast<void***>(rax18);
    }
    *reinterpret_cast<void***>(rsi + 64) = eax10;
    rdi19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp4 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(rsi + 0x70) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbp4 + 0x88) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rcx20) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp4) - reinterpret_cast<uint64_t>(rdi19) + 0x90) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
    while (rcx20) {
        --rcx20;
        *rdi19 = 0;
        ++rdi19;
    }
    return 10;
    addr_45f7_13:
    return rax14;
}

void** cycle_check(void** rdi, void*** rsi);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8) {
    void** rdi6;
    void** rax7;
    void** rax8;
    void** rax9;
    void** rdi10;
    void** rax11;
    void** rax12;
    void** rax13;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rdi6 = *reinterpret_cast<void***>(rdi + 88);
        rax7 = cycle_check(rdi6, rsi + 0x70);
        if (*reinterpret_cast<signed char*>(&rax7)) {
            *reinterpret_cast<void***>(rsi) = rsi;
            *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
            return rax7;
        }
    } else {
        rax8 = fun_2750(24, rsi, rdx);
        if (!rax8) 
            goto addr_4c08_5;
        rax9 = *reinterpret_cast<void***>(rsi + 0x70);
        rdi10 = *reinterpret_cast<void***>(rdi + 88);
        *reinterpret_cast<void***>(rax8 + 16) = rsi;
        *reinterpret_cast<void***>(rax8) = rax9;
        *reinterpret_cast<void***>(rax8 + 8) = *reinterpret_cast<void***>(rsi + 0x78);
        rax11 = hash_insert(rdi10, rax8, rdx, rcx, r8);
        if (rax8 != rax11) 
            goto addr_4bb3_7;
    }
    addr_4bd0_8:
    *reinterpret_cast<int32_t*>(&rax12) = 1;
    *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
    addr_4bd5_9:
    return rax12;
    addr_4bb3_7:
    fun_2480(rax8, rax8);
    if (!rax11) {
        addr_4c08_5:
        *reinterpret_cast<int32_t*>(&rax12) = 0;
        *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
        goto addr_4bd5_9;
    } else {
        rax13 = *reinterpret_cast<void***>(rax11 + 16);
        *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
        *reinterpret_cast<void***>(rsi) = rax13;
        goto addr_4bd0_8;
    }
}

struct s7 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s7* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s7* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xcfe3);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xcfdc);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xcfe7);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xcfd8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g11d98 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g11d98;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2463() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2473() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2483() {
    __asm__("cli ");
    goto free;
}

int64_t __cxa_finalize = 0;

void fun_2493() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t endgrent = 0x2030;

void fun_24a3() {
    __asm__("cli ");
    goto endgrent;
}

int64_t abort = 0x2040;

void fun_24b3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_24c3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_24d3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_24e3() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x2080;

void fun_24f3() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x2090;

void fun_2503() {
    __asm__("cli ");
    goto __fpending;
}

int64_t qsort = 0x20a0;

void fun_2513() {
    __asm__("cli ");
    goto qsort;
}

int64_t reallocarray = 0x20b0;

void fun_2523() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20c0;

void fun_2533() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x20d0;

void fun_2543() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20e0;

void fun_2553() {
    __asm__("cli ");
    goto fclose;
}

int64_t getpwuid = 0x20f0;

void fun_2563() {
    __asm__("cli ");
    goto getpwuid;
}

int64_t bindtextdomain = 0x2100;

void fun_2573() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t stpcpy = 0x2110;

void fun_2583() {
    __asm__("cli ");
    goto stpcpy;
}

int64_t dcgettext = 0x2120;

void fun_2593() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2130;

void fun_25a3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2140;

void fun_25b3() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x2150;

void fun_25c3() {
    __asm__("cli ");
    goto openat;
}

int64_t __stack_chk_fail = 0x2160;

void fun_25d3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2170;

void fun_25e3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2180;

void fun_25f3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2190;

void fun_2603() {
    __asm__("cli ");
    goto strchr;
}

int64_t getgrgid = 0x21a0;

void fun_2613() {
    __asm__("cli ");
    goto getgrgid;
}

int64_t strrchr = 0x21b0;

void fun_2623() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21c0;

void fun_2633() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21d0;

void fun_2643() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21e0;

void fun_2653() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x21f0;

void fun_2663() {
    __asm__("cli ");
    goto close;
}

int64_t __openat_2 = 0x2200;

void fun_2673() {
    __asm__("cli ");
    goto __openat_2;
}

int64_t closedir = 0x2210;

void fun_2683() {
    __asm__("cli ");
    goto closedir;
}

int64_t lstat = 0x2220;

void fun_2693() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x2230;

void fun_26a3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2240;

void fun_26b3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2250;

void fun_26c3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2260;

void fun_26d3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t dirfd = 0x2270;

void fun_26e3() {
    __asm__("cli ");
    goto dirfd;
}

int64_t fputc_unlocked = 0x2280;

void fun_26f3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x2290;

void fun_2703() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x22a0;

void fun_2713() {
    __asm__("cli ");
    goto memcpy;
}

int64_t getgrnam = 0x22b0;

void fun_2723() {
    __asm__("cli ");
    goto getgrnam;
}

int64_t fileno = 0x22c0;

void fun_2733() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x22d0;

void fun_2743() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x22e0;

void fun_2753() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22f0;

void fun_2763() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2300;

void fun_2773() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2310;

void fun_2783() {
    __asm__("cli ");
    goto __freading;
}

int64_t fchdir = 0x2320;

void fun_2793() {
    __asm__("cli ");
    goto fchdir;
}

int64_t realloc = 0x2330;

void fun_27a3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2340;

void fun_27b3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2350;

void fun_27c3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t memmove = 0x2360;

void fun_27d3() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2370;

void fun_27e3() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x2380;

void fun_27f3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2390;

void fun_2803() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fchown = 0x23a0;

void fun_2813() {
    __asm__("cli ");
    goto fchown;
}

int64_t strtoumax = 0x23b0;

void fun_2823() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t fdopendir = 0x23c0;

void fun_2833() {
    __asm__("cli ");
    goto fdopendir;
}

int64_t fstatfs = 0x23d0;

void fun_2843() {
    __asm__("cli ");
    goto fstatfs;
}

int64_t __cxa_atexit = 0x23e0;

void fun_2853() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t fchownat = 0x23f0;

void fun_2863() {
    __asm__("cli ");
    goto fchownat;
}

int64_t exit = 0x2400;

void fun_2873() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2410;

void fun_2883() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2420;

void fun_2893() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2430;

void fun_28a3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2440;

void fun_28b3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2450;

void fun_28c3() {
    __asm__("cli ");
    goto fstat;
}

int64_t fstatat = 0x2460;

void fun_28d3() {
    __asm__("cli ");
    goto fstatat;
}

int64_t __ctype_b_loc = 0x2470;

void fun_28e3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_27b0(int64_t rdi, ...);

void fun_2570(int64_t rdi, int64_t rsi);

void fun_2540(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

void chopt_init(void* rdi, int64_t rsi);

int32_t fun_25e0(int64_t rdi, void** rsi);

int32_t optind = 0;

void** reference_file = reinterpret_cast<void**>(0);

void** fun_2590();

void fun_27e0();

void** quote(void** rdi, ...);

void usage();

void** optarg = reinterpret_cast<void**>(0);

int64_t stdout = 0;

int64_t Version = 0xce8c;

void version_etc(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

void fun_2870();

void** xstrdup(void** rdi, void** rsi, ...);

struct s8 {
    signed char[16] pad16;
    uint32_t f10;
};

struct s8* fun_2720(void** rdi, void** rsi);

int32_t xstrtoumax(void** rdi);

void fun_24a0(void** rdi, void** rsi);

int64_t get_root_dev_ino(int64_t rdi, void** rsi);

int32_t chown_files(void*** rdi);

int32_t fun_2700(void** rdi, void** rsi);

int64_t gid_to_name(int64_t rdi, void** rsi);

void** quotearg_style(int64_t rdi, void** rsi, ...);

void fun_2973(int32_t edi, void** rsi) {
    void** rbp3;
    void** rbx4;
    void* rsp5;
    void** rdi6;
    void** r12_7;
    void* rsp8;
    int32_t v9;
    void** rsi10;
    int64_t rdi11;
    int32_t eax12;
    void* rsp13;
    signed char v14;
    int64_t rax15;
    uint64_t rdx16;
    void** rdi17;
    void** rax18;
    void** rax19;
    int64_t rdi20;
    int64_t rcx21;
    int64_t rax22;
    void** rdi23;
    struct s8* rax24;
    void* rsp25;
    int32_t eax26;
    uint64_t rax27;
    uint64_t v28;
    signed char v29;
    int64_t rax30;
    int64_t rax31;
    int32_t eax32;
    uint32_t v33;
    int64_t rdi34;
    void** rax35;
    void** rax36;

    __asm__("cli ");
    rbp3 = reinterpret_cast<void**>(static_cast<int64_t>(edi));
    rbx4 = rsi;
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xf8);
    rdi6 = *reinterpret_cast<void***>(rsi);
    set_program_name(rdi6);
    fun_27b0(6, 6);
    fun_2570("coreutils", "/usr/local/share/locale");
    r12_7 = reinterpret_cast<void**>(0xc9a0);
    fun_2540("coreutils", "/usr/local/share/locale");
    atexit(0x4440, "/usr/local/share/locale");
    chopt_init(reinterpret_cast<int64_t>(rsp5) + 32, "/usr/local/share/locale");
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v9 = -1;
    while (1) {
        rsi10 = rbx4;
        *reinterpret_cast<int32_t*>(&rdi11) = *reinterpret_cast<int32_t*>(&rbp3);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
        eax12 = fun_25e0(rdi11, rsi10);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8);
        if (eax12 == -1) {
            if (!v14) {
                addr_2a79_4:
            } else {
                if (!0) {
                    if (v9 == 1) 
                        goto addr_2d9c_7;
                    v9 = 0;
                }
            }
            rax15 = optind;
            r12_7 = reference_file;
            *reinterpret_cast<int32_t*>(&rdx16) = *reinterpret_cast<int32_t*>(&rbp3) - *reinterpret_cast<int32_t*>(&rax15);
            if (!r12_7) {
                if (*reinterpret_cast<int32_t*>(&rdx16) > 1) 
                    break;
            } else {
                if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx16) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rdx16) == 0))) 
                    goto addr_2ca3_12;
            }
            if (*reinterpret_cast<int32_t*>(&rax15) >= *reinterpret_cast<int32_t*>(&rbp3)) {
                fun_2590();
                fun_27e0();
                rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8);
                goto addr_2ae7_15;
            } else {
                rdi17 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx4 + reinterpret_cast<unsigned char>(rbp3) * 8) - 8);
                rax18 = quote(rdi17, rdi17);
                r12_7 = rax18;
                fun_2590();
                fun_27e0();
                rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_2ae7_15;
            }
        } else {
            if (eax12 > 0x83) {
                addr_2ae7_15:
                usage();
                rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
                rax19 = optarg;
                reference_file = rax19;
                continue;
            } else {
                if (eax12 <= 71) {
                    if (eax12 == 0xffffff7d) {
                        rdi20 = stdout;
                        rcx21 = Version;
                        version_etc(rdi20, "chgrp", "GNU coreutils", rcx21, "David MacKenzie");
                        fun_2870();
                        rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 - 8 - 8 + 8 - 8 + 8);
                        continue;
                    }
                    if (eax12 != 0xffffff7e) 
                        goto addr_2ae7_15;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax22) = eax12 - 72;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax22) + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rax22) > 59) 
                        goto addr_2ae7_15; else 
                        goto addr_2a50_23;
                }
            }
            usage();
            rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
            goto addr_2a79_4;
        }
    }
    rbp3 = *reinterpret_cast<void***>(rbx4 + rax15 * 8);
    optind = static_cast<int32_t>(rax15 + 1);
    if (!*reinterpret_cast<void***>(rbp3)) 
        goto addr_2c36_26;
    while (1) {
        xstrdup(rbp3, rsi10);
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
        addr_2c36_26:
        *reinterpret_cast<uint32_t*>(&r12_7) = 0xffffffff;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        if (*reinterpret_cast<void***>(rbp3)) {
            rdi23 = rbp3;
            rax24 = fun_2720(rdi23, rsi10);
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8);
            if (!rax24) {
                rsi10 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rsi10 + 4) = 0;
                rdi23 = rbp3;
                eax26 = xstrtoumax(rdi23);
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                if (eax26) 
                    goto addr_2d6a_30;
                rax27 = v28;
                *reinterpret_cast<uint32_t*>(&r12_7) = *reinterpret_cast<uint32_t*>(&rax27);
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                if (rax27 > 0xffffffff) 
                    goto addr_2d6a_30;
            } else {
                *reinterpret_cast<uint32_t*>(&r12_7) = rax24->f10;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
            }
            fun_24a0(rdi23, rsi10);
            rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
        }
        while (!v29 || (!0 || (rax30 = get_root_dev_ino(0x120d0, rsi10), rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8), !!rax30))) {
            rax31 = optind;
            chown_files(rbx4 + rax31 * 8);
            fun_2870();
            rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 - 8 - 8 + 8 - 8 + 8);
            addr_2ca3_12:
            rsi10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp13) + 80);
            eax32 = fun_2700(r12_7, rsi10);
            if (eax32) 
                goto addr_2dc0_36;
            *reinterpret_cast<uint32_t*>(&r12_7) = v33;
            *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&r12_7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
            gid_to_name(rdi34, rsi10);
            rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8);
        }
        quotearg_style(4, "/", 4, "/");
        fun_2590();
        rax35 = fun_24c0();
        rsi10 = *reinterpret_cast<void***>(rax35);
        *reinterpret_cast<int32_t*>(&rsi10 + 4) = 0;
        fun_27e0();
        rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp13) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    }
    addr_2dc0_36:
    quotearg_style(4, r12_7, 4, r12_7);
    fun_2590();
    fun_24c0();
    fun_27e0();
    addr_2d6a_30:
    rax36 = quote(rbp3, rbp3);
    r12_7 = rax36;
    fun_2590();
    fun_27e0();
    addr_2d9c_7:
    fun_2590();
    fun_27e0();
    goto addr_2dc0_36;
    addr_2a50_23:
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_7) + reinterpret_cast<uint64_t>(rax22 * 4)))) + reinterpret_cast<unsigned char>(r12_7);
}

int64_t __libc_start_main = 0;

void fun_2e03() {
    __asm__("cli ");
    __libc_start_main(0x2970, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x12008;

void fun_2490(int64_t rdi);

int64_t fun_2ea3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2490(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2ee3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_27c0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void fun_26b0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

int32_t fun_26d0(int64_t rdi);

int32_t fun_24d0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

int64_t stderr = 0;

void fun_2890(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2ef3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** r8_6;
    int64_t r12_7;
    void** rax8;
    int64_t r12_9;
    void** rax10;
    int64_t r12_11;
    void** rax12;
    int64_t r12_13;
    void** rax14;
    int64_t r12_15;
    void** rax16;
    int64_t r12_17;
    void** rax18;
    int64_t r12_19;
    void** rax20;
    int64_t r12_21;
    void** rax22;
    int64_t r12_23;
    void** rax24;
    int64_t r12_25;
    void** rax26;
    void** r12_27;
    void** rax28;
    void** r8_29;
    int32_t eax30;
    void** r13_31;
    void** rax32;
    void** r8_33;
    void** rax34;
    int32_t eax35;
    void** rax36;
    void** r8_37;
    void** rax38;
    void** r8_39;
    void** rax40;
    int32_t eax41;
    void** rax42;
    void** r8_43;
    int64_t r15_44;
    void** rax45;
    void** rax46;
    void** r8_47;
    void** rax48;
    int64_t rdi49;
    void** r8_50;
    void** r9_51;
    int64_t v52;
    int64_t v53;
    int64_t v54;
    int64_t v55;
    int64_t v56;
    int64_t v57;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2590();
            fun_27c0(1, rax5, r12_2, r12_2, r8_6);
            r12_7 = stdout;
            rax8 = fun_2590();
            fun_26b0(rax8, r12_7, 5, r12_2);
            r12_9 = stdout;
            rax10 = fun_2590();
            fun_26b0(rax10, r12_9, 5, r12_2);
            r12_11 = stdout;
            rax12 = fun_2590();
            fun_26b0(rax12, r12_11, 5, r12_2);
            r12_13 = stdout;
            rax14 = fun_2590();
            fun_26b0(rax14, r12_13, 5, r12_2);
            r12_15 = stdout;
            rax16 = fun_2590();
            fun_26b0(rax16, r12_15, 5, r12_2);
            r12_17 = stdout;
            rax18 = fun_2590();
            fun_26b0(rax18, r12_17, 5, r12_2);
            r12_19 = stdout;
            rax20 = fun_2590();
            fun_26b0(rax20, r12_19, 5, r12_2);
            r12_21 = stdout;
            rax22 = fun_2590();
            fun_26b0(rax22, r12_21, 5, r12_2);
            r12_23 = stdout;
            rax24 = fun_2590();
            fun_26b0(rax24, r12_23, 5, r12_2);
            r12_25 = stdout;
            rax26 = fun_2590();
            fun_26b0(rax26, r12_25, 5, r12_2);
            r12_27 = program_name;
            rax28 = fun_2590();
            fun_27c0(1, rax28, r12_27, r12_27, r8_29);
            do {
                if (1) 
                    break;
                eax30 = fun_26d0("chgrp");
            } while (eax30);
            r13_31 = v4;
            if (!r13_31) {
                rax32 = fun_2590();
                fun_27c0(1, rax32, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_33);
                rax34 = fun_27b0(5);
                if (!rax34 || (eax35 = fun_24d0(rax34, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax35)) {
                    rax36 = fun_2590();
                    r13_31 = reinterpret_cast<void**>("chgrp");
                    fun_27c0(1, rax36, "https://www.gnu.org/software/coreutils/", "chgrp", r8_37);
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_31 = reinterpret_cast<void**>("chgrp");
                    goto addr_3330_9;
                }
            } else {
                rax38 = fun_2590();
                fun_27c0(1, rax38, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_39);
                rax40 = fun_27b0(5);
                if (!rax40 || (eax41 = fun_24d0(rax40, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax41)) {
                    addr_3236_11:
                    rax42 = fun_2590();
                    fun_27c0(1, rax42, "https://www.gnu.org/software/coreutils/", "chgrp", r8_43);
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_31 == "chgrp")) {
                        r12_2 = reinterpret_cast<void**>(0xd401);
                    }
                } else {
                    addr_3330_9:
                    r15_44 = stdout;
                    rax45 = fun_2590();
                    fun_26b0(rax45, r15_44, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3236_11;
                }
            }
            rax46 = fun_2590();
            fun_27c0(1, rax46, r13_31, r12_2, r8_47);
            addr_2f4e_14:
            fun_2870();
        }
    } else {
        rax48 = fun_2590();
        rdi49 = stderr;
        fun_2890(rdi49, 1, rax48, r12_2, r8_50, r9_51, v52, v53, v54, v55, v56, v57);
        goto addr_2f4e_14;
    }
}

struct s9 {
    int32_t f0;
    signed char f4;
    signed char[3] pad8;
    int64_t f8;
    int16_t f10;
    signed char[6] pad24;
    int64_t f18;
    int64_t f20;
};

void fun_33e3(struct s9* rdi) {
    __asm__("cli ");
    rdi->f0 = 2;
    rdi->f4 = 0;
    rdi->f8 = 0;
    rdi->f10 = 1;
    rdi->f18 = 0;
    rdi->f20 = 0;
    return;
}

struct s10 {
    signed char[24] pad24;
    void** f18;
};

void fun_3413(struct s10* rdi, void** rsi) {
    void** rdi3;

    __asm__("cli ");
    rdi3 = rdi->f18;
    fun_2480(rdi3, rsi);
    goto fun_2480;
}

void*** fun_2610(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** umaxtostr(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** fun_3433(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    void*** rax7;
    int64_t rdi8;
    void** rsi9;
    void** rax10;
    void** rax11;
    void* rdx12;
    void* rdx13;

    __asm__("cli ");
    rax6 = g28;
    rax7 = fun_2610(rdi, rsi, rdx, rcx, r8);
    if (!rax7) {
        *reinterpret_cast<int32_t*>(&rdi8) = *reinterpret_cast<int32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        rsi9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32 - 8 + 8);
        rax10 = umaxtostr(rdi8, rsi9, rdx, rcx, r8);
        rax11 = xstrdup(rax10, rsi9);
        rdx12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (!rdx12) {
            return rax11;
        }
    } else {
        rdx13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (!rdx13) {
            goto xstrdup;
        }
    }
    fun_25d0();
}

void*** fun_2560(int64_t rdi, void** rsi, void** rdx, ...);

void** fun_34b3(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** rax6;
    void*** rax7;
    int64_t rdi8;
    void** rsi9;
    void** rax10;
    void** rax11;
    void* rdx12;
    void* rdx13;

    __asm__("cli ");
    rax6 = g28;
    rax7 = fun_2560(rdi, rsi, rdx);
    if (!rax7) {
        *reinterpret_cast<int32_t*>(&rdi8) = *reinterpret_cast<int32_t*>(&rdi);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        rsi9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32 - 8 + 8);
        rax10 = umaxtostr(rdi8, rsi9, rdx, rcx, r8);
        rax11 = xstrdup(rax10, rsi9);
        rdx12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (!rdx12) {
            return rax11;
        }
    } else {
        rdx13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (!rdx13) {
            goto xstrdup;
        }
    }
    fun_25d0();
}

struct s11 {
    void** f0;
    signed char[3] pad4;
    signed char f4;
    signed char[3] pad8;
    void** f8;
    signed char[7] pad16;
    unsigned char f10;
    unsigned char f11;
    signed char[6] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void** xfts_open();

void** rpl_fts_read(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** quotearg_n_style();

int32_t fun_2860(int64_t rdi, void** rsi, void** rdx, void** rcx, ...);

int32_t fun_2670(int64_t rdi, void** rsi, ...);

int32_t fun_2810();

void rpl_fts_set(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int32_t rpl_fts_close(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

int64_t fun_3533() {
    void** v1;
    void** r8d2;
    void** r8_3;
    uint32_t r8d4;
    uint32_t r9d5;
    struct s11* r15_6;
    struct s11* v7;
    void** v8;
    void** edx9;
    void** v10;
    void** ecx11;
    void** v12;
    void** r9d13;
    void** rax14;
    void** v15;
    uint32_t v16;
    uint32_t eax17;
    void** rsi18;
    uint32_t esi19;
    void** rdx20;
    void** rax21;
    void* rsp22;
    unsigned char v23;
    void** v24;
    void** rcx25;
    void** rax26;
    int64_t rbx27;
    void** r13_28;
    void** v29;
    uint32_t eax30;
    void** r12_31;
    int64_t rdi32;
    int32_t eax33;
    uint32_t r14d34;
    void** rax35;
    void** rax36;
    void* rsp37;
    uint32_t eax38;
    uint32_t ebx39;
    int32_t eax40;
    uint32_t ebx41;
    uint32_t eax42;
    uint32_t ebx43;
    uint32_t edx44;
    uint32_t ebx45;
    void** r14d46;
    int32_t ebx47;
    void** r13d48;
    void** rax49;
    void** rax50;
    void* rsp51;
    void** rax52;
    void** rax53;
    uint32_t eax54;
    int64_t rdi55;
    int32_t eax56;
    int64_t rdi57;
    void*** rax58;
    void* rsp59;
    int64_t rdi60;
    int64_t rdi61;
    void** rax62;
    void** rax63;
    void* rsp64;
    void** v65;
    void** rdi66;
    void** rax67;
    void** rax68;
    void** ebx69;
    int64_t rdi70;
    int32_t eax71;
    void** rax72;
    void* rsp73;
    void** rax74;
    void* rsp75;
    void** rbx76;
    void** rax77;
    int64_t rdi78;
    int32_t eax79;
    void** rax80;
    int64_t rdi81;
    int32_t eax82;
    int32_t eax83;
    void* rsp84;
    void** v85;
    void** v86;
    void** rax87;
    void** r13d88;
    void** v89;
    void** v90;
    void** rax91;
    void** r13d92;
    int32_t eax93;
    void** rax94;
    void** r13d95;
    int32_t eax96;
    void** r12d97;
    int64_t rdi98;
    void*** rax99;
    void* rsp100;
    void** rsi101;
    int64_t rdi102;
    void** rax103;
    void** rax104;
    void* rsp105;
    void** v106;
    void** rdi107;
    void** rax108;
    void** r12_109;
    void** rsi110;
    int64_t rdi111;
    void** rax112;
    void** rax113;
    void** r13_114;
    int64_t rdi115;
    void** rsi116;
    void** rax117;
    void** rax118;
    void** rax119;
    void* rsp120;
    void** rsi121;
    void** v122;
    void** rax123;
    void** rax124;
    void* rsp125;
    void** rax126;
    void** rdi127;
    void** rax128;
    void* rsp129;
    void** v130;
    void** rax131;
    void* rsp132;
    void** rbx133;
    void** rax134;
    void** rax135;
    void** rax136;
    void** rax137;
    void** rax138;
    void** rax139;
    void** rax140;
    void** rax141;
    void** rax142;
    void** rax143;
    void** rax144;
    void** rax145;
    void** rax146;
    uint32_t eax147;
    void** rax148;
    int32_t eax149;
    void* rax150;
    int64_t rax151;
    int64_t rax152;

    __asm__("cli ");
    v1 = r8d2;
    *reinterpret_cast<uint32_t*>(&r8_3) = r8d4 & r9d5;
    *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
    r15_6 = v7;
    v8 = edx9;
    v10 = ecx11;
    v12 = r9d13;
    rax14 = g28;
    v15 = rax14;
    v16 = *reinterpret_cast<uint32_t*>(&r8_3);
    if (*reinterpret_cast<uint32_t*>(&r8_3) == 0xffffffff && !r15_6->f10) {
        eax17 = 0;
        *reinterpret_cast<unsigned char*>(&eax17) = reinterpret_cast<uint1_t>(r15_6->f0 == 2);
        rsi18 = reinterpret_cast<void**>(esi19 | eax17 << 3);
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
    }
    rdx20 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
    rax21 = xfts_open();
    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x198 - 8 + 8);
    v23 = 1;
    v24 = rax21;
    while (rax26 = rpl_fts_read(v24, rsi18, rdx20, rcx25, r8_3), rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8), !!rax26) {
        *reinterpret_cast<uint32_t*>(&rbx27) = *reinterpret_cast<uint16_t*>(rax26 + 0x68);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx27) + 4) = 0;
        r13_28 = *reinterpret_cast<void***>(rax26 + 48);
        v29 = *reinterpret_cast<void***>(rax26 + 56);
        if (*reinterpret_cast<uint16_t*>(&rbx27) <= 10) 
            goto addr_35c5_6;
        eax30 = r15_6->f10;
        r12_31 = rax26 + 0x70;
        if (v16 != 0xffffffff || (!reinterpret_cast<int1_t>(r15_6->f0 == 2) || r15_6->f8)) {
            if (!*reinterpret_cast<signed char*>(&eax30)) 
                goto addr_3626_9; else 
                goto addr_3610_10;
        }
        if (*reinterpret_cast<signed char*>(&eax30)) {
            addr_3610_10:
            if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax26 + 0x88)) & 0xf000) == 0xa000) {
                r12_31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 80);
                rcx25 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
                rsi18 = r13_28;
                rdx20 = r12_31;
                *reinterpret_cast<void***>(&rdi32) = *reinterpret_cast<void***>(v24 + 44);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
                eax33 = fun_28d0(rdi32, rsi18, rdx20);
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                if (!eax33) {
                    *reinterpret_cast<uint32_t*>(&rbx27) = *reinterpret_cast<uint16_t*>(rax26 + 0x68);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx27) + 4) = 0;
                    goto addr_3626_9;
                } else {
                    r14d34 = r15_6->f11;
                    if (*reinterpret_cast<unsigned char*>(&r14d34)) 
                        goto addr_36ce_15;
                    rax35 = quotearg_style(4, v29, 4, v29);
                    rax36 = fun_2590();
                    fun_24c0();
                    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8 - 8 + 8);
                    rcx25 = rax35;
                    rdx20 = rax36;
                }
            } else {
                addr_3626_9:
                if (v1 == 0xffffffff || v1 == *reinterpret_cast<void***>(r12_31 + 28)) {
                    if (v12 == 0xffffffff) {
                        eax38 = *reinterpret_cast<uint32_t*>(&rbx27) & 0xfffffffd;
                        ebx39 = *reinterpret_cast<uint32_t*>(&rbx27) - 1;
                        *reinterpret_cast<unsigned char*>(&eax40) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<int16_t*>(&eax38) == 4)) | static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint16_t*>(&ebx39) <= 1)));
                        if (!*reinterpret_cast<unsigned char*>(&eax40)) 
                            goto addr_367b_19;
                        rdx20 = r15_6->f8;
                        if (!rdx20) 
                            goto addr_367b_19;
                        if (*reinterpret_cast<void***>(r12_31 + 8) != *reinterpret_cast<void***>(rdx20)) 
                            goto addr_367b_19;
                        goto addr_3dc0_23;
                    } else {
                        rdx20 = reinterpret_cast<void**>(static_cast<uint32_t>(rbx27 - 1));
                        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&eax40) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r12_31 + 32) == v12);
                        if (*reinterpret_cast<uint16_t*>(&rdx20) <= 1) 
                            goto addr_3b4b_25;
                        ebx41 = *reinterpret_cast<uint32_t*>(&rbx27) & 0xfffffffd;
                        if (*reinterpret_cast<int16_t*>(&ebx41) != 4) 
                            goto addr_3673_27;
                        addr_3b4b_25:
                        rdx20 = r15_6->f8;
                        if (!rdx20) 
                            goto addr_3673_27;
                        goto addr_3665_29;
                    }
                } else {
                    eax42 = *reinterpret_cast<uint32_t*>(&rbx27) & 0xfffffffd;
                    if (*reinterpret_cast<int16_t*>(&eax42) == 4) 
                        goto addr_3656_31;
                    ebx43 = *reinterpret_cast<uint32_t*>(&rbx27) - 1;
                    if (*reinterpret_cast<uint16_t*>(&ebx43) > 1) 
                        goto addr_43fa_33;
                    addr_3656_31:
                    rdx20 = r15_6->f8;
                    if (!rdx20) 
                        goto addr_43fa_33;
                    eax40 = 0;
                    goto addr_3665_29;
                }
            }
        } else {
            edx44 = *reinterpret_cast<uint32_t*>(&rbx27) & 0xfffffffd;
            if (*reinterpret_cast<int16_t*>(&edx44) == 4 || (ebx45 = *reinterpret_cast<uint32_t*>(&rbx27) - 1, *reinterpret_cast<uint16_t*>(&ebx45) <= 1)) {
                r14d46 = *reinterpret_cast<void***>(v24 + 44);
                goto addr_3691_37;
            }
        }
        addr_3f0e_38:
        fun_27e0();
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp37) - 8 + 8);
        rsi18 = r15_6->f0;
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
        if (!rsi18) {
            addr_36dc_39:
            ebx47 = 3;
        } else {
            goto addr_3b63_41;
        }
        addr_36e1_42:
        r13d48 = *reinterpret_cast<void***>(r12_31 + 28);
        goto addr_36e6_43;
        addr_3665_29:
        rcx25 = *reinterpret_cast<void***>(rdx20);
        if (*reinterpret_cast<void***>(r12_31 + 8) == rcx25) {
            addr_3dc0_23:
            rcx25 = *reinterpret_cast<void***>(rdx20 + 8);
            if (*reinterpret_cast<void***>(r12_31) == rcx25) {
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v29) == 47) || *reinterpret_cast<void***>(v29 + 1)) {
                    rax49 = quotearg_n_style();
                    rax50 = quotearg_n_style();
                    fun_2590();
                    r8_3 = rax49;
                    rcx25 = rax50;
                    fun_27e0();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                } else {
                    rax52 = quotearg_style(4, v29, 4, v29);
                    fun_2590();
                    rcx25 = rax52;
                    fun_27e0();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 + 8 - 8 + 8);
                }
                rax53 = fun_2590();
                rsi18 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
                rdx20 = rax53;
                fun_27e0();
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                v23 = 0;
                continue;
            }
        } else {
            addr_3673_27:
            if (!*reinterpret_cast<unsigned char*>(&eax40)) {
                addr_43fa_33:
                r14d34 = 1;
                if (r15_6->f0) {
                    addr_3b63_41:
                    v23 = reinterpret_cast<unsigned char>(v23 & *reinterpret_cast<unsigned char*>(&r14d34));
                    goto addr_3814_48;
                }
            } else {
                addr_367b_19:
                eax30 = r15_6->f10;
                goto addr_3680_50;
            }
        }
        addr_3fe0_51:
        r14d34 = 1;
        ebx47 = 4;
        goto addr_36e1_42;
        addr_3680_50:
        rcx25 = v24;
        r14d46 = *reinterpret_cast<void***>(rcx25 + 44);
        if (*reinterpret_cast<signed char*>(&eax30)) {
            if (v16 == 0xffffffff) 
                goto addr_3bde_53;
            eax54 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_31 + 24)) & 0xf000;
            if (eax54 != 0x8000) 
                goto addr_3b8c_55;
        } else {
            addr_3691_37:
            rcx25 = v10;
            *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
            rdx20 = v8;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rsi18 = r13_28;
            *reinterpret_cast<void***>(&rdi55) = r14d46;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r8_3) = 0x100;
            *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
            eax56 = fun_2860(rdi55, rsi18, rdx20, rcx25, rdi55, rsi18, rdx20, rcx25);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            if (!eax56) {
                addr_3c00_56:
                if (r15_6->f0 == 2) {
                    addr_3814_48:
                    if (r15_6->f4) 
                        continue; else 
                        goto addr_381f_57;
                } else {
                    if (v8 == 0xffffffff || (r13d48 = *reinterpret_cast<void***>(r12_31 + 28), ebx47 = 2, r14d34 = 1, v8 == r13d48)) {
                        rcx25 = v10;
                        *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
                        if (rcx25 != 0xffffffff && rcx25 != *reinterpret_cast<void***>(r12_31 + 32)) {
                            r13d48 = *reinterpret_cast<void***>(r12_31 + 28);
                            ebx47 = 2;
                            r14d34 = 1;
                            *reinterpret_cast<void***>(&rdi57) = r13d48;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi57) + 4) = 0;
                            rax58 = fun_2560(rdi57, rsi18, rdx20);
                            rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                            if (rax58) 
                                goto addr_36f7_61;
                            goto addr_3c70_63;
                        }
                        if (r15_6->f0) 
                            goto addr_3814_48; else 
                            goto addr_3fe0_51;
                    } else {
                        addr_36e6_43:
                        *reinterpret_cast<void***>(&rdi60) = r13d48;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi60) + 4) = 0;
                        rax58 = fun_2560(rdi60, rsi18, rdx20, rdi60, rsi18, rdx20);
                        rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                        if (!rax58) {
                            addr_3c70_63:
                            rsi18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp59) + 0x170);
                            *reinterpret_cast<void***>(&rdi61) = r13d48;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi61) + 4) = 0;
                            rax62 = umaxtostr(rdi61, rsi18, rdx20, rcx25, r8_3);
                            rax63 = xstrdup(rax62, rsi18, rax62, rsi18);
                            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8);
                            v65 = rax63;
                            goto addr_3704_65;
                        } else {
                            addr_36f7_61:
                            rdi66 = *rax58;
                            rax67 = xstrdup(rdi66, rsi18, rdi66, rsi18);
                            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
                            v65 = rax67;
                            goto addr_3704_65;
                        }
                    }
                }
            } else {
                rax68 = fun_24c0();
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                if (*reinterpret_cast<void***>(rax68) != 95) 
                    goto addr_36c0_67;
                rcx25 = r15_6->f0;
                *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
                r14d34 = 1;
                if (rcx25) 
                    goto addr_3b63_41; else 
                    goto addr_4161_69;
            }
        }
        ebx69 = reinterpret_cast<void**>(0x900);
        goto addr_3b98_71;
        addr_3b8c_55:
        ebx69 = reinterpret_cast<void**>(0x10900);
        if (eax54 != 0x4000) {
            addr_3bde_53:
            rcx25 = v10;
            *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
            rdx20 = v8;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r8_3) = 0;
            *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
            rsi18 = r13_28;
            *reinterpret_cast<void***>(&rdi70) = r14d46;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi70) + 4) = 0;
            eax71 = fun_2860(rdi70, rsi18, rdx20, rcx25);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            if (eax71) {
                addr_36c0_67:
                r14d34 = r15_6->f11;
                if (!*reinterpret_cast<unsigned char*>(&r14d34)) {
                    rax72 = quotearg_style(4, v29, 4, v29);
                    rsp73 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
                    if (v8 == 0xffffffff) {
                        rax74 = fun_2590();
                        rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
                        rbx76 = rax74;
                    } else {
                        rax77 = fun_2590();
                        rsp75 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp73) - 8 + 8);
                        rbx76 = rax77;
                    }
                    fun_24c0();
                    rsp37 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp75) - 8 + 8);
                    rcx25 = rax72;
                    rdx20 = rbx76;
                    goto addr_3f0e_38;
                } else {
                    addr_36ce_15:
                    if (r15_6->f0) {
                        r14d34 = 0;
                        goto addr_3b63_41;
                    } else {
                        r14d34 = 0;
                        goto addr_36dc_39;
                    }
                }
            } else {
                goto addr_3c00_56;
            }
        } else {
            addr_3b98_71:
            *reinterpret_cast<void***>(&rdi78) = r14d46;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
            rdx20 = ebx69;
            *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
            rsi18 = r13_28;
            eax79 = fun_2670(rdi78, rsi18, rdi78, rsi18);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            if (eax79 >= 0) 
                goto addr_3e6c_79;
        }
        rax80 = fun_24c0();
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
        rcx25 = rax80;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax80) == 13)) 
            goto addr_36c0_67;
        if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_31 + 24)) & 0xf000) != 0x8000) 
            goto addr_3bd5_82;
        *reinterpret_cast<void***>(&rdi81) = r14d46;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi81) + 4) = 0;
        rsi18 = r13_28;
        rdx20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebx69) | 1);
        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
        eax82 = fun_2670(rdi81, rsi18);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
        rcx25 = rcx25;
        if (eax82 < 0) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rcx25) == 13)) 
                goto addr_36c0_67;
        } else {
            addr_3e6c_79:
            rsi18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp22) + 0xe0);
            eax83 = fun_28c0();
            rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            if (eax83) 
                goto addr_3ff0_86; else 
                goto addr_3e89_87;
        }
        addr_3bd5_82:
        r14d46 = *reinterpret_cast<void***>(v24 + 44);
        goto addr_3bde_53;
        addr_3e89_87:
        if (*reinterpret_cast<void***>(r12_31 + 8) != v85 || *reinterpret_cast<void***>(r12_31) != v86) {
            r14d34 = 0;
            rax87 = fun_24c0();
            r13d88 = *reinterpret_cast<void***>(rax87);
            fun_2660();
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8 - 8 + 8);
            *reinterpret_cast<void***>(rax87) = r13d88;
            if (!r15_6->f0) 
                goto addr_36dc_39;
            goto addr_3b63_41;
        } else {
            if (v1 != 0xffffffff && v1 != v89 || v12 != 0xffffffff && v12 != v90) {
                rax91 = fun_24c0();
                r13d92 = *reinterpret_cast<void***>(rax91);
                fun_2660();
                rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8 - 8 + 8);
                *reinterpret_cast<void***>(rax91) = r13d92;
                goto addr_3c00_56;
            } else {
                rdx20 = v10;
                *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
                rsi18 = v8;
                *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
                eax93 = fun_2810();
                rsp84 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
                if (eax93) {
                    addr_3ff0_86:
                    rax94 = fun_24c0();
                    r13d95 = *reinterpret_cast<void***>(rax94);
                    fun_2660();
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8 - 8 + 8);
                    *reinterpret_cast<void***>(rax94) = r13d95;
                    goto addr_36c0_67;
                } else {
                    eax96 = fun_2660();
                    rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
                    if (eax96) 
                        goto addr_36c0_67;
                    goto addr_3c00_56;
                }
            }
        }
        addr_381f_57:
        rdx20 = reinterpret_cast<void**>(4);
        *reinterpret_cast<int32_t*>(&rdx20 + 4) = 0;
        rsi18 = rax26;
        rpl_fts_set(v24, rsi18, 4, rcx25, r8_3);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
        continue;
        addr_3704_65:
        r12d97 = *reinterpret_cast<void***>(r12_31 + 32);
        *reinterpret_cast<void***>(&rdi98) = r12d97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi98) + 4) = 0;
        rax99 = fun_2610(rdi98, rsi18, rdx20, rcx25, r8_3);
        rsp100 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
        if (!rax99) {
            rsi101 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp100) + 0x170);
            *reinterpret_cast<void***>(&rdi102) = r12d97;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi102) + 4) = 0;
            rax103 = umaxtostr(rdi102, rsi101, rdx20, rcx25, r8_3);
            rax104 = xstrdup(rax103, rsi101, rax103, rsi101);
            rsp105 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8 - 8 + 8);
            v106 = rax104;
        } else {
            rdi107 = *rax99;
            rax108 = xstrdup(rdi107, rsi18, rdi107, rsi18);
            rsp105 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp100) - 8 + 8);
            v106 = rax108;
        }
        r12_109 = r15_6->f18;
        if (!r12_109 && v8 != 0xffffffff) {
            rsi110 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp105) + 0x170);
            *reinterpret_cast<void***>(&rdi111) = v8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi111) + 4) = 0;
            rax112 = umaxtostr(rdi111, rsi110, rdx20, rcx25, r8_3);
            rax113 = xstrdup(rax112, rsi110, rax112, rsi110);
            rsp105 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8 - 8 + 8);
            r13_114 = r15_6->f20;
            r12_109 = rax113;
            if (r13_114) 
                goto addr_3741_99;
            goto addr_3a40_101;
        }
        r13_114 = r15_6->f20;
        if (!r13_114) {
            addr_3a40_101:
            if (!reinterpret_cast<int1_t>(v10 == 0xffffffff)) {
                *reinterpret_cast<void***>(&rdi115) = v10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi115) + 4) = 0;
                rsi116 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp105) + 0x170);
                rax117 = umaxtostr(rdi115, rsi116, rdx20, rcx25, r8_3);
                rax118 = xstrdup(rax117, rsi116, rax117, rsi116);
                rsp105 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8 - 8 + 8);
                r13_114 = rax118;
                goto addr_3741_99;
            } else {
                if (ebx47 != 1) {
                    rax119 = user_group_str(r12_109, 0, rdx20, rcx25, r8_3);
                    rsp120 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8);
                    *reinterpret_cast<int32_t*>(&rsi121) = 0;
                    *reinterpret_cast<int32_t*>(&rsi121 + 4) = 0;
                    v122 = rax119;
                    if (r12_109) 
                        goto addr_3768_106; else 
                        goto addr_3a6e_107;
                }
            }
        } else {
            addr_3741_99:
            if (ebx47 == 1) {
                rax123 = quotearg_style(4, v29, 4, v29);
                rax124 = fun_2590();
                rdx20 = rax123;
                rsi18 = rax124;
                fun_27c0(1, rsi18, rdx20, rcx25, r8_3);
                rsp125 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8 - 8 + 8 - 8 + 8);
            } else {
                rax126 = user_group_str(r12_109, r13_114, rdx20, rcx25, r8_3);
                rsp120 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp105) - 8 + 8);
                rsi121 = v106;
                v122 = rax126;
                if (!r12_109) {
                    addr_3a6e_107:
                    *reinterpret_cast<int32_t*>(&rdi127) = 0;
                    *reinterpret_cast<int32_t*>(&rdi127 + 4) = 0;
                    rax128 = user_group_str(0, rsi121, rdx20, rcx25, r8_3);
                    rsp129 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp120) - 8 + 8);
                    v130 = rax128;
                    if (ebx47 != 2) {
                        if (ebx47 == 1) 
                            goto addr_28f0_111;
                        if (ebx47 == 3) 
                            goto addr_3f58_113; else 
                            goto addr_3d52_114;
                    } else {
                        if (!r13_114) {
                            rax131 = fun_2590();
                            rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
                            rbx133 = rax131;
                            goto addr_37a0_117;
                        } else {
                            rax134 = fun_2590();
                            rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
                            rbx133 = rax134;
                            goto addr_37a0_117;
                        }
                    }
                } else {
                    addr_3768_106:
                    rdi127 = v65;
                    rax135 = user_group_str(rdi127, rsi121, rdx20, rcx25, r8_3);
                    rsp129 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp120) - 8 + 8);
                    v130 = rax135;
                    if (ebx47 == 3) {
                        if (!v130) {
                            goto addr_4294_121;
                        } else {
                            rax136 = fun_2590();
                            rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
                            rbx133 = rax136;
                            goto addr_37a0_117;
                        }
                    } else {
                        if (ebx47 != 4) {
                            if (ebx47 != 2) 
                                goto addr_28f0_111;
                            rax137 = fun_2590();
                            rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
                            rbx133 = rax137;
                            goto addr_37a0_117;
                        } else {
                            rax138 = fun_2590();
                            rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
                            rbx133 = rax138;
                            goto addr_37a0_117;
                        }
                    }
                }
            }
        }
        addr_37df_127:
        fun_2480(v65, rsi18, v65, rsi18);
        fun_2480(v106, rsi18, v106, rsi18);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp125) - 8 + 8 - 8 + 8);
        if (r12_109 != r15_6->f18) {
            fun_2480(r12_109, rsi18, r12_109, rsi18);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
        }
        v23 = reinterpret_cast<unsigned char>(v23 & *reinterpret_cast<unsigned char*>(&r14d34));
        if (r15_6->f20 != r13_114) {
            fun_2480(r13_114, rsi18, r13_114, rsi18);
            rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8);
            goto addr_3814_48;
        }
        addr_3f58_113:
        if (!v130) {
            if (!r13_114) {
            }
        } else {
            if (!r13_114) {
                rax139 = fun_2590();
                rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
                rbx133 = rax139;
                goto addr_37a0_117;
            } else {
                rax140 = fun_2590();
                rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
                rbx133 = rax140;
                goto addr_37a0_117;
            }
        }
        addr_4294_121:
        rax141 = fun_2590();
        rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
        rbx133 = rax141;
        rax142 = v122;
        v122 = reinterpret_cast<void**>(0);
        v130 = rax142;
        addr_37a0_117:
        rax143 = quotearg_style(4, v29, 4, v29);
        r8_3 = v122;
        rcx25 = v130;
        rsi18 = rbx133;
        rdx20 = rax143;
        fun_27c0(1, rsi18, rdx20, rcx25, r8_3);
        fun_2480(v130, rsi18, v130, rsi18);
        fun_2480(v122, rsi18, v122, rsi18);
        rsp125 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp132) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_37df_127;
        addr_3d52_114:
        if (ebx47 != 4) 
            goto addr_440f_136;
        if (r13_114) 
            goto addr_3d64_138;
        rax144 = fun_2590();
        rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
        rbx133 = rax144;
        goto addr_37a0_117;
        addr_3d64_138:
        rax145 = fun_2590();
        rsp132 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8);
        rbx133 = rax145;
        goto addr_37a0_117;
        addr_4161_69:
        ebx47 = 1;
        goto addr_36e1_42;
    }
    rax146 = fun_24c0();
    if (*reinterpret_cast<void***>(rax146)) {
        eax147 = r15_6->f11;
        v23 = *reinterpret_cast<unsigned char*>(&eax147);
        if (!*reinterpret_cast<unsigned char*>(&eax147)) {
            rax148 = fun_2590();
            rsi18 = *reinterpret_cast<void***>(rax146);
            *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
            rdx20 = rax148;
            fun_27e0();
        } else {
            v23 = 0;
        }
    }
    eax149 = rpl_fts_close(v24, rsi18, rdx20, rcx25, r8_3);
    if (eax149) {
        fun_2590();
        fun_27e0();
        v23 = 0;
    }
    rax150 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(g28));
    if (rax150) {
        fun_25d0();
    } else {
        *reinterpret_cast<uint32_t*>(&rax151) = v23;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax151) + 4) = 0;
        return rax151;
    }
    addr_35c5_6:
    *reinterpret_cast<uint32_t*>(&rax152) = *reinterpret_cast<uint16_t*>(&rbx27);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax152) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xce60 + rax152 * 4) + 0xce60;
    addr_28f0_111:
    fun_24b0(rdi127, rdi127);
    addr_440f_136:
    goto addr_28f0_111;
}

int64_t file_name = 0;

void fun_4423(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4433(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_24e0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4443() {
    int64_t rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    int64_t rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_24c0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2590();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_44d3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_27e0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_24e0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_44d3_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_27e0();
    }
}

struct s12 {
    int64_t f0;
    int64_t f8;
};

struct s13 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_44f3(struct s12* rdi, struct s13* rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (rdi->f8 == rsi->f8) {
        rax3 = rsi->f0;
        *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f0 == rax3);
        return rax3;
    } else {
        return 0;
    }
}

struct s14 {
    signed char[8] pad8;
    int64_t f8;
};

uint64_t fun_4523(struct s14* rdi, int64_t rsi) {
    __asm__("cli ");
    return rdi->f8 % reinterpret_cast<uint64_t>(rsi);
}

uint64_t fun_4543(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_4553(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

struct s15 {
    signed char[120] pad120;
    uint64_t f78;
};

struct s16 {
    signed char[120] pad120;
    uint64_t f78;
};

int64_t fun_4563(struct s15** rdi, struct s16** rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>((*rdi)->f78 > (*rsi)->f78);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>((*rdi)->f78 < (*rsi)->f78)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

struct s17 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_26c0(void** rdi, void** rsi);

void i_ring_init(void*** rdi, int64_t rsi);

void** fun_5b43(struct s17* rdi, void** esi, void** rdx, void** rcx) {
    void** ebp5;
    void** rax6;
    void** r12_7;
    struct s17* rbx8;
    void** r14_9;
    void** rax10;
    void** eax11;
    void** rdi12;
    void** eax13;
    void** rsi14;
    unsigned char al15;
    unsigned char v16;
    void** rdi17;
    void** r15_18;
    void** v19;
    void** rax20;
    void** rdi21;
    uint32_t eax22;
    void** rax23;
    unsigned char al24;
    void** eax25;
    int64_t rdi26;
    int64_t rsi27;
    void** eax28;
    void** v29;
    void** r13_30;
    void** rbp31;
    uint32_t eax32;
    signed char v33;
    void** rax34;
    void** rdx35;
    void** rax36;
    void** rax37;
    void** rax38;
    void** r13_39;
    void** rdi40;
    void** rax41;
    void** rax42;
    unsigned char al43;
    struct s17* r15_44;
    void** r13_45;
    void** rax46;

    __asm__("cli ");
    if (reinterpret_cast<unsigned char>(esi) & 0xfffff000 || ((ebp5 = esi, (reinterpret_cast<unsigned char>(esi) & 0x204) == 0x204) || !(*reinterpret_cast<unsigned char*>(&esi) & 18))) {
        rax6 = fun_24c0();
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(22);
        goto addr_5d5e_3;
    }
    rbx8 = rdi;
    r14_9 = rdx;
    rax10 = fun_26c0(1, 0x80);
    r12_7 = rax10;
    if (!rax10) {
        addr_5d5e_3:
        return r12_7;
    } else {
        *reinterpret_cast<void***>(rax10 + 64) = r14_9;
        eax11 = ebp5;
        rdi12 = rbx8->f0;
        *reinterpret_cast<void***>(r12_7 + 44) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<unsigned char*>(&eax11 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax11 + 1) & 0xfd);
        eax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(eax11) | 4);
        if (!(*reinterpret_cast<unsigned char*>(&ebp5) & 2)) {
            eax13 = ebp5;
        }
        *reinterpret_cast<void***>(r12_7 + 72) = eax13;
        if (rdi12) 
            goto addr_5bca_8;
    }
    *reinterpret_cast<int32_t*>(&rsi14) = 0x1000;
    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
    addr_5bf9_10:
    al15 = fts_palloc(r12_7, rsi14, rdx);
    v16 = al15;
    if (!al15) {
        addr_5da6_11:
        rdi17 = r12_7;
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        fun_2480(rdi17, rsi14);
        goto addr_5d5e_3;
    } else {
        r15_18 = rbx8->f0;
        if (!r15_18) {
            v19 = reinterpret_cast<void**>(0);
        } else {
            rsi14 = reinterpret_cast<void**>(0xd401);
            rax20 = fts_alloc(r12_7, 0xd401, 0);
            v19 = rax20;
            if (!rax20) {
                addr_5d9c_15:
                rdi21 = *reinterpret_cast<void***>(r12_7 + 32);
                fun_2480(rdi21, rsi14);
                goto addr_5da6_11;
            } else {
                *reinterpret_cast<void***>(rax20 + 88) = reinterpret_cast<void**>(0xffffffffffffffff);
                r15_18 = rbx8->f0;
            }
        }
    }
    if (r14_9) {
        eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) >> 10 & 1;
        v16 = *reinterpret_cast<unsigned char*>(&eax22);
    }
    if (!r15_18) {
        rsi14 = reinterpret_cast<void**>(0xd401);
        rax23 = fts_alloc(r12_7, 0xd401, 0);
        *reinterpret_cast<void***>(r12_7) = rax23;
        if (!rax23) {
            addr_5d92_21:
            fun_2480(v19, rsi14);
            goto addr_5d9c_15;
        } else {
            *reinterpret_cast<void***>(rax23 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint16_t*>(rax23 + 0x68) = 9;
            *reinterpret_cast<void***>(rax23 + 88) = reinterpret_cast<void**>(1);
            al24 = setup_dir(r12_7, 0xd401, 9);
            if (al24) {
                addr_5e5f_23:
                eax25 = *reinterpret_cast<void***>(r12_7 + 72);
                if (!(reinterpret_cast<unsigned char>(eax25) & 0x204)) {
                    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r12_7 + 44);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(&eax25 + 1) & 2)) {
                        *reinterpret_cast<uint32_t*>(&rsi27) = reinterpret_cast<unsigned char>(eax25) << 13 & 0x20000 | 0x90900;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi27) + 4) = 0;
                        eax28 = open_safer(".", rsi27);
                    } else {
                        eax28 = openat_safer(rdi26, ".");
                    }
                    *reinterpret_cast<void***>(r12_7 + 40) = eax28;
                    if (reinterpret_cast<signed char>(eax28) < reinterpret_cast<signed char>(0)) {
                        *reinterpret_cast<void***>(r12_7 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) | 4);
                    }
                }
            } else {
                goto addr_5d92_21;
            }
        }
    } else {
        v29 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r13_30) = 0;
        *reinterpret_cast<int32_t*>(&r13_30 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp31) = 0;
        *reinterpret_cast<int32_t*>(&rbp31 + 4) = 0;
        eax32 = (reinterpret_cast<unsigned char>(ebp5) >> 11 ^ 1) & 1;
        v33 = *reinterpret_cast<signed char*>(&eax32);
        do {
            addr_5cfd_31:
            rax34 = fun_25b0(r15_18, r15_18);
            rdx35 = rax34;
            if (reinterpret_cast<unsigned char>(rax34) <= reinterpret_cast<unsigned char>(2) || (!v33 || *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rax34) + 0xffffffffffffffff) != 47)) {
                addr_5c80_32:
                rsi14 = r15_18;
                rax36 = fts_alloc(r12_7, rsi14, rdx35);
                if (!rax36) 
                    goto addr_5d8d_33;
            } else {
                do {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rdx35) + 0xfffffffffffffffe) != 47) 
                        goto addr_5c80_32;
                    --rdx35;
                } while (!reinterpret_cast<int1_t>(rdx35 == 1));
                goto addr_5d46_37;
            }
            *reinterpret_cast<void***>(rax36 + 88) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax36 + 8) = v19;
            *reinterpret_cast<void***>(rax36 + 48) = rax36 + 0x100;
            if (!rbp31 || !v16) {
                rax37 = fts_stat(r12_7, rax36, 0);
                *reinterpret_cast<uint16_t*>(rax36 + 0x68) = *reinterpret_cast<uint16_t*>(&rax37);
                if (r14_9) {
                    addr_5ce5_40:
                    *reinterpret_cast<void***>(rax36 + 16) = rbp31;
                    rbp31 = rax36;
                    continue;
                } else {
                    *reinterpret_cast<void***>(rax36 + 16) = reinterpret_cast<void**>(0);
                    if (!rbp31) {
                        ++r13_30;
                        v29 = rax36;
                        rbp31 = rax36;
                        r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_30) * 8);
                        if (r15_18) 
                            goto addr_5cfd_31; else 
                            goto addr_5dfd_43;
                    }
                }
            } else {
                *reinterpret_cast<int64_t*>(rax36 + 0xa0) = 2;
                *reinterpret_cast<uint16_t*>(rax36 + 0x68) = 11;
                if (r14_9) 
                    goto addr_5ce5_40;
                *reinterpret_cast<void***>(rax36 + 16) = reinterpret_cast<void**>(0);
            }
            rax38 = v29;
            v29 = rax36;
            *reinterpret_cast<void***>(rax38 + 16) = rax36;
            continue;
            addr_5d46_37:
            goto addr_5c80_32;
            ++r13_30;
            r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_30) * 8);
        } while (r15_18);
        goto addr_5e00_48;
    }
    i_ring_init(r12_7 + 96, 0xffffffff);
    goto addr_5d5e_3;
    addr_5d8d_33:
    while (rbp31) {
        r13_39 = rbp31;
        rbp31 = *reinterpret_cast<void***>(rbp31 + 16);
        rdi40 = *reinterpret_cast<void***>(r13_39 + 24);
        if (rdi40) {
            fun_2680(rdi40, rsi14);
        }
        fun_2480(r13_39, rsi14);
    }
    goto addr_5d92_21;
    addr_5e00_48:
    if (r14_9 && reinterpret_cast<unsigned char>(r13_30) > reinterpret_cast<unsigned char>(1)) {
        rax41 = fts_sort(r12_7, rbp31, r13_30, rcx);
        rbp31 = rax41;
    }
    rsi14 = reinterpret_cast<void**>(0xd401);
    rax42 = fts_alloc(r12_7, 0xd401, 0);
    *reinterpret_cast<void***>(r12_7) = rax42;
    if (!rax42) 
        goto addr_5d8d_33;
    *reinterpret_cast<void***>(rax42 + 16) = rbp31;
    *reinterpret_cast<uint16_t*>(rax42 + 0x68) = 9;
    *reinterpret_cast<void***>(rax42 + 88) = reinterpret_cast<void**>(1);
    al43 = setup_dir(r12_7, 0xd401, 0);
    if (!al43) 
        goto addr_5d8d_33; else 
        goto addr_5e5f_23;
    addr_5dfd_43:
    goto addr_5e00_48;
    addr_5bca_8:
    r15_44 = rbx8;
    *reinterpret_cast<int32_t*>(&r13_45) = 0;
    *reinterpret_cast<int32_t*>(&r13_45 + 4) = 0;
    do {
        rax46 = fun_25b0(rdi12, rdi12);
        if (reinterpret_cast<unsigned char>(r13_45) < reinterpret_cast<unsigned char>(rax46)) {
            r13_45 = rax46;
        }
        rdi12 = r15_44->f8;
        r15_44 = reinterpret_cast<struct s17*>(&r15_44->f8);
    } while (rdi12);
    rsi14 = r13_45 + 1;
    if (reinterpret_cast<unsigned char>(rsi14) >= reinterpret_cast<unsigned char>(0x1000)) 
        goto addr_5bf9_10;
    rsi14 = reinterpret_cast<void**>(0x1000);
    goto addr_5bf9_10;
}

void hash_free();

int64_t fun_5f63(void** rdi, void** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void** rbp6;
    void** rbx7;
    void** rbp8;
    void** rdi9;
    void** rdi10;
    void** rdi11;
    void** eax12;
    int32_t eax13;
    void** rax14;
    void** r13d15;
    void** rbx16;
    int32_t eax17;
    void*** rbx18;
    unsigned char al19;
    void** eax20;
    void** rdi21;
    void** rax22;
    int64_t rax23;
    int32_t eax24;
    void** rax25;
    int32_t eax26;
    void** rax27;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *reinterpret_cast<void***>(rdi);
    if (rdi5) {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi5 + 88)) >= reinterpret_cast<signed char>(0)) {
            while (1) {
                rbp6 = *reinterpret_cast<void***>(rdi5 + 16);
                if (rbp6) {
                    fun_2480(rdi5, rsi);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                } else {
                    rbp6 = *reinterpret_cast<void***>(rdi5 + 8);
                    fun_2480(rdi5, rsi);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                }
                rdi5 = rbp6;
            }
        } else {
            rbp6 = rdi5;
        }
        fun_2480(rbp6, rsi);
    }
    rbx7 = *reinterpret_cast<void***>(r12_4 + 8);
    if (rbx7) {
        do {
            rbp8 = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdi9 = *reinterpret_cast<void***>(rbp8 + 24);
            if (rdi9) {
                fun_2680(rdi9, rsi);
            }
            fun_2480(rbp8, rsi);
        } while (rbx7);
    }
    rdi10 = *reinterpret_cast<void***>(r12_4 + 16);
    fun_2480(rdi10, rsi);
    rdi11 = *reinterpret_cast<void***>(r12_4 + 32);
    fun_2480(rdi11, rsi);
    eax12 = *reinterpret_cast<void***>(r12_4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
        if (!(*reinterpret_cast<unsigned char*>(&eax12) & 4)) {
            eax13 = fun_2790();
            if (eax13) {
                rax14 = fun_24c0();
                r13d15 = *reinterpret_cast<void***>(rax14);
                rbx16 = rax14;
                eax17 = fun_2660();
                if (r13d15 || !eax17) {
                    addr_601c_19:
                    rbx18 = reinterpret_cast<void***>(r12_4 + 96);
                } else {
                    addr_6128_20:
                    r13d15 = *reinterpret_cast<void***>(rbx16);
                    goto addr_601c_19;
                }
                while (al19 = i_ring_empty(rbx18, rsi, rdx), al19 == 0) {
                    eax20 = i_ring_pop(rbx18, rsi, rdx);
                    if (reinterpret_cast<signed char>(eax20) < reinterpret_cast<signed char>(0)) 
                        continue;
                    fun_2660();
                }
                if (*reinterpret_cast<void***>(r12_4 + 80)) {
                    hash_free();
                }
                rdi21 = *reinterpret_cast<void***>(r12_4 + 88);
                if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_4 + 72)) & 0x102)) {
                    fun_2480(rdi21, rsi);
                } else {
                    if (rdi21) {
                        hash_free();
                    }
                }
                fun_2480(r12_4, rsi);
                if (r13d15) {
                    rax22 = fun_24c0();
                    *reinterpret_cast<void***>(rax22) = r13d15;
                    r13d15 = reinterpret_cast<void**>(0xffffffff);
                }
                *reinterpret_cast<void***>(&rax23) = r13d15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
                return rax23;
            } else {
                eax24 = fun_2660();
                if (eax24) {
                    rax25 = fun_24c0();
                    rbx16 = rax25;
                    goto addr_6128_20;
                }
            }
        }
    } else {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_4 + 44)) >= reinterpret_cast<signed char>(0) && (eax26 = fun_2660(), !!eax26)) {
            rax27 = fun_24c0();
            r13d15 = *reinterpret_cast<void***>(rax27);
            goto addr_601c_19;
        }
    }
    r13d15 = reinterpret_cast<void**>(0);
    goto addr_601c_19;
}

struct s18 {
    signed char f0;
    void** f1;
};

void** fun_2620(void** rdi, void** rsi, void** rdx);

void** fun_6153(void** rdi, void** rsi) {
    void** r12_3;
    void** edx4;
    void** rbp5;
    uint32_t eax6;
    void** rax7;
    void** rcx8;
    int32_t eax9;
    void** rdx10;
    void** rax11;
    void** eax12;
    int64_t rdi13;
    int64_t rsi14;
    void** eax15;
    void** r13_16;
    uint32_t eax17;
    void** r13_18;
    void** rax19;
    void** r14_20;
    void** rdi21;
    int32_t eax22;
    void** rax23;
    void** eax24;
    void** rax25;
    void** rax26;
    void** eax27;
    void** rax28;
    int64_t r8_29;
    void** rax30;
    void** rax31;
    void** r14_32;
    void** rdx33;
    void** rax34;
    void** rax35;
    void** rax36;
    void** rsi37;
    void** rdi38;
    struct s18* rdi39;
    void** rdi40;
    uint32_t eax41;
    void** rax42;
    uint32_t eax43;
    void** eax44;
    int32_t eax45;
    void** rax46;
    void** esi47;
    void** rsi48;
    int32_t eax49;
    uint32_t eax50;
    void** rdi51;
    void** rax52;
    void** rdi53;
    void** r14_54;
    void** rsi55;
    void** rax56;
    void** rax57;
    void** r13_58;
    void** rax59;
    void** rax60;
    void** eax61;
    int64_t rdi62;
    int64_t rsi63;
    void** eax64;
    void** rax65;
    void** eax66;
    void** r13_67;
    void** r14_68;
    void** rdi69;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi);
    if (!r12_3) 
        goto addr_6278_2;
    edx4 = *reinterpret_cast<void***>(rdi + 72);
    rbp5 = rdi;
    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 32) 
        goto addr_6278_2;
    eax6 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
    if (*reinterpret_cast<int16_t*>(&eax6) == 1) {
        rax7 = fts_stat(rdi, r12_3, 0);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax7);
        goto addr_627b_6;
    }
    *reinterpret_cast<uint32_t*>(&rcx8) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&eax6) != 2) 
        goto addr_61a2_8;
    eax9 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx8 + 0xfffffffffffffff4));
    if (*reinterpret_cast<uint16_t*>(&eax9) <= 1) {
        *reinterpret_cast<uint32_t*>(&rdx10) = 1;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fts_stat(rdi, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax11);
        if (*reinterpret_cast<uint16_t*>(&rax11) != 1) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            if (*reinterpret_cast<uint16_t*>(&rax11) != 11) 
                goto addr_627b_6;
            goto addr_6528_13;
        }
        eax12 = *reinterpret_cast<void***>(rbp5 + 72);
        if (*reinterpret_cast<unsigned char*>(&eax12) & 4) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            goto addr_64bf_16;
        }
        *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(rbp5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(eax12) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
            *reinterpret_cast<uint32_t*>(&rsi14) = *reinterpret_cast<uint32_t*>(&rdx10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi14) + 4) = 0;
            eax15 = open_safer(".", rsi14);
        } else {
            eax15 = openat_safer(rdi13, ".");
        }
        *reinterpret_cast<void***>(r12_3 + 68) = eax15;
        if (reinterpret_cast<signed char>(eax15) >= reinterpret_cast<signed char>(0)) 
            goto addr_6826_21;
    } else {
        if (*reinterpret_cast<int16_t*>(&rcx8) != 1) {
            do {
                addr_61d8_23:
                r13_16 = r12_3;
                r12_3 = *reinterpret_cast<void***>(r12_3 + 16);
                if (!r12_3) 
                    goto addr_61e5_24;
                *reinterpret_cast<void***>(rbp5) = r12_3;
                fun_2480(r13_16, rsi);
                if (!*reinterpret_cast<void***>(r12_3 + 88)) 
                    goto addr_63b0_26;
                eax17 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
            } while (*reinterpret_cast<int16_t*>(&eax17) == 4);
            goto addr_6460_28;
        } else {
            addr_62a7_29:
            if (!(*reinterpret_cast<unsigned char*>(&edx4) & 64) || *reinterpret_cast<void***>(r12_3 + 0x70) == *reinterpret_cast<void***>(rbp5 + 24)) {
                r13_18 = *reinterpret_cast<void***>(rbp5 + 8);
                if (!r13_18) {
                    addr_659a_31:
                    rax19 = fts_build(rbp5, 3);
                    *reinterpret_cast<void***>(rbp5 + 8) = rax19;
                    if (!rax19) {
                        if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) {
                            if (*reinterpret_cast<void***>(r12_3 + 64) && *reinterpret_cast<uint16_t*>(r12_3 + 0x68) != 4) {
                                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                            }
                            leave_dir(rbp5, r12_3);
                            goto addr_627b_6;
                        }
                    } else {
                        r12_3 = rax19;
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 16) {
                        *reinterpret_cast<unsigned char*>(&edx4 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx4 + 1) & 0xef);
                        *reinterpret_cast<void***>(rbp5 + 72) = edx4;
                        do {
                            r14_20 = r13_18;
                            r13_18 = *reinterpret_cast<void***>(r13_18 + 16);
                            rdi21 = *reinterpret_cast<void***>(r14_20 + 24);
                            if (rdi21) {
                                fun_2680(rdi21, rsi);
                            }
                            fun_2480(r14_20, rsi);
                        } while (r13_18);
                        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                        goto addr_659a_31;
                    } else {
                        rcx8 = *reinterpret_cast<void***>(r12_3 + 48);
                        eax22 = fts_safe_changedir(rbp5, r12_3, 0xffffffff, rcx8);
                        if (!eax22) {
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                        } else {
                            rax23 = fun_24c0();
                            eax24 = *reinterpret_cast<void***>(rax23);
                            *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 1);
                            *reinterpret_cast<void***>(r12_3 + 64) = eax24;
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                            if (r12_3) {
                                rax25 = r12_3;
                                do {
                                    *reinterpret_cast<void***>(rax25 + 48) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rax25 + 8) + 48);
                                    rax25 = *reinterpret_cast<void***>(rax25 + 16);
                                } while (rax25);
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                goto addr_646a_50;
            } else {
                addr_6342_51:
                if (*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) & 2) {
                    fun_2660();
                    goto addr_634e_53;
                }
            }
        }
    }
    rax26 = fun_24c0();
    eax27 = *reinterpret_cast<void***>(rax26);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
    *reinterpret_cast<void***>(r12_3 + 64) = eax27;
    *reinterpret_cast<void***>(rbp5) = r12_3;
    goto addr_627b_6;
    addr_6826_21:
    *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    addr_64ab_55:
    *reinterpret_cast<void***>(rbp5) = r12_3;
    if (*reinterpret_cast<uint16_t*>(&rax28) == 11) {
        addr_6528_13:
        if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) == 2) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax28 = fts_stat(rbp5, r12_3, 0, rbp5, r12_3, 0);
            *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax28);
            goto addr_64b5_57;
        } else {
            if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) != 1) {
                goto 0x2904;
            }
        }
    } else {
        addr_64b5_57:
        if (*reinterpret_cast<uint16_t*>(&rax28) != 1) {
            addr_627b_6:
            return r12_3;
        } else {
            addr_64bf_16:
            if (!*reinterpret_cast<void***>(r12_3 + 88)) {
                *reinterpret_cast<void***>(rbp5 + 24) = *reinterpret_cast<void***>(r12_3 + 0x70);
            }
        }
    }
    rax30 = enter_dir(rbp5, r12_3, rdx10, rcx8, r8_29);
    if (!*reinterpret_cast<signed char*>(&rax30)) {
        rax31 = fun_24c0();
        *reinterpret_cast<int32_t*>(&r12_3) = 0;
        *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
        *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(12);
        goto addr_627b_6;
    }
    addr_61e5_24:
    r14_32 = *reinterpret_cast<void***>(r13_16 + 8);
    if (*reinterpret_cast<void***>(r14_32 + 24)) {
        rdx33 = *reinterpret_cast<void***>(rbp5 + 32);
        rax34 = *reinterpret_cast<void***>(r14_32 + 72);
        *reinterpret_cast<void***>(rbp5) = r14_32;
        *reinterpret_cast<int32_t*>(&rsi) = 3;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx33) + reinterpret_cast<unsigned char>(rax34)) = 0;
        rax35 = fts_build(rbp5, 3);
        if (rax35) {
            r12_3 = rax35;
            fun_2480(r13_16, 3);
        } else {
            if (*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32) {
                addr_6278_2:
                *reinterpret_cast<int32_t*>(&r12_3) = 0;
                *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
                goto addr_627b_6;
            } else {
                r14_32 = *reinterpret_cast<void***>(r13_16 + 8);
                goto addr_61f4_67;
            }
        }
    } else {
        addr_61f4_67:
        *reinterpret_cast<void***>(rbp5) = r14_32;
        fun_2480(r13_16, rsi);
        if (*reinterpret_cast<void***>(r14_32 + 88) == 0xffffffffffffffff) {
            fun_2480(r14_32, rsi);
            rax36 = fun_24c0();
            *reinterpret_cast<void***>(rax36) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0);
            goto addr_627b_6;
        }
    }
    addr_646a_50:
    rsi37 = r12_3 + 0x100;
    rdi38 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72) + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 56)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72)) + 0xffffffffffffffff) != 47) {
        rdi38 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72);
    }
    rdi39 = reinterpret_cast<struct s18*>(reinterpret_cast<unsigned char>(rdi38) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)));
    rdi39->f0 = 47;
    rdi40 = reinterpret_cast<void**>(&rdi39->f1);
    rdx10 = *reinterpret_cast<void***>(r12_3 + 96) + 1;
    fun_27d0(rdi40, rsi37, rdx10, rdi40, rsi37, rdx10);
    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    goto addr_64ab_55;
    if (*reinterpret_cast<uint16_t*>(r14_32 + 0x68) == 11) 
        goto 0x2904;
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_32 + 72))) = 0;
    if (*reinterpret_cast<void***>(r14_32 + 88)) 
        goto addr_622e_73;
    eax41 = restore_initial_cwd(rbp5, rsi);
    if (!eax41) {
        addr_6243_75:
        if (*reinterpret_cast<uint16_t*>(r14_32 + 0x68) != 2) {
            if (*reinterpret_cast<void***>(r14_32 + 64)) {
                *reinterpret_cast<uint16_t*>(r14_32 + 0x68) = 7;
            } else {
                *reinterpret_cast<uint16_t*>(r14_32 + 0x68) = 6;
                leave_dir(rbp5, r14_32);
            }
        }
    } else {
        addr_6688_79:
        rax42 = fun_24c0();
        *reinterpret_cast<void***>(r14_32 + 64) = *reinterpret_cast<void***>(rax42);
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_6243_75;
    }
    r12_3 = r14_32;
    if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) 
        goto addr_627b_6;
    goto addr_6278_2;
    addr_622e_73:
    eax43 = *reinterpret_cast<unsigned char*>(r14_32 + 0x6a);
    if (*reinterpret_cast<unsigned char*>(&eax43) & 2) {
        eax44 = *reinterpret_cast<void***>(rbp5 + 72);
        if (!(*reinterpret_cast<unsigned char*>(&eax44) & 4)) {
            if (!(*reinterpret_cast<unsigned char*>(&eax44 + 1) & 2)) {
                eax45 = fun_2790();
                if (eax45) {
                    rax46 = fun_24c0();
                    *reinterpret_cast<void***>(r14_32 + 64) = *reinterpret_cast<void***>(rax46);
                    *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
                }
            } else {
                esi47 = *reinterpret_cast<void***>(r14_32 + 68);
                cwd_advance_fd(rbp5, esi47, 1);
            }
        }
        fun_2660();
        goto addr_6243_75;
    } else {
        if (*reinterpret_cast<unsigned char*>(&eax43) & 1) 
            goto addr_6243_75;
        rsi48 = *reinterpret_cast<void***>(r14_32 + 8);
        eax49 = fts_safe_changedir(rbp5, rsi48, 0xffffffff, "..");
        if (!eax49) 
            goto addr_6243_75;
        goto addr_6688_79;
    }
    addr_63b0_26:
    eax50 = restore_initial_cwd(rbp5, rsi);
    if (eax50) {
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_6278_2;
    }
    rdi51 = *reinterpret_cast<void***>(rbp5 + 88);
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) & 0x102)) {
        fun_2480(rdi51, rsi);
    } else {
        if (rdi51) {
            hash_free();
        }
    }
    rax52 = *reinterpret_cast<void***>(r12_3 + 96);
    rdi53 = *reinterpret_cast<void***>(rbp5 + 32);
    r14_54 = r12_3 + 0x100;
    *reinterpret_cast<void***>(r12_3 + 72) = rax52;
    rdx10 = rax52 + 1;
    fun_27d0(rdi53, r14_54, rdx10);
    *reinterpret_cast<int32_t*>(&rsi55) = 47;
    *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    rax56 = fun_2620(r14_54, 47, rdx10);
    if (!rax56) 
        goto addr_643b_98;
    if (r14_54 == rax56) {
        if (!*reinterpret_cast<void***>(r14_54 + 1)) {
            addr_643b_98:
            rax57 = *reinterpret_cast<void***>(rbp5 + 32);
            *reinterpret_cast<void***>(r12_3 + 56) = rax57;
            *reinterpret_cast<void***>(r12_3 + 48) = rax57;
            setup_dir(rbp5, rsi55, rdx10);
            *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
            goto addr_64ab_55;
        } else {
            goto addr_6418_102;
        }
    } else {
        addr_6418_102:
        r13_58 = rax56 + 1;
        rax59 = fun_25b0(r13_58, r13_58);
        rsi55 = r13_58;
        rdx10 = rax59 + 1;
        fun_27d0(r14_54, rsi55, rdx10);
        *reinterpret_cast<void***>(r12_3 + 96) = rax59;
        goto addr_643b_98;
    }
    addr_6460_28:
    if (*reinterpret_cast<int16_t*>(&eax17) == 2) {
        rax60 = fts_stat(rbp5, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax60);
        if (*reinterpret_cast<uint16_t*>(&rax60) == 1 && (eax61 = *reinterpret_cast<void***>(rbp5 + 72), !(*reinterpret_cast<unsigned char*>(&eax61) & 4))) {
            *reinterpret_cast<void***>(&rdi62) = *reinterpret_cast<void***>(rbp5 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi62) + 4) = 0;
            if (!(*reinterpret_cast<unsigned char*>(&eax61 + 1) & 2)) {
                *reinterpret_cast<uint32_t*>(&rsi63) = reinterpret_cast<unsigned char>(eax61) << 13 & 0x20000 | 0x90900;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi63) + 4) = 0;
                eax64 = open_safer(".", rsi63);
            } else {
                eax64 = openat_safer(rdi62, ".");
            }
            *reinterpret_cast<void***>(r12_3 + 68) = eax64;
            if (reinterpret_cast<signed char>(eax64) < reinterpret_cast<signed char>(0)) {
                rax65 = fun_24c0();
                eax66 = *reinterpret_cast<void***>(rax65);
                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                *reinterpret_cast<void***>(r12_3 + 64) = eax66;
            } else {
                *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
            }
        }
        *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
        goto addr_646a_50;
    }
    addr_634e_53:
    r13_67 = *reinterpret_cast<void***>(rbp5 + 8);
    if (r13_67) {
        do {
            r14_68 = r13_67;
            r13_67 = *reinterpret_cast<void***>(r13_67 + 16);
            rdi69 = *reinterpret_cast<void***>(r14_68 + 24);
            if (rdi69) {
                fun_2680(rdi69, rsi);
            }
            fun_2480(r14_68, rsi);
        } while (r13_67);
        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
    }
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 6;
    leave_dir(rbp5, r12_3);
    goto addr_627b_6;
    addr_61a2_8:
    if (*reinterpret_cast<int16_t*>(&rcx8) != 1) 
        goto addr_61d8_23;
    if (*reinterpret_cast<int16_t*>(&eax6) != 4) 
        goto addr_62a7_29; else 
        goto addr_6342_51;
}

struct s19 {
    signed char[108] pad108;
    int16_t f6c;
};

int64_t fun_68a3() {
    uint32_t edx1;
    void** rax2;
    struct s19* rsi3;
    int16_t dx4;

    __asm__("cli ");
    if (edx1 > 4) {
        rax2 = fun_24c0();
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(22);
        return 1;
    } else {
        rsi3->f6c = dx4;
        return 0;
    }
}

void** fun_68d3(void** rdi, void** rsi) {
    uint32_t r13d3;
    void** rbp4;
    void** rax5;
    void** r14_6;
    void** r15_7;
    uint32_t edx8;
    void** rax9;
    void** rbx10;
    void** r12_11;
    void** rdi12;
    int32_t r12d13;
    void** eax14;
    void** rsi15;
    int64_t rdi16;
    int64_t rsi17;
    void** eax18;
    void** r13d19;
    void** eax20;
    void** rsi21;
    void** rax22;
    int32_t eax23;
    void** ebx24;

    __asm__("cli ");
    r13d3 = *reinterpret_cast<uint32_t*>(&rsi);
    rbp4 = rdi;
    rax5 = fun_24c0();
    r14_6 = rax5;
    if (r13d3 & 0xffffefff) {
        *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(22);
        return 0;
    }
    r15_7 = *reinterpret_cast<void***>(rbp4);
    *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 32) {
        return 0;
    }
    edx8 = *reinterpret_cast<uint16_t*>(r15_7 + 0x68);
    if (*reinterpret_cast<int16_t*>(&edx8) == 9) {
        return *reinterpret_cast<void***>(r15_7 + 16);
    }
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&edx8) == 1) 
        goto addr_6928_8;
    addr_699d_9:
    return rax9;
    addr_6928_8:
    rbx10 = *reinterpret_cast<void***>(rbp4 + 8);
    if (rbx10) {
        do {
            r12_11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 16);
            rdi12 = *reinterpret_cast<void***>(r12_11 + 24);
            if (rdi12) {
                fun_2680(rdi12, rsi);
            }
            fun_2480(r12_11, rsi);
        } while (rbx10);
    }
    r12d13 = 1;
    if (r13d3 == 0x1000) {
        *reinterpret_cast<void***>(rbp4 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 72)) | 0x1000);
        r12d13 = 2;
    }
    if (*reinterpret_cast<void***>(r15_7 + 88)) 
        goto addr_698e_17;
    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(r15_7 + 48)) == 47) 
        goto addr_698e_17;
    eax14 = *reinterpret_cast<void***>(rbp4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax14) & 4)) 
        goto addr_69b0_20;
    addr_698e_17:
    *reinterpret_cast<int32_t*>(&rsi15) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    rax9 = fts_build(rbp4, rsi15);
    *reinterpret_cast<void***>(rbp4 + 8) = rax9;
    goto addr_699d_9;
    addr_69b0_20:
    *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rbp4 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&eax14 + 1) & 2)) {
        *reinterpret_cast<uint32_t*>(&rsi17) = reinterpret_cast<unsigned char>(eax14) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        eax18 = open_safer(".", rsi17);
        r13d19 = eax18;
    } else {
        eax20 = openat_safer(rdi16, ".");
        r13d19 = eax20;
    }
    if (reinterpret_cast<signed char>(r13d19) >= reinterpret_cast<signed char>(0)) 
        goto addr_69e7_24;
    *reinterpret_cast<void***>(rbp4 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    goto addr_699d_9;
    addr_69e7_24:
    *reinterpret_cast<int32_t*>(&rsi21) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    rax22 = fts_build(rbp4, rsi21);
    *reinterpret_cast<void***>(rbp4 + 8) = rax22;
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 2) {
        cwd_advance_fd(rbp4, r13d19, 1);
    } else {
        eax23 = fun_2790();
        if (eax23) {
            ebx24 = *reinterpret_cast<void***>(r14_6);
            fun_2660();
            *reinterpret_cast<int32_t*>(&rax9) = 0;
            *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
            *reinterpret_cast<void***>(r14_6) = ebx24;
            goto addr_699d_9;
        } else {
            fun_2660();
        }
    }
    rax9 = *reinterpret_cast<void***>(rbp4 + 8);
    goto addr_699d_9;
}

uint64_t fun_6ad3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_6af3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s20 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_6f53(struct s20* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s21 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_6f63(struct s21* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s22 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_6f73(struct s22* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s25 {
    signed char[8] pad8;
    struct s25* f8;
};

struct s24 {
    int64_t f0;
    struct s25* f8;
};

struct s23 {
    struct s24* f0;
    struct s24* f8;
};

uint64_t fun_6f83(struct s23* rdi) {
    struct s24* rcx2;
    struct s24* rsi3;
    uint64_t r8_4;
    struct s25* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s28 {
    signed char[8] pad8;
    struct s28* f8;
};

struct s27 {
    int64_t f0;
    struct s28* f8;
};

struct s26 {
    struct s27* f0;
    struct s27* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_6fe3(struct s26* rdi) {
    struct s27* rcx2;
    struct s27* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s28* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s31 {
    signed char[8] pad8;
    struct s31* f8;
};

struct s30 {
    int64_t f0;
    struct s31* f8;
};

struct s29 {
    struct s30* f0;
    struct s30* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_7053(struct s29* rdi, int64_t rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r13_5;
    int64_t v6;
    int64_t r12_7;
    uint64_t r12_8;
    int64_t v9;
    int64_t rbp10;
    int64_t rbp11;
    int64_t v12;
    int64_t rbx13;
    struct s30* rcx14;
    struct s30* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s31* rax19;
    uint64_t rdx20;
    void** r9_21;
    int64_t v22;
    void** r9_23;
    int64_t v24;
    void** r9_25;
    int64_t v26;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_2890(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_2890(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x5e0c]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_710a_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_7189_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_2890(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_2890;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x5e8b]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_7189_14; else 
            goto addr_710a_13;
    }
}

struct s32 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s33 {
    int64_t f0;
    struct s33* f8;
};

int64_t fun_71b3(struct s32* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s32* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s33* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x2913;
    rbx7 = reinterpret_cast<struct s33*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_7218_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_720b_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_720b_7;
    }
    addr_721b_10:
    return r12_3;
    addr_7218_5:
    r12_3 = rbx7->f0;
    goto addr_721b_10;
    addr_720b_7:
    return 0;
}

struct s34 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_7233(struct s34* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x2918;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_726f_7;
    return *rax2;
    addr_726f_7:
    goto 0x2918;
}

struct s36 {
    int64_t f0;
    struct s36* f8;
};

struct s35 {
    int64_t* f0;
    struct s36* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_7283(struct s35* rdi, int64_t rsi) {
    struct s35* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s36* rax7;
    struct s36* rdx8;
    struct s36* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x291e;
    rax7 = reinterpret_cast<struct s36*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_72ce_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_72ce_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_72ec_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_72ec_10:
    return r8_10;
}

struct s38 {
    int64_t f0;
    struct s38* f8;
};

struct s37 {
    struct s38* f0;
    struct s38* f8;
};

void fun_7313(struct s37* rdi, int64_t rsi, uint64_t rdx) {
    struct s38* r9_4;
    uint64_t rax5;
    struct s38* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_7352_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_7352_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s40 {
    int64_t f0;
    struct s40* f8;
};

struct s39 {
    struct s40* f0;
    struct s40* f8;
};

int64_t fun_7363(struct s39* rdi, int64_t rsi, int64_t rdx) {
    struct s40* r14_4;
    int64_t r12_5;
    struct s39* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s40* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_738f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_73d1_10;
            }
            addr_738f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_7399_11:
    return r12_5;
    addr_73d1_10:
    goto addr_7399_11;
}

uint64_t fun_73e3(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s41 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_7423(struct s41* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_7453(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    void** rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    void** rax12;
    void** rax13;
    void** rdi14;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x6ad0);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<void**>(0x6af0);
    }
    rax9 = fun_2750(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0xcf60);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((*reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax12 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&rsi)), *reinterpret_cast<void***>(r12_10 + 16) = rax12, rax12 == 0) || (*reinterpret_cast<uint32_t*>(&rsi) = 16, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax13 = fun_26c0(rax12, 16), *reinterpret_cast<void***>(r12_10) = rax13, rax13 == 0))) {
            rdi14 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_2480(rdi14, rsi);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<void***>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s44 {
    int64_t f0;
    struct s44* f8;
};

struct s43 {
    int64_t f0;
    struct s44* f8;
};

struct s42 {
    struct s43* f0;
    struct s43* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s44* f48;
};

void fun_7553(struct s42* rdi) {
    struct s42* rbp2;
    struct s43* r12_3;
    struct s44* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s44* rax7;
    struct s44* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s45 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_7603(struct s45* rdi, void** rsi) {
    struct s45* r12_3;
    void** r13_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rdi9;
    void** rbx10;
    void** rbx11;
    void** rdi12;
    void** rdi13;

    __asm__("cli ");
    r12_3 = rdi;
    r13_4 = rdi->f0;
    rax5 = rdi->f8;
    rbp6 = r13_4;
    if (!rdi->f40 || !rdi->f20) {
        addr_7673_2:
        if (reinterpret_cast<unsigned char>(rax5) > reinterpret_cast<unsigned char>(rbp6)) {
            do {
                rbx7 = *reinterpret_cast<void***>(rbp6 + 8);
                if (rbx7) {
                    do {
                        rdi8 = rbx7;
                        rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
                        fun_2480(rdi8, rsi);
                    } while (rbx7);
                }
                rbp6 = rbp6 + 16;
            } while (reinterpret_cast<unsigned char>(r12_3->f8) > reinterpret_cast<unsigned char>(rbp6));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) < reinterpret_cast<unsigned char>(rax5)) {
            while (1) {
                rdi9 = *reinterpret_cast<void***>(r13_4);
                if (!rdi9) {
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                } else {
                    rbx10 = r13_4;
                    while (r12_3->f40(rdi9), rbx10 = *reinterpret_cast<void***>(rbx10 + 8), !!rbx10) {
                        rdi9 = *reinterpret_cast<void***>(rbx10);
                    }
                    rax5 = r12_3->f8;
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                }
            }
            rbp6 = r12_3->f0;
            goto addr_7673_2;
        }
    }
    rbx11 = r12_3->f48;
    if (rbx11) {
        do {
            rdi12 = rbx11;
            rbx11 = *reinterpret_cast<void***>(rbx11 + 8);
            fun_2480(rdi12, rsi);
        } while (rbx11);
    }
    rdi13 = r12_3->f0;
    fun_2480(rdi13, rsi);
    goto fun_2480;
}

int64_t fun_76f3(void** rdi, uint64_t rsi) {
    void** r12_3;
    void** rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    void** r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi + 40);
    rax4 = g28;
    esi5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3 + 16));
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_7830_2;
    if (*reinterpret_cast<void***>(rdi + 16) == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_26c0(rax6, 16);
        if (!rax8) {
            addr_7830_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8) - 8 + 8);
            v10 = *reinterpret_cast<void***>(rdi + 72);
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = *reinterpret_cast<void***>(rdi);
                fun_2480(rdi12, rdi);
                *reinterpret_cast<void***>(rdi) = rax8;
                *reinterpret_cast<void***>(rdi + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                *reinterpret_cast<void***>(rdi + 16) = rax6;
                *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rdi + 72) = v10;
            } else {
                *reinterpret_cast<void***>(rdi + 72) = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x2923;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x2923;
                fun_2480(rax8, r13_9);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        fun_25d0();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s46 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_7883(void** rdi, void** rsi, void*** rdx) {
    void** rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s46* v17;
    int32_t r8d18;
    void** rax19;
    void* rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x2928;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_799e_5; else 
                goto addr_790f_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_790f_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_799e_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x5591]");
            if (!cf14) 
                goto addr_79f5_12;
            __asm__("comiss xmm4, [rip+0x5541]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x5500]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_79f5_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x2928;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_2750(16, rsi, rdx6);
                if (!rax19) {
                    addr_79f5_12:
                    r8d18 = -1;
                } else {
                    goto addr_7952_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_7952_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_78ce_28:
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_25d0();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_7952_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_78ce_28;
}

int32_t hash_insert_if_absent();

int64_t fun_7aa3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_25d0();
    } else {
        return rax3;
    }
}

void** fun_7b03(void** rdi, void** rsi) {
    void** rbx3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_7b90_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_7c46_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x53ae]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x5318]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_2480(rdi15, rsi, rdi15, rsi);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_7c46_5; else 
                goto addr_7b90_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (rax16) {
        fun_25d0();
    } else {
        return r12_7;
    }
}

void fun_7c93() {
    __asm__("cli ");
    goto hash_remove;
}

struct s47 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int64_t f14;
    signed char f1c;
};

void fun_7ca3(struct s47* rdi, int32_t esi) {
    __asm__("cli ");
    rdi->f14 = 0;
    rdi->f1c = 1;
    rdi->f0 = esi;
    rdi->f4 = esi;
    rdi->f8 = esi;
    rdi->fc = esi;
    rdi->f10 = esi;
    return;
}

struct s48 {
    signed char[28] pad28;
    unsigned char f1c;
};

int64_t fun_7cc3(struct s48* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rax2) = rdi->f1c;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

struct s49 {
    int32_t f0;
    signed char[16] pad20;
    uint32_t f14;
    uint32_t f18;
    unsigned char f1c;
};

int64_t fun_7cd3(struct s49* rdi, int32_t esi) {
    uint32_t eax3;
    uint32_t eax4;
    uint32_t edx5;
    int64_t rcx6;
    int32_t r8d7;
    uint32_t ecx8;
    int64_t rax9;

    __asm__("cli ");
    eax3 = static_cast<uint32_t>(rdi->f1c) ^ 1;
    eax4 = *reinterpret_cast<unsigned char*>(&eax3);
    edx5 = rdi->f14 + eax4 & 3;
    *reinterpret_cast<uint32_t*>(&rcx6) = edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    r8d7 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4) = esi;
    ecx8 = rdi->f18;
    rdi->f14 = edx5;
    if (ecx8 == edx5) {
        rdi->f18 = eax4 + ecx8 & 3;
    }
    rdi->f1c = 0;
    *reinterpret_cast<int32_t*>(&rax9) = r8d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

struct s50 {
    int32_t f0;
    signed char[12] pad16;
    int32_t f10;
    uint32_t f14;
    uint32_t f18;
    signed char f1c;
};

int64_t fun_7d13(struct s50* rdi) {
    int64_t rdx2;
    int32_t r8d3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f1c) 
        goto 0x292d;
    *reinterpret_cast<uint32_t*>(&rdx2) = rdi->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx2) + 4) = 0;
    r8d3 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4);
    rax4 = rdx2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4) = rdi->f10;
    if (*reinterpret_cast<uint32_t*>(&rdx2) == rdi->f18) {
        rdi->f1c = 1;
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        rdi->f14 = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax4) + 3) & 3;
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s51 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_7d53(uint64_t rdi, struct s51* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

int32_t fun_25c0();

void fd_safer(int64_t rdi);

void fun_7db3() {
    void** rax1;
    unsigned char dl2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (dl2 & 64) {
    }
    eax3 = fun_25c0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_25d0();
    } else {
        return;
    }
}

int64_t fun_2830(int64_t rdi);

int64_t fun_7e33(int64_t rdi, void** rsi, int32_t edx, void*** rcx) {
    int64_t r12_5;
    void** eax6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;
    void** r13d10;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    eax6 = openat_safer(rdi, rsi);
    if (reinterpret_cast<signed char>(eax6) >= reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<void***>(&rdi7) = eax6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        rax8 = fun_2830(rdi7);
        r12_5 = rax8;
        if (!rax8) {
            rax9 = fun_24c0();
            r13d10 = *reinterpret_cast<void***>(rax9);
            fun_2660();
            *reinterpret_cast<void***>(rax9) = r13d10;
        } else {
            *rcx = eax6;
        }
    }
    return r12_5;
}

void fun_2880(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_7e93(void** rdi) {
    int64_t rcx2;
    void** rbx3;
    void** rdx4;
    void** rax5;
    void** r12_6;
    void** rcx7;
    int32_t eax8;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2880("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_24b0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax5 = fun_2620(rdi, 47, rdx4);
        if (rax5 && ((r12_6 = rax5 + 1, reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_6) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax8 = fun_24d0(rax5 + 0xfffffffffffffffa, "/.libs/", 7, rcx7), !eax8))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax5 + 1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_6 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_6 + 2) != 45)) {
                rbx3 = r12_6;
            } else {
                rbx3 = rax5 + 4;
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_9633(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_24c0();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x12200;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_9673(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x12200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_9693(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x12200);
    }
    *rdi = esi;
    return 0x12200;
}

int64_t fun_96b3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x12200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s52 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_96f3(struct s52* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s52*>(0x12200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s53 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s53* fun_9713(struct s53* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s53*>(0x12200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x293d;
    if (!rdx) 
        goto 0x293d;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x12200;
}

struct s54 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_9753(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s54* r8) {
    struct s54* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s54*>(0x12200);
    }
    rax7 = fun_24c0();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x9786);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s55 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_97d3(int64_t rdi, int64_t rsi, void*** rdx, struct s55* rcx) {
    struct s55* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s55*>(0x12200);
    }
    rax6 = fun_24c0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x9801);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x985c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_98c3() {
    __asm__("cli ");
}

void** g12078 = reinterpret_cast<void**>(0);

int64_t slotvec0 = 0x100;

void fun_98d3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2480(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0x12100) {
        fun_2480(rdi8, rsi9);
        g12078 = reinterpret_cast<void**>(0x12100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x12070) {
        fun_2480(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0x12070);
    }
    nslots = 1;
    return;
}

void fun_9973() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9993() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_99a3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_99c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_99e3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2943;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax6;
    }
}

void** fun_9a73(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2948;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_25d0();
    } else {
        return rax7;
    }
}

void** fun_9b03(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s5* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x294d;
    rcx4 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_25d0();
    } else {
        return rax5;
    }
}

void** fun_9b93(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x2952;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax6;
    }
}

void** fun_9c23(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s5* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x85d0]");
    __asm__("movdqa xmm1, [rip+0x85d8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x85c1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_25d0();
    } else {
        return rax10;
    }
}

void** fun_9cc3(int64_t rdi, uint32_t esi) {
    struct s5* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8530]");
    __asm__("movdqa xmm1, [rip+0x8538]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8521]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_25d0();
    } else {
        return rax9;
    }
}

void** fun_9d63(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8490]");
    __asm__("movdqa xmm1, [rip+0x8498]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x8479]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_25d0();
    } else {
        return rax3;
    }
}

void** fun_9df3(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8400]");
    __asm__("movdqa xmm1, [rip+0x8408]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x83f6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_25d0();
    } else {
        return rax4;
    }
}

void** fun_9e83(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2957;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax6;
    }
}

void** fun_9f23(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x82ca]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x82c2]");
    __asm__("movdqa xmm2, [rip+0x82ca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x295c;
    if (!rdx) 
        goto 0x295c;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_25d0();
    } else {
        return rax7;
    }
}

void** fun_9fc3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s5* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x822a]");
    __asm__("movdqa xmm1, [rip+0x8232]");
    __asm__("movdqa xmm2, [rip+0x823a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2961;
    if (!rdx) 
        goto 0x2961;
    rcx7 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_25d0();
    } else {
        return rax9;
    }
}

void** fun_a073(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x817a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x8172]");
    __asm__("movdqa xmm2, [rip+0x817a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2966;
    if (!rsi) 
        goto 0x2966;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax6;
    }
}

void** fun_a113(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x80da]");
    __asm__("movdqa xmm1, [rip+0x80e2]");
    __asm__("movdqa xmm2, [rip+0x80ea]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x296b;
    if (!rsi) 
        goto 0x296b;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_25d0();
    } else {
        return rax7;
    }
}

void fun_a1b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a1c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a1e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a203(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s56 {
    int64_t f0;
    int64_t f8;
};

int32_t fun_2690(int64_t rdi, void* rsi);

struct s56* fun_a223(struct s56* rdi) {
    void** rax2;
    int32_t eax3;
    struct s56* rax4;
    int64_t v5;
    int64_t v6;
    void* rdx7;

    __asm__("cli ");
    rax2 = g28;
    eax3 = fun_2690("/", reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
    if (eax3) {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    } else {
        rdi->f0 = v5;
        rdi->f8 = v6;
        rax4 = rdi;
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax4;
    }
}

int32_t dup_safer();

int64_t fun_a2a3(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_24c0();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_2660();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s57 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_26f0(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_a303(int64_t rdi, void** rsi, void** rdx, void** rcx, struct s57* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2890(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2890(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2590();
    fun_2890(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_26f0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2590();
    fun_2890(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_26f0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2590();
        fun_2890(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xd6a8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xd6a8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_a773() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s58 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_a793(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s58* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s58* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_25d0();
    } else {
        return;
    }
}

void fun_a833(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_a8d6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_a8e0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_25d0();
    } else {
        return;
    }
    addr_a8d6_5:
    goto addr_a8e0_7;
}

void fun_a913() {
    int64_t rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** r8_8;
    void** rax9;
    void** r8_10;

    __asm__("cli ");
    rsi1 = stdout;
    fun_26f0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2590();
    fun_27c0(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8);
    rax9 = fun_2590();
    fun_27c0(1, rax9, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_10);
    fun_2590();
    goto fun_27c0;
}

int64_t fun_2520();

void xalloc_die();

void fun_a9b3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2520();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a9f3(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2750(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_aa13(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2750(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_aa33(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2750(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_aa53(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_27a0(rdi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_aa83(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_27a0(rdi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_aab3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2520();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_aaf3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab33(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab63(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2520();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_abb3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2520();
            if (rax5) 
                break;
            addr_abfd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_abfd_5;
        rax8 = fun_2520();
        if (rax8) 
            goto addr_abe6_9;
        if (rbx4) 
            goto addr_abfd_5;
        addr_abe6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_ac43(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2520();
            if (rax8) 
                break;
            addr_ac8a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_ac8a_5;
        rax11 = fun_2520();
        if (rax11) 
            goto addr_ac72_9;
        if (!rbx6) 
            goto addr_ac72_9;
        if (r12_4) 
            goto addr_ac8a_5;
        addr_ac72_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_acd3(void** rdi, void** rsi, void** rdx, void* rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void** r12_8;
    void* rsi9;
    void* rcx10;
    void* rbx11;
    void* rax12;
    void* rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void*>((reinterpret_cast<int64_t>(rcx10) >> 1) + reinterpret_cast<uint64_t>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<int64_t>(rbx11) <= reinterpret_cast<int64_t>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_ad7d_9:
            rbx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_ad90_10:
                *r12_8 = reinterpret_cast<void*>(0);
            }
            addr_ad30_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbx11) - reinterpret_cast<uint64_t>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_ad56_12;
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_ada4_14;
            if (reinterpret_cast<int64_t>(rcx10) <= reinterpret_cast<int64_t>(rsi9)) 
                goto addr_ad4d_16;
            if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) 
                goto addr_ada4_14;
            addr_ad4d_16:
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_ada4_14;
            addr_ad56_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_27a0(rdi7);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_ada4_14;
            if (!rbp13) 
                break;
            addr_ada4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<int64_t>(rbp13) <= reinterpret_cast<int64_t>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_ad7d_9;
        } else {
            if (!r13_6) 
                goto addr_ad90_10;
            goto addr_ad30_11;
        }
    }
}

void fun_add3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_26c0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ae03(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_26c0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ae33(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ae53(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ae73(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2750(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2710;
    }
}

void fun_aeb3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2750(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2710;
    }
}

void fun_aef3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2750(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2710;
    }
}

void fun_af33(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_25b0(rdi);
    rax5 = fun_2750(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_2710;
    }
}

void fun_af73() {
    void** rdi1;

    __asm__("cli ");
    fun_2590();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_27e0();
    fun_24b0(rdi1);
}

int64_t rpl_fts_open();

void fun_2640(int64_t rdi, void** rsi, int64_t rdx, void* rcx);

void fun_afb3() {
    int64_t rax1;
    void** rax2;

    __asm__("cli ");
    rax1 = rpl_fts_open();
    if (!rax1) {
        rax2 = fun_24c0();
        if (*reinterpret_cast<void***>(rax2) != 22) {
            xalloc_die();
        }
        fun_2640("errno != EINVAL", "lib/xfts.c", 41, "xfts_open");
    } else {
        return;
    }
}

struct s59 {
    signed char[72] pad72;
    uint32_t f48;
};

struct s60 {
    signed char[88] pad88;
    int64_t f58;
};

int64_t fun_b003(struct s59* rdi, struct s60* rsi) {
    int32_t r8d3;
    uint32_t eax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    r8d3 = 1;
    eax4 = rdi->f48 & 17;
    if (eax4 == 16 || (r8d3 = 0, eax4 != 17)) {
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(!!rsi->f58);
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

void** fun_28e0(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_2820(void** rdi);

int64_t fun_2600(void** rdi);

int64_t fun_b043(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    void* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_2640("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax");
        do {
            fun_25d0();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_b3b4_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_b3b4_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_b0fd_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_b105_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_24c0();
    *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_28e0(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_b13b_21;
    rsi = r15_14;
    rax24 = fun_2820(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2600(r13_18), r8 = r8, rax26 == 0))) {
            addr_b13b_21:
            r12d11 = 4;
            goto addr_b105_13;
        } else {
            addr_b179_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2600(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xd780 + rbp33 * 4) + 0xd780;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_20)) {
            r12d11 = 1;
            if (*reinterpret_cast<void***>(r12_20) != 34) 
                goto addr_b13b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_b0fd_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_b0fd_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2600(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_b179_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_b105_13;
}

int64_t fun_2500();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_b473(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_2500();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_b4ce_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_24c0();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_b4ce_3;
            rax6 = fun_24c0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s61 {
    signed char[16] pad16;
    int64_t f10;
    int32_t f18;
};

void fun_b4e3(struct s61* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f18 = 0x95f616;
    return;
}

struct s62 {
    int64_t f0;
    int64_t f8;
    uint64_t f10;
    int32_t f18;
};

struct s63 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_b503(struct s62* rdi, struct s63* rsi) {
    uint64_t rax3;
    int64_t rdx4;
    uint64_t rcx5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f18 != 0x95f616) {
        fun_2640("state->magic == 9827862", "lib/cycle-check.c", 60, "cycle_check");
    } else {
        rax3 = rdi->f10;
        rdx4 = rsi->f8;
        if (!rax3) {
            rdi->f10 = 1;
        } else {
            if (rdi->f0 != rdx4 || rsi->f0 != rdi->f8) {
                rcx5 = rax3 + 1;
                rdi->f10 = rcx5;
                if (!(rax3 & rcx5)) {
                    if (!rcx5) {
                        return 1;
                    }
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        }
        rax6 = rsi->f0;
        rdi->f0 = rdx4;
        rdi->f8 = rax6;
        return 0;
    }
}

struct s64 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2730(struct s64* rdi);

int32_t fun_2780(struct s64* rdi);

int64_t fun_2630(int64_t rdi, ...);

int32_t rpl_fflush(struct s64* rdi);

int64_t fun_2550(struct s64* rdi);

int64_t fun_b593(struct s64* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2730(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2780(rdi);
        if (!(eax3 && (eax4 = fun_2730(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2630(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_24c0();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_2550(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2550;
}

uint32_t fun_2530(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_b623(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_2530(rdi);
        r12d10 = eax9;
        goto addr_b724_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_2530(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_b724_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_25d0();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_b7d9_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_2530(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_2530(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_24c0();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_2660();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_b724_3;
                }
            }
        } else {
            eax22 = fun_2530(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_24c0(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_b724_3;
            } else {
                eax25 = fun_2530(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_b724_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_b7d9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_b689_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_b68d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_b68d_18:
            if (0) {
            }
        } else {
            addr_b6d5_23:
            eax28 = fun_2530(rdi);
            r12d10 = eax28;
            goto addr_b724_3;
        }
        eax29 = fun_2530(rdi);
        r12d10 = eax29;
        goto addr_b724_3;
    }
    if (0) {
    }
    eax30 = fun_2530(rdi);
    r12d10 = eax30;
    goto addr_b724_3;
    addr_b689_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_b68d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_b6d5_23;
        goto addr_b68d_18;
    }
}

int32_t fun_27f0();

void fun_b893() {
    void** rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_27f0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_25d0();
    } else {
        return;
    }
}

void rpl_fseeko(struct s64* rdi);

void fun_b913(struct s64* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2780(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_b963(struct s64* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2730(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2630(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2770(int64_t rdi);

signed char* fun_b9e3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2770(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_25f0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_ba23(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_25f0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_25d0();
    } else {
        return r12_7;
    }
}

void fun_bab3() {
    __asm__("cli ");
    goto rpl_fcntl;
}

int32_t setlocale_null_r();

int64_t fun_bad3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_25d0();
    } else {
        return rax3;
    }
}

int64_t fun_bb53(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_27b0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_25b0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2710(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2710(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_bc03() {
    __asm__("cli ");
    goto fun_27b0;
}

void fun_bc13() {
    __asm__("cli ");
}

void fun_bc27() {
    __asm__("cli ");
    return;
}

signed char cycle_warning_required(int64_t rdi, int64_t rsi);

int64_t quotearg_n_style_colon();

void fun_35e0() {
    int64_t v1;
    int64_t rbp2;
    signed char al3;

    al3 = cycle_warning_required(v1, rbp2);
    if (al3) {
        quotearg_n_style_colon();
        fun_2590();
        fun_27e0();
        goto 0x3598;
    }
}

struct s65 {
    signed char[88] pad88;
    int64_t f58;
};

struct s66 {
    signed char[32] pad32;
    int64_t f20;
};

struct s67 {
    signed char[32] pad32;
    int64_t f20;
};

struct s68 {
    signed char[17] pad17;
    unsigned char f11;
};

struct s69 {
    signed char[24] pad24;
    int64_t f18;
};

void fun_3840() {
    struct s65* rbp1;
    struct s66* rbp2;
    struct s67* rbp3;
    void** v4;
    void** rbp5;
    void** rcx6;
    void** r8_7;
    uint32_t r14d8;
    struct s68* r15_9;
    int32_t* r15_10;
    void** v11;
    int32_t* r15_12;
    int64_t r12_13;
    struct s69* r15_14;

    if (!rbp1->f58 && !rbp2->f20) {
        rbp3->f20 = 1;
        rpl_fts_set(v4, rbp5, 1, rcx6, r8_7);
        goto 0x3598;
    }
    r14d8 = r15_9->f11;
    if (*reinterpret_cast<signed char*>(&r14d8)) {
        if (*r15_10) 
            goto 0x3b60;
    } else {
        quotearg_style(4, v11);
        fun_2590();
        fun_27e0();
        if (*r15_12) 
            goto addr_3af1_8;
    }
    r12_13 = r15_14->f18;
    if (r12_13) 
        goto 0x3734; else 
        goto "???";
    addr_3af1_8:
    goto 0x3b63;
}

struct s70 {
    signed char[4] pad4;
    signed char f4;
};

void fun_3898() {
    struct s70* r15_1;
    int32_t v2;

    if (!r15_1->f4) 
        goto 0x3598;
    if (v2 != -1) 
        goto 0x360c; else 
        goto "???";
}

struct s71 {
    signed char[4] pad4;
    signed char f4;
};

struct s73 {
    int64_t f0;
    int64_t f8;
};

struct s72 {
    signed char[8] pad8;
    struct s73* f8;
};

struct s74 {
    signed char[120] pad120;
    int64_t f78;
};

struct s75 {
    signed char[112] pad112;
    int64_t f70;
};

void fun_3900() {
    struct s71* r15_1;
    struct s72* r15_2;
    struct s74* rbp3;
    struct s75* rbp4;
    void** v5;
    void** rax6;
    void** rax7;
    void** r8_8;
    void** rcx9;
    void** rax10;
    void** v11;
    void** rbp12;

    if (!r15_1->f4) 
        goto 0x35f8;
    if (!r15_2->f8) 
        goto 0x3598;
    if (rbp3->f78 != r15_2->f8->f0) 
        goto 0x3598;
    if (rbp4->f70 != r15_2->f8->f8) 
        goto 0x3598;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v5) == 47) || *reinterpret_cast<void***>(v5 + 1)) {
        rax6 = quotearg_n_style();
        rax7 = quotearg_n_style();
        fun_2590();
        r8_8 = rax6;
        rcx9 = rax7;
        fun_27e0();
    } else {
        rax10 = quotearg_style(4, v5);
        fun_2590();
        rcx9 = rax10;
        fun_27e0();
    }
    fun_2590();
    fun_27e0();
    rpl_fts_set(v11, rbp12, 4, rcx9, r8_8);
    rpl_fts_read(v11, rbp12, 4, rcx9, r8_8);
    goto 0x3598;
}

struct s76 {
    signed char[17] pad17;
    unsigned char f11;
};

void fun_39c8() {
    uint32_t r14d1;
    struct s76* r15_2;
    void** v3;

    r14d1 = r15_2->f11;
    if (*reinterpret_cast<signed char*>(&r14d1)) 
        goto "???";
    quotearg_style(4, v3);
    goto 0x387e;
}

struct s77 {
    signed char[17] pad17;
    unsigned char f11;
};

void fun_3ab0() {
    uint32_t r14d1;
    struct s77* r15_2;

    r14d1 = r15_2->f11;
    if (*reinterpret_cast<signed char*>(&r14d1)) 
        goto 0x39d6;
    quotearg_n_style_colon();
}

uint32_t fun_26a0(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_28b0(int64_t rdi, void** rsi);

uint32_t fun_28a0(void** rdi, void** rsi);

void fun_80c5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2590();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2590();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_25b0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_83c3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_83c3_22; else 
                            goto addr_87bd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_887d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_8bd0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_83c0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_83c0_30; else 
                                goto addr_8be9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_25b0(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_8bd0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_26a0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_8bd0_28; else 
                            goto addr_826c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_8d30_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_8bb0_40:
                        if (r11_27 == 1) {
                            addr_873d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_8cf8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_8377_44;
                            }
                        } else {
                            goto addr_8bc0_46;
                        }
                    } else {
                        addr_8d3f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_873d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_83c3_22:
                                if (v47 != 1) {
                                    addr_8919_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_25b0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_8964_54;
                                    }
                                } else {
                                    goto addr_83d0_56;
                                }
                            } else {
                                addr_8375_57:
                                ebp36 = 0;
                                goto addr_8377_44;
                            }
                        } else {
                            addr_8ba4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_8d3f_47; else 
                                goto addr_8bae_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_873d_41;
                        if (v47 == 1) 
                            goto addr_83d0_56; else 
                            goto addr_8919_52;
                    }
                }
                addr_8431_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_82c8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_82ed_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_85f0_65;
                    } else {
                        addr_8459_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_8ca8_67;
                    }
                } else {
                    goto addr_8450_69;
                }
                addr_8301_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_834c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_8ca8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_834c_81;
                }
                addr_8450_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_82ed_64; else 
                    goto addr_8459_66;
                addr_8377_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_842f_91;
                if (v22) 
                    goto addr_838f_93;
                addr_842f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8431_62;
                addr_8964_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_90eb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_915b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_8f5f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_28b0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_28a0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_8a5e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_841c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_8a68_112;
                    }
                } else {
                    addr_8a68_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_8b39_114;
                }
                addr_8428_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_842f_91;
                while (1) {
                    addr_8b39_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_9047_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_8aa6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_9055_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_8b27_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_8b27_128;
                        }
                    }
                    addr_8ad5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_8b27_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_8aa6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_8ad5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_834c_81;
                addr_9055_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8ca8_67;
                addr_90eb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_8a5e_109;
                addr_915b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_8a5e_109;
                addr_83d0_56:
                rax93 = fun_28e0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_841c_110;
                addr_8bae_59:
                goto addr_8bb0_40;
                addr_887d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_83c3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_8428_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_8375_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_83c3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_88c2_160;
                if (!v22) 
                    goto addr_8c97_162; else 
                    goto addr_8ea3_163;
                addr_88c2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_8c97_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_8ca8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_876b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_85d3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_8301_70; else 
                    goto addr_85e7_169;
                addr_876b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_82c8_63;
                goto addr_8450_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_8ba4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_8cdf_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_83c0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_82b8_178; else 
                        goto addr_8c62_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_8ba4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_83c3_22;
                }
                addr_8cdf_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_83c0_30:
                    r8d42 = 0;
                    goto addr_83c3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_8431_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_8cf8_42;
                    }
                }
                addr_82b8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_82c8_63;
                addr_8c62_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_8bc0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_8431_62;
                } else {
                    addr_8c72_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_83c3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_9422_188;
                if (v28) 
                    goto addr_8c97_162;
                addr_9422_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_85d3_168;
                addr_826c_37:
                if (v22) 
                    goto addr_9263_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_8283_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_8d30_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_8dbb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_83c3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_82b8_178; else 
                        goto addr_8d97_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_8ba4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_83c3_22;
                }
                addr_8dbb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_83c3_22;
                }
                addr_8d97_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_8bc0_46;
                goto addr_8c72_186;
                addr_8283_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_83c3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_83c3_22; else 
                    goto addr_8294_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_936e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_91f4_210;
            if (1) 
                goto addr_91f2_212;
            if (!v29) 
                goto addr_8e2e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_25a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_9361_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_85f0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_83ab_219; else 
            goto addr_860a_220;
        addr_838f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_83a3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_860a_220; else 
            goto addr_83ab_219;
        addr_8f5f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_860a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_25a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_8f7d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_25a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_93f0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_8e56_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_9047_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_83a3_221;
        addr_8ea3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_83a3_221;
        addr_85e7_169:
        goto addr_85f0_65;
        addr_936e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_860a_220;
        goto addr_8f7d_222;
        addr_91f4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_924e_236;
        fun_25d0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_93f0_225;
        addr_91f2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_91f4_210;
        addr_8e2e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_91f4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_8e56_226;
        }
        addr_9361_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_87bd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd16c + rax113 * 4) + 0xd16c;
    addr_8be9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd26c + rax114 * 4) + 0xd26c;
    addr_9263_190:
    addr_83ab_219:
    goto 0x8090;
    addr_8294_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd06c + rax115 * 4) + 0xd06c;
    addr_924e_236:
    goto v116;
}

void fun_82b0() {
}

void fun_8468() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x8162;
}

void fun_84c1() {
    goto 0x8162;
}

void fun_85ae() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x8431;
    }
    if (v2) 
        goto 0x8ea3;
    if (!r10_3) 
        goto addr_900e_5;
    if (!v4) 
        goto addr_8ede_7;
    addr_900e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_8ede_7:
    goto 0x82e4;
}

void fun_85cc() {
}

void fun_8677() {
    signed char v1;

    if (v1) {
        goto 0x85ff;
    } else {
        goto 0x833a;
    }
}

void fun_8691() {
    signed char v1;

    if (!v1) 
        goto 0x868a; else 
        goto "???";
}

void fun_86b8() {
    goto 0x85d3;
}

void fun_8738() {
}

void fun_8750() {
}

void fun_877f() {
    goto 0x85d3;
}

void fun_87d1() {
    goto 0x8760;
}

void fun_8800() {
    goto 0x8760;
}

void fun_8833() {
    goto 0x8760;
}

void fun_8c00() {
    goto 0x82b8;
}

void fun_8efe() {
    signed char v1;

    if (v1) 
        goto 0x8ea3;
    goto 0x82e4;
}

void fun_8fa5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x82e4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x82c8;
        goto 0x82e4;
    }
}

void fun_93c2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x8630;
    } else {
        goto 0x8162;
    }
}

void fun_a3d8() {
    fun_2590();
}

void fun_b1ec() {
    if (!__intrinsic()) 
        goto "???";
    goto 0xb1fb;
}

void fun_b2bc() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0xb2c9;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0xb1fb;
}

void fun_b2e0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_b30c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xb1fb;
}

void fun_b32d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xb1fb;
}

void fun_b351() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xb1fb;
}

void fun_b375() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xb304;
}

void fun_b399() {
}

void fun_b3b9() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xb304;
}

void fun_b3d5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xb304;
}

void fun_2b04() {
    goto 0x2a18;
}

void fun_84ee() {
    goto 0x8162;
}

void fun_86c4() {
    goto 0x867c;
}

void fun_878b() {
    goto 0x82b8;
}

void fun_87dd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x8760;
    goto 0x838f;
}

void fun_880f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x876b;
        goto 0x8190;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x860a;
        goto 0x83ab;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x8fa8;
    if (r10_8 > r15_9) 
        goto addr_86f5_9;
    addr_86fa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x8fb3;
    goto 0x82e4;
    addr_86f5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_86fa_10;
}

void fun_8842() {
    goto 0x8377;
}

void fun_8c10() {
    goto 0x8377;
}

void fun_93af() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x84cc;
    } else {
        goto 0x8630;
    }
}

void fun_a490() {
}

void fun_b28f() {
    if (__intrinsic()) 
        goto 0xb2c9; else 
        goto "???";
}

void fun_2b0e() {
    goto 0x2a18;
}

void fun_2bb5() {
    goto 0x2a18;
}

void fun_884c() {
    goto 0x87e7;
}

void fun_8c1a() {
    goto 0x873d;
}

void fun_a4f0() {
    fun_2590();
    goto fun_2890;
}

void fun_2b18() {
    goto 0x2a18;
}

void fun_851d() {
    goto 0x8162;
}

void fun_8858() {
    goto 0x87e7;
}

void fun_8c27() {
    goto 0x878e;
}

void fun_a530() {
    fun_2590();
    goto fun_2890;
}

void fun_2b25() {
    goto 0x2a18;
}

void fun_854a() {
    goto 0x8162;
}

void fun_8864() {
    goto 0x8760;
}

void fun_a570() {
    fun_2590();
    goto fun_2890;
}

void fun_2b32() {
    goto 0x2a18;
}

void fun_856c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x8f00;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x8431;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x8431;
    }
    if (v11) 
        goto 0x9263;
    if (r10_12 > r15_13) 
        goto addr_92b3_8;
    addr_92b8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x8ff1;
    addr_92b3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_92b8_9;
}

struct s78 {
    signed char[24] pad24;
    int64_t f18;
};

struct s79 {
    signed char[16] pad16;
    void** f10;
};

struct s80 {
    signed char[8] pad8;
    void** f8;
};

void fun_a5c0() {
    int64_t r15_1;
    struct s78* rbx2;
    void** r14_3;
    struct s79* rbx4;
    void** r13_5;
    struct s80* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2590();
    fun_2890(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0xa5e2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_2b3f() {
    goto 0x2a18;
}

void fun_a618() {
    fun_2590();
    goto 0xa5e9;
}

void fun_2b49() {
    goto 0x2a18;
}

struct s81 {
    signed char[32] pad32;
    int64_t f20;
};

struct s82 {
    signed char[24] pad24;
    int64_t f18;
};

struct s83 {
    signed char[16] pad16;
    void** f10;
};

struct s84 {
    signed char[8] pad8;
    void** f8;
};

struct s85 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_a650() {
    int64_t rcx1;
    struct s81* rbx2;
    int64_t r15_3;
    struct s82* rbx4;
    void** r14_5;
    struct s83* rbx6;
    void** r13_7;
    struct s84* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s85* rbx12;
    void** rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2590();
    fun_2890(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0xa684, __return_address(), rcx1);
    goto v15;
}

void fun_2b56() {
    goto 0x2a18;
}

void fun_a6c8() {
    fun_2590();
    goto 0xa68b;
}

void fun_2b60() {
    goto 0x2a18;
}
