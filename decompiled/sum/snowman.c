
struct s0 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[15] pad24;
    struct s0* f18;
};

int64_t fun_24f0();

int64_t fun_2410(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24f0();
    if (r8d > 10) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x94e0 + rax11 * 4) + 0x94e0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s2 {
    struct s2* f0;
    unsigned char f1;
    signed char f2;
    signed char[37] pad40;
    signed char* f28;
    signed char* f30;
    signed char[590] pad646;
    signed char f286;
    struct s2* f287;
    struct s2* f288;
};

struct s2* g28;

struct s2** fun_2420();

struct s0* slotvec = reinterpret_cast<struct s0*>(0x70);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_2590();

struct s3 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_2400(struct s0* rdi, struct s0* rsi, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

void* fun_2510();

struct s0* quotearg_n_options(struct s0* rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s2* rax6;
    int64_t v7;
    struct s2** rax8;
    struct s0* r15_9;
    struct s2* v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s3* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x5b1f;
    rax8 = fun_2420();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xd070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x73b1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5bab;
            fun_2590();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->pad8);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xd120) {
                fun_2400(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x5c3a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2510();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xd088;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s2* gettext_quote_part_0(struct s2* rdi, int32_t esi, struct s2* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s2* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s2* rax10;
    struct s2* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s2*>(0x946b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s2**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s2*>(0x9464);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s2*>(0x946f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s2**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s2*>(0x9460);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s2*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s2*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gcde0 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gcde0;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23e3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_23f3() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2040;

void fun_2403() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2413() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2423() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2433() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2443() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_2453() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20a0;

void fun_2463() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t localeconv = 0x20b0;

void fun_2473() {
    __asm__("cli ");
    goto localeconv;
}

int64_t fcntl = 0x20c0;

void fun_2483() {
    __asm__("cli ");
    goto fcntl;
}

int64_t clearerr_unlocked = 0x20d0;

void fun_2493() {
    __asm__("cli ");
    goto clearerr_unlocked;
}

int64_t fread_unlocked = 0x20e0;

void fun_24a3() {
    __asm__("cli ");
    goto fread_unlocked;
}

int64_t textdomain = 0x20f0;

void fun_24b3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2100;

void fun_24c3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2110;

void fun_24d3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2120;

void fun_24e3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2130;

void fun_24f3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2140;

void fun_2503() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2150;

void fun_2513() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2160;

void fun_2523() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2170;

void fun_2533() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2180;

void fun_2543() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x2190;

void fun_2553() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21a0;

void fun_2563() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21b0;

void fun_2573() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21c0;

void fun_2583() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21d0;

void fun_2593() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x21e0;

void fun_25a3() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x21f0;

void fun_25b3() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memcmp = 0x2200;

void fun_25c3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2210;

void fun_25d3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2220;

void fun_25e3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2230;

void fun_25f3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2240;

void fun_2603() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t __memcpy_chk = 0x2250;

void fun_2613() {
    __asm__("cli ");
    goto __memcpy_chk;
}

int64_t memcpy = 0x2260;

void fun_2623() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2270;

void fun_2633() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2280;

void fun_2643() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2290;

void fun_2653() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22a0;

void fun_2663() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22b0;

void fun_2673() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x22c0;

void fun_2683() {
    __asm__("cli ");
    goto realloc;
}

int64_t fdopen = 0x22d0;

void fun_2693() {
    __asm__("cli ");
    goto fdopen;
}

int64_t setlocale = 0x22e0;

void fun_26a3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22f0;

void fun_26b3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t setvbuf = 0x2300;

void fun_26c3() {
    __asm__("cli ");
    goto setvbuf;
}

int64_t memmove = 0x2310;

void fun_26d3() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2320;

void fun_26e3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2330;

void fun_26f3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2340;

void fun_2703() {
    __asm__("cli ");
    goto fopen;
}

int64_t strtoumax = 0x2350;

void fun_2713() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2360;

void fun_2723() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2370;

void fun_2733() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2380;

void fun_2743() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2390;

void fun_2753() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x23a0;

void fun_2763() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23b0;

void fun_2773() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x23c0;

void fun_2783() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x23d0;

void fun_2793() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(struct s2* rdi);

struct s2* fun_26a0(int64_t rdi, ...);

void fun_24d0(int64_t rdi, int64_t rsi);

void fun_24b0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

struct s2* stdout = reinterpret_cast<struct s2*>(0);

void fun_26c0(struct s2* rdi);

int32_t fun_2520(int64_t rdi, struct s2** rsi, struct s2* rdx, struct s2** rcx);

void usage();

int64_t Version = 0x9373;

void version_etc(struct s2* rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_2730();

int32_t sum_algorithm = 0;

int32_t optind = 0;

struct s5 {
    unsigned char f0;
    unsigned char f1;
};

int32_t fun_25f0(struct s2* rdi, struct s5* rsi, struct s2* rdx, struct s2** rcx, struct s2** r8, int64_t r9, struct s2** a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, struct s2* a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18, int64_t a19, int64_t a20);

struct s6 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

struct s6* stdin = reinterpret_cast<struct s6*>(0);

signed char have_read_stdin = 0;

void fadvise(struct s6* rdi, int64_t rsi, struct s2* rdx, struct s2** rcx, struct s2** r8, int64_t r9);

struct s2** fun_2490(struct s6* rdi, struct s2** rsi, void* rdx);

struct s6* fopen_safer(struct s2* rdi, int64_t rsi, struct s2* rdx, struct s2** rcx, struct s2** r8, int64_t r9);

struct s2** quotearg_n_style_colon();

void fun_26e0();

struct s2** rpl_fclose(struct s6* rdi, struct s2** rsi, struct s2* rdx, struct s2** rcx, struct s2** r8, int64_t r9);

struct s2* fun_24e0();

int64_t fun_27e3(int32_t edi, struct s2** rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r15_5;
    int64_t v6;
    int64_t r14_7;
    int64_t v8;
    int64_t r13_9;
    int64_t v10;
    int64_t r12_11;
    int64_t v12;
    int64_t rbp13;
    struct s2** rbp14;
    int64_t v15;
    int64_t rbx16;
    int32_t ebx17;
    struct s2* rdi18;
    struct s2* rax19;
    struct s2* v20;
    struct s2* rdi21;
    struct s2*** rsp22;
    struct s2** r8_23;
    struct s2** rcx24;
    struct s2* rdx25;
    struct s2** rsi26;
    int64_t rdi27;
    int32_t eax28;
    int32_t eax29;
    struct s2* rdi30;
    int64_t rcx31;
    int64_t r9_32;
    struct s2** v33;
    int64_t rax34;
    struct s2** rbp35;
    int64_t v36;
    struct s2** r13_37;
    struct s2* r12_38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int32_t eax43;
    void* rsp44;
    struct s6* r14_45;
    void* rsp46;
    void* rdx47;
    int64_t rax48;
    struct s2* eax49;
    void* rsp50;
    struct s2* r15d51;
    struct s2** rax52;
    struct s2** rax53;
    void* rsp54;
    struct s6* rax55;
    void* rsp56;
    struct s2** rax57;
    struct s2** rax58;
    void* rsp59;
    struct s2* rdx60;
    int64_t rax61;
    int32_t eax62;
    void* rsp63;
    struct s2** rax64;
    struct s2** rax65;
    int64_t rax66;
    int1_t zf67;
    struct s2* v68;
    int1_t zf69;
    struct s6* rdi70;
    struct s2** rax71;
    uint32_t eax72;
    int64_t rax73;
    void* rdx74;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r15_5;
    v6 = r14_7;
    v8 = r13_9;
    v10 = r12_11;
    v12 = rbp13;
    rbp14 = rsi;
    v15 = rbx16;
    ebx17 = edi;
    rdi18 = *rsi;
    rax19 = g28;
    v20 = rax19;
    set_program_name(rdi18);
    fun_26a0(6, 6);
    fun_24d0("coreutils", "/usr/local/share/locale");
    fun_24b0("coreutils", "/usr/local/share/locale");
    atexit(0x3460, "/usr/local/share/locale");
    rdi21 = stdout;
    fun_26c0(rdi21);
    rsp22 = reinterpret_cast<struct s2***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    while (*reinterpret_cast<int32_t*>(&r8_23) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_23) + 4) = 0, rcx24 = reinterpret_cast<struct s2**>(0xcac0), rdx25 = reinterpret_cast<struct s2*>("rs"), rsi26 = rbp14, *reinterpret_cast<int32_t*>(&rdi27) = ebx17, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi27) + 4) = 0, eax28 = fun_2520(rdi27, rsi26, "rs", 0xcac0), rsp22 = rsp22 - 1 + 1, eax28 != -1) {
        if (eax28 == 0x72) {
            addr_2908_4:
            eax29 = 0;
        } else {
            if (eax28 > 0x72) {
                addr_28d8_6:
                if (eax28 != 0x73) 
                    goto addr_2b6a_7; else 
                    goto addr_28e1_8;
            } else {
                if (eax28 != 0xffffff7d) {
                    if (eax28 != 0xffffff7e) 
                        goto addr_2b6a_7;
                    usage();
                    rsp22 = rsp22 - 1 + 1;
                    goto addr_2908_4;
                } else {
                    rdi30 = stdout;
                    rcx31 = Version;
                    r9_32 = reinterpret_cast<int64_t>("David MacKenzie");
                    version_etc(rdi30, "sum", "GNU coreutils", rcx31, "Kayvan Aghaiepour", "David MacKenzie", 0, 0x2880);
                    eax28 = fun_2730();
                    rsp22 = rsp22 - 1 - 1 - 1 + 1 - 1 + 1;
                    goto addr_28d8_6;
                }
            }
        }
        addr_28e6_13:
        sum_algorithm = eax29;
        continue;
        addr_28e1_8:
        eax29 = 1;
        goto addr_28e6_13;
    }
    v33 = rbp14 + ebx17 * 8;
    rax34 = optind;
    if (*reinterpret_cast<int32_t*>(&rax34) == ebx17) {
        rdx25 = reinterpret_cast<struct s2*>("-");
        *v33 = reinterpret_cast<struct s2*>("-");
        rcx24 = v33 + 8;
        v33 = rcx24;
    }
    rbp35 = rbp14 + rax34 * 8;
    if (reinterpret_cast<uint64_t>(v33) <= reinterpret_cast<uint64_t>(rbp35)) {
        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v36) + 7) = 1;
    } else {
        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v36) + 7) = 1;
        r13_37 = reinterpret_cast<struct s2**>(reinterpret_cast<int64_t>(rsp22) + 36);
        do {
            r12_38 = *rbp35;
            eax43 = fun_25f0(r12_38, "-", rdx25, rcx24, r8_23, r9_32, v33, v36, v39, v40, v41, v20, v42, v15, v12, v10, v8, v6, v4, v3);
            rsp44 = reinterpret_cast<void*>(rsp22 - 1 + 1);
            if (!eax43) {
                r14_45 = stdin;
                have_read_stdin = 1;
                fadvise(r14_45, 2, rdx25, rcx24, r8_23, r9_32);
                rsp46 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
                rdx47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) + 24);
                *reinterpret_cast<int32_t*>(&rax48) = sum_algorithm;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax48) + 4) = 0;
                eax49 = reinterpret_cast<struct s2*>(*reinterpret_cast<int64_t*>("p," + rax48 * 8)(r14_45, r13_37, rdx47));
                rsp50 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp46) - 8 + 8);
                r15d51 = eax49;
                if (eax49) {
                    rax52 = fun_2420();
                    rsp50 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp50) - 8 + 8);
                    r15d51 = *rax52;
                }
                rax53 = fun_2490(r14_45, r13_37, rdx47);
                rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp50) - 8 + 8);
                goto addr_29a4_23;
            }
            rax55 = fopen_safer(r12_38, "r", rdx25, rcx24, r8_23, r9_32);
            rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp44) - 8 + 8);
            if (!rax55) {
                rax57 = quotearg_n_style_colon();
                rax58 = fun_2420();
                rcx24 = rax57;
                rdx25 = reinterpret_cast<struct s2*>("%s");
                *reinterpret_cast<struct s2**>(&rsi26) = *rax58;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi26) + 4) = 0;
                fun_26e0();
                rsp22 = reinterpret_cast<struct s2***>(reinterpret_cast<int64_t>(rsp56) - 8 + 8 - 8 + 8 - 8 + 8);
                *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v36) + 7) = 0;
                continue;
            } else {
                fadvise(rax55, 2, rdx25, rcx24, r8_23, r9_32);
                rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                rdx60 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(rsp59) + 24);
                *reinterpret_cast<int32_t*>(&rax61) = sum_algorithm;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax61) + 4) = 0;
                eax62 = reinterpret_cast<int32_t>(*reinterpret_cast<struct s2**>("p," + rax61 * 8)(rax55, r13_37, rdx60));
                rsp63 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
                if (eax62) {
                    rax64 = fun_2420();
                    r15d51 = *rax64;
                    rax53 = rpl_fclose(rax55, r13_37, rdx60, "p,", r8_23, r9_32);
                    rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8 - 8 + 8);
                    if (!*reinterpret_cast<int32_t*>(&rax53)) {
                        addr_29a4_23:
                        if (r15d51) {
                            addr_2a8c_28:
                            rax65 = quotearg_n_style_colon();
                            rdx25 = reinterpret_cast<struct s2*>("%s");
                            *reinterpret_cast<struct s2**>(&rsi26) = r15d51;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi26) + 4) = 0;
                            rcx24 = rax65;
                            fun_26e0();
                            rsp22 = reinterpret_cast<struct s2***>(reinterpret_cast<int64_t>(rsp54) - 8 + 8 - 8 + 8);
                            *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v36) + 7) = 0;
                            continue;
                        } else {
                            addr_29ad_29:
                            *reinterpret_cast<int32_t*>(&rax66) = sum_algorithm;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax66) + 4) = 0;
                            *reinterpret_cast<int32_t*>(&r9_32) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_32) + 4) = 0;
                            zf67 = optind == ebx17;
                            *reinterpret_cast<unsigned char*>(&r9_32) = reinterpret_cast<uint1_t>(!zf67);
                            *reinterpret_cast<int32_t*>(&r8_23) = 10;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_23) + 4) = 0;
                            *reinterpret_cast<struct s2**>(&rsi26) = reinterpret_cast<struct s2*>(0);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi26) + 4) = 0;
                            *reinterpret_cast<int64_t*>(0xcbc0 + rax66 * 8)(r12_38);
                            rdx25 = v68;
                            rcx24 = rax53;
                            rsp22 = reinterpret_cast<struct s2***>(reinterpret_cast<int64_t>(rsp54) - 8 - 8 - 8 + 8 + 8 + 8);
                            continue;
                        }
                    } else {
                        if (r15d51) 
                            goto addr_2a8c_28;
                    }
                } else {
                    rax53 = rpl_fclose(rax55, r13_37, rdx60, "p,", r8_23, r9_32);
                    rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp63) - 8 + 8);
                    if (!*reinterpret_cast<int32_t*>(&rax53)) 
                        goto addr_29ad_29;
                }
            }
            rax53 = fun_2420();
            rsp54 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp54) - 8 + 8);
            r15d51 = *rax53;
            goto addr_29a4_23;
            rbp35 = rbp35 + 8;
        } while (reinterpret_cast<uint64_t>(v33) > reinterpret_cast<uint64_t>(rbp35));
    }
    zf69 = have_read_stdin == 0;
    if (zf69 || (rdi70 = stdin, rax71 = rpl_fclose(rdi70, rsi26, rdx25, rcx24, r8_23, r9_32), !!(*reinterpret_cast<int32_t*>(&rax71) + 1))) {
        eax72 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&v36) + 7)) ^ 1;
        *reinterpret_cast<uint32_t*>(&rax73) = *reinterpret_cast<unsigned char*>(&eax72);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax73) + 4) = 0;
        rdx74 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v20) - reinterpret_cast<unsigned char>(g28));
        if (!rdx74) {
            return rax73;
        }
    } else {
        fun_24e0();
        fun_2420();
        fun_26e0();
        goto addr_2b6a_7;
    }
    addr_2b74_38:
    fun_2510();
    addr_2b6a_7:
    usage();
    goto addr_2b74_38;
}

int64_t __libc_start_main = 0;

void fun_2b83() {
    __asm__("cli ");
    __libc_start_main(0x27e0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xd008;

void fun_23e0(int64_t rdi);

int64_t fun_2c23() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23e0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2c63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* fun_2640(unsigned char* rdi);

void* fun_24a0(void* rdi, struct s0* rsi);

int64_t fun_2c73(uint32_t* rdi, uint32_t* rsi, uint64_t* rdx) {
    uint32_t* r14_4;
    uint32_t* v5;
    uint64_t* v6;
    struct s0* rax7;
    uint32_t ebx8;
    struct s0* rbp9;
    uint32_t r15d10;
    uint64_t r12_11;
    void* r13_12;
    struct s0* rsi13;
    void* rax14;
    struct s0* rdx15;
    struct s0* rdi16;
    uint32_t eax17;
    uint32_t eax18;
    uint64_t tmp64_19;
    int1_t cf20;
    int64_t rax21;
    int64_t rdi22;
    uint64_t tmp64_23;
    uint64_t r12_24;
    struct s0* rdx25;
    uint32_t eax26;
    uint32_t eax27;
    struct s2** rax28;

    __asm__("cli ");
    r14_4 = rdi;
    v5 = rsi;
    v6 = rdx;
    rax7 = fun_2640(0x8000);
    if (!rax7) {
        ebx8 = 0xffffffff;
    } else {
        rbp9 = rax7;
        r15d10 = 0;
        *reinterpret_cast<int32_t*>(&r12_11) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_11) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&r13_12) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_12) + 4) = 0;
            do {
                *reinterpret_cast<uint32_t*>(&rsi13) = 1;
                *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                rax14 = fun_24a0(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<int64_t>(r13_12), 1);
                r13_12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r13_12) + reinterpret_cast<int64_t>(rax14));
                if (reinterpret_cast<int1_t>(r13_12 == 0x8000)) 
                    break;
                if (!rax14) 
                    goto addr_2d53_7;
            } while (!(*r14_4 & 16));
            goto addr_2d5d_9;
            rdx15 = rbp9;
            rdi16 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp9) + 0x8000);
            do {
                *reinterpret_cast<uint32_t*>(&rsi13) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx15->f0));
                *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                rdx15 = reinterpret_cast<struct s0*>(&rdx15->pad8);
                eax17 = r15d10 << 15;
                eax18 = *reinterpret_cast<uint16_t*>(&eax17) + (reinterpret_cast<int32_t>(r15d10) >> 1) + *reinterpret_cast<uint32_t*>(&rsi13);
                r15d10 = *reinterpret_cast<uint16_t*>(&eax18);
            } while (rdx15 != rdi16);
            tmp64_19 = r12_11 + 0x8000;
            cf20 = tmp64_19 < r12_11;
            r12_11 = tmp64_19;
        } while (!cf20);
        goto addr_2d2a_13;
    }
    addr_2d42_14:
    *reinterpret_cast<uint32_t*>(&rax21) = ebx8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
    return rax21;
    addr_2d53_7:
    ebx8 = 0xffffffff;
    if (*r14_4 & 32) {
        addr_2d3a_15:
        fun_2400(rbp9, rsi13);
        goto addr_2d42_14;
    } else {
        addr_2d5d_9:
        *reinterpret_cast<int32_t*>(&rdi22) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
        tmp64_23 = r12_11 + reinterpret_cast<int64_t>(r13_12);
        r12_24 = tmp64_23;
        rsi13 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(r13_12) + reinterpret_cast<unsigned char>(rbp9));
        rdx25 = rbp9;
        *reinterpret_cast<unsigned char*>(&rdi22) = reinterpret_cast<uint1_t>(tmp64_23 < r12_11);
        if (!r13_12) {
            addr_2d9e_16:
            ebx8 = 0;
            *v5 = r15d10;
            *v6 = r12_24;
            goto addr_2d3a_15;
        } else {
            do {
                rdx25 = reinterpret_cast<struct s0*>(&rdx25->pad8);
                eax26 = r15d10 << 15;
                eax27 = *reinterpret_cast<uint16_t*>(&eax26) + (reinterpret_cast<int32_t>(r15d10) >> 1) + reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx25->f0));
                r15d10 = *reinterpret_cast<uint16_t*>(&eax27);
            } while (rsi13 != rdx25);
            if (!rdi22) 
                goto addr_2d9e_16;
        }
    }
    addr_2d2a_13:
    rax28 = fun_2420();
    ebx8 = 0xffffffff;
    *rax28 = reinterpret_cast<struct s2*>(75);
    goto addr_2d3a_15;
}

int64_t fun_2dc3(uint32_t* rdi, uint32_t* rsi, uint64_t* rdx) {
    uint32_t* r14_4;
    uint32_t* v5;
    uint64_t* v6;
    struct s0* rax7;
    uint32_t ebx8;
    struct s0* rbp9;
    uint32_t r15d10;
    uint64_t r12_11;
    void* r13_12;
    struct s0* rsi13;
    void* rax14;
    struct s0* rax15;
    uint64_t tmp64_16;
    int1_t cf17;
    int64_t rax18;
    uint64_t tmp64_19;
    uint64_t r12_20;
    struct s0* rcx21;
    struct s0* rax22;
    uint32_t eax23;
    struct s2** rax24;

    __asm__("cli ");
    r14_4 = rdi;
    v5 = rsi;
    v6 = rdx;
    rax7 = fun_2640(0x8000);
    if (!rax7) {
        ebx8 = 0xffffffff;
    } else {
        rbp9 = rax7;
        r15d10 = 0;
        *reinterpret_cast<int32_t*>(&r12_11) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_11) + 4) = 0;
        do {
            *reinterpret_cast<int32_t*>(&r13_12) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_12) + 4) = 0;
            do {
                *reinterpret_cast<int32_t*>(&rsi13) = 1;
                *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
                rax14 = fun_24a0(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<int64_t>(r13_12), 1);
                r13_12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r13_12) + reinterpret_cast<int64_t>(rax14));
                if (reinterpret_cast<int1_t>(r13_12 == 0x8000)) 
                    break;
                if (!rax14) 
                    goto addr_2e89_7;
            } while (!(*r14_4 & 16));
            goto addr_2e93_9;
            rax15 = rbp9;
            rsi13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbp9) + 0x8000);
            do {
                rax15 = reinterpret_cast<struct s0*>(&rax15->pad8);
                r15d10 = r15d10 + reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax15->f0));
            } while (rsi13 != rax15);
            tmp64_16 = r12_11 + 0x8000;
            cf17 = tmp64_16 < r12_11;
            r12_11 = tmp64_16;
        } while (!cf17);
        goto addr_2e60_13;
    }
    addr_2e78_14:
    *reinterpret_cast<uint32_t*>(&rax18) = ebx8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax18) + 4) = 0;
    return rax18;
    addr_2e89_7:
    ebx8 = 0xffffffff;
    if (*r14_4 & 32) {
        addr_2e70_15:
        fun_2400(rbp9, rsi13);
        goto addr_2e78_14;
    } else {
        addr_2e93_9:
        *reinterpret_cast<int32_t*>(&rsi13) = 0;
        *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
        tmp64_19 = r12_11 + reinterpret_cast<int64_t>(r13_12);
        r12_20 = tmp64_19;
        rcx21 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(r13_12) + reinterpret_cast<unsigned char>(rbp9));
        rax22 = rbp9;
        *reinterpret_cast<unsigned char*>(&rsi13) = reinterpret_cast<uint1_t>(tmp64_19 < r12_11);
        if (!r13_12) {
            addr_2ec4_16:
            eax23 = *reinterpret_cast<uint16_t*>(&r15d10) + (r15d10 >> 16);
            *v5 = (reinterpret_cast<int32_t>(eax23) >> 16) + *reinterpret_cast<uint16_t*>(&eax23);
            ebx8 = 0;
            *v6 = r12_20;
            goto addr_2e70_15;
        } else {
            do {
                rax22 = reinterpret_cast<struct s0*>(&rax22->pad8);
                r15d10 = r15d10 + reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax22->f0));
            } while (rcx21 != rax22);
            if (!rsi13) 
                goto addr_2ec4_16;
        }
    }
    addr_2e60_13:
    rax24 = fun_2420();
    ebx8 = 0xffffffff;
    *rax24 = reinterpret_cast<struct s2*>(75);
    goto addr_2e70_15;
}

struct s2* human_readable(int64_t rdi, void* rsi);

void fun_26b0(int64_t rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, int64_t r8);

void fun_2550();

void fun_2ef3(struct s2* rdi) {
    int32_t ebp2;
    int32_t r9d3;
    int32_t ebx4;
    int32_t r8d5;
    struct s2* rax6;
    int64_t v7;
    struct s2* rax8;
    struct s2* rdx9;
    int32_t* rdx10;
    struct s2* rdi11;
    signed char* rax12;
    void* rax13;

    __asm__("cli ");
    ebp2 = r9d3;
    ebx4 = r8d5;
    rax6 = g28;
    rax8 = human_readable(v7, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x2a8);
    *reinterpret_cast<int32_t*>(&rdx9) = *rdx10;
    *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
    fun_26b0(1, "%05d %5s", rdx9, rax8, 0x400);
    if (*reinterpret_cast<signed char*>(&ebp2)) {
        fun_26b0(1, " %s", rdi, rax8, 0x400);
    }
    rdi11 = stdout;
    rax12 = rdi11->f28;
    if (reinterpret_cast<uint64_t>(rax12) >= reinterpret_cast<uint64_t>(rdi11->f30)) {
        fun_2550();
    } else {
        rdi11->f28 = rax12 + 1;
        *rax12 = *reinterpret_cast<signed char*>(&ebx4);
    }
    rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
    if (rax13) {
        fun_2510();
    } else {
        return;
    }
}

void fun_2fd3(struct s2* rdi) {
    int32_t ebp2;
    int32_t r9d3;
    int32_t ebx4;
    int32_t r8d5;
    struct s2* rax6;
    int64_t v7;
    struct s2* rax8;
    struct s2* rdx9;
    int32_t* rdx10;
    struct s2* rdi11;
    signed char* rax12;
    void* rax13;

    __asm__("cli ");
    ebp2 = r9d3;
    ebx4 = r8d5;
    rax6 = g28;
    rax8 = human_readable(v7, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x2a8);
    *reinterpret_cast<int32_t*>(&rdx9) = *rdx10;
    *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
    fun_26b0(1, "%d %s", rdx9, rax8, 0x200);
    if (*reinterpret_cast<signed char*>(&ebp2)) {
        fun_26b0(1, " %s", rdi, rax8, 0x200);
    }
    rdi11 = stdout;
    rax12 = rdi11->f28;
    if (reinterpret_cast<uint64_t>(rax12) >= reinterpret_cast<uint64_t>(rdi11->f30)) {
        fun_2550();
    } else {
        rdi11->f28 = rax12 + 1;
        *rax12 = *reinterpret_cast<signed char*>(&ebx4);
    }
    rax13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
    if (rax13) {
        fun_2510();
    } else {
        return;
    }
}

struct s2* program_name = reinterpret_cast<struct s2*>(0);

void fun_25d0(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx);

int32_t fun_2430(struct s2* rdi, struct s2* rsi, struct s2* rdx, ...);

struct s2* stderr = reinterpret_cast<struct s2*>(0);

void fun_2750(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, struct s2* r8, struct s2* r9, int64_t a7, int64_t* a8, int64_t a9, int64_t a10, int64_t a11, int64_t* a12);

void fun_30b3(int32_t edi) {
    struct s2* r12_2;
    struct s2* rax3;
    struct s2* v4;
    struct s2* rax5;
    struct s2* rcx6;
    int64_t r8_7;
    struct s2* rax8;
    int64_t r8_9;
    struct s2* r12_10;
    struct s2* rax11;
    struct s2* r12_12;
    struct s2* rax13;
    struct s2* r12_14;
    struct s2* rax15;
    struct s2* r12_16;
    struct s2* rax17;
    struct s2** r8_18;
    int64_t r9_19;
    int32_t eax20;
    struct s2* r13_21;
    struct s2* rax22;
    int64_t r8_23;
    struct s2* rax24;
    int32_t eax25;
    struct s2* rax26;
    int64_t r8_27;
    struct s2* rax28;
    int64_t r8_29;
    struct s2* rax30;
    int32_t eax31;
    struct s2* rax32;
    int64_t r8_33;
    struct s2* r15_34;
    struct s2* rax35;
    struct s2* rax36;
    int64_t r8_37;
    struct s2* rax38;
    struct s2* rdi39;
    struct s2* r8_40;
    struct s2* r9_41;
    int64_t v42;
    int64_t* v43;
    int64_t v44;
    int64_t v45;
    int64_t v46;
    int64_t* v47;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_24e0();
            fun_26b0(1, rax5, r12_2, rcx6, r8_7);
            rax8 = fun_24e0();
            fun_26b0(1, rax8, "BSD", 16, r8_9);
            r12_10 = stdout;
            rax11 = fun_24e0();
            fun_25d0(rax11, r12_10, 5, 16);
            r12_12 = stdout;
            rax13 = fun_24e0();
            fun_25d0(rax13, r12_12, 5, 16);
            r12_14 = stdout;
            rax15 = fun_24e0();
            fun_25d0(rax15, r12_14, 5, 16);
            r12_16 = stdout;
            rax17 = fun_24e0();
            fun_25d0(rax17, r12_16, 5, 16);
            do {
                if (1) 
                    break;
                eax20 = fun_25f0("sum", 0, 5, "sha512sum", r8_18, r9_19, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            } while (eax20);
            r13_21 = v4;
            if (!r13_21) {
                rax22 = fun_24e0();
                fun_26b0(1, rax22, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_23);
                rax24 = fun_26a0(5);
                if (!rax24 || (eax25 = fun_2430(rax24, "en_", 3, rax24, "en_", 3), !eax25)) {
                    rax26 = fun_24e0();
                    r13_21 = reinterpret_cast<struct s2*>("sum");
                    fun_26b0(1, rax26, "https://www.gnu.org/software/coreutils/", "sum", r8_27);
                    r12_2 = reinterpret_cast<struct s2*>(" invocation");
                } else {
                    r13_21 = reinterpret_cast<struct s2*>("sum");
                    goto addr_3410_9;
                }
            } else {
                rax28 = fun_24e0();
                fun_26b0(1, rax28, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_29);
                rax30 = fun_26a0(5);
                if (!rax30 || (eax31 = fun_2430(rax30, "en_", 3, rax30, "en_", 3), !eax31)) {
                    addr_3316_11:
                    rax32 = fun_24e0();
                    fun_26b0(1, rax32, "https://www.gnu.org/software/coreutils/", "sum", r8_33);
                    r12_2 = reinterpret_cast<struct s2*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_21 == "sum")) {
                        r12_2 = reinterpret_cast<struct s2*>(0x98a1);
                    }
                } else {
                    addr_3410_9:
                    r15_34 = stdout;
                    rax35 = fun_24e0();
                    fun_25d0(rax35, r15_34, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3316_11;
                }
            }
            rax36 = fun_24e0();
            rcx6 = r12_2;
            fun_26b0(1, rax36, r13_21, rcx6, r8_37);
            addr_310e_14:
            fun_2730();
        }
    } else {
        rax38 = fun_24e0();
        rdi39 = stderr;
        rcx6 = r12_2;
        fun_2750(rdi39, 1, rax38, rcx6, r8_40, r9_41, v42, v43, v44, v45, v46, v47);
        goto addr_310e_14;
    }
}

int64_t file_name = 0;

void fun_3443(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3453(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s2* rdi);

struct s2* quotearg_colon();

int32_t exit_failure = 1;

struct s2* fun_2440(int64_t rdi, int64_t rsi, int64_t rdx, struct s2* rcx, struct s2* r8);

void fun_3463() {
    struct s2* rdi1;
    int32_t eax2;
    struct s2** rax3;
    int1_t zf4;
    struct s2** rbx5;
    struct s2* rdi6;
    int32_t eax7;
    struct s2* rax8;
    int64_t rdi9;
    struct s2* rax10;
    int64_t rsi11;
    struct s2* r8_12;
    struct s2* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2420(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*rax3 == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_24e0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_34f3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<struct s2**>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_26e0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2440(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_34f3_5:
        *reinterpret_cast<struct s2**>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_26e0();
    }
}

void fun_3513() {
    __asm__("cli ");
}

uint32_t fun_2630(struct s6* rdi);

void fun_3523(struct s6* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2630(rdi);
        goto 0x25b0;
    }
}

int32_t fun_2670(struct s6* rdi);

int64_t fun_2570(int64_t rdi, ...);

int32_t rpl_fflush(struct s6* rdi);

int64_t fun_24c0(struct s6* rdi);

int64_t fun_3553(struct s6* rdi) {
    uint32_t eax2;
    int32_t eax3;
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    struct s2** rax8;
    struct s2* r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2630(rdi);
    if (reinterpret_cast<int32_t>(eax2) >= reinterpret_cast<int32_t>(0)) {
        eax3 = fun_2670(rdi);
        if (!(eax3 && (eax4 = fun_2630(rdi), *reinterpret_cast<uint32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2570(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2420();
            r12d9 = *rax8;
            rax10 = fun_24c0(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_24c0;
}

void rpl_fseeko(struct s6* rdi);

void fun_35e3(struct s6* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2670(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

struct s6* fun_2700();

int32_t dup_safer(int64_t rdi);

struct s6* fun_2690(int64_t rdi, int64_t rsi);

void fun_25a0(int64_t rdi, int64_t rsi, int64_t rdx);

struct s6* fun_3633() {
    struct s6* rax1;
    struct s6* r12_2;
    uint32_t eax3;
    int64_t rdi4;
    int32_t eax5;
    struct s2** rax6;
    struct s6* rdi7;
    struct s2* r13d8;
    struct s2** rsi9;
    struct s2* rdx10;
    struct s2** rcx11;
    struct s2** r8_12;
    int64_t r9_13;
    struct s2** rsi14;
    struct s2* rdx15;
    struct s2** rcx16;
    struct s2** r8_17;
    int64_t r9_18;
    struct s2** rax19;
    int64_t rsi20;
    int64_t rsi21;
    int64_t rdi22;
    struct s6* rax23;
    struct s2** rax24;
    int64_t rdi25;
    struct s2* r12d26;
    int64_t rdx27;

    __asm__("cli ");
    rax1 = fun_2700();
    r12_2 = rax1;
    if (!rax1) 
        goto addr_3656_2;
    eax3 = fun_2630(rax1);
    if (eax3 > 2) 
        goto addr_3656_2;
    *reinterpret_cast<uint32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    eax5 = dup_safer(rdi4);
    if (eax5 < 0) {
        rax6 = fun_2420();
        rdi7 = r12_2;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_2) + 4) = 0;
        r13d8 = *rax6;
        rpl_fclose(rdi7, rsi9, rdx10, rcx11, r8_12, r9_13);
        *rax6 = r13d8;
        goto addr_3656_2;
    } else {
        rax19 = rpl_fclose(r12_2, rsi14, rdx15, rcx16, r8_17, r9_18);
        if (!*reinterpret_cast<int32_t*>(&rax19)) {
            rsi20 = rsi21;
            *reinterpret_cast<int32_t*>(&rdi22) = eax5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
            rax23 = fun_2690(rdi22, rsi20);
            r12_2 = rax23;
            if (rax23) {
                addr_3656_2:
                return r12_2;
            }
        }
        rax24 = fun_2420();
        *reinterpret_cast<int32_t*>(&rdi25) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
        r12d26 = *rax24;
        fun_25a0(rdi25, rsi20, rdx27);
        *rax24 = r12d26;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_2) + 4) = 0;
        goto addr_3656_2;
    }
}

int64_t fun_36d3(struct s6* rdi, int64_t rsi, int32_t edx) {
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2630(rdi);
        *reinterpret_cast<uint32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2570(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

struct s7 {
    struct s2* f0;
    signed char f1;
};

struct s8 {
    struct s2* f0;
    signed char[7] pad8;
    struct s2* f8;
    signed char[7] pad16;
    unsigned char* f10;
};

struct s8* fun_2470();

struct s2* fun_2500(struct s2* rdi, ...);

void fun_2790(struct s2* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_26d0(struct s7* rdi, struct s2* rsi);

void fun_2610(struct s2* rdi, struct s7* rsi, struct s2* rdx, int64_t rcx);

void fun_2620(struct s7* rdi, struct s2* rsi, struct s2* rdx, int64_t rcx);

struct s7* fun_3753(uint64_t rdi, struct s2* rsi, uint32_t edx, uint64_t rcx, uint64_t r8) {
    struct s2* v6;
    uint64_t v7;
    uint32_t v8;
    struct s2* rax9;
    struct s2* v10;
    uint32_t edx11;
    uint32_t v12;
    uint32_t eax13;
    uint32_t v14;
    uint32_t v15;
    struct s8* rax16;
    struct s2* r15_17;
    struct s2* rax18;
    struct s2* r14_19;
    struct s2* rbp20;
    int1_t cf21;
    unsigned char* v22;
    struct s2* rax23;
    struct s2** rsp24;
    struct s7* v25;
    uint64_t r8_26;
    uint64_t rdx27;
    uint64_t rcx28;
    uint64_t rax29;
    uint64_t rax30;
    uint64_t rdx31;
    uint64_t rax32;
    uint64_t rdx33;
    int64_t rdi34;
    uint32_t esi35;
    uint32_t r10d36;
    uint64_t v37;
    int64_t rbx38;
    uint64_t rax39;
    int1_t zf40;
    struct s2* rax41;
    struct s2** rsp42;
    struct s2* rdx43;
    struct s2* r12_44;
    struct s7* rbp45;
    struct s7* r8_46;
    struct s2* r13_47;
    struct s2* rax48;
    void* rsp49;
    struct s2* r12_50;
    struct s2* rax51;
    struct s2* v52;
    void* rsp53;
    uint32_t v54;
    struct s7* rbp55;
    struct s2* rbx56;
    unsigned char* r12_57;
    struct s2* rax58;
    struct s2* rsi59;
    uint64_t rcx60;
    uint64_t rdx61;
    uint64_t rax62;
    uint32_t eax63;
    struct s7* rdx64;
    uint32_t ecx65;
    void* rax66;
    void* rax67;
    uint32_t tmp32_68;
    int1_t cf69;
    struct s2* rbp70;
    struct s2* rax71;
    uint64_t rax72;
    int1_t zf73;
    struct s2* rax74;
    int1_t cf75;
    int1_t below_or_equal76;
    uint64_t rax77;
    struct s2* rax78;
    uint64_t rax79;
    int1_t zf80;
    uint64_t r8_81;
    uint64_t r11_82;
    uint64_t rdx83;
    int64_t rcx84;
    uint64_t r9_85;
    int64_t rax86;
    int32_t eax87;
    int64_t rdx88;
    int64_t rax89;
    uint32_t edx90;
    uint32_t esi91;
    uint64_t rax92;
    int64_t rax93;
    uint32_t esi94;
    int64_t rdx95;
    uint32_t eax96;
    uint64_t rax97;
    uint64_t rdx98;
    uint64_t rdi99;
    uint64_t rax100;
    int32_t eax101;
    uint64_t rax102;
    void* rcx103;
    void* rax104;
    void* rax105;
    uint32_t eax106;
    uint32_t eax107;
    uint32_t edx108;
    void* rsi109;
    uint32_t edx110;
    uint32_t edx111;
    void* rax112;
    void* rsi113;
    void* rax114;
    void* rax115;
    void* rdi116;
    uint32_t eax117;
    uint32_t r9d118;
    uint32_t eax119;
    void* rdx120;
    uint32_t edx121;
    uint32_t edx122;

    __asm__("cli ");
    v6 = rsi;
    v7 = r8;
    v8 = edx;
    rax9 = g28;
    v10 = rax9;
    edx11 = edx & 32;
    v12 = edx11;
    eax13 = edx & 3;
    v14 = eax13;
    v15 = (eax13 - (eax13 + reinterpret_cast<uint1_t>(eax13 < eax13 + reinterpret_cast<uint1_t>(edx11 < 1))) & 0xffffffe8) + 0x400;
    rax16 = fun_2470();
    r15_17 = rax16->f0;
    rax18 = fun_2500(r15_17);
    r14_19 = rax16->f8;
    rbp20 = rax18;
    cf21 = reinterpret_cast<unsigned char>(rax18) - 1 < 16;
    v22 = rax16->f10;
    if (!cf21) {
        rbp20 = reinterpret_cast<struct s2*>(1);
    }
    if (!cf21) {
        r15_17 = reinterpret_cast<struct s2*>(".");
    }
    rax23 = fun_2500(r14_19);
    rsp24 = reinterpret_cast<struct s2**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8 - 8 + 8);
    if (reinterpret_cast<unsigned char>(rax23) > reinterpret_cast<unsigned char>(16)) {
        r14_19 = reinterpret_cast<struct s2*>(0x98a1);
    }
    v25 = reinterpret_cast<struct s7*>(&v6->f287);
    if (r8 > rcx) {
        if (!rcx || (r8_26 = v7 / rcx, !!(v7 % rcx))) {
            addr_3834_9:
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
                __asm__("fadd dword [rip+0x58a6]");
            }
        } else {
            rdx27 = rdi % r8_26;
            rcx28 = rdi / r8_26;
            rax29 = rdx27 + rdx27 * 4;
            rax30 = rax29 + rax29;
            rdx31 = rax30 % r8_26;
            rax32 = rax30 / r8_26;
            rdx33 = rdx31 + rdx31;
            *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
            if (r8_26 <= rdx33) {
                esi35 = 2;
                if (r8_26 < rdx33) {
                    esi35 = 3;
                }
            } else {
                esi35 = 0;
                *reinterpret_cast<unsigned char*>(&esi35) = reinterpret_cast<uint1_t>(!!rdx33);
            }
            r10d36 = v8 & 16;
            if (!r10d36) 
                goto addr_3ddb_17; else 
                goto addr_39cc_18;
        }
    } else {
        if (rcx % r8) 
            goto addr_3834_9;
        rcx28 = rcx / r8 * rdi;
        if (__intrinsic()) 
            goto addr_3834_9;
        esi35 = 0;
        *reinterpret_cast<uint32_t*>(&rdi34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        r10d36 = v8 & 16;
        if (r10d36) 
            goto addr_39cc_18; else 
            goto addr_3ddb_17;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(v7) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x58c6]");
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) >= reinterpret_cast<int64_t>(0)) {
            addr_3871_24:
            __asm__("fmulp st1, st0");
            if (!(*reinterpret_cast<unsigned char*>(&v8) & 16)) {
                addr_3b68_25:
                if (v14 == 1) 
                    goto addr_3bfa_26;
                __asm__("fld tword [rip+0x5897]");
                __asm__("fcomip st0, st1");
                if (v14 <= 1) 
                    goto addr_3bfa_26;
            } else {
                addr_387e_28:
                __asm__("fild dword [rsp+0x34]");
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                __asm__("fld st0");
                goto addr_3894_29;
            }
        } else {
            goto addr_3b20_31;
        }
    } else {
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) 
            goto addr_3b20_31; else 
            goto addr_3871_24;
    }
    __asm__("fld dword [rip+0x5885]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax39) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x583d]");
    }
    zf40 = v14 == 0;
    if (zf40) 
        goto addr_3bd1_39;
    __asm__("fstp st1");
    goto addr_3bfa_26;
    addr_3bd1_39:
    __asm__("fxch st0, st1");
    __asm__("fucomip st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf40) {
            addr_3bfa_26:
            __asm__("fstp tword [rsp]");
            fun_2790(v6, 1, -1, "%.0Lf");
            *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
            rax41 = fun_2500(v6, v6);
            rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            rdx43 = rax41;
            r12_44 = rax41;
            goto addr_3c38_43;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax39 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x52e7]");
        goto addr_3bfa_26;
    } else {
        goto addr_3bfa_26;
    }
    addr_3c38_43:
    rbp45 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(v25) - reinterpret_cast<unsigned char>(rdx43));
    fun_26d0(rbp45, v6);
    rsp24 = rsp42 - 8 + 8;
    r8_46 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(rbp45) + reinterpret_cast<unsigned char>(r12_44));
    while (1) {
        if (*reinterpret_cast<unsigned char*>(&v8) & 4) {
            addr_3a60_49:
            r13_47 = reinterpret_cast<struct s2*>(0xffffffffffffffff);
            rax48 = fun_2500(r14_19, r14_19);
            rsp49 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            r12_50 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(r8_46) - reinterpret_cast<uint64_t>(rbp45));
            r15_17 = rax48;
            rax51 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(rsp49) + 80);
            v52 = rax51;
            fun_2610(rax51, rbp45, r12_50, 41);
            rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
            v54 = *reinterpret_cast<uint32_t*>(&rbx38);
            rbp55 = r8_46;
            rbx56 = r12_50;
            r12_57 = v22;
            while (1) {
                *reinterpret_cast<uint32_t*>(&rax58) = *r12_57;
                *reinterpret_cast<int32_t*>(&rax58 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&rax58)) {
                    if (reinterpret_cast<unsigned char>(r13_47) > reinterpret_cast<unsigned char>(rbx56)) {
                        r13_47 = rbx56;
                    }
                    rbx56 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rbx56) - reinterpret_cast<unsigned char>(r13_47));
                    rsi59 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(v52) + reinterpret_cast<unsigned char>(rbx56));
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rax58) > 0x7e) {
                        r13_47 = rbx56;
                        rsi59 = v52;
                        *reinterpret_cast<int32_t*>(&rbx56) = 0;
                        *reinterpret_cast<int32_t*>(&rbx56 + 4) = 0;
                    } else {
                        if (reinterpret_cast<unsigned char>(rax58) > reinterpret_cast<unsigned char>(rbx56)) {
                            rax58 = rbx56;
                        }
                        rbx56 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rbx56) - reinterpret_cast<unsigned char>(rax58));
                        r13_47 = rax58;
                        rsi59 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(v52) + reinterpret_cast<unsigned char>(rbx56));
                    }
                    ++r12_57;
                }
                rbp45 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(rbp55) - reinterpret_cast<unsigned char>(r13_47));
                fun_2620(rbp45, rsi59, r13_47, 41);
                rsp24 = reinterpret_cast<struct s2**>(reinterpret_cast<int64_t>(rsp53) - 8 + 8);
                if (!rbx56) 
                    break;
                rbp55 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(rbp45) - reinterpret_cast<unsigned char>(r15_17));
                fun_2620(rbp55, r14_19, r15_17, 41);
                rsp53 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            }
            *reinterpret_cast<uint32_t*>(&rbx38) = v54;
        }
        addr_3c5c_63:
        if (!(*reinterpret_cast<unsigned char*>(&v8) & 0x80)) 
            goto addr_3c7f_64;
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 0xffffffff) {
            rcx60 = v7;
            if (rcx60 <= 1) {
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                goto addr_3c6c_68;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx61) = v15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx61) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx38) = 1;
                *reinterpret_cast<int32_t*>(&rax62) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax62) + 4) = 0;
                do {
                    rax62 = rax62 * rdx61;
                    if (rcx60 <= rax62) 
                        break;
                    *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
                } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
                eax63 = v8 & 0x100;
                if (!(v8 & 64)) 
                    goto addr_3e48_73;
            }
        } else {
            addr_3c6c_68:
            eax63 = v8 & 0x100;
            if (eax63 | *reinterpret_cast<uint32_t*>(&rbx38)) {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 64)) {
                    addr_3e40_75:
                    if (!*reinterpret_cast<uint32_t*>(&rbx38)) {
                        rdx64 = v25;
                        if (eax63) {
                            addr_3e8a_77:
                            rdx64->f0 = reinterpret_cast<struct s2*>(66);
                            v25 = reinterpret_cast<struct s7*>(&rdx64->f1);
                            goto addr_3c7f_64;
                        } else {
                            goto addr_3c7f_64;
                        }
                    } else {
                        addr_3e48_73:
                        rdx64 = reinterpret_cast<struct s7*>(&v25->f1);
                        if (v12 || *reinterpret_cast<uint32_t*>(&rbx38) != 1) {
                            rbx38 = *reinterpret_cast<int32_t*>(&rbx38);
                            ecx65 = *reinterpret_cast<unsigned char*>(0x93f8 + rbx38);
                            v25->f0 = *reinterpret_cast<struct s2**>(&ecx65);
                            if (eax63) {
                                *reinterpret_cast<uint32_t*>(&r8_46) = v12;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_46) + 4) = 0;
                                if (*reinterpret_cast<uint32_t*>(&r8_46)) {
                                    v25->f1 = 0x69;
                                    rdx64 = v25 + 1;
                                    goto addr_3e8a_77;
                                }
                            }
                        } else {
                            v25->f0 = reinterpret_cast<struct s2*>(0x6b);
                            if (eax63) 
                                goto addr_3e8a_77; else 
                                goto addr_413b_83;
                        }
                    }
                }
            } else {
                addr_3c7f_64:
                v25->f0 = reinterpret_cast<struct s2*>(0);
                rax66 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
                if (rax66) 
                    goto addr_431b_85; else 
                    break;
            }
        }
        v6->f287 = reinterpret_cast<struct s2*>(32);
        v25 = reinterpret_cast<struct s7*>(&v6->f288);
        goto addr_3e40_75;
        addr_413b_83:
        v25 = rdx64;
        goto addr_3c7f_64;
        addr_431b_85:
        rax67 = fun_2510();
        rsp24 = rsp24 - 8 + 8;
        addr_4320_87:
        r8_46->f0 = *reinterpret_cast<struct s2**>(&r15_17->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(r8_46) + reinterpret_cast<uint64_t>(rax67) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax67) - 4);
        addr_4109_88:
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_46) + 0xffffffffffffffff) = 49;
        rbp45 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(r8_46) + 0xffffffffffffffff);
    }
    return rbp45;
    addr_3894_29:
    while (tmp32_68 = *reinterpret_cast<uint32_t*>(&rbx38) + 1, cf69 = tmp32_68 < *reinterpret_cast<uint32_t*>(&rbx38), *reinterpret_cast<uint32_t*>(&rbx38) = tmp32_68, !cf69) {
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) 
            goto addr_38a6_91;
        __asm__("fstp st1");
        __asm__("fxch st0, st2");
    }
    __asm__("fstp st2");
    __asm__("fstp st2");
    addr_38b4_94:
    r15_17 = reinterpret_cast<struct s2*>(&rbp20->f1);
    __asm__("fdivrp st1, st0");
    rbp70 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(&rbp20->f2) + reinterpret_cast<uint1_t>(v12 < 1));
    if (v14 == 1) {
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fstp tword [rsp+0x30]");
        fun_2790(v6, 1, -1, "%.1Lf");
        rax71 = fun_2500(v6, v6);
        rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        __asm__("fld tword [rsp+0x20]");
        rdx43 = rax71;
        if (reinterpret_cast<unsigned char>(rax71) > reinterpret_cast<unsigned char>(rbp70)) {
            __asm__("fld dword [rip+0x551d]");
            __asm__("fmul st1, st0");
            goto addr_3ef1_97;
        }
    }
    __asm__("fld tword [rip+0x5b3c]");
    __asm__("fcomip st0, st1");
    if (v14 <= 1) {
        __asm__("fld st0");
        goto addr_3d50_100;
    }
    __asm__("fld dword [rip+0x5b26]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax72 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax72 = v37;
    }
    v37 = rax72;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax72) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x5ade]");
    }
    zf73 = v14 == 0;
    if (zf73) 
        goto addr_3932_107;
    __asm__("fxch st0, st1");
    goto addr_3d50_100;
    addr_3932_107:
    __asm__("fxch st0, st1");
    __asm__("fucomi st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st1");
    } else {
        if (zf73) {
            addr_3d50_100:
            __asm__("fstp tword [rsp+0x20]");
            __asm__("fstp tword [rsp]");
            fun_2790(v6, 1, -1, "%.1Lf");
            rax74 = fun_2500(v6, v6);
            rdx43 = rax74;
            rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            __asm__("fld tword [rsp+0x20]");
            cf75 = reinterpret_cast<unsigned char>(rdx43) < reinterpret_cast<unsigned char>(rbp70);
            below_or_equal76 = reinterpret_cast<unsigned char>(rdx43) <= reinterpret_cast<unsigned char>(rbp70);
            if (!below_or_equal76) {
                __asm__("fld dword [rip+0x54b6]");
                __asm__("fmul st1, st0");
                goto addr_3f58_112;
            } else {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 8)) {
                    __asm__("fstp st0");
                    goto addr_3dba_115;
                } else {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v6) + reinterpret_cast<unsigned char>(rdx43) + 0xffffffffffffffff) == 48) {
                        __asm__("fld dword [rip+0x5396]");
                        cf75 = v14 < 1;
                        below_or_equal76 = v14 <= 1;
                        __asm__("fmul st1, st0");
                        if (v14 == 1) {
                            goto addr_3ef1_97;
                        }
                    } else {
                        __asm__("fstp st0");
                        goto addr_3dba_115;
                    }
                }
            }
        } else {
            __asm__("fstp st1");
        }
    }
    rax77 = rax72 + 1;
    v37 = rax77;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax77) >= reinterpret_cast<int64_t>(0)) {
        __asm__("fxch st0, st1");
        goto addr_3d50_100;
    } else {
        __asm__("fadd dword [rip+0x5aa3]");
        __asm__("fxch st0, st1");
        goto addr_3d50_100;
    }
    addr_3f58_112:
    __asm__("fld tword [rip+0x54b2]");
    __asm__("fcomip st0, st2");
    if (below_or_equal76) {
        addr_3ef1_97:
        __asm__("fdivp st1, st0");
        __asm__("fstp tword [rsp]");
        fun_2790(v6, 1, -1, "%.0Lf");
        rax78 = fun_2500(v6, v6);
        r15_17 = reinterpret_cast<struct s2*>(0x37f4);
        rsp42 = rsp42 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        rdx43 = rax78;
        r12_44 = rax78;
        goto addr_3c38_43;
    } else {
        __asm__("fld dword [rip+0x54a0]");
        __asm__("fxch st0, st2");
        __asm__("fcomi st0, st2");
        if (!cf75) {
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fsubr st2, st0");
            __asm__("fxch st0, st2");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fxch st0, st1");
            rax79 = v37;
            __asm__("btc rax, 0x3f");
        } else {
            __asm__("fstp st2");
            __asm__("fxch st0, st1");
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fld st0");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            rax79 = v37;
        }
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rax79) < reinterpret_cast<int64_t>(0)) {
            __asm__("fadd dword [rip+0x5456]");
        }
        zf80 = v14 == 0;
        if (zf80) 
            goto addr_3fb6_130;
    }
    __asm__("fstp st1");
    goto addr_3fe2_132;
    addr_3fb6_130:
    __asm__("fucomi st0, st1");
    __asm__("fstp st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf80) {
            addr_3fe2_132:
            __asm__("fxch st0, st1");
            goto addr_3ef1_97;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax79 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x52dc]");
        __asm__("fxch st0, st1");
        goto addr_3ef1_97;
    } else {
        goto addr_3fe2_132;
    }
    addr_3dba_115:
    r12_44 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rdx43) - reinterpret_cast<unsigned char>(r15_17));
    goto addr_3c38_43;
    addr_38a6_91:
    __asm__("fstp st2");
    __asm__("fstp st2");
    goto addr_38b4_94;
    addr_3b20_31:
    __asm__("fadd dword [rip+0x58de]");
    __asm__("fmulp st1, st0");
    if (*reinterpret_cast<unsigned char*>(&v8) & 16) 
        goto addr_387e_28;
    goto addr_3b68_25;
    addr_3ddb_17:
    *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
    goto addr_39df_140;
    addr_39cc_18:
    *reinterpret_cast<uint32_t*>(&r8_81) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_81) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbx38) = 0;
    r11_82 = r8_81;
    if (r8_81 > rcx28) 
        goto addr_39df_140;
    do {
        rdx83 = rcx28 % r8_81;
        *reinterpret_cast<int32_t*>(&rcx84) = reinterpret_cast<int32_t>(esi35) >> 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx84) + 4) = 0;
        r9_85 = rcx28 / r8_81;
        *reinterpret_cast<int32_t*>(&rax86) = static_cast<int32_t>(rdx83 + rdx83 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax86) + 4) = 0;
        eax87 = static_cast<int32_t>(rdi34 + rax86 * 2);
        *reinterpret_cast<uint32_t*>(&rdx88) = eax87 % *reinterpret_cast<uint32_t*>(&r11_82);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx88) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax89) = eax87 / *reinterpret_cast<uint32_t*>(&r11_82);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0;
        edx90 = static_cast<uint32_t>(rcx84 + rdx88 * 2);
        *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax89);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        rcx28 = r9_85;
        esi91 = esi35 + edx90;
        if (*reinterpret_cast<uint32_t*>(&r11_82) > edx90) {
            esi35 = reinterpret_cast<uint1_t>(!!esi91);
        } else {
            esi35 = static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r11_82) < esi91)) + 2;
        }
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (r8_81 > r9_85) 
            break;
    } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
    goto addr_39df_140;
    if (r9_85 > 9) {
        addr_39df_140:
        r8_46 = v25;
        if (v14 == 1) {
            rax92 = rcx28;
            *reinterpret_cast<uint32_t*>(&rax93) = *reinterpret_cast<uint32_t*>(&rax92) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax93) + 4) = 0;
            if (reinterpret_cast<int32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!(rax93 + esi35))) + *reinterpret_cast<uint32_t*>(&rdi34)) <= reinterpret_cast<int32_t>(5)) {
                goto addr_3a18_149;
            }
        } else {
            if (v14) 
                goto addr_3a18_149;
            esi94 = esi35 + *reinterpret_cast<uint32_t*>(&rdi34);
            if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(esi94) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(esi94 == 0)) 
                goto addr_3a18_149;
        }
    } else {
        if (v14 != 1) {
            if (v14) 
                goto addr_41dc_154;
            if (!esi35) 
                goto addr_41dc_154; else 
                goto addr_4167_156;
        }
        if (reinterpret_cast<int32_t>((*reinterpret_cast<uint32_t*>(&rax89) & 1) + esi35) > reinterpret_cast<int32_t>(2)) {
            addr_4167_156:
            *reinterpret_cast<int32_t*>(&rdx95) = static_cast<int32_t>(rax89 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx95) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax89) == 9) {
                rcx28 = r9_85 + 1;
                if (r9_85 == 9) {
                    r8_46 = v25;
                    goto addr_41aa_160;
                } else {
                    esi35 = 0;
                    goto addr_41e4_162;
                }
            } else {
                eax96 = static_cast<uint32_t>(rdx95 + 48);
                goto addr_4176_164;
            }
        } else {
            addr_41dc_154:
            if (*reinterpret_cast<uint32_t*>(&rax89)) {
                eax96 = *reinterpret_cast<uint32_t*>(&rax89) + 48;
                goto addr_4176_164;
            } else {
                addr_41e4_162:
                r8_46 = v25;
                if (*reinterpret_cast<unsigned char*>(&v8) & 8) {
                    addr_41ac_166:
                    *reinterpret_cast<uint32_t*>(&rdi34) = 0;
                    if (v14 == 1) {
                        goto addr_3a18_149;
                    }
                } else {
                    eax96 = 48;
                    goto addr_4176_164;
                }
            }
        }
    }
    ++rcx28;
    if (!r10d36) 
        goto addr_3a18_149;
    *reinterpret_cast<uint32_t*>(&rax97) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
    if (rax97 != rcx28) {
        goto addr_3a18_149;
    }
    if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) {
        addr_3a18_149:
        rbp45 = r8_46;
    } else {
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (*reinterpret_cast<unsigned char*>(&v8) & 8) 
            goto addr_4109_88;
        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r8_46) + 0xffffffffffffffff) = 48;
        r8_46 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(r8_46) + ~reinterpret_cast<unsigned char>(rbp20));
        *reinterpret_cast<uint32_t*>(&rax67) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax67) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) 
            goto addr_42b0_175; else 
            goto addr_427e_176;
    }
    do {
        rbp45 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(rbp45) - 1);
        rdx98 = __intrinsic() >> 3;
        rdi99 = rdx98 + rdx98 * 4;
        rax100 = rcx28 - (rdi99 + rdi99);
        eax101 = *reinterpret_cast<int32_t*>(&rax100) + 48;
        rbp45->f0 = *reinterpret_cast<struct s2**>(&eax101);
        rax102 = rcx28;
        rcx28 = rdx98;
    } while (rax102 > 9);
    if (!(*reinterpret_cast<unsigned char*>(&v8) & 4)) 
        goto addr_3c5c_63; else 
        goto addr_3a60_49;
    addr_42b0_175:
    rcx103 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_46 + 4) & 0xfffffffffffffff8);
    r8_46->f0 = *reinterpret_cast<struct s2**>(&r15_17->f0);
    *reinterpret_cast<uint32_t*>(&rax104) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r8_46) + reinterpret_cast<uint64_t>(rax104) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax104) - 8);
    rax105 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_46) - reinterpret_cast<uint64_t>(rcx103));
    r15_17 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax105));
    eax106 = *reinterpret_cast<int32_t*>(&rax105) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
    if (eax106 < 8) 
        goto addr_4109_88;
    eax107 = eax106 & 0xfffffff8;
    edx108 = 0;
    do {
        *reinterpret_cast<uint32_t*>(&rsi109) = edx108;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi109) + 4) = 0;
        edx108 = edx108 + 8;
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rcx103) + reinterpret_cast<uint64_t>(rsi109)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rsi109));
    } while (edx108 < eax107);
    goto addr_4109_88;
    addr_427e_176:
    if (*reinterpret_cast<uint32_t*>(&rbp20) & 4) 
        goto addr_4320_87;
    if (!*reinterpret_cast<uint32_t*>(&rax67)) 
        goto addr_4109_88;
    edx110 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&r15_17->f0));
    r8_46->f0 = *reinterpret_cast<struct s2**>(&edx110);
    if (!(*reinterpret_cast<unsigned char*>(&rax67) & 2)) 
        goto addr_4109_88;
    edx111 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax67) - 2);
    *reinterpret_cast<int16_t*>(reinterpret_cast<uint64_t>(r8_46) + reinterpret_cast<uint64_t>(rax67) - 2) = *reinterpret_cast<int16_t*>(&edx111);
    goto addr_4109_88;
    addr_41aa_160:
    esi35 = 0;
    goto addr_41ac_166;
    addr_4176_164:
    v6->f286 = *reinterpret_cast<signed char*>(&eax96);
    *reinterpret_cast<uint32_t*>(&rax112) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax112) + 4) = 0;
    r8_46 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(&v6->f286) - reinterpret_cast<unsigned char>(rbp20));
    if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) {
        rsi113 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_46 + 4) & 0xfffffffffffffff8);
        r8_46->f0 = *reinterpret_cast<struct s2**>(&r15_17->f0);
        *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r8_46) + reinterpret_cast<uint64_t>(rax114) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax114) - 8);
        rax115 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_46) - reinterpret_cast<uint64_t>(rsi113));
        rdi116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax115));
        eax117 = *reinterpret_cast<int32_t*>(&rax115) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
        if (eax117 >= 8) {
            r9d118 = eax117 & 0xfffffff8;
            eax119 = 0;
            do {
                *reinterpret_cast<uint32_t*>(&rdx120) = eax119;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx120) + 4) = 0;
                eax119 = eax119 + 8;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsi113) + reinterpret_cast<int64_t>(rdx120)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi116) + reinterpret_cast<int64_t>(rdx120));
            } while (eax119 < r9d118);
            goto addr_41aa_160;
        }
    } else {
        if (*reinterpret_cast<unsigned char*>(&rbp20) & 4) {
            r8_46->f0 = *reinterpret_cast<struct s2**>(&r15_17->f0);
            *reinterpret_cast<int32_t*>(reinterpret_cast<uint64_t>(r8_46) + reinterpret_cast<uint64_t>(rax112) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax112) - 4);
            goto addr_41aa_160;
        } else {
            if (*reinterpret_cast<uint32_t*>(&rax112) && (edx121 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&r15_17->f0)), r8_46->f0 = *reinterpret_cast<struct s2**>(&edx121), !!(*reinterpret_cast<unsigned char*>(&rax112) & 2))) {
                edx122 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax112) - 2);
                *reinterpret_cast<int16_t*>(reinterpret_cast<uint64_t>(r8_46) + reinterpret_cast<uint64_t>(rax112) - 2) = *reinterpret_cast<int16_t*>(&edx122);
                goto addr_41aa_160;
            }
        }
    }
}

struct s5* fun_23f0(int64_t rdi, struct s5** rsi);

int64_t argmatch(struct s5* rdi, struct s2* rsi, struct s2** rdx, struct s2** rcx);

int64_t xstrtoumax(struct s5* rdi, struct s5** rsi);

int64_t fun_4353(struct s5* rdi, struct s5** rsi, int64_t* rdx) {
    struct s5** r13_4;
    int64_t* rbp5;
    struct s5* rbx6;
    void* rsp7;
    struct s2* rax8;
    struct s2* v9;
    struct s5* rax10;
    struct s5* rax11;
    struct s5* r12d12;
    int64_t rax13;
    struct s5* rax14;
    int64_t rax15;
    struct s5** rsi16;
    int64_t rdx17;
    int64_t rcx18;
    int32_t edx19;
    struct s5* rcx20;
    struct s5* v21;
    int64_t rdi22;
    int32_t edx23;
    struct s5* rax24;
    uint64_t rax25;
    int64_t rax26;
    void* rdx27;

    __asm__("cli ");
    r13_4 = rsi;
    rbp5 = rdx;
    rbx6 = rdi;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 16);
    rax8 = g28;
    v9 = rax8;
    if (rdi || ((rax10 = fun_23f0("BLOCK_SIZE", rsi), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), rbx6 = rax10, !!rax10) || (rax11 = fun_23f0("BLOCKSIZE", rsi), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), rbx6 = rax11, !!rax11))) {
        r12d12 = reinterpret_cast<struct s5*>(0);
        if (*reinterpret_cast<unsigned char*>(&rbx6->f0) == 39) {
            rbx6 = reinterpret_cast<struct s5*>(&rbx6->f1);
            r12d12 = reinterpret_cast<struct s5*>(4);
        }
        rax13 = argmatch(rbx6, 0xcb40, 0x93f0, 4);
        if (*reinterpret_cast<int32_t*>(&rax13) >= 0) 
            goto addr_43b6_5;
    } else {
        rax14 = fun_23f0("POSIXLY_CORRECT", rsi);
        if (!rax14) {
            *rbp5 = 0x400;
            *reinterpret_cast<int32_t*>(&rax15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
            *r13_4 = reinterpret_cast<struct s5*>(0);
            goto addr_43ca_8;
        } else {
            *rbp5 = 0x200;
            *reinterpret_cast<int32_t*>(&rax15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
            *r13_4 = reinterpret_cast<struct s5*>(0);
            goto addr_43ca_8;
        }
    }
    rsi16 = reinterpret_cast<struct s5**>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
    rax15 = xstrtoumax(rbx6, rsi16);
    if (*reinterpret_cast<int32_t*>(&rax15)) {
        *r13_4 = reinterpret_cast<struct s5*>(0);
        rdx17 = *rbp5;
    } else {
        *reinterpret_cast<uint32_t*>(&rcx18) = *reinterpret_cast<unsigned char*>(&rbx6->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx18) + 4) = 0;
        edx19 = static_cast<int32_t>(rcx18 - 48);
        rcx20 = v21;
        if (*reinterpret_cast<unsigned char*>(&edx19) <= 9) {
            goto addr_4447_14;
        }
        do {
            if (rcx20 == rbx6) 
                break;
            *reinterpret_cast<uint32_t*>(&rdi22) = rbx6->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
            rbx6 = reinterpret_cast<struct s5*>(&rbx6->f1);
            edx23 = static_cast<int32_t>(rdi22 - 48);
        } while (*reinterpret_cast<unsigned char*>(&edx23) > 9);
        goto addr_4447_14;
        if (*reinterpret_cast<signed char*>(reinterpret_cast<uint32_t>(rcx20) - 1) == 66) 
            goto addr_4500_18; else 
            goto addr_443f_19;
    }
    addr_4464_20:
    if (!rdx17) {
        rax24 = fun_23f0("POSIXLY_CORRECT", rsi16);
        rax25 = reinterpret_cast<uint32_t>(rax24) - (reinterpret_cast<uint32_t>(rax24) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(rax24) < reinterpret_cast<uint32_t>(reinterpret_cast<uint32_t>(rax24) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>(rax24) < reinterpret_cast<uint32_t>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax26) = *reinterpret_cast<uint32_t*>(&rax25) & 0x200;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
        *rbp5 = rax26 + 0x200;
        *reinterpret_cast<int32_t*>(&rax15) = 4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    }
    addr_43ca_8:
    rdx27 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
    if (rdx27) {
        fun_2510();
    } else {
        return rax15;
    }
    addr_4500_18:
    r12d12 = reinterpret_cast<struct s5*>(reinterpret_cast<uint32_t>(r12d12) | 0x180);
    if (*reinterpret_cast<signed char*>(&(rcx20 - 1)->f0) != 0x69) {
        addr_4447_14:
        rdx17 = *rbp5;
        *r13_4 = r12d12;
        goto addr_4464_20;
    }
    addr_4443_25:
    r12d12 = reinterpret_cast<struct s5*>(reinterpret_cast<uint32_t>(r12d12) | 32);
    goto addr_4447_14;
    addr_443f_19:
    *reinterpret_cast<unsigned char*>(&r12d12) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d12) | 0x80);
    goto addr_4443_25;
    addr_43b6_5:
    *rbp5 = 1;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    *r13_4 = reinterpret_cast<struct s5*>(reinterpret_cast<uint32_t>(r12d12) | *reinterpret_cast<uint32_t*>(0x93f0 + *reinterpret_cast<int32_t*>(&rax13) * 4));
    goto addr_43ca_8;
}

void fun_2740(struct s0* rdi, int64_t rsi, int64_t rdx, struct s2* rcx);

struct s9 {
    signed char[1] pad1;
    struct s2* f1;
    signed char[2] pad4;
    struct s2* f4;
};

struct s9* fun_2560();

struct s2* __progname = reinterpret_cast<struct s2*>(0);

struct s2* __progname_full = reinterpret_cast<struct s2*>(0);

void fun_4543(struct s2* rdi) {
    struct s2* rcx2;
    struct s2* rbx3;
    struct s9* rax4;
    struct s2* r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2740("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2410("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2560();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s2*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_2430(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s2**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s2*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5ce3(int64_t rdi) {
    int64_t rbp2;
    struct s2** rax3;
    struct s2* r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2420();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xd220;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5d23(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5d43(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd220);
    }
    *rdi = esi;
    return 0xd220;
}

int64_t fun_5d63(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xd220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s10 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5da3(struct s10* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s10*>(0xd220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s11 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s11* fun_5dc3(struct s11* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s11*>(0xd220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x27aa;
    if (!rdx) 
        goto 0x27aa;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xd220;
}

struct s12 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5e03(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s12* r8) {
    struct s12* rbx6;
    struct s2** rax7;
    struct s2* r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s12*>(0xd220);
    }
    rax7 = fun_2420();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5e36);
    *rax7 = r15d8;
    return rax13;
}

struct s13 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5e83(int64_t rdi, int64_t rsi, struct s0** rdx, struct s13* rcx) {
    struct s13* rbx5;
    struct s2** rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    struct s2* v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s13*>(0xd220);
    }
    rax6 = fun_2420();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5eb1);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->pad8);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5f0c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5f73() {
    __asm__("cli ");
}

struct s0* gd078 = reinterpret_cast<struct s0*>(32);

int64_t slotvec0 = 0x100;

void fun_5f83() {
    uint32_t eax1;
    struct s0* r12_2;
    int64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rsi7;
    struct s0* rdi8;
    struct s0* rsi9;
    struct s0* rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4)) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2400(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = r12_2->f8;
    if (rdi8 != 0xd120) {
        fun_2400(rdi8, rsi9);
        gd078 = reinterpret_cast<struct s0*>(0xd120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xd070) {
        fun_2400(r12_2, rsi10);
        slotvec = reinterpret_cast<struct s0*>(0xd070);
    }
    nslots = 1;
    return;
}

void fun_6023() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6043() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6053(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6073(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_6093(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x27b0;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax6;
    }
}

struct s0* fun_6123(struct s0* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x27b5;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2510();
    } else {
        return rax7;
    }
}

struct s0* fun_61b3(int32_t edi, int64_t rsi) {
    struct s2* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x27ba;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2510();
    } else {
        return rax5;
    }
}

struct s0* fun_6243(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s2* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x27bf;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax6;
    }
}

struct s0* fun_62d3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s2* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6f40]");
    __asm__("movdqa xmm1, [rip+0x6f48]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6f31]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2510();
    } else {
        return rax10;
    }
}

struct s0* fun_6373(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s2* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6ea0]");
    __asm__("movdqa xmm1, [rip+0x6ea8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6e91]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2510();
    } else {
        return rax9;
    }
}

struct s0* fun_6413(int64_t rdi) {
    struct s2* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6e00]");
    __asm__("movdqa xmm1, [rip+0x6e08]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6de9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2510();
    } else {
        return rax3;
    }
}

struct s0* fun_64a3(int64_t rdi, int64_t rsi) {
    struct s2* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6d70]");
    __asm__("movdqa xmm1, [rip+0x6d78]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6d66]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2510();
    } else {
        return rax4;
    }
}

struct s0* fun_6533(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x27c4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax6;
    }
}

struct s0* fun_65d3(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6c3a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6c32]");
    __asm__("movdqa xmm2, [rip+0x6c3a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x27c9;
    if (!rdx) 
        goto 0x27c9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2510();
    } else {
        return rax7;
    }
}

struct s0* fun_6673(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s2* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6b9a]");
    __asm__("movdqa xmm1, [rip+0x6ba2]");
    __asm__("movdqa xmm2, [rip+0x6baa]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x27ce;
    if (!rdx) 
        goto 0x27ce;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2510();
    } else {
        return rax9;
    }
}

struct s0* fun_6723(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6aea]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6ae2]");
    __asm__("movdqa xmm2, [rip+0x6aea]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x27d3;
    if (!rsi) 
        goto 0x27d3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax6;
    }
}

struct s0* fun_67c3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6a4a]");
    __asm__("movdqa xmm1, [rip+0x6a52]");
    __asm__("movdqa xmm2, [rip+0x6a5a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x27d8;
    if (!rsi) 
        goto 0x27d8;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2510();
    } else {
        return rax7;
    }
}

void fun_6863() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6873(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6893() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_68b3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_68d3() {
    __asm__("cli ");
}

struct s14 {
    struct s2* f0;
    signed char[7] pad8;
    struct s2* f8;
    signed char[7] pad16;
    struct s2* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t* f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t* f40;
};

void fun_2600(int64_t rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, struct s2* r8, struct s2* r9);

void fun_68f3(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, struct s14* r8, struct s2* r9) {
    struct s2* r12_7;
    int64_t v8;
    int64_t* v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t* v13;
    int64_t v14;
    int64_t* v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t* v19;
    struct s2* rax20;
    int64_t v21;
    int64_t* v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t* v26;
    struct s2* rax27;
    int64_t v28;
    int64_t* v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t* v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t* rcx37;
    int64_t r15_38;
    int64_t* v39;
    struct s2* r14_40;
    struct s2* r13_41;
    struct s2* r12_42;
    struct s2* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2750(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2750(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_24e0();
    fun_2750(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2600(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_24e0();
    fun_2750(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2600(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_24e0();
        fun_2750(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x9b48 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x9b48;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6d63() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s15 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6d83(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s15* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s15* rcx8;
    struct s2* rax9;
    struct s2* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2510();
    } else {
        return;
    }
}

void fun_6e23(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s2* rax15;
    struct s2* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6ec6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6ed0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2510();
    } else {
        return;
    }
    addr_6ec6_5:
    goto addr_6ed0_7;
}

void fun_6f03() {
    struct s2* rsi1;
    struct s2* rdx2;
    struct s2* rcx3;
    struct s2* r8_4;
    struct s2* r9_5;
    struct s2* rax6;
    struct s2* rcx7;
    int64_t r8_8;
    struct s2* rax9;
    int64_t r8_10;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2600(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_24e0();
    fun_26b0(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8);
    rax9 = fun_24e0();
    fun_26b0(1, rax9, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_10);
    fun_24e0();
    goto fun_26b0;
}

int64_t fun_2460();

void xalloc_die();

void fun_6fa3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2460();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6fe3(unsigned char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_2640(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7003(unsigned char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_2640(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7023(unsigned char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_2640(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2680();

void fun_7043(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2680();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7073() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_70a3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2460();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_70e3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7123(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7153(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_71a3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2460();
            if (rax5) 
                break;
            addr_71ed_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_71ed_5;
        rax8 = fun_2460();
        if (rax8) 
            goto addr_71d6_9;
        if (rbx4) 
            goto addr_71ed_5;
        addr_71d6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7233(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2460();
            if (rax8) 
                break;
            addr_727a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_727a_5;
        rax11 = fun_2460();
        if (rax11) 
            goto addr_7262_9;
        if (!rbx6) 
            goto addr_7262_9;
        if (r12_4) 
            goto addr_727a_5;
        addr_7262_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_72c3(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_736d_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7380_10:
                *r12_8 = 0;
            }
            addr_7320_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_7346_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7394_14;
            if (rcx10 <= rsi9) 
                goto addr_733d_16;
            if (rsi9 >= 0) 
                goto addr_7394_14;
            addr_733d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7394_14;
            addr_7346_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2680();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7394_14;
            if (!rbp13) 
                break;
            addr_7394_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_736d_9;
        } else {
            if (!r13_6) 
                goto addr_7380_10;
            goto addr_7320_11;
        }
    }
}

int64_t fun_25e0();

void fun_73c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_73f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7423() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7443() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25e0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7463(int64_t rdi, unsigned char* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2640(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2620;
    }
}

void fun_74a3(int64_t rdi, unsigned char* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2640(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2620;
    }
}

struct s16 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_74e3(int64_t rdi, struct s16* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2640(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2620;
    }
}

void fun_7523(struct s2* rdi) {
    struct s2* rax2;
    struct s0* rax3;

    __asm__("cli ");
    rax2 = fun_2500(rdi);
    rax3 = fun_2640(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2620;
    }
}

void fun_7563() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_24e0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_26e0();
    fun_2410(rdi1);
}

void fun_2580(int64_t rdi);

void** fun_2780(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx);

struct s2* fun_2710(struct s2* rdi);

int64_t fun_2540(struct s2* rdi);

int64_t fun_75a3(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, struct s2* r8) {
    struct s2* v6;
    struct s2* rax7;
    struct s2* v8;
    void* rcx9;
    struct s2* rbx10;
    uint32_t r12d11;
    void* r9_12;
    struct s2* rax13;
    struct s2* r15_14;
    void* rax15;
    int64_t rax16;
    struct s2* rbp17;
    struct s2* r13_18;
    struct s2** rax19;
    struct s2** r12_20;
    void** rax21;
    struct s2* rax22;
    int64_t rdx23;
    struct s2* rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<struct s2*>("lib/xstrtol.c");
        fun_2580("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2510();
            while (1) {
                rbx10 = reinterpret_cast<struct s2*>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_7914_6;
                    rbx10 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_7914_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<struct s2**>(&r15_14->f0) = rax13;
            if (*reinterpret_cast<struct s2**>(&rax13->f0)) {
                r12d11 = r12d11 | 2;
            }
            addr_765d_12:
            *reinterpret_cast<struct s2**>(&v6->f0) = rbx10;
            addr_7665_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2420();
    *rax19 = reinterpret_cast<struct s2*>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&rbp17->f0));
    rax21 = fun_2780(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = rax22->f1;
        rax22 = reinterpret_cast<struct s2*>(&rax22->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_769b_21;
    rsi = r15_14;
    rax24 = fun_2710(rbp17);
    r8 = *reinterpret_cast<struct s2**>(&r15_14->f0);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&rbp17->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2540(r13_18), r8 = r8, rax26 == 0))) {
            addr_769b_21:
            r12d11 = 4;
            goto addr_7665_13;
        } else {
            addr_76d9_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2540(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = r8->f1;
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(r8->f2 == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x9bf8 + rbp33 * 4) + 0x9bf8;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_769b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_765d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&r8->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_765d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2540(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_76d9_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<struct s2**>(&v6->f0) = rbx10;
    goto addr_7665_13;
}

void fun_79d3() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_25c0(struct s2* rdi, struct s2* rsi, struct s2* rdx, ...);

int64_t fun_79e3(struct s2* rdi, struct s2** rsi, struct s2* rdx, struct s2* rcx) {
    struct s2* r14_5;
    struct s2* r13_6;
    struct s2* rbp7;
    struct s2** v8;
    struct s2* v9;
    struct s2* rax10;
    struct s2* r15_11;
    int64_t v12;
    unsigned char v13;
    struct s2* r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    struct s2* rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2500(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_2430(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_7a63_5:
                ++rbx15;
                rbp7 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_7ab0_6; else 
                    continue;
            } else {
                rax18 = fun_2500(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_7ae0_8;
                if (v16 == -1) 
                    goto addr_7a9e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_7a63_5;
            } else {
                eax19 = fun_25c0(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_7a63_5;
            }
            addr_7a9e_10:
            v16 = rbx15;
            goto addr_7a63_5;
        }
    }
    addr_7ac5_16:
    return v12;
    addr_7ab0_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_7ac5_16;
    addr_7ae0_8:
    v12 = rbx15;
    goto addr_7ac5_16;
}

int64_t fun_7af3(struct s5* rdi, struct s2** rsi, struct s2* rdx, struct s2** rcx, struct s2** r8, int64_t r9, int64_t a7, struct s2* a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16) {
    int64_t v17;
    int64_t v18;
    int64_t r12_19;
    struct s5* r12_20;
    int64_t v21;
    int64_t rbp22;
    struct s2** v23;
    struct s2** rbx24;
    struct s2* rdi25;
    struct s2** rbp26;
    int64_t rbx27;
    int32_t eax28;

    v17 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v18 = r12_19;
    r12_20 = rdi;
    v21 = rbp22;
    v23 = rbx24;
    rdi25 = *rsi;
    if (!rdi25) {
        addr_7b38_2:
        return -1;
    } else {
        rbp26 = rsi;
        *reinterpret_cast<int32_t*>(&rbx27) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx27) + 4) = 0;
        do {
            eax28 = fun_25f0(rdi25, r12_20, rdx, rcx, r8, r9, v23, v21, v18, v17, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16);
            if (!eax28) 
                break;
            ++rbx27;
            rdi25 = rbp26[rbx27 * 8];
        } while (rdi25);
        goto addr_7b38_2;
    }
    return rbx27;
}

int64_t quote_n(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t quotearg_n_style();

void fun_7b53(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_24e0();
    } else {
        fun_24e0();
    }
    quote_n(1, rdi, 5);
    quotearg_n_style();
    goto fun_26e0;
}

struct s2* quote(int64_t rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx);

void fun_7be3(int64_t* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, struct s2* r8, struct s2* r9) {
    struct s2* r13_7;
    int64_t* v8;
    int64_t* r12_9;
    struct s2* r12_10;
    struct s2* rdx11;
    int64_t v12;
    int64_t rbp13;
    struct s2* rbp14;
    int64_t v15;
    int64_t rbx16;
    struct s2* r14_17;
    int64_t* v18;
    struct s2* rax19;
    struct s2* rsi20;
    int64_t r15_21;
    int64_t rbx22;
    uint32_t eax23;
    struct s2* rax24;
    struct s2* rdi25;
    int64_t v26;
    int64_t v27;
    struct s2* rax28;
    struct s2* rdi29;
    int64_t v30;
    int64_t v31;
    struct s2* rdi32;
    signed char* rax33;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    v8 = r12_9;
    r12_10 = rdx;
    *reinterpret_cast<int32_t*>(&rdx11) = 5;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    v12 = rbp13;
    rbp14 = rsi;
    v15 = rbx16;
    r14_17 = stderr;
    v18 = rdi;
    rax19 = fun_24e0();
    rsi20 = r14_17;
    fun_25d0(rax19, rsi20, 5, rcx);
    r15_21 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx22) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx22) + 4) = 0;
    if (r15_21) {
        do {
            if (!rbx22 || (rdx11 = r12_10, rsi20 = rbp14, eax23 = fun_25c0(r13_7, rsi20, rdx11, r13_7, rsi20, rdx11), !!eax23)) {
                r13_7 = rbp14;
                rax24 = quote(r15_21, rsi20, rdx11, rcx);
                rdi25 = stderr;
                rdx11 = reinterpret_cast<struct s2*>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi20) = 1;
                *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
                rcx = rax24;
                fun_2750(rdi25, 1, "\n  - %s", rcx, r8, r9, v26, v18, v27, v15, v12, v8);
            } else {
                rax28 = quote(r15_21, rsi20, rdx11, rcx);
                rdi29 = stderr;
                *reinterpret_cast<int32_t*>(&rsi20) = 1;
                *reinterpret_cast<int32_t*>(&rsi20 + 4) = 0;
                rdx11 = reinterpret_cast<struct s2*>(", %s");
                rcx = rax28;
                fun_2750(rdi29, 1, ", %s", rcx, r8, r9, v30, v18, v31, v15, v12, v8);
            }
            ++rbx22;
            rbp14 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rbp14) + reinterpret_cast<unsigned char>(r12_10));
            r15_21 = v18[rbx22];
        } while (r15_21);
    }
    rdi32 = stderr;
    rax33 = rdi32->f28;
    if (reinterpret_cast<uint64_t>(rax33) >= reinterpret_cast<uint64_t>(rdi32->f30)) {
        goto fun_2550;
    } else {
        rdi32->f28 = rax33 + 1;
        *rax33 = 10;
        return;
    }
}

void argmatch_invalid(int64_t rdi, struct s5* rsi, int64_t rdx, struct s2** rcx);

void argmatch_valid(struct s2* rdi, struct s2** rsi, struct s2** rdx, struct s2** rcx);

int64_t fun_7d13(int64_t rdi, struct s5* rsi, struct s2* rdx, struct s2** rcx, struct s2** r8, int64_t r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t v11;
    int64_t v12;
    int64_t r15_13;
    int64_t r15_14;
    int64_t v15;
    int64_t r14_16;
    struct s5* r14_17;
    int64_t v18;
    int64_t r13_19;
    struct s2** r13_20;
    struct s2* v21;
    struct s2* r12_22;
    struct s2** r12_23;
    int64_t v24;
    int64_t rbp25;
    struct s2* rbp26;
    int64_t v27;
    int64_t rbx28;
    int64_t v29;
    int64_t rax30;
    struct s2* rdi31;
    int64_t rbx32;
    struct s2** v33;
    int64_t v34;
    int32_t eax35;

    v11 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v12 = r15_13;
    r15_14 = rdi;
    v15 = r14_16;
    r14_17 = rsi;
    v18 = r13_19;
    r13_20 = r8;
    v21 = r12_22;
    r12_23 = rcx;
    v24 = rbp25;
    rbp26 = rdx;
    v27 = rbx28;
    v29 = r9;
    if (*reinterpret_cast<signed char*>(&a7)) {
        rcx = r8;
        rax30 = argmatch(r14_17, rbp26, r12_23, rcx);
        if (rax30 < 0) {
            addr_7d57_3:
            argmatch_invalid(r15_14, r14_17, rax30, rcx);
            argmatch_valid(rbp26, r12_23, r13_20, rcx);
            v29(rbp26, r12_23, r13_20, rcx);
            rax30 = -1;
            goto addr_7dce_4;
        } else {
            addr_7dce_4:
            return rax30;
        }
    }
    rdi31 = *reinterpret_cast<struct s2**>(&rdx->f0);
    *reinterpret_cast<int32_t*>(&rbx32) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx32) + 4) = 0;
    if (rdi31) {
        do {
            eax35 = fun_25f0(rdi31, r14_17, rdx, rcx, r8, r9, v33, v29, v34, v27, v24, v21, v18, v15, v12, v11, a7, a8, a9, a10);
            if (!eax35) 
                break;
            ++rbx32;
            rdi31 = *reinterpret_cast<struct s2**>(reinterpret_cast<unsigned char>(rbp26) + rbx32 * 8);
        } while (rdi31);
        goto addr_7d50_8;
    } else {
        goto addr_7d50_8;
    }
    return rbx32;
    addr_7d50_8:
    rax30 = -1;
    goto addr_7d57_3;
}

struct s17 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_7de3(struct s2* rdi, struct s17* rsi, struct s2* rdx, struct s2* rcx) {
    int64_t r14_5;
    struct s2* r12_6;
    struct s2* r13_7;
    int64_t* rbx8;
    struct s2* rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_25c0(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t fun_2450();

struct s2** fun_7e43(struct s6* rdi, struct s2** rsi, struct s2* rdx, struct s2** rcx, struct s2** r8, int64_t r9) {
    int64_t rax7;
    uint32_t ebx8;
    struct s2** rax9;
    struct s2** rax10;
    struct s2** rax11;

    __asm__("cli ");
    rax7 = fun_2450();
    ebx8 = rdi->f0 & 32;
    rax9 = rpl_fclose(rdi, rsi, rdx, rcx, r8, r9);
    if (ebx8) {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            addr_7e9e_3:
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        } else {
            rax10 = fun_2420();
            *rax10 = reinterpret_cast<struct s2*>(0);
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            if (rax7) 
                goto addr_7e9e_3;
            rax11 = fun_2420();
            *reinterpret_cast<int32_t*>(&rax9) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*rax11 == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    }
    return rax9;
}

uint32_t fun_2480(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_7eb3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    int64_t v8;
    struct s2* rax9;
    uint32_t eax10;
    uint32_t r12d11;
    int32_t eax12;
    int64_t rdx13;
    uint32_t eax14;
    int1_t zf15;
    void* rax16;
    int64_t rax17;
    int64_t rsi18;
    int64_t rdi19;
    uint32_t eax20;
    int64_t rdi21;
    uint32_t eax22;
    struct s2** rax23;
    int64_t rdi24;
    struct s2* r13d25;
    uint32_t eax26;
    struct s2** rax27;
    int64_t rdi28;
    uint32_t eax29;
    uint32_t ecx30;
    int64_t rax31;
    uint32_t eax32;
    uint32_t eax33;
    uint32_t eax34;
    int32_t ecx35;
    int64_t rax36;

    __asm__("cli ");
    v8 = rdx;
    rax9 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax10 = fun_2480(rdi);
        r12d11 = eax10;
        goto addr_7fb4_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax12 = have_dupfd_cloexec_0;
        *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
        if (eax12 < 0) {
            eax14 = fun_2480(rdi);
            r12d11 = eax14;
            if (reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0) || (zf15 = have_dupfd_cloexec_0 == -1, !zf15)) {
                addr_7fb4_3:
                rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax9) - reinterpret_cast<unsigned char>(g28));
                if (rax16) {
                    fun_2510();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax17) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    return rax17;
                }
            } else {
                addr_8069_9:
                *reinterpret_cast<int32_t*>(&rsi18) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi19) = r12d11;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
                eax20 = fun_2480(rdi19, rdi19);
                if (reinterpret_cast<int32_t>(eax20) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<int32_t*>(&rsi18) = 2, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi21) = r12d11, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx13) = eax20 | 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0, eax22 = fun_2480(rdi21, rdi21), eax22 == 0xffffffff)) {
                    rax23 = fun_2420();
                    *reinterpret_cast<uint32_t*>(&rdi24) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
                    r12d11 = 0xffffffff;
                    r13d25 = *rax23;
                    fun_25a0(rdi24, rsi18, rdx13);
                    *rax23 = r13d25;
                    goto addr_7fb4_3;
                }
            }
        } else {
            eax26 = fun_2480(rdi, rdi);
            r12d11 = eax26;
            if (reinterpret_cast<int32_t>(eax26) >= reinterpret_cast<int32_t>(0) || (rax27 = fun_2420(), *reinterpret_cast<int32_t*>(&rdi28) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0, !reinterpret_cast<int1_t>(*rax27 == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_7fb4_3;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
                eax29 = fun_2480(rdi28);
                r12d11 = eax29;
                if (reinterpret_cast<int32_t>(eax29) < reinterpret_cast<int32_t>(0)) 
                    goto addr_7fb4_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_8069_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_7f19_16;
    ecx30 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx30 > 10) 
        goto addr_7f1d_18;
    rax31 = 1 << *reinterpret_cast<unsigned char*>(&ecx30);
    if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x502)) {
            addr_7f1d_18:
            if (0) {
            }
        } else {
            addr_7f65_23:
            eax32 = fun_2480(rdi);
            r12d11 = eax32;
            goto addr_7fb4_3;
        }
        eax33 = fun_2480(rdi);
        r12d11 = eax33;
        goto addr_7fb4_3;
    }
    if (0) {
    }
    eax34 = fun_2480(rdi);
    r12d11 = eax34;
    goto addr_7fb4_3;
    addr_7f19_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_7f1d_18;
    ecx35 = *reinterpret_cast<int32_t*>(&rsi);
    rax36 = 1 << *reinterpret_cast<unsigned char*>(&ecx35);
    if (!(*reinterpret_cast<uint32_t*>(&rax36) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax36) & 0xa0a) 
            goto addr_7f65_23;
        goto addr_7f1d_18;
    }
}

signed char* fun_2660(int64_t rdi);

signed char* fun_8123() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2660(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2530(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_8163(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s2* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2530(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2510();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_81f3() {
    struct s2* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax3;
    }
}

int64_t fun_8273(int64_t rdi, struct s7* rsi, struct s2* rdx, int64_t rcx) {
    struct s2* rax5;
    int32_t r13d6;
    struct s2* rax7;
    int64_t rax8;

    __asm__("cli ");
    rax5 = fun_26a0(rdi);
    if (!rax5) {
        r13d6 = 22;
        if (rdx) {
            rsi->f0 = reinterpret_cast<struct s2*>(0);
        }
    } else {
        rax7 = fun_2500(rax5);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax7)) {
            fun_2620(rsi, rax5, &rax7->f1, rcx);
            return 0;
        } else {
            r13d6 = 34;
            if (rdx) {
                fun_2620(rsi, rax5, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff, rcx);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax8) = r13d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
}

void fun_8323() {
    __asm__("cli ");
    goto fun_26a0;
}

void fun_8333() {
    __asm__("cli ");
}

void fun_8347() {
    __asm__("cli ");
    return;
}

struct s2* rpl_mbrtowc(void* rdi, struct s2* rsi);

int32_t fun_2770(int64_t rdi, struct s2* rsi);

uint32_t fun_2760(struct s2* rdi, struct s2* rsi);

void fun_4775() {
    struct s2** rsp1;
    int32_t ebp2;
    struct s2* rax3;
    struct s2** rsp4;
    struct s2* r11_5;
    struct s2* r11_6;
    struct s2* v7;
    int32_t ebp8;
    struct s2* rax9;
    struct s2* rdx10;
    struct s2* rax11;
    struct s2* r11_12;
    struct s2* v13;
    int32_t ebp14;
    struct s2* rax15;
    struct s2* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s2* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s2* v22;
    int32_t ebx23;
    struct s2* rax24;
    struct s2** rsp25;
    struct s2* v26;
    struct s2* r11_27;
    struct s2* v28;
    struct s2* v29;
    struct s2* rsi30;
    struct s2* v31;
    struct s2* v32;
    struct s2* r10_33;
    struct s2* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s2* r9_37;
    struct s2* v38;
    struct s2* rdi39;
    struct s2* v40;
    struct s2* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s2* rcx44;
    unsigned char al45;
    struct s2* v46;
    int64_t v47;
    struct s2* v48;
    struct s2* v49;
    struct s2* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s2* v58;
    unsigned char v59;
    struct s2* v60;
    struct s2* v61;
    struct s2* v62;
    signed char* v63;
    struct s2* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s2* r13_69;
    struct s2* rsi70;
    void* v71;
    struct s2* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s2* v107;
    struct s2* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s2**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_24e0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_24e0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2500(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s2*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s2*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s2**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4a73_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4a73_22; else 
                            goto addr_4e6d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4f2d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5280_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4a70_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4a70_30; else 
                                goto addr_5299_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2500(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5280_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_25c0(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5280_28; else 
                            goto addr_491c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_53e0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5260_40:
                        if (r11_27 == 1) {
                            addr_4ded_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_53a8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4a27_44;
                            }
                        } else {
                            goto addr_5270_46;
                        }
                    } else {
                        addr_53ef_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_4ded_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4a73_22:
                                if (v47 != 1) {
                                    addr_4fc9_52:
                                    v48 = reinterpret_cast<struct s2*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2500(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_5014_54;
                                    }
                                } else {
                                    goto addr_4a80_56;
                                }
                            } else {
                                addr_4a25_57:
                                ebp36 = 0;
                                goto addr_4a27_44;
                            }
                        } else {
                            addr_5254_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_53ef_47; else 
                                goto addr_525e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4ded_41;
                        if (v47 == 1) 
                            goto addr_4a80_56; else 
                            goto addr_4fc9_52;
                    }
                }
                addr_4ae1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s2*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4978_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_499d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4ca0_65;
                    } else {
                        addr_4b09_66:
                        r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5358_67;
                    }
                } else {
                    goto addr_4b00_69;
                }
                addr_49b1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s2*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s2*>(&r15_16->pad40);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                addr_49fc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5358_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s2*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_49fc_81;
                }
                addr_4b00_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_499d_64; else 
                    goto addr_4b09_66;
                addr_4a27_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_4adf_91;
                if (v22) 
                    goto addr_4a3f_93;
                addr_4adf_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4ae1_62;
                addr_5014_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_579b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_580b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_560f_103;
                            rdx10 = reinterpret_cast<struct s2*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2770(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2760(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_510e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_4acc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_5118_112;
                    }
                } else {
                    addr_5118_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_51e9_114;
                }
                addr_4ad8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_4adf_91;
                while (1) {
                    addr_51e9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_56f7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_5156_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_5705_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_51d7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s2*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_51d7_128;
                        }
                    }
                    addr_5185_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s2*>(&r15_16->pad40);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_51d7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                    continue;
                    addr_5156_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s2*>(&r15_16->pad40);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_5185_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_49fc_81;
                addr_5705_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5358_67;
                addr_579b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_510e_109;
                addr_580b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_510e_109;
                addr_4a80_56:
                rax93 = fun_2780(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_4acc_110;
                addr_525e_59:
                goto addr_5260_40;
                addr_4f2d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4a73_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4ad8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4a25_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4a73_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4f72_160;
                if (!v22) 
                    goto addr_5347_162; else 
                    goto addr_5553_163;
                addr_4f72_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5347_162:
                    r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5358_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_4e1b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4c83_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_49b1_70; else 
                    goto addr_4c97_169;
                addr_4e1b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4978_63;
                goto addr_4b00_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5254_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_538f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4a70_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4968_178; else 
                        goto addr_5312_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5254_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4a73_22;
                }
                addr_538f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4a70_30:
                    r8d42 = 0;
                    goto addr_4a73_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4ae1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_53a8_42;
                    }
                }
                addr_4968_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4978_63;
                addr_5312_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5270_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4ae1_62;
                } else {
                    addr_5322_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4a73_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5ad2_188;
                if (v28) 
                    goto addr_5347_162;
                addr_5ad2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4c83_168;
                addr_491c_37:
                if (v22) 
                    goto addr_5913_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4933_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_53e0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_546b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4a73_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4968_178; else 
                        goto addr_5447_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5254_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4a73_22;
                }
                addr_546b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4a73_22;
                }
                addr_5447_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5270_46;
                goto addr_5322_186;
                addr_4933_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4a73_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4a73_22; else 
                    goto addr_4944_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_5a1e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_58a4_210;
            if (1) 
                goto addr_58a2_212;
            if (!v29) 
                goto addr_54de_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_24f0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s2*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s2*>("\"");
            if (!0) 
                goto addr_5a11_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s2*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s2*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4ca0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_4a5b_219; else 
            goto addr_4cba_220;
        addr_4a3f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4a53_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_4cba_220; else 
            goto addr_4a5b_219;
        addr_560f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_4cba_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_24f0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_562d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_24f0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s2*>("'");
        v29 = reinterpret_cast<struct s2*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s2*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s2*>(1);
        v22 = reinterpret_cast<struct s2*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s2*>(0);
            continue;
        }
        addr_5aa0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_5506_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s2*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s2*>(0);
        v22 = reinterpret_cast<struct s2*>(0);
        v28 = reinterpret_cast<struct s2*>(1);
        v26 = reinterpret_cast<struct s2*>("'");
        continue;
        addr_56f7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4a53_221;
        addr_5553_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4a53_221;
        addr_4c97_169:
        goto addr_4ca0_65;
        addr_5a1e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_4cba_220;
        goto addr_562d_222;
        addr_58a4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s2**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s2*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_58fe_236;
        fun_2510();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5aa0_225;
        addr_58a2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_58a4_210;
        addr_54de_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_58a4_210;
        } else {
            rdx10 = reinterpret_cast<struct s2*>(0);
            goto addr_5506_226;
        }
        addr_5a11_216:
        r13_34 = reinterpret_cast<struct s2*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s2*>("\"");
        v29 = reinterpret_cast<struct s2*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s2*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s2*>(1);
        v22 = reinterpret_cast<struct s2*>(0);
        v31 = reinterpret_cast<struct s2*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4e6d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x960c + rax113 * 4) + 0x960c;
    addr_5299_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x970c + rax114 * 4) + 0x970c;
    addr_5913_190:
    addr_4a5b_219:
    goto 0x4740;
    addr_4944_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x950c + rax115 * 4) + 0x950c;
    addr_58fe_236:
    goto v116;
}

void fun_4960() {
}

void fun_4b18() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4812;
}

void fun_4b71() {
    goto 0x4812;
}

void fun_4c5e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4ae1;
    }
    if (v2) 
        goto 0x5553;
    if (!r10_3) 
        goto addr_56be_5;
    if (!v4) 
        goto addr_558e_7;
    addr_56be_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_558e_7:
    goto 0x4994;
}

void fun_4c7c() {
}

void fun_4d27() {
    signed char v1;

    if (v1) {
        goto 0x4caf;
    } else {
        goto 0x49ea;
    }
}

void fun_4d41() {
    signed char v1;

    if (!v1) 
        goto 0x4d3a; else 
        goto "???";
}

void fun_4d68() {
    goto 0x4c83;
}

void fun_4de8() {
}

void fun_4e00() {
}

void fun_4e2f() {
    goto 0x4c83;
}

void fun_4e81() {
    goto 0x4e10;
}

void fun_4eb0() {
    goto 0x4e10;
}

void fun_4ee3() {
    goto 0x4e10;
}

void fun_52b0() {
    goto 0x4968;
}

void fun_55ae() {
    signed char v1;

    if (v1) 
        goto 0x5553;
    goto 0x4994;
}

void fun_5655() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4994;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4978;
        goto 0x4994;
    }
}

void fun_5a72() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4ce0;
    } else {
        goto 0x4812;
    }
}

void fun_69c8() {
    fun_24e0();
}

void fun_774c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x775b;
}

void fun_781c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x7829;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x775b;
}

void fun_7840() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_786c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x775b;
}

void fun_788d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x775b;
}

void fun_78b1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x775b;
}

void fun_78d5() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7864;
}

void fun_78f9() {
}

void fun_7919() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7864;
}

void fun_7935() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x7864;
}

void fun_4b9e() {
    goto 0x4812;
}

void fun_4d74() {
    goto 0x4d2c;
}

void fun_4e3b() {
    goto 0x4968;
}

void fun_4e8d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4e10;
    goto 0x4a3f;
}

void fun_4ebf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4e1b;
        goto 0x4840;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4cba;
        goto 0x4a5b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5658;
    if (r10_8 > r15_9) 
        goto addr_4da5_9;
    addr_4daa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5663;
    goto 0x4994;
    addr_4da5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4daa_10;
}

void fun_4ef2() {
    goto 0x4a27;
}

void fun_52c0() {
    goto 0x4a27;
}

void fun_5a5f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4b7c;
    } else {
        goto 0x4ce0;
    }
}

void fun_6a80() {
}

void fun_77ef() {
    if (__intrinsic()) 
        goto 0x7829; else 
        goto "???";
}

void fun_4efc() {
    goto 0x4e97;
}

void fun_52ca() {
    goto 0x4ded;
}

void fun_6ae0() {
    fun_24e0();
    goto fun_2750;
}

void fun_4bcd() {
    goto 0x4812;
}

void fun_4f08() {
    goto 0x4e97;
}

void fun_52d7() {
    goto 0x4e3e;
}

void fun_6b20() {
    fun_24e0();
    goto fun_2750;
}

void fun_4bfa() {
    goto 0x4812;
}

void fun_4f14() {
    goto 0x4e10;
}

void fun_6b60() {
    fun_24e0();
    goto fun_2750;
}

void fun_4c1c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x55b0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4ae1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4ae1;
    }
    if (v11) 
        goto 0x5913;
    if (r10_12 > r15_13) 
        goto addr_5963_8;
    addr_5968_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x56a1;
    addr_5963_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5968_9;
}

struct s18 {
    signed char[24] pad24;
    int64_t f18;
};

struct s19 {
    signed char[16] pad16;
    struct s2* f10;
};

struct s20 {
    signed char[8] pad8;
    struct s2* f8;
};

void fun_6bb0() {
    int64_t r15_1;
    struct s18* rbx2;
    struct s2* r14_3;
    struct s19* rbx4;
    struct s2* r13_5;
    struct s20* rbx6;
    struct s2* r12_7;
    struct s2** rbx8;
    struct s2* rax9;
    struct s2* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t* v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_24e0();
    fun_2750(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6bd2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6c08() {
    fun_24e0();
    goto 0x6bd9;
}

struct s21 {
    signed char[32] pad32;
    int64_t* f20;
};

struct s22 {
    signed char[24] pad24;
    int64_t f18;
};

struct s23 {
    signed char[16] pad16;
    struct s2* f10;
};

struct s24 {
    signed char[8] pad8;
    struct s2* f8;
};

struct s25 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6c40() {
    int64_t* rcx1;
    struct s21* rbx2;
    int64_t r15_3;
    struct s22* rbx4;
    struct s2* r14_5;
    struct s23* rbx6;
    struct s2* r13_7;
    struct s24* rbx8;
    struct s2* r12_9;
    struct s2** rbx10;
    int64_t v11;
    struct s25* rbx12;
    struct s2* rax13;
    struct s2* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_24e0();
    fun_2750(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6c74, __return_address(), rcx1);
    goto v15;
}

void fun_6cb8() {
    fun_24e0();
    goto 0x6c7b;
}
