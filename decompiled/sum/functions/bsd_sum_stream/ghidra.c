undefined8 bsd_sum_stream(FILE *param_1,uint *param_2,long *param_3)

{
  byte bVar1;
  byte *__ptr;
  size_t sVar2;
  int *piVar3;
  byte *pbVar4;
  byte *pbVar5;
  undefined8 uVar6;
  ulong uVar7;
  ulong uVar8;
  uint uVar9;
  bool bVar10;
  
  __ptr = (byte *)malloc(0x8000);
  if (__ptr == (byte *)0x0) {
    return 0xffffffff;
  }
  uVar9 = 0;
  uVar7 = 0;
  do {
    uVar8 = 0;
LAB_00102ccd:
    sVar2 = fread_unlocked(__ptr + uVar8,1,0x8000 - uVar8,param_1);
    uVar8 = uVar8 + sVar2;
    if (uVar8 != 0x8000) {
      if (sVar2 != 0) goto code_r0x00102cc4;
      uVar6 = 0xffffffff;
      if ((param_1->_flags & 0x20U) == 0) goto LAB_00102d5d;
      goto LAB_00102d3a;
    }
    pbVar4 = __ptr;
    do {
      bVar1 = *pbVar4;
      pbVar4 = pbVar4 + 1;
      uVar9 = (uVar9 & 1) * 0x8000 + ((int)uVar9 >> 1) + (uint)bVar1 & 0xffff;
    } while (pbVar4 != __ptr + 0x8000);
    bVar10 = uVar7 < 0xffffffffffff8000;
    uVar7 = uVar7 + 0x8000;
  } while (bVar10);
LAB_00102d2a:
  piVar3 = __errno_location();
  uVar6 = 0xffffffff;
  *piVar3 = 0x4b;
LAB_00102d3a:
  free(__ptr);
  return uVar6;
code_r0x00102cc4:
  if ((param_1->_flags & 0x10U) == 0) goto LAB_00102ccd;
LAB_00102d5d:
  pbVar4 = __ptr;
  if (uVar8 == 0) goto LAB_00102d9e;
  do {
    pbVar5 = pbVar4 + 1;
    uVar9 = (uVar9 & 1) * 0x8000 + ((int)uVar9 >> 1) + (uint)*pbVar4 & 0xffff;
    pbVar4 = pbVar5;
  } while (__ptr + uVar8 != pbVar5);
  if (CARRY8(uVar7,uVar8) == false) {
LAB_00102d9e:
    uVar6 = 0;
    *param_2 = uVar9;
    *param_3 = uVar7 + uVar8;
    goto LAB_00102d3a;
  }
  goto LAB_00102d2a;
}