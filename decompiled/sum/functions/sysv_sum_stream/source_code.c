sysv_sum_stream (FILE *stream, void *resstream, uintmax_t *length)
{
  int ret = -1;
  size_t sum, n;
  uintmax_t total_bytes = 0;
  static const size_t buffer_length = 32768;
  uint8_t *buffer = malloc (buffer_length);

  if (! buffer)
    return -1;

  /* The sum of all the input bytes, modulo (UINT_MAX + 1).  */
  unsigned int s = 0;

  /* Process file */
  while (true)
  {
    sum = 0;

    /* Read block */
    while (true)
    {
      n = fread (buffer + sum, 1, buffer_length - sum, stream);
      sum += n;

      if (buffer_length == sum)
        break;

      if (n == 0)
        {
          if (ferror (stream))
            goto cleanup_buffer;
          goto final_process;
        }

      if (feof (stream))
        goto final_process;
    }

    for (size_t i = 0; i < sum; i++)
      s += buffer[i];
    if (total_bytes + sum < total_bytes)
      {
        errno = EOVERFLOW;
        goto cleanup_buffer;
      }
    total_bytes += sum;
  }

final_process:;

  for (size_t i = 0; i < sum; i++)
    s += buffer[i];
  if (total_bytes + sum < total_bytes)
    {
      errno = EOVERFLOW;
      goto cleanup_buffer;
    }
  total_bytes += sum;

  int r = (s & 0xffff) + ((s & 0xffffffff) >> 16);
  int checksum = (r & 0xffff) + (r >> 16);

  memcpy (resstream, &checksum, sizeof checksum);
  *length = total_bytes;
  ret = 0;
cleanup_buffer:
  free (buffer);
  return ret;
}