void sysv_sum_stream(uint64 * rdx, int32 * rsi, ui32 * rdi)
{
	Eq_17 rax_32 = fn0000000000002640(&g_t8000);
	if (rax_32 == 0x00)
		return;
	uint64 r12_190 = 0x00;
	Eq_967 r15_117 = 0x00;
	rax_175 = rax_32;
l0000000000002DFF:
	Eq_17 rax_175;
	uint64 r13_53 = 0x00;
	do
	{
		fn00000000000024A0();
		r13_53 = (word64) rax_175 + r13_53;
		Eq_967 r15d_182 = r15_117;
		if (r13_53 == 0x8000)
		{
			rax_175 = rax_32;
			do
			{
				r15_117.u0 = (uint64) ((word64) r15d_182 + (word32) (*rax_175));
				rax_175 = (word64) rax_175 + 1;
				r15d_182 = (word32) r15_117;
			} while ((word64) rax_32 + 0x00008000 != rax_175);
			r12_190 += 0x8000;
			if (r12_190 < 0x00)
				goto l0000000000002E60;
			goto l0000000000002DFF;
		}
		ui32 edx_78 = *rdi;
		if (rax_175 == 0x00)
		{
			if ((edx_78 & 0x20) == 0x00)
				break;
			goto l0000000000002E70;
		}
	} while ((edx_78 & 0x10) == 0x00);
	uint64 r12_99 = r12_190 + r13_53;
	Eq_17 rcx_102 = (word64) rax_32 + r13_53;
	Eq_17 rax_103 = rax_32;
	uint64 rsi_123 = (uint64) (int8) (r12_99 < 0x00);
	Eq_967 r15d_114 = r15_117;
	if (r13_53 != 0x00)
	{
		do
		{
			r15_117.u0 = (uint64) ((word64) r15d_114 + (word32) (*rax_103));
			rax_103 = (word64) rax_103 + 1;
			r15d_114 = (word32) r15_117;
		} while (rcx_102 != rax_103);
		if (rsi_123 != 0x00)
		{
l0000000000002E60:
			*fn0000000000002420() = 0x4B;
			goto l0000000000002E70;
		}
	}
	uint64 rax_153 = (uint64) ((word32) (r15_117 >> 0x10) + (word32) ((word16) r15_117));
	*rsi = ((word32) rax_153 >> 0x10) + (word32) ((word16) rax_153);
	*rdx = r12_99;
l0000000000002E70:
	fn0000000000002400(rax_32);
}