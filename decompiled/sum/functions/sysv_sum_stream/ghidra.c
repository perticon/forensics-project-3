undefined8 sysv_sum_stream(FILE *param_1,int *param_2,long *param_3)

{
  byte bVar1;
  byte *__ptr;
  size_t sVar2;
  byte *pbVar3;
  int *piVar4;
  undefined8 uVar5;
  ulong uVar6;
  ulong uVar7;
  uint uVar8;
  bool bVar9;
  
  __ptr = (byte *)malloc(0x8000);
  if (__ptr == (byte *)0x0) {
    return 0xffffffff;
  }
  uVar8 = 0;
  uVar6 = 0;
  do {
    uVar7 = 0;
LAB_00102e15:
    sVar2 = fread_unlocked(__ptr + uVar7,1,0x8000 - uVar7,param_1);
    uVar7 = uVar7 + sVar2;
    if (uVar7 != 0x8000) {
      if (sVar2 != 0) goto code_r0x00102e10;
      uVar5 = 0xffffffff;
      if ((param_1->_flags & 0x20U) == 0) goto LAB_00102e93;
      goto LAB_00102e70;
    }
    pbVar3 = __ptr;
    do {
      bVar1 = *pbVar3;
      pbVar3 = pbVar3 + 1;
      uVar8 = uVar8 + bVar1;
    } while (__ptr + 0x8000 != pbVar3);
    bVar9 = uVar6 < 0xffffffffffff8000;
    uVar6 = uVar6 + 0x8000;
  } while (bVar9);
LAB_00102e60:
  piVar4 = __errno_location();
  uVar5 = 0xffffffff;
  *piVar4 = 0x4b;
LAB_00102e70:
  free(__ptr);
  return uVar5;
code_r0x00102e10:
  if ((param_1->_flags & 0x10U) == 0) goto LAB_00102e15;
LAB_00102e93:
  pbVar3 = __ptr;
  if (uVar7 == 0) goto LAB_00102ec4;
  do {
    bVar1 = *pbVar3;
    pbVar3 = pbVar3 + 1;
    uVar8 = uVar8 + bVar1;
  } while (__ptr + uVar7 != pbVar3);
  if (CARRY8(uVar6,uVar7) == false) {
LAB_00102ec4:
    uVar8 = (uVar8 & 0xffff) + (uVar8 >> 0x10);
    *param_2 = ((int)uVar8 >> 0x10) + (uVar8 & 0xffff);
    uVar5 = 0;
    *param_3 = uVar6 + uVar7;
    goto LAB_00102e70;
  }
  goto LAB_00102e60;
}