output_sysv (char const *file, int binary_file, void const *digest,
             bool tagged, unsigned char delim, bool args,
             uintmax_t length)
{

  char hbuf[LONGEST_HUMAN_READABLE + 1];
  printf ("%d %s", *(int *)digest,
          human_readable (length, hbuf, human_ceiling, 1, 512));
  if (args)
    printf (" %s", file);
  putchar (delim);
}