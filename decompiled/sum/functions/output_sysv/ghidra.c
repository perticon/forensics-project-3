void output_sysv(undefined8 param_1,undefined8 param_2,undefined4 *param_3,undefined8 param_4,
                byte param_5,char param_6,undefined8 param_7)

{
  byte *pbVar1;
  undefined8 uVar2;
  long in_FS_OFFSET;
  undefined auStack712 [664];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  uVar2 = human_readable(param_7,auStack712,0,1,0x200);
  __printf_chk(1,"%d %s",*param_3,uVar2);
  if (param_6 != '\0') {
    __printf_chk(1,&DAT_00109d31,param_1);
  }
  pbVar1 = (byte *)stdout->_IO_write_ptr;
  if (pbVar1 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = (char *)(pbVar1 + 1);
    *pbVar1 = param_5;
  }
  else {
    __overflow(stdout,(uint)param_5);
  }
  if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}