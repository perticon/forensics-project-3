
void** g28;

int32_t fun_2690(void* rdi, void* rsi);

void fun_2750(int64_t rdi, void** rsi, void** rdx, ...);

signed char include_fullname = 1;

signed char include_idle = 1;

/* now.2 */
int64_t now_2 = 0;

void fun_26c0(int64_t rdi, void** rsi, void** rdx);

void fun_2850(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, void*** r8, int64_t r9);

void** fun_2560();

void** fun_2670(void** rdi, void** rsi, ...);

int64_t fun_2480(void* rdi, void** rsi, void** rdx, int64_t rcx, void*** r8, int64_t r9);

void** imaxtostr(int64_t rdi, int64_t rsi, void** rdx, int64_t rcx, void*** r8, int64_t r9);

void** time_format = reinterpret_cast<void**>(0);

void fun_2760(int64_t rdi, int64_t rsi, void** rdx, int64_t rcx, void*** r8, int64_t r9);

signed char include_where = 1;

struct s0 {
    signed char[8] pad8;
    void** f8;
    signed char[31] pad40;
    void** f28;
    signed char[3] pad44;
    unsigned char f2c;
    signed char[3] pad48;
    void** f30;
    signed char[27] pad76;
    unsigned char f4c;
    signed char[255] pad332;
    unsigned char f14c;
    signed char[7] pad340;
    int32_t f154;
};

struct s0* stdout = reinterpret_cast<struct s0*>(0);

void fun_25e0();

signed char* fun_25c0(void** rdi, int64_t rsi, ...);

void fun_2590();

void** canon_host(void** rdi, int64_t rsi);

void fun_2470(void** rdi, void** rsi, void** rdx, ...);

void** create_fullname(void** rdi, void** rsi, ...);

void print_entry(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9) {
    void** rbp7;
    void* rsp8;
    uint32_t edx9;
    void** rax10;
    void** v11;
    void* rdi12;
    void*** r12_13;
    void*** rsi14;
    void*** rax15;
    int32_t eax16;
    uint32_t r13d17;
    int64_t r14_18;
    int64_t v19;
    int32_t r13d20;
    int32_t r13d21;
    uint32_t r13d22;
    int32_t r13d23;
    uint32_t v24;
    unsigned char* rbx25;
    void* rsp26;
    int1_t zf27;
    void*** r8_28;
    int64_t rcx29;
    void** rdx30;
    void** rsi31;
    void* rsp32;
    int1_t zf33;
    int64_t rcx34;
    void*** rdx35;
    uint64_t rdx36;
    void** rax37;
    void** r15_38;
    unsigned char* rcx39;
    uint32_t edx40;
    void** rax41;
    void* rsp42;
    int64_t v43;
    int64_t rax44;
    void* rsp45;
    void** rax46;
    void* rsp47;
    void** rdx48;
    void** rdx49;
    int1_t zf50;
    uint32_t edx51;
    struct s0* rdi52;
    void** rax53;
    void** r15_54;
    unsigned char* rax55;
    unsigned char* rbp56;
    signed char* rax57;
    void* rax58;
    signed char v59;
    void** rax60;
    void** rbp61;
    void** rdx62;
    void** rsi63;
    signed char v64;
    void** rax65;
    void** rax66;
    void** r15_67;
    signed char* rax68;
    void** rsi69;
    void** rax70;

    rbp7 = rdi;
    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x1e8);
    edx9 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 8));
    rax10 = g28;
    v11 = rax10;
    if (*reinterpret_cast<signed char*>(&edx9) != 47) {
        rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) + 0xa0);
    } else {
        rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) + 0xa0);
    }
    r12_13 = reinterpret_cast<void***>(rbp7 + 8);
    rsi14 = reinterpret_cast<void***>(rbp7 + 40);
    rax15 = r12_13;
    while (*reinterpret_cast<signed char*>(&edx9) && (++rax15, reinterpret_cast<uint64_t>(rsi14) > reinterpret_cast<uint64_t>(rax15))) {
        edx9 = reinterpret_cast<unsigned char>(*rax15);
    }
    eax16 = fun_2690(rdi12, reinterpret_cast<int64_t>(rsp8) + 16);
    if (eax16) {
        r13d17 = 63;
        *reinterpret_cast<int32_t*>(&r14_18) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_18) + 4) = 0;
    } else {
        r14_18 = v19;
        r13d17 = (r13d20 - (r13d21 + reinterpret_cast<uint1_t>(r13d22 < r13d23 + reinterpret_cast<uint1_t>((v24 & 16) < 1))) & 10) + 32;
    }
    rbx25 = reinterpret_cast<unsigned char*>(rbp7 + 44);
    fun_2750(1, "%-8.*s", 32, 1, "%-8.*s", 32);
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 + 8 - 8 + 8);
    zf27 = include_fullname == 0;
    if (zf27) {
        addr_351f_11:
        r8_28 = r12_13;
        *reinterpret_cast<int32_t*>(&rcx29) = 32;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx29) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx30) = r13d17;
        *reinterpret_cast<int32_t*>(&rdx30 + 4) = 0;
        rsi31 = reinterpret_cast<void**>(" %c%-8.*s");
        fun_2750(1, " %c%-8.*s", rdx30, 1, " %c%-8.*s", rdx30);
        rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
        zf33 = include_idle == 0;
        if (!zf33) {
            if (r14_18) {
                rcx34 = now_2;
                if (!rcx34) {
                    fun_26c0(0xc128, " %c%-8.*s", rdx30);
                    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                    rcx34 = now_2;
                }
                rcx29 = rcx34 - r14_18;
                rdx30 = reinterpret_cast<void**>("     ");
                if (rcx29 > 59) {
                    if (rcx29 > 0x1517f) {
                        rdx35 = reinterpret_cast<void***>((__intrinsic() >> 13) - reinterpret_cast<uint64_t>(rcx29 >> 63));
                        rcx29 = reinterpret_cast<int64_t>("%lud");
                        r8_28 = rdx35;
                        fun_2850(0xc110, 1, 22, "%lud", r8_28, r9);
                        rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                        rdx30 = reinterpret_cast<void**>(0xc110);
                    } else {
                        r8_28 = reinterpret_cast<void***>((__intrinsic() >> 10) - reinterpret_cast<uint64_t>(rcx29 >> 63));
                        rcx29 = reinterpret_cast<int64_t>("%02d:%02d");
                        rdx36 = __intrinsic() >> 5;
                        *reinterpret_cast<int32_t*>(&r9) = *reinterpret_cast<int32_t*>(&rdx36);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9) + 4) = 0;
                        fun_2850(0xc110, 1, 22, "%02d:%02d", r8_28, r9);
                        rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                        rdx30 = reinterpret_cast<void**>(0xc110);
                    }
                }
            } else {
                rax37 = fun_2560();
                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                rdx30 = rax37;
            }
            rsi31 = reinterpret_cast<void**>(" %-6s");
            fun_2750(1, " %-6s", rdx30, 1, " %-6s", rdx30);
            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
        }
    } else {
        r15_38 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp26) + 0xd0);
        rcx39 = reinterpret_cast<unsigned char*>(rbp7 + 76);
        do {
            edx40 = *rbx25;
            if (!*reinterpret_cast<signed char*>(&edx40)) 
                break;
            ++rbx25;
        } while (reinterpret_cast<uint64_t>(rcx39) > reinterpret_cast<uint64_t>(rbx25));
        rax41 = fun_2670(r15_38, "%-8.*s");
        rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
        if (!rax41) 
            goto addr_37c9_25; else 
            goto addr_34d6_26;
    }
    v43 = *reinterpret_cast<int32_t*>(rbp7 + 0x154);
    rax44 = fun_2480(reinterpret_cast<int64_t>(rsp32) + 8, rsi31, rdx30, rcx29, r8_28, r9);
    rsp45 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
    if (!rax44) {
        rax46 = imaxtostr(v43, 0xc0e0, rdx30, rax44, r8_28, r9);
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
        rdx48 = rax46;
    } else {
        rdx49 = time_format;
        fun_2760(0xc0e0, 33, rdx49, rax44, r8_28, r9);
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp45) - 8 + 8);
        rdx48 = reinterpret_cast<void**>(0xc0e0);
    }
    fun_2750(1, " %s", rdx48, 1, " %s", rdx48);
    zf50 = include_where == 0;
    if (zf50 || (edx51 = *reinterpret_cast<unsigned char*>(rbp7 + 76), !*reinterpret_cast<signed char*>(&edx51))) {
        addr_35df_31:
        rdi52 = stdout;
        rax53 = rdi52->f28;
        if (reinterpret_cast<unsigned char>(rax53) >= reinterpret_cast<unsigned char>(rdi52->f30)) {
            fun_25e0();
        } else {
            rdi52->f28 = rax53 + 1;
            *reinterpret_cast<void***>(rax53) = reinterpret_cast<void**>(10);
        }
    } else {
        r15_54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 + 0xd0);
        rax55 = reinterpret_cast<unsigned char*>(rbp7 + 76);
        rbp56 = reinterpret_cast<unsigned char*>(rbp7 + 0x14c);
        do {
            ++rax55;
            if (reinterpret_cast<uint64_t>(rbp56) <= reinterpret_cast<uint64_t>(rax55)) 
                break;
            edx51 = *rax55;
        } while (*reinterpret_cast<signed char*>(&edx51));
        rax57 = fun_25c0(r15_54, 58);
        if (!rax57) 
            goto addr_380f_38; else 
            goto addr_3690_39;
    }
    rax58 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax58) {
        fun_2590();
    } else {
        return;
    }
    addr_380f_38:
    if (v59) {
        rax60 = canon_host(r15_54, 58);
        rbp61 = rax60;
        if (rax60) {
            addr_3820_45:
            rdx62 = rbp61;
            rsi63 = reinterpret_cast<void**>(" %s");
            fun_2750(1, " %s", rdx62, 1, " %s", rdx62);
        } else {
            goto addr_381d_47;
        }
    } else {
        addr_381d_47:
        rbp61 = r15_54;
        goto addr_3820_45;
    }
    addr_36c1_48:
    if (rbp61 != r15_54) {
        fun_2470(rbp61, rsi63, rdx62, rbp61, rsi63, rdx62);
        goto addr_35df_31;
    }
    addr_3690_39:
    *rax57 = 0;
    if (v64) {
        rax65 = canon_host(r15_54, 58);
        rbp61 = rax65;
        if (rax65) {
            addr_36a8_51:
            rdx62 = rbp61;
            rsi63 = reinterpret_cast<void**>(" %s:%s");
            fun_2750(1, " %s:%s", rdx62, 1, " %s:%s", rdx62);
            goto addr_36c1_48;
        } else {
            goto addr_36a5_53;
        }
    } else {
        addr_36a5_53:
        rbp61 = r15_54;
        goto addr_36a8_51;
    }
    addr_37c9_25:
    rax66 = fun_2560();
    fun_2750(1, " %19s", rax66, 1, " %19s", rax66);
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8 - 8 + 8);
    goto addr_351f_11;
    addr_34d6_26:
    r15_67 = *reinterpret_cast<void***>(rax41 + 24);
    rax68 = fun_25c0(r15_67, 44);
    if (rax68) {
        *rax68 = 0;
        r15_67 = *reinterpret_cast<void***>(rax41 + 24);
    }
    rsi69 = *reinterpret_cast<void***>(rax41);
    rax70 = create_fullname(r15_67, rsi69);
    fun_2750(1, " %-19.19s", rax70, 1, " %-19.19s", rax70);
    fun_2470(rax70, " %-19.19s", rax70);
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_351f_11;
}

signed char include_home_and_shell = 1;

signed char include_project = 1;

void** fun_2580(void** rdi, ...);

void** xmalloc(void** rdi, void** rsi, void** rdx, ...);

struct s1 {
    int32_t f0;
    int16_t f4;
    signed char f6;
    signed char[1] pad8;
    int16_t f8;
};

struct s1* fun_2550(void** rdi, void** rsi, void** rdx, ...);

void** fun_27a0(void** rdi, void** rsi, ...);

void** fun_2500(void* rdi, void** rsi, int64_t rdx, void** rcx);

void fun_2720(void* rdi, int64_t rsi, void** rdx, struct s0* rcx);

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx, void** rcx);

signed char include_plan = 1;

void print_long_entry(void** rdi, void** rsi) {
    void** rax3;
    void** v4;
    void** rax5;
    void** rbp6;
    void** rax7;
    void** rax8;
    void* rsp9;
    struct s0* rdi10;
    void* rsp11;
    void* rax12;
    void** r12_13;
    signed char* rax14;
    void** rsi15;
    void** r13_16;
    void** rax17;
    void** rsi18;
    void** rdx19;
    struct s0* rdi20;
    void** rax21;
    int1_t zf22;
    struct s0* rbp23;
    void* rsp24;
    uint32_t edx25;
    void** rax26;
    void** v27;
    void* rdi28;
    void*** r12_29;
    void*** rsi30;
    void*** rax31;
    int32_t eax32;
    uint32_t r13d33;
    int64_t r14_34;
    int64_t v35;
    uint32_t v36;
    unsigned char* rbx37;
    void* rsp38;
    int1_t zf39;
    void*** r8_40;
    int64_t rcx41;
    void** rdx42;
    void** rsi43;
    void* rsp44;
    int1_t zf45;
    int64_t rcx46;
    void*** rdx47;
    int64_t r9_48;
    uint64_t rdx49;
    int64_t r9_50;
    void** rax51;
    void** r15_52;
    unsigned char* rcx53;
    uint32_t edx54;
    void** rax55;
    void* rsp56;
    int64_t v57;
    int64_t rax58;
    void* rsp59;
    void** rax60;
    void* rsp61;
    void** rdx62;
    void** rdx63;
    int1_t zf64;
    uint32_t edx65;
    struct s0* rdi66;
    void** rax67;
    void** r15_68;
    unsigned char* rax69;
    unsigned char* rbp70;
    signed char* rax71;
    void* rax72;
    int64_t v73;
    signed char v74;
    void** rax75;
    void** rbp76;
    void** rdx77;
    void** rsi78;
    signed char v79;
    void** rax80;
    void** rax81;
    void** r15_82;
    signed char* rax83;
    void** rsi84;
    void** rax85;
    void** rax86;
    void** rdx87;
    void** rax88;
    struct s0* rdi89;
    void** rax90;
    int1_t zf91;
    int1_t zf92;
    void** rdi93;
    void** rax94;
    void** rax95;
    void** rsi96;
    struct s1* rax97;
    void** rax98;
    void* rsp99;
    void** r12_100;
    void* rbx101;
    void** rax102;
    void* rsp103;
    void** rax104;
    void* rsp105;
    struct s0* rcx106;
    int1_t zf107;
    int1_t zf108;
    void** rax109;
    void* rax110;
    void* rax111;
    void** rdi112;
    void** rax113;
    void** rdi114;
    void** rax115;
    void** rsi116;
    struct s1* rax117;
    void** rdx118;
    void** rsi119;
    void** rax120;
    void* rsp121;
    void** rax122;
    void* rsp123;
    void** rax124;
    void* rsp125;
    struct s0* rcx126;

    rax3 = g28;
    v4 = rax3;
    rax5 = fun_2670(rdi, rsi);
    rbp6 = rax5;
    rax7 = fun_2560();
    fun_2750(1, rax7, 5);
    fun_2750(1, "%-28s", rdi);
    rax8 = fun_2560();
    fun_2750(1, rax8, 5);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x418 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (!rbp6) {
        *reinterpret_cast<int32_t*>(&rdi10) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
        fun_2560();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v4) - reinterpret_cast<unsigned char>(g28));
        if (!rax12) {
        }
    } else {
        r12_13 = *reinterpret_cast<void***>(rbp6 + 24);
        rax14 = fun_25c0(r12_13, 44, r12_13, 44);
        if (rax14) {
            *rax14 = 0;
            r12_13 = *reinterpret_cast<void***>(rbp6 + 24);
        }
        rsi15 = *reinterpret_cast<void***>(rbp6);
        r13_16 = reinterpret_cast<void**>(" %s");
        rax17 = create_fullname(r12_13, rsi15, r12_13, rsi15);
        rsi18 = reinterpret_cast<void**>(" %s");
        rdx19 = rax17;
        fun_2750(1, " %s", rdx19);
        fun_2470(rax17, " %s", rdx19);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rdi20 = stdout;
        rax21 = rdi20->f28;
        if (reinterpret_cast<unsigned char>(rax21) >= reinterpret_cast<unsigned char>(rdi20->f30)) {
            *reinterpret_cast<int32_t*>(&rsi18) = 10;
            *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
            fun_25e0();
            rsp11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp11) - 8 + 8);
        } else {
            rdx19 = rax21 + 1;
            rdi20->f28 = rdx19;
            *reinterpret_cast<void***>(rax21) = reinterpret_cast<void**>(10);
        }
        zf22 = include_home_and_shell == 0;
        if (!zf22) 
            goto addr_30f8_11; else 
            goto addr_3094_12;
    }
    fun_2590();
    rbp23 = rdi10;
    rsp24 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp11) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1e8);
    edx25 = reinterpret_cast<unsigned char>(rdi10->f8);
    rax26 = g28;
    v27 = rax26;
    if (*reinterpret_cast<signed char*>(&edx25) != 47) {
        rdi28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp24) + 0xa0);
    } else {
        rdi28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp24) + 0xa0);
    }
    r12_29 = &rbp23->f8;
    rsi30 = &rbp23->f28;
    rax31 = r12_29;
    while (*reinterpret_cast<signed char*>(&edx25) && (++rax31, reinterpret_cast<uint64_t>(rsi30) > reinterpret_cast<uint64_t>(rax31))) {
        edx25 = reinterpret_cast<unsigned char>(*rax31);
    }
    eax32 = fun_2690(rdi28, reinterpret_cast<uint64_t>(rsp24) + 16);
    if (eax32) {
        r13d33 = 63;
        *reinterpret_cast<int32_t*>(&r14_34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_34) + 4) = 0;
    } else {
        r14_34 = v35;
        r13d33 = (*reinterpret_cast<uint32_t*>(&r13_16) - (*reinterpret_cast<uint32_t*>(&r13_16) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_16) < *reinterpret_cast<uint32_t*>(&r13_16) + reinterpret_cast<uint1_t>((v36 & 16) < 1))) & 10) + 32;
    }
    rbx37 = &rbp23->f2c;
    fun_2750(1, "%-8.*s", 32, 1, "%-8.*s", 32);
    rsp38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp24) - 8 + 8 - 8 + 8);
    zf39 = include_fullname == 0;
    if (zf39) {
        addr_351f_24:
        r8_40 = r12_29;
        *reinterpret_cast<int32_t*>(&rcx41) = 32;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx41) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx42) = r13d33;
        *reinterpret_cast<int32_t*>(&rdx42 + 4) = 0;
        rsi43 = reinterpret_cast<void**>(" %c%-8.*s");
        fun_2750(1, " %c%-8.*s", rdx42, 1, " %c%-8.*s", rdx42);
        rsp44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp38) - 8 + 8);
        zf45 = include_idle == 0;
        if (!zf45) {
            if (r14_34) {
                rcx46 = now_2;
                if (!rcx46) {
                    fun_26c0(0xc128, " %c%-8.*s", rdx42);
                    rsp44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp44) - 8 + 8);
                    rcx46 = now_2;
                }
                rcx41 = rcx46 - r14_34;
                rdx42 = reinterpret_cast<void**>("     ");
                if (rcx41 > 59) {
                    if (rcx41 > 0x1517f) {
                        rdx47 = reinterpret_cast<void***>((__intrinsic() >> 13) - reinterpret_cast<uint64_t>(rcx41 >> 63));
                        rcx41 = reinterpret_cast<int64_t>("%lud");
                        r8_40 = rdx47;
                        fun_2850(0xc110, 1, 22, "%lud", r8_40, r9_48);
                        rsp44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp44) - 8 + 8);
                        rdx42 = reinterpret_cast<void**>(0xc110);
                    } else {
                        r8_40 = reinterpret_cast<void***>((__intrinsic() >> 10) - reinterpret_cast<uint64_t>(rcx41 >> 63));
                        rcx41 = reinterpret_cast<int64_t>("%02d:%02d");
                        rdx49 = __intrinsic() >> 5;
                        *reinterpret_cast<int32_t*>(&r9_50) = *reinterpret_cast<int32_t*>(&rdx49);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_50) + 4) = 0;
                        fun_2850(0xc110, 1, 22, "%02d:%02d", r8_40, r9_50);
                        rsp44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp44) - 8 + 8);
                        rdx42 = reinterpret_cast<void**>(0xc110);
                    }
                }
            } else {
                rax51 = fun_2560();
                rsp44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp44) - 8 + 8);
                rdx42 = rax51;
            }
            rsi43 = reinterpret_cast<void**>(" %-6s");
            fun_2750(1, " %-6s", rdx42, 1, " %-6s", rdx42);
            rsp44 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp44) - 8 + 8);
        }
    } else {
        r15_52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp38) + 0xd0);
        rcx53 = &rbp23->f4c;
        do {
            edx54 = *rbx37;
            if (!*reinterpret_cast<signed char*>(&edx54)) 
                break;
            ++rbx37;
        } while (reinterpret_cast<uint64_t>(rcx53) > reinterpret_cast<uint64_t>(rbx37));
        rax55 = fun_2670(r15_52, "%-8.*s");
        rsp56 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp38) - 8 + 8);
        if (!rax55) 
            goto addr_37c9_38; else 
            goto addr_34d6_39;
    }
    v57 = rbp23->f154;
    rax58 = fun_2480(reinterpret_cast<uint64_t>(rsp44) + 8, rsi43, rdx42, rcx41, r8_40, r9_50);
    rsp59 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp44) - 8 + 8);
    if (!rax58) {
        rax60 = imaxtostr(v57, 0xc0e0, rdx42, rax58, r8_40, r9_50);
        rsp61 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp59) - 8 + 8);
        rdx62 = rax60;
    } else {
        rdx63 = time_format;
        fun_2760(0xc0e0, 33, rdx63, rax58, r8_40, r9_50);
        rsp61 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp59) - 8 + 8);
        rdx62 = reinterpret_cast<void**>(0xc0e0);
    }
    fun_2750(1, " %s", rdx62, 1, " %s", rdx62);
    zf64 = include_where == 0;
    if (zf64 || (edx65 = rbp23->f4c, !*reinterpret_cast<signed char*>(&edx65))) {
        addr_35df_44:
        rdi66 = stdout;
        rax67 = rdi66->f28;
        if (reinterpret_cast<unsigned char>(rax67) >= reinterpret_cast<unsigned char>(rdi66->f30)) {
            fun_25e0();
        } else {
            rdi66->f28 = rax67 + 1;
            *reinterpret_cast<void***>(rax67) = reinterpret_cast<void**>(10);
        }
    } else {
        r15_68 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp61) - 8 + 8 + 0xd0);
        rax69 = &rbp23->f4c;
        rbp70 = &rbp23->f14c;
        do {
            ++rax69;
            if (reinterpret_cast<uint64_t>(rbp70) <= reinterpret_cast<uint64_t>(rax69)) 
                break;
            edx65 = *rax69;
        } while (*reinterpret_cast<signed char*>(&edx65));
        rax71 = fun_25c0(r15_68, 58);
        if (!rax71) 
            goto addr_380f_51; else 
            goto addr_3690_52;
    }
    rax72 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(g28));
    if (rax72) {
        fun_2590();
    } else {
        goto v73;
    }
    addr_380f_51:
    if (v74) {
        rax75 = canon_host(r15_68, 58);
        rbp76 = rax75;
        if (rax75) {
            addr_3820_58:
            rdx77 = rbp76;
            rsi78 = reinterpret_cast<void**>(" %s");
            fun_2750(1, " %s", rdx77, 1, " %s", rdx77);
        } else {
            goto addr_381d_60;
        }
    } else {
        addr_381d_60:
        rbp76 = r15_68;
        goto addr_3820_58;
    }
    addr_36c1_61:
    if (rbp76 != r15_68) {
        fun_2470(rbp76, rsi78, rdx77, rbp76, rsi78, rdx77);
        goto addr_35df_44;
    }
    addr_3690_52:
    *rax71 = 0;
    if (v79) {
        rax80 = canon_host(r15_68, 58);
        rbp76 = rax80;
        if (rax80) {
            addr_36a8_64:
            rdx77 = rbp76;
            rsi78 = reinterpret_cast<void**>(" %s:%s");
            fun_2750(1, " %s:%s", rdx77, 1, " %s:%s", rdx77);
            goto addr_36c1_61;
        } else {
            goto addr_36a5_66;
        }
    } else {
        addr_36a5_66:
        rbp76 = r15_68;
        goto addr_36a8_64;
    }
    addr_37c9_38:
    rax81 = fun_2560();
    fun_2750(1, " %19s", rax81, 1, " %19s", rax81);
    rsp38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp56) - 8 + 8 - 8 + 8);
    goto addr_351f_24;
    addr_34d6_39:
    r15_82 = *reinterpret_cast<void***>(rax55 + 24);
    rax83 = fun_25c0(r15_82, 44);
    if (rax83) {
        *rax83 = 0;
        r15_82 = *reinterpret_cast<void***>(rax55 + 24);
    }
    rsi84 = *reinterpret_cast<void***>(rax55);
    rax85 = create_fullname(r15_82, rsi84);
    fun_2750(1, " %-19.19s", rax85, 1, " %-19.19s", rax85);
    fun_2470(rax85, " %-19.19s", rax85);
    rsp38 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp56) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_351f_24;
    addr_30f8_11:
    rax86 = fun_2560();
    fun_2750(1, rax86, 5);
    rdx87 = *reinterpret_cast<void***>(rbp6 + 32);
    fun_2750(1, "%-29s", rdx87);
    rax88 = fun_2560();
    fun_2750(1, rax88, 5);
    rdx19 = *reinterpret_cast<void***>(rbp6 + 40);
    rsi18 = reinterpret_cast<void**>(" %s");
    fun_2750(1, " %s", rdx19);
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp11) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    rdi89 = stdout;
    rax90 = rdi89->f28;
    if (reinterpret_cast<unsigned char>(rax90) >= reinterpret_cast<unsigned char>(rdi89->f30)) {
        *reinterpret_cast<int32_t*>(&rsi18) = 10;
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
        fun_25e0();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp11) - 8 + 8);
    } else {
        rdx19 = rax90 + 1;
        zf91 = include_project == 0;
        rdi89->f28 = rdx19;
        *reinterpret_cast<void***>(rax90) = reinterpret_cast<void**>(10);
        if (zf91) 
            goto addr_30a1_71; else 
            goto addr_3193_72;
    }
    addr_3094_12:
    zf92 = include_project == 0;
    if (!zf92) {
        addr_3193_72:
        rdi93 = *reinterpret_cast<void***>(rbp6 + 32);
        rax94 = fun_2580(rdi93, rdi93);
        rax95 = xmalloc(rax94 + 10, rsi18, rdx19);
        rsi96 = *reinterpret_cast<void***>(rbp6 + 32);
        r13_16 = rax95;
        rax97 = fun_2550(rax95, rsi96, rdx19);
        rsi18 = reinterpret_cast<void**>("r");
        rax97->f0 = reinterpret_cast<int32_t>(0x63656a6f72702e2f);
        rax97->f8 = 0x74;
        rax98 = fun_27a0(r13_16, "r", r13_16, "r");
        rsp99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp11) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        r12_100 = rax98;
        if (rax98) {
            rbx101 = rsp99;
            rax102 = fun_2560();
            fun_2750(1, rax102, 5);
            rsp103 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp99) - 8 + 8 - 8 + 8);
            while (*reinterpret_cast<int32_t*>(&rsi18) = 1, *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0, rax104 = fun_2500(rbx101, 1, 0x400, r12_100), rsp105 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp103) - 8 + 8), rdx19 = rax104, !!rax104) {
                rcx106 = stdout;
                fun_2720(rbx101, 1, rdx19, rcx106);
                rsp103 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp105) - 8 + 8);
            }
            rpl_fclose(r12_100, 1, rdx19, r12_100);
            rsp99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp105) - 8 + 8);
        }
    } else {
        addr_30a1_71:
        zf107 = include_plan == 0;
        if (!zf107) 
            goto addr_325e_77; else 
            goto addr_30ae_78;
    }
    fun_2470(r13_16, rsi18, rdx19, r13_16, rsi18, rdx19);
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp99) - 8 + 8);
    zf108 = include_plan == 0;
    if (zf108) {
        addr_30ae_78:
        rdi10 = stdout;
        rax109 = rdi10->f28;
        if (reinterpret_cast<unsigned char>(rax109) >= reinterpret_cast<unsigned char>(rdi10->f30)) {
            rax110 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v4) - reinterpret_cast<unsigned char>(g28));
            if (!rax110) {
            }
        } else {
            rdi10->f28 = rax109 + 1;
            *reinterpret_cast<void***>(rax109) = reinterpret_cast<void**>(10);
            rax111 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v4) - reinterpret_cast<unsigned char>(g28));
            if (!rax111) {
                return;
            }
        }
    } else {
        addr_325e_77:
        rdi112 = *reinterpret_cast<void***>(rbp6 + 32);
        rax113 = fun_2580(rdi112, rdi112);
        rdi114 = rax113 + 7;
        rax115 = xmalloc(rdi114, rsi18, rdx19, rdi114, rsi18, rdx19);
        rsi116 = *reinterpret_cast<void***>(rbp6 + 32);
        r12_100 = rax115;
        rax117 = fun_2550(rax115, rsi116, rdx19, rax115, rsi116, rdx19);
        *reinterpret_cast<int32_t*>(&rdx118) = 0x6e61;
        *reinterpret_cast<int32_t*>(&rdx118 + 4) = 0;
        rsi119 = reinterpret_cast<void**>("r");
        rax117->f0 = 0x6c702e2f;
        rax117->f4 = 0x6e61;
        rax117->f6 = 0;
        rax120 = fun_27a0(r12_100, "r", r12_100, "r");
        rsp121 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp11) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rbp6 = rax120;
        if (rax120) {
            rbx101 = rsp121;
            rax122 = fun_2560();
            fun_2750(1, rax122, 5, 1, rax122, 5);
            rsp123 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp121) - 8 + 8 - 8 + 8);
            while (*reinterpret_cast<int32_t*>(&rsi119) = 1, *reinterpret_cast<int32_t*>(&rsi119 + 4) = 0, rax124 = fun_2500(rbx101, 1, 0x400, rbp6), rsp125 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp123) - 8 + 8), rdx118 = rax124, !!rax124) {
                rcx126 = stdout;
                fun_2720(rbx101, 1, rdx118, rcx126);
                rsp123 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp125) - 8 + 8);
            }
            rpl_fclose(rbp6, 1, rdx118, rbp6);
            rsp121 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp125) - 8 + 8);
        }
    }
    fun_2470(r12_100, rsi119, rdx118, r12_100, rsi119, rdx118);
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp121) - 8 + 8);
    goto addr_30ae_78;
}

int64_t fun_2570();

int64_t fun_2490(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2570();
    if (r8d > 10) {
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x87c0 + rax11 * 4) + 0x87c0;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

int32_t* fun_24a0();

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_2610();

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, void** rsi, int64_t rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s3* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x571f;
    rax8 = fun_24a0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
        fun_2490(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x67d1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x57ab;
            fun_2610();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xc1a0) {
                fun_2470(r14_19, rsi24, rsi, r14_19, rsi24, rsi);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x583a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2590();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

void** fun_2830(void** rdi, void** rsi, void** rdx, ...);

int32_t** fun_2450(void** rdi);

void xalloc_die();

void** create_fullname(void** rdi, void** rsi, ...) {
    void** r13_3;
    void** rbp4;
    int64_t v5;
    int64_t rbx6;
    void* rbx7;
    void** rax8;
    void* rsp9;
    void** rdx10;
    void** r12_11;
    uint32_t ecx12;
    void** rax13;
    void** tmp64_14;
    int1_t cf15;
    void** rdi16;
    void** rax17;
    void** r12_18;
    uint32_t eax19;
    void** rbx20;
    void** rax21;
    int64_t r14_22;
    int64_t rax23;
    int32_t** rax24;
    int32_t eax25;
    void** rax26;
    void** v27;
    void** rax28;
    void** rbp29;
    void** rax30;
    void** rax31;
    void* rsp32;
    struct s0* rdi33;
    void* rsp34;
    void* rax35;
    struct s0* rbp36;
    void* rsp37;
    uint32_t edx38;
    void** rax39;
    void** v40;
    void* rdi41;
    void*** r12_42;
    void*** rsi43;
    void*** rax44;
    int32_t eax45;
    uint32_t r13d46;
    int64_t r14_47;
    int64_t v48;
    uint32_t v49;
    unsigned char* rbx50;
    void* rsp51;
    int1_t zf52;
    void*** r8_53;
    int64_t rcx54;
    void** rdx55;
    void** rsi56;
    void* rsp57;
    int1_t zf58;
    int64_t rcx59;
    void*** rdx60;
    int64_t r9_61;
    uint64_t rdx62;
    int64_t r9_63;
    void** rax64;
    void** r15_65;
    unsigned char* rcx66;
    uint32_t edx67;
    void** rax68;
    void* rsp69;
    int64_t v70;
    int64_t rax71;
    void* rsp72;
    void** rax73;
    void* rsp74;
    void** rdx75;
    void** rdx76;
    int1_t zf77;
    uint32_t edx78;
    struct s0* rdi79;
    void** rax80;
    void** r15_81;
    unsigned char* rax82;
    unsigned char* rbp83;
    signed char* rax84;
    void* rax85;
    int64_t v86;
    signed char v87;
    void** rax88;
    void** rbp89;
    void** rdx90;
    void** rsi91;
    signed char v92;
    void** rax93;
    void** rax94;
    void** r15_95;
    signed char* rax96;
    void** rsi97;
    void** rax98;
    void** r12_99;
    signed char* rax100;
    void** rsi101;
    void** rax102;
    void** rsi103;
    void** rdx104;
    struct s0* rdi105;
    void** rax106;
    int1_t zf107;
    void** rax108;
    void** rdx109;
    void** rax110;
    struct s0* rdi111;
    void** rax112;
    int1_t zf113;
    int1_t zf114;
    void** rdi115;
    void** rax116;
    void** rax117;
    void** rsi118;
    struct s1* rax119;
    void** rax120;
    void* rsp121;
    void** r12_122;
    void* rbx123;
    void** rax124;
    void* rsp125;
    void** rax126;
    void* rsp127;
    struct s0* rcx128;
    int1_t zf129;
    int1_t zf130;
    void** rax131;
    void* rax132;
    void* rax133;
    void** rdi134;
    void** rax135;
    void** rdi136;
    void** rax137;
    void** rsi138;
    struct s1* rax139;
    void** rdx140;
    void** rsi141;
    void** rax142;
    void* rsp143;
    void** rax144;
    void* rsp145;
    void** rax146;
    void* rsp147;
    struct s0* rcx148;

    r13_3 = rdi;
    rbp4 = rsi;
    v5 = rbx6;
    *reinterpret_cast<int32_t*>(&rbx7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
    rax8 = fun_2580(rdi);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 + 8);
    rdx10 = r13_3;
    r12_11 = rax8 + 1;
    while (1) {
        ecx12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10));
        ++rdx10;
        if (*reinterpret_cast<signed char*>(&ecx12) == 38) {
            rbx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbx7) + 1);
        } else {
            if (!*reinterpret_cast<signed char*>(&ecx12)) 
                break;
        }
    }
    if (!rbx7 || (rax13 = fun_2580(rbp4), rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8), rdx10 = __intrinsic(), !__intrinsic()) && (tmp64_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_11) + reinterpret_cast<unsigned char>(rax13) * (reinterpret_cast<int64_t>(rbx7) - 1)), cf15 = reinterpret_cast<unsigned char>(tmp64_14) < reinterpret_cast<unsigned char>(r12_11), r12_11 = tmp64_14, !cf15)) {
        rdi16 = r12_11;
        rax17 = xmalloc(rdi16, rsi, rdx10);
        r12_18 = rax17;
        eax19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_3));
        rbx20 = r12_18;
        if (*reinterpret_cast<void***>(&eax19)) {
            do {
                addr_2f03_7:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax19) == 38)) {
                    *reinterpret_cast<void***>(rbx20) = *reinterpret_cast<void***>(&eax19);
                    ++rbx20;
                } else {
                    rax21 = fun_2830(rdi16, rsi, rdx10);
                    *reinterpret_cast<uint32_t*>(&r14_22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_22) + 4) = 0;
                    rdx10 = rbp4;
                    rax23 = r14_22;
                    if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax21) + r14_22 * 2 + 1) & 2) {
                        rax24 = fun_2450(rdi16);
                        ++rbx20;
                        rdx10 = rbp4 + 1;
                        eax25 = (*rax24)[r14_22];
                        *reinterpret_cast<signed char*>(rbx20 + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&eax25);
                        *reinterpret_cast<uint32_t*>(&rax23) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 1));
                    }
                    if (*reinterpret_cast<void***>(&rax23)) {
                        do {
                            ++rdx10;
                            *reinterpret_cast<void***>(rbx20) = *reinterpret_cast<void***>(&rax23);
                            ++rbx20;
                            *reinterpret_cast<uint32_t*>(&rax23) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10));
                        } while (*reinterpret_cast<void***>(&rax23));
                        eax19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_3 + 1));
                        ++r13_3;
                        if (*reinterpret_cast<void***>(&eax19)) 
                            goto addr_2f03_7; else 
                            break;
                    }
                }
                eax19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_3 + 1));
                ++r13_3;
            } while (*reinterpret_cast<void***>(&eax19));
        }
        *reinterpret_cast<void***>(rbx20) = reinterpret_cast<void**>(0);
        return r12_18;
    }
    xalloc_die();
    rax26 = g28;
    v27 = rax26;
    rax28 = fun_2670(rbp4, rsi, rbp4, rsi);
    rbp29 = rax28;
    rax30 = fun_2560();
    fun_2750(1, rax30, 5);
    fun_2750(1, "%-28s", rbp4);
    rax31 = fun_2560();
    fun_2750(1, rax31, 5);
    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 - 8 - 8 - 8 - 0x418 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (rbp29) 
        goto addr_301e_20;
    *reinterpret_cast<int32_t*>(&rdi33) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0;
    fun_2560();
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
    rax35 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(g28));
    if (!rax35) {
    }
    fun_2590();
    rbp36 = rdi33;
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1e8);
    edx38 = reinterpret_cast<unsigned char>(rdi33->f8);
    rax39 = g28;
    v40 = rax39;
    if (*reinterpret_cast<signed char*>(&edx38) != 47) {
        rdi41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp37) + 0xa0);
    } else {
        rdi41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp37) + 0xa0);
    }
    r12_42 = &rbp36->f8;
    rsi43 = &rbp36->f28;
    rax44 = r12_42;
    while (*reinterpret_cast<signed char*>(&edx38) && (++rax44, reinterpret_cast<uint64_t>(rsi43) > reinterpret_cast<uint64_t>(rax44))) {
        edx38 = reinterpret_cast<unsigned char>(*rax44);
    }
    eax45 = fun_2690(rdi41, reinterpret_cast<uint64_t>(rsp37) + 16);
    if (eax45) {
        r13d46 = 63;
        *reinterpret_cast<int32_t*>(&r14_47) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_47) + 4) = 0;
    } else {
        r14_47 = v48;
        r13d46 = (*reinterpret_cast<uint32_t*>(&r13_3) - (*reinterpret_cast<uint32_t*>(&r13_3) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_3) < *reinterpret_cast<uint32_t*>(&r13_3) + reinterpret_cast<uint1_t>((v49 & 16) < 1))) & 10) + 32;
    }
    rbx50 = &rbp36->f2c;
    fun_2750(1, "%-8.*s", 32, 1, "%-8.*s", 32);
    rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp37) - 8 + 8 - 8 + 8);
    zf52 = include_fullname == 0;
    if (zf52) {
        addr_351f_35:
        r8_53 = r12_42;
        *reinterpret_cast<int32_t*>(&rcx54) = 32;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx54) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx55) = r13d46;
        *reinterpret_cast<int32_t*>(&rdx55 + 4) = 0;
        rsi56 = reinterpret_cast<void**>(" %c%-8.*s");
        fun_2750(1, " %c%-8.*s", rdx55, 1, " %c%-8.*s", rdx55);
        rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp51) - 8 + 8);
        zf58 = include_idle == 0;
        if (!zf58) {
            if (r14_47) {
                rcx59 = now_2;
                if (!rcx59) {
                    fun_26c0(0xc128, " %c%-8.*s", rdx55);
                    rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
                    rcx59 = now_2;
                }
                rcx54 = rcx59 - r14_47;
                rdx55 = reinterpret_cast<void**>("     ");
                if (rcx54 > 59) {
                    if (rcx54 > 0x1517f) {
                        rdx60 = reinterpret_cast<void***>((__intrinsic() >> 13) - reinterpret_cast<uint64_t>(rcx54 >> 63));
                        rcx54 = reinterpret_cast<int64_t>("%lud");
                        r8_53 = rdx60;
                        fun_2850(0xc110, 1, 22, "%lud", r8_53, r9_61);
                        rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
                        rdx55 = reinterpret_cast<void**>(0xc110);
                    } else {
                        r8_53 = reinterpret_cast<void***>((__intrinsic() >> 10) - reinterpret_cast<uint64_t>(rcx54 >> 63));
                        rcx54 = reinterpret_cast<int64_t>("%02d:%02d");
                        rdx62 = __intrinsic() >> 5;
                        *reinterpret_cast<int32_t*>(&r9_63) = *reinterpret_cast<int32_t*>(&rdx62);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_63) + 4) = 0;
                        fun_2850(0xc110, 1, 22, "%02d:%02d", r8_53, r9_63);
                        rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
                        rdx55 = reinterpret_cast<void**>(0xc110);
                    }
                }
            } else {
                rax64 = fun_2560();
                rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
                rdx55 = rax64;
            }
            rsi56 = reinterpret_cast<void**>(" %-6s");
            fun_2750(1, " %-6s", rdx55, 1, " %-6s", rdx55);
            rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
        }
    } else {
        r15_65 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp51) + 0xd0);
        rcx66 = &rbp36->f4c;
        do {
            edx67 = *rbx50;
            if (!*reinterpret_cast<signed char*>(&edx67)) 
                break;
            ++rbx50;
        } while (reinterpret_cast<uint64_t>(rcx66) > reinterpret_cast<uint64_t>(rbx50));
        rax68 = fun_2670(r15_65, "%-8.*s");
        rsp69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp51) - 8 + 8);
        if (!rax68) 
            goto addr_37c9_49; else 
            goto addr_34d6_50;
    }
    v70 = rbp36->f154;
    rax71 = fun_2480(reinterpret_cast<uint64_t>(rsp57) + 8, rsi56, rdx55, rcx54, r8_53, r9_63);
    rsp72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
    if (!rax71) {
        rax73 = imaxtostr(v70, 0xc0e0, rdx55, rax71, r8_53, r9_63);
        rsp74 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp72) - 8 + 8);
        rdx75 = rax73;
    } else {
        rdx76 = time_format;
        fun_2760(0xc0e0, 33, rdx76, rax71, r8_53, r9_63);
        rsp74 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp72) - 8 + 8);
        rdx75 = reinterpret_cast<void**>(0xc0e0);
    }
    fun_2750(1, " %s", rdx75, 1, " %s", rdx75);
    zf77 = include_where == 0;
    if (zf77 || (edx78 = rbp36->f4c, !*reinterpret_cast<signed char*>(&edx78))) {
        addr_35df_55:
        rdi79 = stdout;
        rax80 = rdi79->f28;
        if (reinterpret_cast<unsigned char>(rax80) >= reinterpret_cast<unsigned char>(rdi79->f30)) {
            fun_25e0();
        } else {
            rdi79->f28 = rax80 + 1;
            *reinterpret_cast<void***>(rax80) = reinterpret_cast<void**>(10);
        }
    } else {
        r15_81 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp74) - 8 + 8 + 0xd0);
        rax82 = &rbp36->f4c;
        rbp83 = &rbp36->f14c;
        do {
            ++rax82;
            if (reinterpret_cast<uint64_t>(rbp83) <= reinterpret_cast<uint64_t>(rax82)) 
                break;
            edx78 = *rax82;
        } while (*reinterpret_cast<signed char*>(&edx78));
        rax84 = fun_25c0(r15_81, 58);
        if (!rax84) 
            goto addr_380f_62; else 
            goto addr_3690_63;
    }
    rax85 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v40) - reinterpret_cast<unsigned char>(g28));
    if (rax85) {
        fun_2590();
    } else {
        goto v86;
    }
    addr_380f_62:
    if (v87) {
        rax88 = canon_host(r15_81, 58);
        rbp89 = rax88;
        if (rax88) {
            addr_3820_69:
            rdx90 = rbp89;
            rsi91 = reinterpret_cast<void**>(" %s");
            fun_2750(1, " %s", rdx90, 1, " %s", rdx90);
        } else {
            goto addr_381d_71;
        }
    } else {
        addr_381d_71:
        rbp89 = r15_81;
        goto addr_3820_69;
    }
    addr_36c1_72:
    if (rbp89 != r15_81) {
        fun_2470(rbp89, rsi91, rdx90, rbp89, rsi91, rdx90);
        goto addr_35df_55;
    }
    addr_3690_63:
    *rax84 = 0;
    if (v92) {
        rax93 = canon_host(r15_81, 58);
        rbp89 = rax93;
        if (rax93) {
            addr_36a8_75:
            rdx90 = rbp89;
            rsi91 = reinterpret_cast<void**>(" %s:%s");
            fun_2750(1, " %s:%s", rdx90, 1, " %s:%s", rdx90);
            goto addr_36c1_72;
        } else {
            goto addr_36a5_77;
        }
    } else {
        addr_36a5_77:
        rbp89 = r15_81;
        goto addr_36a8_75;
    }
    addr_37c9_49:
    rax94 = fun_2560();
    fun_2750(1, " %19s", rax94, 1, " %19s", rax94);
    rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp69) - 8 + 8 - 8 + 8);
    goto addr_351f_35;
    addr_34d6_50:
    r15_95 = *reinterpret_cast<void***>(rax68 + 24);
    rax96 = fun_25c0(r15_95, 44);
    if (rax96) {
        *rax96 = 0;
        r15_95 = *reinterpret_cast<void***>(rax68 + 24);
    }
    rsi97 = *reinterpret_cast<void***>(rax68);
    rax98 = create_fullname(r15_95, rsi97);
    fun_2750(1, " %-19.19s", rax98, 1, " %-19.19s", rax98);
    fun_2470(rax98, " %-19.19s", rax98);
    rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp69) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_351f_35;
    addr_301e_20:
    r12_99 = *reinterpret_cast<void***>(rbp29 + 24);
    rax100 = fun_25c0(r12_99, 44, r12_99, 44);
    if (rax100) {
        *rax100 = 0;
        r12_99 = *reinterpret_cast<void***>(rbp29 + 24);
    }
    rsi101 = *reinterpret_cast<void***>(rbp29);
    r13_3 = reinterpret_cast<void**>(" %s");
    rax102 = create_fullname(r12_99, rsi101, r12_99, rsi101);
    rsi103 = reinterpret_cast<void**>(" %s");
    rdx104 = rax102;
    fun_2750(1, " %s", rdx104);
    fun_2470(rax102, " %s", rdx104);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    rdi105 = stdout;
    rax106 = rdi105->f28;
    if (reinterpret_cast<unsigned char>(rax106) >= reinterpret_cast<unsigned char>(rdi105->f30)) {
        *reinterpret_cast<int32_t*>(&rsi103) = 10;
        *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
        fun_25e0();
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8);
    } else {
        rdx104 = rax106 + 1;
        rdi105->f28 = rdx104;
        *reinterpret_cast<void***>(rax106) = reinterpret_cast<void**>(10);
    }
    zf107 = include_home_and_shell == 0;
    if (!zf107) {
        rax108 = fun_2560();
        fun_2750(1, rax108, 5);
        rdx109 = *reinterpret_cast<void***>(rbp29 + 32);
        fun_2750(1, "%-29s", rdx109);
        rax110 = fun_2560();
        fun_2750(1, rax110, 5);
        rdx104 = *reinterpret_cast<void***>(rbp29 + 40);
        rsi103 = reinterpret_cast<void**>(" %s");
        fun_2750(1, " %s", rdx104);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rdi111 = stdout;
        rax112 = rdi111->f28;
        if (reinterpret_cast<unsigned char>(rax112) >= reinterpret_cast<unsigned char>(rdi111->f30)) {
            *reinterpret_cast<int32_t*>(&rsi103) = 10;
            *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
            fun_25e0();
            rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8);
        } else {
            rdx104 = rax112 + 1;
            zf113 = include_project == 0;
            rdi111->f28 = rdx104;
            *reinterpret_cast<void***>(rax112) = reinterpret_cast<void**>(10);
            if (zf113) 
                goto addr_30a1_88; else 
                goto addr_3193_89;
        }
    }
    zf114 = include_project == 0;
    if (!zf114) {
        addr_3193_89:
        rdi115 = *reinterpret_cast<void***>(rbp29 + 32);
        rax116 = fun_2580(rdi115, rdi115);
        rax117 = xmalloc(rax116 + 10, rsi103, rdx104);
        rsi118 = *reinterpret_cast<void***>(rbp29 + 32);
        r13_3 = rax117;
        rax119 = fun_2550(rax117, rsi118, rdx104);
        rsi103 = reinterpret_cast<void**>("r");
        rax119->f0 = reinterpret_cast<int32_t>(0x63656a6f72702e2f);
        rax119->f8 = 0x74;
        rax120 = fun_27a0(r13_3, "r", r13_3, "r");
        rsp121 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        r12_122 = rax120;
        if (rax120) {
            rbx123 = rsp121;
            rax124 = fun_2560();
            fun_2750(1, rax124, 5);
            rsp125 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp121) - 8 + 8 - 8 + 8);
            while (*reinterpret_cast<int32_t*>(&rsi103) = 1, *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0, rax126 = fun_2500(rbx123, 1, 0x400, r12_122), rsp127 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp125) - 8 + 8), rdx104 = rax126, !!rax126) {
                rcx128 = stdout;
                fun_2720(rbx123, 1, rdx104, rcx128);
                rsp125 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp127) - 8 + 8);
            }
            rpl_fclose(r12_122, 1, rdx104, r12_122);
            rsp121 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp127) - 8 + 8);
        }
    } else {
        addr_30a1_88:
        zf129 = include_plan == 0;
        if (!zf129) 
            goto addr_325e_95; else 
            goto addr_30ae_96;
    }
    fun_2470(r13_3, rsi103, rdx104, r13_3, rsi103, rdx104);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp121) - 8 + 8);
    zf130 = include_plan == 0;
    if (zf130) {
        addr_30ae_96:
        rdi33 = stdout;
        rax131 = rdi33->f28;
        if (reinterpret_cast<unsigned char>(rax131) >= reinterpret_cast<unsigned char>(rdi33->f30)) {
            rax132 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(g28));
            if (!rax132) {
            }
        } else {
            rdi33->f28 = rax131 + 1;
            *reinterpret_cast<void***>(rax131) = reinterpret_cast<void**>(10);
            rax133 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(g28));
            if (!rax133) {
                goto v5;
            }
        }
    } else {
        addr_325e_95:
        rdi134 = *reinterpret_cast<void***>(rbp29 + 32);
        rax135 = fun_2580(rdi134, rdi134);
        rdi136 = rax135 + 7;
        rax137 = xmalloc(rdi136, rsi103, rdx104, rdi136, rsi103, rdx104);
        rsi138 = *reinterpret_cast<void***>(rbp29 + 32);
        r12_122 = rax137;
        rax139 = fun_2550(rax137, rsi138, rdx104, rax137, rsi138, rdx104);
        *reinterpret_cast<int32_t*>(&rdx140) = 0x6e61;
        *reinterpret_cast<int32_t*>(&rdx140 + 4) = 0;
        rsi141 = reinterpret_cast<void**>("r");
        rax139->f0 = 0x6c702e2f;
        rax139->f4 = 0x6e61;
        rax139->f6 = 0;
        rax142 = fun_27a0(r12_122, "r", r12_122, "r");
        rsp143 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rbp29 = rax142;
        if (rax142) {
            rbx123 = rsp143;
            rax144 = fun_2560();
            fun_2750(1, rax144, 5, 1, rax144, 5);
            rsp145 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp143) - 8 + 8 - 8 + 8);
            while (*reinterpret_cast<int32_t*>(&rsi141) = 1, *reinterpret_cast<int32_t*>(&rsi141 + 4) = 0, rax146 = fun_2500(rbx123, 1, 0x400, rbp29), rsp147 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp145) - 8 + 8), rdx140 = rax146, !!rax146) {
                rcx148 = stdout;
                fun_2720(rbx123, 1, rdx140, rcx148);
                rsp145 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp147) - 8 + 8);
            }
            rpl_fclose(rbp29, 1, rdx140, rbp29);
            rsp143 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp147) - 8 + 8);
        }
    }
    fun_2470(r12_122, rsi141, rdx140, r12_122, rsi141, rdx140);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp143) - 8 + 8);
    goto addr_30ae_96;
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x8763);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x875c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x8767);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x8758);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbdb8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbdb8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2443() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x2030;

void fun_2453() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t utmpxname = 0x2040;

void fun_2463() {
    __asm__("cli ");
    goto utmpxname;
}

int64_t free = 0x2050;

void fun_2473() {
    __asm__("cli ");
    goto free;
}

int64_t localtime = 0x2060;

void fun_2483() {
    __asm__("cli ");
    goto localtime;
}

int64_t abort = 0x2070;

void fun_2493() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2080;

void fun_24a3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncpy = 0x2090;

void fun_24b3() {
    __asm__("cli ");
    goto strncpy;
}

int64_t strncmp = 0x20a0;

void fun_24c3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20b0;

void fun_24d3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20c0;

void fun_24e3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20d0;

void fun_24f3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fread_unlocked = 0x20e0;

void fun_2503() {
    __asm__("cli ");
    goto fread_unlocked;
}

int64_t textdomain = 0x20f0;

void fun_2513() {
    __asm__("cli ");
    goto textdomain;
}

int64_t endutxent = 0x2100;

void fun_2523() {
    __asm__("cli ");
    goto endutxent;
}

int64_t fclose = 0x2110;

void fun_2533() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2120;

void fun_2543() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t stpcpy = 0x2130;

void fun_2553() {
    __asm__("cli ");
    goto stpcpy;
}

int64_t dcgettext = 0x2140;

void fun_2563() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2150;

void fun_2573() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2160;

void fun_2583() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2170;

void fun_2593() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2180;

void fun_25a3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2190;

void fun_25b3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x21a0;

void fun_25c3() {
    __asm__("cli ");
    goto strchr;
}

int64_t gai_strerror = 0x21b0;

void fun_25d3() {
    __asm__("cli ");
    goto gai_strerror;
}

int64_t __overflow = 0x21c0;

void fun_25e3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21d0;

void fun_25f3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21e0;

void fun_2603() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x21f0;

void fun_2613() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x2200;

void fun_2623() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2210;

void fun_2633() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2220;

void fun_2643() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2230;

void fun_2653() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2240;

void fun_2663() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t getpwnam = 0x2250;

void fun_2673() {
    __asm__("cli ");
    goto getpwnam;
}

int64_t setutxent = 0x2260;

void fun_2683() {
    __asm__("cli ");
    goto setutxent;
}

int64_t stat = 0x2270;

void fun_2693() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2280;

void fun_26a3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t kill = 0x2290;

void fun_26b3() {
    __asm__("cli ");
    goto kill;
}

int64_t time = 0x22a0;

void fun_26c3() {
    __asm__("cli ");
    goto time;
}

int64_t fileno = 0x22b0;

void fun_26d3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x22c0;

void fun_26e3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22d0;

void fun_26f3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22e0;

void fun_2703() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22f0;

void fun_2713() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x2300;

void fun_2723() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x2310;

void fun_2733() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2320;

void fun_2743() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2330;

void fun_2753() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t strftime = 0x2340;

void fun_2763() {
    __asm__("cli ");
    goto strftime;
}

int64_t getutxent = 0x2350;

void fun_2773() {
    __asm__("cli ");
    goto getutxent;
}

int64_t error = 0x2360;

void fun_2783() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2370;

void fun_2793() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2380;

void fun_27a3() {
    __asm__("cli ");
    goto fopen;
}

int64_t __cxa_atexit = 0x2390;

void fun_27b3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x23a0;

void fun_27c3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23b0;

void fun_27d3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23c0;

void fun_27e3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t getaddrinfo = 0x23d0;

void fun_27f3() {
    __asm__("cli ");
    goto getaddrinfo;
}

int64_t strdup = 0x23e0;

void fun_2803() {
    __asm__("cli ");
    goto strdup;
}

int64_t mbsinit = 0x23f0;

void fun_2813() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2400;

void fun_2823() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2410;

void fun_2833() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t freeaddrinfo = 0x2420;

void fun_2843() {
    __asm__("cli ");
    goto freeaddrinfo;
}

int64_t __sprintf_chk = 0x2430;

void fun_2853() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_2740(int64_t rdi, ...);

void fun_2540(int64_t rdi, int64_t rsi);

void fun_2510(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_25a0(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t optind = 0;

signed char do_short_format = 1;

void fun_2780();

int32_t read_utmp(int64_t rdi, void** rsi, void* rdx);

void** quotearg_n_style_colon();

uint32_t hard_locale();

signed char include_heading = 1;

uint32_t time_format_width = 0;

void usage();

void** Version = reinterpret_cast<void**>(0xec);

void version_etc(struct s0* rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_27c0();

int32_t fun_24c0(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8, void** a9, void** a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18, int64_t a19, int64_t a20);

int64_t fun_28a3(int32_t edi, void** rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r15_5;
    int64_t v6;
    int64_t r14_7;
    void** r14_8;
    int64_t v9;
    int64_t r13_10;
    void** r13_11;
    int64_t v12;
    int64_t r12_13;
    void** r12_14;
    int64_t v15;
    int64_t rbp16;
    int64_t v17;
    int64_t rbx18;
    void** rbx19;
    void** rdi20;
    void** rax21;
    void** v22;
    void*** rbp23;
    int64_t r8_24;
    void** rcx25;
    void** rdx26;
    void** rsi27;
    int64_t rdi28;
    int32_t eax29;
    void* rsp30;
    void* rax31;
    int1_t zf32;
    uint1_t zf33;
    void*** rbx34;
    void** rdi35;
    void* rax36;
    int32_t eax37;
    void** rax38;
    int32_t* rax39;
    void** v40;
    uint32_t eax41;
    int1_t zf42;
    struct s0* rdi43;
    int64_t rax44;
    void** rax45;
    int1_t zf46;
    void** rax47;
    void** rax48;
    int1_t zf49;
    void** rax50;
    void** rax51;
    int1_t zf52;
    void** rax53;
    struct s0* rdi54;
    void** rax55;
    void** r15_56;
    int64_t v57;
    int64_t v58;
    int64_t v59;
    int64_t v60;
    int64_t v61;
    int32_t eax62;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r15_5;
    v6 = r14_7;
    r14_8 = reinterpret_cast<void**>(0xbb00);
    v9 = r13_10;
    r13_11 = reinterpret_cast<void**>("sfwiqbhlp");
    v12 = r12_13;
    *reinterpret_cast<int32_t*>(&r12_14) = edi;
    v15 = rbp16;
    v17 = rbx18;
    rbx19 = rsi;
    rdi20 = *reinterpret_cast<void***>(rsi);
    rax21 = g28;
    v22 = rax21;
    set_program_name(rdi20);
    fun_2740(6, 6);
    fun_2540("coreutils", "/usr/local/share/locale");
    rbp23 = reinterpret_cast<void***>(0x8694);
    fun_2510("coreutils", "/usr/local/share/locale");
    atexit(0x3e10, "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&r8_24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_24) + 4) = 0;
    rcx25 = reinterpret_cast<void**>(0xbb00);
    rdx26 = reinterpret_cast<void**>("sfwiqbhlp");
    rsi27 = rbx19;
    *reinterpret_cast<int32_t*>(&rdi28) = *reinterpret_cast<int32_t*>(&r12_14);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0;
    eax29 = fun_25a0(rdi28, rsi27, "sfwiqbhlp", 0xbb00);
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (eax29 == -1) {
        addr_2a73_3:
        rax31 = reinterpret_cast<void*>(static_cast<int64_t>(optind));
        *reinterpret_cast<int32_t*>(&r12_14) = *reinterpret_cast<int32_t*>(&r12_14) - *reinterpret_cast<int32_t*>(&rax31);
        *reinterpret_cast<int32_t*>(&r12_14 + 4) = 0;
        zf32 = do_short_format == 0;
        if (zf32) {
            zf33 = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r12_14) == 0);
            if (zf33) {
                fun_2560();
                fun_2780();
                goto addr_2bb8_6;
            } else {
                if (!(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&r12_14) < 0) | zf33)) {
                    *reinterpret_cast<uint32_t*>(&rdx26) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(r12_14 + 0xffffffffffffffff));
                    *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                    rbp23 = reinterpret_cast<void***>(rbx19 + reinterpret_cast<int64_t>(rax31) * 8);
                    rbx34 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx19 + (reinterpret_cast<int64_t>(rax31) + reinterpret_cast<unsigned char>(rdx26)) * 8) + 8);
                    do {
                        rdi35 = *rbp23;
                        rbp23 = rbp23 + 8;
                        print_long_entry(rdi35, rsi27);
                    } while (rbp23 != rbx34);
                }
                rax36 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v22) - reinterpret_cast<unsigned char>(g28));
                if (!rax36) {
                    return 0;
                }
            }
        } else {
            rsi27 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp30) + 8);
            rbp23 = reinterpret_cast<void***>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax31)));
            r13_11 = reinterpret_cast<void**>("/var/run/utmp");
            eax37 = read_utmp("/var/run/utmp", rsi27, reinterpret_cast<int64_t>(rsp30) + 16);
            if (eax37) {
                rax38 = quotearg_n_style_colon();
                r12_14 = rax38;
                rax39 = fun_24a0();
                rcx25 = r12_14;
                rdx26 = reinterpret_cast<void**>("%s");
                *reinterpret_cast<int32_t*>(&rsi27) = *rax39;
                *reinterpret_cast<int32_t*>(&rsi27 + 4) = 0;
                fun_2780();
            } else {
                r13_11 = reinterpret_cast<void**>(0);
                r14_8 = v40;
                eax41 = hard_locale();
                rdx26 = reinterpret_cast<void**>("%b %e %H:%M");
                rcx25 = reinterpret_cast<void**>("%Y-%m-%d %H:%M");
                if (*reinterpret_cast<unsigned char*>(&eax41)) {
                    rdx26 = reinterpret_cast<void**>("%Y-%m-%d %H:%M");
                }
                time_format = rdx26;
                zf42 = include_heading == 0;
                time_format_width = (eax41 - (eax41 + reinterpret_cast<uint1_t>(eax41 < eax41 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&eax41) < 1))) & 0xfffffffc) + 16;
                if (!zf42) 
                    goto addr_2bc9_17; else 
                    goto addr_2b05_18;
            }
        }
    } else {
        if (eax29 > 0x77) {
            addr_2bb8_6:
            usage();
            goto addr_2bc2_20;
        } else {
            if (eax29 <= 97) {
                if (eax29 == 0xffffff7d) {
                    rdi43 = stdout;
                    rcx25 = Version;
                    r8_24 = reinterpret_cast<int64_t>("Joseph Arceneaux");
                    rdx26 = reinterpret_cast<void**>("GNU coreutils");
                    rsi27 = reinterpret_cast<void**>("pinky");
                    version_etc(rdi43, "pinky", "GNU coreutils", rcx25, "Joseph Arceneaux", "David MacKenzie", "Kaveh Ghazi", 0);
                    eax29 = fun_27c0();
                    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 - 8 - 8 + 8 - 8 + 8);
                }
                if (eax29 != 0xffffff7e) 
                    goto addr_2bb8_6; else 
                    goto addr_2a6c_25;
            } else {
                *reinterpret_cast<uint32_t*>(&rax44) = eax29 - 98;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax44) + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax44) <= 21) {
                    goto reinterpret_cast<int32_t>(*reinterpret_cast<void***>(0x8694 + rax44 * 4)) + 0x8694;
                }
            }
        }
    }
    fun_2590();
    goto addr_2cc6_29;
    while (1) {
        addr_2bc2_20:
        fun_27c0();
        addr_2bc9_17:
        rax45 = fun_2560();
        fun_2750(1, "%-8s", rax45, 1, "%-8s", rax45);
        zf46 = include_fullname == 0;
        if (!zf46) {
            rax47 = fun_2560();
            fun_2750(1, " %-19s", rax47, 1, " %-19s", rax47);
        }
        rax48 = fun_2560();
        fun_2750(1, " %-9s", rax48, 1, " %-9s", rax48);
        zf49 = include_idle == 0;
        if (!zf49) {
            rax50 = fun_2560();
            fun_2750(1, " %-6s", rax50, 1, " %-6s", rax50);
        }
        rax51 = fun_2560();
        *reinterpret_cast<uint32_t*>(&rdx26) = time_format_width;
        *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
        rsi27 = reinterpret_cast<void**>(" %-*s");
        rcx25 = rax51;
        fun_2750(1, " %-*s", rdx26, 1, " %-*s", rdx26);
        zf52 = include_where == 0;
        if (!zf52) {
            rax53 = fun_2560();
            rsi27 = reinterpret_cast<void**>(" %s");
            rdx26 = rax53;
            fun_2750(1, " %s", rdx26, 1, " %s", rdx26);
        }
        rdi54 = stdout;
        rax55 = rdi54->f28;
        if (reinterpret_cast<unsigned char>(rax55) >= reinterpret_cast<unsigned char>(rdi54->f30)) {
            *reinterpret_cast<int32_t*>(&rsi27) = 10;
            *reinterpret_cast<int32_t*>(&rsi27 + 4) = 0;
            fun_25e0();
        } else {
            rdx26 = rax55 + 1;
            rdi54->f28 = rdx26;
            *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(10);
        }
        addr_2b05_18:
        rbp23 = reinterpret_cast<void***>(rbx19 + reinterpret_cast<uint64_t>(rbp23) * 8);
        while (r14_8) {
            if (!*reinterpret_cast<unsigned char*>(r13_11 + 44) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_11) == 7)) {
                addr_2b0b_40:
                r13_11 = r13_11 + 0x180;
                --r14_8;
                continue;
            } else {
                if (*reinterpret_cast<int32_t*>(&r12_14)) {
                    addr_2cc6_29:
                    *reinterpret_cast<int32_t*>(&rbx19) = 0;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                    r15_56 = r13_11 + 44;
                    do {
                        if (*reinterpret_cast<int32_t*>(&r12_14) <= *reinterpret_cast<int32_t*>(&rbx19)) 
                            break;
                        rsi27 = rbp23[reinterpret_cast<unsigned char>(rbx19) * 8];
                        *reinterpret_cast<uint32_t*>(&rdx26) = 32;
                        *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                        ++rbx19;
                        eax62 = fun_24c0(r15_56, rsi27, 32, rcx25, r8_24, "David MacKenzie", v57, v58, 0, v22, v59, v17, v15, v12, v9, v6, v4, v3, v60, v61);
                    } while (eax62);
                    goto addr_2b37_44;
                } else {
                    addr_2b37_44:
                    print_entry(r13_11, rsi27, rdx26, rcx25, r8_24, "David MacKenzie");
                    goto addr_2b0b_40;
                }
            }
            goto addr_2b0b_40;
        }
    }
    addr_2a6c_25:
    usage();
    rsp30 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp30) - 8 + 8);
    goto addr_2a73_3;
}

int64_t __libc_start_main = 0;

void fun_2d93() {
    __asm__("cli ");
    __libc_start_main(0x28a0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_2440(int64_t rdi);

int64_t fun_2e33() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2440(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2e73() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2630(void** rdi, struct s0* rsi, int64_t rdx, void** rcx);

int32_t fun_2650(int64_t rdi);

struct s0* stderr = reinterpret_cast<struct s0*>(0);

void fun_27e0(struct s0* rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_38d3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    struct s0* r12_6;
    void** rax7;
    void** rcx8;
    struct s0* r12_9;
    void** rax10;
    struct s0* r12_11;
    void** rax12;
    struct s0* r12_13;
    void** rax14;
    void** rax15;
    int32_t eax16;
    void** r13_17;
    void** rax18;
    void** rax19;
    int64_t r8_20;
    int64_t r9_21;
    int32_t eax22;
    void** rax23;
    void** rax24;
    void** rax25;
    int64_t r8_26;
    int64_t r9_27;
    int32_t eax28;
    void** rax29;
    struct s0* r15_30;
    void** rax31;
    void** rax32;
    void** rax33;
    struct s0* rdi34;
    void** r8_35;
    void** r9_36;
    int64_t v37;
    int64_t v38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2560();
            fun_2750(1, rax5, r12_2, 1, rax5, r12_2);
            r12_6 = stdout;
            rax7 = fun_2560();
            fun_2630(rax7, r12_6, 5, rcx8);
            r12_9 = stdout;
            rax10 = fun_2560();
            fun_2630(rax10, r12_9, 5, rcx8);
            r12_11 = stdout;
            rax12 = fun_2560();
            fun_2630(rax12, r12_11, 5, rcx8);
            r12_13 = stdout;
            rax14 = fun_2560();
            fun_2630(rax14, r12_13, 5, rcx8);
            rax15 = fun_2560();
            fun_2750(1, rax15, "/var/run/utmp", 1, rax15, "/var/run/utmp");
            do {
                if (1) 
                    break;
                eax16 = fun_2650("pinky");
            } while (eax16);
            r13_17 = v4;
            if (!r13_17) {
                rax18 = fun_2560();
                fun_2750(1, rax18, "GNU coreutils", 1, rax18, "GNU coreutils");
                rax19 = fun_2740(5);
                if (!rax19 || (eax22 = fun_24c0(rax19, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_20, r9_21, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0), !eax22)) {
                    rax23 = fun_2560();
                    r13_17 = reinterpret_cast<void**>("pinky");
                    fun_2750(1, rax23, "https://www.gnu.org/software/coreutils/", 1, rax23, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_17 = reinterpret_cast<void**>("pinky");
                    goto addr_3c30_9;
                }
            } else {
                rax24 = fun_2560();
                fun_2750(1, rax24, "GNU coreutils", 1, rax24, "GNU coreutils");
                rax25 = fun_2740(5);
                if (!rax25 || (eax28 = fun_24c0(rax25, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_26, r9_27, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0), !eax28)) {
                    addr_3b36_11:
                    rax29 = fun_2560();
                    fun_2750(1, rax29, "https://www.gnu.org/software/coreutils/", 1, rax29, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_17 == "pinky")) {
                        r12_2 = reinterpret_cast<void**>(0x8b81);
                    }
                } else {
                    addr_3c30_9:
                    r15_30 = stdout;
                    rax31 = fun_2560();
                    fun_2630(rax31, r15_30, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3b36_11;
                }
            }
            rax32 = fun_2560();
            rcx8 = r12_2;
            fun_2750(1, rax32, r13_17, 1, rax32, r13_17);
            addr_392e_14:
            fun_27c0();
        }
    } else {
        rax33 = fun_2560();
        rdi34 = stderr;
        rcx8 = r12_2;
        fun_27e0(rdi34, 1, rax33, rcx8, r8_35, r9_36, v37, v38, v39, v40, v41, v42);
        goto addr_392e_14;
    }
}

/* hints.0 */
int32_t hints_0 = 0;

int32_t fun_27f0();

int32_t last_cherror = 0;

int64_t g20;

int64_t fun_2800(int64_t rdi);

void fun_2840(int64_t rdi);

int64_t fun_3c63(int64_t rdi) {
    void** rax2;
    int32_t eax3;
    int64_t r12_4;
    int64_t rdi5;
    int64_t rax6;
    void* rax7;

    __asm__("cli ");
    rax2 = g28;
    hints_0 = 2;
    eax3 = fun_27f0();
    if (eax3) {
        last_cherror = eax3;
        *reinterpret_cast<int32_t*>(&r12_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_4) + 4) = 0;
    } else {
        rdi5 = g20;
        if (!rdi5) {
            rdi5 = rdi;
        }
        rax6 = fun_2800(rdi5);
        r12_4 = rax6;
        if (!rax6) {
            last_cherror = -10;
        }
        fun_2840(0);
    }
    rax7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax7) {
        fun_2590();
    } else {
        return r12_4;
    }
}

int64_t fun_3d23(int64_t rdi, int32_t* rsi) {
    void** rax3;
    int32_t eax4;
    int64_t r12_5;
    int64_t rdi6;
    int64_t rax7;
    void* rax8;

    __asm__("cli ");
    rax3 = g28;
    hints_0 = 2;
    eax4 = fun_27f0();
    if (eax4) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        if (rsi) {
            *rsi = eax4;
        }
    } else {
        rdi6 = g20;
        if (!rdi6) {
            rdi6 = rdi;
        }
        rax7 = fun_2800(rdi6);
        r12_5 = rax7;
        if (!rax7 && rsi) {
            *rsi = -10;
        }
        fun_2840(0);
    }
    rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rax8) {
        fun_2590();
    } else {
        return r12_5;
    }
}

void fun_3de3() {
    __asm__("cli ");
}

int64_t file_name = 0;

void fun_3df3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3e03(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s0* rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_24d0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_3e13() {
    struct s0* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s0* rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_24a0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2560();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3ea3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2780();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_24d0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3ea3_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2780();
    }
}

struct s5 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_26d0(struct s5* rdi);

int32_t fun_2710(struct s5* rdi);

int64_t fun_2600(int64_t rdi, ...);

int32_t rpl_fflush(struct s5* rdi);

int64_t fun_2530(struct s5* rdi);

int64_t fun_3ec3(struct s5* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_26d0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2710(rdi);
        if (!(eax3 && (eax4 = fun_26d0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2600(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_24a0();
            r12d9 = *rax8;
            rax10 = fun_2530(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2530;
}

void rpl_fseeko(struct s5* rdi);

void fun_3f53(struct s5* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2710(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_3fa3(struct s5* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_26d0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2600(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

int32_t setlocale_null_r();

int64_t fun_4023() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax3;
    }
}

struct s6 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_40a3(uint64_t rdi, struct s6* rsi) {
    signed char* r8_3;
    uint64_t rcx4;
    signed char* rsi5;
    uint64_t rdx6;
    int32_t eax7;
    uint64_t rdx8;
    uint64_t rax9;
    uint64_t rcx10;
    int32_t ecx11;

    __asm__("cli ");
    rsi->f14 = 0;
    r8_3 = &rsi->f14;
    rcx4 = rdi;
    if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
        do {
            rsi5 = r8_3;
            --r8_3;
            rdx6 = (__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rcx4) >> 63);
            eax7 = static_cast<int32_t>(48 + (rdx6 + rdx6 * 4) * 2) - *reinterpret_cast<int32_t*>(&rcx4);
            rcx4 = rdx6;
            *r8_3 = *reinterpret_cast<signed char*>(&eax7);
        } while (rdx6);
        *(r8_3 - 1) = 45;
        return rsi5 - 2;
    } else {
        do {
            --r8_3;
            rdx8 = __intrinsic() >> 3;
            rax9 = rdx8 + rdx8 * 4;
            rcx10 = rcx4 - (rax9 + rax9);
            ecx11 = *reinterpret_cast<int32_t*>(&rcx10) + 48;
            *r8_3 = *reinterpret_cast<signed char*>(&ecx11);
            rcx4 = rdx8;
        } while (rdx8);
        return r8_3;
    }
}

void fun_27d0(void** rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

struct s7 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s7* fun_25f0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4143(void** rdi) {
    struct s0* rcx2;
    void** rbx3;
    struct s7* rax4;
    void** r12_5;
    void** rcx6;
    int64_t r8_7;
    int64_t r9_8;
    int64_t rbx9;
    int64_t rbp10;
    void** r12_11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    int64_t v20;
    int64_t v21;
    int32_t eax22;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_27d0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2490("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_25f0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax22 = fun_24c0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6, r8_7, r9_8, rbx9, rbp10, r12_11, __return_address(), v12, v13, v14, v15, v16, v17, v18, v19, v20, v21), !eax22))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_58e3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_24a0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xc2a0;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5923(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc2a0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5943(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc2a0);
    }
    *rdi = esi;
    return 0xc2a0;
}

int64_t fun_5963(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc2a0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s8 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_59a3(struct s8* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xc2a0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s9 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* fun_59c3(struct s9* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xc2a0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x286a;
    if (!rdx) 
        goto 0x286a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc2a0;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5a03(void** rdi, void** rsi, void** rdx, int64_t rcx, struct s10* r8) {
    struct s10* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s10*>(0xc2a0);
    }
    rax7 = fun_24a0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5a36);
    *rax7 = r15d8;
    return rax13;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5a83(void** rdi, int64_t rsi, void*** rdx, struct s11* rcx) {
    struct s11* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s11*>(0xc2a0);
    }
    rax6 = fun_24a0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5ab1);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5b0c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5b73() {
    __asm__("cli ");
}

void** gc098 = reinterpret_cast<void**>(0xa0);

int64_t slotvec0 = 0x100;

void fun_5b83() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdx8;
    void** rdi9;
    void** rsi10;
    void** rdx11;
    void** rsi12;
    void** rdx13;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2470(rdi6, rsi7, rdx8);
        } while (rbx4 != rbp5);
    }
    rdi9 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi9 != 0xc1a0) {
        fun_2470(rdi9, rsi10, rdx11);
        gc098 = reinterpret_cast<void**>(0xc1a0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc090) {
        fun_2470(r12_2, rsi12, rdx13);
        slotvec = reinterpret_cast<void**>(0xc090);
    }
    nslots = 1;
    return;
}

void fun_5c23() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c43() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c53(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c73(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_5c93(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2870;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_5d23(void** rdi, int32_t esi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2875;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void** fun_5db3(int32_t edi, void** rsi) {
    void** rax3;
    struct s2* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x287a;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2590();
    } else {
        return rax5;
    }
}

void** fun_5e43(int32_t edi, void** rsi, int64_t rdx) {
    void** rax4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x287f;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_5ed3(void** rdi, int64_t rsi, uint32_t edx) {
    struct s2* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x63c0]");
    __asm__("movdqa xmm1, [rip+0x63c8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x63b1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2590();
    } else {
        return rax10;
    }
}

void** fun_5f73(void** rdi, uint32_t esi) {
    struct s2* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6320]");
    __asm__("movdqa xmm1, [rip+0x6328]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6311]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2590();
    } else {
        return rax9;
    }
}

void** fun_6013(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6280]");
    __asm__("movdqa xmm1, [rip+0x6288]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6269]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2590();
    } else {
        return rax3;
    }
}

void** fun_60a3(void** rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x61f0]");
    __asm__("movdqa xmm1, [rip+0x61f8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x61e6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2590();
    } else {
        return rax4;
    }
}

void** fun_6133(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2884;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_61d3(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x60ba]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x60b2]");
    __asm__("movdqa xmm2, [rip+0x60ba]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2889;
    if (!rdx) 
        goto 0x2889;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void** fun_6273(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, int64_t r8) {
    void** rcx6;
    struct s2* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x601a]");
    __asm__("movdqa xmm1, [rip+0x6022]");
    __asm__("movdqa xmm2, [rip+0x602a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x288e;
    if (!rdx) 
        goto 0x288e;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2590();
    } else {
        return rax9;
    }
}

void** fun_6323(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5f6a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5f62]");
    __asm__("movdqa xmm2, [rip+0x5f6a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2893;
    if (!rsi) 
        goto 0x2893;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2590();
    } else {
        return rax6;
    }
}

void** fun_63c3(int64_t rdi, int64_t rsi, void** rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5eca]");
    __asm__("movdqa xmm1, [rip+0x5ed2]");
    __asm__("movdqa xmm2, [rip+0x5eda]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2898;
    if (!rsi) 
        goto 0x2898;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2590();
    } else {
        return rax7;
    }
}

void fun_6463() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6473(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6493() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_64b3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_24b0(void** rdi, int64_t rsi, int64_t rdx);

void** fun_64d3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    void** r12_5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rax4 = xmalloc(33, rsi, rdx);
    r12_5 = rax4;
    fun_24b0(rax4, rdi + 44, 32);
    *reinterpret_cast<void***>(r12_5 + 32) = reinterpret_cast<void**>(0);
    rax6 = fun_2580(r12_5, r12_5);
    rax7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax6) + reinterpret_cast<unsigned char>(r12_5));
    if (reinterpret_cast<unsigned char>(r12_5) >= reinterpret_cast<unsigned char>(rax7)) {
        addr_6518_2:
        return r12_5;
    } else {
        do {
            if (*reinterpret_cast<signed char*>(rax7 + 0xffffffffffffffff) != 32) 
                goto addr_6518_2;
            --rax7;
            *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(0);
        } while (r12_5 != rax7);
    }
    goto addr_6518_2;
}

void fun_2460();

void fun_2680();

struct s12 {
    void** f0;
    signed char[3] pad4;
    int32_t f4;
    signed char[36] pad44;
    signed char f2c;
    signed char[331] pad376;
    int64_t f178;
};

struct s12* fun_2770();

int32_t fun_26b0();

struct s13 {
    void** f0;
    signed char[375] pad376;
    int64_t f178;
};

void fun_2520();

int64_t fun_6543() {
    void** r15_1;
    int64_t* r14_2;
    int64_t* rsi3;
    uint32_t r12d4;
    uint32_t ecx5;
    uint32_t ebp6;
    int64_t rbx7;
    void*** v8;
    void*** rdx9;
    void** rax10;
    void** v11;
    struct s12* rax12;
    struct s12* r13_13;
    int32_t eax14;
    int32_t* rax15;
    void** rax16;
    int64_t rax17;
    struct s13* rax18;
    int64_t* rdi19;
    void* rax20;
    int64_t* rsi21;
    int64_t rcx22;
    struct s12* rax23;
    void* rax24;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r15_1) = 0;
    *reinterpret_cast<int32_t*>(&r15_1 + 4) = 0;
    r14_2 = rsi3;
    r12d4 = ecx5;
    ebp6 = r12d4 & 2;
    *reinterpret_cast<int32_t*>(&rbx7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
    v8 = rdx9;
    rax10 = g28;
    v11 = rax10;
    fun_2460();
    fun_2680();
    while (rax12 = fun_2770(), r13_13 = rax12, !!rax12) {
        do {
            if (!r13_13->f2c || r13_13->f0 != 7) {
                if (ebp6) 
                    break;
            } else {
                if (*reinterpret_cast<unsigned char*>(&r12d4) & 1 && (!(reinterpret_cast<uint1_t>(r13_13->f4 < 0) | reinterpret_cast<uint1_t>(r13_13->f4 == 0)) && (eax14 = fun_26b0(), eax14 < 0))) {
                    rax15 = fun_24a0();
                    if (*rax15 == 3) 
                        break;
                    if (rbx7) 
                        goto addr_65be_8;
                    goto addr_6688_10;
                }
            }
            if (!rbx7) {
                addr_6688_10:
                rax16 = xpalloc();
                r15_1 = rax16;
                goto addr_65be_8;
            } else {
                addr_65be_8:
                rax17 = rbx7 + rbx7 * 2;
                ++rbx7;
                rax18 = reinterpret_cast<struct s13*>((rax17 << 7) + reinterpret_cast<unsigned char>(r15_1));
                rax18->f0 = r13_13->f0;
                rdi19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rax18) + 8 & 0xfffffffffffffff8);
                rax18->f178 = r13_13->f178;
                rax20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax18) - reinterpret_cast<uint64_t>(rdi19));
                rsi21 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r13_13) - reinterpret_cast<uint64_t>(rax20));
                *reinterpret_cast<uint32_t*>(&rcx22) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax20) + 0x180) >> 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
            }
            while (rcx22) {
                --rcx22;
                *rdi19 = *rsi21;
                ++rdi19;
                ++rsi21;
            }
            rax23 = fun_2770();
            r13_13 = rax23;
        } while (rax23);
        goto addr_660d_15;
    }
    addr_6610_16:
    fun_2520();
    *r14_2 = rbx7;
    *v8 = r15_1;
    rax24 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax24) {
        fun_2590();
    } else {
        return 0;
    }
    addr_660d_15:
    goto addr_6610_16;
}

void fun_26a0(signed char* rdi, void** rsi, void** rdx);

int64_t fun_66c3(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2740(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2580(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_26a0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_26a0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6773() {
    __asm__("cli ");
    goto fun_2740;
}

struct s14 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2660(int64_t rdi, struct s0* rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_6783(struct s0* rdi, void** rsi, void** rdx, void** rcx, struct s14* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_27e0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_27e0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2560();
    fun_27e0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2660(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2560();
    fun_27e0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2660(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2560();
        fun_27e0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x8e28 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x8e28;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6bf3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s15 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6c13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s15* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s15* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2590();
    } else {
        return;
    }
}

void fun_6cb3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6d56_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6d60_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2590();
    } else {
        return;
    }
    addr_6d56_5:
    goto addr_6d60_7;
}

void fun_6d93() {
    struct s0* rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2660(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2560();
    fun_2750(1, rax6, "bug-coreutils@gnu.org");
    rax7 = fun_2560();
    fun_2750(1, rax7, "GNU coreutils", 1, rax7, "GNU coreutils");
    fun_2560();
    goto fun_2750;
}

int64_t fun_24f0();

void fun_6e33(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_26e0(void** rdi);

void fun_6e73(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_26e0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6e93(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_26e0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6eb3(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_26e0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2730();

void fun_6ed3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2730();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6f03() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2730();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6f33(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24f0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6f73() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6fb3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6fe3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24f0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7033(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24f0();
            if (rax5) 
                break;
            addr_707d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_707d_5;
        rax8 = fun_24f0();
        if (rax8) 
            goto addr_7066_9;
        if (rbx4) 
            goto addr_707d_5;
        addr_7066_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_70c3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24f0();
            if (rax8) 
                break;
            addr_710a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_710a_5;
        rax11 = fun_24f0();
        if (rax11) 
            goto addr_70f2_9;
        if (!rbx6) 
            goto addr_70f2_9;
        if (r12_4) 
            goto addr_710a_5;
        addr_70f2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7153(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_71fd_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7210_10:
                *r12_8 = 0;
            }
            addr_71b0_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_71d6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7224_14;
            if (rcx10 <= rsi9) 
                goto addr_71cd_16;
            if (rsi9 >= 0) 
                goto addr_7224_14;
            addr_71cd_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7224_14;
            addr_71d6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2730();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7224_14;
            if (!rbp13) 
                break;
            addr_7224_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_71fd_9;
        } else {
            if (!r13_6) 
                goto addr_7210_10;
            goto addr_71b0_11;
        }
    }
}

int64_t fun_2640();

void fun_7253() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7283() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2640();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72f3(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_26e0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

void fun_7333(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_26e0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

struct s16 {
    signed char[1] pad1;
    void** f1;
};

void fun_7373(int64_t rdi, struct s16* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_26e0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_26a0;
    }
}

void fun_73b3(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2580(rdi);
    rax3 = fun_26e0(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

void fun_73f3() {
    void** rdi1;

    __asm__("cli ");
    fun_2560();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2780();
    fun_2490(rdi1);
}

int64_t fun_24e0();

int64_t fun_7433(void** rdi, void** rsi, void** rdx, void** rcx) {
    int64_t rax5;
    uint32_t ebx6;
    int64_t rax7;
    int32_t* rax8;
    int32_t* rax9;

    __asm__("cli ");
    rax5 = fun_24e0();
    ebx6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax7 = rpl_fclose(rdi, rsi, rdx, rcx);
    if (ebx6) {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            addr_748e_3:
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rax8 = fun_24a0();
            *rax8 = 0;
            *reinterpret_cast<int32_t*>(&rax7) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax7)) {
            if (rax5) 
                goto addr_748e_3;
            rax9 = fun_24a0();
            *reinterpret_cast<int32_t*>(&rax7) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax9 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
    }
    return rax7;
}

signed char* fun_2700(int64_t rdi);

signed char* fun_74a3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2700(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_25b0(uint32_t* rdi);

uint64_t fun_74e3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    uint32_t eax8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_25b0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (eax8 = hard_locale(), !*reinterpret_cast<signed char*>(&eax8)))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2590();
    } else {
        return r12_7;
    }
}

void fun_7573() {
    __asm__("cli ");
}

void fun_7587() {
    __asm__("cli ");
    return;
}

void fun_2970() {
    include_fullname = 0;
    goto 0x2920;
}

uint32_t fun_2620(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2820(int64_t rdi, void** rsi);

uint32_t fun_2810(void** rdi, void** rsi);

void fun_4375() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2560();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2560();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2580(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4673_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4673_22; else 
                            goto addr_4a6d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4b2d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4e80_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4670_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4670_30; else 
                                goto addr_4e99_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2580(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4e80_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2620(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4e80_28; else 
                            goto addr_451c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4fe0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4e60_40:
                        if (r11_27 == 1) {
                            addr_49ed_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4fa8_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4627_44;
                            }
                        } else {
                            goto addr_4e70_46;
                        }
                    } else {
                        addr_4fef_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_49ed_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4673_22:
                                if (v47 != 1) {
                                    addr_4bc9_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2580(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4c14_54;
                                    }
                                } else {
                                    goto addr_4680_56;
                                }
                            } else {
                                addr_4625_57:
                                ebp36 = 0;
                                goto addr_4627_44;
                            }
                        } else {
                            addr_4e54_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_4fef_47; else 
                                goto addr_4e5e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_49ed_41;
                        if (v47 == 1) 
                            goto addr_4680_56; else 
                            goto addr_4bc9_52;
                    }
                }
                addr_46e1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4578_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_459d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_48a0_65;
                    } else {
                        addr_4709_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4f58_67;
                    }
                } else {
                    goto addr_4700_69;
                }
                addr_45b1_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_45fc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4f58_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_45fc_81;
                }
                addr_4700_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_459d_64; else 
                    goto addr_4709_66;
                addr_4627_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_46df_91;
                if (v22) 
                    goto addr_463f_93;
                addr_46df_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_46e1_62;
                addr_4c14_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_539b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_540b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_520f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2820(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2810(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_4d0e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_46cc_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4d18_112;
                    }
                } else {
                    addr_4d18_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4de9_114;
                }
                addr_46d8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_46df_91;
                while (1) {
                    addr_4de9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_52f7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4d56_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_5305_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4dd7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4dd7_128;
                        }
                    }
                    addr_4d85_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4dd7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_4d56_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4d85_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_45fc_81;
                addr_5305_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4f58_67;
                addr_539b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_4d0e_109;
                addr_540b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_4d0e_109;
                addr_4680_56:
                rax93 = fun_2830(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_46cc_110;
                addr_4e5e_59:
                goto addr_4e60_40;
                addr_4b2d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4673_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_46d8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4625_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4673_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4b72_160;
                if (!v22) 
                    goto addr_4f47_162; else 
                    goto addr_5153_163;
                addr_4b72_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_4f47_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_4f58_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_4a1b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4883_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_45b1_70; else 
                    goto addr_4897_169;
                addr_4a1b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4578_63;
                goto addr_4700_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4e54_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_4f8f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4670_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4568_178; else 
                        goto addr_4f12_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4e54_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4673_22;
                }
                addr_4f8f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4670_30:
                    r8d42 = 0;
                    goto addr_4673_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_46e1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4fa8_42;
                    }
                }
                addr_4568_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4578_63;
                addr_4f12_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4e70_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_46e1_62;
                } else {
                    addr_4f22_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4673_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_56d2_188;
                if (v28) 
                    goto addr_4f47_162;
                addr_56d2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4883_168;
                addr_451c_37:
                if (v22) 
                    goto addr_5513_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4533_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4fe0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_506b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4673_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4568_178; else 
                        goto addr_5047_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4e54_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4673_22;
                }
                addr_506b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4673_22;
                }
                addr_5047_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4e70_46;
                goto addr_4f22_186;
                addr_4533_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4673_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4673_22; else 
                    goto addr_4544_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_561e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_54a4_210;
            if (1) 
                goto addr_54a2_212;
            if (!v29) 
                goto addr_50de_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2570();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_5611_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_48a0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_465b_219; else 
            goto addr_48ba_220;
        addr_463f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4653_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_48ba_220; else 
            goto addr_465b_219;
        addr_520f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_48ba_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2570();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_522d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2570();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_56a0_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_5106_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_52f7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4653_221;
        addr_5153_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4653_221;
        addr_4897_169:
        goto addr_48a0_65;
        addr_561e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_48ba_220;
        goto addr_522d_222;
        addr_54a4_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_54fe_236;
        fun_2590();
        rsp25 = rsp25 - 8 + 8;
        goto addr_56a0_225;
        addr_54a2_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_54a4_210;
        addr_50de_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_54a4_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_5106_226;
        }
        addr_5611_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4a6d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x88ec + rax113 * 4) + 0x88ec;
    addr_4e99_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x89ec + rax114 * 4) + 0x89ec;
    addr_5513_190:
    addr_465b_219:
    goto 0x4340;
    addr_4544_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x87ec + rax115 * 4) + 0x87ec;
    addr_54fe_236:
    goto v116;
}

void fun_4560() {
}

void fun_4718() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4412;
}

void fun_4771() {
    goto 0x4412;
}

void fun_485e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x46e1;
    }
    if (v2) 
        goto 0x5153;
    if (!r10_3) 
        goto addr_52be_5;
    if (!v4) 
        goto addr_518e_7;
    addr_52be_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_518e_7:
    goto 0x4594;
}

void fun_487c() {
}

void fun_4927() {
    signed char v1;

    if (v1) {
        goto 0x48af;
    } else {
        goto 0x45ea;
    }
}

void fun_4941() {
    signed char v1;

    if (!v1) 
        goto 0x493a; else 
        goto "???";
}

void fun_4968() {
    goto 0x4883;
}

void fun_49e8() {
}

void fun_4a00() {
}

void fun_4a2f() {
    goto 0x4883;
}

void fun_4a81() {
    goto 0x4a10;
}

void fun_4ab0() {
    goto 0x4a10;
}

void fun_4ae3() {
    goto 0x4a10;
}

void fun_4eb0() {
    goto 0x4568;
}

void fun_51ae() {
    signed char v1;

    if (v1) 
        goto 0x5153;
    goto 0x4594;
}

void fun_5255() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4594;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4578;
        goto 0x4594;
    }
}

void fun_5672() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x48e0;
    } else {
        goto 0x4412;
    }
}

void fun_6858() {
    fun_2560();
}

void fun_2980() {
    do_short_format = 1;
    goto 0x2920;
}

void fun_479e() {
    goto 0x4412;
}

void fun_4974() {
    goto 0x492c;
}

void fun_4a3b() {
    goto 0x4568;
}

void fun_4a8d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4a10;
    goto 0x463f;
}

void fun_4abf() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4a1b;
        goto 0x4440;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x48ba;
        goto 0x465b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5258;
    if (r10_8 > r15_9) 
        goto addr_49a5_9;
    addr_49aa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5263;
    goto 0x4594;
    addr_49a5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_49aa_10;
}

void fun_4af2() {
    goto 0x4627;
}

void fun_4ec0() {
    goto 0x4627;
}

void fun_565f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x477c;
    } else {
        goto 0x48e0;
    }
}

void fun_6910() {
}

void fun_2990() {
    include_fullname = 0;
    include_where = 0;
    include_idle = 0;
    goto 0x2920;
}

void fun_4afc() {
    goto 0x4a97;
}

void fun_4eca() {
    goto 0x49ed;
}

void fun_6970() {
    fun_2560();
    goto fun_27e0;
}

void fun_29b0() {
    include_plan = 0;
    goto 0x2920;
}

void fun_47cd() {
    goto 0x4412;
}

void fun_4b08() {
    goto 0x4a97;
}

void fun_4ed7() {
    goto 0x4a3e;
}

void fun_69b0() {
    fun_2560();
    goto fun_27e0;
}

void fun_29c0() {
    do_short_format = 0;
    goto 0x2920;
}

void fun_47fa() {
    goto 0x4412;
}

void fun_4b14() {
    goto 0x4a10;
}

void fun_69f0() {
    fun_2560();
    goto fun_27e0;
}

void fun_29d0() {
    include_fullname = 0;
    include_where = 0;
    goto 0x2920;
}

void fun_481c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x51b0;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x46e1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x46e1;
    }
    if (v11) 
        goto 0x5513;
    if (r10_12 > r15_13) 
        goto addr_5563_8;
    addr_5568_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x52a1;
    addr_5563_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5568_9;
}

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    void** f10;
};

struct s19 {
    signed char[8] pad8;
    void** f8;
};

void fun_6a40() {
    int64_t r15_1;
    struct s17* rbx2;
    void** r14_3;
    struct s18* rbx4;
    void** r13_5;
    struct s19* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    struct s0* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2560();
    fun_27e0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6a62, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_29e8() {
    include_project = 0;
    goto 0x2920;
}

void fun_6a98() {
    fun_2560();
    goto 0x6a69;
}

void fun_29f8() {
    include_heading = 0;
    goto 0x2920;
}

struct s20 {
    signed char[32] pad32;
    int64_t f20;
};

struct s21 {
    signed char[24] pad24;
    int64_t f18;
};

struct s22 {
    signed char[16] pad16;
    void** f10;
};

struct s23 {
    signed char[8] pad8;
    void** f8;
};

struct s24 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6ad0() {
    int64_t rcx1;
    struct s20* rbx2;
    int64_t r15_3;
    struct s21* rbx4;
    void** r14_5;
    struct s22* rbx6;
    void** r13_7;
    struct s23* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s24* rbx12;
    void** rax13;
    struct s0* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2560();
    fun_27e0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6b04, __return_address(), rcx1);
    goto v15;
}

void fun_2a08() {
    include_home_and_shell = 0;
    goto 0x2920;
}

void fun_6b48() {
    fun_2560();
    goto 0x6b0b;
}
