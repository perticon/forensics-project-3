char * create_fullname(char * gecos_name, char * user_name) {
    int64_t v1 = (int64_t)gecos_name;
    int64_t v2 = function_2580(); // 0x2e90
    int64_t v3 = v1; // 0x2e9c
    int64_t v4 = 0; // 0x2e9c
    int64_t v5; // 0x2e80
    while (true) {
        // 0x2ea0
        v5 = v4;
        char v6 = *(char *)v3; // 0x2ea0
        v3++;
        while (v6 != 38) {
            // 0x2eb0
            if (v6 == 0) {
                // break (via goto) -> 0x2eb4
                goto lab_0x2eb4;
            }
            v6 = *(char *)v3;
            v3++;
        }
        // 0x2f78
        v4 = v5 + 1;
    }
  lab_0x2eb4:
    if (v5 != 0) {
        uint128_t v7 = (int128_t)function_2580() * (int128_t)(v5 - 1); // 0x2ec5
        if (v7 > 0xffffffffffffffff) {
            // 0x2f81
            xalloc_die();
            return (char *)&g31;
        }
        if (-2 - v2 < (int64_t)v7) {
            // 0x2f81
            xalloc_die();
            return (char *)&g31;
        }
    }
    // 0x2ed7
    int64_t v8; // 0x2e80
    char v9 = v8;
    int64_t v10 = xmalloc(); // 0x2eda
    if (v9 == 0) {
        // 0x2f66
        *(char *)v10 = 0;
        return (char *)v10;
    }
    int64_t v11 = (int64_t)user_name;
    int64_t v12 = v11 + 1;
    char v13 = v9; // 0x2e80
    int64_t v14 = v10; // 0x2e80
    int64_t v15 = v1; // 0x2e80
    int64_t v16; // 0x2e80
    int64_t v17; // 0x2e80
    char v18; // 0x2e80
    int64_t v19; // 0x2e80
    int64_t v20; // 0x2e80
    int64_t v21; // 0x2e80
    while (true) {
      lab_0x2f03_2:
        // 0x2f03
        v17 = v15;
        int64_t v22 = v14;
        char v23 = v13;
        if (v23 != 38) {
            // 0x2ef0
            *(char *)v22 = v23;
            v19 = v22 + 1;
            goto lab_0x2ef6;
        } else {
            int64_t v24 = function_2830(); // 0x2f07
            int64_t v25; // 0x2e80
            int64_t v26 = (int64_t)*(char *)&v25; // 0x2f0c
            int64_t v27 = v26; // 0x2f23
            int64_t v28 = v11; // 0x2f23
            int64_t v29 = v22; // 0x2f23
            if ((*(char *)(*(int64_t *)v24 + 1 + 2 * v26) & 2) != 0) {
                int64_t v30 = *(int64_t *)function_2450(); // 0x2f32
                *(char *)v22 = (char)*(int32_t *)(v30 + 4 * v26);
                v27 = (int64_t)*(char *)v12;
                v28 = v12;
                v29 = v22 + 1;
            }
            // 0x2f40
            v19 = v29;
            int64_t v31 = v27; // 0x2f42
            int64_t v32 = v28; // 0x2f42
            int64_t v33 = v29; // 0x2f42
            if (v27 == 0) {
                goto lab_0x2ef6;
            } else {
                int64_t v34 = v32 + 1; // 0x2f48
                *(char *)v33 = (char)v31;
                int64_t v35 = v33 + 1; // 0x2f4e
                unsigned char v36 = *(char *)v34; // 0x2f52
                v32 = v34;
                v33 = v35;
                while (v36 != 0) {
                    // 0x2f48
                    v34 = v32 + 1;
                    *(char *)v33 = (char)(int64_t)v36;
                    v35 = v33 + 1;
                    v36 = *(char *)v34;
                    v32 = v34;
                    v33 = v35;
                }
                int64_t v37 = v17 + 1; // 0x2f59
                char v38 = *(char *)v37; // 0x2f59
                v18 = v38;
                v20 = v35;
                v16 = v37;
                v21 = v35;
                if (v38 == 0) {
                    // break -> 0x2f66
                    break;
                }
                goto lab_0x2f03;
            }
        }
    }
  lab_0x2f66:
    // 0x2f66
    *(char *)v21 = 0;
    return (char *)v10;
  lab_0x2ef6:;
    int64_t v39 = v17 + 1; // 0x2ef6
    char v40 = *(char *)v39; // 0x2ef6
    v18 = v40;
    v20 = v19;
    v16 = v39;
    v21 = v19;
    if (v40 == 0) {
        // break -> 0x2f66
        goto lab_0x2f66;
    }
    goto lab_0x2f03;
  lab_0x2f03:
    // 0x2f03
    v13 = v18;
    v14 = v20;
    v15 = v16;
    goto lab_0x2f03_2;
}