create_fullname (char const *gecos_name, char const *user_name)
{
  size_t rsize = strlen (gecos_name) + 1;
  char *result;
  char *r;
  size_t ampersands = count_ampersands (gecos_name);

  if (ampersands != 0)
    {
      size_t ulen = strlen (user_name);
      size_t product;
      if (INT_MULTIPLY_WRAPV (ulen, ampersands - 1, &product)
          || INT_ADD_WRAPV (rsize, product, &rsize))
        xalloc_die ();
    }

  r = result = xmalloc (rsize);

  while (*gecos_name)
    {
      if (*gecos_name == '&')
        {
          char const *uname = user_name;
          if (islower (to_uchar (*uname)))
            *r++ = toupper (to_uchar (*uname++));
          while (*uname)
            *r++ = *uname++;
        }
      else
        {
          *r++ = *gecos_name;
        }

      gecos_name++;
    }
  *r = 0;

  return result;
}