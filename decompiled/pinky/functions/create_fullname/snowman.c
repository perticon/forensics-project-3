void** create_fullname(void** rdi, void** rsi, ...) {
    void** r13_3;
    void** rbp4;
    int64_t v5;
    int64_t rbx6;
    void* rbx7;
    void** rax8;
    void* rsp9;
    void** rdx10;
    void** r12_11;
    uint32_t ecx12;
    void** rax13;
    void** tmp64_14;
    int1_t cf15;
    void** rdi16;
    void** rax17;
    void** r12_18;
    uint32_t eax19;
    void** rbx20;
    void** rax21;
    int64_t r14_22;
    int64_t rax23;
    int32_t** rax24;
    int32_t eax25;
    void** rax26;
    void** v27;
    void** rax28;
    void** rbp29;
    void** rax30;
    void** rax31;
    void* rsp32;
    struct s0* rdi33;
    void* rsp34;
    void* rax35;
    struct s0* rbp36;
    void* rsp37;
    uint32_t edx38;
    void** rax39;
    void** v40;
    void* rdi41;
    void*** r12_42;
    void*** rsi43;
    void*** rax44;
    int32_t eax45;
    uint32_t r13d46;
    int64_t r14_47;
    int64_t v48;
    uint32_t v49;
    unsigned char* rbx50;
    void* rsp51;
    int1_t zf52;
    void*** r8_53;
    int64_t rcx54;
    void** rdx55;
    void** rsi56;
    void* rsp57;
    int1_t zf58;
    int64_t rcx59;
    void*** rdx60;
    int64_t r9_61;
    uint64_t rdx62;
    int64_t r9_63;
    void** rax64;
    void** r15_65;
    unsigned char* rcx66;
    uint32_t edx67;
    void** rax68;
    void* rsp69;
    int64_t v70;
    int64_t rax71;
    void* rsp72;
    void** rax73;
    void* rsp74;
    void** rdx75;
    void** rdx76;
    int1_t zf77;
    uint32_t edx78;
    struct s0* rdi79;
    void** rax80;
    void** r15_81;
    unsigned char* rax82;
    unsigned char* rbp83;
    signed char* rax84;
    void* rax85;
    int64_t v86;
    signed char v87;
    void** rax88;
    void** rbp89;
    void** rdx90;
    void** rsi91;
    signed char v92;
    void** rax93;
    void** rax94;
    void** r15_95;
    signed char* rax96;
    void** rsi97;
    void** rax98;
    void** r12_99;
    signed char* rax100;
    void** rsi101;
    void** rax102;
    void** rsi103;
    void** rdx104;
    struct s0* rdi105;
    void** rax106;
    int1_t zf107;
    void** rax108;
    void** rdx109;
    void** rax110;
    struct s0* rdi111;
    void** rax112;
    int1_t zf113;
    int1_t zf114;
    void** rdi115;
    void** rax116;
    void** rax117;
    void** rsi118;
    struct s1* rax119;
    void** rax120;
    void* rsp121;
    void** r12_122;
    void* rbx123;
    void** rax124;
    void* rsp125;
    void** rax126;
    void* rsp127;
    struct s0* rcx128;
    int1_t zf129;
    int1_t zf130;
    void** rax131;
    void* rax132;
    void* rax133;
    void** rdi134;
    void** rax135;
    void** rdi136;
    void** rax137;
    void** rsi138;
    struct s1* rax139;
    void** rdx140;
    void** rsi141;
    void** rax142;
    void* rsp143;
    void** rax144;
    void* rsp145;
    void** rax146;
    void* rsp147;
    struct s0* rcx148;

    r13_3 = rdi;
    rbp4 = rsi;
    v5 = rbx6;
    *reinterpret_cast<int32_t*>(&rbx7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
    rax8 = fun_2580(rdi);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 + 8);
    rdx10 = r13_3;
    r12_11 = rax8 + 1;
    while (1) {
        ecx12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10));
        ++rdx10;
        if (*reinterpret_cast<signed char*>(&ecx12) == 38) {
            rbx7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbx7) + 1);
        } else {
            if (!*reinterpret_cast<signed char*>(&ecx12)) 
                break;
        }
    }
    if (!rbx7 || (rax13 = fun_2580(rbp4), rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8), rdx10 = __intrinsic(), !__intrinsic()) && (tmp64_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_11) + reinterpret_cast<unsigned char>(rax13) * (reinterpret_cast<int64_t>(rbx7) - 1)), cf15 = reinterpret_cast<unsigned char>(tmp64_14) < reinterpret_cast<unsigned char>(r12_11), r12_11 = tmp64_14, !cf15)) {
        rdi16 = r12_11;
        rax17 = xmalloc(rdi16, rsi, rdx10);
        r12_18 = rax17;
        eax19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_3));
        rbx20 = r12_18;
        if (*reinterpret_cast<void***>(&eax19)) {
            do {
                addr_2f03_7:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax19) == 38)) {
                    *reinterpret_cast<void***>(rbx20) = *reinterpret_cast<void***>(&eax19);
                    ++rbx20;
                } else {
                    rax21 = fun_2830(rdi16, rsi, rdx10);
                    *reinterpret_cast<uint32_t*>(&r14_22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_22) + 4) = 0;
                    rdx10 = rbp4;
                    rax23 = r14_22;
                    if (*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax21) + r14_22 * 2 + 1) & 2) {
                        rax24 = fun_2450(rdi16);
                        ++rbx20;
                        rdx10 = rbp4 + 1;
                        eax25 = (*rax24)[r14_22];
                        *reinterpret_cast<signed char*>(rbx20 + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&eax25);
                        *reinterpret_cast<uint32_t*>(&rax23) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 1));
                    }
                    if (*reinterpret_cast<void***>(&rax23)) {
                        do {
                            ++rdx10;
                            *reinterpret_cast<void***>(rbx20) = *reinterpret_cast<void***>(&rax23);
                            ++rbx20;
                            *reinterpret_cast<uint32_t*>(&rax23) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10));
                        } while (*reinterpret_cast<void***>(&rax23));
                        eax19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_3 + 1));
                        ++r13_3;
                        if (*reinterpret_cast<void***>(&eax19)) 
                            goto addr_2f03_7; else 
                            break;
                    }
                }
                eax19 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_3 + 1));
                ++r13_3;
            } while (*reinterpret_cast<void***>(&eax19));
        }
        *reinterpret_cast<void***>(rbx20) = reinterpret_cast<void**>(0);
        return r12_18;
    }
    xalloc_die();
    rax26 = g28;
    v27 = rax26;
    rax28 = fun_2670(rbp4, rsi, rbp4, rsi);
    rbp29 = rax28;
    rax30 = fun_2560();
    fun_2750(1, rax30, 5);
    fun_2750(1, "%-28s", rbp4);
    rax31 = fun_2560();
    fun_2750(1, rax31, 5);
    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 - 8 - 8 - 8 - 0x418 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    if (rbp29) 
        goto addr_301e_20;
    *reinterpret_cast<int32_t*>(&rdi33) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0;
    fun_2560();
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
    rax35 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(g28));
    if (!rax35) {
    }
    fun_2590();
    rbp36 = rdi33;
    rsp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x1e8);
    edx38 = reinterpret_cast<unsigned char>(rdi33->f8);
    rax39 = g28;
    v40 = rax39;
    if (*reinterpret_cast<signed char*>(&edx38) != 47) {
        rdi41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp37) + 0xa0);
    } else {
        rdi41 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp37) + 0xa0);
    }
    r12_42 = &rbp36->f8;
    rsi43 = &rbp36->f28;
    rax44 = r12_42;
    while (*reinterpret_cast<signed char*>(&edx38) && (++rax44, reinterpret_cast<uint64_t>(rsi43) > reinterpret_cast<uint64_t>(rax44))) {
        edx38 = reinterpret_cast<unsigned char>(*rax44);
    }
    eax45 = fun_2690(rdi41, reinterpret_cast<uint64_t>(rsp37) + 16);
    if (eax45) {
        r13d46 = 63;
        *reinterpret_cast<int32_t*>(&r14_47) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_47) + 4) = 0;
    } else {
        r14_47 = v48;
        r13d46 = (*reinterpret_cast<uint32_t*>(&r13_3) - (*reinterpret_cast<uint32_t*>(&r13_3) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_3) < *reinterpret_cast<uint32_t*>(&r13_3) + reinterpret_cast<uint1_t>((v49 & 16) < 1))) & 10) + 32;
    }
    rbx50 = &rbp36->f2c;
    fun_2750(1, "%-8.*s", 32, 1, "%-8.*s", 32);
    rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp37) - 8 + 8 - 8 + 8);
    zf52 = include_fullname == 0;
    if (zf52) {
        addr_351f_35:
        r8_53 = r12_42;
        *reinterpret_cast<int32_t*>(&rcx54) = 32;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx54) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx55) = r13d46;
        *reinterpret_cast<int32_t*>(&rdx55 + 4) = 0;
        rsi56 = reinterpret_cast<void**>(" %c%-8.*s");
        fun_2750(1, " %c%-8.*s", rdx55, 1, " %c%-8.*s", rdx55);
        rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp51) - 8 + 8);
        zf58 = include_idle == 0;
        if (!zf58) {
            if (r14_47) {
                rcx59 = now_2;
                if (!rcx59) {
                    fun_26c0(0xc128, " %c%-8.*s", rdx55);
                    rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
                    rcx59 = now_2;
                }
                rcx54 = rcx59 - r14_47;
                rdx55 = reinterpret_cast<void**>("     ");
                if (rcx54 > 59) {
                    if (rcx54 > 0x1517f) {
                        rdx60 = reinterpret_cast<void***>((__intrinsic() >> 13) - reinterpret_cast<uint64_t>(rcx54 >> 63));
                        rcx54 = reinterpret_cast<int64_t>("%lud");
                        r8_53 = rdx60;
                        fun_2850(0xc110, 1, 22, "%lud", r8_53, r9_61);
                        rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
                        rdx55 = reinterpret_cast<void**>(0xc110);
                    } else {
                        r8_53 = reinterpret_cast<void***>((__intrinsic() >> 10) - reinterpret_cast<uint64_t>(rcx54 >> 63));
                        rcx54 = reinterpret_cast<int64_t>("%02d:%02d");
                        rdx62 = __intrinsic() >> 5;
                        *reinterpret_cast<int32_t*>(&r9_63) = *reinterpret_cast<int32_t*>(&rdx62);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_63) + 4) = 0;
                        fun_2850(0xc110, 1, 22, "%02d:%02d", r8_53, r9_63);
                        rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
                        rdx55 = reinterpret_cast<void**>(0xc110);
                    }
                }
            } else {
                rax64 = fun_2560();
                rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
                rdx55 = rax64;
            }
            rsi56 = reinterpret_cast<void**>(" %-6s");
            fun_2750(1, " %-6s", rdx55, 1, " %-6s", rdx55);
            rsp57 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
        }
    } else {
        r15_65 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp51) + 0xd0);
        rcx66 = &rbp36->f4c;
        do {
            edx67 = *rbx50;
            if (!*reinterpret_cast<signed char*>(&edx67)) 
                break;
            ++rbx50;
        } while (reinterpret_cast<uint64_t>(rcx66) > reinterpret_cast<uint64_t>(rbx50));
        rax68 = fun_2670(r15_65, "%-8.*s");
        rsp69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp51) - 8 + 8);
        if (!rax68) 
            goto addr_37c9_49; else 
            goto addr_34d6_50;
    }
    v70 = rbp36->f154;
    rax71 = fun_2480(reinterpret_cast<uint64_t>(rsp57) + 8, rsi56, rdx55, rcx54, r8_53, r9_63);
    rsp72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp57) - 8 + 8);
    if (!rax71) {
        rax73 = imaxtostr(v70, 0xc0e0, rdx55, rax71, r8_53, r9_63);
        rsp74 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp72) - 8 + 8);
        rdx75 = rax73;
    } else {
        rdx76 = time_format;
        fun_2760(0xc0e0, 33, rdx76, rax71, r8_53, r9_63);
        rsp74 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp72) - 8 + 8);
        rdx75 = reinterpret_cast<void**>(0xc0e0);
    }
    fun_2750(1, " %s", rdx75, 1, " %s", rdx75);
    zf77 = include_where == 0;
    if (zf77 || (edx78 = rbp36->f4c, !*reinterpret_cast<signed char*>(&edx78))) {
        addr_35df_55:
        rdi79 = stdout;
        rax80 = rdi79->f28;
        if (reinterpret_cast<unsigned char>(rax80) >= reinterpret_cast<unsigned char>(rdi79->f30)) {
            fun_25e0();
        } else {
            rdi79->f28 = rax80 + 1;
            *reinterpret_cast<void***>(rax80) = reinterpret_cast<void**>(10);
        }
    } else {
        r15_81 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp74) - 8 + 8 + 0xd0);
        rax82 = &rbp36->f4c;
        rbp83 = &rbp36->f14c;
        do {
            ++rax82;
            if (reinterpret_cast<uint64_t>(rbp83) <= reinterpret_cast<uint64_t>(rax82)) 
                break;
            edx78 = *rax82;
        } while (*reinterpret_cast<signed char*>(&edx78));
        rax84 = fun_25c0(r15_81, 58);
        if (!rax84) 
            goto addr_380f_62; else 
            goto addr_3690_63;
    }
    rax85 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v40) - reinterpret_cast<unsigned char>(g28));
    if (rax85) {
        fun_2590();
    } else {
        goto v86;
    }
    addr_380f_62:
    if (v87) {
        rax88 = canon_host(r15_81, 58);
        rbp89 = rax88;
        if (rax88) {
            addr_3820_69:
            rdx90 = rbp89;
            rsi91 = reinterpret_cast<void**>(" %s");
            fun_2750(1, " %s", rdx90, 1, " %s", rdx90);
        } else {
            goto addr_381d_71;
        }
    } else {
        addr_381d_71:
        rbp89 = r15_81;
        goto addr_3820_69;
    }
    addr_36c1_72:
    if (rbp89 != r15_81) {
        fun_2470(rbp89, rsi91, rdx90, rbp89, rsi91, rdx90);
        goto addr_35df_55;
    }
    addr_3690_63:
    *rax84 = 0;
    if (v92) {
        rax93 = canon_host(r15_81, 58);
        rbp89 = rax93;
        if (rax93) {
            addr_36a8_75:
            rdx90 = rbp89;
            rsi91 = reinterpret_cast<void**>(" %s:%s");
            fun_2750(1, " %s:%s", rdx90, 1, " %s:%s", rdx90);
            goto addr_36c1_72;
        } else {
            goto addr_36a5_77;
        }
    } else {
        addr_36a5_77:
        rbp89 = r15_81;
        goto addr_36a8_75;
    }
    addr_37c9_49:
    rax94 = fun_2560();
    fun_2750(1, " %19s", rax94, 1, " %19s", rax94);
    rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp69) - 8 + 8 - 8 + 8);
    goto addr_351f_35;
    addr_34d6_50:
    r15_95 = *reinterpret_cast<void***>(rax68 + 24);
    rax96 = fun_25c0(r15_95, 44);
    if (rax96) {
        *rax96 = 0;
        r15_95 = *reinterpret_cast<void***>(rax68 + 24);
    }
    rsi97 = *reinterpret_cast<void***>(rax68);
    rax98 = create_fullname(r15_95, rsi97);
    fun_2750(1, " %-19.19s", rax98, 1, " %-19.19s", rax98);
    fun_2470(rax98, " %-19.19s", rax98);
    rsp51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp69) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_351f_35;
    addr_301e_20:
    r12_99 = *reinterpret_cast<void***>(rbp29 + 24);
    rax100 = fun_25c0(r12_99, 44, r12_99, 44);
    if (rax100) {
        *rax100 = 0;
        r12_99 = *reinterpret_cast<void***>(rbp29 + 24);
    }
    rsi101 = *reinterpret_cast<void***>(rbp29);
    r13_3 = reinterpret_cast<void**>(" %s");
    rax102 = create_fullname(r12_99, rsi101, r12_99, rsi101);
    rsi103 = reinterpret_cast<void**>(" %s");
    rdx104 = rax102;
    fun_2750(1, " %s", rdx104);
    fun_2470(rax102, " %s", rdx104);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    rdi105 = stdout;
    rax106 = rdi105->f28;
    if (reinterpret_cast<unsigned char>(rax106) >= reinterpret_cast<unsigned char>(rdi105->f30)) {
        *reinterpret_cast<int32_t*>(&rsi103) = 10;
        *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
        fun_25e0();
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8);
    } else {
        rdx104 = rax106 + 1;
        rdi105->f28 = rdx104;
        *reinterpret_cast<void***>(rax106) = reinterpret_cast<void**>(10);
    }
    zf107 = include_home_and_shell == 0;
    if (!zf107) {
        rax108 = fun_2560();
        fun_2750(1, rax108, 5);
        rdx109 = *reinterpret_cast<void***>(rbp29 + 32);
        fun_2750(1, "%-29s", rdx109);
        rax110 = fun_2560();
        fun_2750(1, rax110, 5);
        rdx104 = *reinterpret_cast<void***>(rbp29 + 40);
        rsi103 = reinterpret_cast<void**>(" %s");
        fun_2750(1, " %s", rdx104);
        rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rdi111 = stdout;
        rax112 = rdi111->f28;
        if (reinterpret_cast<unsigned char>(rax112) >= reinterpret_cast<unsigned char>(rdi111->f30)) {
            *reinterpret_cast<int32_t*>(&rsi103) = 10;
            *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0;
            fun_25e0();
            rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8);
        } else {
            rdx104 = rax112 + 1;
            zf113 = include_project == 0;
            rdi111->f28 = rdx104;
            *reinterpret_cast<void***>(rax112) = reinterpret_cast<void**>(10);
            if (zf113) 
                goto addr_30a1_88; else 
                goto addr_3193_89;
        }
    }
    zf114 = include_project == 0;
    if (!zf114) {
        addr_3193_89:
        rdi115 = *reinterpret_cast<void***>(rbp29 + 32);
        rax116 = fun_2580(rdi115, rdi115);
        rax117 = xmalloc(rax116 + 10, rsi103, rdx104);
        rsi118 = *reinterpret_cast<void***>(rbp29 + 32);
        r13_3 = rax117;
        rax119 = fun_2550(rax117, rsi118, rdx104);
        rsi103 = reinterpret_cast<void**>("r");
        rax119->f0 = reinterpret_cast<int32_t>(0x63656a6f72702e2f);
        rax119->f8 = 0x74;
        rax120 = fun_27a0(r13_3, "r", r13_3, "r");
        rsp121 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        r12_122 = rax120;
        if (rax120) {
            rbx123 = rsp121;
            rax124 = fun_2560();
            fun_2750(1, rax124, 5);
            rsp125 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp121) - 8 + 8 - 8 + 8);
            while (*reinterpret_cast<int32_t*>(&rsi103) = 1, *reinterpret_cast<int32_t*>(&rsi103 + 4) = 0, rax126 = fun_2500(rbx123, 1, 0x400, r12_122), rsp127 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp125) - 8 + 8), rdx104 = rax126, !!rax126) {
                rcx128 = stdout;
                fun_2720(rbx123, 1, rdx104, rcx128);
                rsp125 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp127) - 8 + 8);
            }
            rpl_fclose(r12_122, 1, rdx104, r12_122);
            rsp121 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp127) - 8 + 8);
        }
    } else {
        addr_30a1_88:
        zf129 = include_plan == 0;
        if (!zf129) 
            goto addr_325e_95; else 
            goto addr_30ae_96;
    }
    fun_2470(r13_3, rsi103, rdx104, r13_3, rsi103, rdx104);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp121) - 8 + 8);
    zf130 = include_plan == 0;
    if (zf130) {
        addr_30ae_96:
        rdi33 = stdout;
        rax131 = rdi33->f28;
        if (reinterpret_cast<unsigned char>(rax131) >= reinterpret_cast<unsigned char>(rdi33->f30)) {
            rax132 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(g28));
            if (!rax132) {
            }
        } else {
            rdi33->f28 = rax131 + 1;
            *reinterpret_cast<void***>(rax131) = reinterpret_cast<void**>(10);
            rax133 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v27) - reinterpret_cast<unsigned char>(g28));
            if (!rax133) {
                goto v5;
            }
        }
    } else {
        addr_325e_95:
        rdi134 = *reinterpret_cast<void***>(rbp29 + 32);
        rax135 = fun_2580(rdi134, rdi134);
        rdi136 = rax135 + 7;
        rax137 = xmalloc(rdi136, rsi103, rdx104, rdi136, rsi103, rdx104);
        rsi138 = *reinterpret_cast<void***>(rbp29 + 32);
        r12_122 = rax137;
        rax139 = fun_2550(rax137, rsi138, rdx104, rax137, rsi138, rdx104);
        *reinterpret_cast<int32_t*>(&rdx140) = 0x6e61;
        *reinterpret_cast<int32_t*>(&rdx140 + 4) = 0;
        rsi141 = reinterpret_cast<void**>("r");
        rax139->f0 = 0x6c702e2f;
        rax139->f4 = 0x6e61;
        rax139->f6 = 0;
        rax142 = fun_27a0(r12_122, "r", r12_122, "r");
        rsp143 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp34) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        rbp29 = rax142;
        if (rax142) {
            rbx123 = rsp143;
            rax144 = fun_2560();
            fun_2750(1, rax144, 5, 1, rax144, 5);
            rsp145 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp143) - 8 + 8 - 8 + 8);
            while (*reinterpret_cast<int32_t*>(&rsi141) = 1, *reinterpret_cast<int32_t*>(&rsi141 + 4) = 0, rax146 = fun_2500(rbx123, 1, 0x400, rbp29), rsp147 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp145) - 8 + 8), rdx140 = rax146, !!rax146) {
                rcx148 = stdout;
                fun_2720(rbx123, 1, rdx140, rcx148);
                rsp145 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp147) - 8 + 8);
            }
            rpl_fclose(rbp29, 1, rdx140, rbp29);
            rsp143 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp147) - 8 + 8);
        }
    }
    fun_2470(r12_122, rsi141, rdx140, r12_122, rsi141, rdx140);
    rsp34 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp143) - 8 + 8);
    goto addr_30ae_96;
}