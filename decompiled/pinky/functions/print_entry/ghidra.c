void print_entry(long param_1)

{
  char cVar1;
  int iVar2;
  passwd *ppVar3;
  char *pcVar4;
  void *__ptr;
  char *pcVar5;
  tm *__tp;
  undefined8 uVar6;
  undefined4 *puVar7;
  undefined4 *puVar8;
  char *pcVar9;
  char cVar10;
  undefined1 *puVar11;
  long in_FS_OFFSET;
  long local_210;
  stat local_208;
  undefined4 local_178;
  undefined local_174 [44];
  char local_148 [264];
  long local_40;
  
  cVar10 = *(char *)(param_1 + 8);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  if (cVar10 == '/') {
    puVar8 = &local_178;
  }
  else {
    puVar8 = (undefined4 *)(local_174 + 1);
    local_178 = 0x7665642f;
    local_174._0_2_ = 0x2f;
  }
  pcVar5 = (char *)(param_1 + 8);
  while (puVar7 = puVar8, cVar10 != '\0') {
    pcVar5 = pcVar5 + 1;
    puVar7 = (undefined4 *)((long)puVar8 + 1);
    *(char *)puVar8 = cVar10;
    if ((char *)(param_1 + 0x28U) <= pcVar5) break;
    puVar8 = puVar7;
    cVar10 = *pcVar5;
  }
  *(char *)puVar7 = '\0';
  iVar2 = stat((char *)&local_178,&local_208);
  if (iVar2 == 0) {
    cVar10 = (-((local_208.st_mode & 0x10) == 0) & 10U) + 0x20;
  }
  else {
    cVar10 = '?';
    local_208.st_atim.tv_sec = 0;
  }
  pcVar5 = (char *)(param_1 + 0x2c);
  __printf_chk(1,"%-8.*s",0x20,pcVar5);
  if (include_fullname != '\0') {
    pcVar4 = local_148;
    do {
      cVar1 = *pcVar5;
      pcVar9 = pcVar4;
      if (cVar1 == '\0') break;
      pcVar5 = pcVar5 + 1;
      pcVar9 = pcVar4 + 1;
      *pcVar4 = cVar1;
      pcVar4 = pcVar9;
    } while (pcVar5 < (char *)(param_1 + 0x4cU));
    *pcVar9 = '\0';
    ppVar3 = getpwnam(local_148);
    if (ppVar3 == (passwd *)0x0) {
      uVar6 = dcgettext(0,"        ???",5);
      __printf_chk(1," %19s",uVar6);
    }
    else {
      pcVar5 = ppVar3->pw_gecos;
      pcVar4 = strchr(pcVar5,0x2c);
      if (pcVar4 != (char *)0x0) {
        *pcVar4 = '\0';
        pcVar5 = ppVar3->pw_gecos;
      }
      __ptr = (void *)create_fullname(pcVar5,ppVar3->pw_name);
      __printf_chk(1," %-19.19s",__ptr);
      free(__ptr);
    }
  }
  __printf_chk(1,&DAT_00108078,cVar10,0x20,(char *)(param_1 + 8));
  if (include_idle != '\0') {
    if (local_208.st_atim.tv_sec == 0) {
      pcVar5 = (char *)dcgettext(0,"?????",5);
    }
    else {
      if (now_2 == 0) {
        time(&now_2);
      }
      local_208.st_atim.tv_sec = now_2 - local_208.st_atim.tv_sec;
      pcVar5 = "     ";
      if (0x3b < local_208.st_atim.tv_sec) {
        if (local_208.st_atim.tv_sec < 0x15180) {
          __sprintf_chk(buf_1,1,0x16,"%02d:%02d",local_208.st_atim.tv_sec / 0xe10,
                        (ulong)(local_208.st_atim.tv_sec % 0xe10) / 0x3c & 0xffffffff);
          pcVar5 = buf_1;
        }
        else {
          __sprintf_chk(buf_1,1,0x16,&DAT_0010808c,local_208.st_atim.tv_sec / 0x15180);
          pcVar5 = buf_1;
        }
      }
    }
    __printf_chk(1," %-6s",pcVar5);
  }
  local_210 = (long)*(int *)(param_1 + 0x154);
  __tp = localtime(&local_210);
  if (__tp == (tm *)0x0) {
    puVar11 = (undefined1 *)imaxtostr(local_210,buf_0);
  }
  else {
    strftime(buf_0,0x21,time_format,__tp);
    puVar11 = buf_0;
  }
  __printf_chk(1," %s",puVar11);
  if ((include_where != '\0') && (cVar10 = *(char *)(param_1 + 0x4c), cVar10 != '\0')) {
    pcVar5 = (char *)(param_1 + 0x4c);
    pcVar4 = local_148;
    do {
      pcVar5 = pcVar5 + 1;
      pcVar9 = pcVar4 + 1;
      *pcVar4 = cVar10;
      if ((char *)(param_1 + 0x14c) <= pcVar5) break;
      cVar10 = *pcVar5;
      pcVar4 = pcVar9;
    } while (cVar10 != '\0');
    *pcVar9 = '\0';
    pcVar5 = strchr(local_148,0x3a);
    if (pcVar5 == (char *)0x0) {
      if ((local_148[0] == '\0') || (pcVar4 = (char *)canon_host(local_148), pcVar4 == (char *)0x0))
      {
        pcVar4 = local_148;
      }
      __printf_chk(1," %s",pcVar4);
    }
    else {
      *pcVar5 = '\0';
      if ((local_148[0] == '\0') || (pcVar4 = (char *)canon_host(local_148), pcVar4 == (char *)0x0))
      {
        pcVar4 = local_148;
      }
      __printf_chk(1," %s:%s",pcVar4,pcVar5 + 1);
    }
    if (pcVar4 != local_148) {
      free(pcVar4);
    }
  }
  pcVar5 = stdout->_IO_write_ptr;
  if (pcVar5 < stdout->_IO_write_end) {
    stdout->_IO_write_ptr = pcVar5 + 1;
    *pcVar5 = '\n';
  }
  else {
    __overflow(stdout,10);
  }
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}