void print_entry(struct utmpx * utmp_ent) {
    int64_t v1 = (int64_t)utmp_ent;
    int64_t v2 = v1 + 8; // 0x33e4
    char v3 = *(char *)v2; // 0x33e4
    int64_t v4 = __readfsqword(40); // 0x33e8
    int64_t v5; // bp-376, 0x33d0
    int64_t * v6 = &v5; // 0x33fe
    if (v3 != 47) {
        // 0x3780
        v5 = 0x7665642f;
        int64_t v7; // bp-371, 0x33d0
        v6 = &v7;
    }
    int64_t v8 = (int64_t)v6;
    int64_t v9 = v2; // 0x3435
    int64_t v10 = v8; // 0x3435
    if (v3 != 0) {
        v9++;
        int64_t v11 = v8 + 1; // 0x3424
        *(char *)v8 = v3;
        v10 = v11;
        while (v1 + 40 > v9) {
            char v12 = *(char *)v9; // 0x3430
            v10 = v11;
            if (v12 == 0) {
                // break -> 0x3437
                break;
            }
            int64_t v13 = v11;
            v9++;
            v11 = v13 + 1;
            *(char *)v13 = v12;
            v10 = v11;
        }
    }
    // 0x3437
    *(char *)v10 = 0;
    int64_t v14 = function_2690(); // 0x343f
    function_2750();
    char v15; // bp-328, 0x33d0
    if (*(char *)&include_fullname != 0) {
        int64_t v16 = v1 + 44; // 0x34a1
        int64_t v17 = &v15;
        char v18 = *(char *)v16; // 0x34b8
        int64_t v19 = v17; // 0x34bd
        while (v18 != 0) {
            // 0x34a8
            v16++;
            int64_t v20 = v17 + 1; // 0x34ac
            *(char *)v17 = v18;
            v19 = v20;
            if (v1 + 76 <= v16) {
                // break -> 0x34bf
                break;
            }
            v17 = v20;
            v18 = *(char *)v16;
            v19 = v17;
        }
        // 0x34bf
        *(char *)v19 = 0;
        int64_t v21 = function_2670(); // 0x34c5
        if (v21 == 0) {
            // 0x37c9
            function_2560();
            function_2750();
        } else {
            int64_t * v22 = (int64_t *)(v21 + 24); // 0x34d6
            int64_t v23 = function_25c0(); // 0x34e2
            int64_t v24 = *v22; // 0x34ea
            if (v23 != 0) {
                // 0x34ec
                *(char *)v23 = 0;
                v24 = *v22;
            }
            // 0x34f3
            create_fullname((char *)v24, (char *)*(int64_t *)v21);
            function_2750();
            function_2470();
        }
    }
    // 0x351f
    function_2750();
    if (*(char *)&include_idle != 0) {
        // 0x3546
        int64_t v25; // 0x33d0
        int64_t v26 = (int32_t)v14 != 0 ? 0 : v25;
        if (v26 != 0) {
            // 0x36e0
            if (g26 == 0) {
                // 0x37f7
                function_26c0();
            }
            // 0x36f0
            if (g26 - v26 >= 60) {
                // 0x3704
                function_2850();
            }
        } else {
            // 0x354f
            function_2560();
        }
        // 0x3565
        function_2750();
    }
    // 0x3578
    if (function_2480() == 0) {
        // 0x37b0
        imaxtostr((int64_t)*(int32_t *)(v1 + 340), (char *)&g25);
    } else {
        // 0x359a
        function_2760();
    }
    // 0x35b8
    function_2750();
    int64_t v27; // 0x33d0
    int64_t v28; // 0x33d0
    int64_t v29; // 0x3640
    if (*(char *)&include_where == 0) {
        goto lab_0x35df;
    } else {
        int64_t v30 = v1 + 76; // 0x35d7
        char v31 = *(char *)v30; // 0x35d7
        if (v31 != 0) {
            // 0x3640
            v29 = &v15;
            int64_t v32 = v30; // 0x3656
            v32++;
            int64_t v33 = v29 + 1; // 0x3664
            *(char *)v29 = v31;
            while (v1 + 332 > v32) {
                char v34 = *(char *)v32; // 0x3670
                int64_t v35 = v33; // 0x3675
                if (v34 == 0) {
                    // break -> 0x3677
                    break;
                }
                v32++;
                v33 = v35 + 1;
                *(char *)v35 = v34;
            }
            // 0x3677
            *(char *)v33 = 0;
            int64_t v36 = function_25c0(); // 0x3682
            if (v36 == 0) {
                // 0x380f
                if (v15 != 0) {
                    char * v37 = canon_host(&v15); // 0x38b3
                    v28 = (int64_t)v37;
                    if (v37 != NULL) {
                        goto lab_0x3820;
                    } else {
                        // 0x381d
                        v28 = v29;
                        goto lab_0x3820;
                    }
                } else {
                    // 0x381d
                    v28 = v29;
                    goto lab_0x3820;
                }
            } else {
                // 0x3690
                *(char *)v36 = 0;
                if (v15 != 0) {
                    char * v38 = canon_host(&v15); // 0x389a
                    v27 = (int64_t)v38;
                    if (v38 != NULL) {
                        goto lab_0x36a8;
                    } else {
                        // 0x36a5
                        v27 = v29;
                        goto lab_0x36a8;
                    }
                } else {
                    // 0x36a5
                    v27 = v29;
                    goto lab_0x36a8;
                }
            }
        } else {
            goto lab_0x35df;
        }
    }
  lab_0x35df:;
    int64_t v39 = (int64_t)g20; // 0x35df
    int64_t * v40 = (int64_t *)(v39 + 40); // 0x35e6
    uint64_t v41 = *v40; // 0x35e6
    if (v41 >= *(int64_t *)(v39 + 48)) {
        // 0x3840
        function_25e0();
    } else {
        // 0x35f4
        *v40 = v41 + 1;
        *(char *)v41 = 10;
    }
    // 0x35ff
    if (v4 != __readfsqword(40)) {
        // 0x38c9
        function_2590();
        return;
    }
  lab_0x3820:
    // 0x3820
    function_2750();
    int64_t v42 = v28; // 0x3832
    goto lab_0x36c1;
  lab_0x36a8:
    // 0x36a8
    function_2750();
    v42 = v27;
    goto lab_0x36c1;
  lab_0x36c1:
    // 0x36c1
    if (v42 != v29) {
        // 0x36ca
        function_2470();
    }
    goto lab_0x35df;
}