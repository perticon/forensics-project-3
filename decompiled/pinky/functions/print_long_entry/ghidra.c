void print_long_entry(char *param_1)

{
  char cVar1;
  passwd *ppVar2;
  undefined8 uVar3;
  char *pcVar4;
  void *__ptr;
  undefined8 *puVar5;
  size_t sVar6;
  char *pcVar7;
  undefined4 *puVar8;
  FILE *pFVar9;
  long in_FS_OFFSET;
  undefined auStack1080 [1032];
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  ppVar2 = getpwnam(param_1);
  uVar3 = dcgettext(0,"Login name: ",5);
  __printf_chk(1,uVar3);
  __printf_chk(1,"%-28s",param_1);
  uVar3 = dcgettext(0,"In real life: ",5);
  __printf_chk(1,uVar3);
  if (ppVar2 == (passwd *)0x0) {
    uVar3 = dcgettext(0,&DAT_00108026,5);
    if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
      __printf_chk(1," %s",uVar3);
      return;
    }
  }
  else {
    pcVar7 = ppVar2->pw_gecos;
    pcVar4 = strchr(pcVar7,0x2c);
    if (pcVar4 != (char *)0x0) {
      *pcVar4 = '\0';
      pcVar7 = ppVar2->pw_gecos;
    }
    __ptr = (void *)create_fullname(pcVar7,ppVar2->pw_name);
    __printf_chk(1," %s",__ptr);
    free(__ptr);
    pcVar7 = stdout->_IO_write_ptr;
    if (pcVar7 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar7 + 1;
      *pcVar7 = '\n';
    }
    else {
      __overflow(stdout,10);
    }
    cVar1 = include_project;
    if (include_home_and_shell != '\0') {
      uVar3 = dcgettext(0,"Directory: ",5);
      __printf_chk(1,uVar3);
      __printf_chk(1,"%-29s",ppVar2->pw_dir);
      uVar3 = dcgettext(0,"Shell: ",5);
      __printf_chk(1,uVar3);
      __printf_chk(1," %s",ppVar2->pw_shell);
      cVar1 = include_project;
      pcVar7 = stdout->_IO_write_ptr;
      if (pcVar7 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar7 + 1;
        *pcVar7 = '\n';
      }
      else {
        __overflow(stdout,10);
        cVar1 = include_project;
      }
    }
    if (cVar1 != '\0') {
      sVar6 = strlen(ppVar2->pw_dir);
      pcVar7 = (char *)xmalloc(sVar6 + 10);
      puVar5 = (undefined8 *)stpcpy(pcVar7,ppVar2->pw_dir);
      *puVar5 = 0x63656a6f72702e2f;
      *(undefined2 *)(puVar5 + 1) = 0x74;
      pFVar9 = fopen(pcVar7,"r");
      if (pFVar9 != (FILE *)0x0) {
        uVar3 = dcgettext(0,"Project: ",5);
        __printf_chk(1,uVar3);
        while (sVar6 = fread_unlocked(auStack1080,1,0x400,pFVar9), sVar6 != 0) {
          fwrite_unlocked(auStack1080,1,sVar6,stdout);
        }
        rpl_fclose(pFVar9);
      }
      free(pcVar7);
    }
    if (include_plan != '\0') {
      sVar6 = strlen(ppVar2->pw_dir);
      pcVar7 = (char *)xmalloc(sVar6 + 7);
      puVar8 = (undefined4 *)stpcpy(pcVar7,ppVar2->pw_dir);
      *puVar8 = 0x6c702e2f;
      *(undefined2 *)(puVar8 + 1) = 0x6e61;
      *(undefined *)((long)puVar8 + 6) = 0;
      pFVar9 = fopen(pcVar7,"r");
      if (pFVar9 != (FILE *)0x0) {
        uVar3 = dcgettext(0,"Plan:\n",5);
        __printf_chk(1,uVar3);
        while (sVar6 = fread_unlocked(auStack1080,1,0x400,pFVar9), sVar6 != 0) {
          fwrite_unlocked(auStack1080,1,sVar6,stdout);
        }
        rpl_fclose(pFVar9);
      }
      free(pcVar7);
    }
    pcVar7 = stdout->_IO_write_ptr;
    if (pcVar7 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar7 + 1;
      *pcVar7 = '\n';
      if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
        return;
      }
    }
    else if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
      __overflow(stdout,10);
      return;
    }
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}