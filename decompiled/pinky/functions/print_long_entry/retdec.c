void print_long_entry(char * name) {
    int64_t v1 = __readfsqword(40); // 0x2fa0
    int64_t v2 = function_2670(); // 0x2fb3
    function_2560();
    function_2750();
    function_2750();
    function_2560();
    function_2750();
    if (v2 == 0) {
        // 0x3320
        function_2560();
        if (v1 == __readfsqword(40)) {
            // 0x3349
            function_2750();
            return;
        }
        // 0x33be
        function_2590();
        return;
    }
    int64_t * v3 = (int64_t *)(v2 + 24); // 0x301e
    int64_t v4 = function_25c0(); // 0x302a
    int64_t v5 = *v3; // 0x3032
    if (v4 != 0) {
        // 0x3034
        *(char *)v4 = 0;
        v5 = *v3;
    }
    // 0x303b
    create_fullname((char *)v5, (char *)*(int64_t *)v2);
    function_2750();
    function_2470();
    int64_t v6 = (int64_t)g20; // 0x306b
    int64_t * v7 = (int64_t *)(v6 + 40); // 0x3072
    uint64_t v8 = *v7; // 0x3072
    if (v8 >= *(int64_t *)(v6 + 48)) {
        // 0x33a0
        function_25e0();
    } else {
        // 0x3080
        *v7 = v8 + 1;
        *(char *)v8 = 10;
    }
    // 0x308b
    if (*(char *)&include_home_and_shell != 0) {
        // 0x30f8
        function_2560();
        function_2750();
        function_2750();
        function_2560();
        function_2750();
        function_2750();
        int64_t v9 = (int64_t)g20; // 0x3166
        int64_t * v10 = (int64_t *)(v9 + 40); // 0x316d
        uint64_t v11 = *v10; // 0x316d
        if (v11 >= *(int64_t *)(v9 + 48)) {
            // 0x33af
            function_25e0();
            goto lab_0x3094;
        } else {
            // 0x317b
            *v10 = v11 + 1;
            *(char *)v11 = 10;
            if (*(char *)&include_project == 0) {
                goto lab_0x30a1;
            } else {
                goto lab_0x3193;
            }
        }
    } else {
        goto lab_0x3094;
    }
  lab_0x3094:
    // 0x3094
    if (*(char *)&include_project != 0) {
        goto lab_0x3193;
    } else {
        goto lab_0x30a1;
    }
  lab_0x3193:
    // 0x3193
    function_2580();
    xmalloc();
    int64_t v12 = function_2550(); // 0x31af
    *(int64_t *)v12 = 0x63656a6f72702e2f;
    *(int16_t *)(v12 + 8) = 116;
    int64_t v13 = function_27a0(); // 0x31d4
    if (v13 != 0) {
        // 0x31e1
        function_2560();
        function_2750();
        if (function_2500() != 0) {
            function_2720();
            while (function_2500() != 0) {
                // 0x3210
                function_2720();
            }
        }
        // 0x3241
        rpl_fclose((struct _IO_FILE *)v13);
    }
    // 0x3249
    function_2470();
    if (*(char *)&include_plan == 0) {
        goto lab_0x30ae;
    } else {
        goto lab_0x325e;
    }
  lab_0x30a1:
    // 0x30a1
    if (*(char *)&include_plan != 0) {
        goto lab_0x325e;
    } else {
        goto lab_0x30ae;
    }
  lab_0x30ae:;
    int64_t v14 = (int64_t)g20; // 0x30ae
    int64_t * v15 = (int64_t *)(v14 + 40); // 0x30b5
    uint64_t v16 = *v15; // 0x30b5
    if (v16 >= *(int64_t *)(v14 + 48)) {
        // 0x3370
        if (v1 == __readfsqword(40)) {
            // 0x3383
            function_25e0();
            return;
        }
    } else {
        // 0x30c3
        *v15 = v16 + 1;
        *(char *)v16 = 10;
        if (v1 == __readfsqword(40)) {
            // 0x30e5
            return;
        }
    }
    // 0x33be
    function_2590();
  lab_0x325e:
    // 0x325e
    function_2580();
    xmalloc();
    int64_t v17 = function_2550(); // 0x327a
    *(int32_t *)v17 = 0x6c702e2f;
    *(int16_t *)(v17 + 4) = 0x6e61;
    *(char *)(v17 + 6) = 0;
    int64_t v18 = function_27a0(); // 0x329c
    if (v18 != 0) {
        // 0x32a9
        function_2560();
        function_2750();
        if (function_2500() != 0) {
            function_2720();
            while (function_2500() != 0) {
                // 0x32d0
                function_2720();
            }
        }
        // 0x3301
        rpl_fclose((struct _IO_FILE *)v18);
    }
    // 0x3309
    function_2470();
    goto lab_0x30ae;
}