void print_long_entry(char * rdi, struct Eq_502 * fs)
{
	ptr64 fp;
	word64 rax_20 = fs->qw0028;
	struct Eq_1091 * rax_27 = fn0000000000002670(rdi);
	fn0000000000002750(fn0000000000002560(0x05, "Login name: ", null), 0x01);
	fn0000000000002750(0x8011, 0x01);
	fn0000000000002750(fn0000000000002560(0x05, "In real life: ", null), 0x01);
	if (rax_27 == null)
	{
		fn0000000000002560(0x05, "???\n", null);
		if (rax_20 - fs->qw0028 == 0x00)
		{
			fn0000000000002750(0x8708, 0x01);
			return;
		}
		goto l00000000000033BE;
	}
	Eq_18 r12_110 = rax_27->t0018;
	byte * rax_114 = fn00000000000025C0(44, r12_110);
	if (rax_114 != null)
	{
		*rax_114 = 0x00;
		r12_110 = rax_27->t0018;
	}
	Eq_18 rax_131 = create_fullname(rax_27->t0000, r12_110);
	fn0000000000002750(0x8708, 0x01);
	fn0000000000002470(rax_131);
	FILE * rdi_148 = stdout;
	byte * rax_149 = rdi_148->ptr0028;
	if (rax_149 < rdi_148->ptr0030)
	{
		rdi_148->ptr0028 = rax_149 + 1;
		*rax_149 = 0x0A;
	}
	else
		fn00000000000025E0(0x0A, rdi_148);
	if (g_bC012 != 0x00)
	{
		fn0000000000002750(fn0000000000002560(0x05, "Directory: ", null), 0x01);
		fn0000000000002750(32823, 0x01);
		fn0000000000002750(fn0000000000002560(0x05, "Shell: ", null), 0x01);
		fn0000000000002750(0x8708, 0x01);
		FILE * rdi_219 = stdout;
		byte * rax_220 = rdi_219->ptr0028;
		if (rax_220 < rdi_219->ptr0030)
		{
			rdi_219->ptr0028 = rax_220 + 1;
			*rax_220 = 0x0A;
			if (g_bC014 == 0x00)
			{
l00000000000030A1:
				if (g_bC013 == 0x00)
					goto l00000000000030AE;
				goto l000000000000325E;
			}
l0000000000003193:
			Eq_18 rax_240 = xmalloc(&fn0000000000002580(rax_27->t0020)->b0001 + 9);
			struct Eq_1237 * rax_248 = fn0000000000002550(rax_27->t0020, rax_240);
			rax_248->t0000.u0 = 0x63656A6F72702E2F;
			rax_248->w0008 = 116;
			FILE * rax_257 = fn00000000000027A0("r", rax_240);
			if (rax_257 != null)
			{
				Eq_68 rax_292 = (uint64) fn0000000000002750(fn0000000000002560(0x05, "Project: ", null), 0x01);
				while (true)
				{
					fn0000000000002500();
					if (rax_292 == null)
						break;
					rax_292 = fn0000000000002720(stdout, rax_292, (struct <anonymous> *) 0x01, fp - 1080);
				}
				rpl_fclose(rax_257);
			}
			fn0000000000002470(rax_240);
			if (g_bC013 == 0x00)
			{
l00000000000030AE:
				FILE * rdi_405 = stdout;
				byte * rax_406 = rdi_405->ptr0028;
				if (rax_406 < rdi_405->ptr0030)
				{
					rdi_405->ptr0028 = rax_406 + 1;
					*rax_406 = 0x0A;
					if (rax_20 - fs->qw0028 == 0x00)
						return;
				}
				else if (rax_20 - fs->qw0028 == 0x00)
				{
					fn00000000000025E0(0x0A, rdi_405);
					return;
				}
l00000000000033BE:
				fn0000000000002590();
			}
l000000000000325E:
			Eq_18 rax_328 = xmalloc(&fn0000000000002580(rax_27->t0020)->b0001 + 6);
			struct Eq_1237 * rax_336 = fn0000000000002550(rax_27->t0020, rax_328);
			rax_336->t0000.u1 = 1819291183;
			rax_336->w0004 = 0x6E61;
			rax_336->b0006 = 0x00;
			FILE * rax_345 = fn00000000000027A0("r", rax_328);
			if (rax_345 != null)
			{
				Eq_68 rax_381 = (uint64) fn0000000000002750(fn0000000000002560(0x05, "Plan:\n", null), 0x01);
				while (true)
				{
					fn0000000000002500();
					if (rax_381 == null)
						break;
					rax_381 = fn0000000000002720(stdout, rax_381, (struct <anonymous> *) 0x01, fp - 1080);
				}
				rpl_fclose(rax_345);
			}
			fn0000000000002470(rax_328);
			goto l00000000000030AE;
		}
		fn00000000000025E0(0x0A, rdi_219);
	}
	if (g_bC014 == 0x00)
		goto l00000000000030A1;
	goto l0000000000003193;
}