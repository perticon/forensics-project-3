count_ampersands (char const *str)
{
  size_t count = 0;
  do
    {
      if (*str == '&')
        count++;
    } while (*str++);
  return count;
}