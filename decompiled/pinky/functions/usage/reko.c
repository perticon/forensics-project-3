void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000027E0(fn0000000000002560(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000392E;
	}
	fn0000000000002750(fn0000000000002560(0x05, "Usage: %s [OPTION]... [USER]...\n", null), 0x01);
	fn0000000000002630(stdout, fn0000000000002560(0x05, "\n  -l              produce long format output for the specified USERs\n  -b              omit the user's home directory and shell in long format\n  -h              omit the user's project file in long format\n  -p              omit the user's plan file in long format\n  -s              do short format output, this is the default\n", null));
	fn0000000000002630(stdout, fn0000000000002560(0x05, "  -f              omit the line of column headings in short format\n  -w              omit the user's full name in short format\n  -i              omit the user's full name and remote host in short format\n  -q              omit the user's full name, remote host and idle time\n                  in short format\n", null));
	fn0000000000002630(stdout, fn0000000000002560(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002630(stdout, fn0000000000002560(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002750(fn0000000000002560(0x05, "\nA lightweight 'finger' program;  print user information.\nThe utmp file will be %s.\n", null), 0x01);
	struct Eq_1780 * rbx_171 = fp - 0xB8 + 16;
	do
	{
		char * rsi_173 = rbx_171->qw0000;
		++rbx_171;
	} while (rsi_173 != null && fn0000000000002650(rsi_173, "pinky") != 0x00);
	ptr64 r13_186 = rbx_171->qw0008;
	if (r13_186 != 0x00)
	{
		fn0000000000002750(fn0000000000002560(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_18 rax_273 = fn0000000000002740(null, 0x05);
		if (rax_273 == 0x00 || fn00000000000024C0(0x03, "en_", rax_273) == 0x00)
			goto l0000000000003B36;
	}
	else
	{
		fn0000000000002750(fn0000000000002560(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_18 rax_215 = fn0000000000002740(null, 0x05);
		if (rax_215 == 0x00 || fn00000000000024C0(0x03, "en_", rax_215) == 0x00)
		{
			fn0000000000002750(fn0000000000002560(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003B73:
			fn0000000000002750(fn0000000000002560(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000392E:
			fn00000000000027C0(edi);
		}
		r13_186 = 32932;
	}
	fn0000000000002630(stdout, fn0000000000002560(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003B36:
	fn0000000000002750(fn0000000000002560(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003B73;
}