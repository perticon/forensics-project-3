short_pinky (char const *filename,
             const int argc_names, char *const argv_names[])
{
  size_t n_users;
  STRUCT_UTMP *utmp_buf = NULL;

  if (read_utmp (filename, &n_users, &utmp_buf, 0) != 0)
    die (EXIT_FAILURE, errno, "%s", quotef (filename));

  scan_entries (n_users, utmp_buf, argc_names, argv_names);
  exit (EXIT_SUCCESS);
}