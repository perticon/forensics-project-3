void main(char ** rsi, word32 edi, struct Eq_502 * fs)
{
	ptr64 fp;
	word64 qwLoc50;
	word64 rax_33 = fs->qw0028;
	set_program_name(*rsi);
	fn0000000000002740("", 0x06);
	fn0000000000002540("/usr/local/share/locale", "coreutils");
	fn0000000000002510("coreutils");
	atexit(&g_t3E10);
	uint64 rdi_76 = (uint64) edi;
	word32 edi_520 = (word32) rdi_76;
	int32 eax_77 = fn00000000000025A0(&g_tBB00, "sfwiqbhlp", rsi, (word32) rdi_76, null);
	if (eax_77 == ~0x00)
	{
		int64 rax_127 = (int64) g_dwC0B0;
		word32 eax_128 = (word32) rax_127;
		uint64 r12_130 = (uint64) (edi - eax_128);
		int32 r12d_133 = (word32) r12_130;
		if (g_bC011 != 0x00)
		{
			int64 rbp_213 = (int64) eax_128;
			if (read_utmp(0x00, fp - 0x48, fp - 0x50, fs) == 0x00)
			{
				struct Eq_631 * r13_260 = null;
				word64 r14_261 = qwLoc50;
				cu8 al_273 = hard_locale(0x02, fs);
				char * rdx_271 = "%b %e %H:%M";
				if (al_273 != 0x00)
					rdx_271 = (char *) "%Y-%m-%d %H:%M";
				time_format = rdx_271;
				g_dwC130 = (0x00 - (word32) (al_273 < 0x01) & ~0x03) + 0x10;
				if (g_bC016 != 0x00)
				{
					fn0000000000002560(0x05, "Login", null);
					fn0000000000002750(33227, 0x01);
					if (g_bC015 != 0x00)
					{
						fn0000000000002560(0x05, "Name", null);
						fn0000000000002750(33237, 0x01);
					}
					fn0000000000002560(0x05, " TTY", null);
					fn0000000000002750(0x81E1, 0x01);
					if (g_bC017 != 0x00)
					{
						fn0000000000002560(0x05, "Idle", null);
						fn0000000000002750(0x8091, 0x01);
					}
					fn0000000000002560(0x05, "When", null);
					fn0000000000002750(0x81F1, 0x01);
					if (g_bC010 != 0x00)
					{
						fn0000000000002560(0x05, "Where", null);
						fn0000000000002750(0x8708, 0x01);
					}
					FILE * rdi_433 = stdout;
					byte * rax_434 = rdi_433->ptr0028;
					if (rax_434 < rdi_433->ptr0030)
					{
						rdi_433->ptr0028 = rax_434 + 1;
						*rax_434 = 0x0A;
					}
					else
						fn00000000000025E0(0x0A, rdi_433);
				}
				char * rbp_453[] = (char *) rsi + rbp_213 * 0x08;
				for (; r14_261 != 0x00; --r14_261)
				{
					if (r13_260->b002C != 0x00 && r13_260->w0000 == 0x07)
					{
						if (r12d_133 != 0x00)
						{
							Eq_18 r15_473 = &r13_260->b002C;
							ui64 rbx_633 = 0x00;
							do
							{
								int32 ebx_475 = (word32) rbx_636;
								if (r12d_133 <= ebx_475)
									goto l0000000000002B0B;
								rbx_633 = SEQ(SLICE(rbx_636 + 0x01, word32, 32), ebx_475 + 0x01);
								rbx_636 = rbx_633;
							} while (fn00000000000024C0(0x20, rbp_453[rbx_636], r15_473) != 0x00);
						}
						print_entry(r13_260, fs);
					}
l0000000000002B0B:
					++r13_260;
				}
				fn00000000000027C0(0x00);
			}
			quotearg_n_style_colon(0x80AA, 0x03, 0x00, fs);
			fn0000000000002780(0x8709, fn00000000000024A0()->t0000, 0x01);
		}
		else
		{
			if (r12d_133 == 0x00)
			{
				fn0000000000002780(fn0000000000002560(0x05, "no username specified; at least one must be specified when using -l", null), 0x00, 0x00);
l0000000000002BB8:
				usage(0x01);
			}
			if (r12d_133 > 0x00)
			{
				word64 * rbp_158 = (char *) rsi + rax_127 * 0x08;
				word64 * rbx_161 = (char *) rsi + 8 + (rax_127 + (uint64) ((word32) r12_130 - 0x01)) * 0x08;
				do
				{
					print_long_entry(*rbp_158, fs);
					++rbp_158;
				} while (rbp_158 != rbx_161);
			}
			if (rax_33 - fs->qw0028 == 0x00)
				return;
		}
		fn0000000000002590();
	}
	else
	{
		if (eax_77 <= 0x77)
		{
			if (eax_77 > 0x61)
			{
				uint64 rax_296 = (uint64) (eax_77 - 0x62);
				if ((word32) rax_296 <= 0x15)
				{
					<anonymous> * rax_518 = (int64) g_a8694[rax_296 * 0x04] + 34452;
					word64 r9_527;
					word64 r10_528;
					word64 r11_529;
					rax_518();
					return;
				}
			}
			else
			{
				if (eax_77 == ~0x82)
				{
					version_etc(32932, stdout, fs);
					fn00000000000027C0(0x00);
				}
				if (eax_77 == ~0x81)
					usage(0x00);
			}
		}
		goto l0000000000002BB8;
	}
}