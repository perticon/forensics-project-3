undefined8 main(int param_1,undefined8 *param_2)

{
  char *pcVar1;
  long lVar2;
  char cVar3;
  int iVar4;
  long lVar5;
  undefined8 uVar6;
  int *piVar7;
  long lVar8;
  undefined8 *puVar9;
  short *psVar10;
  long in_FS_OFFSET;
  long local_50;
  short *local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  while (iVar4 = getopt_long(param_1,param_2,"sfwiqbhlp",longopts,0), iVar4 != -1) {
    if (0x77 < iVar4) goto switchD_00102963_caseD_63;
    if (iVar4 < 0x62) {
      if (iVar4 == -0x83) {
        version_etc(stdout,"pinky","GNU coreutils",Version,"Joseph Arceneaux","David MacKenzie",
                    "Kaveh Ghazi",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar4 != -0x82) goto switchD_00102963_caseD_63;
      usage(0);
      break;
    }
    switch(iVar4) {
    case 0x62:
      include_home_and_shell = 0;
      break;
    default:
      goto switchD_00102963_caseD_63;
    case 0x66:
      include_heading = '\0';
      break;
    case 0x68:
      include_project = 0;
      break;
    case 0x69:
      include_fullname = '\0';
      include_where = '\0';
      break;
    case 0x6c:
      do_short_format = '\0';
      break;
    case 0x70:
      include_plan = 0;
      break;
    case 0x71:
      include_fullname = '\0';
      include_where = '\0';
      include_idle = '\0';
      break;
    case 0x73:
      do_short_format = '\x01';
      break;
    case 0x77:
      include_fullname = '\0';
    }
  }
  lVar5 = (long)optind;
  param_1 = param_1 - optind;
  if (do_short_format == '\0') {
    if (param_1 != 0) {
      if (0 < param_1) {
        puVar9 = param_2 + lVar5;
        do {
          uVar6 = *puVar9;
          puVar9 = puVar9 + 1;
          print_long_entry(uVar6);
        } while (puVar9 != param_2 + lVar5 + (ulong)(param_1 - 1) + 1);
      }
      if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
        return 0;
      }
      goto LAB_00102cc1;
    }
    uVar6 = dcgettext(0,"no username specified; at least one must be specified when using -l",5);
    error(0,0,uVar6);
switchD_00102963_caseD_63:
    usage();
  }
  else {
    lVar5 = (long)optind;
    local_48 = (short *)0x0;
    iVar4 = read_utmp("/var/run/utmp",&local_50,&local_48,0);
    psVar10 = local_48;
    if (iVar4 != 0) {
      uVar6 = quotearg_n_style_colon(0,3,"/var/run/utmp");
      piVar7 = __errno_location();
      error(1,*piVar7,"%s",uVar6);
LAB_00102cc1:
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    cVar3 = hard_locale();
    time_format = "%b %e %H:%M";
    if (cVar3 != '\0') {
      time_format = "%Y-%m-%d %H:%M";
    }
    time_format_width = (-(uint)(cVar3 == '\0') & 0xfffffffc) + 0x10;
    if (include_heading != '\0') {
      uVar6 = dcgettext(0,"Login",5);
      __printf_chk(1,&DAT_001081cb,uVar6);
      if (include_fullname != '\0') {
        uVar6 = dcgettext(0,&DAT_001081d0,5);
        __printf_chk(1," %-19s",uVar6);
      }
      uVar6 = dcgettext(0,&DAT_001081dc,5);
      __printf_chk(1," %-9s",uVar6);
      if (include_idle != '\0') {
        uVar6 = dcgettext(0,&DAT_001081e7,5);
        __printf_chk(1," %-6s",uVar6);
      }
      uVar6 = dcgettext(0,&DAT_001081ec,5);
      __printf_chk(1," %-*s",time_format_width,uVar6);
      if (include_where != '\0') {
        uVar6 = dcgettext(0,"Where",5);
        __printf_chk(1," %s",uVar6);
      }
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = '\n';
      }
      else {
        __overflow(stdout,10);
      }
    }
    for (; local_50 != 0; local_50 = local_50 + -1) {
      if ((*(char *)(psVar10 + 0x16) != '\0') && (*psVar10 == 7)) {
        if (param_1 != 0) {
          lVar8 = 0;
          do {
            if (param_1 <= (int)lVar8) goto LAB_00102b0b;
            lVar2 = lVar5 + lVar8;
            lVar8 = lVar8 + 1;
            iVar4 = strncmp((char *)(psVar10 + 0x16),(char *)param_2[lVar2],0x20);
          } while (iVar4 != 0);
        }
        print_entry();
      }
LAB_00102b0b:
      psVar10 = psVar10 + 0xc0;
    }
  }
                    /* WARNING: Subroutine does not return */
  exit(0);
}