int32_t parse_additional_groups(struct s0* rdi, struct s0* rsi, struct s0* rdx, int32_t ecx) {
    struct s0* rcx4;
    struct s0* v5;
    struct s0* v6;
    struct s0* v7;
    int32_t v8;
    signed char v9;
    struct s0* rax10;
    struct s0* v11;
    struct s0* rax12;
    struct s0* rsi13;
    struct s0* r13_14;
    struct s0* rax15;
    int32_t* rsp16;
    struct s0* r15_17;
    struct s0* rax18;
    struct s0* rax19;
    int32_t v20;
    struct s0* rbx21;
    struct s0* rbp22;
    void* v23;
    struct s0* r14_24;
    int32_t eax25;
    uint64_t v26;
    struct s5* rax27;
    struct s0* rax28;
    struct s0* rax29;
    uint64_t rax30;
    struct s0** rax31;
    struct s0* rax32;
    uint64_t rax33;
    struct s0* rax34;
    void* rax35;

    *reinterpret_cast<int32_t*>(&rcx4) = ecx;
    v5 = rdi;
    v6 = rsi;
    v7 = rdx;
    v8 = *reinterpret_cast<int32_t*>(&rcx4);
    v9 = *reinterpret_cast<signed char*>(&rcx4);
    rax10 = g28;
    v11 = rax10;
    rax12 = xstrdup(rdi);
    rsi13 = reinterpret_cast<struct s0*>(",");
    r13_14 = rax12;
    rax15 = fun_2760();
    rsp16 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8 - 8 + 8);
    r15_17 = rax15;
    if (!rax15) {
        addr_3284_2:
        if (*reinterpret_cast<signed char*>(&v8)) {
            rax18 = quote(v5, v5);
            rax19 = fun_2510();
            rcx4 = rax18;
            *reinterpret_cast<int32_t*>(&rsi13) = 0;
            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
            rdx = rax19;
            fun_2720();
        }
    } else {
        v20 = 0;
        *reinterpret_cast<int32_t*>(&rbx21) = 0;
        *reinterpret_cast<int32_t*>(&rbx21 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp22) = 0;
        *reinterpret_cast<int32_t*>(&rbp22 + 4) = 0;
        v23 = reinterpret_cast<void*>(rsp16 + 14);
        r14_24 = reinterpret_cast<struct s0*>(rsp16 + 16);
        do {
            *reinterpret_cast<int32_t*>(&rsi13) = 0;
            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
            rcx4 = r14_24;
            *reinterpret_cast<uint32_t*>(&rdx) = 10;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            eax25 = xstrtoumax(r15_17);
            if (eax25 || v26 > 0xffffffff) {
                rax27 = fun_2670(r15_17);
                if (!rax27) {
                    if (!v9) 
                        goto addr_31db_8;
                    rax28 = quote(r15_17, r15_17);
                    rax29 = fun_2510();
                    fun_2470();
                    rcx4 = rax28;
                    rdx = rax29;
                    fun_2720();
                    v20 = -1;
                    continue;
                } else {
                    addr_3178_10:
                    *reinterpret_cast<struct s0**>(&rax30) = rax27->f10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
                    v26 = rax30;
                }
            } else {
                rax31 = fun_2810(r15_17, r15_17);
                rcx4 = *rax31;
                while (*reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r15_17->f0)), *reinterpret_cast<int32_t*>(&rdx + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx4) + reinterpret_cast<unsigned char>(rdx) * 2 + 1) & 32)) {
                    r15_17 = reinterpret_cast<struct s0*>(&r15_17->f1);
                }
                if (*reinterpret_cast<signed char*>(&rdx) != 43) 
                    goto addr_3248_15;
            }
            addr_3180_16:
            if (!rbx21) {
                *reinterpret_cast<uint32_t*>(&rdx) = 4;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                rax32 = x2nrealloc(rbp22, v23, 4, rcx4, 0x8a44);
                rbp22 = rax32;
            }
            rax33 = v26;
            *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp22) + reinterpret_cast<unsigned char>(rbx21) * 4) = *reinterpret_cast<struct s0**>(&rax33);
            rbx21 = reinterpret_cast<struct s0*>(&rbx21->f1);
            continue;
            addr_3248_15:
            rax27 = fun_2670(r15_17);
            if (rax27) 
                goto addr_3178_10;
            goto addr_3180_16;
            rsi13 = reinterpret_cast<struct s0*>(",");
            rax34 = fun_2760();
            r15_17 = rax34;
        } while (rax34);
        goto addr_31aa_21;
    }
    v20 = -1;
    *reinterpret_cast<struct s0**>(&v6->f0) = r15_17;
    addr_31ea_23:
    fun_2450(r13_14, rsi13, rdx, rcx4, r13_14, rsi13, rdx, rcx4);
    rax35 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax35) {
        fun_2550();
    } else {
        return v20;
    }
    addr_31db_8:
    v20 = -1;
    *reinterpret_cast<struct s0**>(&v6->f0) = rbp22;
    goto addr_31ea_23;
    addr_31aa_21:
    if (v20 || rbx21) {
        *reinterpret_cast<struct s0**>(&v6->f0) = rbp22;
        if (v20) {
            v20 = -1;
            goto addr_31ea_23;
        } else {
            *reinterpret_cast<struct s0**>(&v7->f0) = rbx21;
            goto addr_31ea_23;
        }
    } else {
        r15_17 = rbp22;
        goto addr_3284_2;
    }
}