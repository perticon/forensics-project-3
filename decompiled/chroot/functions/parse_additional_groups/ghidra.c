int parse_additional_groups(undefined8 param_1,byte **param_2,long *param_3,char param_4)

{
  int iVar1;
  int iVar2;
  char *__s;
  byte *__name;
  group *pgVar3;
  ushort **ppuVar4;
  byte *pbVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  int *piVar8;
  long lVar9;
  long in_FS_OFFSET;
  long local_50;
  ulong local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_50 = 0;
  __s = (char *)xstrdup();
  __name = (byte *)strtok(__s,",");
  pbVar5 = __name;
  if (__name != (byte *)0x0) {
    iVar1 = 0;
    lVar9 = 0;
    pbVar5 = (byte *)0x0;
    do {
      iVar2 = xstrtoumax(__name,0,10,&local_48,"");
      if ((iVar2 == 0) && (local_48 < 0x100000000)) {
        ppuVar4 = __ctype_b_loc();
        while ((*(byte *)((long)*ppuVar4 + (ulong)*__name * 2 + 1) & 0x20) != 0) {
          __name = __name + 1;
        }
        if ((*__name != 0x2b) && (pgVar3 = getgrnam((char *)__name), pgVar3 != (group *)0x0)) {
LAB_00103178:
          local_48 = (ulong)pgVar3->gr_gid;
        }
        if (local_50 == lVar9) {
          pbVar5 = (byte *)x2nrealloc(pbVar5,&local_50,4);
        }
        *(int *)(pbVar5 + lVar9 * 4) = (int)local_48;
        lVar9 = lVar9 + 1;
      }
      else {
        pgVar3 = getgrnam((char *)__name);
        if (pgVar3 != (group *)0x0) goto LAB_00103178;
        if (param_4 == '\0') {
          iVar1 = -1;
          *param_2 = pbVar5;
          goto LAB_001031ea;
        }
        uVar6 = quote();
        uVar7 = dcgettext(0,"invalid group %s",5);
        piVar8 = __errno_location();
        error(0,*piVar8,uVar7,uVar6);
        iVar1 = -1;
      }
      __name = (byte *)strtok((char *)0x0,",");
    } while (__name != (byte *)0x0);
    if ((iVar1 != 0) || (lVar9 != 0)) {
      *param_2 = pbVar5;
      if (iVar1 == 0) {
        *param_3 = lVar9;
      }
      else {
        iVar1 = -1;
      }
      goto LAB_001031ea;
    }
  }
  if (param_4 != '\0') {
    uVar6 = quote(param_1);
    uVar7 = dcgettext(0,"invalid group list %s",5);
    error(0,0,uVar7,uVar6);
  }
  iVar1 = -1;
  *param_2 = pbVar5;
LAB_001031ea:
  free(__s);
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return iVar1;
}