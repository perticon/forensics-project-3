word32 parse_additional_groups(Eq_20 rdi, struct Eq_465 * fs, union Eq_712 & r8Out)
{
	ptr64 fp;
	Eq_712 r8;
	word32 dwLoc88;
	word64 rax_35 = fs->qw0028;
	word32 edx_521;
	Eq_20 rax_43 = xstrdup(rdi, out edx_521);
	Eq_20 rax_54 = fn0000000000002760(",", rax_43);
	Eq_20 rsp_23 = fp - 0x88;
	Eq_20 r13_311 = rax_43;
	Eq_20 r15_236 = rax_54;
	if (rax_54 != 0x00)
	{
		dwLoc88 = 0x00;
		union Eq_20 * r14_69 = fp - 0x48;
		do
		{
			Eq_1171 rax_111;
			rsp_23.u0 = <invalid>;
			char * r12_91;
			Eq_20 r15_113;
			Eq_20 rbx_221;
			Eq_20 rbp_213;
			if (xstrtoumax(r14_69, 0x0A, 0x00, r15_236, 0x8A44, r15_236, fs, out rbx_221, out rbp_213, out r8, out r12_91, out r13_311, out r14_69, out r15_113) == 0x00 && *((word64) rsp_23 + 64) <= 0xFFFFFFFF)
			{
				rax_111 = fn0000000000002810();
				struct Eq_1183 * rcx_112 = *rax_111;
				while (true)
				{
					uint64 rdx_116 = (uint64) *r15_113;
					byte dl_123 = (byte) rdx_116;
					if ((rcx_112->a0001[rdx_116].b0000 & 0x20) == 0x00)
						break;
					r15_113 = (word64) r15_113 + 1;
				}
				if (dl_123 == 0x2B)
					goto l0000000000003180;
				rax_111 = fn0000000000002670(r15_113);
				if (rax_111 == 0x00)
					goto l0000000000003180;
				goto l0000000000003178;
			}
			rax_111 = fn0000000000002670(r15_113);
			if (rax_111 != 0x00)
			{
l0000000000003178:
				rax_111 = (uint64) *((word64) rax_111 + 16);
				*((word64) rsp_23 + 64) = rax_111;
l0000000000003180:
				if (*((byte) rsp_23.u0 + 56) == rbx_221)
					rbp_213 = x2nrealloc(rax_111, 0x04, *((word64) rsp_23 + 16), rbp_213);
				*((word64) rbp_213 + rbx_221 * 0x04) = *((word64) rsp_23 + 64);
				rbx_221 = (word64) rbx_221 + 1;
			}
			else
			{
				if (*((word64) rsp_23 + 43) == 0x00)
				{
					Eq_20 rax_178 = *((byte) rsp_23.u0 + 8);
					rsp_23.u0->u1 = ~0x00;
					*rax_178 = rbp_213;
					goto l00000000000031EA;
				}
				*rsp_23.u0 = quote(fs, out r8);
				fn0000000000002720(fn0000000000002510(0x05, "invalid group %s", null), *fn0000000000002470(), 0x00);
				rsp_23.u0->u1 = ~0x00;
			}
			Eq_20 rax_235 = fn0000000000002760(r12_91, 0x00);
			r15_236 = rax_235;
		} while (rax_235 != 0x00);
		if (*rsp_23.u0 != 0x00 || rbx_221 != 0x00)
		{
			**((byte) rsp_23.u0 + 8) = rbp_213;
			if (*rsp_23.u0 == 0x00)
				**((byte) rsp_23.u0 + 24) = rbx_221;
			else
				rsp_23.u0->u1 = ~0x00;
			goto l00000000000031EA;
		}
		r15_236 = rbp_213;
	}
	if (*((word64) rsp_23 + 44) != 0x00)
	{
		quote(fs, out r8);
		fn0000000000002720(fn0000000000002510(0x05, "invalid group list %s", null), 0x00, 0x00);
	}
	Eq_20 rax_295 = *((byte) rsp_23.u0 + 8);
	rsp_23.u0->u1 = ~0x00;
	*rax_295 = r15_236;
l00000000000031EA:
	fn0000000000002450(r13_311);
	if (rax_35 - fs->qw0028 != 0x00)
		fn0000000000002550();
	else
	{
		r8Out = r8;
		return dwLoc88;
	}
}