int32_t parse_additional_groups(char * groups, int32_t ** pgids, int64_t * pn_gids, bool show_errors) {
    int64_t v1 = __readfsqword(40); // 0x30ec
    xstrdup(groups);
    int64_t v2 = function_2760(); // 0x3113
    int64_t v3 = 0; // 0x311e
    int64_t v4; // 0x30c0
    int64_t v5; // 0x30c0
    int64_t v6; // 0x30c0
    int64_t v7; // 0x30c0
    int64_t v8; // 0x30c0
    int64_t v9; // 0x30c0
    int64_t v10; // 0x30c0
    int64_t v11; // 0x30c0
    int32_t v12; // 0x30c0
    int32_t v13; // 0x30c0
    int32_t v14; // 0x30c0
    int32_t result; // 0x30c0
    int64_t v15; // bp-72, 0x30c0
    if (v2 == 0) {
        goto lab_0x3284;
    } else {
        // 0x313e
        v12 = 0;
        v9 = 0;
        v6 = 0;
        v4 = v2;
        while (true) {
          lab_0x313e:;
            int64_t v16 = v4;
            v7 = v6;
            v10 = v9;
            v13 = v12;
            char * v17 = (char *)v16; // 0x3152
            int32_t v18 = xstrtoumax(v17, NULL, 10, &v15, (char *)&g15); // 0x3152
            if (v18 == 0 == v15 < 0x100000000) {
                int64_t v19 = *(int64_t *)function_2810() + 1; // 0x3238
                int64_t v20 = v16; // 0x3228
                unsigned char v21 = *(char *)v20; // 0x3234
                v20++;
                while ((*(char *)(v19 + 2 * (int64_t)v21) & 32) != 0) {
                    // 0x3234
                    v21 = *(char *)v20;
                    v20++;
                }
                if (v21 == 43) {
                    goto lab_0x3180;
                } else {
                    int64_t v22 = function_2670(); // 0x324b
                    v5 = v22;
                    if (v22 != 0) {
                        goto lab_0x3178;
                    } else {
                        goto lab_0x3180;
                    }
                }
            } else {
                int64_t v23 = function_2670(); // 0x316e
                v5 = v23;
                if (v23 == 0) {
                    if (!show_errors) {
                        // break -> 0x31db
                        break;
                    }
                    // 0x32d0
                    quote(v17);
                    function_2510();
                    function_2470();
                    function_2720();
                    v14 = -1;
                    v11 = v10;
                    v8 = v7;
                    goto lab_0x3198;
                } else {
                    goto lab_0x3178;
                }
            }
        }
        // 0x31db
        *(int64_t *)pgids = v7;
        result = -1;
        goto lab_0x31ea;
    }
  lab_0x3180:;
    int64_t v24 = v7; // 0x3185
    if (v10 == 0) {
        // 0x325e
        v24 = x2nrealloc();
    }
    // 0x318b
    *(int32_t *)(v24 + 4 * v10) = (int32_t)v15;
    v14 = v13;
    v11 = v10 + 1;
    v8 = v24;
    goto lab_0x3198;
  lab_0x3178:
    // 0x3178
    v15 = (int64_t)*(int32_t *)(v5 + 16);
    goto lab_0x3180;
  lab_0x3198:
    // 0x3198
    v3 = v8;
    int64_t v25 = v11;
    int32_t v26 = v14;
    int64_t v27 = function_2760(); // 0x319d
    v12 = v26;
    v9 = v25;
    v6 = v3;
    v4 = v27;
    if (v27 == 0) {
        // 0x31aa
        if (v26 != 0 || v25 != 0) {
            // 0x31b4
            *(int64_t *)pgids = v3;
            result = -1;
            if (v26 != 0) {
                goto lab_0x31ea;
            } else {
                // 0x31c6
                *pn_gids = v25;
                result = v26;
                goto lab_0x31ea;
            }
        } else {
            goto lab_0x3284;
        }
    }
    goto lab_0x313e;
  lab_0x3284:
    // 0x3284
    if (show_errors) {
        // 0x328b
        quote(groups);
        function_2510();
        function_2720();
    }
    // 0x32bc
    *(int64_t *)pgids = v3;
    result = -1;
    goto lab_0x31ea;
  lab_0x31ea:
    // 0x31ea
    function_2450();
    if (v1 != __readfsqword(40)) {
        // 0x3321
        return function_2550();
    }
    // 0x3206
    return result;
}