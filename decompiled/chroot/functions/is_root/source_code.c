is_root (char const *dir)
{
  char *resolved = canonicalize_file_name (dir);
  bool is_res_root = resolved && STREQ ("/", resolved);
  free (resolved);
  return is_res_root;
}