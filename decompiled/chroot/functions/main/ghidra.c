char main(ulong param_1,char **param_2)

{
  __gid_t _Var1;
  bool bVar2;
  int iVar3;
  size_t sVar4;
  passwd *__s2;
  long lVar5;
  __gid_t *p_Var6;
  undefined8 uVar7;
  undefined8 uVar8;
  passwd *ppVar9;
  char *pcVar10;
  int *piVar11;
  __gid_t *__groups;
  __uid_t __uid;
  char cVar12;
  char *pcVar13;
  long in_FS_OFFSET;
  char *local_78;
  __uid_t local_60;
  __gid_t local_5c;
  __gid_t *local_58;
  size_t local_50;
  __gid_t *local_48;
  long local_40;
  
  p_Var6 = (__gid_t *)(param_1 & 0xffffffff);
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_60 = 0xffffffff;
  local_5c = 0xffffffff;
  local_58 = (__gid_t *)0x0;
  local_50 = 0;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  bVar2 = false;
  textdomain("coreutils");
  exit_failure = 0x7d;
  atexit(close_stdout);
  local_78 = (char *)0x0;
  pcVar13 = (char *)0x0;
LAB_00102918:
  do {
    iVar3 = getopt_long(p_Var6,param_2,&DAT_00108106,long_opts,0);
    pcVar10 = optarg;
    if (iVar3 == -1) {
      if (optind < (int)p_Var6) {
        pcVar10 = param_2[optind];
        __s2 = (passwd *)canonicalize_file_name(pcVar10);
        if ((__s2 != (passwd *)0x0) && (iVar3 = strcmp("/",(char *)__s2), iVar3 == 0)) {
          free(__s2);
          iVar3 = chroot(pcVar10);
          if (iVar3 == 0) {
            __s2 = (passwd *)0x0;
            if (!bVar2) goto LAB_00102cbc;
            goto LAB_00102a6b;
          }
LAB_00102ddb:
          quotearg_style(4,pcVar10);
          uVar7 = dcgettext(0,"cannot change root directory to %s",5);
          piVar11 = __errno_location();
          error(0x7d,*piVar11,uVar7);
          __groups = local_48;
          goto LAB_00102b08;
        }
        free(__s2);
        if (bVar2) {
LAB_00102f89:
          uVar7 = quotearg_style(4,"/");
          uVar8 = dcgettext(0,"option --skip-chdir only permitted if NEWROOT is old %s",5);
          error(0,0,uVar8,uVar7);
                    /* WARNING: Subroutine does not return */
          usage(0x7d);
        }
        if (pcVar13 != (char *)0x0) {
          parse_user_spec(pcVar13,&local_60,&local_5c,0,0);
        }
        if (local_60 != 0xffffffff) {
          if (local_78 == (char *)0x0) {
            __s2 = getpwuid(local_60);
            if (__s2 != (passwd *)0x0) goto LAB_00102e8d;
            if (local_5c == 0xffffffff) goto LAB_00102ca5;
            goto LAB_00102ec8;
          }
          __uid = local_60;
          if (local_5c == 0xffffffff) goto LAB_00102e7c;
          goto LAB_00102c95;
        }
        __s2 = (passwd *)0x0;
LAB_00102d34:
        if (local_78 != (char *)0x0) goto LAB_00102c98;
        if (local_5c != 0xffffffff) {
          if (__s2 == (passwd *)0x0) {
LAB_00102ec8:
            __s2 = (passwd *)0x0;
          }
          else {
            iVar3 = xgetgroups(__s2,local_5c,&local_58);
            if (0 < iVar3) {
              local_50 = (size_t)iVar3;
            }
          }
        }
LAB_00102ca5:
        iVar3 = chroot(pcVar10);
        if (iVar3 == 0) {
LAB_00102cbc:
          iVar3 = chdir("/");
          if (iVar3 != 0) {
            uVar7 = dcgettext(0,"cannot chdir to root directory",5);
            piVar11 = __errno_location();
            pcVar10 = (char *)error(0x7d,*piVar11,uVar7);
            goto LAB_00102cf8;
          }
LAB_00102a6b:
          if (optind + 1 == (int)p_Var6) goto LAB_00102dab;
          param_2 = param_2 + (long)optind + 1;
          do {
            if ((pcVar13 != (char *)0x0) &&
               (lVar5 = parse_user_spec_warn(pcVar13,&local_60,&local_5c,0,0,&local_48), lVar5 != 0)
               ) {
              error(-((char)local_48 == '\0') & 0x7d,0,"%s");
            }
            if (local_60 != 0xffffffff) {
              if ((local_78 != (char *)0x0) && (local_5c != 0xffffffff)) {
LAB_00102af6:
                local_48 = (__gid_t *)0x0;
                pcVar10 = local_78;
                p_Var6 = local_58;
                __groups = local_58;
                if (*local_78 != '\0') {
LAB_00102cf8:
                  iVar3 = parse_additional_groups(pcVar10,&local_48,&local_50);
                  __groups = local_48;
                  if ((iVar3 != 0) && (__groups = p_Var6, local_50 == 0)) {
                    cVar12 = '}';
                    goto LAB_00102ba3;
                  }
                }
LAB_00102b08:
                iVar3 = setgroups(local_50,__groups);
                if (iVar3 == 0) goto LAB_00102b18;
LAB_00102f5d:
                uVar7 = dcgettext(0,"failed to set supplemental groups",5);
                piVar11 = __errno_location();
                error(0x7d,*piVar11,uVar7);
                goto LAB_00102f89;
              }
              ppVar9 = getpwuid(local_60);
              if (ppVar9 != (passwd *)0x0) {
                if (local_5c == 0xffffffff) {
                  local_5c = ppVar9->pw_gid;
                }
                __s2 = (passwd *)ppVar9->pw_name;
                goto LAB_00102be8;
              }
              if (local_5c == 0xffffffff) goto code_r0x00102e48;
            }
LAB_00102be8:
            __groups = local_58;
            local_48 = (__gid_t *)0x0;
            if (local_78 != (char *)0x0) goto LAB_00102af6;
            if ((__s2 != (passwd *)0x0) && (local_5c != 0xffffffff)) {
              iVar3 = xgetgroups(__s2,local_5c,&local_48);
              if (iVar3 < 1) {
                if (local_50 == 0) {
                  uVar7 = dcgettext(0,"failed to get supplemental groups",5);
                  piVar11 = __errno_location();
                  error(0x7d,*piVar11,uVar7);
                  goto LAB_00102f08;
                }
              }
              else {
                local_50 = (size_t)iVar3;
                __groups = local_48;
              }
            }
            if (local_60 != 0xffffffff) goto LAB_00102b08;
LAB_00102b18:
            free(local_48);
            free(local_58);
            _Var1 = local_5c;
            p_Var6 = (__gid_t *)__errno_location();
            if ((_Var1 == 0xffffffff) || (iVar3 = setgid(_Var1), iVar3 == 0)) {
              if ((local_60 == 0xffffffff) || (iVar3 = setuid(local_60), iVar3 == 0)) {
                execvp(*param_2,param_2);
                _Var1 = *p_Var6;
                uVar7 = quote(*param_2);
                cVar12 = (_Var1 == 2) + '~';
                uVar8 = dcgettext(0,"failed to run command %s",5);
                error(0,*p_Var6,uVar8,uVar7);
LAB_00102ba3:
                if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
                  __stack_chk_fail();
                }
                return cVar12;
              }
              uVar7 = dcgettext(0,"failed to set user-ID",5);
              error(0x7d,*p_Var6,uVar7);
              goto LAB_00102f5d;
            }
            uVar7 = dcgettext(0,"failed to set group-ID",5);
            error(0x7d,*p_Var6,uVar7);
LAB_00102dab:
            pcVar10 = getenv("SHELL");
            if (pcVar10 == (char *)0x0) {
              pcVar10 = "/bin/sh";
            }
            *param_2 = pcVar10;
            param_2[1] = "-i";
            param_2[2] = (char *)0x0;
          } while( true );
        }
        goto LAB_00102ddb;
      }
LAB_00102f08:
      uVar7 = dcgettext(0,"missing operand",5);
      error(0,0,uVar7);
LAB_00102f29:
                    /* WARNING: Subroutine does not return */
      usage(0x7d);
    }
    if (iVar3 == 0x100) {
      local_78 = optarg;
      goto LAB_00102918;
    }
    if (iVar3 < 0x101) {
      if (iVar3 == -0x83) {
        version_etc(stdout,"chroot","GNU coreutils",Version,"Roland McGrath",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 == -0x82) {
                    /* WARNING: Subroutine does not return */
        usage(0);
      }
      goto LAB_00102f29;
    }
    if (iVar3 == 0x101) {
      sVar4 = strlen(optarg);
      pcVar13 = pcVar10;
      if ((sVar4 != 0) && (pcVar10[sVar4 - 1] == ':')) {
        pcVar10[sVar4 - 1] = '\0';
      }
    }
    else {
      if (iVar3 != 0x102) goto LAB_00102f29;
      bVar2 = true;
    }
  } while( true );
code_r0x00102e48:
  pcVar10 = (char *)(ulong)local_60;
  uVar7 = dcgettext(0,"no group specified for unknown uid: %d",5);
  piVar11 = __errno_location();
  __uid = 0x7d;
  error(0x7d,*piVar11,uVar7);
LAB_00102e7c:
  __s2 = getpwuid(__uid);
  if (__s2 == (passwd *)0x0) {
LAB_00102c95:
    __s2 = (passwd *)0x0;
LAB_00102c98:
    if (*local_78 != '\0') {
      parse_additional_groups(local_78,&local_58,&local_50);
    }
    goto LAB_00102ca5;
  }
LAB_00102e8d:
  if (local_5c == 0xffffffff) {
    local_5c = __s2->pw_gid;
  }
  __s2 = (passwd *)__s2->pw_name;
  goto LAB_00102d34;
}