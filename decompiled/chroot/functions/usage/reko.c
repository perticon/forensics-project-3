word64 usage(word32 edi)
{
	ptr64 fp;
	struct Eq_465 * fs;
	if (edi != 0x00)
	{
		fn00000000000027D0(fn0000000000002510(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000338E;
	}
	fn0000000000002710(fn0000000000002510(0x05, "Usage: %s [OPTION] NEWROOT [COMMAND [ARG]...]\n  or:  %s OPTION\n", null), 0x01);
	fn0000000000002610(stdout, fn0000000000002510(0x05, "Run COMMAND with root directory set to NEWROOT.\n\n", null));
	fn0000000000002610(stdout, fn0000000000002510(0x05, "      --groups=G_LIST        specify supplementary groups as g1,g2,..,gN\n", null));
	fn0000000000002610(stdout, fn0000000000002510(0x05, "      --userspec=USER:GROUP  specify user and group (ID or name) to use\n", null));
	quotearg_style(0x04, fs);
	fn0000000000002710(fn0000000000002510(0x05, "      --skip-chdir           do not change working directory to %s\n", null), 0x01);
	fn0000000000002610(stdout, fn0000000000002510(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002610(stdout, fn0000000000002510(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002610(stdout, fn0000000000002510(0x05, "\nIf no command is given, run '\"$SHELL\" -i' (default: '/bin/sh -i').\n", null));
	struct Eq_1458 * rbx_208 = fp - 0xB8 + 16;
	do
	{
		Eq_20 rsi_210 = rbx_208->qw0000;
		++rbx_208;
	} while (rsi_210 != 0x00 && fn0000000000002630(rsi_210, "chroot") != 0x00);
	ptr64 r13_223 = rbx_208->qw0008;
	if (r13_223 != 0x00)
	{
		fn0000000000002710(fn0000000000002510(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_20 rax_310 = fn0000000000002700(null, 0x05);
		if (rax_310 == 0x00 || fn0000000000002480(0x03, "en_", rax_310) == 0x00)
			goto l00000000000035EE;
	}
	else
	{
		fn0000000000002710(fn0000000000002510(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_20 rax_252 = fn0000000000002700(null, 0x05);
		if (rax_252 == 0x00 || fn0000000000002480(0x03, "en_", rax_252) == 0x00)
		{
			fn0000000000002710(fn0000000000002510(0x05, "Full documentation <%s%s>\n", null), 0x01);
l000000000000362B:
			fn0000000000002710(fn0000000000002510(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000338E:
			fn00000000000027B0(edi);
		}
		r13_223 = 0x802D;
	}
	fn0000000000002610(stdout, fn0000000000002510(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l00000000000035EE:
	fn0000000000002710(fn0000000000002510(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l000000000000362B;
}