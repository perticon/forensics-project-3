
struct s0 {
    struct s0* f0;
    unsigned char f1;
    signed char f2;
    signed char[1] pad4;
    struct s0* f4;
    signed char[3] pad8;
    struct s0* f8;
    signed char[7] pad16;
    int64_t f10;
    struct s0* f14;
    struct s0* f18;
};

int64_t fun_2520();

int64_t fun_2460(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2520();
    if (r8d > 10) {
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x8640 + rax11 * 4) + 0x8640;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* g28;

struct s0* fun_2470();

struct s0* slotvec = reinterpret_cast<struct s0*>(0x70);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_25d0();

struct s2 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void fun_2450(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

void fun_2550();

struct s0* quotearg_n_options(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    struct s0* rax8;
    struct s0* r15_9;
    struct s0* v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x4dcf;
    rax8 = fun_2470();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<struct s0**>(&rax8->f0);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xc070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7101]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x4e5b;
            fun_25d0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xc100) {
                fun_2450(r14_19, rsi24, rsi, rdx, r14_19, rsi24, rsi, rdx);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x4eea);
        }
        *reinterpret_cast<struct s0**>(&rax8->f0) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2550();
        } else {
            return r14_19;
        }
    }
}

struct s0* xstrdup(struct s0* rdi, ...);

struct s3 {
    signed char[1] pad1;
    struct s0* f1;
};

struct s0* ximemdup();

struct s4 {
    signed char[16] pad16;
    struct s0* f10;
    signed char[3] pad20;
    struct s0* f14;
};

struct s4* fun_2650(struct s0* rdi, struct s0* rsi);

void fun_26d0(struct s0* rdi, ...);

int32_t xstrtoul(struct s0* rdi);

struct s0** fun_2590(int64_t rdi, struct s0* rsi);

struct s0* umaxtostr(int64_t rdi, struct s0* rsi);

void fun_2430(struct s0* rdi, ...);

struct s5 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s5* fun_2670(struct s0* rdi, ...);

int64_t parse_with_separator(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0** r8, struct s0* r9) {
    struct s0* r10_7;
    uint64_t r14_8;
    struct s0* r12_9;
    struct s0* rbp10;
    struct s0** rbx11;
    void* rsp12;
    struct s0* v13;
    struct s0* rax14;
    struct s0* v15;
    struct s0* v16;
    struct s0* r13_17;
    void* rax18;
    struct s0* r15_19;
    struct s0* rax20;
    void* rsp21;
    struct s0* r10_22;
    uint32_t eax23;
    struct s3* r15_24;
    struct s0* rax25;
    struct s4* rax26;
    unsigned char v27;
    uint32_t eax28;
    int1_t zf29;
    int32_t eax30;
    struct s0** v31;
    int64_t rdi32;
    struct s0** rax33;
    int64_t rdi34;
    struct s0* rax35;
    struct s0* rdi36;
    struct s0* rax37;
    uint32_t eax38;
    void* rax39;
    struct s5* rax40;
    int32_t eax41;
    uint64_t v42;
    struct s0* rax43;
    struct s0* rax44;
    struct s0* rax45;

    r10_7 = rsi;
    *reinterpret_cast<struct s0**>(&r14_8) = reinterpret_cast<struct s0*>(0xffffffff);
    r12_9 = rcx;
    rbp10 = r9;
    rbx11 = r8;
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88);
    v13 = rdx;
    rax14 = g28;
    v15 = rax14;
    v16 = *reinterpret_cast<struct s0**>(&rdx->f0);
    if (rcx) {
        *reinterpret_cast<struct s0**>(&r14_8) = *reinterpret_cast<struct s0**>(&rcx->f0);
    }
    if (rbx11) {
        *rbx11 = reinterpret_cast<struct s0*>(0);
    }
    if (rbp10) {
        *reinterpret_cast<struct s0**>(&rbp10->f0) = reinterpret_cast<struct s0*>(0);
    }
    if (!r10_7) {
        *reinterpret_cast<int32_t*>(&r13_17) = 0;
        *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
        if (!*reinterpret_cast<struct s0**>(&rdi->f0)) {
            while (1) {
                addr_5c30_9:
                rcx = v16;
                *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                *reinterpret_cast<struct s0**>(&v13->f0) = rcx;
                if (r12_9) {
                    *reinterpret_cast<struct s0**>(&r12_9->f0) = *reinterpret_cast<struct s0**>(&r14_8);
                }
                if (rbx11) {
                    *rbx11 = r13_17;
                    *reinterpret_cast<int32_t*>(&r13_17) = 0;
                    *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
                }
                addr_5c4f_13:
                if (rbp10) {
                    *reinterpret_cast<struct s0**>(&rbp10->f0) = r10_7;
                    *reinterpret_cast<int32_t*>(&r10_7) = 0;
                    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                }
                v16 = r10_7;
                fun_2450(r13_17, rsi, rdx, rcx, r13_17, rsi, rdx, rcx);
                fun_2450(v16, rsi, rdx, rcx, v16, rsi, rdx, rcx);
                rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(g28));
                if (!rax18) 
                    break;
                addr_5f02_16:
                fun_2550();
                addr_5f07_17:
                *reinterpret_cast<int32_t*>(&r10_7) = 0;
                *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
            }
        } else {
            v16 = r10_7;
            *reinterpret_cast<int32_t*>(&r15_19) = 0;
            *reinterpret_cast<int32_t*>(&r15_19 + 4) = 0;
            rax20 = xstrdup(rdi);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            r10_22 = v16;
            r13_17 = rax20;
            eax23 = 1;
            goto addr_5ccc_19;
        }
    } else {
        r15_24 = reinterpret_cast<struct s3*>(reinterpret_cast<unsigned char>(r10_7) - reinterpret_cast<unsigned char>(rdi));
        if (r15_24) {
            rsi = reinterpret_cast<struct s0*>(&r15_24->f1);
            v16 = r10_7;
            rax25 = ximemdup();
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            r10_22 = v16;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax25) + reinterpret_cast<uint64_t>(r15_24)) = 0;
            r13_17 = rax25;
            if (!r10_22->f1) {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax25->f0) == 43)) 
                    goto addr_5e19_23;
                rdi = rax25;
                rax26 = fun_2650(rdi, rsi);
                rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                if (!rax26) 
                    goto addr_5e19_23;
                v27 = 1;
                *reinterpret_cast<int32_t*>(&r15_19) = 0;
                *reinterpret_cast<int32_t*>(&r15_19 + 4) = 0;
                goto addr_5cf4_26;
            } else {
                r15_19 = reinterpret_cast<struct s0*>(&r10_22->f1);
                eax23 = 0;
                goto addr_5ccc_19;
            }
        } else {
            eax28 = r10_7->f1;
            if (!*reinterpret_cast<signed char*>(&eax28)) {
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r10_7) = 0;
                *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                goto addr_5c30_9;
            } else {
                r15_19 = reinterpret_cast<struct s0*>(&r10_7->f1);
                *reinterpret_cast<int32_t*>(&r13_17) = 0;
                *reinterpret_cast<int32_t*>(&r13_17 + 4) = 0;
                goto addr_5c03_31;
            }
        }
    }
    return 0;
    addr_5ccc_19:
    *reinterpret_cast<unsigned char*>(&rdx) = reinterpret_cast<uint1_t>(!!r10_22);
    *reinterpret_cast<uint32_t*>(&rdx) = *reinterpret_cast<uint32_t*>(&rdx) & eax23;
    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
    zf29 = reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&r13_17->f0) == 43);
    v27 = *reinterpret_cast<unsigned char*>(&rdx);
    if (zf29) 
        goto addr_5e37_33;
    rdi = r13_17;
    rax26 = fun_2650(rdi, rsi);
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
    if (!rax26) {
        if (v27) {
            addr_5e19_23:
            fun_26d0(rdi, rdi);
            *reinterpret_cast<int32_t*>(&rbp10) = 0;
            *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
            r12_9 = reinterpret_cast<struct s0*>("invalid spec");
        } else {
            addr_5e37_33:
            *reinterpret_cast<int32_t*>(&rsi) = 0;
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            rcx = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp21) + 40);
            *reinterpret_cast<uint32_t*>(&rdx) = 10;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            eax30 = xstrtoul(r13_17);
            if (eax30 || (rbx11 = v31, v16 = *reinterpret_cast<struct s0**>(&rbx11), reinterpret_cast<uint64_t>(rbx11) > 0xfffffffe)) {
                fun_26d0(r13_17, r13_17);
                *reinterpret_cast<int32_t*>(&rbp10) = 0;
                *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
                r12_9 = reinterpret_cast<struct s0*>("invalid user");
            } else {
                fun_26d0(r13_17, r13_17);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8 - 8 + 8);
                if (r15_19) {
                    eax28 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r15_19->f0));
                    *reinterpret_cast<struct s0**>(&rbx11) = reinterpret_cast<struct s0*>(0);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx11) + 4) = 0;
                    goto addr_5c03_31;
                } else {
                    *reinterpret_cast<int32_t*>(&r10_7) = 0;
                    *reinterpret_cast<int32_t*>(&r10_7 + 4) = 0;
                    *reinterpret_cast<struct s0**>(&v13->f0) = *reinterpret_cast<struct s0**>(&rbx11);
                    if (!r12_9) 
                        goto addr_5c4f_13;
                    *reinterpret_cast<struct s0**>(&r12_9->f0) = *reinterpret_cast<struct s0**>(&r14_8);
                    goto addr_5c4f_13;
                }
            }
        }
    } else {
        addr_5cf4_26:
        v16 = rax26->f10;
        if (v27) {
            *reinterpret_cast<struct s0**>(&r14_8) = rax26->f14;
            *reinterpret_cast<struct s0**>(&rdi32) = *reinterpret_cast<struct s0**>(&r14_8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi32) + 4) = 0;
            rax33 = fun_2590(rdi32, rsi);
            if (!rax33) {
                *reinterpret_cast<struct s0**>(&rdi34) = *reinterpret_cast<struct s0**>(&r14_8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
                rsi = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8 + 48);
                rax35 = umaxtostr(rdi34, rsi);
                rdi36 = rax35;
            } else {
                rdi36 = *rax33;
            }
            rax37 = xstrdup(rdi36, rdi36);
            fun_2430(rdi36, rdi36);
            fun_26d0(rdi36, rdi36);
            r10_7 = rax37;
            goto addr_5c30_9;
        } else {
            fun_26d0(rdi, rdi);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
            if (!r15_19) 
                goto addr_5f07_17;
            eax38 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r15_19->f0));
            if (*reinterpret_cast<signed char*>(&eax38) != 43) 
                goto addr_5c0b_47; else 
                goto addr_5d20_48;
        }
    }
    addr_5d5a_49:
    fun_2450(r13_17, rsi, rdx, rcx, r13_17, rsi, rdx, rcx);
    fun_2450(rbp10, rsi, rdx, rcx, rbp10, rsi, rdx, rcx);
    rax39 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v15) - reinterpret_cast<unsigned char>(g28));
    if (rax39) 
        goto addr_5f02_16;
    addr_5c03_31:
    if (*reinterpret_cast<signed char*>(&eax28) == 43) 
        goto addr_5d20_48;
    addr_5c0b_47:
    rax40 = fun_2670(r15_19, r15_19);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
    if (!rax40) {
        addr_5d20_48:
        *reinterpret_cast<int32_t*>(&rsi) = 0;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rcx = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp12) + 40);
        *reinterpret_cast<uint32_t*>(&rdx) = 10;
        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
        eax41 = xstrtoul(r15_19);
        if (eax41 || (r14_8 = v42, r14_8 > 0xfffffffe)) {
            fun_2430(r15_19, r15_19);
            r12_9 = reinterpret_cast<struct s0*>("invalid group");
            rax43 = xstrdup(r15_19, r15_19);
            rbp10 = rax43;
            goto addr_5d5a_49;
        } else {
            fun_2430(r15_19, r15_19);
            *reinterpret_cast<int32_t*>(&rbp10) = 0;
            *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
            rax44 = xstrdup(r15_19, r15_19);
            r10_7 = rax44;
            goto addr_5c30_9;
        }
    } else {
        *reinterpret_cast<struct s0**>(&r14_8) = rax40->f10;
        fun_2430(r15_19, r15_19);
        rax45 = xstrdup(r15_19, r15_19);
        r10_7 = rax45;
        goto addr_5c30_9;
    }
}

struct s0* fun_2760();

struct s0* quote(struct s0* rdi, ...);

struct s0* fun_2510();

struct s0* fun_2720();

int32_t xstrtoumax(struct s0* rdi);

struct s0** fun_2810(struct s0* rdi, ...);

struct s0* x2nrealloc(struct s0* rdi, void* rsi, struct s0* rdx, struct s0* rcx, int64_t r8);

int32_t parse_additional_groups(struct s0* rdi, struct s0* rsi, struct s0* rdx, int32_t ecx) {
    struct s0* rcx4;
    struct s0* v5;
    struct s0* v6;
    struct s0* v7;
    int32_t v8;
    signed char v9;
    struct s0* rax10;
    struct s0* v11;
    struct s0* rax12;
    struct s0* rsi13;
    struct s0* r13_14;
    struct s0* rax15;
    int32_t* rsp16;
    struct s0* r15_17;
    struct s0* rax18;
    struct s0* rax19;
    int32_t v20;
    struct s0* rbx21;
    struct s0* rbp22;
    void* v23;
    struct s0* r14_24;
    int32_t eax25;
    uint64_t v26;
    struct s5* rax27;
    struct s0* rax28;
    struct s0* rax29;
    uint64_t rax30;
    struct s0** rax31;
    struct s0* rax32;
    uint64_t rax33;
    struct s0* rax34;
    void* rax35;

    *reinterpret_cast<int32_t*>(&rcx4) = ecx;
    v5 = rdi;
    v6 = rsi;
    v7 = rdx;
    v8 = *reinterpret_cast<int32_t*>(&rcx4);
    v9 = *reinterpret_cast<signed char*>(&rcx4);
    rax10 = g28;
    v11 = rax10;
    rax12 = xstrdup(rdi);
    rsi13 = reinterpret_cast<struct s0*>(",");
    r13_14 = rax12;
    rax15 = fun_2760();
    rsp16 = reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88 - 8 + 8 - 8 + 8);
    r15_17 = rax15;
    if (!rax15) {
        addr_3284_2:
        if (*reinterpret_cast<signed char*>(&v8)) {
            rax18 = quote(v5, v5);
            rax19 = fun_2510();
            rcx4 = rax18;
            *reinterpret_cast<int32_t*>(&rsi13) = 0;
            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
            rdx = rax19;
            fun_2720();
        }
    } else {
        v20 = 0;
        *reinterpret_cast<int32_t*>(&rbx21) = 0;
        *reinterpret_cast<int32_t*>(&rbx21 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp22) = 0;
        *reinterpret_cast<int32_t*>(&rbp22 + 4) = 0;
        v23 = reinterpret_cast<void*>(rsp16 + 14);
        r14_24 = reinterpret_cast<struct s0*>(rsp16 + 16);
        do {
            *reinterpret_cast<int32_t*>(&rsi13) = 0;
            *reinterpret_cast<int32_t*>(&rsi13 + 4) = 0;
            rcx4 = r14_24;
            *reinterpret_cast<uint32_t*>(&rdx) = 10;
            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
            eax25 = xstrtoumax(r15_17);
            if (eax25 || v26 > 0xffffffff) {
                rax27 = fun_2670(r15_17);
                if (!rax27) {
                    if (!v9) 
                        goto addr_31db_8;
                    rax28 = quote(r15_17, r15_17);
                    rax29 = fun_2510();
                    fun_2470();
                    rcx4 = rax28;
                    rdx = rax29;
                    fun_2720();
                    v20 = -1;
                    continue;
                } else {
                    addr_3178_10:
                    *reinterpret_cast<struct s0**>(&rax30) = rax27->f10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax30) + 4) = 0;
                    v26 = rax30;
                }
            } else {
                rax31 = fun_2810(r15_17, r15_17);
                rcx4 = *rax31;
                while (*reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r15_17->f0)), *reinterpret_cast<int32_t*>(&rdx + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx4) + reinterpret_cast<unsigned char>(rdx) * 2 + 1) & 32)) {
                    r15_17 = reinterpret_cast<struct s0*>(&r15_17->f1);
                }
                if (*reinterpret_cast<signed char*>(&rdx) != 43) 
                    goto addr_3248_15;
            }
            addr_3180_16:
            if (!rbx21) {
                *reinterpret_cast<uint32_t*>(&rdx) = 4;
                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                rax32 = x2nrealloc(rbp22, v23, 4, rcx4, 0x8a44);
                rbp22 = rax32;
            }
            rax33 = v26;
            *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbp22) + reinterpret_cast<unsigned char>(rbx21) * 4) = *reinterpret_cast<struct s0**>(&rax33);
            rbx21 = reinterpret_cast<struct s0*>(&rbx21->f1);
            continue;
            addr_3248_15:
            rax27 = fun_2670(r15_17);
            if (rax27) 
                goto addr_3178_10;
            goto addr_3180_16;
            rsi13 = reinterpret_cast<struct s0*>(",");
            rax34 = fun_2760();
            r15_17 = rax34;
        } while (rax34);
        goto addr_31aa_21;
    }
    v20 = -1;
    *reinterpret_cast<struct s0**>(&v6->f0) = r15_17;
    addr_31ea_23:
    fun_2450(r13_14, rsi13, rdx, rcx4, r13_14, rsi13, rdx, rcx4);
    rax35 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax35) {
        fun_2550();
    } else {
        return v20;
    }
    addr_31db_8:
    v20 = -1;
    *reinterpret_cast<struct s0**>(&v6->f0) = rbp22;
    goto addr_31ea_23;
    addr_31aa_21:
    if (v20 || rbx21) {
        *reinterpret_cast<struct s0**>(&v6->f0) = rbp22;
        if (v20) {
            v20 = -1;
            goto addr_31ea_23;
        } else {
            *reinterpret_cast<struct s0**>(&v7->f0) = rbx21;
            goto addr_31ea_23;
        }
    } else {
        r15_17 = rbp22;
        goto addr_3284_2;
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xc080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s6 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s6* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s6* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0x85d9);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0x85d4);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0x85dd);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0x85d0);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gbdd8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gbdd8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2423() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t endgrent = 0x2030;

void fun_2433() {
    __asm__("cli ");
    goto endgrent;
}

int64_t getenv = 0x2040;

void fun_2443() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2050;

void fun_2453() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2060;

void fun_2463() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2070;

void fun_2473() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2080;

void fun_2483() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_2493() {
    __asm__("cli ");
    goto _exit;
}

int64_t chroot = 0x20a0;

void fun_24a3() {
    __asm__("cli ");
    goto chroot;
}

int64_t __fpending = 0x20b0;

void fun_24b3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20c0;

void fun_24c3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20d0;

void fun_24d3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20e0;

void fun_24e3() {
    __asm__("cli ");
    goto fclose;
}

int64_t getpwuid = 0x20f0;

void fun_24f3() {
    __asm__("cli ");
    goto getpwuid;
}

int64_t bindtextdomain = 0x2100;

void fun_2503() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2110;

void fun_2513() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2120;

void fun_2523() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2130;

void fun_2533() {
    __asm__("cli ");
    goto strlen;
}

int64_t chdir = 0x2140;

void fun_2543() {
    __asm__("cli ");
    goto chdir;
}

int64_t __stack_chk_fail = 0x2150;

void fun_2553() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2160;

void fun_2563() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2170;

void fun_2573() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2180;

void fun_2583() {
    __asm__("cli ");
    goto strchr;
}

int64_t getgrgid = 0x2190;

void fun_2593() {
    __asm__("cli ");
    goto getgrgid;
}

int64_t strrchr = 0x21a0;

void fun_25a3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21b0;

void fun_25b3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21c0;

void fun_25c3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21d0;

void fun_25d3() {
    __asm__("cli ");
    goto memset;
}

int64_t canonicalize_file_name = 0x21e0;

void fun_25e3() {
    __asm__("cli ");
    goto canonicalize_file_name;
}

int64_t setgroups = 0x21f0;

void fun_25f3() {
    __asm__("cli ");
    goto setgroups;
}

int64_t memcmp = 0x2200;

void fun_2603() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2210;

void fun_2613() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2220;

void fun_2623() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2230;

void fun_2633() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2240;

void fun_2643() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t getpwnam = 0x2250;

void fun_2653() {
    __asm__("cli ");
    goto getpwnam;
}

int64_t memcpy = 0x2260;

void fun_2663() {
    __asm__("cli ");
    goto memcpy;
}

int64_t getgrnam = 0x2270;

void fun_2673() {
    __asm__("cli ");
    goto getgrnam;
}

int64_t fileno = 0x2280;

void fun_2683() {
    __asm__("cli ");
    goto fileno;
}

int64_t getgroups = 0x2290;

void fun_2693() {
    __asm__("cli ");
    goto getgroups;
}

int64_t malloc = 0x22a0;

void fun_26a3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22b0;

void fun_26b3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22c0;

void fun_26c3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t endpwent = 0x22d0;

void fun_26d3() {
    __asm__("cli ");
    goto endpwent;
}

int64_t __freading = 0x22e0;

void fun_26e3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x22f0;

void fun_26f3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2300;

void fun_2703() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2310;

void fun_2713() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2320;

void fun_2723() {
    __asm__("cli ");
    goto error;
}

int64_t setgid = 0x2330;

void fun_2733() {
    __asm__("cli ");
    goto setgid;
}

int64_t getgrouplist = 0x2340;

void fun_2743() {
    __asm__("cli ");
    goto getgrouplist;
}

int64_t fseeko = 0x2350;

void fun_2753() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtok = 0x2360;

void fun_2763() {
    __asm__("cli ");
    goto strtok;
}

int64_t strtoumax = 0x2370;

void fun_2773() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t strtoul = 0x2380;

void fun_2783() {
    __asm__("cli ");
    goto strtoul;
}

int64_t execvp = 0x2390;

void fun_2793() {
    __asm__("cli ");
    goto execvp;
}

int64_t __cxa_atexit = 0x23a0;

void fun_27a3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x23b0;

void fun_27b3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23c0;

void fun_27c3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23d0;

void fun_27d3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t setuid = 0x23e0;

void fun_27e3() {
    __asm__("cli ");
    goto setuid;
}

int64_t mbsinit = 0x23f0;

void fun_27f3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2400;

void fun_2803() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2410;

void fun_2813() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s0* fun_2700(int64_t rdi, ...);

void fun_2500(int64_t rdi, int64_t rsi);

void fun_24d0(int64_t rdi, int64_t rsi);

int32_t exit_failure = 1;

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2560(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* optarg = reinterpret_cast<struct s0*>(0);

void usage();

int64_t stdout = 0;

struct s0* Version = reinterpret_cast<struct s0*>(0x6a);

void version_etc(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, int64_t r8);

int32_t fun_27b0();

struct s0* fun_2530(struct s0* rdi, ...);

int32_t optind = 0;

struct s0* fun_25e0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_2630(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void parse_user_spec(struct s0* rdi, struct s0* rsi, struct s0* rdx);

struct s0* fun_24f0();

int32_t fun_24a0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, ...);

struct s0* quotearg_style(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, int64_t r8, void* r9, struct s0* a7, int64_t a8, int64_t a9, int64_t a10, struct s0* a11, int64_t a12, struct s0* a13, struct s0* a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18, int64_t a19, int64_t a20);

int32_t fun_25f0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_2730(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* fun_2440(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* parse_user_spec_warn();

int32_t fun_2540(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t xgetgroups(struct s0* rdi);

int32_t fun_27e0();

void fun_2790(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int64_t fun_2863(struct s0* edi, struct s0* rsi) {
    struct s0* r15_3;
    int64_t v4;
    int64_t r14_5;
    int64_t v6;
    int64_t r13_7;
    int64_t v8;
    int64_t r12_9;
    int64_t v10;
    int64_t rbp11;
    struct s0* rbp12;
    int64_t v13;
    int64_t rbx14;
    struct s0* rbx15;
    struct s0* rdi16;
    struct s0* rax17;
    struct s0* v18;
    int64_t v19;
    int64_t v20;
    int32_t r12d21;
    struct s0** rsp22;
    struct s0* v23;
    int64_t r8_24;
    struct s0* rcx25;
    struct s0* rdx26;
    struct s0* rsi27;
    int64_t rdi28;
    int32_t eax29;
    struct s0* rax30;
    int64_t rdi31;
    void* r9_32;
    struct s0* rax33;
    signed char* rax34;
    int64_t rax35;
    struct s0* r13_36;
    struct s0* rax37;
    void* rsp38;
    struct s0* r14_39;
    int32_t eax40;
    struct s0** rsp41;
    struct s0* rax42;
    int32_t eax43;
    struct s0** rsp44;
    int64_t v45;
    int64_t v46;
    struct s0* rax47;
    struct s0* rax48;
    struct s0* rdx49;
    int64_t rdi50;
    struct s0* rsi51;
    int32_t eax52;
    struct s0* rax53;
    int64_t rdi54;
    int32_t eax55;
    struct s0* rax56;
    struct s0* rax57;
    struct s0* rax58;
    struct s0* rax59;
    struct s0* rax60;
    struct s0* rax61;
    struct s0* rax62;
    int32_t eax63;
    int32_t eax64;
    int64_t rax65;
    int32_t eax66;
    int32_t eax67;
    struct s0* rax68;
    int32_t eax69;
    int64_t v70;
    int64_t v71;
    struct s0* rax72;
    int32_t eax73;
    struct s0* rdi74;
    int32_t r12d75;
    struct s0* rdi76;
    struct s0* rax77;
    int32_t r12d78;
    void* rax79;
    int64_t rax80;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r15_3) = 0;
    *reinterpret_cast<int32_t*>(&r15_3 + 4) = 0;
    v4 = r14_5;
    v6 = r13_7;
    v8 = r12_9;
    v10 = rbp11;
    rbp12 = edi;
    *reinterpret_cast<int32_t*>(&rbp12 + 4) = 0;
    v13 = rbx14;
    rbx15 = rsi;
    rdi16 = *reinterpret_cast<struct s0**>(&rsi->f0);
    rax17 = g28;
    v18 = rax17;
    *reinterpret_cast<int32_t*>(&v19) = -1;
    *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4) = reinterpret_cast<struct s0*>(0xffffffff);
    v20 = 0;
    set_program_name(rdi16);
    fun_2700(6, 6);
    fun_2500("coreutils", "/usr/local/share/locale");
    r12d21 = 0;
    fun_24d0("coreutils", "/usr/local/share/locale");
    exit_failure = 0x7d;
    atexit(0x3740, "/usr/local/share/locale");
    rsp22 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 72 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    v23 = reinterpret_cast<struct s0*>(0);
    while (*reinterpret_cast<int32_t*>(&r8_24) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_24) + 4) = 0, rcx25 = reinterpret_cast<struct s0*>(0xbac0), rdx26 = reinterpret_cast<struct s0*>("+"), rsi27 = rbx15, *reinterpret_cast<struct s0**>(&rdi28) = rbp12, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0, eax29 = fun_2560(rdi28, rsi27, "+", 0xbac0), rsp22 = rsp22 - 8 + 8, eax29 != -1) {
        if (eax29 == 0x100) {
            addr_29e0_4:
            rax30 = optarg;
            v23 = rax30;
            continue;
        } else {
            if (eax29 <= 0x100) {
                if (eax29 != 0xffffff7d) {
                    if (eax29 != 0xffffff7e) 
                        goto addr_2f29_8;
                    usage();
                    rsp22 = rsp22 - 8 + 8;
                    goto addr_29e0_4;
                } else {
                    rdi31 = stdout;
                    rcx25 = Version;
                    *reinterpret_cast<int32_t*>(&r9_32) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_32) + 4) = 0;
                    r8_24 = reinterpret_cast<int64_t>("Roland McGrath");
                    version_etc(rdi31, "chroot", "GNU coreutils", rcx25, "Roland McGrath");
                    eax29 = fun_27b0();
                    rsp22 = rsp22 - 8 + 8 - 8 + 8;
                }
            } else {
                if (eax29 == 0x101) {
                    r15_3 = optarg;
                    rax33 = fun_2530(r15_3, r15_3);
                    rsp22 = rsp22 - 8 + 8;
                    if (!rax33) 
                        continue;
                    rax34 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_3) + reinterpret_cast<unsigned char>(rax33) + 0xffffffffffffffff);
                    if (*rax34 != 58) 
                        continue;
                    *rax34 = 0;
                    continue;
                }
            }
        }
        if (eax29 != 0x102) 
            goto addr_2f29_8;
        r12d21 = 1;
    }
    rax35 = optind;
    if (reinterpret_cast<signed char>(*reinterpret_cast<struct s0**>(&rax35)) >= reinterpret_cast<signed char>(rbp12)) {
        addr_2f08_18:
        fun_2510();
        fun_2720();
    } else {
        r13_36 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rbx15) + rax35 * 8);
        rax37 = fun_25e0(r13_36, rsi27, "+", 0xbac0);
        rsp38 = reinterpret_cast<void*>(rsp22 - 8 + 8);
        r14_39 = rax37;
        if (!rax37 || (rsi27 = rax37, eax40 = fun_2630("/", rsi27, "+", 0xbac0), rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8), r8_24 = reinterpret_cast<int64_t>("/"), !!eax40)) {
            fun_2450(r14_39, rsi27, "+", 0xbac0);
            rsp41 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
            if (!*reinterpret_cast<signed char*>(&r12d21)) {
                if (r15_3) {
                    rdx26 = reinterpret_cast<struct s0*>(rsp41 + 28);
                    rsi27 = reinterpret_cast<struct s0*>(rsp41 + 24);
                    *reinterpret_cast<int32_t*>(&rcx25) = 0;
                    *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
                    parse_user_spec(r15_3, rsi27, rdx26);
                    rsp41 = rsp41 - 8 + 8;
                }
                if (1) {
                    *reinterpret_cast<int32_t*>(&r14_39) = 0;
                    *reinterpret_cast<int32_t*>(&r14_39 + 4) = 0;
                    goto addr_2d34_25;
                } else {
                    if (!v23) {
                        rax42 = fun_24f0();
                        rsp41 = rsp41 - 8 + 8;
                        r14_39 = rax42;
                        if (rax42) 
                            goto addr_2e8d_28;
                        if (1) 
                            goto addr_2ca5_30; else 
                            goto addr_2ec8_31;
                    } else {
                        if (1) 
                            goto addr_2e7c_33; else 
                            goto addr_2c95_34;
                    }
                }
            }
        } else {
            fun_2450(r14_39, rsi27, "+", 0xbac0);
            eax43 = fun_24a0(r13_36, rsi27, "+", 0xbac0);
            rsp44 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp38) - 8 + 8 - 8 + 8);
            r8_24 = reinterpret_cast<int64_t>("/");
            if (!eax43) {
                *reinterpret_cast<int32_t*>(&r14_39) = 0;
                *reinterpret_cast<int32_t*>(&r14_39 + 4) = 0;
                if (!*reinterpret_cast<signed char*>(&r12d21)) 
                    goto addr_2cbc_37; else 
                    goto addr_2a6b_38;
            }
            while (1) {
                addr_2ddb_39:
                rax47 = quotearg_style(4, r13_36, rdx26, rcx25, "/", r9_32, v23, "/", v45, v19, 0, v20, 0, v18, v46, v13, v10, v8, v6, v4);
                rax48 = fun_2510();
                fun_2470();
                rcx25 = rax47;
                rdx49 = rax48;
                fun_2720();
                rsp44 = rsp44 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                while (1) {
                    rbp12 = reinterpret_cast<struct s0*>(0);
                    do {
                        addr_2c3b_41:
                        rdi50 = v20;
                        do {
                            rsi51 = rbp12;
                            eax52 = fun_25f0(rdi50, rsi51, rdx49, rcx25);
                            rsp44 = rsp44 - 8 + 8;
                            if (eax52) 
                                goto addr_2f5d_43;
                            do {
                                fun_2450(0, rsi51, rdx49, rcx25, 0, rsi51, rdx49, rcx25);
                                fun_2450(0, rsi51, rdx49, rcx25, 0, rsi51, rdx49, rcx25);
                                rax53 = fun_2470();
                                rbp12 = rax53;
                                if (reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4) == 0xffffffff)) 
                                    goto addr_2b43_45;
                                *reinterpret_cast<struct s0**>(&rdi54) = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4);
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
                                eax55 = fun_2730(rdi54, rsi51, rdx49, rcx25);
                                if (!eax55) 
                                    goto addr_2b43_45;
                                rax56 = fun_2510();
                                rsi27 = *reinterpret_cast<struct s0**>(&rbp12->f0);
                                *reinterpret_cast<int32_t*>(&rsi27 + 4) = 0;
                                rdx49 = rax56;
                                fun_2720();
                                rsp44 = rsp44 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
                                while (1) {
                                    rax57 = fun_2440("SHELL", rsi27, rdx49, rcx25);
                                    rsp44 = rsp44 - 8 + 8;
                                    if (!rax57) {
                                        rax57 = reinterpret_cast<struct s0*>("/bin/sh");
                                    }
                                    *reinterpret_cast<struct s0**>(&rbx15->f0) = rax57;
                                    rbx15->f8 = reinterpret_cast<struct s0*>("-i");
                                    rbx15->f10 = 0;
                                    while (1) {
                                        if (r15_3 && (rdx49 = reinterpret_cast<struct s0*>(rsp44 + 28), *reinterpret_cast<int32_t*>(&r8_24) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_24) + 4) = 0, r9_32 = reinterpret_cast<void*>(rsp44 + 48), rax58 = parse_user_spec_warn(), rsp44 = rsp44 - 8 + 8, rcx25 = rax58, !!rax58)) {
                                            rdx49 = reinterpret_cast<struct s0*>("%s");
                                            fun_2720();
                                            rsp44 = rsp44 - 8 + 8;
                                        }
                                        if (1) 
                                            goto addr_2be8_54;
                                        if (!v23) 
                                            goto addr_2bc9_56;
                                        if (*reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4) != 0xffffffff) 
                                            goto addr_2ae8_58;
                                        addr_2bc9_56:
                                        rax59 = fun_24f0();
                                        rsp44 = rsp44 - 8 + 8;
                                        if (rax59) 
                                            goto addr_2bd7_59;
                                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4) == 0xffffffff)) 
                                            goto addr_2be8_54;
                                        *reinterpret_cast<int32_t*>(&r13_36) = -1;
                                        *reinterpret_cast<int32_t*>(&r13_36 + 4) = 0;
                                        rax60 = fun_2510();
                                        rax61 = fun_2470();
                                        *reinterpret_cast<int32_t*>(&rcx25) = -1;
                                        *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
                                        rdx26 = rax60;
                                        rsi27 = *reinterpret_cast<struct s0**>(&rax61->f0);
                                        *reinterpret_cast<int32_t*>(&rsi27 + 4) = 0;
                                        fun_2720();
                                        rsp41 = rsp44 - 8 + 8 - 8 + 8 - 8 + 8;
                                        addr_2e7c_33:
                                        rax62 = fun_24f0();
                                        rsp41 = rsp41 - 8 + 8;
                                        r14_39 = rax62;
                                        if (!rax62) {
                                            addr_2c95_34:
                                            *reinterpret_cast<int32_t*>(&r14_39) = 0;
                                            *reinterpret_cast<int32_t*>(&r14_39 + 4) = 0;
                                        } else {
                                            addr_2e8d_28:
                                            if (reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4) == 0xffffffff)) {
                                                *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4) = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r14_39) + 20);
                                            }
                                            r14_39 = *reinterpret_cast<struct s0**>(&r14_39->f0);
                                            addr_2d34_25:
                                            if (!v23) 
                                                goto addr_2d3f_64;
                                        }
                                        if (*reinterpret_cast<struct s0**>(&v23->f0)) {
                                            rdx26 = reinterpret_cast<struct s0*>(rsp41 + 40);
                                            rsi27 = reinterpret_cast<struct s0*>(rsp41 + 32);
                                            *reinterpret_cast<int32_t*>(&rcx25) = 0;
                                            *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
                                            parse_additional_groups(v23, rsi27, rdx26, 0);
                                            rsp41 = rsp41 - 8 + 8;
                                        }
                                        addr_2ca5_30:
                                        eax63 = fun_24a0(r13_36, rsi27, rdx26, rcx25, r13_36, rsi27, rdx26, rcx25);
                                        rsp44 = rsp41 - 8 + 8;
                                        r8_24 = reinterpret_cast<int64_t>("/");
                                        if (eax63) 
                                            goto addr_2ddb_39;
                                        addr_2cbc_37:
                                        eax64 = fun_2540("/", rsi27, rdx26, rcx25);
                                        rsp44 = rsp44 - 8 + 8;
                                        if (eax64) 
                                            goto addr_2ccc_67;
                                        addr_2a6b_38:
                                        rax65 = optind;
                                        rdx49 = reinterpret_cast<struct s0*>(static_cast<uint32_t>(rax65 + 1));
                                        *reinterpret_cast<int32_t*>(&rdx49 + 4) = 0;
                                        if (rdx49 == rbp12) 
                                            break;
                                        rbx15 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx15) + rax65 * 8 + 8);
                                        continue;
                                        addr_2d3f_64:
                                        rsi27 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4);
                                        *reinterpret_cast<int32_t*>(&rsi27 + 4) = 0;
                                        if (rsi27 != 0xffffffff) {
                                            if (!r14_39) {
                                                addr_2ec8_31:
                                                *reinterpret_cast<int32_t*>(&r14_39) = 0;
                                                *reinterpret_cast<int32_t*>(&r14_39 + 4) = 0;
                                                goto addr_2ca5_30;
                                            } else {
                                                rdx26 = reinterpret_cast<struct s0*>(rsp41 + 32);
                                                eax66 = xgetgroups(r14_39);
                                                rsp41 = rsp41 - 8 + 8;
                                                if (!(reinterpret_cast<uint1_t>(eax66 < 0) | reinterpret_cast<uint1_t>(eax66 == 0))) {
                                                    v20 = eax66;
                                                    goto addr_2ca5_30;
                                                }
                                            }
                                        }
                                    }
                                }
                                addr_2be8_54:
                                rbp12 = reinterpret_cast<struct s0*>(0);
                                if (v23) 
                                    goto addr_2af6_72;
                                rsi51 = *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4);
                                *reinterpret_cast<int32_t*>(&rsi51 + 4) = 0;
                                if (r14_39 && rsi51 != 0xffffffff) {
                                    rdx49 = reinterpret_cast<struct s0*>(rsp44 + 48);
                                    eax67 = xgetgroups(r14_39);
                                    rsp44 = rsp44 - 8 + 8;
                                    if (reinterpret_cast<uint1_t>(eax67 < 0) | reinterpret_cast<uint1_t>(eax67 == 0)) {
                                        if (v20) 
                                            continue; else 
                                            goto addr_2edc_76;
                                    } else {
                                        rbp12 = reinterpret_cast<struct s0*>(0);
                                        v20 = eax67;
                                        continue;
                                    }
                                }
                                addr_2bd7_59:
                                if (reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4) == 0xffffffff)) {
                                    rdx49 = *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(rax59) + 20);
                                    *reinterpret_cast<int32_t*>(&rdx49 + 4) = 0;
                                    *reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(&v19) + 4) = rdx49;
                                }
                                r14_39 = *reinterpret_cast<struct s0**>(&rax59->f0);
                                goto addr_2be8_54;
                            } while (1);
                            goto addr_2c3b_41;
                            addr_2ae8_58:
                            rbp12 = reinterpret_cast<struct s0*>(0);
                            addr_2af6_72:
                            rax68 = v23;
                            rdi50 = v20;
                        } while (!*reinterpret_cast<struct s0**>(&rax68->f0));
                        goto addr_2cf8_81;
                        addr_2ccc_67:
                        fun_2510();
                        fun_2470();
                        *reinterpret_cast<int32_t*>(&rdi50) = 0x7d;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi50) + 4) = 0;
                        rax68 = fun_2720();
                        rsp44 = rsp44 - 8 + 8 - 8 + 8 - 8 + 8;
                        addr_2cf8_81:
                        *reinterpret_cast<int32_t*>(&rcx25) = 0;
                        *reinterpret_cast<int32_t*>(&rcx25 + 4) = 0;
                        rdx49 = reinterpret_cast<struct s0*>(rsp44 + 40);
                        *reinterpret_cast<unsigned char*>(&rcx25) = reinterpret_cast<uint1_t>(rdi50 == 0);
                        eax69 = parse_additional_groups(rax68, rsp44 + 48, rdx49, *reinterpret_cast<int32_t*>(&rcx25));
                        rsp44 = rsp44 - 8 + 8;
                        if (!eax69) 
                            break;
                    } while (v20);
                    goto addr_2d26_83;
                }
            }
        }
    }
    addr_2f29_8:
    usage();
    fun_2550();
    goto addr_2f38_85;
    addr_2f89_86:
    quotearg_style(4, "/", rdx26, rcx25, r8_24, r9_32, v23, "/", v70, v19, 0, v20, 0, v18, v71, v13, v10, v8, v6, v4);
    fun_2510();
    fun_2720();
    usage();
    addr_2f5d_43:
    rax72 = fun_2510();
    fun_2470();
    rdx26 = rax72;
    fun_2720();
    goto addr_2f89_86;
    addr_2b43_45:
    if (1 || (eax73 = fun_27e0(), !eax73)) {
        rdi74 = *reinterpret_cast<struct s0**>(&rbx15->f0);
        r12d75 = 0;
        fun_2790(rdi74, rbx15, rdx49, rcx25);
        rdi76 = *reinterpret_cast<struct s0**>(&rbx15->f0);
        *reinterpret_cast<unsigned char*>(&r12d75) = reinterpret_cast<uint1_t>(*reinterpret_cast<struct s0**>(&rbp12->f0) == 2);
        rax77 = quote(rdi76, rdi76);
        r12d78 = r12d75 + 0x7e;
        fun_2510();
        rcx25 = rax77;
        fun_2720();
    } else {
        addr_2f38_85:
        fun_2510();
        fun_2720();
        goto addr_2f5d_43;
    }
    addr_2ba3_88:
    rax79 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
    if (!rax79) {
        *reinterpret_cast<int32_t*>(&rax80) = r12d78;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax80) + 4) = 0;
        return rax80;
    }
    addr_2edc_76:
    fun_2510();
    fun_2470();
    fun_2720();
    goto addr_2f08_18;
    addr_2d26_83:
    r12d78 = 0x7d;
    goto addr_2ba3_88;
}

int64_t __libc_start_main = 0;

void fun_2fd3() {
    __asm__("cli ");
    __libc_start_main(0x2860, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xc008;

void fun_2420(int64_t rdi);

int64_t fun_3073() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2420(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_30b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_2710(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_2610(struct s0* rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_2480(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int64_t stderr = 0;

void fun_27d0(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3333(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    int64_t r12_6;
    struct s0* rax7;
    int64_t r12_8;
    struct s0* rax9;
    int64_t r12_10;
    struct s0* rax11;
    int64_t r8_12;
    void* r9_13;
    struct s0* rax14;
    struct s0* rax15;
    int64_t r12_16;
    struct s0* rax17;
    int64_t r12_18;
    struct s0* rax19;
    int64_t r12_20;
    struct s0* rax21;
    int32_t eax22;
    struct s0* r13_23;
    struct s0* rax24;
    struct s0* rax25;
    int32_t eax26;
    struct s0* rax27;
    struct s0* rax28;
    struct s0* rax29;
    int32_t eax30;
    struct s0* rax31;
    int64_t r15_32;
    struct s0* rax33;
    struct s0* rax34;
    struct s0* rax35;
    int64_t rdi36;
    struct s0* r8_37;
    struct s0* r9_38;
    int64_t v39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;
    int64_t v44;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2510();
            fun_2710(1, rax5, r12_2, r12_2);
            r12_6 = stdout;
            rax7 = fun_2510();
            fun_2610(rax7, r12_6, 5, r12_2);
            r12_8 = stdout;
            rax9 = fun_2510();
            fun_2610(rax9, r12_8, 5, r12_2);
            r12_10 = stdout;
            rax11 = fun_2510();
            fun_2610(rax11, r12_10, 5, r12_2);
            rax14 = quotearg_style(4, "/", 5, r12_2, r8_12, r9_13, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            rax15 = fun_2510();
            fun_2710(1, rax15, rax14, r12_2);
            r12_16 = stdout;
            rax17 = fun_2510();
            fun_2610(rax17, r12_16, 5, r12_2);
            r12_18 = stdout;
            rax19 = fun_2510();
            fun_2610(rax19, r12_18, 5, r12_2);
            r12_20 = stdout;
            rax21 = fun_2510();
            fun_2610(rax21, r12_20, 5, r12_2);
            do {
                if (1) 
                    break;
                eax22 = fun_2630("chroot", 0, 5, "sha512sum");
            } while (eax22);
            r13_23 = v4;
            if (!r13_23) {
                rax24 = fun_2510();
                fun_2710(1, rax24, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax25 = fun_2700(5);
                if (!rax25 || (eax26 = fun_2480(rax25, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax26)) {
                    rax27 = fun_2510();
                    r13_23 = reinterpret_cast<struct s0*>("chroot");
                    fun_2710(1, rax27, "https://www.gnu.org/software/coreutils/", "chroot");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_23 = reinterpret_cast<struct s0*>("chroot");
                    goto addr_36e8_9;
                }
            } else {
                rax28 = fun_2510();
                fun_2710(1, rax28, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax29 = fun_2700(5);
                if (!rax29 || (eax30 = fun_2480(rax29, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax30)) {
                    addr_35ee_11:
                    rax31 = fun_2510();
                    fun_2710(1, rax31, "https://www.gnu.org/software/coreutils/", "chroot");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_23 == "chroot")) {
                        r12_2 = reinterpret_cast<struct s0*>(0x8a44);
                    }
                } else {
                    addr_36e8_9:
                    r15_32 = stdout;
                    rax33 = fun_2510();
                    fun_2610(rax33, r15_32, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_35ee_11;
                }
            }
            rax34 = fun_2510();
            fun_2710(1, rax34, r13_23, r12_2);
            addr_338e_14:
            fun_27b0();
        }
    } else {
        rax35 = fun_2510();
        rdi36 = stderr;
        fun_27d0(rdi36, 1, rax35, r12_2, r8_37, r9_38, v39, v40, v41, v42, v43, v44);
        goto addr_338e_14;
    }
}

int64_t file_name = 0;

void fun_3723(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3733(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

struct s0* quotearg_colon();

struct s0* fun_2490(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_3743() {
    int64_t rdi1;
    int32_t eax2;
    struct s0* rax3;
    int1_t zf4;
    struct s0* rbx5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2470(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax3->f0) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2510();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_37d3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<struct s0**>(&rsi11) = *reinterpret_cast<struct s0**>(&rbx5->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2720();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2490(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_37d3_5:
        *reinterpret_cast<struct s0**>(&rsi11) = *reinterpret_cast<struct s0**>(&rbx5->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2720();
    }
}

void fun_27c0(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s7 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s7* fun_25a0();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_37f3(struct s0* rdi) {
    int64_t rcx2;
    struct s0* rbx3;
    struct s7* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_27c0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2460("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_25a0();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2480(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_4f93(int64_t rdi) {
    int64_t rbp2;
    struct s0* rax3;
    struct s0* r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2470();
    r12d4 = *reinterpret_cast<struct s0**>(&rax3->f0);
    if (!rbp2) {
        rbp2 = 0xc200;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<struct s0**>(&rax3->f0) = r12d4;
    return;
}

int64_t fun_4fd3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4ff3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xc200);
    }
    *rdi = esi;
    return 0xc200;
}

int64_t fun_5013(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xc200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s8 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5053(struct s8* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xc200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s9 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* fun_5073(struct s9* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xc200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x282a;
    if (!rdx) 
        goto 0x282a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xc200;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_50b3(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s10* r8) {
    struct s10* rbx6;
    struct s0* rax7;
    struct s0* r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s10*>(0xc200);
    }
    rax7 = fun_2470();
    r15d8 = *reinterpret_cast<struct s0**>(&rax7->f0);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x50e6);
    *reinterpret_cast<struct s0**>(&rax7->f0) = r15d8;
    return rax13;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5133(struct s0* rdi, struct s0* rsi, struct s0** rdx, struct s11* rcx) {
    struct s11* rbx5;
    struct s0* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    struct s0* v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s11*>(0xc200);
    }
    rax6 = fun_2470();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<struct s0**>(&rax6->f0);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5161);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x51bc);
    *reinterpret_cast<struct s0**>(&rax6->f0) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5223() {
    __asm__("cli ");
}

struct s0* gc078 = reinterpret_cast<struct s0*>(0);

int64_t slotvec0 = 0x100;

void fun_5233() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rsi7;
    struct s0* rdx8;
    struct s0* rcx9;
    struct s0* rdi10;
    struct s0* rsi11;
    struct s0* rdx12;
    struct s0* rcx13;
    struct s0* rsi14;
    struct s0* rdx15;
    struct s0* rcx16;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f14;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2450(rdi6, rsi7, rdx8, rcx9);
        } while (rbx4 != rbp5);
    }
    rdi10 = r12_2->f8;
    if (rdi10 != 0xc100) {
        fun_2450(rdi10, rsi11, rdx12, rcx13);
        gc078 = reinterpret_cast<struct s0*>(0xc100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xc070) {
        fun_2450(r12_2, rsi14, rdx15, rcx16);
        slotvec = reinterpret_cast<struct s0*>(0xc070);
    }
    nslots = 1;
    return;
}

void fun_52d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_52f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5303(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5323(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_5343(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2830;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2550();
    } else {
        return rax6;
    }
}

struct s0* fun_53d3(struct s0* rdi, int32_t esi, struct s0* rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2835;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2550();
    } else {
        return rax7;
    }
}

struct s0* fun_5463(int32_t edi, struct s0* rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x283a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2550();
    } else {
        return rax5;
    }
}

struct s0* fun_54f3(int32_t edi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x283f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2550();
    } else {
        return rax6;
    }
}

struct s0* fun_5583(struct s0* rdi, struct s0* rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6c70]");
    __asm__("movdqa xmm1, [rip+0x6c78]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6c61]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2550();
    } else {
        return rax10;
    }
}

struct s0* fun_5623(struct s0* rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6bd0]");
    __asm__("movdqa xmm1, [rip+0x6bd8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x6bc1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2550();
    } else {
        return rax9;
    }
}

struct s0* fun_56c3(struct s0* rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6b30]");
    __asm__("movdqa xmm1, [rip+0x6b38]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x6b19]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2550();
    } else {
        return rax3;
    }
}

struct s0* fun_5753(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6aa0]");
    __asm__("movdqa xmm1, [rip+0x6aa8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6a96]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2550();
    } else {
        return rax4;
    }
}

struct s0* fun_57e3(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2844;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2550();
    } else {
        return rax6;
    }
}

struct s0* fun_5883(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x696a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6962]");
    __asm__("movdqa xmm2, [rip+0x696a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2849;
    if (!rdx) 
        goto 0x2849;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2550();
    } else {
        return rax7;
    }
}

struct s0* fun_5923(int32_t edi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x68ca]");
    __asm__("movdqa xmm1, [rip+0x68d2]");
    __asm__("movdqa xmm2, [rip+0x68da]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x284e;
    if (!rdx) 
        goto 0x284e;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2550();
    } else {
        return rax9;
    }
}

struct s0* fun_59d3(int64_t rdi, int64_t rsi, struct s0* rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x681a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x6812]");
    __asm__("movdqa xmm2, [rip+0x681a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2853;
    if (!rsi) 
        goto 0x2853;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2550();
    } else {
        return rax6;
    }
}

struct s0* fun_5a73(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x677a]");
    __asm__("movdqa xmm1, [rip+0x6782]");
    __asm__("movdqa xmm2, [rip+0x678a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2858;
    if (!rsi) 
        goto 0x2858;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2550();
    } else {
        return rax7;
    }
}

void fun_5b13() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5b23(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5b43() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5b63(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_2580(struct s0* rdi, ...);

int64_t fun_5f33(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0** rcx, struct s0* r8, signed char* r9) {
    int64_t rax7;
    int64_t r10_8;
    struct s0* rax9;
    int64_t rax10;
    struct s0* rax11;
    int32_t eax12;
    int64_t rax13;

    __asm__("cli ");
    if (!rdx) {
        rax7 = parse_with_separator(rdi, 0, rsi, 0, rcx, r8);
        r10_8 = rax7;
        goto addr_5fd8_3;
    } else {
        rax9 = fun_2580(rdi, rdi);
        rax10 = parse_with_separator(rdi, rax9, rsi, rdx, rcx, r8);
        r10_8 = rax10;
        if (rax9 || (!rax10 || (rax11 = fun_2580(rdi, rdi), r10_8 = rax10, rax11 == 0))) {
            addr_5fd8_3:
            if (!r9) {
                addr_5fe1_5:
                return r10_8;
            } else {
                eax12 = 0;
            }
        } else {
            rax13 = parse_with_separator(rdi, rax11, rsi, rdx, rcx, r8);
            r10_8 = rax10;
            if (!rax13) {
                eax12 = 1;
                r10_8 = reinterpret_cast<int64_t>("warning: '.' should be ':'");
                if (!r9) {
                    *reinterpret_cast<int32_t*>(&r10_8) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_8) + 4) = 0;
                    goto addr_5fe1_5;
                }
            } else {
                goto addr_5fd8_3;
            }
        }
        *r9 = *reinterpret_cast<signed char*>(&eax12);
        goto addr_5fe1_5;
    }
}

int64_t fun_6033(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0** rcx, struct s0* r8) {
    struct s0* rax6;
    int64_t rax7;
    int64_t r10_8;
    struct s0* rax9;
    int64_t rax10;

    __asm__("cli ");
    if (!rdx) {
        goto parse_with_separator;
    } else {
        rax6 = fun_2580(rdi, rdi);
        rax7 = parse_with_separator(rdi, rax6, rsi, rdx, rcx, r8);
        r10_8 = rax7;
        if (!rax6 && (!rax7 || (rax9 = fun_2580(rdi, rdi), r10_8 = rax7, !!rax9) && (rax10 = parse_with_separator(rdi, rax9, rsi, rdx, rcx, r8), r10_8 = rax7, rax10 == 0))) {
            *reinterpret_cast<int32_t*>(&r10_8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r10_8) + 4) = 0;
        }
        return r10_8;
    }
}

struct s12 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2640(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_6113(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s12* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_27d0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_27d0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2510();
    fun_27d0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2640(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2510();
    fun_27d0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2640(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2510();
        fun_27d0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x8ce8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x8ce8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6583() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s13 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_65a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s13* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s13* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2550();
    } else {
        return;
    }
}

void fun_6643(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_66e6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_66f0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2550();
    } else {
        return;
    }
    addr_66e6_5:
    goto addr_66f0_7;
}

void fun_6723() {
    int64_t rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2640(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2510();
    fun_2710(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2510();
    fun_2710(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2510();
    goto fun_2710;
}

int64_t fun_24c0();

void xalloc_die();

void fun_67c3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24c0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

struct s0* fun_26a0(unsigned char* rdi);

void fun_6803(unsigned char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_26a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6823(unsigned char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_26a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6843(unsigned char* rdi) {
    struct s0* rax2;

    __asm__("cli ");
    rax2 = fun_26a0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

struct s0* fun_26f0(struct s0* rdi);

void fun_6863(struct s0* rdi, int64_t rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26f0(rdi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6893(struct s0* rdi, int64_t rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26f0(rdi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_68c3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24c0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6903() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6943(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6973(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24c0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_69c3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24c0();
            if (rax5) 
                break;
            addr_6a0d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_6a0d_5;
        rax8 = fun_24c0();
        if (rax8) 
            goto addr_69f6_9;
        if (rbx4) 
            goto addr_6a0d_5;
        addr_69f6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6a53(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24c0();
            if (rax8) 
                break;
            addr_6a9a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_6a9a_5;
        rax11 = fun_24c0();
        if (rax11) 
            goto addr_6a82_9;
        if (!rbx6) 
            goto addr_6a82_9;
        if (r12_4) 
            goto addr_6a9a_5;
        addr_6a82_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6ae3(struct s0* rdi, void** rsi, struct s0* rdx, void* rcx, uint64_t r8) {
    struct s0* r13_6;
    struct s0* rdi7;
    void** r12_8;
    void* rsi9;
    void* rcx10;
    void* rbx11;
    void* rax12;
    void* rbp13;
    void* rbp14;
    struct s0* rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void*>((reinterpret_cast<int64_t>(rcx10) >> 1) + reinterpret_cast<uint64_t>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<int64_t>(rbx11) <= reinterpret_cast<int64_t>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_6b8d_9:
            rbx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_6ba0_10:
                *r12_8 = reinterpret_cast<void*>(0);
            }
            addr_6b40_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbx11) - reinterpret_cast<uint64_t>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_6b66_12;
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_6bb4_14;
            if (reinterpret_cast<int64_t>(rcx10) <= reinterpret_cast<int64_t>(rsi9)) 
                goto addr_6b5d_16;
            if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) 
                goto addr_6bb4_14;
            addr_6b5d_16:
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_6bb4_14;
            addr_6b66_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_26f0(rdi7);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_6bb4_14;
            if (!rbp13) 
                break;
            addr_6bb4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<int64_t>(rbp13) <= reinterpret_cast<int64_t>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_6b8d_9;
        } else {
            if (!r13_6) 
                goto addr_6ba0_10;
            goto addr_6b40_11;
        }
    }
}

int64_t fun_2620();

void fun_6be3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2620();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6c13() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2620();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6c43() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2620();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6c63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2620();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2660(signed char* rdi, struct s0* rsi, unsigned char* rdx);

void fun_6c83(int64_t rdi, unsigned char* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26a0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2660;
    }
}

void fun_6cc3(int64_t rdi, unsigned char* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26a0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2660;
    }
}

struct s14 {
    signed char[1] pad1;
    unsigned char f1;
};

void fun_6d03(int64_t rdi, struct s14* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26a0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<uint64_t>(rsi)) = 0;
        goto fun_2660;
    }
}

void fun_6d43(struct s0* rdi) {
    struct s0* rax2;
    struct s0* rax3;

    __asm__("cli ");
    rax2 = fun_2530(rdi);
    rax3 = fun_26a0(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2660;
    }
}

void fun_6d83() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2510();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2720();
    fun_2460(rdi1);
}

int32_t mgetgroups();

int64_t fun_6dc3() {
    int32_t eax1;
    struct s0* rax2;
    int64_t rax3;

    __asm__("cli ");
    eax1 = mgetgroups();
    if (eax1 != -1 || (rax2 = fun_2470(), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax2->f0) == 12))) {
        *reinterpret_cast<int32_t*>(&rax3) = eax1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        return rax3;
    } else {
        xalloc_die();
    }
}

void fun_25c0(int64_t rdi);

uint64_t fun_2780(struct s0* rdi);

int64_t fun_6df3(struct s0* rdi, struct s0** rsi, uint32_t edx, uint64_t* rcx, struct s0* r8) {
    uint64_t* v6;
    struct s0* rax7;
    struct s0* v8;
    struct s0* rcx9;
    uint64_t rbx10;
    uint32_t r12d11;
    void* r9_12;
    struct s0* rax13;
    struct s0** r15_14;
    void* rax15;
    int64_t rax16;
    struct s0* rbp17;
    struct s0* r13_18;
    struct s0* rax19;
    struct s0* r12_20;
    struct s0** rax21;
    struct s0* rax22;
    int64_t rdx23;
    uint64_t rax24;
    int64_t rbp25;
    struct s0* rax26;
    int64_t rax27;
    struct s0* rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    struct s0* rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (edx > 36) {
        rcx9 = reinterpret_cast<struct s0*>("xstrtoul");
        rsi = reinterpret_cast<struct s0**>("lib/xstrtol.c");
        fun_25c0("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2550();
            while (1) {
                rbx10 = 0xffffffffffffffff;
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_7164_6;
                    rbx10 = rbx10 * reinterpret_cast<unsigned char>(rcx9);
                } while (!__intrinsic());
            }
            addr_7164_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *r15_14 = rax13;
            if (*reinterpret_cast<struct s0**>(&rax13->f0)) {
                r12d11 = r12d11 | 2;
            }
            addr_6ead_12:
            *v6 = rbx10;
            addr_6eb5_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2470();
    *reinterpret_cast<struct s0**>(&rax19->f0) = reinterpret_cast<struct s0*>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0));
    rax21 = fun_2810(rdi);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx23 * 2) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = rax22->f1;
        rax22 = reinterpret_cast<struct s0*>(&rax22->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_6eeb_21;
    rsi = r15_14;
    rax24 = fun_2780(rbp17);
    r8 = *r15_14;
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx10) + 4) = 0, rax26 = fun_2580(r13_18), r8 = r8, rax26 == 0))) {
            addr_6eeb_21:
            r12d11 = 4;
            goto addr_6eb5_13;
        } else {
            addr_6f29_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2580(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = r8->f1;
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(r8->f2 == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<struct s0*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x8d98 + rbp33 * 4) + 0x8d98;
        }
    } else {
        if (*reinterpret_cast<struct s0**>(&r12_20->f0)) {
            r12d11 = 1;
            if (*reinterpret_cast<struct s0**>(&r12_20->f0) != 34) 
                goto addr_6eeb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_6ead_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_6ead_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2580(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_6f29_24;
    }
    r12d11 = r12d11 | 2;
    *v6 = rbx10;
    goto addr_6eb5_13;
}

uint64_t fun_2770(struct s0* rdi);

int64_t fun_7223(struct s0* rdi, struct s0** rsi, uint32_t edx, uint64_t* rcx, struct s0* r8) {
    uint64_t* v6;
    struct s0* rax7;
    struct s0* v8;
    struct s0* rcx9;
    uint64_t rbx10;
    uint32_t r12d11;
    void* r9_12;
    struct s0* rax13;
    struct s0** r15_14;
    void* rax15;
    int64_t rax16;
    struct s0* rbp17;
    struct s0* r13_18;
    struct s0* rax19;
    struct s0* r12_20;
    struct s0** rax21;
    struct s0* rax22;
    int64_t rdx23;
    uint64_t rax24;
    int64_t rbp25;
    struct s0* rax26;
    int64_t rax27;
    struct s0* rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    struct s0* rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (edx > 36) {
        rcx9 = reinterpret_cast<struct s0*>("xstrtoumax");
        rsi = reinterpret_cast<struct s0**>("lib/xstrtol.c");
        fun_25c0("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2550();
            while (1) {
                rbx10 = 0xffffffffffffffff;
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_7594_6;
                    rbx10 = rbx10 * reinterpret_cast<unsigned char>(rcx9);
                } while (!__intrinsic());
            }
            addr_7594_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *r15_14 = rax13;
            if (*reinterpret_cast<struct s0**>(&rax13->f0)) {
                r12d11 = r12d11 | 2;
            }
            addr_72dd_12:
            *v6 = rbx10;
            addr_72e5_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2470();
    *reinterpret_cast<struct s0**>(&rax19->f0) = reinterpret_cast<struct s0*>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0));
    rax21 = fun_2810(rdi);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx23 * 2) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = rax22->f1;
        rax22 = reinterpret_cast<struct s0*>(&rax22->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_731b_21;
    rsi = r15_14;
    rax24 = fun_2770(rbp17);
    r8 = *r15_14;
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx10) + 4) = 0, rax26 = fun_2580(r13_18), r8 = r8, rax26 == 0))) {
            addr_731b_21:
            r12d11 = 4;
            goto addr_72e5_13;
        } else {
            addr_7359_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2580(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = r8->f1;
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(r8->f2 == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<struct s0*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x8e80 + rbp33 * 4) + 0x8e80;
        }
    } else {
        if (*reinterpret_cast<struct s0**>(&r12_20->f0)) {
            r12d11 = 1;
            if (*reinterpret_cast<struct s0**>(&r12_20->f0) != 34) 
                goto addr_731b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_72dd_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_72dd_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2580(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_7359_24;
    }
    r12d11 = r12d11 | 2;
    *v6 = rbx10;
    goto addr_72e5_13;
}

int64_t fun_24b0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_7653(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    struct s0* rax5;
    struct s0* rax6;

    __asm__("cli ");
    rax2 = fun_24b0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_76ae_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2470();
            *reinterpret_cast<struct s0**>(&rax5->f0) = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_76ae_3;
            rax6 = fun_2470();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax6->f0) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s15 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2680(struct s15* rdi);

int32_t fun_26e0(struct s15* rdi);

int64_t fun_25b0(int64_t rdi, ...);

int32_t rpl_fflush(struct s15* rdi);

int64_t fun_24e0(struct s15* rdi);

int64_t fun_76c3(struct s15* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    struct s0* rax8;
    struct s0* r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2680(rdi);
    if (eax2 >= 0) {
        eax3 = fun_26e0(rdi);
        if (!(eax3 && (eax4 = fun_2680(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_25b0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2470();
            r12d9 = *reinterpret_cast<struct s0**>(&rax8->f0);
            rax10 = fun_24e0(rdi);
            if (r12d9) {
                *reinterpret_cast<struct s0**>(&rax8->f0) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_24e0;
}

void rpl_fseeko(struct s15* rdi);

void fun_7753(struct s15* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_26e0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_77a3(struct s15* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2680(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_25b0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

struct s16 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_7823(uint64_t rdi, struct s16* rsi) {
    uint64_t rcx3;
    signed char* r8_4;
    uint64_t rdx5;
    uint64_t rsi6;
    uint64_t rax7;
    int32_t eax8;
    uint64_t rax9;

    __asm__("cli ");
    rsi->f14 = 0;
    rcx3 = rdi;
    r8_4 = &rsi->f14;
    do {
        --r8_4;
        rdx5 = __intrinsic() >> 3;
        rsi6 = rdx5 + rdx5 * 4;
        rax7 = rcx3 - (rsi6 + rsi6);
        eax8 = *reinterpret_cast<int32_t*>(&rax7) + 48;
        *r8_4 = *reinterpret_cast<signed char*>(&eax8);
        rax9 = rcx3;
        rcx3 = rdx5;
    } while (rax9 > 9);
    return r8_4;
}

signed char* fun_26c0(int64_t rdi);

signed char* fun_7883() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_26c0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2570(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_78c3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2570(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2550();
    } else {
        return r12_7;
    }
}

int64_t fun_2690();

int32_t fun_2740(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx);

int64_t fun_7953(int64_t rdi, struct s0* esi, struct s0* rdx, struct s0* rcx) {
    struct s0* ebp5;
    struct s0* v6;
    struct s0* rax7;
    struct s0* v8;
    int64_t rax9;
    int64_t r12_10;
    struct s0* rax11;
    struct s0* rax12;
    int32_t r12d13;
    struct s0* rax14;
    struct s0* r13_15;
    struct s0* rax16;
    int64_t rbx17;
    int32_t v18;
    struct s0* rax19;
    struct s0* r14_20;
    struct s0* r13_21;
    int64_t rsi22;
    int32_t eax23;
    uint64_t rsi24;
    int64_t rax25;
    struct s0* rax26;
    void* rax27;
    int64_t rax28;
    struct s0* rsi29;
    int64_t rax30;
    struct s0* esi31;
    struct s0** rcx32;
    struct s0** rax33;
    int64_t rax34;
    struct s0* rax35;

    __asm__("cli ");
    ebp5 = esi;
    v6 = rdx;
    rax7 = g28;
    v8 = rax7;
    if (!rdi) {
        rax9 = fun_2690();
        *reinterpret_cast<int32_t*>(&r12_10) = *reinterpret_cast<int32_t*>(&rax9);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_10) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax9) < 0) {
            rax11 = fun_2470();
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax11->f0) == 38) || (rax12 = fun_26a0(4), rax12 == 0)) {
                addr_7b94_4:
                r12d13 = -1;
            } else {
                r12d13 = 0;
                *reinterpret_cast<struct s0**>(&rax12->f0) = ebp5;
                *reinterpret_cast<unsigned char*>(&r12d13) = reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(ebp5 == 0xffffffff));
                *reinterpret_cast<struct s0**>(&v6->f0) = rax12;
            }
        } else {
            if (!*reinterpret_cast<int32_t*>(&rax9) || ebp5 != 0xffffffff) {
                rax14 = fun_26a0(reinterpret_cast<uint64_t>(static_cast<int64_t>(static_cast<int32_t>(r12_10 + 1))) << 2);
                r13_15 = rax14;
                if (!rax14) 
                    goto addr_7b94_4;
                if (ebp5 == 0xffffffff) 
                    goto addr_7b9f_9; else 
                    goto addr_7abd_10;
            } else {
                rax16 = fun_26a0(reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rax9))) << 2);
                r13_15 = rax16;
                if (!rax16) 
                    goto addr_7b94_4; else 
                    goto addr_7b76_12;
            }
        }
    } else {
        rbx17 = rdi;
        v18 = 10;
        rax19 = fun_26a0(40);
        r14_20 = rax19;
        if (!rax19) 
            goto addr_7b94_4;
        r12d13 = 10;
        r13_21 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 40 - 8 + 8 + 20);
        while (1) {
            *reinterpret_cast<struct s0**>(&rsi22) = ebp5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi22) + 4) = 0;
            eax23 = fun_2740(rbx17, rsi22, r14_20, r13_21);
            if (eax23 >= 0) {
                r12d13 = v18;
            } else {
                if (v18 == r12d13) {
                    r12d13 = r12d13 + r12d13;
                    v18 = r12d13;
                } else {
                    r12d13 = v18;
                }
            }
            rsi24 = reinterpret_cast<uint64_t>(static_cast<int64_t>(r12d13));
            *reinterpret_cast<uint32_t*>(&rax25) = reinterpret_cast<uint1_t>(!!(rsi24 >> 62));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax25) + 4) = 0;
            if (__undefined()) 
                goto addr_7a10_21;
            if (rax25) 
                goto addr_7a10_21;
            rax26 = fun_26f0(r14_20);
            if (!rax26) 
                goto addr_7a1b_24;
            if (eax23 >= 0) 
                goto addr_7a60_26;
            r14_20 = rax26;
        }
    }
    addr_7a29_28:
    rax27 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
    if (rax27) {
        fun_2550();
    } else {
        *reinterpret_cast<int32_t*>(&rax28) = r12d13;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
        return rax28;
    }
    addr_7b9f_9:
    addr_7b76_12:
    rsi29 = r13_15;
    rax30 = fun_2690();
    r12d13 = *reinterpret_cast<int32_t*>(&rax30);
    if (*reinterpret_cast<int32_t*>(&rax30) >= 0) {
        addr_7ad9_31:
        *reinterpret_cast<struct s0**>(&v6->f0) = r13_15;
        if (r12d13 > 1) {
            esi31 = *reinterpret_cast<struct s0**>(&r13_15->f0);
            rcx32 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r13_15) + r12d13 * 4);
            rax33 = &r13_15->f4;
            if (reinterpret_cast<uint64_t>(rcx32) > reinterpret_cast<uint64_t>(rax33)) {
                do {
                    if (*rax33 == esi31 || *rax33 == *reinterpret_cast<struct s0**>(&r13_15->f0)) {
                        --r12d13;
                    } else {
                        r13_15->f4 = *rax33;
                        r13_15 = reinterpret_cast<struct s0*>(&r13_15->f4);
                    }
                    rax33 = rax33 + 4;
                } while (reinterpret_cast<uint64_t>(rcx32) > reinterpret_cast<uint64_t>(rax33));
                goto addr_7a29_28;
            } else {
                goto addr_7a29_28;
            }
        }
    } else {
        addr_7b8c_38:
        fun_2450(r13_15, rsi29, rdx, rcx);
        goto addr_7b94_4;
    }
    addr_7abd_10:
    rsi29 = reinterpret_cast<struct s0*>(&rax14->f4);
    rax34 = fun_2690();
    if (*reinterpret_cast<int32_t*>(&rax34) < 0) 
        goto addr_7b8c_38;
    *reinterpret_cast<struct s0**>(&r13_15->f0) = ebp5;
    r12d13 = static_cast<int32_t>(rax34 + 1);
    goto addr_7ad9_31;
    addr_7a10_21:
    rax35 = fun_2470();
    *reinterpret_cast<struct s0**>(&rax35->f0) = reinterpret_cast<struct s0*>(12);
    addr_7a1b_24:
    r12d13 = -1;
    fun_2450(r14_20, rsi24 << 2, r14_20, r13_21);
    goto addr_7a29_28;
    addr_7a60_26:
    *reinterpret_cast<struct s0**>(&v6->f0) = rax26;
    goto addr_7a29_28;
}

int32_t setlocale_null_r();

int64_t fun_7bb3() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2550();
    } else {
        return rax3;
    }
}

int64_t fun_7c33(int64_t rdi, signed char* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2700(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2530(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2660(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2660(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7ce3() {
    __asm__("cli ");
    goto fun_2700;
}

void fun_7cf3() {
    __asm__("cli ");
}

void fun_7d07() {
    __asm__("cli ");
    return;
}

uint32_t fun_2600(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, unsigned char* rsi);

int32_t fun_2800(int64_t rdi, unsigned char* rsi);

uint32_t fun_27f0(struct s0* rdi, unsigned char* rsi);

void fun_3a25() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rax10;
    struct s0* r11_11;
    struct s0* v12;
    int32_t ebp13;
    struct s0* rax14;
    void* r15_15;
    int32_t ebx16;
    struct s0* rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    void* rsi30;
    void* v31;
    struct s0* v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rdx44;
    struct s0* rcx45;
    uint32_t edx46;
    unsigned char al47;
    struct s0* v48;
    int64_t v49;
    struct s0* v50;
    struct s0* v51;
    struct s0* rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    struct s0* v61;
    unsigned char v62;
    void* v63;
    void* v64;
    struct s0* v65;
    signed char* v66;
    struct s0* r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void* r14_71;
    struct s0* r13_72;
    unsigned char* rsi73;
    void* v74;
    struct s0* r15_75;
    unsigned char* rdx76;
    void* v77;
    int64_t rax78;
    int64_t rdi79;
    int32_t v80;
    int32_t eax81;
    void* rdi82;
    uint32_t edx83;
    unsigned char v84;
    void* rdi85;
    void* v86;
    uint32_t esi87;
    uint32_t ebp88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    uint32_t eax94;
    void* rdx95;
    void* rcx96;
    void* v97;
    struct s0** rax98;
    uint1_t zf99;
    int32_t ecx100;
    uint32_t ecx101;
    uint32_t edi102;
    int32_t ecx103;
    uint32_t edi104;
    uint32_t edx105;
    uint32_t edi106;
    uint32_t edx107;
    int64_t rax108;
    uint32_t eax109;
    uint32_t r12d110;
    int64_t rax111;
    int64_t rax112;
    uint32_t r12d113;
    void* v114;
    void* rdx115;
    void* rax116;
    void* v117;
    uint64_t rax118;
    int64_t v119;
    int64_t rax120;
    int64_t rax121;
    int64_t rax122;
    int64_t v123;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2510();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_2510();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx17->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2530(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3d23_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3d23_22; else 
                            goto addr_411d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_41dd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_4530_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3d20_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3d20_30; else 
                                goto addr_4549_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2530(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_4530_28;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2600(rdi39, v26, v28, rcx45);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_4530_28; else 
                            goto addr_3bcc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4690_39:
                    *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_4510_40:
                        if (r11_27 == 1) {
                            addr_409d_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_4658_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx45);
                                goto addr_3cd7_44;
                            }
                        } else {
                            goto addr_4520_46;
                        }
                    } else {
                        addr_469f_47:
                        rax24 = v48;
                        if (!rax24->f1) {
                            goto addr_409d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3d23_22:
                                if (v49 != 1) {
                                    addr_4279_52:
                                    v50 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_2530(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_42c4_54;
                                    }
                                } else {
                                    goto addr_3d30_56;
                                }
                            } else {
                                addr_3cd5_57:
                                ebp36 = 0;
                                goto addr_3cd7_44;
                            }
                        } else {
                            addr_4504_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_469f_47; else 
                                goto addr_450e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_409d_41;
                        if (v49 == 1) 
                            goto addr_3d30_56; else 
                            goto addr_4279_52;
                    }
                }
                addr_3d91_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<struct s0*>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3c28_63:
                    if (!1 && (edx54 = *reinterpret_cast<uint32_t*>(&rcx45), *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<unsigned char*>(&rcx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_3c4d_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_3f50_65;
                    } else {
                        addr_3db9_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_4608_67;
                    }
                } else {
                    goto addr_3db0_69;
                }
                addr_3c61_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_3cac_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_4608_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_3cac_81;
                }
                addr_3db0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_3c4d_64; else 
                    goto addr_3db9_66;
                addr_3cd7_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_3d8f_91;
                if (v22) 
                    goto addr_3cef_93;
                addr_3d8f_91:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_3d91_62;
                addr_42c4_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx45 = r12_67;
                    r13_72 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4a4b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4abb_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72)) + 1);
                        rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = *rdx76 - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_48bf_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    *reinterpret_cast<int32_t*>(&rdi79) = v80;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi79) + 4) = 0;
                    eax81 = fun_2800(rdi79, rsi73);
                    if (!eax81) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_27f0(r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx83 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx83) & reinterpret_cast<unsigned char>(v32));
                addr_43be_109:
                if (reinterpret_cast<uint64_t>(rdi82) <= 1) {
                    addr_3d7c_110:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        edx83 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_43c8_112;
                    }
                } else {
                    addr_43c8_112:
                    v84 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi85 = v86;
                    esi87 = 0;
                    ebp88 = reinterpret_cast<unsigned char>(v22);
                    rcx45 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi82) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4499_114;
                }
                addr_3d88_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_3d8f_91;
                while (1) {
                    addr_4499_114:
                    if (*reinterpret_cast<unsigned char*>(&edx83)) {
                        *reinterpret_cast<unsigned char*>(&esi87) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax89 = esi87;
                        if (*reinterpret_cast<signed char*>(&ebp88)) 
                            goto addr_49a7_117;
                        eax90 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax90) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax90) & *reinterpret_cast<unsigned char*>(&esi87));
                        if (*reinterpret_cast<unsigned char*>(&eax90)) 
                            goto addr_4406_119;
                    } else {
                        eax57 = (esi87 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                            goto addr_49b5_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_4487_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_4487_128;
                        }
                    }
                    addr_4435_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax91 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax91) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax91) >> 6);
                        eax92 = eax91 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax92);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax93 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax93) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax93) >> 3);
                        eax94 = (eax93 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax94);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx45)) 
                        break;
                    esi87 = edx83;
                    addr_4487_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi85) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_4406_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax90;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_4435_134;
                }
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_3cac_81;
                addr_49b5_125:
                ebp36 = v84;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_4608_67;
                addr_4a4b_96:
                rdi82 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx83 = reinterpret_cast<unsigned char>(v32);
                goto addr_43be_109;
                addr_4abb_98:
                r11_27 = v65;
                rdi82 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx95 = rdi82;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx96 = v97;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx96) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx95 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx95) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx95));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi82 = rdx95;
                }
                edx83 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_43be_109;
                addr_3d30_56:
                rax98 = fun_2810(rdi39, rdi39);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi82) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf99 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(*rax98) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf99);
                *reinterpret_cast<unsigned char*>(&edx83) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf99) & reinterpret_cast<unsigned char>(v32));
                goto addr_3d7c_110;
                addr_450e_59:
                goto addr_4510_40;
                addr_41dd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3d23_22;
                *reinterpret_cast<uint32_t*>(&rcx45) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx45));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_3d88_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3cd5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3d23_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4222_160;
                if (!v22) 
                    goto addr_45f7_162; else 
                    goto addr_4803_163;
                addr_4222_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_45f7_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    goto addr_4608_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                    *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                    if (!v32) 
                        goto addr_40cb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                addr_3f33_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_3c61_70; else 
                    goto addr_3f47_169;
                addr_40cb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3c28_63;
                goto addr_3db0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = 0;
                        goto addr_4504_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_463f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3d20_30;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx101 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_3c18_178; else 
                        goto addr_45c2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4504_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3d23_22;
                }
                addr_463f_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3d20_30:
                    r8d42 = 0;
                    goto addr_3d23_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3d91_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx45) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                        goto addr_4658_42;
                    }
                }
                addr_3c18_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx101;
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                goto addr_3c28_63;
                addr_45c2_179:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_4520_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3d91_62;
                } else {
                    addr_45d2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3d23_22;
                }
                edi102 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi102))) 
                    goto addr_4d82_188;
                if (v28) 
                    goto addr_45f7_162;
                addr_4d82_188:
                *reinterpret_cast<uint32_t*>(&rcx45) = 92;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                ebp36 = 0;
                goto addr_3f33_168;
                addr_3bcc_37:
                if (v22) 
                    goto addr_4bc3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3be3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4690_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_471b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3d23_22;
                    ecx103 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx103));
                    ecx101 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_3c18_178; else 
                        goto addr_46f7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_4504_58;
                *reinterpret_cast<uint32_t*>(&rcx45) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3d23_22;
                }
                addr_471b_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3d23_22;
                }
                addr_46f7_199:
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4520_46;
                goto addr_45d2_186;
                addr_3be3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3d23_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3d23_22; else 
                    goto addr_3bf4_206;
            }
            edi104 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx45) = reinterpret_cast<uint1_t>(r15_15 == 0);
            edx105 = edi104 & *reinterpret_cast<uint32_t*>(&rax24);
            if (*reinterpret_cast<unsigned char*>(&rcx45) & *reinterpret_cast<unsigned char*>(&edx105)) 
                goto addr_4cce_208;
            edi106 = edi104 ^ 1;
            edx107 = edi106;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi106));
            if (!rax24) 
                goto addr_4b54_210;
            if (1) 
                goto addr_4b52_212;
            if (!v29) 
                goto addr_478e_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax108 = fun_2520();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v49 = rax108;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_4cc1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_3f50_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax109 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax109)) 
            goto addr_3d0b_219; else 
            goto addr_3f6a_220;
        addr_3cef_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax89 = reinterpret_cast<unsigned char>(v32);
        addr_3d03_221:
        if (*reinterpret_cast<signed char*>(&eax89)) 
            goto addr_3f6a_220; else 
            goto addr_3d0b_219;
        addr_48bf_103:
        r12d110 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d110)) {
            addr_3f6a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax111 = fun_2520();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax111;
        } else {
            addr_48dd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax112 = fun_2520();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax112;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_4d50_225:
        v31 = r13_34;
        addr_47b6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_49a7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3d03_221;
        addr_4803_163:
        eax89 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3d03_221;
        addr_3f47_169:
        goto addr_3f50_65;
        addr_4cce_208:
        r14_35 = r12_21;
        r12d113 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d113)) 
            goto addr_3f6a_220;
        goto addr_48dd_222;
        addr_4b54_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx107) && (*reinterpret_cast<uint32_t*>(&rcx45) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx45)))) {
            rsi30 = v114;
            rdx115 = r15_15;
            rax116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx115)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rsi30) + reinterpret_cast<uint64_t>(rdx115)) = *reinterpret_cast<unsigned char*>(&rcx45);
                }
                rdx115 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx115) + 1);
                *reinterpret_cast<uint32_t*>(&rcx45) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax116) + reinterpret_cast<uint64_t>(rdx115));
                *reinterpret_cast<int32_t*>(&rcx45 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx45));
            r15_15 = rdx115;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v117) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax118 = reinterpret_cast<uint64_t>(v119 - reinterpret_cast<unsigned char>(g28));
        if (!rax118) 
            goto addr_4bae_236;
        fun_2550();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4d50_225;
        addr_4b52_212:
        edx107 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4b54_210;
        addr_478e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx107 = 0;
            goto addr_4b54_210;
        } else {
            goto addr_47b6_226;
        }
        addr_4cc1_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_411d_24:
    *reinterpret_cast<uint32_t*>(&rax120) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax120) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x876c + rax120 * 4) + 0x876c;
    addr_4549_32:
    *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax121) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x886c + rax121 * 4) + 0x886c;
    addr_4bc3_190:
    addr_3d0b_219:
    goto 0x39f0;
    addr_3bf4_206:
    *reinterpret_cast<uint32_t*>(&rax122) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x866c + rax122 * 4) + 0x866c;
    addr_4bae_236:
    goto v123;
}

void fun_3c10() {
}

void fun_3dc8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3ac2;
}

void fun_3e21() {
    goto 0x3ac2;
}

void fun_3f0e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3d91;
    }
    if (v2) 
        goto 0x4803;
    if (!r10_3) 
        goto addr_496e_5;
    if (!v4) 
        goto addr_483e_7;
    addr_496e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_483e_7:
    goto 0x3c44;
}

void fun_3f2c() {
}

void fun_3fd7() {
    signed char v1;

    if (v1) {
        goto 0x3f5f;
    } else {
        goto 0x3c9a;
    }
}

void fun_3ff1() {
    signed char v1;

    if (!v1) 
        goto 0x3fea; else 
        goto "???";
}

void fun_4018() {
    goto 0x3f33;
}

void fun_4098() {
}

void fun_40b0() {
}

void fun_40df() {
    goto 0x3f33;
}

void fun_4131() {
    goto 0x40c0;
}

void fun_4160() {
    goto 0x40c0;
}

void fun_4193() {
    goto 0x40c0;
}

void fun_4560() {
    goto 0x3c18;
}

void fun_485e() {
    signed char v1;

    if (v1) 
        goto 0x4803;
    goto 0x3c44;
}

void fun_4905() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3c44;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3c28;
        goto 0x3c44;
    }
}

void fun_4d22() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x3f90;
    } else {
        goto 0x3ac2;
    }
}

void fun_61e8() {
    fun_2510();
}

void fun_6f9c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x6fab;
}

void fun_706c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x7079;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x6fab;
}

void fun_7090() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_70bc() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x6fab;
}

void fun_70dd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x6fab;
}

void fun_7101() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x6fab;
}

void fun_7125() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x70b4;
}

void fun_7149() {
}

void fun_7169() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x70b4;
}

void fun_7185() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x70b4;
}

void fun_73cc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x73db;
}

void fun_749c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x74a9;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x73db;
}

void fun_74c0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_74ec() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x73db;
}

void fun_750d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x73db;
}

void fun_7531() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x73db;
}

void fun_7555() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x74e4;
}

void fun_7579() {
}

void fun_7599() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x74e4;
}

void fun_75b5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x74e4;
}

void fun_3e4e() {
    goto 0x3ac2;
}

void fun_4024() {
    goto 0x3fdc;
}

void fun_40eb() {
    goto 0x3c18;
}

void fun_413d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x40c0;
    goto 0x3cef;
}

void fun_416f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x40cb;
        goto 0x3af0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x3f6a;
        goto 0x3d0b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x4908;
    if (r10_8 > r15_9) 
        goto addr_4055_9;
    addr_405a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x4913;
    goto 0x3c44;
    addr_4055_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_405a_10;
}

void fun_41a2() {
    goto 0x3cd7;
}

void fun_4570() {
    goto 0x3cd7;
}

void fun_4d0f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x3e2c;
    } else {
        goto 0x3f90;
    }
}

void fun_62a0() {
}

void fun_703f() {
    if (__intrinsic()) 
        goto 0x7079; else 
        goto "???";
}

void fun_746f() {
    if (__intrinsic()) 
        goto 0x74a9; else 
        goto "???";
}

void fun_41ac() {
    goto 0x4147;
}

void fun_457a() {
    goto 0x409d;
}

void fun_6300() {
    fun_2510();
    goto fun_27d0;
}

void fun_3e7d() {
    goto 0x3ac2;
}

void fun_41b8() {
    goto 0x4147;
}

void fun_4587() {
    goto 0x40ee;
}

void fun_6340() {
    fun_2510();
    goto fun_27d0;
}

void fun_3eaa() {
    goto 0x3ac2;
}

void fun_41c4() {
    goto 0x40c0;
}

void fun_6380() {
    fun_2510();
    goto fun_27d0;
}

void fun_3ecc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4860;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3d91;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3d91;
    }
    if (v11) 
        goto 0x4bc3;
    if (r10_12 > r15_13) 
        goto addr_4c13_8;
    addr_4c18_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4951;
    addr_4c13_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4c18_9;
}

struct s17 {
    signed char[24] pad24;
    int64_t f18;
};

struct s18 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s19 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_63d0() {
    int64_t r15_1;
    struct s17* rbx2;
    struct s0* r14_3;
    struct s18* rbx4;
    struct s0* r13_5;
    struct s19* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2510();
    fun_27d0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x63f2, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_6428() {
    fun_2510();
    goto 0x63f9;
}

struct s20 {
    signed char[32] pad32;
    int64_t f20;
};

struct s21 {
    signed char[24] pad24;
    int64_t f18;
};

struct s22 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s23 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s24 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6460() {
    int64_t rcx1;
    struct s20* rbx2;
    int64_t r15_3;
    struct s21* rbx4;
    struct s0* r14_5;
    struct s22* rbx6;
    struct s0* r13_7;
    struct s23* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s24* rbx12;
    struct s0* rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2510();
    fun_27d0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6494, __return_address(), rcx1);
    goto v15;
}

void fun_64d8() {
    fun_2510();
    goto 0x649b;
}
