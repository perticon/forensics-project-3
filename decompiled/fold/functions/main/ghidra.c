byte main(int param_1,undefined8 *param_2)

{
  undefined8 uVar1;
  byte bVar2;
  byte bVar3;
  int iVar4;
  int *piVar5;
  long lVar6;
  undefined8 uVar7;
  undefined8 *puVar8;
  long in_FS_OFFSET;
  undefined local_42;
  undefined local_41;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  uVar7 = 0x50;
  textdomain("coreutils");
  atexit(close_stdout);
  have_read_stdin = '\0';
  count_bytes = 0;
  break_spaces = 0;
  while( true ) {
    iVar4 = getopt_long(param_1,param_2,shortopts,longopts,0);
    if (iVar4 == -1) break;
    if (iVar4 == 0x62) {
LAB_001028a0:
      count_bytes = 1;
    }
    else {
      if (iVar4 < 99) {
        if (iVar4 != -0x82) {
          if (iVar4 < -0x81) {
            if (iVar4 == -0x83) {
              version_etc(stdout,&DAT_00107004,"GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
              exit(0);
            }
          }
          else if (iVar4 - 0x30U < 10) {
            if (optarg == (undefined *)0x0) {
              local_42 = (undefined)iVar4;
              optarg = &local_42;
              local_41 = 0;
            }
            else {
              optarg = optarg + -1;
            }
            goto LAB_00102850;
          }
LAB_00102895:
          usage();
          goto LAB_001028a0;
        }
        usage(0);
        goto LAB_00102981;
      }
      if (iVar4 == 0x73) {
        break_spaces = 1;
      }
      else {
        if (iVar4 != 0x77) goto LAB_00102895;
LAB_00102850:
        uVar7 = dcgettext(0,"invalid number of columns",5);
        uVar7 = xdectoumax(optarg,1,0xfffffffffffffff6,"",uVar7,0);
      }
    }
  }
  if (optind == param_1) goto LAB_00102960;
  if (optind < param_1) {
    param_1 = param_1 - optind;
    lVar6 = (long)optind;
    puVar8 = param_2 + lVar6;
    bVar3 = 1;
    do {
      uVar1 = *puVar8;
      puVar8 = puVar8 + 1;
      bVar2 = fold_file(uVar1,uVar7);
      bVar3 = bVar3 & bVar2;
    } while (puVar8 != param_2 + (ulong)(param_1 - 1) + lVar6 + 1);
  }
  else {
    bVar3 = 1;
  }
  while (have_read_stdin != '\0') {
    iVar4 = rpl_fclose(stdin);
    if (iVar4 != -1) break;
    piVar5 = __errno_location();
    error(1,*piVar5,&DAT_001070ee);
LAB_00102960:
    bVar3 = fold_file(&DAT_001070ee);
  }
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return bVar3 ^ 1;
  }
LAB_00102981:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}