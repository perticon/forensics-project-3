uint32_t fold_file(void** rdi, void** rsi, void** rdx, void** rcx) {
    int1_t zf5;
    void** v6;
    void** v7;
    void** rax8;
    void** r14_9;
    int32_t* rax10;
    int32_t* v11;
    void** rsi12;
    void** rdi13;
    void** r12_14;
    void** r13_15;
    int32_t* rax16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char v19;
    uint32_t eax20;
    void** r15_21;
    uint32_t eax22;
    void** rax23;
    uint32_t eax24;
    uint32_t ebx25;
    void** rbp26;
    int1_t cf27;
    void** rax28;
    uint32_t eax29;
    uint32_t eax30;
    int1_t zf31;
    void** rbx32;
    unsigned char** rax33;
    uint32_t eax34;
    uint32_t eax35;
    void** rdi36;
    void** rax37;
    void** r8_38;
    int1_t zf39;
    uint32_t eax40;
    int32_t ebp41;
    void** rdi42;
    int64_t rax43;

    zf5 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45);
    v6 = rdi;
    v7 = rsi;
    if (!zf5 || *reinterpret_cast<void***>(rdi + 1)) {
        rax8 = fun_2630(v6, "r");
        r14_9 = rax8;
        rax10 = fun_23b0(v6, v6);
        v11 = rax10;
        if (!r14_9) {
            addr_2e43_3:
            quotearg_n_style_colon();
            fun_2610();
            return 0;
        } else {
            addr_2ac8_4:
            *reinterpret_cast<int32_t*>(&rsi12) = 2;
            *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
            rdi13 = r14_9;
            *reinterpret_cast<int32_t*>(&r12_14) = 0;
            *reinterpret_cast<int32_t*>(&r12_14 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r13_15) = 0;
            *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
            fadvise(rdi13, 2);
            goto addr_2adb_5;
        }
    } else {
        r14_9 = stdin;
        have_read_stdin = 1;
        rax16 = fun_23b0(rdi);
        v11 = rax16;
        if (r14_9) 
            goto addr_2ac8_4; else 
            goto addr_2e43_3;
    }
    while (1) {
        addr_2c08_7:
        ++r13_15;
        while (1) {
            eax17 = v18;
            v19 = *reinterpret_cast<unsigned char*>(&eax17);
            while (1) {
                while (1) {
                    if (reinterpret_cast<unsigned char>(v7) >= reinterpret_cast<unsigned char>(r13_15)) {
                        while (1) {
                            eax20 = v19;
                            r15_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(r12_14));
                            ++r12_14;
                            v18 = *reinterpret_cast<unsigned char*>(&eax20);
                            while (1) {
                                addr_2dce_11:
                                eax22 = v18;
                                *reinterpret_cast<void***>(r15_21) = *reinterpret_cast<void***>(&eax22);
                                while (1) {
                                    addr_2adb_5:
                                    rax23 = *reinterpret_cast<void***>(r14_9 + 8);
                                    if (reinterpret_cast<unsigned char>(rax23) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_9 + 16))) {
                                        addr_2b9d_12:
                                        rdi13 = r14_9;
                                        eax24 = fun_2380(rdi13, rsi12, rdx, rcx);
                                        ebx25 = eax24;
                                        if (eax24 == 0xffffffff) 
                                            goto addr_2bb0_13;
                                    } else {
                                        addr_2ae9_14:
                                        rdx = rax23 + 1;
                                        *reinterpret_cast<void***>(r14_9 + 8) = rdx;
                                        ebx25 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax23));
                                    }
                                    rbp26 = r12_14 + 1;
                                    cf27 = reinterpret_cast<unsigned char>(rbp26) < reinterpret_cast<unsigned char>(allocated_out_1);
                                    r15_21 = line_out_0;
                                    if (!cf27) {
                                        rdi13 = r15_21;
                                        rsi12 = reinterpret_cast<void**>(0xb0d8);
                                        rax28 = x2realloc(rdi13, 0xb0d8, rdx, rcx);
                                        line_out_0 = rax28;
                                        r15_21 = rax28;
                                        if (ebx25 != 10) 
                                            break;
                                    } else {
                                        if (ebx25 != 10) 
                                            break;
                                    }
                                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(r12_14)) = 10;
                                    rdx = rbp26;
                                    *reinterpret_cast<int32_t*>(&rsi12) = 1;
                                    *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
                                    rdi13 = r15_21;
                                    rcx = stdout;
                                    *reinterpret_cast<int32_t*>(&r12_14) = 0;
                                    *reinterpret_cast<int32_t*>(&r12_14 + 4) = 0;
                                    *reinterpret_cast<int32_t*>(&r13_15) = 0;
                                    *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
                                    fun_25c0(rdi13, 1, rdx, rcx);
                                }
                                v18 = *reinterpret_cast<unsigned char*>(&ebx25);
                                eax29 = count_bytes;
                                addr_2b21_20:
                                if (*reinterpret_cast<signed char*>(&eax29)) 
                                    goto addr_2c08_7;
                                if (v18 != 8) 
                                    break;
                                if (r13_15) 
                                    goto addr_2de9_23;
                                r15_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(r12_14));
                                ++r12_14;
                            }
                            addr_2b34_25:
                            eax30 = v18;
                            if (*reinterpret_cast<signed char*>(&eax30) != 13) 
                                break;
                            addr_2db6_26:
                            v19 = 13;
                            *reinterpret_cast<int32_t*>(&r13_15) = 0;
                            *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
                        }
                    } else {
                        zf31 = break_spaces == 0;
                        if (!zf31) {
                            rbx32 = r12_14;
                            while (rbx32) {
                                rax33 = fun_26b0(rdi13, rsi12, rdx, rcx);
                                *reinterpret_cast<uint32_t*>(&rcx) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(rbx32) + 0xffffffffffffffff);
                                *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                                if ((*rax33)[reinterpret_cast<unsigned char>(rcx) * 2] & 1) 
                                    goto addr_2cdc_31;
                                --rbx32;
                            }
                            goto addr_2b78_33;
                        } else {
                            addr_2b78_33:
                            if (!r12_14) {
                                eax34 = v19;
                                *reinterpret_cast<int32_t*>(&r12_14) = 1;
                                *reinterpret_cast<int32_t*>(&r12_14 + 4) = 0;
                                *reinterpret_cast<void***>(r15_21) = *reinterpret_cast<void***>(&eax34);
                                rax23 = *reinterpret_cast<void***>(r14_9 + 8);
                                if (reinterpret_cast<unsigned char>(rax23) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_9 + 16))) 
                                    goto addr_2ae9_14; else 
                                    goto addr_2b9d_12;
                            }
                        }
                    }
                    addr_2b41_35:
                    if (*reinterpret_cast<signed char*>(&eax30) == 9) {
                        v19 = 9;
                        r13_15 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r13_15) & 0xfffffffffffffff8) + 8);
                        continue;
                    } else {
                        eax35 = v18;
                        ++r13_15;
                        v19 = *reinterpret_cast<unsigned char*>(&eax35);
                        continue;
                    }
                    addr_2cdc_31:
                    rcx = stdout;
                    fun_25c0(r15_21, 1, rbx32, rcx);
                    rdi36 = stdout;
                    rax37 = *reinterpret_cast<void***>(rdi36 + 40);
                    if (reinterpret_cast<unsigned char>(rax37) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi36 + 48))) {
                        fun_24b0();
                    } else {
                        rcx = rax37 + 1;
                        *reinterpret_cast<void***>(rdi36 + 40) = rcx;
                        *reinterpret_cast<void***>(rax37) = reinterpret_cast<void**>(10);
                    }
                    r15_21 = line_out_0;
                    r12_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_14) - reinterpret_cast<unsigned char>(rbx32));
                    rdx = r12_14;
                    rsi12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(rbx32));
                    rdi13 = r15_21;
                    fun_2600(rdi13, rsi12, rdx, rcx);
                    eax29 = count_bytes;
                    if (!r12_14) {
                        if (*reinterpret_cast<signed char*>(&eax29)) 
                            goto addr_2f67_42;
                        *reinterpret_cast<int32_t*>(&r13_15) = 0;
                        *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
                        if (v18 != 8) 
                            goto addr_2b34_25;
                        *reinterpret_cast<int32_t*>(&r12_14) = 1;
                        *reinterpret_cast<int32_t*>(&r12_14 + 4) = 0;
                        goto addr_2dce_11;
                    } else {
                        rdx = r15_21;
                        rsi12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_14) + reinterpret_cast<unsigned char>(r15_21));
                        *reinterpret_cast<int32_t*>(&r13_15) = 0;
                        *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
                        do {
                            *reinterpret_cast<uint32_t*>(&rcx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx));
                            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
                            if (!*reinterpret_cast<signed char*>(&eax29)) {
                                if (*reinterpret_cast<signed char*>(&rcx) == 8) {
                                    r13_15 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r13_15 + 0xffffffffffffffff) + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r13_15) < reinterpret_cast<unsigned char>(1)));
                                } else {
                                    if (*reinterpret_cast<signed char*>(&rcx) == 13) {
                                        *reinterpret_cast<int32_t*>(&r13_15) = 0;
                                        *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
                                    } else {
                                        r8_38 = r13_15;
                                        ++r13_15;
                                        if (*reinterpret_cast<signed char*>(&rcx) == 9) {
                                            r13_15 = reinterpret_cast<void**>((reinterpret_cast<unsigned char>(r8_38) & 0xfffffffffffffff8) + 8);
                                        }
                                    }
                                }
                            } else {
                                ++r13_15;
                            }
                            ++rdx;
                        } while (rdx != rsi12);
                        goto addr_2b21_20;
                    }
                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_21) + reinterpret_cast<unsigned char>(r12_14)) = 10;
                    rcx = stdout;
                    rdi13 = r15_21;
                    rdx = r12_14 + 1;
                    *reinterpret_cast<int32_t*>(&rsi12) = 1;
                    *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
                    fun_25c0(rdi13, 1, rdx, rcx);
                    zf39 = count_bytes == 0;
                    r15_21 = line_out_0;
                    if (!zf39) 
                        goto addr_2ca2_56;
                    if (v18 != 8) 
                        goto addr_2da3_58;
                    eax40 = v18;
                    *reinterpret_cast<int32_t*>(&r12_14) = 1;
                    *reinterpret_cast<int32_t*>(&r12_14 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r13_15) = 0;
                    *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
                    *reinterpret_cast<void***>(r15_21) = *reinterpret_cast<void***>(&eax40);
                    goto addr_2adb_5;
                    addr_2da3_58:
                    eax30 = v18;
                    *reinterpret_cast<int32_t*>(&r12_14) = 0;
                    *reinterpret_cast<int32_t*>(&r12_14 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r13_15) = 0;
                    *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&eax30) != 13) 
                        goto addr_2b41_35; else 
                        goto addr_2db6_26;
                }
                addr_2de9_23:
                v19 = 8;
                --r13_15;
            }
            addr_2ca2_56:
            *reinterpret_cast<int32_t*>(&r13_15) = 1;
            *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_14) = 0;
            *reinterpret_cast<int32_t*>(&r12_14 + 4) = 0;
            continue;
            addr_2f67_42:
            *reinterpret_cast<int32_t*>(&r13_15) = 1;
            *reinterpret_cast<int32_t*>(&r13_15 + 4) = 0;
        }
    }
    addr_2bb0_13:
    ebp41 = *v11;
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_9)) & 32)) {
        ebp41 = 0;
    }
    if (r12_14) {
        rcx = stdout;
        rdi42 = line_out_0;
        rdx = r12_14;
        *reinterpret_cast<int32_t*>(&rsi12) = 1;
        *reinterpret_cast<int32_t*>(&rsi12 + 4) = 0;
        fun_25c0(rdi42, 1, rdx, rcx);
    }
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v6) == 45) || *reinterpret_cast<void***>(v6 + 1)) {
        rax43 = rpl_fclose(r14_9, rsi12, rdx, rcx);
        if (*reinterpret_cast<int32_t*>(&rax43)) {
            if (ebp41) {
                quotearg_n_style_colon();
                fun_2610();
                return 0;
            } else {
                ebp41 = *v11;
            }
        }
    } else {
        fun_2400(r14_9, rsi12, rdx, rcx);
    }
    if (!ebp41) {
        return 1;
    }
}