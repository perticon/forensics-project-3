word32 fold_file(Eq_32 rsi, struct Eq_334 * rdi)
{
	struct Eq_682 * fs;
	word32 * qwLoc40_689;
	FILE * r14_111;
	if (rdi->b0000 != 0x2D || rdi->b0001 != 0x00)
	{
		FILE * rax_42 = fn0000000000002630("r", rdi);
		r14_111 = rax_42;
		qwLoc40_689 = fn00000000000023B0();
		if (rax_42 != null)
			goto l0000000000002AC8;
	}
	else
	{
		r14_111 = stdin;
		g_bB0E0 = 0x01;
		qwLoc40_689 = fn00000000000023B0();
		if (r14_111 != null)
		{
l0000000000002AC8:
			fadvise(r14_111);
			Eq_48 r12_114 = 0x00;
			Eq_32 r13_116 = 0x00;
l0000000000002ADB:
			word32 ebx_154;
			byte * rax_128 = r14_111->ptr0008;
			if (rax_128 >= r14_111->ptr0010)
			{
l0000000000002B9D:
				word32 eax_447 = fn0000000000002380(r14_111);
				ebx_154 = eax_447;
				if (eax_447 == ~0x00)
				{
					uint64 rbp_459 = (uint64) *qwLoc40_689;
					word32 ebp_466 = (word32) rbp_459;
					word32 rbp_32_32_565 = SLICE(rbp_459, word32, 32);
					if ((r14_111->t0000 & 0x20) == 0x00)
						ebp_466 = 0x00;
					if (r12_114 != 0x00)
						fn00000000000025C0(stdout, r12_114, 0x01, line_out.0);
					uint64 rbp_561;
					uint64 rbp_1061;
					if (rdi->b0000 == 0x2D && rdi->b0001 == 0x00)
					{
						fn0000000000002400();
						rbp_1061 = SEQ(rbp_32_32_565, ebp_466);
					}
					else
					{
						rbp_1061 = SEQ(rbp_32_32_565, ebp_466);
						if (rpl_fclose(r14_111) != 0x00)
						{
							rbp_561 = SEQ(rbp_32_32_565, ebp_466);
							if (ebp_466 != 0x00)
							{
l0000000000002EF8:
								quotearg_n_style_colon(0x03, 0x00, fs);
								fn0000000000002610("%s", (word32) rbp_561, 0x00);
								return 0x00;
							}
							rbp_1061 = (uint64) *qwLoc40_689;
						}
					}
					rbp_561 = rbp_1061;
					if ((word32) rbp_1061 == 0x00)
						return 0x01;
					goto l0000000000002EF8;
				}
				goto l0000000000002AF4;
			}
l0000000000002AE9:
			r14_111->ptr0008 = rax_128 + 1;
			ebx_154 = (word32) *rax_128;
l0000000000002AF4:
			byte bl_176 = (byte) ebx_154;
			Eq_48 rbp_138 = (byte) r12_114.u0 + 1;
			byte * r15_141 = line_out.0;
			if (rbp_138 < allocated_out.1)
			{
				if (ebx_154 != 0x0A)
					goto l0000000000002B16;
l0000000000002C42:
				*r15_141 = 0x0A;
				fn00000000000025C0(stdout, rbp_138, 0x01, r15_141);
				r12_114.u0 = 0x00;
				r13_116.u0 = 0x00;
				goto l0000000000002ADB;
			}
			byte * rax_145 = x2realloc(&allocated_out.1, r15_141);
			line_out.0 = rax_145;
			r15_141 = rax_145;
			if (ebx_154 == 0x0A)
				goto l0000000000002C42;
l0000000000002B16:
			byte al_182 = g_bB0E1;
l0000000000002B21:
			byte bLoc51_708;
			byte bLoc52_707 = bl_176;
			if (al_182 == 0x00)
			{
				if (bl_176 != 0x08)
				{
l0000000000002B34:
					if (bl_176 != 0x0D)
						goto l0000000000002B41;
l0000000000002DB6:
					bLoc51_708 = 0x0D;
					r13_116.u0 = 0x00;
l0000000000002DBE:
					r15_141 += r12_114;
					r12_114 = (byte) r12_114.u0 + 1;
					bLoc52_707 = bLoc51_708;
					goto l0000000000002DCE;
				}
				if (r13_116 == 0x00)
				{
					r15_141 += r12_114;
					r12_114 = (byte) r12_114.u0 + 1;
l0000000000002DCE:
					*r15_141 = bLoc52_707;
					goto l0000000000002ADB;
				}
				bLoc51_708 = 0x08;
				--r13_116;
			}
			else
			{
				++r13_116.u0;
l0000000000002C0C:
				bLoc51_708 = bl_176;
			}
			while (true)
			{
				bLoc52_707 = bl_176;
				if (rsi >= r13_116)
					break;
				if (g_bB0E2 != 0x00)
				{
					Eq_48 rbx_235 = r12_114;
					while (rbx_235 != 0x00)
					{
						Eq_48 rbp_245 = rbx_235 - 1;
						if ((Mem228[fn00000000000026B0() + 0x00:word64][CONVERT(Mem228[r15_141 - 1 + rbx_235:byte], byte, uint64) * 0x02] & 0x01) != 0x00)
						{
							fn00000000000025C0(stdout, rbx_235, 0x01, r15_141);
							FILE * rdi_257 = stdout;
							byte * rax_258 = rdi_257->ptr0028;
							if (rax_258 < rdi_257->ptr0030)
							{
								rdi_257->ptr0028 = rax_258 + 1;
								*rax_258 = 0x0A;
							}
							else
								fn00000000000024B0(0x0A, rdi_257);
							r15_141 = line_out.0;
							r12_114 -= rbx_235;
							fn0000000000002600(r12_114, r15_141 + rbx_235, r15_141);
							al_182 = g_bB0E1;
							if (r12_114 != 0x00)
							{
								byte * rdx_297 = r15_141;
								word64 rsi_298 = r12_114 + r15_141;
								r13_116.u0 = 0x00;
								do
								{
									byte cl_312 = *rdx_297;
									if (al_182 != 0x00)
										++r13_116.u0;
									else if (cl_312 != 0x08)
									{
										if (cl_312 != 0x0D)
										{
											++r13_116.u0;
											ui64 r8_329 = r13_116 & ~0x07;
											if (cl_312 == 0x09)
												r13_116 = r8_329 + 0x08;
										}
										else
											r13_116.u0 = 0x00;
									}
									else
										r13_116.u0 = r13_116.u0 + -1 + (word64) (r13_116 < 0x01);
									++rdx_297;
								} while (rdx_297 != rsi_298);
								goto l0000000000002B21;
							}
							if (al_182 != 0x00)
							{
								r13_116.u0 = 0x01;
								goto l0000000000002C0C;
							}
							else
							{
								r13_116.u0 = 0x00;
								if (bl_176 != 0x08)
									goto l0000000000002B34;
								r12_114.u0 = 0x01;
								goto l0000000000002DCE;
							}
						}
						rbx_235 = rbp_245;
					}
				}
				if (r12_114 == 0x00)
				{
					*r15_141 = bLoc51_708;
					r12_114.u0 = 0x01;
					rax_128 = r14_111->ptr0008;
					if (rax_128 >= r14_111->ptr0010)
						goto l0000000000002B9D;
					goto l0000000000002AE9;
				}
				*r15_141 = 0x0A;
				fn00000000000025C0(stdout, (word64) r12_114 + 1, 0x01, r15_141);
				r15_141 = line_out.0;
				if (g_bB0E1 != 0x00)
				{
					r13_116.u0 = 0x01;
					r12_114.u0 = 0x00;
					goto l0000000000002C0C;
				}
				if (bl_176 == 0x08)
				{
					*r15_141 = bl_176;
					r12_114.u0 = 0x01;
					r13_116.u0 = 0x00;
					goto l0000000000002ADB;
				}
				r12_114.u0 = 0x00;
				r13_116.u0 = 0x00;
				if (bl_176 == 0x0D)
					goto l0000000000002DB6;
l0000000000002B41:
				if (bl_176 != 0x09)
				{
					++r13_116.u0;
					bLoc51_708 = bl_176;
				}
				else
				{
					bLoc51_708 = 0x09;
					r13_116 = (r13_116 & ~0x07) + 0x08;
				}
			}
			goto l0000000000002DBE;
		}
	}
	quotearg_n_style_colon(0x03, 0x00, fs);
	fn0000000000002610("%s", *qwLoc40_689, 0x00);
	return 0x00;
}