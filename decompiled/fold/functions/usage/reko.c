void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002680(fn0000000000002440(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002FDE;
	}
	fn00000000000025F0(fn0000000000002440(0x05, "Usage: %s [OPTION]... [FILE]...\n", null), 0x01);
	fn0000000000002520(stdout, fn0000000000002440(0x05, "Wrap input lines in each FILE, writing to standard output.\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "\nWith no FILE, or when FILE is -, read standard input.\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "  -b, --bytes         count bytes rather than columns\n  -s, --spaces        break at spaces\n  -w, --width=WIDTH   use WIDTH columns instead of 80\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002520(stdout, fn0000000000002440(0x05, "      --version     output version information and exit\n", null));
	struct Eq_1124 * rbx_178 = fp - 0xB8 + 16;
	do
	{
		char * rsi_180 = rbx_178->qw0000;
		++rbx_178;
	} while (rsi_180 != null && fn0000000000002540(rsi_180, "fold") != 0x00);
	ptr64 r13_193 = rbx_178->qw0008;
	if (r13_193 != 0x00)
	{
		fn00000000000025F0(fn0000000000002440(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_280 = fn00000000000025E0(null, 0x05);
		if (rax_280 == 0x00 || fn00000000000023C0(0x03, "en_", rax_280) == 0x00)
			goto l0000000000003206;
	}
	else
	{
		fn00000000000025F0(fn0000000000002440(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_32 rax_222 = fn00000000000025E0(null, 0x05);
		if (rax_222 == 0x00 || fn00000000000023C0(0x03, "en_", rax_222) == 0x00)
		{
			fn00000000000025F0(fn0000000000002440(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003243:
			fn00000000000025F0(fn0000000000002440(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002FDE:
			fn0000000000002660(edi);
		}
		r13_193 = 0x7004;
	}
	fn0000000000002520(stdout, fn0000000000002440(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003206:
	fn00000000000025F0(fn0000000000002440(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003243;
}