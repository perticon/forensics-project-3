
struct s0 {
    struct s0* f0;
    unsigned char f1;
    unsigned char f2;
    unsigned char f3;
    signed char[4] pad8;
    struct s0* f8;
    signed char[7] pad16;
    signed char* f10;
    signed char[4] pad24;
    struct s0* f18;
    signed char[7] pad32;
    struct s0* f20;
    signed char[7] pad40;
    uint64_t f28;
    struct s0* f30;
    signed char[7] pad56;
    struct s0* f38;
    signed char[7] pad64;
    uint64_t f40;
};

int64_t fun_24a0();

int64_t fun_23f0(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24a0();
    if (r8d > 10) {
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xa960 + rax11 * 4) + 0xa960;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* g28;

struct s0* fun_2400();

struct s0* slotvec = reinterpret_cast<struct s0*>(0x70);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_2530();

struct s2 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

void free(struct s0* rdi, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

void fun_24c0();

struct s0* quotearg_n_options(struct s0* rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    struct s0* rax8;
    struct s0* r15_9;
    struct s0* v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x4c7f;
    rax8 = fun_2400();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<struct s0**>(&rax8->f0);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
        fun_23f0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xe070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x9251]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x4d0b;
            fun_2530();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xe160) {
                free(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x4d9a);
        }
        *reinterpret_cast<struct s0**>(&rax8->f0) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_24c0();
        } else {
            return r14_19;
        }
    }
}

struct s3 {
    int64_t f0;
    int64_t f8;
    int64_t f10;
    int64_t f18;
};

void* readlink_stk(int32_t edi, int64_t rsi, void* rdx, uint64_t rcx, struct s3* r8, int64_t r9, void* a7) {
    int64_t r13_8;
    int32_t r12d9;
    struct s3* rbx10;
    int64_t v11;
    void* v12;
    void* rbp13;
    uint64_t r14_14;
    uint64_t rax15;
    void* r15_16;
    uint64_t rcx17;
    void* rdx18;
    int64_t rdi19;
    uint64_t rax20;
    void* rax21;
    struct s0* rax22;
    void* rdi23;
    struct s0* r12d24;
    uint64_t r12_25;
    int64_t rax26;
    int64_t rax27;
    void* rax28;
    int64_t rax29;
    struct s0* rax30;
    struct s0* rax31;

    r13_8 = rsi;
    r12d9 = edi;
    rbx10 = r8;
    v11 = r9;
    v12 = a7;
    if (!r8) {
        rbx10 = reinterpret_cast<struct s3*>(0xdbe0);
    }
    if (!rdx) {
        rbp13 = v12;
        *reinterpret_cast<int32_t*>(&r14_14) = 0x400;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_14) + 4) = 0;
    } else {
        rax15 = 0x7fffffffffffffff;
        rbp13 = rdx;
        if (rcx <= 0x7fffffffffffffff) {
            rax15 = rcx;
        }
        r14_14 = rax15;
    }
    r15_16 = rbp13;
    do {
        rcx17 = r14_14;
        rdx18 = r15_16;
        *reinterpret_cast<int32_t*>(&rdi19) = r12d9;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
        rax20 = reinterpret_cast<uint64_t>(v11(rdi19, r13_8, rdx18, rcx17));
        if (reinterpret_cast<int64_t>(rax20) < reinterpret_cast<int64_t>(0)) 
            break;
        if (reinterpret_cast<int64_t>(rax20) < reinterpret_cast<int64_t>(r14_14)) 
            goto addr_6e60_11;
        if (rbp13 != r15_16) {
            rbx10->f10(r15_16, r13_8, rdx18, rcx17);
        }
        if (reinterpret_cast<int64_t>(r14_14) > reinterpret_cast<int64_t>(0x3ffffffffffffffe)) 
            goto addr_6e98_15;
        r14_14 = r14_14 + r14_14 + 1;
        rax21 = reinterpret_cast<void*>(rbx10->f0(r14_14, r13_8, rdx18, rcx17));
        r15_16 = rax21;
    } while (rax21);
    goto addr_6e00_17;
    if (rbp13 == r15_16) {
        *reinterpret_cast<int32_t*>(&r15_16) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_16) + 4) = 0;
    } else {
        rax22 = fun_2400();
        rdi23 = r15_16;
        *reinterpret_cast<int32_t*>(&r15_16) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_16) + 4) = 0;
        r12d24 = *reinterpret_cast<struct s0**>(&rax22->f0);
        rbx10->f10(rdi23, r13_8, rdx18, rcx17);
        *reinterpret_cast<struct s0**>(&rax22->f0) = r12d24;
    }
    addr_6e1c_21:
    return r15_16;
    addr_6e60_11:
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r15_16) + rax20) = 0;
    r12_25 = rax20 + 1;
    if (v12 == r15_16) {
        rax26 = reinterpret_cast<int64_t>(rbx10->f0(r12_25, r13_8, rdx18, rcx17));
        if (!rax26) {
            r14_14 = r12_25;
        }
    } else {
        if (reinterpret_cast<int64_t>(r12_25) < reinterpret_cast<int64_t>(r14_14) && (rbp13 != r15_16 && (rax27 = rbx10->f8, !!rax27))) {
            rax28 = reinterpret_cast<void*>(rax27(r15_16, r12_25, rdx18, rcx17));
            if (rax28) {
                r15_16 = rax28;
            }
            goto addr_6e1c_21;
        }
    }
    addr_6e00_17:
    rax29 = rbx10->f18;
    if (rax29) {
        rax29(r14_14, r13_8, rdx18, rcx17);
    }
    rax30 = fun_2400();
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_16) + 4) = 0;
    *reinterpret_cast<struct s0**>(&rax30->f0) = reinterpret_cast<struct s0*>(12);
    goto addr_6e1c_21;
    addr_6e98_15:
    rax31 = fun_2400();
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_16) + 4) = 0;
    *reinterpret_cast<struct s0**>(&rax31->f0) = reinterpret_cast<struct s0*>(36);
    goto addr_6e1c_21;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xe080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0xa903);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0xa8fc);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0xa907);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0xa8f8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gde00 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gde00;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23a3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x2030;

void fun_23b3() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t getenv = 0x2040;

void fun_23c3() {
    __asm__("cli ");
    goto getenv;
}

int64_t __snprintf_chk = 0x2050;

void fun_23d3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2060;

void fun_23e3() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2070;

void fun_23f3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2080;

void fun_2403() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2090;

void fun_2413() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20a0;

void fun_2423() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20b0;

void fun_2433() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20c0;

void fun_2443() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t readlink = 0x20d0;

void fun_2453() {
    __asm__("cli ");
    goto readlink;
}

int64_t textdomain = 0x20e0;

void fun_2463() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20f0;

void fun_2473() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2100;

void fun_2483() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2110;

void fun_2493() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2120;

void fun_24a3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2130;

void fun_24b3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2140;

void fun_24c3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2150;

void fun_24d3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2160;

void fun_24e3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2170;

void fun_24f3() {
    __asm__("cli ");
    goto strchr;
}

int64_t strrchr = 0x2180;

void fun_2503() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2190;

void fun_2513() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21a0;

void fun_2523() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21b0;

void fun_2533() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x21c0;

void fun_2543() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21d0;

void fun_2553() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21e0;

void fun_2563() {
    __asm__("cli ");
    goto calloc;
}

int64_t putenv = 0x21f0;

void fun_2573() {
    __asm__("cli ");
    goto putenv;
}

int64_t strcmp = 0x2200;

void fun_2583() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2210;

void fun_2593() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x2220;

void fun_25a3() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2230;

void fun_25b3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2240;

void fun_25c3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2250;

void fun_25d3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2260;

void fun_25e3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2270;

void fun_25f3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2280;

void fun_2603() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2290;

void fun_2613() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22a0;

void fun_2623() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22b0;

void fun_2633() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x22c0;

void fun_2643() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t error = 0x22d0;

void fun_2653() {
    __asm__("cli ");
    goto error;
}

int64_t access = 0x22e0;

void fun_2663() {
    __asm__("cli ");
    goto access;
}

int64_t fseeko = 0x22f0;

void fun_2673() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtok = 0x2300;

void fun_2683() {
    __asm__("cli ");
    goto strtok;
}

int64_t strtoumax = 0x2310;

void fun_2693() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t execvp = 0x2320;

void fun_26a3() {
    __asm__("cli ");
    goto execvp;
}

int64_t __cxa_atexit = 0x2330;

void fun_26b3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2340;

void fun_26c3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2350;

void fun_26d3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2360;

void fun_26e3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2370;

void fun_26f3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2380;

void fun_2703() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2390;

void fun_2713() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s0* fun_2620(int64_t rdi, ...);

void fun_2480(int64_t rdi, int64_t rsi);

void fun_2460(int64_t rdi, int64_t rsi);

int32_t exit_failure = 1;

void atexit(int64_t rdi, int64_t rsi);

struct s0* usage();

void fun_2520(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* quote(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* fun_2490();

void fun_2650();

struct s0* optarg = reinterpret_cast<struct s0*>(0);

void xalloc_die();

int32_t optind = 0;

struct s0*** fun_23b0(int64_t rdi, struct s0* rsi);

int32_t rpl_asprintf(void* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int32_t fun_2570(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* fun_24d0(int64_t rdi, struct s0* rsi);

struct s0* xstrtoumax();

int64_t stdout = 0;

int64_t Version = 0xa891;

void version_etc(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s0* fun_26c0();

struct s0* program_name = reinterpret_cast<struct s0*>(0);

int64_t fun_24f0(struct s0* rdi, ...);

struct s0* xreadlink(int64_t rdi, struct s0* rsi);

struct s0* fun_23c0(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* program_path = reinterpret_cast<struct s0*>(0);

struct s0* xstrdup(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

int64_t fun_2680();

struct s0* file_name_concat();

int32_t fun_2660(struct s0* rdi, struct s0* rsi);

int32_t fun_25a0(struct s0* rdi, struct s0* rsi);

struct s0* __cxa_finalize;

void fun_26a0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* dir_name(struct s0* rdi, struct s0* rsi);

int64_t fun_2773(int32_t edi, struct s0* rsi) {
    struct s0* r15_3;
    struct s0* r14_4;
    struct s0* r13_5;
    struct s0* rbp6;
    struct s0* rbx7;
    struct s0* rdi8;
    struct s0* rax9;
    struct s0* v10;
    struct s0* r12_11;
    struct s0** rsp12;
    struct s0* rdx13;
    struct s0* rdx14;
    struct s0* eax15;
    struct s0* rax16;
    struct s0* rcx17;
    struct s0* rdx18;
    struct s0* rsi19;
    struct s0* rax20;
    struct s0* rax21;
    struct s0* rsi22;
    struct s0* r8_23;
    struct s0* rdi24;
    int64_t rdx25;
    int64_t rax26;
    struct s0* rdx27;
    uint32_t edx28;
    int64_t rdi29;
    struct s0*** rax30;
    int32_t eax31;
    void* rsp32;
    struct s0*** rax33;
    int64_t v34;
    int32_t eax35;
    struct s0* rax36;
    struct s0* rax37;
    int64_t r9_38;
    struct s0* rdi39;
    uint32_t edx40;
    struct s0* eax41;
    int64_t rdi42;
    int64_t rcx43;
    struct s0* v44;
    struct s0* v45;
    struct s0* rax46;
    struct s0* rax47;
    struct s0* r12_48;
    int64_t rax49;
    void* rsp50;
    struct s0* rax51;
    void* rsp52;
    struct s0* rbp53;
    struct s0* rax54;
    void* rsp55;
    void* rsp56;
    int1_t zf57;
    struct s0* rax58;
    struct s0* rax59;
    int64_t rax60;
    int64_t rdi61;
    struct s0* rax62;
    int32_t eax63;
    void* rsp64;
    int64_t rax65;
    struct s0* rax66;
    struct s0** rsp67;
    struct s0* v68;
    int1_t zf69;
    void* r12_70;
    struct s0* r13_71;
    int32_t eax72;
    struct s0* v73;
    int32_t eax74;
    struct s0** rsp75;
    int1_t zf76;
    struct s0* rax77;
    struct s0* rcx78;
    void* rdi79;
    struct s0* rdx80;
    struct s0* rsi81;
    int32_t eax82;
    int64_t v83;
    int32_t eax84;
    struct s0* rax85;
    void* rsp86;
    struct s0* rdi87;
    int32_t r12d88;
    struct s0* rdi89;
    struct s0* rdi90;
    struct s0* rax91;
    struct s0* rax92;
    void* rax93;
    struct s0* v94;
    struct s0* rax95;
    struct s0* rax96;
    struct s0* rax97;
    int64_t rax98;
    struct s0* rax99;
    struct s0* rax100;

    __asm__("cli ");
    r15_3 = reinterpret_cast<struct s0*>(0xdac0);
    *reinterpret_cast<int32_t*>(&r14_4) = 0x411;
    *reinterpret_cast<int32_t*>(&r14_4 + 4) = 0;
    r13_5 = reinterpret_cast<struct s0*>(0xe0e0);
    *reinterpret_cast<int32_t*>(&rbp6) = edi;
    *reinterpret_cast<int32_t*>(&rbp6 + 4) = 0;
    rbx7 = rsi;
    rdi8 = *reinterpret_cast<struct s0**>(&rsi->f0);
    rax9 = g28;
    v10 = rax9;
    set_program_name(rdi8);
    fun_2620(6, 6);
    fun_2480("coreutils", "/usr/local/share/locale");
    r12_11 = reinterpret_cast<struct s0*>("+i:o:e:");
    fun_2460("coreutils", "/usr/local/share/locale");
    exit_failure = 0x7d;
    atexit(0x33b0, "/usr/local/share/locale");
    rsp12 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xf8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    goto addr_2805_2;
    addr_2cad_3:
    *reinterpret_cast<uint32_t*>(&rdx13) = reinterpret_cast<unsigned char>(rdx14) - 1;
    *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
    if (!*reinterpret_cast<uint32_t*>(&rdx13)) 
        goto addr_2d50_4;
    eax15 = *reinterpret_cast<struct s0**>(&rax16->f0);
    goto addr_2cb8_6;
    addr_2ca6_7:
    rax16 = usage();
    goto addr_2cad_3;
    while (1) {
        addr_2cff_8:
        rcx17 = reinterpret_cast<struct s0*>("main");
        *reinterpret_cast<int32_t*>(&rdx18) = 0x150;
        *reinterpret_cast<int32_t*>(&rdx18 + 4) = 0;
        rsi19 = reinterpret_cast<struct s0*>("src/stdbuf.c");
        fun_2520("0 <= opt_fileno && opt_fileno < ARRAY_CARDINALITY (stdbuf)", "src/stdbuf.c", 0x150, "main");
        addr_2d1e_9:
        rax20 = quote(rbp6, rsi19, rdx18, rcx17);
        rax21 = fun_2490();
        rcx17 = rax20;
        *reinterpret_cast<int32_t*>(&rsi22) = 0;
        *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
        rdx13 = rax21;
        fun_2650();
        addr_2d50_4:
        eax15 = reinterpret_cast<struct s0*>(75);
        addr_2cb8_6:
        *reinterpret_cast<struct s0**>(&r8_23->f0) = eax15;
        rdi24 = optarg;
        quote(rdi24, rsi22, rdx13, rcx17);
        fun_2490();
        r8_23 = r8_23;
        fun_2650();
        addr_2cfa_10:
        xalloc_die();
    }
    while (1) {
        addr_2980_11:
        rdx25 = optind;
        r14_4 = reinterpret_cast<struct s0*>(0xe0e0);
        r12_11 = reinterpret_cast<struct s0*>("_STDBUF_");
        r13_5 = reinterpret_cast<struct s0*>("%s%c=%lu");
        rax26 = rdx25;
        r15_3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx7) + rdx25 * 8);
        rbx7 = reinterpret_cast<struct s0*>(0xe128);
        *reinterpret_cast<int32_t*>(&rdx27) = 0;
        *reinterpret_cast<int32_t*>(&rdx27 + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rbp6) > *reinterpret_cast<int32_t*>(&rax26)) {
            do {
                if (r14_4->f10) {
                    rcx17 = r14_4->f8;
                    *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                    edx28 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rcx17) + 0x80);
                    if (*r14_4->f10 == 76) {
                        if (edx28 <= 0x17f) {
                            rax30 = fun_23b0(rdi29, rsi22);
                            rsp12 = rsp12 - 8 + 8;
                            rcx17 = (*rax30)[reinterpret_cast<int32_t>(rcx17) * 4];
                            *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                        }
                        rsi22 = reinterpret_cast<struct s0*>("%s%c=L");
                        eax31 = rpl_asprintf(rsp12 + 24, "%s%c=L", "_STDBUF_", rcx17);
                        rsp32 = reinterpret_cast<void*>(rsp12 - 8 + 8);
                    } else {
                        r8_23 = *reinterpret_cast<struct s0**>(&r14_4->f0);
                        if (edx28 <= 0x17f) {
                            rax33 = fun_23b0(rdi29, rsi22);
                            rsp12 = rsp12 - 8 + 8;
                            r8_23 = r8_23;
                            rcx17 = (*rax33)[reinterpret_cast<int32_t>(rcx17) * 4];
                            *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                        }
                        rsi22 = reinterpret_cast<struct s0*>("%s%c=%lu");
                        eax31 = rpl_asprintf(rsp12 + 24, "%s%c=%lu", "_STDBUF_", rcx17);
                        rsp32 = reinterpret_cast<void*>(rsp12 - 8 + 8);
                    }
                    if (eax31 < 0) 
                        goto addr_2cfa_10;
                    rdi29 = v34;
                    eax35 = fun_2570(rdi29, rsi22, "_STDBUF_", rcx17);
                    rsp12 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
                    if (eax35) 
                        goto addr_2d6b_23;
                    *reinterpret_cast<int32_t*>(&rdx27) = 1;
                    *reinterpret_cast<int32_t*>(&rdx27 + 4) = 0;
                }
                r14_4 = reinterpret_cast<struct s0*>(&r14_4->f18);
            } while (!reinterpret_cast<int1_t>(0xe128 == r14_4));
            goto addr_2a2e_26;
        }
        while (1) {
            addr_2966_27:
            rax36 = fun_2490();
            rdx14 = rax36;
            fun_2650();
            rsp12 = rsp12 - 8 + 8 - 8 + 8;
            while (1) {
                rax37 = usage();
                rsp12 = rsp12 - 8 + 8;
                do {
                    r9_38 = *reinterpret_cast<int32_t*>(0xa860 + reinterpret_cast<unsigned char>(rdx14) * 4);
                    if (*reinterpret_cast<uint32_t*>(&r9_38) > 2) 
                        goto addr_2cff_8;
                    rdi39 = optarg;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r13_5) + reinterpret_cast<uint64_t>((r9_38 + r9_38 * 2) * 8) + 8) = *reinterpret_cast<int32_t*>(&rax37);
                    while (1) {
                        edx40 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdi39->f0));
                        if (*reinterpret_cast<signed char*>(&edx40) > 13) {
                            if (*reinterpret_cast<signed char*>(&edx40) != 32) 
                                break;
                        } else {
                            if (*reinterpret_cast<signed char*>(&edx40) <= 8) 
                                break;
                        }
                        rdi39 = reinterpret_cast<struct s0*>(&rdi39->f1);
                        optarg = rdi39;
                    }
                    *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r13_5) + reinterpret_cast<uint64_t>((r9_38 + r9_38 * 2) * 8) + 16) = rdi39;
                    if (*reinterpret_cast<int32_t*>(&rax37) != 0x69) 
                        goto addr_28d4_36;
                    if (reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi39->f0) == 76)) 
                        goto addr_295a_38;
                    addr_28d4_36:
                    if (*reinterpret_cast<struct s0**>(&rdi39->f0) == 76) {
                        if (!rdi39->f1) {
                            addr_2805_2:
                            *reinterpret_cast<int32_t*>(&r8_23) = 0;
                            *reinterpret_cast<int32_t*>(&r8_23 + 4) = 0;
                            rcx17 = r15_3;
                            rdx14 = r12_11;
                            rsi22 = rbx7;
                            *reinterpret_cast<int32_t*>(&rdi29) = *reinterpret_cast<int32_t*>(&rbp6);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
                            rax37 = fun_24d0(rdi29, rsi22);
                            rsp12 = rsp12 - 8 + 8;
                            if (*reinterpret_cast<int32_t*>(&rax37) == -1) 
                                goto addr_2980_11;
                        } else {
                            goto addr_28d9_41;
                        }
                    } else {
                        addr_28d9_41:
                        *reinterpret_cast<int32_t*>(&rsi22) = 0;
                        *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                        rcx17 = reinterpret_cast<struct s0*>(rsp12 + 40);
                        eax41 = xstrtoumax();
                        rax16 = fun_2400();
                        rsp12 = rsp12 - 8 + 8 - 8 + 8;
                        rdx14 = eax41;
                        r8_23 = rax16;
                        if (rdx14) 
                            goto addr_2cad_3; else 
                            goto addr_290f_42;
                    }
                    if (*reinterpret_cast<int32_t*>(&rax37) == 0xffffff7e) 
                        goto addr_2ca6_7;
                    if (*reinterpret_cast<int32_t*>(&rax37) < 0xffffff7f) {
                        if (*reinterpret_cast<int32_t*>(&rax37) != 0xffffff7d) 
                            break;
                        rdi42 = stdout;
                        rcx43 = Version;
                        r8_23 = reinterpret_cast<struct s0*>("Padraig Brady");
                        version_etc(rdi42, "stdbuf", "GNU coreutils", rcx43);
                        rax37 = fun_26c0();
                        rsp12 = rsp12 - 8 + 8 - 8 + 8;
                        continue;
                    }
                    addr_290f_42:
                    *reinterpret_cast<struct s0**>(&rax16->f0) = reinterpret_cast<struct s0*>(0);
                    *reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r13_5) + r9_38 * 24) = v44;
                    goto addr_2805_2;
                    rdx14 = reinterpret_cast<struct s0*>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rax37) + 0xffffffffffffff9b));
                    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
                } while (reinterpret_cast<unsigned char>(rdx14) <= reinterpret_cast<unsigned char>(10) && static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_4) >> reinterpret_cast<unsigned char>(rdx14))));
            }
            addr_295a_38:
        }
        addr_2a2e_26:
        if (*reinterpret_cast<signed char*>(&rdx27)) 
            break;
        addr_2da7_48:
        goto addr_2966_27;
        addr_2d6b_23:
        rax46 = quote(v45, rsi22, "_STDBUF_", rcx17);
        r13_5 = rax46;
        rax47 = fun_2490();
        r12_11 = rax47;
        fun_2400();
        fun_2650();
        rsp12 = rsp12 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        goto addr_2da7_48;
    }
    r12_48 = program_name;
    rsi19 = reinterpret_cast<struct s0*>(47);
    *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
    rax49 = fun_24f0(r12_48, r12_48);
    rsp50 = reinterpret_cast<void*>(rsp12 - 8 + 8);
    if (!rax49) {
        rax51 = xreadlink("/proc/self/exe", 47);
        rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp50) - 8 + 8);
        rbp53 = rax51;
        if (!rax51) {
            while (1) {
                rax54 = fun_23c0("PATH", rsi19, rdx27, rcx17);
                rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
                rbp53 = rax54;
                if (!rax54) {
                    addr_2c5b_52:
                    free(rbp53, rbp53);
                    rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8);
                    zf57 = program_path == 0;
                    if (zf57) {
                        rax58 = xstrdup("/usr/local/lib/coreutils", rsi19, rdx27, rcx17);
                        rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                        program_path = rax58;
                    }
                } else {
                    rax59 = xstrdup(rax54, rsi19, rdx27, rcx17);
                    rsi19 = reinterpret_cast<struct s0*>(":");
                    rbp53 = rax59;
                    rax60 = fun_2680();
                    rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8 - 8 + 8);
                    rdi61 = rax60;
                    while (rdi61) {
                        *reinterpret_cast<int32_t*>(&rdx27) = 0;
                        *reinterpret_cast<int32_t*>(&rdx27 + 4) = 0;
                        rax62 = file_name_concat();
                        rsi19 = reinterpret_cast<struct s0*>(1);
                        *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                        eax63 = fun_2660(rax62, 1);
                        rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp55) - 8 + 8 - 8 + 8);
                        if (!eax63) 
                            goto addr_2e67_57;
                        free(rax62, rax62);
                        rsi19 = reinterpret_cast<struct s0*>(":");
                        rax65 = fun_2680();
                        rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 8 + 8);
                        rdi61 = rax65;
                    }
                    goto addr_2c5b_52;
                }
                addr_2a62_59:
                rax66 = fun_23c0("LD_PRELOAD", rsi19, rdx27, rcx17);
                rsp67 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(rsp56) - 8 + 8);
                rdx18 = program_path;
                v68 = rax66;
                zf69 = *reinterpret_cast<struct s0**>(&rdx18->f0) == 0;
                if (zf69) {
                    rbp6 = reinterpret_cast<struct s0*>("libstdbuf.so");
                } else {
                    r12_70 = reinterpret_cast<void*>(rsp67 + 40);
                    rbp6 = reinterpret_cast<struct s0*>("libstdbuf.so");
                    r13_71 = reinterpret_cast<struct s0*>(rsp67 + 80);
                    do {
                        rcx17 = reinterpret_cast<struct s0*>("libstdbuf.so");
                        eax72 = rpl_asprintf(r12_70, "%s/%s", rdx18, "libstdbuf.so");
                        if (eax72 < 0) 
                            goto addr_2cfa_10;
                        rsi19 = r13_71;
                        eax74 = fun_25a0(v73, rsi19);
                        rsp75 = rsp67 - 8 + 8 - 8 + 8;
                        if (!eax74) 
                            goto addr_2b0c_64;
                        free(v73, v73);
                        rsp67 = rsp75 - 8 + 8;
                        rdx18 = reinterpret_cast<struct s0*>(0);
                        if (1) 
                            goto addr_2d1e_9;
                        zf76 = __cxa_finalize == 0;
                    } while (!zf76);
                }
                rax77 = xstrdup("libstdbuf.so", rsi19, rdx18, rcx17);
                rsp75 = rsp67 - 8 + 8;
                v73 = rax77;
                addr_2b0c_64:
                rcx78 = v68;
                r8_23 = v73;
                rdi79 = reinterpret_cast<void*>(rsp75 + 32);
                if (!rcx78) {
                    rcx78 = r8_23;
                    rdx80 = reinterpret_cast<struct s0*>("LD_PRELOAD");
                    rsi81 = reinterpret_cast<struct s0*>("%s=%s");
                    eax82 = rpl_asprintf(rdi79, "%s=%s", "LD_PRELOAD", rcx78);
                    rsp32 = reinterpret_cast<void*>(rsp75 - 8 + 8);
                } else {
                    rdx80 = reinterpret_cast<struct s0*>("LD_PRELOAD");
                    rsi81 = reinterpret_cast<struct s0*>("%s=%s:%s");
                    eax82 = rpl_asprintf(rdi79, "%s=%s:%s", "LD_PRELOAD", rcx78);
                    rsp32 = reinterpret_cast<void*>(rsp75 - 8 + 8);
                }
                if (eax82 < 0) 
                    goto addr_2cfa_10;
                free(v73, v73);
                eax84 = fun_2570(v83, rsi81, "LD_PRELOAD", rcx78);
                rax85 = fun_2400();
                rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8 - 8 + 8 - 8 + 8);
                if (!eax84) {
                    rdi87 = program_path;
                    r12d88 = 0;
                    free(rdi87, rdi87);
                    rdi89 = *reinterpret_cast<struct s0**>(&r15_3->f0);
                    fun_26a0(rdi89, r15_3, "LD_PRELOAD", rcx78);
                    rdi90 = *reinterpret_cast<struct s0**>(&r15_3->f0);
                    *reinterpret_cast<unsigned char*>(&r12d88) = reinterpret_cast<uint1_t>(*reinterpret_cast<struct s0**>(&rax85->f0) == 2);
                    rax91 = quote(rdi90, r15_3, "LD_PRELOAD", rcx78);
                    rax92 = fun_2490();
                    rsi81 = *reinterpret_cast<struct s0**>(&rax85->f0);
                    *reinterpret_cast<int32_t*>(&rsi81 + 4) = 0;
                    rcx78 = rax91;
                    rdx80 = rax92;
                    fun_2650();
                    rax93 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
                    if (!rax93) 
                        break;
                    fun_24c0();
                    rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                }
                rax95 = quote(v94, rsi81, rdx80, rcx78);
                rax96 = fun_2490();
                rsi19 = *reinterpret_cast<struct s0**>(&rax85->f0);
                *reinterpret_cast<int32_t*>(&rsi19 + 4) = 0;
                rcx17 = rax95;
                rdx27 = rax96;
                fun_2650();
                rsp52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8 - 8 + 8 - 8 + 8);
                continue;
                addr_2e67_57:
                rax97 = dir_name(rax62, 1);
                program_path = rax97;
                free(rax62, rax62);
                rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 8 + 8);
                goto addr_2c5b_52;
            }
            *reinterpret_cast<int32_t*>(&rax98) = r12d88 + 0x7e;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax98) + 4) = 0;
            return rax98;
        } else {
            rax99 = dir_name(rax51, 47);
            rsp55 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp52) - 8 + 8);
            program_path = rax99;
            goto addr_2c5b_52;
        }
    } else {
        rax100 = dir_name(r12_48, 47);
        rsp56 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp50) - 8 + 8);
        program_path = rax100;
        goto addr_2a62_59;
    }
}

int64_t __libc_start_main = 0;

void fun_2e83() {
    __asm__("cli ");
    __libc_start_main(0x2770, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xe008;

void fun_23a0(int64_t rdi);

int64_t fun_2f23() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23a0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2f63() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_2630(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_2550(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int32_t fun_2580(int64_t rdi);

int32_t fun_2410(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int64_t stderr = 0;

void fun_26e0(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2f73(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    int64_t r12_7;
    struct s0* rax8;
    int64_t r12_9;
    struct s0* rax10;
    int64_t r12_11;
    struct s0* rax12;
    int64_t r12_13;
    struct s0* rax14;
    int64_t r12_15;
    struct s0* rax16;
    int64_t r12_17;
    struct s0* rax18;
    int64_t r12_19;
    struct s0* rax20;
    int64_t r12_21;
    struct s0* rax22;
    int64_t r12_23;
    struct s0* rax24;
    int32_t eax25;
    struct s0* r13_26;
    struct s0* rax27;
    struct s0* rax28;
    int32_t eax29;
    struct s0* rax30;
    struct s0* rax31;
    struct s0* rax32;
    int32_t eax33;
    struct s0* rax34;
    int64_t r15_35;
    struct s0* rax36;
    struct s0* rax37;
    struct s0* rax38;
    int64_t rdi39;
    struct s0* r8_40;
    struct s0* r9_41;
    int64_t v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;
    int64_t v46;
    int64_t v47;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2490();
            fun_2630(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2490();
            fun_2550(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_2490();
            fun_2550(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2490();
            fun_2550(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2490();
            fun_2550(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2490();
            fun_2550(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2490();
            fun_2550(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_2490();
            fun_2550(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_2490();
            fun_2550(rax22, r12_21, 5, rcx6);
            r12_23 = stdout;
            rax24 = fun_2490();
            fun_2550(rax24, r12_23, 5, rcx6);
            do {
                if (1) 
                    break;
                eax25 = fun_2580("stdbuf");
            } while (eax25);
            r13_26 = v4;
            if (!r13_26) {
                rax27 = fun_2490();
                fun_2630(1, rax27, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax28 = fun_2620(5);
                if (!rax28 || (eax29 = fun_2410(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax29)) {
                    rax30 = fun_2490();
                    r13_26 = reinterpret_cast<struct s0*>("stdbuf");
                    fun_2630(1, rax30, "https://www.gnu.org/software/coreutils/", "stdbuf");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_26 = reinterpret_cast<struct s0*>("stdbuf");
                    goto addr_3360_9;
                }
            } else {
                rax31 = fun_2490();
                fun_2630(1, rax31, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax32 = fun_2620(5);
                if (!rax32 || (eax33 = fun_2410(rax32, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax33)) {
                    addr_3266_11:
                    rax34 = fun_2490();
                    fun_2630(1, rax34, "https://www.gnu.org/software/coreutils/", "stdbuf");
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_26 == "stdbuf")) {
                        r12_2 = reinterpret_cast<struct s0*>(0xad21);
                    }
                } else {
                    addr_3360_9:
                    r15_35 = stdout;
                    rax36 = fun_2490();
                    fun_2550(rax36, r15_35, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3266_11;
                }
            }
            rax37 = fun_2490();
            rcx6 = r12_2;
            fun_2630(1, rax37, r13_26, rcx6);
            addr_2fce_14:
            fun_26c0();
        }
    } else {
        rax38 = fun_2490();
        rdi39 = stderr;
        rcx6 = r12_2;
        fun_26e0(rdi39, 1, rax38, rcx6, r8_40, r9_41, v42, v43, v44, v45, v46, v47);
        goto addr_2fce_14;
    }
}

int64_t file_name = 0;

void fun_3393(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_33a3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

struct s0* quotearg_colon();

struct s0* fun_2420(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_33b3() {
    int64_t rdi1;
    int32_t eax2;
    struct s0* rax3;
    int1_t zf4;
    struct s0* rbx5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2400(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax3->f0) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2490();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3443_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<struct s0**>(&rsi11) = *reinterpret_cast<struct s0**>(&rbx5->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2650();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2420(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3443_5:
        *reinterpret_cast<struct s0**>(&rsi11) = *reinterpret_cast<struct s0**>(&rbx5->f0);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2650();
    }
}

int64_t mdir_name();

void fun_3463() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mdir_name();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void* last_component();

void fun_3483(signed char* rdi) {
    void* rbp2;
    signed char* rbx3;
    void* rax4;
    void* rax5;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp2) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp2) + 4) = 0;
    rbx3 = rdi;
    *reinterpret_cast<unsigned char*>(&rbp2) = reinterpret_cast<uint1_t>(*rdi == 47);
    rax4 = last_component();
    rax5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<uint64_t>(rbx3));
    while (reinterpret_cast<uint64_t>(rax5) > reinterpret_cast<uint64_t>(rbp2) && *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbx3) + reinterpret_cast<uint64_t>(rax5) - 1) == 47) {
        rax5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - 1);
    }
    return;
}

struct s0* malloc(struct s0* rdi, struct s0* rsi, ...);

struct s0* fun_25b0(struct s0* rdi, struct s0* rsi, struct s0* rdx);

struct s0* fun_34c3(struct s0* rdi, struct s0* rsi) {
    struct s0* rbp3;
    struct s0* rbx4;
    void* rax5;
    struct s0* r12_6;
    void* rax7;
    uint32_t ebx8;
    struct s0* rax9;
    struct s0* r8_10;
    struct s0* rax11;
    struct s0* rax12;
    struct s0* rax13;

    __asm__("cli ");
    rbp3 = rdi;
    *reinterpret_cast<int32_t*>(&rbx4) = 0;
    *reinterpret_cast<int32_t*>(&rbx4 + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rbx4) = reinterpret_cast<uint1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 47);
    rax5 = last_component();
    r12_6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax5) - reinterpret_cast<unsigned char>(rbp3));
    while (reinterpret_cast<unsigned char>(rbx4) < reinterpret_cast<unsigned char>(r12_6)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp3) + reinterpret_cast<unsigned char>(r12_6) + 0xffffffffffffffff) != 47) 
            goto addr_3540_4;
        r12_6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r12_6) + 0xffffffffffffffff);
    }
    rax7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r12_6) ^ 1);
    ebx8 = *reinterpret_cast<uint32_t*>(&rax7) & 1;
    rax9 = malloc(reinterpret_cast<unsigned char>(r12_6) + reinterpret_cast<uint64_t>(rax7) + 1, rsi);
    if (!rax9) {
        addr_356d_7:
        *reinterpret_cast<int32_t*>(&r8_10) = 0;
        *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
    } else {
        rax11 = fun_25b0(rax9, rbp3, r12_6);
        r8_10 = rax11;
        if (!*reinterpret_cast<signed char*>(&ebx8)) {
            *reinterpret_cast<int32_t*>(&r12_6) = 1;
            *reinterpret_cast<int32_t*>(&r12_6 + 4) = 0;
            goto addr_352e_10;
        } else {
            *reinterpret_cast<struct s0**>(&rax11->f0) = reinterpret_cast<struct s0*>(46);
            *reinterpret_cast<int32_t*>(&r12_6) = 1;
            *reinterpret_cast<int32_t*>(&r12_6 + 4) = 0;
            goto addr_352e_10;
        }
    }
    addr_3533_12:
    return r8_10;
    addr_352e_10:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r8_10) + reinterpret_cast<unsigned char>(r12_6)) = 0;
    goto addr_3533_12;
    addr_3540_4:
    rax12 = malloc(&r12_6->f1, rsi);
    if (!rax12) 
        goto addr_356d_7;
    rax13 = fun_25b0(rax12, rbp3, r12_6);
    r8_10 = rax13;
    goto addr_352e_10;
}

int64_t mfile_name_concat();

void fun_3583() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mfile_name_concat();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void* base_len(void* rdi);

struct s0* fun_24b0(struct s0* rdi, ...);

signed char* fun_2640(struct s0* rdi, struct s0* rsi, struct s0* rdx);

struct s0* fun_35a3(struct s0* rdi, struct s0* rsi, struct s0** rdx) {
    void* rax4;
    void* rax5;
    struct s0* r14_6;
    struct s0* rax7;
    void* rbx8;
    uint1_t zf9;
    int32_t eax10;
    unsigned char v11;
    int1_t zf12;
    int32_t eax13;
    struct s0* rax14;
    signed char* rax15;
    uint32_t ecx16;
    struct s0* rdi17;
    signed char* rax18;

    __asm__("cli ");
    rax4 = last_component();
    rax5 = base_len(rax4);
    r14_6 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax5));
    rax7 = fun_24b0(rsi);
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rbx8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
        zf9 = reinterpret_cast<uint1_t>(*reinterpret_cast<struct s0**>(&rsi->f0) == 47);
        eax10 = 46;
        if (!zf9) {
            eax10 = 0;
        }
        *reinterpret_cast<unsigned char*>(&rbx8) = zf9;
        v11 = *reinterpret_cast<unsigned char*>(&eax10);
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(r14_6) + 0xffffffffffffffff) == 47) {
            v11 = 0;
            *reinterpret_cast<int32_t*>(&rbx8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rbx8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
            zf12 = reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rsi->f0) == 47);
            eax13 = 47;
            if (zf12) {
                eax13 = 0;
            }
            *reinterpret_cast<unsigned char*>(&rbx8) = reinterpret_cast<uint1_t>(!zf12);
            v11 = *reinterpret_cast<unsigned char*>(&eax13);
        }
    }
    rax14 = malloc(reinterpret_cast<unsigned char>(r14_6) + reinterpret_cast<unsigned char>(rax7) + 1 + reinterpret_cast<uint64_t>(rbx8), rsi);
    if (rax14) {
        rax15 = fun_2640(rax14, rdi, r14_6);
        ecx16 = v11;
        rdi17 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rax15) + reinterpret_cast<uint64_t>(rbx8));
        *rax15 = *reinterpret_cast<signed char*>(&ecx16);
        if (rdx) {
            *rdx = rdi17;
        }
        rax18 = fun_2640(rdi17, rsi, rax7);
        *rax18 = 0;
    }
    return rax14;
}

void fun_26d0(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s5 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s5* fun_2500();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_36a3(struct s0* rdi) {
    int64_t rcx2;
    struct s0* rbx3;
    struct s5* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_26d0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23f0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2500();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2410(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_4e43(int64_t rdi) {
    int64_t rbp2;
    struct s0* rax3;
    struct s0* r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2400();
    r12d4 = *reinterpret_cast<struct s0**>(&rax3->f0);
    if (!rbp2) {
        rbp2 = 0xe260;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<struct s0**>(&rax3->f0) = r12d4;
    return;
}

int64_t fun_4e83(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe260);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_4ea3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe260);
    }
    *rdi = esi;
    return 0xe260;
}

int64_t fun_4ec3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xe260);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_4f03(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xe260);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_4f23(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xe260);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x272a;
    if (!rdx) 
        goto 0x272a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xe260;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_4f63(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s8* r8) {
    struct s8* rbx6;
    struct s0* rax7;
    struct s0* r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xe260);
    }
    rax7 = fun_2400();
    r15d8 = *reinterpret_cast<struct s0**>(&rax7->f0);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x4f96);
    *reinterpret_cast<struct s0**>(&rax7->f0) = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_4fe3(int64_t rdi, int64_t rsi, struct s0** rdx, struct s9* rcx) {
    struct s9* rbx5;
    struct s0* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    struct s0* v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xe260);
    }
    rax6 = fun_2400();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<struct s0**>(&rax6->f0);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5011);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x506c);
    *reinterpret_cast<struct s0**>(&rax6->f0) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_50d3() {
    __asm__("cli ");
}

struct s0* ge078 = reinterpret_cast<struct s0*>(96);

int64_t slotvec0 = 0x100;

void fun_50e3() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            free(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xe160) {
        free(rdi7);
        ge078 = reinterpret_cast<struct s0*>(0xe160);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xe070) {
        free(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xe070);
    }
    nslots = 1;
    return;
}

void fun_5183() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_51a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_51b3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_51d3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_51f3(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2730;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

struct s0* fun_5283(struct s0* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2735;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

struct s0* fun_5313(int32_t edi, int64_t rsi) {
    struct s0* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x273a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_24c0();
    } else {
        return rax5;
    }
}

struct s0* fun_53a3(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x273f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

struct s0* fun_5433(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8e20]");
    __asm__("movdqa xmm1, [rip+0x8e28]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x8e11]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_24c0();
    } else {
        return rax10;
    }
}

struct s0* fun_54d3(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8d80]");
    __asm__("movdqa xmm1, [rip+0x8d88]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8d71]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

struct s0* fun_5573(int64_t rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8ce0]");
    __asm__("movdqa xmm1, [rip+0x8ce8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x8cc9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_24c0();
    } else {
        return rax3;
    }
}

struct s0* fun_5603(int64_t rdi, int64_t rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8c50]");
    __asm__("movdqa xmm1, [rip+0x8c58]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x8c46]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_24c0();
    } else {
        return rax4;
    }
}

struct s0* fun_5693(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2744;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

struct s0* fun_5733(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8b1a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x8b12]");
    __asm__("movdqa xmm2, [rip+0x8b1a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2749;
    if (!rdx) 
        goto 0x2749;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

struct s0* fun_57d3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s0* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8a7a]");
    __asm__("movdqa xmm1, [rip+0x8a82]");
    __asm__("movdqa xmm2, [rip+0x8a8a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x274e;
    if (!rdx) 
        goto 0x274e;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

struct s0* fun_5883(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s0* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x89ca]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x89c2]");
    __asm__("movdqa xmm2, [rip+0x89ca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2753;
    if (!rsi) 
        goto 0x2753;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

struct s0* fun_5923(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x892a]");
    __asm__("movdqa xmm1, [rip+0x8932]");
    __asm__("movdqa xmm2, [rip+0x893a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2758;
    if (!rsi) 
        goto 0x2758;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void fun_59c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_59d3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_59f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5a13(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s10 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2590(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_5a33(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s10* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_26e0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_26e0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2490();
    fun_26e0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2590(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2490();
    fun_26e0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2590(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2490();
        fun_26e0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xafc8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xafc8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_5ea3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_5ec3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s11* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_24c0();
    } else {
        return;
    }
}

void fun_5f63(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6006_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6010_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_24c0();
    } else {
        return;
    }
    addr_6006_5:
    goto addr_6010_7;
}

void fun_6043() {
    int64_t rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2590(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2490();
    fun_2630(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2490();
    fun_2630(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2490();
    goto fun_2630;
}

int64_t fun_2440();

void fun_60e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2440();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6123(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = malloc(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6143(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = malloc(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6163(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = malloc(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

struct s0* realloc(struct s0* rdi, struct s0* rsi, ...);

void fun_6183(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = realloc(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_61b3(struct s0* rdi, uint64_t rsi) {
    uint64_t rax3;
    struct s0* rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = realloc(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_61e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2440();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6223() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2440();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6263(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2440();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6293(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2440();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_62e3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2440();
            if (rax5) 
                break;
            addr_632d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_632d_5;
        rax8 = fun_2440();
        if (rax8) 
            goto addr_6316_9;
        if (rbx4) 
            goto addr_632d_5;
        addr_6316_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6373(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2440();
            if (rax8) 
                break;
            addr_63ba_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_63ba_5;
        rax11 = fun_2440();
        if (rax11) 
            goto addr_63a2_9;
        if (!rbx6) 
            goto addr_63a2_9;
        if (r12_4) 
            goto addr_63ba_5;
        addr_63a2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6403(struct s0* rdi, struct s0** rsi, struct s0* rdx, struct s0* rcx, uint64_t r8) {
    struct s0* r13_6;
    struct s0* rdi7;
    struct s0** r12_8;
    struct s0* rsi9;
    struct s0* rcx10;
    struct s0* rbx11;
    struct s0* rax12;
    struct s0* rbp13;
    void* rbp14;
    struct s0* rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<struct s0*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_64ad_9:
            rbx11 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_64c0_10:
                *r12_8 = reinterpret_cast<struct s0*>(0);
            }
            addr_6460_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_6486_12;
            rcx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_64d4_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_647d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_64d4_14;
            addr_647d_16:
            rcx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_64d4_14;
            addr_6486_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = realloc(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_64d4_14;
            if (!rbp13) 
                break;
            addr_64d4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_64ad_9;
        } else {
            if (!r13_6) 
                goto addr_64c0_10;
            goto addr_6460_11;
        }
    }
}

int64_t fun_2560();

void fun_6503() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6533() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6563() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6583() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_65a3(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = malloc(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25b0;
    }
}

void fun_65e3(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = malloc(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25b0;
    }
}

void fun_6623(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = malloc(&rsi->f1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_25b0;
    }
}

void fun_6663(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;
    struct s0* rax4;

    __asm__("cli ");
    rax3 = fun_24b0(rdi);
    rax4 = malloc(&rax3->f1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_25b0;
    }
}

void fun_66a3() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2490();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2650();
    fun_23f0(rdi1);
}

int64_t areadlink();

int64_t fun_66e3() {
    int64_t rax1;
    struct s0* rax2;

    __asm__("cli ");
    rax1 = areadlink();
    if (rax1 || (rax2 = fun_2400(), !reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax2->f0) == 12))) {
        return rax1;
    } else {
        xalloc_die();
    }
}

struct s0** fun_2710(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* fun_2690(struct s0* rdi);

int64_t fun_6713(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8) {
    struct s0* v6;
    struct s0* rax7;
    struct s0* v8;
    struct s0* rcx9;
    struct s0* rbx10;
    uint32_t r12d11;
    void* r9_12;
    struct s0* rax13;
    struct s0* r15_14;
    void* rax15;
    int64_t rax16;
    struct s0* rbp17;
    struct s0* r13_18;
    struct s0* rax19;
    struct s0* r12_20;
    struct s0** rax21;
    struct s0* rax22;
    int64_t rdx23;
    struct s0* rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<struct s0*>("xstrtoumax");
        rsi = reinterpret_cast<struct s0*>("lib/xstrtol.c");
        fun_2520("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 85, "xstrtoumax");
        do {
            fun_24c0();
            while (1) {
                rbx10 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_6a84_6;
                    rbx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<unsigned char>(rcx9));
                } while (!__intrinsic());
            }
            addr_6a84_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<struct s0**>(&r15_14->f0) = rax13;
            if (*reinterpret_cast<struct s0**>(&rax13->f0)) {
                r12d11 = r12d11 | 2;
            }
            addr_67cd_12:
            *reinterpret_cast<struct s0**>(&v6->f0) = rbx10;
            addr_67d5_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2400();
    *reinterpret_cast<struct s0**>(&rax19->f0) = reinterpret_cast<struct s0*>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0));
    rax21 = fun_2710(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx23 * 2) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = rax22->f1;
        rax22 = reinterpret_cast<struct s0*>(&rax22->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_680b_21;
    rsi = r15_14;
    rax24 = fun_2690(rbp17);
    r8 = *reinterpret_cast<struct s0**>(&r15_14->f0);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbp17->f0)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_24f0(r13_18, r13_18), r8 = r8, rax26 == 0))) {
            addr_680b_21:
            r12d11 = 4;
            goto addr_67d5_13;
        } else {
            addr_6849_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_24f0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = r8->f1;
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(r8->f2 == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<struct s0*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xb078 + rbp33 * 4) + 0xb078;
        }
    } else {
        if (*reinterpret_cast<struct s0**>(&r12_20->f0)) {
            r12d11 = 1;
            if (*reinterpret_cast<struct s0**>(&r12_20->f0) != 34) 
                goto addr_680b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_67cd_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&r8->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_67cd_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_24f0(r13_18, r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_6849_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<struct s0**>(&v6->f0) = rbx10;
    goto addr_67d5_13;
}

void rpl_vasprintf();

void fun_6b43() {
    signed char al1;
    struct s0* rax2;
    void* rdx3;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    rpl_vasprintf();
    rdx3 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx3) {
        fun_24c0();
    } else {
        return;
    }
}

struct s0* vasnprintf();

uint64_t fun_6c03(struct s0** rdi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t v7;
    struct s0* rax8;
    void* rdx9;

    __asm__("cli ");
    rax4 = g28;
    rax5 = vasnprintf();
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rax6 = v7;
        if (rax6 > 0x7fffffff) {
            free(rax5, rax5);
            rax8 = fun_2400();
            *reinterpret_cast<struct s0**>(&rax8->f0) = reinterpret_cast<struct s0*>(75);
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            *rdi = rax5;
        }
    }
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void fun_6c83(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    __asm__("cli ");
    if (edi != -100) 
        goto 0x275d;
}

void fun_6ca3(int64_t rdi) {
    __asm__("cli ");
}

struct s12 {
    unsigned char f0;
    unsigned char f1;
};

struct s12* fun_6cc3(struct s12* rdi) {
    uint32_t edx2;
    struct s12* rax3;
    struct s12* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s12*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s12*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s12*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_6d23(struct s0* rdi) {
    struct s0* rbx2;
    struct s0* rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_24b0(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        rax3 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff);
    }
    return;
}

void* fun_6ef3(int32_t edi, int64_t rsi, void* rdx, uint64_t rcx, struct s3* r8, int64_t r9) {
    struct s0* rax7;
    void* rax8;
    void* rdx9;

    __asm__("cli ");
    rax7 = g28;
    rax8 = readlink_stk(edi, rsi, rdx, rcx, r8, r9, reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x420 + 8);
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_24c0();
    } else {
        return rax8;
    }
}

int64_t fun_2430();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_6f43(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    struct s0* rax5;
    struct s0* rax6;

    __asm__("cli ");
    rax2 = fun_2430();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_6f9e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2400();
            *reinterpret_cast<struct s0**>(&rax5->f0) = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_6f9e_3;
            rax6 = fun_2400();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax6->f0) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s13 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_25c0(struct s13* rdi);

int32_t fun_2600(struct s13* rdi);

int64_t fun_2510(int64_t rdi, ...);

int32_t rpl_fflush(struct s13* rdi);

int64_t fun_2470(struct s13* rdi);

int64_t fun_6fb3(struct s13* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    struct s0* rax8;
    struct s0* r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_25c0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2600(rdi);
        if (!(eax3 && (eax4 = fun_25c0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2510(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2400();
            r12d9 = *reinterpret_cast<struct s0**>(&rax8->f0);
            rax10 = fun_2470(rdi);
            if (r12d9) {
                *reinterpret_cast<struct s0**>(&rax8->f0) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2470;
}

void rpl_fseeko(struct s13* rdi);

void fun_7043(struct s13* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2600(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_7093(struct s13* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_25c0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2510(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_25f0(int64_t rdi);

signed char* fun_7113() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25f0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24e0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_7153(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24e0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_24c0();
    } else {
        return r12_7;
    }
}

int32_t printf_parse(struct s0* rdi, void* rsi, struct s0* rdx);

int32_t printf_fetchargs(struct s0* rdi, struct s0* rsi, struct s0* rdx);

struct s14 {
    signed char[7] pad7;
    struct s0* f7;
};

struct s15 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    struct s0* f18;
    signed char[7] pad32;
    struct s0* f20;
    signed char[7] pad40;
    int64_t f28;
    struct s0* f30;
    signed char[7] pad56;
    struct s0* f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    struct s0* f58;
};

struct s16 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

struct s0* fun_71e3(struct s0* rdi, struct s0** rsi, struct s0* rdx, struct s0* rcx) {
    void* rsp5;
    void* rbp6;
    struct s0* r14_7;
    struct s0* r13_8;
    struct s0* r12_9;
    struct s0* v10;
    struct s0** v11;
    struct s0* rax12;
    struct s0* v13;
    int32_t eax14;
    void* rsp15;
    struct s0* r10_16;
    void* rax17;
    int64_t* rsp18;
    struct s0* rbx19;
    int64_t* rsp20;
    struct s0* rax21;
    struct s0* v22;
    int64_t* rsp23;
    struct s0* v24;
    int64_t* rsp25;
    struct s0* v26;
    int64_t* rsp27;
    struct s0* v28;
    int64_t* rsp29;
    struct s0* rax30;
    struct s0* r15_31;
    struct s0* v32;
    int64_t* rsp33;
    int64_t* rsp34;
    struct s0* v35;
    int64_t* rsp36;
    struct s0* v37;
    int64_t* rsp38;
    struct s0* rsi39;
    int32_t eax40;
    struct s0* rax41;
    struct s0* rax42;
    struct s14* v43;
    struct s0* tmp64_44;
    void* v45;
    struct s0* r8_46;
    struct s0* tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    struct s0* v53;
    struct s0* rax54;
    struct s0* rax55;
    struct s15* r14_56;
    struct s15* v57;
    struct s0* r9_58;
    struct s0* r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    struct s0* tmp64_63;
    struct s0* r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    struct s0* rax67;
    int64_t* rsp68;
    struct s0* rax69;
    int64_t* rsp70;
    struct s0* rax71;
    int64_t* rsp72;
    struct s0* rax73;
    int64_t* rsp74;
    struct s0* rax75;
    int64_t* rsp76;
    struct s0* rax77;
    struct s0* tmp64_78;
    int64_t* rsp79;
    struct s0* rax80;
    int64_t* rsp81;
    struct s0* rax82;
    int64_t* rsp83;
    struct s0* rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    struct s0* rsi89;
    struct s0* rax90;
    int64_t* rsp91;
    struct s0* rsi92;
    struct s0* rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s16* rcx98;
    int64_t rax99;
    struct s0* tmp64_100;
    struct s0* r15_101;
    struct s0* rax102;
    int64_t rax103;
    int64_t* rsp104;
    struct s0* rax105;
    int64_t* rsp106;
    struct s0* rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    struct s0* rax110;
    int64_t* rsp111;
    struct s0* rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0x811a;
                fun_24c0();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_811a_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_8124_6;
                addr_800e_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0x8033, rax21 = realloc(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0x8059;
                    free(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0x807f;
                    free(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0x80a5;
                    free(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_8124_6:
            addr_7d08_16:
            v28 = r10_16;
            addr_7d0f_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0x7d14;
            rax30 = fun_2400();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_7d22_18:
            *reinterpret_cast<struct s0**>(&v32->f0) = reinterpret_cast<struct s0*>(12);
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0x78c1;
                free(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0x78d9;
                free(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_7518_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0x7530;
                free(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0x7548;
            free(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_2400();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *reinterpret_cast<struct s0**>(&rax41->f0) = reinterpret_cast<struct s0*>(22);
        goto addr_7518_23;
    }
    rax42 = reinterpret_cast<struct s0*>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_750d_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(tmp64_44) + 6);
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_750d_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax49 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(tmp64_44) + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 0x1000);
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<struct s0*>(0);
        v53 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = malloc(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_750d_33:
            rax55 = fun_2400();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *reinterpret_cast<struct s0**>(&rax55->f0) = reinterpret_cast<struct s0*>(12);
            goto addr_7518_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_730c_46;
    while (1) {
        addr_7c64_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_73cf_50;
            if (r14_56->f50 != -1) 
                goto 0x2763;
            r9_58 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = reinterpret_cast<struct s0*>(&r12_9->f1);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_7c3f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_7d08_16;
                    rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_7d08_16;
                if (r10_16 == v10) 
                    goto addr_7f54_64; else 
                    goto addr_7c13_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s15*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_7c64_47;
            addr_730c_46:
            r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_7dc0_73;
                    rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_7dc0_73;
                if (r15_31 == v10) 
                    goto addr_7d50_79; else 
                    goto addr_7367_80;
            }
            addr_738c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0x73a2;
            fun_25b0(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_7d50_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0x7d58;
            rax67 = malloc(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_7dc0_73;
            if (!r9_58) 
                goto addr_738c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0x7d97;
            rax69 = fun_25b0(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_738c_81;
            addr_7367_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0x7372;
            rax71 = realloc(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_7dc0_73; else 
                goto addr_738c_81;
            addr_7f54_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0x7f6a;
            rax73 = malloc(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_8129_84;
            if (r12_9) 
                goto addr_7f8a_86;
            r10_16 = rax73;
            goto addr_7c3f_55;
            addr_7f8a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0x7f9f;
            rax75 = fun_25b0(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_7c3f_55;
            addr_7c13_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0x7c2c;
            rax77 = realloc(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_7d0f_17;
            r10_16 = rax77;
            goto addr_7c3f_55;
        }
        break;
    }
    tmp64_78 = reinterpret_cast<struct s0*>(&r12_9->f1);
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_811a_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_800e_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_7d08_16;
        rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_80cd_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_80cd_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_7d08_16; else 
                goto addr_80d7_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_7fe3_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0x80ee;
        rax80 = malloc(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_800e_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0x810d;
                rax82 = fun_25b0(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_800e_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0x8002;
        rax84 = realloc(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_7d0f_17; else 
            goto addr_800e_7;
    }
    addr_80d7_96:
    rbx19 = r13_8;
    goto addr_7fe3_98;
    addr_73cf_50:
    if (r14_56->f50 == -1) 
        goto 0x2763;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x2768;
        goto *reinterpret_cast<int32_t*>(0xb1d4 + r13_87 * 4) + 0xb1d4;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<struct s0**>(&v53->f0) = reinterpret_cast<struct s0*>(37);
    r13_8 = reinterpret_cast<struct s0*>(&v53->f1);
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        v53->f1 = 39;
        r13_8 = reinterpret_cast<struct s0*>(&v53->f2);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(45);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(43);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(32);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(35);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(73);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(48);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0x749f;
        fun_25b0(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0x74d9;
        fun_25b0(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0xb164 + rax95 * 4) + 0xb164;
    }
    eax96 = r14_56->f48;
    r13_8->f1 = 0;
    *reinterpret_cast<struct s0**>(&r13_8->f0) = *reinterpret_cast<struct s0**>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x2763;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s16*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x2763;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = reinterpret_cast<struct s0*>(&r12_9->f2);
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_7612_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_7d08_16;
    }
    addr_7612_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_7d08_16;
            rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_7633_142; else 
                goto addr_7ec2_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_7ec2_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_7d08_16; else 
                    goto addr_7ecc_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_7633_142;
            }
        }
    }
    addr_7665_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0x766f;
    rax102 = fun_2400();
    *reinterpret_cast<struct s0**>(&rax102->f0) = reinterpret_cast<struct s0*>(0);
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x2768;
    goto *reinterpret_cast<int32_t*>(0xb18c + rax103 * 4) + 0xb18c;
    addr_7633_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0x7f28;
        rax105 = malloc(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_8129_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0x812e;
            rax107 = fun_2400();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_7d22_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0x7f4f;
                fun_25b0(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_7665_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0x7652;
        rax110 = realloc(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_7d08_16; else 
            goto addr_7665_147;
    }
    addr_7ecc_145:
    rbx19 = tmp64_100;
    goto addr_7633_142;
    addr_7dc0_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0x7dc5;
    rax112 = fun_2400();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_7d22_18;
}

int32_t setlocale_null_r();

int64_t fun_8163() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax3;
    }
}

int64_t fun_81e3(int64_t rdi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2620(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<struct s0**>(&rsi->f0) = reinterpret_cast<struct s0*>(0);
        }
    } else {
        rax6 = fun_24b0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_25b0(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_25b0(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_8293() {
    __asm__("cli ");
    goto fun_2620;
}

struct s17 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_82a3(int64_t rdi, struct s17* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_82d9_5;
    return 0xffffffff;
    addr_82d9_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb200 + rdx3 * 4) + 0xb200;
}

struct s18 {
    int64_t f0;
    struct s0* f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    struct s0* f20;
};

struct s19 {
    uint64_t f0;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
};

struct s20 {
    unsigned char f0;
    signed char[1] pad2;
    struct s0* f2;
};

struct s21 {
    unsigned char f0;
    signed char[1] pad2;
    struct s0* f2;
};

int64_t fun_84d3(struct s0* rdi, struct s18* rsi, struct s19* rdx) {
    struct s0* r10_4;
    struct s0* rax5;
    struct s0* rdi6;
    struct s19* r15_7;
    struct s18* r14_8;
    struct s0* rcx9;
    uint64_t r9_10;
    struct s0* v11;
    uint64_t v12;
    uint32_t edx13;
    struct s0* rbx14;
    int64_t rax15;
    struct s0* r12_16;
    int64_t rbp17;
    int32_t edx18;
    struct s0* rdx19;
    int64_t rcx20;
    int32_t esi21;
    struct s0* rdx22;
    struct s20* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s20* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    struct s0* rdx38;
    uint32_t eax39;
    void* rax40;
    struct s0* rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    struct s0* rcx45;
    int64_t rsi46;
    int32_t eax47;
    unsigned char* rbx48;
    struct s0* rax49;
    int64_t rsi50;
    int32_t edi51;
    uint64_t rbp52;
    struct s0* rdx53;
    struct s0* r8_54;
    uint64_t rdx55;
    struct s0** rax56;
    struct s0** rcx57;
    uint64_t r9_58;
    struct s0** rbp59;
    struct s0* rsi60;
    struct s0* rax61;
    struct s0* rax62;
    uint64_t rdx63;
    struct s0* rax64;
    struct s20* rbx65;
    uint64_t rsi66;
    int32_t eax67;
    struct s20* rdx68;
    uint64_t rax69;
    uint64_t rcx70;
    uint64_t rcx71;
    uint64_t tmp64_72;
    int32_t eax73;
    struct s0* rax74;
    int64_t rdx75;
    int32_t edi76;
    uint64_t rbx77;
    uint64_t r9_78;
    uint64_t rdx79;
    struct s0** rax80;
    struct s0** rsi81;
    struct s0* rsi82;
    struct s0* rax83;
    struct s0* rdi84;
    struct s0* r8_85;
    struct s0* rdi86;
    uint64_t rdx87;
    struct s0* rax88;
    struct s0* rax89;
    struct s0* rax90;
    struct s0** rax91;
    struct s0* rdi92;
    struct s0* rax93;
    struct s21* rbx94;
    uint64_t rdi95;
    int32_t eax96;
    struct s21* rcx97;
    uint64_t rax98;
    uint64_t rdx99;
    uint64_t rdx100;
    uint64_t tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<struct s0*>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<struct s0*>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_10) + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = 0;
    rdx->f8 = rdi6;
    v12 = 0;
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax5->f0)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = reinterpret_cast<struct s0*>(&rax5->f1);
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_8588_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<struct s0**>(&rcx9->f0) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_8575_7:
    return rax15;
    addr_8588_4:
    r12_16 = reinterpret_cast<struct s0*>(&rcx9->f0);
    *reinterpret_cast<struct s0**>(&r12_16->f0) = rax5;
    r12_16->f10 = reinterpret_cast<signed char*>(0);
    r12_16->f18 = reinterpret_cast<struct s0*>(0);
    r12_16->f20 = reinterpret_cast<struct s0*>(0);
    r12_16->f28 = 0xffffffffffffffff;
    r12_16->f30 = reinterpret_cast<struct s0*>(0);
    r12_16->f38 = reinterpret_cast<struct s0*>(0);
    r12_16->f40 = 0xffffffffffffffff;
    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r12_16) + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = rax5->f1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = rdx19->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            rdx19 = reinterpret_cast<struct s0*>(&rdx19->f1);
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_85f9_11;
    } else {
        addr_85f9_11:
        rdx22 = reinterpret_cast<struct s0*>(&rbx14->f1);
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                r12_16->f10 = reinterpret_cast<signed char*>(reinterpret_cast<uint32_t>(r12_16->f10) | 1);
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx22->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = reinterpret_cast<struct s0*>(&rbx14->f1);
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_8610_14;
        } else {
            goto addr_8610_14;
        }
    }
    rax23 = reinterpret_cast<struct s20*>(&rax5->f2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s20*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_8ad8_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s20*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s20*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_8ad8_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<struct s0*>(&rcx26->f2);
    goto addr_85f9_11;
    addr_8610_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb27c + rax33 * 4) + 0xb27c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        r12_16->f18 = rbx14;
        r12_16->f20 = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = rbx14->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_8757_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            r12_16->f18 = rbx14;
            eax37 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0)) - 48;
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_8e59_36:
                r12_16->f20 = rbx14;
                goto addr_8e5e_37;
            } else {
                rdx38 = rbx14;
                do {
                    rdx38 = reinterpret_cast<struct s0*>(&rdx38->f1);
                    eax39 = rdx38->f1 - 48;
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_8e54_42;
            }
        } else {
            addr_863d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_8858_44:
                if (rbx14->f1 != 42) {
                    r12_16->f30 = rbx14;
                    rdx41 = reinterpret_cast<struct s0*>(&rbx14->f1);
                    eax42 = rbx14->f1 - 48;
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            rdx41 = reinterpret_cast<struct s0*>(&rdx41->f1);
                            eax44 = rdx41->f1 - 48;
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    r12_16->f38 = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_8647_52;
                } else {
                    rcx45 = reinterpret_cast<struct s0*>(&rbx14->f2);
                    r12_16->f30 = rbx14;
                    r12_16->f38 = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = rbx14->f2;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_9044_56; else 
                        goto addr_8895_57;
                }
            } else {
                addr_8647_52:
                rbx48 = &rbx14->f1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = *rbx48;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_8668_60;
                } else {
                    goto addr_8668_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = rax49->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        rax49 = reinterpret_cast<struct s0*>(&rax49->f1);
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_8bdb_65;
    addr_8757_33:
    r12_16->f28 = 0;
    if (0) 
        goto addr_8ad8_22;
    rbp52 = 0;
    v12 = 1;
    rbx14 = rdx22;
    addr_877c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (7 > rbp52) {
        addr_87fa_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<struct s0**>((rdx55 << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (rdx55 <= rbp52) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<struct s0*>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (rdx55 <= rbp52);
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<struct s0*>(0);
        }
    } else {
        r9_58 = 14;
        if (14 <= rbp52) {
            r9_58 = rbp52 + 1;
        }
        if (r9_58 >> 59) 
            goto addr_90fb_75; else 
            goto addr_87a3_76;
    }
    rbp59 = reinterpret_cast<struct s0**>((rbp52 << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_8adc_80;
        }
    } else {
        *rbp59 = reinterpret_cast<struct s0*>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_8647_52;
        goto addr_8858_44;
    }
    addr_8e5e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_863d_43;
    addr_90fb_75:
    if (v11 != rdx53) {
        free(rdx53);
        r10_4 = r10_4;
        goto addr_8f0a_84;
    } else {
        goto addr_8f0a_84;
    }
    addr_87a3_76:
    rsi60 = reinterpret_cast<struct s0*>(r9_58 << 5);
    if (v11 == rdx53) {
        rax61 = malloc(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = realloc(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_90fb_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_25b0(r8_54, v11, rdx63 << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_87fa_68;
    addr_8bdb_65:
    rbx65 = reinterpret_cast<struct s20*>(&rbx14->f2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s20*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (rsi66 > 0x1999999999999999) {
            rcx70 = 0xffffffffffffffff;
        } else {
            rcx71 = rsi66 + rsi66 * 4;
            rcx70 = rcx71 + rcx71;
        }
        while (tmp64_72 = rcx70 + rax69, rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), tmp64_72 < rcx70) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_8ad8_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s20*>(&rbx65->pad2);
            rcx70 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s20*>(&rbx65->pad2);
    }
    rbp52 = tmp64_72 - 1;
    if (rbp52 > 0xfffffffffffffffd) 
        goto addr_8ad8_22;
    r12_16->f28 = rbp52;
    rbx14 = reinterpret_cast<struct s0*>(&rdx68->f2);
    goto addr_877c_67;
    label_41:
    addr_8e54_42:
    goto addr_8e59_36;
    addr_9044_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = rax74->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        rax74 = reinterpret_cast<struct s0*>(&rax74->f1);
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_906a_104;
    addr_8895_57:
    rbx77 = r12_16->f40;
    if (rbx77 == 0xffffffffffffffff) {
        r12_16->f40 = v12;
        if (0) {
            addr_8ad8_22:
            r8_54 = r15_7->f8;
            goto addr_8adc_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_88a4_107:
    r8_54 = r15_7->f8;
    if (r9_10 <= rbx77) {
        r9_78 = r9_10 + r9_10;
        if (r9_78 <= rbx77) {
            r9_78 = rbx77 + 1;
        }
        if (!(r9_78 >> 59)) 
            goto addr_8f52_111;
    } else {
        addr_88b1_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<struct s0**>((rdx79 << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (rdx79 <= rbx77) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<struct s0*>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (rdx79 <= rbx77);
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<struct s0*>(0);
            goto addr_88e7_116;
        }
    }
    rdx53 = r8_54;
    goto addr_90fb_75;
    addr_8f52_111:
    rsi82 = reinterpret_cast<struct s0*>(r9_78 << 5);
    if (v11 == r8_54) {
        rax83 = malloc(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_8f0a_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                free(rdi86);
            }
        } else {
            addr_9190_120:
            rdx87 = r15_7->f0;
            rax88 = fun_25b0(rdi84, r8_85, rdx87 << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_8faf_121;
        }
    } else {
        rax89 = realloc(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_90fb_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_9190_120;
            }
        }
    }
    rax90 = fun_2400();
    *reinterpret_cast<struct s0**>(&rax90->f0) = reinterpret_cast<struct s0*>(12);
    return 0xffffffff;
    addr_8faf_121:
    r15_7->f8 = r8_54;
    goto addr_88b1_112;
    addr_88e7_116:
    rax91 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_54) + (rbx77 << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_8adc_80:
            if (v11 != r8_54) {
                free(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx45->f0));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_8647_52;
        }
    } else {
        *rax91 = reinterpret_cast<struct s0*>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx45->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_8647_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        free(rdi92, rdi92);
    }
    rax93 = fun_2400();
    *reinterpret_cast<struct s0**>(&rax93->f0) = reinterpret_cast<struct s0*>(22);
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_8575_7;
    addr_906a_104:
    rbx94 = reinterpret_cast<struct s21*>(&rbx14->f3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s21*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (rdi95 > 0x1999999999999999) {
            rdx99 = 0xffffffffffffffff;
        } else {
            rdx100 = rdi95 + rdi95 * 4;
            rdx99 = rdx100 + rdx100;
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = rdx99 + rax98, rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), tmp64_101 < rdx99) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_8ad8_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s21*>(&rbx94->pad2);
            rdx99 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s21*>(&rbx94->pad2);
    }
    rbx77 = tmp64_101 + 0xffffffffffffffff;
    if (rbx77 > 0xfffffffffffffffd) 
        goto addr_8ad8_22;
    r12_16->f40 = rbx77;
    rcx45 = reinterpret_cast<struct s0*>(&rcx97->f2);
    goto addr_88a4_107;
    addr_8668_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xb3e0 + rax105 * 4) + 0xb3e0;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb324 + rax106 * 4) + 0xb324;
    }
}

void fun_9263() {
    __asm__("cli ");
}

void fun_9277() {
    __asm__("cli ");
    return;
}

uint32_t fun_2540(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_2700(int64_t rdi, struct s0* rsi);

uint32_t fun_26f0(struct s0* rdi, struct s0* rsi);

void fun_38d5() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    struct s0** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2490();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2490();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24b0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3bd3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3bd3_22; else 
                            goto addr_3fcd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_408d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_43e0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3bd0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3bd0_30; else 
                                goto addr_43f9_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24b0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_43e0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2540(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_43e0_28; else 
                            goto addr_3a7c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4540_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_43c0_40:
                        if (r11_27 == 1) {
                            addr_3f4d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4508_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3b87_44;
                            }
                        } else {
                            goto addr_43d0_46;
                        }
                    } else {
                        addr_454f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_3f4d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3bd3_22:
                                if (v47 != 1) {
                                    addr_4129_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_24b0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4174_54;
                                    }
                                } else {
                                    goto addr_3be0_56;
                                }
                            } else {
                                addr_3b85_57:
                                ebp36 = 0;
                                goto addr_3b87_44;
                            }
                        } else {
                            addr_43b4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_454f_47; else 
                                goto addr_43be_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_3f4d_41;
                        if (v47 == 1) 
                            goto addr_3be0_56; else 
                            goto addr_4129_52;
                    }
                }
                addr_3c41_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3ad8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_3afd_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_3e00_65;
                    } else {
                        addr_3c69_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_44b8_67;
                    }
                } else {
                    goto addr_3c60_69;
                }
                addr_3b11_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_3b5c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_44b8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3b5c_81;
                }
                addr_3c60_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_3afd_64; else 
                    goto addr_3c69_66;
                addr_3b87_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_3c3f_91;
                if (v22) 
                    goto addr_3b9f_93;
                addr_3c3f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3c41_62;
                addr_4174_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_48fb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_496b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_476f_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2700(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_26f0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_426e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_3c2c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4278_112;
                    }
                } else {
                    addr_4278_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4349_114;
                }
                addr_3c38_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_3c3f_91;
                while (1) {
                    addr_4349_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4857_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_42b6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4865_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4337_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4337_128;
                        }
                    }
                    addr_42e5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4337_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_42b6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_42e5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3b5c_81;
                addr_4865_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_44b8_67;
                addr_48fb_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_426e_109;
                addr_496b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_426e_109;
                addr_3be0_56:
                rax93 = fun_2710(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_3c2c_110;
                addr_43be_59:
                goto addr_43c0_40;
                addr_408d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3bd3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3c38_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3b85_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3bd3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_40d2_160;
                if (!v22) 
                    goto addr_44a7_162; else 
                    goto addr_46b3_163;
                addr_40d2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_44a7_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_44b8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_3f7b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_3de3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3b11_70; else 
                    goto addr_3df7_169;
                addr_3f7b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3ad8_63;
                goto addr_3c60_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_43b4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_44ef_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3bd0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3ac8_178; else 
                        goto addr_4472_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_43b4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3bd3_22;
                }
                addr_44ef_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3bd0_30:
                    r8d42 = 0;
                    goto addr_3bd3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3c41_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4508_42;
                    }
                }
                addr_3ac8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3ad8_63;
                addr_4472_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_43d0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3c41_62;
                } else {
                    addr_4482_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3bd3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4c32_188;
                if (v28) 
                    goto addr_44a7_162;
                addr_4c32_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_3de3_168;
                addr_3a7c_37:
                if (v22) 
                    goto addr_4a73_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3a93_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4540_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_45cb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3bd3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3ac8_178; else 
                        goto addr_45a7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_43b4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3bd3_22;
                }
                addr_45cb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3bd3_22;
                }
                addr_45a7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_43d0_46;
                goto addr_4482_186;
                addr_3a93_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3bd3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3bd3_22; else 
                    goto addr_3aa4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_4b7e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4a04_210;
            if (1) 
                goto addr_4a02_212;
            if (!v29) 
                goto addr_463e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_4b71_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_3e00_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_3bbb_219; else 
            goto addr_3e1a_220;
        addr_3b9f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_3bb3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_3e1a_220; else 
            goto addr_3bbb_219;
        addr_476f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_3e1a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_478d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_4c00_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4666_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_4857_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3bb3_221;
        addr_46b3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3bb3_221;
        addr_3df7_169:
        goto addr_3e00_65;
        addr_4b7e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_3e1a_220;
        goto addr_478d_222;
        addr_4a04_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_4a5e_236;
        fun_24c0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4c00_225;
        addr_4a02_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4a04_210;
        addr_463e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4a04_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_4666_226;
        }
        addr_4b71_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_3fcd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xaa8c + rax113 * 4) + 0xaa8c;
    addr_43f9_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xab8c + rax114 * 4) + 0xab8c;
    addr_4a73_190:
    addr_3bbb_219:
    goto 0x38a0;
    addr_3aa4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa98c + rax115 * 4) + 0xa98c;
    addr_4a5e_236:
    goto v116;
}

void fun_3ac0() {
}

void fun_3c78() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3972;
}

void fun_3cd1() {
    goto 0x3972;
}

void fun_3dbe() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3c41;
    }
    if (v2) 
        goto 0x46b3;
    if (!r10_3) 
        goto addr_481e_5;
    if (!v4) 
        goto addr_46ee_7;
    addr_481e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_46ee_7:
    goto 0x3af4;
}

void fun_3ddc() {
}

void fun_3e87() {
    signed char v1;

    if (v1) {
        goto 0x3e0f;
    } else {
        goto 0x3b4a;
    }
}

void fun_3ea1() {
    signed char v1;

    if (!v1) 
        goto 0x3e9a; else 
        goto "???";
}

void fun_3ec8() {
    goto 0x3de3;
}

void fun_3f48() {
}

void fun_3f60() {
}

void fun_3f8f() {
    goto 0x3de3;
}

void fun_3fe1() {
    goto 0x3f70;
}

void fun_4010() {
    goto 0x3f70;
}

void fun_4043() {
    goto 0x3f70;
}

void fun_4410() {
    goto 0x3ac8;
}

void fun_470e() {
    signed char v1;

    if (v1) 
        goto 0x46b3;
    goto 0x3af4;
}

void fun_47b5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3af4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3ad8;
        goto 0x3af4;
    }
}

void fun_4bd2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x3e40;
    } else {
        goto 0x3972;
    }
}

void fun_5b08() {
    fun_2490();
}

void fun_68bc() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x68cb;
}

void fun_698c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x6999;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x68cb;
}

void fun_69b0() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_69dc() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x68cb;
}

void fun_69fd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x68cb;
}

void fun_6a21() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x68cb;
}

void fun_6a45() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x69d4;
}

void fun_6a69() {
}

void fun_6a89() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x69d4;
}

void fun_6aa5() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x69d4;
}

int32_t fun_23d0(int64_t rdi);

struct s22 {
    signed char[1] pad1;
    signed char f1;
};

struct s23 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_76d8() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    struct s0* rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    struct s0* rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    struct s0* rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    struct s0* rax30;
    struct s0* rcx31;
    void* rbx32;
    void* rbx33;
    struct s0* tmp64_34;
    void* r12_35;
    struct s0* rax36;
    struct s0* rbx37;
    int64_t r15_38;
    int64_t rbp39;
    struct s0* r15_40;
    struct s0* rax41;
    struct s0* rax42;
    int64_t r12_43;
    struct s0* r15_44;
    struct s0* r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s23* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_23d0(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<struct s0**>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_7863_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_23d0(rdi1);
            rsi10 = *reinterpret_cast<struct s0**>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_23d0(rdi1);
            rsi10 = *reinterpret_cast<struct s0**>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_7863_5;
    }
    rcx19 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x2763;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_776d_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0x7c47;
        }
    } else {
        addr_7765_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_776d_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0x78b0;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0x7688;
        goto 0x7d22;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0x7d22;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_77a6_26;
    rax36 = rcx31;
    addr_77a6_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0x7688;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0x7d22;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = realloc(r15_40, rax36);
        if (!rax41) 
            goto 0x7d22;
        goto 0x7688;
    }
    rax42 = malloc(rax36, rsi10);
    if (!rax42) 
        goto 0x7d22;
    if (r12_43) 
        goto addr_7b6a_36;
    goto 0x7688;
    addr_7b6a_36:
    fun_25b0(rax42, r15_44, r12_45);
    goto 0x7688;
    addr_7863_5:
    if ((*reinterpret_cast<struct s22**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s22**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0x7688;
    }
    if (eax7 >= 0) 
        goto addr_7765_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0x78b0;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_78a7_42;
    eax50 = 84;
    addr_78a7_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_77f0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_78e0() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x7a25;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_23d0(r15_4 + r12_5);
            goto 0x773d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x7713;
        }
    }
}

void fun_7928() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_23d0(rdi5);
            goto 0x773d;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_23d0(rdi5);
    goto 0x773d;
}

void fun_7990() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x7816;
}

void fun_79e0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x79c0;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x781f;
}

void fun_7a90() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x7816;
    goto 0x79c0;
}

void fun_7b23() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x7592;
}

void fun_7cc0() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x7c47;
}

struct s24 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s25 {
    signed char[8] pad8;
    int64_t f8;
};

struct s26 {
    signed char[16] pad16;
    int64_t f10;
};

struct s27 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_82e8() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s24* rcx3;
    struct s25* rcx4;
    int64_t r11_5;
    struct s26* rcx6;
    uint32_t* rcx7;
    struct s27* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x82d0; else 
        goto "???";
}

struct s28 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s29 {
    signed char[8] pad8;
    int64_t f8;
};

struct s30 {
    signed char[16] pad16;
    int64_t f10;
};

struct s31 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8320() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s28* rcx3;
    struct s29* rcx4;
    int64_t r11_5;
    struct s30* rcx6;
    uint32_t* rcx7;
    struct s31* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x8306;
}

struct s32 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s33 {
    signed char[8] pad8;
    int64_t f8;
};

struct s34 {
    signed char[16] pad16;
    int64_t f10;
};

struct s35 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_8340() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s32* rcx3;
    struct s33* rcx4;
    int64_t r11_5;
    struct s34* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s35* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x8306;
}

struct s36 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s37 {
    signed char[8] pad8;
    int64_t f8;
};

struct s38 {
    signed char[16] pad16;
    int64_t f10;
};

struct s39 {
    signed char[16] pad16;
    signed char f10;
};

void fun_8360() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s36* rcx3;
    struct s37* rcx4;
    int64_t r11_5;
    struct s38* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s39* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x8306;
}

struct s40 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s41 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_83e0() {
    struct s40* rcx1;
    struct s41* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x8306;
}

struct s42 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s43 {
    signed char[8] pad8;
    int64_t f8;
};

struct s44 {
    signed char[16] pad16;
    int64_t f10;
};

struct s45 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8430() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s42* rcx3;
    struct s43* rcx4;
    int64_t r11_5;
    struct s44* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s45* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x8306;
}

void fun_867c() {
}

void fun_86ab() {
    goto 0x8688;
}

void fun_8700() {
}

void fun_8910() {
    goto 0x8703;
}

struct s46 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s47 {
    signed char[8] pad8;
    int64_t f8;
};

struct s48 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s49 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_89b8() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    struct s0* rbp4;
    struct s46* r14_5;
    struct s0* rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    struct s0* r10_10;
    struct s0* rax11;
    struct s0* rcx12;
    int64_t v13;
    struct s47* r15_14;
    struct s0* rax15;
    struct s48* r14_16;
    struct s0* r10_17;
    int64_t r13_18;
    struct s0* rax19;
    struct s49* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x90f7;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x90f7;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<struct s0*>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = malloc(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x8f18; else 
                goto "???";
        }
    } else {
        rax15 = realloc(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x90f7;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_924a_9; else 
            goto addr_8a2e_10;
    }
    addr_8b84_11:
    rax19 = fun_25b0(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_8a2e_10:
    r14_20->f8 = rcx12;
    goto 0x8549;
    addr_924a_9:
    r13_18 = *r14_21;
    goto addr_8b84_11;
}

struct s50 {
    signed char[11] pad11;
    struct s0* fb;
};

struct s51 {
    signed char[80] pad80;
    int64_t f50;
};

struct s52 {
    signed char[80] pad80;
    int64_t f50;
};

struct s53 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s54 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s55 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s56 {
    signed char[72] pad72;
    signed char f48;
};

struct s57 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_8c41() {
    struct s0* ecx1;
    int32_t edx2;
    struct s50* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s51* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s52* r12_10;
    int64_t r13_11;
    struct s0* r8_12;
    struct s53* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    struct s0* rsi18;
    struct s0* v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    struct s0** rax22;
    struct s0** rsi23;
    uint64_t* r15_24;
    struct s0* rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    struct s0* rdi28;
    struct s0* r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    struct s0* rax32;
    struct s54* r15_33;
    struct s0* rax34;
    uint64_t r11_35;
    struct s0* v36;
    struct s55* r15_37;
    struct s0** r13_38;
    struct s56* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s57* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<struct s0*>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s50*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<struct s0*>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x8ad8;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x922c;
        rsi18 = reinterpret_cast<struct s0*>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_8d9d_12;
    } else {
        addr_8944_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<struct s0**>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<struct s0*>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<struct s0*>(0);
            goto addr_897f_17;
        }
    }
    rax25 = malloc(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x8f0a;
    addr_8eb0_19:
    rdx30 = *r15_31;
    rax32 = fun_25b0(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_8de6_20:
    r15_33->f8 = r8_12;
    goto addr_8944_13;
    addr_8d9d_12:
    rax34 = realloc(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x90f7;
    if (v36 != r15_37->f8) 
        goto addr_8de6_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_8eb0_19;
    addr_897f_17:
    r13_38 = reinterpret_cast<struct s0**>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x8adc;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x89c0;
    goto 0x8549;
}

void fun_8c5f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x8928;
    if (dl2 & 4) 
        goto 0x8928;
    if (edx3 > 7) 
        goto 0x8928;
    if (dl4 & 2) 
        goto 0x8928;
    goto 0x8928;
}

void fun_8ca8() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x8928;
    if (dl2 & 4) 
        goto 0x8928;
    if (edx3 > 7) 
        goto 0x8928;
    if (dl4 & 2) 
        goto 0x8928;
    goto 0x8928;
}

void fun_8cf0() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x8928;
    if (dl2 & 4) 
        goto 0x8928;
    if (edx3 > 7) 
        goto 0x8928;
    if (dl4 & 2) 
        goto 0x8928;
    goto 0x8928;
}

void fun_8d38() {
    goto 0x8928;
}

void fun_3cfe() {
    goto 0x3972;
}

void fun_3ed4() {
    goto 0x3e8c;
}

void fun_3f9b() {
    goto 0x3ac8;
}

void fun_3fed() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x3f70;
    goto 0x3b9f;
}

void fun_401f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x3f7b;
        goto 0x39a0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x3e1a;
        goto 0x3bbb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x47b8;
    if (r10_8 > r15_9) 
        goto addr_3f05_9;
    addr_3f0a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x47c3;
    goto 0x3af4;
    addr_3f05_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_3f0a_10;
}

void fun_4052() {
    goto 0x3b87;
}

void fun_4420() {
    goto 0x3b87;
}

void fun_4bbf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x3cdc;
    } else {
        goto 0x3e40;
    }
}

void fun_5bc0() {
}

void fun_695f() {
    if (__intrinsic()) 
        goto 0x6999; else 
        goto "???";
}

struct s58 {
    signed char[1] pad1;
    signed char f1;
};

void fun_7580() {
    signed char* r13_1;
    struct s58* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_7cce() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x7c47;
}

struct s59 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s60 {
    signed char[8] pad8;
    int64_t f8;
};

struct s61 {
    signed char[16] pad16;
    int64_t f10;
};

struct s62 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8400() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s59* rcx3;
    struct s60* rcx4;
    int64_t r11_5;
    struct s61* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s62* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x8306;
}

void fun_86b5() {
    goto 0x8688;
}

void fun_9004() {
    goto 0x8928;
}

void fun_8918() {
}

void fun_8d48() {
    goto 0x8928;
}

void fun_405c() {
    goto 0x3ff7;
}

void fun_442a() {
    goto 0x3f4d;
}

void fun_5c20() {
    fun_2490();
    goto fun_26e0;
}

void fun_7a60() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x7816;
    goto 0x79c0;
}

void fun_7cdc() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x7c47;
}

struct s63 {
    int32_t f0;
    int32_t f4;
};

struct s64 {
    int32_t f0;
    int32_t f4;
};

struct s65 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s66 {
    signed char[8] pad8;
    int64_t f8;
};

struct s67 {
    signed char[8] pad8;
    int64_t f8;
};

struct s68 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_83b0(struct s63* rdi, struct s64* rsi) {
    struct s65* rcx3;
    struct s66* rcx4;
    struct s67* rcx5;
    struct s68* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x8306;
}

void fun_86bf() {
    goto 0x8688;
}

void fun_900e() {
    goto 0x8928;
}

void fun_3d2d() {
    goto 0x3972;
}

void fun_4068() {
    goto 0x3ff7;
}

void fun_4437() {
    goto 0x3f9e;
}

void fun_5c60() {
    fun_2490();
    goto fun_26e0;
}

void fun_7ceb() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x7c47;
}

void fun_86c9() {
    goto 0x8688;
}

void fun_3d5a() {
    goto 0x3972;
}

void fun_4074() {
    goto 0x3f70;
}

void fun_5ca0() {
    fun_2490();
    goto fun_26e0;
}

void fun_86d3() {
    goto 0x8688;
}

void fun_3d7c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4710;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3c41;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3c41;
    }
    if (v11) 
        goto 0x4a73;
    if (r10_12 > r15_13) 
        goto addr_4ac3_8;
    addr_4ac8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4801;
    addr_4ac3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4ac8_9;
}

struct s69 {
    signed char[24] pad24;
    int64_t f18;
};

struct s70 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s71 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_5cf0() {
    int64_t r15_1;
    struct s69* rbx2;
    struct s0* r14_3;
    struct s70* rbx4;
    struct s0* r13_5;
    struct s71* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2490();
    fun_26e0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x5d12, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_5d48() {
    fun_2490();
    goto 0x5d19;
}

struct s72 {
    signed char[32] pad32;
    int64_t f20;
};

struct s73 {
    signed char[24] pad24;
    int64_t f18;
};

struct s74 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s75 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s76 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_5d80() {
    int64_t rcx1;
    struct s72* rbx2;
    int64_t r15_3;
    struct s73* rbx4;
    struct s0* r14_5;
    struct s74* rbx6;
    struct s0* r13_7;
    struct s75* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s76* rbx12;
    struct s0* rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2490();
    fun_26e0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x5db4, __return_address(), rcx1);
    goto v15;
}

void fun_5df8() {
    fun_2490();
    goto 0x5dbb;
}
