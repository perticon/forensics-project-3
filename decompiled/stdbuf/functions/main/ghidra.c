char main(int param_1,undefined8 *param_2)

{
  char **__argv;
  char cVar1;
  bool bVar2;
  int iVar3;
  int *piVar4;
  int *piVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  ulong uVar8;
  ulong extraout_RDX;
  char **ppcVar9;
  char *pcVar10;
  long lVar11;
  char *pcVar12;
  char *pcVar13;
  undefined1 *puVar14;
  long in_FS_OFFSET;
  undefined auVar15 [12];
  char *local_110;
  char *local_108;
  int *local_100;
  char *local_f8;
  char *local_f0 [3];
  stat local_d8;
  long local_40;
  
  puVar14 = (undefined1 *)0x411;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  pcVar12 = "+i:o:e:";
  textdomain("coreutils");
  exit_failure = 0x7d;
  atexit(close_stdout);
  pcVar13 = stdbuf;
LAB_00102805:
  piVar4 = (int *)0x0;
  iVar3 = getopt_long(param_1,param_2,pcVar12);
  if (iVar3 == -1) {
    puVar14 = stdbuf;
    pcVar12 = "_STDBUF_";
    pcVar13 = "%s%c=%lu";
    __argv = (char **)(param_2 + optind);
    param_2 = &program_path;
    bVar2 = false;
    if (optind < param_1) {
      do {
        if (*(int **)((long)puVar14 + 0x10) != (int *)0x0) {
          if (*(char *)*(int **)((long)puVar14 + 0x10) == 'L') {
            if (*(int *)((long)puVar14 + 8) + 0x80U < 0x180) {
              __ctype_toupper_loc();
            }
            iVar3 = rpl_asprintf(&local_110,"%s%c=L","_STDBUF_");
          }
          else {
            piVar4 = *(int **)puVar14;
            if (*(int *)((long)puVar14 + 8) + 0x80U < 0x180) {
              __ctype_toupper_loc();
            }
            iVar3 = rpl_asprintf(&local_110,"%s%c=%lu","_STDBUF_");
          }
          if (iVar3 < 0) goto LAB_00102cfa;
          iVar3 = putenv(local_110);
          if (iVar3 != 0) {
            pcVar13 = (char *)quote(local_110);
            pcVar12 = (char *)dcgettext(0,"failed to update the environment with %s",5);
            piVar4 = __errno_location();
            error(0x7d,*piVar4,pcVar12,pcVar13);
            goto LAB_00102da7;
          }
          bVar2 = true;
        }
        pcVar10 = program_name;
        puVar14 = (undefined1 *)((long)puVar14 + 0x18);
      } while ((char **)puVar14 != &program_path);
      if (bVar2) {
        pcVar13 = strchr(program_name,0x2f);
        if (pcVar13 == (char *)0x0) {
          pcVar13 = (char *)xreadlink("/proc/self/exe");
          if (pcVar13 == (char *)0x0) goto LAB_00102df1;
          program_path = (char *)dir_name(pcVar13);
          goto LAB_00102c5b;
        }
        program_path = (char *)dir_name(pcVar10);
        goto LAB_00102a62;
      }
LAB_00102da7:
      pcVar10 = "you must specify a buffering mode option";
    }
    else {
      pcVar10 = "missing operand";
    }
LAB_00102966:
    uVar6 = dcgettext(0,pcVar10,5);
    error(0,0,uVar6);
LAB_0010287e:
    iVar3 = usage(0x7d);
    uVar8 = extraout_RDX;
LAB_00102890:
    pcVar10 = optarg;
    lVar11 = (long)(int)*(uint *)(CSWTCH_55 + uVar8 * 4);
    if (2 < *(uint *)(CSWTCH_55 + uVar8 * 4)) {
                    /* WARNING: Subroutine does not return */
      __assert_fail("0 <= opt_fileno && opt_fileno < ARRAY_CARDINALITY (stdbuf)","src/stdbuf.c",
                    0x150,(char *)&__PRETTY_FUNCTION___0);
    }
    *(int *)(pcVar13 + lVar11 * 0x18 + 8) = iVar3;
    do {
      cVar1 = *pcVar10;
      if (cVar1 < '\x0e') {
        if (cVar1 < '\t') goto LAB_001028c2;
      }
      else if (cVar1 != ' ') goto LAB_001028c2;
      pcVar10 = pcVar10 + 1;
      optarg = pcVar10;
    } while( true );
  }
  if (iVar3 != -0x82) {
    if (iVar3 < -0x81) {
      if (iVar3 != -0x83) goto LAB_0010287e;
      version_etc(stdout,"stdbuf","GNU coreutils",Version,"Padraig Brady",0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    uVar8 = (ulong)(iVar3 - 0x65U);
    if (10 < iVar3 - 0x65U) goto LAB_0010287e;
    if (((ulong)puVar14 >> (uVar8 & 0x3f) & 1) != 0) goto LAB_00102890;
    goto LAB_0010287e;
  }
  auVar15 = usage(0);
LAB_00102cad:
  if (SUB124(auVar15 >> 0x40,0) == 1) {
LAB_00102d50:
    iVar3 = 0x4b;
  }
  else {
    iVar3 = *SUB128(auVar15,0);
  }
  *piVar4 = iVar3;
  uVar6 = quote(optarg);
  uVar7 = dcgettext(0,"invalid mode %s",5);
  error(0x7d,*piVar4,uVar7,uVar6);
LAB_00102cfa:
                    /* WARNING: Subroutine does not return */
  xalloc_die();
LAB_001028c2:
  *(char **)(pcVar13 + lVar11 * 0x18 + 0x10) = pcVar10;
  if ((iVar3 != 0x69) || (*pcVar10 != 'L')) goto LAB_001028d4;
  pcVar10 = "line buffering stdin is meaningless";
  goto LAB_00102966;
LAB_001028d4:
  if ((*pcVar10 != 'L') || (pcVar10[1] != '\0')) {
    iVar3 = xstrtoumax(pcVar10,0,10,&local_100,"EGkKMPTYZ0");
    piVar4 = __errno_location();
    auVar15 = CONCAT48(iVar3,piVar4);
    if (iVar3 != 0) goto LAB_00102cad;
    *piVar4 = 0;
    *(int **)(pcVar13 + lVar11 * 0x18) = local_100;
  }
  goto LAB_00102805;
LAB_00102a62:
  pcVar13 = getenv("LD_PRELOAD");
  local_f0[1] = (char *)0x0;
  local_f8 = program_path;
  local_f0[0] = "/usr/local/libexec/coreutils";
  if (*program_path != '\0') {
    ppcVar9 = &local_f8;
    pcVar12 = program_path;
    do {
      iVar3 = rpl_asprintf(&local_100,"%s/%s",pcVar12,"libstdbuf.so");
      if (iVar3 < 0) goto LAB_00102cfa;
      iVar3 = stat((char *)local_100,&local_d8);
      if (iVar3 == 0) goto LAB_00102b0c;
      ppcVar9 = ppcVar9 + 1;
      free(local_100);
      pcVar12 = *ppcVar9;
      if (pcVar12 == (char *)0x0) {
        uVar6 = quote("libstdbuf.so");
        uVar7 = dcgettext(0,"failed to find %s",5);
        error(0x7d,0,uVar7,uVar6);
        goto LAB_00102d50;
      }
    } while (*pcVar12 != '\0');
  }
  local_100 = (int *)xstrdup("libstdbuf.so");
LAB_00102b0c:
  piVar4 = local_100;
  if (pcVar13 == (char *)0x0) {
    iVar3 = rpl_asprintf(&local_108,"%s=%s","LD_PRELOAD",local_100);
  }
  else {
    iVar3 = rpl_asprintf(&local_108,"%s=%s:%s","LD_PRELOAD");
  }
  if (iVar3 < 0) goto LAB_00102cfa;
  free(local_100);
  iVar3 = putenv(local_108);
  piVar5 = __errno_location();
  if (iVar3 == 0) {
    free(program_path);
    execvp(*__argv,__argv);
    iVar3 = *piVar5;
    uVar6 = quote(*__argv);
    uVar7 = dcgettext(0,"failed to run command %s",5);
    error(0,*piVar5,uVar7,uVar6);
    if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return (iVar3 == 2) + '~';
  }
  pcVar10 = (char *)quote(local_108);
  uVar6 = dcgettext(0,"failed to update the environment with %s",5);
  error(0x7d,*piVar5,uVar6,pcVar10);
LAB_00102df1:
  pcVar13 = getenv("PATH");
  if (pcVar13 != (char *)0x0) {
    pcVar13 = (char *)xstrdup(pcVar13);
    pcVar12 = strtok(pcVar13,":");
    while (pcVar12 != (char *)0x0) {
      pcVar12 = (char *)file_name_concat(pcVar12,pcVar10,0);
      iVar3 = access(pcVar12,1);
      if (iVar3 == 0) {
        program_path = (char *)dir_name();
        free(pcVar12);
        break;
      }
      free(pcVar12);
      pcVar12 = strtok((char *)0x0,":");
    }
  }
LAB_00102c5b:
  free(pcVar13);
  if (program_path == (char *)0x0) {
    program_path = (char *)xstrdup("/usr/local/lib/coreutils");
  }
  goto LAB_00102a62;
}