void main(int param_1,undefined8 *param_2)

{
  int iVar1;
  uint uVar2;
  undefined8 uVar3;
  long lVar4;
  undefined8 uVar5;
  int *piVar6;
  undefined8 extraout_RDX;
  char *pcVar7;
  long in_FS_OFFSET;
  undefined auVar8 [16];
  undefined8 uStack328;
  undefined auStack320 [4];
  int local_13c;
  uint local_138;
  char local_131;
  byte local_119;
  __uid_t local_118;
  __gid_t local_114;
  undefined4 local_110;
  undefined4 local_10c;
  undefined local_108 [4];
  char local_104;
  long local_100;
  undefined local_f8;
  long local_f0;
  long local_e8 [2];
  stat local_d8;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  local_118 = 0xffffffff;
  local_114 = 0xffffffff;
  local_110 = 0xffffffff;
  local_10c = 0xffffffff;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  pcVar7 = &DAT_0010dc40;
  textdomain("coreutils");
  atexit(close_stdout);
  chopt_init(local_108);
  local_13c = -1;
  local_138 = 0x10;
  local_131 = '\0';
  do {
    iVar1 = getopt_long(param_1,param_2,"HLPRcfhv",long_options,0);
    if (iVar1 == -1) {
      if (local_104 == '\0') {
LAB_00102ad9:
        local_138 = 0x10;
      }
      else if (local_138 == 0x10) {
        if (local_13c == 1) {
          uVar3 = dcgettext(0,"-R --dereference requires either -H or -L",5);
          error(1,0,uVar3);
          goto LAB_00102e83;
        }
        local_13c = 0;
      }
      pcVar7 = reference_file;
      local_f8 = local_13c != 0;
      if (reference_file == (char *)0x0) {
        if (1 < param_1 - optind) {
          lVar4 = parse_user_spec_warn
                            (param_2[optind],&local_118,&local_114,&local_f0,local_e8,&local_119);
          if (lVar4 != 0) {
            uVar3 = quote(param_2[optind]);
            error(local_119 ^ 1,0,"%s: %s",lVar4,uVar3);
          }
          if (local_f0 == 0) goto LAB_00102e41;
          goto LAB_00102dd8;
        }
      }
      else if (0 < param_1 - optind) {
        iVar1 = stat(reference_file,&local_d8);
        if (iVar1 != 0) {
LAB_00102e83:
          uVar3 = quotearg_style(4,pcVar7);
          uVar5 = dcgettext(0,"failed to get attributes of %s",5);
          piVar6 = __errno_location();
          auVar8 = error(1,*piVar6,uVar5,uVar3);
          uVar3 = uStack328;
          uStack328 = SUB168(auVar8,0);
          (*(code *)PTR___libc_start_main_00112fc0)
                    (main,uVar3,auStack320,0,0,SUB168(auVar8 >> 0x40,0),&uStack328);
          do {
                    /* WARNING: Do nothing block with infinite loop */
          } while( true );
        }
        local_118 = local_d8.st_uid;
        local_114 = local_d8.st_gid;
        local_f0 = uid_to_name();
        local_e8[0] = gid_to_name(local_d8.st_gid);
        break;
      }
      if (optind < param_1) {
        pcVar7 = (char *)quote(param_2[(long)param_1 + -1]);
        uVar3 = dcgettext(0,"missing operand after %s",5);
        error(0,0,uVar3,pcVar7);
      }
      else {
        uVar3 = dcgettext(0,"missing operand",5);
        error(0,0,uVar3);
      }
    }
    else if (iVar1 < 0x85) {
      if (iVar1 < 0x48) {
        if (iVar1 == -0x83) {
          version_etc(stdout,"chown","GNU coreutils",Version,"David MacKenzie","Jim Meyering",0,
                      extraout_RDX);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (iVar1 == -0x82) {
          usage(0);
          goto LAB_00102ad9;
        }
      }
      else if (iVar1 - 0x48U < 0x3d) {
                    /* WARNING: Could not recover jumptable at 0x00102ab7. Too many branches */
                    /* WARNING: Treating indirect jump as call */
        (*(code *)(pcVar7 + *(int *)(pcVar7 + (ulong)(iVar1 - 0x48U) * 4)))();
        return;
      }
    }
    usage();
    reference_file = optarg;
  } while( true );
LAB_00102cf9:
  if ((local_104 == '\0') || (local_131 == '\0')) {
LAB_00102d0b:
    uVar2 = chown_files(param_2 + optind,local_138 | 0x400,local_118,local_114,local_110,local_10c,
                        local_108,(long)optind);
                    /* WARNING: Subroutine does not return */
    exit((uVar2 ^ 1) & 0xff);
  }
  local_100 = get_root_dev_ino(dev_ino_buf_0);
  if (local_100 != 0) goto LAB_00102d0b;
  uVar3 = quotearg_style(4,"/");
  uVar5 = dcgettext(0,"failed to get attributes of %s",5);
  piVar6 = __errno_location();
  error(1,*piVar6,uVar5,uVar3);
LAB_00102e41:
  if (local_e8[0] != 0) {
    local_f0 = xstrdup("");
  }
LAB_00102dd8:
  optind = optind + 1;
  goto LAB_00102cf9;
}