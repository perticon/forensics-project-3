void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000028D0(fn00000000000025B0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l000000000000301E;
	}
	fn0000000000002800(fn00000000000025B0(0x05, "Usage: %s [OPTION]... [OWNER][:[GROUP]] FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n", null), 0x01);
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "Change the owner and/or group of each FILE to OWNER and/or GROUP.\nWith --reference, change the owner and group of each FILE to those of RFILE.\n\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "      --dereference      affect the referent of each symbolic link (this is\n                         the default), rather than the symbolic link itself\n  -h, --no-dereference   affect symbolic links instead of any referenced file\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "                         (useful only on systems that can change the\n                         ownership of a symlink)\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "      --from=CURRENT_OWNER:CURRENT_GROUP\n                         change the owner and/or group of each file only if\n                         its current owner and/or group match those specified\n                         here.  Either may be omitted, in which case a match\n                         is not required for the omitted attribute\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "      --no-preserve-root  do not treat '/' specially (the default)\n      --preserve-root    fail to operate recursively on '/'\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "      --reference=RFILE  use RFILE's owner and group rather than\n                         specifying OWNER:GROUP values\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "  -R, --recursive        operate on files and directories recursively\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "\nThe following options modify how a hierarchy is traversed when the -R\noption is also specified.  If more than one is specified, only the final\none takes effect.\n\n  -H                     if a command line argument is a symbolic link\n                         to a directory, traverse it\n  -L                     traverse every symbolic link to a directory\n                         encountered\n  -P                     do not traverse any symbolic links (default)\n\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "      --help        display this help and exit\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "      --version     output version information and exit\n", null));
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "\nOwner is unchanged if missing.  Group is unchanged if missing, but changed\nto login group if implied by a ':' following a symbolic OWNER.\nOWNER and GROUP may be numeric as well as symbolic.\n", null));
	fn0000000000002800(fn00000000000025B0(0x05, "\nExamples:\n  %s root /u        Change the owner of /u to \"root\".\n  %s root:staff /u  Likewise, but also change its group to \"staff\".\n  %s -hR root /u    Change the owner of /u and subfiles to \"root\".\n", null), 0x01);
	struct Eq_1091 * rbx_279 = fp - 0xB8 + 16;
	do
	{
		char * rsi_281 = rbx_279->qw0000;
		++rbx_279;
	} while (rsi_281 != null && fn00000000000026F0(rsi_281, "chown") != 0x00);
	ptr64 r13_294 = rbx_279->qw0008;
	if (r13_294 != 0x00)
	{
		fn0000000000002800(fn00000000000025B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_8 rax_381 = fn00000000000027F0(null, 0x05);
		if (rax_381 == 0x00 || fn00000000000024F0(0x03, "en_", rax_381) == 0x00)
			goto l0000000000003356;
	}
	else
	{
		fn0000000000002800(fn00000000000025B0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_8 rax_323 = fn00000000000027F0(null, 0x05);
		if (rax_323 == 0x00 || fn00000000000024F0(0x03, "en_", rax_323) == 0x00)
		{
			fn0000000000002800(fn00000000000025B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003393:
			fn0000000000002800(fn00000000000025B0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l000000000000301E:
			fn00000000000028B0(edi);
		}
		r13_294 = 0xD004;
	}
	fn00000000000026D0(stdout, fn00000000000025B0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003356:
	fn0000000000002800(fn00000000000025B0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003393;
}