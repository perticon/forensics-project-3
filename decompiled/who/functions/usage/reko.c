void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000027A0(fn0000000000002540(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000003DCE;
	}
	fn0000000000002720(fn0000000000002540(0x05, "Usage: %s [OPTION]... [ FILE | ARG1 ARG2 ]\n", null), 0x01);
	fn0000000000002610(stdout, fn0000000000002540(0x05, "Print information about users who are currently logged in.\n", null));
	fn0000000000002610(stdout, fn0000000000002540(0x05, "\n  -a, --all         same as -b -d --login -p -r -t -T -u\n  -b, --boot        time of last system boot\n  -d, --dead        print dead processes\n  -H, --heading     print line of column headings\n", null));
	fn0000000000002610(stdout, fn0000000000002540(0x05, "  -l, --login       print system login processes\n", null));
	fn0000000000002610(stdout, fn0000000000002540(0x05, "      --lookup      attempt to canonicalize hostnames via DNS\n  -m                only hostname and user associated with stdin\n  -p, --process     print active processes spawned by init\n", null));
	fn0000000000002610(stdout, fn0000000000002540(0x05, "  -q, --count       all login names and number of users logged on\n  -r, --runlevel    print current runlevel\n  -s, --short       print only name, line, and time (default)\n  -t, --time        print last system clock change\n", null));
	fn0000000000002610(stdout, fn0000000000002540(0x05, "  -T, -w, --mesg    add user's message status as +, - or ?\n  -u, --users       list users logged in\n      --message     same as -T\n      --writable    same as -T\n", null));
	fn0000000000002610(stdout, fn0000000000002540(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002610(stdout, fn0000000000002540(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002720(fn0000000000002540(0x05, "\nIf FILE is not specified, use %s.  %s as FILE is common.\nIf ARG1 ARG2 given, -m presumed: 'am i' or 'mom likes' are usual.\n", null), 0x01);
	struct Eq_1861 * rbx_224 = fp - 0xB8 + 16;
	do
	{
		char * rsi_226 = rbx_224->qw0000;
		++rbx_224;
	} while (rsi_226 != null && fn0000000000002630(rsi_226, "who") != 0x00);
	ptr64 r13_239 = rbx_224->qw0008;
	if (r13_239 != 0x00)
	{
		fn0000000000002720(fn0000000000002540(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_15 rax_326 = fn0000000000002710(null, 0x05);
		if (rax_326 == 0x00 || fn00000000000024A0(0x03, &g_tA0AC.b0024 + 0x00D6, rax_326) == 0x00)
			goto l000000000000406E;
	}
	else
	{
		fn0000000000002720(fn0000000000002540(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_15 rax_268 = fn0000000000002710(null, 0x05);
		if (rax_268 == 0x00 || fn00000000000024A0(0x03, &g_tA0AC.b0024 + 0x00D6, rax_268) == 0x00)
		{
			fn0000000000002720(fn0000000000002540(0x05, "Full documentation <%s%s>\n", null), 0x01);
l00000000000040AB:
			fn0000000000002720(fn0000000000002540(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000003DCE:
			fn0000000000002780(edi);
		}
		r13_239 = 0xA102;
	}
	fn0000000000002610(stdout, fn0000000000002540(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l000000000000406E:
	fn0000000000002720(fn0000000000002540(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l00000000000040AB;
}