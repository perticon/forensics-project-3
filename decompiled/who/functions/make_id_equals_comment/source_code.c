make_id_equals_comment (STRUCT_UTMP const *utmp_ent)
{
  size_t utmpsize = sizeof UT_ID (utmp_ent);
  char *comment = xmalloc (strlen (_("id=")) + utmpsize + 1);

  char *p = stpcpy (comment, _("id="));
  stzncpy (p, UT_ID (utmp_ent), utmpsize);
  return comment;
}