struct s0* make_id_equals_comment(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9) {
    struct s0* rax7;
    struct s0* rax8;
    struct s0* rdi9;
    struct s0* rax10;
    struct s0* r12_11;
    struct s0* rax12;
    signed char* rax13;
    unsigned char* rdx14;
    unsigned char* rsi15;
    uint32_t ecx16;

    rax7 = fun_2540();
    rax8 = fun_2560(rax7, rax7);
    rdi9 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax8) + 5);
    rax10 = xmalloc(rdi9, "id=", rdi9, "id=");
    r12_11 = rax10;
    rax12 = fun_2540();
    rax13 = fun_2530(r12_11, rax12, 5, rcx, r8, r9);
    rdx14 = &rdi->f28;
    rsi15 = &rdi->f2c;
    do {
        ecx16 = *rdx14;
        if (!*reinterpret_cast<signed char*>(&ecx16)) 
            break;
        ++rdx14;
        ++rax13;
        *(rax13 - 1) = *reinterpret_cast<signed char*>(&ecx16);
    } while (reinterpret_cast<uint64_t>(rsi15) > reinterpret_cast<uint64_t>(rdx14));
    *rax13 = 0;
    return r12_11;
}