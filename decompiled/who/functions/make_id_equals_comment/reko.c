Eq_15 make_id_equals_comment(struct Eq_886 * rdi)
{
	Eq_15 rax_31 = xmalloc((word64) fn0000000000002560(fn0000000000002540(0x05, "id=", null)) + 5);
	struct Eq_899 * rax_46 = fn0000000000002530(fn0000000000002540(0x05, "id=", null), rax_31);
	byte * rdx_47 = &rdi->b0008 + 32;
	do
	{
		byte cl_53 = *rdx_47;
		if (cl_53 == 0x00)
			break;
		++rax_46;
		rax_46->bFFFFFFFF = cl_53;
		++rdx_47;
	} while (&rdi->b0008 + 36 > rdx_47);
	rax_46->b0000 = 0x00;
	return rax_31;
}