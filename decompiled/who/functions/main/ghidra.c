undefined  [16] main(int param_1,undefined8 *param_2)

{
  bool bVar1;
  char cVar2;
  int iVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  ulong uStack56;
  
  bVar1 = true;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  while (iVar3 = getopt_long(param_1,param_2,"abdlmpqrstuwHT",longopts,0), iVar3 != -1) {
    if (0x80 < iVar3) goto switchD_0010292b_caseD_49;
    if (iVar3 < 0x48) {
      if (iVar3 == -0x83) {
        version_etc(stdout,&DAT_0010a102,"GNU coreutils",Version,"Joseph Arceneaux",
                    "David MacKenzie","Michael Stone",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 == -0x82) {
                    /* WARNING: Subroutine does not return */
        usage(0);
      }
      goto switchD_0010292b_caseD_49;
    }
    switch(iVar3) {
    case 0x48:
      include_heading = 1;
      break;
    default:
      goto switchD_0010292b_caseD_49;
    case 0x54:
    case 0x77:
      include_mesg = 1;
      break;
    case 0x61:
      need_boottime = 1;
      bVar1 = false;
      need_deadprocs = 1;
      need_login = 1;
      need_initspawn = 1;
      need_runlevel = 1;
      need_clockchange = 1;
      need_users = 1;
      include_mesg = 1;
      include_idle = 1;
      include_exit = '\x01';
      break;
    case 0x62:
      need_boottime = 1;
      bVar1 = false;
      break;
    case 100:
      need_deadprocs = 1;
      bVar1 = false;
      include_idle = 1;
      include_exit = '\x01';
      break;
    case 0x6c:
      need_login = 1;
      bVar1 = false;
      include_idle = 1;
      break;
    case 0x6d:
      my_line_only = 1;
      break;
    case 0x70:
      need_initspawn = 1;
      bVar1 = false;
      break;
    case 0x71:
      short_list = 1;
      break;
    case 0x72:
      need_runlevel = 1;
      bVar1 = false;
      include_idle = 1;
      break;
    case 0x73:
      short_output = 1;
      break;
    case 0x74:
      need_clockchange = 1;
      bVar1 = false;
      break;
    case 0x75:
      need_users = 1;
      bVar1 = false;
      include_idle = 1;
      break;
    case 0x80:
      do_lookup = 1;
    }
  }
  if (bVar1) {
    need_users = 1;
    short_output = 1;
  }
  if (include_exit != '\0') {
    short_output = 0;
  }
  cVar2 = hard_locale(2);
  time_format = "%b %e %H:%M";
  if (cVar2 != '\0') {
    time_format = "%Y-%m-%d %H:%M";
  }
  time_format_width = (-(uint)(cVar2 == '\0') & 0xfffffffc) + 0x10;
  param_1 = param_1 - optind;
  if (param_1 == 1) {
    who(param_2[optind],0);
  }
  else {
    if (param_1 < 2) {
      if (1 < param_1 + 1U) {
LAB_00102b9d:
        uVar4 = quote(param_2[(long)optind + 2]);
        uVar5 = dcgettext(0,"extra operand %s",5);
        error(0,0,uVar5,uVar4);
switchD_0010292b_caseD_49:
                    /* WARNING: Subroutine does not return */
        usage(1);
      }
    }
    else {
      if (param_1 != 2) goto LAB_00102b9d;
      my_line_only = 1;
    }
    who("/var/run/utmp",1);
  }
  return ZEXT816(uStack56) << 0x40;
}