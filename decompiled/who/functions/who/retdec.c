void who(char * filename, int32_t options) {
    // 0x3590
    __readfsqword(40);
    struct utmpx * v1; // bp-88, 0x3590
    int32_t * utmp_buf; // bp-96, 0x3590
    if (read_utmp(filename, (int64_t *)&utmp_buf, &v1, options) != 0) {
        // 0x3d34
        quotearg_n_style_colon();
        function_2480();
        function_2750();
        // 0x3d63
        function_2570();
        return;
    }
    // 0x35ca
    int64_t v2; // bp-136, 0x3590
    int64_t v3 = &v2; // 0x359f
    int64_t v4 = (int64_t)v1; // 0x35d1
    int64_t v5 = (int64_t)utmp_buf; // 0x35d6
    int64_t v6; // 0x3590
    if (*(char *)&short_list != 0) {
        // 0x3809
        if (utmp_buf != NULL) {
            if (*(char *)(v4 + 44) != 0) {
                // 0x383e
                if (*(int16_t *)v4 == 7) {
                    // 0x38a7
                    extract_trimmed_name((struct utmpx *)v4);
                    function_2720();
                    function_2450();
                }
            }
            int64_t v7 = v5 - 1;
            int64_t v8 = v7; // 0x3850
            int64_t v9 = v4 + 384; // 0x3850
            while (v7 != 0) {
                int64_t v10 = v9;
                if (*(char *)(v10 + 44) != 0) {
                    // 0x383e
                    if (*(int16_t *)v10 == 7) {
                        // 0x38a7
                        extract_trimmed_name((struct utmpx *)v10);
                        function_2720();
                        function_2450();
                    }
                }
                // 0x3845
                v7 = v8 - 1;
                v8 = v7;
                v9 = v10 + 384;
            }
        }
        // 0x3855
        function_2540();
        function_2720();
        v6 = v3;
        goto lab_0x387a_2;
    } else {
        // 0x35e1
        if (*(char *)&include_heading != 0) {
            int64_t v11 = function_2540(); // 0x3ba9
            int64_t v12 = function_2540(); // 0x3bc1
            int64_t v13 = function_2540(); // 0x3bd9
            int64_t v14 = function_2540(); // 0x3bef
            int64_t v15 = function_2540(); // 0x3c05
            int64_t v16 = function_2540(); // 0x3c1b
            print_line(-1, (char *)function_2540(), 32, -1, (char *)v16, (char *)v15, (char *)v14, (char *)v13, (char *)v12, (char *)v11);
        }
        // 0x35ee
        if (*(char *)&my_line_only != 0) {
            // 0x3b63
            v6 = v3;
            if (function_26f0() == 0) {
                goto lab_0x387a_2;
            } else {
                // 0x3b76
                function_24a0();
                goto lab_0x35fd;
            }
        } else {
            goto lab_0x35fd;
        }
    }
  lab_0x3668:;
    // 0x3668
    int64_t v17; // 0x3662
    int16_t * v18 = (int16_t *)v17;
    int16_t v19 = *v18; // 0x366f
    int64_t v20; // 0x3590
    int16_t v21; // 0x3590
    int64_t v22; // 0x3590
    int64_t v23; // 0x3590
    int64_t v24; // 0x3590
    if (*(char *)&need_users == 0) {
        goto lab_0x3687;
    } else {
        // 0x3676
        if (v19 == 7 == (*(char *)(v20 + 36) != 0)) {
            // 0x3c69
            print_user((struct utmpx *)v17, v22);
            v21 = *v18;
            v23 = v24;
            goto lab_0x3639;
        } else {
            goto lab_0x3687;
        }
    }
  lab_0x3687:;
    // 0x3687
    int16_t * v25; // 0x3590
    int64_t v26; // 0x3590
    int64_t v27; // 0x3590
    int64_t v28; // 0x3590
    int64_t v29; // 0x3590
    int64_t v30; // 0x3a9a
    int64_t v31; // 0x3abd
    if (v19 == 1 == (*(char *)&need_runlevel != 0)) {
        uint32_t v32 = *(int32_t *)(v20 - 4); // 0x38e5
        if (g38 == NULL) {
            // 0x3cae
            function_2540();
            function_2560();
            g38 = (char *)xmalloc();
        }
        // 0x390c
        function_2540();
        function_2810();
        if (g37 == 0) {
            // 0x3c7e
            function_2540();
            function_2560();
            g37 = xmalloc();
        }
        // 0x3952
        function_2540();
        function_2810();
        int64_t v33 = (int32_t)((0x100000000 * (int64_t)(v32 / 0x80000000) | (int64_t)v32) / 256) % 256 < 127 ? g37 : (int64_t)&g16;
        int64_t v34 = time_string_isra_0(*(int32_t *)(v20 + 332)); // 0x39b7
        *(int64_t *)(v24 - 8) = (int64_t)&g16;
        *(int64_t *)(v24 - 16) = v33;
        *(int64_t *)(v24 - 24) = (int64_t)&g16;
        int64_t v35 = v24 - 32; // 0x39cc
        *(int64_t *)v35 = (int64_t)&g16;
        v26 = (int64_t)g38;
        v27 = v34;
        v28 = v35;
        goto lab_0x3a07;
    } else {
        // 0x369a
        if (v19 == 2 == (*(char *)&need_boottime != 0)) {
            // 0x39d0
            v29 = time_string_isra_0(*(int32_t *)(v20 + 332));
            goto lab_0x39eb;
        } else {
            // 0x36ad
            if (v19 == 3 == (*(char *)&need_clockchange != 0)) {
                // 0x3a2c
                v29 = time_string_isra_0(*(int32_t *)(v20 + 332));
                goto lab_0x39eb;
            } else {
                // 0x36c0
                if (v19 == 5 == (*(char *)&need_initspawn != 0)) {
                    char * v36 = make_id_equals_comment((struct utmpx *)v17); // 0x3a62
                    int64_t * v37 = (int64_t *)(v24 + 8); // 0x3a89
                    *v37 = v24 + 60;
                    function_2810();
                    v30 = time_string_isra_0(*(int32_t *)(v20 + 332));
                    *(int64_t *)(v24 - 8) = (int64_t)&g16;
                    *(int64_t *)(v24 - 16) = (int64_t)v36;
                    *(int64_t *)(v24 - 24) = *v37;
                    v31 = v24 - 32;
                    *(int64_t *)v31 = (int64_t)&g16;
                    goto lab_0x3abf;
                } else {
                    // 0x36d3
                    if (v19 == 6 == (*(char *)&need_login != 0)) {
                        char * v38 = make_id_equals_comment((struct utmpx *)v17); // 0x3ae7
                        int64_t * v39 = (int64_t *)(v24 + 16); // 0x3b0e
                        *v39 = v24 + 60;
                        function_2810();
                        int32_t v40 = *(int32_t *)(v20 + 332); // 0x3b18
                        int64_t v41 = time_string_isra_0(v40); // 0x3b1f
                        int64_t * v42 = (int64_t *)(v24 + 8); // 0x3b32
                        *v42 = v41;
                        int64_t v43 = function_2540(); // 0x3b37
                        *(int64_t *)(v24 - 8) = (int64_t)&g16;
                        *(int64_t *)(v24 - 16) = (int64_t)v38;
                        int64_t v44 = *v39; // 0x3b48
                        *(int64_t *)(v24 - 24) = v44;
                        int64_t v45 = v24 - 32; // 0x3b57
                        *(int64_t *)v45 = (int64_t)&g16;
                        goto lab_0x3abf;
                    } else {
                        // 0x36e6
                        v21 = v19;
                        v23 = v24;
                        if (v19 != 8 | *(char *)&need_deadprocs == 0) {
                            goto lab_0x3639;
                        } else {
                            char * v46 = make_id_equals_comment((struct utmpx *)v17); // 0x3705
                            function_2810();
                            int64_t * v47; // 0x3590
                            int64_t v48; // 0x3590
                            if (g36 == 0) {
                                // 0x3cde
                                function_2540();
                                int64_t v49 = function_2560(); // 0x3cf4
                                int64_t v50 = v24 + 8;
                                int64_t * v51 = (int64_t *)v50;
                                *v51 = v49;
                                function_2540();
                                function_2560();
                                g36 = xmalloc();
                                v47 = v51;
                                v48 = v50;
                            } else {
                                int64_t v52 = v24 + 8; // 0x3775
                                v47 = (int64_t *)v52;
                                v48 = v52;
                            }
                            int32_t * v53 = (int32_t *)(v24 + 28); // 0x3750
                            *v53 = (int32_t)*(int16_t *)(v20 + 326);
                            int64_t v54 = function_2540(); // 0x3755
                            int64_t * v55 = (int64_t *)(v24 + 16); // 0x3770
                            *v55 = v54;
                            *(int32_t *)v48 = (int32_t)*(int16_t *)(v20 + 324);
                            function_2540();
                            *(int64_t *)(v24 - 8) = (int64_t)*v53;
                            *(int64_t *)(v24 - 16) = *v55;
                            function_2810();
                            *v47 = g36;
                            int64_t v56 = time_string_isra_0(*(int32_t *)(v20 + 332)); // 0x37c5
                            *(int64_t *)(v24 - 24) = *v47;
                            *(int64_t *)(v24 - 32) = (int64_t)v46;
                            *(int64_t *)(v24 - 40) = v24 + 60;
                            *(int64_t *)(v24 - 48) = (int64_t)&g16;
                            print_line(-1, (char *)&g16, 32, 32, (char *)v20, (char *)v56, (char *)&g48, (char *)&g48, (char *)&g48, (char *)&g48);
                            function_2450();
                            v25 = v18;
                            goto lab_0x3634;
                        }
                    }
                }
            }
        }
    }
  lab_0x3634:
    // 0x3634
    v21 = *v25;
    v23 = v24;
    goto lab_0x3639;
  lab_0x3639:;
    int64_t v57 = v22; // 0x363d
    if (v21 == 2) {
        // 0x363f
        v57 = (int64_t)*(int32_t *)(v20 + 332);
    }
    // 0x3646
    int64_t v58; // 0x3590
    int64_t v59 = v58 - 1;
    int64_t v60 = v23; // 0x3655
    int64_t v61 = v57; // 0x3655
    v58 = v59;
    int64_t v62 = v20 + 384; // 0x3655
    v6 = v23;
    if (v59 == 0) {
        // break -> 0x387a
        goto lab_0x387a_2;
    }
    goto lab_0x365b;
  lab_0x3a07:
    // 0x3a07
    print_line(-1, (char *)&g16, 32, -1, (char *)v26, (char *)v27, (char *)&g48, (char *)&g48, (char *)&g48, (char *)&g48);
    v21 = *v18;
    v23 = v28 + 32;
    goto lab_0x3639;
  lab_0x39eb:;
    int64_t v63 = function_2540(); // 0x39f4
    *(int64_t *)(v24 - 8) = (int64_t)&g16;
    *(int64_t *)(v24 - 16) = (int64_t)&g16;
    *(int64_t *)(v24 - 24) = (int64_t)&g16;
    int64_t v64 = v24 - 32; // 0x3a05
    *(int64_t *)v64 = (int64_t)&g16;
    v26 = v63;
    v27 = v29;
    v28 = v64;
    goto lab_0x3a07;
  lab_0x3abf:;
    int64_t v65 = (int64_t)&g16;
    print_line(-1, (char *)v65, 32, 32, (char *)v20, (char *)v30, (char *)&g48, (char *)&g48, (char *)&g48, (char *)&g48);
    function_2450();
    v21 = *v18;
    v23 = v31 + 32;
    goto lab_0x3639;
  lab_0x387a_2:
    // 0x387a
    function_2450();
    if (*(int64_t *)(v6 + 72) != __readfsqword(40)) {
        // 0x3d63
        function_2570();
        return;
    }
  lab_0x35fd:
    // 0x35fd
    v6 = v3;
    if (utmp_buf != NULL) {
        // 0x365b
        v60 = v3;
        v61 = -0x8000000000000000;
        v58 = v5;
        v62 = v4 + 8;
        while (true) {
          lab_0x365b:
            // 0x365b
            v20 = v62;
            v22 = v61;
            v24 = v60;
            v17 = v20 - 8;
            if (*(char *)&my_line_only != 0) {
                // 0x3620
                if ((int32_t)function_24a0() == 0) {
                    goto lab_0x3668;
                } else {
                    // 0x3620
                    v25 = (int16_t *)v17;
                    goto lab_0x3634;
                }
            } else {
                goto lab_0x3668;
            }
        }
    }
    goto lab_0x387a_2;
}