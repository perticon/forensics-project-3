void who(undefined8 param_1,undefined4 param_2)

{
  short *psVar1;
  short sVar2;
  ulong uVar3;
  short sVar4;
  int iVar5;
  void *pvVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  long lVar9;
  char *pcVar10;
  undefined8 uVar11;
  undefined8 uVar12;
  undefined8 uVar13;
  undefined8 uVar14;
  undefined8 uVar15;
  char *pcVar16;
  size_t sVar17;
  size_t sVar18;
  int *piVar19;
  long lVar20;
  short *psVar21;
  ulong uVar22;
  long lVar23;
  ulong uVar24;
  long in_FS_OFFSET;
  long local_60;
  short *local_58;
  undefined local_4c [12];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  iVar5 = read_utmp(param_1,&local_60,&local_58,param_2);
  psVar21 = local_58;
  if (iVar5 != 0) {
    uVar7 = quotearg_n_style_colon(0,3,param_1);
    piVar19 = __errno_location();
    error(1,*piVar19,"%s",uVar7);
    goto LAB_00103d63;
  }
  if (short_list == '\0') {
    if (include_heading != '\0') {
      uVar7 = dcgettext(0,&DAT_0010a087,5);
      uVar8 = dcgettext(0,"COMMENT",5);
      uVar11 = dcgettext(0,&DAT_0010a094,5);
      uVar12 = dcgettext(0,&DAT_0010a098,5);
      uVar13 = dcgettext(0,&DAT_0010a09d,5);
      uVar14 = dcgettext(0,&DAT_0010a0a2,5);
      uVar15 = dcgettext(0,&DAT_0010a0a7,5);
      print_line(0xffffffff,uVar15,0x20,0xffffffff,uVar14,uVar13,uVar12,uVar11,uVar8,uVar7);
    }
    pcVar10 = (char *)0x0;
    if (my_line_only != '\0') {
      pcVar10 = ttyname(0);
      if (pcVar10 == (char *)0x0) goto LAB_0010387a;
      iVar5 = strncmp(pcVar10,"/dev/",5);
      if (iVar5 == 0) {
        pcVar10 = pcVar10 + 5;
      }
    }
    psVar21 = psVar21 + 4;
    lVar23 = local_60 + -1;
    lVar20 = -0x8000000000000000;
    if (local_60 != 0) {
      do {
        psVar1 = psVar21 + -4;
        if ((my_line_only == '\0') || (iVar5 = strncmp(pcVar10,(char *)psVar21,0x20), iVar5 == 0)) {
          sVar4 = psVar21[-4];
          if ((need_users == '\0') || ((*(char *)(psVar21 + 0x12) == '\0' || (sVar4 != 7)))) {
            if ((need_runlevel == '\0') || (sVar4 != 1)) {
              if ((need_boottime == '\0') || (sVar4 != 2)) {
                if ((need_clockchange == '\0') || (sVar4 != 3)) {
                  if ((need_initspawn == '\0') || (sVar4 != 5)) {
                    if ((need_login == '\0') || (sVar4 != 6)) {
                      if ((need_deadprocs != '\0') && (sVar4 == 8)) {
                        pvVar6 = (void *)make_id_equals_comment(psVar1);
                        __sprintf_chk(local_4c,1,0xc,&DAT_0010a026,(long)*(int *)(psVar21 + -2));
                        if (exitstr_0 == 0) {
                          pcVar16 = (char *)dcgettext(0,"term=",5);
                          sVar17 = strlen(pcVar16);
                          pcVar16 = (char *)dcgettext(0,"exit=",5);
                          sVar18 = strlen(pcVar16);
                          exitstr_0 = xmalloc(sVar17 + 0xe + sVar18);
                        }
                        sVar4 = psVar21[0xa3];
                        uVar7 = dcgettext(0,"exit=",5);
                        sVar2 = psVar21[0xa2];
                        uVar8 = dcgettext(0,"term=",5);
                        __sprintf_chk(exitstr_0,1,0xffffffffffffffff,"%s%d %s%d",uVar8,(int)sVar2,
                                      uVar7,(int)sVar4);
                        lVar9 = exitstr_0;
                        uVar7 = time_string_isra_0(*(undefined4 *)(psVar21 + 0xa6));
                        print_line(0xffffffff,"",0x20,0x20,psVar21,uVar7,"",local_4c,pvVar6,lVar9);
                        free(pvVar6);
                        goto LAB_00103634;
                      }
                      goto LAB_00103639;
                    }
                    pvVar6 = (void *)make_id_equals_comment(psVar1);
                    __sprintf_chk(local_4c,1,0xc,&DAT_0010a026,(long)*(int *)(psVar21 + -2));
                    uVar7 = time_string_isra_0(*(undefined4 *)(psVar21 + 0xa6));
                    pcVar16 = (char *)dcgettext(0,"LOGIN",5);
                  }
                  else {
                    pvVar6 = (void *)make_id_equals_comment(psVar1);
                    __sprintf_chk(local_4c,1,0xc,&DAT_0010a026,(long)*(int *)(psVar21 + -2));
                    uVar7 = time_string_isra_0(*(undefined4 *)(psVar21 + 0xa6));
                    pcVar16 = "";
                  }
                  print_line(0xffffffff,pcVar16,0x20,0x20,psVar21,uVar7,"",local_4c,pvVar6,"");
                  free(pvVar6);
                  sVar4 = psVar21[-4];
                  goto LAB_00103639;
                }
                uVar7 = time_string_isra_0(*(undefined4 *)(psVar21 + 0xa6));
                pcVar16 = "clock change";
              }
              else {
                uVar7 = time_string_isra_0(*(undefined4 *)(psVar21 + 0xa6));
                pcVar16 = "system boot";
              }
              lVar9 = dcgettext(0,pcVar16,5);
              pcVar16 = "";
            }
            else {
              iVar5 = *(int *)(psVar21 + -2);
              uVar3 = (long)iVar5 / 0x100;
              if (runlevline_2 == 0) {
                pcVar16 = (char *)dcgettext(0,"run-level",5);
                sVar17 = strlen(pcVar16);
                runlevline_2 = xmalloc(sVar17 + 3);
              }
              uVar7 = dcgettext(0,"run-level",5);
              __sprintf_chk(runlevline_2,1,0xffffffffffffffff,"%s %c",uVar7,
                            (long)iVar5 % 0x100 & 0xff);
              if (comment_1 == (char *)0x0) {
                pcVar16 = (char *)dcgettext(0,"last=",5);
                sVar17 = strlen(pcVar16);
                comment_1 = (char *)xmalloc(sVar17 + 2);
              }
              uVar24 = uVar3 & 0xff;
              uVar22 = 0x53;
              if ((char)uVar3 != 'N') {
                uVar22 = uVar24;
              }
              uVar7 = dcgettext(0,"last=",5);
              __sprintf_chk(comment_1,1,0xffffffffffffffff,&DAT_0010a0c8,uVar7,uVar22);
              pcVar16 = comment_1;
              if (0x5e < (int)uVar24 - 0x20U) {
                pcVar16 = "";
              }
              uVar7 = time_string_isra_0(*(undefined4 *)(psVar21 + 0xa6));
              lVar9 = runlevline_2;
            }
            print_line(0xffffffff,"",0x20,0xffffffff,lVar9,uVar7,"","",pcVar16,"");
            sVar4 = psVar21[-4];
          }
          else {
            print_user(psVar1,lVar20);
            sVar4 = psVar21[-4];
          }
        }
        else {
LAB_00103634:
          sVar4 = psVar21[-4];
        }
LAB_00103639:
        if (sVar4 == 2) {
          lVar20 = (long)*(int *)(psVar21 + 0xa6);
        }
        lVar23 = lVar23 + -1;
        psVar21 = psVar21 + 0xc0;
      } while (lVar23 != -1);
    }
  }
  else {
    lVar23 = local_60;
    if (local_60 != 0) {
      lVar23 = 0;
      pcVar10 = "";
      do {
        local_60 = local_60 + -1;
        if ((*(char *)(psVar21 + 0x16) != '\0') && (*psVar21 == 7)) {
          pvVar6 = (void *)extract_trimmed_name(psVar21);
          __printf_chk(1,&DAT_0010a074,pcVar10,pvVar6);
          free(pvVar6);
          lVar23 = lVar23 + 1;
          pcVar10 = " ";
        }
        psVar21 = psVar21 + 0xc0;
      } while (local_60 != 0);
    }
    uVar7 = dcgettext(0,"\n# users=%lu\n",5);
    __printf_chk(1,uVar7,lVar23);
  }
LAB_0010387a:
  free(local_58);
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
LAB_00103d63:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}