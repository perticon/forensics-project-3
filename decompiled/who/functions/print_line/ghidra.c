void print_line(undefined4 param_1,undefined *param_2,undefined param_3,undefined4 param_4,
               undefined8 param_5,undefined8 param_6,char *param_7,char *param_8,undefined8 param_9,
               char *param_10)

{
  int iVar1;
  size_t sVar2;
  undefined *__ptr;
  char *pcVar3;
  char *pcVar4;
  char cVar5;
  long lVar6;
  undefined3 *puVar7;
  long in_FS_OFFSET;
  undefined8 uVar8;
  undefined *local_78;
  char *local_60;
  undefined local_55 [8];
  undefined local_4d [13];
  long local_40;
  
  cVar5 = short_output;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  mesg_9._1_1_ = param_3;
  if ((include_idle == '\0') || (short_output != '\0')) {
LAB_00102d44:
    local_55[0] = 0;
  }
  else {
    sVar2 = strlen(param_7);
    if (6 < sVar2) goto LAB_00102d44;
    __sprintf_chk(local_55,1,8," %-6s",param_7);
    cVar5 = short_output;
  }
  local_78 = local_55;
  if (cVar5 == '\0') {
    sVar2 = strlen(param_8);
    if (sVar2 < 0xc) {
      __sprintf_chk(local_4d,1,0xd," %10s",param_8);
      goto LAB_00102d7e;
    }
  }
  local_4d[0] = 0;
LAB_00102d7e:
  lVar6 = 1;
  if (include_exit != '\0') {
    sVar2 = strlen(param_10);
    lVar6 = sVar2 + 2;
    if (sVar2 < 0xc) {
      lVar6 = 0xe;
    }
  }
  uVar8 = 0x102daa;
  __ptr = (undefined *)xmalloc(lVar6);
  if (include_exit == '\0') {
    *__ptr = 0;
  }
  else {
    uVar8 = 0x102f10;
    __sprintf_chk(__ptr,1,0xffffffffffffffff," %-12s",param_10);
  }
  puVar7 = &mesg_9;
  if (include_mesg == '\0') {
    puVar7 = (undefined3 *)0x10ade1;
  }
  if (param_2 == (undefined *)0x0) {
    param_2 = &DAT_0010a004;
  }
  iVar1 = rpl_asprintf(&local_60,"%-8.*s%s %-12.*s %-*s%s%s %-8s%s",param_1,param_2,puVar7,param_4,
                       param_5,time_format_width,param_6,local_78,local_4d,param_9,__ptr,uVar8);
  pcVar4 = local_60;
  if (iVar1 != -1) {
    sVar2 = strlen(local_60);
    pcVar4 = pcVar4 + sVar2;
    do {
      pcVar3 = pcVar4;
      pcVar4 = pcVar3 + -1;
    } while (*pcVar4 == ' ');
    *pcVar3 = '\0';
    puts(local_60);
    free(local_60);
    if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
      free(__ptr);
      return;
    }
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
                    /* WARNING: Subroutine does not return */
  xalloc_die();
}