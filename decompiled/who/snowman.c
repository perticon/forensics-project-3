
struct s0 {
    struct s0* f0;
    unsigned char f1;
    unsigned char f2;
    unsigned char f3;
    int32_t f4;
    struct s0* f8;
    signed char[7] pad16;
    uint32_t f10;
    signed char[4] pad24;
    struct s0* f18;
    signed char[7] pad32;
    struct s0* f20;
    signed char[3] pad36;
    signed char f24;
    signed char[3] pad40;
    unsigned char f28;
    signed char[3] pad44;
    unsigned char f2c;
    signed char[3] pad48;
    struct s0* f30;
    signed char[7] pad56;
    struct s0* f38;
    signed char[7] pad64;
    unsigned char f40;
    signed char[11] pad76;
    unsigned char f4c;
    signed char[3] pad80;
    int64_t f50;
    signed char[236] pad324;
    int16_t f144;
    int16_t f146;
    signed char[4] pad332;
    unsigned char f14c;
    signed char[7] pad340;
    unsigned char f154;
};

struct s0* g28;

int32_t read_utmp();

int64_t quotearg_n_style_colon();

int32_t* fun_2480();

void fun_2750();

signed char short_list = 0;

struct s1 {
    int16_t f0;
    signed char[6] pad8;
    struct s0* f8;
    signed char[35] pad44;
    signed char f2c;
};

struct s0* extract_trimmed_name(struct s1* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_2720(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, struct s0* a8, struct s0* a9, int64_t a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, struct s0* a16, int64_t a17, struct s0* a18, struct s0* a19, struct s0* a20);

void fun_2450(struct s0* rdi, ...);

struct s0* fun_2540();

signed char include_heading = 0;

void print_line(int32_t edi, struct s0* rsi, signed char dl, uint32_t ecx, struct s0* r8, struct s0* r9, struct s0* a7, struct s0* a8, struct s0* a9, struct s0* a10, struct s0* a11, struct s0* a12);

signed char my_line_only = 0;

void fun_2570();

struct s0* fun_26f0();

int32_t fun_24a0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, struct s0* a8, struct s0* a9, int64_t a10, int64_t a11, int64_t a12, int64_t a13, int64_t a14, int64_t a15, struct s0* a16, int64_t a17, struct s0* a18, struct s0* a19, struct s0* a20);

signed char need_users = 0;

void print_user(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

signed char need_runlevel = 0;

signed char need_boottime = 0;

signed char need_clockchange = 0;

signed char need_initspawn = 0;

signed char need_login = 0;

signed char need_deadprocs = 0;

struct s0* make_id_equals_comment(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_2810(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, ...);

/* exitstr.0 */
struct s0* exitstr_0 = reinterpret_cast<struct s0*>(0);

struct s0* fun_2560(struct s0* rdi, ...);

struct s0* xmalloc(struct s0* rdi, struct s0* rsi, ...);

/* time_string.isra.0 */
struct s0* time_string_isra_0(unsigned char edi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, struct s0* a7, struct s0* a8);

/* runlevline.2 */
struct s0* runlevline_2 = reinterpret_cast<struct s0*>(0);

/* comment.1 */
struct s0* comment_1 = reinterpret_cast<struct s0*>(0);

void who(int64_t rdi, uint32_t esi) {
    struct s0* rcx3;
    struct s0* v4;
    struct s0* r12_5;
    struct s0* v6;
    struct s0* rbp7;
    struct s0* v8;
    struct s0* rbx9;
    void* rsp10;
    struct s0* rax11;
    struct s0* v12;
    struct s0* rdx13;
    struct s0* rsi14;
    int32_t eax15;
    void* rsp16;
    int1_t zf17;
    struct s1* rbp18;
    struct s1* v19;
    struct s0* r12_20;
    struct s0* v21;
    signed char* rbx22;
    struct s0* rdx23;
    struct s0* r8_24;
    struct s0* r12_25;
    struct s0* v26;
    struct s0* rax27;
    struct s0* r9_28;
    int64_t v29;
    struct s0* v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;
    int1_t cf38;
    struct s0* rax39;
    struct s0* r9_40;
    int64_t v41;
    struct s0* v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;
    int64_t v46;
    int64_t v47;
    int64_t v48;
    int64_t v49;
    int1_t zf50;
    struct s0* rax51;
    struct s0* v52;
    struct s0* rax53;
    struct s0* v54;
    struct s0* rax55;
    struct s0* rax56;
    struct s0* rax57;
    struct s0* rax58;
    struct s0* rax59;
    struct s0* r9_60;
    struct s0* r8_61;
    struct s0* v62;
    struct s0* rbx63;
    int1_t zf64;
    struct s0* rax65;
    struct s0* v66;
    void* rax67;
    int64_t v68;
    int64_t v69;
    int64_t v70;
    int64_t v71;
    int64_t v72;
    int64_t v73;
    int64_t v74;
    int64_t v75;
    int32_t eax76;
    struct s0* r14_77;
    signed char* r13_78;
    struct s0* rbp79;
    int1_t zf80;
    struct s0* r12_81;
    int64_t v82;
    int64_t v83;
    int64_t v84;
    int64_t v85;
    int64_t v86;
    int64_t v87;
    int64_t v88;
    int64_t v89;
    int32_t eax90;
    int1_t zf91;
    uint32_t eax92;
    int1_t zf93;
    int1_t zf94;
    int1_t zf95;
    int1_t zf96;
    int1_t zf97;
    int1_t zf98;
    struct s0* r12_99;
    struct s0* rax100;
    void* rsp101;
    int1_t zf102;
    struct s0* rax103;
    struct s0* rax104;
    struct s0* rax105;
    struct s0* rax106;
    struct s0* rdi107;
    struct s0* rax108;
    struct s0* rax109;
    int32_t r9d110;
    struct s0* rax111;
    struct s0* r10_112;
    struct s0* rdi113;
    struct s0* r9_114;
    struct s0* rdx115;
    unsigned char edi116;
    struct s0* rax117;
    struct s0* rax118;
    void* rsp119;
    struct s0* r10_120;
    struct s0* r8_121;
    struct s0* r15_122;
    unsigned char edi123;
    struct s0* v124;
    struct s0* rax125;
    struct s0* rax126;
    struct s0* v127;
    struct s0* v128;
    void* rsp129;
    struct s0* rax130;
    void* rsp131;
    struct s0* r10_132;
    struct s0* r8_133;
    unsigned char edi134;
    struct s0* v135;
    struct s0* rax136;
    unsigned char edi137;
    struct s0* v138;
    struct s0* rax139;
    void* rsp140;
    struct s0* r15_141;
    unsigned char edi142;
    struct s0* v143;
    struct s0* rax144;
    struct s0* v145;
    struct s0* rax146;
    struct s0* v147;
    void* rsp148;
    struct s0* v149;
    int1_t zf150;
    int32_t r12d151;
    struct s0* rax152;
    struct s0* rax153;
    struct s0* rdi154;
    struct s0* rax155;
    struct s0* rdi156;
    void* rsp157;
    int1_t zf158;
    struct s0* rax159;
    struct s0* rax160;
    struct s0* rdi161;
    struct s0* rax162;
    uint32_t r15d163;
    uint32_t r12d164;
    struct s0* rax165;
    struct s0* rdi166;
    struct s0* r9_167;
    struct s0* r15_168;
    unsigned char edi169;
    struct s0* v170;
    struct s0* rax171;

    *reinterpret_cast<uint32_t*>(&rcx3) = esi;
    *reinterpret_cast<int32_t*>(&rcx3 + 4) = 0;
    v4 = r12_5;
    v6 = rbp7;
    v8 = rbx9;
    rsp10 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 88);
    rax11 = g28;
    v12 = rax11;
    rdx13 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp10) + 48);
    rsi14 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp10) + 40);
    eax15 = read_utmp();
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp10) - 8 + 8);
    if (eax15) {
        quotearg_n_style_colon();
        fun_2480();
        fun_2750();
    } else {
        zf17 = short_list == 0;
        rbp18 = v19;
        r12_20 = v21;
        if (!zf17) {
            rbx22 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_20) + 0xffffffffffffffff);
            if (r12_20) {
                rdx23 = reinterpret_cast<struct s0*>(0xade1);
                r8_24 = reinterpret_cast<struct s0*>(0);
                r12_25 = reinterpret_cast<struct s0*>(0xade1);
                do {
                    if (rbp18->f2c && rbp18->f0 == 7) {
                        v26 = r8_24;
                        rax27 = extract_trimmed_name(rbp18, rsi14, rdx23, rcx3);
                        rdx23 = r12_25;
                        rsi14 = reinterpret_cast<struct s0*>("%s%s");
                        rcx3 = rax27;
                        r12_25 = reinterpret_cast<struct s0*>(" ");
                        fun_2720(1, "%s%s", rdx23, rcx3, r8_24, r9_28, v29, v26, v30, v31, v32, v33, v34, v35, v36, v12, v37, v8, v6, v4);
                        fun_2450(rax27, rax27);
                        r8_24 = reinterpret_cast<struct s0*>(&v26->f1);
                    }
                    rbp18 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rbp18) + 0x180);
                    cf38 = reinterpret_cast<uint64_t>(rbx22) < 1;
                    --rbx22;
                } while (!cf38);
                r12_20 = r8_24;
            }
            rax39 = fun_2540();
            fun_2720(1, rax39, r12_20, rcx3, r8_24, r9_40, v41, v26, v42, v43, v44, v45, v46, v47, v48, v12, v49, v8, v6, v4);
            goto addr_387a_11;
        }
        zf50 = include_heading == 0;
        if (!zf50) {
            rax51 = fun_2540();
            v52 = rax51;
            rax53 = fun_2540();
            v54 = rax53;
            rax55 = fun_2540();
            rax56 = fun_2540();
            rax57 = fun_2540();
            rax58 = fun_2540();
            rax59 = fun_2540();
            r9_60 = rax57;
            rsi14 = rax59;
            *reinterpret_cast<int32_t*>(&rdx13) = 32;
            *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
            r8_61 = rax58;
            *reinterpret_cast<uint32_t*>(&rcx3) = 0xffffffff;
            *reinterpret_cast<int32_t*>(&rcx3 + 4) = 0;
            print_line(-1, rsi14, 32, 0xffffffff, r8_61, r9_60, rax56, rax55, v54, v52, v62, v54);
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        }
        *reinterpret_cast<int32_t*>(&rbx63) = 0;
        *reinterpret_cast<int32_t*>(&rbx63 + 4) = 0;
        zf64 = my_line_only == 0;
        if (!zf64) 
            goto addr_3b63_15; else 
            goto addr_35fd_16;
    }
    fun_2570();
    addr_3b63_15:
    rax65 = fun_26f0();
    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
    rbx63 = rax65;
    if (!rax65) {
        addr_387a_11:
        fun_2450(v66, v66);
        rax67 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
        if (!rax67) {
            return;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx13) = 5;
        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
        rsi14 = reinterpret_cast<struct s0*>("/dev/");
        eax76 = fun_24a0(rax65, "/dev/", 5, rcx3, r8_61, r9_60, v68, v54, v52, v69, v70, v71, v72, v73, v74, v12, v75, v8, v6, v4);
        rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
        if (!eax76) {
            rbx63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx63) + 5);
        }
    }
    addr_35fd_16:
    r14_77 = reinterpret_cast<struct s0*>(&rbp18->f8);
    r13_78 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_20) + 0xffffffffffffffff);
    rbp79 = reinterpret_cast<struct s0*>(0x8000000000000000);
    if (r12_20) {
        do {
            zf80 = my_line_only == 0;
            r12_81 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffff8);
            if (zf80 || (*reinterpret_cast<int32_t*>(&rdx13) = 32, *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0, rsi14 = r14_77, eax90 = fun_24a0(rbx63, rsi14, 32, rcx3, r8_61, r9_60, v82, v54, v52, v83, v84, v85, v86, v87, v88, v12, v89, v8, v6, v4), rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8), eax90 == 0)) {
                zf91 = need_users == 0;
                eax92 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffff8);
                if (!zf91 && (r14_77->f24 && *reinterpret_cast<int16_t*>(&eax92) == 7)) {
                    rsi14 = rbp79;
                    print_user(r12_81, rsi14, rdx13, rcx3, r8_61, r9_60);
                    rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    eax92 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffff8);
                    goto addr_3639_25;
                }
                zf93 = need_runlevel == 0;
                if (zf93) 
                    goto addr_369a_27;
                if (*reinterpret_cast<int16_t*>(&eax92) == 1) 
                    goto addr_38e5_29;
            } else {
                addr_3634_30:
                eax92 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffff8);
                goto addr_3639_25;
            }
            addr_369a_27:
            zf94 = need_boottime == 0;
            if (zf94 || *reinterpret_cast<int16_t*>(&eax92) != 2) {
                zf95 = need_clockchange == 0;
                if (zf95 || *reinterpret_cast<int16_t*>(&eax92) != 3) {
                    zf96 = need_initspawn == 0;
                    if (zf96 || *reinterpret_cast<int16_t*>(&eax92) != 5) {
                        zf97 = need_login == 0;
                        if (zf97 || *reinterpret_cast<int16_t*>(&eax92) != 6) {
                            zf98 = need_deadprocs == 0;
                            if (zf98 || *reinterpret_cast<int16_t*>(&eax92) != 8) {
                                addr_3639_25:
                                if (*reinterpret_cast<int16_t*>(&eax92) == 2) {
                                    rbp79 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(r14_77->f14c)));
                                    continue;
                                }
                            } else {
                                r12_99 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp16) + 60);
                                rax100 = make_id_equals_comment(r12_81, rsi14, rdx13, rcx3, r8_61, r9_60);
                                fun_2810(r12_99, 1, 12, "%ld", r12_99, 1, 12, "%ld");
                                rsp101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
                                zf102 = exitstr_0 == 0;
                                if (zf102) {
                                    rax103 = fun_2540();
                                    rax104 = fun_2560(rax103, rax103);
                                    rax105 = fun_2540();
                                    rax106 = fun_2560(rax105, rax105);
                                    rdi107 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax104) + reinterpret_cast<unsigned char>(rax106) + 14);
                                    rax108 = xmalloc(rdi107, "exit=", rdi107, "exit=");
                                    rsp101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp101) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    exitstr_0 = rax108;
                                }
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v83) + 4) = r14_77->f146;
                                rax109 = fun_2540();
                                r9d110 = r14_77->f144;
                                v52 = rax109;
                                rax111 = fun_2540();
                                *reinterpret_cast<int32_t*>(&r10_112) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v83) + 4);
                                *reinterpret_cast<int32_t*>(&r10_112 + 4) = 0;
                                rdi113 = exitstr_0;
                                *reinterpret_cast<int32_t*>(&r9_114) = r9d110;
                                *reinterpret_cast<int32_t*>(&r9_114 + 4) = 0;
                                fun_2810(rdi113, 1, 0xffffffffffffffff, "%s%d %s%d", rdi113, 1, 0xffffffffffffffff, "%s%d %s%d");
                                rdx115 = exitstr_0;
                                edi116 = r14_77->f14c;
                                v54 = rdx115;
                                rax117 = time_string_isra_0(edi116, 1, rdx115, "%s%d %s%d", rax111, r9_114, v52, r10_112);
                                r8_61 = r14_77;
                                r9_60 = rax117;
                                *reinterpret_cast<uint32_t*>(&rcx3) = 32;
                                *reinterpret_cast<int32_t*>(&rcx3 + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rdx13) = 32;
                                *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                                rsi14 = reinterpret_cast<struct s0*>(0xade1);
                                print_line(-1, 0xade1, 32, 32, r8_61, r9_60, 0xade1, r12_99, rax100, v54, v52, r10_112);
                                fun_2450(rax100, rax100);
                                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp101) - 8 + 8 - 8 + 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 48 - 8 + 8);
                                goto addr_3634_30;
                            }
                        } else {
                            rax118 = make_id_equals_comment(r12_81, rsi14, rdx13, rcx3, r8_61, r9_60);
                            rsp119 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                            r10_120 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp119) + 60);
                            r8_121 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffffc)));
                            r15_122 = rax118;
                            v52 = r10_120;
                            fun_2810(r10_120, 1, 12, "%ld", r10_120, 1, 12, "%ld");
                            edi123 = r14_77->f14c;
                            rax125 = time_string_isra_0(edi123, 1, 12, "%ld", r8_121, r9_60, v124, v54);
                            v54 = rax125;
                            rax126 = fun_2540();
                            r8_61 = r14_77;
                            *reinterpret_cast<uint32_t*>(&rcx3) = 32;
                            *reinterpret_cast<int32_t*>(&rcx3 + 4) = 0;
                            v127 = r15_122;
                            rsi14 = rax126;
                            *reinterpret_cast<int32_t*>(&rdx13) = 32;
                            *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                            v128 = v52;
                            rsp129 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp119) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
                            r9_60 = v54;
                        }
                    } else {
                        rax130 = make_id_equals_comment(r12_81, rsi14, rdx13, rcx3, r8_61, r9_60);
                        rsp131 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                        r10_132 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp131) + 60);
                        r8_133 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffffc)));
                        r15_122 = rax130;
                        v54 = r10_132;
                        fun_2810(r10_132, 1, 12, "%ld", r10_132, 1, 12, "%ld");
                        edi134 = r14_77->f14c;
                        rax136 = time_string_isra_0(edi134, 1, 12, "%ld", r8_133, r9_60, v135, v54);
                        r8_61 = r14_77;
                        *reinterpret_cast<uint32_t*>(&rcx3) = 32;
                        *reinterpret_cast<int32_t*>(&rcx3 + 4) = 0;
                        v127 = r15_122;
                        r9_60 = rax136;
                        *reinterpret_cast<int32_t*>(&rdx13) = 32;
                        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                        rsi14 = reinterpret_cast<struct s0*>(0xade1);
                        v128 = v54;
                        rsp129 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp131) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
                    }
                } else {
                    edi137 = r14_77->f14c;
                    rax139 = time_string_isra_0(edi137, rsi14, rdx13, rcx3, r8_61, r9_60, v138, v54);
                    rsp140 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                    r15_141 = rax139;
                    goto addr_39eb_42;
                }
            } else {
                edi142 = r14_77->f14c;
                rax144 = time_string_isra_0(edi142, rsi14, rdx13, rcx3, r8_61, r9_60, v143, v54);
                rsp140 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8);
                r15_141 = rax144;
                goto addr_39eb_42;
            }
            print_line(-1, rsi14, 32, 32, r8_61, r9_60, 0xade1, v128, v127, 0xade1, v145, v54);
            fun_2450(r15_122, r15_122);
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp129) - 8 + 8 + 32 - 8 + 8);
            eax92 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffff8);
            goto addr_3639_25;
            addr_39eb_42:
            rax146 = fun_2540();
            r9_60 = r15_141;
            v147 = reinterpret_cast<struct s0*>(0xade1);
            r8_61 = rax146;
            rsp148 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp140) - 8 + 8 - 8 - 8 - 8 - 8);
            addr_3a07_45:
            *reinterpret_cast<uint32_t*>(&rcx3) = 0xffffffff;
            *reinterpret_cast<int32_t*>(&rcx3 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx13) = 32;
            *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
            rsi14 = reinterpret_cast<struct s0*>(0xade1);
            print_line(-1, 0xade1, 32, 0xffffffff, r8_61, r9_60, 0xade1, 0xade1, v147, 0xade1, v149, v54);
            eax92 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffff8);
            rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp148) - 8 + 8 + 32);
            goto addr_3639_25;
            addr_38e5_29:
            __asm__("cdq ");
            zf150 = runlevline_2 == 0;
            r12d151 = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_77) + 0xfffffffffffffffc) / 0x100;
            if (zf150) {
                rax152 = fun_2540();
                rax153 = fun_2560(rax152, rax152);
                rdi154 = reinterpret_cast<struct s0*>(&rax153->f3);
                rax155 = xmalloc(rdi154, "run-level", rdi154, "run-level");
                rsp16 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8 - 8 + 8);
                runlevline_2 = rax155;
            }
            fun_2540();
            rdi156 = runlevline_2;
            fun_2810(rdi156, 1, 0xffffffffffffffff, "%s %c", rdi156, 1, 0xffffffffffffffff, "%s %c");
            rsp157 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp16) - 8 + 8 - 8 + 8);
            zf158 = comment_1 == 0;
            if (zf158) {
                rax159 = fun_2540();
                rax160 = fun_2560(rax159, rax159);
                rdi161 = reinterpret_cast<struct s0*>(&rax160->f2);
                rax162 = xmalloc(rdi161, "last=", rdi161, "last=");
                rsp157 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp157) - 8 + 8 - 8 + 8 - 8 + 8);
                comment_1 = rax162;
            }
            r15d163 = *reinterpret_cast<unsigned char*>(&r12d151);
            r12d164 = 83;
            if (*reinterpret_cast<unsigned char*>(&r12d151) != 78) {
                r12d164 = r15d163;
            }
            rax165 = fun_2540();
            rdi166 = comment_1;
            *reinterpret_cast<uint32_t*>(&r9_167) = r12d164;
            *reinterpret_cast<int32_t*>(&r9_167 + 4) = 0;
            fun_2810(rdi166, 1, 0xffffffffffffffff, "%s%c", rdi166, 1, 0xffffffffffffffff, "%s%c");
            if (r15d163 - 32 > 94) {
                r15_168 = reinterpret_cast<struct s0*>(0xade1);
            } else {
                r15_168 = comment_1;
            }
            edi169 = r14_77->f14c;
            rax171 = time_string_isra_0(edi169, 1, 0xffffffffffffffff, "%s%c", rax165, r9_167, v170, v54);
            r8_61 = runlevline_2;
            v147 = r15_168;
            r9_60 = rax171;
            rsp148 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp157) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
            goto addr_3a07_45;
            --r13_78;
            r14_77 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_77) + 0x180);
        } while (r13_78 != 0xffffffffffffffff);
        goto addr_387a_11;
    } else {
        goto addr_387a_11;
    }
}

int64_t fun_2460(struct s0** rdi);

struct s0* imaxtostr();

void fun_2730();

int32_t fun_2660(struct s0* rdi, struct s0* rsi);

/* now.6 */
struct s0* now_6 = reinterpret_cast<struct s0*>(0);

void fun_2690(int64_t rdi, struct s0* rsi);

/* hostlen.8 */
struct s0* hostlen_8 = reinterpret_cast<struct s0*>(0);

/* hoststr.7 */
struct s0* hoststr_7 = reinterpret_cast<struct s0*>(0);

struct s2 {
    signed char f0;
    struct s0* f1;
};

struct s2* fun_25a0(struct s0* rdi, struct s0* rsi);

signed char do_lookup = 0;

struct s0* canon_host(struct s0* rdi, struct s0* rsi);

void fun_25e0();

/* time_string.isra.0 */
struct s0* time_string_isra_0(unsigned char edi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, struct s0* a7, struct s0* a8) {
    struct s0** rsp9;
    struct s0* rax10;
    struct s0* v11;
    int64_t rax12;
    struct s0** rsp13;
    struct s0* rdi14;
    struct s0* rsi15;
    struct s0* rax16;
    void* rsp17;
    void* rdx18;
    struct s0* rbp19;
    struct s0* rbx20;
    void* rsp21;
    struct s0* r8_22;
    struct s0* rax23;
    struct s0* v24;
    struct s0* r13_25;
    void* rsp26;
    uint32_t edx27;
    struct s0* rcx28;
    struct s0* rdi29;
    struct s0* r12_30;
    struct s0* rsi31;
    struct s0* rax32;
    struct s0* rsi33;
    int32_t eax34;
    void* rsp35;
    int64_t r14_36;
    struct s0* r15_37;
    struct s0* v38;
    int32_t r14d39;
    int32_t r14d40;
    uint32_t r14d41;
    int32_t r14d42;
    uint32_t v43;
    int1_t zf44;
    struct s0* rax45;
    uint32_t edx46;
    int1_t zf47;
    struct s0* rax48;
    struct s0* rbp49;
    unsigned char* rax50;
    unsigned char* rsi51;
    unsigned char edi52;
    struct s0* v53;
    struct s0* rax54;
    uint32_t edx55;
    struct s0* v56;
    void* rax57;
    struct s2* rax58;
    void* rsp59;
    struct s0* r8_60;
    signed char v61;
    int1_t zf62;
    struct s0* rax63;
    struct s0* rax64;
    void* rsp65;
    struct s0* rdi66;
    struct s0* r8_67;
    int1_t below_or_equal68;
    struct s0* rax69;
    struct s0* r9_70;
    signed char v71;
    int1_t zf72;
    struct s0* r8_73;
    struct s0* rax74;
    struct s0* rax75;
    struct s0* rax76;
    void* rsp77;
    struct s0* rdi78;
    struct s0* r8_79;
    struct s0* r10_80;
    int1_t below_or_equal81;
    struct s0* rax82;
    struct s0* rax83;
    int32_t eax84;
    struct s0* rcx85;
    struct s0* v86;
    struct s0* v87;
    struct s0* v88;
    void* rsp89;
    struct s0* rax90;
    struct s0* v91;
    struct s0* rdx92;
    struct s0* rsi93;
    int32_t eax94;
    void* rsp95;
    int1_t zf96;
    struct s1* rbp97;
    struct s1* v98;
    struct s0* r12_99;
    struct s0* v100;
    signed char* rbx101;
    struct s0* rdx102;
    struct s0* r12_103;
    struct s0* v104;
    struct s0* rax105;
    int64_t v106;
    struct s0* v107;
    int64_t v108;
    int64_t v109;
    int64_t v110;
    int64_t v111;
    int64_t v112;
    int64_t v113;
    int64_t v114;
    int1_t cf115;
    struct s0* rax116;
    int64_t v117;
    struct s0* v118;
    int64_t v119;
    int64_t v120;
    int64_t v121;
    int64_t v122;
    int64_t v123;
    int64_t v124;
    int64_t v125;
    int1_t zf126;
    struct s0* rax127;
    struct s0* v128;
    struct s0* rax129;
    struct s0* v130;
    struct s0* rax131;
    struct s0* rax132;
    struct s0* rax133;
    struct s0* rax134;
    struct s0* rax135;
    struct s0* v136;
    struct s0* rbx137;
    int1_t zf138;
    struct s0* rax139;
    struct s0* v140;
    void* rax141;
    int64_t v142;
    int64_t v143;
    int64_t v144;
    int64_t v145;
    int64_t v146;
    int64_t v147;
    int64_t v148;
    int64_t v149;
    int64_t v150;
    int32_t eax151;
    struct s0* r14_152;
    signed char* r13_153;
    struct s0* rbp154;
    int1_t zf155;
    struct s0* r12_156;
    int64_t v157;
    int64_t v158;
    int64_t v159;
    int64_t v160;
    int64_t v161;
    int64_t v162;
    int64_t v163;
    int64_t v164;
    int32_t eax165;
    int1_t zf166;
    uint32_t eax167;
    int1_t zf168;
    int1_t zf169;
    int1_t zf170;
    int1_t zf171;
    int1_t zf172;
    int1_t zf173;
    struct s0* r12_174;
    struct s0* rax175;
    void* rsp176;
    int1_t zf177;
    struct s0* rax178;
    struct s0* rax179;
    struct s0* rax180;
    struct s0* rax181;
    struct s0* rdi182;
    struct s0* rax183;
    struct s0* rax184;
    int32_t r9d185;
    struct s0* rax186;
    struct s0* r10_187;
    struct s0* rdi188;
    struct s0* r9_189;
    struct s0* rdx190;
    unsigned char edi191;
    struct s0* rax192;
    struct s0* rax193;
    void* rsp194;
    struct s0* r10_195;
    struct s0* r8_196;
    struct s0* r15_197;
    unsigned char edi198;
    struct s0* v199;
    struct s0* rax200;
    struct s0* rax201;
    struct s0* v202;
    struct s0* v203;
    void* rsp204;
    struct s0* rax205;
    void* rsp206;
    struct s0* r10_207;
    struct s0* r8_208;
    unsigned char edi209;
    struct s0* v210;
    struct s0* rax211;
    unsigned char edi212;
    struct s0* v213;
    struct s0* rax214;
    void* rsp215;
    struct s0* r15_216;
    unsigned char edi217;
    struct s0* v218;
    struct s0* rax219;
    struct s0* v220;
    struct s0* rax221;
    struct s0* v222;
    void* rsp223;
    struct s0* v224;
    int1_t zf225;
    int32_t r12d226;
    struct s0* rax227;
    struct s0* rax228;
    struct s0* rdi229;
    struct s0* rax230;
    struct s0* rdi231;
    void* rsp232;
    int1_t zf233;
    struct s0* rax234;
    struct s0* rax235;
    struct s0* rdi236;
    struct s0* rax237;
    uint32_t r15d238;
    uint32_t r12d239;
    struct s0* rax240;
    struct s0* rdi241;
    struct s0* r9_242;
    struct s0* r15_243;
    unsigned char edi244;
    struct s0* v245;
    struct s0* rax246;

    rsp9 = reinterpret_cast<struct s0**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 16);
    rax10 = g28;
    v11 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(edi)));
    rax12 = fun_2460(rsp9);
    rsp13 = rsp9 - 8 + 8;
    if (!rax12) {
        rdi14 = v11;
        rsi15 = reinterpret_cast<struct s0*>(0xf100);
        rax16 = imaxtostr();
        rsp17 = reinterpret_cast<void*>(rsp13 - 8 + 8);
    } else {
        *reinterpret_cast<int32_t*>(&rsi15) = 33;
        *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
        rdi14 = reinterpret_cast<struct s0*>(0xf100);
        fun_2730();
        rsp17 = reinterpret_cast<void*>(rsp13 - 8 + 8);
        rax16 = reinterpret_cast<struct s0*>(0xf100);
    }
    rdx18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax10) - reinterpret_cast<unsigned char>(g28));
    if (!rdx18) {
        return rax16;
    }
    fun_2570();
    rbp19 = rsi15;
    rbx20 = rdi14;
    rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x218);
    r8_22 = reinterpret_cast<struct s0*>(static_cast<int64_t>(rdi14->f4));
    rax23 = g28;
    v24 = rax23;
    r13_25 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp21) + 0xc4);
    fun_2810(r13_25, 1, 12, "%ld", r13_25, 1, 12, "%ld");
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
    edx27 = reinterpret_cast<unsigned char>(rbx20->f8);
    if (*reinterpret_cast<signed char*>(&edx27) == 47) 
        goto addr_30ac_8;
    rcx28 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp26) + 0xd5);
    rdi29 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp26) + 0xd0);
    addr_30b7_10:
    r12_30 = reinterpret_cast<struct s0*>(&rbx20->f8);
    rsi31 = reinterpret_cast<struct s0*>(&rbx20->f28);
    rax32 = r12_30;
    while (*reinterpret_cast<signed char*>(&edx27) && (rax32 = reinterpret_cast<struct s0*>(&rax32->f1), rcx28 = reinterpret_cast<struct s0*>(&rcx28->f1), reinterpret_cast<unsigned char>(rsi31) > reinterpret_cast<unsigned char>(rax32))) {
        edx27 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax32->f0));
    }
    rsi33 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp26) + 32);
    eax34 = fun_2660(rdi29, rsi33);
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
    if (eax34) {
        *reinterpret_cast<uint32_t*>(&r14_36) = 63;
    } else {
        r15_37 = v38;
        *reinterpret_cast<uint32_t*>(&r14_36) = (r14d39 - (r14d40 + reinterpret_cast<uint1_t>(r14d41 < r14d42 + reinterpret_cast<uint1_t>((v43 & 16) < 1))) & 2) + 43;
        if (r15_37) {
            zf44 = reinterpret_cast<int1_t>(now_6 == 0x8000000000000000);
            if (zf44) {
                fun_2690(0xf010, rsi33);
                rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            }
            if (reinterpret_cast<signed char>(r15_37) <= reinterpret_cast<signed char>(rbp19)) 
                goto addr_32c8_19;
            rax45 = now_6;
            if (reinterpret_cast<signed char>(r15_37) < reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rax45) + 0xfffffffffffeae81)) 
                goto addr_32c8_19;
            if (reinterpret_cast<signed char>(r15_37) <= reinterpret_cast<signed char>(rax45)) 
                goto addr_34ce_22; else 
                goto addr_32c8_19;
        } else {
            *reinterpret_cast<uint32_t*>(&r14_36) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&r14_36)));
        }
    }
    edx46 = rbx20->f4c;
    r15_37 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp35) + 0xbd);
    if (!*reinterpret_cast<signed char*>(&edx46)) {
        addr_3317_25:
        zf47 = hostlen_8 == 0;
        rbp19 = hoststr_7;
        if (zf47) {
            hostlen_8 = reinterpret_cast<struct s0*>(1);
            fun_2450(rbp19, rbp19);
            rax48 = xmalloc(1, rsi33, 1, rsi33);
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
            hoststr_7 = rax48;
            rbp19 = rax48;
        }
    } else {
        addr_313a_27:
        rbp49 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp35) + 0x100);
        rax50 = &rbx20->f4c;
        rsi51 = &rbx20->f14c;
        goto addr_3150_28;
    }
    *reinterpret_cast<struct s0**>(&rbp19->f0) = reinterpret_cast<struct s0*>(0);
    addr_3228_30:
    edi52 = rbx20->f154;
    rax54 = time_string_isra_0(edi52, rsi33, 0xade1, rcx28, r8_22, r9, v53, 0xade1);
    r8_22 = r12_30;
    r9 = rax54;
    edx55 = *reinterpret_cast<uint32_t*>(&r14_36);
    print_line(32, &rbx20->f2c, *reinterpret_cast<signed char*>(&edx55), 32, r8_22, r9, r15_37, r13_25, rbp19, 0xade1, v56, 0xade1);
    rax57 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v24) - reinterpret_cast<unsigned char>(g28));
    if (rax57) {
        fun_2570();
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
        goto addr_3571_32;
    } else {
        goto v11;
    }
    do {
        addr_3150_28:
        ++rax50;
        if (reinterpret_cast<uint64_t>(rsi51) <= reinterpret_cast<uint64_t>(rax50)) 
            break;
        edx46 = *rax50;
    } while (*reinterpret_cast<signed char*>(&edx46));
    rax58 = fun_25a0(rbp49, 58);
    rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    if (!rax58) {
        r8_60 = rbp49;
        if (v61 && ((zf62 = do_lookup == 0, !zf62) && (rax63 = canon_host(rbp49, 58), rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8), r8_60 = rax63, !rax63))) {
            r8_60 = rbp49;
        }
        rax64 = fun_2560(r8_60, r8_60);
        rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
        rdi66 = hoststr_7;
        r8_67 = r8_60;
        r9 = reinterpret_cast<struct s0*>(&rax64->f3);
        below_or_equal68 = reinterpret_cast<unsigned char>(r9) <= reinterpret_cast<unsigned char>(hostlen_8);
        if (!below_or_equal68) {
            hostlen_8 = r9;
            fun_2450(rdi66, rdi66);
            rax69 = xmalloc(r9, 58);
            rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8 - 8 + 8);
            r8_67 = r8_67;
            hoststr_7 = rax69;
            rdi66 = rax69;
        }
        rcx28 = reinterpret_cast<struct s0*>("(%s)");
        *reinterpret_cast<int32_t*>(&rsi33) = 1;
        *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
        fun_2810(rdi66, 1, 0xffffffffffffffff, "(%s)");
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8);
        r8_22 = r8_67;
    } else {
        rax58->f0 = 0;
        r9_70 = reinterpret_cast<struct s0*>(&rax58->f1);
        if (!v71 || (zf72 = do_lookup == 0, zf72)) {
            addr_3195_42:
            r8_73 = rbp49;
            goto addr_3198_43;
        } else {
            rax74 = canon_host(rbp49, 58);
            rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
            r9_70 = r9_70;
            r8_73 = rax74;
            if (rax74) {
                addr_3198_43:
                rax75 = fun_2560(r8_73, r8_73);
                rax76 = fun_2560(r9_70, r9_70);
                rsp77 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8);
                r9 = r9_70;
                rdi78 = hoststr_7;
                r8_79 = r8_73;
                r10_80 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax75) + reinterpret_cast<unsigned char>(rax76) + 4);
                below_or_equal81 = reinterpret_cast<unsigned char>(r10_80) <= reinterpret_cast<unsigned char>(hostlen_8);
                if (!below_or_equal81) {
                    hostlen_8 = r10_80;
                    fun_2450(rdi78, rdi78);
                    rax82 = xmalloc(r10_80, 58);
                    rsp77 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8 - 8 + 8);
                    r8_79 = r8_73;
                    r9 = r9;
                    hoststr_7 = rax82;
                    rdi78 = rax82;
                }
            } else {
                goto addr_3195_42;
            }
        }
        rcx28 = reinterpret_cast<struct s0*>("(%s:%s)");
        *reinterpret_cast<int32_t*>(&rsi33) = 1;
        *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
        fun_2810(rdi78, 1, 0xffffffffffffffff, "(%s:%s)");
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8);
        r8_22 = r8_79;
    }
    if (r8_22 != rbp49) {
        fun_2450(r8_22, r8_22);
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    }
    rbp19 = hoststr_7;
    if (!rbp19) {
        rbp19 = reinterpret_cast<struct s0*>(0xade1);
        goto addr_3228_30;
    }
    addr_32c8_19:
    rax83 = fun_2540();
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    r9 = rax83;
    addr_32de_52:
    *reinterpret_cast<int32_t*>(&rsi33) = 1;
    *reinterpret_cast<int32_t*>(&rsi33 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r14_36) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&r14_36)));
    r15_37 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp35) + 0xbd);
    *reinterpret_cast<int32_t*>(&r8_22) = 6;
    *reinterpret_cast<int32_t*>(&r8_22 + 4) = 0;
    rcx28 = reinterpret_cast<struct s0*>("%.*s");
    fun_2810(r15_37, 1, 7, "%.*s", r15_37, 1, 7, "%.*s");
    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    edx46 = rbx20->f4c;
    if (*reinterpret_cast<signed char*>(&edx46)) 
        goto addr_313a_27; else 
        goto addr_3317_25;
    addr_34ce_22:
    eax84 = *reinterpret_cast<int32_t*>(&rax45) - *reinterpret_cast<int32_t*>(&r15_37);
    if (eax84 <= 59) {
        r9 = reinterpret_cast<struct s0*>("  .  ");
        goto addr_32de_52;
    } else {
        if (eax84 > 0x1517f) {
            addr_3571_32:
            fun_25e0();
        } else {
            __asm__("cdq ");
            fun_2810(0xf121, 1, 6, "%02d:%02d");
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
            r9 = reinterpret_cast<struct s0*>(0xf121);
            goto addr_32de_52;
        }
    }
    *reinterpret_cast<uint32_t*>(&rcx85) = reinterpret_cast<uint32_t>("src/who.c");
    *reinterpret_cast<int32_t*>(&rcx85 + 4) = 0;
    v86 = r12_30;
    v87 = rbp19;
    v88 = rbx20;
    rsp89 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 88);
    rax90 = g28;
    v91 = rax90;
    rdx92 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp89) + 48);
    rsi93 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp89) + 40);
    eax94 = read_utmp();
    rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp89) - 8 + 8);
    if (eax94) {
        quotearg_n_style_colon();
        fun_2480();
        fun_2750();
    } else {
        zf96 = short_list == 0;
        rbp97 = v98;
        r12_99 = v100;
        if (!zf96) {
            rbx101 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_99) + 0xffffffffffffffff);
            if (r12_99) {
                rdx102 = reinterpret_cast<struct s0*>(0xade1);
                r8_22 = reinterpret_cast<struct s0*>(0);
                r12_103 = reinterpret_cast<struct s0*>(0xade1);
                do {
                    if (rbp97->f2c && rbp97->f0 == 7) {
                        v104 = r8_22;
                        rax105 = extract_trimmed_name(rbp97, rsi93, rdx102, rcx85);
                        rdx102 = r12_103;
                        rsi93 = reinterpret_cast<struct s0*>("%s%s");
                        rcx85 = rax105;
                        r12_103 = reinterpret_cast<struct s0*>(" ");
                        fun_2720(1, "%s%s", rdx102, rcx85, r8_22, r9, v106, v104, v107, v108, v109, v110, v111, v112, v113, v91, v114, v88, v87, v86);
                        fun_2450(rax105, rax105);
                        r8_22 = reinterpret_cast<struct s0*>(&v104->f1);
                    }
                    rbp97 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rbp97) + 0x180);
                    cf115 = reinterpret_cast<uint64_t>(rbx101) < 1;
                    --rbx101;
                } while (!cf115);
                r12_99 = r8_22;
            }
            rax116 = fun_2540();
            fun_2720(1, rax116, r12_99, rcx85, r8_22, r9, v117, v104, v118, v119, v120, v121, v122, v123, v124, v91, v125, v88, v87, v86);
            goto addr_387a_66;
        }
        zf126 = include_heading == 0;
        if (!zf126) {
            rax127 = fun_2540();
            v128 = rax127;
            rax129 = fun_2540();
            v130 = rax129;
            rax131 = fun_2540();
            rax132 = fun_2540();
            rax133 = fun_2540();
            rax134 = fun_2540();
            rax135 = fun_2540();
            r9 = rax133;
            rsi93 = rax135;
            *reinterpret_cast<int32_t*>(&rdx92) = 32;
            *reinterpret_cast<int32_t*>(&rdx92 + 4) = 0;
            r8_22 = rax134;
            *reinterpret_cast<uint32_t*>(&rcx85) = 0xffffffff;
            *reinterpret_cast<int32_t*>(&rcx85 + 4) = 0;
            print_line(-1, rsi93, 32, 0xffffffff, r8_22, r9, rax132, rax131, v130, v128, v136, v130);
            rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        }
        *reinterpret_cast<int32_t*>(&rbx137) = 0;
        *reinterpret_cast<int32_t*>(&rbx137 + 4) = 0;
        zf138 = my_line_only == 0;
        if (!zf138) 
            goto addr_3b63_70; else 
            goto addr_35fd_71;
    }
    fun_2570();
    addr_3b63_70:
    rax139 = fun_26f0();
    rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
    rbx137 = rax139;
    if (!rax139) {
        addr_387a_66:
        fun_2450(v140, v140);
        rax141 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v91) - reinterpret_cast<unsigned char>(g28));
        if (!rax141) {
            goto v142;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx92) = 5;
        *reinterpret_cast<int32_t*>(&rdx92 + 4) = 0;
        rsi93 = reinterpret_cast<struct s0*>("/dev/");
        eax151 = fun_24a0(rax139, "/dev/", 5, rcx85, r8_22, r9, v143, v130, v128, v144, v145, v146, v147, v148, v149, v91, v150, v88, v87, v86);
        rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
        if (!eax151) {
            rbx137 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx137) + 5);
        }
    }
    addr_35fd_71:
    r14_152 = reinterpret_cast<struct s0*>(&rbp97->f8);
    r13_153 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_99) + 0xffffffffffffffff);
    rbp154 = reinterpret_cast<struct s0*>(0x8000000000000000);
    if (r12_99) {
        do {
            zf155 = my_line_only == 0;
            r12_156 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffff8);
            if (zf155 || (*reinterpret_cast<int32_t*>(&rdx92) = 32, *reinterpret_cast<int32_t*>(&rdx92 + 4) = 0, rsi93 = r14_152, eax165 = fun_24a0(rbx137, rsi93, 32, rcx85, r8_22, r9, v157, v130, v128, v158, v159, v160, v161, v162, v163, v91, v164, v88, v87, v86), rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8), eax165 == 0)) {
                zf166 = need_users == 0;
                eax167 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffff8);
                if (!zf166 && (r14_152->f24 && *reinterpret_cast<int16_t*>(&eax167) == 7)) {
                    rsi93 = rbp154;
                    print_user(r12_156, rsi93, rdx92, rcx85, r8_22, r9);
                    rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
                    eax167 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffff8);
                    goto addr_3639_80;
                }
                zf168 = need_runlevel == 0;
                if (zf168) 
                    goto addr_369a_82;
                if (*reinterpret_cast<int16_t*>(&eax167) == 1) 
                    goto addr_38e5_84;
            } else {
                addr_3634_85:
                eax167 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffff8);
                goto addr_3639_80;
            }
            addr_369a_82:
            zf169 = need_boottime == 0;
            if (zf169 || *reinterpret_cast<int16_t*>(&eax167) != 2) {
                zf170 = need_clockchange == 0;
                if (zf170 || *reinterpret_cast<int16_t*>(&eax167) != 3) {
                    zf171 = need_initspawn == 0;
                    if (zf171 || *reinterpret_cast<int16_t*>(&eax167) != 5) {
                        zf172 = need_login == 0;
                        if (zf172 || *reinterpret_cast<int16_t*>(&eax167) != 6) {
                            zf173 = need_deadprocs == 0;
                            if (zf173 || *reinterpret_cast<int16_t*>(&eax167) != 8) {
                                addr_3639_80:
                                if (*reinterpret_cast<int16_t*>(&eax167) == 2) {
                                    rbp154 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(r14_152->f14c)));
                                    continue;
                                }
                            } else {
                                r12_174 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp95) + 60);
                                rax175 = make_id_equals_comment(r12_156, rsi93, rdx92, rcx85, r8_22, r9);
                                fun_2810(r12_174, 1, 12, "%ld", r12_174, 1, 12, "%ld");
                                rsp176 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 + 8);
                                zf177 = exitstr_0 == 0;
                                if (zf177) {
                                    rax178 = fun_2540();
                                    rax179 = fun_2560(rax178, rax178);
                                    rax180 = fun_2540();
                                    rax181 = fun_2560(rax180, rax180);
                                    rdi182 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax179) + reinterpret_cast<unsigned char>(rax181) + 14);
                                    rax183 = xmalloc(rdi182, "exit=", rdi182, "exit=");
                                    rsp176 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp176) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    exitstr_0 = rax183;
                                }
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v158) + 4) = r14_152->f146;
                                rax184 = fun_2540();
                                r9d185 = r14_152->f144;
                                v128 = rax184;
                                rax186 = fun_2540();
                                *reinterpret_cast<int32_t*>(&r10_187) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v158) + 4);
                                *reinterpret_cast<int32_t*>(&r10_187 + 4) = 0;
                                rdi188 = exitstr_0;
                                *reinterpret_cast<int32_t*>(&r9_189) = r9d185;
                                *reinterpret_cast<int32_t*>(&r9_189 + 4) = 0;
                                fun_2810(rdi188, 1, 0xffffffffffffffff, "%s%d %s%d", rdi188, 1, 0xffffffffffffffff, "%s%d %s%d");
                                rdx190 = exitstr_0;
                                edi191 = r14_152->f14c;
                                v130 = rdx190;
                                rax192 = time_string_isra_0(edi191, 1, rdx190, "%s%d %s%d", rax186, r9_189, v128, r10_187);
                                r8_22 = r14_152;
                                r9 = rax192;
                                *reinterpret_cast<uint32_t*>(&rcx85) = 32;
                                *reinterpret_cast<int32_t*>(&rcx85 + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rdx92) = 32;
                                *reinterpret_cast<int32_t*>(&rdx92 + 4) = 0;
                                rsi93 = reinterpret_cast<struct s0*>(0xade1);
                                print_line(-1, 0xade1, 32, 32, r8_22, r9, 0xade1, r12_174, rax175, v130, v128, r10_187);
                                fun_2450(rax175, rax175);
                                rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp176) - 8 + 8 - 8 + 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 48 - 8 + 8);
                                goto addr_3634_85;
                            }
                        } else {
                            rax193 = make_id_equals_comment(r12_156, rsi93, rdx92, rcx85, r8_22, r9);
                            rsp194 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
                            r10_195 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp194) + 60);
                            r8_196 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffffc)));
                            r15_197 = rax193;
                            v128 = r10_195;
                            fun_2810(r10_195, 1, 12, "%ld", r10_195, 1, 12, "%ld");
                            edi198 = r14_152->f14c;
                            rax200 = time_string_isra_0(edi198, 1, 12, "%ld", r8_196, r9, v199, v130);
                            v130 = rax200;
                            rax201 = fun_2540();
                            r8_22 = r14_152;
                            *reinterpret_cast<uint32_t*>(&rcx85) = 32;
                            *reinterpret_cast<int32_t*>(&rcx85 + 4) = 0;
                            v202 = r15_197;
                            rsi93 = rax201;
                            *reinterpret_cast<int32_t*>(&rdx92) = 32;
                            *reinterpret_cast<int32_t*>(&rdx92 + 4) = 0;
                            v203 = v128;
                            rsp204 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp194) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
                            r9 = v130;
                        }
                    } else {
                        rax205 = make_id_equals_comment(r12_156, rsi93, rdx92, rcx85, r8_22, r9);
                        rsp206 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
                        r10_207 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp206) + 60);
                        r8_208 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffffc)));
                        r15_197 = rax205;
                        v130 = r10_207;
                        fun_2810(r10_207, 1, 12, "%ld", r10_207, 1, 12, "%ld");
                        edi209 = r14_152->f14c;
                        rax211 = time_string_isra_0(edi209, 1, 12, "%ld", r8_208, r9, v210, v130);
                        r8_22 = r14_152;
                        *reinterpret_cast<uint32_t*>(&rcx85) = 32;
                        *reinterpret_cast<int32_t*>(&rcx85 + 4) = 0;
                        v202 = r15_197;
                        r9 = rax211;
                        *reinterpret_cast<int32_t*>(&rdx92) = 32;
                        *reinterpret_cast<int32_t*>(&rdx92 + 4) = 0;
                        rsi93 = reinterpret_cast<struct s0*>(0xade1);
                        v203 = v130;
                        rsp204 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp206) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
                    }
                } else {
                    edi212 = r14_152->f14c;
                    rax214 = time_string_isra_0(edi212, rsi93, rdx92, rcx85, r8_22, r9, v213, v130);
                    rsp215 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
                    r15_216 = rax214;
                    goto addr_39eb_97;
                }
            } else {
                edi217 = r14_152->f14c;
                rax219 = time_string_isra_0(edi217, rsi93, rdx92, rcx85, r8_22, r9, v218, v130);
                rsp215 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
                r15_216 = rax219;
                goto addr_39eb_97;
            }
            print_line(-1, rsi93, 32, 32, r8_22, r9, 0xade1, v203, v202, 0xade1, v220, v130);
            fun_2450(r15_197, r15_197);
            rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp204) - 8 + 8 + 32 - 8 + 8);
            eax167 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffff8);
            goto addr_3639_80;
            addr_39eb_97:
            rax221 = fun_2540();
            r9 = r15_216;
            v222 = reinterpret_cast<struct s0*>(0xade1);
            r8_22 = rax221;
            rsp223 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp215) - 8 + 8 - 8 - 8 - 8 - 8);
            addr_3a07_100:
            *reinterpret_cast<uint32_t*>(&rcx85) = 0xffffffff;
            *reinterpret_cast<int32_t*>(&rcx85 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx92) = 32;
            *reinterpret_cast<int32_t*>(&rdx92 + 4) = 0;
            rsi93 = reinterpret_cast<struct s0*>(0xade1);
            print_line(-1, 0xade1, 32, 0xffffffff, r8_22, r9, 0xade1, 0xade1, v222, 0xade1, v224, v130);
            eax167 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffff8);
            rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp223) - 8 + 8 + 32);
            goto addr_3639_80;
            addr_38e5_84:
            __asm__("cdq ");
            zf225 = runlevline_2 == 0;
            r12d226 = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_152) + 0xfffffffffffffffc) / 0x100;
            if (zf225) {
                rax227 = fun_2540();
                rax228 = fun_2560(rax227, rax227);
                rdi229 = reinterpret_cast<struct s0*>(&rax228->f3);
                rax230 = xmalloc(rdi229, "run-level", rdi229, "run-level");
                rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 + 8 - 8 + 8);
                runlevline_2 = rax230;
            }
            fun_2540();
            rdi231 = runlevline_2;
            fun_2810(rdi231, 1, 0xffffffffffffffff, "%s %c", rdi231, 1, 0xffffffffffffffff, "%s %c");
            rsp232 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 + 8);
            zf233 = comment_1 == 0;
            if (zf233) {
                rax234 = fun_2540();
                rax235 = fun_2560(rax234, rax234);
                rdi236 = reinterpret_cast<struct s0*>(&rax235->f2);
                rax237 = xmalloc(rdi236, "last=", rdi236, "last=");
                rsp232 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp232) - 8 + 8 - 8 + 8 - 8 + 8);
                comment_1 = rax237;
            }
            r15d238 = *reinterpret_cast<unsigned char*>(&r12d226);
            r12d239 = 83;
            if (*reinterpret_cast<unsigned char*>(&r12d226) != 78) {
                r12d239 = r15d238;
            }
            rax240 = fun_2540();
            rdi241 = comment_1;
            *reinterpret_cast<uint32_t*>(&r9_242) = r12d239;
            *reinterpret_cast<int32_t*>(&r9_242 + 4) = 0;
            fun_2810(rdi241, 1, 0xffffffffffffffff, "%s%c", rdi241, 1, 0xffffffffffffffff, "%s%c");
            if (r15d238 - 32 > 94) {
                r15_243 = reinterpret_cast<struct s0*>(0xade1);
            } else {
                r15_243 = comment_1;
            }
            edi244 = r14_152->f14c;
            rax246 = time_string_isra_0(edi244, 1, 0xffffffffffffffff, "%s%c", rax240, r9_242, v245, v130);
            r8_22 = runlevline_2;
            v222 = r15_243;
            r9 = rax246;
            rsp223 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp232) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
            goto addr_3a07_100;
            --r13_153;
            r14_152 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_152) + 0x180);
        } while (r13_153 != 0xffffffffffffffff);
        goto addr_387a_66;
    } else {
        goto addr_387a_66;
    }
    addr_30ac_8:
    rdi29 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp26) + 0xd0);
    rcx28 = rdi29;
    goto addr_30b7_10;
}

unsigned char short_output = 0;

signed char include_idle = 0;

signed char gf019 = 0x78;

signed char include_exit = 0;

signed char include_mesg = 0;

int32_t rpl_asprintf();

void xalloc_die();

void fun_24d0(int64_t rdi, int64_t rsi);

signed char* fun_2530(struct s0* rdi, struct s0* rsi, int64_t rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void print_line(int32_t edi, struct s0* rsi, signed char dl, uint32_t ecx, struct s0* r8, struct s0* r9, struct s0* a7, struct s0* a8, struct s0* a9, struct s0* a10, struct s0* a11, struct s0* a12) {
    struct s0* r12_13;
    void* rsp14;
    uint32_t ebp15;
    struct s0* r9_16;
    struct s0* rax17;
    struct s0* v18;
    int1_t zf19;
    struct s0* rax20;
    struct s0* rdi21;
    struct s0* rax22;
    struct s0* rdi23;
    int1_t zf24;
    struct s0* rdi25;
    struct s0* rax26;
    struct s0* rax27;
    void* rsp28;
    int1_t zf29;
    int1_t zf30;
    struct s0* rdi31;
    struct s0* r9_32;
    struct s0* r8_33;
    struct s0* rcx34;
    int32_t eax35;
    struct s0* v36;
    struct s0* rax37;
    signed char* rax38;
    signed char* rdx39;
    int64_t v40;
    struct s0* v41;
    void* rax42;
    struct s0* rax43;
    struct s0* rax44;
    struct s0* rdi45;
    struct s0* rax46;
    struct s0* rax47;
    signed char* rax48;
    unsigned char* rdx49;
    unsigned char* rsi50;
    uint32_t ecx51;
    unsigned char v52;
    int64_t v53;

    r12_13 = rsi;
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x68);
    ebp15 = short_output;
    r9_16 = a8;
    rax17 = g28;
    v18 = rax17;
    zf19 = include_idle == 0;
    gf019 = dl;
    if (!(zf19 || (*reinterpret_cast<signed char*>(&ebp15) || (rax20 = fun_2560(a7), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8), r9_16 = r9_16, reinterpret_cast<unsigned char>(rax20) > reinterpret_cast<unsigned char>(6))))) {
        rdi21 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 67);
        *reinterpret_cast<int32_t*>(&rsi) = 1;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        fun_2810(rdi21, 1, 8, " %-6s", rdi21, 1, 8, " %-6s");
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
        ebp15 = short_output;
        r9_16 = r9_16;
    }
    if (!(*reinterpret_cast<signed char*>(&ebp15) || (rax22 = fun_2560(r9_16, r9_16), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8), reinterpret_cast<unsigned char>(rax22) > reinterpret_cast<unsigned char>(11)))) {
        *reinterpret_cast<int32_t*>(&rsi) = 1;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rdi23 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 75);
        fun_2810(rdi23, 1, 13, " %10s", rdi23, 1, 13, " %10s");
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    }
    zf24 = include_exit == 0;
    *reinterpret_cast<int32_t*>(&rdi25) = 1;
    *reinterpret_cast<int32_t*>(&rdi25 + 4) = 0;
    if (!zf24 && (rax26 = fun_2560(a10, a10), rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8), rdi25 = reinterpret_cast<struct s0*>(&rax26->f2), reinterpret_cast<unsigned char>(rax26) <= reinterpret_cast<unsigned char>(11))) {
        rdi25 = reinterpret_cast<struct s0*>(14);
    }
    rax27 = xmalloc(rdi25, rsi, rdi25, rsi);
    rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    zf29 = include_exit == 0;
    if (!zf29) {
        fun_2810(rax27, 1, 0xffffffffffffffff, " %-12s", rax27, 1, 0xffffffffffffffff, " %-12s");
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
    } else {
        *reinterpret_cast<struct s0**>(&rax27->f0) = reinterpret_cast<struct s0*>(0);
    }
    zf30 = include_mesg == 0;
    rdi31 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp28) + 56);
    *reinterpret_cast<uint32_t*>(&r9_32) = ecx;
    *reinterpret_cast<int32_t*>(&r9_32 + 4) = 0;
    r8_33 = reinterpret_cast<struct s0*>(" x");
    if (zf30) {
        r8_33 = reinterpret_cast<struct s0*>(0xade1);
    }
    if (!r12_13) {
        r12_13 = reinterpret_cast<struct s0*>("   .");
    }
    rcx34 = r12_13;
    eax35 = rpl_asprintf();
    if (eax35 == -1) {
        xalloc_die();
    } else {
        rax37 = fun_2560(v36, v36);
        rax38 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax37) + reinterpret_cast<unsigned char>(v36));
        do {
            rdx39 = rax38;
            --rax38;
        } while (*rax38 == 32);
        *rdx39 = 0;
        fun_24d0(v40, "%-8.*s%s %-12.*s %-*s%s%s %-8s%s");
        rdi31 = v41;
        fun_2450(rdi31, rdi31);
        rax42 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v18) - reinterpret_cast<unsigned char>(g28));
        if (!rax42) 
            goto addr_2e77_21;
    }
    fun_2570();
    rax43 = fun_2540();
    rax44 = fun_2560(rax43, rax43);
    rdi45 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax44) + 5);
    rax46 = xmalloc(rdi45, "id=", rdi45, "id=");
    rax47 = fun_2540();
    rax48 = fun_2530(rax46, rax47, 5, rcx34, r8_33, r9_32);
    rdx49 = &rdi31->f28;
    rsi50 = &rdi31->f2c;
    do {
        ecx51 = v52;
        if (!*reinterpret_cast<signed char*>(&ecx51)) 
            break;
        ++rdx49;
        ++rax48;
        *(rax48 - 1) = *reinterpret_cast<signed char*>(&ecx51);
    } while (reinterpret_cast<uint64_t>(rsi50) > reinterpret_cast<uint64_t>(rdx49));
    *rax48 = 0;
    goto v53;
    addr_2e77_21:
}

struct s0* make_id_equals_comment(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9) {
    struct s0* rax7;
    struct s0* rax8;
    struct s0* rdi9;
    struct s0* rax10;
    struct s0* r12_11;
    struct s0* rax12;
    signed char* rax13;
    unsigned char* rdx14;
    unsigned char* rsi15;
    uint32_t ecx16;

    rax7 = fun_2540();
    rax8 = fun_2560(rax7, rax7);
    rdi9 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax8) + 5);
    rax10 = xmalloc(rdi9, "id=", rdi9, "id=");
    r12_11 = rax10;
    rax12 = fun_2540();
    rax13 = fun_2530(r12_11, rax12, 5, rcx, r8, r9);
    rdx14 = &rdi->f28;
    rsi15 = &rdi->f2c;
    do {
        ecx16 = *rdx14;
        if (!*reinterpret_cast<signed char*>(&ecx16)) 
            break;
        ++rdx14;
        ++rax13;
        *(rax13 - 1) = *reinterpret_cast<signed char*>(&ecx16);
    } while (reinterpret_cast<uint64_t>(rsi15) > reinterpret_cast<uint64_t>(rdx14));
    *rax13 = 0;
    return r12_11;
}

int64_t fun_2550();

int64_t fun_2470(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2550();
    if (r8d > 10) {
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xaa20 + rax11 * 4) + 0xaa20;
    }
}

struct s3 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* slotvec = reinterpret_cast<struct s0*>(0x90);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_25f0();

struct s4 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
};

struct s0* xcharalloc(struct s0* rdi, ...);

struct s0* quotearg_n_options(struct s0* rdi, int64_t rsi, int64_t rdx, struct s3* rcx, ...) {
    int64_t rbx5;
    struct s0* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s4* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x5aff;
    rax8 = fun_2480();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
        fun_2470(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xf090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x93f1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x5b8b;
            fun_25f0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s4*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(&rax23->f1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xf1c0) {
                fun_2450(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x5c1a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2570();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xf0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

void print_user(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9) {
    struct s0* rbp7;
    struct s0* rbx8;
    void* rsp9;
    struct s0* r8_10;
    struct s0* rax11;
    struct s0* v12;
    struct s0* r13_13;
    void* rsp14;
    uint32_t edx15;
    struct s0* rcx16;
    struct s0* rdi17;
    struct s0* r12_18;
    struct s0* rsi19;
    struct s0* rax20;
    struct s0* rsi21;
    int32_t eax22;
    void* rsp23;
    int64_t r14_24;
    struct s0* r15_25;
    struct s0* v26;
    int32_t r14d27;
    int32_t r14d28;
    uint32_t r14d29;
    int32_t r14d30;
    uint32_t v31;
    int1_t zf32;
    struct s0* rax33;
    uint32_t edx34;
    int1_t zf35;
    struct s0* rax36;
    struct s0* rbp37;
    unsigned char* rax38;
    unsigned char* rsi39;
    unsigned char edi40;
    struct s0* v41;
    struct s0* rax42;
    uint32_t edx43;
    struct s0* v44;
    void* rax45;
    struct s2* rax46;
    void* rsp47;
    struct s0* r8_48;
    signed char v49;
    int1_t zf50;
    struct s0* rax51;
    struct s0* rax52;
    void* rsp53;
    struct s0* rdi54;
    struct s0* r8_55;
    int1_t below_or_equal56;
    struct s0* rax57;
    struct s0* r9_58;
    signed char v59;
    int1_t zf60;
    struct s0* r8_61;
    struct s0* rax62;
    struct s0* rax63;
    struct s0* rax64;
    void* rsp65;
    struct s0* rdi66;
    struct s0* r8_67;
    struct s0* r10_68;
    int1_t below_or_equal69;
    struct s0* rax70;
    struct s0* rax71;
    int32_t eax72;
    struct s0* rcx73;
    struct s0* v74;
    struct s0* v75;
    struct s0* v76;
    void* rsp77;
    struct s0* rax78;
    struct s0* v79;
    struct s0* rdx80;
    struct s0* rsi81;
    int32_t eax82;
    void* rsp83;
    int1_t zf84;
    struct s1* rbp85;
    struct s1* v86;
    struct s0* r12_87;
    struct s0* v88;
    signed char* rbx89;
    struct s0* rdx90;
    struct s0* r12_91;
    struct s0* v92;
    struct s0* rax93;
    int64_t v94;
    struct s0* v95;
    int64_t v96;
    int64_t v97;
    int64_t v98;
    int64_t v99;
    int64_t v100;
    int64_t v101;
    int64_t v102;
    int1_t cf103;
    struct s0* rax104;
    int64_t v105;
    struct s0* v106;
    int64_t v107;
    int64_t v108;
    int64_t v109;
    int64_t v110;
    int64_t v111;
    int64_t v112;
    int64_t v113;
    int1_t zf114;
    struct s0* rax115;
    struct s0* v116;
    struct s0* rax117;
    struct s0* v118;
    struct s0* rax119;
    struct s0* rax120;
    struct s0* rax121;
    struct s0* rax122;
    struct s0* rax123;
    struct s0* v124;
    struct s0* rbx125;
    int1_t zf126;
    struct s0* rax127;
    struct s0* v128;
    void* rax129;
    int64_t v130;
    int64_t v131;
    int64_t v132;
    int64_t v133;
    int64_t v134;
    int64_t v135;
    int64_t v136;
    int64_t v137;
    int64_t v138;
    int32_t eax139;
    struct s0* r14_140;
    signed char* r13_141;
    struct s0* rbp142;
    int1_t zf143;
    struct s0* r12_144;
    int64_t v145;
    int64_t v146;
    int64_t v147;
    int64_t v148;
    int64_t v149;
    int64_t v150;
    int64_t v151;
    int64_t v152;
    int32_t eax153;
    int1_t zf154;
    uint32_t eax155;
    int1_t zf156;
    int1_t zf157;
    int1_t zf158;
    int1_t zf159;
    int1_t zf160;
    int1_t zf161;
    struct s0* r12_162;
    struct s0* rax163;
    void* rsp164;
    int1_t zf165;
    struct s0* rax166;
    struct s0* rax167;
    struct s0* rax168;
    struct s0* rax169;
    struct s0* rdi170;
    struct s0* rax171;
    struct s0* rax172;
    int32_t r9d173;
    struct s0* rax174;
    struct s0* r10_175;
    struct s0* rdi176;
    struct s0* r9_177;
    struct s0* rdx178;
    unsigned char edi179;
    struct s0* rax180;
    struct s0* rax181;
    void* rsp182;
    struct s0* r10_183;
    struct s0* r8_184;
    struct s0* r15_185;
    unsigned char edi186;
    struct s0* v187;
    struct s0* rax188;
    struct s0* rax189;
    struct s0* v190;
    struct s0* v191;
    void* rsp192;
    struct s0* rax193;
    void* rsp194;
    struct s0* r10_195;
    struct s0* r8_196;
    unsigned char edi197;
    struct s0* v198;
    struct s0* rax199;
    unsigned char edi200;
    struct s0* v201;
    struct s0* rax202;
    void* rsp203;
    struct s0* r15_204;
    unsigned char edi205;
    struct s0* v206;
    struct s0* rax207;
    struct s0* v208;
    struct s0* rax209;
    struct s0* v210;
    void* rsp211;
    struct s0* v212;
    int1_t zf213;
    int32_t r12d214;
    struct s0* rax215;
    struct s0* rax216;
    struct s0* rdi217;
    struct s0* rax218;
    struct s0* rdi219;
    void* rsp220;
    int1_t zf221;
    struct s0* rax222;
    struct s0* rax223;
    struct s0* rdi224;
    struct s0* rax225;
    uint32_t r15d226;
    uint32_t r12d227;
    struct s0* rax228;
    struct s0* rdi229;
    struct s0* r9_230;
    struct s0* r15_231;
    unsigned char edi232;
    struct s0* v233;
    struct s0* rax234;

    rbp7 = rsi;
    rbx8 = rdi;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x218);
    r8_10 = reinterpret_cast<struct s0*>(static_cast<int64_t>(rdi->f4));
    rax11 = g28;
    v12 = rax11;
    r13_13 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp9) + 0xc4);
    fun_2810(r13_13, 1, 12, "%ld", r13_13, 1, 12, "%ld");
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    edx15 = reinterpret_cast<unsigned char>(rbx8->f8);
    if (*reinterpret_cast<signed char*>(&edx15) != 47) {
        rcx16 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 0xd5);
        rdi17 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 0xd0);
    } else {
        rdi17 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 0xd0);
        rcx16 = rdi17;
    }
    r12_18 = reinterpret_cast<struct s0*>(&rbx8->f8);
    rsi19 = reinterpret_cast<struct s0*>(&rbx8->f28);
    rax20 = r12_18;
    while (*reinterpret_cast<signed char*>(&edx15) && (rax20 = reinterpret_cast<struct s0*>(&rax20->f1), rcx16 = reinterpret_cast<struct s0*>(&rcx16->f1), reinterpret_cast<unsigned char>(rsi19) > reinterpret_cast<unsigned char>(rax20))) {
        edx15 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax20->f0));
    }
    rsi21 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp14) + 32);
    eax22 = fun_2660(rdi17, rsi21);
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    if (eax22) {
        *reinterpret_cast<uint32_t*>(&r14_24) = 63;
    } else {
        r15_25 = v26;
        *reinterpret_cast<uint32_t*>(&r14_24) = (r14d27 - (r14d28 + reinterpret_cast<uint1_t>(r14d29 < r14d30 + reinterpret_cast<uint1_t>((v31 & 16) < 1))) & 2) + 43;
        if (r15_25) {
            zf32 = reinterpret_cast<int1_t>(now_6 == 0x8000000000000000);
            if (zf32) {
                fun_2690(0xf010, rsi21);
                rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            }
            if (reinterpret_cast<signed char>(r15_25) <= reinterpret_cast<signed char>(rbp7)) 
                goto addr_32c8_13;
            rax33 = now_6;
            if (reinterpret_cast<signed char>(r15_25) < reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rax33) + 0xfffffffffffeae81)) 
                goto addr_32c8_13;
            if (reinterpret_cast<signed char>(r15_25) <= reinterpret_cast<signed char>(rax33)) 
                goto addr_34ce_16; else 
                goto addr_32c8_13;
        } else {
            *reinterpret_cast<uint32_t*>(&r14_24) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&r14_24)));
        }
    }
    edx34 = rbx8->f4c;
    r15_25 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp23) + 0xbd);
    if (!*reinterpret_cast<signed char*>(&edx34)) {
        addr_3317_19:
        zf35 = hostlen_8 == 0;
        rbp7 = hoststr_7;
        if (zf35) {
            hostlen_8 = reinterpret_cast<struct s0*>(1);
            fun_2450(rbp7, rbp7);
            rax36 = xmalloc(1, rsi21, 1, rsi21);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8);
            hoststr_7 = rax36;
            rbp7 = rax36;
        }
    } else {
        addr_313a_21:
        rbp37 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp23) + 0x100);
        rax38 = &rbx8->f4c;
        rsi39 = &rbx8->f14c;
        goto addr_3150_22;
    }
    *reinterpret_cast<struct s0**>(&rbp7->f0) = reinterpret_cast<struct s0*>(0);
    addr_3228_24:
    edi40 = rbx8->f154;
    rax42 = time_string_isra_0(edi40, rsi21, 0xade1, rcx16, r8_10, r9, v41, 0xade1);
    r8_10 = r12_18;
    r9 = rax42;
    edx43 = *reinterpret_cast<uint32_t*>(&r14_24);
    print_line(32, &rbx8->f2c, *reinterpret_cast<signed char*>(&edx43), 32, r8_10, r9, r15_25, r13_13, rbp7, 0xade1, v44, 0xade1);
    rax45 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v12) - reinterpret_cast<unsigned char>(g28));
    if (rax45) {
        fun_2570();
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32 - 8 + 8);
        goto addr_3571_26;
    } else {
        return;
    }
    do {
        addr_3150_22:
        ++rax38;
        if (reinterpret_cast<uint64_t>(rsi39) <= reinterpret_cast<uint64_t>(rax38)) 
            break;
        edx34 = *rax38;
    } while (*reinterpret_cast<signed char*>(&edx34));
    rax46 = fun_25a0(rbp37, 58);
    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
    if (!rax46) {
        r8_48 = rbp37;
        if (v49 && ((zf50 = do_lookup == 0, !zf50) && (rax51 = canon_host(rbp37, 58), rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8), r8_48 = rax51, !rax51))) {
            r8_48 = rbp37;
        }
        rax52 = fun_2560(r8_48, r8_48);
        rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
        rdi54 = hoststr_7;
        r8_55 = r8_48;
        r9 = reinterpret_cast<struct s0*>(&rax52->f3);
        below_or_equal56 = reinterpret_cast<unsigned char>(r9) <= reinterpret_cast<unsigned char>(hostlen_8);
        if (!below_or_equal56) {
            hostlen_8 = r9;
            fun_2450(rdi54, rdi54);
            rax57 = xmalloc(r9, 58);
            rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8 - 8 + 8);
            r8_55 = r8_55;
            hoststr_7 = rax57;
            rdi54 = rax57;
        }
        rcx16 = reinterpret_cast<struct s0*>("(%s)");
        *reinterpret_cast<int32_t*>(&rsi21) = 1;
        *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
        fun_2810(rdi54, 1, 0xffffffffffffffff, "(%s)");
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8);
        r8_10 = r8_55;
    } else {
        rax46->f0 = 0;
        r9_58 = reinterpret_cast<struct s0*>(&rax46->f1);
        if (!v59 || (zf60 = do_lookup == 0, zf60)) {
            addr_3195_36:
            r8_61 = rbp37;
            goto addr_3198_37;
        } else {
            rax62 = canon_host(rbp37, 58);
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            r9_58 = r9_58;
            r8_61 = rax62;
            if (rax62) {
                addr_3198_37:
                rax63 = fun_2560(r8_61, r8_61);
                rax64 = fun_2560(r9_58, r9_58);
                rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 8 + 8);
                r9 = r9_58;
                rdi66 = hoststr_7;
                r8_67 = r8_61;
                r10_68 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax63) + reinterpret_cast<unsigned char>(rax64) + 4);
                below_or_equal69 = reinterpret_cast<unsigned char>(r10_68) <= reinterpret_cast<unsigned char>(hostlen_8);
                if (!below_or_equal69) {
                    hostlen_8 = r10_68;
                    fun_2450(rdi66, rdi66);
                    rax70 = xmalloc(r10_68, 58);
                    rsp65 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8 - 8 + 8);
                    r8_67 = r8_61;
                    r9 = r9;
                    hoststr_7 = rax70;
                    rdi66 = rax70;
                }
            } else {
                goto addr_3195_36;
            }
        }
        rcx16 = reinterpret_cast<struct s0*>("(%s:%s)");
        *reinterpret_cast<int32_t*>(&rsi21) = 1;
        *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
        fun_2810(rdi66, 1, 0xffffffffffffffff, "(%s:%s)");
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp65) - 8 + 8);
        r8_10 = r8_67;
    }
    if (r8_10 != rbp37) {
        fun_2450(r8_10, r8_10);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
    }
    rbp7 = hoststr_7;
    if (!rbp7) {
        rbp7 = reinterpret_cast<struct s0*>(0xade1);
        goto addr_3228_24;
    }
    addr_32c8_13:
    rax71 = fun_2540();
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
    r9 = rax71;
    addr_32de_46:
    *reinterpret_cast<int32_t*>(&rsi21) = 1;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&r14_24) = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&r14_24)));
    r15_25 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp23) + 0xbd);
    *reinterpret_cast<int32_t*>(&r8_10) = 6;
    *reinterpret_cast<int32_t*>(&r8_10 + 4) = 0;
    rcx16 = reinterpret_cast<struct s0*>("%.*s");
    fun_2810(r15_25, 1, 7, "%.*s", r15_25, 1, 7, "%.*s");
    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
    edx34 = rbx8->f4c;
    if (*reinterpret_cast<signed char*>(&edx34)) 
        goto addr_313a_21; else 
        goto addr_3317_19;
    addr_34ce_16:
    eax72 = *reinterpret_cast<int32_t*>(&rax33) - *reinterpret_cast<int32_t*>(&r15_25);
    if (eax72 <= 59) {
        r9 = reinterpret_cast<struct s0*>("  .  ");
        goto addr_32de_46;
    } else {
        if (eax72 > 0x1517f) {
            addr_3571_26:
            fun_25e0();
        } else {
            __asm__("cdq ");
            fun_2810(0xf121, 1, 6, "%02d:%02d");
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            r9 = reinterpret_cast<struct s0*>(0xf121);
            goto addr_32de_46;
        }
    }
    *reinterpret_cast<uint32_t*>(&rcx73) = reinterpret_cast<uint32_t>("src/who.c");
    *reinterpret_cast<int32_t*>(&rcx73 + 4) = 0;
    v74 = r12_18;
    v75 = rbp7;
    v76 = rbx8;
    rsp77 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 88);
    rax78 = g28;
    v79 = rax78;
    rdx80 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp77) + 48);
    rsi81 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp77) + 40);
    eax82 = read_utmp();
    rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8);
    if (eax82) {
        quotearg_n_style_colon();
        fun_2480();
        fun_2750();
    } else {
        zf84 = short_list == 0;
        rbp85 = v86;
        r12_87 = v88;
        if (!zf84) {
            rbx89 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_87) + 0xffffffffffffffff);
            if (r12_87) {
                rdx90 = reinterpret_cast<struct s0*>(0xade1);
                r8_10 = reinterpret_cast<struct s0*>(0);
                r12_91 = reinterpret_cast<struct s0*>(0xade1);
                do {
                    if (rbp85->f2c && rbp85->f0 == 7) {
                        v92 = r8_10;
                        rax93 = extract_trimmed_name(rbp85, rsi81, rdx90, rcx73);
                        rdx90 = r12_91;
                        rsi81 = reinterpret_cast<struct s0*>("%s%s");
                        rcx73 = rax93;
                        r12_91 = reinterpret_cast<struct s0*>(" ");
                        fun_2720(1, "%s%s", rdx90, rcx73, r8_10, r9, v94, v92, v95, v96, v97, v98, v99, v100, v101, v79, v102, v76, v75, v74);
                        fun_2450(rax93, rax93);
                        r8_10 = reinterpret_cast<struct s0*>(&v92->f1);
                    }
                    rbp85 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(rbp85) + 0x180);
                    cf103 = reinterpret_cast<uint64_t>(rbx89) < 1;
                    --rbx89;
                } while (!cf103);
                r12_87 = r8_10;
            }
            rax104 = fun_2540();
            fun_2720(1, rax104, r12_87, rcx73, r8_10, r9, v105, v92, v106, v107, v108, v109, v110, v111, v112, v79, v113, v76, v75, v74);
            goto addr_387a_60;
        }
        zf114 = include_heading == 0;
        if (!zf114) {
            rax115 = fun_2540();
            v116 = rax115;
            rax117 = fun_2540();
            v118 = rax117;
            rax119 = fun_2540();
            rax120 = fun_2540();
            rax121 = fun_2540();
            rax122 = fun_2540();
            rax123 = fun_2540();
            r9 = rax121;
            rsi81 = rax123;
            *reinterpret_cast<int32_t*>(&rdx80) = 32;
            *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
            r8_10 = rax122;
            *reinterpret_cast<uint32_t*>(&rcx73) = 0xffffffff;
            *reinterpret_cast<int32_t*>(&rcx73 + 4) = 0;
            print_line(-1, rsi81, 32, 0xffffffff, r8_10, r9, rax120, rax119, v118, v116, v124, v118);
            rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
        }
        *reinterpret_cast<int32_t*>(&rbx125) = 0;
        *reinterpret_cast<int32_t*>(&rbx125 + 4) = 0;
        zf126 = my_line_only == 0;
        if (!zf126) 
            goto addr_3b63_64; else 
            goto addr_35fd_65;
    }
    fun_2570();
    addr_3b63_64:
    rax127 = fun_26f0();
    rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
    rbx125 = rax127;
    if (!rax127) {
        addr_387a_60:
        fun_2450(v128, v128);
        rax129 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v79) - reinterpret_cast<unsigned char>(g28));
        if (!rax129) {
            goto v130;
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx80) = 5;
        *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
        rsi81 = reinterpret_cast<struct s0*>("/dev/");
        eax139 = fun_24a0(rax127, "/dev/", 5, rcx73, r8_10, r9, v131, v118, v116, v132, v133, v134, v135, v136, v137, v79, v138, v76, v75, v74);
        rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
        if (!eax139) {
            rbx125 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx125) + 5);
        }
    }
    addr_35fd_65:
    r14_140 = reinterpret_cast<struct s0*>(&rbp85->f8);
    r13_141 = reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_87) + 0xffffffffffffffff);
    rbp142 = reinterpret_cast<struct s0*>(0x8000000000000000);
    if (r12_87) {
        do {
            zf143 = my_line_only == 0;
            r12_144 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffff8);
            if (zf143 || (*reinterpret_cast<int32_t*>(&rdx80) = 32, *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0, rsi81 = r14_140, eax153 = fun_24a0(rbx125, rsi81, 32, rcx73, r8_10, r9, v145, v118, v116, v146, v147, v148, v149, v150, v151, v79, v152, v76, v75, v74), rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8), eax153 == 0)) {
                zf154 = need_users == 0;
                eax155 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffff8);
                if (!zf154 && (r14_140->f24 && *reinterpret_cast<int16_t*>(&eax155) == 7)) {
                    rsi81 = rbp142;
                    print_user(r12_144, rsi81, rdx80, rcx73, r8_10, r9);
                    rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
                    eax155 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffff8);
                    goto addr_3639_74;
                }
                zf156 = need_runlevel == 0;
                if (zf156) 
                    goto addr_369a_76;
                if (*reinterpret_cast<int16_t*>(&eax155) == 1) 
                    goto addr_38e5_78;
            } else {
                addr_3634_79:
                eax155 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffff8);
                goto addr_3639_74;
            }
            addr_369a_76:
            zf157 = need_boottime == 0;
            if (zf157 || *reinterpret_cast<int16_t*>(&eax155) != 2) {
                zf158 = need_clockchange == 0;
                if (zf158 || *reinterpret_cast<int16_t*>(&eax155) != 3) {
                    zf159 = need_initspawn == 0;
                    if (zf159 || *reinterpret_cast<int16_t*>(&eax155) != 5) {
                        zf160 = need_login == 0;
                        if (zf160 || *reinterpret_cast<int16_t*>(&eax155) != 6) {
                            zf161 = need_deadprocs == 0;
                            if (zf161 || *reinterpret_cast<int16_t*>(&eax155) != 8) {
                                addr_3639_74:
                                if (*reinterpret_cast<int16_t*>(&eax155) == 2) {
                                    rbp142 = reinterpret_cast<struct s0*>(static_cast<int64_t>(reinterpret_cast<int32_t>(r14_140->f14c)));
                                    continue;
                                }
                            } else {
                                r12_162 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp83) + 60);
                                rax163 = make_id_equals_comment(r12_144, rsi81, rdx80, rcx73, r8_10, r9);
                                fun_2810(r12_162, 1, 12, "%ld", r12_162, 1, 12, "%ld");
                                rsp164 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8 - 8 + 8);
                                zf165 = exitstr_0 == 0;
                                if (zf165) {
                                    rax166 = fun_2540();
                                    rax167 = fun_2560(rax166, rax166);
                                    rax168 = fun_2540();
                                    rax169 = fun_2560(rax168, rax168);
                                    rdi170 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax167) + reinterpret_cast<unsigned char>(rax169) + 14);
                                    rax171 = xmalloc(rdi170, "exit=", rdi170, "exit=");
                                    rsp164 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp164) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                                    exitstr_0 = rax171;
                                }
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v146) + 4) = r14_140->f146;
                                rax172 = fun_2540();
                                r9d173 = r14_140->f144;
                                v116 = rax172;
                                rax174 = fun_2540();
                                *reinterpret_cast<int32_t*>(&r10_175) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v146) + 4);
                                *reinterpret_cast<int32_t*>(&r10_175 + 4) = 0;
                                rdi176 = exitstr_0;
                                *reinterpret_cast<int32_t*>(&r9_177) = r9d173;
                                *reinterpret_cast<int32_t*>(&r9_177 + 4) = 0;
                                fun_2810(rdi176, 1, 0xffffffffffffffff, "%s%d %s%d", rdi176, 1, 0xffffffffffffffff, "%s%d %s%d");
                                rdx178 = exitstr_0;
                                edi179 = r14_140->f14c;
                                v118 = rdx178;
                                rax180 = time_string_isra_0(edi179, 1, rdx178, "%s%d %s%d", rax174, r9_177, v116, r10_175);
                                r8_10 = r14_140;
                                r9 = rax180;
                                *reinterpret_cast<uint32_t*>(&rcx73) = 32;
                                *reinterpret_cast<int32_t*>(&rcx73 + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rdx80) = 32;
                                *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
                                rsi81 = reinterpret_cast<struct s0*>(0xade1);
                                print_line(-1, 0xade1, 32, 32, r8_10, r9, 0xade1, r12_162, rax163, v118, v116, r10_175);
                                fun_2450(rax163, rax163);
                                rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp164) - 8 + 8 - 8 + 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 48 - 8 + 8);
                                goto addr_3634_79;
                            }
                        } else {
                            rax181 = make_id_equals_comment(r12_144, rsi81, rdx80, rcx73, r8_10, r9);
                            rsp182 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
                            r10_183 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp182) + 60);
                            r8_184 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffffc)));
                            r15_185 = rax181;
                            v116 = r10_183;
                            fun_2810(r10_183, 1, 12, "%ld", r10_183, 1, 12, "%ld");
                            edi186 = r14_140->f14c;
                            rax188 = time_string_isra_0(edi186, 1, 12, "%ld", r8_184, r9, v187, v118);
                            v118 = rax188;
                            rax189 = fun_2540();
                            r8_10 = r14_140;
                            *reinterpret_cast<uint32_t*>(&rcx73) = 32;
                            *reinterpret_cast<int32_t*>(&rcx73 + 4) = 0;
                            v190 = r15_185;
                            rsi81 = rax189;
                            *reinterpret_cast<int32_t*>(&rdx80) = 32;
                            *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
                            v191 = v116;
                            rsp192 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp182) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
                            r9 = v118;
                        }
                    } else {
                        rax193 = make_id_equals_comment(r12_144, rsi81, rdx80, rcx73, r8_10, r9);
                        rsp194 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
                        r10_195 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp194) + 60);
                        r8_196 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffffc)));
                        r15_185 = rax193;
                        v118 = r10_195;
                        fun_2810(r10_195, 1, 12, "%ld", r10_195, 1, 12, "%ld");
                        edi197 = r14_140->f14c;
                        rax199 = time_string_isra_0(edi197, 1, 12, "%ld", r8_196, r9, v198, v118);
                        r8_10 = r14_140;
                        *reinterpret_cast<uint32_t*>(&rcx73) = 32;
                        *reinterpret_cast<int32_t*>(&rcx73 + 4) = 0;
                        v190 = r15_185;
                        r9 = rax199;
                        *reinterpret_cast<int32_t*>(&rdx80) = 32;
                        *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
                        rsi81 = reinterpret_cast<struct s0*>(0xade1);
                        v191 = v118;
                        rsp192 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp194) - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
                    }
                } else {
                    edi200 = r14_140->f14c;
                    rax202 = time_string_isra_0(edi200, rsi81, rdx80, rcx73, r8_10, r9, v201, v118);
                    rsp203 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
                    r15_204 = rax202;
                    goto addr_39eb_91;
                }
            } else {
                edi205 = r14_140->f14c;
                rax207 = time_string_isra_0(edi205, rsi81, rdx80, rcx73, r8_10, r9, v206, v118);
                rsp203 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8);
                r15_204 = rax207;
                goto addr_39eb_91;
            }
            print_line(-1, rsi81, 32, 32, r8_10, r9, 0xade1, v191, v190, 0xade1, v208, v118);
            fun_2450(r15_185, r15_185);
            rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp192) - 8 + 8 + 32 - 8 + 8);
            eax155 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffff8);
            goto addr_3639_74;
            addr_39eb_91:
            rax209 = fun_2540();
            r9 = r15_204;
            v210 = reinterpret_cast<struct s0*>(0xade1);
            r8_10 = rax209;
            rsp211 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp203) - 8 + 8 - 8 - 8 - 8 - 8);
            addr_3a07_94:
            *reinterpret_cast<uint32_t*>(&rcx73) = 0xffffffff;
            *reinterpret_cast<int32_t*>(&rcx73 + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx80) = 32;
            *reinterpret_cast<int32_t*>(&rdx80 + 4) = 0;
            rsi81 = reinterpret_cast<struct s0*>(0xade1);
            print_line(-1, 0xade1, 32, 0xffffffff, r8_10, r9, 0xade1, 0xade1, v210, 0xade1, v212, v118);
            eax155 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffff8);
            rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp211) - 8 + 8 + 32);
            goto addr_3639_74;
            addr_38e5_78:
            __asm__("cdq ");
            zf213 = runlevline_2 == 0;
            r12d214 = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r14_140) + 0xfffffffffffffffc) / 0x100;
            if (zf213) {
                rax215 = fun_2540();
                rax216 = fun_2560(rax215, rax215);
                rdi217 = reinterpret_cast<struct s0*>(&rax216->f3);
                rax218 = xmalloc(rdi217, "run-level", rdi217, "run-level");
                rsp83 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8 - 8 + 8 - 8 + 8);
                runlevline_2 = rax218;
            }
            fun_2540();
            rdi219 = runlevline_2;
            fun_2810(rdi219, 1, 0xffffffffffffffff, "%s %c", rdi219, 1, 0xffffffffffffffff, "%s %c");
            rsp220 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp83) - 8 + 8 - 8 + 8);
            zf221 = comment_1 == 0;
            if (zf221) {
                rax222 = fun_2540();
                rax223 = fun_2560(rax222, rax222);
                rdi224 = reinterpret_cast<struct s0*>(&rax223->f2);
                rax225 = xmalloc(rdi224, "last=", rdi224, "last=");
                rsp220 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp220) - 8 + 8 - 8 + 8 - 8 + 8);
                comment_1 = rax225;
            }
            r15d226 = *reinterpret_cast<unsigned char*>(&r12d214);
            r12d227 = 83;
            if (*reinterpret_cast<unsigned char*>(&r12d214) != 78) {
                r12d227 = r15d226;
            }
            rax228 = fun_2540();
            rdi229 = comment_1;
            *reinterpret_cast<uint32_t*>(&r9_230) = r12d227;
            *reinterpret_cast<int32_t*>(&r9_230 + 4) = 0;
            fun_2810(rdi229, 1, 0xffffffffffffffff, "%s%c", rdi229, 1, 0xffffffffffffffff, "%s%c");
            if (r15d226 - 32 > 94) {
                r15_231 = reinterpret_cast<struct s0*>(0xade1);
            } else {
                r15_231 = comment_1;
            }
            edi232 = r14_140->f14c;
            rax234 = time_string_isra_0(edi232, 1, 0xffffffffffffffff, "%s%c", rax228, r9_230, v233, v118);
            r8_10 = runlevline_2;
            v210 = r15_231;
            r9 = rax234;
            rsp211 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp220) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8);
            goto addr_3a07_94;
            --r13_141;
            r14_140 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_140) + 0x180);
        } while (r13_141 != 0xffffffffffffffff);
        goto addr_387a_60;
    } else {
        goto addr_387a_60;
    }
}

struct s5 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s5* locale_charset();

/* gettext_quote.part.0 */
struct s0* gettext_quote_part_0(struct s0* rdi, int32_t esi, struct s0* rdx) {
    struct s5* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s0* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s0* rax10;
    struct s0* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s0*>(0xa9bb);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax7 = reinterpret_cast<struct s0*>(0xa9b4);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s0*>(0xa9bf);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rdi->f0) == 96)) {
                rax10 = reinterpret_cast<struct s0*>(0xa9b0);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s0*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s0*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gedd8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gedd8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2423() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t utmpxname = 0x2030;

void fun_2433() {
    __asm__("cli ");
    goto utmpxname;
}

int64_t __snprintf_chk = 0x2040;

void fun_2443() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2050;

void fun_2453() {
    __asm__("cli ");
    goto free;
}

int64_t localtime = 0x2060;

void fun_2463() {
    __asm__("cli ");
    goto localtime;
}

int64_t abort = 0x2070;

void fun_2473() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2080;

void fun_2483() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncpy = 0x2090;

void fun_2493() {
    __asm__("cli ");
    goto strncpy;
}

int64_t strncmp = 0x20a0;

void fun_24a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20b0;

void fun_24b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20c0;

void fun_24c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t puts = 0x20d0;

void fun_24d3() {
    __asm__("cli ");
    goto puts;
}

int64_t reallocarray = 0x20e0;

void fun_24e3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20f0;

void fun_24f3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t endutxent = 0x2100;

void fun_2503() {
    __asm__("cli ");
    goto endutxent;
}

int64_t fclose = 0x2110;

void fun_2513() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2120;

void fun_2523() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t stpcpy = 0x2130;

void fun_2533() {
    __asm__("cli ");
    goto stpcpy;
}

int64_t dcgettext = 0x2140;

void fun_2543() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2150;

void fun_2553() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2160;

void fun_2563() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2170;

void fun_2573() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2180;

void fun_2583() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2190;

void fun_2593() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x21a0;

void fun_25a3() {
    __asm__("cli ");
    goto strchr;
}

int64_t gai_strerror = 0x21b0;

void fun_25b3() {
    __asm__("cli ");
    goto gai_strerror;
}

int64_t strrchr = 0x21c0;

void fun_25c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21d0;

void fun_25d3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21e0;

void fun_25e3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21f0;

void fun_25f3() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x2200;

void fun_2603() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2210;

void fun_2613() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2220;

void fun_2623() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2230;

void fun_2633() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2240;

void fun_2643() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t setutxent = 0x2250;

void fun_2653() {
    __asm__("cli ");
    goto setutxent;
}

int64_t stat = 0x2260;

void fun_2663() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2270;

void fun_2673() {
    __asm__("cli ");
    goto memcpy;
}

int64_t kill = 0x2280;

void fun_2683() {
    __asm__("cli ");
    goto kill;
}

int64_t time = 0x2290;

void fun_2693() {
    __asm__("cli ");
    goto time;
}

int64_t fileno = 0x22a0;

void fun_26a3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x22b0;

void fun_26b3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22c0;

void fun_26c3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22d0;

void fun_26d3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22e0;

void fun_26e3() {
    __asm__("cli ");
    goto __freading;
}

int64_t ttyname = 0x22f0;

void fun_26f3() {
    __asm__("cli ");
    goto ttyname;
}

int64_t realloc = 0x2300;

void fun_2703() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2310;

void fun_2713() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2320;

void fun_2723() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t strftime = 0x2330;

void fun_2733() {
    __asm__("cli ");
    goto strftime;
}

int64_t getutxent = 0x2340;

void fun_2743() {
    __asm__("cli ");
    goto getutxent;
}

int64_t error = 0x2350;

void fun_2753() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2360;

void fun_2763() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x2370;

void fun_2773() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2380;

void fun_2783() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2390;

void fun_2793() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23a0;

void fun_27a3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t getaddrinfo = 0x23b0;

void fun_27b3() {
    __asm__("cli ");
    goto getaddrinfo;
}

int64_t strdup = 0x23c0;

void fun_27c3() {
    __asm__("cli ");
    goto strdup;
}

int64_t mbsinit = 0x23d0;

void fun_27d3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23e0;

void fun_27e3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x23f0;

void fun_27f3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t freeaddrinfo = 0x2400;

void fun_2803() {
    __asm__("cli ");
    goto freeaddrinfo;
}

int64_t __sprintf_chk = 0x2410;

void fun_2813() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(int64_t rdi);

struct s0* fun_2710(int64_t rdi, ...);

void fun_2520(int64_t rdi, int64_t rsi);

void fun_24f0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2580(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx);

void usage();

int64_t stdout = 0;

int64_t Version = 0xa944;

void version_etc(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_2780();

uint32_t hard_locale();

int64_t time_format = 0;

uint32_t time_format_width = 0;

int32_t optind = 0;

int64_t quote(int64_t rdi, int64_t* rsi);

int64_t fun_2873(int32_t edi, int64_t* rsi) {
    int64_t rdi3;
    int64_t* rsi4;
    int64_t rdi5;
    int32_t eax6;
    int64_t rdi7;
    int64_t rcx8;
    int64_t rax9;
    int1_t zf10;
    uint32_t eax11;
    int64_t rdx12;
    int64_t rax13;
    int32_t ebx14;
    int64_t rdi15;
    int64_t rdi16;

    __asm__("cli ");
    rdi3 = *rsi;
    set_program_name(rdi3);
    fun_2710(6, 6);
    fun_2520("coreutils", "/usr/local/share/locale");
    fun_24f0("coreutils", "/usr/local/share/locale");
    atexit(0x4350, "/usr/local/share/locale");
    rsi4 = rsi;
    *reinterpret_cast<int32_t*>(&rdi5) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
    eax6 = fun_2580(rdi5, rsi4, "abdlmpqrstuwHT", 0xe940);
    if (eax6 != -1) {
        if (eax6 > 0x80) {
            addr_2bce_4:
            usage();
        } else {
            if (eax6 <= 71) {
                if (eax6 == 0xffffff7d) {
                    rdi7 = stdout;
                    rcx8 = Version;
                    rsi4 = reinterpret_cast<int64_t*>("who");
                    version_etc(rdi7, "who", "GNU coreutils", rcx8, "Joseph Arceneaux", "David MacKenzie", "Michael Stone", 0);
                    eax6 = fun_2780();
                }
                if (eax6 != 0xffffff7e) 
                    goto addr_2bce_4;
            } else {
                *reinterpret_cast<uint32_t*>(&rax9) = eax6 - 72;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax9) <= 56) {
                    goto *reinterpret_cast<int32_t*>(0xa850 + rax9 * 4) + 0xa850;
                }
            }
        }
        usage();
    }
    if (!0) {
        need_users = 1;
        short_output = 1;
    }
    zf10 = include_exit == 0;
    if (!zf10) {
        short_output = 0;
    }
    eax11 = hard_locale();
    rdx12 = reinterpret_cast<int64_t>("%b %e %H:%M");
    if (*reinterpret_cast<unsigned char*>(&eax11)) {
        rdx12 = reinterpret_cast<int64_t>("%Y-%m-%d %H:%M");
    }
    time_format = rdx12;
    time_format_width = (eax11 - (eax11 + reinterpret_cast<uint1_t>(eax11 < eax11 + reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&eax11) < 1))) & 0xfffffffc) + 16;
    rax13 = optind;
    ebx14 = edi - *reinterpret_cast<int32_t*>(&rax13);
    if (ebx14 == 1) {
        rdi15 = rsi[rax13];
        who(rdi15, 0);
    } else {
        if (ebx14 > 1) {
            if (ebx14 != 2) {
                addr_2b9d_22:
                rdi16 = (rsi + rax13)[2];
                quote(rdi16, rsi4);
                fun_2540();
                fun_2750();
                goto addr_2bce_4;
            } else {
                my_line_only = 1;
                goto addr_2b62_24;
            }
        } else {
            if (ebx14 + 1 > 1) 
                goto addr_2b9d_22; else 
                goto addr_2b62_24;
        }
    }
    addr_2b73_26:
    return 0;
    addr_2b62_24:
    who("/var/run/utmp", 1);
    goto addr_2b73_26;
}

int64_t __libc_start_main = 0;

void fun_2be3() {
    __asm__("cli ");
    __libc_start_main(0x2870, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xf008;

void fun_2420(int64_t rdi);

int64_t fun_2c83() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2420(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2cc3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s0* program_name = reinterpret_cast<struct s0*>(0);

void fun_2610(struct s0* rdi, int64_t rsi, int64_t rdx, struct s0* rcx);

int32_t fun_2630(int64_t rdi);

int64_t stderr = 0;

void fun_27a0(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3d73(int32_t edi) {
    struct s0* r12_2;
    struct s0* rax3;
    struct s0* v4;
    struct s0* rax5;
    struct s0* rcx6;
    struct s0* r8_7;
    struct s0* r9_8;
    int64_t r12_9;
    struct s0* rax10;
    int64_t r12_11;
    struct s0* rax12;
    int64_t r12_13;
    struct s0* rax14;
    int64_t r12_15;
    struct s0* rax16;
    int64_t r12_17;
    struct s0* rax18;
    int64_t r12_19;
    struct s0* rax20;
    int64_t r12_21;
    struct s0* rax22;
    int64_t r12_23;
    struct s0* rax24;
    struct s0* rax25;
    struct s0* r8_26;
    struct s0* r9_27;
    int32_t eax28;
    struct s0* r13_29;
    struct s0* rax30;
    struct s0* r8_31;
    struct s0* r9_32;
    struct s0* rax33;
    struct s0* r8_34;
    struct s0* r9_35;
    int32_t eax36;
    struct s0* rax37;
    struct s0* r8_38;
    struct s0* r9_39;
    struct s0* rax40;
    struct s0* r8_41;
    struct s0* r9_42;
    struct s0* rax43;
    struct s0* r8_44;
    struct s0* r9_45;
    int32_t eax46;
    struct s0* rax47;
    struct s0* r8_48;
    struct s0* r9_49;
    int64_t r15_50;
    struct s0* rax51;
    struct s0* rax52;
    struct s0* r8_53;
    struct s0* r9_54;
    struct s0* rax55;
    int64_t rdi56;
    struct s0* r8_57;
    struct s0* r9_58;
    int64_t v59;
    int64_t v60;
    int64_t v61;
    int64_t v62;
    int64_t v63;
    int64_t v64;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2540();
            fun_2720(1, rax5, r12_2, rcx6, r8_7, r9_8, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_9 = stdout;
            rax10 = fun_2540();
            fun_2610(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_2540();
            fun_2610(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_2540();
            fun_2610(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_2540();
            fun_2610(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_2540();
            fun_2610(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_2540();
            fun_2610(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_2540();
            fun_2610(rax22, r12_21, 5, rcx6);
            r12_23 = stdout;
            rax24 = fun_2540();
            fun_2610(rax24, r12_23, 5, rcx6);
            rax25 = fun_2540();
            fun_2720(1, rax25, "/var/run/utmp", "/var/log/wtmp", r8_26, r9_27, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            do {
                if (1) 
                    break;
                eax28 = fun_2630("who");
            } while (eax28);
            r13_29 = v4;
            if (!r13_29) {
                rax30 = fun_2540();
                fun_2720(1, rax30, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_31, r9_32, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax33 = fun_2710(5);
                if (!rax33 || (eax36 = fun_24a0(rax33, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_34, r9_35, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0), !eax36)) {
                    rax37 = fun_2540();
                    r13_29 = reinterpret_cast<struct s0*>("who");
                    fun_2720(1, rax37, "https://www.gnu.org/software/coreutils/", "who", r8_38, r9_39, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                } else {
                    r13_29 = reinterpret_cast<struct s0*>("who");
                    goto addr_4168_9;
                }
            } else {
                rax40 = fun_2540();
                fun_2720(1, rax40, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_41, r9_42, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax43 = fun_2710(5);
                if (!rax43 || (eax46 = fun_24a0(rax43, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_44, r9_45, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0), !eax46)) {
                    addr_406e_11:
                    rax47 = fun_2540();
                    fun_2720(1, rax47, "https://www.gnu.org/software/coreutils/", "who", r8_48, r9_49, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<struct s0*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_29 == "who")) {
                        r12_2 = reinterpret_cast<struct s0*>(0xade1);
                    }
                } else {
                    addr_4168_9:
                    r15_50 = stdout;
                    rax51 = fun_2540();
                    fun_2610(rax51, r15_50, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_406e_11;
                }
            }
            rax52 = fun_2540();
            rcx6 = r12_2;
            fun_2720(1, rax52, r13_29, rcx6, r8_53, r9_54, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            addr_3dce_14:
            fun_2780();
        }
    } else {
        rax55 = fun_2540();
        rdi56 = stderr;
        rcx6 = r12_2;
        fun_27a0(rdi56, 1, rax55, rcx6, r8_57, r9_58, v59, v60, v61, v62, v63, v64);
        goto addr_3dce_14;
    }
}

/* hints.0 */
int32_t hints_0 = 0;

int32_t fun_27b0();

int32_t last_cherror = 0;

int64_t g20;

int64_t fun_27c0(int64_t rdi);

void fun_2800(int64_t rdi);

int64_t fun_41a3(int64_t rdi) {
    struct s0* rax2;
    int32_t eax3;
    int64_t r12_4;
    int64_t rdi5;
    int64_t rax6;
    void* rax7;

    __asm__("cli ");
    rax2 = g28;
    hints_0 = 2;
    eax3 = fun_27b0();
    if (eax3) {
        last_cherror = eax3;
        *reinterpret_cast<int32_t*>(&r12_4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_4) + 4) = 0;
    } else {
        rdi5 = g20;
        if (!rdi5) {
            rdi5 = rdi;
        }
        rax6 = fun_27c0(rdi5);
        r12_4 = rax6;
        if (!rax6) {
            last_cherror = -10;
        }
        fun_2800(0);
    }
    rax7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax7) {
        fun_2570();
    } else {
        return r12_4;
    }
}

int64_t fun_4263(int64_t rdi, int32_t* rsi) {
    struct s0* rax3;
    int32_t eax4;
    int64_t r12_5;
    int64_t rdi6;
    int64_t rax7;
    void* rax8;

    __asm__("cli ");
    rax3 = g28;
    hints_0 = 2;
    eax4 = fun_27b0();
    if (eax4) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        if (rsi) {
            *rsi = eax4;
        }
    } else {
        rdi6 = g20;
        if (!rdi6) {
            rdi6 = rdi;
        }
        rax7 = fun_27c0(rdi6);
        r12_5 = rax7;
        if (!rax7 && rsi) {
            *rsi = -10;
        }
        fun_2800(0);
    }
    rax8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rax8) {
        fun_2570();
    } else {
        return r12_5;
    }
}

void fun_4323() {
    __asm__("cli ");
}

int64_t file_name = 0;

void fun_4333(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4343(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

struct s0* quotearg_colon();

int32_t exit_failure = 1;

struct s0* fun_24b0(int64_t rdi, int64_t rsi, int64_t rdx, struct s0* rcx, struct s0* r8);

void fun_4353() {
    int64_t rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    int64_t rdi6;
    int32_t eax7;
    struct s0* rax8;
    int64_t rdi9;
    struct s0* rax10;
    int64_t rsi11;
    struct s0* r8_12;
    struct s0* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2480(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2540();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_43e3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2750();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_24b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_43e3_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2750();
    }
}

int32_t setlocale_null_r();

int64_t fun_4403() {
    struct s0* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax3;
    }
}

struct s6 {
    signed char[20] pad20;
    signed char f14;
};

signed char* fun_4483(uint64_t rdi, struct s6* rsi) {
    signed char* r8_3;
    uint64_t rcx4;
    signed char* rsi5;
    uint64_t rdx6;
    int32_t eax7;
    uint64_t rdx8;
    uint64_t rax9;
    uint64_t rcx10;
    int32_t ecx11;

    __asm__("cli ");
    rsi->f14 = 0;
    r8_3 = &rsi->f14;
    rcx4 = rdi;
    if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
        do {
            rsi5 = r8_3;
            --r8_3;
            rdx6 = (__intrinsic() >> 2) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rcx4) >> 63);
            eax7 = static_cast<int32_t>(48 + (rdx6 + rdx6 * 4) * 2) - *reinterpret_cast<int32_t*>(&rcx4);
            rcx4 = rdx6;
            *r8_3 = *reinterpret_cast<signed char*>(&eax7);
        } while (rdx6);
        *(r8_3 - 1) = 45;
        return rsi5 - 2;
    } else {
        do {
            --r8_3;
            rdx8 = __intrinsic() >> 3;
            rax9 = rdx8 + rdx8 * 4;
            rcx10 = rcx4 - (rax9 + rax9);
            ecx11 = *reinterpret_cast<int32_t*>(&rcx10) + 48;
            *r8_3 = *reinterpret_cast<signed char*>(&ecx11);
            rcx4 = rdx8;
        } while (rdx8);
        return r8_3;
    }
}

void fun_2790(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

struct s7 {
    signed char[1] pad1;
    struct s0* f1;
    signed char[2] pad4;
    struct s0* f4;
};

struct s7* fun_25c0();

struct s0* __progname = reinterpret_cast<struct s0*>(0);

struct s0* __progname_full = reinterpret_cast<struct s0*>(0);

void fun_4523(struct s0* rdi) {
    int64_t rcx2;
    struct s0* rbx3;
    struct s7* rax4;
    struct s0* r12_5;
    struct s0* rcx6;
    struct s0* r8_7;
    struct s0* r9_8;
    int64_t rbx9;
    struct s0* rbp10;
    struct s0* r12_11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    struct s0* v17;
    int64_t v18;
    struct s0* v19;
    struct s0* v20;
    struct s0* v21;
    int32_t eax22;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2790("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2470("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_25c0();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s0*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax22 = fun_24a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6, r8_7, r9_8, rbx9, rbp10, r12_11, __return_address(), v12, v13, v14, v15, v16, v17, v18, v19, v20, v21), !eax22))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s0**>(&rax4->f1) == 0x6c) || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s0*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5cc3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2480();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xf2c0;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5d03(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf2c0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5d23(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf2c0);
    }
    *rdi = esi;
    return 0xf2c0;
}

int64_t fun_5d43(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xf2c0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s8 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5d83(struct s8* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s8*>(0xf2c0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s9 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s9* fun_5da3(struct s9* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s9*>(0xf2c0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x282a;
    if (!rdx) 
        goto 0x282a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xf2c0;
}

struct s10 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5de3(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s10* r8) {
    struct s10* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s10*>(0xf2c0);
    }
    rax7 = fun_2480();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5e16);
    *rax7 = r15d8;
    return rax13;
}

struct s11 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5e63(int64_t rdi, int64_t rsi, struct s0** rdx, struct s11* rcx) {
    struct s11* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s11*>(0xf2c0);
    }
    rax6 = fun_2480();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5e91);
    rsi15 = reinterpret_cast<struct s0*>(&rax14->f1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5eec);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5f53() {
    __asm__("cli ");
}

struct s0* gf098 = reinterpret_cast<struct s0*>(0xc0);

int64_t slotvec0 = 0x100;

void fun_5f63() {
    uint32_t eax1;
    struct s0* r12_2;
    uint64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2450(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xf1c0) {
        fun_2450(rdi7);
        gf098 = reinterpret_cast<struct s0*>(0xf1c0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xf090) {
        fun_2450(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xf090);
    }
    nslots = 1;
    return;
}

void fun_6003() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6023() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6033(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6053(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_6073(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s3* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2830;
    rcx5 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax6;
    }
}

struct s0* fun_6103(struct s0* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s3* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2835;
    rcx6 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2570();
    } else {
        return rax7;
    }
}

struct s0* fun_6193(int32_t edi, int64_t rsi) {
    struct s0* rax3;
    struct s3* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x283a;
    rcx4 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2570();
    } else {
        return rax5;
    }
}

struct s0* fun_6223(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s3* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x283f;
    rcx5 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax6;
    }
}

struct s0* fun_62b3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s3* rsp4;
    struct s0* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x9000]");
    __asm__("movdqa xmm1, [rip+0x9008]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x8ff1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2570();
    } else {
        return rax10;
    }
}

struct s0* fun_6353(int64_t rdi, uint32_t esi) {
    struct s3* rsp3;
    struct s0* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8f60]");
    __asm__("movdqa xmm1, [rip+0x8f68]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8f51]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2570();
    } else {
        return rax9;
    }
}

struct s0* fun_63f3(int64_t rdi) {
    struct s0* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8ec0]");
    __asm__("movdqa xmm1, [rip+0x8ec8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x8ea9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2570();
    } else {
        return rax3;
    }
}

struct s0* fun_6483(int64_t rdi, int64_t rsi) {
    struct s0* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8e30]");
    __asm__("movdqa xmm1, [rip+0x8e38]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x8e26]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2570();
    } else {
        return rax4;
    }
}

struct s0* fun_6513(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s0* rdx4;
    struct s3* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2844;
    rcx5 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax6;
    }
}

struct s0* fun_65b3(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s3* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8cfa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x8cf2]");
    __asm__("movdqa xmm2, [rip+0x8cfa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2849;
    if (!rdx) 
        goto 0x2849;
    rcx6 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2570();
    } else {
        return rax7;
    }
}

struct s0* fun_6653(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s0* rcx6;
    struct s3* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8c5a]");
    __asm__("movdqa xmm1, [rip+0x8c62]");
    __asm__("movdqa xmm2, [rip+0x8c6a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x284e;
    if (!rdx) 
        goto 0x284e;
    rcx7 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2570();
    } else {
        return rax9;
    }
}

struct s0* fun_6703(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s0* rdx4;
    struct s3* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8baa]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x8ba2]");
    __asm__("movdqa xmm2, [rip+0x8baa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2853;
    if (!rsi) 
        goto 0x2853;
    rcx5 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax6;
    }
}

struct s0* fun_67a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s0* rcx5;
    struct s3* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8b0a]");
    __asm__("movdqa xmm1, [rip+0x8b12]");
    __asm__("movdqa xmm2, [rip+0x8b1a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2858;
    if (!rsi) 
        goto 0x2858;
    rcx6 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2570();
    } else {
        return rax7;
    }
}

void fun_6843() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6853(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6873() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6893(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_2490(struct s0* rdi, int64_t rsi, int64_t rdx);

struct s0* fun_68b3(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;
    struct s0* r12_4;
    struct s0* rax5;
    struct s0* rax6;

    __asm__("cli ");
    rax3 = xmalloc(33, rsi);
    r12_4 = rax3;
    fun_2490(rax3, rdi + 44, 32);
    r12_4->f20 = reinterpret_cast<struct s0*>(0);
    rax5 = fun_2560(r12_4, r12_4);
    rax6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax5) + reinterpret_cast<unsigned char>(r12_4));
    if (reinterpret_cast<unsigned char>(r12_4) >= reinterpret_cast<unsigned char>(rax6)) {
        addr_68f8_2:
        return r12_4;
    } else {
        do {
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax6) + 0xffffffffffffffff) != 32) 
                goto addr_68f8_2;
            rax6 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax6) - 1);
            *reinterpret_cast<struct s0**>(&rax6->f0) = reinterpret_cast<struct s0*>(0);
        } while (r12_4 != rax6);
    }
    goto addr_68f8_2;
}

void fun_2430();

void fun_2650();

struct s12 {
    struct s0* f0;
    signed char[3] pad4;
    int32_t f4;
    signed char[36] pad44;
    signed char f2c;
    signed char[331] pad376;
    int64_t f178;
};

struct s12* fun_2740();

int32_t fun_2680();

struct s13 {
    struct s0* f0;
    signed char[375] pad376;
    int64_t f178;
};

void fun_2500();

int64_t fun_6923() {
    struct s0* r15_1;
    int64_t* r14_2;
    int64_t* rsi3;
    uint32_t r12d4;
    uint32_t ecx5;
    uint32_t ebp6;
    int64_t rbx7;
    struct s0** v8;
    struct s0** rdx9;
    struct s0* rax10;
    struct s0* v11;
    struct s12* rax12;
    struct s12* r13_13;
    int32_t eax14;
    int32_t* rax15;
    struct s0* rax16;
    int64_t rax17;
    struct s13* rax18;
    int64_t* rdi19;
    void* rax20;
    int64_t* rsi21;
    int64_t rcx22;
    struct s12* rax23;
    void* rax24;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r15_1) = 0;
    *reinterpret_cast<int32_t*>(&r15_1 + 4) = 0;
    r14_2 = rsi3;
    r12d4 = ecx5;
    ebp6 = r12d4 & 2;
    *reinterpret_cast<int32_t*>(&rbx7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
    v8 = rdx9;
    rax10 = g28;
    v11 = rax10;
    fun_2430();
    fun_2650();
    while (rax12 = fun_2740(), r13_13 = rax12, !!rax12) {
        do {
            if (!r13_13->f2c || r13_13->f0 != 7) {
                if (ebp6) 
                    break;
            } else {
                if (*reinterpret_cast<unsigned char*>(&r12d4) & 1 && (!(reinterpret_cast<uint1_t>(r13_13->f4 < 0) | reinterpret_cast<uint1_t>(r13_13->f4 == 0)) && (eax14 = fun_2680(), eax14 < 0))) {
                    rax15 = fun_2480();
                    if (*rax15 == 3) 
                        break;
                    if (rbx7) 
                        goto addr_699e_8;
                    goto addr_6a68_10;
                }
            }
            if (!rbx7) {
                addr_6a68_10:
                rax16 = xpalloc();
                r15_1 = rax16;
                goto addr_699e_8;
            } else {
                addr_699e_8:
                rax17 = rbx7 + rbx7 * 2;
                ++rbx7;
                rax18 = reinterpret_cast<struct s13*>((rax17 << 7) + reinterpret_cast<unsigned char>(r15_1));
                rax18->f0 = r13_13->f0;
                rdi19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rax18) + 8 & 0xfffffffffffffff8);
                rax18->f178 = r13_13->f178;
                rax20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax18) - reinterpret_cast<uint64_t>(rdi19));
                rsi21 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(r13_13) - reinterpret_cast<uint64_t>(rax20));
                *reinterpret_cast<uint32_t*>(&rcx22) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax20) + 0x180) >> 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx22) + 4) = 0;
            }
            while (rcx22) {
                --rcx22;
                *rdi19 = *rsi21;
                ++rdi19;
                ++rsi21;
            }
            rax23 = fun_2740();
            r13_13 = rax23;
        } while (rax23);
        goto addr_69ed_15;
    }
    addr_69f0_16:
    fun_2500();
    *r14_2 = rbx7;
    *v8 = r15_1;
    rax24 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28));
    if (rax24) {
        fun_2570();
    } else {
        return 0;
    }
    addr_69ed_15:
    goto addr_69f0_16;
}

struct s0* fun_2670(struct s0* rdi, struct s0* rsi, struct s0* rdx);

int64_t fun_6aa3(int64_t rdi, struct s0* rsi, struct s0* rdx) {
    struct s0* rax4;
    int32_t r13d5;
    struct s0* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2710(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<struct s0**>(&rsi->f0) = reinterpret_cast<struct s0*>(0);
        }
    } else {
        rax6 = fun_2560(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2670(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2670(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6b53() {
    __asm__("cli ");
    goto fun_2710;
}

struct s14 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2640(int64_t rdi, int64_t rsi, struct s0* rdx, struct s0* rcx, struct s0* r8, struct s0* r9);

void fun_6b63(int64_t rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx, struct s14* r8, struct s0* r9) {
    struct s0* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s0* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s0* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s0* r14_40;
    struct s0* r13_41;
    struct s0* r12_42;
    struct s0* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_27a0(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_27a0(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2540();
    fun_27a0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2640(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2540();
    fun_27a0(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2640(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2540();
        fun_27a0(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xb088 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xb088;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6fd3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s15 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6ff3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s15* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s15* rcx8;
    struct s0* rax9;
    struct s0* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2570();
    } else {
        return;
    }
}

void fun_7093(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s0* rax15;
    struct s0* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_7136_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7140_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2570();
    } else {
        return;
    }
    addr_7136_5:
    goto addr_7140_7;
}

void fun_7173() {
    int64_t rsi1;
    struct s0* rdx2;
    struct s0* rcx3;
    struct s0* r8_4;
    struct s0* r9_5;
    struct s0* rax6;
    struct s0* rcx7;
    struct s0* r8_8;
    struct s0* r9_9;
    int64_t v10;
    struct s0* v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    struct s0* v18;
    int64_t v19;
    struct s0* v20;
    struct s0* v21;
    struct s0* v22;
    struct s0* rax23;
    struct s0* r8_24;
    struct s0* r9_25;
    int64_t v26;
    struct s0* v27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    struct s0* v34;
    int64_t v35;
    struct s0* v36;
    struct s0* v37;
    struct s0* v38;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2640(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2540();
    fun_2720(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22);
    rax23 = fun_2540();
    fun_2720(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_24, r9_25, v26, __return_address(), v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
    fun_2540();
    goto fun_2720;
}

int64_t fun_24e0();

void fun_7213(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

struct s0* fun_26b0(struct s0* rdi, struct s0* rsi, ...);

void fun_7253(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7273(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7293(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

struct s0* fun_2700(struct s0* rdi, struct s0* rsi, ...);

void fun_72b3(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_2700(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_72e3(struct s0* rdi, uint64_t rsi) {
    uint64_t rax3;
    struct s0* rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2700(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7313(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7353() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7393(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_73c3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7413(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24e0();
            if (rax5) 
                break;
            addr_745d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_745d_5;
        rax8 = fun_24e0();
        if (rax8) 
            goto addr_7446_9;
        if (rbx4) 
            goto addr_745d_5;
        addr_7446_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_74a3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24e0();
            if (rax8) 
                break;
            addr_74ea_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_74ea_5;
        rax11 = fun_24e0();
        if (rax11) 
            goto addr_74d2_9;
        if (!rbx6) 
            goto addr_74d2_9;
        if (r12_4) 
            goto addr_74ea_5;
        addr_74d2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7533(struct s0* rdi, struct s0** rsi, struct s0* rdx, struct s0* rcx, uint64_t r8) {
    struct s0* r13_6;
    struct s0* rdi7;
    struct s0** r12_8;
    struct s0* rsi9;
    struct s0* rcx10;
    struct s0* rbx11;
    struct s0* rax12;
    struct s0* rbp13;
    void* rbp14;
    struct s0* rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<struct s0*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_75dd_9:
            rbx11 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_75f0_10:
                *r12_8 = reinterpret_cast<struct s0*>(0);
            }
            addr_7590_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_75b6_12;
            rcx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7604_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_75ad_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_7604_14;
            addr_75ad_16:
            rcx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7604_14;
            addr_75b6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2700(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7604_14;
            if (!rbp13) 
                break;
            addr_7604_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_75dd_9;
        } else {
            if (!r13_6) 
                goto addr_75f0_10;
            goto addr_7590_11;
        }
    }
}

int64_t fun_2620();

void fun_7633() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2620();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7663() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2620();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7693() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2620();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_76b3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2620();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_76d3(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2670;
    }
}

void fun_7713(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2670;
    }
}

void fun_7753(int64_t rdi, struct s0* rsi) {
    struct s0* rax3;

    __asm__("cli ");
    rax3 = fun_26b0(&rsi->f1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2670;
    }
}

void fun_7793(struct s0* rdi, struct s0* rsi) {
    struct s0* rax3;
    struct s0* rax4;

    __asm__("cli ");
    rax3 = fun_2560(rdi);
    rax4 = fun_26b0(&rax3->f1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2670;
    }
}

void fun_77d3() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2540();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2750();
    fun_2470(rdi1);
}

void rpl_vasprintf();

void fun_7813() {
    signed char al1;
    struct s0* rax2;
    void* rdx3;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    rpl_vasprintf();
    rdx3 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx3) {
        fun_2570();
    } else {
        return;
    }
}

struct s0* vasnprintf();

uint64_t fun_78d3(struct s0** rdi, int64_t rsi, int64_t rdx) {
    struct s0* rax4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t v7;
    int32_t* rax8;
    void* rdx9;

    __asm__("cli ");
    rax4 = g28;
    rax5 = vasnprintf();
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rax6 = v7;
        if (rax6 > 0x7fffffff) {
            fun_2450(rax5, rax5);
            rax8 = fun_2480();
            *rax8 = 75;
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            *rdi = rax5;
        }
    }
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_2570();
    } else {
        return rax6;
    }
}

int64_t fun_24c0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_7953(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_24c0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_79ae_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2480();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_79ae_3;
            rax6 = fun_2480();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s16 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_26a0(struct s16* rdi);

int32_t fun_26e0(struct s16* rdi);

int64_t fun_25d0(int64_t rdi, ...);

int32_t rpl_fflush(struct s16* rdi);

int64_t fun_2510(struct s16* rdi);

int64_t fun_79c3(struct s16* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_26a0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_26e0(rdi);
        if (!(eax3 && (eax4 = fun_26a0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_25d0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2480();
            r12d9 = *rax8;
            rax10 = fun_2510(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2510;
}

void rpl_fseeko(struct s16* rdi);

void fun_7a53(struct s16* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_26e0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_7aa3(struct s16* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_26a0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_25d0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_26d0(int64_t rdi);

signed char* fun_7b23() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_26d0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2590(uint32_t* rdi);

uint64_t fun_7b63(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s0* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    uint32_t eax8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2590(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (eax8 = hard_locale(), !*reinterpret_cast<signed char*>(&eax8)))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2570();
    } else {
        return r12_7;
    }
}

int32_t printf_parse(struct s0* rdi, void* rsi, struct s0* rdx);

int32_t printf_fetchargs(struct s0* rdi, struct s0* rsi, struct s0* rdx);

struct s17 {
    signed char[7] pad7;
    struct s0* f7;
};

struct s18 {
    struct s0* f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    struct s0* f18;
    signed char[7] pad32;
    struct s0* f20;
    signed char[7] pad40;
    int64_t f28;
    struct s0* f30;
    signed char[7] pad56;
    struct s0* f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    struct s0* f58;
};

struct s19 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

struct s0* fun_7bf3(struct s0* rdi, struct s0** rsi, struct s0* rdx, struct s0* rcx) {
    void* rsp5;
    void* rbp6;
    struct s0* r14_7;
    struct s0* r13_8;
    struct s0* r12_9;
    struct s0* v10;
    struct s0** v11;
    struct s0* rax12;
    struct s0* v13;
    int32_t eax14;
    void* rsp15;
    struct s0* r10_16;
    void* rax17;
    int64_t* rsp18;
    struct s0* rbx19;
    int64_t* rsp20;
    struct s0* rax21;
    struct s0* v22;
    int64_t* rsp23;
    struct s0* v24;
    int64_t* rsp25;
    struct s0* v26;
    int64_t* rsp27;
    struct s0* v28;
    int64_t* rsp29;
    int32_t* rax30;
    struct s0* r15_31;
    int32_t* v32;
    int64_t* rsp33;
    int64_t* rsp34;
    struct s0* v35;
    int64_t* rsp36;
    struct s0* v37;
    int64_t* rsp38;
    struct s0* rsi39;
    int32_t eax40;
    int32_t* rax41;
    struct s0* rax42;
    struct s17* v43;
    struct s0* tmp64_44;
    void* v45;
    struct s0* r8_46;
    struct s0* tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    struct s0* v53;
    struct s0* rax54;
    int32_t* rax55;
    struct s18* r14_56;
    struct s18* v57;
    struct s0* r9_58;
    struct s0* r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    struct s0* tmp64_63;
    struct s0* r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    struct s0* rax67;
    int64_t* rsp68;
    struct s0* rax69;
    int64_t* rsp70;
    struct s0* rax71;
    int64_t* rsp72;
    struct s0* rax73;
    int64_t* rsp74;
    struct s0* rax75;
    int64_t* rsp76;
    struct s0* rax77;
    struct s0* tmp64_78;
    int64_t* rsp79;
    struct s0* rax80;
    int64_t* rsp81;
    struct s0* rax82;
    int64_t* rsp83;
    struct s0* rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    struct s0* rsi89;
    struct s0* rax90;
    int64_t* rsp91;
    struct s0* rsi92;
    struct s0* rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s19* rcx98;
    int64_t rax99;
    struct s0* tmp64_100;
    struct s0* r15_101;
    int32_t* rax102;
    int64_t rax103;
    int64_t* rsp104;
    struct s0* rax105;
    int64_t* rsp106;
    int32_t* rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    struct s0* rax110;
    int64_t* rsp111;
    int32_t* rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0x8b2a;
                fun_2570();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_8b2a_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_8b34_6;
                addr_8a1e_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0x8a43, rax21 = fun_2700(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0x8a69;
                    fun_2450(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0x8a8f;
                    fun_2450(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0x8ab5;
                    fun_2450(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_8b34_6:
            addr_8718_16:
            v28 = r10_16;
            addr_871f_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0x8724;
            rax30 = fun_2480();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_8732_18:
            *v32 = 12;
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0x82d1;
                fun_2450(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0x82e9;
                fun_2450(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_7f28_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0x7f40;
                fun_2450(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0x7f58;
            fun_2450(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_2480();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *rax41 = 22;
        goto addr_7f28_23;
    }
    rax42 = reinterpret_cast<struct s0*>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_7f1d_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(tmp64_44) + 6);
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_7f1d_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax49 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(tmp64_44) + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 0x1000);
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<struct s0*>(0);
        v53 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_26b0(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_7f1d_33:
            rax55 = fun_2480();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *rax55 = 12;
            goto addr_7f28_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_7d1c_46;
    while (1) {
        addr_8674_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_7ddf_50;
            if (r14_56->f50 != -1) 
                goto 0x285d;
            r9_58 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = reinterpret_cast<struct s0*>(&r12_9->f1);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_864f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_8718_16;
                    rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_8718_16;
                if (r10_16 == v10) 
                    goto addr_8964_64; else 
                    goto addr_8623_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s18*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_8674_47;
            addr_7d1c_46:
            r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<struct s0*>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_87d0_73;
                    rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_87d0_73;
                if (r15_31 == v10) 
                    goto addr_8760_79; else 
                    goto addr_7d77_80;
            }
            addr_7d9c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0x7db2;
            fun_2670(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_8760_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0x8768;
            rax67 = fun_26b0(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_87d0_73;
            if (!r9_58) 
                goto addr_7d9c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0x87a7;
            rax69 = fun_2670(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_7d9c_81;
            addr_7d77_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0x7d82;
            rax71 = fun_2700(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_87d0_73; else 
                goto addr_7d9c_81;
            addr_8964_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0x897a;
            rax73 = fun_26b0(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_8b39_84;
            if (r12_9) 
                goto addr_899a_86;
            r10_16 = rax73;
            goto addr_864f_55;
            addr_899a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0x89af;
            rax75 = fun_2670(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_864f_55;
            addr_8623_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0x863c;
            rax77 = fun_2700(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_871f_17;
            r10_16 = rax77;
            goto addr_864f_55;
        }
        break;
    }
    tmp64_78 = reinterpret_cast<struct s0*>(&r12_9->f1);
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_8b2a_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_8a1e_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_8718_16;
        rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_8add_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_8add_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_8718_16; else 
                goto addr_8ae7_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_89f3_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0x8afe;
        rax80 = fun_26b0(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_8a1e_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0x8b1d;
                rax82 = fun_2670(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_8a1e_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0x8a12;
        rax84 = fun_2700(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_871f_17; else 
            goto addr_8a1e_7;
    }
    addr_8ae7_96:
    rbx19 = r13_8;
    goto addr_89f3_98;
    addr_7ddf_50:
    if (r14_56->f50 == -1) 
        goto 0x285d;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x2862;
        goto *reinterpret_cast<int32_t*>(0xb178 + r13_87 * 4) + 0xb178;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<struct s0**>(&v53->f0) = reinterpret_cast<struct s0*>(37);
    r13_8 = reinterpret_cast<struct s0*>(&v53->f1);
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        v53->f1 = 39;
        r13_8 = reinterpret_cast<struct s0*>(&v53->f2);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(45);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(43);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(32);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(35);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(73);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<struct s0**>(&r13_8->f0) = reinterpret_cast<struct s0*>(48);
        r13_8 = reinterpret_cast<struct s0*>(&r13_8->f1);
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0x7eaf;
        fun_2670(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0x7ee9;
        fun_2670(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0xb108 + rax95 * 4) + 0xb108;
    }
    eax96 = r14_56->f48;
    r13_8->f1 = 0;
    *reinterpret_cast<struct s0**>(&r13_8->f0) = *reinterpret_cast<struct s0**>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x285d;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x285d;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = reinterpret_cast<struct s0*>(&r12_9->f2);
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_8022_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_8718_16;
    }
    addr_8022_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_8718_16;
            rbx19 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_8043_142; else 
                goto addr_88d2_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_88d2_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_8718_16; else 
                    goto addr_88dc_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_8043_142;
            }
        }
    }
    addr_8075_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0x807f;
    rax102 = fun_2480();
    *rax102 = 0;
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x2862;
    goto *reinterpret_cast<int32_t*>(0xb130 + rax103 * 4) + 0xb130;
    addr_8043_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0x8938;
        rax105 = fun_26b0(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_8b39_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0x8b3e;
            rax107 = fun_2480();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_8732_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0x895f;
                fun_2670(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_8075_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0x8062;
        rax110 = fun_2700(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_8718_16; else 
            goto addr_8075_147;
    }
    addr_88dc_145:
    rbx19 = tmp64_100;
    goto addr_8043_142;
    addr_87d0_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0x87d5;
    rax112 = fun_2480();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_8732_18;
}

struct s20 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_8b73(int64_t rdi, struct s20* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_8ba9_5;
    return 0xffffffff;
    addr_8ba9_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb1a0 + rdx3 * 4) + 0xb1a0;
}

struct s21 {
    int64_t f0;
    struct s0* f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    struct s0* f20;
};

struct s22 {
    unsigned char f0;
    signed char[7] pad8;
    struct s0* f8;
    signed char[7] pad16;
    struct s0* f10;
};

struct s23 {
    unsigned char f0;
    signed char[1] pad2;
    struct s0* f2;
};

struct s24 {
    unsigned char f0;
    signed char[1] pad2;
    struct s0* f2;
};

int64_t fun_8da3(struct s0* rdi, struct s21* rsi, struct s22* rdx) {
    struct s0* r10_4;
    struct s0* rax5;
    struct s0* rdi6;
    struct s22* r15_7;
    struct s21* r14_8;
    struct s0* rcx9;
    unsigned char r9_10;
    struct s0* v11;
    unsigned char v12;
    uint32_t edx13;
    struct s0* rbx14;
    int64_t rax15;
    struct s0* r12_16;
    int64_t rbp17;
    int32_t edx18;
    struct s0* rdx19;
    int64_t rcx20;
    int32_t esi21;
    struct s0* rdx22;
    struct s23* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s23* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    struct s0* rdx38;
    uint32_t eax39;
    void* rax40;
    struct s0* rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    struct s0* rcx45;
    int64_t rsi46;
    int32_t eax47;
    unsigned char* rbx48;
    struct s0* rax49;
    int64_t rsi50;
    int32_t edi51;
    unsigned char rbp52;
    struct s0* rdx53;
    struct s0* r8_54;
    unsigned char rdx55;
    struct s0** rax56;
    struct s0** rcx57;
    unsigned char r9_58;
    struct s0** rbp59;
    struct s0* rsi60;
    struct s0* rax61;
    struct s0* rax62;
    unsigned char rdx63;
    struct s0* rax64;
    struct s23* rbx65;
    uint64_t rsi66;
    int32_t eax67;
    struct s23* rdx68;
    uint64_t rax69;
    uint64_t rcx70;
    uint64_t rcx71;
    uint64_t tmp64_72;
    int32_t eax73;
    struct s0* rax74;
    int64_t rdx75;
    int32_t edi76;
    unsigned char rbx77;
    unsigned char r9_78;
    unsigned char rdx79;
    struct s0** rax80;
    struct s0** rsi81;
    struct s0* rsi82;
    struct s0* rax83;
    struct s0* rdi84;
    struct s0* r8_85;
    struct s0* rdi86;
    unsigned char rdx87;
    struct s0* rax88;
    struct s0* rax89;
    int32_t* rax90;
    struct s0** rax91;
    struct s0* rdi92;
    int32_t* rax93;
    struct s24* rbx94;
    uint64_t rdi95;
    int32_t eax96;
    struct s24* rcx97;
    uint64_t rax98;
    uint64_t rdx99;
    uint64_t rdx100;
    uint64_t tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<struct s0*>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<struct s0*>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<unsigned char>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<unsigned char>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rax5->f0)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = reinterpret_cast<struct s0*>(&rax5->f1);
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_8e58_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<struct s0**>(&rcx9->f0) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_8e45_7:
    return rax15;
    addr_8e58_4:
    r12_16 = reinterpret_cast<struct s0*>(&rcx9->f0);
    *reinterpret_cast<struct s0**>(&r12_16->f0) = rax5;
    r12_16->f10 = 0;
    r12_16->f18 = reinterpret_cast<struct s0*>(0);
    r12_16->f20 = reinterpret_cast<struct s0*>(0);
    r12_16->f28 = reinterpret_cast<unsigned char>(0xffffffffffffffff);
    r12_16->f30 = reinterpret_cast<struct s0*>(0);
    r12_16->f38 = reinterpret_cast<struct s0*>(0);
    r12_16->f40 = reinterpret_cast<unsigned char>(0xffffffffffffffff);
    r12_16->f50 = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = rax5->f1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = rdx19->f1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            rdx19 = reinterpret_cast<struct s0*>(&rdx19->f1);
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_8ec9_11;
    } else {
        addr_8ec9_11:
        rdx22 = reinterpret_cast<struct s0*>(&rbx14->f1);
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                r12_16->f10 = r12_16->f10 | 1;
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx22->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = reinterpret_cast<struct s0*>(&rbx14->f1);
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_8ee0_14;
        } else {
            goto addr_8ee0_14;
        }
    }
    rax23 = reinterpret_cast<struct s23*>(&rax5->f2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s23*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_93a8_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s23*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s23*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_93a8_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<struct s0*>(&rcx26->f2);
    goto addr_8ec9_11;
    addr_8ee0_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb21c + rax33 * 4) + 0xb21c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        r12_16->f18 = rbx14;
        r12_16->f20 = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = rbx14->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_9027_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            r12_16->f18 = rbx14;
            eax37 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0)) - 48;
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_9729_36:
                r12_16->f20 = rbx14;
                goto addr_972e_37;
            } else {
                rdx38 = rbx14;
                do {
                    rdx38 = reinterpret_cast<struct s0*>(&rdx38->f1);
                    eax39 = rdx38->f1 - 48;
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_9724_42;
            }
        } else {
            addr_8f0d_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_9128_44:
                if (rbx14->f1 != 42) {
                    r12_16->f30 = rbx14;
                    rdx41 = reinterpret_cast<struct s0*>(&rbx14->f1);
                    eax42 = rbx14->f1 - 48;
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            rdx41 = reinterpret_cast<struct s0*>(&rdx41->f1);
                            eax44 = rdx41->f1 - 48;
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    r12_16->f38 = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_8f17_52;
                } else {
                    rcx45 = reinterpret_cast<struct s0*>(&rbx14->f2);
                    r12_16->f30 = rbx14;
                    r12_16->f38 = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = rbx14->f2;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_9914_56; else 
                        goto addr_9165_57;
                }
            } else {
                addr_8f17_52:
                rbx48 = &rbx14->f1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = *rbx48;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_8f38_60;
                } else {
                    goto addr_8f38_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = rax49->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        rax49 = reinterpret_cast<struct s0*>(&rax49->f1);
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_94ab_65;
    addr_9027_33:
    r12_16->f28 = reinterpret_cast<unsigned char>(0);
    if (0) 
        goto addr_93a8_22;
    rbp52 = reinterpret_cast<unsigned char>(0);
    v12 = reinterpret_cast<unsigned char>(1);
    rbx14 = rdx22;
    addr_904c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > rbp52) {
        addr_90ca_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<struct s0**>(reinterpret_cast<uint64_t>(rdx55 << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (rdx55 <= rbp52) {
            do {
                rdx55 = reinterpret_cast<unsigned char>(rdx55 + 1);
                *rax56 = reinterpret_cast<struct s0*>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (rdx55 <= rbp52);
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<struct s0*>(0);
        }
    } else {
        r9_58 = reinterpret_cast<unsigned char>(14);
        if (reinterpret_cast<unsigned char>(14) <= rbp52) {
            r9_58 = reinterpret_cast<unsigned char>(rbp52 + 1);
        }
        if (r9_58 >> 59) 
            goto addr_99cb_75; else 
            goto addr_9073_76;
    }
    rbp59 = reinterpret_cast<struct s0**>(reinterpret_cast<uint64_t>(rbp52 << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_93ac_80;
        }
    } else {
        *rbp59 = reinterpret_cast<struct s0*>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_8f17_52;
        goto addr_9128_44;
    }
    addr_972e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx14->f0));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_8f0d_43;
    addr_99cb_75:
    if (v11 != rdx53) {
        fun_2450(rdx53);
        r10_4 = r10_4;
        goto addr_97da_84;
    } else {
        goto addr_97da_84;
    }
    addr_9073_76:
    rsi60 = reinterpret_cast<struct s0*>(r9_58 << 5);
    if (v11 == rdx53) {
        rax61 = fun_26b0(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_2700(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_99cb_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_2670(r8_54, v11, rdx63 << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_90ca_68;
    addr_94ab_65:
    rbx65 = reinterpret_cast<struct s23*>(&rbx14->f2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s23*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (rsi66 > 0x1999999999999999) {
            rcx70 = 0xffffffffffffffff;
        } else {
            rcx71 = rsi66 + rsi66 * 4;
            rcx70 = rcx71 + rcx71;
        }
        while (tmp64_72 = rcx70 + rax69, rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), tmp64_72 < rcx70) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_93a8_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s23*>(&rbx65->pad2);
            rcx70 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s23*>(&rbx65->pad2);
    }
    rbp52 = reinterpret_cast<unsigned char>(tmp64_72 - 1);
    if (rbp52 > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_93a8_22;
    r12_16->f28 = rbp52;
    rbx14 = reinterpret_cast<struct s0*>(&rdx68->f2);
    goto addr_904c_67;
    label_41:
    addr_9724_42:
    goto addr_9729_36;
    addr_9914_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = rax74->f1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        rax74 = reinterpret_cast<struct s0*>(&rax74->f1);
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_993a_104;
    addr_9165_57:
    rbx77 = r12_16->f40;
    if (rbx77 == 0xffffffffffffffff) {
        r12_16->f40 = v12;
        if (0) {
            addr_93a8_22:
            r8_54 = r15_7->f8;
            goto addr_93ac_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_9174_107:
    r8_54 = r15_7->f8;
    if (r9_10 <= rbx77) {
        r9_78 = reinterpret_cast<unsigned char>(r9_10 + r9_10);
        if (r9_78 <= rbx77) {
            r9_78 = reinterpret_cast<unsigned char>(rbx77 + 1);
        }
        if (!(r9_78 >> 59)) 
            goto addr_9822_111;
    } else {
        addr_9181_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<struct s0**>(reinterpret_cast<uint64_t>(rdx79 << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (rdx79 <= rbx77) {
            do {
                rdx79 = reinterpret_cast<unsigned char>(rdx79 + 1);
                *rax80 = reinterpret_cast<struct s0*>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (rdx79 <= rbx77);
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<struct s0*>(0);
            goto addr_91b7_116;
        }
    }
    rdx53 = r8_54;
    goto addr_99cb_75;
    addr_9822_111:
    rsi82 = reinterpret_cast<struct s0*>(r9_78 << 5);
    if (v11 == r8_54) {
        rax83 = fun_26b0(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_97da_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_2450(rdi86);
            }
        } else {
            addr_9a60_120:
            rdx87 = r15_7->f0;
            rax88 = fun_2670(rdi84, r8_85, rdx87 << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_987f_121;
        }
    } else {
        rax89 = fun_2700(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_99cb_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_9a60_120;
            }
        }
    }
    rax90 = fun_2480();
    *rax90 = 12;
    return 0xffffffff;
    addr_987f_121:
    r15_7->f8 = r8_54;
    goto addr_9181_112;
    addr_91b7_116:
    rax91 = reinterpret_cast<struct s0**>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(rbx77 << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_93ac_80:
            if (v11 != r8_54) {
                fun_2450(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx45->f0));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_8f17_52;
        }
    } else {
        *rax91 = reinterpret_cast<struct s0*>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rcx45->f0));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_8f17_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_2450(rdi92, rdi92);
    }
    rax93 = fun_2480();
    *rax93 = 22;
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_8e45_7;
    addr_993a_104:
    rbx94 = reinterpret_cast<struct s24*>(&rbx14->f3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s24*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (rdi95 > 0x1999999999999999) {
            rdx99 = 0xffffffffffffffff;
        } else {
            rdx100 = rdi95 + rdi95 * 4;
            rdx99 = rdx100 + rdx100;
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = rdx99 + rax98, rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), tmp64_101 < rdx99) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_93a8_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s24*>(&rbx94->pad2);
            rdx99 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s24*>(&rbx94->pad2);
    }
    rbx77 = reinterpret_cast<unsigned char>(tmp64_101 + 0xffffffffffffffff);
    if (rbx77 > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_93a8_22;
    r12_16->f40 = rbx77;
    rcx45 = reinterpret_cast<struct s0*>(&rcx97->f2);
    goto addr_9174_107;
    addr_8f38_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xb380 + rax105 * 4) + 0xb380;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb2c4 + rax106 * 4) + 0xb2c4;
    }
}

void fun_9b33() {
    __asm__("cli ");
}

void fun_9b47() {
    __asm__("cli ");
    return;
}

void fun_2930() {
    include_mesg = 1;
    goto 0x28e8;
}

uint32_t fun_2600(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

struct s0* rpl_mbrtowc(void* rdi, struct s0* rsi);

int32_t fun_27e0(int64_t rdi, struct s0* rsi);

uint32_t fun_27d0(struct s0* rdi, struct s0* rsi);

void** fun_27f0(struct s0* rdi, struct s0* rsi, struct s0* rdx, struct s0* rcx);

void fun_4755() {
    struct s0** rsp1;
    int32_t ebp2;
    struct s0* rax3;
    struct s0** rsp4;
    struct s0* r11_5;
    struct s0* r11_6;
    struct s0* v7;
    int32_t ebp8;
    struct s0* rax9;
    struct s0* rdx10;
    struct s0* rax11;
    struct s0* r11_12;
    struct s0* v13;
    int32_t ebp14;
    struct s0* rax15;
    struct s0* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s0* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s0* v22;
    int32_t ebx23;
    struct s0* rax24;
    struct s0** rsp25;
    struct s0* v26;
    struct s0* r11_27;
    struct s0* v28;
    struct s0* v29;
    struct s0* rsi30;
    struct s0* v31;
    struct s0* v32;
    struct s0* r10_33;
    struct s0* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s0* r9_37;
    struct s0* v38;
    struct s0* rdi39;
    struct s0* v40;
    struct s0* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s0* rcx44;
    unsigned char al45;
    struct s0* v46;
    int64_t v47;
    struct s0* v48;
    struct s0* v49;
    struct s0* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s0* v58;
    unsigned char v59;
    struct s0* v60;
    struct s0* v61;
    struct s0* v62;
    signed char* v63;
    struct s0* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s0* r13_69;
    struct s0* rsi70;
    void* v71;
    struct s0* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s0* v107;
    struct s0* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s0**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2540();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2540();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2560(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s0*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s0*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s0**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4a53_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4a53_22; else 
                            goto addr_4e4d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4f0d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5260_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4a50_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4a50_30; else 
                                goto addr_5279_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2560(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5260_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2600(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5260_28; else 
                            goto addr_48fc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_53c0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5240_40:
                        if (r11_27 == 1) {
                            addr_4dcd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5388_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_4a07_44;
                            }
                        } else {
                            goto addr_5250_46;
                        }
                    } else {
                        addr_53cf_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_4dcd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4a53_22:
                                if (v47 != 1) {
                                    addr_4fa9_52:
                                    v48 = reinterpret_cast<struct s0*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2560(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4ff4_54;
                                    }
                                } else {
                                    goto addr_4a60_56;
                                }
                            } else {
                                addr_4a05_57:
                                ebp36 = 0;
                                goto addr_4a07_44;
                            }
                        } else {
                            addr_5234_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_53cf_47; else 
                                goto addr_523e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4dcd_41;
                        if (v47 == 1) 
                            goto addr_4a60_56; else 
                            goto addr_4fa9_52;
                    }
                }
                addr_4ac1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s0*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4958_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_497d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4c80_65;
                    } else {
                        addr_4ae9_66:
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5338_67;
                    }
                } else {
                    goto addr_4ae0_69;
                }
                addr_4991_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                addr_49dc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5338_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_49dc_81;
                }
                addr_4ae0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_497d_64; else 
                    goto addr_4ae9_66;
                addr_4a07_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_4abf_91;
                if (v22) 
                    goto addr_4a1f_93;
                addr_4abf_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4ac1_62;
                addr_4ff4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_577b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_57eb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rdx10->f0)) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_55ef_103;
                            rdx10 = reinterpret_cast<struct s0*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_27e0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_27d0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_50ee_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_4aac_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_50f8_112;
                    }
                } else {
                    addr_50f8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_51c9_114;
                }
                addr_4ab8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_4abf_91;
                while (1) {
                    addr_51c9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_56d7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_5136_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_56e5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_51b7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s0*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_51b7_128;
                        }
                    }
                    addr_5165_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_51b7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f1);
                    continue;
                    addr_5136_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s0*>(&r15_16->f3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_5165_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_49dc_81;
                addr_56e5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5338_67;
                addr_577b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_50ee_109;
                addr_57eb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_50ee_109;
                addr_4a60_56:
                rax93 = fun_27f0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_4aac_110;
                addr_523e_59:
                goto addr_5240_40;
                addr_4f0d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4a53_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_4ab8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_4a05_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4a53_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4f52_160;
                if (!v22) 
                    goto addr_5327_162; else 
                    goto addr_5533_163;
                addr_4f52_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5327_162:
                    r9_37 = reinterpret_cast<struct s0*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5338_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_4dfb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4c63_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4991_70; else 
                    goto addr_4c77_169;
                addr_4dfb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4958_63;
                goto addr_4ae0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5234_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_536f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4a50_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4948_178; else 
                        goto addr_52f2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5234_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4a53_22;
                }
                addr_536f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4a50_30:
                    r8d42 = 0;
                    goto addr_4a53_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_4ac1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5388_42;
                    }
                }
                addr_4948_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4958_63;
                addr_52f2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5250_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_4ac1_62;
                } else {
                    addr_5302_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4a53_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_5ab2_188;
                if (v28) 
                    goto addr_5327_162;
                addr_5ab2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4c63_168;
                addr_48fc_37:
                if (v22) 
                    goto addr_58f3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&rbx41->f0));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4913_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_53c0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_544b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4a53_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s0*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s0*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4948_178; else 
                        goto addr_5427_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5234_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4a53_22;
                }
                addr_544b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4a53_22;
                }
                addr_5427_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5250_46;
                goto addr_5302_186;
                addr_4913_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4a53_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4a53_22; else 
                    goto addr_4924_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s0*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_59fe_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5884_210;
            if (1) 
                goto addr_5882_212;
            if (!v29) 
                goto addr_54be_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2550();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s0*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s0*>("\"");
            if (!0) 
                goto addr_59f1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s0*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s0*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4c80_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_4a3b_219; else 
            goto addr_4c9a_220;
        addr_4a1f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4a33_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_4c9a_220; else 
            goto addr_4a3b_219;
        addr_55ef_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_4c9a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2550();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_560d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2550();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s0*>("'");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s0*>(0);
            continue;
        }
        addr_5a80_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_54e6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s0*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s0*>(0);
        v22 = reinterpret_cast<struct s0*>(0);
        v28 = reinterpret_cast<struct s0*>(1);
        v26 = reinterpret_cast<struct s0*>("'");
        continue;
        addr_56d7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4a33_221;
        addr_5533_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4a33_221;
        addr_4c77_169:
        goto addr_4c80_65;
        addr_59fe_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_4c9a_220;
        goto addr_560d_222;
        addr_5884_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<struct s0**>(&v26->f0)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s0*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_58de_236;
        fun_2570();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5a80_225;
        addr_5882_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5884_210;
        addr_54be_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5884_210;
        } else {
            rdx10 = reinterpret_cast<struct s0*>(0);
            goto addr_54e6_226;
        }
        addr_59f1_216:
        r13_34 = reinterpret_cast<struct s0*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s0*>("\"");
        v29 = reinterpret_cast<struct s0*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s0*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s0*>(1);
        v22 = reinterpret_cast<struct s0*>(0);
        v31 = reinterpret_cast<struct s0*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4e4d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xab4c + rax113 * 4) + 0xab4c;
    addr_5279_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xac4c + rax114 * 4) + 0xac4c;
    addr_58f3_190:
    addr_4a3b_219:
    goto 0x4720;
    addr_4924_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xaa4c + rax115 * 4) + 0xaa4c;
    addr_58de_236:
    goto v116;
}

void fun_4940() {
}

void fun_4af8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x47f2;
}

void fun_4b51() {
    goto 0x47f2;
}

void fun_4c3e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x4ac1;
    }
    if (v2) 
        goto 0x5533;
    if (!r10_3) 
        goto addr_569e_5;
    if (!v4) 
        goto addr_556e_7;
    addr_569e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_556e_7:
    goto 0x4974;
}

void fun_4c5c() {
}

void fun_4d07() {
    signed char v1;

    if (v1) {
        goto 0x4c8f;
    } else {
        goto 0x49ca;
    }
}

void fun_4d21() {
    signed char v1;

    if (!v1) 
        goto 0x4d1a; else 
        goto "???";
}

void fun_4d48() {
    goto 0x4c63;
}

void fun_4dc8() {
}

void fun_4de0() {
}

void fun_4e0f() {
    goto 0x4c63;
}

void fun_4e61() {
    goto 0x4df0;
}

void fun_4e90() {
    goto 0x4df0;
}

void fun_4ec3() {
    goto 0x4df0;
}

void fun_5290() {
    goto 0x4948;
}

void fun_558e() {
    signed char v1;

    if (v1) 
        goto 0x5533;
    goto 0x4974;
}

void fun_5635() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4974;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4958;
        goto 0x4974;
    }
}

void fun_5a52() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4cc0;
    } else {
        goto 0x47f2;
    }
}

void fun_6c38() {
    fun_2540();
}

int32_t fun_2440(int64_t rdi);

struct s25 {
    signed char[1] pad1;
    signed char f1;
};

struct s26 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_80e8() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    struct s0* rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    struct s0* rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    struct s0* rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    struct s0* rax30;
    struct s0* rcx31;
    void* rbx32;
    void* rbx33;
    struct s0* tmp64_34;
    void* r12_35;
    struct s0* rax36;
    struct s0* rbx37;
    int64_t r15_38;
    int64_t rbp39;
    struct s0* r15_40;
    struct s0* rax41;
    struct s0* rax42;
    int64_t r12_43;
    struct s0* r15_44;
    struct s0* r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s26* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_2440(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<struct s0**>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_8273_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_2440(rdi1);
            rsi10 = *reinterpret_cast<struct s0**>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_2440(rdi1);
            rsi10 = *reinterpret_cast<struct s0**>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_8273_5;
    }
    rcx19 = reinterpret_cast<struct s0*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x285d;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_817d_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0x8657;
        }
    } else {
        addr_8175_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_817d_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0x82c0;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0x8098;
        goto 0x8732;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0x8732;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_81b6_26;
    rax36 = rcx31;
    addr_81b6_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0x8098;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0x8732;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = fun_2700(r15_40, rax36);
        if (!rax41) 
            goto 0x8732;
        goto 0x8098;
    }
    rax42 = fun_26b0(rax36, rsi10);
    if (!rax42) 
        goto 0x8732;
    if (r12_43) 
        goto addr_857a_36;
    goto 0x8098;
    addr_857a_36:
    fun_2670(rax42, r15_44, r12_45);
    goto 0x8098;
    addr_8273_5:
    if ((*reinterpret_cast<struct s25**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s25**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0x8098;
    }
    if (eax7 >= 0) 
        goto addr_8175_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0x82c0;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_82b7_42;
    eax50 = 84;
    addr_82b7_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_8200() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_82f0() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x8435;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_2440(r15_4 + r12_5);
            goto 0x814d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x8123;
        }
    }
}

void fun_8338() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_2440(rdi5);
            goto 0x814d;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_2440(rdi5);
    goto 0x814d;
}

void fun_83a0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8226;
}

void fun_83f0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x83d0;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x822f;
}

void fun_84a0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8226;
    goto 0x83d0;
}

void fun_8533() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x7fa2;
}

void fun_86d0() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x8657;
}

struct s27 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s28 {
    signed char[8] pad8;
    int64_t f8;
};

struct s29 {
    signed char[16] pad16;
    int64_t f10;
};

struct s30 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8bb8() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s27* rcx3;
    struct s28* rcx4;
    int64_t r11_5;
    struct s29* rcx6;
    uint32_t* rcx7;
    struct s30* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x8ba0; else 
        goto "???";
}

struct s31 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s32 {
    signed char[8] pad8;
    int64_t f8;
};

struct s33 {
    signed char[16] pad16;
    int64_t f10;
};

struct s34 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8bf0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s31* rcx3;
    struct s32* rcx4;
    int64_t r11_5;
    struct s33* rcx6;
    uint32_t* rcx7;
    struct s34* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x8bd6;
}

struct s35 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s36 {
    signed char[8] pad8;
    int64_t f8;
};

struct s37 {
    signed char[16] pad16;
    int64_t f10;
};

struct s38 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_8c10() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s35* rcx3;
    struct s36* rcx4;
    int64_t r11_5;
    struct s37* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s38* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x8bd6;
}

struct s39 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s40 {
    signed char[8] pad8;
    int64_t f8;
};

struct s41 {
    signed char[16] pad16;
    int64_t f10;
};

struct s42 {
    signed char[16] pad16;
    signed char f10;
};

void fun_8c30() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s39* rcx3;
    struct s40* rcx4;
    int64_t r11_5;
    struct s41* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s42* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x8bd6;
}

struct s43 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s44 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_8cb0() {
    struct s43* rcx1;
    struct s44* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x8bd6;
}

struct s45 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s46 {
    signed char[8] pad8;
    int64_t f8;
};

struct s47 {
    signed char[16] pad16;
    int64_t f10;
};

struct s48 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8d00() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s45* rcx3;
    struct s46* rcx4;
    int64_t r11_5;
    struct s47* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s48* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x8bd6;
}

void fun_8f4c() {
}

void fun_8f7b() {
    goto 0x8f58;
}

void fun_8fd0() {
}

void fun_91e0() {
    goto 0x8fd3;
}

struct s49 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s50 {
    signed char[8] pad8;
    int64_t f8;
};

struct s51 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s52 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_9288() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    struct s0* rbp4;
    struct s49* r14_5;
    struct s0* rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    struct s0* r10_10;
    struct s0* rax11;
    struct s0* rcx12;
    int64_t v13;
    struct s50* r15_14;
    struct s0* rax15;
    struct s51* r14_16;
    struct s0* r10_17;
    int64_t r13_18;
    struct s0* rax19;
    struct s52* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x99c7;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x99c7;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<struct s0*>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_26b0(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x97e8; else 
                goto "???";
        }
    } else {
        rax15 = fun_2700(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x99c7;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_9b1a_9; else 
            goto addr_92fe_10;
    }
    addr_9454_11:
    rax19 = fun_2670(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_92fe_10:
    r14_20->f8 = rcx12;
    goto 0x8e19;
    addr_9b1a_9:
    r13_18 = *r14_21;
    goto addr_9454_11;
}

struct s53 {
    signed char[11] pad11;
    struct s0* fb;
};

struct s54 {
    signed char[80] pad80;
    int64_t f50;
};

struct s55 {
    signed char[80] pad80;
    int64_t f50;
};

struct s56 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s57 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s58 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s59 {
    signed char[72] pad72;
    signed char f48;
};

struct s60 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_9511() {
    struct s0* ecx1;
    int32_t edx2;
    struct s53* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s54* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s55* r12_10;
    int64_t r13_11;
    struct s0* r8_12;
    struct s56* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    struct s0* rsi18;
    struct s0* v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    struct s0** rax22;
    struct s0** rsi23;
    uint64_t* r15_24;
    struct s0* rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    struct s0* rdi28;
    struct s0* r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    struct s0* rax32;
    struct s57* r15_33;
    struct s0* rax34;
    uint64_t r11_35;
    struct s0* v36;
    struct s58* r15_37;
    struct s0** r13_38;
    struct s59* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s60* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<struct s0*>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s53*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<struct s0*>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x93a8;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x9afc;
        rsi18 = reinterpret_cast<struct s0*>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_966d_12;
    } else {
        addr_9214_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<struct s0**>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<struct s0*>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<struct s0*>(0);
            goto addr_924f_17;
        }
    }
    rax25 = fun_26b0(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x97da;
    addr_9780_19:
    rdx30 = *r15_31;
    rax32 = fun_2670(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_96b6_20:
    r15_33->f8 = r8_12;
    goto addr_9214_13;
    addr_966d_12:
    rax34 = fun_2700(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x99c7;
    if (v36 != r15_37->f8) 
        goto addr_96b6_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_9780_19;
    addr_924f_17:
    r13_38 = reinterpret_cast<struct s0**>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x93ac;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x9290;
    goto 0x8e19;
}

void fun_952f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x91f8;
    if (dl2 & 4) 
        goto 0x91f8;
    if (edx3 > 7) 
        goto 0x91f8;
    if (dl4 & 2) 
        goto 0x91f8;
    goto 0x91f8;
}

void fun_9578() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x91f8;
    if (dl2 & 4) 
        goto 0x91f8;
    if (edx3 > 7) 
        goto 0x91f8;
    if (dl4 & 2) 
        goto 0x91f8;
    goto 0x91f8;
}

void fun_95c0() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x91f8;
    if (dl2 & 4) 
        goto 0x91f8;
    if (edx3 > 7) 
        goto 0x91f8;
    if (dl4 & 2) 
        goto 0x91f8;
    goto 0x91f8;
}

void fun_9608() {
    goto 0x91f8;
}

void fun_2940() {
    include_heading = 1;
    goto 0x28e8;
}

void fun_4b7e() {
    goto 0x47f2;
}

void fun_4d54() {
    goto 0x4d0c;
}

void fun_4e1b() {
    goto 0x4948;
}

void fun_4e6d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4df0;
    goto 0x4a1f;
}

void fun_4e9f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4dfb;
        goto 0x4820;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4c9a;
        goto 0x4a3b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5638;
    if (r10_8 > r15_9) 
        goto addr_4d85_9;
    addr_4d8a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5643;
    goto 0x4974;
    addr_4d85_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4d8a_10;
}

void fun_4ed2() {
    goto 0x4a07;
}

void fun_52a0() {
    goto 0x4a07;
}

void fun_5a3f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x4b5c;
    } else {
        goto 0x4cc0;
    }
}

void fun_6cf0() {
}

struct s61 {
    signed char[1] pad1;
    signed char f1;
};

void fun_7f90() {
    signed char* r13_1;
    struct s61* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_86de() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x8657;
}

struct s62 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s63 {
    signed char[8] pad8;
    int64_t f8;
};

struct s64 {
    signed char[16] pad16;
    int64_t f10;
};

struct s65 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8cd0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s62* rcx3;
    struct s63* rcx4;
    int64_t r11_5;
    struct s64* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s65* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x8bd6;
}

void fun_8f85() {
    goto 0x8f58;
}

void fun_98d4() {
    goto 0x91f8;
}

void fun_91e8() {
}

void fun_9618() {
    goto 0x91f8;
}

void fun_2950() {
    do_lookup = 1;
    goto 0x28e8;
}

void fun_4edc() {
    goto 0x4e77;
}

void fun_52aa() {
    goto 0x4dcd;
}

void fun_6d50() {
    fun_2540();
    goto fun_27a0;
}

void fun_8470() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x8226;
    goto 0x83d0;
}

void fun_86ec() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x8657;
}

struct s66 {
    int32_t f0;
    int32_t f4;
};

struct s67 {
    int32_t f0;
    int32_t f4;
};

struct s68 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s69 {
    signed char[8] pad8;
    int64_t f8;
};

struct s70 {
    signed char[8] pad8;
    int64_t f8;
};

struct s71 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_8c80(struct s66* rdi, struct s67* rsi) {
    struct s68* rcx3;
    struct s69* rcx4;
    struct s70* rcx5;
    struct s71* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x8bd6;
}

void fun_8f8f() {
    goto 0x8f58;
}

void fun_98de() {
    goto 0x91f8;
}

void fun_2960() {
    need_users = 1;
    include_idle = 1;
    goto 0x28e8;
}

void fun_4bad() {
    goto 0x47f2;
}

void fun_4ee8() {
    goto 0x4e77;
}

void fun_52b7() {
    goto 0x4e1e;
}

void fun_6d90() {
    fun_2540();
    goto fun_27a0;
}

void fun_86fb() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x8657;
}

void fun_8f99() {
    goto 0x8f58;
}

void fun_2980() {
    need_clockchange = 1;
    goto 0x28e8;
}

void fun_4bda() {
    goto 0x47f2;
}

void fun_4ef4() {
    goto 0x4df0;
}

void fun_6dd0() {
    fun_2540();
    goto fun_27a0;
}

void fun_8fa3() {
    goto 0x8f58;
}

void fun_2990() {
    short_output = 1;
    goto 0x28e8;
}

void fun_4bfc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5590;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x4ac1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x4ac1;
    }
    if (v11) 
        goto 0x58f3;
    if (r10_12 > r15_13) 
        goto addr_5943_8;
    addr_5948_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5681;
    addr_5943_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5948_9;
}

struct s72 {
    signed char[24] pad24;
    int64_t f18;
};

struct s73 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s74 {
    signed char[8] pad8;
    struct s0* f8;
};

void fun_6e20() {
    int64_t r15_1;
    struct s72* rbx2;
    struct s0* r14_3;
    struct s73* rbx4;
    struct s0* r13_5;
    struct s74* rbx6;
    struct s0* r12_7;
    struct s0** rbx8;
    struct s0* rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2540();
    fun_27a0(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x6e42, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_29a0() {
    need_runlevel = 1;
    include_idle = 1;
    goto 0x28e8;
}

void fun_6e78() {
    fun_2540();
    goto 0x6e49;
}

void fun_29c0() {
    short_list = 1;
    goto 0x28e8;
}

struct s75 {
    signed char[32] pad32;
    int64_t f20;
};

struct s76 {
    signed char[24] pad24;
    int64_t f18;
};

struct s77 {
    signed char[16] pad16;
    struct s0* f10;
};

struct s78 {
    signed char[8] pad8;
    struct s0* f8;
};

struct s79 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_6eb0() {
    int64_t rcx1;
    struct s75* rbx2;
    int64_t r15_3;
    struct s76* rbx4;
    struct s0* r14_5;
    struct s77* rbx6;
    struct s0* r13_7;
    struct s78* rbx8;
    struct s0* r12_9;
    struct s0** rbx10;
    int64_t v11;
    struct s79* rbx12;
    struct s0* rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2540();
    fun_27a0(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x6ee4, __return_address(), rcx1);
    goto v15;
}

void fun_29d0() {
    need_initspawn = 1;
    goto 0x28e8;
}

void fun_6f28() {
    fun_2540();
    goto 0x6eeb;
}

void fun_29e0() {
    my_line_only = 1;
    goto 0x28e8;
}

void fun_29f0() {
    need_login = 1;
    include_idle = 1;
    goto 0x28e8;
}

void fun_2a10() {
    need_deadprocs = 1;
    include_idle = 1;
    include_exit = 1;
    goto 0x28e8;
}

void fun_2a30() {
    need_boottime = 1;
    goto 0x28e8;
}

void fun_2a40() {
    need_boottime = 1;
    need_deadprocs = 1;
    need_login = 1;
    need_initspawn = 1;
    need_runlevel = 1;
    need_clockchange = 1;
    need_users = 1;
    include_mesg = 1;
    include_idle = 1;
    include_exit = 1;
    goto 0x28e8;
}
