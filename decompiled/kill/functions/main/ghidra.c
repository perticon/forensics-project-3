undefined8 main(int param_1,char **param_2)

{
  uint uVar1;
  int iVar2;
  int iVar3;
  undefined8 uVar4;
  long lVar5;
  int *piVar6;
  size_t sVar7;
  undefined8 uVar8;
  undefined8 uVar9;
  intmax_t iVar10;
  undefined *puVar11;
  char **ppcVar12;
  char *pcVar13;
  undefined *puVar14;
  long in_FS_OFFSET;
  undefined4 local_a4;
  char *local_80;
  undefined local_78 [32];
  char local_58 [24];
  long local_40;
  
  iVar2 = -1;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar14 = &DAT_00107640;
  textdomain("coreutils");
  atexit(close_stdout);
  local_a4 = 0;
  do {
    ppcVar12 = param_2;
    uVar1 = getopt_long(param_1,param_2,
                        "0::1::2::3::4::5::6::7::8::9::A::B::C::D::E::F::G::H::I::J::K::M::N::O::P::Q::R::S::T::U::V::W::X::Y::Z::Lln:s:t"
                        ,long_options,0);
    puVar11 = (undefined *)(ulong)uVar1;
    if (uVar1 == 0xffffffff) goto LAB_00102845;
    if ((int)uVar1 < 0x75) {
      if ((int)uVar1 < 0x30) {
        if (uVar1 == 0xffffff7d) {
          version_etc(stdout,&DAT_00107004,"GNU coreutils",Version,"Paul Eggert",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        if (uVar1 == 0xffffff7e) {
          usage(0);
          goto LAB_0010291f;
        }
      }
      else if (uVar1 - 0x30 < 0x45) {
                    /* WARNING: Could not recover jumptable at 0x001027cc. Too many branches */
                    /* WARNING: Treating indirect jump as call */
        uVar4 = (*(code *)(puVar14 + *(int *)(puVar14 + (ulong)(uVar1 - 0x30) * 4)))();
        return uVar4;
      }
    }
    do {
      while( true ) {
        usage();
        uVar1 = (uint)puVar11;
        if (optind == 2) break;
        optind = optind + -1;
LAB_00102845:
        lVar5 = (long)optind;
        if (iVar2 == -1) {
          if ((char)local_a4 != '\0') {
            if (optind < param_1) {
              uVar4 = 0;
              for (param_2 = param_2 + lVar5; *param_2 != (char *)0x0; param_2 = param_2 + 1) {
                iVar2 = operand2sig(*param_2,local_58);
                if (iVar2 < 0) {
                  uVar4 = 1;
                }
                else if ((int)**param_2 - 0x30U < 10) {
                  puts(local_58);
                }
                else {
                  __printf_chk(1,&DAT_0010710c,iVar2);
                }
              }
            }
            else {
              iVar2 = 1;
              do {
                iVar3 = sig2str(iVar2,local_58);
                if (iVar3 == 0) {
                  puts(local_58);
                }
                iVar2 = iVar2 + 1;
              } while (iVar2 != 0x41);
              uVar4 = 0;
            }
            goto LAB_001029d8;
          }
LAB_00102c91:
          iVar2 = 0xf;
LAB_0010286c:
          pcVar13 = "no process ID specified";
          if ((int)lVar5 < param_1) {
            param_2 = param_2 + lVar5;
            uVar4 = 0;
            piVar6 = __errno_location();
            pcVar13 = *param_2;
            do {
              *piVar6 = 0;
              iVar10 = strtoimax(pcVar13,&local_80,10);
              if ((((*piVar6 == 0x22) || (iVar10 != (int)iVar10)) || (pcVar13 == local_80)) ||
                 (*local_80 != '\0')) {
                uVar8 = quote(pcVar13);
                uVar9 = dcgettext(0,"%s: invalid process id",5);
                uVar4 = 1;
                error(0,0,uVar9,uVar8);
              }
              else {
                iVar3 = kill((int)iVar10,iVar2);
                if (iVar3 != 0) {
                  uVar4 = 1;
                  uVar8 = quote(pcVar13);
                  error(0,*piVar6,"%s",uVar8);
                }
              }
              pcVar13 = param_2[1];
              param_2 = param_2 + 1;
            } while (pcVar13 != (char *)0x0);
LAB_001029d8:
            if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
              __stack_chk_fail();
            }
            return uVar4;
          }
        }
        else {
          pcVar13 = "cannot combine signal with -l or -t";
          if ((char)local_a4 == '\0') goto LAB_0010286c;
        }
LAB_00102c44:
        uVar4 = dcgettext(0,pcVar13,5);
        error(0,0,uVar4);
      }
      ppcVar12 = param_2 + (long)optind + -1;
      if (optarg == (char *)0x0) {
LAB_0010291f:
        pcVar13 = *ppcVar12;
        sVar7 = strlen(pcVar13);
        puVar11 = (undefined *)(ulong)uVar1;
        optarg = pcVar13 + sVar7;
      }
      if (optarg != *ppcVar12 + 2) {
        local_a4 = SUB84(puVar11,0);
        uVar4 = dcgettext(0,"invalid option -- %c",5);
        puVar11 = (undefined *)((ulong)puVar11 & 0xffffffff);
        error(0,0,uVar4);
        lVar5 = usage(1);
        goto LAB_00102c91;
      }
      optarg = *ppcVar12 + 1;
      if (iVar2 != -1) {
        puVar14 = (undefined *)quote();
        uVar4 = dcgettext(0,"%s: multiple signals specified",5);
        puVar11 = puVar14;
        error(0,0,uVar4);
        usage(1);
        pcVar13 = "multiple -l or -t options specified";
        goto LAB_00102c44;
      }
      iVar2 = operand2sig(optarg,local_78);
    } while (iVar2 < 0);
  } while( true );
}