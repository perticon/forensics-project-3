int main(int param_1,undefined8 *param_2)

{
  char **__argv;
  bool bVar1;
  bool bVar2;
  byte bVar3;
  byte bVar4;
  int iVar5;
  uint uVar6;
  uint uVar7;
  int iVar8;
  int iVar9;
  char *pcVar10;
  undefined8 uVar11;
  int *piVar12;
  char *pcVar13;
  int iVar14;
  bool bVar15;
  undefined8 uVar16;
  __mode_t local_54;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  pcVar10 = getenv("POSIXLY_CORRECT");
  iVar14 = (-(uint)(pcVar10 == (char *)0x0) & 0xfffffffe) + 0x7f;
  uVar16 = 0x10277c;
  exit_failure = iVar14;
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,"nohup","GNU coreutils",Version,0,usage,"Jim Meyering",0,uVar16);
  if (param_1 <= optind) {
    uVar16 = dcgettext(0,"missing operand",5);
    error(0,0,uVar16);
                    /* WARNING: Subroutine does not return */
    usage(iVar14);
  }
  iVar5 = isatty(0);
  uVar6 = isatty(1);
  if ((uVar6 == 0) && (piVar12 = __errno_location(), *piVar12 == 9)) {
    bVar3 = 1;
    uVar7 = isatty(2);
    bVar1 = true;
    bVar15 = uVar7 != 0;
    bVar4 = 1;
    if (iVar5 == 0) {
LAB_00102a38:
      bVar2 = (bool)(bVar3 & bVar15);
      if (bVar2) {
        local_54 = umask(0xfffffe7f);
        iVar8 = open("nohup.out",0x441,0x180);
        goto LAB_00102843;
      }
LAB_00102972:
      if (bVar15) {
        iVar9 = rpl_fcntl(2,0x406,3);
        iVar8 = 1;
        goto LAB_0010299a;
      }
      goto LAB_001028c2;
    }
LAB_001029ff:
    bVar3 = bVar4;
    iVar8 = fd_reopen(0,"/dev/null",1,0);
    if (iVar8 < 0) {
      uVar16 = dcgettext(0,"failed to render standard input unusable",5);
      piVar12 = __errno_location();
      error(iVar14,*piVar12,uVar16);
    }
    if ((uVar7 | uVar6) != 0) {
      bVar2 = bVar15;
      if (uVar6 != 0) goto LAB_0010280f;
      goto LAB_00102a38;
    }
    iVar9 = 2;
    uVar16 = dcgettext(0,"ignoring input",5);
    error(0,0,uVar16);
  }
  else {
    uVar7 = isatty(2);
    bVar15 = uVar7 != 0;
    if (iVar5 != 0) {
      bVar1 = false;
      bVar4 = 0;
      goto LAB_001029ff;
    }
    bVar1 = false;
    bVar2 = bVar15;
    if (uVar6 == 0) goto LAB_00102972;
LAB_0010280f:
    local_54 = umask(0xfffffe7f);
    iVar8 = fd_reopen(1,"nohup.out",0x441,0x180);
LAB_00102843:
    pcVar13 = "nohup.out";
    pcVar10 = (char *)0x0;
    if (iVar8 < 0) {
      piVar12 = __errno_location();
      iVar9 = *piVar12;
      pcVar10 = getenv("HOME");
      if (pcVar10 == (char *)0x0) {
        uVar16 = quotearg_style(4,"nohup.out");
        uVar11 = dcgettext(0,"failed to open %s",5);
        error(0,iVar9,uVar11,uVar16);
        return iVar14;
      }
      pcVar13 = (char *)file_name_concat(pcVar10,"nohup.out",0);
      if (uVar6 == 0) {
        iVar8 = open(pcVar13,0x441,0x180);
      }
      else {
        iVar8 = fd_reopen(1,pcVar13,0x441,0x180);
      }
      pcVar10 = pcVar13;
      if (iVar8 < 0) {
        pcVar10 = "failed to open %s";
        iVar5 = *piVar12;
        uVar16 = quotearg_style(4,"nohup.out");
        uVar11 = dcgettext(0,"failed to open %s",5);
        error(0,iVar9,uVar11,uVar16);
        uVar16 = quotearg_style(4,pcVar13);
        goto LAB_00102b54;
      }
    }
    umask(local_54);
    uVar16 = quotearg_style(4,pcVar13);
    pcVar13 = "ignoring input and appending output to %s";
    if (iVar5 == 0) {
      pcVar13 = "appending output to %s";
    }
    uVar11 = dcgettext(0,pcVar13,5);
    error(0,0,uVar11,uVar16);
    free(pcVar10);
    if (bVar2) {
      iVar9 = rpl_fcntl(2,0x406,3);
      if (uVar6 == 0) {
LAB_0010299a:
        pcVar10 = "ignoring input and redirecting stderr to stdout";
        if (iVar5 == 0) {
          pcVar10 = "redirecting stderr to stdout";
        }
        uVar16 = dcgettext(0,pcVar10,5);
        error(0,0,uVar16);
      }
      iVar5 = dup2(iVar8,2);
      if (iVar5 < 0) {
        uVar16 = dcgettext(0,"failed to redirect standard error",5);
        piVar12 = __errno_location();
        error(iVar14,*piVar12,uVar16);
      }
      if (bVar1) {
        close(iVar8);
      }
    }
    else {
LAB_001028c2:
      iVar9 = 2;
    }
  }
  if ((*stderr & 0x20) != 0) {
    return iVar14;
  }
  signal(1,(__sighandler_t)0x1);
  __argv = (char **)(param_2 + optind);
  execvp(*__argv,__argv);
  piVar12 = __errno_location();
  iVar5 = *piVar12;
  iVar14 = (iVar5 == 2) + 0x7e;
  iVar8 = dup2(iVar9,2);
  if (iVar8 != 2) {
    return iVar14;
  }
  uVar16 = quotearg_style(4,*__argv);
  pcVar10 = "failed to run command %s";
LAB_00102b54:
  uVar11 = dcgettext(0,pcVar10,5);
  error(0,iVar5,uVar11,uVar16);
  return iVar14;
}