void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002680(fn0000000000002450(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002E1E;
	}
	fn00000000000025F0(fn0000000000002450(0x05, "Usage: %s COMMAND [ARG]...\n  or:  %s OPTION\n", null), 0x01);
	fn0000000000002510(stdout, fn0000000000002450(0x05, "Run COMMAND, ignoring hangup signals.\n\n", null));
	fn0000000000002510(stdout, fn0000000000002450(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002510(stdout, fn0000000000002450(0x05, "      --version     output version information and exit\n", null));
	fn00000000000025F0(fn0000000000002450(0x05, "\nIf standard input is a terminal, redirect it from an unreadable file.\nIf standard output is a terminal, append output to 'nohup.out' if possible,\n'$HOME/nohup.out' otherwise.\nIf standard error is a terminal, redirect it to standard output.\nTo save output to FILE, use '%s COMMAND > FILE'.\n", null), 0x01);
	fn00000000000025F0(fn0000000000002450(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	struct Eq_972 * rbx_179 = fp - 0xB8 + 16;
	do
	{
		char * rsi_181 = rbx_179->qw0000;
		++rbx_179;
	} while (rsi_181 != null && fn0000000000002530(rsi_181, "nohup") != 0x00);
	ptr64 r13_194 = rbx_179->qw0008;
	if (r13_194 != 0x00)
	{
		fn00000000000025F0(fn0000000000002450(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_17 rax_281 = fn00000000000025E0(null, 0x05);
		if (rax_281 == 0x00 || fn00000000000023C0(0x03, "en_", rax_281) == 0x00)
			goto l0000000000003026;
	}
	else
	{
		fn00000000000025F0(fn0000000000002450(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_17 rax_223 = fn00000000000025E0(null, 0x05);
		if (rax_223 == 0x00 || fn00000000000023C0(0x03, "en_", rax_223) == 0x00)
		{
			fn00000000000025F0(fn0000000000002450(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003063:
			fn00000000000025F0(fn0000000000002450(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002E1E:
			fn0000000000002660(edi);
		}
		r13_194 = 0x7004;
	}
	fn0000000000002510(stdout, fn0000000000002450(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003026:
	fn00000000000025F0(fn0000000000002450(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003063;
}