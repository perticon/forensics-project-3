
struct s0 {
    struct s0* f0;
    struct s0* f8;
    signed char[8] pad24;
    struct s0* f18;
};

int64_t fun_2460();

int64_t fun_23a0(struct s0* rdi, ...);

struct s0* quotearg_buffer_restyled(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, struct s0* a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2460();
    if (r8d > 10) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x76a0 + rax11 * 4) + 0x76a0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[24] pad40;
    int64_t f28;
    int64_t f30;
};

struct s2 {
    unsigned char f0;
    signed char f1;
    signed char f2;
};

struct s2* g28;

int32_t* fun_23b0();

struct s0* slotvec = reinterpret_cast<struct s0*>(0xb070);

uint32_t nslots = 1;

struct s0* xpalloc();

void fun_24e0();

struct s3 {
    struct s0* f0;
    struct s0* f8;
};

void fun_2390(struct s0* rdi, ...);

struct s0* xcharalloc(struct s0* rdi, ...);

void fun_2480();

struct s0* quotearg_n_options(struct s0* rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    struct s2* rax6;
    int64_t v7;
    int32_t* rax8;
    struct s0* r15_9;
    int32_t v10;
    uint32_t eax11;
    struct s0* rax12;
    struct s0* rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s3* rbx16;
    uint32_t r15d17;
    struct s0* rsi18;
    struct s0* r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    struct s0* rax23;
    struct s0* rsi24;
    struct s0* rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x4e8f;
    rax8 = fun_23b0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
        fun_23a0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xb070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6041]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x4f1b;
            fun_24e0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<uint64_t>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<uint64_t>(rsi18) <= reinterpret_cast<uint64_t>(rax23)) {
            rsi24 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax23) + 1);
            rbx16->f0 = rsi24;
            if (r14_19 != 0xb100) {
                fun_2390(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x4faa);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2480();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xb080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
struct s2* gettext_quote_part_0(struct s2* rdi, int32_t esi, struct s2* rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    struct s2* rax7;
    uint32_t edx8;
    uint32_t edx9;
    struct s2* rax10;
    struct s2* rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<struct s2*>(0x7633);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax7 = reinterpret_cast<struct s2*>(0x762c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<struct s2*>(0x7637);
            if (*reinterpret_cast<unsigned char*>(&rdi->f0) != 96) {
                rax10 = reinterpret_cast<struct s2*>(0x7628);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<struct s2*>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<struct s2*>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gae18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gae18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2373() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_2383() {
    __asm__("cli ");
    goto getenv;
}

int64_t free = 0x2040;

void fun_2393() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_23a3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_23b3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_23c3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_23d3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2090;

void fun_23e3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t isatty = 0x20a0;

void fun_23f3() {
    __asm__("cli ");
    goto isatty;
}

int64_t reallocarray = 0x20b0;

void fun_2403() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20c0;

void fun_2413() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x20d0;

void fun_2423() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20e0;

void fun_2433() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20f0;

void fun_2443() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2100;

void fun_2453() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2110;

void fun_2463() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2120;

void fun_2473() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2130;

void fun_2483() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2140;

void fun_2493() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2150;

void fun_24a3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x2160;

void fun_24b3() {
    __asm__("cli ");
    goto dup2;
}

int64_t strrchr = 0x2170;

void fun_24c3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2180;

void fun_24d3() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2190;

void fun_24e3() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x21a0;

void fun_24f3() {
    __asm__("cli ");
    goto close;
}

int64_t memcmp = 0x21b0;

void fun_2503() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21c0;

void fun_2513() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21d0;

void fun_2523() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21e0;

void fun_2533() {
    __asm__("cli ");
    goto strcmp;
}

int64_t signal = 0x21f0;

void fun_2543() {
    __asm__("cli ");
    goto signal;
}

int64_t fputc_unlocked = 0x2200;

void fun_2553() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t umask = 0x2210;

void fun_2563() {
    __asm__("cli ");
    goto umask;
}

int64_t memcpy = 0x2220;

void fun_2573() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_2583() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_2593() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_25a3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_25b3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25c3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2280;

void fun_25d3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2290;

void fun_25e3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22a0;

void fun_25f3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x22b0;

void fun_2603() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t error = 0x22c0;

void fun_2613() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x22d0;

void fun_2623() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x22e0;

void fun_2633() {
    __asm__("cli ");
    goto fseeko;
}

int64_t execvp = 0x22f0;

void fun_2643() {
    __asm__("cli ");
    goto execvp;
}

int64_t __cxa_atexit = 0x2300;

void fun_2653() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2310;

void fun_2663() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2320;

void fun_2673() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2330;

void fun_2683() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2340;

void fun_2693() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2350;

void fun_26a3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2360;

void fun_26b3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(struct s0* rdi);

struct s2* fun_25e0(int64_t rdi, ...);

void fun_2440(int64_t rdi, struct s0* rsi);

void fun_2420(int64_t rdi, struct s0* rsi);

uint64_t fun_2380(int64_t rdi, struct s0* rsi, struct s2* rdx, int64_t rcx);

uint32_t exit_failure = 1;

void atexit(int64_t rdi, struct s0* rsi);

int64_t Version = 0x75b2;

void parse_gnu_standard_options_only(int64_t rdi, struct s0* rsi, struct s2* rdx, int64_t rcx, int64_t r8);

int32_t optind = 0;

struct s2* fun_2450();

void fun_2610();

void usage(int64_t rdi);

uint32_t fun_23f0();

int32_t fd_reopen();

int32_t fun_2560(int64_t rdi, struct s0* rsi, struct s2* rdx, int64_t rcx);

int32_t fun_2620(struct s0* rdi, int64_t rsi, struct s2* rdx, int64_t rcx);

unsigned char* stderr = reinterpret_cast<unsigned char*>(0);

void fun_2540(int64_t rdi, int64_t rsi, struct s2* rdx, int64_t rcx);

void fun_2640(struct s0* rdi, struct s0** rsi, struct s2* rdx, int64_t rcx);

int32_t fun_24b0(int64_t rdi, int64_t rsi, struct s2* rdx, int64_t rcx);

int64_t quotearg_style(int64_t rdi, struct s0* rsi, struct s2* rdx, int64_t rcx);

struct s0* file_name_concat(uint64_t rdi, int64_t rsi);

int32_t rpl_fcntl(int64_t rdi, int64_t rsi, struct s2* rdx, int64_t rcx);

void fun_24f0(int64_t rdi, int64_t rsi, struct s2* rdx, int64_t rcx);

int64_t fun_2703(int32_t edi, struct s0* rsi, struct s2* rdx, int64_t rcx) {
    struct s0* rdi5;
    uint64_t rax6;
    uint32_t r12d7;
    struct s0* rsi8;
    int64_t rdi9;
    int64_t r8_10;
    int64_t rcx11;
    struct s2* rdx12;
    int1_t less13;
    int64_t rdi14;
    uint32_t eax15;
    uint32_t eax16;
    int32_t* rax17;
    uint32_t eax18;
    uint32_t r15d19;
    uint32_t r8d20;
    int32_t v21;
    int32_t r14d22;
    uint32_t eax23;
    int32_t eax24;
    struct s2* rax25;
    int32_t* rax26;
    int32_t r15d27;
    struct s2* rax28;
    unsigned char v29;
    int32_t eax30;
    struct s2* rdx31;
    int32_t v32;
    int32_t eax33;
    int32_t r14d34;
    unsigned char r14b35;
    int32_t eax36;
    int32_t eax37;
    unsigned char* rax38;
    int32_t r12d39;
    int64_t rax40;
    struct s0** rbx41;
    struct s0* rdi42;
    int32_t* rax43;
    int64_t rdi44;
    int32_t eax45;
    int64_t rax46;
    struct s0* rsi47;
    struct s0* rsi48;
    struct s0* r8_49;
    uint64_t rax50;
    struct s0* rax51;
    int32_t eax52;
    int32_t eax53;
    int64_t rdi54;
    int64_t rax55;
    int64_t rax56;
    struct s2* rax57;
    struct s2* rax58;
    int32_t eax59;
    int64_t rsi60;
    int64_t rdi61;
    int32_t eax62;
    struct s2* rax63;
    int32_t* rax64;
    int64_t rdi65;
    struct s2* rax66;
    int32_t eax67;

    __asm__("cli ");
    rdi5 = rsi->f0;
    set_program_name(rdi5);
    fun_25e0(6, 6);
    fun_2440("coreutils", "/usr/local/share/locale");
    fun_2420("coreutils", "/usr/local/share/locale");
    rax6 = fun_2380("POSIXLY_CORRECT", "/usr/local/share/locale", rdx, rcx);
    r12d7 = (reinterpret_cast<uint32_t>("coreutils") - (reinterpret_cast<uint32_t>("coreutils") + reinterpret_cast<uint1_t>(reinterpret_cast<uint32_t>("coreutils") < reinterpret_cast<uint32_t>("coreutils") + reinterpret_cast<uint1_t>(rax6 < 1))) & 0xfffffffe) + 0x7f;
    exit_failure = r12d7;
    atexit(0x3170, "/usr/local/share/locale");
    rsi8 = rsi;
    *reinterpret_cast<int32_t*>(&rdi9) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
    r8_10 = Version;
    rcx11 = reinterpret_cast<int64_t>("GNU coreutils");
    rdx12 = reinterpret_cast<struct s2*>("nohup");
    parse_gnu_standard_options_only(rdi9, rsi8, "nohup", "GNU coreutils", r8_10);
    less13 = optind < edi;
    if (!less13) {
        fun_2450();
        fun_2610();
        *reinterpret_cast<uint32_t*>(&rdi14) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi14) + 4) = 0;
        usage(rdi14);
    }
    eax15 = fun_23f0();
    eax16 = fun_23f0();
    if (eax16 || (rax17 = fun_23b0(), *rax17 != 9)) {
        eax18 = fun_23f0();
        r15d19 = eax18;
        *reinterpret_cast<unsigned char*>(&r8d20) = reinterpret_cast<uint1_t>(!!eax18);
        if (eax15) {
            v21 = 0;
            r14d22 = 0;
        } else {
            v21 = 0;
            if (!eax16) 
                goto addr_2972_7; else 
                goto addr_280f_8;
        }
    } else {
        r14d22 = 1;
        eax23 = fun_23f0();
        v21 = 1;
        r15d19 = eax23;
        *reinterpret_cast<unsigned char*>(&r8d20) = reinterpret_cast<uint1_t>(!!eax23);
        if (!eax15) 
            goto addr_2a38_10;
    }
    *reinterpret_cast<int32_t*>(&rcx11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx12) = 1;
    *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
    rsi8 = reinterpret_cast<struct s0*>("/dev/null");
    eax24 = fd_reopen();
    r8d20 = *reinterpret_cast<unsigned char*>(&r8d20);
    if (eax24 < 0) {
        rax25 = fun_2450();
        rax26 = fun_23b0();
        rdx12 = rax25;
        *reinterpret_cast<int32_t*>(&rsi8) = *rax26;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi8) + 4) = 0;
        fun_2610();
        r8d20 = *reinterpret_cast<unsigned char*>(&r8d20);
    }
    if (!(r15d19 | eax16)) {
        r15d27 = 2;
        rax28 = fun_2450();
        rdx12 = rax28;
        fun_2610();
    } else {
        if (eax16) {
            addr_280f_8:
            v29 = *reinterpret_cast<unsigned char*>(&r8d20);
            eax30 = fun_2560(0xfffffe7f, rsi8, rdx12, rcx11);
            *reinterpret_cast<int32_t*>(&rcx11) = 0x180;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx31) = 0x441;
            *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
            v32 = eax30;
            eax33 = fd_reopen();
            r14d34 = eax33;
            goto addr_2843_17;
        } else {
            addr_2a38_10:
            r14b35 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r14d22) & *reinterpret_cast<unsigned char*>(&r8d20));
            v29 = r14b35;
            if (!r14b35) {
                addr_2972_7:
                if (!*reinterpret_cast<unsigned char*>(&r8d20)) 
                    goto addr_28c2_18; else 
                    goto addr_297b_19;
            } else {
                eax36 = fun_2560(0xfffffe7f, rsi8, rdx12, rcx11);
                *reinterpret_cast<int32_t*>(&rdx31) = 0x180;
                *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
                v32 = eax36;
                eax37 = fun_2620("nohup.out", 0x441, 0x180, rcx11);
                r14d34 = eax37;
                goto addr_2843_17;
            }
        }
    }
    addr_28c8_21:
    rax38 = stderr;
    if (*rax38 & 32 || (r12d39 = 0, fun_2540(1, 1, rdx12, rcx11), rax40 = optind, rbx41 = reinterpret_cast<struct s0**>(reinterpret_cast<uint64_t>(rsi) + rax40 * 8), rdi42 = *rbx41, fun_2640(rdi42, rbx41, rdx12, rcx11), rax43 = fun_23b0(), *reinterpret_cast<int32_t*>(&rdi44) = r15d27, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi44) + 4) = 0, *reinterpret_cast<unsigned char*>(&r12d39) = reinterpret_cast<uint1_t>(*rax43 == 2), r12d7 = reinterpret_cast<uint32_t>(r12d39 + 0x7e), eax45 = fun_24b0(rdi44, 2, rdx12, rcx11), eax45 != 2)) {
        addr_2924_22:
        *reinterpret_cast<uint32_t*>(&rax46) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax46) + 4) = 0;
        return rax46;
    } else {
        rsi47 = *rbx41;
        quotearg_style(4, rsi47, rdx12, rcx11);
    }
    addr_2b54_24:
    fun_2450();
    fun_2610();
    goto addr_2924_22;
    addr_2843_17:
    rsi48 = reinterpret_cast<struct s0*>("nohup.out");
    *reinterpret_cast<int32_t*>(&r8_49) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_49) + 4) = 0;
    if (r14d34 < 0) {
        fun_23b0();
        rax50 = fun_2380("HOME", "nohup.out", rdx31, rcx11);
        if (!rax50) {
            quotearg_style(4, "nohup.out", rdx31, rcx11);
            fun_2450();
            fun_2610();
            goto addr_2924_22;
        }
        rax51 = file_name_concat(rax50, "nohup.out");
        if (!eax16) {
            *reinterpret_cast<int32_t*>(&rdx31) = 0x180;
            *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
            eax52 = fun_2620(rax51, 0x441, 0x180, rcx11);
            r8_49 = rax51;
            r14d34 = eax52;
        } else {
            *reinterpret_cast<int32_t*>(&rcx11) = 0x180;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx11) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdx31) = 0x441;
            *reinterpret_cast<int32_t*>(&rdx31 + 4) = 0;
            eax53 = fd_reopen();
            r8_49 = rax51;
            r14d34 = eax53;
        }
        if (r14d34 >= 0) 
            goto addr_2ade_31;
    } else {
        addr_2856_32:
        *reinterpret_cast<int32_t*>(&rdi54) = v32;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi54) + 4) = 0;
        fun_2560(rdi54, rsi48, rdx31, rcx11);
        rax55 = quotearg_style(4, rsi48, rdx31, rcx11);
        if (!eax15) {
            goto addr_2895_34;
        }
    }
    rax56 = quotearg_style(4, "nohup.out", rdx31, rcx11);
    rax57 = fun_2450();
    fun_2610();
    quotearg_style(4, r8_49, rax57, rax56);
    goto addr_2b54_24;
    addr_2ade_31:
    rsi48 = r8_49;
    goto addr_2856_32;
    addr_2895_34:
    rax58 = fun_2450();
    rcx11 = rax55;
    rdx12 = rax58;
    fun_2610();
    fun_2390(r8_49);
    if (v29) {
        *reinterpret_cast<int32_t*>(&rdx12) = 3;
        *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
        eax59 = rpl_fcntl(2, 0x406, 3, rcx11);
        r15d27 = eax59;
        if (eax16) {
            addr_29c9_37:
            *reinterpret_cast<int32_t*>(&rsi60) = 2;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi60) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi61) = r14d34;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi61) + 4) = 0;
            eax62 = fun_24b0(rdi61, 2, rdx12, rcx11);
            if (eax62 < 0) {
                rax63 = fun_2450();
                rax64 = fun_23b0();
                rdx12 = rax63;
                *reinterpret_cast<int32_t*>(&rsi60) = *rax64;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi60) + 4) = 0;
                fun_2610();
            }
        } else {
            goto addr_299a_40;
        }
    } else {
        addr_28c2_18:
        r15d27 = 2;
        goto addr_28c8_21;
    }
    if (v21) {
        *reinterpret_cast<int32_t*>(&rdi65) = r14d34;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi65) + 4) = 0;
        fun_24f0(rdi65, rsi60, rdx12, rcx11);
        goto addr_28c8_21;
    }
    addr_299a_40:
    if (!eax15) {
    }
    rax66 = fun_2450();
    rdx12 = rax66;
    fun_2610();
    goto addr_29c9_37;
    addr_297b_19:
    eax67 = rpl_fcntl(2, 0x406, 3, rcx11);
    r14d34 = 1;
    r15d27 = eax67;
    goto addr_299a_40;
}

int64_t __libc_start_main = 0;

void fun_2cd3() {
    __asm__("cli ");
    __libc_start_main(0x2700, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xb008;

void fun_2370(int64_t rdi);

int64_t fun_2d73() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2370(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2db3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

struct s2* program_name = reinterpret_cast<struct s2*>(0);

void fun_25f0(int64_t rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx);

unsigned char* stdout = reinterpret_cast<unsigned char*>(0);

void fun_2510(struct s2* rdi, unsigned char* rsi, int64_t rdx, struct s2* rcx);

int32_t fun_2530(int64_t rdi);

int32_t fun_23c0(struct s2* rdi, int64_t rsi, int64_t rdx, struct s2* rcx);

void fun_2660();

void fun_2680(unsigned char* rdi, int64_t rsi, struct s2* rdx, struct s2* rcx, struct s2* r8, struct s2* r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2dc3(int32_t edi) {
    struct s2* r12_2;
    struct s2* rax3;
    struct s2* v4;
    struct s2* rax5;
    unsigned char* r12_6;
    struct s2* rax7;
    unsigned char* r12_8;
    struct s2* rax9;
    unsigned char* r12_10;
    struct s2* rax11;
    struct s2* r12_12;
    struct s2* rax13;
    struct s2* rax14;
    int32_t eax15;
    struct s2* r13_16;
    struct s2* rax17;
    struct s2* rax18;
    int32_t eax19;
    struct s2* rax20;
    struct s2* rax21;
    struct s2* rax22;
    int32_t eax23;
    struct s2* rax24;
    unsigned char* r15_25;
    struct s2* rax26;
    struct s2* rax27;
    struct s2* rax28;
    unsigned char* rdi29;
    struct s2* r8_30;
    struct s2* r9_31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2450();
            fun_25f0(1, rax5, r12_2, r12_2);
            r12_6 = stdout;
            rax7 = fun_2450();
            fun_2510(rax7, r12_6, 5, r12_2);
            r12_8 = stdout;
            rax9 = fun_2450();
            fun_2510(rax9, r12_8, 5, r12_2);
            r12_10 = stdout;
            rax11 = fun_2450();
            fun_2510(rax11, r12_10, 5, r12_2);
            r12_12 = program_name;
            rax13 = fun_2450();
            fun_25f0(1, rax13, r12_12, r12_2);
            rax14 = fun_2450();
            fun_25f0(1, rax14, "nohup", r12_2);
            do {
                if (1) 
                    break;
                eax15 = fun_2530("nohup");
            } while (eax15);
            r13_16 = v4;
            if (!r13_16) {
                rax17 = fun_2450();
                fun_25f0(1, rax17, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax18 = fun_25e0(5);
                if (!rax18 || (eax19 = fun_23c0(rax18, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax19)) {
                    rax20 = fun_2450();
                    r13_16 = reinterpret_cast<struct s2*>("nohup");
                    fun_25f0(1, rax20, "https://www.gnu.org/software/coreutils/", "nohup");
                    r12_2 = reinterpret_cast<struct s2*>(" invocation");
                } else {
                    r13_16 = reinterpret_cast<struct s2*>("nohup");
                    goto addr_3120_9;
                }
            } else {
                rax21 = fun_2450();
                fun_25f0(1, rax21, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax22 = fun_25e0(5);
                if (!rax22 || (eax23 = fun_23c0(rax22, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax23)) {
                    addr_3026_11:
                    rax24 = fun_2450();
                    fun_25f0(1, rax24, "https://www.gnu.org/software/coreutils/", "nohup");
                    r12_2 = reinterpret_cast<struct s2*>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_16 == "nohup")) {
                        r12_2 = reinterpret_cast<struct s2*>(0x7a61);
                    }
                } else {
                    addr_3120_9:
                    r15_25 = stdout;
                    rax26 = fun_2450();
                    fun_2510(rax26, r15_25, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3026_11;
                }
            }
            rax27 = fun_2450();
            fun_25f0(1, rax27, r13_16, r12_2);
            addr_2e1e_14:
            fun_2660();
        }
    } else {
        rax28 = fun_2450();
        rdi29 = stderr;
        fun_2680(rdi29, 1, rax28, r12_2, r8_30, r9_31, v32, v33, v34, v35, v36, v37);
        goto addr_2e1e_14;
    }
}

int64_t file_name = 0;

void fun_3153(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3163(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(unsigned char* rdi);

struct s2* quotearg_colon();

struct s2* fun_23d0(int64_t rdi, int64_t rsi, int64_t rdx, struct s2* rcx, struct s2* r8);

void fun_3173() {
    unsigned char* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    unsigned char* rdi6;
    int32_t eax7;
    struct s2* rax8;
    int64_t rdi9;
    struct s2* rax10;
    int64_t rsi11;
    struct s2* r8_12;
    struct s2* rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23b0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2450();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3203_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2610();
    }
    while (1) {
        *reinterpret_cast<uint32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23d0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3203_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2610();
    }
}

uint32_t fun_2410(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_3223(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    int64_t v8;
    struct s2* rax9;
    uint32_t eax10;
    uint32_t r12d11;
    int32_t eax12;
    struct s2* rdx13;
    uint32_t eax14;
    int1_t zf15;
    void* rax16;
    int64_t rax17;
    int64_t rsi18;
    int64_t rdi19;
    uint32_t eax20;
    int64_t rdi21;
    uint32_t eax22;
    int32_t* rax23;
    int64_t rdi24;
    int32_t r13d25;
    uint32_t eax26;
    int32_t* rax27;
    int64_t rdi28;
    uint32_t eax29;
    uint32_t ecx30;
    int64_t rax31;
    uint32_t eax32;
    uint32_t eax33;
    uint32_t eax34;
    int32_t ecx35;
    int64_t rax36;

    __asm__("cli ");
    v8 = rdx;
    rax9 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax10 = fun_2410(rdi);
        r12d11 = eax10;
        goto addr_3324_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax12 = have_dupfd_cloexec_0;
        *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
        *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
        if (eax12 < 0) {
            eax14 = fun_2410(rdi);
            r12d11 = eax14;
            if (reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0) || (zf15 = have_dupfd_cloexec_0 == -1, !zf15)) {
                addr_3324_3:
                rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax9) - reinterpret_cast<unsigned char>(g28));
                if (rax16) {
                    fun_2480();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax17) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    return rax17;
                }
            } else {
                addr_33d9_9:
                *reinterpret_cast<int32_t*>(&rsi18) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi19) = r12d11;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
                eax20 = fun_2410(rdi19, rdi19);
                if (reinterpret_cast<int32_t>(eax20) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<int32_t*>(&rsi18) = 2, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi21) = r12d11, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx13) = eax20 | 1, *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0, eax22 = fun_2410(rdi21, rdi21), eax22 == 0xffffffff)) {
                    rax23 = fun_23b0();
                    *reinterpret_cast<uint32_t*>(&rdi24) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
                    r12d11 = 0xffffffff;
                    r13d25 = *rax23;
                    fun_24f0(rdi24, rsi18, rdx13, rcx);
                    *rax23 = r13d25;
                    goto addr_3324_3;
                }
            }
        } else {
            eax26 = fun_2410(rdi, rdi);
            r12d11 = eax26;
            if (reinterpret_cast<int32_t>(eax26) >= reinterpret_cast<int32_t>(0) || (rax27 = fun_23b0(), *reinterpret_cast<int32_t*>(&rdi28) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0, *rax27 != 22)) {
                have_dupfd_cloexec_0 = 1;
                goto addr_3324_3;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
                *reinterpret_cast<int32_t*>(&rdx13 + 4) = 0;
                eax29 = fun_2410(rdi28);
                r12d11 = eax29;
                if (reinterpret_cast<int32_t>(eax29) < reinterpret_cast<int32_t>(0)) 
                    goto addr_3324_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_33d9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_3289_16;
    ecx30 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx30 > 10) 
        goto addr_328d_18;
    rax31 = 1 << *reinterpret_cast<unsigned char*>(&ecx30);
    if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x502)) {
            addr_328d_18:
            if (0) {
            }
        } else {
            addr_32d5_23:
            eax32 = fun_2410(rdi);
            r12d11 = eax32;
            goto addr_3324_3;
        }
        eax33 = fun_2410(rdi);
        r12d11 = eax33;
        goto addr_3324_3;
    }
    if (0) {
    }
    eax34 = fun_2410(rdi);
    r12d11 = eax34;
    goto addr_3324_3;
    addr_3289_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_328d_18;
    ecx35 = *reinterpret_cast<int32_t*>(&rsi);
    rax36 = 1 << *reinterpret_cast<unsigned char*>(&ecx35);
    if (!(*reinterpret_cast<uint32_t*>(&rax36) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax36) & 0xa0a) 
            goto addr_32d5_23;
        goto addr_328d_18;
    }
}

int64_t fun_3493(int32_t edi, struct s0* rsi, int32_t edx, int64_t rcx) {
    int64_t rsi5;
    struct s2* rdx6;
    int32_t eax7;
    int64_t rax8;
    int64_t rsi9;
    int64_t rdi10;
    int32_t eax11;
    int32_t* rax12;
    int64_t rdi13;
    int32_t r13d14;
    int64_t rax15;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rsi5) = edx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi5) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx6) = *reinterpret_cast<int32_t*>(&rcx);
    *reinterpret_cast<int32_t*>(&rdx6 + 4) = 0;
    eax7 = fun_2620(rsi, rsi5, rdx6, rcx);
    if (edi == eax7 || eax7 < 0) {
        *reinterpret_cast<int32_t*>(&rax8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rsi9) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi10) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
        eax11 = fun_24b0(rdi10, rsi9, rdx6, rcx);
        rax12 = fun_23b0();
        *reinterpret_cast<int32_t*>(&rdi13) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        r13d14 = *rax12;
        fun_24f0(rdi13, rsi9, rdx6, rcx);
        *reinterpret_cast<int32_t*>(&rax15) = eax11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
        *rax12 = r13d14;
        return rax15;
    }
}

int64_t mfile_name_concat();

void xalloc_die();

void fun_3513() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = mfile_name_concat();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void* last_component();

void* base_len(void* rdi);

struct s2* fun_2470(struct s2* rdi, ...);

void* fun_2590(signed char* rdi);

signed char* fun_2600(void* rdi, struct s2* rsi, struct s2* rdx);

void* fun_3533(struct s2* rdi, struct s2* rsi, void** rdx) {
    void* rax4;
    void* rax5;
    struct s2* r14_6;
    struct s2* rax7;
    void* rbx8;
    uint1_t zf9;
    int32_t eax10;
    unsigned char v11;
    int1_t zf12;
    int32_t eax13;
    void* rax14;
    signed char* rax15;
    uint32_t ecx16;
    void* rdi17;
    signed char* rax18;

    __asm__("cli ");
    rax4 = last_component();
    rax5 = base_len(rax4);
    r14_6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(rax4) - reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax5));
    rax7 = fun_2470(rsi);
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rbx8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
        zf9 = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(&rsi->f0) == 47);
        eax10 = 46;
        if (!zf9) {
            eax10 = 0;
        }
        *reinterpret_cast<unsigned char*>(&rbx8) = zf9;
        v11 = *reinterpret_cast<unsigned char*>(&eax10);
    } else {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(r14_6) + 0xffffffffffffffff) == 47) {
            v11 = 0;
            *reinterpret_cast<int32_t*>(&rbx8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rbx8) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx8) + 4) = 0;
            zf12 = *reinterpret_cast<unsigned char*>(&rsi->f0) == 47;
            eax13 = 47;
            if (zf12) {
                eax13 = 0;
            }
            *reinterpret_cast<unsigned char*>(&rbx8) = reinterpret_cast<uint1_t>(!zf12);
            v11 = *reinterpret_cast<unsigned char*>(&eax13);
        }
    }
    rax14 = fun_2590(reinterpret_cast<unsigned char>(r14_6) + reinterpret_cast<unsigned char>(rax7) + 1 + reinterpret_cast<int64_t>(rbx8));
    if (rax14) {
        rax15 = fun_2600(rax14, rdi, r14_6);
        ecx16 = v11;
        rdi17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax15) + reinterpret_cast<int64_t>(rbx8));
        *rax15 = *reinterpret_cast<signed char*>(&ecx16);
        if (rdx) {
            *rdx = rdi17;
        }
        rax18 = fun_2600(rdi17, rsi, rax7);
        *rax18 = 0;
    }
    return rax14;
}

int32_t opterr = 0;

int32_t fun_2490();

void version_etc_va(unsigned char* rdi, int64_t rsi, int64_t rdx, int64_t rcx, void* r8);

void fun_3633(int32_t edi) {
    signed char al2;
    struct s2* rax3;
    int32_t r14d4;
    int32_t eax5;
    void* rax6;
    unsigned char* rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t r9_11;

    __asm__("cli ");
    if (al2) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax3 = g28;
    r14d4 = opterr;
    opterr = 0;
    if (edi != 2) 
        goto addr_36b0_4;
    eax5 = fun_2490();
    if (eax5 == -1) 
        goto addr_36b0_4;
    if (eax5 != 0x68) {
        if (eax5 != 0x76) {
            addr_36b0_4:
            opterr = r14d4;
            optind = 0;
            rax6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
            if (rax6) {
                fun_2480();
            } else {
                return;
            }
        } else {
            rdi7 = stdout;
            version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
            fun_2660();
        }
    }
    r9_11();
    goto addr_36b0_4;
}

void fun_3773() {
    signed char al1;
    struct s2* rax2;
    signed char r9b3;
    int32_t ebx4;
    int32_t eax5;
    int64_t v6;
    unsigned char* rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t rdi11;
    void* rax12;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    if (!r9b3) {
    }
    ebx4 = opterr;
    opterr = 1;
    eax5 = fun_2490();
    if (eax5 != -1) {
        if (eax5 == 0x68) {
            addr_38a0_7:
            v6();
        } else {
            if (eax5 == 0x76) {
                rdi7 = stdout;
                version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
                fun_2660();
                goto addr_38a0_7;
            } else {
                *reinterpret_cast<uint32_t*>(&rdi11) = exit_failure;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
                v6(rdi11);
            }
        }
    }
    opterr = ebx4;
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2480();
    } else {
        return;
    }
}

void fun_2670(struct s0* rdi, int64_t rsi, int64_t rdx, unsigned char* rcx);

struct s5 {
    signed char[1] pad1;
    unsigned char f1;
    signed char[2] pad4;
    unsigned char f4;
};

struct s5* fun_24c0();

struct s2* __progname = reinterpret_cast<struct s2*>(0);

struct s2* __progname_full = reinterpret_cast<struct s2*>(0);

void fun_38b3(struct s2* rdi) {
    unsigned char* rcx2;
    struct s2* rbx3;
    struct s5* rax4;
    struct s2* r12_5;
    struct s2* rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2670("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23a0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24c0();
        if (rax4 && ((r12_5 = reinterpret_cast<struct s2*>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23c0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (*reinterpret_cast<unsigned char*>(&rax4->f1) != 0x6c || (r12_5->f1 != 0x74 || r12_5->f2 != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<struct s2*>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5053(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23b0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xb200;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5093(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb200);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_50b3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xb200);
    }
    *rdi = esi;
    return 0xb200;
}

int64_t fun_50d3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xb200);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5113(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xb200);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_5133(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xb200);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x26ca;
    if (!rdx) 
        goto 0x26ca;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xb200;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[24] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_5173(struct s0* rdi, struct s0* rsi, int64_t rdx, int64_t rcx, struct s8* r8) {
    struct s8* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    struct s0* rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xb200);
    }
    rax7 = fun_23b0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x51a6);
    *rax7 = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    struct s0* f8;
    signed char[24] pad40;
    int64_t f28;
    int64_t f30;
};

struct s0* fun_51f3(int64_t rdi, int64_t rsi, struct s0** rdx, struct s9* rcx) {
    struct s9* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    struct s0* r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    struct s0* rax14;
    struct s0* rsi15;
    struct s0* rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xb200);
    }
    rax6 = fun_23b0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<struct s0*>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5221);
    rsi15 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax14) + 1);
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x527c);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_52e3() {
    __asm__("cli ");
}

struct s0* gb078 = reinterpret_cast<struct s0*>(0xb100);

int64_t slotvec0 = 0x100;

void fun_52f3() {
    uint32_t eax1;
    struct s0* r12_2;
    int64_t rax3;
    struct s0** rbx4;
    struct s0** rbp5;
    struct s0* rdi6;
    struct s0* rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = &r12_2->f18;
        rbp5 = reinterpret_cast<struct s0**>(reinterpret_cast<uint64_t>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 2;
            fun_2390(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = r12_2->f8;
    if (rdi7 != 0xb100) {
        fun_2390(rdi7);
        gb078 = reinterpret_cast<struct s0*>(0xb100);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xb070) {
        fun_2390(r12_2);
        slotvec = reinterpret_cast<struct s0*>(0xb070);
    }
    nslots = 1;
    return;
}

void fun_5393() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_53b3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_53c3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_53e3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s0* fun_5403(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26d0;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s0* fun_5493(struct s0* rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x26d5;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

struct s0* fun_5523(int32_t edi, int64_t rsi) {
    struct s2* rax3;
    struct s1* rcx4;
    struct s0* rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x26da;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2480();
    } else {
        return rax5;
    }
}

struct s0* fun_55b3(int32_t edi, int64_t rsi, int64_t rdx) {
    struct s2* rax4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26df;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s0* fun_5643(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    struct s2* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    struct s0* rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5bb0]");
    __asm__("movdqa xmm1, [rip+0x5bb8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x5ba1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2480();
    } else {
        return rax10;
    }
}

struct s0* fun_56e3(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    struct s2* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x5b10]");
    __asm__("movdqa xmm1, [rip+0x5b18]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x5b01]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2480();
    } else {
        return rax9;
    }
}

struct s0* fun_5783(int64_t rdi) {
    struct s2* rax2;
    struct s0* rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x5a70]");
    __asm__("movdqa xmm1, [rip+0x5a78]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x5a59]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2480();
    } else {
        return rax3;
    }
}

struct s0* fun_5813(int64_t rdi, int64_t rsi) {
    struct s2* rax3;
    struct s0* rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x59e0]");
    __asm__("movdqa xmm1, [rip+0x59e8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x59d6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2480();
    } else {
        return rax4;
    }
}

struct s0* fun_58a3(struct s0* rdi, int32_t esi, int64_t rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26e4;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s0* fun_5943(struct s0* rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x58aa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x58a2]");
    __asm__("movdqa xmm2, [rip+0x58aa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26e9;
    if (!rdx) 
        goto 0x26e9;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

struct s0* fun_59e3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    struct s2* rcx6;
    struct s1* rcx7;
    struct s0* rdi8;
    struct s0* rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x580a]");
    __asm__("movdqa xmm1, [rip+0x5812]");
    __asm__("movdqa xmm2, [rip+0x581a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ee;
    if (!rdx) 
        goto 0x26ee;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2480();
    } else {
        return rax9;
    }
}

struct s0* fun_5a93(int64_t rdi, int64_t rsi, int64_t rdx) {
    struct s2* rdx4;
    struct s1* rcx5;
    struct s0* rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x575a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x5752]");
    __asm__("movdqa xmm2, [rip+0x575a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26f3;
    if (!rsi) 
        goto 0x26f3;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax6;
    }
}

struct s0* fun_5b33(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    struct s2* rcx5;
    struct s1* rcx6;
    struct s0* rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x56ba]");
    __asm__("movdqa xmm1, [rip+0x56c2]");
    __asm__("movdqa xmm2, [rip+0x56ca]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26f8;
    if (!rsi) 
        goto 0x26f8;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2480();
    } else {
        return rax7;
    }
}

void fun_5bd3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5be3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c03() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5c23(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s10 {
    struct s2* f0;
    signed char[7] pad8;
    struct s2* f8;
    signed char[7] pad16;
    struct s2* f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2550(int64_t rdi, unsigned char* rsi, struct s2* rdx, struct s2* rcx, struct s2* r8, struct s2* r9);

void fun_5c43(unsigned char* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx, struct s10* r8, struct s2* r9) {
    struct s2* r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    struct s2* rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    struct s2* rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    struct s2* r14_40;
    struct s2* r13_41;
    struct s2* r12_42;
    struct s2* rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2680(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2680(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2450();
    fun_2680(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2550(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2450();
    fun_2680(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2550(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2450();
        fun_2680(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x7d08 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x7d08;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_60b3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_60d3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s11* rcx8;
    struct s2* rax9;
    struct s2* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2480();
    } else {
        return;
    }
}

void fun_6173(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    struct s2* rax15;
    struct s2* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_6216_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_6220_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2480();
    } else {
        return;
    }
    addr_6216_5:
    goto addr_6220_7;
}

void fun_6253() {
    unsigned char* rsi1;
    struct s2* rdx2;
    struct s2* rcx3;
    struct s2* r8_4;
    struct s2* r9_5;
    struct s2* rax6;
    struct s2* rcx7;
    struct s2* rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2550(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2450();
    fun_25f0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2450();
    fun_25f0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2450();
    goto fun_25f0;
}

int64_t fun_2400();

void fun_62f3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2400();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6333(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6353(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6373(signed char* rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2590(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_25d0();

void fun_6393(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25d0();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_63c3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_63f3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2400();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_6433() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6473(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_64a3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2400();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_64f3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2400();
            if (rax5) 
                break;
            addr_653d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_653d_5;
        rax8 = fun_2400();
        if (rax8) 
            goto addr_6526_9;
        if (rbx4) 
            goto addr_653d_5;
        addr_6526_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_6583(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2400();
            if (rax8) 
                break;
            addr_65ca_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_65ca_5;
        rax11 = fun_2400();
        if (rax11) 
            goto addr_65b2_9;
        if (!rbx6) 
            goto addr_65b2_9;
        if (r12_4) 
            goto addr_65ca_5;
        addr_65b2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_6613(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_66bd_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_66d0_10:
                *r12_8 = 0;
            }
            addr_6670_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_6696_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_66e4_14;
            if (rcx10 <= rsi9) 
                goto addr_668d_16;
            if (rsi9 >= 0) 
                goto addr_66e4_14;
            addr_668d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_66e4_14;
            addr_6696_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25d0();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_66e4_14;
            if (!rbp13) 
                break;
            addr_66e4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_66bd_9;
        } else {
            if (!r13_6) 
                goto addr_66d0_10;
            goto addr_6670_11;
        }
    }
}

int64_t fun_2520();

void fun_6713() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2520();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6743() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2520();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6773() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2520();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_6793() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2520();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_2570(signed char* rdi, struct s2* rsi, signed char* rdx);

void fun_67b3(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2590(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

void fun_67f3(int64_t rdi, signed char* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2590(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

struct s12 {
    signed char[1] pad1;
    signed char f1;
};

void fun_6833(int64_t rdi, struct s12* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2590(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_2570;
    }
}

void fun_6873(struct s2* rdi) {
    struct s2* rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_2470(rdi);
    rax3 = fun_2590(&rax2->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2570;
    }
}

void fun_68b3() {
    struct s0* rdi1;

    __asm__("cli ");
    fun_2450();
    *reinterpret_cast<uint32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi1) + 4) = 0;
    fun_2610();
    fun_23a0(rdi1);
}

struct s13 {
    unsigned char f0;
    unsigned char f1;
};

struct s13* fun_68f3(struct s13* rdi) {
    uint32_t edx2;
    struct s13* rax3;
    struct s13* rcx4;
    int32_t esi5;

    __asm__("cli ");
    edx2 = rdi->f0;
    rax3 = rdi;
    if (*reinterpret_cast<signed char*>(&edx2) == 47) {
        do {
            edx2 = rax3->f1;
            rax3 = reinterpret_cast<struct s13*>(&rax3->f1);
        } while (*reinterpret_cast<signed char*>(&edx2) == 47);
    }
    if (*reinterpret_cast<signed char*>(&edx2)) {
        rcx4 = rax3;
        esi5 = 0;
        while (1) {
            if (*reinterpret_cast<signed char*>(&edx2) != 47) {
                if (*reinterpret_cast<signed char*>(&esi5)) {
                    rax3 = rcx4;
                    esi5 = 0;
                }
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s13*>(&rcx4->f1);
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            } else {
                edx2 = rcx4->f1;
                rcx4 = reinterpret_cast<struct s13*>(&rcx4->f1);
                esi5 = 1;
                if (!*reinterpret_cast<signed char*>(&edx2)) 
                    break;
            }
        }
    }
    return rax3;
}

void fun_6953(struct s2* rdi) {
    struct s2* rbx2;
    struct s2* rax3;

    __asm__("cli ");
    rbx2 = rdi;
    rax3 = fun_2470(rdi);
    while (reinterpret_cast<unsigned char>(rax3) > reinterpret_cast<unsigned char>(1) && *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbx2) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) == 47) {
        rax3 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff);
    }
    return;
}

int64_t fun_23e0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_6983(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23e0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_69de_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23b0();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_69de_3;
            rax6 = fun_23b0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s14 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2580(struct s14* rdi);

int32_t fun_25c0(struct s14* rdi);

int64_t fun_24d0(int64_t rdi, ...);

int32_t rpl_fflush(struct s14* rdi);

int64_t fun_2430(struct s14* rdi);

int64_t fun_69f3(struct s14* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2580(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25c0(rdi);
        if (!(eax3 && (eax4 = fun_2580(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24d0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23b0();
            r12d9 = *rax8;
            rax10 = fun_2430(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2430;
}

void rpl_fseeko(struct s14* rdi);

void fun_6a83(struct s14* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25c0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6ad3(struct s14* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2580(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24d0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_25b0(int64_t rdi);

signed char* fun_6b53() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25b0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_24a0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_6b93(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    struct s2* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24a0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2480();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_6c23() {
    struct s2* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2480();
    } else {
        return rax3;
    }
}

int64_t fun_6ca3(int64_t rdi, signed char* rsi, struct s2* rdx) {
    struct s2* rax4;
    int32_t r13d5;
    struct s2* rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_25e0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2470(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2570(rsi, rax4, &rax6->f1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2570(rsi, rax4, reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_6d53() {
    __asm__("cli ");
    goto fun_25e0;
}

void fun_6d63() {
    __asm__("cli ");
}

void fun_6d77() {
    __asm__("cli ");
    return;
}

uint32_t fun_2500(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx);

struct s2* rpl_mbrtowc(void* rdi, struct s2* rsi);

int32_t fun_26a0(int64_t rdi, struct s2* rsi);

uint32_t fun_2690(struct s2* rdi, struct s2* rsi);

void** fun_26b0(struct s2* rdi, struct s2* rsi, struct s2* rdx, struct s2* rcx);

void fun_3ae5() {
    struct s2** rsp1;
    int32_t ebp2;
    struct s2* rax3;
    struct s2** rsp4;
    struct s2* r11_5;
    struct s2* r11_6;
    struct s2* v7;
    int32_t ebp8;
    struct s2* rax9;
    struct s2* rdx10;
    struct s2* rax11;
    struct s2* r11_12;
    struct s2* v13;
    int32_t ebp14;
    struct s2* rax15;
    struct s2* r15_16;
    int32_t ebx17;
    uint32_t eax18;
    struct s2* r13_19;
    void* r14_20;
    signed char* r12_21;
    struct s2* v22;
    int32_t ebx23;
    struct s2* rax24;
    struct s2** rsp25;
    struct s2* v26;
    struct s2* r11_27;
    struct s2* v28;
    struct s2* v29;
    struct s2* rsi30;
    struct s2* v31;
    struct s2* v32;
    struct s2* r10_33;
    struct s2* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    struct s2* r9_37;
    struct s2* v38;
    struct s2* rdi39;
    struct s2* v40;
    struct s2* rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    struct s2* rcx44;
    unsigned char al45;
    struct s2* v46;
    int64_t v47;
    struct s2* v48;
    struct s2* v49;
    struct s2* rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    struct s2* v58;
    unsigned char v59;
    struct s2* v60;
    struct s2* v61;
    struct s2* v62;
    signed char* v63;
    struct s2* r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    struct s2* r13_69;
    struct s2* rsi70;
    void* v71;
    struct s2* r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    struct s2* v107;
    struct s2* rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<struct s2**>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2450();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2450();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = *reinterpret_cast<unsigned char*>(&rdx10->f0), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2470(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<struct s2*>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<struct s2*>(0);
    while (1) {
        v32 = *reinterpret_cast<struct s2**>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_3de3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_3de3_22; else 
                            goto addr_41dd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_429d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_45f0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_3de0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_3de0_30; else 
                                goto addr_4609_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2470(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_45f0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2500(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_45f0_28; else 
                            goto addr_3c8c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_4750_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_45d0_40:
                        if (r11_27 == 1) {
                            addr_415d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_4718_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_3d97_44;
                            }
                        } else {
                            goto addr_45e0_46;
                        }
                    } else {
                        addr_475f_47:
                        rax24 = v46;
                        if (!rax24->f1) {
                            goto addr_415d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_3de3_22:
                                if (v47 != 1) {
                                    addr_4339_52:
                                    v48 = reinterpret_cast<struct s2*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2470(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4384_54;
                                    }
                                } else {
                                    goto addr_3df0_56;
                                }
                            } else {
                                addr_3d95_57:
                                ebp36 = 0;
                                goto addr_3d97_44;
                            }
                        } else {
                            addr_45c4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_475f_47; else 
                                goto addr_45ce_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_415d_41;
                        if (v47 == 1) 
                            goto addr_3df0_56; else 
                            goto addr_4339_52;
                    }
                }
                addr_3e51_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<struct s2*>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_3ce8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_3d0d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4010_65;
                    } else {
                        addr_3e79_66:
                        r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_46c8_67;
                    }
                } else {
                    goto addr_3e70_69;
                }
                addr_3d21_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = reinterpret_cast<struct s2*>(&r15_16->f2);
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                addr_3d6c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_46c8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = reinterpret_cast<struct s2*>(&r15_16->f2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_3d6c_81;
                }
                addr_3e70_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_3d0d_64; else 
                    goto addr_3e79_66;
                addr_3d97_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_3e4f_91;
                if (v22) 
                    goto addr_3daf_93;
                addr_3e4f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3e51_62;
                addr_4384_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_4b0b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_4b7b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = *reinterpret_cast<unsigned char*>(&rdx10->f0) - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_497f_103;
                            rdx10 = reinterpret_cast<struct s2*>(&rdx10->f1);
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26a0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2690(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_447e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_3e3c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4488_112;
                    }
                } else {
                    addr_4488_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<struct s2*>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4559_114;
                }
                addr_3e48_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_3e4f_91;
                while (1) {
                    addr_4559_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_4a67_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_44c6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                        }
                        r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_4a75_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4547_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = reinterpret_cast<struct s2*>(&r15_16->f2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4547_128;
                        }
                    }
                    addr_44f5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4547_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    r15_16 = reinterpret_cast<struct s2*>(&r15_16->f1);
                    continue;
                    addr_44c6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(&r15_16->f2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    ++r15_16;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_44f5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3d6c_81;
                addr_4a75_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_46c8_67;
                addr_4b0b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_447e_109;
                addr_4b7b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_447e_109;
                addr_3df0_56:
                rax93 = fun_26b0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_3e3c_110;
                addr_45ce_59:
                goto addr_45d0_40;
                addr_429d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_3de3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_3e48_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_3d95_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_3de3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_42e2_160;
                if (!v22) 
                    goto addr_46b7_162; else 
                    goto addr_48c3_163;
                addr_42e2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_46b7_162:
                    r9_37 = reinterpret_cast<struct s2*>(&r9_37->f1);
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_46c8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_418b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_3ff3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_3d21_70; else 
                    goto addr_4007_169;
                addr_418b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_3ce8_63;
                goto addr_3e70_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_45c4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_46ff_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3de0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3cd8_178; else 
                        goto addr_4682_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_45c4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3de3_22;
                }
                addr_46ff_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_3de0_30:
                    r8d42 = 0;
                    goto addr_3de3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_3e51_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_4718_42;
                    }
                }
                addr_3cd8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_3ce8_63;
                addr_4682_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_45e0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_3e51_62;
                } else {
                    addr_4692_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_3de3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_4e42_188;
                if (v28) 
                    goto addr_46b7_162;
                addr_4e42_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_3ff3_168;
                addr_3c8c_37:
                if (v22) 
                    goto addr_4c83_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(&rbx41->f0);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_3ca3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_4750_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_47db_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_3de3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<struct s2*>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<struct s2*>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_3cd8_178; else 
                        goto addr_47b7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_45c4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_3de3_22;
                }
                addr_47db_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_3de3_22;
                }
                addr_47b7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_45e0_46;
                goto addr_4692_186;
                addr_3ca3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_3de3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_3de3_22; else 
                    goto addr_3cb4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<struct s2*>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_4d8e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<struct s2*>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_4c14_210;
            if (1) 
                goto addr_4c12_212;
            if (!v29) 
                goto addr_484e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<struct s2*>(1);
            v47 = rax101;
            v26 = reinterpret_cast<struct s2*>("\"");
            if (!0) 
                goto addr_4d81_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<struct s2*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<struct s2*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4010_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_3dcb_219; else 
            goto addr_402a_220;
        addr_3daf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_3dc3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_402a_220; else 
            goto addr_3dcb_219;
        addr_497f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_402a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_499d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2460();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<struct s2*>("'");
        v29 = reinterpret_cast<struct s2*>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s2*>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<struct s2*>(1);
        v22 = reinterpret_cast<struct s2*>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<struct s2*>(0);
            continue;
        }
        addr_4e10_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_4876_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<struct s2*>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<struct s2*>(0);
        v22 = reinterpret_cast<struct s2*>(0);
        v28 = reinterpret_cast<struct s2*>(1);
        v26 = reinterpret_cast<struct s2*>("'");
        continue;
        addr_4a67_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3dc3_221;
        addr_48c3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_3dc3_221;
        addr_4007_169:
        goto addr_4010_65;
        addr_4d8e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_402a_220;
        goto addr_499d_222;
        addr_4c14_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(&v26->f0), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                rdx108 = reinterpret_cast<struct s2*>(&rdx108->f1);
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_4c6e_236;
        fun_2480();
        rsp25 = rsp25 - 8 + 8;
        goto addr_4e10_225;
        addr_4c12_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_4c14_210;
        addr_484e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_4c14_210;
        } else {
            rdx10 = reinterpret_cast<struct s2*>(0);
            goto addr_4876_226;
        }
        addr_4d81_216:
        r13_34 = reinterpret_cast<struct s2*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<struct s2*>("\"");
        v29 = reinterpret_cast<struct s2*>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<struct s2*>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<struct s2*>(1);
        v22 = reinterpret_cast<struct s2*>(0);
        v31 = reinterpret_cast<struct s2*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_41dd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x77cc + rax113 * 4) + 0x77cc;
    addr_4609_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x78cc + rax114 * 4) + 0x78cc;
    addr_4c83_190:
    addr_3dcb_219:
    goto 0x3ab0;
    addr_3cb4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x76cc + rax115 * 4) + 0x76cc;
    addr_4c6e_236:
    goto v116;
}

void fun_3cd0() {
}

void fun_3e88() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x3b82;
}

void fun_3ee1() {
    goto 0x3b82;
}

void fun_3fce() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x3e51;
    }
    if (v2) 
        goto 0x48c3;
    if (!r10_3) 
        goto addr_4a2e_5;
    if (!v4) 
        goto addr_48fe_7;
    addr_4a2e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_48fe_7:
    goto 0x3d04;
}

void fun_3fec() {
}

void fun_4097() {
    signed char v1;

    if (v1) {
        goto 0x401f;
    } else {
        goto 0x3d5a;
    }
}

void fun_40b1() {
    signed char v1;

    if (!v1) 
        goto 0x40aa; else 
        goto "???";
}

void fun_40d8() {
    goto 0x3ff3;
}

void fun_4158() {
}

void fun_4170() {
}

void fun_419f() {
    goto 0x3ff3;
}

void fun_41f1() {
    goto 0x4180;
}

void fun_4220() {
    goto 0x4180;
}

void fun_4253() {
    goto 0x4180;
}

void fun_4620() {
    goto 0x3cd8;
}

void fun_491e() {
    signed char v1;

    if (v1) 
        goto 0x48c3;
    goto 0x3d04;
}

void fun_49c5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x3d04;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x3ce8;
        goto 0x3d04;
    }
}

void fun_4de2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4050;
    } else {
        goto 0x3b82;
    }
}

void fun_5d18() {
    fun_2450();
}

void fun_3f0e() {
    goto 0x3b82;
}

void fun_40e4() {
    goto 0x409c;
}

void fun_41ab() {
    goto 0x3cd8;
}

void fun_41fd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4180;
    goto 0x3daf;
}

void fun_422f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x418b;
        goto 0x3bb0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x402a;
        goto 0x3dcb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x49c8;
    if (r10_8 > r15_9) 
        goto addr_4115_9;
    addr_411a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x49d3;
    goto 0x3d04;
    addr_4115_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_411a_10;
}

void fun_4262() {
    goto 0x3d97;
}

void fun_4630() {
    goto 0x3d97;
}

void fun_4dcf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x3eec;
    } else {
        goto 0x4050;
    }
}

void fun_5dd0() {
}

void fun_426c() {
    goto 0x4207;
}

void fun_463a() {
    goto 0x415d;
}

void fun_5e30() {
    fun_2450();
    goto fun_2680;
}

void fun_3f3d() {
    goto 0x3b82;
}

void fun_4278() {
    goto 0x4207;
}

void fun_4647() {
    goto 0x41ae;
}

void fun_5e70() {
    fun_2450();
    goto fun_2680;
}

void fun_3f6a() {
    goto 0x3b82;
}

void fun_4284() {
    goto 0x4180;
}

void fun_5eb0() {
    fun_2450();
    goto fun_2680;
}

void fun_3f8c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x4920;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x3e51;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x3e51;
    }
    if (v11) 
        goto 0x4c83;
    if (r10_12 > r15_13) 
        goto addr_4cd3_8;
    addr_4cd8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x4a11;
    addr_4cd3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_4cd8_9;
}

struct s15 {
    signed char[24] pad24;
    int64_t f18;
};

struct s16 {
    signed char[16] pad16;
    struct s2* f10;
};

struct s17 {
    signed char[8] pad8;
    struct s2* f8;
};

void fun_5f00() {
    int64_t r15_1;
    struct s15* rbx2;
    struct s2* r14_3;
    struct s16* rbx4;
    struct s2* r13_5;
    struct s17* rbx6;
    struct s2* r12_7;
    struct s2** rbx8;
    struct s2* rax9;
    unsigned char* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2450();
    fun_2680(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x5f22, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_5f58() {
    fun_2450();
    goto 0x5f29;
}

struct s18 {
    signed char[32] pad32;
    int64_t f20;
};

struct s19 {
    signed char[24] pad24;
    int64_t f18;
};

struct s20 {
    signed char[16] pad16;
    struct s2* f10;
};

struct s21 {
    signed char[8] pad8;
    struct s2* f8;
};

struct s22 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_5f90() {
    int64_t rcx1;
    struct s18* rbx2;
    int64_t r15_3;
    struct s19* rbx4;
    struct s2* r14_5;
    struct s20* rbx6;
    struct s2* r13_7;
    struct s21* rbx8;
    struct s2* r12_9;
    struct s2** rbx10;
    int64_t v11;
    struct s22* rbx12;
    struct s2* rax13;
    unsigned char* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2450();
    fun_2680(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x5fc4, __return_address(), rcx1);
    goto v15;
}

void fun_6008() {
    fun_2450();
    goto 0x5fcb;
}
