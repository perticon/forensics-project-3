
int64_t fun_2410(void** rdi, ...);

int32_t i_ring_push(void*** rdi);

int32_t fun_2570();

int32_t cwd_advance_fd(void** rdi, void** esi, signed char dl) {
    int32_t eax4;
    int32_t eax5;

    if (*reinterpret_cast<void***>(rdi + 44) == esi && !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    }
    if (dl) {
        eax4 = i_ring_push(rdi + 96);
        if (eax4 < 0) {
            addr_4209_27:
            *reinterpret_cast<void***>(rdi + 44) = esi;
            return eax4;
        } else {
            eax5 = fun_2570();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_4209_27;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_4209_27;
        eax5 = fun_2570();
    }
    *reinterpret_cast<void***>(rdi + 44) = esi;
    return eax5;
}

void** fun_2650(void** rdi, void** rsi, void** rdx, ...);

void fun_2620(void** rdi, void** rsi, void** rdx);

void** fts_alloc(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    rax4 = fun_2650(reinterpret_cast<uint64_t>(rdx + 0x108) & 0xfffffffffffffff8, rsi, rdx);
    if (rax4) {
        fun_2620(rax4 + 0x100, rsi, rdx);
        rax5 = *reinterpret_cast<void***>(rdi + 32);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rdx) + 0x100) = 0;
        *reinterpret_cast<void***>(rax4 + 96) = rdx;
        *reinterpret_cast<void***>(rax4 + 80) = rdi;
        *reinterpret_cast<void***>(rax4 + 56) = rax5;
        *reinterpret_cast<void***>(rax4 + 64) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 24) = reinterpret_cast<void**>(0);
        *reinterpret_cast<unsigned char*>(rax4 + 0x6a) = reinterpret_cast<unsigned char>(0x30000);
        *reinterpret_cast<void***>(rax4 + 32) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 40) = reinterpret_cast<void**>(0);
    }
    return rax4;
}

void** g28;

int64_t free = 0;

void** hash_initialize(void** rdi);

struct s0 {
    signed char[8] pad8;
    void** f8;
};

struct s0* hash_lookup();

int32_t fun_2730();

void** hash_insert(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

uint32_t fun_2500();

void fun_23f0(void** rdi, ...);

void** filesystem_type(void** rdi, void** esi, ...) {
    void** rsi2;
    void** rsp3;
    void** r12_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** r13d8;
    int64_t r8_9;
    void** rax10;
    void** v11;
    struct s0* rax12;
    void** rax13;
    int32_t eax14;
    void** r12_15;
    void** v16;
    void** rax17;
    void** rax18;
    void** rax19;
    void* rdx20;
    int32_t eax21;
    void** v22;

    rsi2 = esi;
    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x98);
    r12_4 = *reinterpret_cast<void***>(rdi + 80);
    rax5 = g28;
    rbp6 = *reinterpret_cast<void***>(r12_4 + 80);
    if (!(*reinterpret_cast<unsigned char*>(r12_4 + 73) & 2)) 
        goto addr_4158_2;
    rbx7 = rdi;
    r13d8 = rsi2;
    if (!rbp6 && (r8_9 = free, rsi2 = reinterpret_cast<void**>(0), rdi = reinterpret_cast<void**>(13), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax10 = hash_initialize(13), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), *reinterpret_cast<void***>(r12_4 + 80) = rax10, rbp6 = rax10, !rax10) || (rsi2 = rsp3, rdi = rbp6, v11 = *reinterpret_cast<void***>(rbx7 + 0x70), rax12 = hash_lookup(), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), rax12 == 0)) {
        if (reinterpret_cast<signed char>(r13d8) < reinterpret_cast<signed char>(0)) {
            addr_4158_2:
            *reinterpret_cast<int32_t*>(&rax13) = 0;
            *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
        } else {
            rsi2 = rsp3 + 16;
            rdi = r13d8;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax14 = fun_2730();
            if (!eax14) {
                r12_15 = v16;
                if (!rbp6) {
                    addr_41c6_7:
                    rax13 = r12_15;
                } else {
                    rdi = reinterpret_cast<void**>(16);
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    rax17 = fun_2650(16, rsi2, 0x3d10, 16, rsi2, 0x3d10);
                    if (!rax17) 
                        goto addr_41c1_9;
                    rax18 = *reinterpret_cast<void***>(rbx7 + 0x70);
                    *reinterpret_cast<void***>(rax17 + 8) = r12_15;
                    rsi2 = rax17;
                    rdi = rbp6;
                    *reinterpret_cast<void***>(rax17) = rax18;
                    rax19 = hash_insert(rdi, rsi2, 0x3d10, 0x3d20, r8_9);
                    if (!rax19) 
                        goto addr_41d0_11; else 
                        goto addr_41b8_12;
                }
            } else {
                goto addr_4158_2;
            }
        }
    } else {
        rax13 = rax12->f8;
    }
    rdx20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (!rdx20) {
        return rax13;
    }
    fun_2500();
    if (*reinterpret_cast<void***>(rdi + 44) != rsi2) 
        goto addr_41fb_19;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) 
        goto addr_27d5_21;
    addr_41fb_19:
    if (*reinterpret_cast<signed char*>(&rdx20)) {
        eax21 = i_ring_push(rdi + 96);
        if (eax21 < 0) {
            addr_4209_23:
            *reinterpret_cast<void***>(rdi + 44) = rsi2;
            goto v11;
        } else {
            fun_2570();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_4209_23;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_4209_23;
        fun_2570();
    }
    *reinterpret_cast<void***>(rdi + 44) = rsi2;
    goto v11;
    addr_27d5_21:
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    fun_2410(rdi);
    addr_41d0_11:
    rdi = rax17;
    fun_23f0(rdi, rdi);
    goto addr_41c1_9;
    addr_41b8_12:
    if (rax17 != rax19) {
        fun_2410(rdi);
        goto addr_27d5_21;
    } else {
        addr_41c1_9:
        r12_15 = v22;
        goto addr_41c6_7;
    }
}

void** fun_26b0(void** rdi);

void fun_2460();

void** fts_sort(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rsi9;
    void** r8_10;
    void** rax11;
    void** rdx12;
    void** r8_13;
    void** rax14;
    void** rdx15;
    void** rsi16;
    void** rcx17;
    void** rdx18;

    r12_5 = rdi;
    rbp6 = rdx;
    rbx7 = rsi;
    rdi8 = *reinterpret_cast<void***>(rdi + 16);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 56)) < reinterpret_cast<unsigned char>(rdx)) {
        rsi9 = rdx + 40;
        r8_10 = rdi8;
        *reinterpret_cast<void***>(r12_5 + 56) = rsi9;
        if (reinterpret_cast<unsigned char>(rsi9) >> 61) {
            addr_3fd2_3:
            fun_23f0(r8_10);
            *reinterpret_cast<void***>(r12_5 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_5 + 56) = reinterpret_cast<void**>(0);
            return rbx7;
        } else {
            rax11 = fun_26b0(rdi8);
            rdi8 = rax11;
            if (!rax11) {
                r8_10 = *reinterpret_cast<void***>(r12_5 + 16);
                goto addr_3fd2_3;
            } else {
                *reinterpret_cast<void***>(r12_5 + 16) = rax11;
            }
        }
    }
    rdx12 = rdi8;
    if (rbx7) {
        do {
            *reinterpret_cast<void***>(rdx12) = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdx12 = rdx12 + 8;
        } while (rbx7);
    }
    fun_2460();
    r8_13 = *reinterpret_cast<void***>(r12_5 + 16);
    rax14 = *reinterpret_cast<void***>(r8_13);
    rdx15 = r8_13;
    rsi16 = rax14;
    rcx17 = rbp6 - 1;
    if (rcx17) {
        while (rdx15 = rdx15 + 8, *reinterpret_cast<void***>(rsi16 + 16) = *reinterpret_cast<void***>(rdx15 + 8), --rcx17, !!rcx17) {
            rsi16 = *reinterpret_cast<void***>(rdx15);
        }
        rdx18 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_13 + reinterpret_cast<unsigned char>(rbp6) * 8) - 8);
    } else {
        rdx18 = rax14;
    }
    *reinterpret_cast<void***>(rdx18 + 16) = reinterpret_cast<void**>(0);
    return rax14;
}

int32_t fun_26a0();

unsigned char i_ring_empty(void*** rdi, void** rsi, void** rdx);

void** i_ring_pop(void*** rdi, void** rsi, void** rdx);

uint32_t restore_initial_cwd(void** rdi, void** rsi) {
    void** eax3;
    uint32_t r12d4;
    int32_t eax5;
    void*** rbx6;
    unsigned char al7;
    void** eax8;

    eax3 = *reinterpret_cast<void***>(rdi + 72);
    r12d4 = reinterpret_cast<unsigned char>(eax3) & 4;
    if (r12d4) {
        r12d4 = 0;
    } else {
        if (!(*reinterpret_cast<unsigned char*>(&eax3 + 1) & 2)) {
            r12d4 = 0;
            eax5 = fun_26a0();
            *reinterpret_cast<unsigned char*>(&r12d4) = reinterpret_cast<uint1_t>(!!eax5);
        } else {
            rsi = reinterpret_cast<void**>(0xffffff9c);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            cwd_advance_fd(rdi, 0xffffff9c, 1);
        }
    }
    rbx6 = reinterpret_cast<void***>(rdi + 96);
    while (al7 = i_ring_empty(rbx6, rsi, 1), al7 == 0) {
        eax8 = i_ring_pop(rbx6, rsi, 1);
        if (reinterpret_cast<signed char>(eax8) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_2570();
    }
    return r12d4;
}

void** fun_2420();

unsigned char fts_palloc(void** rdi, void** rsi, void** rdx) {
    void** rsi4;
    void** rdi5;
    void** tmp64_6;
    void** rax7;
    void** rax8;
    void** rdi9;

    rsi4 = rsi + 0x100;
    rdi5 = *reinterpret_cast<void***>(rdi + 32);
    tmp64_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48)));
    if (reinterpret_cast<unsigned char>(tmp64_6) < reinterpret_cast<unsigned char>(rsi4)) {
        fun_23f0(rdi5);
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
        rax7 = fun_2420();
        *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(36);
        return 0;
    } else {
        *reinterpret_cast<void***>(rdi + 48) = tmp64_6;
        rax8 = fun_26b0(rdi5);
        if (!rax8) {
            rdi9 = *reinterpret_cast<void***>(rdi + 32);
            fun_23f0(rdi9);
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
            return 0;
        } else {
            *reinterpret_cast<void***>(rdi + 32) = rax8;
            return 1;
        }
    }
}

void cycle_check_init(void** rdi);

unsigned char setup_dir(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rax4 = fun_2650(32, rsi, rdx);
        *reinterpret_cast<void***>(rdi + 88) = rax4;
        if (!rax4) {
            return 0;
        } else {
            cycle_check_init(rax4);
            return 1;
        }
    } else {
        rax5 = hash_initialize(31);
        *reinterpret_cast<void***>(rdi + 88) = rax5;
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rax5));
    }
}

void** hash_remove(void** rdi, void** rsi);

void leave_dir(void** rdi, void** rsi, ...) {
    void** rax3;
    void** rdi4;
    void** v5;
    void** rax6;
    void** rdx7;
    void** rax8;
    void* rax9;
    void** eax10;
    void*** rbx11;
    unsigned char al12;
    void** eax13;

    rax3 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102) {
        rdi4 = *reinterpret_cast<void***>(rdi + 88);
        v5 = *reinterpret_cast<void***>(rsi + 0x70);
        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 40);
        rax6 = hash_remove(rdi4, rsi);
        rdi = rax6;
        if (!rax6) {
            addr_27da_3:
            fun_2410(rdi, rdi);
        } else {
            fun_23f0(rdi, rdi);
            goto addr_4428_5;
        }
    } else {
        if (*reinterpret_cast<void***>(rsi + 8) && reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 88)) >= reinterpret_cast<signed char>(0)) {
            rdx7 = *reinterpret_cast<void***>(rdi + 88);
            if (!*reinterpret_cast<void***>(rdx7 + 16)) 
                goto addr_27da_3;
            if (*reinterpret_cast<void***>(rdx7) == *reinterpret_cast<void***>(rsi + 0x78)) {
                if (*reinterpret_cast<void***>(rdx7 + 8) == *reinterpret_cast<void***>(rsi + 0x70)) {
                    rax8 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x78);
                    *reinterpret_cast<void***>(rdx7 + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x70);
                    *reinterpret_cast<void***>(rdx7) = rax8;
                    goto addr_4428_5;
                }
            } else {
                goto addr_4428_5;
            }
        }
    }
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    fun_2410(rdi, rdi);
    addr_4428_5:
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (!rax9) {
        return;
    }
    fun_2500();
    eax10 = *reinterpret_cast<void***>(rdi + 72);
    if (!(reinterpret_cast<unsigned char>(eax10) & 4)) 
        goto addr_44a6_36;
    addr_44c3_38:
    rbx11 = reinterpret_cast<void***>(rdi + 96);
    while (al12 = i_ring_empty(rbx11, rsi, rdx7), al12 == 0) {
        eax13 = i_ring_pop(rbx11, rsi, rdx7);
        if (reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_2570();
    }
    goto v5;
    addr_44a6_36:
    if (!(*reinterpret_cast<unsigned char*>(&eax10 + 1) & 2)) {
        fun_26a0();
        goto addr_44c3_38;
    } else {
        *reinterpret_cast<int32_t*>(&rdx7) = 1;
        *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
        rsi = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        cwd_advance_fd(rdi, 0xffffff9c, 1);
        goto addr_44c3_38;
    }
}

int32_t fun_27a0(int64_t rdi, void* rsi);

void** openat_safer(int64_t rdi, void** rsi);

void** open_safer(void** rdi, int64_t rsi);

int32_t fts_safe_changedir(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r14d6;
    void** r12_7;
    void** rbx8;
    void* rsp9;
    void** rax10;
    void** v11;
    uint32_t eax12;
    void** ebp13;
    unsigned char v14;
    uint32_t eax15;
    void** r13d16;
    void* rax17;
    uint32_t eax18;
    int32_t eax19;
    int64_t rdi20;
    int32_t eax21;
    void** v22;
    void** v23;
    void** rax24;
    void** rax25;
    int64_t rdi26;
    void** eax27;
    int64_t rsi28;
    void** eax29;
    void*** r13_30;
    unsigned char al31;
    void** eax32;

    r15_5 = rdi;
    r14d6 = rdx;
    r12_7 = rsi;
    rbx8 = rcx;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rax10 = g28;
    v11 = rax10;
    if (!rcx || ((eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)), eax12 != 46) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rcx + 1) == 46) || *reinterpret_cast<signed char*>(rcx + 2)))) {
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 4) {
            addr_46a0_3:
            ebp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) & 0x200);
            if (!ebp13 || reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
            } else {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                fun_2570();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            }
        } else {
            if (reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                v14 = 0;
                eax15 = reinterpret_cast<unsigned char>(ebp13) & 0x200;
                goto addr_47bc_8;
            } else {
                v14 = 0;
                r13d16 = r14d6;
                if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) 
                    goto addr_4668_10; else 
                    goto addr_458a_11;
            }
        }
    } else {
        ebp13 = *reinterpret_cast<void***>(rdi + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 4) 
            goto addr_46a0_3;
        if (reinterpret_cast<signed char>(rdx) >= reinterpret_cast<signed char>(0)) 
            goto addr_47f0_14;
        if (reinterpret_cast<unsigned char>(ebp13) & 0x200) 
            goto addr_4720_16; else 
            goto addr_4625_17;
    }
    addr_45c0_18:
    while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
        eax12 = fun_2500();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        addr_47f0_14:
        v14 = 1;
        r13d16 = rdx;
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) {
            goto addr_4668_10;
        }
        addr_4592_21:
        if (eax12 != 46 || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx8 + 1) == 46)) {
            addr_459b_22:
            ebp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) & 0x200);
            if (ebp13) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                eax18 = static_cast<uint32_t>(v14) ^ 1;
                rdx = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&eax18)));
                cwd_advance_fd(r15_5, r13d16, *reinterpret_cast<signed char*>(&rdx));
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                continue;
            } else {
                eax19 = fun_26a0();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                *reinterpret_cast<int32_t*>(&r12_7) = eax19;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) {
                    continue;
                }
            }
        } else {
            if (!*reinterpret_cast<signed char*>(rbx8 + 2)) {
                addr_4668_10:
                *reinterpret_cast<void***>(&rdi20) = r13d16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                eax21 = fun_27a0(rdi20, reinterpret_cast<int64_t>(rsp9) + 16);
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                if (eax21) {
                    addr_46c3_27:
                    *reinterpret_cast<int32_t*>(&r12_7) = -1;
                    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                    if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) 
                        continue;
                } else {
                    if (*reinterpret_cast<void***>(r12_7 + 0x70) != v22 || *reinterpret_cast<void***>(r12_7 + 0x78) != v23) {
                        rax24 = fun_2420();
                        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                        *reinterpret_cast<void***>(rax24) = reinterpret_cast<void**>(2);
                        goto addr_46c3_27;
                    } else {
                        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
                        goto addr_459b_22;
                    }
                }
            } else {
                goto addr_459b_22;
            }
        }
        rax25 = fun_2420();
        ebp13 = *reinterpret_cast<void***>(rax25);
        rbx8 = rax25;
        fun_2570();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
        *reinterpret_cast<void***>(rbx8) = ebp13;
    }
    return *reinterpret_cast<int32_t*>(&r12_7);
    addr_47bc_8:
    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r15_5 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
    if (eax15) {
        addr_4749_34:
        eax27 = openat_safer(rdi26, rbx8);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        r13d16 = eax27;
    } else {
        goto addr_463b_36;
    }
    addr_464a_37:
    if (reinterpret_cast<signed char>(r13d16) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<int32_t*>(&r12_7) = -1;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        goto addr_45c0_18;
    } else {
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) {
            goto addr_4668_10;
        }
    }
    addr_458a_11:
    if (!rbx8) 
        goto addr_459b_22;
    eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8));
    goto addr_4592_21;
    addr_463b_36:
    *reinterpret_cast<void***>(&rsi28) = rdx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi28) + 4) = 0;
    eax29 = open_safer(rbx8, rsi28);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    r13d16 = eax29;
    goto addr_464a_37;
    addr_4720_16:
    r13_30 = reinterpret_cast<void***>(rdi + 96);
    al31 = i_ring_empty(r13_30, rsi, rdx);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    v14 = al31;
    if (!al31) {
        eax32 = i_ring_pop(r13_30, rsi, rdx);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        r13d16 = eax32;
        if (reinterpret_cast<signed char>(eax32) < reinterpret_cast<signed char>(0)) {
            v14 = 1;
            eax15 = reinterpret_cast<unsigned char>(ebp13) & 0x200;
            goto addr_47bc_8;
        } else {
            v14 = 1;
            r14d6 = eax32;
            if (!(*reinterpret_cast<unsigned char*>(&ebp13) & 2)) 
                goto addr_459b_22;
            goto addr_4668_10;
        }
    } else {
        *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r15_5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
        goto addr_4749_34;
    }
    addr_4625_17:
    v14 = 1;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
    goto addr_463b_36;
}

void** opendirat(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

void** fun_25e0(void** rdi);

void fun_2580(void** rdi, void** rsi, ...);

void** fts_stat(void** rdi, void** rsi, signed char dl, ...);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void** rpl_fcntl();

struct s1 {
    signed char f0;
    void** f1;
};

struct s2 {
    void** f0;
    signed char[17] pad18;
    unsigned char f12;
    void** f13;
    signed char f14;
};

struct s2* fun_2640(void** rdi, void** rsi);

void** fun_24e0(void** rdi, ...);

void fun_26e0(void** rdi, void** rsi, void** rdx, ...);

void** fts_build(void** rdi, void** rsi) {
    void** r14_3;
    void** rbp4;
    void** v5;
    void** rax6;
    void** v7;
    void** rax8;
    void** v9;
    void** eax10;
    int64_t rdx11;
    uint32_t edx12;
    int64_t rdi13;
    void** rcx14;
    void** rax15;
    void** r13_16;
    void** eax17;
    void** v18;
    void** rdi19;
    void** rax20;
    void** rax21;
    void* rax22;
    int64_t r8_23;
    uint64_t rax24;
    void* rax25;
    void** v26;
    void** rax27;
    void** edi28;
    void** v29;
    int32_t r12d30;
    int32_t ebx31;
    void** rax32;
    void** eax33;
    void** rdx34;
    int32_t eax35;
    void** rax36;
    void** rdi37;
    void** edx38;
    unsigned char v39;
    void** rcx40;
    void** v41;
    void** v42;
    void** v43;
    struct s1* rax44;
    void** r15_45;
    void** rax46;
    int1_t zf47;
    void** v48;
    void** v49;
    signed char v50;
    void** r12_51;
    void** rax52;
    void** rdi53;
    void** v54;
    void** rbx55;
    unsigned char v56;
    void** v57;
    void** v58;
    void** v59;
    struct s2* rax60;
    void** rax61;
    void** r14_62;
    void** rax63;
    void** rsi64;
    void** rax65;
    void** r15_66;
    uint32_t eax67;
    void** tmp64_68;
    void** rsi69;
    void** rax70;
    void** edx71;
    void** rdx72;
    void** eax73;
    int64_t rax74;
    int64_t rdx75;
    void** eax76;
    void** rax77;
    void** rax78;
    uint32_t eax79;
    int32_t eax80;
    void** rax81;
    void** rax82;
    uint32_t eax83;
    void** rbp84;
    void** rdi85;
    void** rbp86;
    void** rdi87;
    uint64_t rax88;
    uint32_t eax89;
    void** rdi90;
    void** rax91;
    void** rax92;
    void** rdx93;
    void** r13_94;
    void** r14_95;
    void** rbp96;
    void** ebx97;
    void** r12_98;
    void** rdi99;
    void** rdi100;
    void** r13_101;
    void** rbp102;
    void** r14_103;
    void** r12_104;
    void** rdi105;
    void** rdi106;
    void** v107;
    void** v108;

    r14_3 = rdi;
    rbp4 = *reinterpret_cast<void***>(rdi);
    v5 = rsi;
    rax6 = g28;
    v7 = rax6;
    rax8 = *reinterpret_cast<void***>(rbp4 + 24);
    v9 = rax8;
    if (!rax8) {
        eax10 = *reinterpret_cast<void***>(rdi + 72);
        *reinterpret_cast<uint32_t*>(&rdx11) = reinterpret_cast<unsigned char>(eax10) & 16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rdx11) && (*reinterpret_cast<uint32_t*>(&rdx11) = 0x20000, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(&eax10) & 1))) {
            edx12 = 0;
            *reinterpret_cast<unsigned char*>(&edx12) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbp4 + 88));
            *reinterpret_cast<uint32_t*>(&rdx11) = edx12 << 17;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        }
        rsi = *reinterpret_cast<void***>(rbp4 + 48);
        *reinterpret_cast<void***>(&rdi13) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        if ((reinterpret_cast<unsigned char>(eax10) & 0x204) == 0x200) {
            *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(r14_3 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        }
        rcx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78 + 100);
        rax15 = opendirat(rdi13, rsi, rdx11, rcx14);
        *reinterpret_cast<void***>(rbp4 + 24) = rax15;
        r13_16 = rax15;
        if (rax15) 
            goto addr_4d0a_7;
    } else {
        eax17 = fun_25e0(rax8);
        v18 = eax17;
        if (reinterpret_cast<signed char>(eax17) < reinterpret_cast<signed char>(0)) {
            rdi19 = *reinterpret_cast<void***>(rbp4 + 24);
            fun_2580(rdi19, rsi);
            *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
            if (reinterpret_cast<int1_t>(v5 == 3)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
                rax20 = fun_2420();
                *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax20);
                goto addr_505a_11;
            }
        }
        if (!*reinterpret_cast<void***>(r14_3 + 64)) 
            goto addr_5100_13; else 
            goto addr_4879_14;
    }
    if (!reinterpret_cast<int1_t>(v5 == 3)) {
        addr_505a_11:
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    } else {
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
        rax21 = fun_2420();
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax21);
    }
    addr_4e71_17:
    rax22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (rax22) {
        fun_2500();
    } else {
        return r13_16;
    }
    addr_4d0a_7:
    if (*reinterpret_cast<uint16_t*>(rbp4 + 0x68) == 11) {
        rsi = rbp4;
        rax15 = fts_stat(r14_3, rsi, 0, r14_3, rsi, 0);
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&rax15);
        goto addr_4d20_22;
    }
    if (!(*reinterpret_cast<unsigned char*>(r14_3 + 73) & 1) || (leave_dir(r14_3, rbp4, r14_3, rbp4), fts_stat(r14_3, rbp4, 0, r14_3, rbp4, 0), rsi = rbp4, rax15 = enter_dir(r14_3, rsi, 0, rcx14, r8_23), !!*reinterpret_cast<signed char*>(&rax15))) {
        addr_4d20_22:
        rax24 = reinterpret_cast<unsigned char>(rax15) - (reinterpret_cast<unsigned char>(rax15) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax15) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax15) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 64)) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax25) = *reinterpret_cast<uint32_t*>(&rax24) & 0x186a1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax25) + 4) = 0;
        v26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax25) - 1);
        if (v5 == 2) 
            goto addr_50b0_24;
    } else {
        rax27 = fun_2420();
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
        *reinterpret_cast<void***>(rax27) = reinterpret_cast<void**>(12);
        goto addr_4e71_17;
    }
    edi28 = v29;
    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 56) != 24 || *reinterpret_cast<int64_t*>(rbp4 + 0x80) != 2) {
        addr_4d55_27:
        r12d30 = 1;
        *reinterpret_cast<unsigned char*>(&ebx31) = reinterpret_cast<uint1_t>(v5 == 3);
    } else {
        rsi = edi28;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rax32 = filesystem_type(rbp4, rsi, rbp4, rsi);
        if (rax32 == 0x9fa0) 
            goto addr_526a_29;
        if (reinterpret_cast<signed char>(rax32) > reinterpret_cast<signed char>(0x9fa0)) 
            goto addr_5254_31; else 
            goto addr_4edc_32;
    }
    addr_4d63_33:
    if (*reinterpret_cast<unsigned char*>(r14_3 + 73) & 2) {
        rsi = reinterpret_cast<void**>(0x406);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax33 = rpl_fcntl();
        v18 = eax33;
        edi28 = eax33;
    }
    if (reinterpret_cast<signed char>(edi28) < reinterpret_cast<signed char>(0) || (rdx34 = edi28, *reinterpret_cast<int32_t*>(&rdx34 + 4) = 0, rsi = rbp4, eax35 = fts_safe_changedir(r14_3, rsi, rdx34, 0), !!eax35)) {
        if (*reinterpret_cast<unsigned char*>(&ebx31) && *reinterpret_cast<signed char*>(&r12d30)) {
            rax36 = fun_2420();
            *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax36);
        }
        *reinterpret_cast<unsigned char*>(rbp4 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(rbp4 + 0x6a) | 1);
        rdi37 = *reinterpret_cast<void***>(rbp4 + 24);
        fun_2580(rdi37, rsi, rdi37, rsi);
        edx38 = *reinterpret_cast<void***>(r14_3 + 72);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        if (*reinterpret_cast<unsigned char*>(&edx38 + 1) & 2 && reinterpret_cast<signed char>(v18) >= reinterpret_cast<signed char>(0)) {
            fun_2570();
            edx38 = *reinterpret_cast<void***>(r14_3 + 72);
        }
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        v39 = 0;
    } else {
        goto addr_4882_42;
    }
    addr_488b_43:
    rcx40 = *reinterpret_cast<void***>(rbp4 + 72);
    v41 = rcx40;
    v42 = rcx40 + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 56)) + reinterpret_cast<unsigned char>(rcx40) + 0xffffffffffffffff) != 47) {
        v42 = rcx40;
        v41 = rcx40 + 1;
    }
    v43 = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(edx38) & 4) {
        rax44 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 32)));
        rcx40 = reinterpret_cast<void**>(&rax44->f1);
        rax44->f0 = 47;
        v43 = rcx40;
    }
    r15_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 48)) - reinterpret_cast<unsigned char>(v41));
    rax46 = *reinterpret_cast<void***>(rbp4 + 88) + 1;
    zf47 = *reinterpret_cast<void***>(rbp4 + 24) == 0;
    v48 = *reinterpret_cast<void***>(rbp4 + 24);
    v49 = rax46;
    if (zf47) {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4)) {
            *reinterpret_cast<int32_t*>(&r13_16) = 0;
            *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
            if (!(v39 & static_cast<unsigned char>(reinterpret_cast<uint1_t>(v9 == 0)))) 
                goto addr_5025_50;
            v50 = 0;
            *reinterpret_cast<int32_t*>(&r12_51) = 0;
            *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        } else {
            v50 = 0;
            *reinterpret_cast<int32_t*>(&r13_16) = 0;
            *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_51) = 0;
            *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
            goto addr_4f7b_53;
        }
    } else {
        rax52 = fun_2420();
        rdi53 = v48;
        *reinterpret_cast<int32_t*>(&r12_51) = 0;
        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        v54 = rax52;
        rbx55 = r14_3;
        v50 = 0;
        v56 = 0;
        v57 = reinterpret_cast<void**>(0);
        v58 = rbp4;
        v59 = reinterpret_cast<void**>(0);
        while (*reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(0), rax60 = fun_2640(rdi53, rsi), !!rax60) {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 72)) & 32) 
                goto addr_4992_57;
            if (*reinterpret_cast<void***>(&rax60->f13) != 46) 
                goto addr_4992_57;
            if (!rax60->f14) {
                addr_4954_60:
                rax61 = v58;
                rdi53 = *reinterpret_cast<void***>(rax61 + 24);
                if (!rdi53) 
                    goto addr_4aa8_61; else 
                    continue;
            } else {
                if (rax60->f14 != 46) {
                    addr_4992_57:
                    r14_62 = reinterpret_cast<void**>(&rax60->f13);
                    rax63 = fun_24e0(r14_62, r14_62);
                    rsi64 = r14_62;
                    rax65 = fts_alloc(rbx55, rsi64, rax63);
                    if (!rax65) 
                        goto addr_4df8_63;
                } else {
                    goto addr_4954_60;
                }
            }
            if (reinterpret_cast<unsigned char>(rax63) >= reinterpret_cast<unsigned char>(r15_45)) {
                r15_66 = *reinterpret_cast<void***>(rbx55 + 32);
                rsi64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(rax63) + 2);
                *reinterpret_cast<unsigned char*>(&eax67) = fts_palloc(rbx55, rsi64, rax63);
                if (!*reinterpret_cast<unsigned char*>(&eax67)) 
                    goto addr_4df8_63;
                rsi64 = *reinterpret_cast<void***>(rbx55 + 32);
                if (rsi64 != r15_66) 
                    goto addr_4b7b_68;
            } else {
                addr_49c4_69:
                tmp64_68 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax63) + reinterpret_cast<unsigned char>(v41));
                if (reinterpret_cast<unsigned char>(tmp64_68) < reinterpret_cast<unsigned char>(rax63)) 
                    goto addr_5273_70; else 
                    goto addr_49d2_71;
            }
            eax67 = v56;
            addr_4b8f_73:
            r15_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 48)) - reinterpret_cast<unsigned char>(v41));
            v56 = *reinterpret_cast<unsigned char*>(&eax67);
            goto addr_49c4_69;
            addr_4b7b_68:
            rsi64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi64) + reinterpret_cast<unsigned char>(v41));
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 72)) & 4)) {
                rsi64 = v43;
            }
            v43 = rsi64;
            goto addr_4b8f_73;
            addr_49d2_71:
            rsi69 = rax65 + 0x100;
            *reinterpret_cast<void***>(rax65 + 88) = v49;
            rax70 = *reinterpret_cast<void***>(rbx55);
            *reinterpret_cast<void***>(rax65 + 72) = tmp64_68;
            edx71 = *reinterpret_cast<void***>(rbx55 + 72);
            *reinterpret_cast<void***>(rax65 + 8) = rax70;
            *reinterpret_cast<void***>(rax65 + 0x78) = rax60->f0;
            if (*reinterpret_cast<unsigned char*>(&edx71) & 4) {
                *reinterpret_cast<void***>(rax65 + 48) = *reinterpret_cast<void***>(rax65 + 56);
                rdx72 = *reinterpret_cast<void***>(rax65 + 96) + 1;
                fun_26e0(v43, rsi69, rdx72);
                edx71 = *reinterpret_cast<void***>(rbx55 + 72);
            } else {
                *reinterpret_cast<void***>(rax65 + 48) = rsi69;
            }
            if (!*reinterpret_cast<void***>(rbx55 + 64) || *reinterpret_cast<unsigned char*>(&edx71 + 1) & 4) {
                eax73 = reinterpret_cast<void**>(static_cast<uint32_t>(rax60->f12));
                rsi = eax73;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                *reinterpret_cast<void***>(&rax74) = eax73 - 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                if (!(*reinterpret_cast<unsigned char*>(&edx71) & 8) || !(*reinterpret_cast<unsigned char*>(&rsi) & 0xfb)) {
                    rsi = reinterpret_cast<void**>(11);
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax74)) <= reinterpret_cast<unsigned char>(11)) {
                        addr_4bb8_81:
                        *reinterpret_cast<int32_t*>(&rdx75) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
                    } else {
                        eax76 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx75) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
                        goto addr_4a48_83;
                    }
                } else {
                    if (!(reinterpret_cast<unsigned char>(edx71) & 16) && *reinterpret_cast<unsigned char*>(&rsi) == 10) {
                        *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                        goto addr_4bb8_81;
                    }
                    *reinterpret_cast<int32_t*>(&rcx40) = 11;
                    *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax74)) <= reinterpret_cast<unsigned char>(11)) 
                        goto addr_4dd0_87; else 
                        goto addr_4c62_88;
                }
            } else {
                rsi = rax65;
                rax77 = fts_stat(rbx55, rsi, 0);
                *reinterpret_cast<uint16_t*>(rax65 + 0x68) = *reinterpret_cast<uint16_t*>(&rax77);
                goto addr_4a56_90;
            }
            addr_4bbd_91:
            rcx40 = reinterpret_cast<void**>(0xca00);
            eax76 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0xca00) + reinterpret_cast<uint64_t>(rax74 * 4));
            addr_4a48_83:
            *reinterpret_cast<void***>(rax65 + 0x88) = eax76;
            *reinterpret_cast<int64_t*>(rax65 + 0xa0) = rdx75;
            addr_4a56_90:
            *reinterpret_cast<void***>(rax65 + 16) = reinterpret_cast<void**>(0);
            if (!v59) {
                v59 = rax65;
            } else {
                *reinterpret_cast<void***>(v57 + 16) = rax65;
            }
            if (!reinterpret_cast<int1_t>(r12_51 == 0x2710)) {
                ++r12_51;
                if (reinterpret_cast<unsigned char>(r12_51) >= reinterpret_cast<unsigned char>(v26)) 
                    goto addr_5068_96;
                v57 = rax65;
                goto addr_4954_60;
            } else {
                if (!*reinterpret_cast<void***>(rbx55 + 64)) {
                    rsi = v18;
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    rax78 = filesystem_type(v58, rsi);
                    if (rax78 == 0x1021994 || ((*reinterpret_cast<int32_t*>(&rcx40) = 0xff534d42, *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0, rax78 == 0xff534d42) || rax78 == 0x6969)) {
                        v57 = rax65;
                        *reinterpret_cast<int32_t*>(&r12_51) = 0x2711;
                        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
                        v50 = 0;
                        goto addr_4954_60;
                    } else {
                        v50 = 1;
                        goto addr_4a8b_102;
                    }
                } else {
                    addr_4a8b_102:
                    rax61 = v58;
                    v57 = rax65;
                    *reinterpret_cast<int32_t*>(&r12_51) = 0x2711;
                    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
                    rdi53 = *reinterpret_cast<void***>(rax61 + 24);
                    if (rdi53) 
                        continue; else 
                        goto addr_4aa8_61;
                }
            }
            addr_4dd0_87:
            *reinterpret_cast<int32_t*>(&rdx75) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            goto addr_4bbd_91;
            addr_4c62_88:
            eax76 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx75) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            goto addr_4a48_83;
        }
        goto addr_4f10_103;
    }
    addr_5000_104:
    if (!*reinterpret_cast<void***>(rbp4 + 88)) {
        eax79 = restore_initial_cwd(r14_3, rsi);
        if (eax79) 
            goto addr_514d_106;
        goto addr_5020_108;
    }
    rsi = *reinterpret_cast<void***>(rbp4 + 8);
    rcx40 = reinterpret_cast<void**>("..");
    eax80 = fts_safe_changedir(r14_3, rsi, 0xffffffff, "..");
    if (!eax80) {
        addr_5020_108:
        if (r12_51) {
            addr_4fac_110:
            if (v50) {
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0x3d30);
                rax81 = fts_sort(r14_3, r13_16, r12_51, rcx40);
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0);
                r13_16 = rax81;
                goto addr_4e71_17;
            } else {
                if (*reinterpret_cast<void***>(r14_3 + 64) && r12_51 != 1) {
                    rax82 = fts_sort(r14_3, r13_16, r12_51, rcx40);
                    r13_16 = rax82;
                    goto addr_4e71_17;
                }
            }
        } else {
            addr_5025_50:
            if (v5 == 3 && ((eax83 = *reinterpret_cast<uint16_t*>(rbp4 + 0x68), *reinterpret_cast<int16_t*>(&eax83) != 7) && *reinterpret_cast<int16_t*>(&eax83) != 4)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 6;
            }
        }
    } else {
        addr_514d_106:
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 7;
        *reinterpret_cast<void***>(r14_3 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) | 0x2000);
        if (r13_16) {
            do {
                rbp84 = r13_16;
                r13_16 = *reinterpret_cast<void***>(r13_16 + 16);
                rdi85 = *reinterpret_cast<void***>(rbp84 + 24);
                if (rdi85) {
                    fun_2580(rdi85, rsi, rdi85, rsi);
                }
                fun_23f0(rbp84, rbp84);
            } while (r13_16);
            goto addr_505a_11;
        }
    }
    if (r13_16) {
        do {
            rbp86 = r13_16;
            r13_16 = *reinterpret_cast<void***>(r13_16 + 16);
            rdi87 = *reinterpret_cast<void***>(rbp86 + 24);
            if (rdi87) {
                fun_2580(rdi87, rsi, rdi87, rsi);
            }
            fun_23f0(rbp86, rbp86);
        } while (r13_16);
        goto addr_505a_11;
    }
    addr_4f10_103:
    rbp4 = v58;
    r14_3 = rbx55;
    r13_16 = v59;
    if (*reinterpret_cast<void***>(v54)) {
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(v54);
        rax88 = reinterpret_cast<unsigned char>(v9) | reinterpret_cast<unsigned char>(r12_51);
        eax89 = (*reinterpret_cast<uint32_t*>(&rax88) - (*reinterpret_cast<uint32_t*>(&rax88) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax88) < *reinterpret_cast<uint32_t*>(&rax88) + reinterpret_cast<uint1_t>(rax88 < 1))) & 0xfffffffd) + 7;
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&eax89);
    }
    rdi90 = *reinterpret_cast<void***>(rbp4 + 24);
    if (rdi90) {
        fun_2580(rdi90, rsi);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
    }
    addr_4f59_128:
    if (v56) {
        addr_4abe_129:
        rax91 = *reinterpret_cast<void***>(r14_3 + 8);
        rcx40 = *reinterpret_cast<void***>(r14_3 + 32);
        if (rax91) {
            do {
                rsi = rax91 + 0x100;
                if (*reinterpret_cast<void***>(rax91 + 48) != rsi) {
                    *reinterpret_cast<void***>(rax91 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax91 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax91 + 56))) + reinterpret_cast<unsigned char>(rcx40));
                }
                *reinterpret_cast<void***>(rax91 + 56) = rcx40;
                rax91 = *reinterpret_cast<void***>(rax91 + 16);
            } while (rax91);
        }
    } else {
        addr_4f64_134:
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4) {
            if (*reinterpret_cast<void***>(r14_3 + 48) == v41 || !r12_51) {
                addr_4f7b_53:
                --v43;
                goto addr_4f81_136;
            } else {
                addr_4f81_136:
                *reinterpret_cast<void***>(v43) = reinterpret_cast<void**>(0);
                goto addr_4f89_137;
            }
        }
    }
    rax92 = r13_16;
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r13_16 + 88)) >= reinterpret_cast<signed char>(0)) {
        do {
            rsi = rax92 + 0x100;
            if (*reinterpret_cast<void***>(rax92 + 48) != rsi) {
                *reinterpret_cast<void***>(rax92 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax92 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax92 + 56))) + reinterpret_cast<unsigned char>(rcx40));
            }
            rdx93 = *reinterpret_cast<void***>(rax92 + 16);
            *reinterpret_cast<void***>(rax92 + 56) = rcx40;
            if (rdx93) {
                rax92 = rdx93;
            } else {
                rax92 = *reinterpret_cast<void***>(rax92 + 8);
            }
        } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax92 + 88)) >= reinterpret_cast<signed char>(0));
        goto addr_4f64_134;
    } else {
        goto addr_4f64_134;
    }
    addr_4f89_137:
    if (v9) 
        goto addr_5020_108;
    if (!v39) 
        goto addr_5020_108;
    if (v5 == 1) 
        goto addr_5000_104;
    if (!r12_51) 
        goto addr_5000_104; else 
        goto addr_4fac_110;
    addr_4df8_63:
    r13_94 = v59;
    r14_95 = rbx55;
    rbp96 = v58;
    ebx97 = *reinterpret_cast<void***>(v54);
    fun_23f0(rax65, rax65);
    if (r13_94) {
        do {
            r12_98 = r13_94;
            r13_94 = *reinterpret_cast<void***>(r13_94 + 16);
            rdi99 = *reinterpret_cast<void***>(r12_98 + 24);
            if (rdi99) {
                fun_2580(rdi99, rsi64, rdi99, rsi64);
            }
            fun_23f0(r12_98, r12_98);
        } while (r13_94);
    }
    rdi100 = *reinterpret_cast<void***>(rbp96 + 24);
    *reinterpret_cast<int32_t*>(&r13_16) = 0;
    *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    fun_2580(rdi100, rsi64, rdi100, rsi64);
    *reinterpret_cast<void***>(rbp96 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp96 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_95 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_95 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v54) = ebx97;
    goto addr_4e71_17;
    addr_5273_70:
    r13_101 = v59;
    rbp102 = v58;
    r14_103 = rbx55;
    fun_23f0(rax65, rax65);
    if (r13_101) {
        do {
            r12_104 = r13_101;
            r13_101 = *reinterpret_cast<void***>(r13_101 + 16);
            rdi105 = *reinterpret_cast<void***>(r12_104 + 24);
            if (rdi105) {
                fun_2580(rdi105, rsi64);
            }
            fun_23f0(r12_104, r12_104);
        } while (r13_101);
    }
    rdi106 = *reinterpret_cast<void***>(rbp102 + 24);
    *reinterpret_cast<int32_t*>(&r13_16) = 0;
    *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    fun_2580(rdi106, rsi64);
    *reinterpret_cast<void***>(rbp102 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp102 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_103 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_103 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(36);
    goto addr_4e71_17;
    addr_5068_96:
    rbp4 = v58;
    r13_16 = v59;
    r14_3 = rbx55;
    goto addr_4f59_128;
    addr_4aa8_61:
    r13_16 = v59;
    rbp4 = rax61;
    r14_3 = rbx55;
    if (!v56) 
        goto addr_4f64_134; else 
        goto addr_4abe_129;
    addr_4882_42:
    v39 = 1;
    edx38 = *reinterpret_cast<void***>(r14_3 + 72);
    goto addr_488b_43;
    addr_5254_31:
    if (rax32 == 0x5346414f || reinterpret_cast<int1_t>(rax32 == 0xff534d42)) {
        addr_526a_29:
        edi28 = v107;
        goto addr_4d55_27;
    } else {
        addr_4ef1_158:
        if (!reinterpret_cast<int1_t>(v5 == 3)) {
            addr_50b0_24:
            v39 = 0;
            edx38 = *reinterpret_cast<void***>(r14_3 + 72);
            goto addr_488b_43;
        } else {
            edi28 = v108;
            r12d30 = 0;
            ebx31 = 1;
            goto addr_4d63_33;
        }
    }
    addr_4edc_32:
    if (!rax32) 
        goto addr_526a_29;
    if (rax32 == 0x6969) 
        goto addr_526a_29; else 
        goto addr_4ef1_158;
    addr_5100_13:
    v26 = reinterpret_cast<void**>(0x186a0);
    edx38 = *reinterpret_cast<void***>(r14_3 + 72);
    v39 = 1;
    goto addr_488b_43;
    addr_4879_14:
    v26 = reinterpret_cast<void**>(0xffffffffffffffff);
    goto addr_4882_42;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0xcaa0);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0xcaa0);
    if (*reinterpret_cast<void***>(rdi + 40) != 0xcaa0) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x67c8]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0xcaa0);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x662f]");
        if (1) 
            goto addr_657c_6;
        __asm__("comiss xmm1, [rip+0x6626]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x6618]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_655c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_6552_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_655c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_6552_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_6552_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_655c_13:
        return 0;
    } else {
        addr_657c_6:
        return r8_3;
    }
}

struct s4 {
    signed char[16] pad16;
    unsigned char f10;
};

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    signed char[8] pad40;
    struct s4* f28;
    int64_t f30;
    signed char[16] pad72;
    void** f48;
};

struct s5 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s6 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int32_t transfer_entries(struct s3* rdi, struct s3* rsi, int32_t edx) {
    void** rdx3;
    struct s3* r14_4;
    int32_t r12d5;
    struct s3* rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rdi12;
    void** rax13;
    struct s5* rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s6* r13_18;
    void** rax19;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = rsi->f0;
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(rsi->f8)) {
        do {
            addr_65e6_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_65d8_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(rbp6->f8) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_65e6_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = r14_4->f10;
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rdi12 = r15_11, rax13 = reinterpret_cast<void**>(r14_4->f30(rdi12, rsi10)), rsi10 = r14_4->f10, reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax14 = reinterpret_cast<struct s5*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax14->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14->f8;
                            rax14->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_665e_9;
                        } else {
                            rax14->f0 = r15_11;
                            rax15 = r14_4->f48;
                            r14_4->f18 = r14_4->f18 + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            r14_4->f48 = r13_9;
                            if (!rdx3) 
                                goto addr_665e_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_27e9_12;
                    addr_665e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_65d8_3;
            }
            rsi16 = r14_4->f10;
            rdi12 = r15_8;
            rax17 = reinterpret_cast<void**>(r14_4->f30(rdi12, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(r14_4->f10)) 
                goto addr_27e9_12;
            r13_18 = reinterpret_cast<struct s6*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(r14_4->f0));
            if (r13_18->f0) 
                goto addr_6698_16;
            r13_18->f0 = r15_8;
            r14_4->f18 = r14_4->f18 + 1;
            continue;
            addr_6698_16:
            rax19 = r14_4->f48;
            if (!rax19) {
                rax19 = fun_2650(16, rsi16, rdx3);
                if (!rax19) 
                    goto addr_670a_19;
            } else {
                r14_4->f48 = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx3 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx3;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            rbp6->f18 = rbp6->f18 - 1;
        } while (reinterpret_cast<unsigned char>(rbp6->f8) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_27e9_12:
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    fun_2410(rdi12, rdi12);
    addr_670a_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
        fun_2410(r13_5, r13_5);
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_640f_23:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_63b4_26;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_6420_30;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_6420_30;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_640f_23;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_63b4_26;
            }
        }
    }
    addr_6411_34:
    return rax11;
    addr_63b4_26:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_6411_34;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_6420_30:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_24d0();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24d0();
    if (r8d > 10) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xcc20 + rax11 * 4) + 0xcc20;
    }
}

struct s7 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

void fun_2560();

struct s8 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s7* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s8* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x919f;
    rax8 = fun_2420();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x11090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7d51]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x922b;
            fun_2560();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s8*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x11140) {
                fun_23f0(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x92ba);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2500();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x110a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

uint32_t fun_27b0(int64_t rdi, void** rsi, void** rdx, ...);

void** fts_stat(void** rdi, void** rsi, signed char dl, ...) {
    void** rbp4;
    void** eax5;
    void** rsi6;
    int64_t rdi7;
    uint32_t eax8;
    void** rax9;
    void** eax10;
    int64_t rdi11;
    uint32_t eax12;
    uint32_t eax13;
    void** rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    void** rax18;
    int64_t* rdi19;
    int64_t rcx20;

    rbp4 = rsi + 0x70;
    eax5 = *reinterpret_cast<void***>(rdi + 72);
    if (*reinterpret_cast<unsigned char*>(&eax5) & 2) 
        goto addr_3d80_2;
    if (*reinterpret_cast<unsigned char*>(&eax5) & 1 && !*reinterpret_cast<void***>(rsi + 88)) {
        goto addr_3d80_2;
    }
    if (dl) {
        addr_3d80_2:
        rsi6 = *reinterpret_cast<void***>(rsi + 48);
        *reinterpret_cast<void***>(&rdi7) = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        eax8 = fun_27b0(rdi7, rsi6, rbp4);
        if (reinterpret_cast<int32_t>(eax8) < reinterpret_cast<int32_t>(0)) {
            rax9 = fun_2420();
            eax10 = *reinterpret_cast<void***>(rax9);
            if (eax10 == 2) {
                rsi6 = *reinterpret_cast<void***>(rsi + 48);
                *reinterpret_cast<void***>(&rdi11) = *reinterpret_cast<void***>(rdi + 44);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
                eax12 = fun_27b0(rdi11, rsi6, rbp4, rdi11, rsi6, rbp4);
                if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0)) {
                    eax10 = *reinterpret_cast<void***>(rax9);
                } else {
                    *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(0);
                    return 13;
                }
            }
        } else {
            addr_3d97_10:
            eax13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x88)) & reinterpret_cast<uint32_t>("!");
            if (eax13 == 0x4000) {
                *reinterpret_cast<uint32_t*>(&rax14) = 1;
                *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi + 0x100) == 46) && (!*reinterpret_cast<signed char*>(rsi + 0x101) || (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x100)) & 0xffff00) == 0x2e00)) {
                    *reinterpret_cast<uint32_t*>(&rax14) = (1 - (1 + reinterpret_cast<uint1_t>(1 < 1 + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 88)) < reinterpret_cast<unsigned char>(1)))) & 0xfffffffc) + 5;
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_3dc7_13;
                }
            } else {
                if (eax13 == 0xa000) {
                    *reinterpret_cast<uint32_t*>(&rax14) = 12;
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_3dc7_13;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = reinterpret_cast<uint1_t>(eax13 == 0x8000);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax14) = static_cast<uint32_t>(rax15 + rax15 * 4 + 3);
                    *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                    goto addr_3dc7_13;
                }
            }
        }
    } else {
        rsi6 = *reinterpret_cast<void***>(rsi + 48);
        *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
        eax17 = fun_27b0(rdi16, rsi6, rbp4, rdi16, rsi6, rbp4);
        if (reinterpret_cast<int32_t>(eax17) >= reinterpret_cast<int32_t>(0)) 
            goto addr_3d97_10;
        rax18 = fun_2420();
        eax10 = *reinterpret_cast<void***>(rax18);
    }
    *reinterpret_cast<void***>(rsi + 64) = eax10;
    rdi19 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp4 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(rsi + 0x70) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(rbp4 + 0x88) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rcx20) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(rbp4) - reinterpret_cast<uint64_t>(rdi19) + 0x90) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
    while (rcx20) {
        --rcx20;
        *rdi19 = 0;
        ++rdi19;
    }
    return 10;
    addr_3dc7_13:
    return rax14;
}

void** cycle_check(void** rdi, void*** rsi);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8) {
    void** rdi6;
    void** rax7;
    void** rax8;
    void** rax9;
    void** rdi10;
    void** rax11;
    void** rax12;
    void** rax13;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rdi6 = *reinterpret_cast<void***>(rdi + 88);
        rax7 = cycle_check(rdi6, rsi + 0x70);
        if (*reinterpret_cast<signed char*>(&rax7)) {
            *reinterpret_cast<void***>(rsi) = rsi;
            *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
            return rax7;
        }
    } else {
        rax8 = fun_2650(24, rsi, rdx);
        if (!rax8) 
            goto addr_43d8_5;
        rax9 = *reinterpret_cast<void***>(rsi + 0x70);
        rdi10 = *reinterpret_cast<void***>(rdi + 88);
        *reinterpret_cast<void***>(rax8 + 16) = rsi;
        *reinterpret_cast<void***>(rax8) = rax9;
        *reinterpret_cast<void***>(rax8 + 8) = *reinterpret_cast<void***>(rsi + 0x78);
        rax11 = hash_insert(rdi10, rax8, rdx, rcx, r8);
        if (rax8 != rax11) 
            goto addr_4383_7;
    }
    addr_43a0_8:
    *reinterpret_cast<int32_t*>(&rax12) = 1;
    *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
    addr_43a5_9:
    return rax12;
    addr_4383_7:
    fun_23f0(rax8, rax8);
    if (!rax11) {
        addr_43d8_5:
        *reinterpret_cast<int32_t*>(&rax12) = 0;
        *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
        goto addr_43a5_9;
    } else {
        rax13 = *reinterpret_cast<void***>(rax11 + 16);
        *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
        *reinterpret_cast<void***>(rsi) = rax13;
        goto addr_43a0_8;
    }
}

struct s9 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s9* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s9* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xcbab);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xcba4);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xcbaf);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xcba0);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g10dd8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g10dd8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto free;
}

int64_t __cxa_finalize = 0;

void fun_2403() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t abort = 0x2030;

void fun_2413() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2040;

void fun_2423() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2050;

void fun_2433() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2060;

void fun_2443() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2070;

void fun_2453() {
    __asm__("cli ");
    goto __fpending;
}

int64_t qsort = 0x2080;

void fun_2463() {
    __asm__("cli ");
    goto qsort;
}

int64_t reallocarray = 0x2090;

void fun_2473() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20a0;

void fun_2483() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x20b0;

void fun_2493() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20c0;

void fun_24a3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20d0;

void fun_24b3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20e0;

void fun_24c3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x20f0;

void fun_24d3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2100;

void fun_24e3() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x2110;

void fun_24f3() {
    __asm__("cli ");
    goto openat;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2503() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2513() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2523() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strrchr = 0x2150;

void fun_2533() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2160;

void fun_2543() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2170;

void fun_2553() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2180;

void fun_2563() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x2190;

void fun_2573() {
    __asm__("cli ");
    goto close;
}

int64_t closedir = 0x21a0;

void fun_2583() {
    __asm__("cli ");
    goto closedir;
}

int64_t lstat = 0x21b0;

void fun_2593() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x21c0;

void fun_25a3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21d0;

void fun_25b3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21e0;

void fun_25c3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21f0;

void fun_25d3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t dirfd = 0x2200;

void fun_25e3() {
    __asm__("cli ");
    goto dirfd;
}

int64_t fputc_unlocked = 0x2210;

void fun_25f3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t umask = 0x2220;

void fun_2603() {
    __asm__("cli ");
    goto umask;
}

int64_t stat = 0x2230;

void fun_2613() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2240;

void fun_2623() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2250;

void fun_2633() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x2260;

void fun_2643() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x2270;

void fun_2653() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2280;

void fun_2663() {
    __asm__("cli ");
    goto fflush;
}

int64_t fchmodat = 0x2290;

void fun_2673() {
    __asm__("cli ");
    goto fchmodat;
}

int64_t nl_langinfo = 0x22a0;

void fun_2683() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22b0;

void fun_2693() {
    __asm__("cli ");
    goto __freading;
}

int64_t fchdir = 0x22c0;

void fun_26a3() {
    __asm__("cli ");
    goto fchdir;
}

int64_t realloc = 0x22d0;

void fun_26b3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22e0;

void fun_26c3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22f0;

void fun_26d3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t memmove = 0x2300;

void fun_26e3() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2310;

void fun_26f3() {
    __asm__("cli ");
    goto error;
}

int64_t open = 0x2320;

void fun_2703() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x2330;

void fun_2713() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fdopendir = 0x2340;

void fun_2723() {
    __asm__("cli ");
    goto fdopendir;
}

int64_t fstatfs = 0x2350;

void fun_2733() {
    __asm__("cli ");
    goto fstatfs;
}

int64_t __cxa_atexit = 0x2360;

void fun_2743() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2370;

void fun_2753() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2380;

void fun_2763() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2390;

void fun_2773() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x23a0;

void fun_2783() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23b0;

void fun_2793() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x23c0;

void fun_27a3() {
    __asm__("cli ");
    goto fstat;
}

int64_t fstatat = 0x23d0;

void fun_27b3() {
    __asm__("cli ");
    goto fstatat;
}

int64_t __ctype_b_loc = 0x23e0;

void fun_27c3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_26c0(int64_t rdi, ...);

void fun_24b0(int64_t rdi, int64_t rsi);

struct s10 {
    signed char[44] pad44;
    int32_t f2c;
};

void fun_2490(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

signed char diagnose_surprises = 0;

unsigned char force_silent = 0;

signed char recurse = 0;

struct s10* quote(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_24c0();

void fun_26f0();

int64_t usage();

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, ...);

int32_t optind = 0;

void** mode_compile(void** rdi, void** rsi);

void** change = reinterpret_cast<void**>(0);

int32_t fun_2600();

int32_t umask_value = 0;

struct s11 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s11* get_root_dev_ino(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

struct s11* root_dev_ino = reinterpret_cast<struct s11*>(0);

void** optarg = reinterpret_cast<void**>(0);

int64_t fun_2510(int64_t rdi);

int32_t verbosity = 2;

int64_t stdout = 0;

int64_t Version = 0xc9c4;

void version_etc(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, void** r9, int64_t a7, void** a8);

void fun_2750();

void** x2realloc(void** rdi, void* rsi);

void** mode_create_from_ref(void** rdi);

struct s10* xfts_open(void*** rdi, void** rsi);

void** rpl_fts_read(struct s10* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** mode_adjust(int64_t rdi, void** rsi, ...);

uint32_t fun_2670(int64_t rdi, void** rsi, void** rdx);

int32_t fun_25d0(void** rdi, int64_t rsi, void** rdx);

void** quotearg_n_style();

void rpl_fts_set(struct s10* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7);

void strmode();

void fun_26d0(int64_t rdi, void** rsi, void** rdx, ...);

void** quotearg_n_style_colon();

int32_t rpl_fts_close(struct s10* rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_2853(int32_t edi, void** rsi) {
    void** r15_3;
    void** r14_4;
    void** r13_5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** v9;
    struct s10* r12_10;
    void* rsp11;
    void** v12;
    void** v13;
    void** v14;
    void** rsi15;
    void** rdx16;
    void** rcx17;
    void** r8_18;
    void** r9_19;
    int64_t rax20;
    uint32_t eax21;
    void** rax22;
    void** rax23;
    void** rax24;
    int32_t eax25;
    void** rax26;
    int32_t eax27;
    void* rsp28;
    int1_t zf29;
    struct s11* rax30;
    void** rax31;
    void** rax32;
    void** rax33;
    void** rcx34;
    void** rdi35;
    struct s10* rax36;
    int64_t rax37;
    void** rax38;
    int64_t rdi39;
    int32_t ecx40;
    int64_t rdx41;
    int64_t rdi42;
    int64_t rcx43;
    int64_t rdx44;
    int64_t rax45;
    void** rdi46;
    void** v47;
    void** rax48;
    void* rsp49;
    void** rdx50;
    void* rcx51;
    void** r8_52;
    void** rax53;
    int1_t less_or_equal54;
    void** rax55;
    int64_t rax56;
    void** rdx57;
    struct s10* rax58;
    void* rsp59;
    void** rax60;
    void** rbx61;
    struct s11* rax62;
    uint32_t eax63;
    uint32_t ebx64;
    void** rsi65;
    int64_t rdi66;
    void** eax67;
    int64_t rdi68;
    int1_t zf69;
    void** rax70;
    void** rax71;
    void** rax72;
    int1_t zf73;
    int64_t rdi74;
    void** eax75;
    int32_t eax76;
    void* rsp77;
    void** rax78;
    void** rax79;
    void* rsp80;
    void** rax81;
    int64_t v82;
    int1_t zf83;
    int1_t zf84;
    void** rax85;
    void* rsp86;
    void** r9_87;
    void** rax88;
    void* rsp89;
    void** r11_90;
    void** rax91;
    void*** rsp92;
    void*** rsp93;
    int1_t zf94;
    int64_t v95;
    int1_t zf96;
    int64_t rdi97;
    void** eax98;
    void** rax99;
    void** rax100;
    void* rsp101;
    int1_t zf102;
    void** rax103;
    void** rax104;
    void** rax105;
    void** v106;
    int1_t zf107;
    void** rax108;
    void** rax109;
    void* rsp110;
    void** rax111;
    void** r9_112;
    void** rax113;
    void* rsp114;
    void** rax115;
    int32_t eax116;
    void* rsp117;

    __asm__("cli ");
    r15_3 = reinterpret_cast<void**>(0x40200000041fe3);
    r14_4 = reinterpret_cast<void**>(0x10a20);
    r13_5 = reinterpret_cast<void**>("Rcfvr::w::x::X::s::t::u::g::o::a::,::+::=::0::1::2::3::4::5::6::7::");
    rbp6 = reinterpret_cast<void**>(static_cast<int64_t>(edi));
    rbx7 = rsi;
    rdi8 = *reinterpret_cast<void***>(rsi);
    v9 = reinterpret_cast<void**>(0);
    set_program_name(rdi8);
    fun_26c0(6, 6);
    fun_24b0("coreutils", "/usr/local/share/locale");
    *reinterpret_cast<int32_t*>(&r12_10) = 0;
    fun_2490("coreutils", "/usr/local/share/locale");
    atexit(0x3a80, "/usr/local/share/locale");
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x108 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    diagnose_surprises = 0;
    force_silent = 0;
    recurse = 0;
    v12 = reinterpret_cast<void**>(0);
    v13 = reinterpret_cast<void**>(0);
    v14 = reinterpret_cast<void**>(0);
    goto addr_2918_2;
    addr_3532_3:
    quote(v14, rsi15, rdx16, rcx17, r8_18, r9_19);
    fun_24c0();
    fun_26f0();
    usage();
    addr_2be2_4:
    *reinterpret_cast<uint32_t*>(&rax20) = *reinterpret_cast<uint16_t*>(&eax21);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax20) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xc994 + rax20 * 4) + 0xc994;
    addr_34f1_5:
    rax22 = quotearg_style(4, v12, rdx16, 4, v12, rdx16);
    rax23 = fun_24c0();
    rax24 = fun_2420();
    rcx17 = rax22;
    rdx16 = rax23;
    rsi15 = *reinterpret_cast<void***>(rax24);
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    fun_26f0();
    goto addr_3532_3;
    while (1) {
        addr_305d_6:
        rdx16 = reinterpret_cast<void**>(static_cast<int64_t>(optind));
        if (!v14) {
            rcx17 = *reinterpret_cast<void***>(rbx7 + reinterpret_cast<unsigned char>(rdx16) * 8);
            eax25 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx16 + 1));
            optind = eax25;
            v14 = rcx17;
            if (eax25 < *reinterpret_cast<int32_t*>(&rbp6)) {
                addr_3286_8:
                rax26 = mode_compile(v14, rsi15);
                change = rax26;
                if (!rax26) 
                    goto addr_3532_3;
            } else {
                if (!rcx17) {
                    goto addr_308c_11;
                }
            }
        } else {
            eax25 = *reinterpret_cast<int32_t*>(&rdx16);
            if (*reinterpret_cast<int32_t*>(&rbp6) > *reinterpret_cast<int32_t*>(&rdx16)) 
                goto addr_3286_8; else 
                goto addr_307a_13;
        }
        eax27 = fun_2600();
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
        umask_value = eax27;
        while ((zf29 = recurse == 0, !zf29) && *reinterpret_cast<signed char*>(&r12_10)) {
            rax30 = get_root_dev_ino(0x110f0, rsi15, rdx16, rcx17, r8_18, r9_19);
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
            root_dev_ino = rax30;
            if (rax30) 
                goto addr_2b99_17;
            rax31 = quotearg_style(4, "/", rdx16, 4, "/", rdx16);
            r13_5 = rax31;
            rax32 = fun_24c0();
            rax33 = fun_2420();
            rcx34 = r13_5;
            rdx16 = rax32;
            rsi15 = *reinterpret_cast<void***>(rax33);
            *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
            fun_26f0();
            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            addr_3446_19:
            rdi35 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7 + reinterpret_cast<unsigned char>(rbp6) * 8) - 8);
            rax36 = quote(rdi35, rsi15, rdx16, rcx34, r8_18, r9_19);
            r12_10 = rax36;
            fun_24c0();
            fun_26f0();
            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8 - 8 + 8);
            while (1) {
                while (1) {
                    addr_2967_20:
                    rax37 = usage();
                    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                    while (1) {
                        if (*reinterpret_cast<int32_t*>(&rax37) == 0x81) {
                            *reinterpret_cast<int32_t*>(&r12_10) = 1;
                        } else {
                            if (*reinterpret_cast<int32_t*>(&rax37) != 0x82) {
                                if (*reinterpret_cast<int32_t*>(&rax37) != 0x80) 
                                    break;
                                *reinterpret_cast<int32_t*>(&r12_10) = 0;
                            } else {
                                rax38 = optarg;
                                v12 = rax38;
                            }
                        }
                        while (1) {
                            addr_2918_2:
                            *reinterpret_cast<int32_t*>(&r8_18) = 0;
                            *reinterpret_cast<int32_t*>(&r8_18 + 4) = 0;
                            rcx17 = r14_4;
                            rdx16 = r13_5;
                            rsi15 = rbx7;
                            *reinterpret_cast<int32_t*>(&rdi39) = *reinterpret_cast<int32_t*>(&rbp6);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi39) + 4) = 0;
                            rax37 = fun_2510(rdi39);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                            if (*reinterpret_cast<int32_t*>(&rax37) == -1) 
                                goto addr_2b1c_27;
                            if (*reinterpret_cast<int32_t*>(&rax37) > 0x78) 
                                break;
                            if (*reinterpret_cast<int32_t*>(&rax37) > 0x65) {
                                addr_29b0_30:
                                ecx40 = static_cast<int32_t>(rax37 - 0x66);
                                rdx41 = 1 << *reinterpret_cast<unsigned char*>(&ecx40);
                                if (!(*reinterpret_cast<uint32_t*>(&rdx41) & 0x6f202)) {
                                    if (*reinterpret_cast<int32_t*>(&rax37) == 0x76) {
                                        verbosity = 0;
                                        continue;
                                    } else {
                                        if (!(*reinterpret_cast<uint32_t*>(&rdx41) & 1)) 
                                            goto addr_2967_20;
                                        force_silent = 1;
                                        continue;
                                    }
                                }
                            } else {
                                if (*reinterpret_cast<int32_t*>(&rax37) > 99) 
                                    goto addr_2967_20;
                                if (*reinterpret_cast<int32_t*>(&rax37) > 42) 
                                    goto addr_2948_37;
                            }
                            if (*reinterpret_cast<int32_t*>(&rax37) == 0xffffff7d) {
                                rdi42 = stdout;
                                r9_19 = reinterpret_cast<void**>("Jim Meyering");
                                rcx43 = Version;
                                version_etc(rdi42, "chmod", "GNU coreutils", rcx43, "David MacKenzie", "Jim Meyering", 0, rsi15);
                                fun_2750();
                                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 - 8 - 8 + 8 - 8 + 8);
                            } else {
                                if (*reinterpret_cast<int32_t*>(&rax37) != 0xffffff7e) 
                                    goto addr_2967_20;
                                rax37 = usage();
                                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                                goto addr_29b0_30;
                            }
                            addr_2a45_42:
                            recurse = 1;
                            continue;
                            addr_2948_37:
                            *reinterpret_cast<int32_t*>(&rdx44) = static_cast<int32_t>(rax37 - 43);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx44) + 4) = 0;
                            if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r15_3) >> rdx44))) {
                                rax45 = optind;
                                rdi46 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7 + rax45 * 8) - 8);
                                v47 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7 + rax45 * 8) - 8);
                                rax48 = fun_24e0(rdi46, rdi46);
                                rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
                                r9_19 = v47;
                                rdx50 = rax48;
                                rcx51 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v13) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(v13) < reinterpret_cast<unsigned char>(1))))))));
                                r8_52 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx50) + reinterpret_cast<uint64_t>(rcx51));
                                if (reinterpret_cast<unsigned char>(v9) <= reinterpret_cast<unsigned char>(r8_52)) {
                                    v9 = r8_52 + 1;
                                    rax53 = x2realloc(v14, reinterpret_cast<int64_t>(rsp49) + 72);
                                    rsp49 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
                                    rdx50 = rdx50;
                                    rcx51 = rcx51;
                                    r9_19 = r9_19;
                                    r8_52 = r8_52;
                                    v14 = rax53;
                                }
                            } else {
                                if (*reinterpret_cast<int32_t*>(&rax37) == 99) {
                                    verbosity = 1;
                                    continue;
                                } else {
                                    if (*reinterpret_cast<int32_t*>(&rax37) == 82) 
                                        goto addr_2a45_42; else 
                                        goto addr_2967_20;
                                }
                            }
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v14) + reinterpret_cast<unsigned char>(v13)) = 44;
                            fun_2620(reinterpret_cast<unsigned char>(v14) + reinterpret_cast<uint64_t>(rcx51), r9_19, rdx50 + 1);
                            rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
                            diagnose_surprises = 1;
                            v13 = r8_52;
                        }
                    }
                }
                addr_2b1c_27:
                if (!v12) 
                    goto addr_305d_6;
                if (!v14) {
                    less_or_equal54 = *reinterpret_cast<int32_t*>(&rbp6) <= optind;
                    if (!less_or_equal54) 
                        break;
                    addr_308c_11:
                }
                fun_24c0();
                fun_26f0();
                rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8 - 8 + 8);
            }
            rax55 = mode_create_from_ref(v12);
            rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
            change = rax55;
            if (!rax55) 
                goto addr_34f1_5;
        }
        root_dev_ino = reinterpret_cast<struct s11*>(0);
        addr_2b99_17:
        rax56 = optind;
        rdx57 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rdx57 + 4) = 0;
        rsi15 = reinterpret_cast<void**>(0x411);
        *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_4) = 1;
        *reinterpret_cast<int32_t*>(&r14_4 + 4) = 0;
        rax58 = xfts_open(rbx7 + rax56 * 8, 0x411);
        rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
        r12_10 = rax58;
        while (rax60 = rpl_fts_read(r12_10, rsi15, rdx57, rcx17, r8_18, r9_19), rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8), rbp6 = rax60, !!rax60) {
            r15_3 = *reinterpret_cast<void***>(rax60 + 56);
            rbx61 = *reinterpret_cast<void***>(rax60 + 48);
            eax21 = *reinterpret_cast<uint16_t*>(rax60 + 0x68) - 2;
            if (*reinterpret_cast<uint16_t*>(&eax21) <= 11) 
                goto addr_2be2_4;
            rax62 = root_dev_ino;
            if (!rax62 || ((rcx17 = rax62->f0, *reinterpret_cast<void***>(rbp6 + 0x78) != rcx17) || *reinterpret_cast<void***>(rbp6 + 0x70) != rax62->f8)) {
                r13_5 = *reinterpret_cast<void***>(rbp6 + 0x88);
                *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0;
                eax63 = reinterpret_cast<unsigned char>(r13_5) & reinterpret_cast<uint32_t>("!");
                if (eax63 == 0xa000) {
                    v13 = reinterpret_cast<void**>(0);
                    r13_5 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0;
                    ebx64 = 2;
                } else {
                    *reinterpret_cast<int32_t*>(&rsi65) = 0;
                    *reinterpret_cast<int32_t*>(&rsi65 + 4) = 0;
                    *reinterpret_cast<unsigned char*>(&rsi65) = reinterpret_cast<uint1_t>(eax63 == 0x4000);
                    *reinterpret_cast<void***>(&rdi66) = r13_5;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi66) + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r8_18) = 0;
                    *reinterpret_cast<int32_t*>(&r8_18 + 4) = 0;
                    eax67 = mode_adjust(rdi66, rsi65, rdi66, rsi65);
                    *reinterpret_cast<int32_t*>(&rdi68) = r12_10->f2c;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi68) + 4) = 0;
                    rcx17 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                    rsi15 = rbx61;
                    rdx57 = eax67;
                    *reinterpret_cast<int32_t*>(&rdx57 + 4) = 0;
                    v13 = eax67;
                    eax63 = fun_2670(rdi68, rsi15, rdx57);
                    rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8);
                    if (eax63) {
                        zf69 = force_silent == 0;
                        if (zf69) {
                            rax70 = quotearg_style(4, r15_3, rdx57);
                            v14 = rax70;
                            rax71 = fun_24c0();
                            rax72 = fun_2420();
                            rcx17 = v14;
                            rdx57 = rax71;
                            rsi15 = *reinterpret_cast<void***>(rax72);
                            *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
                            eax63 = 0;
                            fun_26f0();
                            rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                        }
                        ebx64 = 1;
                    } else {
                        zf73 = verbosity == 2;
                        if (zf73) {
                            ebx64 = 4;
                            goto addr_2d49_66;
                        }
                        rcx17 = v13;
                        *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                        *reinterpret_cast<int32_t*>(&rdi74) = r12_10->f2c;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi74) + 4) = 0;
                        eax75 = rcx17;
                        *reinterpret_cast<unsigned char*>(&rcx17 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rcx17 + 1) & 14);
                        if (*reinterpret_cast<unsigned char*>(&rcx17 + 1)) 
                            goto addr_3352_68; else 
                            goto addr_2c8a_69;
                    }
                }
            } else {
                eax76 = fun_25d0(r15_3, "/", rdx57);
                rsp77 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
                if (eax76) {
                    rax78 = quotearg_n_style();
                    rax79 = quotearg_n_style();
                    r13_5 = rax79;
                    fun_24c0();
                    r8_18 = rax78;
                    rcx17 = r13_5;
                    fun_26f0();
                    rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                } else {
                    rax81 = quotearg_style(4, r15_3, rdx57, 4, r15_3, rdx57);
                    r13_5 = rax81;
                    fun_24c0();
                    rcx17 = r13_5;
                    fun_26f0();
                    rsp80 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8 - 8 + 8 - 8 + 8);
                }
                fun_24c0();
                fun_26f0();
                rdx57 = reinterpret_cast<void**>(4);
                *reinterpret_cast<int32_t*>(&rdx57 + 4) = 0;
                rsi15 = rbp6;
                rpl_fts_set(r12_10, rsi15, 4, rcx17, r8_18, r9_19, v82);
                rpl_fts_read(r12_10, rsi15, 4, rcx17, r8_18, r9_19);
                rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp80) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                eax63 = 0;
                goto addr_2d69_74;
            }
            zf83 = verbosity == 2;
            if (zf83) 
                goto addr_2d56_76;
            zf84 = verbosity == 0;
            if (!zf84) 
                goto addr_2d56_76;
            rax85 = quotearg_style(4, r15_3, rdx57, 4, r15_3, rdx57);
            rsp86 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
            if (ebx64 != 2) {
                v12 = rax85;
                ebx64 = 1;
                *reinterpret_cast<uint32_t*>(&r9_87) = reinterpret_cast<unsigned char>(v13) & 0xfff;
                *reinterpret_cast<int32_t*>(&r9_87 + 4) = 0;
                v14 = r9_87;
                strmode();
                strmode();
                rax88 = fun_24c0();
                rsp89 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8 - 8 + 8 - 8 + 8);
                r9_19 = v14;
                r11_90 = v12;
                rsi15 = rax88;
            } else {
                v13 = rax85;
                rax91 = fun_24c0();
                rdx57 = v13;
                rsi15 = rax91;
                eax63 = 0;
                fun_26d0(1, rsi15, rdx57, 1, rsi15, rdx57);
                rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp86) - 8 + 8 - 8 + 8);
                goto addr_2d56_76;
            }
            addr_2d1c_81:
            rsp92 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp89) - 8);
            rsp93 = rsp92 - 8;
            eax63 = 0;
            r8_18 = reinterpret_cast<void**>(rsp93 + 0xfd);
            fun_26d0(1, rsi15, r11_90, 1, rsi15, r11_90);
            rdx57 = reinterpret_cast<void**>(rsp92 + 0xe9);
            rcx17 = rax88;
            rsp59 = reinterpret_cast<void*>(rsp93 - 8 + 8 + 8 + 8);
            if (ebx64 <= 2) {
                addr_2d56_76:
                zf94 = recurse == 0;
                if (zf94) {
                    rdx57 = reinterpret_cast<void**>(4);
                    *reinterpret_cast<int32_t*>(&rdx57 + 4) = 0;
                    rsi15 = rbp6;
                    rpl_fts_set(r12_10, rsi15, 4, rcx17, r8_18, r9_19, v95);
                    rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
                }
            } else {
                addr_2d49_66:
                zf96 = diagnose_surprises == 0;
                if (!zf96 && (rsi15 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0, rcx17 = change, *reinterpret_cast<void***>(&rdi97) = r13_5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi97) + 4) = 0, *reinterpret_cast<unsigned char*>(&rsi15) = reinterpret_cast<uint1_t>((reinterpret_cast<unsigned char>(r13_5) & reinterpret_cast<uint32_t>("!")) == 0x4000), *reinterpret_cast<int32_t*>(&r8_18) = 0, *reinterpret_cast<int32_t*>(&r8_18 + 4) = 0, rdx57 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rdx57 + 4) = 0, eax98 = mode_adjust(rdi97, rsi15), rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8), r13_5 = eax98, *reinterpret_cast<int32_t*>(&r13_5 + 4) = 0, eax63 = ~reinterpret_cast<unsigned char>(eax98), !!(reinterpret_cast<unsigned char>(v13) & eax63))) {
                    ebx64 = 1;
                    strmode();
                    strmode();
                    rax99 = quotearg_n_style_colon();
                    r13_5 = rax99;
                    rax100 = fun_24c0();
                    rsp101 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    rcx17 = r13_5;
                    rsi15 = reinterpret_cast<void**>(0);
                    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
                    rdx57 = rax100;
                    r9_19 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp101) + 0xed);
                    eax63 = 0;
                    r8_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp101) + 0xe1);
                    fun_26f0();
                    rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp101) - 8 + 8);
                    goto addr_2d56_76;
                }
            }
            *reinterpret_cast<unsigned char*>(&eax63) = reinterpret_cast<uint1_t>(ebx64 > 1);
            addr_2d69_74:
            *reinterpret_cast<uint32_t*>(&r14_4) = *reinterpret_cast<uint32_t*>(&r14_4) & eax63;
            *reinterpret_cast<int32_t*>(&r14_4 + 4) = 0;
            continue;
            addr_3352_68:
            rcx17 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
            rdx57 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp59) + 80);
            rsi15 = rbx61;
            eax63 = fun_27b0(rdi74, rsi15, rdx57);
            rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
            if (eax63) {
                zf102 = force_silent == 0;
                if (zf102) {
                    rax103 = quotearg_style(4, r15_3, rdx57);
                    v14 = rax103;
                    rax104 = fun_24c0();
                    rax105 = fun_2420();
                    rcx17 = v14;
                    rdx57 = rax104;
                    rsi15 = *reinterpret_cast<void***>(rax105);
                    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
                    eax63 = 0;
                    fun_26f0();
                    rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_3199_87;
                }
            } else {
                eax75 = v106;
            }
            addr_2c8a_69:
            eax63 = reinterpret_cast<unsigned char>(eax75) ^ reinterpret_cast<unsigned char>(r13_5);
            if (!(eax63 & 0xfff)) {
                addr_3199_87:
                zf107 = verbosity == 0;
                if (zf107) {
                    rax108 = quotearg_style(4, r15_3, rdx57, 4, r15_3, rdx57);
                    strmode();
                    strmode();
                    rax109 = fun_24c0();
                    rsp110 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    rdx57 = rax108;
                    r8_18 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp110) + 0xe1);
                    rsi15 = rax109;
                    eax63 = 0;
                    rcx17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v13) & 0xfff);
                    *reinterpret_cast<int32_t*>(&rcx17 + 4) = 0;
                    fun_26d0(1, rsi15, rdx57, 1, rsi15, rdx57);
                    rsp59 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp110) - 8 + 8);
                }
            } else {
                ebx64 = 4;
                rax111 = quotearg_style(4, r15_3, rdx57);
                v12 = rax111;
                *reinterpret_cast<uint32_t*>(&r9_112) = reinterpret_cast<unsigned char>(v13) & 0xfff;
                *reinterpret_cast<int32_t*>(&r9_112 + 4) = 0;
                v14 = r9_112;
                strmode();
                strmode();
                rax88 = fun_24c0();
                rsp89 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                r9_19 = v14;
                r11_90 = v12;
                rsi15 = rax88;
                goto addr_2d1c_81;
            }
            ebx64 = 3;
            goto addr_2d49_66;
        }
        rax113 = fun_2420();
        rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp59) - 8 + 8);
        rbx7 = rax113;
        if (*reinterpret_cast<void***>(rax113)) {
            *reinterpret_cast<uint32_t*>(&r14_4) = force_silent;
            *reinterpret_cast<int32_t*>(&r14_4 + 4) = 0;
            if (!*reinterpret_cast<signed char*>(&r14_4)) {
                rax115 = fun_24c0();
                rsi15 = *reinterpret_cast<void***>(rbx7);
                *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
                rdx57 = rax115;
                fun_26f0();
                rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8 - 8 + 8);
            } else {
                *reinterpret_cast<uint32_t*>(&r14_4) = 0;
                *reinterpret_cast<int32_t*>(&r14_4 + 4) = 0;
            }
        }
        eax116 = rpl_fts_close(r12_10, rsi15, rdx57, rcx17, r8_18, r9_19);
        rsp117 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
        if (eax116) {
            *reinterpret_cast<uint32_t*>(&r14_4) = 0;
            *reinterpret_cast<int32_t*>(&r14_4 + 4) = 0;
            fun_24c0();
            rsi15 = *reinterpret_cast<void***>(rbx7);
            *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
            fun_26f0();
            rsp117 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp117) - 8 + 8 - 8 + 8);
        }
        fun_2750();
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp117) - 8 + 8);
        continue;
        addr_307a_13:
        rcx34 = v14;
        if (*reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx7 + eax25 * 8) - 8) == rcx34) 
            goto addr_3446_19; else 
            goto addr_308c_11;
    }
}

int64_t __libc_start_main = 0;

void fun_3573() {
    __asm__("cli ");
    __libc_start_main(0x2850, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x11008;

void fun_2400(int64_t rdi);

int64_t fun_3613() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2400(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3653() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_25b0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8);

int32_t fun_2430(void** rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

int64_t stderr = 0;

void fun_2770(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3663(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** r8_6;
    int64_t r12_7;
    void** rax8;
    int64_t r12_9;
    void** rax10;
    int64_t r12_11;
    void** rax12;
    int64_t r12_13;
    void** rax14;
    int64_t r12_15;
    void** rax16;
    int64_t r12_17;
    void** rax18;
    int64_t r12_19;
    void** rax20;
    int64_t r12_21;
    void** rax22;
    int32_t eax23;
    void** r13_24;
    void** rax25;
    void** rax26;
    int32_t eax27;
    void** rax28;
    void** rax29;
    void** rax30;
    int32_t eax31;
    void** rax32;
    int64_t r15_33;
    void** rax34;
    void** rax35;
    void** rax36;
    int64_t rdi37;
    void** r8_38;
    void** r9_39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_24c0();
            r8_6 = r12_2;
            fun_26d0(1, rax5, r12_2, 1, rax5, r12_2);
            r12_7 = stdout;
            rax8 = fun_24c0();
            fun_25b0(rax8, r12_7, 5, r12_2, r8_6);
            r12_9 = stdout;
            rax10 = fun_24c0();
            fun_25b0(rax10, r12_9, 5, r12_2, r8_6);
            r12_11 = stdout;
            rax12 = fun_24c0();
            fun_25b0(rax12, r12_11, 5, r12_2, r8_6);
            r12_13 = stdout;
            rax14 = fun_24c0();
            fun_25b0(rax14, r12_13, 5, r12_2, r8_6);
            r12_15 = stdout;
            rax16 = fun_24c0();
            fun_25b0(rax16, r12_15, 5, r12_2, r8_6);
            r12_17 = stdout;
            rax18 = fun_24c0();
            fun_25b0(rax18, r12_17, 5, r12_2, r8_6);
            r12_19 = stdout;
            rax20 = fun_24c0();
            fun_25b0(rax20, r12_19, 5, r12_2, r8_6);
            r12_21 = stdout;
            rax22 = fun_24c0();
            fun_25b0(rax22, r12_21, 5, r12_2, r8_6);
            do {
                if (1) 
                    break;
                eax23 = fun_25d0("chmod", 0, 5);
            } while (eax23);
            r13_24 = v4;
            if (!r13_24) {
                rax25 = fun_24c0();
                fun_26d0(1, rax25, "GNU coreutils", 1, rax25, "GNU coreutils");
                rax26 = fun_26c0(5);
                if (!rax26 || (eax27 = fun_2430(rax26, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_6), !eax27)) {
                    rax28 = fun_24c0();
                    r13_24 = reinterpret_cast<void**>("chmod");
                    fun_26d0(1, rax28, "https://www.gnu.org/software/coreutils/", 1, rax28, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_24 = reinterpret_cast<void**>("chmod");
                    goto addr_3a30_9;
                }
            } else {
                rax29 = fun_24c0();
                fun_26d0(1, rax29, "GNU coreutils", 1, rax29, "GNU coreutils");
                rax30 = fun_26c0(5);
                if (!rax30 || (eax31 = fun_2430(rax30, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_6), !eax31)) {
                    addr_3936_11:
                    rax32 = fun_24c0();
                    fun_26d0(1, rax32, "https://www.gnu.org/software/coreutils/", 1, rax32, "https://www.gnu.org/software/coreutils/");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_24 == "chmod")) {
                        r12_2 = reinterpret_cast<void**>(0xcfe1);
                    }
                } else {
                    addr_3a30_9:
                    r15_33 = stdout;
                    rax34 = fun_24c0();
                    fun_25b0(rax34, r15_33, 5, "https://www.gnu.org/software/coreutils/", r8_6);
                    goto addr_3936_11;
                }
            }
            rax35 = fun_24c0();
            fun_26d0(1, rax35, r13_24, 1, rax35, r13_24);
            addr_36be_14:
            fun_2750();
        }
    } else {
        rax36 = fun_24c0();
        rdi37 = stderr;
        fun_2770(rdi37, 1, rax36, r12_2, r8_38, r9_39, v40, v41, v42, v43, v44, v45);
        goto addr_36be_14;
    }
}

int64_t file_name = 0;

void fun_3a63(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_3a73(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2440(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_3a83() {
    int64_t rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    int64_t rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2420(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_24c0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3b13_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_26f0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2440(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3b13_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_26f0();
    }
}

struct s12 {
    signed char f0;
    signed char f1;
    signed char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
    signed char f8;
    signed char f9;
    int16_t fa;
};

void fun_3b33(uint32_t edi, struct s12* rsi) {
    uint32_t eax3;
    int32_t ecx4;
    uint32_t esi5;
    uint32_t ecx6;
    uint32_t ecx7;
    uint32_t ecx8;
    uint32_t ecx9;
    uint32_t ecx10;
    uint32_t ecx11;
    uint32_t ecx12;
    uint32_t ecx13;
    uint32_t ecx14;
    uint32_t ecx15;
    uint32_t ecx16;
    uint32_t ecx17;
    uint32_t ecx18;
    uint32_t ecx19;
    uint32_t ecx20;
    uint32_t ecx21;
    uint32_t ecx22;
    uint32_t ecx23;
    uint32_t ecx24;
    uint32_t eax25;
    uint32_t eax26;

    __asm__("cli ");
    eax3 = edi;
    ecx4 = 45;
    esi5 = edi & reinterpret_cast<uint32_t>("!");
    if (esi5 != 0x8000 && ((ecx4 = 100, esi5 != 0x4000) && ((ecx4 = 98, esi5 != 0x6000) && ((ecx4 = 99, esi5 != 0x2000) && ((ecx4 = 0x6c, esi5 != 0xa000) && ((ecx4 = 0x70, esi5 != 0x1000) && (ecx4 = 0x73, esi5 != 0xc000))))))) {
        ecx4 = 63;
    }
    rsi->f0 = *reinterpret_cast<signed char*>(&ecx4);
    ecx6 = eax3 & 0x100;
    ecx7 = (ecx6 - (ecx6 + reinterpret_cast<uint1_t>(ecx6 < ecx6 + reinterpret_cast<uint1_t>(ecx6 < 1))) & 0xffffffbb) + 0x72;
    rsi->f1 = *reinterpret_cast<signed char*>(&ecx7);
    ecx8 = eax3 & 0x80;
    ecx9 = (ecx8 - (ecx8 + reinterpret_cast<uint1_t>(ecx8 < ecx8 + reinterpret_cast<uint1_t>(ecx8 < 1))) & 0xffffffb6) + 0x77;
    rsi->f2 = *reinterpret_cast<signed char*>(&ecx9);
    ecx10 = eax3 & 64;
    ecx11 = ecx10 - (ecx10 + reinterpret_cast<uint1_t>(ecx10 < ecx10 + reinterpret_cast<uint1_t>(ecx10 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 8)) {
        ecx12 = (ecx11 & 0xffffffb5) + 0x78;
    } else {
        ecx12 = (ecx11 & 0xffffffe0) + 0x73;
    }
    rsi->f3 = *reinterpret_cast<signed char*>(&ecx12);
    ecx13 = eax3 & 32;
    ecx14 = (ecx13 - (ecx13 + reinterpret_cast<uint1_t>(ecx13 < ecx13 + reinterpret_cast<uint1_t>(ecx13 < 1))) & 0xffffffbb) + 0x72;
    rsi->f4 = *reinterpret_cast<signed char*>(&ecx14);
    ecx15 = eax3 & 16;
    ecx16 = (ecx15 - (ecx15 + reinterpret_cast<uint1_t>(ecx15 < ecx15 + reinterpret_cast<uint1_t>(ecx15 < 1))) & 0xffffffb6) + 0x77;
    rsi->f5 = *reinterpret_cast<signed char*>(&ecx16);
    ecx17 = eax3 & 8;
    ecx18 = ecx17 - (ecx17 + reinterpret_cast<uint1_t>(ecx17 < ecx17 + reinterpret_cast<uint1_t>(ecx17 < 1)));
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 4)) {
        ecx19 = (ecx18 & 0xffffffb5) + 0x78;
    } else {
        ecx19 = (ecx18 & 0xffffffe0) + 0x73;
    }
    rsi->f6 = *reinterpret_cast<signed char*>(&ecx19);
    ecx20 = eax3 & 4;
    ecx21 = (ecx20 - (ecx20 + reinterpret_cast<uint1_t>(ecx20 < ecx20 + reinterpret_cast<uint1_t>(ecx20 < 1))) & 0xffffffbb) + 0x72;
    rsi->f7 = *reinterpret_cast<signed char*>(&ecx21);
    ecx22 = eax3 & 2;
    ecx23 = (ecx22 - (ecx22 + reinterpret_cast<uint1_t>(ecx22 < ecx22 + reinterpret_cast<uint1_t>(ecx22 < 1))) & 0xffffffb6) + 0x77;
    rsi->f8 = *reinterpret_cast<signed char*>(&ecx23);
    ecx24 = eax3 & 1;
    if (!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax3) + 1) & 2)) {
        eax25 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffb5) + 0x78;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax25);
        rsi->fa = 32;
        return;
    } else {
        eax26 = (eax3 - (eax3 + reinterpret_cast<uint1_t>(eax3 < eax3 + reinterpret_cast<uint1_t>(ecx24 < 1))) & 0xffffffe0) + 0x74;
        rsi->f9 = *reinterpret_cast<signed char*>(&eax26);
        rsi->fa = 32;
        return;
    }
}

void fun_3cb3(int64_t rdi) {
    __asm__("cli ");
    goto strmode;
}

struct s13 {
    int64_t f0;
    int64_t f8;
};

struct s14 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_3cc3(struct s13* rdi, struct s14* rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (rdi->f8 == rsi->f8) {
        rax3 = rsi->f0;
        *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f0 == rax3);
        return rax3;
    } else {
        return 0;
    }
}

struct s15 {
    signed char[8] pad8;
    int64_t f8;
};

uint64_t fun_3cf3(struct s15* rdi, int64_t rsi) {
    __asm__("cli ");
    return rdi->f8 % reinterpret_cast<uint64_t>(rsi);
}

uint64_t fun_3d13(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_3d23(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

struct s16 {
    signed char[120] pad120;
    uint64_t f78;
};

struct s17 {
    signed char[120] pad120;
    uint64_t f78;
};

int64_t fun_3d33(struct s16** rdi, struct s17** rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>((*rdi)->f78 > (*rsi)->f78);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>((*rdi)->f78 < (*rsi)->f78)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

struct s18 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_25c0(void** rdi, int64_t rsi);

void i_ring_init(void*** rdi, int64_t rsi);

void** fun_5313(struct s18* rdi, void** esi, void** rdx, void** rcx) {
    void** ebp5;
    void** rax6;
    void** r12_7;
    struct s18* rbx8;
    void** r14_9;
    void** rax10;
    void** eax11;
    void** rdi12;
    void** eax13;
    void** rsi14;
    unsigned char al15;
    unsigned char v16;
    void** rdi17;
    void** r15_18;
    void** v19;
    void** rax20;
    void** rdi21;
    uint32_t eax22;
    void** rax23;
    unsigned char al24;
    void** eax25;
    int64_t rdi26;
    int64_t rsi27;
    void** eax28;
    void** v29;
    void** r13_30;
    void** rbp31;
    uint32_t eax32;
    signed char v33;
    void** rax34;
    void** rdx35;
    void** rsi36;
    void** rax37;
    void** rax38;
    void** rax39;
    void** r13_40;
    void** rdi41;
    void** rax42;
    void** rax43;
    unsigned char al44;
    struct s18* r15_45;
    void** r13_46;
    void** rax47;

    __asm__("cli ");
    if (reinterpret_cast<unsigned char>(esi) & 0xfffff000 || ((ebp5 = esi, (reinterpret_cast<unsigned char>(esi) & 0x204) == 0x204) || !(*reinterpret_cast<unsigned char*>(&esi) & 18))) {
        rax6 = fun_2420();
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(22);
        goto addr_552e_3;
    }
    rbx8 = rdi;
    r14_9 = rdx;
    rax10 = fun_25c0(1, 0x80);
    r12_7 = rax10;
    if (!rax10) {
        addr_552e_3:
        return r12_7;
    } else {
        *reinterpret_cast<void***>(rax10 + 64) = r14_9;
        eax11 = ebp5;
        rdi12 = rbx8->f0;
        *reinterpret_cast<void***>(r12_7 + 44) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<unsigned char*>(&eax11 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax11 + 1) & 0xfd);
        eax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(eax11) | 4);
        if (!(*reinterpret_cast<unsigned char*>(&ebp5) & 2)) {
            eax13 = ebp5;
        }
        *reinterpret_cast<void***>(r12_7 + 72) = eax13;
        if (rdi12) 
            goto addr_539a_8;
    }
    *reinterpret_cast<int32_t*>(&rsi14) = 0x1000;
    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
    addr_53c9_10:
    al15 = fts_palloc(r12_7, rsi14, rdx);
    v16 = al15;
    if (!al15) {
        addr_5576_11:
        rdi17 = r12_7;
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        fun_23f0(rdi17, rdi17);
        goto addr_552e_3;
    } else {
        r15_18 = rbx8->f0;
        if (!r15_18) {
            v19 = reinterpret_cast<void**>(0);
        } else {
            rax20 = fts_alloc(r12_7, 0xcfe1, 0);
            v19 = rax20;
            if (!rax20) {
                addr_556c_15:
                rdi21 = *reinterpret_cast<void***>(r12_7 + 32);
                fun_23f0(rdi21, rdi21);
                goto addr_5576_11;
            } else {
                *reinterpret_cast<void***>(rax20 + 88) = reinterpret_cast<void**>(0xffffffffffffffff);
                r15_18 = rbx8->f0;
            }
        }
    }
    if (r14_9) {
        eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) >> 10 & 1;
        v16 = *reinterpret_cast<unsigned char*>(&eax22);
    }
    if (!r15_18) {
        rax23 = fts_alloc(r12_7, 0xcfe1, 0);
        *reinterpret_cast<void***>(r12_7) = rax23;
        if (!rax23) {
            addr_5562_21:
            fun_23f0(v19, v19);
            goto addr_556c_15;
        } else {
            *reinterpret_cast<void***>(rax23 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint16_t*>(rax23 + 0x68) = 9;
            *reinterpret_cast<void***>(rax23 + 88) = reinterpret_cast<void**>(1);
            al24 = setup_dir(r12_7, 0xcfe1, 9);
            if (al24) {
                addr_562f_23:
                eax25 = *reinterpret_cast<void***>(r12_7 + 72);
                if (!(reinterpret_cast<unsigned char>(eax25) & 0x204)) {
                    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r12_7 + 44);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(&eax25 + 1) & 2)) {
                        *reinterpret_cast<uint32_t*>(&rsi27) = reinterpret_cast<unsigned char>(eax25) << 13 & 0x20000 | 0x90900;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi27) + 4) = 0;
                        eax28 = open_safer(".", rsi27);
                    } else {
                        eax28 = openat_safer(rdi26, ".");
                    }
                    *reinterpret_cast<void***>(r12_7 + 40) = eax28;
                    if (reinterpret_cast<signed char>(eax28) < reinterpret_cast<signed char>(0)) {
                        *reinterpret_cast<void***>(r12_7 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) | 4);
                    }
                }
            } else {
                goto addr_5562_21;
            }
        }
    } else {
        v29 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r13_30) = 0;
        *reinterpret_cast<int32_t*>(&r13_30 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp31) = 0;
        *reinterpret_cast<int32_t*>(&rbp31 + 4) = 0;
        eax32 = (reinterpret_cast<unsigned char>(ebp5) >> 11 ^ 1) & 1;
        v33 = *reinterpret_cast<signed char*>(&eax32);
        do {
            addr_54cd_31:
            rax34 = fun_24e0(r15_18, r15_18);
            rdx35 = rax34;
            if (reinterpret_cast<unsigned char>(rax34) <= reinterpret_cast<unsigned char>(2) || (!v33 || *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rax34) + 0xffffffffffffffff) != 47)) {
                addr_5450_32:
                rsi36 = r15_18;
                rax37 = fts_alloc(r12_7, rsi36, rdx35);
                if (!rax37) 
                    goto addr_555d_33;
            } else {
                do {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rdx35) + 0xfffffffffffffffe) != 47) 
                        goto addr_5450_32;
                    --rdx35;
                } while (!reinterpret_cast<int1_t>(rdx35 == 1));
                goto addr_5516_37;
            }
            *reinterpret_cast<void***>(rax37 + 88) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax37 + 8) = v19;
            *reinterpret_cast<void***>(rax37 + 48) = rax37 + 0x100;
            if (!rbp31 || !v16) {
                rax38 = fts_stat(r12_7, rax37, 0);
                *reinterpret_cast<uint16_t*>(rax37 + 0x68) = *reinterpret_cast<uint16_t*>(&rax38);
                if (r14_9) {
                    addr_54b5_40:
                    *reinterpret_cast<void***>(rax37 + 16) = rbp31;
                    rbp31 = rax37;
                    continue;
                } else {
                    *reinterpret_cast<void***>(rax37 + 16) = reinterpret_cast<void**>(0);
                    if (!rbp31) {
                        ++r13_30;
                        v29 = rax37;
                        rbp31 = rax37;
                        r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_30) * 8);
                        if (r15_18) 
                            goto addr_54cd_31; else 
                            goto addr_55cd_43;
                    }
                }
            } else {
                *reinterpret_cast<int64_t*>(rax37 + 0xa0) = 2;
                *reinterpret_cast<uint16_t*>(rax37 + 0x68) = 11;
                if (r14_9) 
                    goto addr_54b5_40;
                *reinterpret_cast<void***>(rax37 + 16) = reinterpret_cast<void**>(0);
            }
            rax39 = v29;
            v29 = rax37;
            *reinterpret_cast<void***>(rax39 + 16) = rax37;
            continue;
            addr_5516_37:
            goto addr_5450_32;
            ++r13_30;
            r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_30) * 8);
        } while (r15_18);
        goto addr_55d0_48;
    }
    i_ring_init(r12_7 + 96, 0xffffffff);
    goto addr_552e_3;
    addr_555d_33:
    while (rbp31) {
        r13_40 = rbp31;
        rbp31 = *reinterpret_cast<void***>(rbp31 + 16);
        rdi41 = *reinterpret_cast<void***>(r13_40 + 24);
        if (rdi41) {
            fun_2580(rdi41, rsi36);
        }
        fun_23f0(r13_40, r13_40);
    }
    goto addr_5562_21;
    addr_55d0_48:
    if (r14_9 && reinterpret_cast<unsigned char>(r13_30) > reinterpret_cast<unsigned char>(1)) {
        rax42 = fts_sort(r12_7, rbp31, r13_30, rcx);
        rbp31 = rax42;
    }
    rsi36 = reinterpret_cast<void**>(0xcfe1);
    rax43 = fts_alloc(r12_7, 0xcfe1, 0);
    *reinterpret_cast<void***>(r12_7) = rax43;
    if (!rax43) 
        goto addr_555d_33;
    *reinterpret_cast<void***>(rax43 + 16) = rbp31;
    *reinterpret_cast<uint16_t*>(rax43 + 0x68) = 9;
    *reinterpret_cast<void***>(rax43 + 88) = reinterpret_cast<void**>(1);
    al44 = setup_dir(r12_7, 0xcfe1, 0);
    if (!al44) 
        goto addr_555d_33; else 
        goto addr_562f_23;
    addr_55cd_43:
    goto addr_55d0_48;
    addr_539a_8:
    r15_45 = rbx8;
    *reinterpret_cast<int32_t*>(&r13_46) = 0;
    *reinterpret_cast<int32_t*>(&r13_46 + 4) = 0;
    do {
        rax47 = fun_24e0(rdi12, rdi12);
        if (reinterpret_cast<unsigned char>(r13_46) < reinterpret_cast<unsigned char>(rax47)) {
            r13_46 = rax47;
        }
        rdi12 = r15_45->f8;
        r15_45 = reinterpret_cast<struct s18*>(&r15_45->f8);
    } while (rdi12);
    rsi14 = r13_46 + 1;
    if (reinterpret_cast<unsigned char>(rsi14) >= reinterpret_cast<unsigned char>(0x1000)) 
        goto addr_53c9_10;
    rsi14 = reinterpret_cast<void**>(0x1000);
    goto addr_53c9_10;
}

void hash_free();

int64_t fun_5733(void** rdi, void** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void** rbp6;
    void** rbx7;
    void** rbp8;
    void** rdi9;
    void** rdi10;
    void** rdi11;
    void** eax12;
    int32_t eax13;
    void** rax14;
    void** r13d15;
    void** rbx16;
    int32_t eax17;
    void*** rbx18;
    unsigned char al19;
    void** eax20;
    void** rdi21;
    void** rax22;
    int64_t rax23;
    int32_t eax24;
    void** rax25;
    int32_t eax26;
    void** rax27;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *reinterpret_cast<void***>(rdi);
    if (rdi5) {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi5 + 88)) >= reinterpret_cast<signed char>(0)) {
            while (1) {
                rbp6 = *reinterpret_cast<void***>(rdi5 + 16);
                if (rbp6) {
                    fun_23f0(rdi5);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                } else {
                    rbp6 = *reinterpret_cast<void***>(rdi5 + 8);
                    fun_23f0(rdi5);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                }
                rdi5 = rbp6;
            }
        } else {
            rbp6 = rdi5;
        }
        fun_23f0(rbp6);
    }
    rbx7 = *reinterpret_cast<void***>(r12_4 + 8);
    if (rbx7) {
        do {
            rbp8 = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdi9 = *reinterpret_cast<void***>(rbp8 + 24);
            if (rdi9) {
                fun_2580(rdi9, rsi);
            }
            fun_23f0(rbp8);
        } while (rbx7);
    }
    rdi10 = *reinterpret_cast<void***>(r12_4 + 16);
    fun_23f0(rdi10);
    rdi11 = *reinterpret_cast<void***>(r12_4 + 32);
    fun_23f0(rdi11);
    eax12 = *reinterpret_cast<void***>(r12_4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
        if (!(*reinterpret_cast<unsigned char*>(&eax12) & 4)) {
            eax13 = fun_26a0();
            if (eax13) {
                rax14 = fun_2420();
                r13d15 = *reinterpret_cast<void***>(rax14);
                rbx16 = rax14;
                eax17 = fun_2570();
                if (r13d15 || !eax17) {
                    addr_57ec_19:
                    rbx18 = reinterpret_cast<void***>(r12_4 + 96);
                } else {
                    addr_58f8_20:
                    r13d15 = *reinterpret_cast<void***>(rbx16);
                    goto addr_57ec_19;
                }
                while (al19 = i_ring_empty(rbx18, rsi, rdx), al19 == 0) {
                    eax20 = i_ring_pop(rbx18, rsi, rdx);
                    if (reinterpret_cast<signed char>(eax20) < reinterpret_cast<signed char>(0)) 
                        continue;
                    fun_2570();
                }
                if (*reinterpret_cast<void***>(r12_4 + 80)) {
                    hash_free();
                }
                rdi21 = *reinterpret_cast<void***>(r12_4 + 88);
                if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_4 + 72)) & 0x102)) {
                    fun_23f0(rdi21);
                } else {
                    if (rdi21) {
                        hash_free();
                    }
                }
                fun_23f0(r12_4);
                if (r13d15) {
                    rax22 = fun_2420();
                    *reinterpret_cast<void***>(rax22) = r13d15;
                    r13d15 = reinterpret_cast<void**>(0xffffffff);
                }
                *reinterpret_cast<void***>(&rax23) = r13d15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
                return rax23;
            } else {
                eax24 = fun_2570();
                if (eax24) {
                    rax25 = fun_2420();
                    rbx16 = rax25;
                    goto addr_58f8_20;
                }
            }
        }
    } else {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_4 + 44)) >= reinterpret_cast<signed char>(0) && (eax26 = fun_2570(), !!eax26)) {
            rax27 = fun_2420();
            r13d15 = *reinterpret_cast<void***>(rax27);
            goto addr_57ec_19;
        }
    }
    r13d15 = reinterpret_cast<void**>(0);
    goto addr_57ec_19;
}

struct s19 {
    signed char f0;
    void** f1;
};

void** fun_2530(void** rdi, void** rsi, void** rdx);

void** fun_5923(void** rdi, void** rsi) {
    void** r12_3;
    void** edx4;
    void** rbp5;
    uint32_t eax6;
    void** rax7;
    void** rcx8;
    int32_t eax9;
    void** rdx10;
    void** rax11;
    void** eax12;
    int64_t rdi13;
    int64_t rsi14;
    void** eax15;
    void** r13_16;
    uint32_t eax17;
    void** r13_18;
    void** rax19;
    void** r14_20;
    void** rdi21;
    int32_t eax22;
    void** rax23;
    void** eax24;
    void** rax25;
    void** rax26;
    void** eax27;
    void** rax28;
    int64_t r8_29;
    void** rax30;
    void** rax31;
    void** r14_32;
    void** rdx33;
    void** rax34;
    void** rax35;
    void** rax36;
    void** rsi37;
    void** rdi38;
    struct s19* rdi39;
    void** rdi40;
    uint32_t eax41;
    void** rax42;
    uint32_t eax43;
    void** eax44;
    int32_t eax45;
    void** rax46;
    void** esi47;
    void** rsi48;
    int32_t eax49;
    uint32_t eax50;
    void** rdi51;
    void** rax52;
    void** rdi53;
    void** r14_54;
    void** rsi55;
    void** rax56;
    void** rax57;
    void** r13_58;
    void** rax59;
    void** rax60;
    void** eax61;
    int64_t rdi62;
    int64_t rsi63;
    void** eax64;
    void** rax65;
    void** eax66;
    void** r13_67;
    void** r14_68;
    void** rdi69;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi);
    if (!r12_3) 
        goto addr_5a48_2;
    edx4 = *reinterpret_cast<void***>(rdi + 72);
    rbp5 = rdi;
    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 32) 
        goto addr_5a48_2;
    eax6 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
    if (*reinterpret_cast<int16_t*>(&eax6) == 1) {
        rax7 = fts_stat(rdi, r12_3, 0);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax7);
        goto addr_5a4b_6;
    }
    *reinterpret_cast<uint32_t*>(&rcx8) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&eax6) != 2) 
        goto addr_5972_8;
    eax9 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx8 + 0xfffffffffffffff4));
    if (*reinterpret_cast<uint16_t*>(&eax9) <= 1) {
        *reinterpret_cast<uint32_t*>(&rdx10) = 1;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fts_stat(rdi, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax11);
        if (*reinterpret_cast<uint16_t*>(&rax11) != 1) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            if (*reinterpret_cast<uint16_t*>(&rax11) != 11) 
                goto addr_5a4b_6;
            goto addr_5cf8_13;
        }
        eax12 = *reinterpret_cast<void***>(rbp5 + 72);
        if (*reinterpret_cast<unsigned char*>(&eax12) & 4) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            goto addr_5c8f_16;
        }
        *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(rbp5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(eax12) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
            *reinterpret_cast<uint32_t*>(&rsi14) = *reinterpret_cast<uint32_t*>(&rdx10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi14) + 4) = 0;
            eax15 = open_safer(".", rsi14);
        } else {
            eax15 = openat_safer(rdi13, ".");
        }
        *reinterpret_cast<void***>(r12_3 + 68) = eax15;
        if (reinterpret_cast<signed char>(eax15) >= reinterpret_cast<signed char>(0)) 
            goto addr_5ff6_21;
    } else {
        if (*reinterpret_cast<int16_t*>(&rcx8) != 1) {
            do {
                addr_59a8_23:
                r13_16 = r12_3;
                r12_3 = *reinterpret_cast<void***>(r12_3 + 16);
                if (!r12_3) 
                    goto addr_59b5_24;
                *reinterpret_cast<void***>(rbp5) = r12_3;
                fun_23f0(r13_16);
                if (!*reinterpret_cast<void***>(r12_3 + 88)) 
                    goto addr_5b80_26;
                eax17 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
            } while (*reinterpret_cast<int16_t*>(&eax17) == 4);
            goto addr_5c30_28;
        } else {
            addr_5a77_29:
            if (!(*reinterpret_cast<unsigned char*>(&edx4) & 64) || *reinterpret_cast<void***>(r12_3 + 0x70) == *reinterpret_cast<void***>(rbp5 + 24)) {
                r13_18 = *reinterpret_cast<void***>(rbp5 + 8);
                if (!r13_18) {
                    addr_5d6a_31:
                    rax19 = fts_build(rbp5, 3);
                    *reinterpret_cast<void***>(rbp5 + 8) = rax19;
                    if (!rax19) {
                        if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) {
                            if (*reinterpret_cast<void***>(r12_3 + 64) && *reinterpret_cast<uint16_t*>(r12_3 + 0x68) != 4) {
                                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                            }
                            leave_dir(rbp5, r12_3);
                            goto addr_5a4b_6;
                        }
                    } else {
                        r12_3 = rax19;
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 16) {
                        *reinterpret_cast<unsigned char*>(&edx4 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx4 + 1) & 0xef);
                        *reinterpret_cast<void***>(rbp5 + 72) = edx4;
                        do {
                            r14_20 = r13_18;
                            r13_18 = *reinterpret_cast<void***>(r13_18 + 16);
                            rdi21 = *reinterpret_cast<void***>(r14_20 + 24);
                            if (rdi21) {
                                fun_2580(rdi21, rsi);
                            }
                            fun_23f0(r14_20);
                        } while (r13_18);
                        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                        goto addr_5d6a_31;
                    } else {
                        rcx8 = *reinterpret_cast<void***>(r12_3 + 48);
                        eax22 = fts_safe_changedir(rbp5, r12_3, 0xffffffff, rcx8);
                        if (!eax22) {
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                        } else {
                            rax23 = fun_2420();
                            eax24 = *reinterpret_cast<void***>(rax23);
                            *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 1);
                            *reinterpret_cast<void***>(r12_3 + 64) = eax24;
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                            if (r12_3) {
                                rax25 = r12_3;
                                do {
                                    *reinterpret_cast<void***>(rax25 + 48) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rax25 + 8) + 48);
                                    rax25 = *reinterpret_cast<void***>(rax25 + 16);
                                } while (rax25);
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                goto addr_5c3a_50;
            } else {
                addr_5b12_51:
                if (*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) & 2) {
                    fun_2570();
                    goto addr_5b1e_53;
                }
            }
        }
    }
    rax26 = fun_2420();
    eax27 = *reinterpret_cast<void***>(rax26);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
    *reinterpret_cast<void***>(r12_3 + 64) = eax27;
    *reinterpret_cast<void***>(rbp5) = r12_3;
    goto addr_5a4b_6;
    addr_5ff6_21:
    *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    addr_5c7b_55:
    *reinterpret_cast<void***>(rbp5) = r12_3;
    if (*reinterpret_cast<uint16_t*>(&rax28) == 11) {
        addr_5cf8_13:
        if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) == 2) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax28 = fts_stat(rbp5, r12_3, 0, rbp5, r12_3, 0);
            *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax28);
            goto addr_5c85_57;
        } else {
            if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) != 1) {
                goto 0x27df;
            }
        }
    } else {
        addr_5c85_57:
        if (*reinterpret_cast<uint16_t*>(&rax28) != 1) {
            addr_5a4b_6:
            return r12_3;
        } else {
            addr_5c8f_16:
            if (!*reinterpret_cast<void***>(r12_3 + 88)) {
                *reinterpret_cast<void***>(rbp5 + 24) = *reinterpret_cast<void***>(r12_3 + 0x70);
            }
        }
    }
    rax30 = enter_dir(rbp5, r12_3, rdx10, rcx8, r8_29);
    if (!*reinterpret_cast<signed char*>(&rax30)) {
        rax31 = fun_2420();
        *reinterpret_cast<int32_t*>(&r12_3) = 0;
        *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
        *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(12);
        goto addr_5a4b_6;
    }
    addr_59b5_24:
    r14_32 = *reinterpret_cast<void***>(r13_16 + 8);
    if (*reinterpret_cast<void***>(r14_32 + 24)) {
        rdx33 = *reinterpret_cast<void***>(rbp5 + 32);
        rax34 = *reinterpret_cast<void***>(r14_32 + 72);
        *reinterpret_cast<void***>(rbp5) = r14_32;
        *reinterpret_cast<int32_t*>(&rsi) = 3;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx33) + reinterpret_cast<unsigned char>(rax34)) = 0;
        rax35 = fts_build(rbp5, 3);
        if (rax35) {
            r12_3 = rax35;
            fun_23f0(r13_16, r13_16);
        } else {
            if (*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32) {
                addr_5a48_2:
                *reinterpret_cast<int32_t*>(&r12_3) = 0;
                *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
                goto addr_5a4b_6;
            } else {
                r14_32 = *reinterpret_cast<void***>(r13_16 + 8);
                goto addr_59c4_67;
            }
        }
    } else {
        addr_59c4_67:
        *reinterpret_cast<void***>(rbp5) = r14_32;
        fun_23f0(r13_16, r13_16);
        if (*reinterpret_cast<void***>(r14_32 + 88) == 0xffffffffffffffff) {
            fun_23f0(r14_32, r14_32);
            rax36 = fun_2420();
            *reinterpret_cast<void***>(rax36) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0);
            goto addr_5a4b_6;
        }
    }
    addr_5c3a_50:
    rsi37 = r12_3 + 0x100;
    rdi38 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72) + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 56)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72)) + 0xffffffffffffffff) != 47) {
        rdi38 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72);
    }
    rdi39 = reinterpret_cast<struct s19*>(reinterpret_cast<unsigned char>(rdi38) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)));
    rdi39->f0 = 47;
    rdi40 = reinterpret_cast<void**>(&rdi39->f1);
    rdx10 = *reinterpret_cast<void***>(r12_3 + 96) + 1;
    fun_26e0(rdi40, rsi37, rdx10, rdi40, rsi37, rdx10);
    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    goto addr_5c7b_55;
    if (*reinterpret_cast<uint16_t*>(r14_32 + 0x68) == 11) 
        goto 0x27df;
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_32 + 72))) = 0;
    if (*reinterpret_cast<void***>(r14_32 + 88)) 
        goto addr_59fe_73;
    eax41 = restore_initial_cwd(rbp5, rsi);
    if (!eax41) {
        addr_5a13_75:
        if (*reinterpret_cast<uint16_t*>(r14_32 + 0x68) != 2) {
            if (*reinterpret_cast<void***>(r14_32 + 64)) {
                *reinterpret_cast<uint16_t*>(r14_32 + 0x68) = 7;
            } else {
                *reinterpret_cast<uint16_t*>(r14_32 + 0x68) = 6;
                leave_dir(rbp5, r14_32);
            }
        }
    } else {
        addr_5e58_79:
        rax42 = fun_2420();
        *reinterpret_cast<void***>(r14_32 + 64) = *reinterpret_cast<void***>(rax42);
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_5a13_75;
    }
    r12_3 = r14_32;
    if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) 
        goto addr_5a4b_6;
    goto addr_5a48_2;
    addr_59fe_73:
    eax43 = *reinterpret_cast<unsigned char*>(r14_32 + 0x6a);
    if (*reinterpret_cast<unsigned char*>(&eax43) & 2) {
        eax44 = *reinterpret_cast<void***>(rbp5 + 72);
        if (!(*reinterpret_cast<unsigned char*>(&eax44) & 4)) {
            if (!(*reinterpret_cast<unsigned char*>(&eax44 + 1) & 2)) {
                eax45 = fun_26a0();
                if (eax45) {
                    rax46 = fun_2420();
                    *reinterpret_cast<void***>(r14_32 + 64) = *reinterpret_cast<void***>(rax46);
                    *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
                }
            } else {
                esi47 = *reinterpret_cast<void***>(r14_32 + 68);
                cwd_advance_fd(rbp5, esi47, 1);
            }
        }
        fun_2570();
        goto addr_5a13_75;
    } else {
        if (*reinterpret_cast<unsigned char*>(&eax43) & 1) 
            goto addr_5a13_75;
        rsi48 = *reinterpret_cast<void***>(r14_32 + 8);
        eax49 = fts_safe_changedir(rbp5, rsi48, 0xffffffff, "..");
        if (!eax49) 
            goto addr_5a13_75;
        goto addr_5e58_79;
    }
    addr_5b80_26:
    eax50 = restore_initial_cwd(rbp5, rsi);
    if (eax50) {
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_5a48_2;
    }
    rdi51 = *reinterpret_cast<void***>(rbp5 + 88);
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) & 0x102)) {
        fun_23f0(rdi51);
    } else {
        if (rdi51) {
            hash_free();
        }
    }
    rax52 = *reinterpret_cast<void***>(r12_3 + 96);
    rdi53 = *reinterpret_cast<void***>(rbp5 + 32);
    r14_54 = r12_3 + 0x100;
    *reinterpret_cast<void***>(r12_3 + 72) = rax52;
    rdx10 = rax52 + 1;
    fun_26e0(rdi53, r14_54, rdx10);
    *reinterpret_cast<int32_t*>(&rsi55) = 47;
    *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    rax56 = fun_2530(r14_54, 47, rdx10);
    if (!rax56) 
        goto addr_5c0b_98;
    if (r14_54 == rax56) {
        if (!*reinterpret_cast<void***>(r14_54 + 1)) {
            addr_5c0b_98:
            rax57 = *reinterpret_cast<void***>(rbp5 + 32);
            *reinterpret_cast<void***>(r12_3 + 56) = rax57;
            *reinterpret_cast<void***>(r12_3 + 48) = rax57;
            setup_dir(rbp5, rsi55, rdx10);
            *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
            goto addr_5c7b_55;
        } else {
            goto addr_5be8_102;
        }
    } else {
        addr_5be8_102:
        r13_58 = rax56 + 1;
        rax59 = fun_24e0(r13_58, r13_58);
        rsi55 = r13_58;
        rdx10 = rax59 + 1;
        fun_26e0(r14_54, rsi55, rdx10);
        *reinterpret_cast<void***>(r12_3 + 96) = rax59;
        goto addr_5c0b_98;
    }
    addr_5c30_28:
    if (*reinterpret_cast<int16_t*>(&eax17) == 2) {
        rax60 = fts_stat(rbp5, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax60);
        if (*reinterpret_cast<uint16_t*>(&rax60) == 1 && (eax61 = *reinterpret_cast<void***>(rbp5 + 72), !(*reinterpret_cast<unsigned char*>(&eax61) & 4))) {
            *reinterpret_cast<void***>(&rdi62) = *reinterpret_cast<void***>(rbp5 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi62) + 4) = 0;
            if (!(*reinterpret_cast<unsigned char*>(&eax61 + 1) & 2)) {
                *reinterpret_cast<uint32_t*>(&rsi63) = reinterpret_cast<unsigned char>(eax61) << 13 & 0x20000 | 0x90900;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi63) + 4) = 0;
                eax64 = open_safer(".", rsi63);
            } else {
                eax64 = openat_safer(rdi62, ".");
            }
            *reinterpret_cast<void***>(r12_3 + 68) = eax64;
            if (reinterpret_cast<signed char>(eax64) < reinterpret_cast<signed char>(0)) {
                rax65 = fun_2420();
                eax66 = *reinterpret_cast<void***>(rax65);
                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                *reinterpret_cast<void***>(r12_3 + 64) = eax66;
            } else {
                *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
            }
        }
        *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
        goto addr_5c3a_50;
    }
    addr_5b1e_53:
    r13_67 = *reinterpret_cast<void***>(rbp5 + 8);
    if (r13_67) {
        do {
            r14_68 = r13_67;
            r13_67 = *reinterpret_cast<void***>(r13_67 + 16);
            rdi69 = *reinterpret_cast<void***>(r14_68 + 24);
            if (rdi69) {
                fun_2580(rdi69, rsi);
            }
            fun_23f0(r14_68);
        } while (r13_67);
        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
    }
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 6;
    leave_dir(rbp5, r12_3);
    goto addr_5a4b_6;
    addr_5972_8:
    if (*reinterpret_cast<int16_t*>(&rcx8) != 1) 
        goto addr_59a8_23;
    if (*reinterpret_cast<int16_t*>(&eax6) != 4) 
        goto addr_5a77_29; else 
        goto addr_5b12_51;
}

struct s20 {
    signed char[108] pad108;
    int16_t f6c;
};

int64_t fun_6073() {
    uint32_t edx1;
    void** rax2;
    struct s20* rsi3;
    int16_t dx4;

    __asm__("cli ");
    if (edx1 > 4) {
        rax2 = fun_2420();
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(22);
        return 1;
    } else {
        rsi3->f6c = dx4;
        return 0;
    }
}

void** fun_60a3(void** rdi, void** rsi) {
    uint32_t r13d3;
    void** rbp4;
    void** rax5;
    void** r14_6;
    void** r15_7;
    uint32_t edx8;
    void** rax9;
    void** rbx10;
    void** r12_11;
    void** rdi12;
    int32_t r12d13;
    void** eax14;
    void** rsi15;
    int64_t rdi16;
    int64_t rsi17;
    void** eax18;
    void** r13d19;
    void** eax20;
    void** rsi21;
    void** rax22;
    int32_t eax23;
    void** ebx24;

    __asm__("cli ");
    r13d3 = *reinterpret_cast<uint32_t*>(&rsi);
    rbp4 = rdi;
    rax5 = fun_2420();
    r14_6 = rax5;
    if (r13d3 & 0xffffefff) {
        *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(22);
        return 0;
    }
    r15_7 = *reinterpret_cast<void***>(rbp4);
    *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 32) {
        return 0;
    }
    edx8 = *reinterpret_cast<uint16_t*>(r15_7 + 0x68);
    if (*reinterpret_cast<int16_t*>(&edx8) == 9) {
        return *reinterpret_cast<void***>(r15_7 + 16);
    }
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&edx8) == 1) 
        goto addr_60f8_8;
    addr_616d_9:
    return rax9;
    addr_60f8_8:
    rbx10 = *reinterpret_cast<void***>(rbp4 + 8);
    if (rbx10) {
        do {
            r12_11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 16);
            rdi12 = *reinterpret_cast<void***>(r12_11 + 24);
            if (rdi12) {
                fun_2580(rdi12, rsi);
            }
            fun_23f0(r12_11);
        } while (rbx10);
    }
    r12d13 = 1;
    if (r13d3 == 0x1000) {
        *reinterpret_cast<void***>(rbp4 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 72)) | 0x1000);
        r12d13 = 2;
    }
    if (*reinterpret_cast<void***>(r15_7 + 88)) 
        goto addr_615e_17;
    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(r15_7 + 48)) == 47) 
        goto addr_615e_17;
    eax14 = *reinterpret_cast<void***>(rbp4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax14) & 4)) 
        goto addr_6180_20;
    addr_615e_17:
    *reinterpret_cast<int32_t*>(&rsi15) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    rax9 = fts_build(rbp4, rsi15);
    *reinterpret_cast<void***>(rbp4 + 8) = rax9;
    goto addr_616d_9;
    addr_6180_20:
    *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rbp4 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&eax14 + 1) & 2)) {
        *reinterpret_cast<uint32_t*>(&rsi17) = reinterpret_cast<unsigned char>(eax14) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        eax18 = open_safer(".", rsi17);
        r13d19 = eax18;
    } else {
        eax20 = openat_safer(rdi16, ".");
        r13d19 = eax20;
    }
    if (reinterpret_cast<signed char>(r13d19) >= reinterpret_cast<signed char>(0)) 
        goto addr_61b7_24;
    *reinterpret_cast<void***>(rbp4 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    goto addr_616d_9;
    addr_61b7_24:
    *reinterpret_cast<int32_t*>(&rsi21) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    rax22 = fts_build(rbp4, rsi21);
    *reinterpret_cast<void***>(rbp4 + 8) = rax22;
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 2) {
        cwd_advance_fd(rbp4, r13d19, 1);
    } else {
        eax23 = fun_26a0();
        if (eax23) {
            ebx24 = *reinterpret_cast<void***>(r14_6);
            fun_2570();
            *reinterpret_cast<int32_t*>(&rax9) = 0;
            *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
            *reinterpret_cast<void***>(r14_6) = ebx24;
            goto addr_616d_9;
        } else {
            fun_2570();
        }
    }
    rax9 = *reinterpret_cast<void***>(rbp4 + 8);
    goto addr_616d_9;
}

uint64_t fun_62a3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_62c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s21 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_6723(struct s21* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s22 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_6733(struct s22* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s23 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_6743(struct s23* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s26 {
    signed char[8] pad8;
    struct s26* f8;
};

struct s25 {
    int64_t f0;
    struct s26* f8;
};

struct s24 {
    struct s25* f0;
    struct s25* f8;
};

uint64_t fun_6753(struct s24* rdi) {
    struct s25* rcx2;
    struct s25* rsi3;
    uint64_t r8_4;
    struct s26* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s29 {
    signed char[8] pad8;
    struct s29* f8;
};

struct s28 {
    int64_t f0;
    struct s29* f8;
};

struct s27 {
    struct s28* f0;
    struct s28* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_67b3(struct s27* rdi) {
    struct s28* rcx2;
    struct s28* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s29* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s32 {
    signed char[8] pad8;
    struct s32* f8;
};

struct s31 {
    int64_t f0;
    struct s32* f8;
};

struct s30 {
    struct s31* f0;
    struct s31* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_6823(struct s30* rdi, int64_t rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r13_5;
    int64_t v6;
    int64_t r12_7;
    uint64_t r12_8;
    int64_t v9;
    int64_t rbp10;
    int64_t rbp11;
    int64_t v12;
    int64_t rbx13;
    struct s31* rcx14;
    struct s31* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s32* rax19;
    uint64_t rdx20;
    void** r9_21;
    int64_t v22;
    void** r9_23;
    int64_t v24;
    void** r9_25;
    int64_t v26;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_2770(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_2770(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x617c]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_68da_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_6959_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_2770(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_2770;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x61fb]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_6959_14; else 
            goto addr_68da_13;
    }
}

struct s33 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s34 {
    int64_t f0;
    struct s34* f8;
};

int64_t fun_6983(struct s33* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s33* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s34* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x27ee;
    rbx7 = reinterpret_cast<struct s34*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_69e8_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_69db_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_69db_7;
    }
    addr_69eb_10:
    return r12_3;
    addr_69e8_5:
    r12_3 = rbx7->f0;
    goto addr_69eb_10;
    addr_69db_7:
    return 0;
}

struct s35 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_6a03(struct s35* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x27f3;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_6a3f_7;
    return *rax2;
    addr_6a3f_7:
    goto 0x27f3;
}

struct s37 {
    int64_t f0;
    struct s37* f8;
};

struct s36 {
    int64_t* f0;
    struct s37* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_6a53(struct s36* rdi, int64_t rsi) {
    struct s36* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s37* rax7;
    struct s37* rdx8;
    struct s37* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x27f9;
    rax7 = reinterpret_cast<struct s37*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_6a9e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_6a9e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_6abc_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_6abc_10:
    return r8_10;
}

struct s39 {
    int64_t f0;
    struct s39* f8;
};

struct s38 {
    struct s39* f0;
    struct s39* f8;
};

void fun_6ae3(struct s38* rdi, int64_t rsi, uint64_t rdx) {
    struct s39* r9_4;
    uint64_t rax5;
    struct s39* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_6b22_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_6b22_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s41 {
    int64_t f0;
    struct s41* f8;
};

struct s40 {
    struct s41* f0;
    struct s41* f8;
};

int64_t fun_6b33(struct s40* rdi, int64_t rsi, int64_t rdx) {
    struct s41* r14_4;
    int64_t r12_5;
    struct s40* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s41* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_6b5f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_6ba1_10;
            }
            addr_6b5f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_6b69_11:
    return r12_5;
    addr_6ba1_10:
    goto addr_6b69_11;
}

uint64_t fun_6bb3(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s42 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_6bf3(struct s42* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_6c23(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    void** rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    uint32_t esi12;
    void** rax13;
    void** rax14;
    void** rdi15;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x62a0);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<void**>(0x62c0);
    }
    rax9 = fun_2650(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0xcaa0);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((esi12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), rax13 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&esi12)), *reinterpret_cast<void***>(r12_10 + 16) = rax13, rax13 == 0) || (rax14 = fun_25c0(rax13, 16), *reinterpret_cast<void***>(r12_10) = rax14, rax14 == 0))) {
            rdi15 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_23f0(rdi15, rdi15);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<void***>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax14) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s45 {
    int64_t f0;
    struct s45* f8;
};

struct s44 {
    int64_t f0;
    struct s45* f8;
};

struct s43 {
    struct s44* f0;
    struct s44* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s45* f48;
};

void fun_6d23(struct s43* rdi) {
    struct s43* rbp2;
    struct s44* r12_3;
    struct s45* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s45* rax7;
    struct s45* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s46 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_6dd3(struct s46* rdi) {
    struct s46* r12_2;
    void** r13_3;
    void** rax4;
    void** rbp5;
    void** rbx6;
    void** rdi7;
    void** rdi8;
    void** rbx9;
    void** rbx10;
    void** rdi11;
    void** rdi12;

    __asm__("cli ");
    r12_2 = rdi;
    r13_3 = rdi->f0;
    rax4 = rdi->f8;
    rbp5 = r13_3;
    if (!rdi->f40 || !rdi->f20) {
        addr_6e43_2:
        if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rbp5)) {
            do {
                rbx6 = *reinterpret_cast<void***>(rbp5 + 8);
                if (rbx6) {
                    do {
                        rdi7 = rbx6;
                        rbx6 = *reinterpret_cast<void***>(rbx6 + 8);
                        fun_23f0(rdi7);
                    } while (rbx6);
                }
                rbp5 = rbp5 + 16;
            } while (reinterpret_cast<unsigned char>(r12_2->f8) > reinterpret_cast<unsigned char>(rbp5));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_3) < reinterpret_cast<unsigned char>(rax4)) {
            while (1) {
                rdi8 = *reinterpret_cast<void***>(r13_3);
                if (!rdi8) {
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                } else {
                    rbx9 = r13_3;
                    while (r12_2->f40(rdi8), rbx9 = *reinterpret_cast<void***>(rbx9 + 8), !!rbx9) {
                        rdi8 = *reinterpret_cast<void***>(rbx9);
                    }
                    rax4 = r12_2->f8;
                    r13_3 = r13_3 + 16;
                    if (reinterpret_cast<unsigned char>(rax4) <= reinterpret_cast<unsigned char>(r13_3)) 
                        break;
                }
            }
            rbp5 = r12_2->f0;
            goto addr_6e43_2;
        }
    }
    rbx10 = r12_2->f48;
    if (rbx10) {
        do {
            rdi11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
            fun_23f0(rdi11);
        } while (rbx10);
    }
    rdi12 = r12_2->f0;
    fun_23f0(rdi12);
    goto fun_23f0;
}

int64_t fun_6ec3(struct s3* rdi, uint64_t rsi) {
    struct s4* r12_3;
    void** rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    struct s3* r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = rdi->f28;
    rax4 = g28;
    esi5 = r12_3->f10;
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_7000_2;
    if (rdi->f10 == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_25c0(rax6, 16);
        if (!rax8) {
            addr_7000_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<struct s3*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8 - 8 + 8);
            v10 = rdi->f48;
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = rdi->f0;
                fun_23f0(rdi12, rdi12);
                rdi->f0 = rax8;
                rdi->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                rdi->f10 = rax6;
                rdi->f18 = 0;
                rdi->f48 = v10;
            } else {
                rdi->f48 = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x27fe;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x27fe;
                fun_23f0(rax8, rax8);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        fun_2500();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s47 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_7053(void** rdi, void** rsi, void*** rdx) {
    void** rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s47* v17;
    int32_t r8d18;
    void** rax19;
    void* rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x2803;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_716e_5; else 
                goto addr_70df_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_70df_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_716e_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x5901]");
            if (!cf14) 
                goto addr_71c5_12;
            __asm__("comiss xmm4, [rip+0x58b1]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x5870]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_71c5_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x2803;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_2650(16, rsi, rdx6);
                if (!rax19) {
                    addr_71c5_12:
                    r8d18 = -1;
                } else {
                    goto addr_7122_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_7122_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_709e_28:
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2500();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_7122_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_709e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_7273() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2500();
    } else {
        return rax3;
    }
}

void** fun_72d3(void** rdi, void** rsi) {
    void** rbx3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_7360_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_7416_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x571e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x5688]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_23f0(rdi15, rdi15);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_7416_5; else 
                goto addr_7360_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (rax16) {
        fun_2500();
    } else {
        return r12_7;
    }
}

void fun_7463() {
    __asm__("cli ");
    goto hash_remove;
}

struct s48 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int64_t f14;
    signed char f1c;
};

void fun_7473(struct s48* rdi, int32_t esi) {
    __asm__("cli ");
    rdi->f14 = 0;
    rdi->f1c = 1;
    rdi->f0 = esi;
    rdi->f4 = esi;
    rdi->f8 = esi;
    rdi->fc = esi;
    rdi->f10 = esi;
    return;
}

struct s49 {
    signed char[28] pad28;
    unsigned char f1c;
};

int64_t fun_7493(struct s49* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rax2) = rdi->f1c;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

struct s50 {
    int32_t f0;
    signed char[16] pad20;
    uint32_t f14;
    uint32_t f18;
    unsigned char f1c;
};

int64_t fun_74a3(struct s50* rdi, int32_t esi) {
    uint32_t eax3;
    uint32_t eax4;
    uint32_t edx5;
    int64_t rcx6;
    int32_t r8d7;
    uint32_t ecx8;
    int64_t rax9;

    __asm__("cli ");
    eax3 = static_cast<uint32_t>(rdi->f1c) ^ 1;
    eax4 = *reinterpret_cast<unsigned char*>(&eax3);
    edx5 = rdi->f14 + eax4 & 3;
    *reinterpret_cast<uint32_t*>(&rcx6) = edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    r8d7 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4) = esi;
    ecx8 = rdi->f18;
    rdi->f14 = edx5;
    if (ecx8 == edx5) {
        rdi->f18 = eax4 + ecx8 & 3;
    }
    rdi->f1c = 0;
    *reinterpret_cast<int32_t*>(&rax9) = r8d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

struct s51 {
    int32_t f0;
    signed char[12] pad16;
    int32_t f10;
    uint32_t f14;
    uint32_t f18;
    signed char f1c;
};

int64_t fun_74e3(struct s51* rdi) {
    int64_t rdx2;
    int32_t r8d3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f1c) 
        goto 0x2808;
    *reinterpret_cast<uint32_t*>(&rdx2) = rdi->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx2) + 4) = 0;
    r8d3 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4);
    rax4 = rdx2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4) = rdi->f10;
    if (*reinterpret_cast<uint32_t*>(&rdx2) == rdi->f18) {
        rdi->f1c = 1;
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        rdi->f14 = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax4) + 3) & 3;
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s52 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
};

void** xnmalloc();

void** xmalloc(int64_t rdi, void* rsi);

struct s53 {
    void** f0;
    signed char f1;
    signed char[2] pad4;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
};

void** fun_7523(struct s52* rdi, void* rsi) {
    struct s52* rbx3;
    int64_t rcx4;
    int32_t eax5;
    struct s52* rdx6;
    void** rax7;
    uint64_t rdi8;
    void** r9_9;
    struct s52* rax10;
    int64_t rbp11;
    int32_t edx12;
    uint32_t ebx13;
    void** rax14;
    int64_t r8_15;
    int32_t r8d16;
    int64_t rax17;
    struct s52* rcx18;
    uint32_t edx19;
    int32_t ebx20;
    struct s53* rdi21;
    uint32_t esi22;
    uint32_t r10d23;
    uint32_t edx24;
    uint64_t r11_25;
    int1_t less_or_equal26;
    int1_t less_or_equal27;
    uint32_t eax28;
    int64_t rdx29;
    int32_t r8d30;

    __asm__("cli ");
    rbx3 = rdi;
    *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rdi->f0)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
    eax5 = static_cast<int32_t>(rcx4 - 48);
    if (*reinterpret_cast<unsigned char*>(&eax5) > 7) {
        rdx6 = rdi;
        if (*reinterpret_cast<unsigned char*>(&rcx4)) {
            do {
                if (*reinterpret_cast<unsigned char*>(&rcx4) <= 61) {
                }
                *reinterpret_cast<uint32_t*>(&rcx4) = rdx6->f1;
                rdx6 = reinterpret_cast<struct s52*>(&rdx6->f1);
            } while (*reinterpret_cast<unsigned char*>(&rcx4));
        }
        rax7 = xnmalloc();
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        r9_9 = rax7;
        goto addr_758e_8;
    }
    rax10 = rdi;
    *reinterpret_cast<void***>(&rbp11) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp11) + 4) = 0;
    do {
        *reinterpret_cast<void***>(&rbp11) = reinterpret_cast<void**>(static_cast<uint32_t>(rcx4 + rbp11 * 8 - 48));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp11) + 4) = 0;
        rax10 = reinterpret_cast<struct s52*>(&rax10->f1);
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbp11)) > reinterpret_cast<unsigned char>(0xfff)) 
            break;
        *reinterpret_cast<uint32_t*>(&rcx4) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rax10->f0)));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx4) + 4) = 0;
        edx12 = static_cast<int32_t>(rcx4 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edx12) <= 7);
    goto addr_7820_12;
    return 0;
    addr_7820_12:
    *reinterpret_cast<int32_t*>(&r9_9) = 0;
    *reinterpret_cast<int32_t*>(&r9_9 + 4) = 0;
    if (!*reinterpret_cast<unsigned char*>(&rcx4)) {
        ebx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbp11)) & reinterpret_cast<uint32_t>("ettext") | 0x3ff;
        if (reinterpret_cast<int64_t>(rax10) - reinterpret_cast<int64_t>(rbx3) >= 5) {
            ebx13 = 0xfff;
        }
        rax14 = xmalloc(32, rsi);
        *reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(0x13d);
        *reinterpret_cast<int32_t*>(rax14 + 4) = 0xfff;
        *reinterpret_cast<void***>(rax14 + 8) = *reinterpret_cast<void***>(&rbp11);
        *reinterpret_cast<uint32_t*>(rax14 + 12) = ebx13;
        *reinterpret_cast<signed char*>(rax14 + 17) = 0;
        return rax14;
    }
    addr_75bb_17:
    return r9_9;
    addr_76f5_18:
    *reinterpret_cast<uint32_t*>(&r8_15) = *reinterpret_cast<unsigned char*>(&r8d16);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xcad8 + r8_15 * 4) + 0xcad8;
    while (1) {
        addr_7650_19:
        *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
        rcx18 = reinterpret_cast<struct s52*>(&rbx3->f2);
        edx19 = 7;
        ebx20 = 3;
        do {
            addr_7609_20:
            rdi21->f0 = *reinterpret_cast<void***>(&esi22);
            rdi21->f1 = *reinterpret_cast<signed char*>(&ebx20);
            rdi21->f4 = r10d23;
            rdi21->f8 = edx19;
            if (r10d23) {
                edx19 = edx19 & r10d23;
            }
            esi22 = *reinterpret_cast<uint32_t*>(&rax17);
            rbx3 = rcx18;
            while (1) {
                rdi21->fc = edx19;
                ++rdi21;
                edx24 = *reinterpret_cast<uint32_t*>(&rax17) & 0xffffffef;
                if (*reinterpret_cast<signed char*>(&edx24) == 45 || *reinterpret_cast<signed char*>(&rax17) == 43) {
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rbx3->f1)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    ++r11_25;
                    rcx18 = reinterpret_cast<struct s52*>(&rbx3->f1);
                    less_or_equal26 = *reinterpret_cast<signed char*>(&rax17) <= 0x6f;
                    if (*reinterpret_cast<signed char*>(&rax17) == 0x6f) 
                        goto addr_7650_19;
                } else {
                    if (*reinterpret_cast<signed char*>(&rax17) != 44) 
                        goto addr_789a_26;
                    rbx3 = reinterpret_cast<struct s52*>(&rcx18->f1);
                    rdi8 = r11_25;
                    addr_758e_8:
                    esi22 = rbx3->f0;
                    r10d23 = 0;
                    less_or_equal27 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&esi22)) <= reinterpret_cast<signed char>(0x67);
                    if (*reinterpret_cast<void***>(&esi22) != 0x67) 
                        goto addr_759e_28;
                    while (1) {
                        r10d23 = r10d23 | 0x438;
                        rbx3 = reinterpret_cast<struct s52*>(&rbx3->f1);
                        while (esi22 = rbx3->f0, less_or_equal27 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&esi22)) <= reinterpret_cast<signed char>(0x67), !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi22) == 0x67)) {
                            addr_759e_28:
                            if (!less_or_equal27) {
                                if (*reinterpret_cast<void***>(&esi22) == 0x6f) {
                                    r10d23 = r10d23 | 0x207;
                                    rbx3 = reinterpret_cast<struct s52*>(&rbx3->f1);
                                } else {
                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi22) == 0x75)) 
                                        goto addr_75b0_34;
                                    r10d23 = r10d23 | 0x9c0;
                                    rbx3 = reinterpret_cast<struct s52*>(&rbx3->f1);
                                }
                            } else {
                                if (*reinterpret_cast<void***>(&esi22) != 97) 
                                    goto addr_75ae_37;
                                r10d23 = 0xfff;
                                rbx3 = reinterpret_cast<struct s52*>(&rbx3->f1);
                            }
                        }
                    }
                    addr_75ae_37:
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(&esi22)) > reinterpret_cast<signed char>(97)) 
                        goto addr_75b0_34;
                    eax28 = esi22 & 0xffffffef;
                    if (*reinterpret_cast<signed char*>(&eax28) == 45) 
                        goto addr_75d7_40;
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&esi22) == 43)) 
                        goto addr_75b0_34;
                    addr_75d7_40:
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rbx3->f1)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    r11_25 = rdi8 + 1;
                    rcx18 = reinterpret_cast<struct s52*>(&rbx3->f1);
                    rdi21 = reinterpret_cast<struct s53*>((rdi8 << 4) + reinterpret_cast<unsigned char>(r9_9));
                    less_or_equal26 = *reinterpret_cast<signed char*>(&rax17) <= 0x6f;
                    if (*reinterpret_cast<signed char*>(&rax17) == 0x6f) 
                        goto addr_7650_19;
                }
                if (!less_or_equal26) 
                    break;
                if (*reinterpret_cast<signed char*>(&rax17) > 55) 
                    goto addr_7668_45;
                if (*reinterpret_cast<signed char*>(&rax17) <= 47) 
                    goto addr_7602_47;
                *reinterpret_cast<uint32_t*>(&rdx29) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
                do {
                    *reinterpret_cast<uint32_t*>(&rdx29) = static_cast<uint32_t>(rax17 + rdx29 * 8 - 48);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx29) + 4) = 0;
                    rcx18 = reinterpret_cast<struct s52*>(&rcx18->f1);
                    if (*reinterpret_cast<uint32_t*>(&rdx29) > 0xfff) 
                        goto addr_75b0_34;
                    *reinterpret_cast<uint32_t*>(&rax17) = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(rcx18->f0)));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    r8d30 = static_cast<int32_t>(rax17 - 48);
                } while (*reinterpret_cast<unsigned char*>(&r8d30) <= 7);
                if (r10d23) 
                    goto addr_75b0_34;
                if (!*reinterpret_cast<signed char*>(&rax17)) 
                    goto addr_7740_53;
                if (*reinterpret_cast<signed char*>(&rax17) != 44) 
                    goto addr_75b0_34;
                addr_7740_53:
                rdi21->f0 = *reinterpret_cast<void***>(&esi22);
                rbx3 = rcx18;
                esi22 = *reinterpret_cast<uint32_t*>(&rax17);
                r10d23 = 0xfff;
                rdi21->f8 = *reinterpret_cast<uint32_t*>(&rdx29);
                edx19 = 0xfff;
                rdi21->f1 = 1;
                rdi21->f4 = 0xfff;
            }
            if (*reinterpret_cast<signed char*>(&rax17) == 0x75) {
                *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
                rcx18 = reinterpret_cast<struct s52*>(&rbx3->f2);
                edx19 = 0x1c0;
                ebx20 = 3;
                goto addr_7609_20;
            }
            ebx20 = 1;
            edx19 = 0;
            continue;
            addr_7668_45:
            if (*reinterpret_cast<signed char*>(&rax17) == 0x67) {
                *reinterpret_cast<uint32_t*>(&rax17) = rbx3->f2;
                rcx18 = reinterpret_cast<struct s52*>(&rbx3->f2);
                edx19 = 56;
                ebx20 = 3;
                goto addr_7609_20;
            }
            addr_7602_47:
            ebx20 = 1;
            edx19 = 0;
            goto addr_7609_20;
            r8d16 = static_cast<int32_t>(rax17 - 88);
        } while (*reinterpret_cast<unsigned char*>(&r8d16) > 32);
        goto addr_76f5_18;
    }
    addr_789a_26:
    if (*reinterpret_cast<signed char*>(&rax17)) {
        addr_75b0_34:
        fun_23f0(r9_9);
        *reinterpret_cast<int32_t*>(&r9_9) = 0;
        *reinterpret_cast<int32_t*>(&r9_9 + 4) = 0;
        goto addr_75bb_17;
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r9_9) + (r11_25 << 4) + 1) = 0;
        goto addr_75bb_17;
    }
}

int32_t fun_2610();

void** fun_78c3() {
    void** rax1;
    int32_t eax2;
    void** rax3;
    void** v4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    eax2 = fun_2610();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(&rax3 + 4) = 0;
    if (!eax2) {
        rax3 = xmalloc(32, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
        *reinterpret_cast<void***>(rax3) = reinterpret_cast<void**>(0x13d);
        *reinterpret_cast<int32_t*>(rax3 + 4) = 0xfff;
        *reinterpret_cast<void***>(rax3 + 8) = v4;
        *reinterpret_cast<uint32_t*>(rax3 + 12) = 0xfff;
        *reinterpret_cast<signed char*>(rax3 + 17) = 0;
    }
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2500();
    } else {
        return rax3;
    }
}

struct s54 {
    unsigned char f0;
    unsigned char f1;
    signed char[2] pad4;
    uint32_t f4;
    uint32_t f8;
    uint32_t fc;
    signed char[1] pad17;
    unsigned char f11;
};

int64_t fun_7943(uint32_t edi, int32_t esi, int32_t edx, struct s54* rcx, uint32_t* r8) {
    uint32_t r9d6;
    uint32_t edi7;
    int64_t rax8;
    int32_t r11d9;
    uint32_t r10d10;
    uint32_t edx11;
    uint32_t eax12;
    uint32_t esi13;
    uint32_t ebx14;
    uint32_t eax15;
    uint32_t edi16;
    uint32_t edi17;
    uint32_t edi18;
    uint32_t r12d19;
    uint32_t edi20;
    uint32_t eax21;
    uint32_t eax22;
    int64_t rax23;

    __asm__("cli ");
    r9d6 = edi & 0xfff;
    edi7 = rcx->f1;
    if (!*reinterpret_cast<signed char*>(&edi7)) {
        if (r8) {
            *r8 = 0;
        }
        *reinterpret_cast<uint32_t*>(&rax8) = r9d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    }
    r11d9 = esi;
    r10d10 = 0;
    edx11 = reinterpret_cast<uint32_t>(~edx);
    while (1) {
        eax12 = rcx->f8;
        if (*reinterpret_cast<signed char*>(&r11d9)) {
            esi13 = rcx->fc | 0xfffff3ff;
            ebx14 = ~rcx->fc & reinterpret_cast<uint32_t>("ettext");
            if (*reinterpret_cast<signed char*>(&edi7) != 2) {
                addr_79c1_8:
                if (*reinterpret_cast<signed char*>(&edi7) == 3) {
                    eax15 = eax12 & r9d6;
                    edi16 = eax15 & 0x124;
                    edi17 = -edi16;
                    edi18 = edi17 - (edi17 + reinterpret_cast<uint1_t>(edi17 < edi17 + reinterpret_cast<uint1_t>(!!edi16))) & 0x124;
                    r12d19 = edi18;
                    *reinterpret_cast<unsigned char*>(&r12d19) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d19) | 0x92);
                    if (*reinterpret_cast<unsigned char*>(&eax15) & 0x92) {
                        edi18 = r12d19;
                    }
                    if (*reinterpret_cast<unsigned char*>(&eax15) & 73) {
                        edi18 = edi18 | 73;
                    }
                    eax12 = eax15 | edi18;
                }
            } else {
                addr_7a75_14:
                eax12 = eax12 | 73;
                esi13 = ~ebx14;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&edi7) == 2) {
                ebx14 = r9d6 & 73;
                if (ebx14) {
                    ebx14 = 0;
                    goto addr_7a75_14;
                } else {
                    esi13 = 0xffffffff;
                }
            } else {
                esi13 = 0xffffffff;
                ebx14 = 0;
                goto addr_79c1_8;
            }
        }
        edi20 = rcx->f0;
        eax21 = eax12 & esi13;
        if (!rcx->f4) {
            eax22 = eax21 & edx11;
            if (*reinterpret_cast<signed char*>(&edi20) == 45) {
                r10d10 = r10d10 | eax22;
                r9d6 = r9d6 & ~eax22;
            } else {
                if (*reinterpret_cast<signed char*>(&edi20) == 61) {
                    addr_7a1f_24:
                    edi7 = rcx->f11;
                    rcx = reinterpret_cast<struct s54*>(&rcx->pad17);
                    r10d10 = r10d10 | esi13 & 0xfff;
                    r9d6 = ebx14 & r9d6 | eax22;
                    if (*reinterpret_cast<signed char*>(&edi7)) 
                        continue; else 
                        break;
                } else {
                    addr_7986_25:
                    if (*reinterpret_cast<signed char*>(&edi20) == 43) {
                        r10d10 = r10d10 | eax22;
                        r9d6 = r9d6 | eax22;
                    }
                }
            }
            edi7 = rcx->f11;
            rcx = reinterpret_cast<struct s54*>(&rcx->pad17);
            if (!*reinterpret_cast<signed char*>(&edi7)) 
                break;
        } else {
            eax22 = eax21 & rcx->f4;
            if (*reinterpret_cast<signed char*>(&edi20) != 45) {
                if (*reinterpret_cast<signed char*>(&edi20) != 61) 
                    goto addr_7986_25;
                ebx14 = ebx14 | ~rcx->f4;
                esi13 = ~ebx14;
                goto addr_7a1f_24;
            }
        }
    }
    if (r8) 
        goto addr_7a46_32;
    addr_7a49_33:
    *reinterpret_cast<uint32_t*>(&rax23) = r9d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
    return rax23;
    addr_7a46_32:
    *r8 = r10d10;
    goto addr_7a49_33;
}

int32_t fun_24f0();

void fd_safer(int64_t rdi);

void fun_7ae3() {
    void** rax1;
    unsigned char dl2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (dl2 & 64) {
    }
    eax3 = fun_24f0();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2500();
    } else {
        return;
    }
}

int64_t fun_2720(int64_t rdi);

int64_t fun_7b63(int64_t rdi, void** rsi, int32_t edx, void*** rcx) {
    int64_t r12_5;
    void** eax6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;
    void** r13d10;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    eax6 = openat_safer(rdi, rsi);
    if (reinterpret_cast<signed char>(eax6) >= reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<void***>(&rdi7) = eax6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        rax8 = fun_2720(rdi7);
        r12_5 = rax8;
        if (!rax8) {
            rax9 = fun_2420();
            r13d10 = *reinterpret_cast<void***>(rax9);
            fun_2570();
            *reinterpret_cast<void***>(rax9) = r13d10;
        } else {
            *rcx = eax6;
        }
    }
    return r12_5;
}

void fun_2760(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_7bc3(void** rdi) {
    int64_t rcx2;
    void** rbx3;
    void** rdx4;
    void** rax5;
    void** r12_6;
    void** rcx7;
    void** r8_8;
    int32_t eax9;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2760("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2410("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax5 = fun_2530(rdi, 47, rdx4);
        if (rax5 && ((r12_6 = rax5 + 1, reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_6) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax9 = fun_2430(rax5 + 0xfffffffffffffffa, "/.libs/", 7, rcx7, r8_8), !eax9))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax5 + 1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_6 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_6 + 2) != 45)) {
                rbx3 = r12_6;
            } else {
                rbx3 = rax5 + 4;
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_9363(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2420();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x11240;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_93a3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x11240);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_93c3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x11240);
    }
    *rdi = esi;
    return 0x11240;
}

int64_t fun_93e3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x11240);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s55 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_9423(struct s55* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s55*>(0x11240);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s56 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s56* fun_9443(struct s56* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s56*>(0x11240);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x2818;
    if (!rdx) 
        goto 0x2818;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x11240;
}

struct s57 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_9483(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s57* r8) {
    struct s57* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s57*>(0x11240);
    }
    rax7 = fun_2420();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x94b6);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s58 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_9503(int64_t rdi, int64_t rsi, void*** rdx, struct s58* rcx) {
    struct s58* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s58*>(0x11240);
    }
    rax6 = fun_2420();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x9531);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x958c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_95f3() {
    __asm__("cli ");
}

void** g11098 = reinterpret_cast<void**>(64);

int64_t slotvec0 = 0x100;

void fun_9603() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23f0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x11140) {
        fun_23f0(rdi7);
        g11098 = reinterpret_cast<void**>(0x11140);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x11090) {
        fun_23f0(r12_2);
        slotvec = reinterpret_cast<void**>(0x11090);
    }
    nslots = 1;
    return;
}

void fun_96a3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_96c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_96d3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_96f3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_9713(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x281e;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2500();
    } else {
        return rax6;
    }
}

void** fun_97a3(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2823;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2500();
    } else {
        return rax7;
    }
}

void** fun_9833(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s7* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x2828;
    rcx4 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2500();
    } else {
        return rax5;
    }
}

void** fun_98c3(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x282d;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2500();
    } else {
        return rax6;
    }
}

void** fun_9953(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s7* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x78e0]");
    __asm__("movdqa xmm1, [rip+0x78e8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x78d1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2500();
    } else {
        return rax10;
    }
}

void** fun_99f3(int64_t rdi, uint32_t esi) {
    struct s7* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x7840]");
    __asm__("movdqa xmm1, [rip+0x7848]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x7831]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2500();
    } else {
        return rax9;
    }
}

void** fun_9a93(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x77a0]");
    __asm__("movdqa xmm1, [rip+0x77a8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x7789]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2500();
    } else {
        return rax3;
    }
}

void** fun_9b23(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7710]");
    __asm__("movdqa xmm1, [rip+0x7718]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x7706]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2500();
    } else {
        return rax4;
    }
}

void** fun_9bb3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2832;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2500();
    } else {
        return rax6;
    }
}

void** fun_9c53(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x75da]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x75d2]");
    __asm__("movdqa xmm2, [rip+0x75da]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2837;
    if (!rdx) 
        goto 0x2837;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2500();
    } else {
        return rax7;
    }
}

void** fun_9cf3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s7* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x753a]");
    __asm__("movdqa xmm1, [rip+0x7542]");
    __asm__("movdqa xmm2, [rip+0x754a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x283c;
    if (!rdx) 
        goto 0x283c;
    rcx7 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2500();
    } else {
        return rax9;
    }
}

void** fun_9da3(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s7* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x748a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x7482]");
    __asm__("movdqa xmm2, [rip+0x748a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2841;
    if (!rsi) 
        goto 0x2841;
    rcx5 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2500();
    } else {
        return rax6;
    }
}

void** fun_9e43(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s7* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x73ea]");
    __asm__("movdqa xmm1, [rip+0x73f2]");
    __asm__("movdqa xmm2, [rip+0x73fa]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2846;
    if (!rsi) 
        goto 0x2846;
    rcx6 = reinterpret_cast<struct s7*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2500();
    } else {
        return rax7;
    }
}

void fun_9ee3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9ef3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9f13() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9f33(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s59 {
    int64_t f0;
    int64_t f8;
};

int32_t fun_2590(int64_t rdi, void* rsi);

struct s59* fun_9f53(struct s59* rdi) {
    void** rax2;
    int32_t eax3;
    struct s59* rax4;
    int64_t v5;
    int64_t v6;
    void* rdx7;

    __asm__("cli ");
    rax2 = g28;
    eax3 = fun_2590("/", reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
    if (eax3) {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    } else {
        rdi->f0 = v5;
        rdi->f8 = v6;
        rax4 = rdi;
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2500();
    } else {
        return rax4;
    }
}

int32_t dup_safer();

int64_t fun_9fd3(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_2420();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_2570();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s60 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_25f0(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_a033(int64_t rdi, void** rsi, void** rdx, void** rcx, struct s60* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2770(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2770(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_24c0();
    fun_2770(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_25f0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_24c0();
    fun_2770(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_25f0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_24c0();
        fun_2770(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xd288 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xd288;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_a4a3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s61 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_a4c3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s61* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s61* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2500();
    } else {
        return;
    }
}

void fun_a563(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_a606_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_a610_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2500();
    } else {
        return;
    }
    addr_a606_5:
    goto addr_a610_7;
}

void fun_a643() {
    int64_t rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rax7;

    __asm__("cli ");
    rsi1 = stdout;
    fun_25f0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_24c0();
    fun_26d0(1, rax6, "bug-coreutils@gnu.org");
    rax7 = fun_24c0();
    fun_26d0(1, rax7, "GNU coreutils", 1, rax7, "GNU coreutils");
    fun_24c0();
    goto fun_26d0;
}

int64_t fun_2470();

void xalloc_die();

void fun_a6e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2470();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a723(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2650(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a743(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2650(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a763(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2650(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a783(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rdi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a7b3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26b0(rdi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a7e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2470();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a823() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2470();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a863(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2470();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a893(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2470();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a8e3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2470();
            if (rax5) 
                break;
            addr_a92d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_a92d_5;
        rax8 = fun_2470();
        if (rax8) 
            goto addr_a916_9;
        if (rbx4) 
            goto addr_a92d_5;
        addr_a916_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_a973(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2470();
            if (rax8) 
                break;
            addr_a9ba_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_a9ba_5;
        rax11 = fun_2470();
        if (rax11) 
            goto addr_a9a2_9;
        if (!rbx6) 
            goto addr_a9a2_9;
        if (r12_4) 
            goto addr_a9ba_5;
        addr_a9a2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_aa03(void** rdi, void** rsi, void** rdx, void* rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void** r12_8;
    void* rsi9;
    void* rcx10;
    void* rbx11;
    void* rax12;
    void* rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void*>((reinterpret_cast<int64_t>(rcx10) >> 1) + reinterpret_cast<uint64_t>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<int64_t>(rbx11) <= reinterpret_cast<int64_t>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_aaad_9:
            rbx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_aac0_10:
                *r12_8 = reinterpret_cast<void*>(0);
            }
            addr_aa60_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbx11) - reinterpret_cast<uint64_t>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_aa86_12;
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_aad4_14;
            if (reinterpret_cast<int64_t>(rcx10) <= reinterpret_cast<int64_t>(rsi9)) 
                goto addr_aa7d_16;
            if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) 
                goto addr_aad4_14;
            addr_aa7d_16:
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_aad4_14;
            addr_aa86_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_26b0(rdi7);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_aad4_14;
            if (!rbp13) 
                break;
            addr_aad4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<int64_t>(rbp13) <= reinterpret_cast<int64_t>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_aaad_9;
        } else {
            if (!r13_6) 
                goto addr_aac0_10;
            goto addr_aa60_11;
        }
    }
}

void fun_ab03(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_25c0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab33(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_25c0(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab63(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ab83(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_aba3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2650(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2620;
    }
}

void fun_abe3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2650(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2620;
    }
}

void fun_ac23(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2650(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2620;
    }
}

void fun_ac63(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_24e0(rdi);
    rax5 = fun_2650(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_2620;
    }
}

void fun_aca3() {
    void** rdi1;

    __asm__("cli ");
    fun_24c0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_26f0();
    fun_2410(rdi1);
}

int64_t rpl_fts_open();

void fun_2550(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_ace3() {
    int64_t rax1;
    void** rax2;

    __asm__("cli ");
    rax1 = rpl_fts_open();
    if (!rax1) {
        rax2 = fun_2420();
        if (*reinterpret_cast<void***>(rax2) != 22) {
            xalloc_die();
        }
        fun_2550("errno != EINVAL", "lib/xfts.c", 41, "xfts_open");
    } else {
        return;
    }
}

struct s62 {
    signed char[72] pad72;
    uint32_t f48;
};

struct s63 {
    signed char[88] pad88;
    int64_t f58;
};

int64_t fun_ad33(struct s62* rdi, struct s63* rsi) {
    int32_t r8d3;
    uint32_t eax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    r8d3 = 1;
    eax4 = rdi->f48 & 17;
    if (eax4 == 16 || (r8d3 = 0, eax4 != 17)) {
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(!!rsi->f58);
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

int64_t fun_2450();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_ad73(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_2450();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_adce_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2420();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_adce_3;
            rax6 = fun_2420();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s64 {
    signed char[16] pad16;
    int64_t f10;
    int32_t f18;
};

void fun_ade3(struct s64* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f18 = 0x95f616;
    return;
}

struct s65 {
    int64_t f0;
    int64_t f8;
    uint64_t f10;
    int32_t f18;
};

struct s66 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_ae03(struct s65* rdi, struct s66* rsi) {
    uint64_t rax3;
    int64_t rdx4;
    uint64_t rcx5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f18 != 0x95f616) {
        fun_2550("state->magic == 9827862", "lib/cycle-check.c", 60, "cycle_check");
    } else {
        rax3 = rdi->f10;
        rdx4 = rsi->f8;
        if (!rax3) {
            rdi->f10 = 1;
        } else {
            if (rdi->f0 != rdx4 || rsi->f0 != rdi->f8) {
                rcx5 = rax3 + 1;
                rdi->f10 = rcx5;
                if (!(rax3 & rcx5)) {
                    if (!rcx5) {
                        return 1;
                    }
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        }
        rax6 = rsi->f0;
        rdi->f0 = rdx4;
        rdi->f8 = rax6;
        return 0;
    }
}

struct s67 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2630(struct s67* rdi);

int32_t fun_2690(struct s67* rdi);

int64_t fun_2540(int64_t rdi, ...);

int32_t rpl_fflush(struct s67* rdi);

int64_t fun_24a0(struct s67* rdi);

int64_t fun_ae93(struct s67* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2630(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2690(rdi);
        if (!(eax3 && (eax4 = fun_2630(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2540(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2420();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_24a0(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_24a0;
}

uint32_t fun_2480(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_af23(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_2480(rdi);
        r12d10 = eax9;
        goto addr_b024_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_2480(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_b024_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_2500();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_b0d9_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_2480(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_2480(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_2420();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_2570();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_b024_3;
                }
            }
        } else {
            eax22 = fun_2480(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_2420(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_b024_3;
            } else {
                eax25 = fun_2480(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_b024_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_b0d9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_af89_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_af8d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_af8d_18:
            if (0) {
            }
        } else {
            addr_afd5_23:
            eax28 = fun_2480(rdi);
            r12d10 = eax28;
            goto addr_b024_3;
        }
        eax29 = fun_2480(rdi);
        r12d10 = eax29;
        goto addr_b024_3;
    }
    if (0) {
    }
    eax30 = fun_2480(rdi);
    r12d10 = eax30;
    goto addr_b024_3;
    addr_af89_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_af8d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_afd5_23;
        goto addr_af8d_18;
    }
}

int32_t fun_2700();

void fun_b193() {
    void** rax1;
    unsigned char sil2;
    int32_t eax3;
    int64_t rdi4;
    void* rdx5;

    __asm__("cli ");
    rax1 = g28;
    if (sil2 & 64) {
    }
    eax3 = fun_2700();
    *reinterpret_cast<int32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    fd_safer(rdi4);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2500();
    } else {
        return;
    }
}

void rpl_fseeko(struct s67* rdi);

void fun_b213(struct s67* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2690(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_b263(struct s67* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2630(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2540(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_2680(int64_t rdi);

signed char* fun_b2e3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2680(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2520(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_b323(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2520(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2500();
    } else {
        return r12_7;
    }
}

void fun_b3b3() {
    __asm__("cli ");
    goto rpl_fcntl;
}

int32_t setlocale_null_r();

int64_t fun_b3d3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2500();
    } else {
        return rax3;
    }
}

int64_t fun_b453(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_26c0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_24e0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2620(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2620(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_b503() {
    __asm__("cli ");
    goto fun_26c0;
}

void fun_b513() {
    __asm__("cli ");
}

void fun_b527() {
    __asm__("cli ");
    return;
}

void fun_2d71() {
    int1_t zf1;
    void** r15_2;
    void** rdx3;
    void** rax4;
    void** rdx5;
    int1_t zf6;
    int32_t ebx7;
    void** r15_8;
    void** rax9;
    void** rax10;

    zf1 = force_silent == 0;
    if (zf1) {
        quotearg_style(4, r15_2, rdx3);
        rax4 = fun_24c0();
        rdx5 = rax4;
        fun_26f0();
    }
    zf6 = verbosity == 2;
    if (zf6 || (ebx7 = verbosity, !!ebx7)) {
        goto 0x2d56;
    } else {
        rax9 = quotearg_style(4, r15_8, rdx5, 4, r15_8, rdx5);
        rax10 = fun_24c0();
        fun_26d0(1, rax10, rax9, 1, rax10, rax9);
        goto 0x2d56;
    }
}

void fun_2dfc() {
    int1_t zf1;
    void** r15_2;
    void** rdx3;

    zf1 = force_silent == 0;
    if (!zf1) 
        goto 0x2d7e;
    quotearg_style(4, r15_2, rdx3);
    fun_24c0();
    fun_26f0();
    goto 0x2d7e;
}

signed char cycle_warning_required(int64_t rdi, int64_t rsi);

void fun_2e43() {
    int64_t r12_1;
    int64_t rbp2;
    signed char al3;

    al3 = cycle_warning_required(r12_1, rbp2);
    if (!al3) 
        goto 0x2bf6;
    quotearg_n_style_colon();
    fun_24c0();
    fun_26f0();
    goto 0x2d69;
}

struct s68 {
    signed char[88] pad88;
    int64_t f58;
};

struct s69 {
    signed char[32] pad32;
    int64_t f20;
};

struct s70 {
    signed char[32] pad32;
    int64_t f20;
};

void fun_2e93() {
    struct s68* rbp1;
    struct s69* rbp2;
    int1_t zf3;
    void** r15_4;
    void** rdx5;
    struct s70* rbp6;
    struct s10* r12_7;
    void** rbp8;
    void** rcx9;
    void** r8_10;
    void** r9_11;

    if (rbp1->f58 || rbp2->f20) {
        zf3 = force_silent == 0;
        if (!zf3) 
            goto 0x2d7e;
        quotearg_style(4, r15_4, rdx5);
        goto 0x2e25;
    } else {
        rbp6->f20 = 1;
        rpl_fts_set(r12_7, rbp8, 1, rcx9, r8_10, r9_11, __return_address());
        goto 0x2d69;
    }
}

void fun_2ed3() {
    goto 0x2d69;
}

void fun_7880() {
    goto 0x76e7;
}

uint32_t fun_25a0(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2790(int64_t rdi, void** rsi);

uint32_t fun_2780(void** rdi, void** rsi);

void** fun_27c0(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_7df5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_24c0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_24c0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24e0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_80f3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_80f3_22; else 
                            goto addr_84ed_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_85ad_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_8900_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_80f0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_80f0_30; else 
                                goto addr_8919_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24e0(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_8900_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_25a0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_8900_28; else 
                            goto addr_7f9c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_8a60_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_88e0_40:
                        if (r11_27 == 1) {
                            addr_846d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_8a28_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_80a7_44;
                            }
                        } else {
                            goto addr_88f0_46;
                        }
                    } else {
                        addr_8a6f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_846d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_80f3_22:
                                if (v47 != 1) {
                                    addr_8649_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_24e0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_8694_54;
                                    }
                                } else {
                                    goto addr_8100_56;
                                }
                            } else {
                                addr_80a5_57:
                                ebp36 = 0;
                                goto addr_80a7_44;
                            }
                        } else {
                            addr_88d4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_8a6f_47; else 
                                goto addr_88de_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_846d_41;
                        if (v47 == 1) 
                            goto addr_8100_56; else 
                            goto addr_8649_52;
                    }
                }
                addr_8161_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_7ff8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_801d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_8320_65;
                    } else {
                        addr_8189_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_89d8_67;
                    }
                } else {
                    goto addr_8180_69;
                }
                addr_8031_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_807c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_89d8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_807c_81;
                }
                addr_8180_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_801d_64; else 
                    goto addr_8189_66;
                addr_80a7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_815f_91;
                if (v22) 
                    goto addr_80bf_93;
                addr_815f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8161_62;
                addr_8694_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_8e1b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_8e8b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_8c8f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2790(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2780(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_878e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_814c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_8798_112;
                    }
                } else {
                    addr_8798_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_8869_114;
                }
                addr_8158_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_815f_91;
                while (1) {
                    addr_8869_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_8d77_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_87d6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_8d85_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_8857_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_8857_128;
                        }
                    }
                    addr_8805_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_8857_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_87d6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_8805_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_807c_81;
                addr_8d85_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_89d8_67;
                addr_8e1b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_878e_109;
                addr_8e8b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_878e_109;
                addr_8100_56:
                rax93 = fun_27c0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_814c_110;
                addr_88de_59:
                goto addr_88e0_40;
                addr_85ad_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_80f3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_8158_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_80a5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_80f3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_85f2_160;
                if (!v22) 
                    goto addr_89c7_162; else 
                    goto addr_8bd3_163;
                addr_85f2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_89c7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_89d8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_849b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_8303_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_8031_70; else 
                    goto addr_8317_169;
                addr_849b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_7ff8_63;
                goto addr_8180_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_88d4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_8a0f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_80f0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_7fe8_178; else 
                        goto addr_8992_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_88d4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_80f3_22;
                }
                addr_8a0f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_80f0_30:
                    r8d42 = 0;
                    goto addr_80f3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_8161_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_8a28_42;
                    }
                }
                addr_7fe8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_7ff8_63;
                addr_8992_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_88f0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_8161_62;
                } else {
                    addr_89a2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_80f3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_9152_188;
                if (v28) 
                    goto addr_89c7_162;
                addr_9152_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_8303_168;
                addr_7f9c_37:
                if (v22) 
                    goto addr_8f93_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_7fb3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_8a60_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_8aeb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_80f3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_7fe8_178; else 
                        goto addr_8ac7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_88d4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_80f3_22;
                }
                addr_8aeb_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_80f3_22;
                }
                addr_8ac7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_88f0_46;
                goto addr_89a2_186;
                addr_7fb3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_80f3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_80f3_22; else 
                    goto addr_7fc4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_909e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_8f24_210;
            if (1) 
                goto addr_8f22_212;
            if (!v29) 
                goto addr_8b5e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_24d0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_9091_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_8320_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_80db_219; else 
            goto addr_833a_220;
        addr_80bf_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_80d3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_833a_220; else 
            goto addr_80db_219;
        addr_8c8f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_833a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_24d0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_8cad_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_24d0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_9120_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_8b86_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_8d77_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_80d3_221;
        addr_8bd3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_80d3_221;
        addr_8317_169:
        goto addr_8320_65;
        addr_909e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_833a_220;
        goto addr_8cad_222;
        addr_8f24_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_8f7e_236;
        fun_2500();
        rsp25 = rsp25 - 8 + 8;
        goto addr_9120_225;
        addr_8f22_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_8f24_210;
        addr_8b5e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_8f24_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_8b86_226;
        }
        addr_9091_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_84ed_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xcd4c + rax113 * 4) + 0xcd4c;
    addr_8919_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xce4c + rax114 * 4) + 0xce4c;
    addr_8f93_190:
    addr_80db_219:
    goto 0x7dc0;
    addr_7fc4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xcc4c + rax115 * 4) + 0xcc4c;
    addr_8f7e_236:
    goto v116;
}

void fun_7fe0() {
}

void fun_8198() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x7e92;
}

void fun_81f1() {
    goto 0x7e92;
}

void fun_82de() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x8161;
    }
    if (v2) 
        goto 0x8bd3;
    if (!r10_3) 
        goto addr_8d3e_5;
    if (!v4) 
        goto addr_8c0e_7;
    addr_8d3e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_8c0e_7:
    goto 0x8014;
}

void fun_82fc() {
}

void fun_83a7() {
    signed char v1;

    if (v1) {
        goto 0x832f;
    } else {
        goto 0x806a;
    }
}

void fun_83c1() {
    signed char v1;

    if (!v1) 
        goto 0x83ba; else 
        goto "???";
}

void fun_83e8() {
    goto 0x8303;
}

void fun_8468() {
}

void fun_8480() {
}

void fun_84af() {
    goto 0x8303;
}

void fun_8501() {
    goto 0x8490;
}

void fun_8530() {
    goto 0x8490;
}

void fun_8563() {
    goto 0x8490;
}

void fun_8930() {
    goto 0x7fe8;
}

void fun_8c2e() {
    signed char v1;

    if (v1) 
        goto 0x8bd3;
    goto 0x8014;
}

void fun_8cd5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x8014;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x7ff8;
        goto 0x8014;
    }
}

void fun_90f2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x8360;
    } else {
        goto 0x7e92;
    }
}

void fun_a108() {
    fun_24c0();
}

void fun_2dcc() {
    int1_t zf1;

    zf1 = force_silent == 0;
    if (!zf1) 
        goto 0x2d7e;
    quotearg_n_style_colon();
    fun_26f0();
    goto 0x2d7e;
}

void fun_77d0() {
}

void fun_821e() {
    goto 0x7e92;
}

void fun_83f4() {
    goto 0x83ac;
}

void fun_84bb() {
    goto 0x7fe8;
}

void fun_850d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x8490;
    goto 0x80bf;
}

void fun_853f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x849b;
        goto 0x7ec0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x833a;
        goto 0x80db;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x8cd8;
    if (r10_8 > r15_9) 
        goto addr_8425_9;
    addr_842a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x8ce3;
    goto 0x8014;
    addr_8425_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_842a_10;
}

void fun_8572() {
    goto 0x80a7;
}

void fun_8940() {
    goto 0x80a7;
}

void fun_90df() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x81fc;
    } else {
        goto 0x8360;
    }
}

void fun_a1c0() {
}

void fun_77e8() {
    goto 0x77d8;
}

void fun_857c() {
    goto 0x8517;
}

void fun_894a() {
    goto 0x846d;
}

void fun_a220() {
    fun_24c0();
    goto fun_2770;
}

void fun_77f0() {
    goto 0x77d8;
}

void fun_824d() {
    goto 0x7e92;
}

void fun_8588() {
    goto 0x8517;
}

void fun_8957() {
    goto 0x84be;
}

void fun_a260() {
    fun_24c0();
    goto fun_2770;
}

void fun_77f8() {
    goto 0x77d8;
}

void fun_827a() {
    goto 0x7e92;
}

void fun_8594() {
    goto 0x8490;
}

void fun_a2a0() {
    fun_24c0();
    goto fun_2770;
}

void fun_7800() {
    goto 0x77d8;
}

void fun_829c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x8c30;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x8161;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x8161;
    }
    if (v11) 
        goto 0x8f93;
    if (r10_12 > r15_13) 
        goto addr_8fe3_8;
    addr_8fe8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x8d21;
    addr_8fe3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_8fe8_9;
}

struct s71 {
    signed char[24] pad24;
    int64_t f18;
};

struct s72 {
    signed char[16] pad16;
    void** f10;
};

struct s73 {
    signed char[8] pad8;
    void** f8;
};

void fun_a2f0() {
    int64_t r15_1;
    struct s71* rbx2;
    void** r14_3;
    struct s72* rbx4;
    void** r13_5;
    struct s73* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_24c0();
    fun_2770(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0xa312, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_a348() {
    fun_24c0();
    goto 0xa319;
}

struct s74 {
    signed char[32] pad32;
    int64_t f20;
};

struct s75 {
    signed char[24] pad24;
    int64_t f18;
};

struct s76 {
    signed char[16] pad16;
    void** f10;
};

struct s77 {
    signed char[8] pad8;
    void** f8;
};

struct s78 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_a380() {
    int64_t rcx1;
    struct s74* rbx2;
    int64_t r15_3;
    struct s75* rbx4;
    void** r14_5;
    struct s76* rbx6;
    void** r13_7;
    struct s77* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s78* rbx12;
    void** rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_24c0();
    fun_2770(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0xa3b4, __return_address(), rcx1);
    goto v15;
}

void fun_a3f8() {
    fun_24c0();
    goto 0xa3bb;
}
