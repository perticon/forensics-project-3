void main(char * rsi[], int32 edi, struct Eq_485 * fs)
{
	ptr64 fp;
	set_program_name(rsi[0]);
	fn00000000000026C0("", 0x06);
	fn00000000000024B0("/usr/local/share/locale", "coreutils");
	fn0000000000002490("coreutils");
	atexit(&g_t3A80);
	g_b11108 = 0x00;
	g_b11109 = 0x00;
	g_b1110A = 0x00;
	char * rbx_186[] = rsi;
	Eq_26 rsp_1307 = fp - 0x0138;
	Eq_526 qwLocF0_1941 = 0x00;
	uint64 r12_120 = 0x00;
	char * qwLoc0120_1942 = null;
	Eq_526 qwLoc0130_1943 = 0x00;
	union Eq_534 * qwLoc0128_1944 = null;
	while (true)
	{
		byte r12b_1890 = (byte) r12_120;
		int32 eax_91 = fn0000000000002510(&g_t10A20, "Rcfvr::w::x::X::s::t::u::g::o::a::,::+::=::0::1::2::3::4::5::6::7::", rsi, edi, null);
		if (eax_91 == ~0x00)
			break;
		if (eax_91 > 0x78)
		{
			if (eax_91 == 0x81)
			{
				r12_120 = 0x01;
				continue;
			}
			if (eax_91 != 0x82)
			{
				if (eax_91 != 0x80)
					goto l0000000000002967;
				r12_120 = 0x00;
				continue;
			}
			else
			{
				qwLoc0120_1942 = optarg;
				continue;
			}
		}
		if (eax_91 > 101)
		{
			ui32 edx_1773 = 0x01 << (word32) ((byte) eax_91 - 0x66);
			if ((edx_1773 & 0x0006F202) == 0x00)
			{
				if (eax_91 == 118)
				{
					g_dw11010 = 0x00;
					continue;
				}
				else
				{
					if ((edx_1773 & 0x01) == 0x00)
						goto l0000000000002967;
					g_b11109 = 0x01;
					continue;
				}
			}
l0000000000002A60:
			Eq_26 r9_1794 = (rsi - 8)[(int64) g_dw110B0];
			Eq_8 rax_1798 = fn00000000000024E0(r9_1794);
			int64 rcx_1805 = qwLoc0130_1943 - ~0x00 - (word64) (qwLoc0130_1943 < 0x01);
			Eq_526 r8_1806 = (word64) rax_1798 + rcx_1805;
			if (qwLocF0_1941 <= r8_1806)
			{
				qwLocF0_1941 = (word64) r8_1806.u1 + 1;
				qwLoc0128_1944 = x2realloc(fp - 0xF0, qwLoc0128_1944);
			}
			Mem1840[qwLoc0128_1944 + qwLoc0130_1943:byte] = 44;
			fn0000000000002620((word64) rax_1798 + 1, r9_1794, (char *) qwLoc0128_1944 + rcx_1805);
			g_b11108 = 0x01;
			qwLoc0130_1943 = r8_1806;
			continue;
		}
		if (eax_91 > 99)
			goto l0000000000002967;
		if (eax_91 <= 0x2A)
		{
			if (eax_91 == ~0x82)
			{
				version_etc(0xC004, stdout, fs);
				fn0000000000002750(0x00);
			}
			else
			{
				if (eax_91 != ~0x81)
					goto l0000000000002967;
				usage(0x00);
			}
		}
		if (__bt<word64>(0x40200000041FE3, (uint64) (eax_91 - 0x2B)))
			goto l0000000000002A60;
		if (eax_91 != 99)
		{
			if (eax_91 != 0x52)
				goto l0000000000002967;
			g_b1110A = 0x01;
			continue;
		}
		g_dw11010 = 0x01;
	}
	char * rsi_1696;
	if (qwLoc0120_1942 != null)
	{
		if (qwLoc0128_1944 != null)
		{
			rsi_1696 = (char *) "cannot combine mode and --reference options";
			goto l0000000000002B3C;
		}
		if (edi > g_dw110B0)
		{
			struct Eq_636 * rax_105 = mode_create_from_ref(qwLoc0120_1942, fs);
			change = rax_105;
			if (rax_105 != null)
				goto l0000000000002B7C;
			quotearg_style(0x04, fs);
			fn00000000000026F0(fn00000000000024C0(0x05, "failed to get attributes of %s", null), *fn0000000000002420(), 0x01);
l0000000000003532:
			quote(fs);
			fn00000000000026F0(fn00000000000024C0(0x05, "invalid mode: %s", null), 0x00, 0x00);
			usage(0x01);
		}
l000000000000308C:
		rsi_1696 = (char *) "missing operand";
l0000000000002B3C:
		fn00000000000026F0(fn00000000000024C0(0x05, rsi_1696, null), 0x00, 0x00);
l0000000000002967:
		usage(0x01);
	}
	int32 eax_374;
	int64 rdx_369 = (int64) g_dw110B0;
	int32 edx_382 = (word32) rdx_369;
	if (qwLoc0128_1944 != null)
	{
		eax_374 = edx_382;
		if (edi <= edx_382)
			goto l000000000000307A;
	}
	else
	{
		union Eq_534 * rcx_371 = rsi[rdx_369];
		eax_374 = (word32) rdx_369 + 0x01;
		g_dw110B0 = eax_374;
		qwLoc0128_1944 = rcx_371;
		if (eax_374 >= edi)
		{
			if (rcx_371 == null)
				goto l000000000000308C;
l000000000000307A:
			if ((rsi - 8)[(int64) eax_374] == qwLoc0128_1944)
				goto l0000000000003446;
			goto l000000000000308C;
		}
	}
	struct Eq_636 * rax_391 = mode_compile(qwLoc0128_1944, out rbx_186);
	change = rax_391;
	rsp_1307.u0 = <invalid>;
	word32 eax_455 = (word32) rax_391;
	if (rax_391 != null)
	{
		fn0000000000002600();
		g_dw1110C = eax_455;
l0000000000002B7C:
		if (g_b1110A != 0x00 && r12b_1890 != 0x00)
		{
			struct Eq_672 * rax_165 = get_root_dev_ino(&g_t110F0, fs);
			root_dev_ino = rax_165;
			if (rax_165 == null)
			{
				quotearg_style(0x04, fs);
				fn00000000000026F0(fn00000000000024C0(0x05, "failed to get attributes of %s", null), *fn0000000000002420(), 0x01);
l0000000000003446:
				quote(fs);
				fn00000000000026F0(fn00000000000024C0(0x05, "missing operand after %s", null), 0x00, 0x00);
				goto l0000000000002967;
			}
		}
		else
			root_dev_ino = null;
		Eq_8 rax_241 = xfts_open(0x00, 0x0411, rbx_186 + (int64) g_dw110B0, fs);
		uint64 r14_1690 = 0x01;
		while (true)
		{
			ui32 r14d_313 = (word32) r14_1690;
			struct Eq_700 * rax_259 = rpl_fts_read(rax_241, fs);
			if (rax_259 == null)
				break;
			word24 eax_24_8_1659;
			ui32 eax_1649;
			up32 ebx_1084;
			char * r15_510 = rax_259->ptr0038;
			cup16 ax_517 = rax_259->w0068 - 0x02;
			if (ax_517 <= 11)
			{
				char * rsi_612;
				int64 rax_524 = (int64) g_aC994[(uint64) ax_517 * 0x04] + 0xC994;
				eax_24_8_1659 = SLICE(rax_524, word24, 8);
				switch ((word16) rax_524)
				{
				case 0x00:
					if (cycle_warning_required(rax_259, rax_241) == 0x00)
						goto l0000000000002BF6;
					quotearg_n_style_colon(0x03, 0x00, fs);
					fn00000000000026F0(fn00000000000024C0(0x05, "WARNING: Circular directory structure.\nThis almost certainly means that you have a corrupted file system.\nNOTIFY YOUR SYSTEM MANAGER.\nThe following directory is part of the cycle:\n  %s\n", null), 0x00, 0x00);
					eax_1649 = 0x00;
					goto l0000000000002D69;
				case 0x01:
				case 0x03:
				case 0x06:
				case 0x07:
				case 0x09:
				case 0x0A:
					goto l0000000000002BF6;
				case 0x02:
					if (g_b11109 == 0x00)
					{
						quotearg_style(0x04, fs);
						rsi_612 = (char *) "cannot read directory %s";
						goto l0000000000002E25;
					}
					break;
				case 0x04:
					eax_1649 = 0x01;
					goto l0000000000002D69;
				case 0x05:
					if (g_b11109 == 0x00)
					{
						quotearg_n_style_colon(0x03, 0x00, fs);
						fn00000000000026F0(49433, rax_259->t0040, 0x00);
						eax_24_8_1659 = 0x00;
					}
					break;
				case 0x08:
					if (rax_259->qw0058 == 0x00 && rax_259->qw0020 == 0x00)
					{
						rax_259->qw0020 = 0x01;
						rpl_fts_set(0x01, rax_259);
						eax_1649 = 0x01;
						goto l0000000000002D69;
					}
					if (g_b11109 == 0x00)
					{
						quotearg_style(0x04, fs);
						rsi_612 = (char *) "cannot access %s";
l0000000000002E25:
						fn00000000000026F0(fn00000000000024C0(0x05, rsi_612, null), rax_259->t0040, 0x00);
						eax_24_8_1659 = 0x00;
					}
					break;
				case 11:
					if (g_b11109 == 0x00)
					{
						quotearg_style(0x04, fs);
						fn00000000000026F0(fn00000000000024C0(0x05, "cannot operate on dangling symlink %s", null), 0x00, 0x00);
						eax_24_8_1659 = 0x00;
					}
					break;
				}
				if (g_dw11010 != 0x02)
				{
					ebx_1084 = g_dw11010;
					if (ebx_1084 != 0x00)
						goto l0000000000002D91;
					quotearg_style(0x04, fs);
					eax_24_8_1659 = SLICE(fn00000000000026D0(fn00000000000024C0(0x05, "%s could not be accessed\n", null), 0x01), word24, 8);
				}
				else
				{
l0000000000002D91:
					ebx_1084 = 0x00;
				}
				goto l0000000000002D56;
			}
l0000000000002BF6:
			struct Eq_672 * rax_762 = root_dev_ino;
			if (rax_762 != null && (rax_259->qw0078 == rax_762->qw0000 && rax_259->qw0070 == rax_762->qw0008))
			{
				if (fn00000000000025D0("/", r15_510) == 0x00)
				{
					quotearg_style(0x04, fs);
					fn00000000000026F0(fn00000000000024C0(0x05, "it is dangerous to operate recursively on %s", null), 0x00, 0x00);
				}
				else
				{
					quotearg_n_style(0x04, 0x01, fs);
					quotearg_n_style(0x04, 0x00, fs);
					fn00000000000026F0(fn00000000000024C0(0x05, "it is dangerous to operate recursively on %s (same as %s)", null), 0x00, 0x00);
				}
				fn00000000000026F0(fn00000000000024C0(0x05, "use --no-preserve-root to override this failsafe", null), 0x00, 0x00);
				rpl_fts_set(0x04, rax_259);
				rpl_fts_read(rax_241, fs);
				eax_1649 = 0x00;
				goto l0000000000002D69;
			}
			Eq_26 rsi_1145;
			Eq_26 rax_1142;
			uint64 rbx_1046;
			uint64 r13_1060 = (uint64) rax_259->dw0088;
			ui32 r13d_938 = (word32) r13_1060;
			uint64 rax_943 = (uint64) (r13d_938 & 0xF000);
			word32 eax_944 = (word32) rax_943;
			eax_24_8_1659 = SLICE(rax_943, word24, 8);
			if (eax_944 != 0xA000)
			{
				uint64 rax_975 = (uint64) mode_adjust(change, g_dw1110C, (word32) (eax_944 == 0x4000), r13d_938, null);
				word32 eax_996 = (word32) rax_975;
				rsp_1307.u0[8] = (char) eax_996;
				fn0000000000002670();
				eax_24_8_1659 = SLICE(rax_975, word24, 8);
				if (eax_996 == 0x00)
				{
					if (g_dw11010 != 0x02)
					{
						uint64 rcx_1171 = (uint64) rsp_1307.u0[8];
						uint64 rax_1176 = (uint64) (word32) rcx_1171;
						word32 eax_1189 = (word32) rax_1176;
						eax_24_8_1659 = SLICE(rax_1176, word24, 8);
						if ((SLICE(rcx_1171, byte, 8) & 0x0E) != 0x00)
						{
							fn00000000000027B0();
							if (eax_1189 != 0x00)
							{
								if (g_b11109 == 0x00)
								{
									*((word64) rsp_1307 + 16) = quotearg_style(0x04, fs);
									fn00000000000026F0(fn00000000000024C0(0x05, "getting new attributes of %s", null), *fn0000000000002420(), 0x00);
									eax_24_8_1659 = 0x00;
								}
l0000000000003199:
								ui32 r13d_1294 = (word32) r13_1060;
								if (g_dw11010 == 0x00)
								{
									quotearg_style(0x04, fs);
									strmode((word64) rsp_1307 + 224, *((word64) rsp_1307 + 8));
									struct Eq_1278 * rsp_1292 = (word64) rsp_1307 + 8;
									rsp_1292->b00EA = 0x00;
									strmode(&rsp_1292->b00EA + 2, r13d_1294);
									rsp_1292->b00FE = 0x00;
									rsp_1307 = &rsp_1292->dw0000 + 2;
									eax_24_8_1659 = SLICE(fn00000000000026D0(fn00000000000024C0(0x05, "mode of %s retained as %04lo (%s)\n", null), 0x01), word24, 8);
								}
								ebx_1084 = 0x03;
								goto l0000000000002D49;
							}
							eax_1189 = (word32) *((word64) rsp_1307 + 0x0068);
						}
						uint64 rax_1237 = (uint64) (eax_1189 ^ r13d_938);
						eax_24_8_1659 = SLICE(rax_1237, word24, 8);
						if (((word32) rax_1237 & 0x0FFF) == 0x00)
							goto l0000000000003199;
						Eq_651 rax_1343 = quotearg_style(0x04, fs);
						ui32 edi_1357 = rsp_1307.u0[8];
						rsp_1307.u0[24] = (char) rax_1343;
						rsp_1307.u0[32] = (char) (uint64) (r13d_938 & 0x0FFF);
						rsp_1307.u0[16] = (char) (uint64) (edi_1357 & 0x0FFF);
						strmode(rsp_1307.u0 + 224, edi_1357);
						struct Eq_1101 * rsp_1379 = rsp_1307.u0 + 8;
						rsp_1379->b00EA = 0x00;
						strmode(&rsp_1379->b00EA + 2, r13d_938);
						rsp_1379->b00FE = 0x00;
						rax_1142 = fn00000000000024C0(0x05, "mode of %s changed from %04lo (%s) to %04lo (%s)\n", null);
						ebx_1084 = 0x04;
						rsi_1145 = rax_1142;
						goto l0000000000002D1C;
					}
					ebx_1084 = 0x04;
					goto l0000000000002D49;
				}
				if (g_b11109 == 0x00)
				{
					*((word64) rsp_1307 + 16) = quotearg_style(0x04, fs);
					fn00000000000026F0(fn00000000000024C0(0x05, "changing permissions of %s", null), *fn0000000000002420(), 0x00);
					eax_24_8_1659 = 0x00;
				}
				rbx_1046 = 0x01;
			}
			else
			{
				rsp_1307.u0[8] = (char) 0x00;
				r13_1060 = 0x00;
				rbx_1046 = 0x02;
			}
			ebx_1084 = (word32) rbx_1046;
			ui32 r13d_1089 = (word32) r13_1060;
			if (g_dw11010 != 0x02 && g_dw11010 == 0x00)
			{
				Eq_651 rax_1075 = quotearg_style(0x04, fs);
				if (ebx_1084 == 0x02)
				{
					*((word64) rsp_1307 + 8) = rax_1075;
					eax_24_8_1659 = SLICE(fn00000000000026D0(fn00000000000024C0(0x05, "neither symbolic link %s nor referent has been changed\n", null), 0x01), word24, 8);
				}
				else
				{
					ui32 edi_1099 = *((word64) rsp_1307 + 8);
					*((word64) rsp_1307 + 24) = rax_1075;
					*((word64) rsp_1307 + 32) = (uint64) (r13d_1089 & 0x0FFF);
					*((word64) rsp_1307 + 16) = (uint64) (edi_1099 & 0x0FFF);
					strmode((word64) rsp_1307 + 224, edi_1099);
					struct Eq_1431 * rsp_1118 = (word64) rsp_1307 + 8;
					rsp_1118->b00EA = 0x00;
					strmode(&rsp_1118->b00EA + 2, r13d_1089);
					rsp_1118->b00FE = 0x00;
					rax_1142 = fn00000000000024C0(0x05, "failed to change mode of %s from %04lo (%s) to %04lo (%s)\n", null);
					ebx_1084 = 0x01;
					rsi_1145 = rax_1142;
l0000000000002D1C:
					Eq_26 rsp_1407 = (word64) rsp_1307 + 16;
					*((word64) rsp_1407 - 8) = rax_1142;
					*((word64) rsp_1407 - 16) = (word64) rsp_1407 + 225;
					eax_24_8_1659 = SLICE(fn00000000000026D0(rsi_1145, 0x01), word24, 8);
					rsp_1307 = rsp_1407;
					if (ebx_1084 > 0x02)
					{
l0000000000002D49:
						ui32 r13d_1440 = (word32) r13_1060;
						if (g_b11108 != 0x00)
						{
							ui32 eax_1479 = mode_adjust(change, 0x00, (word32) ((r13d_1440 & 0xF000) == 0x4000), r13d_1440, null);
							uint64 rax_1483 = (uint64) ~eax_1479;
							eax_24_8_1659 = SLICE(rax_1483, word24, 8);
							if ((rsp_1307.u0[8] & (word32) rax_1483) != 0x00)
							{
								strmode(rsp_1307.u0 + 224, rsp_1307.u0[8]);
								struct Eq_1216 * rsp_1504 = rsp_1307.u0 + 8;
								strmode(&rsp_1504->dw0000 + 59, eax_1479);
								rsp_1504->b00FE = 0x00;
								rsp_1504->b00F2 = 0x00;
								quotearg_n_style_colon(0x03, 0x00, fs);
								fn00000000000026F0(fn00000000000024C0(0x05, "%s: new permissions are %s, not %s", null), 0x00, 0x00);
								ebx_1084 = 0x01;
								rsp_1307 = &rsp_1504->dw0000 + 2;
								eax_24_8_1659 = 0x00;
							}
						}
					}
				}
			}
l0000000000002D56:
			if (g_b1110A == 0x00)
				eax_24_8_1659 = SLICE((uint64) rpl_fts_set(0x04, rax_259), word24, 8);
			eax_1649 = SEQ(eax_24_8_1659, (int8) (ebx_1084 > 0x01));
l0000000000002D69:
			r14_1690 = (uint64) (r14d_313 & eax_1649);
		}
		union Eq_9 * rax_281 = fn0000000000002420();
		if (*rax_281 != 0x00)
		{
			uint64 r14_287 = (uint64) g_b11109;
			r14d_313 = (word32) r14_287;
			if ((byte) r14_287 != 0x00)
				r14d_313 = 0x00;
			else
				fn00000000000026F0(fn00000000000024C0(0x05, "fts_read failed", null), *rax_281, 0x00);
		}
		if (rpl_fts_close(rax_241) != 0x00)
		{
			fn00000000000026F0(fn00000000000024C0(0x05, "fts_close failed", null), *rax_281, 0x00);
			r14d_313 = 0x00;
		}
		fn0000000000002750((word32) ((byte) r14d_313 ^ 0x01));
	}
	goto l0000000000003532;
}