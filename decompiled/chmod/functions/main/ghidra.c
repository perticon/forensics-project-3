void main(int param_1,undefined8 *param_2)

{
  bool bVar1;
  char cVar2;
  int iVar3;
  uint __mode;
  __mode_t _Var4;
  uint uVar5;
  size_t sVar6;
  undefined8 uVar7;
  long lVar8;
  undefined8 uVar9;
  int *piVar10;
  undefined8 uVar11;
  long lVar12;
  ulong uVar13;
  uint uVar14;
  undefined8 *puVar15;
  char *pcVar16;
  char *pcVar17;
  uint uVar18;
  long in_FS_OFFSET;
  ulong local_130;
  long local_128;
  long local_120;
  ulong local_f0;
  stat local_e8;
  undefined local_58;
  undefined local_57 [9];
  undefined local_4e;
  undefined local_4c;
  undefined local_4b [9];
  undefined local_42;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  local_f0 = 0;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  bVar1 = false;
  textdomain("coreutils");
  atexit(close_stdout);
  diagnose_surprises = '\0';
  force_silent = 0;
  recurse = '\0';
  local_120 = 0;
  local_130 = 0;
  local_128 = 0;
  while (puVar15 = param_2,
        iVar3 = getopt_long(param_1,param_2,
                            "Rcfvr::w::x::X::s::t::u::g::o::a::,::+::=::0::1::2::3::4::5::6::7::",
                            long_options,0), iVar3 != -1) {
    if (iVar3 < 0x79) {
      if (iVar3 < 0x66) {
        if (99 < iVar3) goto LAB_00102967;
        if (iVar3 < 0x2b) {
          if (iVar3 == -0x83) {
            version_etc(stdout,"chmod","GNU coreutils",Version,"David MacKenzie","Jim Meyering",0,
                        puVar15);
                    /* WARNING: Subroutine does not return */
            exit(0);
          }
          if (iVar3 == -0x82) {
                    /* WARNING: Subroutine does not return */
            usage(0);
          }
          goto LAB_00102967;
        }
        if ((0x40200000041fe3U >> ((ulong)(iVar3 - 0x2b) & 0x3f) & 1) == 0) {
          if (iVar3 == 99) {
            verbosity = 1;
          }
          else {
            if (iVar3 != 0x52) goto LAB_00102967;
            recurse = '\x01';
          }
        }
        else {
LAB_00102a60:
          pcVar16 = (char *)param_2[(long)optind + -1];
          sVar6 = strlen(pcVar16);
          lVar12 = (local_130 + 1) - (ulong)(local_130 == 0);
          uVar13 = sVar6 + lVar12;
          if (local_f0 <= uVar13) {
            local_f0 = uVar13 + 1;
            local_128 = x2realloc(local_128,&local_f0);
          }
          *(undefined *)(local_128 + local_130) = 0x2c;
          memcpy((void *)(local_128 + lVar12),pcVar16,sVar6 + 1);
          diagnose_surprises = '\x01';
          local_130 = uVar13;
        }
      }
      else {
        uVar13 = 1 << ((char)iVar3 + 0x9aU & 0x3f);
        if ((uVar13 & 0x6f202) != 0) goto LAB_00102a60;
        if (iVar3 == 0x76) {
          verbosity = 0;
        }
        else {
          if ((uVar13 & 1) == 0) goto LAB_00102967;
          force_silent = 1;
        }
      }
    }
    else if (iVar3 == 0x81) {
      bVar1 = true;
    }
    else if (iVar3 == 0x82) {
      local_120 = optarg;
    }
    else {
      if (iVar3 != 0x80) goto LAB_00102967;
      bVar1 = false;
    }
  }
  if (local_120 == 0) {
    if (local_128 == 0) {
      local_128 = param_2[optind];
      optind = optind + 1;
      if (optind < param_1) goto LAB_00103286;
      if (local_128 == 0) goto LAB_0010308c;
    }
    else if (optind < param_1) {
LAB_00103286:
      change = mode_compile();
      if (change == 0) {
LAB_00103532:
        uVar7 = quote(local_128);
        uVar9 = dcgettext(0,"invalid mode: %s",5);
        error(0,0,uVar9,uVar7);
                    /* WARNING: Subroutine does not return */
        usage(1);
      }
      umask_value = umask(0);
      goto LAB_00102b7c;
    }
    if (param_2[(long)optind + -1] == local_128) goto LAB_00103446;
LAB_0010308c:
    pcVar16 = "missing operand";
  }
  else {
    if (local_128 == 0) {
      if (param_1 <= optind) goto LAB_0010308c;
      change = mode_create_from_ref(local_120);
      if (change == 0) {
        uVar7 = quotearg_style(4,local_120);
        uVar9 = dcgettext(0,"failed to get attributes of %s",5);
        piVar10 = __errno_location();
        error(1,*piVar10,uVar9,uVar7);
        goto LAB_00103532;
      }
LAB_00102b7c:
      if ((recurse == '\0') || (!bVar1)) {
        root_dev_ino = (long *)0x0;
LAB_00102b99:
        uVar18 = 1;
        lVar12 = xfts_open(param_2 + optind,0x411,0);
        do {
          lVar8 = rpl_fts_read();
          if (lVar8 == 0) {
            piVar10 = __errno_location();
            if (*piVar10 != 0) {
              uVar18 = (uint)force_silent;
              if (force_silent == 0) {
                uVar7 = dcgettext(0,"fts_read failed",5);
                error(0,*piVar10,uVar7);
              }
              else {
                uVar18 = 0;
              }
            }
            iVar3 = rpl_fts_close();
            if (iVar3 != 0) {
              uVar18 = 0;
              uVar7 = dcgettext(0,"fts_close failed",5);
              error(0,*piVar10,uVar7);
            }
                    /* WARNING: Subroutine does not return */
            exit(uVar18 ^ 1);
          }
          pcVar16 = *(char **)(lVar8 + 0x38);
          pcVar17 = *(char **)(lVar8 + 0x30);
          switch(*(undefined2 *)(lVar8 + 0x68)) {
          case 2:
            cVar2 = cycle_warning_required(lVar12,lVar8);
            if (cVar2 == '\0') goto switchD_00102bf3_caseD_3;
            uVar7 = quotearg_n_style_colon(0,3,pcVar16);
            uVar9 = dcgettext(0,
                              "WARNING: Circular directory structure.\nThis almost certainly means that you have a corrupted file system.\nNOTIFY YOUR SYSTEM MANAGER.\nThe following directory is part of the cycle:\n  %s\n"
                              ,5);
            error(0,0,uVar9,uVar7);
            uVar14 = 0;
            break;
          default:
switchD_00102bf3_caseD_3:
            if (((root_dev_ino != (long *)0x0) && (*(long *)(lVar8 + 0x78) == *root_dev_ino)) &&
               (*(long *)(lVar8 + 0x70) == root_dev_ino[1])) {
              iVar3 = strcmp(pcVar16,"/");
              if (iVar3 == 0) {
                uVar7 = quotearg_style(4,pcVar16);
                uVar9 = dcgettext(0,"it is dangerous to operate recursively on %s",5);
                error(0,0,uVar9,uVar7);
              }
              else {
                uVar7 = quotearg_n_style(1,4,"/");
                uVar9 = quotearg_n_style(0,4,pcVar16);
                uVar11 = dcgettext(0,"it is dangerous to operate recursively on %s (same as %s)",5);
                error(0,0,uVar11,uVar9,uVar7);
              }
              uVar7 = dcgettext(0,"use --no-preserve-root to override this failsafe",5);
              error(0,0,uVar7);
              rpl_fts_set(lVar12,lVar8,4);
              rpl_fts_read(lVar12);
              uVar14 = 0;
              break;
            }
            uVar5 = *(uint *)(lVar8 + 0x88);
            if ((uVar5 & 0xf000) == 0xa000) {
              __mode = 0;
              uVar5 = 0;
              uVar14 = 2;
LAB_00102f02:
              if ((verbosity != 2) && (verbosity == 0)) {
                local_120 = quotearg_style(4,pcVar16);
                if (uVar14 != 2) {
                  uVar14 = 1;
                  strmode(__mode,&local_58);
                  local_4e = 0;
                  strmode(uVar5,&local_4c);
                  local_42 = 0;
                  uVar7 = dcgettext(0,"failed to change mode of %s from %04lo (%s) to %04lo (%s)\n",
                                    5);
                  goto LAB_00102d1c;
                }
                uVar7 = dcgettext(0,"neither symbolic link %s nor referent has been changed\n",5);
                __printf_chk(1,uVar7,local_120);
              }
            }
            else {
              __mode = mode_adjust(uVar5,(uVar5 & 0xf000) == 0x4000,umask_value,change,0);
              iVar3 = fchmodat(*(int *)(lVar12 + 0x2c),pcVar17,__mode,0);
              if (iVar3 != 0) {
                if (force_silent == 0) {
                  uVar7 = quotearg_style(4,pcVar16);
                  uVar9 = dcgettext(0,"changing permissions of %s",5);
                  piVar10 = __errno_location();
                  error(0,*piVar10,uVar9,uVar7);
                }
                uVar14 = 1;
                goto LAB_00102f02;
              }
              if (verbosity == 2) {
                uVar14 = 4;
              }
              else {
                _Var4 = __mode;
                if (((__mode & 0xe00) != 0) &&
                   (iVar3 = fstatat(*(int *)(lVar12 + 0x2c),pcVar17,&local_e8,0),
                   _Var4 = local_e8.st_mode, iVar3 != 0)) {
                  if (force_silent == 0) {
                    uVar7 = quotearg_style(4,pcVar16);
                    uVar9 = dcgettext(0,"getting new attributes of %s",5);
                    piVar10 = __errno_location();
                    error(0,*piVar10,uVar9,uVar7);
                  }
LAB_00103199:
                  if (verbosity == 0) {
                    uVar7 = quotearg_style(4,pcVar16);
                    strmode(__mode,&local_58);
                    local_4e = 0;
                    strmode(uVar5,&local_4c);
                    local_42 = 0;
                    uVar9 = dcgettext(0,"mode of %s retained as %04lo (%s)\n",5);
                    __printf_chk(1,uVar9,uVar7,__mode & 0xfff);
                  }
                  uVar14 = 3;
                  goto LAB_00102d49;
                }
                if (((_Var4 ^ uVar5) & 0xfff) == 0) goto LAB_00103199;
                uVar14 = 4;
                local_120 = quotearg_style(4,pcVar16);
                strmode(__mode,&local_58);
                local_4e = 0;
                strmode(uVar5,&local_4c);
                local_42 = 0;
                uVar7 = dcgettext(0,"mode of %s changed from %04lo (%s) to %04lo (%s)\n",5);
LAB_00102d1c:
                __printf_chk(1,uVar7,local_120,uVar5 & 0xfff,local_4b,__mode & 0xfff,local_57,uVar7)
                ;
                if (uVar14 < 3) goto LAB_00102d56;
              }
LAB_00102d49:
              if ((diagnose_surprises != '\0') &&
                 (uVar5 = mode_adjust(uVar5,(uVar5 & 0xf000) == 0x4000,0,change,0),
                 (__mode & ~uVar5) != 0)) {
                uVar14 = 1;
                strmode(__mode,&local_58);
                strmode(uVar5,&local_4c);
                local_42 = 0;
                local_4e = 0;
                uVar7 = quotearg_n_style_colon(0,3,pcVar16);
                uVar9 = dcgettext(0,"%s: new permissions are %s, not %s",5);
                error(0,0,uVar9,uVar7);
              }
            }
            goto LAB_00102d56;
          case 4:
            if (force_silent == 0) {
              uVar7 = quotearg_style(4,pcVar16);
              pcVar17 = "cannot read directory %s";
LAB_00102e25:
              uVar9 = dcgettext(0,pcVar17,5);
              error(0,*(undefined4 *)(lVar8 + 0x40),uVar9,uVar7);
            }
            goto LAB_00102d7e;
          case 6:
            uVar14 = 1;
            break;
          case 7:
            if (force_silent == 0) {
              uVar7 = quotearg_n_style_colon(0,3,pcVar16);
              error(0,*(undefined4 *)(lVar8 + 0x40),"%s",uVar7);
            }
            goto LAB_00102d7e;
          case 10:
            if ((*(long *)(lVar8 + 0x58) != 0) || (*(long *)(lVar8 + 0x20) != 0)) {
              if (force_silent == 0) {
                uVar7 = quotearg_style(4,pcVar16);
                pcVar17 = "cannot access %s";
                goto LAB_00102e25;
              }
              goto LAB_00102d7e;
            }
            *(undefined8 *)(lVar8 + 0x20) = 1;
            rpl_fts_set(lVar12,lVar8,1);
            uVar14 = 1;
            break;
          case 0xd:
            if (force_silent == 0) {
              uVar7 = quotearg_style(4,pcVar16);
              uVar9 = dcgettext(0,"cannot operate on dangling symlink %s",5);
              error(0,0,uVar9,uVar7);
            }
LAB_00102d7e:
            uVar14 = verbosity;
            if ((verbosity == 2) || (verbosity != 0)) {
              uVar14 = 0;
            }
            else {
              uVar7 = quotearg_style(4,pcVar16);
              uVar9 = dcgettext(0,"%s could not be accessed\n",5);
              __printf_chk(1,uVar9,uVar7);
            }
LAB_00102d56:
            if (recurse == '\0') {
              rpl_fts_set(lVar12,lVar8,4);
            }
            uVar14 = (uint)(1 < uVar14);
          }
          uVar18 = uVar18 & uVar14;
        } while( true );
      }
      root_dev_ino = (long *)get_root_dev_ino(dev_ino_buf_0);
      if (root_dev_ino != (long *)0x0) goto LAB_00102b99;
      uVar7 = quotearg_style(4,"/");
      uVar9 = dcgettext(0,"failed to get attributes of %s",5);
      piVar10 = __errno_location();
      error(1,*piVar10,uVar9,uVar7);
LAB_00103446:
      uVar7 = quote(param_2[(long)param_1 + -1]);
      uVar9 = dcgettext(0,"missing operand after %s",5);
      error(0,0,uVar9,uVar7);
      goto LAB_00102967;
    }
    pcVar16 = "cannot combine mode and --reference options";
  }
  uVar7 = dcgettext(0,pcVar16,5);
  error(0,0,uVar7);
LAB_00102967:
                    /* WARNING: Subroutine does not return */
  usage(1);
}