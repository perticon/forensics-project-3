void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002770(fn00000000000024C0(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000036BE;
	}
	fn00000000000026D0(fn00000000000024C0(0x05, "Usage: %s [OPTION]... MODE[,MODE]... FILE...\n  or:  %s [OPTION]... OCTAL-MODE FILE...\n  or:  %s [OPTION]... --reference=RFILE FILE...\n", null), 0x01);
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "Change the mode of each FILE to MODE.\nWith --reference, change the mode of each FILE to that of RFILE.\n\n", null));
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "  -c, --changes          like verbose but report only when a change is made\n  -f, --silent, --quiet  suppress most error messages\n  -v, --verbose          output a diagnostic for every file processed\n", null));
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "      --no-preserve-root  do not treat '/' specially (the default)\n      --preserve-root    fail to operate recursively on '/'\n", null));
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "      --reference=RFILE  use RFILE's mode instead of MODE values\n", null));
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "  -R, --recursive        change files and directories recursively\n", null));
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "      --help        display this help and exit\n", null));
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "      --version     output version information and exit\n", null));
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "\nEach MODE is of the form '[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+'.\n", null));
	struct Eq_1712 * rbx_206 = fp - 0xB8 + 16;
	do
	{
		char * rsi_208 = rbx_206->qw0000;
		++rbx_206;
	} while (rsi_208 != null && fn00000000000025D0(rsi_208, "chmod") != 0x00);
	ptr64 r13_221 = rbx_206->qw0008;
	if (r13_221 != 0x00)
	{
		fn00000000000026D0(fn00000000000024C0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_308 = fn00000000000026C0(null, 0x05);
		if (rax_308 == 0x00 || fn0000000000002430(0x03, "en_", rax_308) == 0x00)
			goto l0000000000003936;
	}
	else
	{
		fn00000000000026D0(fn00000000000024C0(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_250 = fn00000000000026C0(null, 0x05);
		if (rax_250 == 0x00 || fn0000000000002430(0x03, "en_", rax_250) == 0x00)
		{
			fn00000000000026D0(fn00000000000024C0(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000003973:
			fn00000000000026D0(fn00000000000024C0(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000036BE:
			fn0000000000002750(edi);
		}
		r13_221 = 0xC004;
	}
	fn00000000000025B0(stdout, fn00000000000024C0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003936:
	fn00000000000026D0(fn00000000000024C0(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000003973;
}