print_esc_string (char const *str)
{
  for (; *str; str++)
    if (*str == '\\')
      str += print_esc (str, true);
    else
      putchar (*str);
}