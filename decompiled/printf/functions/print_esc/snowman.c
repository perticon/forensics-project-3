int32_t print_esc(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbp5;
    struct s0* rbx6;
    struct s0* rbx7;
    void** rax8;
    int32_t edi9;
    int64_t rcx10;
    void* r8_11;
    int64_t rdx12;
    int64_t rsi13;
    int64_t rax14;
    int32_t esi15;
    int32_t ecx16;
    int32_t eax17;
    int32_t edx18;
    void** rdi19;
    int32_t r12d20;
    void** rax21;
    void** rsi22;
    void** rdi23;
    int32_t eax24;
    void** rsi25;
    int64_t rax26;
    uint64_t rax27;
    unsigned char* rdx28;
    unsigned char* r12_29;
    uint32_t eax30;
    int64_t rax31;
    uint32_t eax32;
    void** rax33;
    int64_t r13_34;
    int1_t zf35;
    void** rax36;
    int64_t r13_37;
    int64_t r9_38;
    void* rsi39;
    void* rdi40;
    struct s0* rsi41;
    int64_t rdx42;
    int64_t rcx43;
    int32_t ecx44;
    uint32_t r9d45;
    int32_t eax46;
    int32_t edx47;
    uint32_t eax48;
    void** rdi49;
    void** rdx50;
    void** rsi51;
    void** rdi52;
    int64_t rsi53;
    void** rdi54;
    void** rax55;
    void** rsi56;
    int32_t eax57;
    int64_t rax58;
    void* r12_59;

    rbp5 = rdi;
    *reinterpret_cast<uint32_t*>(&rbx6) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    if (*reinterpret_cast<void***>(&rbx6) == 0x78) {
        rbx7 = reinterpret_cast<struct s0*>(rdi + 2);
        rax8 = fun_26e0(rdi, rsi, rdx, rcx);
        edi9 = 0;
        *reinterpret_cast<int32_t*>(&rcx10) = 0;
        r8_11 = *rax8;
        while (*reinterpret_cast<int32_t*>(&rdx12) = reinterpret_cast<signed char>(rbx7->f0), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0, *reinterpret_cast<uint32_t*>(&rsi13) = *reinterpret_cast<unsigned char*>(&rdx12), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0, *reinterpret_cast<int32_t*>(&rax14) = *reinterpret_cast<int32_t*>(&rdx12), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_11) + rsi13 * 2 + 1) & 16)) {
            esi15 = static_cast<int32_t>(rax14 - 97);
            ecx16 = *reinterpret_cast<int32_t*>(&rcx10) << 4;
            if (*reinterpret_cast<unsigned char*>(&esi15) > 5) {
                eax17 = *reinterpret_cast<int32_t*>(&rax14) - 65;
                edx18 = *reinterpret_cast<int32_t*>(&rdx12) - 48;
                if (*reinterpret_cast<unsigned char*>(&eax17) <= 5) {
                    edx18 = static_cast<int32_t>(rdx12 - 55);
                }
                rbx7 = reinterpret_cast<struct s0*>(&rbx7->pad2);
                *reinterpret_cast<int32_t*>(&rcx10) = ecx16 + edx18;
                if (edi9 == 1) 
                    goto addr_370e_8;
            } else {
                rbx7 = reinterpret_cast<struct s0*>(&rbx7->pad2);
                *reinterpret_cast<int32_t*>(&rcx10) = ecx16 + (*reinterpret_cast<int32_t*>(&rdx12) - 87);
                if (edi9 == 1) 
                    goto addr_370e_8;
            }
            edi9 = 1;
        }
        if (edi9) {
            addr_370e_8:
            rdi19 = stdout;
            r12d20 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx7) - reinterpret_cast<unsigned char>(rbp5) + 0xffffffffffffffff);
            rax21 = *reinterpret_cast<void***>(rdi19 + 40);
            if (reinterpret_cast<unsigned char>(rax21) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi19 + 48))) {
                addr_3650_12:
                *reinterpret_cast<void***>(rdi19 + 40) = rax21 + 1;
                *reinterpret_cast<void***>(rax21) = *reinterpret_cast<void***>(&rcx10);
            } else {
                addr_372a_13:
                *reinterpret_cast<uint32_t*>(&rsi22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx10));
                *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                fun_2500(rdi19, rsi22);
            }
        } else {
            addr_3a58_14:
            fun_2490();
            *reinterpret_cast<int32_t*>(&rdi23) = 1;
            *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
            fun_2650();
            goto addr_3a80_15;
        }
    } else {
        eax24 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx6 + 0xfffffffffffffff0));
        if (*reinterpret_cast<unsigned char*>(&eax24) > 7) {
            if (!*reinterpret_cast<void***>(&rbx6)) 
                goto addr_3678_18;
            *reinterpret_cast<int32_t*>(&rsi25) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rbx6));
            *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
            rax26 = fun_24e0("\"\\abcefnrtv", rsi25);
            if (!rax26) 
                goto addr_37a0_20; else 
                goto addr_3778_21;
        } else {
            *reinterpret_cast<int32_t*>(&rax27) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rax27) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(&rbx6) == 48);
            *reinterpret_cast<int32_t*>(&rcx10) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            rdx28 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + (rax27 & reinterpret_cast<unsigned char>(rsi)) + 1);
            r12_29 = rdx28 + 3;
            do {
                eax30 = *rdx28 - 48;
                if (*reinterpret_cast<unsigned char*>(&eax30) > 7) 
                    goto addr_3a48_24;
                *reinterpret_cast<int32_t*>(&rax31) = *reinterpret_cast<signed char*>(&eax30);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax31) + 4) = 0;
                ++rdx28;
                *reinterpret_cast<int32_t*>(&rcx10) = static_cast<int32_t>(rax31 + rcx10 * 8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            } while (rdx28 != r12_29);
            goto addr_3634_26;
        }
    }
    addr_365a_27:
    return r12d20;
    addr_37a0_20:
    eax32 = *reinterpret_cast<uint32_t*>(&rbx6) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&eax32) != 85) {
        addr_3678_18:
        rdi23 = stdout;
        rax33 = *reinterpret_cast<void***>(rdi23 + 40);
        if (reinterpret_cast<unsigned char>(rax33) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi23 + 48))) {
            addr_3a80_15:
            fun_2500(rdi23, 92, rdi23, 92);
        } else {
            *reinterpret_cast<void***>(rdi23 + 40) = rax33 + 1;
            *reinterpret_cast<void***>(rax33) = reinterpret_cast<void**>(92);
        }
    } else {
        *reinterpret_cast<int32_t*>(&r13_34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        zf35 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rbx6) == 0x75);
        rbx6 = reinterpret_cast<struct s0*>(rbp5 + 2);
        *reinterpret_cast<unsigned char*>(&r13_34) = reinterpret_cast<uint1_t>(!zf35);
        rax36 = fun_26e0("\"\\abcefnrtv", rsi25, rdx, rcx);
        *reinterpret_cast<int32_t*>(&r13_37) = static_cast<int32_t>(r13_34 * 4 + 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_37) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r9_38) = 0;
        *reinterpret_cast<int32_t*>(&rsi39) = static_cast<int32_t>(r13_37 + 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi39) + 4) = 0;
        rdi40 = *rax36;
        rsi41 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsi39) + reinterpret_cast<unsigned char>(rbp5));
        while (*reinterpret_cast<int32_t*>(&rdx42) = reinterpret_cast<signed char>(rbx6->f0), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx42) + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx43) = *reinterpret_cast<unsigned char*>(&rdx42), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx43) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi40) + rcx43 * 2 + 1) & 16)) {
            ecx44 = static_cast<int32_t>(rdx42 - 97);
            r9d45 = *reinterpret_cast<uint32_t*>(&r9_38) << 4;
            if (*reinterpret_cast<unsigned char*>(&ecx44) <= 5) {
                rbx6 = reinterpret_cast<struct s0*>(&rbx6->pad2);
                *reinterpret_cast<uint32_t*>(&r9_38) = r9d45 + (*reinterpret_cast<int32_t*>(&rdx42) - 87);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_38) + 4) = 0;
                if (rbx6 == rsi41) 
                    goto addr_3828_33;
            } else {
                eax46 = *reinterpret_cast<int32_t*>(&rdx42) - 65;
                edx47 = *reinterpret_cast<int32_t*>(&rdx42) - 48;
                if (*reinterpret_cast<unsigned char*>(&eax46) <= 5) {
                    edx47 = static_cast<int32_t>(rdx42 - 55);
                }
                rbx6 = reinterpret_cast<struct s0*>(&rbx6->pad2);
                *reinterpret_cast<uint32_t*>(&r9_38) = r9d45 + edx47;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_38) + 4) = 0;
                if (rbx6 == rsi41) 
                    goto addr_3828_33;
            }
        }
        goto addr_3a58_14;
        addr_3828_33:
        if (*reinterpret_cast<uint32_t*>(&r9_38) > 0x9f) 
            goto addr_3870_37;
        if (*reinterpret_cast<uint32_t*>(&r9_38) == 36) 
            goto addr_3870_37; else 
            goto addr_3837_39;
    }
    eax48 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 1));
    r12d20 = 0;
    if (*reinterpret_cast<void***>(&eax48)) {
        rdi49 = stdout;
        rdx50 = *reinterpret_cast<void***>(rdi49 + 40);
        if (reinterpret_cast<unsigned char>(rdx50) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi49 + 48))) {
            *reinterpret_cast<uint32_t*>(&rsi51) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax48));
            *reinterpret_cast<int32_t*>(&rsi51 + 4) = 0;
            fun_2500(rdi49, rsi51);
        } else {
            *reinterpret_cast<void***>(rdi49 + 40) = rdx50 + 1;
            *reinterpret_cast<void***>(rdx50) = *reinterpret_cast<void***>(&eax48);
        }
        r12d20 = 1;
        goto addr_365a_27;
    }
    addr_3870_37:
    if (static_cast<uint32_t>(r9_38 - 0xd800) > 0x7ff) {
        addr_3842_45:
        rdi52 = stdout;
        *reinterpret_cast<uint32_t*>(&rsi53) = *reinterpret_cast<uint32_t*>(&r9_38);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi53) + 4) = 0;
        print_unicode_char(rdi52, rsi53);
        return static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx6) - reinterpret_cast<unsigned char>(rbp5) + 0xffffffffffffffff);
    } else {
        addr_387e_46:
        fun_2490();
        fun_2650();
    }
    rdi54 = stdout;
    r12d20 = 1;
    rax55 = *reinterpret_cast<void***>(rdi54 + 40);
    if (reinterpret_cast<unsigned char>(rax55) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi54 + 48))) {
        *reinterpret_cast<uint32_t*>(&rsi56) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx6));
        *reinterpret_cast<int32_t*>(&rsi56 + 4) = 0;
        fun_2500(rdi54, rsi56, rdi54, rsi56);
        goto addr_365a_27;
    } else {
        *reinterpret_cast<void***>(rdi54 + 40) = rax55 + 1;
        *reinterpret_cast<void***>(rax55) = *reinterpret_cast<void***>(&rbx6);
        goto addr_365a_27;
    }
    addr_3837_39:
    if ((*reinterpret_cast<uint32_t*>(&r9_38) & 0xffffffdf) != 64) 
        goto addr_387e_46; else 
        goto addr_3842_45;
    addr_3778_21:
    eax57 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx6) + 0xffffffffffffff9f);
    if (*reinterpret_cast<unsigned char*>(&eax57) <= 21) {
        *reinterpret_cast<uint32_t*>(&rax58) = *reinterpret_cast<unsigned char*>(&eax57);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax58) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xa930 + rax58 * 4) + 0xa930;
    }
    addr_3a48_24:
    r12_29 = rdx28;
    addr_3634_26:
    rdi19 = stdout;
    r12_59 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_29) - reinterpret_cast<unsigned char>(rbp5));
    r12d20 = *reinterpret_cast<int32_t*>(&r12_59) - 1;
    rax21 = *reinterpret_cast<void***>(rdi19 + 40);
    if (reinterpret_cast<unsigned char>(rax21) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi19 + 48))) 
        goto addr_372a_13; else 
        goto addr_3650_12;
}