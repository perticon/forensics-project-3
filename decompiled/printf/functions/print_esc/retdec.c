int32_t print_esc(char * escstart, bool octal_0) {
    int64_t v1 = (int64_t)escstart;
    int64_t v2 = v1 + 1; // 0x35ed
    char * v3 = (char *)v2; // 0x35ed
    unsigned char v4 = *v3; // 0x35ed
    int64_t * v5; // 0x35e0
    int64_t result3; // 0x35e0
    int64_t v6; // 0x35e0
    int64_t v7; // 0x35e0
    int64_t v8; // 0x35e0
    if (v4 == 120) {
        int64_t v9 = v1 + 2; // 0x36d0
        int64_t v10 = *(int64_t *)function_26e0(); // 0x36dd
        unsigned char v11 = *(char *)v9; // 0x36e0
        int64_t v12 = v10 + 1; // 0x36e8
        if ((*(char *)(2 * (int64_t)v11 + v12) & 16) == 0) {
            // 0x3a58
            function_2490();
            function_2650();
            // 0x3a80
            function_2500();
            goto lab_0x3698;
        } else {
            int32_t v13 = 0;
            int64_t v14 = v11;
            int64_t v15 = 0; // 0x36f7
            int64_t v16; // 0x35e0
            int64_t v17; // 0x35e0
            if (v11 < 103) {
                // 0x3700
                v17 = v15 + v14 + 0xffffffa9 & 0xffffffff;
                v16 = v9 + 1;
                if (v13 == 1) {
                    // break -> 0x370e
                    break;
                }
            } else {
                // 0x3740
                v17 = v15 + v14 + (v11 < 71 ? 0xffffffc9 : 0xffffffd0) & 0xffffffff;
                v16 = v9 + 1;
                if (v13 == 1) {
                    // break -> 0x370e
                    break;
                }
            }
            unsigned char v18 = *(char *)v16; // 0x36e0
            int64_t v19 = v17; // 0x36ee
            int64_t v20 = v16; // 0x36ee
            while ((*(char *)(2 * (int64_t)v18 + v12) & 16) != 0) {
                int64_t v21 = v16;
                v13 = 1;
                char v22 = v18;
                v14 = v22;
                v15 = 16 * v17;
                if (v22 < 103) {
                    int64_t v23 = v21 + 1; // 0x3703
                    int64_t v24 = v15 + v14 + 0xffffffa9 & 0xffffffff; // 0x3707
                    v19 = v24;
                    v20 = v23;
                    v17 = v24;
                    v16 = v23;
                    if (v13 == 1) {
                        // break -> 0x370e
                        break;
                    }
                } else {
                    int64_t v25 = v21 + 1; // 0x374e
                    int64_t v26 = v15 + v14 + (v22 < 71 ? 0xffffffc9 : 0xffffffd0) & 0xffffffff; // 0x3752
                    v19 = v26;
                    v20 = v25;
                    v17 = v26;
                    v16 = v25;
                    if (v13 == 1) {
                        // break -> 0x370e
                        break;
                    }
                }
                // 0x3759
                v18 = *(char *)v16;
                v19 = v17;
                v20 = v16;
            }
            int64_t v27 = (int64_t)g31; // 0x370e
            int64_t result = v20 + (v1 ^ 0xffffffff) & 0xffffffff; // 0x3718
            int64_t * v28 = (int64_t *)(v27 + 40);
            uint64_t v29 = *v28; // 0x371c
            v5 = v28;
            v6 = v29;
            v8 = v19;
            result3 = result;
            if (v29 >= *(int64_t *)(v27 + 48)) {
                // 0x372a
                function_2500();
                // 0x365a
                return result;
            }
            goto lab_0x3650;
        }
    } else {
        if ((v4 & -8) == 48) {
            int64_t v30 = v2 + (int64_t)(v4 == 48 == octal_0); // 0x360e
            int64_t v31 = v30 + 3; // 0x3613
            int64_t v32 = 0; // 0x3613
            int64_t v33 = v30; // 0x3613
            int64_t v34 = (int64_t)*(char *)v33 + 0xffffffd0; // 0x361a
            int64_t v35 = v33; // 0x361f
            while ((char)v34 < 8) {
                // 0x3625
                v33++;
                v32 = (0x100000000000000 * v34 >> 56) + 8 * v32 & 0xffffffff;
                v35 = v31;
                if (v33 == v31) {
                    // break -> 0x3634
                    break;
                }
                v34 = (int64_t)*(char *)v33 + 0xffffffd0;
                v35 = v33;
            }
            int64_t v36 = (int64_t)g31; // 0x3634
            int64_t result2 = 0xffffffff - v1 + v35 & 0xffffffff; // 0x363e
            int64_t * v37 = (int64_t *)(v36 + 40);
            uint64_t v38 = *v37; // 0x3642
            v5 = v37;
            v6 = v38;
            v8 = v32;
            result3 = result2;
            if (v38 >= *(int64_t *)(v36 + 48)) {
                // 0x372a
                function_2500();
                // 0x365a
                return result2;
            }
            goto lab_0x3650;
        } else {
            if (v4 != 0) {
                int64_t v39 = v4; // 0x35ed
                if (function_24e0() == 0) {
                    if ((v39 & 223) != 85) {
                        goto lab_0x3678;
                    } else {
                        int64_t v40 = v1 + 2; // 0x37b3
                        int64_t v41 = *(int64_t *)function_26e0(); // 0x37cf
                        int64_t v42 = v1 + 6 + 4 * (int64_t)(v4 != 117); // 0x37d2
                        unsigned char v43 = *(char *)v40; // 0x37ef
                        int64_t v44 = v41 + 1; // 0x37f7
                        char v45 = v43; // 0x37fc
                        int64_t v46 = 0; // 0x37fc
                        int64_t v47 = v40; // 0x37fc
                        if ((*(char *)(2 * (int64_t)v43 + v44) & 16) == 0) {
                            // 0x3a58
                            function_2490();
                            function_2650();
                            // 0x3a80
                            function_2500();
                            goto lab_0x3698;
                        } else {
                            int64_t v48; // 0x35e0
                            int64_t v49; // 0x35e0
                            while (true) {
                                int64_t v50 = v47;
                                char v51 = v45;
                                int64_t v52 = v51;
                                int64_t v53 = 16 * v46; // 0x3805
                                int64_t v54; // 0x35e0
                                int64_t v55; // 0x35e0
                                if (v51 < 103) {
                                    int64_t v56 = v50 + 1; // 0x37e3
                                    int64_t v57 = v53 + v52 + 0xffffffa9 & 0xffffffff; // 0x37e7
                                    v55 = v56;
                                    v54 = v57;
                                    v49 = v42;
                                    v48 = v57;
                                    if (v56 == v42) {
                                        // break -> 0x3828
                                        break;
                                    }
                                } else {
                                    int64_t v58 = v50 + 1; // 0x381c
                                    int64_t v59 = v53 + v52 + (v51 < 71 ? 0xffffffc9 : 0xffffffd0) & 0xffffffff; // 0x3820
                                    v55 = v58;
                                    v54 = v59;
                                    v49 = v58;
                                    v48 = v59;
                                    if (v58 == v42) {
                                        // break -> 0x3828
                                        break;
                                    }
                                }
                                unsigned char v60 = *(char *)v55; // 0x37ef
                                v45 = v60;
                                v46 = v54;
                                v47 = v55;
                                if ((*(char *)(2 * (int64_t)v60 + v44) & 16) == 0) {
                                    // 0x3a58
                                    function_2490();
                                    function_2650();
                                    // 0x3a80
                                    function_2500();
                                    goto lab_0x3698;
                                }
                            }
                            uint32_t v61 = (int32_t)v48; // 0x3828
                            if (v61 > 159 || v61 == 36) {
                                if (v61 - 0xd800 > (int32_t)&g3) {
                                    // 0x3842
                                    print_unicode_char(g31, v61, 0);
                                    return v49 + (v1 ^ 0xffffffff);
                                }
                            } else {
                                if ((v61 - 64 & -33) == 0) {
                                    // 0x3842
                                    print_unicode_char(g31, v61, 0);
                                    return v49 + (v1 ^ 0xffffffff);
                                }
                            }
                            // 0x387e
                            function_2490();
                            function_2650();
                            v7 = v49;
                            goto lab_0x38b8;
                        }
                    }
                } else {
                    int64_t v62 = v39 + 0xffffff9f; // 0x3778
                    v7 = v39;
                    if ((char)v62 < 22) {
                        int32_t v63 = *(int32_t *)((4 * v62 & 1020) + (int64_t)&g7); // 0x378d
                        return v63 + (int32_t)&g7;
                    }
                    goto lab_0x38b8;
                }
            } else {
                goto lab_0x3678;
            }
        }
    }
  lab_0x3650:
    // 0x3650
    *v5 = v6 + 1;
    *(char *)v6 = (char)v8;
    // 0x365a
    return result3;
  lab_0x3678:;
    int64_t v64 = (int64_t)g31; // 0x3678
    int64_t * v65 = (int64_t *)(v64 + 40); // 0x367f
    uint64_t v66 = *v65; // 0x367f
    if (v66 >= *(int64_t *)(v64 + 48)) {
        // 0x3a80
        function_2500();
        goto lab_0x3698;
    } else {
        // 0x368d
        *v65 = v66 + 1;
        *(char *)v66 = 92;
        goto lab_0x3698;
    }
  lab_0x3698:;
    char v67 = *v3; // 0x3698
    if (v67 == 0) {
        // 0x365a
        return 0;
    }
    int64_t v68 = (int64_t)g31; // 0x36a3
    int64_t * v69 = (int64_t *)(v68 + 40); // 0x36aa
    uint64_t v70 = *v69; // 0x36aa
    if (v70 >= *(int64_t *)(v68 + 48)) {
        // 0x3a8f
        function_2500();
    } else {
        // 0x36b8
        *v69 = v70 + 1;
        *(char *)v70 = v67;
    }
    // 0x365a
    return 1;
  lab_0x38b8:;
    int64_t v71 = (int64_t)g31; // 0x38b8
    int64_t * v72 = (int64_t *)(v71 + 40); // 0x38c5
    uint64_t v73 = *v72; // 0x38c5
    if (v73 >= *(int64_t *)(v71 + 48)) {
        // 0x3aab
        function_2500();
    } else {
        // 0x38d3
        *v72 = v73 + 1;
        *(char *)v73 = (char)v7;
    }
    // 0x365a
    return 1;
}