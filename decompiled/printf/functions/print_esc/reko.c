word32 print_esc(uint64 rsi, Eq_225 rdi, union Eq_586 & rbxOut, union Eq_225 & rbpOut, union Eq_588 & r12Out)
{
	Eq_588 r12;
	Eq_225 rbp;
	Eq_586 rbx;
	word32 r12_32_32_370 = SLICE(r12, word32, 32);
	uint64 rcx_775;
	uint64 rcx_772;
	word32 r12d_502;
	FILE * rdi_456;
	byte * rax_104;
	FILE * rdi_174;
	Eq_586 rbx_20 = (uint64) *((word64) rdi + 1);
	byte bl_21 = (byte) rbx_20;
	word32 ebx_123 = (word32) rbx_20;
	if (bl_21 == 0x78)
	{
		byte * rbx_24 = (word64) rdi + 2;
		word32 edi_46 = 0x00;
		struct Eq_1265 * r8_33 = *fn00000000000026E0();
		uint64 rcx_778 = 0x00;
		while (true)
		{
			uint64 rcx_786;
			int32 edx_36 = (int32) *rbx_24;
			ui32 ecx_52 = (word32) rcx_778;
			uint64 rax_41 = (uint64) edx_36;
			byte cl_473 = (byte) ecx_52;
			word32 eax_58 = (word32) rax_41;
			word56 rcx_56_8_697 = SEQ(SLICE(rcx_778, word32, 32), SLICE(ecx_52, word24, 8));
			if ((r8_33->a0001[(uint64) (byte) edx_36].b0000 & 0x10) == 0x00)
				break;
			if ((byte) rax_41 <= 0x66 && (byte) rax_41 >= 0x61)
			{
				++rbx_24;
				rcx_786 = (uint64) ((ecx_52 << 0x04) + (edx_36 - 0x57));
				if (edi_46 == 0x01)
					goto l000000000000370E;
			}
			else
			{
				int32 edx_71 = edx_36 - 0x30;
				if ((byte) eax_58 <= 0x46 && (byte) eax_58 >= 0x41)
					edx_71 = edx_36 - 55;
				++rbx_24;
				rcx_786 = (uint64) ((ecx_52 << 0x04) + edx_71);
				if (edi_46 == 0x01)
					goto l000000000000370E;
			}
			edi_46 = 0x01;
			rcx_778 = rcx_786;
		}
		rcx_786 = SEQ(rcx_56_8_697, cl_473);
		if (edi_46 != 0x00)
		{
l000000000000370E:
			rdi_456 = stdout;
			r12d_502 = (word32) (rbx_24 - rdi) - 0x01;
			rax_104 = rdi_456->ptr0028;
			rcx_772 = rcx_786;
			rcx_775 = rcx_786;
			if (rax_104 < rdi_456->ptr0030)
				goto l0000000000003650;
			goto l000000000000372A;
		}
l0000000000003A58:
		fn0000000000002650(fn0000000000002490(0x05, "missing hexadecimal number in escape", null), 0x00, 0x01);
		rdi_174 = (FILE *) 0x01;
l0000000000003A80:
		fn0000000000002500(0x5C, rdi_174);
		goto l0000000000003698;
	}
	if ((byte) rbx_20 <= 55 && (byte) rbx_20 >= 0x30)
	{
		byte * rdx_432 = (word64) rdi + 1 + ((uint64) ((int8) (bl_21 == 0x30)) & rsi);
		uint64 rcx_425 = 0x00;
		byte * r12_433 = rdx_432 + 3;
		rdx_434 = rdx_432;
		do
		{
			byte * rdx_434;
			cu8 al_441 = *rdx_434 - 0x30;
			if (al_441 > 0x07)
			{
				r12_433 = rdx_434;
				break;
			}
			++rdx_434;
			rcx_425 = (uint64) ((int32) al_441 + (word32) rcx_425 * 0x08);
		} while (rdx_434 != rdx_432 + 3);
		rdi_456 = stdout;
		r12d_502 = (word32) (r12_433 - rdi) - 0x01;
		rax_104 = rdi_456->ptr0028;
		rcx_772 = rcx_425;
		rcx_775 = rcx_425;
		if (rax_104 < rdi_456->ptr0030)
		{
l0000000000003650:
			rdi_456->ptr0028 = rax_104 + 1;
			*rax_104 = (byte) rcx_775;
			goto l000000000000365A;
		}
l000000000000372A:
		fn0000000000002500((word32) (byte) rcx_772, rdi_456);
		goto l000000000000365A;
	}
	if (bl_21 == 0x00)
	{
l0000000000003678:
		rdi_174 = stdout;
		byte * rax_382 = rdi_174->ptr0028;
		if (rax_382 < rdi_174->ptr0030)
		{
			rdi_174->ptr0028 = rax_382 + 1;
			*rax_382 = 0x5C;
l0000000000003698:
			byte al_403 = *((word64) rdi + 1);
			r12d_502 = 0x00;
			if (al_403 != 0x00)
			{
				FILE * rdi_407 = stdout;
				byte * rdx_408 = rdi_407->ptr0028;
				if (rdx_408 < rdi_407->ptr0030)
				{
					rdi_407->ptr0028 = rdx_408 + 1;
					*rdx_408 = al_403;
				}
				else
					fn0000000000002500((word32) al_403, rdi_407);
				r12d_502 = 0x01;
			}
			goto l000000000000365A;
		}
		goto l0000000000003A80;
	}
	int32 r12d_115 = (int32) bl_21;
	Eq_588 r12_371 = SEQ(r12_32_32_370, r12d_115);
	if (fn00000000000024E0((byte) r12d_115, "\"\\abcefnrtv") != 0x00)
	{
		cu8 al_345 = (byte) rbx_20 - 0x61;
		if (al_345 <= 0x15)
		{
			<anonymous> * rax_368 = (int64) g_aA930[(uint64) al_345 * 0x04] + 0xA930;
			word64 rax_373;
			word64 rcx_374;
			word64 rsi_375;
			word64 r8_377;
			word64 r9_378;
			rax_368();
			rbxOut = rbx_20;
			rbpOut = rdi;
			r12Out = r12_371;
			return (word32) rax_373;
		}
		goto l00000000000038B8;
	}
	if (((byte) ebx_123 & 223) != 0x55)
		goto l0000000000003678;
	word64 rbx_135 = rdi + 2;
	struct Eq_1265 * rdi_147 = *fn00000000000026E0();
	struct Eq_1535 * rsi_148 = (word64) rdi + (uint64) ((word32) (bl_21 != 117) * 0x04 + 0x06);
	uint64 r9_197 = 0x00;
	do
	{
		int32 edx_152 = (int32) rbx_135->b0000;
		ui32 r9d_185 = (word32) r9_197;
		if ((rdi_147->a0001[(uint64) (byte) edx_152].b0000 & 0x10) == 0x00)
			goto l0000000000003A58;
		if ((byte) edx_152 > 0x66)
		{
			int32 edx_213 = edx_152 - 0x30;
			if ((byte) edx_152 <= 0x46 && (byte) edx_152 >= 0x41)
				edx_213 = edx_152 - 55;
			++rbx_135;
			r9_197 = (uint64) ((r9d_185 << 0x04) + edx_213);
			if (rbx_135 == rsi_148)
				break;
			continue;
		}
		++rbx_135;
		r9_197 = (uint64) ((r9d_185 << 0x04) + (edx_152 - 0x57));
	} while (rbx_135 != rsi_148);
	bl_21 = (byte) rbx_135;
	uint32 r9d_224 = (word32) r9_197;
	if (r9d_224 <= 0x9F && r9d_224 != 0x24)
	{
		if ((r9d_224 & ~0x20) == 0x40)
		{
l0000000000003842:
			print_unicode_char(0x00, (word32) r9_197);
			rbxOut = rbx;
			rbpOut = rbp;
			r12Out = r12;
			return (word32) (rbx_135 - rdi) - 0x01;
		}
	}
	else if ((word32) r9_197 > 0xDFFF || (word32) r9_197 < 0xD800)
		goto l0000000000003842;
	fn0000000000002650(fn0000000000002490(0x05, "invalid universal character name \\%c%0*x", null), 0x00, 0x01);
l00000000000038B8:
	FILE * rdi_349 = stdout;
	r12d_502 = 0x01;
	byte * rax_352 = rdi_349->ptr0028;
	if (rax_352 < rdi_349->ptr0030)
	{
		rdi_349->ptr0028 = rax_352 + 1;
		*rax_352 = bl_21;
	}
	else
		fn0000000000002500((word32) bl_21, rdi_349);
l000000000000365A:
	rbxOut = rbx;
	rbpOut = rbp;
	r12Out = r12;
	return r12d_502;
}