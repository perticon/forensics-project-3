int print_esc(long param_1,ulong param_2)

{
  byte bVar1;
  int iVar2;
  bool bVar3;
  byte *pbVar4;
  char *pcVar5;
  ushort **ppuVar6;
  undefined8 uVar7;
  byte bVar8;
  char cVar9;
  uint uVar10;
  int iVar11;
  char *pcVar12;
  byte *pbVar13;
  _IO_FILE *p_Var14;
  uint uVar15;
  int iVar16;
  char *pcVar17;
  
  bVar1 = *(byte *)(param_1 + 1);
  uVar10 = (uint)bVar1;
  iVar16 = (int)param_1;
  if (bVar1 == 0x78) {
    pbVar4 = (byte *)(param_1 + 2);
    ppuVar6 = __ctype_b_loc();
    bVar3 = false;
    bVar8 = 0;
    while (bVar1 = *pbVar4, (*(byte *)((long)*ppuVar6 + (ulong)bVar1 * 2 + 1) & 0x10) != 0) {
      if ((byte)(bVar1 + 0x9f) < 6) {
        cVar9 = bVar1 + 0xa9;
      }
      else {
        cVar9 = bVar1 - 0x30;
        if ((byte)(bVar1 + 0xbf) < 6) {
          cVar9 = bVar1 - 0x37;
        }
      }
      bVar8 = bVar8 * '\x10' + cVar9;
      pbVar13 = pbVar4 + 1;
      if (bVar3) goto LAB_0010370e;
      pbVar4 = pbVar4 + 1;
      bVar3 = true;
    }
    pbVar13 = pbVar4;
    if (bVar3) {
LAB_0010370e:
      iVar16 = ((int)pbVar13 - iVar16) + -1;
      pbVar4 = (byte *)stdout->_IO_write_ptr;
      if (stdout->_IO_write_end <= pbVar4) goto LAB_0010372a;
      goto LAB_00103650;
    }
LAB_00103a58:
    uVar7 = dcgettext(0,"missing hexadecimal number in escape",5);
    p_Var14 = (_IO_FILE *)0x1;
    error(1,0,uVar7);
  }
  else {
    if ((byte)(bVar1 - 0x30) < 8) {
      bVar8 = 0;
      pcVar12 = (char *)(param_1 + 1 + (bVar1 == 0x30 & param_2));
      pcVar5 = pcVar12 + 3;
      do {
        cVar9 = *pcVar12;
        pcVar17 = pcVar12;
        if (7 < (byte)(cVar9 - 0x30U)) break;
        pcVar12 = pcVar12 + 1;
        bVar8 = (cVar9 - 0x30U) + bVar8 * '\b';
        pcVar17 = pcVar5;
      } while (pcVar12 != pcVar5);
      iVar16 = ((int)pcVar17 - iVar16) + -1;
      pbVar4 = (byte *)stdout->_IO_write_ptr;
      if (stdout->_IO_write_end <= pbVar4) {
LAB_0010372a:
        __overflow(stdout,(uint)bVar8);
        return iVar16;
      }
LAB_00103650:
      stdout->_IO_write_ptr = (char *)(pbVar4 + 1);
      *pbVar4 = bVar8;
      return iVar16;
    }
    if (bVar1 != 0) {
      pcVar5 = strchr("\"\\abcefnrtv",(int)(char)bVar1);
      if (pcVar5 != (char *)0x0) {
        switch(bVar1) {
        case 0x61:
          pcVar5 = stdout->_IO_write_ptr;
          if (pcVar5 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar5 + 1;
            *pcVar5 = '\a';
            return 1;
          }
          __overflow(stdout,7);
          return 1;
        case 0x62:
          pcVar5 = stdout->_IO_write_ptr;
          if (pcVar5 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar5 + 1;
            *pcVar5 = '\b';
            return 1;
          }
          __overflow(stdout,8);
          return 1;
        case 99:
                    /* WARNING: Subroutine does not return */
          exit(0);
        case 0x65:
          pcVar5 = stdout->_IO_write_ptr;
          if (pcVar5 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar5 + 1;
            *pcVar5 = '\x1b';
            return 1;
          }
          __overflow(stdout,0x1b);
          return 1;
        case 0x66:
          pcVar5 = stdout->_IO_write_ptr;
          if (pcVar5 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar5 + 1;
            *pcVar5 = '\f';
            return 1;
          }
          __overflow(stdout,0xc);
          return 1;
        case 0x6e:
          pcVar5 = stdout->_IO_write_ptr;
          if (pcVar5 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar5 + 1;
            *pcVar5 = '\n';
            return 1;
          }
          __overflow(stdout,10);
          return 1;
        case 0x72:
          pcVar5 = stdout->_IO_write_ptr;
          if (pcVar5 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar5 + 1;
            *pcVar5 = '\r';
            return 1;
          }
          __overflow(stdout,0xd);
          return 1;
        case 0x74:
          pcVar5 = stdout->_IO_write_ptr;
          if (pcVar5 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar5 + 1;
            *pcVar5 = '\t';
            return 1;
          }
          __overflow(stdout,9);
          return 1;
        case 0x76:
          pcVar5 = stdout->_IO_write_ptr;
          if (pcVar5 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = pcVar5 + 1;
            *pcVar5 = '\v';
            return 1;
          }
          __overflow(stdout,0xb);
          return 1;
        }
switchD_00103794_caseD_64:
        pcVar5 = stdout->_IO_write_ptr;
        if (pcVar5 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar5 + 1;
          *pcVar5 = (char)uVar10;
          return 1;
        }
        __overflow(stdout,uVar10 & 0xff);
        return 1;
      }
      if ((bVar1 & 0xdf) == 0x55) {
        ppuVar6 = __ctype_b_loc();
        iVar2 = (uint)(bVar1 != 0x75) * 4;
        uVar15 = 0;
        pcVar12 = (char *)((ulong)(iVar2 + 6) + param_1);
        pcVar5 = (char *)(param_1 + 2);
        do {
          while( true ) {
            pcVar17 = pcVar5;
            cVar9 = *pcVar17;
            uVar10 = (uint)cVar9;
            if ((*(byte *)((long)*ppuVar6 + (ulong)(uVar10 & 0xff) * 2 + 1) & 0x10) == 0)
            goto LAB_00103a58;
            if ((byte)(cVar9 + 0x9fU) < 6) break;
            iVar11 = uVar10 - 0x30;
            if ((byte)(cVar9 + 0xbfU) < 6) {
              iVar11 = uVar10 - 0x37;
            }
            uVar15 = uVar15 * 0x10 + iVar11;
            pcVar5 = pcVar17 + 1;
            if (pcVar17 + 1 == pcVar12) goto LAB_00103828;
          }
          uVar15 = uVar15 * 0x10 + (uVar10 - 0x57);
          pcVar5 = pcVar17 + 1;
        } while (pcVar17 + 1 != pcVar12);
LAB_00103828:
        uVar10 = (int)pcVar17 + 1;
        if ((uVar15 < 0xa0) && (uVar15 != 0x24)) {
          if ((uVar15 & 0xffffffdf) == 0x40) {
LAB_00103842:
            print_unicode_char(stdout,uVar15,0);
            return (uVar10 - iVar16) + -1;
          }
        }
        else if (0x7ff < uVar15 - 0xd800) goto LAB_00103842;
        uVar7 = dcgettext(0,"invalid universal character name \\%c%0*x",5);
        error(1,0,uVar7,(int)(char)bVar1,iVar2 + 4,uVar15);
        goto switchD_00103794_caseD_64;
      }
    }
    pcVar5 = stdout->_IO_write_ptr;
    p_Var14 = stdout;
    if (pcVar5 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar5 + 1;
      *pcVar5 = '\\';
      goto LAB_00103698;
    }
  }
  __overflow(p_Var14,0x5c);
LAB_00103698:
  bVar1 = *(byte *)(param_1 + 1);
  iVar16 = 0;
  if (bVar1 != 0) {
    pbVar4 = (byte *)stdout->_IO_write_ptr;
    if (pbVar4 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = (char *)(pbVar4 + 1);
      *pbVar4 = bVar1;
    }
    else {
      __overflow(stdout,(uint)bVar1);
    }
    iVar16 = 1;
  }
  return iVar16;
}