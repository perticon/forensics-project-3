void verify_numeric(char *param_1,char *param_2)

{
  int *piVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  char *pcVar4;
  
  piVar1 = __errno_location();
  if (*piVar1 == 0) {
    if (*param_2 != '\0') {
      if (param_2 == param_1) {
        uVar2 = quote(param_2);
        pcVar4 = "%s: expected a numeric value";
      }
      else {
        uVar2 = quote(param_1);
        pcVar4 = "%s: value not completely converted";
      }
      uVar3 = dcgettext(0,pcVar4,5);
      error(0,0,uVar3,uVar2);
      exit_status = 1;
    }
    return;
  }
  uVar2 = quote(param_1);
  error(0,*piVar1,"%s",uVar2);
  exit_status = 1;
  return;
}