void verify_numeric(struct Eq_874 * rsi, struct Eq_874 * rdi)
{
	Eq_18 rax_17 = fn00000000000023F0();
	if (*rax_17 != 0x00)
	{
		quote();
		fn0000000000002650(0xA916, *rax_17, 0x00);
		g_dwF0B0 = 0x01;
	}
	else if (rsi->b0000 != 0x00)
	{
		char * rsi_63;
		if (rsi != rdi)
		{
			quote();
			rsi_63 = (char *) "%s: value not completely converted";
		}
		else
		{
			quote();
			rsi_63 = (char *) "%s: expected a numeric value";
		}
		fn0000000000002650(fn0000000000002490(0x05, rsi_63, null), 0x00, 0x00);
		g_dwF0B0 = 0x01;
	}
}