verify_numeric (char const *s, char const *end)
{
  if (errno)
    {
      error (0, errno, "%s", quote (s));
      exit_status = EXIT_FAILURE;
    }
  else if (*end)
    {
      if (s == end)
        error (0, 0, _("%s: expected a numeric value"), quote (s));
      else
        error (0, 0, _("%s: value not completely converted"), quote (s));
      exit_status = EXIT_FAILURE;
    }
}