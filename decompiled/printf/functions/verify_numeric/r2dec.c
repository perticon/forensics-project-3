uint64_t verify_numeric (char * arg1, char * * arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rax = errno_location ();
    edx = *(rax);
    if (edx != 0) {
        goto label_0;
    }
    if (*(rbp) == 0) {
        goto label_1;
    }
    if (rbp == r12) {
        goto label_2;
    }
    rax = quote (r12, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    do {
        rax = dcgettext (0, "%s: value not completely converted");
        rcx = r12;
        eax = 0;
        rax = error (0, 0, rax);
        *(obj.exit_status) = 1;
label_1:
        return rax;
label_0:
        rbx = rax;
        rax = quote (r12, rsi, rdx, rcx, r8);
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000a916);
        *(obj.exit_status) = 1;
        return rax;
label_2:
        rax = quote (rbp, rsi, rdx, rcx, r8);
        edx = 5;
        rsi = "%s: expected a numeric value";
        r12 = rax;
    } while (1);
}