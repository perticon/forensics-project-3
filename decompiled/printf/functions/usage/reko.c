void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000026B0(fn0000000000002490(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000003D5E;
	}
	fn0000000000002620(fn0000000000002490(0x05, "Usage: %s FORMAT [ARGUMENT]...\n  or:  %s OPTION\n", null), 0x01);
	fn0000000000002550(stdout, fn0000000000002490(0x05, "Print ARGUMENT(s) according to FORMAT, or execute according to OPTION:\n\n", null));
	fn0000000000002550(stdout, fn0000000000002490(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002550(stdout, fn0000000000002490(0x05, "      --version     output version information and exit\n", null));
	fn0000000000002550(stdout, fn0000000000002490(0x05, "\nFORMAT controls the output as in C printf.  Interpreted sequences are:\n\n  \\\"      double quote\n", null));
	fn0000000000002550(stdout, fn0000000000002490(0x05, "  \\\\      backslash\n  \\a      alert (BEL)\n  \\b      backspace\n  \\c      produce no further output\n  \\e      escape\n  \\f      form feed\n  \\n      new line\n  \\r      carriage return\n  \\t      horizontal tab\n  \\v      vertical tab\n", null));
	fn0000000000002550(stdout, fn0000000000002490(0x05, "  \\NNN    byte with octal value NNN (1 to 3 digits)\n  \\xHH    byte with hexadecimal value HH (1 to 2 digits)\n  \\uHHHH  Unicode (ISO/IEC 10646) character with hex value HHHH (4 digits)\n  \\UHHHHHHHH  Unicode character with hex value HHHHHHHH (8 digits)\n", null));
	fn0000000000002550(stdout, fn0000000000002490(0x05, "  %%      a single %\n  %b      ARGUMENT as a string with '\\' escapes interpreted,\n          except that octal escapes are of the form \\0 or \\0NNN\n  %q      ARGUMENT is printed in a format that can be reused as shell input,\n          escaping non-printable characters with the proposed POSIX $'' syntax.\n\nand all C format specifications ending with one of diouxXfeEgGcs, with\nARGUMENTs converted to proper type first.  Variable widths are handled.\n", null));
	fn0000000000002620(fn0000000000002490(0x05, "\nNOTE: your shell may have its own version of %s, which usually supersedes\nthe version described here.  Please refer to your shell's documentation\nfor details about the options it supports.\n", null), 0x01);
	struct Eq_1884 * rbx_211 = fp - 0xB8 + 16;
	do
	{
		char * rsi_213 = rbx_211->qw0000;
		++rbx_211;
	} while (rsi_213 != null && fn0000000000002570(rsi_213, 42998) != 0x00);
	ptr64 r13_226 = rbx_211->qw0008;
	if (r13_226 != 0x00)
	{
		fn0000000000002620(fn0000000000002490(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_18 rax_313 = fn0000000000002610(null, 0x05);
		if (rax_313 == 0x00 || fn0000000000002400(0x03, "en_", rax_313) == 0x00)
			goto l0000000000003FD6;
	}
	else
	{
		fn0000000000002620(fn0000000000002490(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_18 rax_255 = fn0000000000002610(null, 0x05);
		if (rax_255 == 0x00 || fn0000000000002400(0x03, "en_", rax_255) == 0x00)
		{
			fn0000000000002620(fn0000000000002490(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000004013:
			fn0000000000002620(fn0000000000002490(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000003D5E:
			fn0000000000002690(edi);
		}
		r13_226 = 42998;
	}
	fn0000000000002550(stdout, fn0000000000002490(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000003FD6:
	fn0000000000002620(fn0000000000002490(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000004013;
}