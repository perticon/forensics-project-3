int main(int param_1,undefined8 *param_2)

{
  byte *pbVar1;
  byte *pbVar2;
  bool bVar3;
  bool bVar4;
  FILE *__stream;
  char cVar5;
  byte bVar6;
  int iVar7;
  int iVar8;
  uint uVar9;
  char *pcVar10;
  undefined8 uVar11;
  undefined8 uVar12;
  ulong uVar13;
  void *__dest;
  void *pvVar14;
  int *piVar15;
  byte *pbVar16;
  size_t sVar17;
  long lVar18;
  byte *__n;
  ulong uVar19;
  uint uVar20;
  undefined4 uVar21;
  byte *pbVar22;
  undefined8 *puVar23;
  byte *pbVar24;
  byte *pbVar25;
  int iVar26;
  long in_FS_OFFSET;
  bool bVar27;
  byte bVar28;
  byte **local_1a8;
  byte **local_1a0;
  undefined4 local_184;
  undefined4 local_160;
  char *local_158;
  undefined8 local_150;
  undefined8 local_148 [8];
  undefined local_107;
  undefined local_103;
  undefined2 local_102;
  undefined local_f0;
  undefined local_e7;
  undefined local_e5;
  uint local_e4;
  undefined local_df;
  undefined local_d9;
  undefined local_d5;
  undefined local_d3;
  undefined local_d0;
  long local_40;
  
  bVar28 = 0;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  exit_status = 0;
  pcVar10 = getenv("POSIXLY_CORRECT");
  posixly_correct = pcVar10 != (char *)0x0;
  if (param_1 == 2) {
    pcVar10 = (char *)param_2[1];
    iVar7 = strcmp(pcVar10,"--help");
    if (iVar7 == 0) {
LAB_001034de:
                    /* WARNING: Subroutine does not return */
      usage(0);
    }
    iVar7 = strcmp(pcVar10,"--version");
    if (iVar7 == 0) {
      version_etc(stdout,"printf","GNU coreutils",Version,"David MacKenzie",0);
      goto LAB_00102964;
    }
    iVar7 = strcmp(pcVar10,"--");
    if (iVar7 == 0) goto LAB_001027ee;
  }
  else {
    if (param_1 < 2) {
LAB_001027ee:
      uVar11 = dcgettext(0,"missing operand",5);
      error(0,0,uVar11);
                    /* WARNING: Subroutine does not return */
      usage(1);
    }
    iVar7 = strcmp((char *)param_2[1],"--");
    if (iVar7 == 0) {
      param_1 = param_1 + -1;
      param_2 = param_2 + 1;
    }
  }
  pbVar2 = (byte *)param_2[1];
  local_1a0 = (byte **)(param_2 + 2);
  bVar6 = *pbVar2;
  iVar26 = param_1 + -2;
joined_r0x00102865:
  if (bVar6 != 0) {
    uVar20 = (uint)bVar6;
    local_160 = 0;
    local_184 = 0;
    pbVar16 = pbVar2;
    iVar7 = iVar26;
    local_1a8 = local_1a0;
LAB_001028bf:
    __stream = stdout;
    cVar5 = (char)uVar20;
    if (cVar5 != '%') {
      if (cVar5 != '\\') {
        pcVar10 = stdout->_IO_write_ptr;
        pbVar25 = pbVar16;
        if (pcVar10 < stdout->_IO_write_end) {
          stdout->_IO_write_ptr = pcVar10 + 1;
          *pcVar10 = cVar5;
        }
        else {
          __overflow(stdout,uVar20);
        }
        goto LAB_001028b2;
      }
      iVar8 = print_esc(pbVar16);
      bVar6 = pbVar16[(long)iVar8 + 1];
      pbVar16 = pbVar16 + (long)iVar8 + 1;
      goto joined_r0x001028bd;
    }
    bVar6 = pbVar16[1];
    uVar20 = (uint)bVar6;
    pbVar25 = pbVar16 + 1;
    if (bVar6 == 0x25) {
      pcVar10 = stdout->_IO_write_ptr;
      if (pcVar10 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar10 + 1;
        *pcVar10 = '%';
      }
      else {
        __overflow(stdout,0x25);
      }
    }
    else {
      if (bVar6 != 0x62) {
        if (bVar6 != 0x71) {
          pbVar22 = (byte *)0x1;
          bVar4 = false;
          puVar23 = local_148;
          for (lVar18 = 0x20; lVar18 != 0; lVar18 = lVar18 + -1) {
            *puVar23 = 0;
            puVar23 = puVar23 + (ulong)bVar28 * -2 + 1;
          }
          local_f0 = 1;
          bVar3 = false;
          local_d0 = 1;
          local_d3 = 1;
          local_d5 = 1;
          local_d9 = 1;
          local_df = 1;
          local_103 = 1;
          local_102 = 0x101;
          local_e5 = 1;
          local_e4 = 0x1010101;
          local_107 = 1;
          local_e7 = 1;
          pbVar24 = pbVar25;
          do {
            switch(uVar20 - 0x20 & 0xff) {
            default:
              goto switchD_00102b59_caseD_1;
            case 3:
              lVar18 = 1;
            case 0x10:
              bVar4 = true;
            case 0:
            case 0xb:
            case 0xd:
              pbVar24 = pbVar24 + 1;
              pbVar22 = pbVar22 + 1;
              uVar20 = (uint)*pbVar24;
              break;
            case 7:
            case 0x29:
              pbVar24 = pbVar24 + 1;
              bVar4 = true;
              bVar3 = true;
              pbVar22 = pbVar22 + 1;
              uVar20 = (uint)*pbVar24;
            }
          } while( true );
        }
        if (iVar7 != 0) {
          iVar7 = iVar7 + -1;
          pbVar16 = *local_1a8;
          local_1a8 = local_1a8 + 1;
          pcVar10 = (char *)quotearg_style(3,pbVar16);
          fputs_unlocked(pcVar10,__stream);
          goto LAB_001028b2;
        }
LAB_001029b4:
        pbVar25 = pbVar16 + 2;
        uVar20 = (uint)*pbVar25;
        pbVar16 = pbVar16 + 2;
        iVar7 = exit_status;
        if (*pbVar25 == 0) goto LAB_00102964;
        iVar7 = 0;
        goto LAB_001028bf;
      }
      if (iVar7 == 0) goto LAB_001029b4;
      pbVar16 = *local_1a8;
      bVar6 = *pbVar16;
      while (pbVar22 = pbVar16, bVar6 != 0) {
        while (bVar6 != 0x5c) {
          pbVar16 = (byte *)stdout->_IO_write_ptr;
          if (pbVar16 < stdout->_IO_write_end) {
            stdout->_IO_write_ptr = (char *)(pbVar16 + 1);
            *pbVar16 = bVar6;
          }
          else {
            __overflow(stdout,(uint)bVar6);
          }
          bVar6 = pbVar22[1];
          pbVar22 = pbVar22 + 1;
          if (bVar6 == 0) goto LAB_00102a60;
        }
        iVar8 = print_esc(pbVar22);
        pbVar16 = pbVar22 + (long)iVar8 + 1;
        bVar6 = pbVar22[(long)iVar8 + 1];
      }
LAB_00102a60:
      local_1a8 = local_1a8 + 1;
      iVar7 = iVar7 + -1;
    }
    goto LAB_001028b2;
  }
LAB_00102924:
  iVar7 = exit_status;
  if (iVar26 != 0) {
    uVar11 = quote(*local_1a0);
    uVar12 = dcgettext(0,"warning: ignoring excess arguments, starting with %s",5);
    error(0,0,uVar12,uVar11);
    iVar7 = exit_status;
  }
LAB_00102964:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return iVar7;
  }
LAB_00103476:
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
switchD_00102b59_caseD_1:
  bVar27 = (char)lVar18 != '\0';
  if (bVar27) {
    local_e4 = 0x1010100;
    local_df = 0;
    local_d3 = 0;
  }
  local_d3 = !bVar27;
  local_df = !bVar27;
  if (bVar3) {
    local_e7 = 0;
    local_107 = 0;
  }
  local_e7 = !bVar3;
  local_107 = !bVar3;
  if (bVar4) {
    local_e5 = 0;
  }
  local_e5 = !bVar4;
  if (bVar3) {
    local_e4 = local_e4 & 0xffff00ff;
    local_103 = 0;
    local_d9 = 0;
    if (bVar4) {
LAB_00102bc6:
      local_d5 = 0;
      if (!bVar3) goto LAB_00102bd7;
    }
    local_d0 = 0;
    local_f0 = 0;
  }
  else if (bVar4) goto LAB_00102bc6;
LAB_00102bd7:
  if ((char)uVar20 == '*') {
    pbVar22 = pbVar22 + 1;
    if (iVar7 == 0) {
      local_184 = 0;
      uVar20 = (uint)pbVar24[1];
      bVar4 = true;
      pbVar24 = pbVar24 + 1;
    }
    else {
      lVar18 = vstrtoimax(*local_1a8);
      if (lVar18 + 0x80000000U >> 0x20 != 0) {
        uVar11 = quote(*local_1a8);
        uVar12 = dcgettext(0,"invalid field width: %s",5);
        error(1,0,uVar12,uVar11);
        goto LAB_00103476;
      }
      local_1a8 = local_1a8 + 1;
      local_184 = (undefined4)lVar18;
      iVar7 = iVar7 + -1;
      uVar20 = (uint)pbVar24[1];
      bVar4 = true;
      pbVar24 = pbVar24 + 1;
    }
LAB_00102c06:
    __n = pbVar22;
    pbVar25 = pbVar24;
    if ((char)uVar20 == '.') goto LAB_00102c0f;
LAB_00103022:
    bVar3 = false;
  }
  else {
    pbVar25 = pbVar24;
    if (9 < (int)(char)uVar20 - 0x30U) {
      bVar4 = false;
      goto LAB_00102c06;
    }
    do {
      pbVar1 = pbVar25 + 1;
      uVar20 = (uint)(char)*pbVar1;
      pbVar25 = pbVar25 + 1;
      __n = pbVar25 + ((long)pbVar22 - (long)pbVar24);
    } while (uVar20 - 0x30 < 10);
    bVar4 = false;
    pbVar22 = __n;
    pbVar24 = pbVar25;
    if (*pbVar1 != 0x2e) goto LAB_00103022;
LAB_00102c0f:
    bVar6 = pbVar24[1];
    uVar20 = (uint)bVar6;
    local_e5 = 0;
    if (bVar6 == 0x2a) {
      pbVar25 = pbVar24 + 2;
      __n = pbVar22 + 2;
      if (iVar7 == 0) {
        uVar20 = (uint)pbVar24[2];
        local_160 = 0;
        bVar3 = true;
      }
      else {
        lVar18 = vstrtoimax(*local_1a8);
        if (lVar18 < 0) {
          local_160 = 0xffffffff;
        }
        else {
          if (0x7fffffff < lVar18) {
            uVar11 = quote(*local_1a8);
            uVar12 = dcgettext(0,"invalid precision: %s",5);
            error(1,0,uVar12,uVar11);
            goto LAB_001034b1;
          }
          local_160 = (undefined4)lVar18;
        }
        local_1a8 = local_1a8 + 1;
        uVar20 = (uint)pbVar24[2];
        iVar7 = iVar7 + -1;
        bVar3 = true;
      }
    }
    else {
      pbVar25 = pbVar24 + 1;
      __n = pbVar22 + 1;
      uVar9 = (int)(char)bVar6;
      while (uVar9 - 0x30 < 10) {
        pbVar1 = pbVar25 + 1;
        pbVar25 = pbVar25 + 1;
        __n = pbVar25 + ((long)pbVar22 - (long)pbVar24);
        uVar20 = (uint)(char)*pbVar1;
        uVar9 = uVar20;
      }
      bVar3 = false;
    }
  }
  while ((bVar6 = (byte)uVar20, (bVar6 & 0xdf) == 0x4c ||
         (((byte)(uVar20 - 0x68) < 0x13 && ((0x41005U >> ((ulong)(uVar20 - 0x68) & 0x3f) & 1) != 0))
         ))) {
    pbVar25 = pbVar25 + 1;
    uVar20 = (uint)*pbVar25;
  }
  pbVar24 = pbVar25;
  if (*(char *)((long)local_148 + (ulong)(uVar20 & 0xff)) == '\0') {
LAB_001034b1:
    uVar11 = dcgettext(0,"%.*s: invalid conversion specification",5);
    error(1,0,uVar11,((int)pbVar24 + 1) - (int)pbVar16,pbVar16);
    goto LAB_001034de;
  }
  pbVar22 = (byte *)0x10b052;
  if (iVar7 != 0) {
    iVar7 = iVar7 + -1;
    pbVar22 = *local_1a8;
    local_1a8 = local_1a8 + 1;
  }
  pbVar24 = pbVar16;
  if ((byte)(bVar6 + 0xbf) < 0x38) {
    uVar13 = 1 << (bVar6 + 0xbf & 0x3f);
    if ((uVar13 & 0x7100000071) == 0) {
      uVar13 = uVar13 & 0x90410800800000;
      if (uVar13 != 0) {
        uVar13 = 1;
        pbVar24 = &DAT_0010a8a0;
      }
    }
    else {
      uVar13 = 1;
      pbVar24 = &DAT_0010a8a3;
    }
  }
  else {
    uVar13 = 0;
  }
  __dest = (void *)xmalloc(__n + uVar13 + 2);
  pvVar14 = mempcpy(__dest,pbVar16,(size_t)__n);
  if ((uint)uVar13 != 0) {
    uVar9 = 0;
    do {
      uVar19 = (ulong)uVar9;
      uVar9 = uVar9 + 1;
      *(byte *)((long)pvVar14 + uVar19) = pbVar24[uVar19];
    } while (uVar9 < (uint)uVar13);
  }
  *(byte *)((long)pvVar14 + uVar13) = bVar6;
  ((byte *)((long)pvVar14 + uVar13))[1] = 0;
  uVar21 = local_160;
  switch(uVar20 - 0x41 & 0xff) {
  case 0:
  case 4:
  case 5:
  case 6:
  case 0x20:
  case 0x24:
  case 0x25:
  case 0x26:
    if (((*pbVar22 == 0x22) || (*pbVar22 == 0x27)) && (pbVar22[1] != 0)) {
      pbVar16 = pbVar22 + 1;
      sVar17 = __ctype_get_mb_cur_max();
      if ((1 < sVar17) && (pbVar22[2] != 0)) {
        local_150 = 0;
        sVar17 = strlen((char *)pbVar16);
        lVar18 = rpl_mbrtowc(&local_158,pbVar16,sVar17,&local_150);
        if (0 < lVar18) {
          pbVar16 = pbVar22 + lVar18;
        }
      }
      if ((pbVar16[1] != 0) && (posixly_correct == '\0')) {
        uVar11 = dcgettext(0,
                           "warning: %s: character(s) following character constant have been ignored"
                           ,5);
        error(0,0,uVar11,pbVar16 + 1);
      }
    }
    else {
      piVar15 = __errno_location();
      *piVar15 = 0;
      cl_strtold(pbVar22,&local_158);
      verify_numeric(pbVar22);
    }
    if (bVar4) {
      uVar21 = local_184;
      if (!bVar3) goto LAB_0010318c;
      xprintf(__dest,local_184,local_160);
    }
    else if (bVar3) {
LAB_0010318c:
      xprintf(__dest,uVar21);
    }
    else {
      xprintf(__dest);
    }
    break;
  case 0x17:
  case 0x2e:
  case 0x34:
  case 0x37:
    if (((*pbVar22 == 0x22) || (*pbVar22 == 0x27)) &&
       (pbVar16 = (byte *)(ulong)pbVar22[1], pbVar22[1] != 0)) {
      pbVar24 = pbVar22 + 1;
      sVar17 = __ctype_get_mb_cur_max();
      if ((1 < sVar17) && (pbVar22[2] != 0)) {
        local_150 = 0;
        sVar17 = strlen((char *)pbVar24);
        lVar18 = rpl_mbrtowc(&local_158,pbVar24,sVar17,&local_150);
        if (0 < lVar18) {
          pbVar16 = (byte *)(long)(int)local_158;
          pbVar24 = pbVar22 + lVar18;
        }
      }
      pbVar22 = pbVar16;
      if ((pbVar24[1] != 0) && (posixly_correct == '\0')) {
        uVar11 = dcgettext(0,
                           "warning: %s: character(s) following character constant have been ignored"
                           ,5);
        error(0,0,uVar11,pbVar24 + 1);
      }
    }
    else {
      piVar15 = __errno_location();
      *piVar15 = 0;
      pbVar16 = (byte *)strtoumax((char *)pbVar22,&local_158,0);
      verify_numeric(pbVar22);
      pbVar22 = pbVar16;
    }
    if (bVar4) {
joined_r0x001031b8:
      uVar21 = local_184;
      if (bVar3) {
        xprintf(__dest,local_184,local_160,pbVar22);
        free(__dest);
        goto LAB_001028b2;
      }
    }
    else if (!bVar3) goto LAB_00102f62;
LAB_00102e4f:
    xprintf(__dest,uVar21,pbVar22);
    break;
  case 0x22:
    if (bVar4) {
      xprintf(__dest);
    }
    else {
      xprintf(__dest);
    }
    break;
  case 0x23:
  case 0x28:
    pbVar22 = (byte *)vstrtoimax(pbVar22);
    if (bVar4) goto joined_r0x001031b8;
    if (bVar3) goto LAB_00102e4f;
    xprintf(__dest,pbVar22,pbVar22);
    break;
  case 0x32:
    if (bVar4) goto joined_r0x001031b8;
    if (bVar3) goto LAB_00102e4f;
LAB_00102f62:
    xprintf(__dest);
  }
  free(__dest);
LAB_001028b2:
  bVar6 = pbVar25[1];
  pbVar16 = pbVar25 + 1;
joined_r0x001028bd:
  uVar20 = (uint)bVar6;
  if (bVar6 == 0) goto LAB_001028e8;
  goto LAB_001028bf;
LAB_001028e8:
  iVar8 = iVar26 - iVar7;
  local_1a0 = local_1a0 + iVar8;
  iVar26 = iVar7;
  if ((iVar8 < 1) || (iVar7 < 1)) goto LAB_00102924;
  bVar6 = *pbVar2;
  goto joined_r0x00102865;
}