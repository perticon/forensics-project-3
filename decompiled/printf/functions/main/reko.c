void main(struct Eq_414 * rsi, int32 edi, struct Eq_416 * fs, word64 * qwArg00)
{
	struct Eq_418 * fp;
	word64 rax_31 = fs->qw0028;
	set_program_name(rsi->ptr0000);
	fn0000000000002610("", 0x06);
	fn0000000000002480("/usr/local/share/locale", "coreutils");
	fn0000000000002460("coreutils");
	atexit(&g_t41D0);
	g_dwF0B0 = 0x00;
	g_bF0AC = (int8) (fn00000000000023A0("POSIXLY_CORRECT") != 0x00);
	struct Eq_414 * rbx_150 = rsi;
	struct Eq_418 * rsp_1058 = fp - 424;
	if (edi != 0x02)
	{
		if (edi <= 0x01)
			goto l00000000000027EE;
		if (fn0000000000002570("--", rsi->t0008) == 0x00)
			rbx_150 = (struct Eq_414 *) &rsi->t0008;
	}
	else
	{
		Eq_225 r13_72 = rsi->t0008;
		if (fn0000000000002570("--help", r13_72) == 0x00)
			goto l00000000000034DE;
		if (fn0000000000002570("--version", r13_72) == 0x00)
		{
			version_etc(42998, stdout, fs);
			goto l0000000000002964;
		}
		if (fn0000000000002570("--", r13_72) == 0x00)
		{
l00000000000027EE:
			fn0000000000002650(fn0000000000002490(0x05, "missing operand", null), 0x00, 0x00);
			usage(0x01);
		}
	}
	uint64 r15_1107;
	Eq_225 rax_154 = rbx_150->t0008;
	uint64 r12_1059 = 0x00041005;
	Eq_225 rbp_1012 = rax_154;
	byte al_1008 = *rax_154;
	if (al_1008 == 0x00)
	{
l000000000000291F:
		r15_1107 = (uint64) rsp_1058->dw0020;
		goto l0000000000002924;
	}
l0000000000002870:
	Eq_18 rsi_176 = rsp_1058->t0008;
	rsp_1058->dw0048 = 0x00;
	rsp_1058->dw0024 = 0x00;
	uint64 r15_180 = (uint64) rsp_1058->dw0020;
	rsp_1058->t0000 = rsi_176;
l00000000000028BF:
	uint64 r15_1832;
	Eq_225 r13_1026;
	rsp_1058 = fp;
	word32 r15d_205 = (word32) r15_1832;
	if (al_1008 != 0x25)
	{
		if (al_1008 == 0x5C)
		{
			word64 rbp_1052;
			word64 rbx_1849;
			struct Eq_581 * r13_1067 = (int64) print_esc(0x00, rbp_1012, out rbx_1849, out rbp_1052, out r12_1059) + rbp_1052;
			rsp_1058 = (struct Eq_418 *) <invalid>;
			al_1008 = r13_1067->b0001;
			rbp_1012 = &r13_1067->b0001;
			r15_180 = r15_1832;
			r15_1832 = r15_180;
			if (al_1008 == 0x00)
				goto l00000000000028E8;
			goto l00000000000028BF;
		}
		FILE * rdi_1020 = stdout;
		byte * rdx_1021 = rdi_1020->ptr0028;
		if (rdx_1021 < rdi_1020->ptr0030)
		{
			rdi_1020->ptr0028 = rdx_1021 + 1;
			*rdx_1021 = al_1008;
			r13_1026 = rbp_1012;
		}
		else
		{
			fn0000000000002500((word32) al_1008, rdi_1020);
			r13_1026 = rbp_1012;
		}
l00000000000028B2:
		al_1008 = (byte) *((word64) r13_1026 + 1);
		rbp_1012 = (word64) r13_1026 + 1;
		r15_180 = r15_1832;
		if (al_1008 == 0x00)
		{
l00000000000028E8:
			int32 r15d_1082 = (word32) r15_1832;
			int32 eax_1088 = rsp_1058->dw0020 - r15d_1082;
			rsp_1058->t0008 = (word64) rsp_1058->t0008 + (int64) eax_1088 * 0x08;
			r15_1107 = r15_1832;
			if (eax_1088 > 0x00)
			{
				r15_1107 = r15_1832;
				if (r15d_1082 > 0x00)
				{
					rbp_1012 = rsp_1058->t0040;
					rsp_1058->dw0020 = r15d_1082;
					al_1008 = (byte) *rbp_1012;
					if (al_1008 == 0x00)
						goto l000000000000291F;
					goto l0000000000002870;
				}
			}
l0000000000002924:
			if ((word32) r15_1107 != 0x00)
			{
				quote();
				fn0000000000002650(fn0000000000002490(0x05, "warning: ignoring excess arguments, starting with %s", null), 0x00, 0x00);
			}
			goto l000000000000295D;
		}
		goto l00000000000028BF;
	}
	Eq_553 ebx_1840 = (uint64) *((word64) rbp_1012 + 1);
	byte bl_190 = (byte) ebx_1840;
	word32 rbx_32_32_1337 = SLICE(ebx_1840, word32, 32);
	r13_1026 = (word64) rbp_1012 + 1;
	if (bl_190 == 0x25)
	{
		FILE * rdi_193 = stdout;
		byte * rax_194 = rdi_193->ptr0028;
		if (rax_194 < rdi_193->ptr0030)
		{
			rdi_193->ptr0028 = rax_194 + 1;
			*rax_194 = 0x25;
		}
		else
			fn0000000000002500(0x25, rdi_193);
		goto l00000000000028B2;
	}
	if (bl_190 == 0x62)
	{
		if (r15d_205 == 0x00)
			goto l00000000000029B4;
		Eq_225 rbx_211 = *qwArg00;
		byte al_214 = *rbx_211;
		while (al_214 != 0x00)
		{
			if (al_214 != 0x5C)
			{
				FILE * rdi_222 = stdout;
				byte * rdx_223 = rdi_222->ptr0028;
				if (rdx_223 < rdi_222->ptr0030)
				{
					rdi_222->ptr0028 = rdx_223 + 1;
					*rdx_223 = al_214;
				}
				else
					fn0000000000002500((word32) al_214, rdi_222);
				rbx_211 = (word64) rbx_211 + 1;
				al_214 = (byte) *((word64) rbx_211 + 1);
				if (al_214 != 0x00)
					continue;
				break;
			}
			word64 rbx_250;
			word64 rbp_1850;
			struct Eq_817 * rax_264 = (int64) print_esc(0x01, rbx_211, out rbx_250, out rbp_1850, out r12_1059) + rbx_250;
			rsp_1058 = (struct Eq_418 *) <invalid>;
			rbx_211 = &rax_264->b0001;
			al_214 = rax_264->b0001;
		}
		rsp_1058->t0000 = (word64) rsp_1058->t0000 + 8;
		r15_1832 = (uint64) (r15d_205 - 0x01);
		goto l00000000000028B2;
	}
	if (bl_190 == 113)
	{
		if (r15d_205 != 0x00)
		{
			fn0000000000002550(stdout, quotearg_style(0x03, fs));
			r15_1832 = (uint64) (r15d_205 - 0x01);
			++qwArg00;
			goto l00000000000028B2;
		}
l00000000000029B4:
		al_1008 = (byte) *((word64) rbp_1012 + 2);
		Eq_225 rdx_1007 = (word64) rbp_1012 + 2;
		if (al_1008 == 0x00)
		{
l0000000000002964:
			if (rax_31 - fs->qw0028 == 0x00)
				return;
			goto l0000000000003476;
		}
		rbp_1012 = rdx_1007;
		r15_180 = 0x00;
		goto l00000000000028BF;
	}
	uint64 rcx_289 = 0x20;
	word64 * rdi_294 = fp->a0060;
	while (true)
	{
		byte bl_386 = (byte) ebx_1840;
		byte cl_325 = (byte) rcx_289;
		if (rcx_289 == 0x00)
			break;
		*rdi_294 = 0x00;
		++rdi_294;
		--rcx_289;
	}
	cu8 al_322 = (byte) ebx_1840 - 0x20;
	if (al_322 <= 0x29)
	{
		<anonymous> * rax_954 = (int64) g_aA988[(uint64) al_322 * 0x04] + 43400;
		word64 rcx_962;
		word64 r9_963;
		word64 r8_964;
		word64 r10_965;
		word64 r11_966;
		rax_954();
		return;
	}
	if (false)
	{
		if (true)
			goto l000000000000335B;
	}
	else if (true)
		goto l0000000000002BD7;
	if (true)
	{
l0000000000002BD7:
		uint64 rbx_1847;
		uint64 rbx_1837;
		Eq_18 rdx_406;
		if (bl_386 == 0x2A)
		{
			rdx_406.u0 = 0x02;
			if (r15d_205 == 0x00)
			{
				ebx_1840.u0 = (uint64) *((word64) r13_1026 + 1);
				rbx_32_32_1337 = SLICE(ebx_1840, word32, 32);
				++r13_1026;
			}
			else
			{
				if (vstrtoimax(*qwArg00, fs) + 0x80000000 >> 0x20 != 0x00)
				{
					quote();
					fn0000000000002650(fn0000000000002490(0x05, "invalid field width: %s", null), 0x00, 0x01);
l0000000000003476:
					fn00000000000024C0();
				}
				ebx_1840.u0 = (uint64) *((word64) r13_1026 + 1);
				rdx_406.u0 = 0x02;
				r15_1832 = (uint64) (r15d_205 - 0x01);
				++qwArg00;
				rbx_32_32_1337 = SLICE(ebx_1840, word32, 32);
				++r13_1026;
			}
		}
		else
		{
			Eq_225 rax_391 = r13_1026;
			if ((int32) bl_386 <= 0x39 && (int32) bl_386 >= 0x30)
			{
				do
				{
					Eq_1134 ecx_409 = (int32) *((word64) rax_391 + 1);
					rbx_1847 = (uint64) ecx_409;
					rax_391 = (word64) rax_391 + 1;
					byte bl_425 = (byte) rbx_1847;
					rdx_406 = (word64) rax_391 + 1 - r13_1026;
				} while (ecx_409 <= 0x39);
				r13_1026 = rax_391;
				if (bl_425 == 0x2E)
				{
l0000000000002C0F:
					uint64 rbx_1846 = (uint64) *((word64) r13_1026 + 1);
					word32 r15d_610 = (word32) r15_1832;
					byte bl_573 = (byte) rbx_1846;
					if (bl_573 == 0x2A)
					{
						Eq_225 rcx_608 = (word64) r13_1026 + 2;
						rdx_406.u0 = (byte) rdx_406.u0 + 2;
						if (r15d_610 != 0x00)
						{
							int64 rax_649 = vstrtoimax(*qwArg00, fs);
							if (rax_649 >= 0x00 && rax_649 > 0x7FFFFFFF)
							{
								quote();
								fn0000000000002650(fn0000000000002490(0x05, "invalid precision: %s", null), 0x00, 0x01);
								goto l00000000000034B1;
							}
							++qwArg00;
							rbx_1837 = (uint64) *((word64) r13_1026 + 2);
							r15_1832 = (uint64) (r15d_610 - 0x01);
							r13_1026 = rcx_608;
						}
						else
						{
							rbx_1837 = (uint64) *((word64) r13_1026 + 2);
							r13_1026 = rcx_608;
						}
					}
					else
					{
						Eq_225 rax_577 = (word64) r13_1026 + 1;
						Eq_18 rsi_580 = (byte) rdx_406.u0 + 1;
						if ((int32) bl_573 <= 0x39 && (int32) bl_573 >= 0x30)
						{
							do
							{
								Eq_1094 ecx_588 = (int32) *((word64) rax_577 + 1);
								rax_577 = (word64) rax_577 + 1;
								rbx_1846 = (uint64) ecx_588;
								rsi_580 = rax_577 + rdx_406 - r13_1026;
							} while (ecx_588 <= 0x39);
						}
						rdx_406 = rsi_580;
						r13_1026 = rax_577;
						rbx_1837 = rbx_1846;
					}
l0000000000002C88:
					while (true)
					{
						word32 ebx_682 = (word32) rbx_1837;
						word32 r15d_720 = (word32) r15_1832;
						cu8 bl_709 = (byte) ebx_682;
						if (((byte) ebx_682 & 223) != 0x4C)
						{
							uint64 rax_694 = (uint64) (ebx_682 - 0x68);
							if ((byte) rax_694 > 0x12 || !__bt<word64>(r12_1059, rax_694))
							{
								if (fp->a0060[(uint64) bl_709] != 0x00)
								{
									if (r15d_720 != 0x00)
									{
										r15_1832 = (uint64) (r15d_720 - 0x01);
										++qwArg00;
									}
									Eq_225 r10_753;
									uint64 rcx_755;
									if (bl_709 <= 0x78 && bl_709 >= 0x41)
									{
										ui64 rax_761 = 0x01 << bl_709 - 0x41;
										if ((rax_761 & 0x7100000071) == 0x00)
										{
											r10_753 = rbp_1012;
											rcx_755 = rax_761 & 0x90410800800000;
											if ((rax_761 & 0x90410800800000) != 0x00)
											{
												rcx_755 = 0x01;
												r10_753.u0 = 0xA8A0;
											}
										}
										else
										{
											rcx_755 = 0x01;
											r10_753.u0 = 0xA8A3;
										}
									}
									else
									{
										r10_753 = rbp_1012;
										rcx_755 = 0x00;
									}
									Eq_18 rax_851 = xmalloc((byte) rdx_406.u0 + 2 + rcx_755);
									up32 ecx_868 = (word32) rcx_755;
									int64 rax_863 = fn0000000000002630(rdx_406, rbp_1012, rax_851);
									if (ecx_868 != 0x00)
									{
										up32 eax_877 = 0x00;
										do
										{
											Eq_1051 rdx_882 = (uint64) eax_877;
											Mem891[rax_863 + rdx_882:byte] = Mem886[r10_753 + rdx_882:byte];
											++eax_877;
										} while (eax_877 < ecx_868);
									}
									struct Eq_1034 * rsi_899 = rax_863 + rcx_755;
									rsi_899->b0000 = bl_709;
									rsi_899->b0001 = 0x00;
									cu8 bl_911 = (byte) ebx_682 - 0x41;
									if (bl_911 <= 55)
									{
										<anonymous> * rax_921 = (int64) g_aAA30[(uint64) bl_911 * 0x04] + 0xAA30;
										word64 rcx_945;
										word64 r9_946;
										word64 r8_947;
										word64 r10_948;
										word64 r11_949;
										rax_921();
										return;
									}
									fn00000000000023C0(rax_851);
									goto l00000000000028B2;
								}
l00000000000034B1:
								fn0000000000002650(fn0000000000002490(0x05, "%.*s: invalid conversion specification", null), 0x00, 0x01);
l00000000000034DE:
								usage(0x00);
							}
						}
						r13_1026 = (word64) r13_1026 + 1;
						rbx_1837 = (uint64) *r13_1026;
					}
				}
l0000000000003022:
				rbx_1837 = rbx_1847;
				goto l0000000000002C88;
			}
			rdx_406.u0 = 0x01;
		}
		rbx_1847 = SEQ(rbx_32_32_1337, ebx_1840);
		if ((byte) ebx_1840 == 0x2E)
			goto l0000000000002C0F;
		goto l0000000000003022;
	}
l000000000000335B:
	goto l0000000000002BD7;
}