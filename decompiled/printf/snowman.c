
struct s0 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

void** fun_26e0(void** rdi, void** rsi, void** rdx, void** rcx);

void** stdout = reinterpret_cast<void**>(0);

void fun_2500(void** rdi, void** rsi, ...);

void** fun_2490();

void fun_2650();

int64_t fun_24e0(void** rdi, void** rsi);

void print_unicode_char(void** rdi, int64_t rsi);

int32_t print_esc(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** rbp5;
    struct s0* rbx6;
    struct s0* rbx7;
    void** rax8;
    int32_t edi9;
    int64_t rcx10;
    void* r8_11;
    int64_t rdx12;
    int64_t rsi13;
    int64_t rax14;
    int32_t esi15;
    int32_t ecx16;
    int32_t eax17;
    int32_t edx18;
    void** rdi19;
    int32_t r12d20;
    void** rax21;
    void** rsi22;
    void** rdi23;
    int32_t eax24;
    void** rsi25;
    int64_t rax26;
    uint64_t rax27;
    unsigned char* rdx28;
    unsigned char* r12_29;
    uint32_t eax30;
    int64_t rax31;
    uint32_t eax32;
    void** rax33;
    int64_t r13_34;
    int1_t zf35;
    void** rax36;
    int64_t r13_37;
    int64_t r9_38;
    void* rsi39;
    void* rdi40;
    struct s0* rsi41;
    int64_t rdx42;
    int64_t rcx43;
    int32_t ecx44;
    uint32_t r9d45;
    int32_t eax46;
    int32_t edx47;
    uint32_t eax48;
    void** rdi49;
    void** rdx50;
    void** rsi51;
    void** rdi52;
    int64_t rsi53;
    void** rdi54;
    void** rax55;
    void** rsi56;
    int32_t eax57;
    int64_t rax58;
    void* r12_59;

    rbp5 = rdi;
    *reinterpret_cast<uint32_t*>(&rbx6) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx6) + 4) = 0;
    if (*reinterpret_cast<void***>(&rbx6) == 0x78) {
        rbx7 = reinterpret_cast<struct s0*>(rdi + 2);
        rax8 = fun_26e0(rdi, rsi, rdx, rcx);
        edi9 = 0;
        *reinterpret_cast<int32_t*>(&rcx10) = 0;
        r8_11 = *rax8;
        while (*reinterpret_cast<int32_t*>(&rdx12) = reinterpret_cast<signed char>(rbx7->f0), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0, *reinterpret_cast<uint32_t*>(&rsi13) = *reinterpret_cast<unsigned char*>(&rdx12), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi13) + 4) = 0, *reinterpret_cast<int32_t*>(&rax14) = *reinterpret_cast<int32_t*>(&rdx12), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r8_11) + rsi13 * 2 + 1) & 16)) {
            esi15 = static_cast<int32_t>(rax14 - 97);
            ecx16 = *reinterpret_cast<int32_t*>(&rcx10) << 4;
            if (*reinterpret_cast<unsigned char*>(&esi15) > 5) {
                eax17 = *reinterpret_cast<int32_t*>(&rax14) - 65;
                edx18 = *reinterpret_cast<int32_t*>(&rdx12) - 48;
                if (*reinterpret_cast<unsigned char*>(&eax17) <= 5) {
                    edx18 = static_cast<int32_t>(rdx12 - 55);
                }
                rbx7 = reinterpret_cast<struct s0*>(&rbx7->pad2);
                *reinterpret_cast<int32_t*>(&rcx10) = ecx16 + edx18;
                if (edi9 == 1) 
                    goto addr_370e_8;
            } else {
                rbx7 = reinterpret_cast<struct s0*>(&rbx7->pad2);
                *reinterpret_cast<int32_t*>(&rcx10) = ecx16 + (*reinterpret_cast<int32_t*>(&rdx12) - 87);
                if (edi9 == 1) 
                    goto addr_370e_8;
            }
            edi9 = 1;
        }
        if (edi9) {
            addr_370e_8:
            rdi19 = stdout;
            r12d20 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx7) - reinterpret_cast<unsigned char>(rbp5) + 0xffffffffffffffff);
            rax21 = *reinterpret_cast<void***>(rdi19 + 40);
            if (reinterpret_cast<unsigned char>(rax21) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi19 + 48))) {
                addr_3650_12:
                *reinterpret_cast<void***>(rdi19 + 40) = rax21 + 1;
                *reinterpret_cast<void***>(rax21) = *reinterpret_cast<void***>(&rcx10);
            } else {
                addr_372a_13:
                *reinterpret_cast<uint32_t*>(&rsi22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx10));
                *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                fun_2500(rdi19, rsi22);
            }
        } else {
            addr_3a58_14:
            fun_2490();
            *reinterpret_cast<int32_t*>(&rdi23) = 1;
            *reinterpret_cast<int32_t*>(&rdi23 + 4) = 0;
            fun_2650();
            goto addr_3a80_15;
        }
    } else {
        eax24 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx6 + 0xfffffffffffffff0));
        if (*reinterpret_cast<unsigned char*>(&eax24) > 7) {
            if (!*reinterpret_cast<void***>(&rbx6)) 
                goto addr_3678_18;
            *reinterpret_cast<int32_t*>(&rsi25) = reinterpret_cast<signed char>(*reinterpret_cast<void***>(&rbx6));
            *reinterpret_cast<int32_t*>(&rsi25 + 4) = 0;
            rax26 = fun_24e0("\"\\abcefnrtv", rsi25);
            if (!rax26) 
                goto addr_37a0_20; else 
                goto addr_3778_21;
        } else {
            *reinterpret_cast<int32_t*>(&rax27) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<unsigned char*>(&rax27) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(&rbx6) == 48);
            *reinterpret_cast<int32_t*>(&rcx10) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            rdx28 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + (rax27 & reinterpret_cast<unsigned char>(rsi)) + 1);
            r12_29 = rdx28 + 3;
            do {
                eax30 = *rdx28 - 48;
                if (*reinterpret_cast<unsigned char*>(&eax30) > 7) 
                    goto addr_3a48_24;
                *reinterpret_cast<int32_t*>(&rax31) = *reinterpret_cast<signed char*>(&eax30);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax31) + 4) = 0;
                ++rdx28;
                *reinterpret_cast<int32_t*>(&rcx10) = static_cast<int32_t>(rax31 + rcx10 * 8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx10) + 4) = 0;
            } while (rdx28 != r12_29);
            goto addr_3634_26;
        }
    }
    addr_365a_27:
    return r12d20;
    addr_37a0_20:
    eax32 = *reinterpret_cast<uint32_t*>(&rbx6) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&eax32) != 85) {
        addr_3678_18:
        rdi23 = stdout;
        rax33 = *reinterpret_cast<void***>(rdi23 + 40);
        if (reinterpret_cast<unsigned char>(rax33) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi23 + 48))) {
            addr_3a80_15:
            fun_2500(rdi23, 92, rdi23, 92);
        } else {
            *reinterpret_cast<void***>(rdi23 + 40) = rax33 + 1;
            *reinterpret_cast<void***>(rax33) = reinterpret_cast<void**>(92);
        }
    } else {
        *reinterpret_cast<int32_t*>(&r13_34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        zf35 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rbx6) == 0x75);
        rbx6 = reinterpret_cast<struct s0*>(rbp5 + 2);
        *reinterpret_cast<unsigned char*>(&r13_34) = reinterpret_cast<uint1_t>(!zf35);
        rax36 = fun_26e0("\"\\abcefnrtv", rsi25, rdx, rcx);
        *reinterpret_cast<int32_t*>(&r13_37) = static_cast<int32_t>(r13_34 * 4 + 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_37) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r9_38) = 0;
        *reinterpret_cast<int32_t*>(&rsi39) = static_cast<int32_t>(r13_37 + 2);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi39) + 4) = 0;
        rdi40 = *rax36;
        rsi41 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsi39) + reinterpret_cast<unsigned char>(rbp5));
        while (*reinterpret_cast<int32_t*>(&rdx42) = reinterpret_cast<signed char>(rbx6->f0), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx42) + 4) = 0, *reinterpret_cast<uint32_t*>(&rcx43) = *reinterpret_cast<unsigned char*>(&rdx42), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx43) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi40) + rcx43 * 2 + 1) & 16)) {
            ecx44 = static_cast<int32_t>(rdx42 - 97);
            r9d45 = *reinterpret_cast<uint32_t*>(&r9_38) << 4;
            if (*reinterpret_cast<unsigned char*>(&ecx44) <= 5) {
                rbx6 = reinterpret_cast<struct s0*>(&rbx6->pad2);
                *reinterpret_cast<uint32_t*>(&r9_38) = r9d45 + (*reinterpret_cast<int32_t*>(&rdx42) - 87);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_38) + 4) = 0;
                if (rbx6 == rsi41) 
                    goto addr_3828_33;
            } else {
                eax46 = *reinterpret_cast<int32_t*>(&rdx42) - 65;
                edx47 = *reinterpret_cast<int32_t*>(&rdx42) - 48;
                if (*reinterpret_cast<unsigned char*>(&eax46) <= 5) {
                    edx47 = static_cast<int32_t>(rdx42 - 55);
                }
                rbx6 = reinterpret_cast<struct s0*>(&rbx6->pad2);
                *reinterpret_cast<uint32_t*>(&r9_38) = r9d45 + edx47;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_38) + 4) = 0;
                if (rbx6 == rsi41) 
                    goto addr_3828_33;
            }
        }
        goto addr_3a58_14;
        addr_3828_33:
        if (*reinterpret_cast<uint32_t*>(&r9_38) > 0x9f) 
            goto addr_3870_37;
        if (*reinterpret_cast<uint32_t*>(&r9_38) == 36) 
            goto addr_3870_37; else 
            goto addr_3837_39;
    }
    eax48 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 1));
    r12d20 = 0;
    if (*reinterpret_cast<void***>(&eax48)) {
        rdi49 = stdout;
        rdx50 = *reinterpret_cast<void***>(rdi49 + 40);
        if (reinterpret_cast<unsigned char>(rdx50) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi49 + 48))) {
            *reinterpret_cast<uint32_t*>(&rsi51) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax48));
            *reinterpret_cast<int32_t*>(&rsi51 + 4) = 0;
            fun_2500(rdi49, rsi51);
        } else {
            *reinterpret_cast<void***>(rdi49 + 40) = rdx50 + 1;
            *reinterpret_cast<void***>(rdx50) = *reinterpret_cast<void***>(&eax48);
        }
        r12d20 = 1;
        goto addr_365a_27;
    }
    addr_3870_37:
    if (static_cast<uint32_t>(r9_38 - 0xd800) > 0x7ff) {
        addr_3842_45:
        rdi52 = stdout;
        *reinterpret_cast<uint32_t*>(&rsi53) = *reinterpret_cast<uint32_t*>(&r9_38);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi53) + 4) = 0;
        print_unicode_char(rdi52, rsi53);
        return static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx6) - reinterpret_cast<unsigned char>(rbp5) + 0xffffffffffffffff);
    } else {
        addr_387e_46:
        fun_2490();
        fun_2650();
    }
    rdi54 = stdout;
    r12d20 = 1;
    rax55 = *reinterpret_cast<void***>(rdi54 + 40);
    if (reinterpret_cast<unsigned char>(rax55) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi54 + 48))) {
        *reinterpret_cast<uint32_t*>(&rsi56) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx6));
        *reinterpret_cast<int32_t*>(&rsi56 + 4) = 0;
        fun_2500(rdi54, rsi56, rdi54, rsi56);
        goto addr_365a_27;
    } else {
        *reinterpret_cast<void***>(rdi54 + 40) = rax55 + 1;
        *reinterpret_cast<void***>(rax55) = *reinterpret_cast<void***>(&rbx6);
        goto addr_365a_27;
    }
    addr_3837_39:
    if ((*reinterpret_cast<uint32_t*>(&r9_38) & 0xffffffdf) != 64) 
        goto addr_387e_46; else 
        goto addr_3842_45;
    addr_3778_21:
    eax57 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx6) + 0xffffffffffffff9f);
    if (*reinterpret_cast<unsigned char*>(&eax57) <= 21) {
        *reinterpret_cast<uint32_t*>(&rax58) = *reinterpret_cast<unsigned char*>(&eax57);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax58) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xa930 + rax58 * 4) + 0xa930;
    }
    addr_3a48_24:
    r12_29 = rdx28;
    addr_3634_26:
    rdi19 = stdout;
    r12_59 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_29) - reinterpret_cast<unsigned char>(rbp5));
    r12d20 = *reinterpret_cast<int32_t*>(&r12_59) - 1;
    rax21 = *reinterpret_cast<void***>(rdi19 + 40);
    if (reinterpret_cast<unsigned char>(rax21) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi19 + 48))) 
        goto addr_372a_13; else 
        goto addr_3650_12;
}

void** g28;

int32_t* fun_23f0();

void** fun_23d0(void** rdi, void* rsi);

void verify_numeric(void** rdi, void** rsi);

uint64_t fun_24a0();

void fun_24c0();

void** fun_24b0(void** rdi, ...);

void** rpl_mbrtowc(void* rdi, void** rsi, void** rdx, void** rcx);

unsigned char posixly_correct = 0;

void** vstrtoimax(void** rdi) {
    void* rsp2;
    void** rax3;
    uint32_t eax4;
    void** r12_5;
    int32_t* rax6;
    void** rax7;
    void** v8;
    void** rbx9;
    uint64_t rax10;
    uint32_t eax11;
    void* rax12;
    void** rax13;
    void* rsp14;
    void** rax15;
    int32_t v16;
    int1_t zf17;

    rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 32);
    rax3 = g28;
    eax4 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (*reinterpret_cast<signed char*>(&eax4) != 34 && *reinterpret_cast<signed char*>(&eax4) != 39 || (*reinterpret_cast<uint32_t*>(&r12_5) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 1)), *reinterpret_cast<int32_t*>(&r12_5 + 4) = 0, !*reinterpret_cast<signed char*>(&r12_5))) {
        rax6 = fun_23f0();
        *rax6 = 0;
        rax7 = fun_23d0(rdi, reinterpret_cast<int64_t>(rsp2) - 8 + 8 + 8);
        r12_5 = rax7;
        verify_numeric(rdi, v8);
        goto addr_3c3a_3;
    } else {
        rbx9 = rdi + 1;
        rax10 = fun_24a0();
        if (rax10 <= 1) {
            eax11 = *reinterpret_cast<unsigned char*>(rdi + 2);
        } else {
            if (!*reinterpret_cast<unsigned char*>(rdi + 2)) {
                addr_3c3a_3:
                rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
                if (rax12) {
                    fun_24c0();
                } else {
                    return r12_5;
                }
            } else {
                rax13 = fun_24b0(rbx9);
                rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8 - 8 + 8);
                rax15 = rpl_mbrtowc(reinterpret_cast<int64_t>(rsp14) + 8, rbx9, rax13, reinterpret_cast<int64_t>(rsp14) + 16);
                if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax15) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax15 == 0))) {
                    r12_5 = reinterpret_cast<void**>(static_cast<int64_t>(v16));
                    rbx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rax15));
                }
                eax11 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx9 + 1));
            }
        }
        if (*reinterpret_cast<signed char*>(&eax11) && (zf17 = posixly_correct == 0, zf17)) {
            fun_2490();
            fun_2650();
            goto addr_3c3a_3;
        }
    }
}

int64_t quote(void** rdi, void** rsi, ...);

int32_t exit_status = 0;

void verify_numeric(void** rdi, void** rsi) {
    int32_t* rax3;

    rax3 = fun_23f0();
    if (*rax3) {
        quote(rdi, rsi);
        fun_2650();
        exit_status = 1;
        return;
    } else {
        if (*reinterpret_cast<void***>(rsi)) {
            if (rsi == rdi) {
                quote(rsi, rsi);
            } else {
                quote(rdi, rsi);
            }
            fun_2490();
            fun_2650();
            exit_status = 1;
        }
        return;
    }
}

int64_t fun_23e0(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24a0();
    if (r8d > 10) {
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xabe0 + rax11 * 4) + 0xabe0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_2530();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_23c0(void** rdi, ...);

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x58ef;
    rax8 = fun_23f0();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
        fun_23e0(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xf070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x95e1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x597b;
            fun_2530();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xf0e0) {
                fun_23c0(r14_19, r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x5a0a);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_24c0();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xf080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

void** locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    void** rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4))) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4 + 1))) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (*reinterpret_cast<unsigned char*>(rax4 + 2) == 49 && (*reinterpret_cast<unsigned char*>(rax4 + 3) == 56 && (*reinterpret_cast<signed char*>(rax4 + 4) == 48 && (*reinterpret_cast<signed char*>(rax4 + 5) == 51 && (*reinterpret_cast<signed char*>(rax4 + 6) == 48 && !*reinterpret_cast<signed char*>(rax4 + 7)))))))) {
            rax7 = reinterpret_cast<void**>(0xab7b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xab74);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax4 + 1))) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(rax4 + 2)) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (*reinterpret_cast<unsigned char*>(rax4 + 3) == 45 && (*reinterpret_cast<signed char*>(rax4 + 4) == 56 && !*reinterpret_cast<signed char*>(rax4 + 5))))) {
            rax10 = reinterpret_cast<void**>(0xab7f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xab70);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gee18 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gee18;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2393() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t getenv = 0x2030;

void fun_23a3() {
    __asm__("cli ");
    goto getenv;
}

int64_t __snprintf_chk = 0x2040;

void fun_23b3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2050;

void fun_23c3() {
    __asm__("cli ");
    goto free;
}

int64_t strtoimax = 0x2060;

void fun_23d3() {
    __asm__("cli ");
    goto strtoimax;
}

int64_t abort = 0x2070;

void fun_23e3() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2080;

void fun_23f3() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2090;

void fun_2403() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20a0;

void fun_2413() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20b0;

void fun_2423() {
    __asm__("cli ");
    goto __fpending;
}

int64_t iconv = 0x20c0;

void fun_2433() {
    __asm__("cli ");
    goto iconv;
}

int64_t ferror = 0x20d0;

void fun_2443() {
    __asm__("cli ");
    goto ferror;
}

int64_t reallocarray = 0x20e0;

void fun_2453() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t textdomain = 0x20f0;

void fun_2463() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2100;

void fun_2473() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2110;

void fun_2483() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2120;

void fun_2493() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2130;

void fun_24a3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2140;

void fun_24b3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2150;

void fun_24c3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t mbrtowc = 0x2160;

void fun_24d3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2170;

void fun_24e3() {
    __asm__("cli ");
    goto strchr;
}

int64_t newlocale = 0x2180;

void fun_24f3() {
    __asm__("cli ");
    goto newlocale;
}

int64_t __overflow = 0x2190;

void fun_2503() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21a0;

void fun_2513() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21b0;

void fun_2523() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x21c0;

void fun_2533() {
    __asm__("cli ");
    goto memset;
}

int64_t memcmp = 0x21d0;

void fun_2543() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21e0;

void fun_2553() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x21f0;

void fun_2563() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2200;

void fun_2573() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2210;

void fun_2583() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2220;

void fun_2593() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_25a3() {
    __asm__("cli ");
    goto fileno;
}

int64_t uselocale = 0x2240;

void fun_25b3() {
    __asm__("cli ");
    goto uselocale;
}

int64_t malloc = 0x2250;

void fun_25c3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2260;

void fun_25d3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2270;

void fun_25e3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2280;

void fun_25f3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2290;

void fun_2603() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22a0;

void fun_2613() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22b0;

void fun_2623() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x22c0;

void fun_2633() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t strtold = 0x22d0;

void fun_2643() {
    __asm__("cli ");
    goto strtold;
}

int64_t error = 0x22e0;

void fun_2653() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22f0;

void fun_2663() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x2300;

void fun_2673() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2310;

void fun_2683() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2320;

void fun_2693() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2330;

void fun_26a3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2340;

void fun_26b3() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2350;

void fun_26c3() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2360;

void fun_26d3() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2370;

void fun_26e3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t iconv_open = 0x2380;

void fun_26f3() {
    __asm__("cli ");
    goto iconv_open;
}

struct s3 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

void set_program_name(int64_t rdi);

void** fun_2610(int64_t rdi, ...);

void fun_2480(int64_t rdi, int64_t rsi);

void fun_2460(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int64_t fun_23a0(int64_t rdi, int64_t rsi);

int32_t fun_2570(void** rdi, void** rsi, ...);

void usage();

int64_t Version = 0xab10;

void version_etc(void** rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8);

struct s4 {
    signed char[1] pad1;
    void** f1;
};

void** quotearg_style(int64_t rdi, void** rsi, void** rdx);

void fun_2550(void** rdi, void** rsi, void** rdx, void** rcx, ...);

void** xmalloc(uint64_t rdi);

void* fun_2630(void** rdi, void** rsi, void** rdx);

int64_t fun_2753(int32_t edi, struct s3* rsi) {
    void** rbp3;
    struct s3* rbx4;
    int64_t rdi5;
    void** rax6;
    void** v7;
    int64_t rax8;
    void*** rsp9;
    void** rdi10;
    void** rsi11;
    int32_t eax12;
    void** rax13;
    void** rdx14;
    void** r13_15;
    int32_t eax16;
    int32_t eax17;
    int32_t r12d18;
    int64_t rcx19;
    void** rdi20;
    int32_t eax21;
    void** v22;
    int32_t eax23;
    int32_t v24;
    void** v25;
    uint32_t eax26;
    int32_t r15d27;
    void** v28;
    int64_t rbx29;
    void** rdi30;
    void** rax31;
    void** rbx32;
    uint32_t eax33;
    void** rdi34;
    void** rax35;
    void** rcx36;
    int32_t eax37;
    struct s4* rax38;
    int64_t rcx39;
    int32_t eax40;
    void** rbp41;
    void** rsi42;
    void** rax43;
    void** rdi44;
    int32_t eax45;
    void** rcx46;
    void** rax47;
    void** rdx48;
    uint32_t ecx49;
    void** rdi50;
    void** rax51;
    void** rax52;
    void** rsi53;
    uint32_t ecx54;
    void** rcx55;
    void** rdi56;
    void** rax57;
    uint32_t eax58;
    int64_t rax59;
    int64_t rax60;
    int32_t ecx61;
    void** r10_62;
    void** rcx63;
    uint64_t rcx64;
    void** rax65;
    void* rax66;
    void* rsp67;
    void** r8_68;
    void* rsi69;
    uint32_t edi70;
    void** r10_71;
    uint32_t eax72;
    uint32_t r11d73;
    uint32_t ebx74;
    int32_t eax75;
    void* rax76;
    int64_t rax77;
    void** rdi78;
    void** rdi79;
    int64_t rax80;
    int64_t rbx81;
    void** rdi82;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rbp3) = edi;
    *reinterpret_cast<int32_t*>(&rbp3 + 4) = 0;
    rbx4 = rsi;
    rdi5 = rsi->f0;
    rax6 = g28;
    v7 = rax6;
    set_program_name(rdi5);
    fun_2610(6, 6);
    fun_2480("coreutils", "/usr/local/share/locale");
    fun_2460("coreutils", "/usr/local/share/locale");
    atexit(0x41d0, "/usr/local/share/locale");
    exit_status = 0;
    rax8 = fun_23a0("POSIXLY_CORRECT", "/usr/local/share/locale");
    rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x178 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    posixly_correct = reinterpret_cast<uint1_t>(!!rax8);
    if (*reinterpret_cast<int32_t*>(&rbp3) != 2) {
        if (*reinterpret_cast<int32_t*>(&rbp3) > 1) {
            addr_2819_3:
            rdi10 = rbx4->f8;
            rsi11 = reinterpret_cast<void**>("--");
            eax12 = fun_2570(rdi10, "--", rdi10, "--");
            rsp9 = rsp9 - 8 + 8;
            if (!eax12) {
                *reinterpret_cast<int32_t*>(&rbp3) = *reinterpret_cast<int32_t*>(&rbp3) - 1;
                *reinterpret_cast<int32_t*>(&rbp3 + 4) = 0;
                rbx4 = reinterpret_cast<struct s3*>(&rbx4->f8);
                goto addr_2834_5;
            }
        } else {
            addr_27ee_6:
            rax13 = fun_2490();
            rdx14 = rax13;
            fun_2650();
            usage();
            rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
            goto addr_2819_3;
        }
    }
    r13_15 = rbx4->f8;
    eax16 = fun_2570(r13_15, "--help");
    if (!eax16) {
        addr_34de_8:
        usage();
    } else {
        eax17 = fun_2570(r13_15, "--version");
        r12d18 = eax17;
        if (!eax17) {
            rcx19 = Version;
            rdi20 = stdout;
            rsi11 = reinterpret_cast<void**>("printf");
            version_etc(rdi20, "printf", "GNU coreutils", rcx19, "David MacKenzie");
        } else {
            rsi11 = reinterpret_cast<void**>("--");
            eax21 = fun_2570(r13_15, "--");
            rsp9 = rsp9 - 8 + 8 - 8 + 8 - 8 + 8;
            if (eax21) {
                addr_2834_5:
                v22 = rbx4->f8;
                eax23 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbp3 + 0xfffffffffffffffe));
                rbp3 = v22;
                v24 = eax23;
                v25 = reinterpret_cast<void**>(&rbx4->f10);
                eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3));
                if (!*reinterpret_cast<void***>(&eax26)) {
                    addr_291f_12:
                    r15d27 = v24;
                    goto addr_2924_13;
                } else {
                    do {
                        rsi11 = v25;
                        r15d27 = v24;
                        v28 = rsi11;
                        do {
                            addr_28bf_16:
                            if (*reinterpret_cast<void***>(&eax26) == 37) {
                                *reinterpret_cast<uint32_t*>(&rbx29) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3 + 1));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                                r13_15 = rbp3 + 1;
                                if (*reinterpret_cast<void***>(&rbx29) == 37) {
                                    rdi30 = stdout;
                                    rax31 = *reinterpret_cast<void***>(rdi30 + 40);
                                    if (reinterpret_cast<unsigned char>(rax31) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi30 + 48))) {
                                        *reinterpret_cast<uint32_t*>(&rsi11) = 37;
                                        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                        fun_2500(rdi30, 37, rdi30, 37);
                                        rsp9 = rsp9 - 8 + 8;
                                        continue;
                                    } else {
                                        rdx14 = rax31 + 1;
                                        *reinterpret_cast<void***>(rdi30 + 40) = rdx14;
                                        *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(37);
                                        continue;
                                    }
                                } else {
                                    if (*reinterpret_cast<void***>(&rbx29) == 98) {
                                        if (!r15d27) {
                                            addr_29b4_23:
                                            eax26 = *reinterpret_cast<unsigned char*>(rbp3 + 2);
                                            rdx14 = rbp3 + 2;
                                            if (!*reinterpret_cast<void***>(&eax26)) 
                                                goto addr_295d_24;
                                        } else {
                                            rbx32 = *reinterpret_cast<void***>(v28);
                                            eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx32));
                                            if (*reinterpret_cast<void***>(&eax33)) {
                                                while (1) {
                                                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax33) == 92)) {
                                                        rdi34 = stdout;
                                                        rdx14 = *reinterpret_cast<void***>(rdi34 + 40);
                                                        if (reinterpret_cast<unsigned char>(rdx14) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi34 + 48))) {
                                                            *reinterpret_cast<uint32_t*>(&rsi11) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax33));
                                                            *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                                            fun_2500(rdi34, rsi11, rdi34, rsi11);
                                                            rsp9 = rsp9 - 8 + 8;
                                                            rax35 = rbx32;
                                                        } else {
                                                            rcx36 = rdx14 + 1;
                                                            *reinterpret_cast<void***>(rdi34 + 40) = rcx36;
                                                            *reinterpret_cast<void***>(rdx14) = *reinterpret_cast<void***>(&eax33);
                                                            rax35 = rbx32;
                                                        }
                                                        rbx32 = rax35 + 1;
                                                        eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax35 + 1));
                                                        if (!*reinterpret_cast<void***>(&eax33)) 
                                                            break;
                                                    } else {
                                                        *reinterpret_cast<uint32_t*>(&rsi11) = 1;
                                                        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                                        eax37 = print_esc(rbx32, 1, rdx14, rcx36);
                                                        rsp9 = rsp9 - 8 + 8;
                                                        rax38 = reinterpret_cast<struct s4*>(static_cast<int64_t>(eax37) + reinterpret_cast<unsigned char>(rbx32));
                                                        rbx32 = reinterpret_cast<void**>(&rax38->f1);
                                                        eax33 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax38->f1));
                                                        if (!*reinterpret_cast<void***>(&eax33)) 
                                                            break;
                                                    }
                                                }
                                            }
                                            v28 = v28 + 8;
                                            --r15d27;
                                            continue;
                                        }
                                    } else {
                                        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rbx29) == 0x71)) {
                                            *reinterpret_cast<int32_t*>(&rcx39) = 32;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx39) + 4) = 0;
                                            *reinterpret_cast<uint32_t*>(&rsi11) = 1;
                                            *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                            while (rcx39) {
                                                --rcx39;
                                                rsi11 = rsi11 + 8;
                                            }
                                            eax40 = static_cast<int32_t>(rbx29 - 32);
                                            if (*reinterpret_cast<unsigned char*>(&eax40) <= 41) 
                                                goto addr_2b4f_40;
                                            if (!*reinterpret_cast<signed char*>(&rcx39)) 
                                                goto addr_2b7c_42; else 
                                                goto addr_2b64_43;
                                        } else {
                                            if (!r15d27) 
                                                goto addr_29b4_23;
                                            rbp41 = stdout;
                                            --r15d27;
                                            rsi42 = *reinterpret_cast<void***>(v28);
                                            rax43 = quotearg_style(3, rsi42, rdx14);
                                            rsi11 = rbp41;
                                            fun_2550(rax43, rsi11, rdx14, rcx36, rax43, rsi11, rdx14, rcx36);
                                            rsp9 = rsp9 - 8 + 8 - 8 + 8;
                                            v28 = v28 + 8;
                                            continue;
                                        }
                                    }
                                }
                            } else {
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&eax26) == 92)) {
                                    rdi44 = stdout;
                                    rdx14 = *reinterpret_cast<void***>(rdi44 + 40);
                                    if (reinterpret_cast<unsigned char>(rdx14) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi44 + 48))) {
                                        *reinterpret_cast<uint32_t*>(&rsi11) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&eax26));
                                        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                        r13_15 = rbp3;
                                        fun_2500(rdi44, rsi11, rdi44, rsi11);
                                        rsp9 = rsp9 - 8 + 8;
                                        continue;
                                    } else {
                                        rcx36 = rdx14 + 1;
                                        r13_15 = rbp3;
                                        *reinterpret_cast<void***>(rdi44 + 40) = rcx36;
                                        *reinterpret_cast<void***>(rdx14) = *reinterpret_cast<void***>(&eax26);
                                        continue;
                                    }
                                } else {
                                    *reinterpret_cast<uint32_t*>(&rsi11) = 0;
                                    *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
                                    eax45 = print_esc(rbp3, 0, rdx14, rcx36);
                                    rsp9 = rsp9 - 8 + 8;
                                    r13_15 = reinterpret_cast<void**>(static_cast<int64_t>(eax45) + reinterpret_cast<unsigned char>(rbp3));
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_15 + 1));
                                    rbp3 = r13_15 + 1;
                                    if (*reinterpret_cast<void***>(&eax26)) 
                                        goto addr_28bf_16; else 
                                        break;
                                }
                            }
                            rbp3 = rdx14;
                            r15d27 = 0;
                            goto addr_28bf_16;
                            addr_2b7c_42:
                            if (!1) {
                            }
                            if (!1) {
                            }
                            if (1) {
                                if (!0) {
                                    goto addr_2bd7_58;
                                }
                            } else {
                                if (1) 
                                    goto addr_335b_60;
                            }
                            if (0) {
                                addr_335b_60:
                                goto addr_2bd7_58;
                            } else {
                                addr_2bd7_58:
                                rcx46 = r13_15 + 1;
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rbx29) == 42)) {
                                    rax47 = r13_15;
                                    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rbx29) - 48) > 9) {
                                        rdx48 = rsi11;
                                    } else {
                                        do {
                                            ecx49 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax47 + 1))));
                                            ++rax47;
                                            *reinterpret_cast<uint32_t*>(&rbx29) = ecx49;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                                            rdx48 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax47) + reinterpret_cast<unsigned char>(rsi11)) - reinterpret_cast<unsigned char>(r13_15));
                                        } while (ecx49 - 48 <= 9);
                                        r13_15 = rax47;
                                        if (*reinterpret_cast<void***>(&rbx29) == 46) 
                                            goto addr_2c0f_67; else 
                                            goto addr_3022_68;
                                    }
                                } else {
                                    rdx48 = rsi11 + 1;
                                    if (r15d27) {
                                        rdi50 = *reinterpret_cast<void***>(v28);
                                        rax51 = vstrtoimax(rdi50);
                                        rsp9 = rsp9 - 8 + 8;
                                        rsi11 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax51 + 0x80000000) >> 32);
                                        if (rsi11) 
                                            goto addr_3440_71;
                                        rdx48 = rdx48;
                                        --r15d27;
                                        v28 = v28 + 8;
                                        *reinterpret_cast<uint32_t*>(&rbx29) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_15 + 1));
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                                        r13_15 = rcx46;
                                    } else {
                                        *reinterpret_cast<uint32_t*>(&rbx29) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_15 + 1));
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                                        r13_15 = rcx46;
                                    }
                                }
                            }
                            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rbx29) == 46)) {
                                addr_3022_68:
                            } else {
                                addr_2c0f_67:
                                *reinterpret_cast<uint32_t*>(&rbx29) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_15 + 1));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rbx29) == 42)) {
                                    rax52 = r13_15 + 1;
                                    rsi53 = rdx48 + 1;
                                    if (reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(&rbx29) - 48) <= 9) {
                                        do {
                                            ecx54 = reinterpret_cast<uint32_t>(static_cast<int32_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax52 + 1))));
                                            ++rax52;
                                            *reinterpret_cast<uint32_t*>(&rbx29) = ecx54;
                                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                                            rsi53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax52) + reinterpret_cast<unsigned char>(rdx48)) - reinterpret_cast<unsigned char>(r13_15));
                                        } while (ecx54 - 48 <= 9);
                                    }
                                    rdx48 = rsi53;
                                    r13_15 = rax52;
                                } else {
                                    rcx55 = r13_15 + 2;
                                    rdx48 = rdx48 + 2;
                                    if (!r15d27) {
                                        *reinterpret_cast<uint32_t*>(&rbx29) = *reinterpret_cast<unsigned char*>(r13_15 + 2);
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                                        r13_15 = rcx55;
                                    } else {
                                        rdi56 = *reinterpret_cast<void***>(v28);
                                        rax57 = vstrtoimax(rdi56);
                                        rsp9 = rsp9 - 8 + 8;
                                        rdx48 = rdx48;
                                        if (reinterpret_cast<signed char>(rax57) >= reinterpret_cast<signed char>(0)) {
                                            if (reinterpret_cast<signed char>(rax57) > reinterpret_cast<signed char>(0x7fffffff)) 
                                                goto addr_347b_83;
                                        }
                                        v28 = v28 + 8;
                                        *reinterpret_cast<uint32_t*>(&rbx29) = *reinterpret_cast<unsigned char*>(r13_15 + 2);
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                                        --r15d27;
                                        r13_15 = rcx55;
                                    }
                                }
                            }
                            while ((eax58 = *reinterpret_cast<uint32_t*>(&rbx29) & 0xffffffdf, *reinterpret_cast<signed char*>(&eax58) == 76) || (*reinterpret_cast<int32_t*>(&rax59) = static_cast<int32_t>(rbx29 - 0x68), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax59) + 4) = 0, *reinterpret_cast<unsigned char*>(&rax59) <= 18) && static_cast<int1_t>(0x41005 >> rax59)) {
                                ++r13_15;
                                *reinterpret_cast<uint32_t*>(&rbx29) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_15));
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx29) + 4) = 0;
                            }
                            *reinterpret_cast<uint32_t*>(&rax60) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rbx29));
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax60) + 4) = 0;
                            if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsp9) + rax60 + 96)) 
                                goto addr_34b1_89;
                            if (r15d27) {
                                --r15d27;
                                v28 = v28 + 8;
                            }
                            ecx61 = static_cast<int32_t>(rbx29 - 65);
                            if (*reinterpret_cast<unsigned char*>(&ecx61) > 55) {
                                r10_62 = rbp3;
                                *reinterpret_cast<int32_t*>(&rcx63) = 0;
                                *reinterpret_cast<int32_t*>(&rcx63 + 4) = 0;
                            } else {
                                rcx64 = 1 << *reinterpret_cast<unsigned char*>(&ecx61);
                                if (rcx64 & 0x7100000071) {
                                    *reinterpret_cast<int32_t*>(&rcx63) = 1;
                                    *reinterpret_cast<int32_t*>(&rcx63 + 4) = 0;
                                    r10_62 = reinterpret_cast<void**>("L");
                                } else {
                                    r10_62 = rbp3;
                                    rcx63 = reinterpret_cast<void**>(rcx64 & 0x90410800800000);
                                    if (rcx63) {
                                        *reinterpret_cast<int32_t*>(&rcx63) = 1;
                                        *reinterpret_cast<int32_t*>(&rcx63 + 4) = 0;
                                        r10_62 = reinterpret_cast<void**>("ld");
                                    }
                                }
                            }
                            rax65 = xmalloc(reinterpret_cast<unsigned char>(rdx48) + reinterpret_cast<unsigned char>(rcx63) + 2);
                            rdx14 = rdx48;
                            rax66 = fun_2630(rax65, rbp3, rdx14);
                            rsp67 = reinterpret_cast<void*>(rsp9 - 8 + 8 - 8 + 8);
                            rcx36 = rcx63;
                            r8_68 = rax65;
                            rsi69 = rax66;
                            edi70 = *reinterpret_cast<uint32_t*>(&rcx36);
                            if (*reinterpret_cast<uint32_t*>(&rcx36)) {
                                r10_71 = r10_62;
                                eax72 = 0;
                                do {
                                    *reinterpret_cast<uint32_t*>(&rdx14) = eax72;
                                    *reinterpret_cast<int32_t*>(&rdx14 + 4) = 0;
                                    ++eax72;
                                    r11d73 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r10_71) + reinterpret_cast<unsigned char>(rdx14));
                                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsi69) + reinterpret_cast<unsigned char>(rdx14)) = *reinterpret_cast<signed char*>(&r11d73);
                                } while (eax72 < edi70);
                            }
                            rsi11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsi69) + reinterpret_cast<unsigned char>(rcx36));
                            *reinterpret_cast<void***>(rsi11) = *reinterpret_cast<void***>(&rbx29);
                            ebx74 = *reinterpret_cast<uint32_t*>(&rbx29) - 65;
                            *reinterpret_cast<void***>(rsi11 + 1) = reinterpret_cast<void**>(0);
                            if (*reinterpret_cast<unsigned char*>(&ebx74) <= 55) 
                                goto addr_2d8c_102;
                            fun_23c0(r8_68, r8_68);
                            rsp9 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp67) - 8 + 8);
                            continue;
                            addr_2b64_43:
                            goto addr_2b7c_42;
                            eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_15 + 1));
                            rbp3 = r13_15 + 1;
                        } while (*reinterpret_cast<void***>(&eax26));
                        eax75 = v24 - r15d27;
                        rdx14 = reinterpret_cast<void**>(static_cast<int64_t>(eax75));
                        rsi11 = v25 + reinterpret_cast<unsigned char>(rdx14) * 8;
                        v25 = rsi11;
                        if (reinterpret_cast<uint1_t>(eax75 < 0) | reinterpret_cast<uint1_t>(eax75 == 0)) 
                            goto addr_2924_13;
                        if (reinterpret_cast<uint1_t>(r15d27 < 0) | reinterpret_cast<uint1_t>(r15d27 == 0)) 
                            goto addr_2924_13;
                        rbp3 = v22;
                        v24 = r15d27;
                        eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3));
                    } while (*reinterpret_cast<void***>(&eax26));
                    goto addr_291f_12;
                }
            } else {
                goto addr_27ee_6;
            }
        }
    }
    addr_2964_109:
    rax76 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (!rax76) {
        *reinterpret_cast<int32_t*>(&rax77) = r12d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax77) + 4) = 0;
        return rax77;
    }
    addr_3476_111:
    fun_24c0();
    addr_347b_83:
    rdi78 = *reinterpret_cast<void***>(v28);
    quote(rdi78, rsi11, rdi78, rsi11);
    fun_2490();
    fun_2650();
    addr_34b1_89:
    fun_2490();
    fun_2650();
    goto addr_34de_8;
    addr_2924_13:
    if (r15d27) {
        rdi79 = *reinterpret_cast<void***>(v25);
        quote(rdi79, rsi11, rdi79, rsi11);
        fun_2490();
        *reinterpret_cast<uint32_t*>(&rsi11) = 0;
        *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
        fun_2650();
    }
    addr_295d_24:
    r12d18 = exit_status;
    goto addr_2964_109;
    addr_2b4f_40:
    *reinterpret_cast<uint32_t*>(&rax80) = *reinterpret_cast<unsigned char*>(&eax40);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax80) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa988 + rax80 * 4) + 0xa988;
    addr_2d8c_102:
    *reinterpret_cast<uint32_t*>(&rbx81) = *reinterpret_cast<unsigned char*>(&ebx74);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx81) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xaa30 + rbx81 * 4) + 0xaa30;
    addr_3440_71:
    rdi82 = *reinterpret_cast<void***>(v28);
    quote(rdi82, rsi11);
    fun_2490();
    *reinterpret_cast<uint32_t*>(&rsi11) = 0;
    *reinterpret_cast<int32_t*>(&rsi11 + 4) = 0;
    fun_2650();
    goto addr_3476_111;
}

int64_t __libc_start_main = 0;

void fun_34f3() {
    __asm__("cli ");
    __libc_start_main(0x2750, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xf008;

void fun_2390(int64_t rdi);

int64_t fun_3593() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2390(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_35d3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2620(int64_t rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_2400(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void fun_2690();

void** stderr = reinterpret_cast<void**>(0);

void fun_26b0(void** rdi, int64_t rsi, void** rdx, ...);

void fun_3d03(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** r12_6;
    void** rax7;
    void** r12_8;
    void** rax9;
    void** r12_10;
    void** rax11;
    void** r12_12;
    void** rax13;
    void** r12_14;
    void** rax15;
    void** r12_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** rax20;
    int32_t eax21;
    void** r13_22;
    void** rax23;
    void** rax24;
    int32_t eax25;
    void** rax26;
    void** rax27;
    void** rax28;
    int32_t eax29;
    void** rax30;
    void** r15_31;
    void** rax32;
    void** rax33;
    void** rax34;
    void** rdi35;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2490();
            fun_2620(1, rax5, r12_2, r12_2);
            r12_6 = stdout;
            rax7 = fun_2490();
            fun_2550(rax7, r12_6, 5, r12_2, rax7, r12_6, 5, r12_2);
            r12_8 = stdout;
            rax9 = fun_2490();
            fun_2550(rax9, r12_8, 5, r12_2, rax9, r12_8, 5, r12_2);
            r12_10 = stdout;
            rax11 = fun_2490();
            fun_2550(rax11, r12_10, 5, r12_2, rax11, r12_10, 5, r12_2);
            r12_12 = stdout;
            rax13 = fun_2490();
            fun_2550(rax13, r12_12, 5, r12_2, rax13, r12_12, 5, r12_2);
            r12_14 = stdout;
            rax15 = fun_2490();
            fun_2550(rax15, r12_14, 5, r12_2, rax15, r12_14, 5, r12_2);
            r12_16 = stdout;
            rax17 = fun_2490();
            fun_2550(rax17, r12_16, 5, r12_2, rax17, r12_16, 5, r12_2);
            r12_18 = stdout;
            rax19 = fun_2490();
            fun_2550(rax19, r12_18, 5, r12_2, rax19, r12_18, 5, r12_2);
            rax20 = fun_2490();
            fun_2620(1, rax20, "printf", r12_2);
            do {
                if (1) 
                    break;
                eax21 = fun_2570("printf", 0, "printf", 0);
            } while (eax21);
            r13_22 = v4;
            if (!r13_22) {
                rax23 = fun_2490();
                fun_2620(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax24 = fun_2610(5);
                if (!rax24 || (eax25 = fun_2400(rax24, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax25)) {
                    rax26 = fun_2490();
                    r13_22 = reinterpret_cast<void**>("printf");
                    fun_2620(1, rax26, "https://www.gnu.org/software/coreutils/", "printf");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_22 = reinterpret_cast<void**>("printf");
                    goto addr_40d0_9;
                }
            } else {
                rax27 = fun_2490();
                fun_2620(1, rax27, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax28 = fun_2610(5);
                if (!rax28 || (eax29 = fun_2400(rax28, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax29)) {
                    addr_3fd6_11:
                    rax30 = fun_2490();
                    fun_2620(1, rax30, "https://www.gnu.org/software/coreutils/", "printf");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_22 == "printf")) {
                        r12_2 = reinterpret_cast<void**>(0xb052);
                    }
                } else {
                    addr_40d0_9:
                    r15_31 = stdout;
                    rax32 = fun_2490();
                    fun_2550(rax32, r15_31, 5, "https://www.gnu.org/software/coreutils/", rax32, r15_31, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_3fd6_11;
                }
            }
            rax33 = fun_2490();
            fun_2620(1, rax33, r13_22, r12_2);
            addr_3d5e_14:
            fun_2690();
        }
    } else {
        rax34 = fun_2490();
        rdi35 = stderr;
        fun_26b0(rdi35, 1, rax34, rdi35, 1, rax34);
        goto addr_3d5e_14;
    }
}

void fun_2640(int64_t rdi, int64_t* rsi);

void c_strtold(int64_t rdi, void* rsi);

void fun_4103(int64_t rdi, uint64_t* rsi) {
    void* rsp3;
    void** rax4;
    signed char* v5;
    int32_t* rax6;
    int32_t r13d7;
    uint64_t v8;
    uint64_t v9;
    uint64_t v10;
    void* rax11;

    __asm__("cli ");
    rsp3 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 56);
    rax4 = g28;
    fun_2640(rdi, reinterpret_cast<int64_t>(rsp3) + 24);
    __asm__("fld st0");
    if (*v5) {
        __asm__("fstp st0");
        __asm__("fstp tword [rsp]");
        rax6 = fun_23f0();
        r13d7 = *rax6;
        c_strtold(rdi, reinterpret_cast<int64_t>(rsp3) - 8 + 8 - 8 + 8 + 32);
        __asm__("fld tword [rsp]");
        if (v8 >= v9) {
            __asm__("fstp st1");
            *rax6 = r13d7;
        } else {
            __asm__("fstp st0");
            v10 = v9;
        }
    } else {
        __asm__("fstp st1");
    }
    if (rsi) {
        *rsi = v10;
    }
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax11) {
        __asm__("fstp st0");
        fun_24c0();
    } else {
        return;
    }
}

int64_t file_name = 0;

void fun_41b3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_41c3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2410(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_41d3() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_23f0(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2490();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4263_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2650();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2410(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4263_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2650();
    }
}

uint64_t fun_24d0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_4283(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_24d0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_24c0();
    } else {
        return r12_7;
    }
}

uint64_t fun_26a0(void** rdi, void* rsi, uint64_t rdx, void** rcx);

struct s5 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s5* fun_2510();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4313(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s5* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_26a0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_23e0("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2510();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2400(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_5ab3(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_23f0();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xf1e0;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_5af3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf1e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_5b13(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xf1e0);
    }
    *rdi = esi;
    return 0xf1e0;
}

int64_t fun_5b33(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xf1e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s6 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_5b73(struct s6* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s6*>(0xf1e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s7 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s7* fun_5b93(struct s7* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s7*>(0xf1e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x270a;
    if (!rdx) 
        goto 0x270a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xf1e0;
}

struct s8 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5bd3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s8* r8) {
    struct s8* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s8*>(0xf1e0);
    }
    rax7 = fun_23f0();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x5c06);
    *rax7 = r15d8;
    return rax13;
}

struct s9 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_5c53(int64_t rdi, int64_t rsi, void*** rdx, struct s9* rcx) {
    struct s9* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s9*>(0xf1e0);
    }
    rax6 = fun_23f0();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x5c81);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x5cdc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_5d43() {
    __asm__("cli ");
}

void** gf078 = reinterpret_cast<void**>(0xe0);

int64_t slotvec0 = 0x100;

void fun_5d53() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_23c0(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0xf0e0) {
        fun_23c0(rdi7);
        gf078 = reinterpret_cast<void**>(0xf0e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xf070) {
        fun_23c0(r12_2);
        slotvec = reinterpret_cast<void**>(0xf070);
    }
    nslots = 1;
    return;
}

void fun_5df3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5e13() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5e23(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_5e43(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_5e63(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2710;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_5ef3(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x2715;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void** fun_5f83(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s1* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x271a;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_24c0();
    } else {
        return rax5;
    }
}

void** fun_6013(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x271f;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_60a3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x9130]");
    __asm__("movdqa xmm1, [rip+0x9138]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x9121]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_24c0();
    } else {
        return rax10;
    }
}

void** fun_6143(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x9090]");
    __asm__("movdqa xmm1, [rip+0x9098]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x9081]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

void** fun_61e3(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8ff0]");
    __asm__("movdqa xmm1, [rip+0x8ff8]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x8fd9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_24c0();
    } else {
        return rax3;
    }
}

void** fun_6273(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8f60]");
    __asm__("movdqa xmm1, [rip+0x8f68]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x8f56]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_24c0();
    } else {
        return rax4;
    }
}

void** fun_6303(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x2724;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_63a3(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8e2a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x8e22]");
    __asm__("movdqa xmm2, [rip+0x8e2a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2729;
    if (!rdx) 
        goto 0x2729;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void** fun_6443(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s1* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8d8a]");
    __asm__("movdqa xmm1, [rip+0x8d92]");
    __asm__("movdqa xmm2, [rip+0x8d9a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x272e;
    if (!rdx) 
        goto 0x272e;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_24c0();
    } else {
        return rax9;
    }
}

void** fun_64f3(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8cda]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x8cd2]");
    __asm__("movdqa xmm2, [rip+0x8cda]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2733;
    if (!rsi) 
        goto 0x2733;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax6;
    }
}

void** fun_6593(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8c3a]");
    __asm__("movdqa xmm1, [rip+0x8c42]");
    __asm__("movdqa xmm2, [rip+0x8c4a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2738;
    if (!rsi) 
        goto 0x2738;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_24c0();
    } else {
        return rax7;
    }
}

void fun_6633() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6643(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6663() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6683(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int64_t fun_66a3(void** rdi, uint64_t rsi, void** rdx) {
    __asm__("cli ");
    fun_26a0(rdi, 1, rsi, rdx);
    return 0;
}

int64_t fun_66c3(uint32_t edi) {
    void** rdx2;

    __asm__("cli ");
    if (edi > 0xffff) {
        fun_26b0(rdx2, 1, "\\U%08X");
        return -1;
    } else {
        fun_26b0(rdx2, 1, "\\u%04X");
        return -1;
    }
}

int64_t fun_6723(int32_t edi, int64_t rsi) {
    __asm__("cli ");
    if (!rsi) {
        fun_2490();
        fun_2650();
        return -1;
    } else {
        fun_2490();
        fun_2490();
        fun_2650();
        return -1;
    }
}

/* is_utf8.1 */
int32_t is_utf8_1 = 0;

/* initialized.2 */
int32_t initialized_2 = 0;

int64_t fun_26f0(void** rdi, int64_t rsi);

/* utf8_to_local.0 */
int64_t utf8_to_local_0 = 0;

int32_t u8_uctomb_aux(void* rdi, int64_t rsi, int64_t rdx);

int64_t fun_2430(int64_t rdi, ...);

void fun_67c3(uint32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void* rsp5;
    int32_t edx6;
    void** rax7;
    int32_t eax8;
    void** rax9;
    int32_t eax10;
    uint1_t zf11;
    int64_t rax12;
    int64_t rax13;
    int1_t zf14;
    void* r14_15;
    int64_t rsi16;
    int32_t eax17;
    int64_t rsi18;
    int64_t rdi19;
    void* rax20;
    int64_t rdi21;
    void* r14_22;
    int64_t rax23;
    signed char v24;
    int64_t rdi25;
    int64_t rax26;
    int64_t rdi27;
    void* rdx28;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78);
    edx6 = is_utf8_1;
    rax7 = g28;
    eax8 = initialized_2;
    if (!eax8) {
        rax9 = locale_charset();
        eax10 = fun_2570(rax9, "UTF-8");
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8);
        edx6 = 0;
        zf11 = reinterpret_cast<uint1_t>(eax10 == 0);
        *reinterpret_cast<unsigned char*>(&edx6) = zf11;
        is_utf8_1 = edx6;
        if (!zf11) {
            rax12 = fun_26f0(rax9, "UTF-8");
            rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
            utf8_to_local_0 = rax12;
            if (rax12 == -1) {
                rax13 = fun_26f0("ASCII", "UTF-8");
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
                utf8_to_local_0 = rax13;
            }
            edx6 = is_utf8_1;
        }
        initialized_2 = 1;
    }
    if (edx6 || (zf14 = utf8_to_local_0 == -1, !zf14)) {
        if (edi > 0x7f) {
            r14_15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) + 58);
            *reinterpret_cast<uint32_t*>(&rsi16) = edi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi16) + 4) = 0;
            eax17 = u8_uctomb_aux(r14_15, rsi16, 6);
            rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
            if (eax17 >= 0) {
                edx6 = is_utf8_1;
                rsi18 = eax17;
            } else {
                *reinterpret_cast<uint32_t*>(&rdi19) = edi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
                rdx(rdi19, "character out of range", rcx);
                goto addr_6837_12;
            }
        } else {
            *reinterpret_cast<int32_t*>(&rsi18) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0;
            r14_15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) + 58);
        }
    } else {
        rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
        if (!rax20) {
            goto rdx;
        }
    }
    if (!edx6) {
        rdi21 = utf8_to_local_0;
        r14_22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) + 64);
        rax23 = fun_2430(rdi21, rdi21);
        if (rsi18) 
            goto addr_69a0_18;
        if (rax23 == -1) 
            goto addr_69a0_18;
        if (!rax23) 
            goto addr_68d7_21;
        if (reinterpret_cast<int64_t>(r14_22) - reinterpret_cast<int64_t>(r14_22) != 1) 
            goto addr_68d7_21;
        if (v24 == 63) 
            goto addr_699b_24;
    } else {
        rsi(r14_15);
        goto addr_6837_12;
    }
    addr_68d7_21:
    rdi25 = utf8_to_local_0;
    rax26 = fun_2430(rdi25);
    if (rax26 == -1) {
        addr_69a0_18:
        *reinterpret_cast<uint32_t*>(&rdi27) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi27) + 4) = 0;
        rdx(rdi27);
    } else {
        rsi(r14_22);
    }
    addr_6837_12:
    rdx28 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (!rdx28) {
        return;
    }
    fun_24c0();
    addr_699b_24:
    goto addr_69a0_18;
}

void fun_6a33(int64_t rdi, int32_t esi, int32_t edx) {
    __asm__("cli ");
    if (!edx) {
    }
}

struct s10 {
    unsigned char f0;
    signed char f1;
    signed char f2;
    signed char f3;
};

int64_t fun_6a63(struct s10* rdi, int64_t rsi, int64_t rdx) {
    int32_t r8d4;
    uint32_t edx5;
    uint32_t edx6;
    int64_t rax7;
    uint32_t esi8;
    uint32_t edx9;
    uint32_t edx10;
    uint32_t edx11;

    __asm__("cli ");
    if (*reinterpret_cast<uint32_t*>(&rsi) <= 0x7f) 
        goto addr_6b10_2;
    if (*reinterpret_cast<uint32_t*>(&rsi) <= 0x7ff) {
        if (rdx <= 1) {
            addr_6b10_2:
            r8d4 = -2;
        } else {
            r8d4 = 2;
            goto addr_6ab2_6;
        }
    } else {
        if (*reinterpret_cast<uint32_t*>(&rsi) > 0xffff) {
            if (*reinterpret_cast<uint32_t*>(&rsi) > 0x10ffff) {
                addr_6b18_9:
                r8d4 = -1;
            } else {
                if (rdx <= 3) 
                    goto addr_6b10_2;
                r8d4 = 4;
                edx5 = *reinterpret_cast<uint32_t*>(&rsi) & 63;
                *reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<uint32_t*>(&rsi) >> 6 | 0x10000;
                edx6 = edx5 | 0xffffff80;
                rdi->f3 = *reinterpret_cast<signed char*>(&edx6);
                goto addr_6a9e_12;
            }
        } else {
            if (static_cast<uint32_t>(rsi - 0xd800) <= 0x7ff) 
                goto addr_6b18_9;
            if (rdx <= 2) 
                goto addr_6b10_2;
            r8d4 = 3;
            goto addr_6a9e_12;
        }
    }
    addr_6ac7_16:
    *reinterpret_cast<int32_t*>(&rax7) = r8d4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
    addr_6ab2_6:
    esi8 = *reinterpret_cast<uint32_t*>(&rsi) >> 6;
    edx9 = *reinterpret_cast<uint32_t*>(&rsi) & 63 | 0xffffff80;
    rdi->f0 = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&esi8) | 0xc0);
    rdi->f1 = *reinterpret_cast<signed char*>(&edx9);
    goto addr_6ac7_16;
    addr_6a9e_12:
    edx10 = *reinterpret_cast<uint32_t*>(&rsi) & 63;
    *reinterpret_cast<uint32_t*>(&rsi) = *reinterpret_cast<uint32_t*>(&rsi) >> 6 | 0x800;
    edx11 = edx10 | 0xffffff80;
    rdi->f2 = *reinterpret_cast<signed char*>(&edx11);
    goto addr_6ab2_6;
}

void fun_2580(int64_t rdi, void** rsi, void** rdx, void** rcx, int64_t r8, uint64_t r9);

void fun_6b23(void** rdi, int64_t rsi, int64_t rdx, uint64_t rcx, int64_t r8, uint64_t r9) {
    uint64_t r12_7;
    void** rax8;
    void** rax9;
    void** rax10;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_26b0(rdi, 1, "%s %s\n", rdi, 1, "%s %s\n");
    } else {
        r9 = rcx;
        fun_26b0(rdi, 1, "%s (%s) %s\n", rdi, 1, "%s (%s) %s\n");
    }
    rax8 = fun_2490();
    fun_26b0(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rdi, 1, "Copyright %s %d Free Software Foundation, Inc.");
    fun_2580(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_2490();
    fun_26b0(rdi, 1, rax9, rdi, 1, rax9);
    fun_2580(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (r12_7 > 9) {
        rax10 = fun_2490();
        fun_26b0(rdi, 1, rax10, rdi, 1, rax10);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xb2f8 + r12_7 * 4) + 0xb2f8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_6f93() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s11 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_6fb3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s11* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s11* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_24c0();
    } else {
        return;
    }
}

void fun_7053(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_70f6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7100_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_24c0();
    } else {
        return;
    }
    addr_70f6_5:
    goto addr_7100_7;
}

void fun_7133() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    int64_t r8_4;
    uint64_t r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2580(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2490();
    fun_2620(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2490();
    fun_2620(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2490();
    goto fun_2620;
}

int64_t fun_2450();

void xalloc_die();

void fun_71d3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2450();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void** fun_25c0(void** rdi, void** rsi, ...);

void fun_7213(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7233(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7253(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2600(void** rdi, void** rsi, ...);

void fun_7273(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2600(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_72a3(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2600(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_72d3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2450();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7313() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2450();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7353(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2450();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7383(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2450();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_73d3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2450();
            if (rax5) 
                break;
            addr_741d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_741d_5;
        rax8 = fun_2450();
        if (rax8) 
            goto addr_7406_9;
        if (rbx4) 
            goto addr_741d_5;
        addr_7406_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7463(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2450();
            if (rax8) 
                break;
            addr_74aa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_74aa_5;
        rax11 = fun_2450();
        if (rax11) 
            goto addr_7492_9;
        if (!rbx6) 
            goto addr_7492_9;
        if (r12_4) 
            goto addr_74aa_5;
        addr_7492_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_74f3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_759d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_75b0_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_7550_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_7576_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_75c4_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_756d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_75c4_14;
            addr_756d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_75c4_14;
            addr_7576_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2600(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_75c4_14;
            if (!rbp13) 
                break;
            addr_75c4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_759d_9;
        } else {
            if (!r13_6) 
                goto addr_75b0_10;
            goto addr_7550_11;
        }
    }
}

int64_t fun_2560();

void fun_75f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7623() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7653() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7673() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2560();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2590(void** rdi, void** rsi, void** rdx);

void fun_7693(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_76d3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_7713(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_25c0(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2590;
    }
}

void fun_7753(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_24b0(rdi);
    rax4 = fun_25c0(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2590;
    }
}

void fun_7793() {
    void** rdi1;

    __asm__("cli ");
    fun_2490();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2650();
    fun_23e0(rdi1);
}

int32_t rpl_vprintf();

int32_t fun_2440(void** rdi);

int64_t fun_77d3() {
    int32_t eax1;
    void** rdi2;
    int32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax1 = rpl_vprintf();
    if (eax1 < 0 && (rdi2 = stdout, eax3 = fun_2440(rdi2), !eax3)) {
        fun_2490();
        fun_23f0();
        fun_2650();
    }
    *reinterpret_cast<int32_t*>(&rax4) = eax1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

void xvprintf();

void fun_7843() {
    signed char al1;
    void** rax2;
    void* rdx3;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    xvprintf();
    rdx3 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx3) {
        fun_24c0();
    } else {
        return;
    }
}

int32_t rpl_vfprintf(void** rdi);

int64_t fun_7903(void** rdi) {
    signed char al2;
    void** rax3;
    int32_t eax4;
    int32_t eax5;
    void* rax6;
    int64_t rax7;

    __asm__("cli ");
    if (al2) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax3 = g28;
    eax4 = rpl_vfprintf(rdi);
    if (eax4 < 0 && (eax5 = fun_2440(rdi), !eax5)) {
        fun_2490();
        fun_23f0();
        fun_2650();
    }
    rax6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rax6) {
        fun_24c0();
    } else {
        *reinterpret_cast<int32_t*>(&rax7) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    }
}

int64_t fun_7a13(void** rdi) {
    int32_t eax2;
    int32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax2 = rpl_vfprintf(rdi);
    if (eax2 < 0 && (eax3 = fun_2440(rdi), !eax3)) {
        fun_2490();
        fun_23f0();
        fun_2650();
    }
    *reinterpret_cast<int32_t*>(&rax4) = eax2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

void** vasnprintf(void** rdi, void* rsi, uint64_t rdx, void** rcx);

void fseterr(void** rdi, void* rsi, uint64_t rdx, void** rcx);

int64_t fun_7a83(void** rdi, uint64_t rsi, void** rdx) {
    void** rcx4;
    uint64_t rdx5;
    void* rsp6;
    void** rax7;
    void** r13_8;
    void* rsi9;
    void** rax10;
    int64_t rax11;
    uint64_t rax12;
    void* rdx13;
    int32_t* rax14;

    __asm__("cli ");
    rcx4 = rdx;
    rdx5 = rsi;
    rsp6 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x808);
    rax7 = g28;
    r13_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp6) + 32);
    rsi9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp6) + 24);
    rax10 = vasnprintf(r13_8, rsi9, rdx5, rcx4);
    if (!rax10) {
        addr_7b57_2:
        fseterr(rdi, rsi9, rdx5, rcx4);
        *reinterpret_cast<int32_t*>(&rax11) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    } else {
        rcx4 = rdi;
        rdx5 = 0x7d0;
        *reinterpret_cast<int32_t*>(&rsi9) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        rax12 = fun_26a0(rax10, 1, 0x7d0, rcx4);
        if (rax12 < 0x7d0) {
            *reinterpret_cast<int32_t*>(&rax11) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            if (rax10 != r13_8) {
                fun_23c0(rax10, rax10);
                *reinterpret_cast<int32_t*>(&rax11) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            }
        } else {
            if (rax10 != r13_8) {
                fun_23c0(rax10, rax10);
            }
            if (0) 
                goto addr_7b4c_9; else 
                goto addr_7b0a_10;
        }
    }
    addr_7b0c_11:
    rdx13 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx13) {
        fun_24c0();
    } else {
        return rax11;
    }
    addr_7b4c_9:
    rax14 = fun_23f0();
    *rax14 = 75;
    goto addr_7b57_2;
    addr_7b0a_10:
    *reinterpret_cast<int32_t*>(&rax11) = 0x7d0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
    goto addr_7b0c_11;
}

void fun_7b73(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto rpl_vfprintf;
}

int64_t c_locale_cache = 0;

int64_t fun_24f0(int64_t rdi, int64_t* rsi);

int64_t fun_25b0(int64_t rdi, int64_t* rsi);

void fun_7b93(int64_t rdi, int64_t* rsi) {
    int64_t* rbp3;
    int64_t rax4;
    int64_t rax5;
    int64_t rdi6;
    int64_t rax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    rbp3 = rsi;
    rax4 = c_locale_cache;
    if (!rax4) {
        rsi = reinterpret_cast<int64_t*>("C");
        rax5 = fun_24f0(0x1fbf, "C");
        c_locale_cache = rax5;
    }
    rdi6 = c_locale_cache;
    if (!rdi6 || (rax7 = fun_25b0(rdi6, rsi), rax7 == 0)) {
        if (!rbp3) {
            __asm__("fldz ");
            return;
        } else {
            *rbp3 = rdi;
            __asm__("fldz ");
            return;
        }
    } else {
        fun_2640(rdi, rbp3);
        __asm__("fstp tword [rsp]");
        rax8 = fun_23f0();
        r12d9 = *rax8;
        rax10 = fun_25b0(rax7, rbp3);
        if (!rax10) 
            goto 0x273d;
        *rax8 = r12d9;
        __asm__("fld tword [rsp]");
        return;
    }
}

int64_t fun_2420();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_7c63(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_2420();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_7cbe_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_23f0();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_7cbe_3;
            rax6 = fun_23f0();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s12 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_25a0(struct s12* rdi);

int32_t fun_25f0(struct s12* rdi);

int64_t fun_2520(int64_t rdi, ...);

int32_t rpl_fflush(struct s12* rdi);

int64_t fun_2470(struct s12* rdi);

int64_t fun_7cd3(struct s12* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_25a0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25f0(rdi);
        if (!(eax3 && (eax4 = fun_25a0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2520(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_23f0();
            r12d9 = *rax8;
            rax10 = fun_2470(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2470;
}

void rpl_fseeko(struct s12* rdi);

void fun_7d63(struct s12* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25f0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_7db3(struct s12* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_25a0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2520(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void fun_7e33(uint32_t* rdi) {
    __asm__("cli ");
    *rdi = *rdi | 32;
    return;
}

int32_t setlocale_null_r();

int64_t fun_7e43() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_24c0();
    } else {
        return rax3;
    }
}

signed char* fun_25e0(int64_t rdi);

signed char* fun_7ec3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25e0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

int64_t fun_7f03(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2610(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_24b0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2590(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2590(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7fb3() {
    __asm__("cli ");
    goto fun_2610;
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s13 {
    signed char[7] pad7;
    void** f7;
};

struct s14 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s15 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_7fc3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void*** v11;
    void** rax12;
    void** v13;
    int32_t eax14;
    void* rsp15;
    void** r10_16;
    void* rax17;
    int64_t* rsp18;
    void** rbx19;
    int64_t* rsp20;
    void** rax21;
    void** v22;
    int64_t* rsp23;
    void** v24;
    int64_t* rsp25;
    void** v26;
    int64_t* rsp27;
    void** v28;
    int64_t* rsp29;
    int32_t* rax30;
    void** r15_31;
    int32_t* v32;
    int64_t* rsp33;
    int64_t* rsp34;
    void** v35;
    int64_t* rsp36;
    void** v37;
    int64_t* rsp38;
    void** rsi39;
    int32_t eax40;
    int32_t* rax41;
    void** rax42;
    struct s13* v43;
    void** tmp64_44;
    void* v45;
    void** r8_46;
    void** tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    void** v53;
    void** rax54;
    int32_t* rax55;
    struct s14* r14_56;
    struct s14* v57;
    void** r9_58;
    void** r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    void** tmp64_63;
    void** r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    void** rax67;
    int64_t* rsp68;
    void** rax69;
    int64_t* rsp70;
    void** rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;
    void** rax75;
    int64_t* rsp76;
    void** rax77;
    void** tmp64_78;
    int64_t* rsp79;
    void** rax80;
    int64_t* rsp81;
    void** rax82;
    int64_t* rsp83;
    void** rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    void** rsi89;
    void** rax90;
    int64_t* rsp91;
    void** rsi92;
    void** rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s15* rcx98;
    int64_t rax99;
    void** tmp64_100;
    void** r15_101;
    int32_t* rax102;
    int64_t rax103;
    int64_t* rsp104;
    void** rax105;
    int64_t* rsp106;
    int32_t* rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    void** rax110;
    int64_t* rsp111;
    int32_t* rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v13) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0x8efa;
                fun_24c0();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_8efa_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_8f04_6;
                addr_8dee_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0x8e13, rax21 = fun_2600(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0x8e39;
                    fun_23c0(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0x8e5f;
                    fun_23c0(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0x8e85;
                    fun_23c0(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_8f04_6:
            addr_8ae8_16:
            v28 = r10_16;
            addr_8aef_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0x8af4;
            rax30 = fun_23f0();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_8b02_18:
            *v32 = 12;
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0x86a1;
                fun_23c0(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0x86b9;
                fun_23c0(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_82f8_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0x8310;
                fun_23c0(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0x8328;
            fun_23c0(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_23f0();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *rax41 = 22;
        goto addr_82f8_23;
    }
    rax42 = reinterpret_cast<void**>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_82ed_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = tmp64_44 + 6;
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_82ed_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax49 = reinterpret_cast<void*>(tmp64_44 + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 0x1000);
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & 0xfff;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<void**>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_25c0(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_82ed_33:
            rax55 = fun_23f0();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *rax55 = 12;
            goto addr_82f8_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_80ec_46;
    while (1) {
        addr_8a44_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_81af_50;
            if (r14_56->f50 != -1) 
                goto 0x2742;
            r9_58 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_8a1f_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_8ae8_16;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_8ae8_16;
                if (r10_16 == v10) 
                    goto addr_8d34_64; else 
                    goto addr_89f3_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s14*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_8a44_47;
            addr_80ec_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_8ba0_73;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_8ba0_73;
                if (r15_31 == v10) 
                    goto addr_8b30_79; else 
                    goto addr_8147_80;
            }
            addr_816c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0x8182;
            fun_2590(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_8b30_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0x8b38;
            rax67 = fun_25c0(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_8ba0_73;
            if (!r9_58) 
                goto addr_816c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0x8b77;
            rax69 = fun_2590(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_816c_81;
            addr_8147_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0x8152;
            rax71 = fun_2600(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_8ba0_73; else 
                goto addr_816c_81;
            addr_8d34_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0x8d4a;
            rax73 = fun_25c0(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_8f09_84;
            if (r12_9) 
                goto addr_8d6a_86;
            r10_16 = rax73;
            goto addr_8a1f_55;
            addr_8d6a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0x8d7f;
            rax75 = fun_2590(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_8a1f_55;
            addr_89f3_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0x8a0c;
            rax77 = fun_2600(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_8aef_17;
            r10_16 = rax77;
            goto addr_8a1f_55;
        }
        break;
    }
    tmp64_78 = r12_9 + 1;
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_8efa_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_8dee_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_8ae8_16;
        rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_8ead_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_8ead_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_8ae8_16; else 
                goto addr_8eb7_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_8dc3_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0x8ece;
        rax80 = fun_25c0(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_8dee_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0x8eed;
                rax82 = fun_2590(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_8dee_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0x8de2;
        rax84 = fun_2600(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_8aef_17; else 
            goto addr_8dee_7;
    }
    addr_8eb7_96:
    rbx19 = r13_8;
    goto addr_8dc3_98;
    addr_81af_50:
    if (r14_56->f50 == -1) 
        goto 0x2742;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x2747;
        goto *reinterpret_cast<int32_t*>(0xb3f8 + r13_87 * 4) + 0xb3f8;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<void***>(v53) = reinterpret_cast<void**>(37);
    r13_8 = v53 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        *reinterpret_cast<void***>(v53 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v53 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0x827f;
        fun_2590(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0x82b9;
        fun_2590(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0xb388 + rax95 * 4) + 0xb388;
    }
    eax96 = r14_56->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x2742;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s15*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x2742;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_83f2_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_8ae8_16;
    }
    addr_83f2_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_8ae8_16;
            rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_8413_142; else 
                goto addr_8ca2_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_8ca2_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_8ae8_16; else 
                    goto addr_8cac_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_8413_142;
            }
        }
    }
    addr_8445_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0x844f;
    rax102 = fun_23f0();
    *rax102 = 0;
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x2747;
    goto *reinterpret_cast<int32_t*>(0xb3b0 + rax103 * 4) + 0xb3b0;
    addr_8413_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0x8d08;
        rax105 = fun_25c0(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_8f09_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0x8f0e;
            rax107 = fun_23f0();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_8b02_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0x8d2f;
                fun_2590(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_8445_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0x8432;
        rax110 = fun_2600(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_8ae8_16; else 
            goto addr_8445_147;
    }
    addr_8cac_145:
    rbx19 = tmp64_100;
    goto addr_8413_142;
    addr_8ba0_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0x8ba5;
    rax112 = fun_23f0();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_8b02_18;
}

struct s16 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_8f43(int64_t rdi, struct s16* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_8f79_5;
    return 0xffffffff;
    addr_8f79_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xb420 + rdx3 * 4) + 0xb420;
}

struct s17 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s18 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s19 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

int64_t fun_9173(void** rdi, struct s17* rsi, struct s18* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s18* r15_7;
    struct s17* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    struct s0* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s0* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    struct s0* rbx65;
    void* rsi66;
    int32_t eax67;
    struct s0* rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rax88;
    void** rax89;
    int32_t* rax90;
    void*** rax91;
    void** rdi92;
    int32_t* rax93;
    struct s19* rbx94;
    void* rdi95;
    int32_t eax96;
    struct s19* rcx97;
    void* rax98;
    void* rdx99;
    void* rdx100;
    void* tmp64_101;
    int32_t eax102;
    int32_t eax103;
    int32_t eax104;
    int64_t rax105;
    int64_t rax106;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_9228_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_9215_7:
    return rax15;
    addr_9228_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<uint32_t*>(r12_16 + 16) = 0;
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<int64_t*>(r12_16 + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_9299_11;
    } else {
        addr_9299_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<uint32_t*>(r12_16 + 16) = *reinterpret_cast<uint32_t*>(r12_16 + 16) | 1;
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_92b0_14;
        } else {
            goto addr_92b0_14;
        }
    }
    rax23 = reinterpret_cast<struct s0*>(rax5 + 2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_9778_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s0*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s0*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_9778_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<void**>(&rcx26->f2);
    goto addr_9299_11;
    addr_92b0_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb49c + rax33 * 4) + 0xb49c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_93f7_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_9af9_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_9afe_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_9af4_42;
            }
        } else {
            addr_92dd_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_94f8_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_92e7_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = *reinterpret_cast<unsigned char*>(rbx14 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_9ce4_56; else 
                        goto addr_9535_57;
                }
            } else {
                addr_92e7_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_9308_60;
                } else {
                    goto addr_9308_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_987b_65;
    addr_93f7_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_9778_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_941c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_949a_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_9d9b_75; else 
            goto addr_9443_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_977c_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_92e7_52;
        goto addr_94f8_44;
    }
    addr_9afe_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_92dd_43;
    addr_9d9b_75:
    if (v11 != rdx53) {
        fun_23c0(rdx53, rdx53);
        r10_4 = r10_4;
        goto addr_9baa_84;
    } else {
        goto addr_9baa_84;
    }
    addr_9443_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_25c0(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_2600(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_9d9b_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_2590(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_949a_68;
    addr_987b_65:
    rbx65 = reinterpret_cast<struct s0*>(rbx14 + 2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_9778_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s0*>(&rbx65->pad2);
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s0*>(&rbx65->pad2);
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_9778_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = reinterpret_cast<void**>(&rdx68->f2);
    goto addr_941c_67;
    label_41:
    addr_9af4_42:
    goto addr_9af9_36;
    addr_9ce4_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_9d0a_104;
    addr_9535_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_9778_22:
            r8_54 = r15_7->f8;
            goto addr_977c_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_9544_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_9bf2_111;
    } else {
        addr_9551_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_9587_116;
        }
    }
    rdx53 = r8_54;
    goto addr_9d9b_75;
    addr_9bf2_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_25c0(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_9baa_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_23c0(rdi86, rdi86);
            }
        } else {
            addr_9e30_120:
            rdx87 = r15_7->f0;
            rax88 = fun_2590(rdi84, r8_85, reinterpret_cast<unsigned char>(rdx87) << 5);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax88;
            goto addr_9c4f_121;
        }
    } else {
        rax89 = fun_2600(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax89;
        if (!rax89) {
            rdx53 = r15_7->f8;
            goto addr_9d9b_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_9e30_120;
            }
        }
    }
    rax90 = fun_23f0();
    *rax90 = 12;
    return 0xffffffff;
    addr_9c4f_121:
    r15_7->f8 = r8_54;
    goto addr_9551_112;
    addr_9587_116:
    rax91 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax91) {
        if (!reinterpret_cast<int1_t>(*rax91 == 5)) {
            addr_977c_80:
            if (v11 != r8_54) {
                fun_23c0(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_92e7_52;
        }
    } else {
        *rax91 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_92e7_52;
    }
    rdi92 = r14_8->f8;
    if (r10_4 != rdi92) {
        fun_23c0(rdi92, rdi92);
    }
    rax93 = fun_23f0();
    *rax93 = 22;
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_9215_7;
    addr_9d0a_104:
    rbx94 = reinterpret_cast<struct s19*>(rbx14 + 3);
    *reinterpret_cast<int32_t*>(&rdi95) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi95) + 4) = 0;
    while (1) {
        eax96 = static_cast<int32_t>(rsi46 - 48);
        rcx97 = reinterpret_cast<struct s19*>(reinterpret_cast<uint64_t>(rbx94) + 0xffffffffffffffff);
        rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax96)));
        if (reinterpret_cast<uint64_t>(rdi95) > 0x1999999999999999) {
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi95) + reinterpret_cast<uint64_t>(rdi95) * 4);
            rdx99 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rdx100));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx94->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx99) + reinterpret_cast<uint64_t>(rax98)), rdi95 = tmp64_101, eax102 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_101) < reinterpret_cast<uint64_t>(rdx99)) {
            if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
                goto addr_9778_22;
            rcx97 = rbx94;
            rax98 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax102)));
            rbx94 = reinterpret_cast<struct s19*>(&rbx94->pad2);
            rdx99 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax102) > 9) 
            break;
        rbx94 = reinterpret_cast<struct s19*>(&rbx94->pad2);
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_101) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_9778_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = reinterpret_cast<void**>(&rcx97->f2);
    goto addr_9544_107;
    addr_9308_60:
    eax103 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax103) > 46) {
        eax104 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax104) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<unsigned char*>(&eax104);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax105) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xb600 + rax105 * 4) + 0xb600;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xb544 + rax106 * 4) + 0xb544;
    }
}

void fun_9f03() {
    __asm__("cli ");
}

void fun_9f17() {
    __asm__("cli ");
    return;
}

void fun_2da0() {
    goto 0x2b48;
}

void xprintf(void** rdi, void** rsi, ...);

void fun_2f07() {
    void** r9_1;
    void** rax2;
    signed char v3;
    signed char v4;
    void** rsi5;
    int32_t v6;
    void** rsi7;
    int32_t v8;
    void** r8_9;
    signed char v10;
    int32_t v11;

    rax2 = vstrtoimax(r9_1);
    if (v3) {
        if (!v4) {
            *reinterpret_cast<int32_t*>(&rsi5) = v6;
            *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rsi7) = v8;
            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
            xprintf(r8_9, rsi7, r8_9, rsi7);
            fun_23c0(r8_9, r8_9);
            goto 0x28b2;
        }
    } else {
        if (v10) {
            *reinterpret_cast<int32_t*>(&rsi5) = v11;
            *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
        } else {
            xprintf(r8_9, rax2, r8_9, rax2);
            goto 0x2e63;
        }
    }
    xprintf(r8_9, rsi5, r8_9, rsi5);
}

void fun_2f49() {
    signed char v1;
    signed char v2;
    signed char v3;
    void** r8_4;
    void** r9_5;

    if (v1) {
        if (!v2) 
            goto 0x316c;
    } else {
        if (!v3) {
            xprintf(r8_4, r9_5);
            goto 0x2e63;
        }
    }
}

void fun_38e2() {
    void** rdi1;
    void** rax2;

    rdi1 = stdout;
    rax2 = *reinterpret_cast<void***>(rdi1 + 40);
    if (reinterpret_cast<unsigned char>(rax2) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1 + 48))) {
        fun_2500(rdi1, 11);
        goto 0x365a;
    } else {
        *reinterpret_cast<void***>(rdi1 + 40) = rax2 + 1;
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(11);
        goto 0x365a;
    }
}

void fun_390d() {
    void** rdi1;
    void** rax2;

    rdi1 = stdout;
    rax2 = *reinterpret_cast<void***>(rdi1 + 40);
    if (reinterpret_cast<unsigned char>(rax2) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1 + 48))) {
        fun_2500(rdi1, 9);
        goto 0x365a;
    } else {
        *reinterpret_cast<void***>(rdi1 + 40) = rax2 + 1;
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(9);
        goto 0x365a;
    }
}

void fun_3938() {
    void** rdi1;
    void** rax2;

    rdi1 = stdout;
    rax2 = *reinterpret_cast<void***>(rdi1 + 40);
    if (reinterpret_cast<unsigned char>(rax2) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1 + 48))) {
        fun_2500(rdi1, 27);
        goto 0x365a;
    } else {
        *reinterpret_cast<void***>(rdi1 + 40) = rax2 + 1;
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(27);
        goto 0x365a;
    }
}

void fun_3963() {
    void** rdi1;
    void** rax2;

    rdi1 = stdout;
    rax2 = *reinterpret_cast<void***>(rdi1 + 40);
    if (reinterpret_cast<unsigned char>(rax2) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1 + 48))) {
        fun_2500(rdi1, 10);
        goto 0x365a;
    } else {
        *reinterpret_cast<void***>(rdi1 + 40) = rax2 + 1;
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(10);
        goto 0x365a;
    }
}

void fun_398e() {
    void** rdi1;
    void** rax2;

    rdi1 = stdout;
    rax2 = *reinterpret_cast<void***>(rdi1 + 40);
    if (reinterpret_cast<unsigned char>(rax2) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1 + 48))) {
        fun_2500(rdi1, 8);
        goto 0x365a;
    } else {
        *reinterpret_cast<void***>(rdi1 + 40) = rax2 + 1;
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(8);
        goto 0x365a;
    }
}

void fun_39b9() {
    void** rdi1;
    void** rax2;

    rdi1 = stdout;
    rax2 = *reinterpret_cast<void***>(rdi1 + 40);
    if (reinterpret_cast<unsigned char>(rax2) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1 + 48))) {
        fun_2500(rdi1, 13);
        goto 0x365a;
    } else {
        *reinterpret_cast<void***>(rdi1 + 40) = rax2 + 1;
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(13);
        goto 0x365a;
    }
}

void fun_39e4() {
    void** rdi1;
    void** rax2;

    fun_2690();
    rdi1 = stdout;
    rax2 = *reinterpret_cast<void***>(rdi1 + 40);
    if (reinterpret_cast<unsigned char>(rax2) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1 + 48))) {
        fun_2500(rdi1, 12);
        goto 0x365a;
    } else {
        *reinterpret_cast<void***>(rdi1 + 40) = rax2 + 1;
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(12);
        goto 0x365a;
    }
}

void fun_3a16() {
    void** rdi1;
    void** rax2;

    rdi1 = stdout;
    rax2 = *reinterpret_cast<void***>(rdi1 + 40);
    if (reinterpret_cast<unsigned char>(rax2) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1 + 48))) {
        fun_2500(rdi1, 7);
        goto 0x365a;
    } else {
        *reinterpret_cast<void***>(rdi1 + 40) = rax2 + 1;
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(7);
        goto 0x365a;
    }
}

uint32_t fun_2540(void** rdi, void** rsi, void** rdx, void** rcx);

int32_t fun_26d0(int64_t rdi, void** rsi);

uint32_t fun_26c0(void** rdi, void** rsi);

void fun_4545() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    uint64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    uint64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    uint64_t rax104;
    uint64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2490();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2490();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24b0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_4843_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_4843_22; else 
                            goto addr_4c3d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_4cfd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5050_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_4840_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_4840_30; else 
                                goto addr_5069_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24b0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5050_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2540(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5050_28; else 
                            goto addr_46ec_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_51b0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5030_40:
                        if (r11_27 == 1) {
                            addr_4bbd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5178_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_47f7_44;
                            }
                        } else {
                            goto addr_5040_46;
                        }
                    } else {
                        addr_51bf_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_4bbd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_4843_22:
                                if (v47 != 1) {
                                    addr_4d99_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_24b0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_4de4_54;
                                    }
                                } else {
                                    goto addr_4850_56;
                                }
                            } else {
                                addr_47f5_57:
                                ebp36 = 0;
                                goto addr_47f7_44;
                            }
                        } else {
                            addr_5024_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_51bf_47; else 
                                goto addr_502e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_4bbd_41;
                        if (v47 == 1) 
                            goto addr_4850_56; else 
                            goto addr_4d99_52;
                    }
                }
                addr_48b1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_4748_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_476d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_4a70_65;
                    } else {
                        addr_48d9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5128_67;
                    }
                } else {
                    goto addr_48d0_69;
                }
                addr_4781_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_47cc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5128_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_47cc_81;
                }
                addr_48d0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_476d_64; else 
                    goto addr_48d9_66;
                addr_47f7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_48af_91;
                if (v22) 
                    goto addr_480f_93;
                addr_48af_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_48b1_62;
                addr_4de4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70, reinterpret_cast<unsigned char>(v62) - reinterpret_cast<unsigned char>(r13_69), rcx44);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_556b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_55db_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_53df_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_26d0(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_26c0(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_4ede_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_489c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_4ee8_112;
                    }
                } else {
                    addr_4ee8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_4fb9_114;
                }
                addr_48a8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_48af_91;
                while (1) {
                    addr_4fb9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_54c7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_4f26_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_54d5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_4fa7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_4fa7_128;
                        }
                    }
                    addr_4f55_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_4fa7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_4f26_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_4f55_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_47cc_81;
                addr_54d5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5128_67;
                addr_556b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_4ede_109;
                addr_55db_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_4ede_109;
                addr_4850_56:
                rax93 = fun_26e0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_489c_110;
                addr_502e_59:
                goto addr_5030_40;
                addr_4cfd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_4843_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_48a8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_47f5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_4843_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_4d42_160;
                if (!v22) 
                    goto addr_5117_162; else 
                    goto addr_5323_163;
                addr_4d42_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5117_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5128_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_4beb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_4a53_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_4781_70; else 
                    goto addr_4a67_169;
                addr_4beb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_4748_63;
                goto addr_48d0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5024_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_515f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4840_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4738_178; else 
                        goto addr_50e2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5024_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4843_22;
                }
                addr_515f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_4840_30:
                    r8d42 = 0;
                    goto addr_4843_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_48b1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5178_42;
                    }
                }
                addr_4738_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_4748_63;
                addr_50e2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5040_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_48b1_62;
                } else {
                    addr_50f2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_4843_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_58a2_188;
                if (v28) 
                    goto addr_5117_162;
                addr_58a2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_4a53_168;
                addr_46ec_37:
                if (v22) 
                    goto addr_56e3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_4703_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_51b0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_523b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_4843_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_4738_178; else 
                        goto addr_5217_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5024_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_4843_22;
                }
                addr_523b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_4843_22;
                }
                addr_5217_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5040_46;
                goto addr_50f2_186;
                addr_4703_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_4843_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_4843_22; else 
                    goto addr_4714_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_57ee_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_5674_210;
            if (1) 
                goto addr_5672_212;
            if (!v29) 
                goto addr_52ae_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_57e1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_4a70_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_482b_219; else 
            goto addr_4a8a_220;
        addr_480f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_4823_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_4a8a_220; else 
            goto addr_482b_219;
        addr_53df_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_4a8a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_53fd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_24a0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_5870_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_52d6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_54c7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4823_221;
        addr_5323_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_4823_221;
        addr_4a67_169:
        goto addr_4a70_65;
        addr_57ee_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_4a8a_220;
        goto addr_53fd_222;
        addr_5674_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_56ce_236;
        fun_24c0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_5870_225;
        addr_5672_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_5674_210;
        addr_52ae_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_5674_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_52d6_226;
        }
        addr_57e1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_4c3d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xad0c + rax113 * 4) + 0xad0c;
    addr_5069_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xae0c + rax114 * 4) + 0xae0c;
    addr_56e3_190:
    addr_482b_219:
    goto 0x4510;
    addr_4714_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xac0c + rax115 * 4) + 0xac0c;
    addr_56ce_236:
    goto v116;
}

void fun_4730() {
}

void fun_48e8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x45e2;
}

void fun_4941() {
    goto 0x45e2;
}

void fun_4a2e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x48b1;
    }
    if (v2) 
        goto 0x5323;
    if (!r10_3) 
        goto addr_548e_5;
    if (!v4) 
        goto addr_535e_7;
    addr_548e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_535e_7:
    goto 0x4764;
}

void fun_4a4c() {
}

void fun_4af7() {
    signed char v1;

    if (v1) {
        goto 0x4a7f;
    } else {
        goto 0x47ba;
    }
}

void fun_4b11() {
    signed char v1;

    if (!v1) 
        goto 0x4b0a; else 
        goto "???";
}

void fun_4b38() {
    goto 0x4a53;
}

void fun_4bb8() {
}

void fun_4bd0() {
}

void fun_4bff() {
    goto 0x4a53;
}

void fun_4c51() {
    goto 0x4be0;
}

void fun_4c80() {
    goto 0x4be0;
}

void fun_4cb3() {
    goto 0x4be0;
}

void fun_5080() {
    goto 0x4738;
}

void fun_537e() {
    signed char v1;

    if (v1) 
        goto 0x5323;
    goto 0x4764;
}

void fun_5425() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x4764;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x4748;
        goto 0x4764;
    }
}

void fun_5842() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x4ab0;
    } else {
        goto 0x45e2;
    }
}

void fun_6bf8() {
    fun_2490();
}

int32_t fun_23b0(int64_t rdi);

struct s20 {
    signed char[1] pad1;
    signed char f1;
};

struct s21 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_84b8() {
    int64_t rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int64_t rbp4;
    int64_t rbp5;
    int64_t rsi6;
    int32_t eax7;
    int64_t rdx8;
    int64_t rbp9;
    void** rsi10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    int64_t rbp14;
    int64_t rbp15;
    int64_t rsi16;
    int64_t rbp17;
    int64_t rbp18;
    void** rcx19;
    uint64_t r15_20;
    int64_t r12_21;
    void** rax22;
    int64_t rbp23;
    int64_t rbp24;
    int64_t rbp25;
    uint64_t r13_26;
    int64_t rbp27;
    int64_t rbx28;
    int64_t rbx29;
    void** rax30;
    void** rcx31;
    void* rbx32;
    void* rbx33;
    void** tmp64_34;
    void* r12_35;
    void** rax36;
    void** rbx37;
    int64_t r15_38;
    int64_t rbp39;
    void** r15_40;
    void** rax41;
    void** rax42;
    int64_t r12_43;
    void** r15_44;
    void** r12_45;
    int64_t rbp46;
    int64_t rbp47;
    uint32_t eax48;
    struct s21* r14_49;
    int32_t eax50;
    int64_t rbp51;

    rdi1 = r15_2 + r12_3;
    if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 1) {
        *reinterpret_cast<int64_t*>(rbp5 - 0x418) = rsi6;
        eax7 = fun_23b0(rdi1);
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        rsi10 = *reinterpret_cast<void***>(rbp11 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_8643_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp4 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
            eax7 = fun_23b0(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp14 - 0x418);
        } else {
            *reinterpret_cast<int64_t*>(rbp15 - 0x418) = rsi16;
            eax7 = fun_23b0(rdi1);
            rsi10 = *reinterpret_cast<void***>(rbp17 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx8) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx8) < 0) 
            goto addr_8643_5;
    }
    rcx19 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx8)));
    if (reinterpret_cast<unsigned char>(rcx19) < reinterpret_cast<unsigned char>(rsi10)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx19) + r15_20 + r12_21)) 
            goto 0x2742;
    }
    if (*reinterpret_cast<int32_t*>(&rdx8) >= eax7) {
        addr_854d_16:
        *reinterpret_cast<int32_t*>(&rax22) = static_cast<int32_t>(rdx8 + 1);
        *reinterpret_cast<int32_t*>(&rax22 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rsi10)) {
            **reinterpret_cast<int32_t**>(rbp23 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp24 - 0x40c);
            goto 0x8a27;
        }
    } else {
        addr_8545_18:
        *reinterpret_cast<int32_t*>(rbp25 - 0x3bc) = eax7;
        *reinterpret_cast<int32_t*>(&rdx8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx8) + 4) = 0;
        goto addr_854d_16;
    }
    if (r13_26 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp27 - 0x3d0) = 75;
        goto 0x8690;
    }
    if (rbx28 < 0) {
        if (rbx29 == -1) 
            goto 0x8468;
        goto 0x8b02;
    }
    *reinterpret_cast<int32_t*>(&rax30) = static_cast<int32_t>(rdx8 + 2);
    *reinterpret_cast<int32_t*>(&rax30 + 4) = 0;
    rcx31 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx32) + reinterpret_cast<uint64_t>(rbx33));
    tmp64_34 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<uint64_t>(r12_35));
    rax36 = tmp64_34;
    if (reinterpret_cast<unsigned char>(tmp64_34) < reinterpret_cast<unsigned char>(rax30)) 
        goto 0x8b02;
    if (reinterpret_cast<unsigned char>(rax36) >= reinterpret_cast<unsigned char>(rcx31)) 
        goto addr_8586_26;
    rax36 = rcx31;
    addr_8586_26:
    if (reinterpret_cast<unsigned char>(rbx37) >= reinterpret_cast<unsigned char>(rax36)) 
        goto 0x8468;
    if (reinterpret_cast<unsigned char>(rcx31) >= reinterpret_cast<unsigned char>(rax36)) {
        rax36 = rcx31;
    }
    if (rax36 == 0xffffffffffffffff) 
        goto 0x8b02;
    if (r15_38 != *reinterpret_cast<int64_t*>(rbp39 - 0x3e8)) {
        rax41 = fun_2600(r15_40, rax36);
        if (!rax41) 
            goto 0x8b02;
        goto 0x8468;
    }
    rax42 = fun_25c0(rax36, rsi10);
    if (!rax42) 
        goto 0x8b02;
    if (r12_43) 
        goto addr_894a_36;
    goto 0x8468;
    addr_894a_36:
    fun_2590(rax42, r15_44, r12_45);
    goto 0x8468;
    addr_8643_5:
    if ((*reinterpret_cast<struct s20**>(rbp46 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s20**>(rbp46 - 0x3f0))->f1 = 0;
        goto 0x8468;
    }
    if (eax7 >= 0) 
        goto addr_8545_18;
    if (**reinterpret_cast<int32_t**>(rbp47 - 0x3d0)) 
        goto 0x8690;
    eax48 = static_cast<uint32_t>(r14_49->f48) & 0xffffffef;
    eax50 = 22;
    if (*reinterpret_cast<signed char*>(&eax48) != 99) 
        goto addr_8687_42;
    eax50 = 84;
    addr_8687_42:
    **reinterpret_cast<int32_t**>(rbp51 - 0x3d0) = eax50;
}

void fun_85d0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_86c0() {
    int64_t rbp1;
    int64_t rbp2;
    int64_t rsi3;
    int64_t r15_4;
    int64_t r12_5;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0x8805;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int64_t*>(rbp2 - 0x418) = rsi3;
            __asm__("fstp tword [rsp+0x8]");
            fun_23b0(r15_4 + r12_5);
            goto 0x851d;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0x84f3;
        }
    }
}

void fun_8708() {
    int32_t* rdi1;
    int64_t r15_2;
    int64_t r12_3;
    int32_t* rsi4;
    int64_t rdi5;
    int64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    int64_t rbp9;
    int64_t rbp10;

    rdi1 = reinterpret_cast<int32_t*>(r15_2 + r12_3);
    *rdi1 = *rsi4;
    rdi5 = reinterpret_cast<int64_t>(rdi1 + 1);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) != 2) {
            *reinterpret_cast<int64_t*>(rbp9 - 0x418) = rsi6;
            fun_23b0(rdi5);
            goto 0x851d;
        }
    }
    *reinterpret_cast<int64_t*>(rbp10 - 0x418) = rsi6;
    fun_23b0(rdi5);
    goto 0x851d;
}

void fun_8770() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x85f6;
}

void fun_87c0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0x87a0;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0x85ff;
}

void fun_8870() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x85f6;
    goto 0x87a0;
}

void fun_8903() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0x8372;
}

void fun_8aa0() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0x8a27;
}

struct s22 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s23 {
    signed char[8] pad8;
    int64_t f8;
};

struct s24 {
    signed char[16] pad16;
    int64_t f10;
};

struct s25 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_8f88() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s22* rcx3;
    struct s23* rcx4;
    int64_t r11_5;
    struct s24* rcx6;
    uint32_t* rcx7;
    struct s25* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0x8f70; else 
        goto "???";
}

struct s26 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s27 {
    signed char[8] pad8;
    int64_t f8;
};

struct s28 {
    signed char[16] pad16;
    int64_t f10;
};

struct s29 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_8fc0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s26* rcx3;
    struct s27* rcx4;
    int64_t r11_5;
    struct s28* rcx6;
    uint32_t* rcx7;
    struct s29* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0x8fa6;
}

struct s30 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s31 {
    signed char[8] pad8;
    int64_t f8;
};

struct s32 {
    signed char[16] pad16;
    int64_t f10;
};

struct s33 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_8fe0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s30* rcx3;
    struct s31* rcx4;
    int64_t r11_5;
    struct s32* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s33* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0x8fa6;
}

struct s34 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s35 {
    signed char[8] pad8;
    int64_t f8;
};

struct s36 {
    signed char[16] pad16;
    int64_t f10;
};

struct s37 {
    signed char[16] pad16;
    signed char f10;
};

void fun_9000() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s34* rcx3;
    struct s35* rcx4;
    int64_t r11_5;
    struct s36* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s37* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0x8fa6;
}

struct s38 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s39 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_9080() {
    struct s38* rcx1;
    struct s39* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0x8fa6;
}

struct s40 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s41 {
    signed char[8] pad8;
    int64_t f8;
};

struct s42 {
    signed char[16] pad16;
    int64_t f10;
};

struct s43 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_90d0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s40* rcx3;
    struct s41* rcx4;
    int64_t r11_5;
    struct s42* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s43* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0x8fa6;
}

void fun_931c() {
}

void fun_934b() {
    goto 0x9328;
}

void fun_93a0() {
}

void fun_95b0() {
    goto 0x93a3;
}

struct s44 {
    signed char[8] pad8;
    void** f8;
};

struct s45 {
    signed char[8] pad8;
    int64_t f8;
};

struct s46 {
    signed char[8] pad8;
    void** f8;
};

struct s47 {
    signed char[8] pad8;
    void** f8;
};

void fun_9658() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s44* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s45* r15_14;
    void** rax15;
    struct s46* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s47* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0x9d97;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0x9d97;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_25c0(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0x9bb8; else 
                goto "???";
        }
    } else {
        rax15 = fun_2600(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0x9d97;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_9eea_9; else 
            goto addr_96ce_10;
    }
    addr_9824_11:
    rax19 = fun_2590(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_96ce_10:
    r14_20->f8 = rcx12;
    goto 0x91e9;
    addr_9eea_9:
    r13_18 = *r14_21;
    goto addr_9824_11;
}

struct s48 {
    signed char[11] pad11;
    void** fb;
};

struct s49 {
    signed char[80] pad80;
    int64_t f50;
};

struct s50 {
    signed char[80] pad80;
    int64_t f50;
};

struct s51 {
    signed char[8] pad8;
    void** f8;
};

struct s52 {
    signed char[8] pad8;
    void** f8;
};

struct s53 {
    signed char[8] pad8;
    void** f8;
};

struct s54 {
    signed char[72] pad72;
    signed char f48;
};

struct s55 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_98e1() {
    void** ecx1;
    int32_t edx2;
    struct s48* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s49* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s50* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s51* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rax32;
    struct s52* r15_33;
    void** rax34;
    uint64_t r11_35;
    void** v36;
    struct s53* r15_37;
    void*** r13_38;
    struct s54* r12_39;
    signed char bpl40;
    int64_t rax41;
    int64_t* r14_42;
    struct s55* r12_43;
    int64_t rbx44;
    uint64_t r13_45;
    uint64_t* r14_46;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s48*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0x9778;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0x9ecc;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_9a3d_12;
    } else {
        addr_95e4_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_961f_17;
        }
    }
    rax25 = fun_25c0(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0x9baa;
    addr_9b50_19:
    rdx30 = *r15_31;
    rax32 = fun_2590(rdi28, r8_29, rdx30 << 5);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax32;
    addr_9a86_20:
    r15_33->f8 = r8_12;
    goto addr_95e4_13;
    addr_9a3d_12:
    rax34 = fun_2600(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_35;
    r8_12 = rax34;
    if (!rax34) 
        goto 0x9d97;
    if (v36 != r15_37->f8) 
        goto addr_9a86_20;
    rdi28 = r8_12;
    r8_29 = v36;
    goto addr_9b50_19;
    addr_961f_17:
    r13_38 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_38) {
        if (*r13_38 != ecx1) {
            goto 0x977c;
        }
    } else {
        *r13_38 = ecx1;
    }
    r12_39->f48 = bpl40;
    rax41 = *r14_42;
    r12_43->f8 = rbx44;
    r13_45 = reinterpret_cast<uint64_t>(rax41 + 1);
    *r14_46 = r13_45;
    if (r11_26 <= r13_45) 
        goto 0x9660;
    goto 0x91e9;
}

void fun_98ff() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x95c8;
    if (dl2 & 4) 
        goto 0x95c8;
    if (edx3 > 7) 
        goto 0x95c8;
    if (dl4 & 2) 
        goto 0x95c8;
    goto 0x95c8;
}

void fun_9948() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x95c8;
    if (dl2 & 4) 
        goto 0x95c8;
    if (edx3 > 7) 
        goto 0x95c8;
    if (dl4 & 2) 
        goto 0x95c8;
    goto 0x95c8;
}

void fun_9990() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0x95c8;
    if (dl2 & 4) 
        goto 0x95c8;
    if (edx3 > 7) 
        goto 0x95c8;
    if (dl4 & 2) 
        goto 0x95c8;
    goto 0x95c8;
}

void fun_99d8() {
    goto 0x95c8;
}

struct s56 {
    signed char[1] pad1;
    unsigned char f1;
};

void cl_strtold(void** rdi, void* rsi);

struct s57 {
    signed char[1] pad1;
    void** f1;
};

struct s58 {
    signed char[2] pad2;
    signed char f2;
};

void fun_2e70() {
    uint32_t eax1;
    unsigned char* r9_2;
    uint32_t eax3;
    struct s56* r9_4;
    int32_t* rax5;
    void** r9_6;
    void** rsi7;
    void** v8;
    void** r8_9;
    void** r8_10;
    void** rbx11;
    struct s57* r9_12;
    uint64_t rax13;
    void** r8_14;
    struct s58* r9_15;
    void** rax16;
    void* rsp17;
    void** rax18;
    int1_t zf19;
    signed char v20;
    signed char v21;
    void** rsi22;
    int32_t v23;
    void** rsi24;
    int32_t v25;
    signed char v26;
    int32_t v27;

    eax1 = *r9_2;
    if (*reinterpret_cast<signed char*>(&eax1) != 34 && *reinterpret_cast<signed char*>(&eax1) != 39 || (eax3 = r9_4->f1, !*reinterpret_cast<signed char*>(&eax3))) {
        rax5 = fun_23f0();
        *rax5 = 0;
        cl_strtold(r9_6, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8 + 80);
        rsi7 = v8;
        __asm__("fstp tword [rsp+0x10]");
        verify_numeric(r9_6, rsi7);
        r8_9 = r8_10;
    } else {
        __asm__("fild word [rsp+0x10]");
        rbx11 = reinterpret_cast<void**>(&r9_12->f1);
        __asm__("fstp tword [rsp+0x10]");
        rax13 = fun_24a0();
        r8_9 = r8_14;
        if (rax13 > 1 && (r9_15->f2 && (rax16 = fun_24b0(rbx11), rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8 - 8 + 8), rsi7 = rbx11, rax18 = rpl_mbrtowc(reinterpret_cast<int64_t>(rsp17) + 80, rsi7, rax16, reinterpret_cast<int64_t>(rsp17) + 88), r8_9 = r8_14, !(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax18) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax18 == 0))))) {
            __asm__("fild dword [rsp+0x50]");
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r9_15) + reinterpret_cast<unsigned char>(rax18));
            __asm__("fstp tword [rsp+0x10]");
        }
        if (*reinterpret_cast<void***>(rbx11 + 1) && (zf19 = posixly_correct == 0, zf19)) {
            fun_2490();
            *reinterpret_cast<int32_t*>(&rsi7) = 0;
            *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
            fun_2650();
            r8_9 = r8_9;
        }
    }
    if (v20) {
        if (v21) {
            *reinterpret_cast<int32_t*>(&rsi22) = v23;
            *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
            xprintf(r8_9, rsi22, r8_9, rsi22);
            goto 0x2e63;
        } else {
            *reinterpret_cast<int32_t*>(&rsi24) = v25;
            *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
        }
    } else {
        if (v26) {
            *reinterpret_cast<int32_t*>(&rsi24) = v27;
            *reinterpret_cast<int32_t*>(&rsi24 + 4) = 0;
        } else {
            xprintf(r8_9, rsi7, r8_9, rsi7);
            goto 0x2e63;
        }
    }
    xprintf(r8_9, rsi24, r8_9, rsi24);
    goto 0x2e63;
}

void fun_2dbc() {
    goto 0x2b48;
}

void fun_2f7b() {
    signed char v1;
    void** rsi2;
    int32_t v3;
    void** r8_4;
    void** rsi5;
    signed char* r9_6;
    void** r8_7;

    if (v1) {
        *reinterpret_cast<int32_t*>(&rsi2) = v3;
        *reinterpret_cast<int32_t*>(&rsi2 + 4) = 0;
        xprintf(r8_4, rsi2);
        goto 0x2e63;
    } else {
        *reinterpret_cast<int32_t*>(&rsi5) = *r9_6;
        *reinterpret_cast<int32_t*>(&rsi5 + 4) = 0;
        xprintf(r8_7, rsi5);
        goto 0x2e63;
    }
}

void fun_496e() {
    goto 0x45e2;
}

void fun_4b44() {
    goto 0x4afc;
}

void fun_4c0b() {
    goto 0x4738;
}

void fun_4c5d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x4be0;
    goto 0x480f;
}

void fun_4c8f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x4beb;
        goto 0x4610;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x4a8a;
        goto 0x482b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5428;
    if (r10_8 > r15_9) 
        goto addr_4b75_9;
    addr_4b7a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5433;
    goto 0x4764;
    addr_4b75_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_4b7a_10;
}

void fun_4cc2() {
    goto 0x47f7;
}

void fun_5090() {
    goto 0x47f7;
}

void fun_582f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x494c;
    } else {
        goto 0x4ab0;
    }
}

void fun_6cb0() {
}

struct s59 {
    signed char[1] pad1;
    signed char f1;
};

void fun_8360() {
    signed char* r13_1;
    struct s59* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_8aae() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0x8a27;
}

struct s60 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s61 {
    signed char[8] pad8;
    int64_t f8;
};

struct s62 {
    signed char[16] pad16;
    int64_t f10;
};

struct s63 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_90a0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s60* rcx3;
    struct s61* rcx4;
    int64_t r11_5;
    struct s62* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s63* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0x8fa6;
}

void fun_9355() {
    goto 0x9328;
}

void fun_9ca4() {
    goto 0x95c8;
}

void fun_95b8() {
}

void fun_99e8() {
    goto 0x95c8;
}

struct s64 {
    signed char[1] pad1;
    unsigned char f1;
};

int64_t fun_2670(void** rdi, void* rsi);

struct s65 {
    signed char[1] pad1;
    void** f1;
};

struct s66 {
    signed char[2] pad2;
    signed char f2;
};

void fun_2dd8() {
    uint32_t eax1;
    unsigned char* r9_2;
    int64_t rbp3;
    struct s64* r9_4;
    int32_t* rax5;
    void** r9_6;
    void** v7;
    void** rbx8;
    struct s65* r9_9;
    uint64_t rax10;
    struct s66* r9_11;
    void** rax12;
    void* rsp13;
    void** rax14;
    int1_t zf15;
    signed char v16;
    signed char v17;
    signed char v18;

    eax1 = *r9_2;
    if (*reinterpret_cast<signed char*>(&eax1) != 34 && *reinterpret_cast<signed char*>(&eax1) != 39 || (*reinterpret_cast<uint32_t*>(&rbp3) = r9_4->f1, !*reinterpret_cast<signed char*>(&rbp3))) {
        rax5 = fun_23f0();
        *rax5 = 0;
        fun_2670(r9_6, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8 + 80);
        verify_numeric(r9_6, v7);
    } else {
        rbx8 = reinterpret_cast<void**>(&r9_9->f1);
        rax10 = fun_24a0();
        if (rax10 > 1 && (r9_11->f2 && (rax12 = fun_24b0(rbx8), rsp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 + 8 - 8 + 8), rax14 = rpl_mbrtowc(reinterpret_cast<int64_t>(rsp13) + 80, rbx8, rax12, reinterpret_cast<int64_t>(rsp13) + 88), !(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax14) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax14 == 0))))) {
            rbx8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r9_11) + reinterpret_cast<unsigned char>(rax14));
        }
        if (*reinterpret_cast<void***>(rbx8 + 1) && (zf15 = posixly_correct == 0, zf15)) {
            fun_2490();
            fun_2650();
        }
    }
    if (v16) {
        if (v17) 
            goto 0x3136; else 
            goto "???";
    } else {
        if (!v18) 
            goto 0x2f62;
    }
}

void fun_4ccc() {
    goto 0x4c67;
}

void fun_509a() {
    goto 0x4bbd;
}

void fun_6d10() {
    fun_2490();
    goto fun_26b0;
}

void fun_8840() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0x85f6;
    goto 0x87a0;
}

void fun_8abc() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0x8a27;
}

struct s67 {
    int32_t f0;
    int32_t f4;
};

struct s68 {
    int32_t f0;
    int32_t f4;
};

struct s69 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s70 {
    signed char[8] pad8;
    int64_t f8;
};

struct s71 {
    signed char[8] pad8;
    int64_t f8;
};

struct s72 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_9050(struct s67* rdi, struct s68* rsi) {
    struct s69* rcx3;
    struct s70* rcx4;
    struct s71* rcx5;
    struct s72* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0x8fa6;
}

void fun_935f() {
    goto 0x9328;
}

void fun_9cae() {
    goto 0x95c8;
}

void fun_499d() {
    goto 0x45e2;
}

void fun_4cd8() {
    goto 0x4c67;
}

void fun_50a7() {
    goto 0x4c0e;
}

void fun_6d50() {
    fun_2490();
    goto fun_26b0;
}

void fun_8acb() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0x8a27;
}

void fun_9369() {
    goto 0x9328;
}

void fun_49ca() {
    goto 0x45e2;
}

void fun_4ce4() {
    goto 0x4be0;
}

void fun_6d90() {
    fun_2490();
    goto fun_26b0;
}

void fun_9373() {
    goto 0x9328;
}

void fun_49ec() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5380;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x48b1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x48b1;
    }
    if (v11) 
        goto 0x56e3;
    if (r10_12 > r15_13) 
        goto addr_5733_8;
    addr_5738_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5471;
    addr_5733_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_5738_9;
}

void fun_6de0() {
    void** rax1;
    void** rbp2;
    int64_t v3;

    rax1 = fun_2490();
    fun_26b0(rbp2, 1, rax1, rbp2, 1, rax1);
    goto v3;
}

void fun_6e38() {
    fun_2490();
    goto 0x6e09;
}

void fun_6e70() {
    void** rax1;
    void** rbp2;
    int64_t v3;

    rax1 = fun_2490();
    fun_26b0(rbp2, 1, rax1, rbp2, 1, rax1);
    goto v3;
}

void fun_6ee8() {
    fun_2490();
    goto 0x6eab;
}
