
uint72_t g28;

struct s0 {
    int32_t f0;
    void** f1;
};

int32_t xstrtoumax();

struct s0* quote(void** rdi, ...);

void** fun_24b0();

int32_t fun_26c0();

void fun_24e0();

struct s1 {
    signed char f0;
    void** f1;
};

struct s1* fun_2650(void** rdi, void** rsi, void** rdx, struct s0* rcx, int64_t r8);

void** xmalloc(int64_t rdi, void** rsi, ...);

void** outlist_end = reinterpret_cast<void**>(96);

uint72_t g18;

struct s2 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
};

void** empty_filler = reinterpret_cast<void**>(0);

void** string_to_join_field(void** rdi, void** rsi) {
    void** rax3;
    struct s0* rcx4;
    int32_t eax5;
    void** rax6;
    void* v7;
    struct s0* rax8;
    void** rax9;
    void** rdx10;
    void** rbp11;
    void** r13_12;
    struct s1* rax13;
    int64_t rax14;
    void** rbp15;
    void** r12d16;
    int32_t edx17;
    uint32_t eax18;
    void** rax19;
    void** rax20;
    int1_t below_or_equal21;
    struct s2* tmp64_22;
    void** rdi23;
    int64_t v24;
    int64_t v25;

    rax3 = *reinterpret_cast<void***>(&g28);
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 24);
    eax5 = xstrtoumax();
    if (eax5 == 1) {
        rax6 = reinterpret_cast<void**>(0xfffffffffffffffe);
    } else {
        if (eax5 || !v7) {
            rax8 = quote(rdi, rdi);
            rax9 = fun_24b0();
            rcx4 = rax8;
            *reinterpret_cast<int32_t*>(&rdi) = 1;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            rdx10 = rax9;
            fun_26c0();
            goto addr_397b_5;
        } else {
            rax6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v7) - 1);
        }
    }
    rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (!rdx10) {
        return rax6;
    }
    addr_397b_5:
    fun_24e0();
    rbp11 = rdi;
    while (1) {
        *reinterpret_cast<int32_t*>(&r13_12) = 0;
        *reinterpret_cast<int32_t*>(&r13_12 + 4) = 0;
        rax13 = fun_2650(rbp11, ", \t", rdx10, rcx4, 0x9ae8);
        if (rax13) {
            rax13->f0 = 0;
            r13_12 = reinterpret_cast<void**>(&rax13->f1);
        }
        *reinterpret_cast<uint32_t*>(&rax14) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp11));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rax14) == 48) {
            if (*reinterpret_cast<void***>(rbp11 + 1)) 
                break;
            *reinterpret_cast<int32_t*>(&rbp15) = 0;
            *reinterpret_cast<int32_t*>(&rbp15 + 4) = 0;
            r12d16 = reinterpret_cast<void**>(0);
        } else {
            edx17 = static_cast<int32_t>(rax14 - 49);
            if (*reinterpret_cast<unsigned char*>(&edx17) > 1) 
                goto addr_3a3b_16;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp11 + 1) == 46)) 
                goto addr_3a9f_18;
            eax18 = *reinterpret_cast<uint32_t*>(&rax14) - 48;
            r12d16 = reinterpret_cast<void**>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&eax18)));
            rax19 = string_to_join_field(rbp11 + 2, ", \t");
            rbp15 = rax19;
        }
        rax20 = xmalloc(24, ", \t", 24, ", \t");
        rdx10 = outlist_end;
        *reinterpret_cast<void***>(rax20) = r12d16;
        *reinterpret_cast<void***>(rax20 + 8) = rbp15;
        *reinterpret_cast<void***>(rax20 + 16) = reinterpret_cast<void**>(0);
        outlist_end = rax20;
        *reinterpret_cast<void***>(rdx10 + 16) = rax20;
        if (!r13_12) 
            goto addr_3a30_21;
        rbp11 = r13_12;
    }
    addr_3a6d_23:
    quote(rbp11, rbp11);
    fun_24b0();
    fun_26c0();
    addr_3a9f_18:
    quote(rbp11, rbp11);
    fun_24b0();
    fun_26c0();
    below_or_equal21 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g18)) <= reinterpret_cast<unsigned char>(1);
    if (below_or_equal21 || (tmp64_22 = reinterpret_cast<struct s2*>(*reinterpret_cast<void***>(&g28) + 16), tmp64_22->f8 == 0)) {
        rdi23 = empty_filler;
        if (!rdi23) {
            goto v24;
        }
    }
    addr_3a30_21:
    goto v25;
    addr_3a3b_16:
    quote(rbp11, rbp11);
    fun_24b0();
    fun_26c0();
    goto addr_3a6d_23;
}

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

signed char ignore_case = 0;

signed char hard_LC_COLLATE = 0;

uint32_t fun_25a0(void** rdi, void** rsi, void** rdx, void** rcx, ...);

uint32_t memcasecmp(void** rdi);

void set_join_field(void*** rdi, void** rsi, ...) {
    void** rbp3;
    void** rax4;
    void** rcx5;
    void** rax6;
    int1_t below_or_equal7;
    int64_t v8;
    struct s3* tmp64_9;
    void** rdi10;
    int64_t v11;
    void** tmp64_12;
    void** rsi13;
    int64_t v14;
    int1_t zf15;
    int1_t zf16;
    void** rdx17;
    uint32_t eax18;
    int64_t v19;

    rbp3 = *rdi;
    if (rbp3 == 0xffffffffffffffff || rbp3 == rsi) {
        *rdi = rsi;
        return;
    }
    rax4 = fun_24b0();
    rcx5 = rbp3 + 1;
    fun_26c0();
    rax6 = *reinterpret_cast<void***>(&g18);
    below_or_equal7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<int64_t>(&g18) + 1)) <= reinterpret_cast<unsigned char>(rax4);
    if (!below_or_equal7) 
        goto addr_37f1_5;
    if (reinterpret_cast<unsigned char>(rcx5) < reinterpret_cast<unsigned char>(rax6)) {
        goto addr_38ac_8;
    }
    addr_3867_10:
    goto v8;
    addr_37f1_5:
    tmp64_9 = reinterpret_cast<struct s3*>((reinterpret_cast<unsigned char>(rax4) << 4) + reinterpret_cast<uint64_t>(*reinterpret_cast<void****>(reinterpret_cast<int64_t>(&g28) + 1)));
    rdi10 = tmp64_9->f0;
    if (reinterpret_cast<unsigned char>(rcx5) >= reinterpret_cast<unsigned char>(rax6)) {
        goto v11;
    }
    tmp64_12 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx5) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    rsi13 = *reinterpret_cast<void***>(tmp64_12);
    if (!tmp64_9->f8) {
        addr_38ac_8:
        goto v14;
    } else {
        if (!*reinterpret_cast<void***>(tmp64_12 + 8)) {
            goto addr_3867_10;
        } else {
            zf15 = ignore_case == 0;
            if (zf15) {
                zf16 = hard_LC_COLLATE == 0;
                if (zf16) {
                    rdx17 = *reinterpret_cast<void***>(tmp64_12 + 8);
                    if (reinterpret_cast<unsigned char>(tmp64_9->f8) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(tmp64_12 + 8))) {
                        rdx17 = tmp64_9->f8;
                    }
                    eax18 = fun_25a0(rdi10, rsi13, rdx17, tmp64_12, rdi10, rsi13, rdx17, tmp64_12);
                }
            } else {
                if (reinterpret_cast<unsigned char>(tmp64_9->f8) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(tmp64_12 + 8))) {
                }
                eax18 = memcasecmp(rdi10);
            }
            if (!eax18) {
                goto v19;
            }
        }
    }
}

void add_field_list(void** rdi, void** rsi);

int64_t quotearg_style(int64_t rdi, void** rsi);

void usage();

void** ge1b8 = reinterpret_cast<void**>(0);

void** g_names = reinterpret_cast<void**>(0);

/* add_file_name.constprop.0 */
void add_file_name_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9, void** a7) {
    int32_t r15d8;
    void** edx9;
    int64_t rax10;
    void** rdi11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    void* rcx15;
    void** rax16;
    int64_t rax17;

    r15d8 = r8->f0;
    if (r15d8 == 2) {
        edx9 = *reinterpret_cast<void***>(rdx);
        *reinterpret_cast<int32_t*>(&rax10) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        *reinterpret_cast<unsigned char*>(&rax10) = reinterpret_cast<uint1_t>(edx9 == 0);
        rdi11 = *reinterpret_cast<void***>(0xe1b0 + rax10 * 8);
        if (*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<uint64_t>(rax10 * 4)) == 2) {
            *reinterpret_cast<void***>(rcx + 4) = *reinterpret_cast<void***>(rcx + 4) - 1;
            rax12 = string_to_join_field(rdi11, rsi);
            set_join_field(0xe018, rax12);
            edx9 = edx9;
        } else {
            if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<uint64_t>(rax10 * 4))) > reinterpret_cast<signed char>(2)) {
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<uint64_t>(rax10 * 4)) == 3)) {
                    add_field_list(rdi11, rsi);
                    edx9 = edx9;
                }
            } else {
                if (!*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<uint64_t>(rax10 * 4))) {
                    quotearg_style(4, rdi);
                    fun_24b0();
                    fun_26c0();
                    usage();
                } else {
                    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<uint64_t>(rax10 * 4)) == 1)) {
                        *reinterpret_cast<void***>(rcx) = *reinterpret_cast<void***>(rcx) - 1;
                        rax13 = string_to_join_field(rdi11, rsi);
                        set_join_field(0xe020, rax13);
                        edx9 = edx9;
                    }
                }
            }
        }
        if (!edx9) {
            *reinterpret_cast<int32_t*>(&rax14) = 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rcx15) = 4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx15) + 4) = 0;
        } else {
            *reinterpret_cast<int32_t*>(&rcx15) = 4;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx15) + 4) = 0;
            *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(rdx + 4);
            rax16 = ge1b8;
            g_names = rax16;
            *reinterpret_cast<int32_t*>(&rax14) = 8;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        }
    } else {
        rax17 = r15d8;
        ++r15d8;
        rcx15 = reinterpret_cast<void*>(rax17 * 4);
        rax14 = rax17 << 3;
    }
    *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<uint64_t>(rcx15)) = *reinterpret_cast<void***>(r9);
    *reinterpret_cast<void***>(0xe1b0 + rax14) = rdi;
    r8->f0 = r15d8;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r9) == 3)) {
        *reinterpret_cast<void***>(a7) = reinterpret_cast<void**>(3);
    }
    return;
}

void** x2nrealloc(void** rdi, void*** rsi, int64_t rdx);

void fun_2560(void*** rdi);

signed char get_line(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9);

signed char getseq(void** rdi, void** rsi, int32_t edx, void** rcx, struct s0* r8, void** r9) {
    void** r13_7;
    void** r14_8;
    void** rax9;
    void** rdx10;
    void** rdx11;
    signed char al12;

    r13_7 = *reinterpret_cast<void***>(rsi);
    r14_8 = *reinterpret_cast<void***>(rsi + 16);
    if (r13_7 == *reinterpret_cast<void***>(rsi + 8) && (rax9 = x2nrealloc(r14_8, rsi + 8, 8), r13_7 = *reinterpret_cast<void***>(rsi), rdx10 = *reinterpret_cast<void***>(rsi + 8), *reinterpret_cast<void***>(rsi + 16) = rax9, r14_8 = rax9, reinterpret_cast<unsigned char>(r13_7) < reinterpret_cast<unsigned char>(rdx10))) {
        fun_2560(rax9 + reinterpret_cast<unsigned char>(r13_7) * 8);
    }
    *reinterpret_cast<int32_t*>(&rdx11) = edx;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    al12 = get_line(rdi, r14_8 + reinterpret_cast<unsigned char>(r13_7) * 8, rdx11, rcx, r8, r9);
    if (al12) {
        *reinterpret_cast<void***>(rsi) = *reinterpret_cast<void***>(rsi) + 1;
    }
    return al12;
}

uint32_t tab = 0xffffffff;

struct s4 {
    int32_t f0;
    signed char[4] pad8;
    void** f8;
    signed char[7] pad16;
    struct s4* f10;
};

struct s4* ge170 = reinterpret_cast<struct s4*>(0);

void** join_field_1 = reinterpret_cast<void**>(0xff);

void** join_field_2 = reinterpret_cast<void**>(0xff);

void prfield(void** rdi, void** rsi);

struct s5 {
    signed char[40] pad40;
    signed char* f28;
    signed char* f30;
};

struct s5* stdout = reinterpret_cast<struct s5*>(0);

void fun_2520();

unsigned char eolchar = 10;

void** autocount_1 = reinterpret_cast<void**>(0);

void prfields(void** rdi, void** rsi, void** rdx);

void** autocount_2 = reinterpret_cast<void**>(0);

void prjoin(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9) {
    void** r13_7;
    uint32_t r12d8;
    void** rbp9;
    struct s4* rbx10;
    void** rdi11;
    void** rsi12;
    void** rsi13;
    void** rdi14;
    struct s5* rdi15;
    signed char* rax16;
    void** rdi17;
    void** rsi18;
    struct s5* rdi19;
    uint32_t edx20;
    signed char* rax21;
    void** rdx22;
    void** rsi23;
    void** rdx24;
    void** rsi25;

    r13_7 = rsi;
    r12d8 = tab;
    rbp9 = rdi;
    rbx10 = ge170;
    if (reinterpret_cast<int32_t>(r12d8) < reinterpret_cast<int32_t>(0)) {
        r12d8 = 32;
    }
    if (rbx10) {
        while (1) {
            if (!rbx10->f0) {
                rdi11 = join_field_1;
                rsi12 = rbp9;
                if (rbp9 == 0xe120) {
                    rdi11 = join_field_2;
                    rsi12 = r13_7;
                }
                prfield(rdi11, rsi12);
                rbx10 = rbx10->f10;
                if (!rbx10) 
                    break;
            } else {
                rsi13 = r13_7;
                rdi14 = rbx10->f8;
                if (rbx10->f0 == 1) {
                    rsi13 = rbp9;
                }
                prfield(rdi14, rsi13);
                rbx10 = rbx10->f10;
                if (!rbx10) 
                    break;
            }
            rdi15 = stdout;
            rax16 = rdi15->f28;
            if (reinterpret_cast<uint64_t>(rax16) >= reinterpret_cast<uint64_t>(rdi15->f30)) {
                fun_2520();
            } else {
                rdi15->f28 = rax16 + 1;
                *rax16 = *reinterpret_cast<signed char*>(&r12d8);
            }
        }
    } else {
        rdi17 = join_field_1;
        rsi18 = rbp9;
        if (rbp9 == 0xe120) 
            goto addr_3da0_16; else 
            goto addr_3d56_17;
    }
    addr_3cef_18:
    rdi19 = stdout;
    edx20 = eolchar;
    rax21 = rdi19->f28;
    if (reinterpret_cast<uint64_t>(rax21) < reinterpret_cast<uint64_t>(rdi19->f30)) {
        rdi19->f28 = rax21 + 1;
        *rax21 = *reinterpret_cast<signed char*>(&edx20);
        return;
    }
    addr_3da0_16:
    rdi17 = join_field_2;
    rsi18 = r13_7;
    addr_3d56_17:
    prfield(rdi17, rsi18);
    rdx22 = autocount_1;
    rsi23 = join_field_1;
    prfields(rbp9, rsi23, rdx22);
    rdx24 = autocount_2;
    rsi25 = join_field_2;
    prfields(r13_7, rsi25, rdx24);
    goto addr_3cef_18;
}

struct s6 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

uint32_t keycmp(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9) {
    void** rax7;
    void** r12_8;
    uint32_t eax9;
    struct s6* rdx10;
    void** rdi11;
    void** rbp12;
    uint32_t eax13;
    void** rcx14;
    void** rsi15;
    int1_t zf16;
    int1_t zf17;
    void** rdx18;
    uint32_t eax19;

    rax7 = *reinterpret_cast<void***>(rsi + 24);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 24)) <= reinterpret_cast<unsigned char>(rdx)) {
        if (reinterpret_cast<unsigned char>(rcx) < reinterpret_cast<unsigned char>(rax7)) {
            r12_8 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 40)) + 8);
        } else {
            eax9 = 0;
            goto addr_3867_5;
        }
    } else {
        rdx10 = reinterpret_cast<struct s6*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)));
        rdi11 = rdx10->f0;
        rbp12 = rdx10->f8;
        if (reinterpret_cast<unsigned char>(rcx) >= reinterpret_cast<unsigned char>(rax7)) {
            eax13 = 0;
            *reinterpret_cast<unsigned char*>(&eax13) = reinterpret_cast<uint1_t>(!!rbp12);
            return eax13;
        }
        rcx14 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 40)));
        rsi15 = *reinterpret_cast<void***>(rcx14);
        r12_8 = *reinterpret_cast<void***>(rcx14 + 8);
        if (rbp12) 
            goto addr_3821_9;
    }
    return *reinterpret_cast<uint32_t*>(&rax7) - (*reinterpret_cast<uint32_t*>(&rax7) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax7) < *reinterpret_cast<uint32_t*>(&rax7) + reinterpret_cast<uint1_t>(!!r12_8)));
    addr_3867_5:
    return eax9;
    addr_3821_9:
    if (!r12_8) {
        eax9 = 1;
        goto addr_3867_5;
    } else {
        zf16 = ignore_case == 0;
        if (zf16) {
            zf17 = hard_LC_COLLATE == 0;
            if (zf17) {
                rdx18 = r12_8;
                if (reinterpret_cast<unsigned char>(rbp12) <= reinterpret_cast<unsigned char>(r12_8)) {
                    rdx18 = rbp12;
                }
                eax9 = fun_25a0(rdi11, rsi15, rdx18, rcx14);
            }
        } else {
            if (reinterpret_cast<unsigned char>(rbp12) <= reinterpret_cast<unsigned char>(r12_8)) {
            }
            eax9 = memcasecmp(rdi11);
        }
        if (!eax9) {
            eax19 = 0;
            *reinterpret_cast<unsigned char*>(&eax19) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp12) > reinterpret_cast<unsigned char>(r12_8));
            return eax19 - reinterpret_cast<uint1_t>(eax19 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp12) < reinterpret_cast<unsigned char>(r12_8))));
        }
    }
}

void fun_2400(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9);

void delseq(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9) {
    void** r12_7;
    void** rbx8;
    void** rbp9;
    void** rdi10;
    void** rdi11;
    void** rax12;

    r12_7 = rdi;
    *reinterpret_cast<int32_t*>(&rbx8) = 0;
    *reinterpret_cast<int32_t*>(&rbx8 + 4) = 0;
    if (*reinterpret_cast<void***>(rdi + 8)) {
        do {
            rbp9 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_7 + 16) + reinterpret_cast<unsigned char>(rbx8) * 8);
            if (rbp9) {
                rdi10 = *reinterpret_cast<void***>(rbp9 + 40);
                fun_2400(rdi10, rsi, rdx, rcx, r8, r9);
                *reinterpret_cast<void***>(rbp9 + 40) = reinterpret_cast<void**>(0);
                rdi11 = *reinterpret_cast<void***>(rbp9 + 16);
                fun_2400(rdi11, rsi, rdx, rcx, r8, r9);
                rax12 = *reinterpret_cast<void***>(r12_7 + 16);
                *reinterpret_cast<void***>(rbp9 + 16) = reinterpret_cast<void**>(0);
                rbp9 = *reinterpret_cast<void***>(rax12 + reinterpret_cast<unsigned char>(rbx8) * 8);
            }
            ++rbx8;
            fun_2400(rbp9, rsi, rdx, rcx, r8, r9);
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 8)) > reinterpret_cast<unsigned char>(rbx8));
    }
}

void** xcalloc(int64_t rdi, int64_t rsi);

int64_t readlinebuffer_delim(void** rdi, void** rsi, void** rdx);

int32_t* fun_2420();

struct s7 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int32_t check_input_order = 0;

signed char seen_unpairable = 0;

unsigned char** fun_2770(void** rdi, void** rsi, void** rdx, void** rcx);

struct s8 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s9 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s10 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s0* fun_2590(void** rdi, int64_t rsi, void** rdx);

struct s11 {
    void** f0;
    signed char[7] pad8;
    void* f8;
};

signed char get_line(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9) {
    int64_t rbx7;
    void** r15_8;
    int32_t v9;
    void** rax10;
    void** rax11;
    void** rdx12;
    int64_t rax13;
    void** rdi14;
    void** rdi15;
    void** rax16;
    int32_t* rax17;
    void** rdx18;
    struct s7* rsi19;
    void** r13_20;
    void** r14_21;
    void** rax22;
    void** rdx23;
    void** r13_24;
    void** r14_25;
    void** r12_26;
    void** rdi27;
    int32_t eax28;
    int1_t zf29;
    void** rdx30;
    uint32_t eax31;
    uint32_t ebp32;
    void** rdx33;
    signed char al34;
    int64_t v35;
    unsigned char** rax36;
    unsigned char* rdx37;
    unsigned char** r14_38;
    void** rax39;
    void** rdi40;
    int64_t rsi41;
    void** rbp42;
    void** r13_43;
    int64_t r9_44;
    void** rax45;
    void** rdx46;
    struct s8* rdx47;
    int64_t rsi48;
    struct s9* rdi49;
    void** rax50;
    struct s10* rdx51;
    void** v52;
    void*** v53;
    int64_t rsi54;
    struct s0* rax55;
    void** rdx56;
    void** rax57;
    void* r14_58;
    struct s11* rax59;

    rbx7 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx + 0xffffffffffffffff));
    r15_8 = *reinterpret_cast<void***>(rsi);
    v9 = *reinterpret_cast<int32_t*>(&rdx);
    if (*reinterpret_cast<void***>(0xe1d0 + rbx7 * 8) != r15_8) {
        if (!r15_8) 
            goto addr_415e_3; else 
            goto addr_3ebb_4;
    }
    rax10 = *reinterpret_cast<void***>(0xe1a0 + rbx7 * 8);
    *reinterpret_cast<void***>(0xe1a0 + rbx7 * 8) = r15_8;
    r15_8 = rax10;
    *reinterpret_cast<void***>(rsi) = rax10;
    if (r15_8) {
        addr_3ebb_4:
        *reinterpret_cast<void***>(r15_8 + 24) = reinterpret_cast<void**>(0);
    } else {
        addr_415e_3:
        rax11 = xcalloc(1, 48);
        *reinterpret_cast<void***>(rsi) = rax11;
        r15_8 = rax11;
    }
    *reinterpret_cast<int32_t*>(&rdx12) = reinterpret_cast<signed char>(eolchar);
    *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
    rax13 = readlinebuffer_delim(r15_8, rdi, rdx12);
    if (!rax13) {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32)) {
            rdi14 = *reinterpret_cast<void***>(r15_8 + 40);
            fun_2400(rdi14, rdi, rdx12, rcx, r8, r9);
            *reinterpret_cast<void***>(r15_8 + 40) = reinterpret_cast<void**>(0);
            rdi15 = *reinterpret_cast<void***>(r15_8 + 16);
            fun_2400(rdi15, rdi, rdx12, rcx, r8, r9);
            *reinterpret_cast<void***>(r15_8 + 16) = reinterpret_cast<void**>(0);
            return 0;
        }
        rax16 = fun_24b0();
        rax17 = fun_2420();
        rdx18 = rax16;
        *reinterpret_cast<int32_t*>(&rsi19) = *rax17;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi19) + 4) = 0;
        fun_26c0();
        r13_20 = rsi19->f0;
        r14_21 = rsi19->f10;
        if (r13_20 != rsi19->f8) 
            goto addr_434e_11;
        rax22 = x2nrealloc(r14_21, &rsi19->f8, 8);
        r13_20 = rsi19->f0;
        rdx23 = rsi19->f8;
        rsi19->f10 = rax22;
        r14_21 = rax22;
        if (reinterpret_cast<unsigned char>(r13_20) < reinterpret_cast<unsigned char>(rdx23)) 
            goto addr_4394_13;
    } else {
        r13_24 = *reinterpret_cast<void***>(r15_8 + 16);
        *reinterpret_cast<int64_t*>(0xe1c0 + rbx7 * 8) = *reinterpret_cast<int64_t*>(0xe1c0 + rbx7 * 8) + 1;
        r14_25 = *reinterpret_cast<void***>(r15_8 + 8) + 0xffffffffffffffff;
        r12_26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_24) + reinterpret_cast<unsigned char>(r14_25));
        if (r13_24 == r12_26) {
            addr_3fdb_15:
            rdi27 = *reinterpret_cast<void***>(0xe1d0 + rbx7 * 8);
            if (rdi27 && ((eax28 = check_input_order, eax28 != 2) && ((eax28 == 1 || (zf29 = seen_unpairable == 0, !zf29)) && !*reinterpret_cast<signed char*>(0xe198 + rbx7)))) {
                rdx30 = join_field_1;
                if (v9 != 1) {
                    rdx30 = join_field_2;
                }
                eax31 = keycmp(rdi27, r15_8, rdx30, rdx30, r8, r9);
                if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax31) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax31 == 0))) {
                    if (*reinterpret_cast<void***>(r15_8 + 8)) {
                        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 8) + 0xffffffffffffffff) > reinterpret_cast<unsigned char>(0x7fffffff)) {
                        }
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 16)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 8)) + 0xffffffffffffffff) != 10 && reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 8)) <= reinterpret_cast<unsigned char>(0x7fffffff)) {
                        }
                    }
                    fun_24b0();
                    fun_26c0();
                    *reinterpret_cast<signed char*>(0xe198 + rbx7) = 1;
                    goto addr_40dc_25;
                }
            }
        } else {
            ebp32 = tab;
            if (reinterpret_cast<int32_t>(ebp32) < reinterpret_cast<int32_t>(0)) 
                goto addr_41b8_27;
            if (ebp32 == 10) 
                goto addr_41a0_29; else 
                goto addr_3f1b_30;
        }
    }
    addr_434e_11:
    *reinterpret_cast<int32_t*>(&rdx33) = *reinterpret_cast<int32_t*>(&rdx18);
    *reinterpret_cast<int32_t*>(&rdx33 + 4) = 0;
    al34 = get_line(1, r14_21 + reinterpret_cast<unsigned char>(r13_20) * 8, rdx33, rcx, r8, r9);
    if (al34) {
        rsi19->f0 = rsi19->f0 + 1;
    }
    goto v35;
    addr_4394_13:
    fun_2560(rax22 + reinterpret_cast<unsigned char>(r13_20) * 8);
    goto addr_434e_11;
    addr_40dc_25:
    *reinterpret_cast<void***>(0xe1d0 + rbx7 * 8) = r15_8;
    return 1;
    addr_41a0_29:
    if (reinterpret_cast<int32_t>(ebp32) < reinterpret_cast<int32_t>(0)) {
        addr_41b8_27:
        rax36 = fun_2770(r15_8, rdi, rdx12, rcx);
        rdx37 = *rax36;
        r14_38 = rax36;
    } else {
        rax39 = *reinterpret_cast<void***>(r15_8 + 24);
        rdi40 = *reinterpret_cast<void***>(r15_8 + 40);
        goto addr_3fb8_34;
    }
    do {
        *reinterpret_cast<uint32_t*>(&rsi41) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_24));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
        rbp42 = r13_24;
        ++r13_24;
        if (rdx37[rsi41 * 2] & 1) 
            continue;
        if (*reinterpret_cast<signed char*>(&rsi41) != 10) 
            break;
    } while (r12_26 != r13_24);
    goto addr_4245_38;
    rax39 = *reinterpret_cast<void***>(r15_8 + 24);
    rdi40 = *reinterpret_cast<void***>(r15_8 + 40);
    do {
        r13_43 = rbp42 + 1;
        if (r12_26 == r13_43) {
            *reinterpret_cast<int32_t*>(&r9) = 1;
            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        } else {
            do {
                *reinterpret_cast<uint32_t*>(&r9_44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_43));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_44) + 4) = 0;
                if (rdx37[r9_44 * 2] & 1) 
                    goto addr_4250_43;
                if (*reinterpret_cast<signed char*>(&r9_44) == 10) 
                    goto addr_4250_43;
                ++r13_43;
            } while (r12_26 != r13_43);
            goto addr_4210_46;
        }
        addr_4216_47:
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 32)) <= reinterpret_cast<unsigned char>(rax39)) 
            goto addr_42c8_48; else 
            break;
        addr_4250_43:
        r9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_43) - reinterpret_cast<unsigned char>(rbp42));
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 32)) <= reinterpret_cast<unsigned char>(rax39)) {
            addr_42c8_48:
            rax45 = x2nrealloc(rdi40, r15_8 + 32, 16);
            r9 = r9;
            *reinterpret_cast<void***>(r15_8 + 40) = rax45;
            rdi40 = rax45;
            rax39 = *reinterpret_cast<void***>(r15_8 + 24);
            goto addr_425c_49;
        } else {
            addr_425c_49:
            rdx46 = rax39;
            ++rax39;
            rdx47 = reinterpret_cast<struct s8*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx46) << 4) + reinterpret_cast<unsigned char>(rdi40));
            rdx47->f0 = rbp42;
            rdx47->f8 = r9;
            *reinterpret_cast<void***>(r15_8 + 24) = rax39;
            if (r12_26 == r13_43) 
                goto addr_3fdb_15;
        }
        rbp42 = r13_43 + 1;
        if (r12_26 == rbp42) 
            goto addr_42a3_51;
        rdx37 = *r14_38;
        do {
            *reinterpret_cast<uint32_t*>(&rsi48) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp42));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi48) + 4) = 0;
            if (rdx37[rsi48 * 2] & 1) 
                continue;
            if (*reinterpret_cast<signed char*>(&rsi48) != 10) 
                goto addr_42b0_55;
            ++rbp42;
        } while (r12_26 != rbp42);
        goto addr_42a3_51;
        addr_4210_46:
        r9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_26) - reinterpret_cast<unsigned char>(rbp42));
        goto addr_4216_47;
        addr_42b0_55:
    } while (r12_26 != rbp42);
    goto addr_42b9_57;
    rdi49 = reinterpret_cast<struct s9*>(reinterpret_cast<unsigned char>(rdi40) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax39) << 4));
    rdi49->f0 = rbp42;
    rdi49->f8 = r9;
    *reinterpret_cast<void***>(r15_8 + 24) = rax39 + 1;
    goto addr_3fdb_15;
    addr_42a3_51:
    *reinterpret_cast<int32_t*>(&r14_25) = 0;
    *reinterpret_cast<int32_t*>(&r14_25 + 4) = 0;
    r13_24 = r12_26;
    addr_3fb8_34:
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 32)) <= reinterpret_cast<unsigned char>(rax39)) {
        rax50 = x2nrealloc(rdi40, r15_8 + 32, 16);
        *reinterpret_cast<void***>(r15_8 + 40) = rax50;
        rdi40 = rax50;
        rax39 = *reinterpret_cast<void***>(r15_8 + 24);
    }
    rdx51 = reinterpret_cast<struct s10*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax39) << 4) + reinterpret_cast<unsigned char>(rdi40));
    rdx51->f0 = r13_24;
    rdx51->f8 = r14_25;
    *reinterpret_cast<void***>(r15_8 + 24) = rax39 + 1;
    goto addr_3fdb_15;
    addr_42b9_57:
    r13_24 = r12_26;
    *reinterpret_cast<int32_t*>(&r14_25) = 0;
    *reinterpret_cast<int32_t*>(&r14_25 + 4) = 0;
    goto addr_3fb8_34;
    addr_4245_38:
    goto addr_3fdb_15;
    addr_3f1b_30:
    v52 = *reinterpret_cast<void***>(r15_8 + 40);
    v53 = reinterpret_cast<void***>(r15_8 + 32);
    while (*reinterpret_cast<uint32_t*>(&rsi54) = ebp32, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi54) + 4) = 0, r14_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_26) - reinterpret_cast<unsigned char>(r13_24)), rax55 = fun_2590(r13_24, rsi54, r14_25), r8 = rax55, !!rax55) {
        rdx56 = *reinterpret_cast<void***>(r15_8 + 24);
        rax57 = v52;
        r14_58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r8) - reinterpret_cast<unsigned char>(r13_24));
        if (reinterpret_cast<unsigned char>(rdx56) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 32))) {
            rax57 = x2nrealloc(rax57, v53, 16);
            rdx56 = *reinterpret_cast<void***>(r15_8 + 24);
            ebp32 = tab;
            *reinterpret_cast<void***>(r15_8 + 40) = rax57;
            r8 = r8;
            v52 = rax57;
        }
        rax59 = reinterpret_cast<struct s11*>(reinterpret_cast<unsigned char>(rax57) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx56) << 4));
        rax59->f0 = r13_24;
        r13_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r8) + 1);
        rax59->f8 = r14_58;
        *reinterpret_cast<void***>(r15_8 + 24) = rdx56 + 1;
    }
    rax39 = *reinterpret_cast<void***>(r15_8 + 24);
    rdi40 = v52;
    goto addr_3fb8_34;
}

void prfield(void** rdi, void** rsi) {
    void** rdi3;

    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 24)) <= reinterpret_cast<unsigned char>(rdi) || !*reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdi) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 40)) + 8)) {
        rdi3 = empty_filler;
        if (!rdi3) {
            return;
        }
    }
}

signed char autoformat = 0;

void prfields(void** rdi, void** rsi, void** rdx) {
    void** r13_4;
    void** rbp5;
    int1_t zf6;
    void** v7;
    uint32_t r15d8;
    void** r14_9;
    uint32_t r12d10;
    void** rbx11;
    struct s5* rdi12;
    signed char* rax13;
    void** rdi14;
    void** rbx15;
    struct s5* rdi16;
    signed char* rax17;
    void** rdi18;

    r13_4 = rdx;
    rbp5 = rdi;
    zf6 = autoformat == 0;
    v7 = rsi;
    if (zf6) {
        r13_4 = *reinterpret_cast<void***>(rdi + 24);
    }
    r15d8 = tab;
    if (reinterpret_cast<int32_t>(r15d8) < reinterpret_cast<int32_t>(0)) {
        r15d8 = 32;
    }
    r14_9 = v7;
    r12d10 = r15d8;
    if (reinterpret_cast<unsigned char>(r13_4) <= reinterpret_cast<unsigned char>(v7)) {
        r14_9 = r13_4;
    }
    *reinterpret_cast<int32_t*>(&rbx11) = 0;
    *reinterpret_cast<int32_t*>(&rbx11 + 4) = 0;
    if (r14_9) {
        do {
            rdi12 = stdout;
            rax13 = rdi12->f28;
            if (reinterpret_cast<uint64_t>(rax13) >= reinterpret_cast<uint64_t>(rdi12->f30)) {
                fun_2520();
            } else {
                rdi12->f28 = rax13 + 1;
                *rax13 = *reinterpret_cast<signed char*>(&r12d10);
            }
            rdi14 = rbx11;
            ++rbx11;
            prfield(rdi14, rbp5);
        } while (rbx11 != r14_9);
    }
    rbx15 = v7 + 1;
    if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(rbx15)) {
        do {
            rdi16 = stdout;
            rax17 = rdi16->f28;
            if (reinterpret_cast<uint64_t>(rax17) >= reinterpret_cast<uint64_t>(rdi16->f30)) {
                fun_2520();
            } else {
                rdi16->f28 = rax17 + 1;
                *rax17 = *reinterpret_cast<signed char*>(&r12d10);
            }
            rdi18 = rbx15;
            ++rbx15;
            prfield(rdi18, rbp5);
        } while (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(rbx15));
    }
    return;
}

void add_field_list(void** rdi, void** rsi) {
    void** rbp3;
    void** r13_4;
    void** rdx5;
    struct s0* rcx6;
    int64_t r8_7;
    struct s1* rax8;
    int64_t rax9;
    void** rbp10;
    void** r12d11;
    int32_t edx12;
    uint32_t eax13;
    void** rax14;
    void** rax15;
    int1_t below_or_equal16;
    struct s2* tmp64_17;
    void** rdi18;
    int64_t v19;

    rbp3 = rdi;
    while (1) {
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
        rax8 = fun_2650(rbp3, ", \t", rdx5, rcx6, r8_7);
        if (rax8) {
            rax8->f0 = 0;
            r13_4 = reinterpret_cast<void**>(&rax8->f1);
        }
        *reinterpret_cast<uint32_t*>(&rax9) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rax9) == 48) {
            if (*reinterpret_cast<void***>(rbp3 + 1)) 
                break;
            *reinterpret_cast<int32_t*>(&rbp10) = 0;
            *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
            r12d11 = reinterpret_cast<void**>(0);
        } else {
            edx12 = static_cast<int32_t>(rax9 - 49);
            if (*reinterpret_cast<unsigned char*>(&edx12) > 1) 
                goto addr_3a3b_8;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp3 + 1) == 46)) 
                goto addr_3a9f_10;
            eax13 = *reinterpret_cast<uint32_t*>(&rax9) - 48;
            r12d11 = reinterpret_cast<void**>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&eax13)));
            rax14 = string_to_join_field(rbp3 + 2, ", \t");
            rbp10 = rax14;
        }
        rax15 = xmalloc(24, ", \t");
        rdx5 = outlist_end;
        *reinterpret_cast<void***>(rax15) = r12d11;
        *reinterpret_cast<void***>(rax15 + 8) = rbp10;
        *reinterpret_cast<void***>(rax15 + 16) = reinterpret_cast<void**>(0);
        outlist_end = rax15;
        *reinterpret_cast<void***>(rdx5 + 16) = rax15;
        if (!r13_4) 
            goto addr_3a30_13;
        rbp3 = r13_4;
    }
    addr_3a6d_15:
    quote(rbp3, rbp3);
    fun_24b0();
    fun_26c0();
    addr_3a9f_10:
    quote(rbp3, rbp3);
    fun_24b0();
    fun_26c0();
    below_or_equal16 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g18)) <= reinterpret_cast<unsigned char>(1);
    if (below_or_equal16 || (tmp64_17 = reinterpret_cast<struct s2*>(*reinterpret_cast<void***>(&g28) + 16), tmp64_17->f8 == 0)) {
        rdi18 = empty_filler;
        if (!rdi18) {
            goto v19;
        }
    }
    addr_3a30_13:
    return;
    addr_3a3b_8:
    quote(rbp3, rbp3);
    fun_24b0();
    fun_26c0();
    goto addr_3a6d_15;
}

int64_t fun_24c0();

int64_t fun_2410(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, void** rdx, void** rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24c0();
    if (r8d > 10) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x9f00 + rax11 * 4) + 0x9f00;
    }
}

struct s12 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x90);

uint32_t nslots = 1;

void** xpalloc();

struct s13 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, void** rsi, void** rdx, struct s12* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    int64_t rax15;
    struct s0* r8_16;
    struct s13* rbx17;
    uint32_t r15d18;
    void** rsi19;
    void** r14_20;
    int64_t v21;
    int64_t v22;
    uint32_t r15d23;
    void** r9_24;
    void** rax25;
    void** rsi26;
    void** rax27;
    uint32_t r8d28;
    int64_t v29;
    int64_t v30;
    void* rax31;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = *reinterpret_cast<void***>(&g28);
    v7 = 0x64bf;
    rax8 = fun_2420();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xe090) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x7a31]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            v7 = 0x654b;
            fun_2560((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            rax15 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax15);
        }
        *reinterpret_cast<uint32_t*>(&r8_16) = rcx->f0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_16) + 4) = 0;
        rbx17 = reinterpret_cast<struct s13*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d18 = rcx->f4;
        rsi19 = rbx17->f0;
        r14_20 = rbx17->f8;
        v21 = rcx->f30;
        v22 = rcx->f28;
        r15d23 = r15d18 | 1;
        *reinterpret_cast<uint32_t*>(&r9_24) = r15d23;
        *reinterpret_cast<int32_t*>(&r9_24 + 4) = 0;
        rax25 = quotearg_buffer_restyled(r14_20, rsi19, rsi, rdx, *reinterpret_cast<uint32_t*>(&r8_16), *reinterpret_cast<uint32_t*>(&r9_24), &rcx->f8, v22, v21, v7);
        if (reinterpret_cast<unsigned char>(rsi19) <= reinterpret_cast<unsigned char>(rax25)) {
            rsi26 = rax25 + 1;
            rbx17->f0 = rsi26;
            if (r14_20 != 0xe200) {
                fun_2400(r14_20, rsi26, rsi, rdx, r8_16, r9_24);
                rsi26 = rsi26;
            }
            rax27 = xcharalloc(rsi26, rsi26);
            r8d28 = rcx->f0;
            rbx17->f8 = rax27;
            v29 = rcx->f30;
            r14_20 = rax27;
            v30 = rcx->f28;
            quotearg_buffer_restyled(rax27, rsi26, rsi, rdx, r8d28, r15d23, rsi26, v30, v29, 0x65da);
        }
        *rax8 = v10;
        rax31 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
        if (rax31) {
            fun_24e0();
        } else {
            return r14_20;
        }
    }
}

uint32_t fun_2640(void** rdi, void** rsi);

void** fun_24d0(void** rdi, ...);

uint32_t strcoll_loop(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r13_6;
    void** r12_7;
    void** rbp8;
    int32_t* rax9;
    int32_t* r14_10;
    uint32_t eax11;
    void** rax12;
    void** rbx13;
    void** rax14;
    void** rax15;

    r15_5 = rdi;
    r13_6 = rsi;
    r12_7 = rcx;
    rbp8 = rdx;
    rax9 = fun_2420();
    r14_10 = rax9;
    do {
        *r14_10 = 0;
        eax11 = fun_2640(r15_5, rbp8);
        if (eax11) 
            break;
        rax12 = fun_24d0(r15_5, r15_5);
        rbx13 = rax12 + 1;
        rax14 = fun_24d0(rbp8, rbp8);
        r15_5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_5) + reinterpret_cast<unsigned char>(rbx13));
        rax15 = rax14 + 1;
        rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<unsigned char>(rax15));
        r12_7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_7) - reinterpret_cast<unsigned char>(rax15));
        r13_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_6) - reinterpret_cast<unsigned char>(rbx13));
        if (!r13_6) 
            goto addr_8e20_4;
    } while (r12_7);
    goto addr_8e38_6;
    return eax11;
    addr_8e20_4:
    return *reinterpret_cast<uint32_t*>(&rax15) - (*reinterpret_cast<uint32_t*>(&rax15) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax15) < *reinterpret_cast<uint32_t*>(&rax15) + reinterpret_cast<uint1_t>(!!r12_7)));
    addr_8e38_6:
    return 1;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xe0a0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s14 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s14* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s14* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x9e93);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x9e8c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x9e97);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x9e88);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t quotearg_n_style_mem();

void collate_error(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    fun_24b0();
    fun_26c0();
    fun_24b0();
    fun_26c0();
    quotearg_n_style_mem();
    quotearg_n_style_mem();
    fun_24b0();
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gddf8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gddf8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23d3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x2030;

void fun_23e3() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t __uflow = 0x2040;

void fun_23f3() {
    __asm__("cli ");
    goto __uflow;
}

int64_t free = 0x2050;

void fun_2403() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2060;

void fun_2413() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2070;

void fun_2423() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2080;

void fun_2433() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2090;

void fun_2443() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20a0;

void fun_2453() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x20b0;

void fun_2463() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20c0;

void fun_2473() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x20d0;

void fun_2483() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20e0;

void fun_2493() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20f0;

void fun_24a3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2100;

void fun_24b3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2110;

void fun_24c3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2120;

void fun_24d3() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2130;

void fun_24e3() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2140;

void fun_24f3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2150;

void fun_2503() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2160;

void fun_2513() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x2170;

void fun_2523() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2180;

void fun_2533() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2190;

void fun_2543() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21a0;

void fun_2553() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21b0;

void fun_2563() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x21c0;

void fun_2573() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x21d0;

void fun_2583() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t memchr = 0x21e0;

void fun_2593() {
    __asm__("cli ");
    goto memchr;
}

int64_t memcmp = 0x21f0;

void fun_25a3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2200;

void fun_25b3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2210;

void fun_25c3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2220;

void fun_25d3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2230;

void fun_25e3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2240;

void fun_25f3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2250;

void fun_2603() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2260;

void fun_2613() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2270;

void fun_2623() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2280;

void fun_2633() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t strcoll = 0x2290;

void fun_2643() {
    __asm__("cli ");
    goto strcoll;
}

int64_t strpbrk = 0x22a0;

void fun_2653() {
    __asm__("cli ");
    goto strpbrk;
}

int64_t __freading = 0x22b0;

void fun_2663() {
    __asm__("cli ");
    goto __freading;
}

int64_t fwrite_unlocked = 0x22c0;

void fun_2673() {
    __asm__("cli ");
    goto fwrite_unlocked;
}

int64_t realloc = 0x22d0;

void fun_2683() {
    __asm__("cli ");
    goto realloc;
}

int64_t fdopen = 0x22e0;

void fun_2693() {
    __asm__("cli ");
    goto fdopen;
}

int64_t setlocale = 0x22f0;

void fun_26a3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2300;

void fun_26b3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t error = 0x2310;

void fun_26c3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2320;

void fun_26d3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fopen = 0x2330;

void fun_26e3() {
    __asm__("cli ");
    goto fopen;
}

int64_t strtoumax = 0x2340;

void fun_26f3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t strtoul = 0x2350;

void fun_2703() {
    __asm__("cli ");
    goto strtoul;
}

int64_t __cxa_atexit = 0x2360;

void fun_2713() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2370;

void fun_2723() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2380;

void fun_2733() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2390;

void fun_2743() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x23a0;

void fun_2753() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23b0;

void fun_2763() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x23c0;

void fun_2773() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_26a0(int64_t rdi, ...);

void fun_24a0(int64_t rdi, int64_t rsi);

void fun_2480(int64_t rdi, int64_t rsi);

signed char hard_locale();

void atexit(int64_t rdi, int64_t rsi);

signed char print_pairables = 0;

int16_t issued_disorder_warning = 0;

void** __cxa_finalize;

unsigned char print_unpairables_1 = 0;

unsigned char print_unpairables_2 = 0;

int64_t Version = 0x9e20;

void version_etc(struct s5* rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

void fun_2720();

int32_t fun_24f0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** optarg = reinterpret_cast<void**>(0);

int32_t optind = 0;

int32_t fun_25d0(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9, int64_t a7, void** a8, void** a9, void** a10, int64_t a11, int64_t a12, ...);

void** stdin = reinterpret_cast<void**>(0);

void** fopen_safer(void** rdi, int64_t rsi, void** rdx, void** rcx, struct s0* r8, void** r9);

struct s0* quotearg_n_style_colon();

void fadvise(void** rdi, int64_t rsi, void** rdx, void** rcx, struct s0* r8, void** r9);

signed char join_header_lines = 0;

int64_t rpl_fclose(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9);

void** g10;

int64_t prevline = 0;

int64_t ge1d8 = 0;

int64_t fun_27c3(int32_t edi, void** rsi) {
    void** r14_3;
    void** r13_4;
    void** rbp5;
    void** rbx6;
    void** rdi7;
    void** rax8;
    void** v9;
    struct s0* r12_10;
    signed char al11;
    void* rsp12;
    int64_t rax13;
    void** rax14;
    void** v15;
    void** rcx16;
    void** rdx17;
    void** rsi18;
    void** rdi19;
    struct s0* r8_20;
    void** r9_21;
    uint32_t eax22;
    void* rsp23;
    int1_t zf24;
    void** rdi25;
    void** rdx26;
    void** rsi27;
    void** v28;
    void** r12_29;
    int1_t zf30;
    void** rsi31;
    signed char al32;
    void** rdi33;
    void** rdx34;
    void** rsi35;
    uint32_t eax36;
    void** v37;
    signed char al38;
    void** rdi39;
    uint32_t eax40;
    void** v41;
    int1_t zf42;
    void** v43;
    void** r15_44;
    void*** rbx45;
    void** rdi46;
    void** rax47;
    void** rdi48;
    void** rdi49;
    struct s0* rax50;
    void* rsp51;
    struct s5* rdi52;
    int64_t rcx53;
    void** rdx54;
    int64_t rdi55;
    int32_t eax56;
    int64_t v57;
    void* rsp58;
    void** rdi59;
    void** r15_60;
    void*** rsp61;
    int64_t rax62;
    void** rdi63;
    int32_t eax64;
    int1_t zf65;
    int1_t zf66;
    int64_t v67;
    int64_t v68;
    int64_t v69;
    int64_t v70;
    int32_t eax71;
    void* rsp72;
    void** rax73;
    int64_t v74;
    int64_t v75;
    int64_t v76;
    int64_t v77;
    int32_t eax78;
    void* rsp79;
    void** rax80;
    void* rsp81;
    void* rsp82;
    int1_t zf83;
    int1_t zf84;
    int1_t zf85;
    int1_t zf86;
    int1_t zf87;
    int1_t zf88;
    uint32_t eax89;
    uint32_t eax90;
    void** rdi91;
    signed char al92;
    int1_t zf93;
    int1_t zf94;
    int1_t zf95;
    int1_t zf96;
    void** rsi97;
    int64_t rax98;
    void* rsp99;
    struct s0* rax100;
    int64_t rax101;
    int1_t zf102;
    int1_t zf103;
    void* rax104;
    signed char al105;
    int1_t zf106;
    int1_t zf107;
    int1_t zf108;
    int1_t zf109;
    void** rdi110;
    void** rdi111;
    void** rdx112;
    void** rdx113;
    void** rdx114;
    void** rdi115;

    __asm__("cli ");
    r14_3 = reinterpret_cast<void**>(0xdaa0);
    r13_4 = reinterpret_cast<void**>("-a:e:i1:2:j:o:t:v:z");
    rbp5 = reinterpret_cast<void**>(static_cast<int64_t>(edi));
    rbx6 = rsi;
    rdi7 = *reinterpret_cast<void***>(rsi);
    rax8 = *reinterpret_cast<void***>(&g28);
    v9 = rax8;
    set_program_name(rdi7);
    fun_26a0(6, 6);
    fun_24a0("coreutils", "/usr/local/share/locale");
    r12_10 = reinterpret_cast<struct s0*>(0x9cd8);
    fun_2480("coreutils", "/usr/local/share/locale");
    al11 = hard_locale();
    hard_LC_COLLATE = al11;
    atexit(0x49a0, "/usr/local/share/locale");
    atexit(0x3e20, "/usr/local/share/locale");
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    print_pairables = 1;
    seen_unpairable = 0;
    issued_disorder_warning = 0;
    check_input_order = 0;
    goto addr_28a8_2;
    addr_364e_3:
    fun_24b0();
    fun_2420();
    fun_26c0();
    fun_24b0();
    fun_26c0();
    addr_3117_5:
    return 0;
    addr_28ec_6:
    goto *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(r12_10) + rax13 * 4) + reinterpret_cast<int64_t>(r12_10);
    while (1) {
        addr_2e92_7:
        if (!rax14) 
            goto addr_3061_8;
        if (!v15) 
            goto addr_3063_10;
        rcx16 = join_field_2;
        rdx17 = join_field_1;
        rsi18 = __cxa_finalize;
        rdi19 = __cxa_finalize;
        eax22 = keycmp(rdi19, rsi18, rdx17, rcx16, r8_20, r9_21);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        if (reinterpret_cast<int32_t>(eax22) < reinterpret_cast<int32_t>(0)) {
            zf24 = print_unpairables_1 == 0;
            if (!zf24) {
                rdi25 = __cxa_finalize;
                prjoin(rdi25, 0xe120, rdx17, rcx16, r8_20, r9_21);
                rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            }
            *reinterpret_cast<int32_t*>(&rdx26) = 1;
            *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
            rsi27 = r14_3;
            v28 = reinterpret_cast<void**>(0);
            getseq(r12_29, rsi27, 1, rcx16, r8_20, r9_21);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            seen_unpairable = 1;
        } else {
            if (eax22) {
                zf30 = print_unpairables_2 == 0;
                if (!zf30) {
                    rsi31 = __cxa_finalize;
                    prjoin(0xe120, rsi31, rdx17, rcx16, r8_20, r9_21);
                    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                }
                *reinterpret_cast<int32_t*>(&rdx26) = 2;
                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                rsi27 = r13_4;
                v15 = reinterpret_cast<void**>(0);
                getseq(rbp5, rsi27, 2, rcx16, r8_20, r9_21);
                rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                seen_unpairable = 1;
            } else {
                do {
                    al32 = getseq(r12_29, r14_3, 1, rcx16, r8_20, r9_21);
                    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                    if (!al32) 
                        goto addr_2f26_21;
                    rcx16 = join_field_2;
                    rdi33 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v28) * 8 - 8);
                    rdx34 = join_field_1;
                    rsi35 = __cxa_finalize;
                    eax36 = keycmp(rdi33, rsi35, rdx34, rcx16, r8_20, r9_21);
                    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                } while (!eax36);
                goto addr_3034_23;
            }
        }
        addr_2e8d_24:
        rax14 = v28;
        continue;
        addr_2f26_21:
        ++v28;
        *reinterpret_cast<signed char*>(&v37) = 1;
        do {
            addr_2f6a_25:
            *reinterpret_cast<int32_t*>(&rdx26) = 2;
            *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
            rsi27 = r13_4;
            al38 = getseq(rbp5, rsi27, 2, rcx16, r8_20, r9_21);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            if (!al38) 
                break;
            rcx16 = join_field_2;
            rsi27 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v15) * 8 - 8);
            rdx26 = join_field_1;
            rdi39 = __cxa_finalize;
            eax40 = keycmp(rdi39, rsi27, rdx26, rcx16, r8_20, r9_21);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        } while (!eax40);
        goto addr_303e_27;
        ++v15;
        *reinterpret_cast<signed char*>(&v41) = 1;
        addr_2f89_29:
        zf42 = print_pairables == 0;
        if (!zf42 && (v43 = reinterpret_cast<void**>(0), v28 != 1)) {
            do {
                *reinterpret_cast<int32_t*>(&r15_44) = 0;
                *reinterpret_cast<int32_t*>(&r15_44 + 4) = 0;
                rbx45 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(v43) * 8);
                if (v15 != 1) {
                    do {
                        rsi27 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r15_44) * 8);
                        ++r15_44;
                        rdi46 = *rbx45;
                        prjoin(rdi46, rsi27, 0, rcx16, r8_20, r9_21);
                        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                        rdx26 = v15 + 0xffffffffffffffff;
                    } while (reinterpret_cast<unsigned char>(r15_44) < reinterpret_cast<unsigned char>(rdx26));
                }
                ++v43;
                rbx6 = v43;
            } while (reinterpret_cast<unsigned char>(rbx6) < reinterpret_cast<unsigned char>(v28 - 1));
        }
        *reinterpret_cast<int32_t*>(&rax47) = 0;
        *reinterpret_cast<int32_t*>(&rax47 + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&v37)) {
            rdx26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v28) * 8 + 0xfffffffffffffff8);
            rcx16 = __cxa_finalize;
            rsi27 = *reinterpret_cast<void***>(rdx26);
            __cxa_finalize = rsi27;
            *reinterpret_cast<int32_t*>(&rax47) = 1;
            *reinterpret_cast<int32_t*>(&rax47 + 4) = 0;
            *reinterpret_cast<void***>(rdx26) = rcx16;
        }
        v28 = rax47;
        if (*reinterpret_cast<signed char*>(&v41)) {
            v15 = reinterpret_cast<void**>(0);
            goto addr_2e8d_24;
        } else {
            rdx26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v15) * 8 + 0xfffffffffffffff8);
            rcx16 = __cxa_finalize;
            rsi27 = *reinterpret_cast<void***>(rdx26);
            __cxa_finalize = rsi27;
            *reinterpret_cast<void***>(rdx26) = rcx16;
            v15 = reinterpret_cast<void**>(1);
            goto addr_2e8d_24;
        }
        addr_303e_27:
        *reinterpret_cast<signed char*>(&v41) = 0;
        goto addr_2f89_29;
        addr_3034_23:
        *reinterpret_cast<signed char*>(&v37) = 0;
        goto addr_2f6a_25;
        addr_2e05_40:
        rsi27 = __cxa_finalize;
        goto addr_2e0d_41;
        addr_2dfe_42:
        rdi48 = reinterpret_cast<void**>(0xe120);
        goto addr_2e05_40;
        while (1) {
            addr_3618_43:
            rdi49 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(rbx6 + reinterpret_cast<unsigned char>(rbp5) * 8) - 8);
            rax50 = quote(rdi49, rdi49);
            r12_10 = rax50;
            fun_24b0();
            fun_26c0();
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8);
            while (1) {
                while (1) {
                    addr_2b57_44:
                    usage();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    do {
                        rdi52 = stdout;
                        rcx53 = Version;
                        *reinterpret_cast<int32_t*>(&r9_21) = 0;
                        *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
                        version_etc(rdi52, "join", "GNU coreutils", rcx53, "Mike Haertel");
                        fun_2720();
                        rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                        print_unpairables_1 = 1;
                        while (1) {
                            addr_28a8_2:
                            *reinterpret_cast<int32_t*>(&r8_20) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_20) + 4) = 0;
                            rcx16 = r14_3;
                            rdx54 = r13_4;
                            *reinterpret_cast<int32_t*>(&rdi55) = *reinterpret_cast<int32_t*>(&rbp5);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi55) + 4) = 0;
                            eax56 = fun_24f0(rdi55, rbx6, rdx54, rcx16);
                            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
                            if (eax56 == -1) 
                                goto addr_2beb_49;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v57) + 4) = 0;
                            if (eax56 > 0x82) 
                                goto addr_2b57_44;
                            if (eax56 > 48) 
                                goto addr_28e0_52;
                            if (eax56 == 0xffffff7e) 
                                goto addr_3466_54;
                            if (eax56 != 1) 
                                break;
                            rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8);
                            rdi59 = optarg;
                            r15_60 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp58) + 52);
                            rsp61 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp58) - 8);
                            r9_21 = reinterpret_cast<void**>(rsp61 + 64);
                            add_file_name_constprop_0(rdi59, 0xe1b0, reinterpret_cast<int64_t>(rsp51) + 0x78, reinterpret_cast<int64_t>(rsp51) + 0x80, rsp61 + 68, r9_21, r15_60);
                            rsp12 = reinterpret_cast<void*>(rsp61 - 8 + 8 + 8 + 8);
                        }
                    } while (eax56 == 0xffffff7d);
                    continue;
                    addr_28e0_52:
                    *reinterpret_cast<uint32_t*>(&rax13) = eax56 - 49;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
                    if (*reinterpret_cast<uint32_t*>(&rax13) <= 81) 
                        goto addr_28ec_6;
                }
                addr_2beb_49:
                rax62 = optind;
                if (*reinterpret_cast<int32_t*>(&rbp5) > *reinterpret_cast<int32_t*>(&rax62)) {
                    r13_4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 48);
                    r12_10 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(rsp51) + 52);
                    rcx16 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x80);
                    rdx54 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 0x78);
                    r15_60 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp51) + 44);
                    do {
                        r9_21 = r13_4;
                        r8_20 = r12_10;
                        optind = static_cast<int32_t>(rax62 + 1);
                        rdi63 = *reinterpret_cast<void***>(rbx6 + rax62 * 8);
                        v43 = reinterpret_cast<void**>(0xe1b0);
                        v41 = rcx16;
                        v37 = rdx54;
                        add_file_name_constprop_0(rdi63, 0xe1b0, rdx54, rcx16, r8_20, r9_21, r15_60);
                        rax62 = optind;
                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 - 8 - 8 + 8 + 8 + 8);
                        rdx54 = v37;
                        rcx16 = v41;
                    } while (*reinterpret_cast<int32_t*>(&rax62) < *reinterpret_cast<int32_t*>(&rbp5));
                }
                eax64 = 0;
                if (1) {
                    addr_35bb_61:
                    if (eax64) 
                        goto addr_3618_43;
                } else {
                    if (0) {
                        set_join_field(0xe020, 0, 0xe020, 0);
                        set_join_field(0xe018, 0, 0xe018, 0);
                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                    }
                    if (0) {
                        set_join_field(0xe020, 1, 0xe020, 1);
                        set_join_field(0xe018, 1, 0xe018, 1);
                        rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                    }
                    zf65 = reinterpret_cast<int1_t>(join_field_1 == 0xffffffffffffffff);
                    if (zf65) {
                        join_field_1 = reinterpret_cast<void**>(0);
                    }
                    zf66 = reinterpret_cast<int1_t>(join_field_2 == 0xffffffffffffffff);
                    if (zf66) {
                        join_field_2 = reinterpret_cast<void**>(0);
                    }
                    r13_4 = g_names;
                    rbp5 = reinterpret_cast<void**>("-");
                    eax71 = fun_25d0(r13_4, "-", rdx54, rcx16, r8_20, r9_21, v67, v43, v37, v41, v68, v57, r13_4, "-", rdx54, rcx16, r8_20, r9_21, v69, v43, v37, v41, v70, v57);
                    rsp72 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8);
                    r12_29 = stdin;
                    if (eax71) {
                        rax73 = fopen_safer(r13_4, "r", rdx54, rcx16, r8_20, r9_21);
                        rsp72 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp72) - 8 + 8);
                        r12_29 = rax73;
                    }
                    if (!r12_29) 
                        break;
                    r13_4 = ge1b8;
                    eax78 = fun_25d0(r13_4, "-", rdx54, rcx16, r8_20, r9_21, v74, v43, v37, v41, v75, v57, r13_4, "-", rdx54, rcx16, r8_20, r9_21, v76, v43, v37, v41, v77, v57);
                    rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp72) - 8 + 8);
                    rbp5 = stdin;
                    if (eax78) {
                        rax80 = fopen_safer(r13_4, "r", rdx54, rcx16, r8_20, r9_21);
                        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
                        rbp5 = rax80;
                    }
                    if (!rbp5) 
                        goto addr_34a3_76; else 
                        goto addr_2d11_77;
                }
                fun_24b0();
                fun_26c0();
                rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8);
                continue;
                addr_34a3_76:
                quotearg_n_style_colon();
                fun_2420();
                fun_26c0();
                quote(r15_60, r15_60);
                fun_24b0();
                fun_26c0();
                fun_24b0();
                fun_26c0();
                rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                addr_352c_81:
                fun_24e0();
                rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8);
                goto addr_3531_82;
                addr_2d11_77:
                if (r12_29 == rbp5) 
                    goto addr_364e_3;
                r14_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp79) + 64);
                fadvise(r12_29, 2, rdx54, rcx16, r8_20, r9_21);
                rsp82 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
                r13_4 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp82) + 96);
                fadvise(rbp5, 2, rdx54, rcx16, r8_20, r9_21);
                v28 = reinterpret_cast<void**>(0);
                getseq(r12_29, r14_3, 1, rcx16, r8_20, r9_21);
                *reinterpret_cast<int32_t*>(&rdx26) = 2;
                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                rsi27 = r13_4;
                v15 = reinterpret_cast<void**>(0);
                getseq(rbp5, rsi27, 2, rcx16, r8_20, r9_21);
                rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp82) - 8 + 8 - 8 + 8 - 8 + 8);
                zf83 = autoformat == 0;
                rax14 = reinterpret_cast<void**>(0);
                if (!zf83) 
                    goto addr_2da2_84;
                addr_2dda_85:
                zf84 = join_header_lines == 0;
                if (zf84) 
                    goto addr_2e92_7;
                rdx26 = reinterpret_cast<void**>(0);
                if (0) 
                    goto addr_33b3_87;
                if (!1) 
                    goto addr_2dfe_42;
                addr_3061_8:
                *reinterpret_cast<int32_t*>(&rax14) = 0;
                *reinterpret_cast<int32_t*>(&rax14 + 4) = 0;
                addr_3063_10:
                zf85 = check_input_order == 2;
                if (zf85 || (zf86 = *reinterpret_cast<signed char*>(&issued_disorder_warning) == 0, !zf86) && (zf87 = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&issued_disorder_warning) + 1) == 0, !zf87)) {
                    zf88 = print_unpairables_1 == 0;
                    if (zf88 || !rax14) {
                        eax89 = print_unpairables_2;
                        if (!*reinterpret_cast<signed char*>(&eax89)) 
                            goto addr_30ab_91;
                    } else {
                        *reinterpret_cast<signed char*>(&v43) = 0;
                        goto addr_3403_93;
                    }
                } else {
                    if (!rax14) {
                        eax89 = print_unpairables_2;
                    } else {
                        eax90 = print_unpairables_1;
                        *reinterpret_cast<signed char*>(&v43) = *reinterpret_cast<signed char*>(&eax90);
                        if (*reinterpret_cast<signed char*>(&eax90)) {
                            addr_3403_93:
                            rdi91 = __cxa_finalize;
                            prjoin(rdi91, 0xe120, rdx26, rcx16, r8_20, r9_21);
                            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                        } else {
                            *reinterpret_cast<signed char*>(&v43) = 1;
                        }
                        if (v15) {
                            seen_unpairable = 1;
                        }
                        rbx6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp23) + 56);
                        while (*reinterpret_cast<int32_t*>(&rdx26) = 1, *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0, rsi27 = rbx6, al92 = get_line(r12_29, rsi27, 1, rcx16, r8_20, r9_21), rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8), !!al92) {
                            zf93 = print_unpairables_1 == 0;
                            if (!zf93) {
                                rsi27 = reinterpret_cast<void**>(0xe120);
                                prjoin(0, 0xe120, 1, rcx16, r8_20, r9_21);
                                rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                                zf94 = *reinterpret_cast<signed char*>(&issued_disorder_warning) == 0;
                                if (zf94) 
                                    continue;
                                zf95 = print_unpairables_1 == 0;
                                if (zf95) 
                                    goto addr_3443_105;
                            } else {
                                zf96 = *reinterpret_cast<signed char*>(&issued_disorder_warning) == 0;
                                if (!zf96) 
                                    break;
                            }
                        }
                        goto addr_324a_107;
                    }
                }
                rbx6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp23) + 56);
                if (!v15) {
                    addr_30ab_91:
                    *reinterpret_cast<int32_t*>(&r15_60) = 0;
                    *reinterpret_cast<int32_t*>(&r15_60 + 4) = 0;
                } else {
                    addr_3348_109:
                    if (*reinterpret_cast<signed char*>(&eax89)) {
                        rsi97 = __cxa_finalize;
                        prjoin(0xe120, rsi97, rdx26, rcx16, r8_20, r9_21);
                        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                        goto addr_3350_111;
                    }
                }
                addr_30ae_112:
                fun_2400(0, rsi27, rdx26, rcx16, r8_20, r9_21);
                delseq(r14_3, rsi27, rdx26, rcx16, r8_20, r9_21);
                delseq(r13_4, rsi27, rdx26, rcx16, r8_20, r9_21);
                rax98 = rpl_fclose(r12_29, rsi27, rdx26, rcx16, r8_20, r9_21);
                rsp99 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                if (*reinterpret_cast<int32_t*>(&rax98)) {
                    addr_3588_113:
                    rax100 = quotearg_n_style_colon();
                    r12_10 = rax100;
                    fun_2420();
                    eax64 = fun_26c0();
                    rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_35bb_61;
                } else {
                    rax101 = rpl_fclose(rbp5, rsi27, rdx26, rcx16, r8_20, r9_21);
                    rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp99) - 8 + 8);
                    if (*reinterpret_cast<int32_t*>(&rax101)) {
                        addr_3555_115:
                        quotearg_n_style_colon();
                        fun_2420();
                        fun_26c0();
                        rsp99 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8 - 8 + 8 - 8 + 8);
                        goto addr_3588_113;
                    } else {
                        zf102 = *reinterpret_cast<signed char*>(&issued_disorder_warning) == 0;
                        if (!zf102 || (zf103 = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&issued_disorder_warning) + 1) == 0, !zf103)) {
                            addr_3531_82:
                            fun_24b0();
                            fun_26c0();
                            rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8 - 8 + 8);
                            goto addr_3555_115;
                        } else {
                            rax104 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
                            if (rax104) 
                                goto addr_352c_81; else 
                                goto addr_3117_5;
                        }
                    }
                }
                addr_3350_111:
                if (v28) {
                    seen_unpairable = 1;
                }
                while (*reinterpret_cast<int32_t*>(&rdx26) = 2, *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0, rsi27 = rbx6, al105 = get_line(rbp5, rsi27, 2, rcx16, r8_20, r9_21), rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8), !!al105) {
                    zf106 = print_unpairables_2 == 0;
                    if (!zf106) {
                        rsi27 = reinterpret_cast<void**>(0);
                        prjoin(0xe120, 0, 2, rcx16, r8_20, r9_21);
                        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
                        zf107 = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&issued_disorder_warning) + 1) == 0;
                        if (zf107) 
                            continue;
                        zf108 = print_unpairables_2 == 0;
                        if (zf108) 
                            goto addr_33ae_124;
                    } else {
                        zf109 = *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&issued_disorder_warning) + 1) == 0;
                        if (!zf109) 
                            goto addr_338a_126;
                    }
                }
                addr_3268_127:
                r15_60 = reinterpret_cast<void**>(0);
                if (!1) {
                    rdi110 = *reinterpret_cast<void***>(&g28);
                    fun_2400(rdi110, rsi27, rdx26, rcx16, r8_20, r9_21);
                    *reinterpret_cast<void***>(&g28) = reinterpret_cast<void**>(0);
                    rdi111 = g10;
                    fun_2400(rdi111, rsi27, rdx26, rcx16, r8_20, r9_21);
                    rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8 - 8 + 8);
                    g10 = reinterpret_cast<void**>(0);
                    goto addr_30ae_112;
                }
                addr_33ae_124:
                goto addr_3268_127;
                addr_338a_126:
                goto addr_3268_127;
                addr_324a_107:
                eax89 = print_unpairables_2;
                if (*reinterpret_cast<signed char*>(&eax89)) 
                    goto addr_325c_129;
                if (!*reinterpret_cast<signed char*>(&v43)) 
                    goto addr_3268_127;
                addr_325c_129:
                if (v15) 
                    goto addr_3348_109; else 
                    goto addr_3268_127;
                addr_3443_105:
                goto addr_324a_107;
                addr_2da2_84:
                rdx112 = reinterpret_cast<void**>(0);
                if (!1) {
                    rdx113 = __cxa_finalize;
                    rdx112 = *reinterpret_cast<void***>(rdx113 + 24);
                }
                autocount_1 = rdx112;
                rdx26 = reinterpret_cast<void**>(0);
                if (!1) {
                    rdx114 = __cxa_finalize;
                    rdx26 = *reinterpret_cast<void***>(rdx114 + 24);
                }
                autocount_2 = rdx26;
                goto addr_2dda_85;
                addr_3466_54:
                usage();
                rdi115 = optarg;
                quote(rdi115, rdi115);
                fun_24b0();
                fun_26c0();
                rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp51) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_34a3_76;
            }
            quotearg_n_style_colon();
            fun_2420();
            fun_26c0();
            rsp51 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp72) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        addr_33b3_87:
        rsi27 = reinterpret_cast<void**>(0xe120);
        rdi48 = __cxa_finalize;
        if (1) {
            addr_2e0d_41:
            prjoin(rdi48, rsi27, 0, rcx16, r8_20, r9_21);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            prevline = 0;
            ge1d8 = 0;
            if (0) {
                *reinterpret_cast<int32_t*>(&rdx26) = 1;
                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                rsi27 = r14_3;
                v28 = reinterpret_cast<void**>(0);
                getseq(r12_29, rsi27, 1, rcx16, r8_20, r9_21);
                rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            }
        } else {
            goto addr_2e05_40;
        }
        if (!1) {
            *reinterpret_cast<int32_t*>(&rdx26) = 2;
            *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
            rsi27 = r13_4;
            v15 = reinterpret_cast<void**>(0);
            getseq(rbp5, rsi27, 2, rcx16, r8_20, r9_21);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            goto addr_2e8d_24;
        }
    }
}

int64_t __libc_start_main = 0;

void fun_36a3() {
    __asm__("cli ");
    __libc_start_main(0x27c0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xe008;

void fun_23d0(int64_t rdi);

int64_t fun_3743() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23d0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3783() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** spareline = reinterpret_cast<void**>(0);

struct s15 {
    signed char[16] pad16;
    void** f10;
    signed char[23] pad40;
    void** f28;
};

struct s15* ge1a8 = reinterpret_cast<struct s15*>(0);

void fun_3e23() {
    void** rbp1;
    void** rdi2;
    void** rsi3;
    void** rdx4;
    void** rcx5;
    struct s0* r8_6;
    void** r9_7;
    void** rdi8;
    void** rsi9;
    void** rdx10;
    void** rcx11;
    struct s0* r8_12;
    void** r9_13;
    void** rsi14;
    void** rdx15;
    void** rcx16;
    struct s0* r8_17;
    void** r9_18;
    struct s15* rbp19;
    void** rdi20;
    void** rsi21;
    void** rdx22;
    void** rcx23;
    struct s0* r8_24;
    void** r9_25;
    void** rdi26;
    void** rsi27;
    void** rdx28;
    void** rcx29;
    struct s0* r8_30;
    void** r9_31;

    __asm__("cli ");
    rbp1 = spareline;
    if (rbp1) {
        rdi2 = *reinterpret_cast<void***>(rbp1 + 40);
        fun_2400(rdi2, rsi3, rdx4, rcx5, r8_6, r9_7);
        rdi8 = *reinterpret_cast<void***>(rbp1 + 16);
        fun_2400(rdi8, rsi9, rdx10, rcx11, r8_12, r9_13);
        fun_2400(rbp1, rsi14, rdx15, rcx16, r8_17, r9_18);
    }
    rbp19 = ge1a8;
    if (!rbp19) {
        return;
    } else {
        rdi20 = rbp19->f28;
        fun_2400(rdi20, rsi21, rdx22, rcx23, r8_24, r9_25);
        rdi26 = rbp19->f10;
        fun_2400(rdi26, rsi27, rdx28, rcx29, r8_30, r9_31);
        goto fun_2400;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_26b0(int64_t rdi, void** rsi, void** rdx, void** rcx);

void fun_25b0(void** rdi, struct s5* rsi, void** rdx, void** rcx);

int32_t fun_2430(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s5* stderr = reinterpret_cast<struct s5*>(0);

void fun_2740(struct s5* rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_43b3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    struct s5* r12_7;
    void** rax8;
    struct s5* r12_9;
    void** rax10;
    struct s5* r12_11;
    void** rax12;
    struct s5* r12_13;
    void** rax14;
    struct s5* r12_15;
    void** rax16;
    struct s5* r12_17;
    void** rax18;
    struct s5* r12_19;
    void** rax20;
    struct s5* r12_21;
    void** rax22;
    struct s5* r12_23;
    void** rax24;
    struct s5* r12_25;
    void** rax26;
    struct s0* r8_27;
    void** r9_28;
    int64_t r8_29;
    int64_t r9_30;
    int32_t eax31;
    void** r13_32;
    void** rax33;
    void** rax34;
    int32_t eax35;
    void** rax36;
    void** rax37;
    void** rax38;
    int32_t eax39;
    void** rax40;
    struct s5* r15_41;
    void** rax42;
    void** rax43;
    void** rax44;
    struct s5* rdi45;
    void** r8_46;
    void** r9_47;
    int64_t v48;
    int64_t v49;
    int64_t v50;
    int64_t v51;
    int64_t v52;
    int64_t v53;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = *reinterpret_cast<void***>(&g28);
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_24b0();
            fun_26b0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_24b0();
            fun_25b0(rax8, r12_7, 5, rcx6);
            r12_9 = stdout;
            rax10 = fun_24b0();
            fun_25b0(rax10, r12_9, 5, rcx6);
            r12_11 = stdout;
            rax12 = fun_24b0();
            fun_25b0(rax12, r12_11, 5, rcx6);
            r12_13 = stdout;
            rax14 = fun_24b0();
            fun_25b0(rax14, r12_13, 5, rcx6);
            r12_15 = stdout;
            rax16 = fun_24b0();
            fun_25b0(rax16, r12_15, 5, rcx6);
            r12_17 = stdout;
            rax18 = fun_24b0();
            fun_25b0(rax18, r12_17, 5, rcx6);
            r12_19 = stdout;
            rax20 = fun_24b0();
            fun_25b0(rax20, r12_19, 5, rcx6);
            r12_21 = stdout;
            rax22 = fun_24b0();
            fun_25b0(rax22, r12_21, 5, rcx6);
            r12_23 = stdout;
            rax24 = fun_24b0();
            fun_25b0(rax24, r12_23, 5, rcx6);
            r12_25 = stdout;
            rax26 = fun_24b0();
            fun_25b0(rax26, r12_25, 5, rcx6);
            do {
                if (1) 
                    break;
                eax31 = fun_25d0("join", 0, 5, "sha512sum", r8_27, r9_28, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "join", 0, 5, "sha512sum", r8_29, r9_30, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities");
            } while (eax31);
            r13_32 = v4;
            if (!r13_32) {
                rax33 = fun_24b0();
                fun_26b0(1, rax33, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax34 = fun_26a0(5, 5);
                if (!rax34 || (eax35 = fun_2430(rax34, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax35)) {
                    rax36 = fun_24b0();
                    r13_32 = reinterpret_cast<void**>("join");
                    fun_26b0(1, rax36, "https://www.gnu.org/software/coreutils/", "join");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_32 = reinterpret_cast<void**>("join");
                    goto addr_47c0_9;
                }
            } else {
                rax37 = fun_24b0();
                fun_26b0(1, rax37, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax38 = fun_26a0(5, 5);
                if (!rax38 || (eax39 = fun_2430(rax38, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax39)) {
                    addr_46c6_11:
                    rax40 = fun_24b0();
                    fun_26b0(1, rax40, "https://www.gnu.org/software/coreutils/", "join");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_32 == "join")) {
                        r12_2 = reinterpret_cast<void**>(0x9ae8);
                    }
                } else {
                    addr_47c0_9:
                    r15_41 = stdout;
                    rax42 = fun_24b0();
                    fun_25b0(rax42, r15_41, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_46c6_11;
                }
            }
            rax43 = fun_24b0();
            rcx6 = r12_2;
            fun_26b0(1, rax43, r13_32, rcx6);
            addr_440e_14:
            fun_2720();
        }
    } else {
        rax44 = fun_24b0();
        rdi45 = stderr;
        rcx6 = r12_2;
        fun_2740(rdi45, 1, rax44, rcx6, r8_46, r9_47, v48, v49, v50, v51, v52, v53);
        goto addr_440e_14;
    }
}

int64_t file_name = 0;

void fun_4983(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_4993(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(struct s5* rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2440(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_49a3() {
    struct s5* rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    struct s5* rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2420(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_24b0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_4a33_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_26c0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2440(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_4a33_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_26c0();
    }
}

void fun_4a53() {
    __asm__("cli ");
}

uint32_t fun_2600(void** rdi);

void fun_4a63(void** rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        fun_2600(rdi);
        goto 0x2580;
    }
}

int32_t fun_2660(void** rdi);

int64_t fun_2540(int64_t rdi, ...);

int32_t rpl_fflush(void** rdi);

int64_t fun_2490(void** rdi);

int64_t fun_4a93(void** rdi) {
    uint32_t eax2;
    int32_t eax3;
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2600(rdi);
    if (reinterpret_cast<int32_t>(eax2) >= reinterpret_cast<int32_t>(0)) {
        eax3 = fun_2660(rdi);
        if (!(eax3 && (eax4 = fun_2600(rdi), *reinterpret_cast<uint32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2540(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2420();
            r12d9 = *rax8;
            rax10 = fun_2490(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2490;
}

void rpl_fseeko(void** rdi);

void fun_4b23(void** rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2660(rdi), !eax2) || !(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

void** fun_26e0();

int32_t dup_safer(int64_t rdi);

void** fun_2690(int64_t rdi, int64_t rsi);

void fun_2570(int64_t rdi, int64_t rsi, int64_t rdx);

void** fun_4b73() {
    void** rax1;
    void** r12_2;
    uint32_t eax3;
    int64_t rdi4;
    int32_t eax5;
    int32_t* rax6;
    void** rdi7;
    int32_t r13d8;
    void** rsi9;
    void** rdx10;
    void** rcx11;
    struct s0* r8_12;
    void** r9_13;
    void** rsi14;
    void** rdx15;
    void** rcx16;
    struct s0* r8_17;
    void** r9_18;
    int64_t rax19;
    int64_t rsi20;
    int64_t rsi21;
    int64_t rdi22;
    void** rax23;
    int32_t* rax24;
    int64_t rdi25;
    int32_t r12d26;
    int64_t rdx27;

    __asm__("cli ");
    rax1 = fun_26e0();
    r12_2 = rax1;
    if (!rax1) 
        goto addr_4b96_2;
    eax3 = fun_2600(rax1);
    if (eax3 > 2) 
        goto addr_4b96_2;
    *reinterpret_cast<uint32_t*>(&rdi4) = eax3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi4) + 4) = 0;
    eax5 = dup_safer(rdi4);
    if (eax5 < 0) {
        rax6 = fun_2420();
        rdi7 = r12_2;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        r13d8 = *rax6;
        rpl_fclose(rdi7, rsi9, rdx10, rcx11, r8_12, r9_13);
        *rax6 = r13d8;
        goto addr_4b96_2;
    } else {
        rax19 = rpl_fclose(r12_2, rsi14, rdx15, rcx16, r8_17, r9_18);
        if (!*reinterpret_cast<int32_t*>(&rax19)) {
            rsi20 = rsi21;
            *reinterpret_cast<int32_t*>(&rdi22) = eax5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
            rax23 = fun_2690(rdi22, rsi20);
            r12_2 = rax23;
            if (rax23) {
                addr_4b96_2:
                return r12_2;
            }
        }
        rax24 = fun_2420();
        *reinterpret_cast<int32_t*>(&rdi25) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi25) + 4) = 0;
        r12d26 = *rax24;
        fun_2570(rdi25, rsi20, rdx27);
        *rax24 = r12d26;
        *reinterpret_cast<int32_t*>(&r12_2) = 0;
        *reinterpret_cast<int32_t*>(&r12_2 + 4) = 0;
        goto addr_4b96_2;
    }
}

int64_t fun_4c13(void** rdi, int64_t rsi, int32_t edx) {
    uint32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(*reinterpret_cast<void***>(rdi + 16) != *reinterpret_cast<void***>(rdi + 8) || (*reinterpret_cast<void***>(rdi + 40) != *reinterpret_cast<void***>(rdi + 32) || *reinterpret_cast<int64_t*>(rdi + 72)))) {
        eax4 = fun_2600(rdi);
        *reinterpret_cast<uint32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2540(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 0xffffffef);
            *reinterpret_cast<int64_t*>(rdi + 0x90) = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

int32_t setlocale_null_r();

int64_t fun_4c93() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = *reinterpret_cast<void***>(&g28);
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax3;
    }
}

struct s16 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_4d13(struct s16* rdi) {
    __asm__("cli ");
    __asm__("pxor xmm0, xmm0");
    rdi->f10 = 0;
    __asm__("movups [rdi], xmm0");
    return;
}

struct s17 {
    void* f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

uint32_t fun_23f0(struct s17* rdi, struct s17* rsi, void** rdx);

struct s17* fun_4d33(struct s17* rdi, struct s17* rsi, int32_t edx) {
    void** rdx3;
    uint32_t r13d4;
    void** r15_5;
    void* rcx6;
    int32_t v7;
    unsigned char v8;
    void** rcx9;
    struct s17* r14_10;
    struct s17* rbp11;
    void** r12_12;
    void** rax13;
    uint32_t ebx14;
    uint32_t eax15;
    uint32_t eax16;
    uint32_t r11d17;
    void* r12_18;
    void** rax19;
    void* rcx20;
    void** rax21;
    void** rax22;
    uint32_t esi23;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    __asm__("cli ");
    r13d4 = reinterpret_cast<uint32_t>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&rdx3)));
    r15_5 = rdi->f10;
    rcx6 = rdi->f0;
    v7 = *reinterpret_cast<int32_t*>(&rdx3);
    v8 = *reinterpret_cast<unsigned char*>(&r13d4);
    rcx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx6) + reinterpret_cast<unsigned char>(r15_5));
    if (reinterpret_cast<unsigned char>(rsi->f0) & 16) {
        addr_4e20_2:
        return 0;
    } else {
        r14_10 = rdi;
        rbp11 = rsi;
        r12_12 = r15_5;
        do {
            rax13 = rbp11->f8;
            if (reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rbp11->f10)) {
                rdx3 = rax13 + 1;
                rbp11->f8 = rdx3;
                ebx14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax13));
            } else {
                eax15 = fun_23f0(rbp11, rsi, rdx3);
                rcx9 = rcx9;
                ebx14 = eax15;
                if (eax15 == 0xffffffff) {
                    if (r15_5 == r12_12) 
                        goto addr_4e20_2;
                    if (reinterpret_cast<unsigned char>(rbp11->f0) & 32) 
                        goto addr_4e20_2;
                    eax16 = v8;
                    if (*reinterpret_cast<void***>(r12_12 + 0xffffffffffffffff) == *reinterpret_cast<void***>(&eax16)) 
                        break;
                    ebx14 = r13d4;
                    if (r12_12 != rcx9) 
                        goto addr_4e38_11;
                    r11d17 = v8;
                    goto addr_4de0_13;
                }
            }
            r11d17 = ebx14;
            if (r12_12 == rcx9) {
                addr_4de0_13:
                rsi = r14_10;
                r12_18 = r14_10->f0;
                *reinterpret_cast<int32_t*>(&rdx3) = 1;
                *reinterpret_cast<int32_t*>(&rdx3 + 4) = 0;
                rax19 = xpalloc();
                rcx20 = r14_10->f0;
                r11d17 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&r11d17));
                r15_5 = rax19;
                rax21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax19) + reinterpret_cast<unsigned char>(r12_18));
                r14_10->f10 = r15_5;
                rcx9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx20) + reinterpret_cast<unsigned char>(r15_5));
            } else {
                rax21 = r12_12;
            }
            *reinterpret_cast<void***>(rax21) = *reinterpret_cast<void***>(&r11d17);
            r12_12 = rax21 + 1;
        } while (r13d4 != ebx14);
    }
    rax22 = r12_12;
    addr_4e46_18:
    r14_10->f8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax22) - reinterpret_cast<unsigned char>(r15_5));
    return r14_10;
    addr_4e38_11:
    esi23 = *reinterpret_cast<unsigned char*>(&v7);
    rax22 = r12_12 + 1;
    *reinterpret_cast<void***>(r12_12) = *reinterpret_cast<void***>(&esi23);
    goto addr_4e46_18;
}

void fun_4e73() {
    __asm__("cli ");
    goto readlinebuffer_delim;
}

void fun_4e83(int64_t rdi) {
    __asm__("cli ");
    goto fun_2400;
}

int32_t** fun_23e0();

int64_t fun_4e93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t r12_4;
    int64_t rbp5;
    int64_t rbx6;
    int32_t** rax7;
    int64_t rcx8;
    int32_t* r8_9;
    int64_t rax10;
    int64_t r9_11;
    int64_t rax12;

    __asm__("cli ");
    if (!rdx) {
        return 0;
    } else {
        r12_4 = rsi;
        rbp5 = rdi;
        rbx6 = rdx;
        rax7 = fun_23e0();
        *reinterpret_cast<int32_t*>(&rcx8) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
        r8_9 = *rax7;
        do {
            *reinterpret_cast<uint32_t*>(&rax10) = *reinterpret_cast<unsigned char*>(rbp5 + rcx8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r9_11) = *reinterpret_cast<unsigned char*>(r12_4 + rcx8);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rax12) = r8_9[rax10] - r8_9[r9_11];
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
            if (*reinterpret_cast<int32_t*>(&rax12)) 
                break;
            ++rcx8;
        } while (rcx8 != rbx6);
        return rax12;
    }
}

void fun_2730(void** rdi, int64_t rsi, int64_t rdx, struct s5* rcx);

struct s18 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s18* fun_2530();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4ee3(void** rdi) {
    struct s5* rcx2;
    void** rbx3;
    struct s18* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2730("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2410("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_2530();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2430(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_6683(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2420();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xe300;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_66c3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe300);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_66e3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xe300);
    }
    *rdi = esi;
    return 0xe300;
}

int64_t fun_6703(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xe300);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s19 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_6743(struct s19* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s19*>(0xe300);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s20 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s20* fun_6763(struct s20* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s20*>(0xe300);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x278a;
    if (!rdx) 
        goto 0x278a;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xe300;
}

struct s21 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_67a3(void** rdi, void** rsi, void** rdx, void** rcx, struct s21* r8) {
    struct s21* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s21*>(0xe300);
    }
    rax7 = fun_2420();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x67d6);
    *rax7 = r15d8;
    return rax13;
}

struct s22 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_6823(void** rdi, void** rsi, void*** rdx, struct s22* rcx) {
    struct s22* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s22*>(0xe300);
    }
    rax6 = fun_2420();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x6851);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x68ac);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_6913() {
    __asm__("cli ");
}

void** ge098 = reinterpret_cast<void**>(0);

int64_t slotvec0 = 0x100;

void fun_6923() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdx8;
    void** rcx9;
    struct s0* r8_10;
    void** r9_11;
    void** rdi12;
    void** rsi13;
    void** rdx14;
    void** rcx15;
    struct s0* r8_16;
    void** r9_17;
    void** rsi18;
    void** rdx19;
    void** rcx20;
    struct s0* r8_21;
    void** r9_22;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2400(rdi6, rsi7, rdx8, rcx9, r8_10, r9_11);
        } while (rbx4 != rbp5);
    }
    rdi12 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi12 != 0xe200) {
        fun_2400(rdi12, rsi13, rdx14, rcx15, r8_16, r9_17);
        ge098 = reinterpret_cast<void**>(0xe200);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xe090) {
        fun_2400(r12_2, rsi18, rdx19, rcx20, r8_21, r9_22);
        slotvec = reinterpret_cast<void**>(0xe090);
    }
    nslots = 1;
    return;
}

void fun_69c3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_69e3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_69f3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6a13(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_6a33(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s12* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = *reinterpret_cast<void***>(&g28);
    if (esi == 10) 
        goto 0x2790;
    rcx5 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_6ac3(void** rdi, int32_t esi, void** rdx, void** rcx) {
    void** rcx5;
    struct s12* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = *reinterpret_cast<void***>(&g28);
    if (esi == 10) 
        goto 0x2795;
    rcx6 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void** fun_6b53(int32_t edi, void** rsi) {
    void** rax3;
    struct s12* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = *reinterpret_cast<void***>(&g28);
    if (edi == 10) 
        goto 0x279a;
    rcx4 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, 0xffffffffffffffff, rcx4, 0, rsi, 0xffffffffffffffff, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx6) {
        fun_24e0();
    } else {
        return rax5;
    }
}

void** fun_6be3(int32_t edi, void** rsi, void** rdx) {
    void** rax4;
    struct s12* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = *reinterpret_cast<void***>(&g28);
    if (edi == 10) 
        goto 0x279f;
    rcx5 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_6c73(void** rdi, void** rsi, uint32_t edx) {
    struct s12* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x7680]");
    __asm__("movdqa xmm1, [rip+0x7688]");
    rax5 = *reinterpret_cast<void***>(&g28);
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x7671]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx11) {
        fun_24e0();
    } else {
        return rax10;
    }
}

void** fun_6d13(void** rdi, uint32_t esi) {
    struct s12* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x75e0]");
    __asm__("movdqa xmm1, [rip+0x75e8]");
    rax4 = *reinterpret_cast<void***>(&g28);
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x75d1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, 0xffffffffffffffff, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx10) {
        fun_24e0();
    } else {
        return rax9;
    }
}

void** fun_6db3(void** rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7540]");
    __asm__("movdqa xmm1, [rip+0x7548]");
    rax2 = *reinterpret_cast<void***>(&g28);
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x7529]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, 0xffffffffffffffff, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx4) {
        fun_24e0();
    } else {
        return rax3;
    }
}

void** fun_6e43(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x74b0]");
    __asm__("movdqa xmm1, [rip+0x74b8]");
    rax3 = *reinterpret_cast<void***>(&g28);
    __asm__("movdqa xmm2, [rip+0x74a6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx5) {
        fun_24e0();
    } else {
        return rax4;
    }
}

void** fun_6ed3(void** rdi, int32_t esi, void** rdx) {
    void** rdx4;
    struct s12* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = *reinterpret_cast<void***>(&g28);
    if (esi == 10) 
        goto 0x27a4;
    rcx5 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, 0xffffffffffffffff, rcx5, rdi, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_6f73(void** rdi, int64_t rsi, int64_t rdx, void** rcx) {
    void** rcx5;
    struct s12* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x737a]");
    rcx5 = *reinterpret_cast<void***>(&g28);
    __asm__("movdqa xmm1, [rip+0x7372]");
    __asm__("movdqa xmm2, [rip+0x737a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x27a9;
    if (!rdx) 
        goto 0x27a9;
    rcx6 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, 0xffffffffffffffff, rcx6, rdi, rcx, 0xffffffffffffffff, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void** fun_7013(int32_t edi, int64_t rsi, int64_t rdx, void** rcx, void** r8) {
    void** rcx6;
    struct s12* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x72da]");
    __asm__("movdqa xmm1, [rip+0x72e2]");
    __asm__("movdqa xmm2, [rip+0x72ea]");
    rcx6 = *reinterpret_cast<void***>(&g28);
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x27ae;
    if (!rdx) 
        goto 0x27ae;
    rcx7 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx10) {
        fun_24e0();
    } else {
        return rax9;
    }
}

void** fun_70c3(int64_t rdi, int64_t rsi, void** rdx) {
    void** rdx4;
    struct s12* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x722a]");
    rdx4 = *reinterpret_cast<void***>(&g28);
    __asm__("movdqa xmm1, [rip+0x7222]");
    __asm__("movdqa xmm2, [rip+0x722a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x27b3;
    if (!rsi) 
        goto 0x27b3;
    rcx5 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, 0xffffffffffffffff, rcx5, 0, rdx, 0xffffffffffffffff, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx7) {
        fun_24e0();
    } else {
        return rax6;
    }
}

void** fun_7163(int64_t rdi, int64_t rsi, void** rdx, void** rcx) {
    void** rcx5;
    struct s12* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x718a]");
    __asm__("movdqa xmm1, [rip+0x7192]");
    __asm__("movdqa xmm2, [rip+0x719a]");
    rcx5 = *reinterpret_cast<void***>(&g28);
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x27b8;
    if (!rsi) 
        goto 0x27b8;
    rcx6 = reinterpret_cast<struct s12*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rdx8) {
        fun_24e0();
    } else {
        return rax7;
    }
}

void fun_7203() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7213(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7233() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7253(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_25f0(signed char* rdi, void** rsi, void** rdx);

int64_t fun_7273(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_26a0(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_24d0(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_25f0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_25f0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_7323() {
    __asm__("cli ");
    goto fun_26a0;
}

void fun_7333() {
    __asm__("cli ");
}

struct s23 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_25e0(int64_t rdi, struct s5* rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_7353(struct s5* rdi, void** rsi, void** rdx, void** rcx, struct s23* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2740(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2740(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_24b0();
    fun_2740(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_25e0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_24b0();
    fun_2740(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_25e0(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_24b0();
        fun_2740(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xa568 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xa568;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_77c3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s24 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_77e3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s24* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s24* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = *reinterpret_cast<void***>(&g28);
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rax18) {
        fun_24e0();
    } else {
        return;
    }
}

void fun_7883(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = *reinterpret_cast<void***>(&g28);
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_7926_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7930_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rax20) {
        fun_24e0();
    } else {
        return;
    }
    addr_7926_5:
    goto addr_7930_7;
}

void fun_7963() {
    struct s5* rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_25e0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_24b0();
    fun_26b0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_24b0();
    fun_26b0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_24b0();
    goto fun_26b0;
}

int64_t fun_2460();

void xalloc_die();

void fun_7a03(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2460();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void* fun_2610(void** rdi);

void fun_7a43(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7a63(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7a83(void** rdi) {
    void* rax2;

    __asm__("cli ");
    rax2 = fun_2610(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

int64_t fun_2680();

void fun_7aa3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2680();
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7ad3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2680();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7b03(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2460();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7b43() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7b83(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7bb3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2460();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7c03(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2460();
            if (rax5) 
                break;
            addr_7c4d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_7c4d_5;
        rax8 = fun_2460();
        if (rax8) 
            goto addr_7c36_9;
        if (rbx4) 
            goto addr_7c4d_5;
        addr_7c36_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7c93(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2460();
            if (rax8) 
                break;
            addr_7cda_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_7cda_5;
        rax11 = fun_2460();
        if (rax11) 
            goto addr_7cc2_9;
        if (!rbx6) 
            goto addr_7cc2_9;
        if (r12_4) 
            goto addr_7cda_5;
        addr_7cc2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7d23(int64_t rdi, int64_t* rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    int64_t r13_6;
    int64_t rdi7;
    int64_t* r12_8;
    int64_t rsi9;
    int64_t rcx10;
    int64_t rbx11;
    int64_t rax12;
    int64_t rbp13;
    int64_t rbp14;
    int64_t rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = (rcx10 >> 1) + rcx10;
    if (__intrinsic()) {
        rbx11 = 0x7fffffffffffffff;
    }
    rax12 = rsi9;
    if (rbx11 <= rsi9) {
        rax12 = rbx11;
    }
    if (rsi9 >= 0) {
        rbx11 = rax12;
    }
    rbp13 = rbx11 * r8;
    if (__intrinsic()) {
        while (1) {
            rbp14 = 0x7fffffffffffffff;
            addr_7dcd_9:
            rbx11 = rbp14 / r8;
            rbp13 = rbp14 - rbp14 % r8;
            if (!r13_6) {
                addr_7de0_10:
                *r12_8 = 0;
            }
            addr_7d80_11:
            if (rbx11 - rcx10 >= rdi7) 
                goto addr_7da6_12;
            rcx10 = rcx10 + rdi7;
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7df4_14;
            if (rcx10 <= rsi9) 
                goto addr_7d9d_16;
            if (rsi9 >= 0) 
                goto addr_7df4_14;
            addr_7d9d_16:
            rcx10 = rcx10 * r8;
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7df4_14;
            addr_7da6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2680();
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7df4_14;
            if (!rbp13) 
                break;
            addr_7df4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (rbp13 <= 0x7f) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_7dcd_9;
        } else {
            if (!r13_6) 
                goto addr_7de0_10;
            goto addr_7d80_11;
        }
    }
}

int64_t fun_25c0();

void fun_7e23() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7e53() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7e83() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7ea3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25c0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7ec3(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2610(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

void fun_7f03(int64_t rdi, void** rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2610(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

struct s25 {
    signed char[1] pad1;
    void** f1;
};

void fun_7f43(int64_t rdi, struct s25* rsi) {
    void* rax3;

    __asm__("cli ");
    rax3 = fun_2610(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax3) + reinterpret_cast<int64_t>(rsi)) = 0;
        goto fun_25f0;
    }
}

void fun_7f83(void** rdi) {
    void** rax2;
    void* rax3;

    __asm__("cli ");
    rax2 = fun_24d0(rdi);
    rax3 = fun_2610(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_25f0;
    }
}

void fun_7fc3() {
    void** rdi1;

    __asm__("cli ");
    fun_24b0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_26c0();
    fun_2410(rdi1);
}

int32_t memcoll();

int64_t fun_80d3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int32_t eax5;
    int32_t* rax6;
    int32_t edi7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    eax5 = memcoll();
    rax6 = fun_2420();
    edi7 = *rax6;
    if (edi7) {
        collate_error(edi7, rdi, rsi, rdx, rcx);
        *reinterpret_cast<int32_t*>(&rax8) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

int32_t memcoll0();

int64_t fun_8133(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    int32_t eax5;
    int32_t* rax6;
    int32_t edi7;
    int64_t rax8;
    int64_t rax9;

    __asm__("cli ");
    eax5 = memcoll0();
    rax6 = fun_2420();
    edi7 = *rax6;
    if (edi7) {
        collate_error(edi7, rdi, rsi - 1, rdx, rcx - 1);
        *reinterpret_cast<int32_t*>(&rax8) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rax9) = eax5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        return rax9;
    }
}

void fun_2550(int64_t rdi);

void** fun_2700(void** rdi);

int64_t fun_2510(void** rdi);

int64_t fun_8193(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    unsigned char* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    unsigned char** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = *reinterpret_cast<void***>(&g28);
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<unsigned char*>("xstrtoul");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_2550("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_24e0();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_8504_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_8504_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_824d_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_8255_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2420();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_2770(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!((rcx9 + rdx23 * 2)[1] & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_828b_21;
    rsi = r15_14;
    rax24 = fun_2700(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2510(r13_18), r8 = r8, rax26 == 0))) {
            addr_828b_21:
            r12d11 = 4;
            goto addr_8255_13;
        } else {
            addr_82c9_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2510(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<unsigned char*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xa690 + rbp33 * 4) + 0xa690;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_828b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_824d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_824d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2510(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_82c9_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_8255_13;
}

void** fun_26f0(void** rdi);

int64_t fun_85c3(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void** rax7;
    void** v8;
    unsigned char* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    int32_t* rax19;
    int32_t* r12_20;
    unsigned char** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = *reinterpret_cast<void***>(&g28);
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<unsigned char*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_2550("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_24e0();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_8934_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_8934_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_867d_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_8685_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2420();
    *rax19 = 0;
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_2770(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!((rcx9 + rdx23 * 2)[1] & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_86bb_21;
    rsi = r15_14;
    rax24 = fun_26f0(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_2510(r13_18), r8 = r8, rax26 == 0))) {
            addr_86bb_21:
            r12d11 = 4;
            goto addr_8685_13;
        } else {
            addr_86f9_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2510(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<unsigned char*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xa778 + rbp33 * 4) + 0xa778;
        }
    } else {
        if (*r12_20) {
            r12d11 = 1;
            if (*r12_20 != 34) 
                goto addr_86bb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_867d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_867d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2510(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_86f9_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_8685_13;
}

int64_t fun_2450();

int64_t fun_89f3(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9) {
    int64_t rax7;
    uint32_t ebx8;
    int64_t rax9;
    int32_t* rax10;
    int32_t* rax11;

    __asm__("cli ");
    rax7 = fun_2450();
    ebx8 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32;
    rax9 = rpl_fclose(rdi, rsi, rdx, rcx, r8, r9);
    if (ebx8) {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            addr_8a4e_3:
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        } else {
            rax10 = fun_2420();
            *rax10 = 0;
            *reinterpret_cast<int32_t*>(&rax9) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax9)) {
            if (rax7) 
                goto addr_8a4e_3;
            rax11 = fun_2420();
            *reinterpret_cast<int32_t*>(&rax9) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax11 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        }
    }
    return rax9;
}

uint32_t fun_2470(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_8a63(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    int64_t v8;
    void** rax9;
    uint32_t eax10;
    uint32_t r12d11;
    int32_t eax12;
    int64_t rdx13;
    uint32_t eax14;
    int1_t zf15;
    void* rax16;
    int64_t rax17;
    int64_t rsi18;
    int64_t rdi19;
    uint32_t eax20;
    int64_t rdi21;
    uint32_t eax22;
    int32_t* rax23;
    int64_t rdi24;
    int32_t r13d25;
    uint32_t eax26;
    int32_t* rax27;
    int64_t rdi28;
    uint32_t eax29;
    uint32_t ecx30;
    int64_t rax31;
    uint32_t eax32;
    uint32_t eax33;
    uint32_t eax34;
    int32_t ecx35;
    int64_t rax36;

    __asm__("cli ");
    v8 = rdx;
    rax9 = *reinterpret_cast<void***>(&g28);
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax10 = fun_2470(rdi);
        r12d11 = eax10;
        goto addr_8b64_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax12 = have_dupfd_cloexec_0;
        *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
        if (eax12 < 0) {
            eax14 = fun_2470(rdi);
            r12d11 = eax14;
            if (reinterpret_cast<int32_t>(eax14) < reinterpret_cast<int32_t>(0) || (zf15 = have_dupfd_cloexec_0 == -1, !zf15)) {
                addr_8b64_3:
                rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax9) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
                if (rax16) {
                    fun_24e0();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax17) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
                    return rax17;
                }
            } else {
                addr_8c19_9:
                *reinterpret_cast<int32_t*>(&rsi18) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi19) = r12d11;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi19) + 4) = 0;
                eax20 = fun_2470(rdi19, rdi19);
                if (reinterpret_cast<int32_t>(eax20) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<int32_t*>(&rsi18) = 2, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi18) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi21) = r12d11, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi21) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx13) = eax20 | 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0, eax22 = fun_2470(rdi21, rdi21), eax22 == 0xffffffff)) {
                    rax23 = fun_2420();
                    *reinterpret_cast<uint32_t*>(&rdi24) = r12d11;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
                    r12d11 = 0xffffffff;
                    r13d25 = *rax23;
                    fun_2570(rdi24, rsi18, rdx13);
                    *rax23 = r13d25;
                    goto addr_8b64_3;
                }
            }
        } else {
            eax26 = fun_2470(rdi, rdi);
            r12d11 = eax26;
            if (reinterpret_cast<int32_t>(eax26) >= reinterpret_cast<int32_t>(0) || (rax27 = fun_2420(), *reinterpret_cast<int32_t*>(&rdi28) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi28) + 4) = 0, *rax27 != 22)) {
                have_dupfd_cloexec_0 = 1;
                goto addr_8b64_3;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx13) = *reinterpret_cast<uint32_t*>(&v8);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
                eax29 = fun_2470(rdi28);
                r12d11 = eax29;
                if (reinterpret_cast<int32_t>(eax29) < reinterpret_cast<int32_t>(0)) 
                    goto addr_8b64_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_8c19_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_8ac9_16;
    ecx30 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx30 > 10) 
        goto addr_8acd_18;
    rax31 = 1 << *reinterpret_cast<unsigned char*>(&ecx30);
    if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax31) & 0x502)) {
            addr_8acd_18:
            if (0) {
            }
        } else {
            addr_8b15_23:
            eax32 = fun_2470(rdi);
            r12d11 = eax32;
            goto addr_8b64_3;
        }
        eax33 = fun_2470(rdi);
        r12d11 = eax33;
        goto addr_8b64_3;
    }
    if (0) {
    }
    eax34 = fun_2470(rdi);
    r12d11 = eax34;
    goto addr_8b64_3;
    addr_8ac9_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_8acd_18;
    ecx35 = *reinterpret_cast<int32_t*>(&rsi);
    rax36 = 1 << *reinterpret_cast<unsigned char*>(&ecx35);
    if (!(*reinterpret_cast<uint32_t*>(&rax36) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax36) & 0xa0a) 
            goto addr_8b15_23;
        goto addr_8acd_18;
    }
}

signed char* fun_2630(int64_t rdi);

signed char* fun_8cd3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2630(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2500(uint32_t* rdi);

uint64_t fun_8d13(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = *reinterpret_cast<void***>(&g28);
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2500(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (rax9) {
        fun_24e0();
    } else {
        return r12_7;
    }
}

int64_t fun_8e53(void** rdi, void** rsi, void** rdx, void** rcx) {
    uint32_t eax5;
    unsigned char* r15_6;
    unsigned char* r14_7;
    uint32_t r9d8;
    uint32_t r8d9;
    uint32_t eax10;
    uint32_t r9d11;
    uint32_t r8d12;
    uint32_t r10d13;
    int32_t* rax14;
    int64_t rax15;

    __asm__("cli ");
    if (rsi != rcx || (eax5 = fun_25a0(rdi, rdx, rsi, rcx), !!eax5)) {
        r15_6 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
        r14_7 = reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx) + reinterpret_cast<unsigned char>(rcx));
        r9d8 = *r15_6;
        r8d9 = *r14_7;
        *r15_6 = 0;
        *r14_7 = 0;
        eax10 = strcoll_loop(rdi, rsi + 1, rdx, rcx + 1);
        r9d11 = *reinterpret_cast<unsigned char*>(&r9d8);
        r8d12 = *reinterpret_cast<unsigned char*>(&r8d9);
        r10d13 = eax10;
        *r15_6 = *reinterpret_cast<unsigned char*>(&r9d11);
        *r14_7 = *reinterpret_cast<unsigned char*>(&r8d12);
    } else {
        rax14 = fun_2420();
        r10d13 = eax5;
        *rax14 = 0;
    }
    *reinterpret_cast<uint32_t*>(&rax15) = r10d13;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    return rax15;
}

int64_t fun_8f03(void** rdi, void** rsi, void** rdx, void** rcx) {
    uint32_t eax5;
    int32_t* rax6;

    __asm__("cli ");
    if (rsi != rcx || (eax5 = fun_25a0(rdi, rdx, rsi, rcx), !!eax5)) {
        goto strcoll_loop;
    } else {
        rax6 = fun_2420();
        *rax6 = 0;
        return 0;
    }
}

void fun_8f73() {
    __asm__("cli ");
}

void fun_8f87() {
    __asm__("cli ");
    return;
}

int32_t xstrtoul(void** rdi);

void fun_2900() {
    void** rdi1;
    int32_t eax2;
    int64_t v3;

    print_pairables = 0;
    rdi1 = optarg;
    eax2 = xstrtoul(rdi1);
    if (eax2) 
        goto 0x346d;
    if (reinterpret_cast<uint64_t>(v3 - 1) > 1) 
        goto 0x346d;
    if (v3 == 1) 
        goto 0x2b9c;
    print_unpairables_2 = 1;
}

void fun_2960() {
    void** rdi1;
    void** rsi2;
    void** rax3;

    rdi1 = optarg;
    rax3 = string_to_join_field(rdi1, rsi2);
    set_join_field(0xe018, rax3);
    goto 0x2954;
}

void fun_2a20() {
    void** r15_1;
    void** rdx2;
    void** rcx3;
    struct s0* r8_4;
    void** r9_5;
    void** v6;
    void** v7;
    void** v8;
    int64_t v9;
    int64_t v10;
    int32_t eax11;

    r15_1 = optarg;
    eax11 = fun_25d0(r15_1, "auto", rdx2, rcx3, r8_4, r9_5, __return_address(), v6, v7, v8, v9, v10);
    if (eax11) {
        add_field_list(r15_1, "auto");
        goto 0x2954;
    } else {
        autoformat = 1;
        goto 0x2954;
    }
}

struct s26 {
    signed char[2] pad2;
    void** f2;
};

void fun_2a50() {
    void** rdi1;
    int64_t rax2;
    int32_t edx3;
    int64_t rdx4;
    int64_t rbx5;
    void** rsi6;
    void** rax7;
    void** rsi8;
    int64_t rdx9;

    rdi1 = optarg;
    *reinterpret_cast<uint32_t*>(&rax2) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    edx3 = static_cast<int32_t>(rax2 - 49);
    if (*reinterpret_cast<unsigned char*>(&edx3) > 1 || (*reinterpret_cast<void***>(rdi1 + 1) || (rdx4 = optind, rdi1 != &(*reinterpret_cast<struct s26**>(rbx5 + rdx4 * 8 - 8))->f2))) {
        rax7 = string_to_join_field(rdi1, rsi6);
        set_join_field(0xe020, rax7);
        rsi8 = join_field_1;
        set_join_field(0xe018, rsi8);
        goto 0x2952;
    } else {
        *reinterpret_cast<uint32_t*>(&rdx9) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&rax2) == 50);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx9) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) + rdx9 * 4 + 0x80) = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) + rdx9 * 4 + 0x80) + 1;
        goto 0x2954;
    }
}

void fun_2ab0() {
    ignore_case = 1;
    goto 0x2954;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2760(int64_t rdi, void** rsi);

uint32_t fun_2750(void** rdi, void** rsi);

void fun_5115() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    unsigned char** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_24b0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_24b0();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_24d0(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_5413_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_5413_22; else 
                            goto addr_580d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_58cd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5c20_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_5410_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_5410_30; else 
                                goto addr_5c39_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_24d0(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5c20_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_25a0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5c20_28; else 
                            goto addr_52bc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_5d80_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5c00_40:
                        if (r11_27 == 1) {
                            addr_578d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5d48_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_53c7_44;
                            }
                        } else {
                            goto addr_5c10_46;
                        }
                    } else {
                        addr_5d8f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_578d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_5413_22:
                                if (v47 != 1) {
                                    addr_5969_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_24d0(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_59b4_54;
                                    }
                                } else {
                                    goto addr_5420_56;
                                }
                            } else {
                                addr_53c5_57:
                                ebp36 = 0;
                                goto addr_53c7_44;
                            }
                        } else {
                            addr_5bf4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_5d8f_47; else 
                                goto addr_5bfe_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_578d_41;
                        if (v47 == 1) 
                            goto addr_5420_56; else 
                            goto addr_5969_52;
                    }
                }
                addr_5481_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_5318_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_533d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_5640_65;
                    } else {
                        addr_54a9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5cf8_67;
                    }
                } else {
                    goto addr_54a0_69;
                }
                addr_5351_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_539c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5cf8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_539c_81;
                }
                addr_54a0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_533d_64; else 
                    goto addr_54a9_66;
                addr_53c7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_547f_91;
                if (v22) 
                    goto addr_53df_93;
                addr_547f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5481_62;
                addr_59b4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_613b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_61ab_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_5faf_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2760(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2750(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_5aae_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_546c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_5ab8_112;
                    }
                } else {
                    addr_5ab8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_5b89_114;
                }
                addr_5478_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_547f_91;
                while (1) {
                    addr_5b89_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_6097_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_5af6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_60a5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_5b77_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_5b77_128;
                        }
                    }
                    addr_5b25_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_5b77_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_5af6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_5b25_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_539c_81;
                addr_60a5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5cf8_67;
                addr_613b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_5aae_109;
                addr_61ab_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_5aae_109;
                addr_5420_56:
                rax93 = fun_2770(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>(((*rax93 + reinterpret_cast<unsigned char>(rax24) * 2)[1] & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_546c_110;
                addr_5bfe_59:
                goto addr_5c00_40;
                addr_58cd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_5413_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_5478_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_53c5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_5413_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_5912_160;
                if (!v22) 
                    goto addr_5ce7_162; else 
                    goto addr_5ef3_163;
                addr_5912_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5ce7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5cf8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_57bb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_5623_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_5351_70; else 
                    goto addr_5637_169;
                addr_57bb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_5318_63;
                goto addr_54a0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5bf4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_5d2f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_5410_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_5308_178; else 
                        goto addr_5cb2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5bf4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_5413_22;
                }
                addr_5d2f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_5410_30:
                    r8d42 = 0;
                    goto addr_5413_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_5481_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5d48_42;
                    }
                }
                addr_5308_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5318_63;
                addr_5cb2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5c10_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_5481_62;
                } else {
                    addr_5cc2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_5413_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_6472_188;
                if (v28) 
                    goto addr_5ce7_162;
                addr_6472_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_5623_168;
                addr_52bc_37:
                if (v22) 
                    goto addr_62b3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_52d3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_5d80_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_5e0b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_5413_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_5308_178; else 
                        goto addr_5de7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5bf4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_5413_22;
                }
                addr_5e0b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_5413_22;
                }
                addr_5de7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5c10_46;
                goto addr_5cc2_186;
                addr_52d3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_5413_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_5413_22; else 
                    goto addr_52e4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_63be_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_6244_210;
            if (1) 
                goto addr_6242_212;
            if (!v29) 
                goto addr_5e7e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_63b1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_5640_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_53fb_219; else 
            goto addr_565a_220;
        addr_53df_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_53f3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_565a_220; else 
            goto addr_53fb_219;
        addr_5faf_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_565a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_5fcd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_24c0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_6440_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_5ea6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_6097_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_53f3_221;
        addr_5ef3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_53f3_221;
        addr_5637_169:
        goto addr_5640_65;
        addr_63be_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_565a_220;
        goto addr_5fcd_222;
        addr_6244_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
        if (!rax111) 
            goto addr_629e_236;
        fun_24e0();
        rsp25 = rsp25 - 8 + 8;
        goto addr_6440_225;
        addr_6242_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_6244_210;
        addr_5e7e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_6244_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_5ea6_226;
        }
        addr_63b1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_580d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa02c + rax113 * 4) + 0xa02c;
    addr_5c39_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xa12c + rax114 * 4) + 0xa12c;
    addr_62b3_190:
    addr_53fb_219:
    goto 0x50e0;
    addr_52e4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9f2c + rax115 * 4) + 0x9f2c;
    addr_629e_236:
    goto v116;
}

void fun_5300() {
}

void fun_54b8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x51b2;
}

void fun_5511() {
    goto 0x51b2;
}

void fun_55fe() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x5481;
    }
    if (v2) 
        goto 0x5ef3;
    if (!r10_3) 
        goto addr_605e_5;
    if (!v4) 
        goto addr_5f2e_7;
    addr_605e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_5f2e_7:
    goto 0x5334;
}

void fun_561c() {
}

void fun_56c7() {
    signed char v1;

    if (v1) {
        goto 0x564f;
    } else {
        goto 0x538a;
    }
}

void fun_56e1() {
    signed char v1;

    if (!v1) 
        goto 0x56da; else 
        goto "???";
}

void fun_5708() {
    goto 0x5623;
}

void fun_5788() {
}

void fun_57a0() {
}

void fun_57cf() {
    goto 0x5623;
}

void fun_5821() {
    goto 0x57b0;
}

void fun_5850() {
    goto 0x57b0;
}

void fun_5883() {
    goto 0x57b0;
}

void fun_5c50() {
    goto 0x5308;
}

void fun_5f4e() {
    signed char v1;

    if (v1) 
        goto 0x5ef3;
    goto 0x5334;
}

void fun_5ff5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x5334;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x5318;
        goto 0x5334;
    }
}

void fun_6412() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x5680;
    } else {
        goto 0x51b2;
    }
}

void fun_7428() {
    fun_24b0();
}

void fun_833c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x834b;
}

void fun_840c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x8419;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x834b;
}

void fun_8430() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_845c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x834b;
}

void fun_847d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x834b;
}

void fun_84a1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x834b;
}

void fun_84c5() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8454;
}

void fun_84e9() {
}

void fun_8509() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8454;
}

void fun_8525() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8454;
}

void fun_876c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0x877b;
}

void fun_883c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0x8849;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0x877b;
}

void fun_8860() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_888c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x877b;
}

void fun_88ad() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x877b;
}

void fun_88d1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0x877b;
}

void fun_88f5() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8884;
}

void fun_8919() {
}

void fun_8939() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8884;
}

void fun_8955() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0x8884;
}

void fun_2980() {
    void** rdi1;
    void** rsi2;
    void** rax3;

    rdi1 = optarg;
    rax3 = string_to_join_field(rdi1, rsi2);
    set_join_field(0xe020, rax3);
    goto 0x2954;
}

void fun_2ac0() {
    void** rdi1;
    void** r15_2;
    void** rdx3;
    void** rcx4;
    struct s0* r8_5;
    void** r9_6;
    void** v7;
    void** v8;
    void** v9;
    int64_t v10;
    int64_t v11;
    int32_t eax12;

    rdi1 = empty_filler;
    r15_2 = optarg;
    if (rdi1) {
        eax12 = fun_25d0(rdi1, r15_2, rdx3, rcx4, r8_5, r9_6, __return_address(), v7, v8, v9, v10, v11);
        if (eax12) 
            goto 0x367a;
    }
    empty_filler = r15_2;
    goto 0x2954;
}

void fun_553e() {
    goto 0x51b2;
}

void fun_5714() {
    goto 0x56cc;
}

void fun_57db() {
    goto 0x5308;
}

void fun_582d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x57b0;
    goto 0x53df;
}

void fun_585f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x57bb;
        goto 0x51e0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x565a;
        goto 0x53fb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5ff8;
    if (r10_8 > r15_9) 
        goto addr_5745_9;
    addr_574a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x6003;
    goto 0x5334;
    addr_5745_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_574a_10;
}

void fun_5892() {
    goto 0x53c7;
}

void fun_5c60() {
    goto 0x53c7;
}

void fun_63ff() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x551c;
    } else {
        goto 0x5680;
    }
}

void fun_74e0() {
}

void fun_83df() {
    if (__intrinsic()) 
        goto 0x8419; else 
        goto "???";
}

void fun_880f() {
    if (__intrinsic()) 
        goto 0x8849; else 
        goto "???";
}

void fun_29a0() {
    join_header_lines = 1;
    goto 0x2954;
}

void fun_589c() {
    goto 0x5837;
}

void fun_5c6a() {
    goto 0x578d;
}

void fun_7540() {
    fun_24b0();
    goto fun_2740;
}

void fun_29b0() {
    check_input_order = 2;
    goto 0x2954;
}

void fun_556d() {
    goto 0x51b2;
}

void fun_58a8() {
    goto 0x5837;
}

void fun_5c77() {
    goto 0x57de;
}

void fun_7580() {
    fun_24b0();
    goto fun_2740;
}

void fun_29c0() {
    check_input_order = 1;
    goto 0x2954;
}

void fun_559a() {
    goto 0x51b2;
}

void fun_58b4() {
    goto 0x57b0;
}

void fun_75c0() {
    fun_24b0();
    goto fun_2740;
}

void fun_29d0() {
    eolchar = 0;
    goto 0x2954;
}

void fun_55bc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5f50;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x5481;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x5481;
    }
    if (v11) 
        goto 0x62b3;
    if (r10_12 > r15_13) 
        goto addr_6303_8;
    addr_6308_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x6041;
    addr_6303_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_6308_9;
}

struct s27 {
    signed char[24] pad24;
    int64_t f18;
};

struct s28 {
    signed char[16] pad16;
    void** f10;
};

struct s29 {
    signed char[8] pad8;
    void** f8;
};

void fun_7610() {
    int64_t r15_1;
    struct s27* rbx2;
    void** r14_3;
    struct s28* rbx4;
    void** r13_5;
    struct s29* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    struct s5* rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_24b0();
    fun_2740(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x7632, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_29e0() {
    void** r15_1;
    uint32_t eax2;
    void** rdx3;
    void** rcx4;
    struct s0* r8_5;
    void** r9_6;
    void** v7;
    void** v8;
    void** v9;
    int64_t v10;
    int64_t v11;
    int32_t eax12;
    uint32_t edx13;

    r15_1 = optarg;
    eax2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_1));
    if (*reinterpret_cast<signed char*>(&eax2)) {
        if (*reinterpret_cast<void***>(r15_1 + 1)) {
            eax12 = fun_25d0(r15_1, "\\0", rdx3, rcx4, r8_5, r9_6, __return_address(), v7, v8, v9, v10, v11);
            if (eax12) 
                goto 0x34d6;
            eax2 = 0;
        }
    } else {
        eax2 = 10;
    }
    edx13 = tab;
    if (reinterpret_cast<int32_t>(edx13) >= reinterpret_cast<int32_t>(0)) {
        if (edx13 != eax2) 
            goto 0x3508;
    }
    tab = eax2;
    goto 0x2954;
}

void fun_7668() {
    fun_24b0();
    goto 0x7639;
}

struct s30 {
    signed char[32] pad32;
    int64_t f20;
};

struct s31 {
    signed char[24] pad24;
    int64_t f18;
};

struct s32 {
    signed char[16] pad16;
    void** f10;
};

struct s33 {
    signed char[8] pad8;
    void** f8;
};

struct s34 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_76a0() {
    int64_t rcx1;
    struct s30* rbx2;
    int64_t r15_3;
    struct s31* rbx4;
    void** r14_5;
    struct s32* rbx6;
    void** r13_7;
    struct s33* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s34* rbx12;
    void** rax13;
    struct s5* rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_24b0();
    fun_2740(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x76d4, __return_address(), rcx1);
    goto v15;
}

void fun_7718() {
    fun_24b0();
    goto 0x76db;
}
