void prjoin(undefined1 *param_1,undefined1 *param_2)

{
  char *pcVar1;
  byte *pbVar2;
  byte bVar3;
  uint uVar4;
  int *piVar5;
  undefined1 *puVar6;
  undefined8 uVar7;
  
  uVar4 = tab;
  if ((int)tab < 0) {
    uVar4 = 0x20;
  }
  piVar5 = outlist_head._16_8_;
  if (outlist_head._16_8_ == (int *)0x0) {
    puVar6 = param_1;
    uVar7 = join_field_1;
    if (param_1 == uni_blank) {
      puVar6 = param_2;
      uVar7 = join_field_2;
    }
    prfield(uVar7,puVar6);
    prfields(param_1,join_field_1,autocount_1);
    prfields(param_2,join_field_2,autocount_2);
  }
  else {
    while( true ) {
      if (*piVar5 == 0) {
        uVar7 = join_field_1;
        if (param_1 == uni_blank) {
          uVar7 = join_field_2;
        }
        prfield(uVar7);
        piVar5 = *(int **)(piVar5 + 4);
      }
      else {
        prfield(*(undefined8 *)(piVar5 + 2));
        piVar5 = *(int **)(piVar5 + 4);
      }
      if (piVar5 == (int *)0x0) break;
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = (char)uVar4;
      }
      else {
        __overflow(stdout,uVar4 & 0xff);
      }
    }
  }
  bVar3 = eolchar;
  pbVar2 = (byte *)stdout->_IO_write_ptr;
  if (stdout->_IO_write_end <= pbVar2) {
    __overflow(stdout,(uint)eolchar);
    return;
  }
  stdout->_IO_write_ptr = (char *)(pbVar2 + 1);
  *pbVar2 = bVar3;
  return;
}