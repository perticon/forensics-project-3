void prjoin(int32_t * line1, int32_t * line2) {
    int64_t v1 = (int64_t)line2;
    int64_t v2 = (int64_t)line1;
    if (g37 != NULL) {
        int32_t v3 = tab; // 0x3c65
        int64_t v4 = (int64_t)g37; // 0x3c50
        while (true) {
            int64_t v5 = v4;
            int32_t v6 = *(int32_t *)v5; // 0x3ccd
            int64_t v7; // 0x3c50
            if (v6 == 0) {
                int64_t v8 = line1 == (int32_t *)&uni_blank ? join_field_2 : join_field_1;
                prfield(v8, line1 == (int32_t *)&uni_blank ? v1 : v2);
                int64_t v9 = *(int64_t *)(v5 + 16); // 0x3ca8
                v7 = v9;
                if (v9 == 0) {
                    // break -> 0x3cef
                    break;
                }
            } else {
                // 0x3cd3
                prfield(*(int64_t *)(v5 + 8), v6 == 1 ? v2 : v1);
                int64_t v10 = *(int64_t *)(v5 + 16); // 0x3ce6
                v7 = v10;
                if (v10 == 0) {
                    // break -> 0x3cef
                    break;
                }
            }
            int64_t v11 = (int64_t)g30; // 0x3cb1
            int64_t * v12 = (int64_t *)(v11 + 40); // 0x3cb8
            uint64_t v13 = *v12; // 0x3cb8
            if (v13 >= *(int64_t *)(v11 + 48)) {
                // 0x3d30
                function_2520();
            } else {
                // 0x3cc2
                *v12 = v13 + 1;
                *(char *)v13 = (char)(v3 < 0 ? 32 : v3);
            }
            // 0x3ccd
            v4 = v7;
        }
    } else {
        int64_t v14 = line1 == (int32_t *)&uni_blank ? join_field_2 : join_field_1;
        prfield(v14, line1 == (int32_t *)&uni_blank ? v1 : v2);
        prfields(line1, join_field_1, autocount_1);
        prfields(line2, join_field_2, autocount_2);
    }
    int64_t v15 = (int64_t)g30; // 0x3cef
    int64_t * v16 = (int64_t *)(v15 + 40); // 0x3cfd
    uint64_t v17 = *v16; // 0x3cfd
    if (v17 >= *(int64_t *)(v15 + 48)) {
        // 0x3d90
        function_2520();
        return;
    }
    // 0x3d0b
    *v16 = v17 + 1;
    *(char *)v17 = eolchar;
}