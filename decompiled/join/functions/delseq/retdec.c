void delseq(int32_t * seq) {
    int64_t v1 = (int64_t)seq;
    int64_t * v2 = (int64_t *)(v1 + 8); // 0x3db9
    if (*v2 == 0) {
        // 0x3e0c
        function_2400();
        return;
    }
    int64_t v3 = 0; // 0x3dfc
    int64_t v4 = *(int64_t *)(*(int64_t *)(v1 + 16) + 8 * v3); // 0x3dc5
    if (v4 != 0) {
        // 0x3dce
        function_2400();
        *(int64_t *)(v4 + 40) = 0;
        function_2400();
        *(int64_t *)(v4 + 16) = 0;
    }
    // 0x3df9
    v3++;
    function_2400();
    while (*v2 > v3) {
        // 0x3dc0
        v4 = *(int64_t *)(*(int64_t *)(v1 + 16) + 8 * v3);
        if (v4 != 0) {
            // 0x3dce
            function_2400();
            *(int64_t *)(v4 + 40) = 0;
            function_2400();
            *(int64_t *)(v4 + 16) = 0;
        }
        // 0x3df9
        v3++;
        function_2400();
    }
    // 0x3e0c
    function_2400();
}