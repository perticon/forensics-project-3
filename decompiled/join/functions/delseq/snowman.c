void delseq(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9) {
    void** r12_7;
    void** rbx8;
    void** rbp9;
    void** rdi10;
    void** rdi11;
    void** rax12;

    r12_7 = rdi;
    *reinterpret_cast<int32_t*>(&rbx8) = 0;
    *reinterpret_cast<int32_t*>(&rbx8 + 4) = 0;
    if (*reinterpret_cast<void***>(rdi + 8)) {
        do {
            rbp9 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_7 + 16) + reinterpret_cast<unsigned char>(rbx8) * 8);
            if (rbp9) {
                rdi10 = *reinterpret_cast<void***>(rbp9 + 40);
                fun_2400(rdi10, rsi, rdx, rcx, r8, r9);
                *reinterpret_cast<void***>(rbp9 + 40) = reinterpret_cast<void**>(0);
                rdi11 = *reinterpret_cast<void***>(rbp9 + 16);
                fun_2400(rdi11, rsi, rdx, rcx, r8, r9);
                rax12 = *reinterpret_cast<void***>(r12_7 + 16);
                *reinterpret_cast<void***>(rbp9 + 16) = reinterpret_cast<void**>(0);
                rbp9 = *reinterpret_cast<void***>(rax12 + reinterpret_cast<unsigned char>(rbx8) * 8);
            }
            ++rbx8;
            fun_2400(rbp9, rsi, rdx, rcx, r8, r9);
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 8)) > reinterpret_cast<unsigned char>(rbx8));
    }
}