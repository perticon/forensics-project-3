void delseq(long param_1)

{
  long lVar1;
  ulong uVar2;
  void *__ptr;
  
  uVar2 = 0;
  if (*(long *)(param_1 + 8) != 0) {
    do {
      __ptr = *(void **)(*(long *)(param_1 + 0x10) + uVar2 * 8);
      if (__ptr != (void *)0x0) {
        free(*(void **)((long)__ptr + 0x28));
        *(undefined8 *)((long)__ptr + 0x28) = 0;
        free(*(void **)((long)__ptr + 0x10));
        lVar1 = *(long *)(param_1 + 0x10);
        *(undefined8 *)((long)__ptr + 0x10) = 0;
        __ptr = *(void **)(lVar1 + uVar2 * 8);
      }
      uVar2 = uVar2 + 1;
      free(__ptr);
    } while (uVar2 <= *(ulong *)(param_1 + 8) && *(ulong *)(param_1 + 8) != uVar2);
  }
  free(*(void **)(param_1 + 0x10));
  return;
}