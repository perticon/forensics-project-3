decode_field_spec (char const *s, int *file_index, size_t *field_index)
{
  /* The first character must be 0, 1, or 2.  */
  switch (s[0])
    {
    case '0':
      if (s[1])
        {
          /* '0' must be all alone -- no '.FIELD'.  */
          die (EXIT_FAILURE, 0, _("invalid field specifier: %s"), quote (s));
        }
      *file_index = 0;
      *field_index = 0;
      break;

    case '1':
    case '2':
      if (s[1] != '.')
        die (EXIT_FAILURE, 0, _("invalid field specifier: %s"), quote (s));
      *file_index = s[0] - '0';
      *field_index = string_to_join_field (s + 2);
      break;

    default:
      die (EXIT_FAILURE, 0,
           _("invalid file number in field spec: %s"), quote (s));

      /* Tell gcc -W -Wall that we can't get beyond this point.
         This avoids a warning (otherwise legit) that the caller's copies
         of *file_index and *field_index might be used uninitialized.  */
      abort ();

      break;
    }
}