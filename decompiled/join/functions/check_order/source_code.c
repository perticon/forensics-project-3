check_order (const struct line *prev,
             const struct line *current,
             int whatfile)
{
  if (check_input_order != CHECK_ORDER_DISABLED
      && ((check_input_order == CHECK_ORDER_ENABLED) || seen_unpairable))
    {
      if (!issued_disorder_warning[whatfile - 1])
        {
          size_t join_field = whatfile == 1 ? join_field_1 : join_field_2;
          if (keycmp (prev, current, join_field, join_field) > 0)
            {
              /* Exclude any trailing newline. */
              size_t len = current->buf.length;
              if (0 < len && current->buf.buffer[len - 1] == '\n')
                --len;

              /* If the offending line is longer than INT_MAX, output
                 only the first INT_MAX bytes in this diagnostic.  */
              len = MIN (INT_MAX, len);

              error ((check_input_order == CHECK_ORDER_ENABLED
                      ? EXIT_FAILURE : 0),
                     0, _("%s:%"PRIuMAX": is not sorted: %.*s"),
                     g_names[whatfile - 1], line_no[whatfile - 1],
                     (int) len, current->buf.buffer);

              /* If we get to here, the message was merely a warning.
                 Arrange to issue it only once per file.  */
              issued_disorder_warning[whatfile - 1] = true;
            }
        }
    }
}