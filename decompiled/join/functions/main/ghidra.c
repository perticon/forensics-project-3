undefined8 main(int param_1,undefined8 *param_2)

{
  long *plVar1;
  byte bVar2;
  long lVar3;
  bool bVar4;
  char *pcVar5;
  char cVar6;
  char cVar7;
  int iVar8;
  uint uVar9;
  long lVar10;
  long lVar11;
  int *piVar12;
  undefined8 uVar13;
  undefined1 *puVar14;
  undefined1 *puVar15;
  byte *pbVar16;
  void *__ptr;
  ulong uVar17;
  long in_FS_OFFSET;
  bool bVar18;
  undefined8 uVar19;
  ulong local_c0;
  int local_9c;
  int local_98;
  int local_94;
  void *local_90;
  ulong local_88;
  undefined8 local_80;
  long *local_78;
  long local_68;
  undefined8 local_60;
  long *local_58;
  undefined local_50 [8];
  undefined8 local_48;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_98 = 0;
  local_48 = 0;
  local_94 = 0;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  hard_LC_COLLATE = hard_locale(3);
  atexit(close_stdout);
  atexit(free_spareline);
  print_pairables = '\x01';
  seen_unpairable = 0;
  issued_disorder_warning = 0;
  check_input_order = 0;
  while( true ) {
    uVar19 = 0x1028bb;
    iVar8 = getopt_long(param_1,param_2,"-a:e:i1:2:j:o:t:v:z",longopts,0);
    pbVar16 = optarg;
    if (iVar8 == -1) break;
    local_9c = 0;
    if (0x82 < iVar8) {
switchD_001028f3_caseD_33:
                    /* WARNING: Subroutine does not return */
      usage(1);
    }
    if (iVar8 < 0x31) {
      if (iVar8 == -0x82) {
                    /* WARNING: Subroutine does not return */
        usage(0);
      }
      if (iVar8 != 1) {
        if (iVar8 == -0x83) {
          version_etc(stdout,&DAT_00109b2c,"GNU coreutils",Version,"Mike Haertel",0);
                    /* WARNING: Subroutine does not return */
          exit(0);
        }
        goto switchD_001028f3_caseD_33;
      }
      add_file_name_constprop_0
                (optarg,g_names,local_50,&local_48,&local_94,&local_98,&local_9c,uVar19);
      local_98 = local_9c;
    }
    else {
      switch(iVar8) {
      case 0x31:
        uVar19 = string_to_join_field(optarg);
        set_join_field(&join_field_1,uVar19);
        local_98 = 0;
        break;
      case 0x32:
        uVar19 = string_to_join_field(optarg);
        set_join_field(&join_field_2,uVar19);
        local_98 = 0;
        break;
      default:
        goto switchD_001028f3_caseD_33;
      case 0x65:
        if ((empty_filler != (byte *)0x0) &&
           (iVar8 = strcmp((char *)empty_filler,(char *)optarg), iVar8 != 0)) {
          uVar19 = dcgettext(0,"conflicting empty-field replacement strings",5);
                    /* WARNING: Subroutine does not return */
          error(1,0,uVar19);
        }
        local_98 = 0;
        empty_filler = pbVar16;
        break;
      case 0x69:
        ignore_case = 1;
        local_98 = 0;
        break;
      case 0x6a:
        bVar2 = *optarg;
        if (((1 < (byte)(bVar2 - 0x31)) || (optarg[1] != 0)) ||
           (optarg != (byte *)(param_2[(long)optind + -1] + 2))) {
          uVar19 = string_to_join_field();
          set_join_field(&join_field_1,uVar19);
          set_join_field(&join_field_2,join_field_1);
          goto LAB_00102952;
        }
        local_9c = (bVar2 == 0x32) + 1;
        piVar12 = (int *)((long)&local_48 + (ulong)(bVar2 == 0x32) * 4);
        *piVar12 = *piVar12 + 1;
        local_98 = local_9c;
        break;
      case 0x6f:
        iVar8 = strcmp((char *)optarg,"auto");
        if (iVar8 == 0) {
          autoformat = '\x01';
          local_98 = iVar8;
        }
        else {
          add_field_list();
          local_9c = 3;
          local_98 = 3;
        }
        break;
      case 0x74:
        uVar9 = (uint)*optarg;
        if (*optarg == 0) {
          uVar9 = 10;
        }
        else if (optarg[1] != 0) {
          iVar8 = strcmp((char *)optarg,"\\0");
          if (iVar8 != 0) {
            uVar19 = quote(pbVar16);
            uVar13 = dcgettext(0,"multi-character tab %s",5);
                    /* WARNING: Subroutine does not return */
            error(1,0,uVar13,uVar19);
          }
          uVar9 = 0;
        }
        if ((-1 < (int)tab) && (tab != uVar9)) {
          uVar19 = dcgettext(0,"incompatible tabs",5);
                    /* WARNING: Subroutine does not return */
          error(1,0,uVar19);
        }
        tab = uVar9;
        local_98 = 0;
        break;
      case 0x76:
        print_pairables = '\0';
      case 0x61:
        iVar8 = xstrtoul(optarg,0,10,&local_68);
        if ((iVar8 != 0) || (1 < local_68 - 1U)) {
          uVar19 = quote(optarg);
          uVar13 = dcgettext(0,"invalid field number: %s",5);
                    /* WARNING: Subroutine does not return */
          error(1,0,uVar13,uVar19);
        }
        if (local_68 == 1) {
          print_unpairables_1 = '\x01';
        }
        else {
          print_unpairables_2 = '\x01';
        }
LAB_00102952:
        local_98 = 0;
        break;
      case 0x7a:
        eolchar = 0;
        local_98 = 0;
        break;
      case 0x80:
        check_input_order = 1;
        local_98 = 0;
        break;
      case 0x81:
        check_input_order = 2;
        local_98 = 0;
        break;
      case 0x82:
        join_header_lines = '\x01';
        local_98 = 0;
      }
    }
  }
  local_98 = 0;
  while (optind < param_1) {
    optind = optind + 1;
    add_file_name_constprop_0();
  }
  if (local_94 != 2) {
    if (local_94 != 0) {
      uVar19 = quote(param_2[(long)param_1 + -1]);
      uVar13 = dcgettext(0,"missing operand after %s",5);
                    /* WARNING: Subroutine does not return */
      error(0,0,uVar13,uVar19);
    }
    uVar19 = dcgettext(0,"missing operand",5);
                    /* WARNING: Subroutine does not return */
    error(0,0,uVar19);
  }
  if ((int)local_48 != 0) {
    set_join_field(&join_field_1,0);
    set_join_field(&join_field_2,0);
  }
  if (local_48._4_4_ != 0) {
    set_join_field(&join_field_1,1);
    set_join_field(&join_field_2,1);
  }
  pcVar5 = g_names._0_8_;
  if (join_field_1 == -1) {
    join_field_1 = 0;
  }
  if (join_field_2 == -1) {
    join_field_2 = 0;
  }
  iVar8 = strcmp(g_names._0_8_,"-");
  lVar10 = stdin;
  if (iVar8 != 0) {
    lVar10 = fopen_safer(pcVar5,"r");
  }
  pcVar5 = g_names._8_8_;
  if (lVar10 == 0) {
    uVar19 = quotearg_n_style_colon(0,3,g_names._0_8_);
    piVar12 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar12,"%s",uVar19);
  }
  iVar8 = strcmp(g_names._8_8_,"-");
  lVar11 = stdin;
  if (iVar8 != 0) {
    lVar11 = fopen_safer(pcVar5,"r");
  }
  if (lVar11 == 0) {
    uVar19 = quotearg_n_style_colon(0,3,g_names._8_8_);
    piVar12 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar12,"%s",uVar19);
  }
  if (lVar10 == lVar11) {
    uVar19 = dcgettext(0,"both files cannot be standard input",5);
    piVar12 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar12,uVar19);
  }
  fadvise(lVar10,2);
  fadvise(lVar11,2);
  local_88 = 0;
  local_80 = 0;
  local_78 = (long *)0x0;
  getseq(lVar10,&local_88,1);
  local_68 = 0;
  local_60 = 0;
  local_58 = (long *)0x0;
  getseq(lVar11,&local_68,2);
  if (autoformat != '\0') {
    autocount_1 = local_88;
    if (local_88 != 0) {
      autocount_1 = *(ulong *)(*local_78 + 0x18);
    }
    autocount_2 = local_68;
    if (local_68 != 0) {
      autocount_2 = *(long *)(*local_58 + 0x18);
    }
  }
  if (join_header_lines == '\0') {
LAB_00102e92:
    while (local_88 != 0) {
      uVar17 = local_88;
      if (local_68 == 0) goto LAB_00103063;
      iVar8 = keycmp(*local_78,*local_58,join_field_1,join_field_2);
      if (iVar8 < 0) {
        if (print_unpairables_1 != '\0') {
          prjoin(*local_78,uni_blank);
        }
        local_88 = 0;
        getseq(lVar10,&local_88,1);
        seen_unpairable = 1;
      }
      else if (iVar8 == 0) {
        do {
          cVar6 = getseq(lVar10,&local_88,1);
          if (cVar6 == '\0') {
            local_88 = local_88 + 1;
            bVar18 = true;
            goto LAB_00102f6a;
          }
          iVar8 = keycmp(local_78[local_88 - 1],*local_58,join_field_1,join_field_2);
        } while (iVar8 == 0);
        bVar18 = false;
LAB_00102f6a:
        do {
          cVar6 = getseq(lVar11,&local_68,2);
          if (cVar6 == '\0') {
            local_68 = local_68 + 1;
            bVar4 = true;
            goto LAB_00102f89;
          }
          iVar8 = keycmp(*local_78,local_58[local_68 + -1],join_field_1,join_field_2);
        } while (iVar8 == 0);
        bVar4 = false;
LAB_00102f89:
        if ((print_pairables != '\0') && (local_c0 = 0, local_88 != 1)) {
          do {
            uVar17 = 0;
            if (local_68 != 1) {
              do {
                plVar1 = local_58 + uVar17;
                uVar17 = uVar17 + 1;
                prjoin(local_78[local_c0],*plVar1);
              } while (uVar17 < local_68 - 1U);
            }
            local_c0 = local_c0 + 1;
          } while (local_c0 < local_88 - 1);
        }
        if (!bVar18) {
          lVar3 = *local_78;
          *local_78 = local_78[local_88 - 1];
          local_78[local_88 - 1] = lVar3;
        }
        local_88 = (ulong)!bVar18;
        if (bVar4) {
          local_68 = 0;
        }
        else {
          lVar3 = *local_58;
          *local_58 = local_58[local_68 + -1];
          local_58[local_68 + -1] = lVar3;
          local_68 = 1;
        }
      }
      else {
        if (print_unpairables_2 != '\0') {
          prjoin(uni_blank,*local_58);
        }
        local_68 = 0;
        getseq(lVar11,&local_68,2);
        seen_unpairable = 1;
      }
    }
  }
  else {
    if (local_88 != 0) {
      puVar14 = uni_blank;
      puVar15 = (undefined1 *)*local_78;
      if (local_68 != 0) goto LAB_00102e05;
LAB_00102e0d:
      prjoin(puVar15,puVar14);
      prevline._0_8_ = 0;
      prevline._8_8_ = 0;
      if (local_88 != 0) {
        local_88 = 0;
        getseq(lVar10,&local_88,1);
      }
      if (local_68 != 0) {
        local_68 = 0;
        getseq(lVar11,&local_68,2);
      }
      goto LAB_00102e92;
    }
    if (local_68 != 0) {
      puVar15 = uni_blank;
LAB_00102e05:
      puVar14 = (undefined1 *)*local_58;
      goto LAB_00102e0d;
    }
  }
  uVar17 = 0;
LAB_00103063:
  local_90 = (void *)0x0;
  if ((check_input_order == 2) ||
     (((char)issued_disorder_warning != '\0' && (issued_disorder_warning._1_1_ != '\0')))) {
    if ((print_unpairables_1 == '\0') || (uVar17 == 0)) {
      if (print_unpairables_2 != '\0') goto LAB_00103337;
LAB_001030ab:
      __ptr = (void *)0x0;
      goto LAB_001030ae;
    }
    cVar6 = '\0';
LAB_00103403:
    prjoin(*local_78,uni_blank);
LAB_00103204:
    if (local_68 != 0) {
      seen_unpairable = 1;
    }
LAB_00103220:
    do {
      cVar7 = get_line(lVar10,&local_90,1);
      if (cVar7 == '\0') break;
      if (print_unpairables_1 == '\0') {
      }
      else {
        prjoin(local_90,uni_blank);
        bVar18 = (char)issued_disorder_warning == '\0';
        issued_disorder_warning._0_1_ = print_unpairables_1;
        if (bVar18) goto LAB_00103220;
      }
    } while ((char)issued_disorder_warning != '\0');
    if (((print_unpairables_2 != '\0') || (cVar6 != '\0')) && (local_68 != 0)) goto LAB_00103348;
  }
  else {
    if (uVar17 != 0) {
      cVar6 = print_unpairables_1;
      if (print_unpairables_1 != '\0') goto LAB_00103403;
      cVar6 = '\x01';
      goto LAB_00103204;
    }
LAB_00103337:
    if (local_68 == 0) goto LAB_001030ab;
LAB_00103348:
    if (print_unpairables_2 != '\0') {
      prjoin(uni_blank,*local_58);
    }
    if (local_88 != 0) {
      seen_unpairable = 1;
    }
LAB_00103360:
    do {
      cVar6 = get_line(lVar11,&local_90,2);
      if (cVar6 == '\0') break;
      if (print_unpairables_2 == '\0') {
      }
      else {
        prjoin(uni_blank,local_90);
        bVar18 = issued_disorder_warning._1_1_ == '\0';
        issued_disorder_warning._1_1_ = print_unpairables_2;
        if (bVar18) goto LAB_00103360;
      }
    } while (issued_disorder_warning._1_1_ != '\0');
  }
  __ptr = local_90;
  if (local_90 != (void *)0x0) {
    free(*(void **)((long)local_90 + 0x28));
    *(undefined8 *)((long)__ptr + 0x28) = 0;
    free(*(void **)((long)__ptr + 0x10));
    *(undefined8 *)((long)__ptr + 0x10) = 0;
  }
LAB_001030ae:
  free(__ptr);
  delseq(&local_88);
  delseq(&local_68);
  iVar8 = rpl_fclose(lVar10);
  if (iVar8 != 0) {
    uVar19 = quotearg_n_style_colon(0,3,g_names._0_8_);
    piVar12 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar12,"%s",uVar19);
  }
  iVar8 = rpl_fclose(lVar11);
  if (iVar8 != 0) {
    uVar19 = quotearg_n_style_colon(0,3,g_names._8_8_);
    piVar12 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar12,"%s",uVar19);
  }
  if (((char)issued_disorder_warning == '\0') && (issued_disorder_warning._1_1_ == '\0')) {
    if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return 0;
  }
  uVar19 = dcgettext(0,"input is not in sorted order",5);
                    /* WARNING: Subroutine does not return */
  error(1,0,uVar19);
}