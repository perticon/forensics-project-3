void usage(int param_1)

{
  FILE *pFVar1;
  undefined **ppuVar2;
  int iVar3;
  undefined8 uVar4;
  char *pcVar5;
  undefined8 uVar6;
  undefined *puVar7;
  long in_FS_OFFSET;
  undefined *local_b8;
  char *local_b0;
  char *local_a8 [5];
  char *local_80;
  char *local_78;
  char *local_70;
  char *local_68;
  char *local_60;
  undefined8 local_58;
  undefined8 local_50;
  undefined8 local_40;
  
  uVar6 = program_name;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  if (param_1 != 0) {
    uVar4 = dcgettext(0,"Try \'%s --help\' for more information.\n",5);
    __fprintf_chk(stderr,1,uVar4,uVar6);
    goto LAB_0010440e;
  }
  uVar4 = dcgettext(0,"Usage: %s [OPTION]... FILE1 FILE2\n",5);
  __printf_chk(1,uVar4,uVar6);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "For each pair of input lines with identical join fields, write a line to\nstandard output.  The default join field is the first, delimited by blanks.\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,"\nWhen FILE1 or FILE2 (not both) is -, read standard input.\n",5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "\n  -a FILENUM             also print unpairable lines from file FILENUM, where\n                           FILENUM is 1 or 2, corresponding to FILE1 or FILE2\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "  -e STRING              replace missing (empty) input fields with STRING;\n                           I.e., missing fields specified with \'-12jo\' options\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "  -i, --ignore-case      ignore differences in case when comparing fields\n  -j FIELD               equivalent to \'-1 FIELD -2 FIELD\'\n  -o FORMAT              obey FORMAT while constructing output line\n  -t CHAR                use CHAR as input and output field separator\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "  -v FILENUM             like -a FILENUM, but suppress joined output lines\n  -1 FIELD               join on this FIELD of file 1\n  -2 FIELD               join on this FIELD of file 2\n      --check-order      check that the input is correctly sorted, even\n                           if all input lines are pairable\n      --nocheck-order    do not check that the input is correctly sorted\n      --header           treat the first line in each file as field headers,\n                           print them without trying to pair them\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,"  -z, --zero-terminated  line delimiter is NUL, not newline\n",5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,"      --help        display this help and exit\n",5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,"      --version     output version information and exit\n",5);
  fputs_unlocked(pcVar5,pFVar1);
  pFVar1 = stdout;
  pcVar5 = (char *)dcgettext(0,
                             "\nUnless -t CHAR is given, leading blanks separate fields and are ignored,\nelse fields are separated by CHAR.  Any FIELD is a field number counted\nfrom 1.  FORMAT is one or more comma or blank separated specifications,\neach being \'FILENUM.FIELD\' or \'0\'.  Default FORMAT outputs the join field,\nthe remaining fields from FILE1, the remaining fields from FILE2, all\nseparated by CHAR.  If FORMAT is the keyword \'auto\', then the first\nline of each file determines the number of fields output for each line.\n\nImportant: FILE1 and FILE2 must be sorted on the join fields.\nE.g., use \"sort -k 1b,1\" if \'join\' has no options,\nor use \"join -t \'\'\" if \'sort\' has no options.\nNote, comparisons honor the rules specified by \'LC_COLLATE\'.\nIf the input is not sorted and some lines cannot be joined, a\nwarning message will be given.\n"
                             ,5);
  fputs_unlocked(pcVar5,pFVar1);
  local_58 = 0;
  local_b8 = &DAT_00109b31;
  local_b0 = "test invocation";
  local_a8[0] = "coreutils";
  local_a8[1] = "Multi-call invocation";
  local_a8[4] = "sha256sum";
  local_a8[2] = "sha224sum";
  local_78 = "sha384sum";
  local_a8[3] = "sha2 utilities";
  local_80 = "sha2 utilities";
  local_70 = "sha2 utilities";
  local_68 = "sha512sum";
  local_60 = "sha2 utilities";
  local_50 = 0;
  ppuVar2 = &local_b8;
  do {
    puVar7 = (undefined *)ppuVar2;
    if (*(char **)(puVar7 + 0x10) == (char *)0x0) break;
    iVar3 = strcmp("join",*(char **)(puVar7 + 0x10));
    ppuVar2 = (undefined **)(puVar7 + 0x10);
  } while (iVar3 != 0);
  puVar7 = *(undefined **)(puVar7 + 0x18);
  if (puVar7 == (undefined *)0x0) {
    uVar6 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar6,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar5 = setlocale(5,(char *)0x0);
    if (pcVar5 != (char *)0x0) {
      iVar3 = strncmp(pcVar5,"en_",3);
      if (iVar3 != 0) {
        puVar7 = &DAT_00109b2c;
        goto LAB_001047c0;
      }
    }
    uVar6 = dcgettext(0,"Full documentation <%s%s>\n",5);
    puVar7 = &DAT_00109b2c;
    __printf_chk(1,uVar6,"https://www.gnu.org/software/coreutils/",&DAT_00109b2c);
    pcVar5 = " invocation";
  }
  else {
    uVar6 = dcgettext(0,"\n%s online help: <%s>\n",5);
    __printf_chk(1,uVar6,"GNU coreutils","https://www.gnu.org/software/coreutils/");
    pcVar5 = setlocale(5,(char *)0x0);
    if (pcVar5 != (char *)0x0) {
      iVar3 = strncmp(pcVar5,"en_",3);
      if (iVar3 != 0) {
LAB_001047c0:
        pFVar1 = stdout;
        pcVar5 = (char *)dcgettext(0,
                                   "Report any translation bugs to <https://translationproject.org/team/>\n"
                                   ,5);
        fputs_unlocked(pcVar5,pFVar1);
      }
    }
    uVar6 = dcgettext(0,"Full documentation <%s%s>\n",5);
    __printf_chk(1,uVar6,"https://www.gnu.org/software/coreutils/",&DAT_00109b2c);
    pcVar5 = " invocation";
    if (puVar7 != &DAT_00109b2c) {
      pcVar5 = "";
    }
  }
  uVar6 = dcgettext(0,"or available locally via: info \'(coreutils) %s%s\'\n",5);
  __printf_chk(1,uVar6,puVar7,pcVar5);
LAB_0010440e:
                    /* WARNING: Subroutine does not return */
  exit(param_1);
}