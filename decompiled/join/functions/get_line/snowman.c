signed char get_line(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9) {
    int64_t rbx7;
    void** r15_8;
    int32_t v9;
    void** rax10;
    void** rax11;
    void** rdx12;
    int64_t rax13;
    void** rdi14;
    void** rdi15;
    void** rax16;
    int32_t* rax17;
    void** rdx18;
    struct s7* rsi19;
    void** r13_20;
    void** r14_21;
    void** rax22;
    void** rdx23;
    void** r13_24;
    void** r14_25;
    void** r12_26;
    void** rdi27;
    int32_t eax28;
    int1_t zf29;
    void** rdx30;
    uint32_t eax31;
    uint32_t ebp32;
    void** rdx33;
    signed char al34;
    int64_t v35;
    unsigned char** rax36;
    unsigned char* rdx37;
    unsigned char** r14_38;
    void** rax39;
    void** rdi40;
    int64_t rsi41;
    void** rbp42;
    void** r13_43;
    int64_t r9_44;
    void** rax45;
    void** rdx46;
    struct s8* rdx47;
    int64_t rsi48;
    struct s9* rdi49;
    void** rax50;
    struct s10* rdx51;
    void** v52;
    void*** v53;
    int64_t rsi54;
    struct s0* rax55;
    void** rdx56;
    void** rax57;
    void* r14_58;
    struct s11* rax59;

    rbx7 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx + 0xffffffffffffffff));
    r15_8 = *reinterpret_cast<void***>(rsi);
    v9 = *reinterpret_cast<int32_t*>(&rdx);
    if (*reinterpret_cast<void***>(0xe1d0 + rbx7 * 8) != r15_8) {
        if (!r15_8) 
            goto addr_415e_3; else 
            goto addr_3ebb_4;
    }
    rax10 = *reinterpret_cast<void***>(0xe1a0 + rbx7 * 8);
    *reinterpret_cast<void***>(0xe1a0 + rbx7 * 8) = r15_8;
    r15_8 = rax10;
    *reinterpret_cast<void***>(rsi) = rax10;
    if (r15_8) {
        addr_3ebb_4:
        *reinterpret_cast<void***>(r15_8 + 24) = reinterpret_cast<void**>(0);
    } else {
        addr_415e_3:
        rax11 = xcalloc(1, 48);
        *reinterpret_cast<void***>(rsi) = rax11;
        r15_8 = rax11;
    }
    *reinterpret_cast<int32_t*>(&rdx12) = reinterpret_cast<signed char>(eolchar);
    *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
    rax13 = readlinebuffer_delim(r15_8, rdi, rdx12);
    if (!rax13) {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi)) & 32)) {
            rdi14 = *reinterpret_cast<void***>(r15_8 + 40);
            fun_2400(rdi14, rdi, rdx12, rcx, r8, r9);
            *reinterpret_cast<void***>(r15_8 + 40) = reinterpret_cast<void**>(0);
            rdi15 = *reinterpret_cast<void***>(r15_8 + 16);
            fun_2400(rdi15, rdi, rdx12, rcx, r8, r9);
            *reinterpret_cast<void***>(r15_8 + 16) = reinterpret_cast<void**>(0);
            return 0;
        }
        rax16 = fun_24b0();
        rax17 = fun_2420();
        rdx18 = rax16;
        *reinterpret_cast<int32_t*>(&rsi19) = *rax17;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi19) + 4) = 0;
        fun_26c0();
        r13_20 = rsi19->f0;
        r14_21 = rsi19->f10;
        if (r13_20 != rsi19->f8) 
            goto addr_434e_11;
        rax22 = x2nrealloc(r14_21, &rsi19->f8, 8);
        r13_20 = rsi19->f0;
        rdx23 = rsi19->f8;
        rsi19->f10 = rax22;
        r14_21 = rax22;
        if (reinterpret_cast<unsigned char>(r13_20) < reinterpret_cast<unsigned char>(rdx23)) 
            goto addr_4394_13;
    } else {
        r13_24 = *reinterpret_cast<void***>(r15_8 + 16);
        *reinterpret_cast<int64_t*>(0xe1c0 + rbx7 * 8) = *reinterpret_cast<int64_t*>(0xe1c0 + rbx7 * 8) + 1;
        r14_25 = *reinterpret_cast<void***>(r15_8 + 8) + 0xffffffffffffffff;
        r12_26 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_24) + reinterpret_cast<unsigned char>(r14_25));
        if (r13_24 == r12_26) {
            addr_3fdb_15:
            rdi27 = *reinterpret_cast<void***>(0xe1d0 + rbx7 * 8);
            if (rdi27 && ((eax28 = check_input_order, eax28 != 2) && ((eax28 == 1 || (zf29 = seen_unpairable == 0, !zf29)) && !*reinterpret_cast<signed char*>(0xe198 + rbx7)))) {
                rdx30 = join_field_1;
                if (v9 != 1) {
                    rdx30 = join_field_2;
                }
                eax31 = keycmp(rdi27, r15_8, rdx30, rdx30, r8, r9);
                if (!(reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(eax31) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(eax31 == 0))) {
                    if (*reinterpret_cast<void***>(r15_8 + 8)) {
                        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 8) + 0xffffffffffffffff) > reinterpret_cast<unsigned char>(0x7fffffff)) {
                        }
                        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 16)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 8)) + 0xffffffffffffffff) != 10 && reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 8)) <= reinterpret_cast<unsigned char>(0x7fffffff)) {
                        }
                    }
                    fun_24b0();
                    fun_26c0();
                    *reinterpret_cast<signed char*>(0xe198 + rbx7) = 1;
                    goto addr_40dc_25;
                }
            }
        } else {
            ebp32 = tab;
            if (reinterpret_cast<int32_t>(ebp32) < reinterpret_cast<int32_t>(0)) 
                goto addr_41b8_27;
            if (ebp32 == 10) 
                goto addr_41a0_29; else 
                goto addr_3f1b_30;
        }
    }
    addr_434e_11:
    *reinterpret_cast<int32_t*>(&rdx33) = *reinterpret_cast<int32_t*>(&rdx18);
    *reinterpret_cast<int32_t*>(&rdx33 + 4) = 0;
    al34 = get_line(1, r14_21 + reinterpret_cast<unsigned char>(r13_20) * 8, rdx33, rcx, r8, r9);
    if (al34) {
        rsi19->f0 = rsi19->f0 + 1;
    }
    goto v35;
    addr_4394_13:
    fun_2560(rax22 + reinterpret_cast<unsigned char>(r13_20) * 8);
    goto addr_434e_11;
    addr_40dc_25:
    *reinterpret_cast<void***>(0xe1d0 + rbx7 * 8) = r15_8;
    return 1;
    addr_41a0_29:
    if (reinterpret_cast<int32_t>(ebp32) < reinterpret_cast<int32_t>(0)) {
        addr_41b8_27:
        rax36 = fun_2770(r15_8, rdi, rdx12, rcx);
        rdx37 = *rax36;
        r14_38 = rax36;
    } else {
        rax39 = *reinterpret_cast<void***>(r15_8 + 24);
        rdi40 = *reinterpret_cast<void***>(r15_8 + 40);
        goto addr_3fb8_34;
    }
    do {
        *reinterpret_cast<uint32_t*>(&rsi41) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_24));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi41) + 4) = 0;
        rbp42 = r13_24;
        ++r13_24;
        if (rdx37[rsi41 * 2] & 1) 
            continue;
        if (*reinterpret_cast<signed char*>(&rsi41) != 10) 
            break;
    } while (r12_26 != r13_24);
    goto addr_4245_38;
    rax39 = *reinterpret_cast<void***>(r15_8 + 24);
    rdi40 = *reinterpret_cast<void***>(r15_8 + 40);
    do {
        r13_43 = rbp42 + 1;
        if (r12_26 == r13_43) {
            *reinterpret_cast<int32_t*>(&r9) = 1;
            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        } else {
            do {
                *reinterpret_cast<uint32_t*>(&r9_44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_43));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_44) + 4) = 0;
                if (rdx37[r9_44 * 2] & 1) 
                    goto addr_4250_43;
                if (*reinterpret_cast<signed char*>(&r9_44) == 10) 
                    goto addr_4250_43;
                ++r13_43;
            } while (r12_26 != r13_43);
            goto addr_4210_46;
        }
        addr_4216_47:
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 32)) <= reinterpret_cast<unsigned char>(rax39)) 
            goto addr_42c8_48; else 
            break;
        addr_4250_43:
        r9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_43) - reinterpret_cast<unsigned char>(rbp42));
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 32)) <= reinterpret_cast<unsigned char>(rax39)) {
            addr_42c8_48:
            rax45 = x2nrealloc(rdi40, r15_8 + 32, 16);
            r9 = r9;
            *reinterpret_cast<void***>(r15_8 + 40) = rax45;
            rdi40 = rax45;
            rax39 = *reinterpret_cast<void***>(r15_8 + 24);
            goto addr_425c_49;
        } else {
            addr_425c_49:
            rdx46 = rax39;
            ++rax39;
            rdx47 = reinterpret_cast<struct s8*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx46) << 4) + reinterpret_cast<unsigned char>(rdi40));
            rdx47->f0 = rbp42;
            rdx47->f8 = r9;
            *reinterpret_cast<void***>(r15_8 + 24) = rax39;
            if (r12_26 == r13_43) 
                goto addr_3fdb_15;
        }
        rbp42 = r13_43 + 1;
        if (r12_26 == rbp42) 
            goto addr_42a3_51;
        rdx37 = *r14_38;
        do {
            *reinterpret_cast<uint32_t*>(&rsi48) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp42));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi48) + 4) = 0;
            if (rdx37[rsi48 * 2] & 1) 
                continue;
            if (*reinterpret_cast<signed char*>(&rsi48) != 10) 
                goto addr_42b0_55;
            ++rbp42;
        } while (r12_26 != rbp42);
        goto addr_42a3_51;
        addr_4210_46:
        r9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_26) - reinterpret_cast<unsigned char>(rbp42));
        goto addr_4216_47;
        addr_42b0_55:
    } while (r12_26 != rbp42);
    goto addr_42b9_57;
    rdi49 = reinterpret_cast<struct s9*>(reinterpret_cast<unsigned char>(rdi40) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax39) << 4));
    rdi49->f0 = rbp42;
    rdi49->f8 = r9;
    *reinterpret_cast<void***>(r15_8 + 24) = rax39 + 1;
    goto addr_3fdb_15;
    addr_42a3_51:
    *reinterpret_cast<int32_t*>(&r14_25) = 0;
    *reinterpret_cast<int32_t*>(&r14_25 + 4) = 0;
    r13_24 = r12_26;
    addr_3fb8_34:
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 32)) <= reinterpret_cast<unsigned char>(rax39)) {
        rax50 = x2nrealloc(rdi40, r15_8 + 32, 16);
        *reinterpret_cast<void***>(r15_8 + 40) = rax50;
        rdi40 = rax50;
        rax39 = *reinterpret_cast<void***>(r15_8 + 24);
    }
    rdx51 = reinterpret_cast<struct s10*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax39) << 4) + reinterpret_cast<unsigned char>(rdi40));
    rdx51->f0 = r13_24;
    rdx51->f8 = r14_25;
    *reinterpret_cast<void***>(r15_8 + 24) = rax39 + 1;
    goto addr_3fdb_15;
    addr_42b9_57:
    r13_24 = r12_26;
    *reinterpret_cast<int32_t*>(&r14_25) = 0;
    *reinterpret_cast<int32_t*>(&r14_25 + 4) = 0;
    goto addr_3fb8_34;
    addr_4245_38:
    goto addr_3fdb_15;
    addr_3f1b_30:
    v52 = *reinterpret_cast<void***>(r15_8 + 40);
    v53 = reinterpret_cast<void***>(r15_8 + 32);
    while (*reinterpret_cast<uint32_t*>(&rsi54) = ebp32, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi54) + 4) = 0, r14_25 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_26) - reinterpret_cast<unsigned char>(r13_24)), rax55 = fun_2590(r13_24, rsi54, r14_25), r8 = rax55, !!rax55) {
        rdx56 = *reinterpret_cast<void***>(r15_8 + 24);
        rax57 = v52;
        r14_58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(r8) - reinterpret_cast<unsigned char>(r13_24));
        if (reinterpret_cast<unsigned char>(rdx56) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_8 + 32))) {
            rax57 = x2nrealloc(rax57, v53, 16);
            rdx56 = *reinterpret_cast<void***>(r15_8 + 24);
            ebp32 = tab;
            *reinterpret_cast<void***>(r15_8 + 40) = rax57;
            r8 = r8;
            v52 = rax57;
        }
        rax59 = reinterpret_cast<struct s11*>(reinterpret_cast<unsigned char>(rax57) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx56) << 4));
        rax59->f0 = r13_24;
        r13_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r8) + 1);
        rax59->f8 = r14_58;
        *reinterpret_cast<void***>(r15_8 + 24) = rdx56 + 1;
    }
    rax39 = *reinterpret_cast<void***>(r15_8 + 24);
    rdi40 = v52;
    goto addr_3fb8_34;
}