undefined8 get_line(byte *param_1,long *param_2,int param_3)

{
  undefined8 uVar1;
  int iVar2;
  long lVar3;
  byte **ppbVar4;
  void *pvVar5;
  undefined8 uVar6;
  long lVar7;
  ushort **ppuVar8;
  ulong uVar9;
  undefined8 uVar10;
  int *piVar11;
  ushort *puVar12;
  long lVar13;
  byte *__s;
  byte *pbVar14;
  ulong uVar15;
  byte *pbVar16;
  byte *pbVar17;
  undefined8 uVar18;
  long local_58;
  
  lVar13 = (long)(param_3 + -1);
  lVar3 = *param_2;
  if (*(long *)(prevline + lVar13 * 8) == lVar3) {
    lVar7 = *(long *)(spareline + lVar13 * 8);
    *(long *)(spareline + lVar13 * 8) = lVar3;
    *param_2 = lVar7;
    if (lVar7 == 0) goto LAB_0010415e;
LAB_00103ebb:
    *(undefined8 *)(lVar7 + 0x18) = 0;
  }
  else {
    lVar7 = lVar3;
    if (lVar3 != 0) goto LAB_00103ebb;
LAB_0010415e:
    lVar7 = xcalloc(1,0x30);
    *param_2 = lVar7;
  }
  lVar3 = readlinebuffer_delim(lVar7,param_1,(int)eolchar);
  if (lVar3 == 0) {
    if ((*param_1 & 0x20) == 0) {
      free(*(void **)(lVar7 + 0x28));
      *(undefined8 *)(lVar7 + 0x28) = 0;
      free(*(void **)(lVar7 + 0x10));
      *(undefined8 *)(lVar7 + 0x10) = 0;
      return 0;
    }
    uVar10 = dcgettext(0,"read error",5);
    piVar11 = __errno_location();
                    /* WARNING: Subroutine does not return */
    error(1,*piVar11,uVar10);
  }
  __s = *(byte **)(lVar7 + 0x10);
  *(long *)(line_no + lVar13 * 8) = *(long *)(line_no + lVar13 * 8) + 1;
  pbVar17 = (byte *)(*(long *)(lVar7 + 8) + -1);
  pbVar16 = __s + (long)pbVar17;
  if (__s == pbVar16) goto LAB_00103fdb;
  if (tab < 0) {
    ppuVar8 = __ctype_b_loc();
    puVar12 = *ppuVar8;
    do {
      pbVar17 = __s + 1;
      if (((*(byte *)(puVar12 + *__s) & 1) == 0) && (*__s != 10)) {
        local_58 = *(long *)(lVar7 + 0x28);
        uVar15 = *(ulong *)(lVar7 + 0x18);
        goto LAB_001041e8;
      }
      __s = pbVar17;
    } while (pbVar16 != pbVar17);
    goto LAB_00103fdb;
  }
  if (tab == 10) {
    uVar9 = *(ulong *)(lVar7 + 0x18);
    local_58 = *(long *)(lVar7 + 0x28);
    pbVar16 = __s;
  }
  else {
    local_58 = *(long *)(lVar7 + 0x28);
    iVar2 = tab;
    while( true ) {
      pbVar17 = pbVar16 + -(long)__s;
      pvVar5 = memchr(__s,iVar2,(size_t)pbVar17);
      if (pvVar5 == (void *)0x0) break;
      uVar15 = *(ulong *)(lVar7 + 0x18);
      pbVar17 = (byte *)((long)pvVar5 - (long)__s);
      if (*(ulong *)(lVar7 + 0x20) <= uVar15) {
        local_58 = x2nrealloc(local_58,lVar7 + 0x20,0x10);
        iVar2 = tab;
        uVar15 = *(ulong *)(lVar7 + 0x18);
        *(long *)(lVar7 + 0x28) = local_58;
      }
      ppbVar4 = (byte **)(local_58 + uVar15 * 0x10);
      *ppbVar4 = __s;
      __s = (byte *)((long)pvVar5 + 1);
      ppbVar4[1] = pbVar17;
      *(ulong *)(lVar7 + 0x18) = uVar15 + 1;
    }
    uVar9 = *(ulong *)(lVar7 + 0x18);
    pbVar16 = __s;
  }
  goto LAB_00103fb8;
LAB_001041e8:
  do {
    pbVar17 = __s + 1;
    if (pbVar16 == pbVar17) {
      pbVar14 = (byte *)0x1;
    }
    else {
      do {
        if (((*(byte *)(puVar12 + *pbVar17) & 1) != 0) || (*pbVar17 == 10)) {
          pbVar14 = pbVar17 + -(long)__s;
          if (uVar15 <= *(ulong *)(lVar7 + 0x20) && *(ulong *)(lVar7 + 0x20) != uVar15)
          goto LAB_0010425c;
          goto LAB_001042c8;
        }
        pbVar17 = pbVar17 + 1;
      } while (pbVar16 != pbVar17);
      pbVar14 = pbVar16 + -(long)__s;
    }
    if (uVar15 <= *(ulong *)(lVar7 + 0x20) && *(ulong *)(lVar7 + 0x20) != uVar15) {
      ppbVar4 = (byte **)(local_58 + uVar15 * 0x10);
      *ppbVar4 = __s;
      ppbVar4[1] = pbVar14;
      *(ulong *)(lVar7 + 0x18) = uVar15 + 1;
      goto LAB_00103fdb;
    }
LAB_001042c8:
    local_58 = x2nrealloc(local_58,lVar7 + 0x20,0x10);
    *(long *)(lVar7 + 0x28) = local_58;
    uVar15 = *(ulong *)(lVar7 + 0x18);
LAB_0010425c:
    uVar9 = uVar15 + 1;
    ppbVar4 = (byte **)(uVar15 * 0x10 + local_58);
    *ppbVar4 = __s;
    ppbVar4[1] = pbVar14;
    *(ulong *)(lVar7 + 0x18) = uVar9;
    if (pbVar16 == pbVar17) goto LAB_00103fdb;
    __s = pbVar17 + 1;
    if (pbVar16 == __s) {
LAB_001042a3:
      pbVar17 = (byte *)0x0;
      goto LAB_00103fb8;
    }
    puVar12 = *ppuVar8;
    while (((*(byte *)(puVar12 + *__s) & 1) != 0 || (*__s == 10))) {
      __s = __s + 1;
      if (pbVar16 == __s) goto LAB_001042a3;
    }
    uVar15 = uVar9;
  } while (pbVar16 != __s);
  pbVar17 = (byte *)0x0;
LAB_00103fb8:
  if (*(ulong *)(lVar7 + 0x20) < uVar9 || *(ulong *)(lVar7 + 0x20) == uVar9) {
    local_58 = x2nrealloc(local_58,lVar7 + 0x20,0x10);
    *(long *)(lVar7 + 0x28) = local_58;
    uVar9 = *(ulong *)(lVar7 + 0x18);
  }
  ppbVar4 = (byte **)(uVar9 * 0x10 + local_58);
  *ppbVar4 = pbVar16;
  ppbVar4[1] = pbVar17;
  *(ulong *)(lVar7 + 0x18) = uVar9 + 1;
LAB_00103fdb:
  if ((((*(long *)(prevline + lVar13 * 8) != 0) && (check_input_order != 2)) &&
      ((check_input_order == 1 || (seen_unpairable != '\0')))) &&
     (*(char *)((long)&issued_disorder_warning + lVar13) == '\0')) {
    uVar10 = join_field_1;
    if (param_3 != 1) {
      uVar10 = join_field_2;
    }
    iVar2 = keycmp(*(long *)(prevline + lVar13 * 8),lVar7,uVar10,uVar10);
    if (0 < iVar2) {
      uVar9 = *(ulong *)(lVar7 + 8);
      lVar3 = *(long *)(lVar7 + 0x10);
      uVar15 = 0;
      if (uVar9 != 0) {
        uVar15 = uVar9 - 1;
        if (0x7fffffff < uVar15) {
          uVar15 = 0x7fffffff;
        }
        if ((*(char *)(lVar3 + -1 + uVar9) != '\n') && (uVar15 = 0x7fffffff, uVar9 < 0x80000000)) {
          uVar15 = uVar9;
        }
      }
      uVar10 = *(undefined8 *)(line_no + lVar13 * 8);
      uVar1 = *(undefined8 *)(g_names + lVar13 * 8);
      uVar18 = 0x1040ab;
      uVar6 = dcgettext(0,"%s:%lu: is not sorted: %.*s",5);
                    /* WARNING: Subroutine does not return */
      error(check_input_order == 1,0,uVar6,uVar1,uVar10,uVar15 & 0xffffffff,lVar3,uVar18);
    }
  }
  *(long *)(prevline + lVar13 * 8) = lVar7;
  return 1;
}