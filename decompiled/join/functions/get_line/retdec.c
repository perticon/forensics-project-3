bool get_line(struct _IO_FILE * fp, int32_t ** linep, int32_t which) {
    int64_t v1 = (int64_t)linep;
    int64_t v2 = which - 1; // 0x3e9a
    int64_t v3 = 8 * v2; // 0x3ea8
    int64_t * v4 = (int64_t *)(v3 + (int64_t)&prevline); // 0x3ea8
    int64_t * v5; // 0x3e80
    int64_t v6; // 0x3e80
    if (*v4 == v1) {
        int64_t * v7 = (int64_t *)(v3 + (int64_t)&spareline); // 0x4147
        int64_t v8 = *v7; // 0x4147
        *v7 = v1;
        *(int64_t *)linep = v8;
        v6 = v8;
        v5 = (int64_t *)linep;
        if (v8 != 0) {
            goto lab_0x3ebb;
        } else {
            goto lab_0x415e;
        }
    } else {
        // 0x3eb2
        v6 = v1;
        v5 = NULL;
        if (linep == NULL) {
            goto lab_0x415e;
        } else {
            goto lab_0x3ebb;
        }
    }
  lab_0x4216:;
    // 0x4216
    int64_t v9; // 0x3e80
    int64_t v10 = v9;
    int64_t v11 = v10; // 0x421a
    int64_t v12; // 0x3e80
    int64_t v13 = v12; // 0x421a
    uint64_t v14; // 0x3e80
    int64_t * v15; // 0x3e80
    if (*v15 > v14) {
        // break -> 0x4220
        goto lab_0x4220;
    }
    goto lab_0x42c8;
  lab_0x42c8:;
    int64_t v16 = x2nrealloc(); // 0x42d5
    int64_t * v17; // 0x41e4
    *v17 = v16;
    int64_t v18 = v16; // 0x42e9
    int64_t * v19; // 0x41e0
    int64_t v20 = *v19; // 0x42e9
    int64_t v21 = v11; // 0x42e9
    int64_t v22 = v13; // 0x42e9
    goto lab_0x425c;
  lab_0x425c:;
    int64_t v23 = v18;
    int64_t v24 = v20 + 1; // 0x425f
    int64_t v25 = 16 * v20 + v23; // 0x4267
    int64_t v26; // 0x3e80
    *(int64_t *)v25 = v26;
    *(int64_t *)(v25 + 8) = v21;
    *v19 = v24;
    int64_t v27; // 0x3ef6
    if (v27 == v22) {
        goto lab_0x3fdb_2;
    }
    int64_t v28 = v22 + 1; // 0x427e
    int64_t v29 = v23; // 0x4285
    int64_t v30 = v24; // 0x4285
    int64_t v31 = v27; // 0x4285
    int64_t v32 = 0; // 0x4285
    if (v27 == v28) {
        goto lab_0x3fb8_3;
    }
    // 0x4287
    int64_t * v33; // 0x41bd
    int64_t v34 = *v33; // 0x4287
    int64_t v35 = v28;
    unsigned char v36 = *(char *)v35; // 0x428a
    while (v36 != 10 != (*(char *)(2 * (int64_t)v36 + v34) % 2 == 0)) {
        int64_t v37 = v35 + 1; // 0x429a
        v29 = v23;
        v30 = v24;
        v31 = v27;
        v32 = 0;
        if (v27 == v37) {
            goto lab_0x3fb8_3;
        }
        v35 = v37;
        v36 = *(char *)v35;
    }
    // 0x42b0
    v29 = v23;
    v30 = v24;
    v31 = v27;
    v32 = 0;
    int64_t v38 = v23; // 0x42b3
    int64_t v39 = v24; // 0x42b3
    int64_t v40 = v34; // 0x42b3
    int64_t v41 = v35; // 0x42b3
    if (v27 == v35) {
        goto lab_0x3fb8_3;
    }
    goto lab_0x41e8;
  lab_0x3ebb:
    // 0x3ebb
    *(int64_t *)(v6 + 24) = 0;
    int64_t v50 = v6; // 0x3ebb
    goto lab_0x3ec3;
  lab_0x415e:;
    int64_t v84 = xcalloc(); // 0x4168
    *v5 = v84;
    v50 = v84;
    goto lab_0x3ec3;
  lab_0x3ec3:;
    int32_t * v51 = (int32_t *)v50; // 0x3ed0
    if (readlinebuffer_delim(v51, fp, eolchar) == NULL) {
        if ((v50 & 32) != 0) {
            // 0x42fb
            function_24b0();
            function_2420();
            return function_26c0() % 2 != 0;
        }
        // 0x410b
        function_2400();
        *(int64_t *)(v50 + 40) = 0;
        function_2400();
        *(int64_t *)(v50 + 16) = 0;
        return false;
    }
    int64_t v52 = *(int64_t *)(v50 + 16); // 0x3ee5
    int64_t * v53 = (int64_t *)(v3 + (int64_t)&line_no); // 0x3ee9
    *v53 = *v53 + 1;
    int64_t v54 = *(int64_t *)(v50 + 8); // 0x3eee
    if (v54 != 1) {
        int64_t v55 = v54 - 1; // 0x3ef2
        v27 = v55 + v52;
        if (tab < 0) {
            // 0x41b8
            v33 = (int64_t *)function_2770();
            int64_t v56 = *v33; // 0x41bd
            int64_t v57 = v52;
            unsigned char v58 = *(char *)v57; // 0x41c8
            while (v58 == 10 | *(char *)(2 * (int64_t)v58 + v56) % 2 != 0) {
                int64_t v59 = v57 + 1; // 0x41d0
                if (v27 == v59) {
                    goto lab_0x3fdb_2;
                }
                v57 = v59;
                v58 = *(char *)v57;
            }
            // 0x41e0
            v19 = (int64_t *)(v50 + 24);
            v17 = (int64_t *)(v50 + 40);
            v15 = (int64_t *)(v50 + 32);
            v38 = *v17;
            v39 = *v19;
            v40 = v56;
            v41 = v57;
            int64_t v43; // 0x3e80
            while (true) {
              lab_0x41e8:
                // 0x41e8
                v26 = v41;
                int64_t v42 = v40;
                v14 = v39;
                v43 = v38;
                int64_t v44 = v26 + 1; // 0x41e8
                int64_t v45 = v44; // 0x41ef
                v12 = v27;
                v9 = 1;
                if (v27 == v44) {
                    goto lab_0x4216;
                } else {
                    int64_t v46 = v45;
                    unsigned char v47 = *(char *)v46; // 0x41f5
                    while (!((v47 == 10 | *(char *)(2 * (int64_t)v47 + v42) % 2 != 0))) {
                        int64_t v48 = v46 + 1; // 0x4207
                        v45 = v48;
                        if (v27 == v48) {
                            // 0x4210
                            v12 = v48;
                            v9 = v27 - v26;
                            goto lab_0x4216;
                        }
                        v46 = v45;
                        v47 = *(char *)v46;
                    }
                    int64_t v49 = v46 - v26; // 0x4253
                    v18 = v43;
                    v20 = v14;
                    v21 = v49;
                    v22 = v46;
                    v11 = v49;
                    v13 = v46;
                    if (*v15 > v14) {
                        goto lab_0x425c;
                    } else {
                        goto lab_0x42c8;
                    }
                }
            }
          lab_0x4220:;
            int64_t v60 = 16 * v14 + v43; // 0x422b
            *(int64_t *)v60 = v26;
            *(int64_t *)(v60 + 8) = v10;
            *v19 = v14 + 1;
        } else {
            if (tab == 10) {
                // 0x41a4
                v29 = *(int64_t *)(v50 + 40);
                v30 = *(int64_t *)(v50 + 24);
                v31 = v52;
                v32 = v55;
            } else {
                int64_t * v61 = (int64_t *)(v50 + 40); // 0x3f1b
                int64_t v62 = *v61; // 0x3f1b
                int64_t v63 = function_2590(); // 0x3f5b
                int64_t * v64 = (int64_t *)(v50 + 24);
                int64_t v65 = v62; // 0x3f66
                int64_t v66 = v52; // 0x3f66
                if (v63 != 0) {
                    uint64_t v67 = *v64; // 0x3f6b
                    int64_t v68 = v62; // 0x3f7a
                    int64_t v69 = v67; // 0x3f7a
                    if (v67 >= *(int64_t *)(v50 + 32)) {
                        // 0x3f7c
                        v68 = x2nrealloc();
                        v69 = *v64;
                        *v61 = v68;
                    }
                    int64_t v70 = v68;
                    int64_t v71 = 16 * v69 + v70; // 0x3f3b
                    *(int64_t *)v71 = v52;
                    int64_t v72 = v63 + 1; // 0x3f41
                    *(int64_t *)(v71 + 8) = v63 - v52;
                    *v64 = v69 + 1;
                    int64_t v73 = function_2590(); // 0x3f5b
                    v65 = v70;
                    v66 = v72;
                    while (v73 != 0) {
                        int64_t v74 = v72;
                        v67 = *v64;
                        v68 = v70;
                        v69 = v67;
                        if (v67 >= *(int64_t *)(v50 + 32)) {
                            // 0x3f7c
                            v68 = x2nrealloc();
                            v69 = *v64;
                            *v61 = v68;
                        }
                        // 0x3f30
                        v70 = v68;
                        v71 = 16 * v69 + v70;
                        *(int64_t *)v71 = v74;
                        v72 = v73 + 1;
                        *(int64_t *)(v71 + 8) = v73 - v74;
                        *v64 = v69 + 1;
                        v73 = function_2590();
                        v65 = v70;
                        v66 = v72;
                    }
                }
                // 0x3fb0
                v29 = v65;
                v30 = *v64;
                v31 = v66;
                v32 = v27 - v66;
            }
          lab_0x3fb8_3:;
            // 0x3fb8
            int64_t * v75; // 0x3e80
            int64_t v76; // 0x3e80
            int64_t v77; // 0x3e80
            if (*(int64_t *)(v50 + 32) > v30) {
                // 0x3fb8
                v75 = (int64_t *)(v50 + 24);
                v76 = v29;
                v77 = v30;
            } else {
                int64_t v78 = x2nrealloc(); // 0x4189
                *(int64_t *)(v50 + 40) = v78;
                int64_t * v79 = (int64_t *)(v50 + 24);
                v75 = v79;
                v76 = v78;
                v77 = *v79;
            }
            int64_t v80 = 16 * v77 + v76; // 0x3fcd
            *(int64_t *)v80 = v31;
            *(int64_t *)(v80 + 8) = v32;
            *v75 = v77 + 1;
        }
    }
  lab_0x3fdb_2:;
    int64_t v81 = *v4; // 0x3fe2
    if (v81 == 0 || check_input_order == 2 || check_input_order != 1 == *(char *)&seen_unpairable == 0) {
        // 0x40dc
        *v4 = v50;
        return true;
    }
    char * v82 = (char *)(v2 + (int64_t)&issued_disorder_warning); // 0x4017
    if (*v82 != 0) {
        // 0x40dc
        *v4 = v50;
        return true;
    }
    int64_t v83 = which != 1 ? join_field_2 : join_field_1; // 0x4031
    if (keycmp((int32_t *)v81, v51, v83, v83) >= 1) {
        // 0x4049
        function_24b0();
        function_26c0();
        *v82 = 1;
    }
    // 0x40dc
    *v4 = v50;
    return true;
}