void set_join_field(long *param_1,long param_2)

{
  long lVar1;
  undefined8 uVar2;
  
  lVar1 = *param_1;
  if ((lVar1 != -1) && (lVar1 != param_2)) {
    uVar2 = dcgettext(0,"incompatible join fields %lu, %lu",5);
                    /* WARNING: Subroutine does not return */
    error(1,0,uVar2,lVar1 + 1,param_2 + 1);
  }
  *param_1 = param_2;
  return;
}