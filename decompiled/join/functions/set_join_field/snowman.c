void set_join_field(void*** rdi, void** rsi, ...) {
    void** rbp3;
    void** rax4;
    void** rcx5;
    void** rax6;
    int1_t below_or_equal7;
    int64_t v8;
    struct s3* tmp64_9;
    void** rdi10;
    int64_t v11;
    void** tmp64_12;
    void** rsi13;
    int64_t v14;
    int1_t zf15;
    int1_t zf16;
    void** rdx17;
    uint32_t eax18;
    int64_t v19;

    rbp3 = *rdi;
    if (rbp3 == 0xffffffffffffffff || rbp3 == rsi) {
        *rdi = rsi;
        return;
    }
    rax4 = fun_24b0();
    rcx5 = rbp3 + 1;
    fun_26c0();
    rax6 = *reinterpret_cast<void***>(&g18);
    below_or_equal7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<int64_t>(&g18) + 1)) <= reinterpret_cast<unsigned char>(rax4);
    if (!below_or_equal7) 
        goto addr_37f1_5;
    if (reinterpret_cast<unsigned char>(rcx5) < reinterpret_cast<unsigned char>(rax6)) {
        goto addr_38ac_8;
    }
    addr_3867_10:
    goto v8;
    addr_37f1_5:
    tmp64_9 = reinterpret_cast<struct s3*>((reinterpret_cast<unsigned char>(rax4) << 4) + reinterpret_cast<uint64_t>(*reinterpret_cast<void****>(reinterpret_cast<int64_t>(&g28) + 1)));
    rdi10 = tmp64_9->f0;
    if (reinterpret_cast<unsigned char>(rcx5) >= reinterpret_cast<unsigned char>(rax6)) {
        goto v11;
    }
    tmp64_12 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx5) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    rsi13 = *reinterpret_cast<void***>(tmp64_12);
    if (!tmp64_9->f8) {
        addr_38ac_8:
        goto v14;
    } else {
        if (!*reinterpret_cast<void***>(tmp64_12 + 8)) {
            goto addr_3867_10;
        } else {
            zf15 = ignore_case == 0;
            if (zf15) {
                zf16 = hard_LC_COLLATE == 0;
                if (zf16) {
                    rdx17 = *reinterpret_cast<void***>(tmp64_12 + 8);
                    if (reinterpret_cast<unsigned char>(tmp64_9->f8) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(tmp64_12 + 8))) {
                        rdx17 = tmp64_9->f8;
                    }
                    eax18 = fun_25a0(rdi10, rsi13, rdx17, tmp64_12, rdi10, rsi13, rdx17, tmp64_12);
                }
            } else {
                if (reinterpret_cast<unsigned char>(tmp64_9->f8) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(tmp64_12 + 8))) {
                }
                eax18 = memcasecmp(rdi10);
            }
            if (!eax18) {
                goto v19;
            }
        }
    }
}