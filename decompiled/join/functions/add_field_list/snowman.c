void add_field_list(void** rdi, void** rsi) {
    void** rbp3;
    void** r13_4;
    void** rdx5;
    struct s0* rcx6;
    int64_t r8_7;
    struct s1* rax8;
    int64_t rax9;
    void** rbp10;
    void** r12d11;
    int32_t edx12;
    uint32_t eax13;
    void** rax14;
    void** rax15;
    int1_t below_or_equal16;
    struct s2* tmp64_17;
    void** rdi18;
    int64_t v19;

    rbp3 = rdi;
    while (1) {
        *reinterpret_cast<int32_t*>(&r13_4) = 0;
        *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
        rax8 = fun_2650(rbp3, ", \t", rdx5, rcx6, r8_7);
        if (rax8) {
            rax8->f0 = 0;
            r13_4 = reinterpret_cast<void**>(&rax8->f1);
        }
        *reinterpret_cast<uint32_t*>(&rax9) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp3));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rax9) == 48) {
            if (*reinterpret_cast<void***>(rbp3 + 1)) 
                break;
            *reinterpret_cast<int32_t*>(&rbp10) = 0;
            *reinterpret_cast<int32_t*>(&rbp10 + 4) = 0;
            r12d11 = reinterpret_cast<void**>(0);
        } else {
            edx12 = static_cast<int32_t>(rax9 - 49);
            if (*reinterpret_cast<unsigned char*>(&edx12) > 1) 
                goto addr_3a3b_8;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp3 + 1) == 46)) 
                goto addr_3a9f_10;
            eax13 = *reinterpret_cast<uint32_t*>(&rax9) - 48;
            r12d11 = reinterpret_cast<void**>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&eax13)));
            rax14 = string_to_join_field(rbp3 + 2, ", \t");
            rbp10 = rax14;
        }
        rax15 = xmalloc(24, ", \t");
        rdx5 = outlist_end;
        *reinterpret_cast<void***>(rax15) = r12d11;
        *reinterpret_cast<void***>(rax15 + 8) = rbp10;
        *reinterpret_cast<void***>(rax15 + 16) = reinterpret_cast<void**>(0);
        outlist_end = rax15;
        *reinterpret_cast<void***>(rdx5 + 16) = rax15;
        if (!r13_4) 
            goto addr_3a30_13;
        rbp3 = r13_4;
    }
    addr_3a6d_15:
    quote(rbp3, rbp3);
    fun_24b0();
    fun_26c0();
    addr_3a9f_10:
    quote(rbp3, rbp3);
    fun_24b0();
    fun_26c0();
    below_or_equal16 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g18)) <= reinterpret_cast<unsigned char>(1);
    if (below_or_equal16 || (tmp64_17 = reinterpret_cast<struct s2*>(*reinterpret_cast<void***>(&g28) + 16), tmp64_17->f8 == 0)) {
        rdi18 = empty_filler;
        if (!rdi18) {
            goto v19;
        }
    }
    addr_3a30_13:
    return;
    addr_3a3b_8:
    quote(rbp3, rbp3);
    fun_24b0();
    fun_26c0();
    goto addr_3a6d_15;
}