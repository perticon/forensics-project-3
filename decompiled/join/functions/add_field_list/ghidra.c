void add_field_list(char *param_1)

{
  int **ppiVar1;
  char cVar2;
  int *piVar3;
  char *pcVar4;
  undefined8 uVar5;
  undefined8 uVar6;
  int iVar7;
  char *pcVar8;
  
  do {
    pcVar8 = (char *)0x0;
    pcVar4 = strpbrk(param_1,", \t");
    if (pcVar4 != (char *)0x0) {
      *pcVar4 = '\0';
      pcVar8 = pcVar4 + 1;
    }
    cVar2 = *param_1;
    if (cVar2 == '0') {
      if (param_1[1] != '\0') {
        uVar5 = quote(param_1);
        uVar6 = dcgettext(0,"invalid field specifier: %s",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar6,uVar5);
      }
      uVar5 = 0;
      iVar7 = 0;
    }
    else {
      if (1 < (byte)(cVar2 - 0x31U)) {
        uVar5 = quote(param_1);
        uVar6 = dcgettext(0,"invalid file number in field spec: %s",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar6,uVar5);
      }
      if (param_1[1] != '.') {
        uVar5 = quote(param_1);
        uVar6 = dcgettext(0,"invalid field specifier: %s",5);
                    /* WARNING: Subroutine does not return */
        error(1,0,uVar6,uVar5);
      }
      iVar7 = (int)(char)(cVar2 + -0x30);
      uVar5 = string_to_join_field(param_1 + 2);
    }
    piVar3 = (int *)xmalloc(0x18);
    *piVar3 = iVar7;
    *(undefined8 *)(piVar3 + 2) = uVar5;
    *(undefined8 *)(piVar3 + 4) = 0;
    ppiVar1 = (int **)(outlist_end + 0x10);
    outlist_end = (undefined *)piVar3;
    *ppiVar1 = piVar3;
    param_1 = pcVar8;
    if (pcVar8 == (char *)0x0) {
      return;
    }
  } while( true );
}