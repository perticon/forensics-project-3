void free_spareline(void)

{
  void *pvVar1;
  
  pvVar1 = spareline._0_8_;
  if (spareline._0_8_ != (void *)0x0) {
    free(*(void **)((long)spareline._0_8_ + 0x28));
    free(*(void **)((long)pvVar1 + 0x10));
    free(pvVar1);
  }
  pvVar1 = spareline._8_8_;
  if (spareline._8_8_ != (void *)0x0) {
    free(*(void **)((long)spareline._8_8_ + 0x28));
    free(*(void **)((long)pvVar1 + 0x10));
    free(pvVar1);
    return;
  }
  return;
}