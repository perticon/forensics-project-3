free_spareline (void)
{
  for (size_t i = 0; i < ARRAY_CARDINALITY (spareline); i++)
    {
      if (spareline[i])
        {
          freeline (spareline[i]);
          free (spareline[i]);
        }
    }
}