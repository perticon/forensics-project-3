advance_seq (FILE *fp, struct seq *seq, bool first, int whichfile)
{
  if (first)
    seq->count = 0;

  return getseq (fp, seq, whichfile);
}