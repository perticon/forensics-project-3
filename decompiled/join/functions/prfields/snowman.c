void prfields(void** rdi, void** rsi, void** rdx) {
    void** r13_4;
    void** rbp5;
    int1_t zf6;
    void** v7;
    uint32_t r15d8;
    void** r14_9;
    uint32_t r12d10;
    void** rbx11;
    struct s5* rdi12;
    signed char* rax13;
    void** rdi14;
    void** rbx15;
    struct s5* rdi16;
    signed char* rax17;
    void** rdi18;

    r13_4 = rdx;
    rbp5 = rdi;
    zf6 = autoformat == 0;
    v7 = rsi;
    if (zf6) {
        r13_4 = *reinterpret_cast<void***>(rdi + 24);
    }
    r15d8 = tab;
    if (reinterpret_cast<int32_t>(r15d8) < reinterpret_cast<int32_t>(0)) {
        r15d8 = 32;
    }
    r14_9 = v7;
    r12d10 = r15d8;
    if (reinterpret_cast<unsigned char>(r13_4) <= reinterpret_cast<unsigned char>(v7)) {
        r14_9 = r13_4;
    }
    *reinterpret_cast<int32_t*>(&rbx11) = 0;
    *reinterpret_cast<int32_t*>(&rbx11 + 4) = 0;
    if (r14_9) {
        do {
            rdi12 = stdout;
            rax13 = rdi12->f28;
            if (reinterpret_cast<uint64_t>(rax13) >= reinterpret_cast<uint64_t>(rdi12->f30)) {
                fun_2520();
            } else {
                rdi12->f28 = rax13 + 1;
                *rax13 = *reinterpret_cast<signed char*>(&r12d10);
            }
            rdi14 = rbx11;
            ++rbx11;
            prfield(rdi14, rbp5);
        } while (rbx11 != r14_9);
    }
    rbx15 = v7 + 1;
    if (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(rbx15)) {
        do {
            rdi16 = stdout;
            rax17 = rdi16->f28;
            if (reinterpret_cast<uint64_t>(rax17) >= reinterpret_cast<uint64_t>(rdi16->f30)) {
                fun_2520();
            } else {
                rdi16->f28 = rax17 + 1;
                *rax17 = *reinterpret_cast<signed char*>(&r12d10);
            }
            rdi18 = rbx15;
            ++rbx15;
            prfield(rdi18, rbp5);
        } while (reinterpret_cast<unsigned char>(r13_4) > reinterpret_cast<unsigned char>(rbx15));
    }
    return;
}