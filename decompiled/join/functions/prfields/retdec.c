void prfields(int32_t * line, uint64_t join_field, int64_t autocount) {
    int64_t v1 = (int64_t)line;
    int64_t v2 = autocount; // 0x3b60
    if (*(char *)&autoformat == 0) {
        // 0x3b62
        v2 = *(int64_t *)(v1 + 24);
    }
    uint64_t v3 = v2;
    int32_t v4 = tab; // 0x3b66
    int32_t v5 = v4 < 0 ? 32 : v4; // 0x3b75
    int64_t v6 = v3 > join_field ? join_field : v3; // 0x3b8b
    if (v6 != 0) {
        int64_t v7 = 0;
        int64_t v8 = (int64_t)g30; // 0x3ba0
        int64_t * v9 = (int64_t *)(v8 + 40); // 0x3ba7
        uint64_t v10 = *v9; // 0x3ba7
        if (v10 >= *(int64_t *)(v8 + 48)) {
            // 0x3c30
            function_2520();
        } else {
            // 0x3bb1
            *v9 = v10 + 1;
            *(char *)v10 = (char)v5;
        }
        int64_t v11 = v7 + 1; // 0x3bc2
        prfield(v7, v1);
        while (v11 != v6) {
            // 0x3ba0
            v7 = v11;
            v8 = (int64_t)g30;
            v9 = (int64_t *)(v8 + 40);
            v10 = *v9;
            if (v10 >= *(int64_t *)(v8 + 48)) {
                // 0x3c30
                function_2520();
            } else {
                // 0x3bb1
                *v9 = v10 + 1;
                *(char *)v10 = (char)v5;
            }
            // 0x3bbc
            v11 = v7 + 1;
            prfield(v7, v1);
        }
    }
    int64_t v12 = join_field + 1; // 0x3bdd
    if (v3 <= v12) {
        // 0x3c20
        return;
    }
    int64_t v13 = (int64_t)g30; // 0x3bf0
    int64_t * v14 = (int64_t *)(v13 + 40); // 0x3bf7
    uint64_t v15 = *v14; // 0x3bf7
    if (v15 >= *(int64_t *)(v13 + 48)) {
        // 0x3c40
        function_2520();
    } else {
        // 0x3c01
        *v14 = v15 + 1;
        *(char *)v15 = (char)v5;
    }
    int64_t v16 = v12 + 1; // 0x3c12
    prfield(v12, v1);
    while (v16 != v3) {
        int64_t v17 = v16;
        v13 = (int64_t)g30;
        v14 = (int64_t *)(v13 + 40);
        v15 = *v14;
        if (v15 >= *(int64_t *)(v13 + 48)) {
            // 0x3c40
            function_2520();
        } else {
            // 0x3c01
            *v14 = v15 + 1;
            *(char *)v15 = (char)v5;
        }
        // 0x3c0c
        v16 = v17 + 1;
        prfield(v17, v1);
    }
}