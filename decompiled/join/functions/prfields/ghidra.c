void prfields(long param_1,ulong param_2,ulong param_3)

{
  char *pcVar1;
  int iVar2;
  ulong uVar3;
  ulong uVar4;
  ulong uVar5;
  char cVar6;
  
  if (autoformat == '\0') {
    param_3 = *(ulong *)(param_1 + 0x18);
  }
  iVar2 = tab;
  if (tab < 0) {
    iVar2 = 0x20;
  }
  cVar6 = (char)iVar2;
  uVar5 = param_2;
  if (param_3 <= param_2) {
    uVar5 = param_3;
  }
  uVar3 = 0;
  if (uVar5 != 0) {
    do {
      pcVar1 = stdout->_IO_write_ptr;
      if (pcVar1 < stdout->_IO_write_end) {
        stdout->_IO_write_ptr = pcVar1 + 1;
        *pcVar1 = cVar6;
      }
      else {
        __overflow(stdout,(int)cVar6 & 0xff);
      }
      uVar4 = uVar3 + 1;
      prfield(uVar3);
      uVar3 = uVar4;
    } while (uVar4 != uVar5);
  }
  for (param_2 = param_2 + 1; param_2 < param_3; param_2 = param_2 + 1) {
    pcVar1 = stdout->_IO_write_ptr;
    if (pcVar1 < stdout->_IO_write_end) {
      stdout->_IO_write_ptr = pcVar1 + 1;
      *pcVar1 = cVar6;
    }
    else {
      __overflow(stdout,(int)cVar6 & 0xff);
    }
    prfield(param_2);
  }
  return;
}