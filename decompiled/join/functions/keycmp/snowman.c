uint32_t keycmp(void** rdi, void** rsi, void** rdx, void** rcx, struct s0* r8, void** r9) {
    void** rax7;
    void** r12_8;
    uint32_t eax9;
    struct s6* rdx10;
    void** rdi11;
    void** rbp12;
    uint32_t eax13;
    void** rcx14;
    void** rsi15;
    int1_t zf16;
    int1_t zf17;
    void** rdx18;
    uint32_t eax19;

    rax7 = *reinterpret_cast<void***>(rsi + 24);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 24)) <= reinterpret_cast<unsigned char>(rdx)) {
        if (reinterpret_cast<unsigned char>(rcx) < reinterpret_cast<unsigned char>(rax7)) {
            r12_8 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 40)) + 8);
        } else {
            eax9 = 0;
            goto addr_3867_5;
        }
    } else {
        rdx10 = reinterpret_cast<struct s6*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)));
        rdi11 = rdx10->f0;
        rbp12 = rdx10->f8;
        if (reinterpret_cast<unsigned char>(rcx) >= reinterpret_cast<unsigned char>(rax7)) {
            eax13 = 0;
            *reinterpret_cast<unsigned char*>(&eax13) = reinterpret_cast<uint1_t>(!!rbp12);
            return eax13;
        }
        rcx14 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rcx) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 40)));
        rsi15 = *reinterpret_cast<void***>(rcx14);
        r12_8 = *reinterpret_cast<void***>(rcx14 + 8);
        if (rbp12) 
            goto addr_3821_9;
    }
    return *reinterpret_cast<uint32_t*>(&rax7) - (*reinterpret_cast<uint32_t*>(&rax7) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax7) < *reinterpret_cast<uint32_t*>(&rax7) + reinterpret_cast<uint1_t>(!!r12_8)));
    addr_3867_5:
    return eax9;
    addr_3821_9:
    if (!r12_8) {
        eax9 = 1;
        goto addr_3867_5;
    } else {
        zf16 = ignore_case == 0;
        if (zf16) {
            zf17 = hard_LC_COLLATE == 0;
            if (zf17) {
                rdx18 = r12_8;
                if (reinterpret_cast<unsigned char>(rbp12) <= reinterpret_cast<unsigned char>(r12_8)) {
                    rdx18 = rbp12;
                }
                eax9 = fun_25a0(rdi11, rsi15, rdx18, rcx14);
            }
        } else {
            if (reinterpret_cast<unsigned char>(rbp12) <= reinterpret_cast<unsigned char>(r12_8)) {
            }
            eax9 = memcasecmp(rdi11);
        }
        if (!eax9) {
            eax19 = 0;
            *reinterpret_cast<unsigned char*>(&eax19) = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp12) > reinterpret_cast<unsigned char>(r12_8));
            return eax19 - reinterpret_cast<uint1_t>(eax19 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rbp12) < reinterpret_cast<unsigned char>(r12_8))));
        }
    }
}