int32_t keycmp(int32_t * line1, int32_t * line2, uint64_t jf_1, uint64_t jf_2) {
    int64_t v1 = (int64_t)line2;
    int64_t v2 = (int64_t)line1;
    uint64_t v3 = *(int64_t *)(v1 + 24); // 0x37e7
    if (*(int64_t *)(v2 + 24) <= jf_1) {
        // 0x3860
        if (v3 <= jf_2) {
            // 0x3867
            return 0;
        }
        // 0x38ac
        return *(int64_t *)(*(int64_t *)(v1 + 40) + (16 * jf_2 | 8)) != 0;
    }
    int64_t v4 = *(int64_t *)(v2 + 40) + 16 * jf_1; // 0x37f5
    uint64_t v5 = *(int64_t *)(v4 + 8); // 0x37fc
    if (v3 <= jf_2) {
        // 0x3890
        return v5 != 0;
    }
    int64_t v6 = *(int64_t *)v4; // 0x37f9
    int64_t v7 = *(int64_t *)(v1 + 40) + 16 * jf_2; // 0x380d
    int64_t v8 = *(int64_t *)v7; // 0x3811
    uint64_t v9 = *(int64_t *)(v7 + 8); // 0x3814
    if (v5 == 0) {
        // 0x38ac
        return v9 != 0;
    }
    // 0x3821
    if (v9 == 0) {
        // 0x3867
        return 1;
    }
    // 0x382a
    int64_t result; // 0x37e0
    if (*(char *)&ignore_case == 0) {
        // 0x3870
        if (*(char *)&hard_LC_COLLATE != 0) {
            // 0x38c0
            return xmemcoll((char *)v6, v5, (char *)v8, v9);
        }
        // 0x3879
        result = function_25a0();
    } else {
        int64_t v10 = v5 > v9 ? v9 : v5; // 0x3839
        result = memcasecmp((int32_t *)v6, (int32_t *)v8, v10);
    }
    // 0x3842
    if ((int32_t)result != 0) {
        // 0x3867
        return result;
    }
    // 0x3846
    return (int32_t)(v5 > v9) - (int32_t)(v5 < v9);
}