uint keycmp(long param_1,long param_2,ulong param_3,ulong param_4)

{
  void *__s1;
  void *pvVar1;
  void *__s2;
  uint uVar2;
  void **ppvVar3;
  void *pvVar4;
  void *pvVar5;
  
  if (*(ulong *)(param_1 + 0x18) < param_3 || *(ulong *)(param_1 + 0x18) == param_3) {
    if (param_4 < *(ulong *)(param_2 + 0x18)) {
      pvVar5 = *(void **)(param_4 * 0x10 + *(long *)(param_2 + 0x28) + 8);
      goto LAB_001038ac;
    }
    uVar2 = 0;
  }
  else {
    ppvVar3 = (void **)(param_3 * 0x10 + *(long *)(param_1 + 0x28));
    __s1 = *ppvVar3;
    pvVar1 = ppvVar3[1];
    if (*(ulong *)(param_2 + 0x18) <= param_4) {
      return (uint)(pvVar1 != (void *)0x0);
    }
    ppvVar3 = (void **)(param_4 * 0x10 + *(long *)(param_2 + 0x28));
    __s2 = *ppvVar3;
    pvVar5 = ppvVar3[1];
    if (pvVar1 == (void *)0x0) {
LAB_001038ac:
      return -(uint)(pvVar5 != (void *)0x0);
    }
    if (pvVar5 == (void *)0x0) {
      uVar2 = 1;
    }
    else {
      if (ignore_case == '\0') {
        if (hard_LC_COLLATE != '\0') {
          uVar2 = xmemcoll(__s1,pvVar1,__s2,pvVar5);
          return uVar2;
        }
        pvVar4 = pvVar5;
        if (pvVar1 <= pvVar5) {
          pvVar4 = pvVar1;
        }
        uVar2 = memcmp(__s1,__s2,(size_t)pvVar4);
      }
      else {
        pvVar4 = pvVar5;
        if (pvVar1 <= pvVar5) {
          pvVar4 = pvVar1;
        }
        uVar2 = memcasecmp(__s1,__s2,pvVar4);
      }
      if (uVar2 == 0) {
        return (uint)(pvVar1 >= pvVar5 && pvVar1 != pvVar5) - (uint)(pvVar1 < pvVar5);
      }
    }
  }
  return uVar2;
}