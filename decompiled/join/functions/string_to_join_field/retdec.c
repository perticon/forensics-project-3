int64_t string_to_join_field(int64_t a1, int64_t a2) {
    int64_t v1 = __readfsqword(40); // 0x38f8
    char * v2 = (char *)a1; // 0x390b
    int64_t v3; // bp-40, 0x38e0
    int32_t v4 = xstrtoumax(v2, NULL, 10, &v3, (char *)&g6); // 0x390b
    int64_t result = -2; // 0x3913
    if (v4 != 1) {
        if (v4 != 0 || v3 == 0) {
            // 0x3949
            quote(v2);
            function_24b0();
            function_26c0();
            // 0x397b
            return function_24e0();
        }
        // 0x3922
        result = v3 - 1;
    }
    // 0x3926
    if (v1 == __readfsqword(40)) {
        // 0x3936
        return result;
    }
    // 0x397b
    return function_24e0();
}