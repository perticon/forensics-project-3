int64_t string_to_join_field (int64_t arg1) {
    int64_t var_8h;
    rdi = arg1;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    eax = xstrtoumax (rdi, 0, 0xa, rsp, 0x00009ae8);
    if (eax == 1) {
        goto label_0;
    }
    if (eax != 0) {
        goto label_1;
    }
    rax = *(rsp);
    if (rax == 0) {
        goto label_1;
    }
    rax--;
    do {
        rdx = *((rsp + 8));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_2;
        }
        return rax;
label_0:
        rax = 0xfffffffffffffffe;
    } while (1);
label_1:
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid field number: %s");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_2:
    return stack_chk_fail ();
}