void** string_to_join_field(void** rdi, void** rsi) {
    void** rax3;
    struct s0* rcx4;
    int32_t eax5;
    void** rax6;
    void* v7;
    struct s0* rax8;
    void** rax9;
    void** rdx10;
    void** rbp11;
    void** r13_12;
    struct s1* rax13;
    int64_t rax14;
    void** rbp15;
    void** r12d16;
    int32_t edx17;
    uint32_t eax18;
    void** rax19;
    void** rax20;
    int1_t below_or_equal21;
    struct s2* tmp64_22;
    void** rdi23;
    int64_t v24;
    int64_t v25;

    rax3 = *reinterpret_cast<void***>(&g28);
    rcx4 = reinterpret_cast<struct s0*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 24);
    eax5 = xstrtoumax();
    if (eax5 == 1) {
        rax6 = reinterpret_cast<void**>(0xfffffffffffffffe);
    } else {
        if (eax5 || !v7) {
            rax8 = quote(rdi, rdi);
            rax9 = fun_24b0();
            rcx4 = rax8;
            *reinterpret_cast<int32_t*>(&rdi) = 1;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            rdx10 = rax9;
            fun_26c0();
            goto addr_397b_5;
        } else {
            rax6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v7) - 1);
        }
    }
    rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g28)));
    if (!rdx10) {
        return rax6;
    }
    addr_397b_5:
    fun_24e0();
    rbp11 = rdi;
    while (1) {
        *reinterpret_cast<int32_t*>(&r13_12) = 0;
        *reinterpret_cast<int32_t*>(&r13_12 + 4) = 0;
        rax13 = fun_2650(rbp11, ", \t", rdx10, rcx4, 0x9ae8);
        if (rax13) {
            rax13->f0 = 0;
            r13_12 = reinterpret_cast<void**>(&rax13->f1);
        }
        *reinterpret_cast<uint32_t*>(&rax14) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp11));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rax14) == 48) {
            if (*reinterpret_cast<void***>(rbp11 + 1)) 
                break;
            *reinterpret_cast<int32_t*>(&rbp15) = 0;
            *reinterpret_cast<int32_t*>(&rbp15 + 4) = 0;
            r12d16 = reinterpret_cast<void**>(0);
        } else {
            edx17 = static_cast<int32_t>(rax14 - 49);
            if (*reinterpret_cast<unsigned char*>(&edx17) > 1) 
                goto addr_3a3b_16;
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbp11 + 1) == 46)) 
                goto addr_3a9f_18;
            eax18 = *reinterpret_cast<uint32_t*>(&rax14) - 48;
            r12d16 = reinterpret_cast<void**>(static_cast<int32_t>(*reinterpret_cast<signed char*>(&eax18)));
            rax19 = string_to_join_field(rbp11 + 2, ", \t");
            rbp15 = rax19;
        }
        rax20 = xmalloc(24, ", \t", 24, ", \t");
        rdx10 = outlist_end;
        *reinterpret_cast<void***>(rax20) = r12d16;
        *reinterpret_cast<void***>(rax20 + 8) = rbp15;
        *reinterpret_cast<void***>(rax20 + 16) = reinterpret_cast<void**>(0);
        outlist_end = rax20;
        *reinterpret_cast<void***>(rdx10 + 16) = rax20;
        if (!r13_12) 
            goto addr_3a30_21;
        rbp11 = r13_12;
    }
    addr_3a6d_23:
    quote(rbp11, rbp11);
    fun_24b0();
    fun_26c0();
    addr_3a9f_18:
    quote(rbp11, rbp11);
    fun_24b0();
    fun_26c0();
    below_or_equal21 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&g18)) <= reinterpret_cast<unsigned char>(1);
    if (below_or_equal21 || (tmp64_22 = reinterpret_cast<struct s2*>(*reinterpret_cast<void***>(&g28) + 16), tmp64_22->f8 == 0)) {
        rdi23 = empty_filler;
        if (!rdi23) {
            goto v24;
        }
    }
    addr_3a30_21:
    goto v25;
    addr_3a3b_16:
    quote(rbp11, rbp11);
    fun_24b0();
    fun_26c0();
    goto addr_3a6d_23;
}