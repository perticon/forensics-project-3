string_to_join_field (char const *str)
{
  size_t result;
  uintmax_t val;

  strtol_error s_err = xstrtoumax (str, NULL, 10, &val, "");
  if (s_err == LONGINT_OVERFLOW || (s_err == LONGINT_OK && SIZE_MAX < val))
    val = SIZE_MAX;
  else if (s_err != LONGINT_OK || val == 0)
    die (EXIT_FAILURE, 0, _("invalid field number: %s"), quote (str));

  result = val - 1;

  return result;
}