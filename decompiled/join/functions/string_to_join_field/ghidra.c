long string_to_join_field(undefined8 param_1)

{
  int iVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  long in_FS_OFFSET;
  long local_28;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  iVar1 = xstrtoumax(param_1,0,10,&local_28,&DAT_00109ae8);
  if (iVar1 == 1) {
    local_28 = -2;
  }
  else {
    if ((iVar1 != 0) || (local_28 == 0)) {
      uVar2 = quote(param_1);
      uVar3 = dcgettext(0,"invalid field number: %s",5);
                    /* WARNING: Subroutine does not return */
      error(1,0,uVar3,uVar2);
    }
    local_28 = local_28 + -1;
  }
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return local_28;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}