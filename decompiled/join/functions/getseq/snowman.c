signed char getseq(void** rdi, void** rsi, int32_t edx, void** rcx, struct s0* r8, void** r9) {
    void** r13_7;
    void** r14_8;
    void** rax9;
    void** rdx10;
    void** rdx11;
    signed char al12;

    r13_7 = *reinterpret_cast<void***>(rsi);
    r14_8 = *reinterpret_cast<void***>(rsi + 16);
    if (r13_7 == *reinterpret_cast<void***>(rsi + 8) && (rax9 = x2nrealloc(r14_8, rsi + 8, 8), r13_7 = *reinterpret_cast<void***>(rsi), rdx10 = *reinterpret_cast<void***>(rsi + 8), *reinterpret_cast<void***>(rsi + 16) = rax9, r14_8 = rax9, reinterpret_cast<unsigned char>(r13_7) < reinterpret_cast<unsigned char>(rdx10))) {
        fun_2560(rax9 + reinterpret_cast<unsigned char>(r13_7) * 8);
    }
    *reinterpret_cast<int32_t*>(&rdx11) = edx;
    *reinterpret_cast<int32_t*>(&rdx11 + 4) = 0;
    al12 = get_line(rdi, r14_8 + reinterpret_cast<unsigned char>(r13_7) * 8, rdx11, rcx, r8, r9);
    if (al12) {
        *reinterpret_cast<void***>(rsi) = *reinterpret_cast<void***>(rsi) + 1;
    }
    return al12;
}