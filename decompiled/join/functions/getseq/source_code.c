getseq (FILE *fp, struct seq *seq, int whichfile)
{
  if (seq->count == seq->alloc)
    {
      seq->lines = X2NREALLOC (seq->lines, &seq->alloc);
      for (size_t i = seq->count; i < seq->alloc; i++)
        seq->lines[i] = NULL;
    }

  if (get_line (fp, &seq->lines[seq->count], whichfile))
    {
      ++seq->count;
      return true;
    }
  return false;
}