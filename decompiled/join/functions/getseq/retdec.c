bool getseq(struct _IO_FILE * fp, int32_t * seq, int32_t whichfile) {
    int64_t v1 = (int64_t)seq;
    int64_t * v2 = (int64_t *)(v1 + 16); // 0x4344
    int64_t v3 = v1 + 8; // 0x4348
    int64_t * v4 = (int64_t *)v3; // 0x4348
    int64_t v5 = v1; // 0x434c
    int64_t v6 = *v2; // 0x434c
    if (*v4 == v1) {
        int64_t v7 = x2nrealloc(); // 0x437c
        *v2 = v7;
        v5 = v3;
        v6 = v7;
        if (v3 < *v4) {
            // 0x4394
            function_2560();
            v5 = v3;
            v6 = v7;
        }
    }
    int64_t v8 = v6 + 8 * v5; // 0x434e
    bool result = get_line(fp, (int32_t **)v8, whichfile); // 0x4358
    if (result) {
        // 0x4361
        *(int64_t *)seq = v8 + 1;
    }
    // 0x4365
    return result;
}