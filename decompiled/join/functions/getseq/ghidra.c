void getseq(undefined8 param_1,ulong *param_2,undefined4 param_3)

{
  char cVar1;
  ulong uVar2;
  ulong uVar3;
  
  uVar3 = *param_2;
  uVar2 = param_2[2];
  if (uVar3 == param_2[1]) {
    uVar2 = x2nrealloc(uVar2,param_2 + 1,8);
    uVar3 = *param_2;
    param_2[2] = uVar2;
    if (uVar3 < param_2[1]) {
      memset((void *)(uVar2 + uVar3 * 8),0,(param_2[1] - uVar3) * 8);
    }
  }
  cVar1 = get_line(param_1,uVar2 + uVar3 * 8,param_3);
  if (cVar1 != '\0') {
    *param_2 = *param_2 + 1;
  }
  return;
}