void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn00000000000025A0(fn00000000000023D0(0x05, "Try '%s --help' for more information.\n", 0x00), 0x01, stderr);
		goto l000000000000290E;
	}
	fn0000000000002540(fn00000000000023D0(0x05, "Usage: %s [OPTION]...\n", 0x00), 0x01);
	fn0000000000002470(stdout, fn00000000000023D0(0x05, "Print the file name of the terminal connected to standard input.\n\n  -s, --silent, --quiet   print nothing, only return an exit status\n", 0x00));
	fn0000000000002470(stdout, fn00000000000023D0(0x05, "      --help        display this help and exit\n", 0x00));
	fn0000000000002470(stdout, fn00000000000023D0(0x05, "      --version     output version information and exit\n", 0x00));
	struct Eq_600 * rbx_139 = fp - 0xB8 + 16;
	do
	{
		char * rsi_141 = rbx_139->qw0000;
		++rbx_139;
	} while (rsi_141 != null && fn0000000000002490(rsi_141, "tty") != 0x00);
	ptr64 r13_154 = rbx_139->qw0008;
	if (r13_154 != 0x00)
	{
		fn0000000000002540(fn00000000000023D0(0x05, "\n%s online help: <%s>\n", 0x00), 0x01);
		Eq_26 rax_241 = fn0000000000002530(null, 0x05);
		if (rax_241 == 0x00 || fn0000000000002340(0x03, "en_", rax_241) == 0x00)
			goto l0000000000002AC6;
	}
	else
	{
		fn0000000000002540(fn00000000000023D0(0x05, "\n%s online help: <%s>\n", 0x00), 0x01);
		Eq_26 rax_183 = fn0000000000002530(null, 0x05);
		if (rax_183 == 0x00 || fn0000000000002340(0x03, "en_", rax_183) == 0x00)
		{
			fn0000000000002540(fn00000000000023D0(0x05, "Full documentation <%s%s>\n", 0x00), 0x01);
l0000000000002B03:
			fn0000000000002540(fn00000000000023D0(0x05, "or available locally via: info '(coreutils) %s%s'\n", 0x00), 0x01);
l000000000000290E:
			fn0000000000002580(edi);
		}
		r13_154 = 0x70FD;
	}
	fn0000000000002470(stdout, fn00000000000023D0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", 0x00));
l0000000000002AC6:
	fn0000000000002540(fn00000000000023D0(0x05, "Full documentation <%s%s>\n", 0x00), 0x01);
	goto l0000000000002B03;
}