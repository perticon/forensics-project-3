bool main(int param_1,undefined8 *param_2)

{
  int iVar1;
  int *piVar2;
  char *__s;
  long lVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  char cVar6;
  bool bVar7;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  cVar6 = '\0';
  textdomain("coreutils");
  exit_failure = 3;
  atexit(close_stdout);
  do {
    silent = cVar6;
    iVar1 = getopt_long(param_1,param_2,"s",longopts,0);
    if (iVar1 == -1) {
      lVar3 = (long)optind;
      if (param_1 <= optind) {
        piVar2 = __errno_location();
        *piVar2 = 2;
        if (cVar6 == '\0') {
          __s = ttyname(0);
          bVar7 = __s == (char *)0x0;
          if (bVar7) {
            __s = (char *)dcgettext(0,"not a tty",5);
          }
          puts(__s);
        }
        else {
          iVar1 = isatty(0);
          bVar7 = iVar1 == 0;
        }
        return bVar7;
      }
LAB_0010277d:
      uVar4 = quote(param_2[lVar3]);
      uVar5 = dcgettext(0,"extra operand %s",5);
      error(0,0,uVar5,uVar4);
      goto LAB_001026cb;
    }
    if (iVar1 == -0x82) {
      lVar3 = usage(0);
      goto LAB_0010277d;
    }
    cVar6 = '\x01';
  } while (iVar1 == 0x73);
  if (iVar1 != -0x83) {
LAB_001026cb:
    usage(2);
  }
  version_etc(stdout,"tty","GNU coreutils",Version,"David MacKenzie",0);
                    /* WARNING: Subroutine does not return */
  exit(0);
}