
struct s0 {
    signed char f0;
    signed char[11] pad12;
    uint32_t fc;
};

struct s1 {
    signed char[1] pad1;
    void** f1;
};

struct s1* fun_26d0(void** rdi, ...);

void** fun_2680(void** rdi, ...);

void** quotearg_n_style_mem();

void** fun_2660();

void nl_error(uint32_t edi, void** esi, void** rdx, void** rcx, void** r8, void** r9);

void usage(int64_t rdi);

uint32_t parse_symbols(void** rdi, struct s0* rsi, int32_t edx, void** rcx) {
    void** r14_5;
    int32_t r12d6;
    struct s0* rbp7;
    uint32_t ebx8;
    struct s1* rax9;
    struct s0* rdi10;
    struct s1* r8_11;
    void* rdx12;
    uint32_t ecx13;
    uint32_t esi14;
    uint32_t eax15;
    void** rax16;
    void** rax17;
    void** r9_18;

    r14_5 = rdi;
    r12d6 = edx;
    rbp7 = rsi;
    ebx8 = 0;
    while (1) {
        rax9 = fun_26d0(r14_5, r14_5);
        rdi10 = rbp7;
        r8_11 = rax9;
        while (1) {
            *reinterpret_cast<int32_t*>(&rdx12) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
            do {
                ecx13 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi10) + reinterpret_cast<uint64_t>(rdx12));
                esi14 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<uint64_t>(rdx12));
                if (!*reinterpret_cast<signed char*>(&ecx13)) 
                    break;
                rdx12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx12) + 1);
            } while (*reinterpret_cast<signed char*>(&ecx13) == *reinterpret_cast<signed char*>(&esi14));
            goto addr_68d0_6;
            if (*reinterpret_cast<signed char*>(&esi14) == 44) 
                goto addr_68c9_8;
            if (*reinterpret_cast<signed char*>(&esi14)) 
                goto addr_68d0_6;
            addr_68c9_8:
            eax15 = rdi10->fc;
            if (eax15) 
                break;
            addr_68d0_6:
            if (!rdi10->f0) 
                goto addr_68f9_10;
            ++rdi10;
        }
        if (!*reinterpret_cast<signed char*>(&r12d6)) {
            eax15 = ebx8 | eax15;
        }
        if (!r8_11) 
            goto addr_68f0_15;
        r14_5 = reinterpret_cast<void**>(&r8_11->f1);
        ebx8 = eax15;
    }
    addr_68f9_10:
    if (!r8_11) {
        while (1) {
            fun_2680(r14_5, r14_5);
            addr_6904_18:
            rax16 = quotearg_n_style_mem();
            rax17 = fun_2660();
            nl_error(0, 0, "%s: %s", rax17, rax16, r9_18);
            usage(1);
        }
    } else {
        goto addr_6904_18;
    }
    addr_68f0_15:
    return eax15;
}

uint32_t operand_matches(void** rdi, void** rsi, signed char dl, void** rcx) {
    void* rax5;
    uint32_t ecx6;
    uint32_t r8d7;
    uint32_t edx8;

    *reinterpret_cast<uint32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    do {
        ecx6 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(rax5));
        r8d7 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax5));
        if (!*reinterpret_cast<signed char*>(&ecx6)) 
            break;
        rax5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) + 1);
    } while (*reinterpret_cast<signed char*>(&ecx6) == *reinterpret_cast<signed char*>(&r8d7));
    goto addr_4a10_4;
    *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(dl == *reinterpret_cast<signed char*>(&r8d7));
    *reinterpret_cast<unsigned char*>(&edx8) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&r8d7) == 0);
    return *reinterpret_cast<uint32_t*>(&rax5) | edx8;
    addr_4a10_4:
    return 0;
}

void* g28;

int32_t progress_len = 0;

void** stderr = reinterpret_cast<void**>(0);

void fun_26e0();

void verror();

void** fun_2690();

unsigned char o_nocache_eof = 0;

unsigned char input_seekable = 0;

void** input_offset = reinterpret_cast<void**>(0);

void** fun_2570();

/* output_offset.2 */
void** output_offset_2 = reinterpret_cast<void**>(0xfe);

/* o_pending.0 */
void* o_pending_0 = reinterpret_cast<void*>(0);

int32_t fun_2750(int64_t rdi);

void** fun_2710();

unsigned char i_nocache_eof = 0;

void nl_error(uint32_t edi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    signed char al7;
    void* rax8;
    int32_t eax9;
    void** rdi10;
    void** rax11;
    void* rsi12;
    void* rax13;
    void** rdx14;
    uint32_t r13d15;
    void* r12_16;
    void* r15_17;
    uint32_t r14d18;
    void** rsi19;
    void** rax20;
    void* r12_21;
    void* rcx22;
    void* rax23;
    void* rax24;
    void* rax25;
    int64_t v26;
    int64_t rdi27;
    void** rax28;

    if (al7) {
        __asm__("movaps [rsp+0x60], xmm0");
        __asm__("movaps [rsp+0x70], xmm1");
        __asm__("movaps [rsp+0x80], xmm2");
        __asm__("movaps [rsp+0x90], xmm3");
        __asm__("movaps [rsp+0xa0], xmm4");
        __asm__("movaps [rsp+0xb0], xmm5");
        __asm__("movaps [rsp+0xc0], xmm6");
        __asm__("movaps [rsp+0xd0], xmm7");
    }
    rax8 = g28;
    eax9 = progress_len;
    if (!(reinterpret_cast<uint1_t>(eax9 < 0) | reinterpret_cast<uint1_t>(eax9 == 0))) {
        rdi10 = stderr;
        rax11 = *reinterpret_cast<void***>(rdi10 + 40);
        if (reinterpret_cast<unsigned char>(rax11) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi10 + 48))) {
            fun_26e0();
        } else {
            *reinterpret_cast<void***>(rdi10 + 40) = rax11 + 1;
            *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(10);
        }
        progress_len = 0;
    }
    *reinterpret_cast<void***>(&rsi12) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi12) + 4) = 0;
    verror();
    rax13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax8) - reinterpret_cast<uint64_t>(g28));
    if (!rax13) {
        return;
    }
    fun_2690();
    rdx14 = reinterpret_cast<void**>(0x14140);
    r13d15 = o_nocache_eof;
    if (!edi) 
        goto addr_4b56_12;
    addr_4b65_13:
    r12_16 = *rdx14;
    if (!rsi12) {
        if (r12_16 || *reinterpret_cast<signed char*>(&r13d15)) {
            *reinterpret_cast<int32_t*>(&r15_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_17) + 4) = 0;
            if (!edi) {
                addr_4c30_16:
                r14d18 = input_seekable;
                rsi19 = input_offset;
                if (!*reinterpret_cast<signed char*>(&r14d18)) {
                    rax20 = fun_2570();
                    *reinterpret_cast<void***>(rax20) = reinterpret_cast<void**>(29);
                    goto addr_4b9f_18;
                }
            } else {
                rsi19 = output_offset_2;
                if (rsi19 == 0xffffffffffffffff) 
                    goto addr_4b9f_18; else 
                    goto addr_4bd9_20;
            }
        } else {
            addr_4b99_21:
            goto addr_4b9f_18;
        }
    } else {
        r12_21 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_16) + reinterpret_cast<uint64_t>(rsi12));
        if (__intrinsic()) {
            *rdx14 = reinterpret_cast<void*>(0x1ffff);
            r12_16 = reinterpret_cast<void*>(0x7ffffffffffe0000);
        } else {
            rcx22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) >> 63) >> 47);
            rax23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_21) + reinterpret_cast<uint64_t>(rcx22));
            *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<uint32_t*>(&rax23) & 0x1ffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax24) + 4) = 0;
            rax25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax24) - reinterpret_cast<uint64_t>(rcx22));
            *rdx14 = rax25;
            if (reinterpret_cast<int64_t>(r12_21) <= reinterpret_cast<int64_t>(rax25)) 
                goto addr_4b99_21;
            r12_16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_21) - reinterpret_cast<uint64_t>(rax25));
            if (!r12_16) 
                goto addr_4b99_21;
        }
        if (!edi) 
            goto addr_4c30_16;
        rsi19 = output_offset_2;
        r15_17 = o_pending_0;
        if (reinterpret_cast<int1_t>(rsi19 == 0xffffffffffffffff)) {
            goto addr_4b9f_18;
        }
    }
    addr_4bf5_29:
    if (reinterpret_cast<signed char>(rsi19) < reinterpret_cast<signed char>(0)) {
        addr_4b9f_18:
        goto v26;
    } else {
        addr_4bfd_30:
        if (rsi12 || !r12_16) {
            if (r12_16) {
                addr_4c12_32:
                *reinterpret_cast<uint32_t*>(&rdi27) = edi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi27) + 4) = 0;
                fun_2750(rdi27);
                goto addr_4b9f_18;
            } else {
                addr_4c60_33:
                goto addr_4c12_32;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&r13d15)) 
                goto addr_4c60_33;
            goto addr_4c12_32;
        }
    }
    addr_4bd9_20:
    if (reinterpret_cast<signed char>(rsi19) < reinterpret_cast<signed char>(0)) {
        rax28 = fun_2710();
        output_offset_2 = rax28;
        rsi19 = rax28;
        goto addr_4bf5_29;
    } else {
        if (!rsi12) 
            goto addr_4bfd_30;
        rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi19) + (reinterpret_cast<int64_t>(r15_17) + reinterpret_cast<uint64_t>(r12_16)));
        output_offset_2 = rsi19;
        goto addr_4bf5_29;
    }
    addr_4b56_12:
    r13d15 = i_nocache_eof;
    rdx14 = reinterpret_cast<void**>(0x14148);
    goto addr_4b65_13;
}

void process_signals(void** rdi, void** rsi, ...);

int32_t fd_reopen(void** rdi, void** rsi, int64_t rdx, int64_t rcx);

int32_t ifd_reopen(uint32_t edi, void** rsi, void** edx, void** ecx) {
    void** rdi1;
    uint32_t r14d5;
    void** r13_6;
    void** ebp7;
    void** ebx8;
    int64_t rcx9;
    int64_t rdx10;
    int32_t eax11;
    void** rax12;

    *reinterpret_cast<uint32_t*>(&rdi1) = edi;
    r14d5 = *reinterpret_cast<uint32_t*>(&rdi1);
    r13_6 = rsi;
    ebp7 = edx;
    ebx8 = ecx;
    do {
        process_signals(rdi1, rsi, rdi1, rsi);
        *reinterpret_cast<void***>(&rcx9) = ebx8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        *reinterpret_cast<void***>(&rdx10) = ebp7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
        rsi = r13_6;
        *reinterpret_cast<uint32_t*>(&rdi1) = r14d5;
        *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
        eax11 = fd_reopen(rdi1, rsi, rdx10, rcx9);
        if (eax11 >= 0) 
            break;
        rax12 = fun_2570();
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax12) == 4));
    return eax11;
}

void** alloc_obuf(void** rdi, ...);

void** obuf = reinterpret_cast<void**>(0);

void** ibuf = reinterpret_cast<void**>(0);

/* alloc_ibuf.part.0 */
void alloc_ibuf_part_0(void** rdi, ...);

int32_t ifstat(uint32_t edi, void** rsi, ...);

void** quotearg_n_style_colon();

/* quit.constprop.0 */
void quit_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** quotearg_style(int64_t rdi, void** rsi, ...);

void** conversions_mask = reinterpret_cast<void**>(0);

void** print_stats();

int64_t iread_fnc = 0;

void** skip(uint32_t edi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    uint32_t r15d7;
    void** r14_8;
    void** r12_9;
    void** rbx10;
    void** v11;
    void* rax12;
    void* v13;
    void** rax14;
    void** r13_15;
    void*** rax16;
    void** rdi17;
    void** rax18;
    void** v19;
    void*** rbp20;
    void** rax21;
    void** rax22;
    void** rbp23;
    int1_t zf24;
    void** rsi25;
    int32_t eax26;
    void** rcx27;
    uint32_t v28;
    void** rax29;
    void** r12_30;
    void** rax31;
    void** rsi32;
    void** rcx33;
    void** rdx34;
    void** rax35;
    void* rax36;
    void** rax37;
    void** rax38;
    void** esi39;
    void*** rsi40;
    void* v41;
    void*** rax42;
    void** rax43;
    void** rax44;
    int1_t zf45;
    int64_t rdi46;
    int64_t rax47;
    void* rax48;
    void** rdx49;

    r15d7 = edi;
    r14_8 = rdx;
    r12_9 = rcx;
    rbx10 = r8;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    rax14 = fun_2570();
    *reinterpret_cast<void***>(rax14) = reinterpret_cast<void**>(0);
    r13_15 = rax14;
    if (__intrinsic() || (*reinterpret_cast<int32_t*>(&r8) = 0, *reinterpret_cast<int32_t*>(&r8 + 4) = 0, rax16 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r14_8) * reinterpret_cast<unsigned char>(r12_9)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10))), *reinterpret_cast<unsigned char*>(&r8) = __intrinsic(), __intrinsic())) {
        *reinterpret_cast<uint32_t*>(&rdi17) = r15d7;
        *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
        rax18 = fun_2710();
        if (reinterpret_cast<signed char>(rax18) >= reinterpret_cast<signed char>(0)) {
            addr_6732_3:
            v19 = reinterpret_cast<void**>(75);
        } else {
            v19 = reinterpret_cast<void**>(0);
            goto addr_658b_5;
        }
    } else {
        rbp20 = rax16;
        rax21 = fun_2710();
        if (reinterpret_cast<signed char>(rax21) < reinterpret_cast<signed char>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi17) = r15d7;
            *reinterpret_cast<int32_t*>(&rdi17 + 4) = 0;
            v19 = *reinterpret_cast<void***>(r13_15);
            rax22 = fun_2710();
            if (reinterpret_cast<signed char>(rax22) < reinterpret_cast<signed char>(0)) {
                addr_658b_5:
                if (r15d7) {
                    alloc_obuf(rdi17);
                    rbp23 = obuf;
                    goto addr_65b0_9;
                } else {
                    zf24 = ibuf == 0;
                    if (zf24) {
                        alloc_ibuf_part_0(rdi17);
                    }
                    rbp23 = ibuf;
                    goto addr_65b0_9;
                }
            } else {
                if (!v19) 
                    goto addr_6732_3;
            }
        } else {
            if (r15d7) {
                addr_6620_15:
                *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&r8) = 0;
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                goto addr_662a_16;
            } else {
                rsi25 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8 - 8 + 8 - 8 + 8 + 16);
                eax26 = ifstat(0, rsi25, 0, rsi25);
                r8 = r8;
                if (eax26) 
                    goto addr_6826_18;
                rcx27 = input_offset;
                if ((v28 & 0xd000) != 0x8000) 
                    goto addr_6767_20; else 
                    goto addr_66d1_21;
            }
        }
    }
    if (r15d7) {
        while (1) {
            rax29 = quotearg_n_style_colon();
            r12_30 = rax29;
            addr_67ee_24:
            rax31 = fun_2660();
            rsi32 = v19;
            *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
            rcx33 = r12_30;
            rdx34 = rax31;
            nl_error(0, rsi32, rdx34, rcx33, r8, r9);
            addr_67ca_25:
            quit_constprop_0(0, rsi32, rdx34, rcx33, r8, r9);
            addr_67cf_26:
        }
    } else {
        rax35 = quotearg_n_style_colon();
        r12_30 = rax35;
        goto addr_67ee_24;
    }
    addr_6767_20:
    if (reinterpret_cast<signed char>(rcx27) < reinterpret_cast<signed char>(0)) {
        addr_662a_16:
        while (rax36 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v13) - reinterpret_cast<uint64_t>(g28)), !!rax36) {
            fun_2690();
            addr_6826_18:
            rax37 = quotearg_style(4, v11, 4, v11);
            rax38 = fun_2660();
            esi39 = *reinterpret_cast<void***>(r13_15);
            nl_error(1, esi39, rax38, rax37, r8, r9);
            addr_6860_29:
            input_offset = reinterpret_cast<void**>(0xffffffffffffffff);
        }
    } else {
        addr_6770_30:
        if (__intrinsic()) 
            goto addr_6860_29; else 
            goto addr_6779_31;
    }
    return r8;
    addr_6779_31:
    input_offset = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx27) + reinterpret_cast<uint64_t>(rbp20));
    goto addr_662a_16;
    addr_66d1_21:
    if (reinterpret_cast<signed char>(rcx27) < reinterpret_cast<signed char>(0)) 
        goto addr_662a_16;
    rsi40 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(v41) - reinterpret_cast<unsigned char>(rcx27));
    if (reinterpret_cast<int64_t>(rbp20) > reinterpret_cast<int64_t>(rsi40)) {
        rax42 = rbp20;
        rbp20 = rsi40;
        r8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<uint64_t>(rax42) - reinterpret_cast<uint64_t>(v41)) / reinterpret_cast<signed char>(r12_9));
        goto addr_6770_30;
    }
    addr_6785_35:
    if (r15d7) 
        goto addr_67cf_26;
    rax43 = quotearg_style(4, v11, 4, v11);
    rax44 = fun_2660();
    rsi32 = *reinterpret_cast<void***>(r13_15);
    *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
    rcx33 = rax43;
    rdx34 = rax44;
    nl_error(0, rsi32, rdx34, rcx33, r8, r9);
    zf45 = (*reinterpret_cast<unsigned char*>(&conversions_mask + 1) & 1) == 0;
    if (zf45) 
        goto addr_67ca_25;
    print_stats();
    goto addr_67ca_25;
    addr_6665_38:
    r8 = r14_8;
    goto addr_662a_16;
    while (1) {
        addr_6660_39:
        r8 = *reinterpret_cast<void***>(rbx10);
        while (1) {
            *reinterpret_cast<int32_t*>(&r14_8) = 0;
            *reinterpret_cast<int32_t*>(&r14_8 + 4) = 0;
            while (1) {
                *reinterpret_cast<uint32_t*>(&rdi46) = r15d7;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi46) + 4) = 0;
                rax48 = reinterpret_cast<void*>(rax47(rdi46, rbp23, r8));
                if (reinterpret_cast<int64_t>(rax48) < reinterpret_cast<int64_t>(0)) 
                    goto addr_6785_35;
                if (!rax48) 
                    goto addr_6665_38;
                if (!r15d7 && (rdx49 = input_offset, reinterpret_cast<signed char>(rdx49) >= reinterpret_cast<signed char>(0))) {
                    if (__intrinsic()) {
                        input_offset = reinterpret_cast<void**>(0xffffffffffffffff);
                    } else {
                        input_offset = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx49) + reinterpret_cast<uint64_t>(rax48));
                    }
                }
                if (!r14_8) 
                    goto addr_6620_15;
                --r14_8;
                if (!r14_8) 
                    break;
                addr_65b0_9:
                rax47 = iread_fnc;
                r8 = r12_9;
                if (!r14_8) 
                    goto addr_6660_39;
            }
            r8 = *reinterpret_cast<void***>(rbx10);
            if (!r8) 
                goto addr_662a_16;
            rax47 = iread_fnc;
        }
    }
}

void** output_flags = reinterpret_cast<void**>(0);

void** output_blocksize = reinterpret_cast<void**>(0);

uint32_t rpl_fcntl(void** rdi, int64_t rsi, void** rdx, void** rcx);

uint32_t status_level = 3;

signed char invalidate_cache(uint32_t edi, void** rsi, ...);

signed char final_op_was_seek = 0;

uint32_t fun_2770(void** rdi, void** rsi, void** rdx, ...);

void** fun_2620(void** rdi, void** rsi);

signed char o_nocache = 0;

/* iwrite.constprop.0 */
void** iwrite_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rbp7;
    void** rbx8;
    int1_t zf9;
    int1_t less_or_equal10;
    uint32_t eax11;
    void** rdx12;
    uint32_t eax13;
    int1_t zf14;
    void** rax15;
    void** rax16;
    void** rax17;
    void** esi18;
    void** r12_19;
    void** r13d20;
    void** rdx21;
    void** rsi22;
    uint32_t eax23;
    void** rax24;
    void** rax25;
    void** rax26;
    void** rax27;
    int1_t zf28;

    rbp7 = rdi;
    rbx8 = rsi;
    zf9 = (*reinterpret_cast<unsigned char*>(&output_flags + 1) & 64) == 0;
    if (!zf9 && (less_or_equal10 = reinterpret_cast<signed char>(output_blocksize) <= reinterpret_cast<signed char>(rsi), !less_or_equal10)) {
        eax11 = rpl_fcntl(1, 3, rdx, rcx);
        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax11) + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(&eax11) + 1) & 0xbf);
        *reinterpret_cast<uint32_t*>(&rdx12) = eax11;
        *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
        eax13 = rpl_fcntl(1, 4, rdx12, rcx);
        if (eax13 && (zf14 = status_level == 1, !zf14)) {
            rax15 = quotearg_n_style_colon();
            rax16 = fun_2660();
            rax17 = fun_2570();
            esi18 = *reinterpret_cast<void***>(rax17);
            nl_error(0, esi18, rax16, rax15, r8, r9);
        }
        *reinterpret_cast<int32_t*>(&rsi) = 0;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdi) = 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        o_nocache_eof = 1;
        invalidate_cache(1, 0, 1, 0);
        conversions_mask = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(conversions_mask) | 0x8000);
    }
    *reinterpret_cast<int32_t*>(&r12_19) = 0;
    *reinterpret_cast<int32_t*>(&r12_19 + 4) = 0;
    if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rbx8) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rbx8 == 0))) {
        do {
            process_signals(rdi, rsi, rdi, rsi);
            r13d20 = conversions_mask;
            final_op_was_seek = 0;
            if (!(reinterpret_cast<unsigned char>(r13d20) & reinterpret_cast<uint32_t>("ftware: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n"))) 
                goto addr_5f48_7;
            rdx21 = rbx8;
            rsi22 = rbp7;
            do {
                if (*reinterpret_cast<void***>(rsi22)) 
                    goto addr_5f48_7;
                ++rsi22;
                --rdx21;
                if (!rdx21) 
                    goto addr_5f90_11;
            } while (*reinterpret_cast<unsigned char*>(&rdx21) & 15);
            eax23 = fun_2770(rbp7, rsi22, rdx21, rbp7, rsi22, rdx21);
            if (!eax23) {
                addr_5f90_11:
                rsi = rbx8;
                *reinterpret_cast<uint32_t*>(&rdi) = 1;
                *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                rax24 = fun_2710();
                if (reinterpret_cast<signed char>(rax24) >= reinterpret_cast<signed char>(0)) {
                    final_op_was_seek = 1;
                    rax25 = rbx8;
                } else {
                    conversions_mask = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13d20) & 0xfffeffff);
                    goto addr_5f48_7;
                }
            } else {
                addr_5f48_7:
                rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r12_19));
                *reinterpret_cast<uint32_t*>(&rdi) = 1;
                *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                rax25 = fun_2620(1, rsi);
                if (reinterpret_cast<signed char>(rax25) >= reinterpret_cast<signed char>(0)) {
                    if (!rax25) 
                        break;
                } else {
                    rax26 = fun_2570();
                    if (*reinterpret_cast<void***>(rax26) == 4) 
                        continue; else 
                        goto addr_5f6c_18;
                }
            }
            r12_19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_19) + reinterpret_cast<unsigned char>(rax25));
        } while (reinterpret_cast<signed char>(rbx8) > reinterpret_cast<signed char>(r12_19));
        goto addr_5f6c_18;
    } else {
        goto addr_5f7a_22;
    }
    rax27 = fun_2570();
    *reinterpret_cast<void***>(rax27) = reinterpret_cast<void**>(28);
    addr_5f6c_18:
    zf28 = o_nocache == 0;
    if (!zf28 && r12_19) {
        invalidate_cache(1, r12_19, 1, r12_19);
        return r12_19;
    }
    addr_5f7a_22:
    return r12_19;
}

void** page_size = reinterpret_cast<void**>(0);

void** fun_2940(void** rdi, void** rsi, void** rdx, void** rcx, void** r8);

void** human_readable(void** rdi, void*** rsi, int64_t rdx, int64_t rcx, void* r8, void** r9);

void** alloc_obuf(void** rdi, ...) {
    int1_t zf2;
    int1_t zf3;
    void** rbp4;
    void** rdi5;
    void* rax6;
    void** rdx7;
    void** rcx8;
    void** r8_9;
    void** rax10;
    void** r9_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r9_15;
    void** rax16;
    void** rax17;
    int1_t zf18;
    void** rax19;
    void** rax20;

    while (zf2 = obuf == 0, zf2) {
        zf3 = (*reinterpret_cast<unsigned char*>(&conversions_mask + 1) & 8) == 0;
        if (zf3) 
            goto addr_4eb4_3;
        rbp4 = output_blocksize;
        rdi5 = page_size;
        rax6 = g28;
        rax10 = fun_2940(rdi5, rbp4, rdx7, rcx8, r8_9);
        obuf = rax10;
        if (!rax10) {
            rax12 = human_readable(rbp4, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x2a8 - 8 + 8, 0x1f1, 1, 1, r9_11);
            r12_13 = output_blocksize;
            rax14 = fun_2660();
            r8_9 = rax12;
            rcx8 = r12_13;
            rdx7 = rax14;
            nl_error(1, 0, rdx7, rcx8, r8_9, r9_15);
        } else {
            rax16 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax6) - reinterpret_cast<uint64_t>(g28));
            if (!rax16) 
                goto addr_4e32_8;
        }
        rax17 = fun_2690();
    }
    return rax17;
    addr_4eb4_3:
    zf18 = ibuf == 0;
    if (zf18) {
        alloc_ibuf_part_0(rdi, rdi);
        rax19 = ibuf;
        obuf = rax19;
        return rax19;
    } else {
        rax20 = ibuf;
        obuf = rax20;
        return rax20;
    }
    addr_4e32_8:
    return rax16;
}

int64_t col = 0;

int64_t conversion_blocksize = 0;

void** oc = reinterpret_cast<void**>(0);

/* pending_spaces.3 */
int64_t pending_spaces_3 = 0;

unsigned char newline_character = 10;

void write_output(void** rdi, ...);

unsigned char space_character = 32;

void copy_with_unblock(void** rdi, void** rsi, void** rdx, ...) {
    void** r12_4;
    void** rbp5;
    void** rbx6;
    int64_t rax7;
    int1_t less8;
    uint32_t r13d9;
    void** rdx10;
    void** rcx11;
    uint32_t esi12;
    void** rax13;
    int1_t less14;
    uint32_t ecx15;
    int64_t rdx16;
    void** rsi17;
    void** rax18;
    void** rdx19;
    int1_t less20;
    int1_t zf21;
    void** rax22;
    int1_t less23;

    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rsi) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rsi == 0)) {
        return;
    } else {
        *reinterpret_cast<int32_t*>(&r12_4) = 0;
        *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
        rbp5 = rdi;
        rbx6 = rsi;
        while (1) {
            rax7 = col;
            less8 = rax7 < conversion_blocksize;
            r13d9 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(r12_4));
            col = rax7 + 1;
            if (!less8) {
                rdx10 = oc;
                rcx11 = obuf;
                pending_spaces_3 = 0;
                esi12 = newline_character;
                col = 0;
                rax13 = rdx10 + 1;
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx11) + reinterpret_cast<unsigned char>(rdx10)) = *reinterpret_cast<signed char*>(&esi12);
                less14 = reinterpret_cast<signed char>(rax13) < reinterpret_cast<signed char>(output_blocksize);
                oc = rax13;
                if (!less14) {
                    addr_64a0_6:
                    write_output(rdi);
                    goto addr_6423_7;
                } else {
                    addr_6423_7:
                    if (reinterpret_cast<signed char>(rbx6) <= reinterpret_cast<signed char>(r12_4)) 
                        break;
                }
            } else {
                ecx15 = space_character;
                rdx16 = pending_spaces_3;
                ++r12_4;
                if (*reinterpret_cast<signed char*>(&ecx15) == *reinterpret_cast<signed char*>(&r13d9)) {
                    pending_spaces_3 = rdx16 + 1;
                    if (reinterpret_cast<signed char>(rbx6) > reinterpret_cast<signed char>(r12_4)) 
                        continue; else 
                        break;
                }
                rsi17 = obuf;
                rax18 = oc;
                if (rdx16) {
                    while (1) {
                        rdx19 = rax18 + 1;
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi17) + reinterpret_cast<unsigned char>(rax18)) = *reinterpret_cast<signed char*>(&ecx15);
                        less20 = reinterpret_cast<signed char>(rdx19) < reinterpret_cast<signed char>(output_blocksize);
                        oc = rdx19;
                        if (!less20) {
                            write_output(rdi);
                            rsi17 = obuf;
                            rdx19 = oc;
                        }
                        --pending_spaces_3;
                        zf21 = pending_spaces_3 == 0;
                        if (zf21) 
                            break;
                        ecx15 = space_character;
                        rax18 = rdx19;
                    }
                } else {
                    rdx19 = rax18;
                }
                rax22 = rdx19 + 1;
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi17) + reinterpret_cast<unsigned char>(rdx19)) = *reinterpret_cast<signed char*>(&r13d9);
                less23 = reinterpret_cast<signed char>(rax22) < reinterpret_cast<signed char>(output_blocksize);
                oc = rax22;
                if (less23) 
                    goto addr_6423_7; else 
                    goto addr_64a0_6;
            }
        }
        return;
    }
}

void** gethrxtime(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** w_bytes = reinterpret_cast<void**>(0);

void** start_time = reinterpret_cast<void**>(0);

int32_t fun_2530(void** rdi, uint64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, void** r9, void** a7, void** a8, void** a9);

/* slash_s.5 */
uint16_t slash_s_5 = 0x732f;

unsigned char gf490 = 0;

void** gd;

int32_t g11;

void** fun_28c0();

void** fun_2930(void** rdi, void** rsi, void** rdx, ...);

void** reported_w_bytes = reinterpret_cast<void**>(0xff);

void** r_full = reinterpret_cast<void**>(0);

void** w_full = reinterpret_cast<void**>(0);

void** r_partial = reinterpret_cast<void**>(0);

void** r_truncate = reinterpret_cast<void**>(0);

void print_xfer_stats() {
    void** rbp1;
    void* rsp2;
    void* rax3;
    void** rdx4;
    void** rcx5;
    void** r8_6;
    void** r9_7;
    void** rax8;
    void** rdi9;
    void** rax10;
    void* rsp11;
    void** rdi12;
    void** rax13;
    void* rsp14;
    void** rax15;
    void** rax16;
    void* rsp17;
    void** v18;
    void** v19;
    void** v20;
    void* rsp21;
    void** rdi22;
    void** v23;
    void** rax24;
    void** rax25;
    uint32_t edx26;
    void** v27;
    uint32_t edx28;
    int64_t r8_29;
    void** rdi30;
    void** rax31;
    void** v32;
    void** v33;
    void** v34;
    void** rax35;
    void** rax36;
    void** rdi37;
    void** rax38;
    int64_t rbp39;
    void** rax40;
    void** rax41;
    void** rdx42;
    void** rax43;
    void** rdi44;
    void** rax45;
    void** rdi46;
    void** rax47;
    int32_t ecx48;
    void** rdi49;
    void** rax50;
    void* rax51;
    int1_t zf52;
    int32_t ecx53;
    void** rdi54;
    void** rax55;
    void** r12_56;
    void** rbp57;
    void** rbx58;
    void** rax59;
    void** rdi60;
    void** r12_61;
    void** rax62;
    void** rdi63;
    int1_t zf64;
    int64_t v65;
    int64_t v66;

    while (1) {
        rbp1 = reinterpret_cast<void**>(0);
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x7e8);
        rax3 = g28;
        if (1) {
            rax8 = gethrxtime(0, 1, rdx4, rcx5, r8_6, r9_7);
            rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
            rbp1 = rax8;
        }
        rdi9 = w_bytes;
        rax10 = human_readable(rdi9, reinterpret_cast<int64_t>(rsp2) + 16, 0x1d1, 1, 1, r9_7);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        rdi12 = w_bytes;
        rax13 = human_readable(rdi12, reinterpret_cast<int64_t>(rsp11) + 0x29e, 0x1f1, 1, 1, r9_7);
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        rax15 = start_time;
        if (reinterpret_cast<signed char>(rax15) >= reinterpret_cast<signed char>(rbp1)) {
            rax16 = fun_2660();
            rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
            r9_7 = rax16;
            fun_2530(reinterpret_cast<int64_t>(rsp17) + 0x52c, 0x28e, 1, 0x28e, "%s B/s", r9_7, v18, v19, v20);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
            __asm__("pxor xmm0, xmm0");
        } else {
            rdi22 = w_bytes;
            __asm__("pxor xmm0, xmm0");
            __asm__("cvtsi2sd xmm0, r8");
            __asm__("divsd xmm0, [rip+0xa4e2]");
            *reinterpret_cast<void***>(rdi22) = v23;
            rax24 = human_readable(rdi22 + 4, reinterpret_cast<int64_t>(rsp14) + 0x52c + 4, 0x1d1, 0x3b9aca00, reinterpret_cast<unsigned char>(rbp1) - reinterpret_cast<unsigned char>(rax15), r9_7);
            rax25 = fun_2680(rax24, rax24);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8);
            edx26 = slash_s_5;
            *reinterpret_cast<void***>(rax24) = v27;
            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(rax25)) = *reinterpret_cast<int16_t*>(&edx26);
            edx28 = gf490;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(rax25) + 2) = *reinterpret_cast<signed char*>(&edx28);
        }
        r8_29 = reinterpret_cast<int64_t>("%g s");
        if (!1) {
            rdi30 = stderr;
            rax31 = *reinterpret_cast<void***>(rdi30 + 40);
            if (reinterpret_cast<unsigned char>(rax31) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi30 + 48))) {
                *reinterpret_cast<void***>(rdi30) = gd;
                fun_26e0();
                rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                *reinterpret_cast<int32_t*>(rdi30 + 4) = g11;
                r8_29 = reinterpret_cast<int64_t>("%.0f s");
            } else {
                r8_29 = reinterpret_cast<int64_t>("%.0f s");
                *reinterpret_cast<void***>(rdi30 + 40) = rax31 + 1;
                *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(13);
            }
        }
        fun_2530(reinterpret_cast<int64_t>(rsp21) + 0x7c0, 24, 1, 24, r8_29, r9_7, v32, v33, v34);
        rax35 = fun_2680(rax10, rax10);
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax10) + reinterpret_cast<unsigned char>(rax35) + 0xfffffffffffffffe) == 32) {
            rax36 = fun_28c0();
            rdi37 = stderr;
            rax38 = fun_2930(rdi37, 1, rax36, rdi37, 1, rax36);
            *reinterpret_cast<int32_t*>(&rbp39) = *reinterpret_cast<int32_t*>(&rax38);
        } else {
            rax40 = fun_2680(rax13, rax13);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(rax40) + 0xfffffffffffffffe) == 32) {
                rax41 = fun_2660();
                rdx42 = rax41;
            } else {
                rax43 = fun_2660();
                rdx42 = rax43;
            }
            rdi44 = stderr;
            rax45 = fun_2930(rdi44, 1, rdx42, rdi44, 1, rdx42);
            *reinterpret_cast<int32_t*>(&rbp39) = *reinterpret_cast<int32_t*>(&rax45);
        }
        if (1) {
            rdi46 = stderr;
            rax47 = *reinterpret_cast<void***>(rdi46 + 40);
            if (reinterpret_cast<unsigned char>(rax47) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi46 + 48))) {
                fun_26e0();
            } else {
                *reinterpret_cast<void***>(rdi46 + 40) = rax47 + 1;
                *reinterpret_cast<void***>(rax47) = reinterpret_cast<void**>(10);
            }
        } else {
            if (*reinterpret_cast<int32_t*>(&rbp39) >= 0 && (ecx48 = progress_len, ecx48 > *reinterpret_cast<int32_t*>(&rbp39))) {
                rdi49 = stderr;
                fun_2930(rdi49, 1, "%*s");
            }
            progress_len = *reinterpret_cast<int32_t*>(&rbp39);
        }
        rax50 = w_bytes;
        reported_w_bytes = rax50;
        rax51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(g28));
        if (!rax51) 
            break;
        fun_2690();
        zf52 = status_level == 1;
        if (zf52) 
            goto addr_5920_38;
        ecx53 = progress_len;
        if (!(reinterpret_cast<uint1_t>(ecx53 < 0) | reinterpret_cast<uint1_t>(ecx53 == 0))) {
            rdi54 = stderr;
            rax55 = *reinterpret_cast<void***>(rdi54 + 40);
            if (reinterpret_cast<unsigned char>(rax55) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi54 + 48))) {
                fun_26e0();
            } else {
                *reinterpret_cast<void***>(rdi54 + 40) = rax55 + 1;
                *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(10);
            }
            progress_len = 0;
        }
        r12_56 = r_full;
        rbp57 = w_full;
        rbx58 = r_partial;
        rax59 = fun_2660();
        rcx5 = r12_56;
        r9_7 = rbp57;
        rdi60 = stderr;
        r8_6 = rbx58;
        fun_2930(rdi60, 1, rax59, rdi60, 1, rax59);
        r12_61 = r_truncate;
        rdx4 = reinterpret_cast<void**>(0x587a);
        if (r12_61) {
            *reinterpret_cast<int32_t*>(&r8_6) = 5;
            *reinterpret_cast<int32_t*>(&r8_6 + 4) = 0;
            rax62 = fun_28c0();
            rdi63 = stderr;
            rcx5 = r12_61;
            rdx4 = rax62;
            fun_2930(rdi63, 1, rdx4, rdi63, 1, rdx4);
        }
        zf64 = status_level == 2;
        if (zf64) 
            goto addr_58d0_47;
    }
    return;
    addr_5920_38:
    goto v65;
    addr_58d0_47:
    goto v66;
}

void** w_partial = reinterpret_cast<void**>(0);

void** print_stats() {
    int1_t zf1;
    void* rsp2;
    int32_t ecx3;
    void** rdi4;
    void** rax5;
    void** r13_6;
    void** r12_7;
    void** rbp8;
    void** rbx9;
    void** rax10;
    void** rcx11;
    void** r9_12;
    void** rdi13;
    void** r8_14;
    void** r12_15;
    void** rax16;
    void** rdx17;
    void* rsp18;
    void** rax19;
    void** rdi20;
    int1_t zf21;
    void** rbp22;
    void* rsp23;
    void* rax24;
    void** rax25;
    void** rdi26;
    void** rax27;
    void* rsp28;
    void** rdi29;
    void** rax30;
    void* rsp31;
    void** rax32;
    void** rax33;
    void* rsp34;
    void** v35;
    void** v36;
    void** v37;
    void* rsp38;
    void** rdi39;
    void** v40;
    void** rax41;
    void** rax42;
    uint32_t edx43;
    void** v44;
    uint32_t edx45;
    int64_t r8_46;
    void** rdi47;
    void** rax48;
    void** v49;
    void** v50;
    void** v51;
    void** rax52;
    void** rax53;
    void** rdi54;
    void** rax55;
    int64_t rbp56;
    void** rax57;
    void** rax58;
    void** rdx59;
    void** rax60;
    void** rdi61;
    void** rax62;
    void** rdi63;
    void** rax64;
    int32_t ecx65;
    void** rdi66;
    void** rax67;
    void** rax68;
    void** rax69;

    while (zf1 = status_level == 1, !zf1) {
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8);
        ecx3 = progress_len;
        if (!(reinterpret_cast<uint1_t>(ecx3 < 0) | reinterpret_cast<uint1_t>(ecx3 == 0))) {
            rdi4 = stderr;
            rax5 = *reinterpret_cast<void***>(rdi4 + 40);
            if (reinterpret_cast<unsigned char>(rax5) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi4 + 48))) {
                fun_26e0();
                rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
            } else {
                *reinterpret_cast<void***>(rdi4 + 40) = rax5 + 1;
                *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(10);
            }
            progress_len = 0;
        }
        r13_6 = w_partial;
        r12_7 = r_full;
        rbp8 = w_full;
        rbx9 = r_partial;
        rax10 = fun_2660();
        rcx11 = r12_7;
        r9_12 = rbp8;
        rdi13 = stderr;
        r8_14 = rbx9;
        fun_2930(rdi13, 1, rax10, rdi13, 1, rax10);
        r12_15 = r_truncate;
        rax16 = r13_6;
        rdx17 = reinterpret_cast<void**>(0x587a);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
        if (r12_15) {
            *reinterpret_cast<int32_t*>(&r8_14) = 5;
            *reinterpret_cast<int32_t*>(&r8_14 + 4) = 0;
            rax19 = fun_28c0();
            rdi20 = stderr;
            rcx11 = r12_15;
            rdx17 = rax19;
            rax16 = fun_2930(rdi20, 1, rdx17, rdi20, 1, rdx17);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
        }
        zf21 = status_level == 2;
        if (zf21) 
            goto addr_58d0_10;
        rbp22 = reinterpret_cast<void**>(0);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x7e8);
        rax24 = g28;
        if (1) {
            rax25 = gethrxtime(0, 1, rdx17, rcx11, r8_14, r9_12);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            rbp22 = rax25;
        }
        rdi26 = w_bytes;
        rax27 = human_readable(rdi26, reinterpret_cast<int64_t>(rsp23) + 16, 0x1d1, 1, 1, r9_12);
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        rdi29 = w_bytes;
        rax30 = human_readable(rdi29, reinterpret_cast<int64_t>(rsp28) + 0x29e, 0x1f1, 1, 1, r9_12);
        rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
        rax32 = start_time;
        if (reinterpret_cast<signed char>(rax32) >= reinterpret_cast<signed char>(rbp22)) {
            rax33 = fun_2660();
            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            r9_12 = rax33;
            fun_2530(reinterpret_cast<int64_t>(rsp34) + 0x52c, 0x28e, 1, 0x28e, "%s B/s", r9_12, v35, v36, v37);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            __asm__("pxor xmm0, xmm0");
        } else {
            rdi39 = w_bytes;
            __asm__("pxor xmm0, xmm0");
            __asm__("cvtsi2sd xmm0, r8");
            __asm__("divsd xmm0, [rip+0xa4e2]");
            *reinterpret_cast<void***>(rdi39) = v40;
            rax41 = human_readable(rdi39 + 4, reinterpret_cast<int64_t>(rsp31) + 0x52c + 4, 0x1d1, 0x3b9aca00, reinterpret_cast<unsigned char>(rbp22) - reinterpret_cast<unsigned char>(rax32), r9_12);
            rax42 = fun_2680(rax41, rax41);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
            edx43 = slash_s_5;
            *reinterpret_cast<void***>(rax41) = v44;
            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rax41) + reinterpret_cast<unsigned char>(rax42)) = *reinterpret_cast<int16_t*>(&edx43);
            edx45 = gf490;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax41) + reinterpret_cast<unsigned char>(rax42) + 2) = *reinterpret_cast<signed char*>(&edx45);
        }
        r8_46 = reinterpret_cast<int64_t>("%g s");
        if (!1) {
            rdi47 = stderr;
            rax48 = *reinterpret_cast<void***>(rdi47 + 40);
            if (reinterpret_cast<unsigned char>(rax48) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi47 + 48))) {
                *reinterpret_cast<void***>(rdi47) = gd;
                fun_26e0();
                rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
                *reinterpret_cast<int32_t*>(rdi47 + 4) = g11;
                r8_46 = reinterpret_cast<int64_t>("%.0f s");
            } else {
                r8_46 = reinterpret_cast<int64_t>("%.0f s");
                *reinterpret_cast<void***>(rdi47 + 40) = rax48 + 1;
                *reinterpret_cast<void***>(rax48) = reinterpret_cast<void**>(13);
            }
        }
        fun_2530(reinterpret_cast<int64_t>(rsp38) + 0x7c0, 24, 1, 24, r8_46, r9_12, v49, v50, v51);
        rax52 = fun_2680(rax27, rax27);
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax27) + reinterpret_cast<unsigned char>(rax52) + 0xfffffffffffffffe) == 32) {
            rax53 = fun_28c0();
            rdi54 = stderr;
            rax55 = fun_2930(rdi54, 1, rax53, rdi54, 1, rax53);
            *reinterpret_cast<int32_t*>(&rbp56) = *reinterpret_cast<int32_t*>(&rax55);
        } else {
            rax57 = fun_2680(rax30, rax30);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<unsigned char>(rax57) + 0xfffffffffffffffe) == 32) {
                rax58 = fun_2660();
                rdx59 = rax58;
            } else {
                rax60 = fun_2660();
                rdx59 = rax60;
            }
            rdi61 = stderr;
            rax62 = fun_2930(rdi61, 1, rdx59, rdi61, 1, rdx59);
            *reinterpret_cast<int32_t*>(&rbp56) = *reinterpret_cast<int32_t*>(&rax62);
        }
        if (1) {
            rdi63 = stderr;
            rax64 = *reinterpret_cast<void***>(rdi63 + 40);
            if (reinterpret_cast<unsigned char>(rax64) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi63 + 48))) {
                fun_26e0();
            } else {
                *reinterpret_cast<void***>(rdi63 + 40) = rax64 + 1;
                *reinterpret_cast<void***>(rax64) = reinterpret_cast<void**>(10);
            }
        } else {
            if (*reinterpret_cast<int32_t*>(&rbp56) >= 0 && (ecx65 = progress_len, ecx65 > *reinterpret_cast<int32_t*>(&rbp56))) {
                rdi66 = stderr;
                fun_2930(rdi66, 1, "%*s");
            }
            progress_len = *reinterpret_cast<int32_t*>(&rbp56);
        }
        rax67 = w_bytes;
        reported_w_bytes = rax67;
        rax68 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax24) - reinterpret_cast<uint64_t>(g28));
        if (!rax68) 
            goto addr_5686_47;
        rax69 = fun_2690();
    }
    return rax69;
    addr_58d0_10:
    return rax16;
    addr_5686_47:
    return rax68;
}

signed char invalidate_cache(uint32_t edi, void** rsi, ...) {
    void** rdx3;
    uint32_t r13d4;
    void* r12_5;
    void* r15_6;
    uint32_t r14d7;
    void** rsi8;
    void** rax9;
    void* r12_10;
    void* rcx11;
    void* rax12;
    void* rax13;
    void* rax14;
    uint32_t eax15;
    int64_t rdi16;
    int32_t eax17;
    void** rax18;

    rdx3 = reinterpret_cast<void**>(0x14140);
    r13d4 = o_nocache_eof;
    if (!edi) {
        r13d4 = i_nocache_eof;
        rdx3 = reinterpret_cast<void**>(0x14148);
    }
    r12_5 = *rdx3;
    if (!rsi) {
        if (r12_5 || *reinterpret_cast<signed char*>(&r13d4)) {
            *reinterpret_cast<int32_t*>(&r15_6) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_6) + 4) = 0;
            if (!edi) {
                addr_4c30_6:
                r14d7 = input_seekable;
                rsi8 = input_offset;
                if (!*reinterpret_cast<unsigned char*>(&r14d7)) {
                    rax9 = fun_2570();
                    *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(29);
                    goto addr_4b9f_8;
                }
            } else {
                rsi8 = output_offset_2;
                r14d7 = 0;
                if (rsi8 == 0xffffffffffffffff) 
                    goto addr_4b9f_8; else 
                    goto addr_4bd9_10;
            }
        } else {
            addr_4b99_11:
            r14d7 = 1;
            goto addr_4b9f_8;
        }
    } else {
        r12_10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_5) + reinterpret_cast<unsigned char>(rsi));
        if (__intrinsic()) {
            *rdx3 = reinterpret_cast<void*>(0x1ffff);
            r12_5 = reinterpret_cast<void*>(0x7ffffffffffe0000);
        } else {
            rcx11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_10) >> 63) >> 47);
            rax12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_10) + reinterpret_cast<uint64_t>(rcx11));
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax12) & 0x1ffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            rax14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax13) - reinterpret_cast<uint64_t>(rcx11));
            *rdx3 = rax14;
            if (reinterpret_cast<int64_t>(r12_10) <= reinterpret_cast<int64_t>(rax14)) 
                goto addr_4b99_11;
            r12_5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_10) - reinterpret_cast<uint64_t>(rax14));
            if (!r12_5) 
                goto addr_4b99_11;
        }
        if (!edi) 
            goto addr_4c30_6;
        rsi8 = output_offset_2;
        r15_6 = o_pending_0;
        r14d7 = 0;
        if (reinterpret_cast<int1_t>(rsi8 == 0xffffffffffffffff)) {
            goto addr_4b9f_8;
        }
    }
    addr_4bf5_19:
    r14d7 = 0;
    if (reinterpret_cast<signed char>(rsi8) < reinterpret_cast<signed char>(0)) {
        addr_4b9f_8:
        eax15 = r14d7;
        return *reinterpret_cast<signed char*>(&eax15);
    } else {
        addr_4bfd_20:
        if (rsi || !r12_5) {
            if (r12_5) {
                addr_4c12_22:
                *reinterpret_cast<uint32_t*>(&rdi16) = edi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_2750(rdi16);
                *reinterpret_cast<unsigned char*>(&r14d7) = reinterpret_cast<uint1_t>(eax17 != -1);
                goto addr_4b9f_8;
            } else {
                addr_4c60_23:
                goto addr_4c12_22;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&r13d4)) 
                goto addr_4c60_23;
            goto addr_4c12_22;
        }
    }
    addr_4bd9_10:
    if (reinterpret_cast<signed char>(rsi8) < reinterpret_cast<signed char>(0)) {
        rax18 = fun_2710();
        output_offset_2 = rax18;
        rsi8 = rax18;
        goto addr_4bf5_19;
    } else {
        if (!rsi) 
            goto addr_4bfd_20;
        rsi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi8) + (reinterpret_cast<int64_t>(r15_6) + reinterpret_cast<uint64_t>(r12_5)));
        output_offset_2 = rsi8;
        goto addr_4bf5_19;
    }
}

void** output_file = reinterpret_cast<void**>(0);

void write_output(void** rdi, ...) {
    void** rsi2;
    void** rdi3;
    int64_t v4;
    int64_t rbx5;
    void** rdx6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** rax10;
    void** tmp64_11;
    int1_t zf12;
    void** rsi13;
    void** rax14;
    void** rax15;
    void** rax16;
    void** rsi17;
    void** r8_18;
    void** r9_19;
    void** tmp64_20;
    void** r8_21;
    void** r9_22;
    void** r12_23;
    void** rbp24;
    uint32_t eax25;
    int1_t zf26;
    int64_t rbx27;
    int64_t rdx28;
    void** tmp64_29;
    void** rcx30;
    void** rsi31;
    void** rdx32;
    int1_t less33;
    void** rdx34;
    void** rcx35;
    uint32_t esi36;
    void** rax37;
    int1_t less38;
    int1_t less_or_equal39;
    void** tmp64_40;

    rsi2 = output_blocksize;
    rdi3 = obuf;
    v4 = rbx5;
    rax10 = iwrite_constprop_0(rdi3, rsi2, rdx6, rcx7, r8_8, r9_9);
    tmp64_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(w_bytes) + reinterpret_cast<unsigned char>(rax10));
    w_bytes = tmp64_11;
    zf12 = output_blocksize == rax10;
    if (!zf12) {
        rsi13 = output_file;
        rax14 = quotearg_style(4, rsi13);
        rax15 = fun_2660();
        rax16 = fun_2570();
        rsi17 = *reinterpret_cast<void***>(rax16);
        *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
        nl_error(0, rsi17, rax15, rax14, r8_18, r9_19);
        if (rax10) {
            tmp64_20 = w_partial + 1;
            w_partial = tmp64_20;
        }
        quit_constprop_0(0, rsi17, rax15, rax14, r8_21, r9_22);
        r12_23 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rsi17)));
        if (rsi17) {
            rbp24 = reinterpret_cast<void**>(0);
            while (1) {
                eax25 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp24));
                zf26 = *reinterpret_cast<unsigned char*>(&eax25) == newline_character;
                rbx27 = col;
                rdx28 = conversion_blocksize;
                if (!zf26) {
                    if (rdx28 == rbx27) {
                        tmp64_29 = r_truncate + 1;
                        r_truncate = tmp64_29;
                    } else {
                        if (rdx28 > rbx27 && (rcx30 = oc, rsi31 = obuf, rdx32 = rcx30 + 1, *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi31) + reinterpret_cast<unsigned char>(rcx30)) = *reinterpret_cast<unsigned char*>(&eax25), less33 = reinterpret_cast<signed char>(rdx32) < reinterpret_cast<signed char>(output_blocksize), oc = rdx32, !less33)) {
                            write_output(0);
                            rbx27 = col;
                        }
                    }
                    ++rbp24;
                    col = rbx27 + 1;
                    if (rbp24 == r12_23) 
                        break;
                } else {
                    if (rdx28 > rbx27) {
                        do {
                            rdx34 = oc;
                            rcx35 = obuf;
                            esi36 = space_character;
                            rax37 = rdx34 + 1;
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx35) + reinterpret_cast<unsigned char>(rdx34)) = *reinterpret_cast<signed char*>(&esi36);
                            less38 = reinterpret_cast<signed char>(rax37) < reinterpret_cast<signed char>(output_blocksize);
                            oc = rax37;
                            if (!less38) {
                                write_output(0);
                            }
                            ++rbx27;
                            less_or_equal39 = conversion_blocksize <= rbx27;
                        } while (!less_or_equal39);
                    }
                    ++rbp24;
                    col = 0;
                    if (rbp24 == r12_23) 
                        break;
                }
            }
        }
        goto v4;
    } else {
        oc = reinterpret_cast<void**>(0);
        tmp64_40 = w_full + 1;
        w_full = tmp64_40;
        return;
    }
}

uint32_t interrupt_signal = 0;

int32_t info_signal_count = 0;

void fun_2520();

void cleanup(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_2540();

void process_signals(void** rdi, void** rsi, ...) {
    void** rsp3;
    void* rax4;
    void* v5;
    uint32_t eax6;
    int32_t eax7;
    int64_t r12_8;
    int32_t eax9;
    void* rsp10;
    uint32_t eax11;
    int32_t eax12;
    void** rcx13;
    void** r8_14;
    void** r9_15;
    void* rax16;
    uint32_t r14d17;
    void** r13_18;
    int32_t ebx19;
    int32_t ecx20;
    int64_t rcx21;
    int32_t eax22;
    void** rax23;
    int64_t v24;

    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x90);
    rax4 = g28;
    v5 = rax4;
    while ((eax6 = interrupt_signal, !!eax6) || (eax7 = info_signal_count, !!eax7)) {
        do {
            fun_2520();
            *reinterpret_cast<uint32_t*>(&r12_8) = interrupt_signal;
            eax9 = info_signal_count;
            if (eax9) {
                info_signal_count = eax9 - 1;
            }
            rsi = rsp3;
            *reinterpret_cast<uint32_t*>(&rdi) = 2;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            fun_2520();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8 - 8 + 8);
            if (*reinterpret_cast<uint32_t*>(&r12_8)) 
                break;
            print_stats();
            rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10) - 8 + 8);
            eax11 = interrupt_signal;
        } while (eax11 || (eax12 = info_signal_count, !!eax12));
        break;
        cleanup(2, rsi, 0, rcx13, r8_14, r9_15);
        print_stats();
        *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&r12_8);
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        fun_2540();
        rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v5) - reinterpret_cast<uint64_t>(g28));
    if (rax16) {
        fun_2690();
        r14d17 = *reinterpret_cast<uint32_t*>(&rdi);
        r13_18 = rsi;
        ebx19 = ecx20;
        do {
            process_signals(rdi, rsi, rdi, rsi);
            *reinterpret_cast<int32_t*>(&rcx21) = ebx19;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx21) + 4) = 0;
            rsi = r13_18;
            *reinterpret_cast<uint32_t*>(&rdi) = r14d17;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax22 = fd_reopen(rdi, rsi, 0, rcx21);
            if (eax22 >= 0) 
                break;
            rax23 = fun_2570();
        } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 4));
        goto v24;
    } else {
        return;
    }
}

uint32_t synchronize_output(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

int32_t fun_2740();

void fun_2910();

void** input_file = reinterpret_cast<void**>(0);

void cleanup(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    uint32_t eax7;
    uint32_t eax8;
    int32_t eax9;
    int32_t eax10;
    void** rax11;
    void** rbx12;
    void** eax13;
    int32_t eax14;
    void** rax15;
    void** rbx16;
    void** eax17;
    int32_t eax18;
    void** rdi19;
    void** rsp20;
    void* rax21;
    void* v22;
    uint32_t eax23;
    int32_t eax24;
    int64_t r12_25;
    int32_t eax26;
    void* rsp27;
    uint32_t eax28;
    int32_t eax29;
    void* rax30;
    void** rsi31;
    void** rax32;
    void** rax33;
    void** esi34;
    void** rsi35;
    void** rax36;
    void** rax37;
    void** esi38;
    uint32_t r14d39;
    void** r13_40;
    int32_t ebp41;
    int32_t ebx42;
    int64_t rcx43;
    int64_t rdx44;
    int32_t eax45;
    void** rax46;
    int64_t v47;
    int64_t v48;

    eax7 = interrupt_signal;
    if (eax7 || (eax8 = synchronize_output(rdi, rsi, rdx, rcx, r8, r9), !eax8)) {
        eax9 = fun_2740();
        if (!eax9) {
            addr_59d0_3:
            eax10 = fun_2740();
            if (!eax10) {
                addr_5a48_4:
                return;
            } else {
                rax11 = fun_2570();
                rbx12 = rax11;
                eax13 = *reinterpret_cast<void***>(rax11);
                do {
                    if (eax13 != 4) 
                        break;
                    eax14 = fun_2740();
                } while (eax14 && (eax13 = *reinterpret_cast<void***>(rbx12), eax13 != 9));
                goto addr_5a48_4;
            }
        } else {
            rax15 = fun_2570();
            rbx16 = rax15;
            eax17 = *reinterpret_cast<void***>(rax15);
            do {
                if (eax17 != 4) 
                    goto addr_598f_10;
                eax18 = fun_2740();
            } while (eax18 && (eax17 = *reinterpret_cast<void***>(rbx16), eax17 != 9));
            goto addr_59d0_3;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rdi19) = eax8;
        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
        fun_2910();
        rsp20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 0x90);
        rax21 = g28;
        v22 = rax21;
        while ((eax23 = interrupt_signal, !!eax23) || (eax24 = info_signal_count, !!eax24)) {
            do {
                fun_2520();
                *reinterpret_cast<uint32_t*>(&r12_25) = interrupt_signal;
                eax26 = info_signal_count;
                if (eax26) {
                    info_signal_count = eax26 - 1;
                }
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                rsi = rsp20;
                *reinterpret_cast<uint32_t*>(&rdi19) = 2;
                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                fun_2520();
                rsp27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp20 - 8) + 8 - 8 + 8);
                if (*reinterpret_cast<uint32_t*>(&r12_25)) 
                    break;
                print_stats();
                rsp20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp27) - 8 + 8);
                eax28 = interrupt_signal;
            } while (eax28 || (eax29 = info_signal_count, !!eax29));
            break;
            cleanup(2, rsi, 0, rcx, r8, r9);
            print_stats();
            *reinterpret_cast<uint32_t*>(&rdi19) = *reinterpret_cast<uint32_t*>(&r12_25);
            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
            fun_2540();
            rsp20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp27) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        rax30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v22) - reinterpret_cast<uint64_t>(g28));
        if (rax30) 
            goto addr_5b2f_21; else 
            goto addr_5b06_22;
    }
    rsi31 = output_file;
    rax32 = quotearg_style(4, rsi31, 4, rsi31);
    rax33 = fun_2660();
    esi34 = *reinterpret_cast<void***>(rbx12);
    nl_error(1, esi34, rax33, rax32, r8, r9);
    goto addr_5a48_4;
    addr_598f_10:
    rsi35 = input_file;
    rax36 = quotearg_style(4, rsi35);
    rax37 = fun_2660();
    esi38 = *reinterpret_cast<void***>(rbx16);
    nl_error(1, esi38, rax37, rax36, r8, r9);
    goto addr_59d0_3;
    addr_5b2f_21:
    fun_2690();
    r14d39 = *reinterpret_cast<uint32_t*>(&rdi19);
    r13_40 = rsi;
    ebp41 = *reinterpret_cast<int32_t*>(&rdx);
    ebx42 = *reinterpret_cast<int32_t*>(&rcx);
    do {
        process_signals(rdi19, rsi, rdi19, rsi);
        *reinterpret_cast<int32_t*>(&rcx43) = ebx42;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx43) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx44) = ebp41;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx44) + 4) = 0;
        rsi = r13_40;
        *reinterpret_cast<uint32_t*>(&rdi19) = r14d39;
        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
        eax45 = fd_reopen(rdi19, rsi, rdx44, rcx43);
        if (eax45 >= 0) 
            break;
        rax46 = fun_2570();
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax46) == 4));
    goto v47;
    addr_5b06_22:
    goto v48;
}

int32_t fun_2970(void** rdi, void** rsi);

int32_t ifstat(uint32_t edi, void** rsi, ...) {
    void** rdi1;
    uint32_t ebp3;
    void** rbx4;
    int32_t eax5;
    void** rax6;

    *reinterpret_cast<uint32_t*>(&rdi1) = edi;
    ebp3 = *reinterpret_cast<uint32_t*>(&rdi1);
    rbx4 = rsi;
    do {
        process_signals(rdi1, rsi);
        rsi = rbx4;
        *reinterpret_cast<uint32_t*>(&rdi1) = ebp3;
        *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
        eax5 = fun_2970(rdi1, rsi);
        if (eax5 >= 0) 
            break;
        rax6 = fun_2570();
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 4));
    return eax5;
}

int32_t fun_2700(void** rdi, void** rsi);

/* iftruncate.constprop.0 */
int32_t iftruncate_constprop_0(void** rdi, ...) {
    void** rbx2;
    void** rsi3;
    int32_t eax4;
    void** rax5;

    rbx2 = rdi;
    do {
        process_signals(rdi, rsi3);
        rsi3 = rbx2;
        *reinterpret_cast<int32_t*>(&rdi) = 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        eax4 = fun_2700(1, rsi3);
        if (eax4 >= 0) 
            break;
        rax5 = fun_2570();
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax5) == 4));
    return eax4;
}

int32_t fun_2580(void** rdi);

int32_t fun_2890(void** rdi, void** rsi, void** rdx, void** rcx);

uint32_t synchronize_output(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r12d7;
    void** eax8;
    int32_t eax9;
    void** rax10;
    void** rbx11;
    void** eax12;
    uint32_t r12d13;
    void** rax14;
    void** rsi15;
    void** rax16;
    void** rax17;
    int32_t eax18;
    void** rax19;
    void** rbx20;
    void** rax21;
    void** rsi22;
    void** rax23;
    void** rax24;
    void** esi25;

    r12d7 = conversions_mask;
    eax8 = r12d7;
    *reinterpret_cast<unsigned char*>(&eax8 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax8 + 1) & 63);
    conversions_mask = eax8;
    if (!(reinterpret_cast<unsigned char>(r12d7) & 0x4000)) 
        goto addr_60c3_2;
    do {
        process_signals(rdi, rsi);
        *reinterpret_cast<uint32_t*>(&rdi) = 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        eax9 = fun_2580(1);
        if (eax9 >= 0) 
            break;
        rax10 = fun_2570();
        rbx11 = rax10;
        eax12 = *reinterpret_cast<void***>(rax10);
    } while (reinterpret_cast<int1_t>(eax12 == 4));
    goto addr_6110_5;
    if (!eax9) {
        addr_60c3_2:
        r12d13 = reinterpret_cast<unsigned char>(r12d7) & 0x8000;
        if (r12d13) 
            goto addr_6160_7; else 
            goto addr_60d0_8;
    } else {
        rax14 = fun_2570();
        rbx11 = rax14;
        eax12 = *reinterpret_cast<void***>(rax14);
    }
    addr_6110_5:
    if (!(reinterpret_cast<uint32_t>(eax12 - 22) & 0xffffffef)) {
        addr_6160_7:
        r12d13 = 0;
    } else {
        rsi15 = output_file;
        rax16 = quotearg_style(4, rsi15);
        rax17 = fun_2660();
        rsi = *reinterpret_cast<void***>(rbx11);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rcx = rax16;
        *reinterpret_cast<uint32_t*>(&rdi) = 0;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        rdx = rax17;
        r12d13 = 1;
        nl_error(0, rsi, rdx, rcx, r8, r9);
    }
    do {
        process_signals(rdi, rsi, rdi, rsi);
        *reinterpret_cast<uint32_t*>(&rdi) = 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        eax18 = fun_2890(1, rsi, rdx, rcx);
        if (eax18 >= 0) 
            break;
        rax19 = fun_2570();
        rbx20 = rax19;
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax19) == 4));
    goto addr_61a0_13;
    if (!eax18) {
        addr_60d0_8:
        return r12d13;
    } else {
        rax21 = fun_2570();
        rbx20 = rax21;
    }
    addr_61a0_13:
    rsi22 = output_file;
    rax23 = quotearg_style(4, rsi22, 4, rsi22);
    rax24 = fun_2660();
    esi25 = *reinterpret_cast<void***>(rbx20);
    r12d13 = 1;
    nl_error(0, esi25, rax24, rax23, r8, r9);
    goto addr_60d0_8;
}

/* quit.constprop.0 */
void quit_constprop_0(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** rsi7;
    void** rdi8;
    int64_t v9;
    int64_t rbx10;
    void** rax11;
    void** tmp64_12;
    int1_t zf13;
    void** rsi14;
    void** rax15;
    void** rax16;
    void** rax17;
    void** rsi18;
    void** tmp64_19;
    void** r12_20;
    void** rbp21;
    uint32_t eax22;
    int1_t zf23;
    int64_t rbx24;
    int64_t rdx25;
    void** tmp64_26;
    void** rcx27;
    void** rsi28;
    void** rdx29;
    int1_t less30;
    void** rdx31;
    void** rcx32;
    uint32_t esi33;
    void** rax34;
    int1_t less35;
    int1_t less_or_equal36;
    void** tmp64_37;
    int64_t rax38;

    process_signals(rdi, rsi);
    cleanup(rdi, rsi, rdx, rcx, r8, r9);
    print_stats();
    fun_2910();
    rsi7 = output_blocksize;
    rdi8 = obuf;
    v9 = rbx10;
    rax11 = iwrite_constprop_0(rdi8, rsi7, rdx, rcx, r8, r9);
    tmp64_12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(w_bytes) + reinterpret_cast<unsigned char>(rax11));
    w_bytes = tmp64_12;
    zf13 = output_blocksize == rax11;
    if (!zf13) {
        rsi14 = output_file;
        rax15 = quotearg_style(4, rsi14);
        rax16 = fun_2660();
        rax17 = fun_2570();
        rsi18 = *reinterpret_cast<void***>(rax17);
        *reinterpret_cast<int32_t*>(&rsi18 + 4) = 0;
        nl_error(0, rsi18, rax16, rax15, r8, r9);
        if (rax11) {
            tmp64_19 = w_partial + 1;
            w_partial = tmp64_19;
        }
        quit_constprop_0(0, rsi18, rax16, rax15, r8, r9);
        r12_20 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rsi18)));
        if (rsi18) {
            rbp21 = reinterpret_cast<void**>(0);
            while (1) {
                eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp21));
                zf23 = *reinterpret_cast<unsigned char*>(&eax22) == newline_character;
                rbx24 = col;
                rdx25 = conversion_blocksize;
                if (!zf23) {
                    if (rdx25 == rbx24) {
                        tmp64_26 = r_truncate + 1;
                        r_truncate = tmp64_26;
                    } else {
                        if (rdx25 > rbx24 && (rcx27 = oc, rsi28 = obuf, rdx29 = rcx27 + 1, *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi28) + reinterpret_cast<unsigned char>(rcx27)) = *reinterpret_cast<unsigned char*>(&eax22), less30 = reinterpret_cast<signed char>(rdx29) < reinterpret_cast<signed char>(output_blocksize), oc = rdx29, !less30)) {
                            write_output(0);
                            rbx24 = col;
                        }
                    }
                    ++rbp21;
                    col = rbx24 + 1;
                    if (rbp21 == r12_20) 
                        break;
                } else {
                    if (rdx25 > rbx24) {
                        do {
                            rdx31 = oc;
                            rcx32 = obuf;
                            esi33 = space_character;
                            rax34 = rdx31 + 1;
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx32) + reinterpret_cast<unsigned char>(rdx31)) = *reinterpret_cast<signed char*>(&esi33);
                            less35 = reinterpret_cast<signed char>(rax34) < reinterpret_cast<signed char>(output_blocksize);
                            oc = rax34;
                            if (!less35) {
                                write_output(0);
                            }
                            ++rbx24;
                            less_or_equal36 = conversion_blocksize <= rbx24;
                        } while (!less_or_equal36);
                    }
                    ++rbp21;
                    col = 0;
                    if (rbp21 == r12_20) 
                        break;
                }
            }
        }
        goto v9;
    } else {
        oc = reinterpret_cast<void**>(0);
        tmp64_37 = w_full + 1;
        w_full = tmp64_37;
        goto rax38;
    }
}

int64_t fun_2670();

int64_t fun_2560();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2670();
    if (r8d > 10) {
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xfb80 + rax11 * 4) + 0xfb80;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0xb0);

uint32_t nslots = 1;

void** xpalloc();

void** fun_2730(void** rdi);

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2550(void** rdi, ...);

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(int32_t edi, int64_t rsi, int64_t rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    void* rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rdi14;
    int64_t rax15;
    uint32_t r8d16;
    struct s3* rbx17;
    uint32_t r15d18;
    void** rsi19;
    void** r14_20;
    int64_t v21;
    int64_t v22;
    uint32_t r15d23;
    void** rax24;
    void** rsi25;
    void** rax26;
    uint32_t r8d27;
    int64_t v28;
    int64_t v29;
    void* rax30;

    rbx5 = edi;
    rax6 = g28;
    v7 = 0x95ff;
    rax8 = fun_2570();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
        fun_2560();
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x140b0) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0xa911]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            rdi14 = reinterpret_cast<int32_t>(nslots);
            v7 = 0x968b;
            fun_2730((rdi14 << 4) + reinterpret_cast<unsigned char>(r15_9));
            rax15 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax15);
        }
        r8d16 = rcx->f0;
        rbx17 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d18 = rcx->f4;
        rsi19 = rbx17->f0;
        r14_20 = rbx17->f8;
        v21 = rcx->f30;
        v22 = rcx->f28;
        r15d23 = r15d18 | 1;
        rax24 = quotearg_buffer_restyled(r14_20, rsi19, rsi, rdx, r8d16, r15d23, &rcx->f8, v22, v21, v7);
        if (reinterpret_cast<unsigned char>(rsi19) <= reinterpret_cast<unsigned char>(rax24)) {
            rsi25 = rax24 + 1;
            rbx17->f0 = rsi25;
            if (r14_20 != 0x14420) {
                fun_2550(r14_20, r14_20);
                rsi25 = rsi25;
            }
            rax26 = xcharalloc(rsi25, rsi25);
            r8d27 = rcx->f0;
            rbx17->f8 = rax26;
            v28 = rcx->f30;
            r14_20 = rax26;
            v29 = rcx->f28;
            quotearg_buffer_restyled(rax26, rsi25, rsi, rdx, r8d27, r15d23, rsi25, v29, v28, 0x971a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax6) - reinterpret_cast<uint64_t>(g28));
        if (rax30) {
            fun_2690();
        } else {
            return r14_20;
        }
    }
}

signed char translation_needed = 0;

void translate_charset(int64_t rdi) {
    unsigned char* rax2;
    int64_t rdx3;
    uint32_t edx4;

    rax2 = reinterpret_cast<unsigned char*>(0x14160);
    do {
        *reinterpret_cast<uint32_t*>(&rdx3) = *rax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
        ++rax2;
        edx4 = *reinterpret_cast<unsigned char*>(rdi + rdx3);
        *reinterpret_cast<signed char*>(rax2 - 1) = *reinterpret_cast<signed char*>(&edx4);
    } while (!reinterpret_cast<int1_t>(rax2 == 0x14260));
    translation_needed = 1;
    return;
}

void set_fd_flags(void** rdi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    void* rax7;
    void* v8;
    uint32_t ebx9;
    uint32_t ebp10;
    void** r12_11;
    uint32_t eax12;
    void** r13_13;
    uint32_t ebx14;
    void** rax15;
    void** rax16;
    void** rax17;
    void** esi18;
    void** rdx19;
    void** rdi20;
    uint32_t eax21;
    void* rax22;
    int32_t eax23;
    uint32_t v24;
    void** rax25;

    rax7 = g28;
    v8 = rax7;
    ebx9 = reinterpret_cast<unsigned char>(esi) & 0xfffdfeff;
    if (!ebx9) 
        goto addr_5bfa_2;
    ebp10 = *reinterpret_cast<uint32_t*>(&rdi);
    r12_11 = rdx;
    eax12 = rpl_fcntl(rdi, 3, rdx, rcx);
    *reinterpret_cast<uint32_t*>(&r13_13) = eax12;
    ebx14 = ebx9 | eax12;
    if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0)) {
        while (1) {
            addr_5c83_4:
            rax15 = quotearg_style(4, r12_11, 4, r12_11);
            r13_13 = rax15;
            rax16 = fun_2660();
            r12_11 = rax16;
            rax17 = fun_2570();
            rcx = r13_13;
            esi18 = *reinterpret_cast<void***>(rax17);
            nl_error(1, esi18, r12_11, rcx, r8, r9);
            addr_5cc2_5:
            ebx14 = ebx14 & 0xfffeffff;
            if (ebx14 == *reinterpret_cast<uint32_t*>(&r13_13)) 
                goto addr_5bfa_2;
            addr_5c43_7:
            *reinterpret_cast<uint32_t*>(&rdx19) = ebx14;
            *reinterpret_cast<int32_t*>(&rdx19 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi20) = ebp10;
            *reinterpret_cast<int32_t*>(&rdi20 + 4) = 0;
            eax21 = rpl_fcntl(rdi20, 4, rdx19, rcx);
            if (eax21 + 1) 
                goto addr_5bfa_2;
        }
    } else {
        if (eax12 == ebx14) {
            addr_5bfa_2:
            rax22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
            if (rax22) {
                fun_2690();
            } else {
                return;
            }
        } else {
            if (!(ebx14 & reinterpret_cast<uint32_t>("ftware: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n"))) 
                goto addr_5c43_7;
            eax23 = ifstat(ebp10, reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0xa8) - 8 + 8);
            if (eax23) 
                goto addr_5c83_4;
        }
    }
    if ((v24 & reinterpret_cast<uint32_t>("yte) blocks")) == 0x4000) 
        goto addr_5cc2_5;
    rax25 = fun_2570();
    *reinterpret_cast<void***>(rax25) = reinterpret_cast<void**>(20);
    goto addr_5c83_4;
}

void** input_blocksize = reinterpret_cast<void**>(0);

/* alloc_ibuf.part.0 */
void alloc_ibuf_part_0(void** rdi, ...) {
    void** esi2;
    void** rbp3;
    void* rax4;
    void** rdi5;
    void* rsi6;
    void** rdx7;
    void** rcx8;
    void** r8_9;
    void** rax10;
    void*** rsp11;
    void** r9_12;
    void** rax13;
    void** r12_14;
    void** rax15;
    void** r8_16;
    void** rcx17;
    void** rdx18;
    void** r9_19;
    void* rax20;
    int64_t* rsp21;
    void** rdi22;
    void* rax23;
    void** rax24;
    void*** rsp25;
    void** r9_26;
    void** rax27;
    void** rax28;
    void** r9_29;
    void* rax30;
    int1_t zf31;
    int1_t zf32;
    int64_t v33;
    int1_t zf34;
    void** rax35;
    int64_t v36;
    void** rax37;
    int64_t v38;
    int64_t v39;

    esi2 = conversions_mask;
    rbp3 = input_blocksize;
    rax4 = g28;
    rdi5 = page_size;
    *reinterpret_cast<uint32_t*>(&rsi6) = reinterpret_cast<unsigned char>(esi2) >> 7 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi6) + 4) = 0;
    rax10 = fun_2940(rdi5, reinterpret_cast<int64_t>(rsi6) + reinterpret_cast<unsigned char>(rbp3), rdx7, rcx8, r8_9);
    rsp11 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x2a8 - 8 + 8);
    ibuf = rax10;
    if (!rax10) {
        rax13 = human_readable(rbp3, rsp11, 0x1f1, 1, 1, r9_12);
        r12_14 = input_blocksize;
        rax15 = fun_2660();
        r8_16 = rax13;
        rcx17 = r12_14;
        rdx18 = rax15;
        nl_error(1, 0, rdx18, rcx17, r8_16, r9_19);
        rsp11 = rsp11 - 8 + 8 - 8 + 8 - 8 + 8;
    } else {
        rax20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
        if (!rax20) {
            return;
        }
    }
    fun_2690();
    rsp21 = reinterpret_cast<int64_t*>(rsp11 - 8 + 8);
    while (1) {
        rbp3 = output_blocksize;
        rdi22 = page_size;
        rax23 = g28;
        rax24 = fun_2940(rdi22, rbp3, rdx18, rcx17, r8_16);
        rsp25 = reinterpret_cast<void***>(rsp21 - 1 - 1 - 85 - 1 + 1);
        obuf = rax24;
        if (!rax24) {
            rax27 = human_readable(rbp3, rsp25, 0x1f1, 1, 1, r9_26);
            r12_14 = output_blocksize;
            rax28 = fun_2660();
            r8_16 = rax27;
            rcx17 = r12_14;
            rdx18 = rax28;
            *reinterpret_cast<uint32_t*>(&rdi22) = 1;
            *reinterpret_cast<int32_t*>(&rdi22 + 4) = 0;
            nl_error(1, 0, rdx18, rcx17, r8_16, r9_29);
            rsp25 = rsp25 - 8 + 8 - 8 + 8 - 8 + 8;
        } else {
            rax30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax23) - reinterpret_cast<uint64_t>(g28));
            if (!rax30) 
                goto addr_4e32_9;
        }
        fun_2690();
        rsp21 = reinterpret_cast<int64_t*>(rsp25 - 8 + 8);
        zf31 = obuf == 0;
        if (!zf31) 
            break;
        zf32 = (*reinterpret_cast<unsigned char*>(&conversions_mask + 1) & 8) == 0;
        if (zf32) 
            goto addr_4eb4_13;
    }
    goto v33;
    addr_4eb4_13:
    zf34 = ibuf == 0;
    if (zf34) {
        alloc_ibuf_part_0(rdi22, rdi22);
        rax35 = ibuf;
        obuf = rax35;
        goto v36;
    } else {
        rax37 = ibuf;
        obuf = rax37;
        goto v38;
    }
    addr_4e32_9:
    goto v39;
}

void copy_with_block(void** rdi, void** rsi, void** rdx, ...) {
    void** r12_4;
    void** rbp5;
    uint32_t eax6;
    int1_t zf7;
    int64_t rbx8;
    int64_t rdx9;
    void** tmp64_10;
    void** rcx11;
    void** rsi12;
    void** rdx13;
    int1_t less14;
    void** rdx15;
    void** rcx16;
    uint32_t esi17;
    void** rax18;
    int1_t less19;
    int1_t less_or_equal20;

    r12_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
    if (rsi) {
        rbp5 = rdi;
        while (1) {
            eax6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5));
            zf7 = *reinterpret_cast<unsigned char*>(&eax6) == newline_character;
            rbx8 = col;
            rdx9 = conversion_blocksize;
            if (!zf7) {
                if (rdx9 == rbx8) {
                    tmp64_10 = r_truncate + 1;
                    r_truncate = tmp64_10;
                } else {
                    if (rdx9 > rbx8 && (rcx11 = oc, rsi12 = obuf, rdx13 = rcx11 + 1, *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi12) + reinterpret_cast<unsigned char>(rcx11)) = *reinterpret_cast<unsigned char*>(&eax6), less14 = reinterpret_cast<signed char>(rdx13) < reinterpret_cast<signed char>(output_blocksize), oc = rdx13, !less14)) {
                        write_output(rdi);
                        rbx8 = col;
                    }
                }
                ++rbp5;
                col = rbx8 + 1;
                if (rbp5 == r12_4) 
                    break;
            } else {
                if (rdx9 > rbx8) {
                    do {
                        rdx15 = oc;
                        rcx16 = obuf;
                        esi17 = space_character;
                        rax18 = rdx15 + 1;
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx16) + reinterpret_cast<unsigned char>(rdx15)) = *reinterpret_cast<signed char*>(&esi17);
                        less19 = reinterpret_cast<signed char>(rax18) < reinterpret_cast<signed char>(output_blocksize);
                        oc = rax18;
                        if (!less19) {
                            write_output(rdi);
                        }
                        ++rbx8;
                        less_or_equal20 = conversion_blocksize <= rbx8;
                    } while (!less_or_equal20);
                }
                ++rbp5;
                col = 0;
                if (rbp5 == r12_4) 
                    break;
            }
        }
    }
    return;
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x140c8;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xfb23);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xfb1c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xfb27);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xfb18);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g13d58 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g13d58;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2453() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2463() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2473() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2483() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2493() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_24e3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_24f3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __ctype_toupper_loc = 0x2030;

void fun_2503() {
    __asm__("cli ");
    goto __ctype_toupper_loc;
}

int64_t getenv = 0x2040;

void fun_2513() {
    __asm__("cli ");
    goto getenv;
}

int64_t sigprocmask = 0x2050;

void fun_2523() {
    __asm__("cli ");
    goto sigprocmask;
}

int64_t __snprintf_chk = 0x2060;

void fun_2533() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t raise = 0x2070;

void fun_2543() {
    __asm__("cli ");
    goto raise;
}

int64_t free = 0x2080;

void fun_2553() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2090;

void fun_2563() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x20a0;

void fun_2573() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t fdatasync = 0x20b0;

void fun_2583() {
    __asm__("cli ");
    goto fdatasync;
}

int64_t error_at_line = 0x20c0;

void fun_2593() {
    __asm__("cli ");
    goto error_at_line;
}

int64_t strncmp = 0x20d0;

void fun_25a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x20e0;

void fun_25b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x20f0;

void fun_25c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t sigaction = 0x2100;

void fun_25d3() {
    __asm__("cli ");
    goto sigaction;
}

int64_t reallocarray = 0x2110;

void fun_25e3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t localeconv = 0x2120;

void fun_25f3() {
    __asm__("cli ");
    goto localeconv;
}

int64_t fcntl = 0x2130;

void fun_2603() {
    __asm__("cli ");
    goto fcntl;
}

int64_t clock_gettime = 0x2140;

void fun_2613() {
    __asm__("cli ");
    goto clock_gettime;
}

int64_t write = 0x2150;

void fun_2623() {
    __asm__("cli ");
    goto write;
}

int64_t textdomain = 0x2160;

void fun_2633() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2170;

void fun_2643() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2180;

void fun_2653() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2190;

void fun_2663() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x21a0;

void fun_2673() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x21b0;

void fun_2683() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x21c0;

void fun_2693() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x21d0;

void fun_26a3() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x21e0;

void fun_26b3() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t dup2 = 0x21f0;

void fun_26c3() {
    __asm__("cli ");
    goto dup2;
}

int64_t strchr = 0x2200;

void fun_26d3() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x2210;

void fun_26e3() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2220;

void fun_26f3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t ftruncate = 0x2230;

void fun_2703() {
    __asm__("cli ");
    goto ftruncate;
}

int64_t lseek = 0x2240;

void fun_2713() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2250;

void fun_2723() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x2260;

void fun_2733() {
    __asm__("cli ");
    goto memset;
}

int64_t close = 0x2270;

void fun_2743() {
    __asm__("cli ");
    goto close;
}

int64_t posix_fadvise = 0x2280;

void fun_2753() {
    __asm__("cli ");
    goto posix_fadvise;
}

int64_t read = 0x2290;

void fun_2763() {
    __asm__("cli ");
    goto read;
}

int64_t memcmp = 0x22a0;

void fun_2773() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x22b0;

void fun_2783() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x22c0;

void fun_2793() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x22d0;

void fun_27a3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x22e0;

void fun_27b3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t __memcpy_chk = 0x22f0;

void fun_27c3() {
    __asm__("cli ");
    goto __memcpy_chk;
}

int64_t sigemptyset = 0x2300;

void fun_27d3() {
    __asm__("cli ");
    goto sigemptyset;
}

int64_t memcpy = 0x2310;

void fun_27e3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2320;

void fun_27f3() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2330;

void fun_2803() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2340;

void fun_2813() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2350;

void fun_2823() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2360;

void fun_2833() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2370;

void fun_2843() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2380;

void fun_2853() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2390;

void fun_2863() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t memmove = 0x23a0;

void fun_2873() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x23b0;

void fun_2883() {
    __asm__("cli ");
    goto error;
}

int64_t fsync = 0x23c0;

void fun_2893() {
    __asm__("cli ");
    goto fsync;
}

int64_t open = 0x23d0;

void fun_28a3() {
    __asm__("cli ");
    goto open;
}

int64_t fseeko = 0x23e0;

void fun_28b3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t dcngettext = 0x23f0;

void fun_28c3() {
    __asm__("cli ");
    goto dcngettext;
}

int64_t strtoumax = 0x2400;

void fun_28d3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2410;

void fun_28e3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t getpagesize = 0x2420;

void fun_28f3() {
    __asm__("cli ");
    goto getpagesize;
}

int64_t sigismember = 0x2430;

void fun_2903() {
    __asm__("cli ");
    goto sigismember;
}

int64_t exit = 0x2440;

void fun_2913() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2450;

void fun_2923() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2460;

void fun_2933() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t aligned_alloc = 0x2470;

void fun_2943() {
    __asm__("cli ");
    goto aligned_alloc;
}

int64_t mbsinit = 0x2480;

void fun_2953() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2490;

void fun_2963() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x24a0;

void fun_2973() {
    __asm__("cli ");
    goto fstat;
}

int64_t sigaddset = 0x24b0;

void fun_2983() {
    __asm__("cli ");
    goto sigaddset;
}

int64_t __ctype_tolower_loc = 0x24c0;

void fun_2993() {
    __asm__("cli ");
    goto __ctype_tolower_loc;
}

int64_t __ctype_b_loc = 0x24d0;

void fun_29a3() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x24e0;

void fun_29b3() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void** fun_2510(int64_t rdi, void** rsi);

void fun_27d0(int64_t rdi);

void fun_2980(int64_t rdi, int64_t rsi, void** rdx);

void fun_25d0(int64_t rdi, ...);

int32_t fun_2900(int64_t rdi, void** rsi, void** rdx);

void set_program_name(void** rdi, void** rsi, void** rdx);

void** fun_2850(int64_t rdi, ...);

void fun_2650(int64_t rdi, int64_t rsi, void** rdx);

void fun_2630(int64_t rdi, int64_t rsi, void** rdx);

void atexit(int64_t rdi, int64_t rsi, void** rdx);

int32_t fun_28f0(int64_t rdi, int64_t rsi, void** rdx);

void** Version = reinterpret_cast<void**>(56);

void parse_gnu_standard_options_only(int64_t rdi, void** rsi, void** rdx, void** rcx);

signed char close_stdout_required = 1;

struct s5 {
    signed char[82272] pad82272;
    signed char f14160;
};

void** optind = reinterpret_cast<void**>(0);

void** input_flags = reinterpret_cast<void**>(0);

void** quote(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** skip_records = reinterpret_cast<void**>(0);

void* skip_bytes = reinterpret_cast<void*>(0);

void** max_records = reinterpret_cast<void**>(0xff);

void** max_bytes = reinterpret_cast<void**>(0);

void** seek_records = reinterpret_cast<void**>(0);

void** seek_bytes = reinterpret_cast<void**>(0);

int64_t iread(int64_t rdi, int64_t rsi, int64_t rdx);

signed char warn_partial_read = 0;

unsigned char i_nocache = 0;

int32_t** fun_2990(int64_t rdi);

void** input_seek_errno = reinterpret_cast<void**>(0);

int32_t** fun_2500(int64_t rdi);

int64_t xstrtoumax(void** rdi, void** rsi, ...);

struct s6 {
    unsigned char f0;
    void** f1;
    void** f2;
};

int32_t fun_25a0(void** rdi, void** rsi, void** rdx, ...);

void** quote_n();

void** next_time = reinterpret_cast<void**>(0);

void** fun_27e0(void** rdi, void** rsi, void** rdx, ...);

void fun_2a13(void** edi, void** rsi, void** rdx) {
    void** r12_4;
    void** rbp5;
    void** rax6;
    void* rsp7;
    void** rax8;
    void** rdx9;
    void** v10;
    void* rsp11;
    int64_t v12;
    int32_t eax13;
    void* rsp14;
    void** rsi15;
    int32_t eax16;
    void* rsp17;
    void** rdi18;
    void** r13_19;
    void** rbx20;
    int32_t eax21;
    void** rcx22;
    void** r8_23;
    int64_t rdi24;
    void** r9_25;
    void** rdx26;
    uint32_t* rsp27;
    struct s5* rax28;
    void** eax29;
    uint32_t v30;
    unsigned char v31;
    uint32_t v32;
    void** v33;
    void** v34;
    void** v35;
    int1_t zf36;
    int1_t zf37;
    int1_t zf38;
    void** eax39;
    void** rsi40;
    void** rax41;
    void** rax42;
    void** rsi43;
    void** ecx44;
    void* rdx45;
    void** rax46;
    uint32_t edi47;
    void** rcx48;
    void** rdx49;
    void** rax50;
    void** rdx51;
    void** rax52;
    uint32_t edi53;
    uint32_t edx54;
    int64_t rax55;
    int1_t zf56;
    void** rax57;
    int64_t rax58;
    void** rax59;
    void** rax60;
    uint32_t* rsp61;
    void** esi62;
    void** v63;
    void** rax64;
    int64_t rax65;
    void** rax66;
    int64_t rax67;
    void** rax68;
    int64_t rax69;
    void** rax70;
    void** rsi71;
    void** eax72;
    void*** rsp73;
    int64_t rax74;
    int64_t rdi75;
    uint32_t edx76;
    void** rax77;
    void* rsp78;
    void** rax79;
    uint64_t rax80;
    void** rax81;
    uint64_t rax82;
    void** tmp64_83;
    void** rdi84;
    void** rdx85;
    int32_t eax86;
    int32_t eax87;
    void** rsi88;
    void** rax89;
    void** rax90;
    int32_t** rax91;
    int32_t** rsi92;
    unsigned char* rax93;
    int32_t edx94;
    void** r12_95;
    void** rax96;
    void*** rsp97;
    void** rax98;
    int32_t eax99;
    void** rsi100;
    void** rax101;
    void** rax102;
    void** esi103;
    void** rax104;
    uint64_t rdx105;
    uint32_t edx106;
    void** r14_107;
    int32_t** rax108;
    int32_t** rsi109;
    unsigned char* rax110;
    int32_t edx111;
    void* rax112;
    void** r15_113;
    struct s1* rax114;
    void** rdi115;
    void** rax116;
    uint32_t eax117;
    void** rax118;
    uint32_t eax119;
    void** rax120;
    uint32_t ecx121;
    unsigned char* rdx122;
    uint32_t eax123;
    uint32_t eax124;
    void** rax125;
    uint32_t ecx126;
    unsigned char* rdx127;
    uint32_t eax128;
    uint32_t eax129;
    void** rax130;
    uint32_t ecx131;
    unsigned char* rdx132;
    uint32_t eax133;
    uint32_t eax134;
    void** rax135;
    uint32_t ecx136;
    unsigned char* rdx137;
    uint32_t eax138;
    uint32_t eax139;
    void** r14_140;
    int64_t rax141;
    uint32_t* rsp142;
    struct s6* rax143;
    struct s6* v144;
    uint32_t edx145;
    void** rdi146;
    struct s6* rdx147;
    struct s1* rax148;
    struct s1* rax149;
    uint32_t eax150;
    struct s1* r10_151;
    uint32_t eax152;
    uint32_t eax153;
    void*** rax154;
    struct s1* rax155;
    uint32_t eax156;
    uint32_t eax157;
    uint32_t eax158;
    uint32_t eax159;
    uint32_t eax160;
    uint32_t eax161;
    void* rdi162;
    uint32_t eax163;
    uint32_t eax164;
    void** rax165;
    void** rax166;
    int64_t rax167;
    uint32_t r10d168;
    uint32_t r10d169;
    int64_t rdx170;
    int1_t zf171;
    int32_t eax172;
    void** rax173;
    void** rax174;
    void** rax175;
    void** rax176;
    void*** rsp177;
    int1_t zf178;
    int1_t zf179;
    int32_t eax180;
    void** r12d181;
    int32_t eax182;
    void** rsi183;
    void** rax184;
    void** rax185;
    uint32_t v186;
    void** rsi187;
    void** rax188;
    void** rax189;
    void** rax190;
    void** rdx191;
    void* r14_192;
    void** rsi193;
    void** r12_194;
    void** rax195;
    int1_t zf196;
    void** rdx197;
    void** rax198;
    void** rsi199;
    void** rax200;
    int1_t zf201;
    void** rax202;
    void** rax203;
    void* tmp64_204;
    void** v205;
    void** rax206;
    void** r12_207;
    void** rdi208;
    void** rbp209;
    void** rax210;
    void** r12_211;
    void** rax212;
    void** rsi213;
    void** rax214;
    void** rax215;
    void** rsi216;
    void** rax217;
    uint64_t rax218;
    uint32_t eax219;
    void** rdx220;
    int1_t zf221;
    void** r12_222;
    int1_t zf223;
    void** rax224;
    int1_t less225;
    void** tmp64_226;
    void** r14_227;
    void** r13_228;
    void** rbp229;
    void** tmp64_230;
    void** eax231;
    void** rax232;
    int64_t rax233;
    void** rax234;
    uint1_t zf235;
    void** rax236;
    void** rax237;
    int1_t zf238;
    void** r15_239;
    void** rax240;
    void** tmp64_241;
    void** eax242;
    void** tmp64_243;
    int1_t zf244;
    void** rsi245;
    void* rcx246;
    uint32_t ecx247;
    uint32_t edi248;
    uint32_t esi249;
    uint32_t edx250;
    uint32_t esi251;
    uint32_t esi252;
    int1_t zf253;
    void** rsi254;
    void** rax255;
    void** rax256;
    void** esi257;
    void** rbp258;
    void** rbp259;
    int1_t zf260;
    void** eax261;
    void** rax262;
    void** rax263;
    void** rsi264;
    void** rax265;
    void** rax266;
    void** rax267;
    void** rax268;
    void** rax269;
    int1_t zf270;
    void** r13_271;
    int1_t zf272;
    void** rax273;
    void** rax274;
    void** rax275;
    int1_t zf276;
    void** rax277;
    void** tmp64_278;
    int1_t zf279;
    void** tmp64_280;
    void** tmp64_281;
    void** rdi282;
    void** r14_283;
    void** rbp284;
    void** rbp285;
    void** r14_286;
    int1_t less287;
    uint32_t v288;
    void** rax289;
    uint64_t rax290;
    int1_t zf291;
    int1_t zf292;
    int1_t zf293;
    signed char al294;
    void** rax295;
    void** rax296;
    int1_t zf297;
    signed char al298;
    void** rax299;
    void** rax300;
    void** rsi301;
    uint32_t ebx302;
    int1_t less303;
    int64_t rbx304;
    int1_t zf305;
    int1_t less_or_equal306;
    uint32_t esi307;
    void** rax308;
    int1_t less309;
    int1_t less310;
    void** rax311;
    int1_t less312;
    void** rax313;
    void** tmp64_314;
    void** tmp64_315;
    int1_t zf316;
    int1_t zf317;
    void** rsi318;
    void** rax319;
    void** rax320;
    int32_t eax321;
    void** rsi322;
    void** rax323;
    void** rax324;
    uint32_t v325;
    void** rax326;
    void** v327;
    int32_t eax328;
    int1_t zf329;
    int1_t zf330;
    void** rax331;
    int1_t less332;
    void** rsi333;
    void** rax334;
    void** rax335;
    uint32_t edx336;
    int1_t zf337;
    uint32_t edx338;
    uint32_t ebx339;
    void** rax340;
    void** rax341;
    void** rax342;
    void** rax343;
    void** rax344;

    __asm__("cli ");
    r12_4 = rsi;
    rbp5 = edi;
    *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
    rax6 = fun_2510("POSIXLY_CORRECT", rsi);
    fun_27d0(0x14280);
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x108 - 8 + 8 - 8 + 8);
    if (!rax6) {
        fun_2980(0x14280, 10, rdx);
        rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
    }
    rax8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) + 96);
    rdx9 = rax8;
    v10 = rax8;
    fun_25d0(2, 2);
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
    if (v12 != 1) {
        fun_2980(0x14280, 2, rdx9);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
    }
    __asm__("movdqa xmm0, [rip+0x117e6]");
    __asm__("movdqa xmm1, [rip+0x117ee]");
    __asm__("movdqa xmm2, [rip+0x117ee]");
    __asm__("movdqa xmm3, [rip+0x117f6]");
    __asm__("movdqa xmm4, [rip+0x117fe]");
    __asm__("movdqa xmm5, [rip+0x11806]");
    __asm__("movups [rsp+0x68], xmm0");
    __asm__("movdqa xmm6, [rip+0x11809]");
    __asm__("movdqa xmm7, [rip+0x11811]");
    __asm__("movups [rsp+0x78], xmm1");
    __asm__("movups [rsp+0x88], xmm2");
    __asm__("movups [rsp+0x98], xmm3");
    __asm__("movups [rsp+0xa8], xmm4");
    __asm__("movups [rsp+0xb8], xmm5");
    __asm__("movups [rsp+0xc8], xmm6");
    __asm__("movups [rsp+0xd8], xmm7");
    eax13 = fun_2900(0x14280, 10, rdx9);
    rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
    if (eax13) {
        *reinterpret_cast<int32_t*>(&rdx9) = 0;
        *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
        fun_25d0(10, 10);
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    }
    *reinterpret_cast<int32_t*>(&rsi15) = 2;
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    eax16 = fun_2900(0x14280, 2, rdx9);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
    if (eax16) {
        rsi15 = v10;
        *reinterpret_cast<int32_t*>(&rdx9) = 0;
        *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
        fun_25d0(2, 2);
        rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
    }
    rdi18 = *reinterpret_cast<void***>(r12_4);
    r13_19 = reinterpret_cast<void**>("coreutils");
    rbx20 = reinterpret_cast<void**>(0x14160);
    set_program_name(rdi18, rsi15, rdx9);
    fun_2850(6, 6);
    fun_2650("coreutils", "/usr/local/share/locale", rdx9);
    fun_2630("coreutils", "/usr/local/share/locale", rdx9);
    atexit(0x4ce0, "/usr/local/share/locale", rdx9);
    eax21 = fun_28f0(0x4ce0, "/usr/local/share/locale", rdx9);
    rcx22 = reinterpret_cast<void**>("coreutils");
    r8_23 = Version;
    *reinterpret_cast<void***>(&rdi24) = rbp5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    page_size = reinterpret_cast<void**>(static_cast<int64_t>(eax21));
    *reinterpret_cast<int32_t*>(&r9_25) = 1;
    *reinterpret_cast<int32_t*>(&r9_25 + 4) = 0;
    rdx26 = reinterpret_cast<void**>("dd");
    parse_gnu_standard_options_only(rdi24, r12_4, "dd", "coreutils");
    close_stdout_required = 0;
    rsp27 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 8 + 8 + 48);
    *reinterpret_cast<int32_t*>(&rax28) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
    do {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(0x14160) + reinterpret_cast<uint64_t>(rax28)) = *reinterpret_cast<signed char*>(&rax28);
        rax28 = reinterpret_cast<struct s5*>(reinterpret_cast<uint64_t>(rax28) + 1);
    } while (!reinterpret_cast<int1_t>(rax28 == 0x100));
    eax29 = optind;
    if (reinterpret_cast<signed char>(rbp5) <= reinterpret_cast<signed char>(eax29)) {
        while (1) {
            *reinterpret_cast<unsigned char*>(&v30) = 0;
            v31 = 0;
            *reinterpret_cast<unsigned char*>(&v32) = 0;
            v33 = reinterpret_cast<void**>(0x7fffffffffffffff);
            v34 = reinterpret_cast<void**>(0);
            v35 = reinterpret_cast<void**>(0);
            addr_317e_13:
            conversions_mask = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(conversions_mask) | 0x800);
            zf36 = input_blocksize == 0;
            if (zf36) {
                input_blocksize = reinterpret_cast<void**>(0x200);
            }
            zf37 = output_blocksize == 0;
            if (zf37) {
                output_blocksize = reinterpret_cast<void**>(0x200);
            }
            addr_2cdb_17:
            zf38 = conversion_blocksize == 0;
            if (zf38) {
                conversions_mask = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(conversions_mask) & 0xffffffe7);
            }
            eax39 = input_flags;
            if (reinterpret_cast<unsigned char>(eax39) & 0x101000) {
                input_flags = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(eax39) | 0x101000);
            }
            rsi40 = output_flags;
            *reinterpret_cast<int32_t*>(&rsi40 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rsi40) & 1) {
                addr_4810_22:
                rax41 = quote("fullblock", rsi40, rdx26, rcx22, r8_23, r9_25);
                rax42 = fun_2660();
                r8_23 = rax41;
                rsi43 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rsi43 + 4) = 0;
                rcx22 = rax42;
                rdx26 = reinterpret_cast<void**>("%s: %s");
                nl_error(0, 0, "%s: %s", rcx22, r8_23, r9_25);
                usage(1);
                rsp27 = rsp27 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2;
            } else {
                if (*reinterpret_cast<unsigned char*>(&v32)) {
                    input_flags = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(input_flags) | 8);
                }
                ecx44 = input_flags;
                if (!(*reinterpret_cast<unsigned char*>(&ecx44) & 8)) {
                    if (v35) {
                        skip_records = v35;
                    }
                } else {
                    if (v35) {
                        rdx45 = reinterpret_cast<void*>(reinterpret_cast<signed char>(v35) % reinterpret_cast<signed char>(input_blocksize));
                        rax46 = reinterpret_cast<void**>(reinterpret_cast<signed char>(v35) / reinterpret_cast<signed char>(input_blocksize));
                        skip_records = rax46;
                        skip_bytes = rdx45;
                    }
                }
                edi47 = *reinterpret_cast<unsigned char*>(&v30);
                rcx48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ecx44) | 4);
                *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                if (!*reinterpret_cast<signed char*>(&edi47)) {
                    rcx48 = ecx44;
                    *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                }
                if (!(*reinterpret_cast<unsigned char*>(&rcx48) & 4)) {
                    if (v33 != 0x7fffffffffffffff) {
                        max_records = v33;
                    }
                } else {
                    if (v33 != 0x7fffffffffffffff) {
                        rdx49 = reinterpret_cast<void**>(reinterpret_cast<signed char>(v33) % reinterpret_cast<signed char>(input_blocksize));
                        rax50 = reinterpret_cast<void**>(reinterpret_cast<signed char>(v33) / reinterpret_cast<signed char>(input_blocksize));
                        max_records = rax50;
                        max_bytes = rdx49;
                    }
                }
                if (v31) {
                    rsi40 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi40) | 16);
                    output_flags = rsi40;
                }
                if (!(*reinterpret_cast<unsigned char*>(&rsi40) & 16)) {
                    if (v34) {
                        seek_records = v34;
                    }
                } else {
                    if (v34) {
                        rdx51 = reinterpret_cast<void**>(reinterpret_cast<signed char>(v34) % reinterpret_cast<signed char>(output_blocksize));
                        rax52 = reinterpret_cast<void**>(reinterpret_cast<signed char>(v34) / reinterpret_cast<signed char>(output_blocksize));
                        seek_records = rax52;
                        seek_bytes = rdx51;
                    }
                }
                rbp5 = conversions_mask;
                *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
                edi53 = reinterpret_cast<unsigned char>(rcx48) & 1;
                if (reinterpret_cast<unsigned char>(rbp5) & 0x800 | edi53) {
                    edx54 = 0;
                    rax55 = 0x5e00;
                    if (!edi53) {
                        rax55 = reinterpret_cast<int64_t>(iread);
                    }
                } else {
                    zf56 = skip_records == 0;
                    if (!zf56 || (rax57 = max_records, reinterpret_cast<uint64_t>(rax57 - 1) <= 0x7ffffffffffffffd)) {
                        edx54 = 1;
                        rax55 = reinterpret_cast<int64_t>(iread);
                    } else {
                        rax55 = reinterpret_cast<int64_t>(iread);
                        edx54 = (reinterpret_cast<unsigned char>(rcx48) | reinterpret_cast<unsigned char>(rsi40)) >> 14 & 1;
                    }
                }
                iread_fnc = rax55;
                warn_partial_read = *reinterpret_cast<signed char*>(&edx54);
                input_flags = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx48) & 0xfffffffe);
                *reinterpret_cast<uint32_t*>(&rax58) = reinterpret_cast<unsigned char>(rbp5) & 7;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax58) + 4) = 0;
                if (static_cast<uint32_t>(rax58 - 1) & *reinterpret_cast<uint32_t*>(&rax58)) 
                    goto addr_464c_51; else 
                    goto addr_31f9_52;
            }
            addr_4854_53:
            rax59 = quote(rbp5, rsi43, rdx26, rcx22, r8_23, r9_25);
            rbx20 = rax59;
            rax60 = fun_2660();
            rsp61 = rsp27 - 2 + 2 - 2 + 2;
            esi62 = reinterpret_cast<void**>(75);
            rcx22 = rax60;
            if (*reinterpret_cast<uint32_t*>(&v63) != 1) {
                esi62 = reinterpret_cast<void**>(0);
            }
            addr_3138_56:
            r8_23 = rbx20;
            rdx26 = reinterpret_cast<void**>("%s: %s");
            nl_error(1, esi62, "%s: %s", rcx22, r8_23, r9_25);
            rsp27 = rsp61 - 2 + 2;
            continue;
            addr_464c_51:
            rax64 = fun_2660();
            nl_error(1, 0, rax64, rcx48, r8_23, r9_25);
            rsp27 = rsp27 - 2 + 2 - 2 + 2;
            goto addr_4670_57;
            addr_31f9_52:
            *reinterpret_cast<uint32_t*>(&rax65) = reinterpret_cast<unsigned char>(rbp5) & 24;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax65) + 4) = 0;
            if (static_cast<uint32_t>(rax65 - 1) & *reinterpret_cast<uint32_t*>(&rax65)) {
                addr_4670_57:
                rax66 = fun_2660();
                nl_error(1, 0, rax66, rcx48, r8_23, r9_25);
                rsp27 = rsp27 - 2 + 2 - 2 + 2;
                goto addr_4694_58;
            } else {
                *reinterpret_cast<uint32_t*>(&rax67) = reinterpret_cast<unsigned char>(rbp5) & 96;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax67) + 4) = 0;
                if (static_cast<uint32_t>(rax67 - 1) & *reinterpret_cast<uint32_t*>(&rax67)) {
                    addr_4694_58:
                    rax68 = fun_2660();
                    nl_error(1, 0, rax68, rcx48, r8_23, r9_25);
                    rsp27 = rsp27 - 2 + 2 - 2 + 2;
                    goto addr_46b8_60;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax69) = reinterpret_cast<unsigned char>(rbp5) & 0x3000;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax69) + 4) = 0;
                    if (static_cast<uint32_t>(rax69 - 1) & *reinterpret_cast<uint32_t*>(&rax69)) {
                        addr_46b8_60:
                        rax70 = fun_2660();
                        rsi71 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi71 + 4) = 0;
                        eax72 = reinterpret_cast<void**>(0);
                        nl_error(1, 0, rax70, rcx48, r8_23, r9_25);
                        rsp73 = reinterpret_cast<void***>(rsp27 - 2 + 2 - 2 + 2);
                    } else {
                        *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<unsigned char>(rcx48) & 0x4002;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                        *reinterpret_cast<uint32_t*>(&rdi75) = reinterpret_cast<unsigned char>(rsi40) & 0x4002;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                        edx76 = static_cast<uint32_t>(rax74 - 1) & *reinterpret_cast<uint32_t*>(&rax74) | static_cast<uint32_t>(rdi75 - 1) & *reinterpret_cast<uint32_t*>(&rdi75);
                        *reinterpret_cast<uint32_t*>(&v35) = edx76;
                        if (edx76) {
                            rax77 = fun_2660();
                            nl_error(1, 0, rax77, rcx48, r8_23, r9_25);
                            rsp78 = reinterpret_cast<void*>(rsp27 - 2 + 2 - 2 + 2);
                            goto addr_4792_64;
                        }
                        if (*reinterpret_cast<unsigned char*>(&rcx48) & 2) {
                            rax79 = max_records;
                            rax80 = reinterpret_cast<unsigned char>(rax79) | reinterpret_cast<unsigned char>(max_bytes);
                            i_nocache = 1;
                            i_nocache_eof = reinterpret_cast<uint1_t>(rax80 == 0);
                            rcx48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx48) & 0xfffffffc);
                            *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                            input_flags = rcx48;
                        }
                        if (*reinterpret_cast<unsigned char*>(&rsi40) & 2) {
                            rax81 = max_records;
                            rax82 = reinterpret_cast<unsigned char>(rax81) | reinterpret_cast<unsigned char>(max_bytes);
                            o_nocache = 1;
                            o_nocache_eof = reinterpret_cast<uint1_t>(rax82 == 0);
                            output_flags = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi40) & 0xfffffffd);
                        }
                        if (*reinterpret_cast<unsigned char*>(&rbp5) & 1) {
                            rdi75 = 0xf4a0;
                            translate_charset(0xf4a0);
                            rsp27 = rsp27 - 2 + 2;
                        }
                        if (!(*reinterpret_cast<unsigned char*>(&rbp5) & 64)) 
                            goto addr_3c1a_72; else 
                            goto addr_32b9_73;
                    }
                }
            }
            addr_46dc_74:
            tmp64_83 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_19) + reinterpret_cast<unsigned char>(seek_bytes));
            r13_19 = tmp64_83;
            if (__intrinsic()) {
                addr_3d54_75:
                if (!(*reinterpret_cast<unsigned char*>(&eax72 + 1) & 2)) 
                    break;
            }
            rcx48 = reinterpret_cast<void**>(0x1b6);
            *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi84) = 1;
            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
            rdx85 = reinterpret_cast<void**>(*reinterpret_cast<uint32_t*>(&r12_4) | 2);
            *reinterpret_cast<int32_t*>(&rdx85 + 4) = 0;
            eax86 = ifd_reopen(1, rsi71, rdx85, 0x1b6);
            rsp73 = rsp73 - 8 + 8;
            rsi71 = output_file;
            if (eax86 >= 0) 
                goto addr_33f2_78;
            addr_33d5_80:
            rcx48 = reinterpret_cast<void**>(0x1b6);
            *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi84) = 1;
            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
            rdx85 = reinterpret_cast<void**>(*reinterpret_cast<uint32_t*>(&r12_4) | 1);
            *reinterpret_cast<int32_t*>(&rdx85 + 4) = 0;
            eax87 = ifd_reopen(1, rsi71, rdx85, 0x1b6);
            rsp73 = rsp73 - 8 + 8;
            if (eax87 >= 0) 
                goto addr_33f2_78;
            addr_47d1_81:
            rsi88 = output_file;
            rax89 = quotearg_style(4, rsi88, 4, rsi88);
            r12_4 = rax89;
            rax90 = fun_2660();
            rcx22 = r12_4;
            rdx26 = rax90;
            rsi40 = *reinterpret_cast<void***>(v63);
            *reinterpret_cast<int32_t*>(&rsi40 + 4) = 0;
            nl_error(1, rsi40, rdx26, rcx22, r8_23, r9_25);
            rsp27 = reinterpret_cast<uint32_t*>(rsp73 - 8 + 8 - 8 + 8 - 8 + 8);
            goto addr_4810_22;
            addr_3c1a_72:
            if (!(*reinterpret_cast<unsigned char*>(&rbp5) & 32)) {
                addr_32ec_82:
                if (*reinterpret_cast<unsigned char*>(&rbp5) & 2) {
                    translate_charset(0xf6a0);
                    rsp27 = rsp27 - 2 + 2;
                    newline_character = 37;
                    space_character = 64;
                } else {
                    *reinterpret_cast<unsigned char*>(&rbp5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rbp5) & 4);
                    if (*reinterpret_cast<unsigned char*>(&rbp5)) {
                        translate_charset(0xf5a0);
                        rsp27 = rsp27 - 2 + 2;
                        newline_character = 37;
                        space_character = 64;
                    }
                }
            } else {
                rax91 = fun_2990(rdi75);
                rsp27 = rsp27 - 2 + 2;
                rsi92 = rax91;
                rax93 = reinterpret_cast<unsigned char*>(0x14160);
                do {
                    rcx48 = reinterpret_cast<void**>(static_cast<uint32_t>(*rax93));
                    *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                    ++rax93;
                    edx94 = (*rsi92)[reinterpret_cast<unsigned char>(rcx48)];
                    *reinterpret_cast<signed char*>(rax93 - 1) = *reinterpret_cast<signed char*>(&edx94);
                } while (!reinterpret_cast<int1_t>(0x14260 == rax93));
                goto addr_32e5_89;
            }
            r12_95 = input_file;
            r13_19 = input_flags;
            *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
            rax96 = fun_2570();
            rsp97 = reinterpret_cast<void***>(rsp27 - 2 + 2);
            v63 = rax96;
            if (!r12_95) {
                rax98 = fun_2660();
                input_file = rax98;
                set_fd_flags(0, r13_19, rax98, rcx48, r8_23, r9_25);
                rsp78 = reinterpret_cast<void*>(rsp97 - 8 + 8 - 8 + 8);
                goto addr_3337_92;
            }
            rcx48 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
            eax99 = ifd_reopen(0, r12_95, r13_19, 0);
            rsp78 = reinterpret_cast<void*>(rsp97 - 8 + 8);
            if (eax99 < 0) {
                addr_4792_64:
                rsi100 = input_file;
                rax101 = quotearg_style(4, rsi100, 4, rsi100);
                rax102 = fun_2660();
                esi103 = *reinterpret_cast<void***>(v63);
                nl_error(1, esi103, rax102, rax101, r8_23, r9_25);
                rsp73 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp78) - 8 + 8 - 8 + 8 - 8 + 8);
                goto addr_47d1_81;
            } else {
                addr_3337_92:
                rax104 = fun_2710();
                rsp73 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp78) - 8 + 8);
                rsi71 = output_file;
                r13_19 = output_flags;
                *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
                rdx105 = reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rax104)) >> 63;
                input_seekable = *reinterpret_cast<unsigned char*>(&rdx105);
                if (reinterpret_cast<signed char>(rax104) < reinterpret_cast<signed char>(0)) {
                    rax104 = reinterpret_cast<void**>(0);
                }
            }
            input_offset = rax104;
            input_seek_errno = *reinterpret_cast<void***>(v63);
            if (!rsi71) 
                goto addr_42a6_96;
            eax72 = conversions_mask;
            edx106 = 0;
            r14_107 = seek_records;
            r8_23 = output_blocksize;
            *reinterpret_cast<unsigned char*>(&edx106) = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(&eax72 + 1) & 16) == 0);
            *reinterpret_cast<uint32_t*>(&r12_4) = reinterpret_cast<uint32_t>(reinterpret_cast<signed char>(eax72) >> 6) & 0x80 | reinterpret_cast<unsigned char>(r13_19) | edx106 << 6;
            if (r14_107) {
                r13_19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_107) * reinterpret_cast<unsigned char>(r8_23));
                if (!__intrinsic()) 
                    goto addr_46dc_74; else 
                    goto addr_3d54_75;
            } else {
                r13_19 = seek_bytes;
                *reinterpret_cast<uint32_t*>(&r12_4) = *reinterpret_cast<uint32_t*>(&r12_4) | ~reinterpret_cast<unsigned char>(eax72) & 0x200;
                goto addr_33d5_80;
            }
            addr_32e5_89:
            translation_needed = 1;
            goto addr_32ec_82;
            addr_32b9_73:
            rax108 = fun_2500(rdi75);
            rsp27 = rsp27 - 2 + 2;
            rsi109 = rax108;
            rax110 = reinterpret_cast<unsigned char*>(0x14160);
            do {
                rcx48 = reinterpret_cast<void**>(static_cast<uint32_t>(*rax110));
                *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                ++rax110;
                edx111 = (*rsi109)[reinterpret_cast<unsigned char>(rcx48)];
                *reinterpret_cast<signed char*>(rax110 - 1) = *reinterpret_cast<signed char*>(&edx111);
            } while (!reinterpret_cast<int1_t>(0x14260 == rax110));
            goto addr_32e5_89;
        }
    } else {
        rbp5 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp5) - reinterpret_cast<unsigned char>(eax29));
        *reinterpret_cast<int32_t*>(&rbp5 + 4) = 0;
        rdx26 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax29)));
        v31 = 0;
        *reinterpret_cast<int32_t*>(&rax112) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbp5 + 0xffffffffffffffff));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax112) + 4) = 0;
        r13_19 = r12_4 + reinterpret_cast<unsigned char>(rdx26) * 8;
        *reinterpret_cast<unsigned char*>(&v32) = 0;
        *reinterpret_cast<unsigned char*>(&v30) = 0;
        v34 = reinterpret_cast<void**>(0);
        r12_4 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(r12_4 + (reinterpret_cast<int64_t>(rax112) + reinterpret_cast<unsigned char>(rdx26)) * 8) + 8);
        v35 = reinterpret_cast<void**>(0);
        v33 = reinterpret_cast<void**>(0x7fffffffffffffff);
        do {
            r15_113 = *reinterpret_cast<void***>(r13_19);
            rsi43 = reinterpret_cast<void**>(61);
            *reinterpret_cast<int32_t*>(&rsi43 + 4) = 0;
            rax114 = fun_26d0(r15_113, r15_113);
            rsp27 = rsp27 - 2 + 2;
            if (!rax114) {
                rdi115 = r15_113;
                goto addr_4603_104;
            }
            rbp5 = reinterpret_cast<void**>(&rax114->f1);
            *reinterpret_cast<uint32_t*>(&rcx22) = 0x69;
            rax116 = r15_113;
            rdx26 = reinterpret_cast<void**>("if");
            do {
                ++rax116;
                ++rdx26;
                if (*reinterpret_cast<signed char*>(rax116 + 0xffffffffffffffff) != *reinterpret_cast<signed char*>(&rcx22)) 
                    goto addr_2e30_107;
                *reinterpret_cast<uint32_t*>(&rcx22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx26));
                *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
            } while (*reinterpret_cast<signed char*>(&rcx22));
            eax117 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax116));
            if (*reinterpret_cast<signed char*>(&eax117) == 61 || !*reinterpret_cast<signed char*>(&eax117)) {
                input_file = rbp5;
                continue;
            } else {
                addr_2e30_107:
                rax118 = r15_113;
                *reinterpret_cast<uint32_t*>(&rcx22) = 0x6f;
                rdx26 = reinterpret_cast<void**>("of");
            }
            do {
                ++rax118;
                ++rdx26;
                if (*reinterpret_cast<signed char*>(rax118 + 0xffffffffffffffff) != *reinterpret_cast<signed char*>(&rcx22)) 
                    goto addr_2e70_112;
                *reinterpret_cast<uint32_t*>(&rcx22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx26));
                *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
            } while (*reinterpret_cast<signed char*>(&rcx22));
            eax119 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax118));
            if (!*reinterpret_cast<signed char*>(&eax119) || *reinterpret_cast<signed char*>(&eax119) == 61) {
                output_file = rbp5;
                continue;
            } else {
                addr_2e70_112:
                rax120 = r15_113;
                ecx121 = 99;
                rdx122 = reinterpret_cast<unsigned char*>("conv");
            }
            do {
                ++rax120;
                ++rdx122;
                if (*reinterpret_cast<signed char*>(&ecx121) != *reinterpret_cast<signed char*>(rax120 + 0xffffffffffffffff)) 
                    goto addr_2ea7_117;
                ecx121 = *rdx122;
            } while (*reinterpret_cast<signed char*>(&ecx121));
            eax123 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax120));
            if (!*reinterpret_cast<signed char*>(&eax123) || *reinterpret_cast<signed char*>(&eax123) == 61) {
                rcx22 = reinterpret_cast<void**>("invalid conversion");
                *reinterpret_cast<int32_t*>(&rdx26) = 0;
                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                eax124 = parse_symbols(rbp5, "ascii", 0, "invalid conversion");
                rsp27 = rsp27 - 2 + 2;
                conversions_mask = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(conversions_mask) | eax124);
                continue;
            } else {
                addr_2ea7_117:
                rax125 = r15_113;
                ecx126 = 0x69;
                rdx127 = reinterpret_cast<unsigned char*>("iflag");
            }
            do {
                ++rax125;
                ++rdx127;
                if (*reinterpret_cast<signed char*>(rax125 + 0xffffffffffffffff) != *reinterpret_cast<signed char*>(&ecx126)) 
                    goto addr_2ee7_122;
                ecx126 = *rdx127;
            } while (*reinterpret_cast<signed char*>(&ecx126));
            eax128 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax125));
            if (!*reinterpret_cast<signed char*>(&eax128) || *reinterpret_cast<signed char*>(&eax128) == 61) {
                rcx22 = reinterpret_cast<void**>("invalid input flag");
                *reinterpret_cast<int32_t*>(&rdx26) = 0;
                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                eax129 = parse_symbols(rbp5, "append", 0, "invalid input flag");
                rsp27 = rsp27 - 2 + 2;
                input_flags = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(input_flags) | eax129);
                continue;
            } else {
                addr_2ee7_122:
                rax130 = r15_113;
                ecx131 = 0x6f;
                rdx132 = reinterpret_cast<unsigned char*>("oflag");
            }
            do {
                ++rax130;
                ++rdx132;
                if (*reinterpret_cast<signed char*>(rax130 + 0xffffffffffffffff) != *reinterpret_cast<signed char*>(&ecx131)) 
                    goto addr_2f27_127;
                ecx131 = *rdx132;
            } while (*reinterpret_cast<signed char*>(&ecx131));
            eax133 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax130));
            if (*reinterpret_cast<signed char*>(&eax133) == 61 || !*reinterpret_cast<signed char*>(&eax133)) {
                rcx22 = reinterpret_cast<void**>("invalid output flag");
                *reinterpret_cast<int32_t*>(&rdx26) = 0;
                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                eax134 = parse_symbols(rbp5, "append", 0, "invalid output flag");
                rsp27 = rsp27 - 2 + 2;
                output_flags = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(output_flags) | eax134);
                continue;
            } else {
                addr_2f27_127:
                rax135 = r15_113;
                ecx136 = 0x73;
                rdx137 = reinterpret_cast<unsigned char*>("status");
            }
            do {
                ++rax135;
                ++rdx137;
                if (*reinterpret_cast<signed char*>(rax135 + 0xffffffffffffffff) != *reinterpret_cast<signed char*>(&ecx136)) 
                    goto addr_2fcb_132;
                ecx136 = *rdx137;
            } while (*reinterpret_cast<signed char*>(&ecx136));
            eax138 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax135));
            if (!*reinterpret_cast<signed char*>(&eax138) || *reinterpret_cast<signed char*>(&eax138) == 61) {
                rcx22 = reinterpret_cast<void**>("invalid status level");
                *reinterpret_cast<int32_t*>(&rdx26) = 1;
                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                eax139 = parse_symbols(rbp5, "none", 1, "invalid status level");
                rsp27 = rsp27 - 2 + 2;
                status_level = eax139;
                continue;
            } else {
                addr_2fcb_132:
                r14_140 = reinterpret_cast<void**>(rsp27 + 20);
                rcx22 = reinterpret_cast<void**>(rsp27 + 18);
                r8_23 = reinterpret_cast<void**>("bcEGkKMPTwYZ0");
                rax141 = xstrtoumax(rbp5, r14_140, rbp5, r14_140);
                rsp142 = rsp27 - 2 + 2;
                *reinterpret_cast<uint32_t*>(&v63) = *reinterpret_cast<uint32_t*>(&rax141);
                if ((*reinterpret_cast<uint32_t*>(&rax141) & 0xfffffffe) != 2) 
                    goto addr_3004_136;
            }
            rax143 = v144;
            edx145 = rax143->f0;
            if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rax143) - 1) == 66) {
                if (rax143->f0 != 0x78) 
                    goto addr_3004_136;
                rdi146 = reinterpret_cast<void**>(&rax143->f1);
            } else {
                if (*reinterpret_cast<signed char*>(&edx145) == 66) {
                    rdx147 = reinterpret_cast<struct s6*>(&rax143->f1);
                    v144 = rdx147;
                    if (!*reinterpret_cast<void***>(&rax143->f1)) {
                        *reinterpret_cast<uint32_t*>(&v63) = *reinterpret_cast<uint32_t*>(&v63) & 0xfffffffd;
                        goto addr_3004_136;
                    }
                    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax143->f1) == 0x78)) 
                        goto addr_3004_136;
                    rdi146 = reinterpret_cast<void**>(&rax143->f2);
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rdx147) - 1) != 66) 
                        goto addr_417e_145;
                } else {
                    if (rax143->f0 != 0x78) 
                        goto addr_3004_136;
                    rdi146 = reinterpret_cast<void**>(&rax143->f1);
                    goto addr_3cc1_148;
                }
            }
            rax148 = fun_26d0(rdi146, rdi146);
            rsp142 = rsp142 - 2 + 2;
            rdi146 = rdi146;
            if (rax148) {
                addr_3004_136:
                r14_107 = reinterpret_cast<void**>(0);
                if (0) {
                    rax149 = fun_26d0(rbp5, rbp5);
                    rsi43 = reinterpret_cast<void**>("ibs");
                    eax150 = operand_matches(r15_113, "ibs", 61, rcx22);
                    rsp27 = rsp142 - 2 + 2 - 2 + 2;
                    if (*reinterpret_cast<signed char*>(&eax150)) 
                        goto addr_3f94_151;
                    rsi43 = reinterpret_cast<void**>("obs");
                    *reinterpret_cast<unsigned char*>(&r10_151) = reinterpret_cast<uint1_t>(!!rax149);
                    eax152 = operand_matches(r15_113, "obs", 61, rcx22);
                    rsp27 = rsp27 - 2 + 2;
                    if (*reinterpret_cast<signed char*>(&eax152)) 
                        goto addr_3f94_151;
                    *reinterpret_cast<int32_t*>(&rdx26) = 61;
                    *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                    r14_107 = reinterpret_cast<void**>(0x7fffffffffffffff);
                    eax153 = operand_matches(r15_113, "bs", 61, rcx22);
                    rsp27 = rsp27 - 2 + 2;
                    *reinterpret_cast<uint32_t*>(&v63) = 1;
                    if (!*reinterpret_cast<signed char*>(&eax153)) 
                        goto addr_3082_154;
                    rax154 = reinterpret_cast<void***>(rsp27 + 16);
                } else {
                    addr_3012_156:
                    rax155 = fun_26d0(rbp5, rbp5);
                    *reinterpret_cast<int32_t*>(&rdx26) = 61;
                    *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                    rsi43 = reinterpret_cast<void**>("ibs");
                    r10_151 = rax155;
                    eax156 = operand_matches(r15_113, "ibs", 61, rcx22);
                    rsp27 = rsp142 - 2 + 2 - 2 + 2;
                    if (*reinterpret_cast<signed char*>(&eax156)) {
                        rax154 = reinterpret_cast<void***>(0x143d0);
                        goto addr_310c_158;
                    }
                }
            } else {
                goto addr_3cc1_148;
            }
            addr_400a_160:
            rsi43 = reinterpret_cast<void**>(0x7fffffffffffffff);
            if (r14_107 == 0x7fffffffffffffff) 
                goto addr_3f94_151;
            if (*reinterpret_cast<uint32_t*>(&v63)) 
                goto addr_4854_53; else 
                goto addr_4027_162;
            *reinterpret_cast<int32_t*>(&rdx26) = 61;
            *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
            rsi43 = reinterpret_cast<void**>("obs");
            *reinterpret_cast<unsigned char*>(&r10_151) = reinterpret_cast<uint1_t>(!!r10_151);
            eax157 = operand_matches(r15_113, "obs", 61, rcx22);
            rsp27 = rsp27 - 2 + 2;
            *reinterpret_cast<uint32_t*>(&r8_23) = eax157;
            *reinterpret_cast<int32_t*>(&r8_23 + 4) = 0;
            rax154 = reinterpret_cast<void***>(0x143c8);
            if (*reinterpret_cast<signed char*>(&r8_23)) {
                addr_310c_158:
                if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r14_107) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r14_107 == 0))) 
                    goto addr_400a_160; else 
                    goto addr_3115_164;
            } else {
                *reinterpret_cast<int32_t*>(&rdx26) = 61;
                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                rsi43 = reinterpret_cast<void**>("bs");
                eax158 = operand_matches(r15_113, "bs", 61, rcx22);
                rsp27 = rsp27 - 2 + 2;
                if (*reinterpret_cast<signed char*>(&eax158)) {
                    rax154 = reinterpret_cast<void***>(rsp27 + 16);
                    goto addr_310c_158;
                } else {
                    addr_3082_154:
                    rsi43 = reinterpret_cast<void**>("cbs");
                    eax159 = operand_matches(r15_113, "cbs", 61, rcx22);
                    rsp27 = rsp27 - 2 + 2;
                    if (*reinterpret_cast<signed char*>(&eax159)) {
                        rax154 = reinterpret_cast<void***>(0x143c0);
                        *reinterpret_cast<int32_t*>(&rdx26) = 1;
                        *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                    } else {
                        rsi43 = reinterpret_cast<void**>("skip");
                        eax160 = operand_matches(r15_113, "skip", 61, rcx22);
                        rsp27 = rsp27 - 2 + 2;
                        if (*reinterpret_cast<signed char*>(&eax160) || (rsi43 = reinterpret_cast<void**>("iseek"), eax161 = operand_matches(r15_113, "iseek", 61, rcx22), rsp27 = rsp27 - 2 + 2, !!*reinterpret_cast<signed char*>(&eax161))) {
                            *reinterpret_cast<unsigned char*>(&v32) = *reinterpret_cast<unsigned char*>(&r10_151);
                            *reinterpret_cast<int32_t*>(&rax154) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax154) + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rdx26) = 0;
                            *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                            v35 = r14_107;
                        } else {
                            *reinterpret_cast<int32_t*>(&rdi162) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi162) + 4) = 0;
                            *reinterpret_cast<unsigned char*>(&rdi162) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r15_113) == 0x6f);
                            rsi43 = reinterpret_cast<void**>("seek");
                            eax163 = operand_matches(reinterpret_cast<int64_t>(rdi162) + reinterpret_cast<unsigned char>(r15_113), "seek", 61, rcx22);
                            rsp27 = rsp27 - 2 + 2;
                            if (*reinterpret_cast<signed char*>(&eax163)) {
                                addr_4639_171:
                                v31 = *reinterpret_cast<unsigned char*>(&r10_151);
                                *reinterpret_cast<int32_t*>(&rax154) = 0;
                                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax154) + 4) = 0;
                                *reinterpret_cast<int32_t*>(&rdx26) = 0;
                                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                                v34 = r14_107;
                            } else {
                                *reinterpret_cast<int32_t*>(&rdx26) = 61;
                                *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                                rsi43 = reinterpret_cast<void**>("count");
                                rdi115 = r15_113;
                                eax164 = operand_matches(rdi115, "count", 61, rcx22);
                                rsp27 = rsp27 - 2 + 2;
                                if (!*reinterpret_cast<signed char*>(&eax164)) {
                                    addr_4603_104:
                                    rax165 = quote(rdi115, rsi43, rdx26, rcx22, r8_23, r9_25);
                                    r12_4 = rax165;
                                    rax166 = fun_2660();
                                    rcx22 = r12_4;
                                    rsi43 = reinterpret_cast<void**>(0);
                                    *reinterpret_cast<int32_t*>(&rsi43 + 4) = 0;
                                    nl_error(0, 0, rax166, rcx22, r8_23, r9_25);
                                    usage(1);
                                    rsp27 = rsp27 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2;
                                    goto addr_4639_171;
                                } else {
                                    *reinterpret_cast<unsigned char*>(&v30) = *reinterpret_cast<unsigned char*>(&r10_151);
                                    *reinterpret_cast<int32_t*>(&rax154) = 0;
                                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax154) + 4) = 0;
                                    *reinterpret_cast<int32_t*>(&rdx26) = 0;
                                    *reinterpret_cast<int32_t*>(&rdx26 + 4) = 0;
                                    v33 = r14_107;
                                }
                            }
                        }
                    }
                }
            }
            if (reinterpret_cast<signed char>(rdx26) > reinterpret_cast<signed char>(r14_107)) 
                goto addr_3115_164;
            if (*reinterpret_cast<uint32_t*>(&v63)) 
                goto addr_4854_53;
            if (rax154) {
                addr_4027_162:
                *rax154 = r14_107;
                continue;
            } else {
                continue;
            }
            addr_3cc1_148:
            r8_23 = reinterpret_cast<void**>("bcEGkKMPTwYZ0");
            rax167 = xstrtoumax(rdi146, r14_140, rdi146, r14_140);
            rsp142 = rsp142 - 2 + 2;
            *reinterpret_cast<uint32_t*>(&rcx22) = *reinterpret_cast<uint32_t*>(&rax167);
            *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
            r10d168 = *reinterpret_cast<uint32_t*>(&rax167) & 0xfffffffe;
            if (r10d168) {
                r10d169 = *reinterpret_cast<uint32_t*>(&rax167);
                *reinterpret_cast<int32_t*>(&r14_107) = 0;
                *reinterpret_cast<int32_t*>(&r14_107 + 4) = 0;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx170) = __intrinsic();
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx170) + 4) = 0;
                if (static_cast<int1_t>(!!rdx170)) {
                    r14_107 = reinterpret_cast<void**>(0x7fffffffffffffff);
                    r10d169 = 1;
                } else {
                    r14_107 = reinterpret_cast<void**>(0);
                    if (0) {
                        *reinterpret_cast<uint32_t*>(&rcx22) = *reinterpret_cast<uint32_t*>(&rcx22) | *reinterpret_cast<uint32_t*>(&v63);
                        *reinterpret_cast<int32_t*>(&rcx22 + 4) = 0;
                        zf171 = *reinterpret_cast<uint32_t*>(&rcx22) == 2;
                        if (!zf171) {
                            r14_107 = reinterpret_cast<void**>(0x7fffffffffffffff);
                        }
                        r10d169 = reinterpret_cast<uint1_t>(!zf171);
                    } else {
                        r9_25 = reinterpret_cast<void**>("0x");
                        eax172 = fun_25a0(rbp5, "0x", 2);
                        rsp142 = rsp142 - 2 + 2;
                        r10d169 = r10d168;
                        if (!eax172) {
                            rax173 = quote_n();
                            r9_25 = reinterpret_cast<void**>("0x");
                            rax174 = quote_n();
                            v63 = rax174;
                            rax175 = fun_2660();
                            r8_23 = rax173;
                            rcx22 = v63;
                            nl_error(0, 0, rax175, rcx22, r8_23, "0x");
                            rsp142 = rsp142 - 2 + 2 - 2 + 2 - 2 + 2 - 2 + 2;
                            r10d169 = r10d169;
                        }
                    }
                }
            }
            *reinterpret_cast<uint32_t*>(&v63) = r10d169;
            goto addr_3012_156;
            addr_417e_145:
            goto addr_3cc1_148;
            r13_19 = r13_19 + 8;
        } while (r12_4 != r13_19);
        goto addr_2cbf_189;
    }
    v63 = r8_23;
    rax176 = fun_2660();
    r8_23 = v63;
    nl_error(1, 0, rax176, r14_107, r8_23, r9_25);
    rsp177 = rsp73 - 8 + 8 - 8 + 8;
    goto addr_471d_191;
    addr_33f2_78:
    zf178 = seek_records == 0;
    if (!zf178 && ((zf179 = (*reinterpret_cast<unsigned char*>(&conversions_mask + 1) & 2) == 0, zf179) && (rdi84 = r13_19, eax180 = iftruncate_constprop_0(rdi84, rdi84), rsp73 = rsp73 - 8 + 8, !!eax180))) {
        rsi71 = v10;
        *reinterpret_cast<uint32_t*>(&rdi84) = 1;
        *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
        r12d181 = *reinterpret_cast<void***>(v63);
        eax182 = ifstat(1, rsi71);
        rsp73 = rsp73 - 8 + 8;
        if (eax182) {
            rsi183 = output_file;
            rax184 = quotearg_style(4, rsi183, 4, rsi183);
            rax185 = fun_2660();
            rcx48 = rax184;
            *reinterpret_cast<uint32_t*>(&rdi84) = 0;
            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
            rdx85 = rax185;
            rsi71 = *reinterpret_cast<void***>(v63);
            *reinterpret_cast<int32_t*>(&rsi71 + 4) = 0;
            nl_error(0, rsi71, rdx85, rcx48, r8_23, r9_25);
            rsp73 = rsp73 - 8 + 8 - 8 + 8 - 8 + 8;
        } else {
            if (!((v186 & reinterpret_cast<uint32_t>("yte) blocks")) - 0x4000 & 0xffffb000)) {
                rsi187 = output_file;
                rax188 = quotearg_style(4, rsi187, 4, rsi187);
                rax189 = fun_2660();
                r8_23 = rax188;
                rcx48 = r13_19;
                rsi71 = r12d181;
                *reinterpret_cast<int32_t*>(&rsi71 + 4) = 0;
                rdx85 = rax189;
                *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                nl_error(0, rsi71, rdx85, rcx48, r8_23, r9_25);
                rsp73 = rsp73 - 8 + 8 - 8 + 8 - 8 + 8;
            }
        }
    }
    addr_3409_196:
    rax190 = gethrxtime(rdi84, rsi71, rdx85, rcx48, r8_23, r9_25);
    rsp177 = rsp73 - 8 + 8;
    rdx191 = skip_records;
    start_time = rax190;
    next_time = rax190 + 0x3b9aca00;
    if (rdx191) {
        rcx48 = input_blocksize;
        r13_19 = reinterpret_cast<void**>(1);
        *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
        r14_192 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx191) * reinterpret_cast<unsigned char>(rcx48));
        if (__intrinsic()) {
            addr_3458_198:
            rsi193 = input_file;
            *reinterpret_cast<uint32_t*>(&rdi84) = 0;
            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
            r8_23 = reinterpret_cast<void**>(0x143b0);
            r12_194 = input_offset;
            rax195 = skip(0, rsi193, rdx191, rcx48, 0x143b0, r9_25);
            rsp177 = rsp177 - 8 + 8;
            if (rax195) 
                goto addr_348e_199;
        } else {
            goto addr_344a_201;
        }
    } else {
        zf196 = skip_bytes == 0;
        if (zf196) {
            addr_34d1_203:
            rdx197 = seek_records;
            rax198 = seek_bytes;
            rsi199 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx197) | reinterpret_cast<unsigned char>(rax198));
            if (!rsi199) 
                goto addr_3580_204; else 
                goto addr_34eb_205;
        } else {
            rcx48 = input_blocksize;
            *reinterpret_cast<int32_t*>(&r14_192) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_192) + 4) = 0;
            goto addr_344a_201;
        }
    }
    rax200 = input_offset;
    if (reinterpret_cast<signed char>(rax200) >= reinterpret_cast<signed char>(0)) {
        if (r13_19 || !reinterpret_cast<int1_t>(reinterpret_cast<unsigned char>(rax200) - reinterpret_cast<unsigned char>(r12_194) == r14_192)) {
            addr_348e_199:
            zf201 = status_level == 1;
            if (!zf201) {
                rax202 = quotearg_n_style_colon();
                rax203 = fun_2660();
                rcx48 = rax202;
                *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                nl_error(0, 0, rax203, rcx48, 0x143b0, r9_25);
                rsp177 = rsp177 - 8 + 8 - 8 + 8 - 8 + 8;
                goto addr_34d1_203;
            }
        } else {
            goto addr_34d1_203;
        }
    }
    addr_344a_201:
    r13_19 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
    tmp64_204 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r14_192) + reinterpret_cast<uint64_t>(skip_bytes));
    r14_192 = tmp64_204;
    *reinterpret_cast<unsigned char*>(&r13_19) = __intrinsic();
    goto addr_3458_198;
    addr_34eb_205:
    r8_23 = reinterpret_cast<void**>(rsp177 + 88);
    *reinterpret_cast<uint32_t*>(&rdi84) = 1;
    *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
    v205 = rax198;
    rcx48 = output_blocksize;
    rsi199 = output_file;
    rax206 = skip(1, rsi199, rdx197, rcx48, r8_23, r9_25);
    rsp177 = rsp177 - 8 + 8;
    r12_207 = rax206;
    if (!rax206) {
        rdx197 = v205;
        if (!rdx197) {
            goto addr_3580_204;
        }
    } else {
        rdx197 = output_blocksize;
    }
    rdi208 = obuf;
    fun_2730(rdi208);
    rsp177 = rsp177 - 8 + 8;
    do {
        rdi84 = obuf;
        if (!r12_207) 
            break;
        rbp209 = output_blocksize;
        rsi199 = rbp209;
        rax210 = iwrite_constprop_0(rdi84, rsi199, rdx197, rcx48, r8_23, r9_25);
        rsp177 = rsp177 - 8 + 8;
        if (rbp209 != rax210) 
            goto addr_471d_191;
        --r12_207;
    } while (r12_207);
    goto addr_355e_218;
    r12_211 = v205;
    addr_356f_220:
    rsi199 = r12_211;
    rax212 = iwrite_constprop_0(rdi84, rsi199, rdx197, rcx48, r8_23, r9_25);
    rsp177 = rsp177 - 8 + 8;
    if (rax212 != r12_211) {
        addr_471d_191:
        rsi213 = output_file;
        rax214 = quotearg_style(4, rsi213, 4, rsi213);
        rax215 = fun_2660();
        rcx48 = rax214;
        rsi216 = *reinterpret_cast<void***>(v63);
        *reinterpret_cast<int32_t*>(&rsi216 + 4) = 0;
        nl_error(0, rsi216, rax215, rcx48, r8_23, r9_25);
        quit_constprop_0(0, rsi216, rax215, rcx48, r8_23, r9_25);
        rsp177 = rsp177 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8;
        goto addr_475e_221;
    } else {
        addr_3580_204:
        rax217 = max_records;
        rax218 = reinterpret_cast<unsigned char>(rax217) | reinterpret_cast<unsigned char>(max_bytes);
        v32 = 0;
        if (!rax218) {
            addr_3b71_222:
            eax219 = synchronize_output(rdi84, rsi199, rdx197, rcx48, r8_23, r9_25);
            *reinterpret_cast<uint32_t*>(&rdx220) = i_nocache;
            *reinterpret_cast<int32_t*>(&rdx220 + 4) = 0;
            if (!eax219) {
                eax219 = v32;
            }
        } else {
            zf221 = ibuf == 0;
            if (zf221) {
                alloc_ibuf_part_0(rdi84, rdi84);
                rsp177 = rsp177 - 8 + 8;
            }
            alloc_obuf(rdi84, rdi84);
            rsp177 = rsp177 - 8 + 8;
            v32 = 0;
            *reinterpret_cast<int32_t*>(&r12_222) = 0;
            *reinterpret_cast<int32_t*>(&r12_222 + 4) = 0;
            v30 = 0xffffffff;
            while (1) {
                while (1) {
                    addr_35c8_227:
                    zf223 = status_level == 4;
                    if (!zf223) 
                        goto addr_35d5_228;
                    while (1) {
                        rax224 = gethrxtime(rdi84, rsi199, rdx197, rcx48, r8_23, r9_25);
                        rsp177 = rsp177 - 8 + 8;
                        less225 = reinterpret_cast<signed char>(rax224) < reinterpret_cast<signed char>(next_time);
                        if (!less225) {
                            rdi84 = rax224;
                            print_xfer_stats();
                            rsp177 = rsp177 - 8 + 8;
                            tmp64_226 = next_time + 0x3b9aca00;
                            next_time = tmp64_226;
                        }
                        do {
                            addr_35d5_228:
                            r14_227 = max_bytes;
                            r13_228 = max_records;
                            rbp229 = r_full;
                            tmp64_230 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp229) + reinterpret_cast<unsigned char>(r_partial));
                            eax231 = conversions_mask;
                            rdx197 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_228) - (1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r13_228) < reinterpret_cast<unsigned char>(1 - static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(r14_227) < reinterpret_cast<unsigned char>(1))))))));
                            if (reinterpret_cast<signed char>(tmp64_230) >= reinterpret_cast<signed char>(rdx197)) 
                                goto addr_3dc0_231;
                            r8_23 = ibuf;
                            if ((reinterpret_cast<unsigned char>(eax231) & 0x500) == 0x500) {
                                rax232 = fun_2730(r8_23);
                                rsp177 = rsp177 - 8 + 8;
                                r8_23 = rax232;
                            }
                            rax233 = iread_fnc;
                            if (reinterpret_cast<signed char>(tmp64_230) >= reinterpret_cast<signed char>(r13_228)) {
                                rdx197 = r14_227;
                                *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                                *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                                rax234 = reinterpret_cast<void**>(rax233());
                                rsp177 = rsp177 - 8 + 8;
                                r13_19 = rax234;
                                zf235 = reinterpret_cast<uint1_t>(r13_19 == 0);
                                if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r13_19) < reinterpret_cast<signed char>(0)) | zf235) 
                                    goto addr_3782_236; else 
                                    goto addr_3665_237;
                            }
                            rdx197 = input_blocksize;
                            *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                            rax236 = reinterpret_cast<void**>(rax233());
                            rsp177 = rsp177 - 8 + 8;
                            r13_19 = rax236;
                            zf235 = reinterpret_cast<uint1_t>(r13_19 == 0);
                            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r13_19) < reinterpret_cast<signed char>(0)) | zf235)) {
                                addr_3665_237:
                                rax237 = input_offset;
                                if (reinterpret_cast<signed char>(rax237) >= reinterpret_cast<signed char>(0)) {
                                    if (__intrinsic()) {
                                        addr_475e_221:
                                        input_offset = reinterpret_cast<void**>(0xffffffffffffffff);
                                    } else {
                                        input_offset = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax237) + reinterpret_cast<unsigned char>(r13_19));
                                    }
                                }
                            } else {
                                addr_3782_236:
                                eax231 = conversions_mask;
                                if (zf235) 
                                    goto addr_3d86_241; else 
                                    goto addr_378e_242;
                            }
                            zf238 = i_nocache == 0;
                            if (!zf238) {
                                invalidate_cache(0, r13_19, 0, r13_19);
                                rsp177 = rsp177 - 8 + 8;
                            }
                            addr_368e_245:
                            rdx197 = input_blocksize;
                            r15_239 = ibuf;
                            if (reinterpret_cast<signed char>(r13_19) >= reinterpret_cast<signed char>(rdx197)) {
                                rax240 = obuf;
                                tmp64_241 = r_full + 1;
                                r_full = tmp64_241;
                                *reinterpret_cast<int32_t*>(&r12_222) = 0;
                                *reinterpret_cast<int32_t*>(&r12_222 + 4) = 0;
                                if (rax240 == r15_239) 
                                    goto addr_38ae_247;
                            } else {
                                eax242 = conversions_mask;
                                tmp64_243 = r_partial + 1;
                                r_partial = tmp64_243;
                                r12_222 = r13_19;
                                if (*reinterpret_cast<unsigned char*>(&eax242 + 1) & 4) {
                                    if (!(*reinterpret_cast<unsigned char*>(&eax242 + 1) & 1)) {
                                        fun_2730(reinterpret_cast<unsigned char>(r15_239) + reinterpret_cast<unsigned char>(r13_19));
                                        rsp177 = rsp177 - 8 + 8;
                                        rdx197 = input_blocksize;
                                    }
                                    r12_222 = r13_19;
                                    r13_19 = rdx197;
                                }
                                rax240 = obuf;
                                if (rax240 == r15_239) 
                                    goto addr_38ae_247;
                            }
                            zf244 = translation_needed == 0;
                            if (!zf244) {
                                if (!r13_19) {
                                    rcx48 = conversions_mask;
                                    *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                                    if (!(*reinterpret_cast<unsigned char*>(&rcx48) & 0x80)) {
                                        addr_3710_256:
                                        if (*reinterpret_cast<unsigned char*>(&rcx48) & 8) 
                                            goto addr_394e_257;
                                    } else {
                                        goto addr_3a72_259;
                                    }
                                } else {
                                    rsi245 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_239) + reinterpret_cast<unsigned char>(r13_19));
                                    rdx197 = r15_239;
                                    do {
                                        *reinterpret_cast<uint32_t*>(&rcx246) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx197));
                                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx246) + 4) = 0;
                                        ++rdx197;
                                        ecx247 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbx20) + reinterpret_cast<uint64_t>(rcx246));
                                        *reinterpret_cast<signed char*>(rdx197 + 0xffffffffffffffff) = *reinterpret_cast<signed char*>(&ecx247);
                                    } while (rdx197 != rsi245);
                                    rcx48 = conversions_mask;
                                    *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                                    if (!(*reinterpret_cast<unsigned char*>(&rcx48) & 0x80)) 
                                        goto addr_3710_256; else 
                                        goto addr_3a0a_263;
                                }
                            } else {
                                rcx48 = conversions_mask;
                                *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                                if (!(*reinterpret_cast<unsigned char*>(&rcx48) & 0x80)) 
                                    goto addr_3710_256;
                                if (r13_19) 
                                    goto addr_3a0a_263; else 
                                    goto addr_3a72_259;
                            }
                            rcx48 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx48) & 16);
                            *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0;
                            if (!rcx48) 
                                goto addr_38ed_267; else 
                                continue;
                            addr_3a72_259:
                            r13_19 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
                            goto addr_3710_256;
                            addr_3a0a_263:
                            edi248 = 0xffffffff;
                            esi249 = reinterpret_cast<unsigned char>(r13_19) & 1;
                            edx250 = ~v30 >> 31;
                            if (*reinterpret_cast<signed char*>(&esi249) != *reinterpret_cast<signed char*>(&edx250)) {
                                --r13_19;
                                edi248 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r15_239) + reinterpret_cast<unsigned char>(r13_19));
                            }
                            rdx197 = r13_19;
                            if (reinterpret_cast<signed char>(r13_19) > reinterpret_cast<signed char>(1)) {
                                do {
                                    esi251 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r15_239) + reinterpret_cast<unsigned char>(rdx197) + 0xfffffffffffffffe);
                                    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_239) + reinterpret_cast<unsigned char>(rdx197)) = *reinterpret_cast<signed char*>(&esi251);
                                    rdx197 = rdx197 - 2;
                                } while (reinterpret_cast<signed char>(rdx197) > reinterpret_cast<signed char>(1));
                            }
                            if (v30 == 0xffffffff) {
                                v30 = edi248;
                                ++r15_239;
                                goto addr_3710_256;
                            } else {
                                esi252 = *reinterpret_cast<unsigned char*>(&v30);
                                ++r13_19;
                                v30 = edi248;
                                *reinterpret_cast<void***>(r15_239 + 1) = *reinterpret_cast<void***>(&esi252);
                                goto addr_3710_256;
                            }
                            addr_378e_242:
                            if (!(*reinterpret_cast<unsigned char*>(&eax231 + 1) & 1)) 
                                goto addr_379c_275;
                            zf253 = status_level == 1;
                            if (zf253) 
                                goto addr_37e7_277;
                            addr_379c_275:
                            rsi254 = input_file;
                            rax255 = quotearg_style(4, rsi254, 4, rsi254);
                            rax256 = fun_2660();
                            rcx48 = rax255;
                            *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                            rdx197 = rax256;
                            esi257 = *reinterpret_cast<void***>(v63);
                            nl_error(0, esi257, rdx197, rcx48, r8_23, r9_25);
                            rsp177 = rsp177 - 8 + 8 - 8 + 8 - 8 + 8;
                            eax231 = conversions_mask;
                            if (!(*reinterpret_cast<unsigned char*>(&eax231 + 1) & 1)) 
                                goto addr_4434_278;
                            addr_37e7_277:
                            print_stats();
                            rbp258 = input_blocksize;
                            *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                            rbp259 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp258) - reinterpret_cast<unsigned char>(r12_222));
                            rsi199 = rbp259;
                            invalidate_cache(0, rsi199, 0, rsi199);
                            rsp177 = rsp177 - 8 + 8 - 8 + 8;
                            zf260 = input_seekable == 0;
                            if (zf260) {
                                eax261 = input_seek_errno;
                                if (eax261 == 29) 
                                    goto addr_3875_280;
                                *reinterpret_cast<void***>(v63) = eax261;
                                goto addr_381e_282;
                            }
                            rax262 = input_offset;
                            if (reinterpret_cast<signed char>(rax262) < reinterpret_cast<signed char>(0)) 
                                goto addr_3988_284;
                            rax263 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax262) + reinterpret_cast<unsigned char>(rbp259));
                            input_offset = rax263;
                            if (__intrinsic()) {
                                input_offset = reinterpret_cast<void**>(0xffffffffffffffff);
                                goto addr_3988_284;
                            }
                            if (reinterpret_cast<signed char>(rax263) < reinterpret_cast<signed char>(0)) {
                                addr_3988_284:
                                rsi264 = input_file;
                                rax265 = quotearg_style(4, rsi264, 4, rsi264);
                                rax266 = fun_2660();
                                rcx48 = rax265;
                                rsi199 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                                *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                                rdx197 = rax266;
                                nl_error(0, 0, rdx197, rcx48, r8_23, r9_25);
                                rsp177 = rsp177 - 8 + 8 - 8 + 8 - 8 + 8;
                            } else {
                                rsi199 = reinterpret_cast<void**>(0);
                                *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                                *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                                *reinterpret_cast<uint32_t*>(&rdx197) = 1;
                                *reinterpret_cast<int32_t*>(&rdx197 + 4) = 0;
                                rax267 = fun_2710();
                                rsp177 = rsp177 - 8 + 8;
                                if (reinterpret_cast<signed char>(rax267) < reinterpret_cast<signed char>(0)) {
                                    addr_381e_282:
                                    rax268 = quotearg_n_style_colon();
                                    rax269 = fun_2660();
                                    rcx48 = rax268;
                                    *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                                    *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                                    rdx197 = rax269;
                                    rsi199 = *reinterpret_cast<void***>(v63);
                                    *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
                                    nl_error(0, rsi199, rdx197, rcx48, r8_23, r9_25);
                                    rsp177 = rsp177 - 8 + 8 - 8 + 8 - 8 + 8;
                                } else {
                                    if (rax263 == rax267) {
                                        addr_3875_280:
                                        zf270 = (*reinterpret_cast<unsigned char*>(&conversions_mask + 1) & 4) == 0;
                                        if (zf270) 
                                            goto addr_35c8_227;
                                        if (r12_222) 
                                            goto addr_35c8_227; else 
                                            goto addr_388b_291;
                                    } else {
                                        r13_271 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax263) - reinterpret_cast<unsigned char>(rax267));
                                        if ((reinterpret_cast<signed char>(r13_271) < reinterpret_cast<signed char>(0) || reinterpret_cast<signed char>(rbp259) < reinterpret_cast<signed char>(r13_271)) && (zf272 = status_level == 1, !zf272)) {
                                            rax273 = fun_2660();
                                            nl_error(0, 0, rax273, rcx48, r8_23, r9_25);
                                            rsp177 = rsp177 - 8 + 8 - 8 + 8;
                                        }
                                        *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                                        *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                                        *reinterpret_cast<uint32_t*>(&rdx197) = 1;
                                        *reinterpret_cast<int32_t*>(&rdx197 + 4) = 0;
                                        rsi199 = r13_271;
                                        rax274 = fun_2710();
                                        rsp177 = rsp177 - 8 + 8;
                                        if (reinterpret_cast<signed char>(rax274) >= reinterpret_cast<signed char>(0)) 
                                            goto addr_3875_280; else 
                                            goto addr_3acd_295;
                                    }
                                }
                            }
                            input_seekable = 0;
                            input_seek_errno = reinterpret_cast<void**>(29);
                            v32 = 1;
                            goto addr_3875_280;
                            addr_388b_291:
                            r13_19 = reinterpret_cast<void**>(0);
                            *reinterpret_cast<int32_t*>(&r13_19 + 4) = 0;
                            goto addr_368e_245;
                            addr_3acd_295:
                            if (!*reinterpret_cast<void***>(v63)) {
                                rax275 = fun_2660();
                                nl_error(0, 0, rax275, rcx48, r8_23, r9_25);
                                rsp177 = rsp177 - 8 + 8 - 8 + 8;
                                goto addr_381e_282;
                            }
                            rsi199 = r13_19;
                            rdi84 = r15_239;
                            copy_with_unblock(rdi84, rsi199, rdx197);
                            rsp177 = rsp177 - 8 + 8;
                            zf276 = status_level == 4;
                        } while (!zf276);
                    }
                }
                addr_38ae_247:
                rsi199 = r13_19;
                rdi84 = r15_239;
                rax277 = iwrite_constprop_0(rdi84, rsi199, rdx197, rcx48, r8_23, r9_25);
                rsp177 = rsp177 - 8 + 8;
                tmp64_278 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(w_bytes) + reinterpret_cast<unsigned char>(rax277));
                w_bytes = tmp64_278;
                if (rax277 != r13_19) 
                    goto addr_3b2d_299;
                zf279 = input_blocksize == r13_19;
                if (zf279) {
                    tmp64_280 = w_full + 1;
                    w_full = tmp64_280;
                    continue;
                } else {
                    tmp64_281 = w_partial + 1;
                    w_partial = tmp64_281;
                    continue;
                }
                addr_394e_257:
                rsi199 = r13_19;
                rdi84 = r15_239;
                copy_with_block(rdi84, rsi199, rdx197);
                rsp177 = rsp177 - 8 + 8;
                continue;
                addr_38ed_267:
                rdi282 = rax240;
                while (1) {
                    r14_283 = oc;
                    rbp284 = output_blocksize;
                    rsi199 = r15_239;
                    rbp285 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp284) - reinterpret_cast<unsigned char>(r14_283));
                    if (reinterpret_cast<signed char>(rbp285) > reinterpret_cast<signed char>(r13_19)) {
                        rbp285 = r13_19;
                    }
                    rdi84 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi282) + reinterpret_cast<unsigned char>(r14_283));
                    rdx197 = rbp285;
                    r14_286 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_283) + reinterpret_cast<unsigned char>(rbp285));
                    r13_19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_19) - reinterpret_cast<unsigned char>(rbp285));
                    r15_239 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_239) + reinterpret_cast<unsigned char>(rbp285));
                    fun_27e0(rdi84, rsi199, rdx197, rdi84, rsi199, rdx197);
                    rsp177 = rsp177 - 8 + 8;
                    less287 = reinterpret_cast<signed char>(r14_286) < reinterpret_cast<signed char>(output_blocksize);
                    oc = r14_286;
                    if (!less287) {
                        write_output(rdi84, rdi84);
                        rsp177 = rsp177 - 8 + 8;
                    }
                    if (!r13_19) 
                        break;
                    rdi282 = obuf;
                }
            }
        }
    }
    v288 = eax219;
    rax289 = max_records;
    rax290 = reinterpret_cast<unsigned char>(rax289) | reinterpret_cast<unsigned char>(max_bytes);
    if (rax290) {
        while (1) {
            if (*reinterpret_cast<signed char*>(&rdx220) || (zf291 = i_nocache_eof == 0, !zf291)) {
                rsi199 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                invalidate_cache(0, 0);
            }
            zf292 = o_nocache == 0;
            if (!zf292 || (zf293 = o_nocache_eof == 0, !zf293)) {
                rsi199 = reinterpret_cast<void**>(0);
                *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdi84) = 1;
                *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                invalidate_cache(1, 0);
            }
            addr_3bad_314:
            process_signals(rdi84, rsi199);
            cleanup(rdi84, rsi199, rdx220, rcx48, r8_23, r9_25);
            print_stats();
            *reinterpret_cast<uint32_t*>(&rdi84) = v288;
            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
            fun_2910();
        }
    } else {
        if (*reinterpret_cast<signed char*>(&rdx220) && (rsi199 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi84) = 0, *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0, al294 = invalidate_cache(0, 0), !al294)) {
            rax295 = quotearg_n_style_colon();
            rax296 = fun_2660();
            rcx48 = rax295;
            *reinterpret_cast<uint32_t*>(&rdi84) = 0;
            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
            rdx220 = rax296;
            rsi199 = *reinterpret_cast<void***>(v63);
            *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
            nl_error(0, rsi199, rdx220, rcx48, r8_23, r9_25);
            v288 = 1;
        }
        zf297 = o_nocache == 0;
        if (zf297) 
            goto addr_3bad_314;
        rsi199 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdi84) = 1;
        *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
        al298 = invalidate_cache(1, 0);
        if (al298) 
            goto addr_3bad_314;
        rax299 = quotearg_n_style_colon();
        rax300 = fun_2660();
        rcx48 = rax299;
        *reinterpret_cast<uint32_t*>(&rdi84) = 0;
        *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
        rdx220 = rax300;
        rsi199 = *reinterpret_cast<void***>(v63);
        *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
        nl_error(0, rsi199, rdx220, rcx48, r8_23, r9_25);
        v288 = 1;
        goto addr_3bad_314;
    }
    addr_3dc0_231:
    if (v30 != 0xffffffff) {
        if (*reinterpret_cast<unsigned char*>(&eax231) & 8) {
            rdi84 = reinterpret_cast<void**>(rsp177 + 88);
            copy_with_block(rdi84, 1, rdx197, rdi84, 1, rdx197);
            eax231 = conversions_mask;
        } else {
            if (*reinterpret_cast<unsigned char*>(&eax231) & 16) {
                rdi84 = reinterpret_cast<void**>(rsp177 + 88);
                copy_with_unblock(rdi84, 1, rdx197, rdi84, 1, rdx197);
                eax231 = conversions_mask;
            } else {
                rcx48 = oc;
                rsi301 = obuf;
                ebx302 = *reinterpret_cast<unsigned char*>(&v30);
                rdx197 = rcx48 + 1;
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi301) + reinterpret_cast<unsigned char>(rcx48)) = *reinterpret_cast<signed char*>(&ebx302);
                less303 = reinterpret_cast<signed char>(rdx197) < reinterpret_cast<signed char>(output_blocksize);
                oc = rdx197;
                if (!less303) {
                    write_output(rdi84);
                    eax231 = conversions_mask;
                }
            }
        }
    }
    rbx304 = col;
    if (!(*reinterpret_cast<unsigned char*>(&eax231) & 8) || reinterpret_cast<uint1_t>(rbx304 < 0) | reinterpret_cast<uint1_t>(rbx304 == 0)) {
        addr_3e71_327:
        zf305 = col == 0;
        if (!zf305) {
            eax231 = conversions_mask;
        } else {
            addr_3e7f_329:
            rsi199 = oc;
            goto addr_3e86_330;
        }
    } else {
        less_or_equal306 = conversion_blocksize <= rbx304;
        if (!less_or_equal306) {
            do {
                rdx197 = oc;
                rcx48 = obuf;
                esi307 = space_character;
                rax308 = rdx197 + 1;
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx48) + reinterpret_cast<unsigned char>(rdx197)) = *reinterpret_cast<signed char*>(&esi307);
                less309 = reinterpret_cast<signed char>(rax308) < reinterpret_cast<signed char>(output_blocksize);
                oc = rax308;
                if (!less309) {
                    write_output(rdi84);
                }
                ++rbx304;
                less310 = rbx304 < conversion_blocksize;
            } while (less310);
            goto addr_3e71_327;
        }
    }
    rsi199 = oc;
    if (!(*reinterpret_cast<unsigned char*>(&eax231) & 16) || (rdx197 = obuf, rcx48 = reinterpret_cast<void**>(static_cast<uint32_t>(newline_character)), *reinterpret_cast<int32_t*>(&rcx48 + 4) = 0, rax311 = rsi199 + 1, oc = rax311, *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx197) + reinterpret_cast<unsigned char>(rsi199)) = *reinterpret_cast<unsigned char*>(&rcx48), less312 = reinterpret_cast<signed char>(rax311) < reinterpret_cast<signed char>(output_blocksize), rsi199 = rax311, less312)) {
        addr_3e86_330:
        if (rsi199) {
            rdi84 = obuf;
            rax313 = iwrite_constprop_0(rdi84, rsi199, rdx197, rcx48, r8_23, r9_25);
            tmp64_314 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(w_bytes) + reinterpret_cast<unsigned char>(rax313));
            w_bytes = tmp64_314;
            if (rax313) {
                tmp64_315 = w_partial + 1;
                w_partial = tmp64_315;
            }
            zf316 = rax313 == oc;
            if (zf316) 
                goto addr_3e8f_340;
        } else {
            addr_3e8f_340:
            zf317 = final_op_was_seek == 0;
            if (!zf317) 
                goto addr_4371_341; else 
                goto addr_3e9c_342;
        }
    } else {
        write_output(rdi84);
        goto addr_3e7f_329;
    }
    addr_3b2d_299:
    rsi318 = output_file;
    rax319 = quotearg_style(4, rsi318, 4, rsi318);
    rax320 = fun_2660();
    rcx48 = rax319;
    *reinterpret_cast<uint32_t*>(&rdi84) = 0;
    *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
    rdx197 = rax320;
    rsi199 = *reinterpret_cast<void***>(v63);
    *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
    nl_error(0, rsi199, rdx197, rcx48, r8_23, r9_25);
    v32 = 1;
    goto addr_3b71_222;
    addr_4371_341:
    rsi199 = v10;
    *reinterpret_cast<uint32_t*>(&rdi84) = 1;
    *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
    eax321 = ifstat(1, rsi199, 1, rsi199);
    if (eax321) {
        rsi322 = output_file;
        rax323 = quotearg_style(4, rsi322, 4, rsi322);
        rax324 = fun_2660();
        rcx48 = rax323;
        *reinterpret_cast<uint32_t*>(&rdi84) = 0;
        *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
        rdx197 = rax324;
        rsi199 = *reinterpret_cast<void***>(v63);
        *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
        nl_error(0, rsi199, rdx197, rcx48, r8_23, r9_25);
    } else {
        if ((v325 & reinterpret_cast<uint32_t>("yte) blocks")) != 0x8000 || ((*reinterpret_cast<uint32_t*>(&rdx197) = 1, *reinterpret_cast<int32_t*>(&rdx197 + 4) = 0, rsi199 = reinterpret_cast<void**>(0), *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0, *reinterpret_cast<uint32_t*>(&rdi84) = 1, *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0, rax326 = fun_2710(), reinterpret_cast<signed char>(rax326) < reinterpret_cast<signed char>(0)) || (reinterpret_cast<signed char>(rax326) <= reinterpret_cast<signed char>(v327) || (rdi84 = rax326, eax328 = iftruncate_constprop_0(rdi84, rdi84), eax328 == 0)))) {
            addr_3e9c_342:
            zf329 = (reinterpret_cast<unsigned char>(conversions_mask) & 0xc000) == 0;
            if (!zf329 && ((zf330 = status_level == 4, zf330) && ((rax331 = reported_w_bytes, reinterpret_cast<signed char>(rax331) >= reinterpret_cast<signed char>(0)) && (less332 = reinterpret_cast<signed char>(rax331) < reinterpret_cast<signed char>(w_bytes), less332)))) {
                *reinterpret_cast<uint32_t*>(&rdi84) = 0;
                *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
                print_xfer_stats();
                goto addr_3b71_222;
            }
        } else {
            rsi333 = output_file;
            rax334 = quotearg_style(4, rsi333, 4, rsi333);
            rax335 = fun_2660();
            r8_23 = rax334;
            rcx48 = rax326;
            *reinterpret_cast<uint32_t*>(&rdi84) = 0;
            *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
            rdx197 = rax335;
            rsi199 = *reinterpret_cast<void***>(v63);
            *reinterpret_cast<int32_t*>(&rsi199 + 4) = 0;
            nl_error(0, rsi199, rdx197, rcx48, r8_23, r9_25);
        }
    }
    v32 = 1;
    goto addr_3b71_222;
    addr_3d86_241:
    edx336 = i_nocache;
    i_nocache_eof = reinterpret_cast<unsigned char>(i_nocache_eof | *reinterpret_cast<unsigned char*>(&edx336));
    zf337 = o_nocache == 0;
    edx338 = o_nocache_eof;
    if (!zf337) {
        ebx339 = 0;
        *reinterpret_cast<unsigned char*>(&ebx339) = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(&eax231 + 1) & 2) == 0);
        *reinterpret_cast<uint32_t*>(&v35) = ebx339;
    }
    *reinterpret_cast<uint32_t*>(&rdx197) = edx338 | *reinterpret_cast<uint32_t*>(&v35);
    *reinterpret_cast<int32_t*>(&rdx197 + 4) = 0;
    o_nocache_eof = *reinterpret_cast<unsigned char*>(&rdx197);
    o_nocache_eof = reinterpret_cast<unsigned char>(o_nocache_eof & 1);
    goto addr_3dc0_231;
    addr_4434_278:
    v32 = 1;
    goto addr_3dc0_231;
    addr_355e_218:
    r12_211 = v205;
    rdi84 = obuf;
    if (!r12_211) 
        goto addr_3580_204; else 
        goto addr_356f_220;
    addr_42a6_96:
    rax340 = fun_2660();
    rsi71 = r13_19;
    *reinterpret_cast<int32_t*>(&rsi71 + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rdi84) = 1;
    *reinterpret_cast<int32_t*>(&rdi84 + 4) = 0;
    rdx85 = rax340;
    output_file = rax340;
    set_fd_flags(1, rsi71, rdx85, rcx48, r8_23, r9_25);
    rsp73 = rsp73 - 8 + 8 - 8 + 8;
    goto addr_3409_196;
    addr_3115_164:
    rax341 = quote(rbp5, rsi43, rdx26, rcx22, r8_23, r9_25);
    rbx20 = rax341;
    rax342 = fun_2660();
    rsp61 = rsp27 - 2 + 2 - 2 + 2;
    esi62 = reinterpret_cast<void**>(0);
    rcx22 = rax342;
    goto addr_3138_56;
    addr_2cbf_189:
    if (1) 
        goto addr_317e_13;
    output_blocksize = reinterpret_cast<void**>(0);
    input_blocksize = reinterpret_cast<void**>(0);
    goto addr_2cdb_17;
    addr_3f94_151:
    rax343 = quote(rbp5, rsi43, 61, rcx22, r8_23, r9_25);
    rbx20 = rax343;
    rax344 = fun_2660();
    rsp61 = rsp27 - 2 + 2 - 2 + 2;
    esi62 = reinterpret_cast<void**>(75);
    rcx22 = rax344;
    goto addr_3138_56;
}

int64_t __libc_start_main = 0;

void fun_4893() {
    __asm__("cli ");
    __libc_start_main(0x2a10, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x14008;

void fun_24f0(int64_t rdi);

int64_t fun_4933() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_24f0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_4973() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void fun_49b3(uint32_t edi) {
    __asm__("cli ");
    interrupt_signal = edi;
    return;
}

void fun_49c3() {
    int32_t eax1;

    __asm__("cli ");
    eax1 = info_signal_count;
    info_signal_count = eax1 + 1;
    return;
}

int32_t close_stream(void** rdi);

void** fun_25b0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_4ce3() {
    int1_t zf1;
    void** rdi2;
    int32_t eax3;
    int64_t rsi4;
    int64_t rdx5;
    void** rcx6;
    void** r8_7;

    __asm__("cli ");
    zf1 = close_stdout_required == 0;
    if (zf1) {
        rdi2 = stderr;
        eax3 = close_stream(rdi2);
        if (eax3) {
            fun_25b0(1, rsi4, rdx5, rcx6, r8_7);
        } else {
            return;
        }
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2860(int64_t rdi, void** rsi, void** rdx, void** rcx);

void** stdout = reinterpret_cast<void**>(0);

void fun_2780(void** rdi, void** rsi, void** rdx, void** rcx);

unsigned char __cxa_finalize;

unsigned char g1;

signed char g2;

void fun_4ef3(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** r12_4;
    void** rax5;
    void** r12_6;
    void** rax7;
    void** r12_8;
    void** rax9;
    void** r12_10;
    void** rax11;
    void** r12_12;
    void** rax13;
    void** r12_14;
    void** rax15;
    void** r12_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** r12_22;
    void** rax23;
    void** r12_24;
    void** rax25;
    void** r12_26;
    void** rax27;
    void** r12_28;
    void** rax29;
    void** r12_30;
    void** rax31;
    void** r12_32;
    void** rax33;
    void** r12_34;
    void** rax35;
    void** rax36;
    void** r12_37;
    void** rax38;
    void** r12_39;
    void** rax40;
    uint32_t ecx41;
    uint32_t ecx42;
    int1_t zf43;
    void** rax44;
    void** rax45;
    int32_t eax46;
    void** rax47;
    void** r13_48;
    void** rax49;
    void** rax50;
    int32_t eax51;
    void** rax52;
    void** r14_53;
    void** rax54;
    void** rax55;
    void** rax56;
    void** rdi57;

    __asm__("cli ");
    r12_2 = program_name;
    if (!edi) {
        while (1) {
            rax3 = fun_2660();
            fun_2860(1, rax3, r12_2, r12_2);
            r12_4 = stdout;
            rax5 = fun_2660();
            fun_2780(rax5, r12_4, 5, r12_2);
            r12_6 = stdout;
            rax7 = fun_2660();
            fun_2780(rax7, r12_6, 5, r12_2);
            r12_8 = stdout;
            rax9 = fun_2660();
            fun_2780(rax9, r12_8, 5, r12_2);
            r12_10 = stdout;
            rax11 = fun_2660();
            fun_2780(rax11, r12_10, 5, r12_2);
            r12_12 = stdout;
            rax13 = fun_2660();
            fun_2780(rax13, r12_12, 5, r12_2);
            r12_14 = stdout;
            rax15 = fun_2660();
            fun_2780(rax15, r12_14, 5, r12_2);
            r12_16 = stdout;
            rax17 = fun_2660();
            fun_2780(rax17, r12_16, 5, r12_2);
            r12_18 = stdout;
            rax19 = fun_2660();
            fun_2780(rax19, r12_18, 5, r12_2);
            r12_20 = stdout;
            rax21 = fun_2660();
            fun_2780(rax21, r12_20, 5, r12_2);
            r12_22 = stdout;
            rax23 = fun_2660();
            fun_2780(rax23, r12_22, 5, r12_2);
            r12_24 = stdout;
            rax25 = fun_2660();
            fun_2780(rax25, r12_24, 5, r12_2);
            r12_26 = stdout;
            rax27 = fun_2660();
            fun_2780(rax27, r12_26, 5, r12_2);
            r12_28 = stdout;
            rax29 = fun_2660();
            fun_2780(rax29, r12_28, 5, r12_2);
            r12_30 = stdout;
            rax31 = fun_2660();
            fun_2780(rax31, r12_30, 5, r12_2);
            r12_32 = stdout;
            rax33 = fun_2660();
            fun_2780(rax33, r12_32, 5, r12_2);
            r12_34 = stdout;
            rax35 = fun_2660();
            fun_2780(rax35, r12_34, 5, r12_2);
            rax36 = fun_2660();
            fun_2860(1, rax36, "USR1", r12_2);
            r12_37 = stdout;
            rax38 = fun_2660();
            fun_2780(rax38, r12_37, 5, r12_2);
            r12_39 = stdout;
            rax40 = fun_2660();
            fun_2780(rax40, r12_39, 5, r12_2);
            do {
                if (1) 
                    break;
                ecx41 = __cxa_finalize;
            } while (100 != ecx41 || ((ecx42 = g1, 100 != ecx42) || (zf43 = g2 == 0, !zf43)));
            r12_2 = reinterpret_cast<void**>(0);
            if (1) {
                rax44 = fun_2660();
                fun_2860(1, rax44, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax45 = fun_2850(5);
                if (!rax45 || (eax46 = fun_25a0(rax45, "en_", 3, rax45, "en_", 3), !eax46)) {
                    rax47 = fun_2660();
                    r12_2 = reinterpret_cast<void**>("dd");
                    r13_48 = reinterpret_cast<void**>(" invocation");
                    fun_2860(1, rax47, "https://www.gnu.org/software/coreutils/", "dd");
                } else {
                    r12_2 = reinterpret_cast<void**>("dd");
                    goto addr_5463_9;
                }
            } else {
                rax49 = fun_2660();
                fun_2860(1, rax49, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax50 = fun_2850(5);
                if (!rax50 || (eax51 = fun_25a0(rax50, "en_", 3, rax50, "en_", 3), !eax51)) {
                    addr_5366_11:
                    rax52 = fun_2660();
                    r13_48 = reinterpret_cast<void**>(" invocation");
                    fun_2860(1, rax52, "https://www.gnu.org/software/coreutils/", "dd");
                    if (!reinterpret_cast<int1_t>(r12_2 == "dd")) {
                        r13_48 = reinterpret_cast<void**>(0xff61);
                    }
                } else {
                    addr_5463_9:
                    r14_53 = stdout;
                    rax54 = fun_2660();
                    fun_2780(rax54, r14_53, 5, "https://www.gnu.org/software/coreutils/");
                    goto addr_5366_11;
                }
            }
            rax55 = fun_2660();
            fun_2860(1, rax55, r12_2, r13_48);
            addr_4f49_14:
            fun_2910();
        }
    } else {
        rax56 = fun_2660();
        rdi57 = stderr;
        fun_2930(rdi57, 1, rax56, rdi57, 1, rax56);
        goto addr_4f49_14;
    }
}

void** fun_2760(void** rdi, void** rsi, void** rdx);

/* prev_nread.6 */
void** prev_nread_6 = reinterpret_cast<void**>(0);

void** fun_5ce3(int32_t edi, void** rsi, void** rdx) {
    void** rdi1;
    int32_t r13d4;
    void** rbp5;
    void** rbx6;
    void** rax7;
    void** r12_8;
    void** rax9;
    void** edx10;
    void** rax11;
    int1_t zf12;
    void** r13_13;
    int1_t zf14;
    void** rax15;
    void** r9_16;
    void** rdx17;
    int1_t zf18;

    *reinterpret_cast<int32_t*>(&rdi1) = edi;
    __asm__("cli ");
    r13d4 = *reinterpret_cast<int32_t*>(&rdi1);
    rbp5 = rsi;
    rbx6 = rdx;
    do {
        process_signals(rdi1, rsi);
        rsi = rbp5;
        *reinterpret_cast<int32_t*>(&rdi1) = r13d4;
        *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
        rax7 = fun_2760(rdi1, rsi, rbx6);
        r12_8 = rax7;
        if (!reinterpret_cast<int1_t>(rax7 == 0xffffffffffffffff)) {
            if (reinterpret_cast<signed char>(rax7) >= reinterpret_cast<signed char>(0)) 
                break;
            rax9 = fun_2570();
            edx10 = *reinterpret_cast<void***>(rax9);
        } else {
            rax11 = fun_2570();
            edx10 = *reinterpret_cast<void***>(rax11);
            if (reinterpret_cast<int1_t>(edx10 == 22)) 
                goto addr_5d39_6;
        }
    } while (reinterpret_cast<int1_t>(edx10 == 4));
    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r12_8) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r12_8 == 0))) {
        if (reinterpret_cast<signed char>(rbx6) <= reinterpret_cast<signed char>(r12_8)) {
            if (!reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r12_8) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r12_8 == 0))) {
                addr_5d6f_11:
                zf12 = warn_partial_read == 0;
                if (!zf12 && ((r13_13 = prev_nread_6, reinterpret_cast<signed char>(r13_13) < reinterpret_cast<signed char>(rbx6)) && !(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(r13_13) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(r13_13 == 0)))) {
                    zf14 = status_level == 1;
                    if (!zf14) {
                        rax15 = fun_28c0();
                        nl_error(0, 0, rax15, r13_13, 5, r9_16);
                    }
                    warn_partial_read = 0;
                }
            }
        } else {
            process_signals(rdi1, rsi, rdi1, rsi);
            goto addr_5d6f_11;
        }
    }
    addr_5d4a_17:
    prev_nread_6 = r12_8;
    return r12_8;
    addr_5d39_6:
    rdx17 = prev_nread_6;
    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rdx17) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rdx17 == 0)) && (reinterpret_cast<signed char>(rdx17) < reinterpret_cast<signed char>(rbx6) && (zf18 = (*reinterpret_cast<unsigned char*>(&input_flags + 1) & 64) == 0, !zf18))) {
        *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r12_8) = 0;
        *reinterpret_cast<int32_t*>(&r12_8 + 4) = 0;
        goto addr_5d4a_17;
    }
}

int64_t fun_5e03(int32_t edi, int64_t rsi, int64_t rdx) {
    int64_t r13_4;
    int32_t r12d5;
    int64_t rbp6;
    int64_t rbx7;
    int64_t rdi8;
    int64_t rax9;

    __asm__("cli ");
    if (reinterpret_cast<uint1_t>(rdx < 0) | reinterpret_cast<uint1_t>(rdx == 0)) {
        return 0;
    }
    *reinterpret_cast<int32_t*>(&r13_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_4) + 4) = 0;
    r12d5 = edi;
    rbp6 = rsi;
    rbx7 = rdx;
    do {
        *reinterpret_cast<int32_t*>(&rdi8) = r12d5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
        rax9 = iread(rdi8, rbp6, rbx7);
        if (rax9 < 0) 
            break;
    } while (rax9 && (rbx7 = rbx7 - rax9, r13_4 = r13_4 + rax9, rbp6 = rbp6 + rax9, !(reinterpret_cast<uint1_t>(rbx7 < 0) | reinterpret_cast<uint1_t>(rbx7 == 0))));
    goto addr_5e60_6;
    return rax9;
    addr_5e60_6:
    return r13_4;
}

int64_t fun_25c0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_6963(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_25c0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_69be_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2570();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_69be_3;
            rax6 = fun_2570();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

int64_t file_name = 0;

void fun_69d3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_69e3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

void** quotearg_colon();

void fun_2880();

int32_t exit_failure = 1;

void fun_69f3() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2570(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2660();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_6a83_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2880();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_25b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_6a83_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2880();
    }
}

struct s7 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    void** f90;
};

int32_t fun_27f0(struct s7* rdi);

int32_t fun_2830(struct s7* rdi);

int32_t rpl_fflush(struct s7* rdi);

int64_t fun_2640(struct s7* rdi);

int64_t fun_6aa3(struct s7* rdi) {
    int32_t eax2;
    int32_t eax3;
    void** rax4;
    int32_t eax5;
    void** rax6;
    void** r12d7;
    int64_t rax8;

    __asm__("cli ");
    eax2 = fun_27f0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2830(rdi);
        if (!(eax3 && (fun_27f0(rdi), rax4 = fun_2710(), reinterpret_cast<int1_t>(rax4 == 0xffffffffffffffff)) || (eax5 = rpl_fflush(rdi), eax5 == 0))) {
            rax6 = fun_2570();
            r12d7 = *reinterpret_cast<void***>(rax6);
            rax8 = fun_2640(rdi);
            if (r12d7) {
                *reinterpret_cast<void***>(rax6) = r12d7;
                *reinterpret_cast<int32_t*>(&rax8) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
            }
            return rax8;
        }
    }
    goto fun_2640;
}

uint32_t fun_2600(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_6b33(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void* rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_2600(rdi);
        r12d10 = eax9;
        goto addr_6c34_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_2600(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_6c34_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax8) - reinterpret_cast<uint64_t>(g28));
                if (rax14) {
                    fun_2690();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_6ce9_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_2600(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_2600(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_2570();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_2740();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_6c34_3;
                }
            }
        } else {
            eax22 = fun_2600(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_2570(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_6c34_3;
            } else {
                eax25 = fun_2600(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_6c34_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_6ce9_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_6b99_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_6b9d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_6b9d_18:
            if (0) {
            }
        } else {
            addr_6be5_23:
            eax28 = fun_2600(rdi);
            r12d10 = eax28;
            goto addr_6c34_3;
        }
        eax29 = fun_2600(rdi);
        r12d10 = eax29;
        goto addr_6c34_3;
    }
    if (0) {
    }
    eax30 = fun_2600(rdi);
    r12d10 = eax30;
    goto addr_6c34_3;
    addr_6b99_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_6b9d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_6be5_23;
        goto addr_6b9d_18;
    }
}

int32_t fun_28a0(int64_t rdi, int64_t rsi, int64_t rdx);

int32_t fun_26c0(int64_t rdi, int64_t rsi, int64_t rdx);

int64_t fun_6da3(int32_t edi, int64_t rsi, int32_t edx, int32_t ecx) {
    int64_t rsi5;
    int64_t rdx6;
    int32_t eax7;
    int64_t rax8;
    int64_t rsi9;
    int64_t rdi10;
    int32_t eax11;
    void** rax12;
    void** r13d13;
    int64_t rax14;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rsi5) = edx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi5) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx6) = ecx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
    eax7 = fun_28a0(rsi, rsi5, rdx6);
    if (edi == eax7 || eax7 < 0) {
        *reinterpret_cast<int32_t*>(&rax8) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
        return rax8;
    } else {
        *reinterpret_cast<int32_t*>(&rsi9) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi10) = eax7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
        eax11 = fun_26c0(rdi10, rsi9, rdx6);
        rax12 = fun_2570();
        r13d13 = *reinterpret_cast<void***>(rax12);
        fun_2740();
        *reinterpret_cast<int32_t*>(&rax14) = eax11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
        *reinterpret_cast<void***>(rax12) = r13d13;
        return rax14;
    }
}

void rpl_fseeko(struct s7* rdi);

void fun_6e23(struct s7* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2830(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_6e73(struct s7* rdi, int64_t rsi, int32_t edx) {
    void** rax4;
    int64_t rax5;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        fun_27f0(rdi);
        rax4 = fun_2710();
        if (rax4 == 0xffffffffffffffff) {
            *reinterpret_cast<uint32_t*>(&rax5) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax4;
            *reinterpret_cast<uint32_t*>(&rax5) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        }
        return rax5;
    }
}

int32_t fun_2610();

void gettime(void* rdi, void* rsi);

int64_t fun_6ef3() {
    void* rax1;
    void* rbp2;
    int32_t eax3;
    void* rdx4;
    int64_t v5;
    int64_t v6;

    __asm__("cli ");
    rax1 = g28;
    rbp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 32);
    eax3 = fun_2610();
    if (eax3) {
        gettime(rbp2, rbp2);
    }
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax1) - reinterpret_cast<uint64_t>(g28));
    if (rdx4) {
        fun_2690();
    } else {
        return v5 * 0x3b9aca00 + v6;
    }
}

void fun_6f53(int64_t rdi) {
    __asm__("cli ");
    goto fun_2610;
}

int64_t fun_6f63() {
    void* rax1;
    void* rcx2;
    int64_t v3;

    __asm__("cli ");
    rax1 = g28;
    fun_2610();
    rcx2 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax1) - reinterpret_cast<uint64_t>(g28));
    if (rcx2) {
        fun_2690();
    } else {
        return v3;
    }
}

struct s8 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    unsigned char* f10;
};

struct s8* fun_25f0();

void fun_29b0(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_2870(void** rdi, void** rsi);

void fun_27c0(void** rdi, void** rsi, void** rdx, int64_t rcx);

void** fun_6fb3(uint64_t rdi, void** rsi, uint32_t edx, uint64_t rcx, uint64_t r8) {
    void** v6;
    uint64_t v7;
    uint32_t v8;
    void* rax9;
    void* v10;
    uint32_t edx11;
    uint32_t v12;
    uint32_t eax13;
    uint32_t v14;
    uint32_t v15;
    struct s8* rax16;
    void** r15_17;
    void** rax18;
    void** r14_19;
    void** rbp20;
    int1_t cf21;
    unsigned char* v22;
    void** rax23;
    void*** rsp24;
    void** v25;
    uint64_t r8_26;
    uint64_t rdx27;
    uint64_t rcx28;
    uint64_t rax29;
    uint64_t rax30;
    uint64_t rdx31;
    uint64_t rax32;
    uint64_t rdx33;
    int64_t rdi34;
    uint32_t esi35;
    uint32_t r10d36;
    uint64_t v37;
    int64_t rbx38;
    uint64_t rax39;
    int1_t zf40;
    void** rax41;
    void*** rsp42;
    void** rdx43;
    void** r12_44;
    void** rbp45;
    void** r8_46;
    void** r13_47;
    void** rax48;
    void* rsp49;
    void** r12_50;
    void** rax51;
    void** v52;
    void* rsp53;
    uint32_t v54;
    void** rbp55;
    void** rbx56;
    unsigned char* r12_57;
    void** rax58;
    void** rsi59;
    uint64_t rcx60;
    uint64_t rdx61;
    uint64_t rax62;
    uint32_t eax63;
    void** rdx64;
    uint32_t ecx65;
    void* rax66;
    void** rax67;
    uint32_t tmp32_68;
    int1_t cf69;
    void** rbp70;
    void** rax71;
    uint64_t rax72;
    int1_t zf73;
    void** rax74;
    int1_t cf75;
    int1_t below_or_equal76;
    uint64_t rax77;
    void** rax78;
    uint64_t rax79;
    int1_t zf80;
    uint64_t r8_81;
    uint64_t r11_82;
    uint64_t rdx83;
    int64_t rcx84;
    uint64_t r9_85;
    int64_t rax86;
    int32_t eax87;
    int64_t rdx88;
    int64_t rax89;
    uint32_t edx90;
    uint32_t esi91;
    uint64_t rax92;
    int64_t rax93;
    uint32_t esi94;
    int64_t rdx95;
    uint32_t eax96;
    uint64_t rax97;
    uint64_t rdx98;
    uint64_t rdi99;
    uint64_t rax100;
    int32_t eax101;
    uint64_t rax102;
    void* rcx103;
    void* rax104;
    void* rax105;
    uint32_t eax106;
    uint32_t eax107;
    uint32_t edx108;
    void* rsi109;
    uint32_t edx110;
    uint32_t edx111;
    void* rax112;
    void* rsi113;
    void* rax114;
    void* rax115;
    void* rdi116;
    uint32_t eax117;
    uint32_t r9d118;
    uint32_t eax119;
    void* rdx120;
    uint32_t edx121;
    uint32_t edx122;

    __asm__("cli ");
    v6 = rsi;
    v7 = r8;
    v8 = edx;
    rax9 = g28;
    v10 = rax9;
    edx11 = edx & 32;
    v12 = edx11;
    eax13 = edx & 3;
    v14 = eax13;
    v15 = (eax13 - (eax13 + reinterpret_cast<uint1_t>(eax13 < eax13 + reinterpret_cast<uint1_t>(edx11 < 1))) & 0xffffffe8) + 0x400;
    rax16 = fun_25f0();
    r15_17 = rax16->f0;
    rax18 = fun_2680(r15_17);
    r14_19 = rax16->f8;
    rbp20 = rax18;
    cf21 = reinterpret_cast<uint64_t>(rax18 - 1) < 16;
    v22 = rax16->f10;
    if (!cf21) {
        rbp20 = reinterpret_cast<void**>(1);
    }
    if (!cf21) {
        r15_17 = reinterpret_cast<void**>(".");
    }
    rax23 = fun_2680(r14_19);
    rsp24 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x98 - 8 + 8 - 8 + 8 - 8 + 8);
    if (reinterpret_cast<unsigned char>(rax23) > reinterpret_cast<unsigned char>(16)) {
        r14_19 = reinterpret_cast<void**>(0xff61);
    }
    v25 = v6 + 0x287;
    if (r8 > rcx) {
        if (!rcx || (r8_26 = v7 / rcx, !!(v7 % rcx))) {
            addr_7094_9:
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<int64_t>(rcx) < reinterpret_cast<int64_t>(0)) {
                __asm__("fadd dword [rip+0x86ee]");
            }
        } else {
            rdx27 = rdi % r8_26;
            rcx28 = rdi / r8_26;
            rax29 = rdx27 + rdx27 * 4;
            rax30 = rax29 + rax29;
            rdx31 = rax30 % r8_26;
            rax32 = rax30 / r8_26;
            rdx33 = rdx31 + rdx31;
            *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
            if (r8_26 <= rdx33) {
                esi35 = 2;
                if (r8_26 < rdx33) {
                    esi35 = 3;
                }
            } else {
                esi35 = 0;
                *reinterpret_cast<unsigned char*>(&esi35) = reinterpret_cast<uint1_t>(!!rdx33);
            }
            r10d36 = v8 & 16;
            if (!r10d36) 
                goto addr_763b_17; else 
                goto addr_722c_18;
        }
    } else {
        if (rcx % r8) 
            goto addr_7094_9;
        rcx28 = rcx / r8 * rdi;
        if (__intrinsic()) 
            goto addr_7094_9;
        esi35 = 0;
        *reinterpret_cast<uint32_t*>(&rdi34) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        r10d36 = v8 & 16;
        if (r10d36) 
            goto addr_722c_18; else 
            goto addr_763b_17;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(v7) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x870e]");
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) >= reinterpret_cast<int64_t>(0)) {
            addr_70d1_24:
            __asm__("fmulp st1, st0");
            if (!(*reinterpret_cast<unsigned char*>(&v8) & 16)) {
                addr_73c8_25:
                if (v14 == 1) 
                    goto addr_745a_26;
                __asm__("fld tword [rip+0x86e7]");
                __asm__("fcomip st0, st1");
                if (v14 <= 1) 
                    goto addr_745a_26;
            } else {
                addr_70de_28:
                __asm__("fild dword [rsp+0x34]");
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                __asm__("fld st0");
                goto addr_70f4_29;
            }
        } else {
            goto addr_7380_31;
        }
    } else {
        v37 = rdi;
        __asm__("fdivp st1, st0");
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) 
            goto addr_7380_31; else 
            goto addr_70d1_24;
    }
    __asm__("fld dword [rip+0x86cd]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax39 = v37;
    }
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax39) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x8685]");
    }
    zf40 = v14 == 0;
    if (zf40) 
        goto addr_7431_39;
    __asm__("fstp st1");
    goto addr_745a_26;
    addr_7431_39:
    __asm__("fxch st0, st1");
    __asm__("fucomip st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf40) {
            addr_745a_26:
            __asm__("fstp tword [rsp]");
            fun_29b0(v6, 1, -1, "%.0Lf");
            *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
            rax41 = fun_2680(v6, v6);
            rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            rdx43 = rax41;
            r12_44 = rax41;
            goto addr_7498_43;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax39 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x812f]");
        goto addr_745a_26;
    } else {
        goto addr_745a_26;
    }
    addr_7498_43:
    rbp45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v25) - reinterpret_cast<unsigned char>(rdx43));
    fun_2870(rbp45, v6);
    rsp24 = rsp42 - 8 + 8;
    r8_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp45) + reinterpret_cast<unsigned char>(r12_44));
    while (1) {
        if (*reinterpret_cast<unsigned char*>(&v8) & 4) {
            addr_72c0_49:
            r13_47 = reinterpret_cast<void**>(0xffffffffffffffff);
            rax48 = fun_2680(r14_19, r14_19);
            rsp49 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            r12_50 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<unsigned char>(rbp45));
            r15_17 = rax48;
            rax51 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp49) + 80);
            v52 = rax51;
            fun_27c0(rax51, rbp45, r12_50, 41);
            rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp49) - 8 + 8);
            v54 = *reinterpret_cast<uint32_t*>(&rbx38);
            rbp55 = r8_46;
            rbx56 = r12_50;
            r12_57 = v22;
            while (1) {
                *reinterpret_cast<uint32_t*>(&rax58) = *r12_57;
                *reinterpret_cast<int32_t*>(&rax58 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&rax58)) {
                    if (reinterpret_cast<unsigned char>(r13_47) > reinterpret_cast<unsigned char>(rbx56)) {
                        r13_47 = rbx56;
                    }
                    rbx56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx56) - reinterpret_cast<unsigned char>(r13_47));
                    rsi59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v52) + reinterpret_cast<unsigned char>(rbx56));
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rax58) > 0x7e) {
                        r13_47 = rbx56;
                        rsi59 = v52;
                        *reinterpret_cast<int32_t*>(&rbx56) = 0;
                        *reinterpret_cast<int32_t*>(&rbx56 + 4) = 0;
                    } else {
                        if (reinterpret_cast<unsigned char>(rax58) > reinterpret_cast<unsigned char>(rbx56)) {
                            rax58 = rbx56;
                        }
                        rbx56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx56) - reinterpret_cast<unsigned char>(rax58));
                        r13_47 = rax58;
                        rsi59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v52) + reinterpret_cast<unsigned char>(rbx56));
                    }
                    ++r12_57;
                }
                rbp45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp55) - reinterpret_cast<unsigned char>(r13_47));
                fun_27e0(rbp45, rsi59, r13_47, rbp45, rsi59, r13_47);
                rsp24 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp53) - 8 + 8);
                if (!rbx56) 
                    break;
                rbp55 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp45) - reinterpret_cast<unsigned char>(r15_17));
                fun_27e0(rbp55, r14_19, r15_17, rbp55, r14_19, r15_17);
                rsp53 = reinterpret_cast<void*>(rsp24 - 8 + 8);
            }
            *reinterpret_cast<uint32_t*>(&rbx38) = v54;
        }
        addr_74bc_63:
        if (!(*reinterpret_cast<unsigned char*>(&v8) & 0x80)) 
            goto addr_74df_64;
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 0xffffffff) {
            rcx60 = v7;
            if (rcx60 <= 1) {
                *reinterpret_cast<uint32_t*>(&rbx38) = 0;
                goto addr_74cc_68;
            } else {
                *reinterpret_cast<uint32_t*>(&rdx61) = v15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx61) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx38) = 1;
                *reinterpret_cast<int32_t*>(&rax62) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax62) + 4) = 0;
                do {
                    rax62 = rax62 * rdx61;
                    if (rcx60 <= rax62) 
                        break;
                    *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
                } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
                eax63 = v8 & 0x100;
                if (!(v8 & 64)) 
                    goto addr_76a8_73;
            }
        } else {
            addr_74cc_68:
            eax63 = v8 & 0x100;
            if (eax63 | *reinterpret_cast<uint32_t*>(&rbx38)) {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 64)) {
                    addr_76a0_75:
                    if (!*reinterpret_cast<uint32_t*>(&rbx38)) {
                        rdx64 = v25;
                        if (eax63) {
                            addr_76ea_77:
                            *reinterpret_cast<void***>(rdx64) = reinterpret_cast<void**>(66);
                            v25 = rdx64 + 1;
                            goto addr_74df_64;
                        } else {
                            goto addr_74df_64;
                        }
                    } else {
                        addr_76a8_73:
                        rdx64 = v25 + 1;
                        if (v12 || *reinterpret_cast<uint32_t*>(&rbx38) != 1) {
                            rbx38 = *reinterpret_cast<int32_t*>(&rbx38);
                            ecx65 = *reinterpret_cast<unsigned char*>(0xfaa0 + rbx38);
                            *reinterpret_cast<void***>(v25) = *reinterpret_cast<void***>(&ecx65);
                            if (eax63) {
                                *reinterpret_cast<uint32_t*>(&r8_46) = v12;
                                *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
                                if (*reinterpret_cast<uint32_t*>(&r8_46)) {
                                    *reinterpret_cast<void***>(v25 + 1) = reinterpret_cast<void**>(0x69);
                                    rdx64 = v25 + 2;
                                    goto addr_76ea_77;
                                }
                            }
                        } else {
                            *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(0x6b);
                            if (eax63) 
                                goto addr_76ea_77; else 
                                goto addr_799b_83;
                        }
                    }
                }
            } else {
                addr_74df_64:
                *reinterpret_cast<void***>(v25) = reinterpret_cast<void**>(0);
                rax66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
                if (rax66) 
                    goto addr_7b7b_85; else 
                    break;
            }
        }
        *reinterpret_cast<signed char*>(v6 + 0x287) = 32;
        v25 = v6 + 0x288;
        goto addr_76a0_75;
        addr_799b_83:
        v25 = rdx64;
        goto addr_74df_64;
        addr_7b7b_85:
        rax67 = fun_2690();
        rsp24 = rsp24 - 8 + 8;
        addr_7b80_87:
        *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
        *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<unsigned char>(rax67) + 0xfffffffffffffffc) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<unsigned char>(rax67) + 0xfffffffffffffffc);
        addr_7969_88:
        *reinterpret_cast<signed char*>(r8_46 + 0xffffffffffffffff) = 49;
        rbp45 = r8_46 + 0xffffffffffffffff;
    }
    return rbp45;
    addr_70f4_29:
    while (tmp32_68 = *reinterpret_cast<uint32_t*>(&rbx38) + 1, cf69 = tmp32_68 < *reinterpret_cast<uint32_t*>(&rbx38), *reinterpret_cast<uint32_t*>(&rbx38) = tmp32_68, !cf69) {
        if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) 
            goto addr_7106_91;
        __asm__("fstp st1");
        __asm__("fxch st0, st2");
    }
    __asm__("fstp st2");
    __asm__("fstp st2");
    addr_7114_94:
    r15_17 = rbp20 + 1;
    __asm__("fdivrp st1, st0");
    rbp70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rbp20 + 2) + reinterpret_cast<uint1_t>(v12 < 1));
    if (v14 == 1) {
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fstp tword [rsp+0x30]");
        fun_29b0(v6, 1, -1, "%.1Lf");
        rax71 = fun_2680(v6, v6);
        rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        __asm__("fld tword [rsp+0x20]");
        rdx43 = rax71;
        if (reinterpret_cast<unsigned char>(rax71) > reinterpret_cast<unsigned char>(rbp70)) {
            __asm__("fld dword [rip+0x8365]");
            __asm__("fmul st1, st0");
            goto addr_7751_97;
        }
    }
    __asm__("fld tword [rip+0x898c]");
    __asm__("fcomip st0, st1");
    if (v14 <= 1) {
        __asm__("fld st0");
        goto addr_75b0_100;
    }
    __asm__("fld dword [rip+0x896e]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    if (v14 >= 1) {
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fsubr st1, st0");
        __asm__("fxch st0, st1");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax72 = v37;
        __asm__("btc rax, 0x3f");
    } else {
        __asm__("fstp st1");
        __asm__("fnstcw word [rsp+0x4e]");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x20]");
        __asm__("fldcw word [rsp+0x4e]");
        rax72 = v37;
    }
    v37 = rax72;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax72) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x8926]");
    }
    zf73 = v14 == 0;
    if (zf73) 
        goto addr_7192_107;
    __asm__("fxch st0, st1");
    goto addr_75b0_100;
    addr_7192_107:
    __asm__("fxch st0, st1");
    __asm__("fucomi st0, st1");
    if (__intrinsic()) {
        __asm__("fstp st1");
    } else {
        if (zf73) {
            addr_75b0_100:
            __asm__("fstp tword [rsp+0x20]");
            __asm__("fstp tword [rsp]");
            fun_29b0(v6, 1, -1, "%.1Lf");
            rax74 = fun_2680(v6, v6);
            rdx43 = rax74;
            rsp42 = rsp24 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
            __asm__("fld tword [rsp+0x20]");
            cf75 = reinterpret_cast<unsigned char>(rdx43) < reinterpret_cast<unsigned char>(rbp70);
            below_or_equal76 = reinterpret_cast<unsigned char>(rdx43) <= reinterpret_cast<unsigned char>(rbp70);
            if (!below_or_equal76) {
                __asm__("fld dword [rip+0x82fe]");
                __asm__("fmul st1, st0");
                goto addr_77b8_112;
            } else {
                if (!(*reinterpret_cast<unsigned char*>(&v8) & 8)) {
                    __asm__("fstp st0");
                    goto addr_761a_115;
                } else {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(v6) + reinterpret_cast<unsigned char>(rdx43) + 0xffffffffffffffff) == 48) {
                        __asm__("fld dword [rip+0x81de]");
                        cf75 = v14 < 1;
                        below_or_equal76 = v14 <= 1;
                        __asm__("fmul st1, st0");
                        if (v14 == 1) {
                            goto addr_7751_97;
                        }
                    } else {
                        __asm__("fstp st0");
                        goto addr_761a_115;
                    }
                }
            }
        } else {
            __asm__("fstp st1");
        }
    }
    rax77 = rax72 + 1;
    v37 = rax77;
    __asm__("fild qword [rsp+0x20]");
    if (reinterpret_cast<int64_t>(rax77) >= reinterpret_cast<int64_t>(0)) {
        __asm__("fxch st0, st1");
        goto addr_75b0_100;
    } else {
        __asm__("fadd dword [rip+0x88eb]");
        __asm__("fxch st0, st1");
        goto addr_75b0_100;
    }
    addr_77b8_112:
    __asm__("fld tword [rip+0x8302]");
    __asm__("fcomip st0, st2");
    if (below_or_equal76) {
        addr_7751_97:
        __asm__("fdivp st1, st0");
        __asm__("fstp tword [rsp]");
        fun_29b0(v6, 1, -1, "%.0Lf");
        rax78 = fun_2680(v6, v6);
        r15_17 = reinterpret_cast<void**>(0x7054);
        rsp42 = rsp42 - 16 - 8 + 8 - 8 + 8 + 8 + 8;
        rdx43 = rax78;
        r12_44 = rax78;
        goto addr_7498_43;
    } else {
        __asm__("fld dword [rip+0x82e8]");
        __asm__("fxch st0, st2");
        __asm__("fcomi st0, st2");
        if (!cf75) {
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fsubr st2, st0");
            __asm__("fxch st0, st2");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fxch st0, st1");
            rax79 = v37;
            __asm__("btc rax, 0x3f");
        } else {
            __asm__("fstp st2");
            __asm__("fxch st0, st1");
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fld st0");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            rax79 = v37;
        }
        __asm__("fild qword [rsp+0x20]");
        if (reinterpret_cast<int64_t>(rax79) < reinterpret_cast<int64_t>(0)) {
            __asm__("fadd dword [rip+0x829e]");
        }
        zf80 = v14 == 0;
        if (zf80) 
            goto addr_7816_130;
    }
    __asm__("fstp st1");
    goto addr_7842_132;
    addr_7816_130:
    __asm__("fucomi st0, st1");
    __asm__("fstp st1");
    if (__intrinsic()) {
        __asm__("fstp st0");
    } else {
        if (zf80) {
            addr_7842_132:
            __asm__("fxch st0, st1");
            goto addr_7751_97;
        } else {
            __asm__("fstp st0");
        }
    }
    __asm__("fild qword [rsp+0x10]");
    if (reinterpret_cast<int64_t>(rax79 + 1) < reinterpret_cast<int64_t>(0)) {
        __asm__("fadd dword [rip+0x8124]");
        __asm__("fxch st0, st1");
        goto addr_7751_97;
    } else {
        goto addr_7842_132;
    }
    addr_761a_115:
    r12_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx43) - reinterpret_cast<unsigned char>(r15_17));
    goto addr_7498_43;
    addr_7106_91:
    __asm__("fstp st2");
    __asm__("fstp st2");
    goto addr_7114_94;
    addr_7380_31:
    __asm__("fadd dword [rip+0x8726]");
    __asm__("fmulp st1, st0");
    if (*reinterpret_cast<unsigned char*>(&v8) & 16) 
        goto addr_70de_28;
    goto addr_73c8_25;
    addr_763b_17:
    *reinterpret_cast<uint32_t*>(&rbx38) = 0xffffffff;
    goto addr_723f_140;
    addr_722c_18:
    *reinterpret_cast<uint32_t*>(&r8_81) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_81) + 4) = 0;
    *reinterpret_cast<uint32_t*>(&rbx38) = 0;
    r11_82 = r8_81;
    if (r8_81 > rcx28) 
        goto addr_723f_140;
    do {
        rdx83 = rcx28 % r8_81;
        *reinterpret_cast<int32_t*>(&rcx84) = reinterpret_cast<int32_t>(esi35) >> 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx84) + 4) = 0;
        r9_85 = rcx28 / r8_81;
        *reinterpret_cast<int32_t*>(&rax86) = static_cast<int32_t>(rdx83 + rdx83 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax86) + 4) = 0;
        eax87 = static_cast<int32_t>(rdi34 + rax86 * 2);
        *reinterpret_cast<uint32_t*>(&rdx88) = eax87 % *reinterpret_cast<uint32_t*>(&r11_82);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx88) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax89) = eax87 / *reinterpret_cast<uint32_t*>(&r11_82);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax89) + 4) = 0;
        edx90 = static_cast<uint32_t>(rcx84 + rdx88 * 2);
        *reinterpret_cast<uint32_t*>(&rdi34) = *reinterpret_cast<uint32_t*>(&rax89);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        rcx28 = r9_85;
        esi91 = esi35 + edx90;
        if (*reinterpret_cast<uint32_t*>(&r11_82) > edx90) {
            esi35 = reinterpret_cast<uint1_t>(!!esi91);
        } else {
            esi35 = static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r11_82) < esi91)) + 2;
        }
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (r8_81 > r9_85) 
            break;
    } while (*reinterpret_cast<uint32_t*>(&rbx38) != 8);
    goto addr_723f_140;
    if (r9_85 > 9) {
        addr_723f_140:
        r8_46 = v25;
        if (v14 == 1) {
            rax92 = rcx28;
            *reinterpret_cast<uint32_t*>(&rax93) = *reinterpret_cast<uint32_t*>(&rax92) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax93) + 4) = 0;
            if (reinterpret_cast<int32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!(rax93 + esi35))) + *reinterpret_cast<uint32_t*>(&rdi34)) <= reinterpret_cast<int32_t>(5)) {
                goto addr_7278_149;
            }
        } else {
            if (v14) 
                goto addr_7278_149;
            esi94 = esi35 + *reinterpret_cast<uint32_t*>(&rdi34);
            if (reinterpret_cast<uint1_t>(reinterpret_cast<int32_t>(esi94) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(esi94 == 0)) 
                goto addr_7278_149;
        }
    } else {
        if (v14 != 1) {
            if (v14) 
                goto addr_7a3c_154;
            if (!esi35) 
                goto addr_7a3c_154; else 
                goto addr_79c7_156;
        }
        if (reinterpret_cast<int32_t>((*reinterpret_cast<uint32_t*>(&rax89) & 1) + esi35) > reinterpret_cast<int32_t>(2)) {
            addr_79c7_156:
            *reinterpret_cast<int32_t*>(&rdx95) = static_cast<int32_t>(rax89 + 1);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx95) + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rax89) == 9) {
                rcx28 = r9_85 + 1;
                if (r9_85 == 9) {
                    r8_46 = v25;
                    goto addr_7a0a_160;
                } else {
                    esi35 = 0;
                    goto addr_7a44_162;
                }
            } else {
                eax96 = static_cast<uint32_t>(rdx95 + 48);
                goto addr_79d6_164;
            }
        } else {
            addr_7a3c_154:
            if (*reinterpret_cast<uint32_t*>(&rax89)) {
                eax96 = *reinterpret_cast<uint32_t*>(&rax89) + 48;
                goto addr_79d6_164;
            } else {
                addr_7a44_162:
                r8_46 = v25;
                if (*reinterpret_cast<unsigned char*>(&v8) & 8) {
                    addr_7a0c_166:
                    *reinterpret_cast<uint32_t*>(&rdi34) = 0;
                    if (v14 == 1) {
                        goto addr_7278_149;
                    }
                } else {
                    eax96 = 48;
                    goto addr_79d6_164;
                }
            }
        }
    }
    ++rcx28;
    if (!r10d36) 
        goto addr_7278_149;
    *reinterpret_cast<uint32_t*>(&rax97) = v15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax97) + 4) = 0;
    if (rax97 != rcx28) {
        goto addr_7278_149;
    }
    if (*reinterpret_cast<uint32_t*>(&rbx38) == 8) {
        addr_7278_149:
        rbp45 = r8_46;
    } else {
        *reinterpret_cast<uint32_t*>(&rbx38) = *reinterpret_cast<uint32_t*>(&rbx38) + 1;
        if (*reinterpret_cast<unsigned char*>(&v8) & 8) 
            goto addr_7969_88;
        *reinterpret_cast<signed char*>(r8_46 + 0xffffffffffffffff) = 48;
        r8_46 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(~reinterpret_cast<unsigned char>(rbp20)));
        *reinterpret_cast<uint32_t*>(&rax67) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(&rax67 + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) 
            goto addr_7b10_175; else 
            goto addr_7ade_176;
    }
    do {
        --rbp45;
        rdx98 = __intrinsic() >> 3;
        rdi99 = rdx98 + rdx98 * 4;
        rax100 = rcx28 - (rdi99 + rdi99);
        eax101 = *reinterpret_cast<int32_t*>(&rax100) + 48;
        *reinterpret_cast<void***>(rbp45) = *reinterpret_cast<void***>(&eax101);
        rax102 = rcx28;
        rcx28 = rdx98;
    } while (rax102 > 9);
    if (!(*reinterpret_cast<unsigned char*>(&v8) & 4)) 
        goto addr_74bc_63; else 
        goto addr_72c0_49;
    addr_7b10_175:
    rcx103 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_46 + 8) & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
    *reinterpret_cast<uint32_t*>(&rax104) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax104) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax104) - 8);
    rax105 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<uint64_t>(rcx103));
    r15_17 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax105));
    eax106 = *reinterpret_cast<int32_t*>(&rax105) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
    if (eax106 < 8) 
        goto addr_7969_88;
    eax107 = eax106 & 0xfffffff8;
    edx108 = 0;
    do {
        *reinterpret_cast<uint32_t*>(&rsi109) = edx108;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi109) + 4) = 0;
        edx108 = edx108 + 8;
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rcx103) + reinterpret_cast<uint64_t>(rsi109)) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rsi109));
    } while (edx108 < eax107);
    goto addr_7969_88;
    addr_7ade_176:
    if (*reinterpret_cast<uint32_t*>(&rbp20) & 4) 
        goto addr_7b80_87;
    if (!*reinterpret_cast<uint32_t*>(&rax67)) 
        goto addr_7969_88;
    edx110 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17));
    *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(&edx110);
    if (!(*reinterpret_cast<unsigned char*>(&rax67) & 2)) 
        goto addr_7969_88;
    edx111 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<unsigned char>(rax67) + 0xfffffffffffffffe);
    *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<unsigned char>(rax67) + 0xfffffffffffffffe) = *reinterpret_cast<int16_t*>(&edx111);
    goto addr_7969_88;
    addr_7a0a_160:
    esi35 = 0;
    goto addr_7a0c_166;
    addr_79d6_164:
    *reinterpret_cast<signed char*>(v6 + 0x286) = *reinterpret_cast<signed char*>(&eax96);
    *reinterpret_cast<uint32_t*>(&rax112) = *reinterpret_cast<uint32_t*>(&rbp20);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax112) + 4) = 0;
    r8_46 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v6 + 0x286) - reinterpret_cast<unsigned char>(rbp20));
    if (*reinterpret_cast<uint32_t*>(&rbp20) >= 8) {
        rsi113 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r8_46 + 8) & 0xfffffffffffffff8);
        *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
        *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<uint32_t*>(&rbp20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
        *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax114) - 8) = *reinterpret_cast<int64_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax114) - 8);
        rax115 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r8_46) - reinterpret_cast<uint64_t>(rsi113));
        rdi116 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(r15_17) - reinterpret_cast<uint64_t>(rax115));
        eax117 = *reinterpret_cast<int32_t*>(&rax115) + *reinterpret_cast<uint32_t*>(&rbp20) & 0xfffffff8;
        if (eax117 >= 8) {
            r9d118 = eax117 & 0xfffffff8;
            eax119 = 0;
            do {
                *reinterpret_cast<uint32_t*>(&rdx120) = eax119;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx120) + 4) = 0;
                eax119 = eax119 + 8;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsi113) + reinterpret_cast<int64_t>(rdx120)) = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rdi116) + reinterpret_cast<int64_t>(rdx120));
            } while (eax119 < r9d118);
            goto addr_7a0a_160;
        }
    } else {
        if (*reinterpret_cast<unsigned char*>(&rbp20) & 4) {
            *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(r15_17);
            *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax112) - 4) = *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax112) - 4);
            goto addr_7a0a_160;
        } else {
            if (*reinterpret_cast<uint32_t*>(&rax112) && (edx121 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_17)), *reinterpret_cast<void***>(r8_46) = *reinterpret_cast<void***>(&edx121), !!(*reinterpret_cast<unsigned char*>(&rax112) & 2))) {
                edx122 = *reinterpret_cast<uint16_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<uint64_t>(rax112) - 2);
                *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(r8_46) + reinterpret_cast<uint64_t>(rax112) - 2) = *reinterpret_cast<int16_t*>(&edx122);
                goto addr_7a0a_160;
            }
        }
    }
}

int64_t argmatch(void** rdi, int64_t* rsi, int64_t rdx, int64_t rcx);

int64_t fun_7bb3(void** rdi, void** rsi, int64_t* rdx) {
    void** r13_4;
    int64_t* rbp5;
    void** rbx6;
    void* rsp7;
    void* rax8;
    void* v9;
    void** rax10;
    void** rax11;
    void** r12d12;
    int64_t rax13;
    void** rax14;
    int64_t rax15;
    void** rsi16;
    int64_t rdx17;
    int64_t rcx18;
    int32_t edx19;
    void** rcx20;
    void** v21;
    int64_t rdi22;
    int32_t edx23;
    void** rax24;
    uint64_t rax25;
    int64_t rax26;
    void* rdx27;

    __asm__("cli ");
    r13_4 = rsi;
    rbp5 = rdx;
    rbx6 = rdi;
    rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 16);
    rax8 = g28;
    v9 = rax8;
    if (rdi || ((rax10 = fun_2510("BLOCK_SIZE", rsi), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), rbx6 = rax10, !!rax10) || (rax11 = fun_2510("BLOCKSIZE", rsi), rsp7 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp7) - 8 + 8), rbx6 = rax11, !!rax11))) {
        r12d12 = reinterpret_cast<void**>(0);
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx6) == 39)) {
            ++rbx6;
            r12d12 = reinterpret_cast<void**>(4);
        }
        rax13 = argmatch(rbx6, 0x13a80, 0xfa98, 4);
        if (*reinterpret_cast<int32_t*>(&rax13) >= 0) 
            goto addr_7c16_5;
    } else {
        rax14 = fun_2510("POSIXLY_CORRECT", rsi);
        if (!rax14) {
            *rbp5 = 0x400;
            *reinterpret_cast<int32_t*>(&rax15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
            *reinterpret_cast<void***>(r13_4) = reinterpret_cast<void**>(0);
            goto addr_7c2a_8;
        } else {
            *rbp5 = 0x200;
            *reinterpret_cast<int32_t*>(&rax15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
            *reinterpret_cast<void***>(r13_4) = reinterpret_cast<void**>(0);
            goto addr_7c2a_8;
        }
    }
    rsi16 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp7) - 8 + 8);
    rax15 = xstrtoumax(rbx6, rsi16);
    if (*reinterpret_cast<int32_t*>(&rax15)) {
        *reinterpret_cast<void***>(r13_4) = reinterpret_cast<void**>(0);
        rdx17 = *rbp5;
    } else {
        *reinterpret_cast<uint32_t*>(&rcx18) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx6));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx18) + 4) = 0;
        edx19 = static_cast<int32_t>(rcx18 - 48);
        rcx20 = v21;
        if (*reinterpret_cast<unsigned char*>(&edx19) <= 9) {
            goto addr_7ca7_14;
        }
        do {
            if (rcx20 == rbx6) 
                break;
            *reinterpret_cast<uint32_t*>(&rdi22) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx6 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi22) + 4) = 0;
            ++rbx6;
            edx23 = static_cast<int32_t>(rdi22 - 48);
        } while (*reinterpret_cast<unsigned char*>(&edx23) > 9);
        goto addr_7ca7_14;
        if (*reinterpret_cast<signed char*>(rcx20 + 0xffffffffffffffff) == 66) 
            goto addr_7d60_18; else 
            goto addr_7c9f_19;
    }
    addr_7cc4_20:
    if (!rdx17) {
        rax24 = fun_2510("POSIXLY_CORRECT", rsi16);
        rax25 = reinterpret_cast<unsigned char>(rax24) - (reinterpret_cast<unsigned char>(rax24) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax24) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax26) = *reinterpret_cast<uint32_t*>(&rax25) & 0x200;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax26) + 4) = 0;
        *rbp5 = rax26 + 0x200;
        *reinterpret_cast<int32_t*>(&rax15) = 4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    }
    addr_7c2a_8:
    rdx27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v9) - reinterpret_cast<uint64_t>(g28));
    if (rdx27) {
        fun_2690();
    } else {
        return rax15;
    }
    addr_7d60_18:
    r12d12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d12) | 0x180);
    if (*reinterpret_cast<signed char*>(rcx20 + 0xfffffffffffffffe) != 0x69) {
        addr_7ca7_14:
        rdx17 = *rbp5;
        *reinterpret_cast<void***>(r13_4) = r12d12;
        goto addr_7cc4_20;
    }
    addr_7ca3_25:
    r12d12 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d12) | 32);
    goto addr_7ca7_14;
    addr_7c9f_19:
    *reinterpret_cast<unsigned char*>(&r12d12) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&r12d12) | 0x80);
    goto addr_7ca3_25;
    addr_7c16_5:
    *rbp5 = 1;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    *reinterpret_cast<void***>(r13_4) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12d12) | *reinterpret_cast<uint32_t*>(0xfa98 + *reinterpret_cast<int32_t*>(&rax13) * 4));
    goto addr_7c2a_8;
}

int32_t opterr = 0;

int32_t fun_26a0();

void version_etc_va(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, void* r8);

void fun_7da3(int32_t edi) {
    signed char al2;
    void* rax3;
    int32_t r14d4;
    int32_t eax5;
    void* rax6;
    void** rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t r9_11;

    __asm__("cli ");
    if (al2) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax3 = g28;
    r14d4 = opterr;
    opterr = 0;
    if (edi != 2) 
        goto addr_7e20_4;
    eax5 = fun_26a0();
    if (eax5 == -1) 
        goto addr_7e20_4;
    if (eax5 != 0x68) {
        if (eax5 != 0x76) {
            addr_7e20_4:
            opterr = r14d4;
            optind = reinterpret_cast<void**>(0);
            rax6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(g28));
            if (rax6) {
                fun_2690();
            } else {
                return;
            }
        } else {
            rdi7 = stdout;
            version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
            fun_2910();
        }
    }
    r9_11();
    goto addr_7e20_4;
}

void fun_7ee3() {
    signed char al1;
    void* rax2;
    signed char r9b3;
    int32_t ebx4;
    int32_t eax5;
    int64_t v6;
    void** rdi7;
    int64_t rdx8;
    int64_t rcx9;
    int64_t r8_10;
    int64_t rdi11;
    void* rax12;

    __asm__("cli ");
    if (al1) {
        __asm__("movaps [rsp+0x50], xmm0");
        __asm__("movaps [rsp+0x60], xmm1");
        __asm__("movaps [rsp+0x70], xmm2");
        __asm__("movaps [rsp+0x80], xmm3");
        __asm__("movaps [rsp+0x90], xmm4");
        __asm__("movaps [rsp+0xa0], xmm5");
        __asm__("movaps [rsp+0xb0], xmm6");
        __asm__("movaps [rsp+0xc0], xmm7");
    }
    rax2 = g28;
    if (!r9b3) {
    }
    ebx4 = opterr;
    opterr = 1;
    eax5 = fun_26a0();
    if (eax5 != -1) {
        if (eax5 == 0x68) {
            addr_8010_7:
            v6();
        } else {
            if (eax5 == 0x76) {
                rdi7 = stdout;
                version_etc_va(rdi7, rdx8, rcx9, r8_10, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 0xd0 - 8 + 8);
                fun_2910();
                goto addr_8010_7;
            } else {
                *reinterpret_cast<int32_t*>(&rdi11) = exit_failure;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi11) + 4) = 0;
                v6(rdi11);
            }
        }
    }
    opterr = ebx4;
    rax12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax2) - reinterpret_cast<uint64_t>(g28));
    if (rax12) {
        fun_2690();
    } else {
        return;
    }
}

void fun_2920(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s9 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s9* fun_26f0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_8023(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s9* rax4;
    void** r12_5;
    int32_t eax6;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2920("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2560();
    } else {
        rbx3 = rdi;
        rax4 = fun_26f0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax6 = fun_25a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7), !eax6))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_97c3(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2570();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x14520;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_9803(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x14520);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_9823(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x14520);
    }
    *rdi = esi;
    return 0x14520;
}

int64_t fun_9843(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x14520);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s10 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_9883(struct s10* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s10*>(0x14520);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s11 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s11* fun_98a3(struct s11* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s11*>(0x14520);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x29ca;
    if (!rdx) 
        goto 0x29ca;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x14520;
}

struct s12 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_98e3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s12* r8) {
    struct s12* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s12*>(0x14520);
    }
    rax7 = fun_2570();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x9916);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s13 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_9963(int64_t rdi, int64_t rsi, void*** rdx, struct s13* rcx) {
    struct s13* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s13*>(0x14520);
    }
    rax6 = fun_2570();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x9991);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x99ec);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_9a53() {
    __asm__("cli ");
}

void** g140b8 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_9a63() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2550(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x14420) {
        fun_2550(rdi7);
        g140b8 = reinterpret_cast<void**>(0x14420);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x140b0) {
        fun_2550(r12_2);
        slotvec = reinterpret_cast<void**>(0x140b0);
    }
    nslots = 1;
    return;
}

void fun_9b03() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9b23() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9b33(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9b53(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_9b73(int32_t edi, int32_t esi, int64_t rdx) {
    void* rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x29d0;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx4) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_2690();
    } else {
        return rax6;
    }
}

void** fun_9c03(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x29d5;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rdx, rcx, rcx6, edi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx5) - reinterpret_cast<uint64_t>(g28));
    if (rdx8) {
        fun_2690();
    } else {
        return rax7;
    }
}

void** fun_9c93(int32_t edi, int64_t rsi) {
    void* rax3;
    struct s2* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x29da;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(g28));
    if (rdx6) {
        fun_2690();
    } else {
        return rax5;
    }
}

void** fun_9d23(int32_t edi, int64_t rsi, int64_t rdx) {
    void* rax4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x29df;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_2690();
    } else {
        return rax6;
    }
}

void** fun_9db3(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s2* rsp4;
    void* rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xa760]");
    __asm__("movdqa xmm1, [rip+0xa768]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0xa751]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - reinterpret_cast<uint64_t>(g28));
    if (rdx11) {
        fun_2690();
    } else {
        return rax10;
    }
}

void** fun_9e53(int64_t rdi, uint32_t esi) {
    struct s2* rsp3;
    void* rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0xa6c0]");
    __asm__("movdqa xmm1, [rip+0xa6c8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0xa6b1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (rdx10) {
        fun_2690();
    } else {
        return rax9;
    }
}

void** fun_9ef3(int64_t rdi) {
    void* rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa620]");
    __asm__("movdqa xmm1, [rip+0xa628]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0xa609]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax2) - reinterpret_cast<uint64_t>(g28));
    if (rdx4) {
        fun_2690();
    } else {
        return rax3;
    }
}

void** fun_9f83(int64_t rdi, int64_t rsi) {
    void* rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa590]");
    __asm__("movdqa xmm1, [rip+0xa598]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0xa586]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(g28));
    if (rdx5) {
        fun_2690();
    } else {
        return rax4;
    }
}

void** fun_a013(int32_t edi, int32_t esi, int64_t rdx) {
    void* rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x29e4;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(edi, rdx, -1, rcx5, edi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx4) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_2690();
    } else {
        return rax6;
    }
}

void** fun_a0b3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa45a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0xa452]");
    __asm__("movdqa xmm2, [rip+0xa45a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x29e9;
    if (!rdx) 
        goto 0x29e9;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(edi, rcx, -1, rcx6, edi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx5) - reinterpret_cast<uint64_t>(g28));
    if (rdx8) {
        fun_2690();
    } else {
        return rax7;
    }
}

void** fun_a153(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void* rcx6;
    struct s2* rcx7;
    void** rax8;
    void* rdx9;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa3ba]");
    __asm__("movdqa xmm1, [rip+0xa3c2]");
    __asm__("movdqa xmm2, [rip+0xa3ca]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x29ee;
    if (!rdx) 
        goto 0x29ee;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax8 = quotearg_n_options(edi, rcx, r8, rcx7, edi, rcx, r8, rcx7);
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx6) - reinterpret_cast<uint64_t>(g28));
    if (rdx9) {
        fun_2690();
    } else {
        return rax8;
    }
}

void** fun_a203(int64_t rdi, int64_t rsi, int64_t rdx) {
    void* rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa30a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0xa302]");
    __asm__("movdqa xmm2, [rip+0xa30a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x29f3;
    if (!rsi) 
        goto 0x29f3;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx4) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_2690();
    } else {
        return rax6;
    }
}

void** fun_a2a3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void* rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0xa26a]");
    __asm__("movdqa xmm1, [rip+0xa272]");
    __asm__("movdqa xmm2, [rip+0xa27a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x29f8;
    if (!rsi) 
        goto 0x29f8;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx5) - reinterpret_cast<uint64_t>(g28));
    if (rdx8) {
        fun_2690();
    } else {
        return rax7;
    }
}

void fun_a343() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a353(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a373() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_a393(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

int64_t xvasprintf(int64_t rdi, int64_t rsi);

void fun_a3b3(int32_t edi, int32_t esi, int64_t rdx, int64_t rcx) {
    int64_t rax5;

    __asm__("cli ");
    rax5 = xvasprintf(rdx, rcx);
    if (!rax5) {
        fun_2660();
        fun_2570();
        fun_2880();
        fun_2560();
    } else {
        fun_2880();
        goto fun_2550;
    }
}

void fun_2590(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9);

void fun_a433(int32_t edi, int32_t esi, int64_t rdx, int32_t ecx, int64_t r8, int64_t r9) {
    int64_t rax7;
    int64_t rcx8;
    int64_t rsi9;
    int64_t rdi10;

    __asm__("cli ");
    rax7 = xvasprintf(r8, r9);
    if (!rax7) {
        fun_2660();
        fun_2570();
        fun_2880();
        fun_2560();
    } else {
        if (!rdx) {
            fun_2880();
        } else {
            *reinterpret_cast<int32_t*>(&rcx8) = ecx;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx8) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rsi9) = esi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi9) + 4) = 0;
            *reinterpret_cast<int32_t*>(&rdi10) = edi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
            fun_2590(rdi10, rsi9, rdx, rcx8, "%s", rax7);
        }
        goto fun_2550;
    }
}

void fun_27b0(int64_t rdi, void** rsi, void** rdx, void** rcx, int64_t r8, uint64_t r9);

void** fun_a4e3(void** rdi, int64_t rsi, int64_t rdx, uint64_t rcx, int64_t r8, uint64_t r9) {
    uint64_t r12_7;
    void** rax8;
    void** rax9;
    void** rax10;
    void** rax11;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2930(rdi, 1, "%s %s\n", rdi, 1, "%s %s\n");
    } else {
        r9 = rcx;
        fun_2930(rdi, 1, "%s (%s) %s\n", rdi, 1, "%s (%s) %s\n");
    }
    rax8 = fun_2660();
    fun_2930(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rdi, 1, "Copyright %s %d Free Software Foundation, Inc.");
    fun_27b0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_2660();
    fun_2930(rdi, 1, rax9, rdi, 1, rax9);
    fun_27b0(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (r12_7 > 9) {
        rax10 = fun_2660();
        rax11 = fun_2930(rdi, 1, rax10, rdi, 1, rax10);
        return rax11;
    } else {
        goto *reinterpret_cast<int32_t*>(0x10208 + r12_7 * 4) + 0x10208;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_a953() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s14 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_a973(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s14* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s14* rcx8;
    void* rax9;
    void* v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v10) - reinterpret_cast<uint64_t>(g28));
    if (rax18) {
        fun_2690();
    } else {
        return;
    }
}

void fun_aa13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void* rax15;
    void* v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_aab6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_aac0_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v16) - reinterpret_cast<uint64_t>(g28));
    if (rax20) {
        fun_2690();
    } else {
        return;
    }
    addr_aab6_5:
    goto addr_aac0_7;
}

void fun_aaf3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    int64_t r8_4;
    uint64_t r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_27b0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2660();
    fun_2860(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2660();
    fun_2860(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2660();
    goto fun_2860;
}

int64_t fun_25e0();

void xalloc_die();

void fun_ab93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_25e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void** fun_2800(void** rdi, void** rsi, ...);

void fun_abd3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2800(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_abf3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2800(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ac13(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2800(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void** fun_2840(void** rdi, void** rsi, ...);

void fun_ac33(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2840(rdi, rsi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_ac63(void** rdi, uint64_t rsi) {
    uint64_t rax3;
    void** rax4;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rsi == 0);
    rax4 = fun_2840(rdi, rsi | rax3);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ac93(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_25e0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_acd3() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_25e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ad13(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_25e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ad43(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_25e0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_ad93(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_25e0();
            if (rax5) 
                break;
            addr_addd_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_addd_5;
        rax8 = fun_25e0();
        if (rax8) 
            goto addr_adc6_9;
        if (rbx4) 
            goto addr_addd_5;
        addr_adc6_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_ae23(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_25e0();
            if (rax8) 
                break;
            addr_ae6a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_ae6a_5;
        rax11 = fun_25e0();
        if (rax11) 
            goto addr_ae52_9;
        if (!rbx6) 
            goto addr_ae52_9;
        if (r12_4) 
            goto addr_ae6a_5;
        addr_ae52_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_aeb3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_af5d_9:
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_af70_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_af10_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_af36_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_af84_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_af2d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_af84_14;
            addr_af2d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_af84_14;
            addr_af36_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2840(rdi7, rsi9);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_af84_14;
            if (!rbp13) 
                break;
            addr_af84_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_af5d_9;
        } else {
            if (!r13_6) 
                goto addr_af70_10;
            goto addr_af10_11;
        }
    }
}

int64_t fun_2790();

void fun_afb3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2790();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_afe3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2790();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b013() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2790();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b033() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_2790();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_b053(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2800(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_27e0;
    }
}

void fun_b093(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2800(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_27e0;
    }
}

void fun_b0d3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2800(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_27e0;
    }
}

void fun_b113(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_2680(rdi);
    rax4 = fun_2800(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_27e0;
    }
}

void fun_b153() {
    __asm__("cli ");
    fun_2660();
    fun_2880();
    fun_2560();
}

void fun_2720(int64_t rdi);

void** fun_29a0(void** rdi, void** rsi, void** rdx, void** rcx);

void** fun_28d0(void** rdi);

int64_t fun_b193(void** rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** v6;
    void* rax7;
    void* v8;
    void* rcx9;
    void** rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    void** rax24;
    int64_t rbp25;
    struct s1* rax26;
    int64_t rax27;
    struct s1* rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    struct s1* rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (*reinterpret_cast<uint32_t*>(&rdx) > 36) {
        rcx9 = reinterpret_cast<void*>("xstrtoumax");
        rsi = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_2720("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2690();
            while (1) {
                rbx10 = reinterpret_cast<void**>(0xffffffffffffffff);
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_b504_6;
                    rbx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx10) * reinterpret_cast<uint64_t>(rcx9));
                } while (!__intrinsic());
            }
            addr_b504_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *reinterpret_cast<void***>(r15_14) = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_b24d_12:
            *reinterpret_cast<void***>(v6) = rbx10;
            addr_b255_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v8) - reinterpret_cast<uint64_t>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2570();
    *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_29a0(rdi, rsi, rdx, rcx);
    rcx9 = *rax21;
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rcx9) + rdx23 * 2 + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_b28b_21;
    rsi = r15_14;
    rax24 = fun_28d0(rbp17);
    r8 = *reinterpret_cast<void***>(r15_14);
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(&rbx10 + 4) = 0, rax26 = fun_26d0(r13_18, r13_18), r8 = r8, rax26 == 0))) {
            addr_b28b_21:
            r12d11 = 4;
            goto addr_b255_13;
        } else {
            addr_b2c9_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_26d0(r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<unsigned char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void*>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x102b8 + rbp33 * 4) + 0x102b8;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_20)) {
            r12d11 = 1;
            if (*reinterpret_cast<void***>(r12_20) != 34) 
                goto addr_b28b_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_b24d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_b24d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_26d0(r13_18, r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_b2c9_24;
    }
    r12d11 = r12d11 | 2;
    *reinterpret_cast<void***>(v6) = rbx10;
    goto addr_b255_13;
}

struct s15 {
    uint32_t f0;
    signed char[4] pad8;
    void*** f8;
    int64_t f10;
};

int32_t rpl_vasprintf(void*** rdi, unsigned char* rsi);

void** xmalloc(uint64_t rdi);

void** fun_b723(unsigned char* rdi, struct s15* rsi) {
    void*** rsp3;
    unsigned char* r8_4;
    struct s15* rdx5;
    void* rax6;
    void* v7;
    uint32_t eax8;
    int64_t rdi9;
    int32_t eax10;
    void** rax11;
    void** v12;
    void** rax13;
    void* rdx14;
    void* rax15;
    void* rax16;
    void* v17;
    int64_t r15_18;
    void** rax19;
    void** rbp20;
    void** r14_21;
    void* rax22;
    int64_t r13_23;
    struct s15* r12_24;
    int64_t rbp25;
    uint64_t rbx26;
    uint32_t v27;
    int64_t rdx28;
    void*** rdx29;
    void*** v30;
    void** rdi31;
    void** rax32;
    uint64_t tmp64_33;
    int1_t cf34;
    void** rax35;
    void** rax36;
    int64_t rdx37;
    void*** rdx38;
    void** r15_39;
    void** rax40;

    __asm__("cli ");
    rsp3 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 24);
    r8_4 = rdi;
    rdx5 = rsi;
    rax6 = g28;
    v7 = rax6;
    eax8 = *rdi;
    *reinterpret_cast<int32_t*>(&rdi9) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&eax8)) {
        do {
            if (*reinterpret_cast<signed char*>(&eax8) != 37) 
                break;
            if (*reinterpret_cast<signed char*>(r8_4 + rdi9 * 2 + 1) != 0x73) 
                break;
            ++rdi9;
            eax8 = r8_4[rdi9 * 2];
        } while (*reinterpret_cast<signed char*>(&eax8));
        goto addr_b7b0_5;
    } else {
        goto addr_b7b0_5;
    }
    eax10 = rpl_vasprintf(rsp3, r8_4);
    rax11 = v12;
    if (eax10 < 0) {
        rax13 = fun_2570();
        if (*reinterpret_cast<void***>(rax13) == 12) {
            xalloc_die();
            goto addr_b7d1_10;
        } else {
            *reinterpret_cast<int32_t*>(&rax11) = 0;
            *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
            goto addr_b780_12;
        }
    } else {
        addr_b780_12:
        rdx14 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v7) - reinterpret_cast<uint64_t>(g28));
        if (!rdx14) {
            return rax11;
        }
    }
    addr_b7b0_5:
    rax15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v7) - reinterpret_cast<uint64_t>(g28));
    if (rax15) {
        addr_b7d1_10:
        fun_2690();
    } else {
        rax16 = g28;
        v17 = rax16;
        __asm__("movdqu xmm0, [rsi]");
        __asm__("movups [rsp], xmm0");
        r15_18 = rdx5->f10;
        if (rdi9) 
            goto addr_b5f8_16;
    }
    rax19 = xmalloc(1);
    rbp20 = rax19;
    r14_21 = rax19;
    addr_b6d0_18:
    *reinterpret_cast<void***>(rbp20) = reinterpret_cast<void**>(0);
    addr_b6d4_19:
    rax22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v17) - reinterpret_cast<uint64_t>(g28));
    if (rax22) {
        fun_2690();
    } else {
        return r14_21;
    }
    addr_b5f8_16:
    r13_23 = rdi9;
    r12_24 = rdx5;
    rbp25 = rdi9;
    *reinterpret_cast<int32_t*>(&rbx26) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx26) + 4) = 0;
    do {
        if (v27 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx28) = v27;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx28) + 4) = 0;
            v27 = v27 + 8;
            rdx29 = reinterpret_cast<void***>(rdx28 + r15_18);
        } else {
            rdx29 = v30;
            v30 = rdx29 + 8;
        }
        rdi31 = *rdx29;
        rax32 = fun_2680(rdi31);
        tmp64_33 = rbx26 + reinterpret_cast<unsigned char>(rax32);
        cf34 = tmp64_33 < rbx26;
        rbx26 = tmp64_33;
        if (cf34) {
            rbx26 = 0xffffffffffffffff;
        }
        --rbp25;
    } while (rbp25);
    if (rbx26 <= 0x7fffffff) 
        goto addr_b65d_29;
    rax35 = fun_2570();
    *reinterpret_cast<int32_t*>(&r14_21) = 0;
    *reinterpret_cast<int32_t*>(&r14_21 + 4) = 0;
    *reinterpret_cast<void***>(rax35) = reinterpret_cast<void**>(75);
    goto addr_b6d4_19;
    addr_b65d_29:
    rax36 = xmalloc(rbx26 + 1);
    r14_21 = rax36;
    rbp20 = rax36;
    do {
        if (r12_24->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx37) = r12_24->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx37) + 4) = 0;
            rdx38 = reinterpret_cast<void***>(rdx37 + r12_24->f10);
            r12_24->f0 = r12_24->f0 + 8;
        } else {
            rdx38 = r12_24->f8;
            r12_24->f8 = rdx38 + 8;
        }
        r15_39 = *rdx38;
        rax40 = fun_2680(r15_39, r15_39);
        fun_27e0(rbp20, r15_39, rax40);
        rbp20 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp20) + reinterpret_cast<unsigned char>(rax40));
        --r13_23;
    } while (r13_23);
    goto addr_b6d0_18;
}

void** vasnprintf();

uint64_t fun_b7e3(void*** rdi, int64_t rsi, int64_t rdx) {
    void* rax4;
    void** rax5;
    uint64_t rax6;
    uint64_t v7;
    void** rax8;
    void* rdx9;

    __asm__("cli ");
    rax4 = g28;
    rax5 = vasnprintf();
    if (!rax5) {
        *reinterpret_cast<int32_t*>(&rax6) = -1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    } else {
        rax6 = v7;
        if (rax6 > 0x7fffffff) {
            fun_2550(rax5, rax5);
            rax8 = fun_2570();
            *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(75);
            *reinterpret_cast<int32_t*>(&rax6) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        } else {
            *rdi = rax5;
        }
    }
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax4) - reinterpret_cast<uint64_t>(g28));
    if (rdx9) {
        fun_2690();
    } else {
        return rax6;
    }
}

void fun_b863() {
    __asm__("cli ");
    goto usage;
}

int64_t fun_b873(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2680(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_25a0(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_b8f3_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_b940_6; else 
                    continue;
            } else {
                rax18 = fun_2680(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_b970_8;
                if (v16 == -1) 
                    goto addr_b92e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_b8f3_5;
            } else {
                eax19 = fun_2770(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_b8f3_5;
            }
            addr_b92e_10:
            v16 = rbx15;
            goto addr_b8f3_5;
        }
    }
    addr_b955_16:
    return v12;
    addr_b940_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_b955_16;
    addr_b970_8:
    v12 = rbx15;
    goto addr_b955_16;
}

int32_t fun_27a0();

int64_t fun_b983(int64_t rdi, int64_t* rsi) {
    int64_t* rbp3;
    int64_t rbx4;
    int32_t eax5;

    __asm__("cli ");
    if (!*rsi) {
        addr_b9c8_2:
        return -1;
    } else {
        rbp3 = rsi;
        *reinterpret_cast<int32_t*>(&rbx4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx4) + 4) = 0;
        do {
            eax5 = fun_27a0();
            if (!eax5) 
                break;
            ++rbx4;
        } while (rbp3[rbx4]);
        goto addr_b9c8_2;
    }
    return rbx4;
}

int64_t quotearg_n_style();

void fun_b9e3(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_2660();
    } else {
        fun_2660();
    }
    quote_n();
    quotearg_n_style();
    goto fun_2880;
}

void fun_ba73(void*** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r13_7;
    void** r12_8;
    void** rdx9;
    void** rbp10;
    void** r14_11;
    void*** v12;
    void** rax13;
    void** rsi14;
    void** r15_15;
    int64_t rbx16;
    uint32_t eax17;
    void** rax18;
    void** rdi19;
    void** rax20;
    void** rdi21;
    void** rdi22;
    void** rax23;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_7) = 0;
    *reinterpret_cast<int32_t*>(&r13_7 + 4) = 0;
    r12_8 = rdx;
    *reinterpret_cast<int32_t*>(&rdx9) = 5;
    *reinterpret_cast<int32_t*>(&rdx9 + 4) = 0;
    rbp10 = rsi;
    r14_11 = stderr;
    v12 = rdi;
    rax13 = fun_2660();
    rsi14 = r14_11;
    fun_2780(rax13, rsi14, 5, rcx);
    r15_15 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (r15_15) {
        do {
            if (!rbx16 || (rdx9 = r12_8, rsi14 = rbp10, eax17 = fun_2770(r13_7, rsi14, rdx9, r13_7, rsi14, rdx9), !!eax17)) {
                r13_7 = rbp10;
                rax18 = quote(r15_15, rsi14, rdx9, rcx, r8, r9);
                rdi19 = stderr;
                rdx9 = reinterpret_cast<void**>("\n  - %s");
                *reinterpret_cast<int32_t*>(&rsi14) = 1;
                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                rcx = rax18;
                fun_2930(rdi19, 1, "\n  - %s", rdi19, 1, "\n  - %s");
            } else {
                rax20 = quote(r15_15, rsi14, rdx9, rcx, r8, r9);
                rdi21 = stderr;
                *reinterpret_cast<int32_t*>(&rsi14) = 1;
                *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
                rdx9 = reinterpret_cast<void**>(", %s");
                rcx = rax20;
                fun_2930(rdi21, 1, ", %s", rdi21, 1, ", %s");
            }
            ++rbx16;
            rbp10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp10) + reinterpret_cast<unsigned char>(r12_8));
            r15_15 = v12[rbx16 * 8];
        } while (r15_15);
    }
    rdi22 = stderr;
    rax23 = *reinterpret_cast<void***>(rdi22 + 40);
    if (reinterpret_cast<unsigned char>(rax23) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi22 + 48))) {
        goto fun_26e0;
    } else {
        *reinterpret_cast<void***>(rdi22 + 40) = rax23 + 1;
        *reinterpret_cast<void***>(rax23) = reinterpret_cast<void**>(10);
        return;
    }
}

void argmatch_invalid(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(int64_t* rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_bba3(int64_t rdi, void** rsi, int64_t* rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    void** r14_9;
    int64_t r13_10;
    int64_t r12_11;
    int64_t* rbp12;
    int64_t v13;
    int64_t rax14;
    int64_t rbx15;
    int32_t eax16;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_bbe7_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_bc5e_4;
        } else {
            addr_bc5e_4:
            return rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
    if (*rdx) {
        do {
            eax16 = fun_27a0();
            if (!eax16) 
                break;
            ++rbx15;
        } while (rbp12[rbx15]);
        goto addr_bbe0_8;
    } else {
        goto addr_bbe0_8;
    }
    return rbx15;
    addr_bbe0_8:
    rax14 = -1;
    goto addr_bbe7_3;
}

struct s16 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_bc73(void** rdi, struct s16* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_2770(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

signed char* fun_2820(int64_t rdi);

signed char* fun_bcd3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_2820(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_26b0(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_bd13(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void* rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_26b0(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) - reinterpret_cast<uint64_t>(g28));
    if (rax9) {
        fun_2690();
    } else {
        return r12_7;
    }
}

int32_t printf_parse(void** rdi, void* rsi, void** rdx);

int32_t printf_fetchargs(void** rdi, void** rsi, void** rdx);

struct s17 {
    signed char[7] pad7;
    void** f7;
};

struct s18 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    int32_t f10;
    signed char[4] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
    signed char[7] pad40;
    int64_t f28;
    void** f30;
    signed char[7] pad56;
    void** f38;
    signed char[7] pad64;
    int64_t f40;
    unsigned char f48;
    signed char[7] pad80;
    int64_t f50;
    void** f58;
};

struct s19 {
    uint32_t f0;
    signed char[12] pad16;
    int32_t f10;
};

void** fun_bda3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void* rsp5;
    void* rbp6;
    void** r14_7;
    void** r13_8;
    void** r12_9;
    void** v10;
    void*** v11;
    void* rax12;
    void* v13;
    int32_t eax14;
    void* rsp15;
    void** r10_16;
    void* rax17;
    int64_t* rsp18;
    void** rbx19;
    int64_t* rsp20;
    void** rax21;
    void** v22;
    int64_t* rsp23;
    void** v24;
    int64_t* rsp25;
    void** v26;
    int64_t* rsp27;
    void** v28;
    int64_t* rsp29;
    void** rax30;
    void** r15_31;
    void** v32;
    int64_t* rsp33;
    int64_t* rsp34;
    void** v35;
    int64_t* rsp36;
    void** v37;
    int64_t* rsp38;
    void** rsi39;
    int32_t eax40;
    void** rax41;
    void** rax42;
    struct s17* v43;
    void** tmp64_44;
    void* v45;
    void** r8_46;
    void** tmp64_47;
    uint1_t cf48;
    void* rax49;
    void* rcx50;
    uint64_t rdx51;
    void* rdx52;
    void** v53;
    void** rax54;
    void** rax55;
    struct s18* r14_56;
    struct s18* v57;
    void** r9_58;
    void** r8_59;
    int64_t v60;
    int64_t v61;
    uint32_t edx62;
    void** tmp64_63;
    void** r10_64;
    int64_t* rsp65;
    int64_t* rsp66;
    void** rax67;
    int64_t* rsp68;
    void** rax69;
    int64_t* rsp70;
    void** rax71;
    int64_t* rsp72;
    void** rax73;
    int64_t* rsp74;
    void** rax75;
    int64_t* rsp76;
    void** rax77;
    void** tmp64_78;
    int64_t* rsp79;
    void** rax80;
    int64_t* rsp81;
    void** rax82;
    int64_t* rsp83;
    void** rax84;
    uint32_t ecx85;
    uint32_t* v86;
    int64_t r13_87;
    int32_t eax88;
    void** rsi89;
    void** rax90;
    int64_t* rsp91;
    void** rsi92;
    void** rax93;
    int64_t* rsp94;
    int64_t rax95;
    uint32_t eax96;
    int32_t v97;
    struct s19* rcx98;
    int64_t rax99;
    void** tmp64_100;
    void** r15_101;
    void** rax102;
    int64_t rax103;
    int64_t* rsp104;
    void** rax105;
    int64_t* rsp106;
    void** rax107;
    int64_t* rsp108;
    int64_t* rsp109;
    void** rax110;
    int64_t* rsp111;
    void** rax112;

    __asm__("cli ");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp6 = rsp5;
    r14_7 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc50);
    r13_8 = rdx;
    r12_9 = rcx;
    v10 = rdi;
    v11 = rsi;
    rax12 = g28;
    v13 = rax12;
    eax14 = printf_parse(r13_8, reinterpret_cast<int64_t>(rbp6) - 0x2c0, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 - 8 - 8 - 8 - 8 - 0x3f8 - 8 + 8);
    if (eax14 < 0) {
        while (1) {
            *reinterpret_cast<int32_t*>(&r10_16) = 0;
            *reinterpret_cast<int32_t*>(&r10_16 + 4) = 0;
            while (rax17 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v13) - reinterpret_cast<uint64_t>(g28)), !!rax17) {
                rsp18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp18 = 0xccda;
                fun_2690();
                rsp15 = reinterpret_cast<void*>(rsp18 + 1);
                addr_ccda_5:
                if (rbx19 != 0xffffffffffffffff) 
                    goto addr_cce4_6;
                addr_cbce_7:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 0;
                if (reinterpret_cast<unsigned char>(rbx19) > reinterpret_cast<unsigned char>(r13_8) && (r10_16 != v10 && (rsp20 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8), *rsp20 = 0xcbf3, rax21 = fun_2840(r10_16, r13_8, r10_16, r13_8), rsp15 = reinterpret_cast<void*>(rsp20 + 1), r10_16 = r10_16, !!rax21))) {
                    r10_16 = rax21;
                }
                if (v22) {
                    rsp23 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp23 = 0xcc19;
                    fun_2550(v22, v22);
                    rsp15 = reinterpret_cast<void*>(rsp23 + 1);
                    r10_16 = r10_16;
                }
                if (v24 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                    rsp25 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp25 = 0xcc3f;
                    fun_2550(v24, v24);
                    rsp15 = reinterpret_cast<void*>(rsp25 + 1);
                    r10_16 = r10_16;
                }
                if (v26 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) {
                    rsp27 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                    *rsp27 = 0xcc65;
                    fun_2550(v26, v26);
                    rsp15 = reinterpret_cast<void*>(rsp27 + 1);
                    r10_16 = r10_16;
                }
                *v11 = r12_9;
            }
            break;
            addr_cce4_6:
            addr_c8c8_16:
            v28 = r10_16;
            addr_c8cf_17:
            rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp29 = 0xc8d4;
            rax30 = fun_2570();
            rsp15 = reinterpret_cast<void*>(rsp29 + 1);
            r15_31 = v28;
            v32 = rax30;
            addr_c8e2_18:
            *reinterpret_cast<void***>(v32) = reinterpret_cast<void**>(12);
            if (r15_31 != v10) {
                rsp33 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp33 = 0xc481;
                fun_2550(r15_31, r15_31);
                rsp15 = reinterpret_cast<void*>(rsp33 + 1);
            }
            if (v22) {
                rsp34 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp34 = 0xc499;
                fun_2550(v22, v22);
                rsp15 = reinterpret_cast<void*>(rsp34 + 1);
            }
            addr_c0d8_23:
            if (v35 != reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffd60) {
                rsp36 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp36 = 0xc0f0;
                fun_2550(v35, v35);
                rsp15 = reinterpret_cast<void*>(rsp36 + 1);
            }
            if (v37 == reinterpret_cast<int64_t>(rbp6) + 0xfffffffffffffc60) 
                continue;
            rsp38 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp38 = 0xc108;
            fun_2550(v37, v37);
            rsp15 = reinterpret_cast<void*>(rsp38 + 1);
        }
        return r10_16;
    }
    rsi39 = r14_7;
    eax40 = printf_fetchargs(r12_9, rsi39, r14_7);
    rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
    if (eax40 < 0) {
        rax41 = fun_2570();
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
        *reinterpret_cast<void***>(rax41) = reinterpret_cast<void**>(22);
        goto addr_c0d8_23;
    }
    rax42 = reinterpret_cast<void**>(&v43->f7);
    if (reinterpret_cast<uint64_t>(v43) >= 0xfffffffffffffff9) {
        rax42 = reinterpret_cast<void**>(0xffffffffffffffff);
    }
    tmp64_44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax42) + reinterpret_cast<uint64_t>(v45));
    if (reinterpret_cast<unsigned char>(tmp64_44) < reinterpret_cast<unsigned char>(rax42)) 
        goto addr_c0cd_33;
    *reinterpret_cast<int32_t*>(&r8_46) = 0;
    *reinterpret_cast<int32_t*>(&r8_46 + 4) = 0;
    tmp64_47 = tmp64_44 + 6;
    cf48 = reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(tmp64_47) < reinterpret_cast<unsigned char>(tmp64_44));
    *reinterpret_cast<unsigned char*>(&r8_46) = cf48;
    if (cf48) 
        goto addr_c0cd_33;
    if (reinterpret_cast<unsigned char>(tmp64_47) <= reinterpret_cast<unsigned char>(0xf9f)) {
        rax49 = reinterpret_cast<void*>(tmp64_44 + 29);
        rcx50 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - (reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffff000));
        rdx51 = reinterpret_cast<uint64_t>(rax49) & 0xfffffffffffffff0;
        if (rsp15 != rcx50) {
            do {
                rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>("C_2.14"));
            } while (rsp15 != rcx50);
        }
        *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<uint32_t*>(&rdx51) & reinterpret_cast<uint32_t>("BC_2.14");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0;
        rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - reinterpret_cast<int64_t>(rdx52));
        if (rdx52) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp15) + reinterpret_cast<int64_t>(rdx52) - 8);
        }
        v22 = reinterpret_cast<void**>(0);
        v53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp15) + 15 & 0xfffffffffffffff0);
    } else {
        if (tmp64_47 == 0xffffffffffffffff || (rax54 = fun_2800(tmp64_47, rsi39), rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8), v53 = rax54, rax54 == 0)) {
            addr_c0cd_33:
            rax55 = fun_2570();
            rsp15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp15) - 8 + 8);
            *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(12);
            goto addr_c0d8_23;
        } else {
            v22 = rax54;
            r8_46 = r8_46;
        }
    }
    *reinterpret_cast<int32_t*>(&rbx19) = 0;
    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
    if (v10) {
        rbx19 = *v11;
    }
    r14_56 = v57;
    r9_58 = r8_46;
    r8_59 = r13_8;
    v60 = 0;
    r15_31 = v10;
    r13_8 = r14_56->f0;
    if (r13_8 != r8_59) 
        goto addr_becc_46;
    while (1) {
        addr_c824_47:
        r12_9 = r9_58;
        r10_16 = r15_31;
        while (v61 != v60) {
            edx62 = r14_56->f48;
            if (*reinterpret_cast<signed char*>(&edx62) != 37) 
                goto addr_bf8f_50;
            if (r14_56->f50 != -1) 
                goto 0x29fd;
            r9_58 = reinterpret_cast<void**>(0xffffffffffffffff);
            if (reinterpret_cast<unsigned char>(r12_9) < reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
                r9_58 = r12_9 + 1;
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r9_58)) {
                addr_c7ff_55:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_16) + reinterpret_cast<unsigned char>(r12_9)) = 37;
                r15_31 = r10_16;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_c8c8_16;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r9_58)) {
                    rbx19 = r9_58;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_c8c8_16;
                if (r10_16 == v10) 
                    goto addr_cb14_64; else 
                    goto addr_c7d3_65;
            }
            r8_59 = r14_56->f8;
            r13_8 = r14_56->f58;
            r14_56 = reinterpret_cast<struct s18*>(&r14_56->f58);
            ++v60;
            if (r13_8 == r8_59) 
                goto addr_c824_47;
            addr_becc_46:
            r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) - reinterpret_cast<unsigned char>(r8_59));
            tmp64_63 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) + reinterpret_cast<unsigned char>(r13_8));
            r12_9 = tmp64_63;
            if (reinterpret_cast<unsigned char>(tmp64_63) < reinterpret_cast<unsigned char>(r9_58)) {
                r12_9 = reinterpret_cast<void**>(0xffffffffffffffff);
            }
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r12_9)) {
                r10_64 = r15_31;
            } else {
                if (!rbx19) {
                    *reinterpret_cast<int32_t*>(&rbx19) = 12;
                    *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                } else {
                    if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                        goto addr_c980_73;
                    rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
                }
                if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r12_9)) {
                    rbx19 = r12_9;
                }
                if (rbx19 == 0xffffffffffffffff) 
                    goto addr_c980_73;
                if (r15_31 == v10) 
                    goto addr_c910_79; else 
                    goto addr_bf27_80;
            }
            addr_bf4c_81:
            rsi39 = r8_59;
            rsp65 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp65 = 0xbf62;
            fun_27e0(reinterpret_cast<unsigned char>(r10_64) + reinterpret_cast<unsigned char>(r9_58), rsi39, r13_8);
            rsp15 = reinterpret_cast<void*>(rsp65 + 1);
            r10_16 = r10_64;
            continue;
            addr_c910_79:
            rsp66 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp66 = 0xc918;
            rax67 = fun_2800(rbx19, rsi39, rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp66 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax67;
            if (!rax67) 
                goto addr_c980_73;
            if (!r9_58) 
                goto addr_bf4c_81;
            rsp68 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp68 = 0xc957;
            rax69 = fun_27e0(rax67, v10, r9_58);
            rsp15 = reinterpret_cast<void*>(rsp68 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax69;
            goto addr_bf4c_81;
            addr_bf27_80:
            rsp70 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp70 = 0xbf32;
            rax71 = fun_2840(r15_31, rbx19, r15_31, rbx19);
            rsp15 = reinterpret_cast<void*>(rsp70 + 1);
            r9_58 = r9_58;
            r8_59 = r8_59;
            r10_64 = rax71;
            if (!rax71) 
                goto addr_c980_73; else 
                goto addr_bf4c_81;
            addr_cb14_64:
            rsp72 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp72 = 0xcb2a;
            rax73 = fun_2800(rbx19, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp72 + 1);
            r9_58 = r9_58;
            if (!rax73) 
                goto addr_cce9_84;
            if (r12_9) 
                goto addr_cb4a_86;
            r10_16 = rax73;
            goto addr_c7ff_55;
            addr_cb4a_86:
            rsi39 = r10_16;
            rsp74 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp74 = 0xcb5f;
            rax75 = fun_27e0(rax73, rsi39, r12_9);
            rsp15 = reinterpret_cast<void*>(rsp74 + 1);
            r9_58 = r9_58;
            r10_16 = rax75;
            goto addr_c7ff_55;
            addr_c7d3_65:
            rsi39 = rbx19;
            v28 = r10_16;
            rsp76 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp76 = 0xc7ec;
            rax77 = fun_2840(r10_16, rsi39);
            rsp15 = reinterpret_cast<void*>(rsp76 + 1);
            r9_58 = r9_58;
            if (!rax77) 
                goto addr_c8cf_17;
            r10_16 = rax77;
            goto addr_c7ff_55;
        }
        break;
    }
    tmp64_78 = r12_9 + 1;
    r13_8 = tmp64_78;
    if (reinterpret_cast<unsigned char>(tmp64_78) < reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_ccda_5;
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(r13_8)) 
        goto addr_cbce_7;
    if (rbx19) {
        if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
            goto addr_c8c8_16;
        rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
        if (reinterpret_cast<unsigned char>(rbx19) < reinterpret_cast<unsigned char>(r13_8)) 
            goto addr_cc8d_94;
    } else {
        if (reinterpret_cast<unsigned char>(r13_8) > reinterpret_cast<unsigned char>(12)) {
            addr_cc8d_94:
            if (r13_8 == 0xffffffffffffffff) 
                goto addr_c8c8_16; else 
                goto addr_cc97_96;
        } else {
            *reinterpret_cast<int32_t*>(&rbx19) = 12;
            *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
        }
    }
    addr_cba3_98:
    if (r10_16 == v10) {
        rsp79 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp79 = 0xccae;
        rax80 = fun_2800(rbx19, rsi39, rbx19, rsi39);
        rsp15 = reinterpret_cast<void*>(rsp79 + 1);
        if (rax80) {
            if (!r12_9) {
                r10_16 = rax80;
                goto addr_cbce_7;
            } else {
                rsp81 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp81 = 0xcccd;
                rax82 = fun_27e0(rax80, r10_16, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp81 + 1);
                r10_16 = rax82;
                goto addr_cbce_7;
            }
        }
    } else {
        v28 = r10_16;
        rsp83 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp83 = 0xcbc2;
        rax84 = fun_2840(r10_16, rbx19, r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp83 + 1);
        r10_16 = rax84;
        if (!rax84) 
            goto addr_c8cf_17; else 
            goto addr_cbce_7;
    }
    addr_cc97_96:
    rbx19 = r13_8;
    goto addr_cba3_98;
    addr_bf8f_50:
    if (r14_56->f50 == -1) 
        goto 0x29fd;
    ecx85 = *reinterpret_cast<uint32_t*>((r14_56->f50 << 5) + reinterpret_cast<int64_t>(v86));
    if (*reinterpret_cast<signed char*>(&edx62) == 0x6e) {
        *reinterpret_cast<uint32_t*>(&r13_87) = ecx85 - 18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_87) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r13_87) > 4) 
            goto 0x2a02;
        goto *reinterpret_cast<int32_t*>(0x10468 + r13_87 * 4) + 0x10468;
    }
    eax88 = r14_56->f10;
    *reinterpret_cast<void***>(v53) = reinterpret_cast<void**>(37);
    r13_8 = v53 + 1;
    if (*reinterpret_cast<unsigned char*>(&eax88) & 1) {
        *reinterpret_cast<void***>(v53 + 1) = reinterpret_cast<void**>(39);
        r13_8 = v53 + 2;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 2) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(45);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 4) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(43);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 8) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(32);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 16) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(35);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 64) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(73);
        ++r13_8;
    }
    if (*reinterpret_cast<unsigned char*>(&eax88) & 32) {
        *reinterpret_cast<void***>(r13_8) = reinterpret_cast<void**>(48);
        ++r13_8;
    }
    rsi89 = r14_56->f18;
    if (rsi89 != r14_56->f20) {
        rax90 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f20) - reinterpret_cast<unsigned char>(rsi89));
        rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp91 = 0xc05f;
        fun_27e0(r13_8, rsi89, rax90);
        rsp15 = reinterpret_cast<void*>(rsp91 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax90));
    }
    rsi92 = r14_56->f30;
    if (rsi92 != r14_56->f38) {
        rax93 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_56->f38) - reinterpret_cast<unsigned char>(rsi92));
        rsp94 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp94 = 0xc099;
        fun_27e0(r13_8, rsi92, rax93);
        rsp15 = reinterpret_cast<void*>(rsp94 + 1);
        r10_16 = r10_16;
        r13_8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_8) + reinterpret_cast<unsigned char>(rax93));
    }
    *reinterpret_cast<uint32_t*>(&rax95) = ecx85 - 7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax95) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax95) <= 9) {
        goto *reinterpret_cast<int32_t*>(0x103f8 + rax95 * 4) + 0x103f8;
    }
    eax96 = r14_56->f48;
    *reinterpret_cast<void***>(r13_8 + 1) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r13_8) = *reinterpret_cast<void***>(&eax96);
    if (r14_56->f28 == -1) {
        v97 = 0;
    } else {
        if (*reinterpret_cast<uint32_t*>((r14_56->f28 << 5) + reinterpret_cast<int64_t>(v86)) != 5) 
            goto 0x29fd;
        v97 = 1;
    }
    if (r14_56->f40 != -1) {
        rcx98 = reinterpret_cast<struct s19*>(reinterpret_cast<int64_t>(v86) + (r14_56->f40 << 5));
        if (rcx98->f0 != 5) 
            goto 0x29fd;
        *reinterpret_cast<int32_t*>(&rax99) = v97;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rbp6) + rax99 * 4 - 0x3b8) = rcx98->f10;
    }
    tmp64_100 = r12_9 + 2;
    if (reinterpret_cast<unsigned char>(tmp64_100) >= reinterpret_cast<unsigned char>(r12_9)) 
        goto addr_c1d2_135;
    if (rbx19 != 0xffffffffffffffff) {
        goto addr_c8c8_16;
    }
    addr_c1d2_135:
    if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) {
        r15_101 = r10_16;
    } else {
        if (rbx19) {
            if (reinterpret_cast<signed char>(rbx19) < reinterpret_cast<signed char>(0)) 
                goto addr_c8c8_16;
            rbx19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx19) + reinterpret_cast<unsigned char>(rbx19));
            if (reinterpret_cast<unsigned char>(rbx19) >= reinterpret_cast<unsigned char>(tmp64_100)) 
                goto addr_c1f3_142; else 
                goto addr_ca82_143;
        } else {
            if (reinterpret_cast<unsigned char>(tmp64_100) > reinterpret_cast<unsigned char>(12)) {
                addr_ca82_143:
                if (tmp64_100 == 0xffffffffffffffff) 
                    goto addr_c8c8_16; else 
                    goto addr_ca8c_145;
            } else {
                *reinterpret_cast<int32_t*>(&rbx19) = 12;
                *reinterpret_cast<int32_t*>(&rbx19 + 4) = 0;
                goto addr_c1f3_142;
            }
        }
    }
    addr_c225_147:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_101) + reinterpret_cast<unsigned char>(r12_9)) = 0;
    *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8) = 0xc22f;
    rax102 = fun_2570();
    *reinterpret_cast<void***>(rax102) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rax103) = ecx85;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax103) + 4) = 0;
    if (reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx19) - reinterpret_cast<unsigned char>(r12_9)) <= 0x7fffffff) {
    }
    if (*reinterpret_cast<uint32_t*>(&rax103) > 17) 
        goto 0x2a02;
    goto *reinterpret_cast<int32_t*>(0x10420 + rax103 * 4) + 0x10420;
    addr_c1f3_142:
    if (r10_16 == v10) {
        rsp104 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp104 = 0xcae8;
        rax105 = fun_2800(rbx19, rsi92);
        rsp15 = reinterpret_cast<void*>(rsp104 + 1);
        r15_101 = rax105;
        if (!rax105) {
            addr_cce9_84:
            rsp106 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
            *rsp106 = 0xccee;
            rax107 = fun_2570();
            rsp15 = reinterpret_cast<void*>(rsp106 + 1);
            r15_31 = v10;
            v32 = rax107;
            goto addr_c8e2_18;
        } else {
            if (r12_9) {
                rsp108 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
                *rsp108 = 0xcb0f;
                fun_27e0(rax105, v10, r12_9);
                rsp15 = reinterpret_cast<void*>(rsp108 + 1);
                goto addr_c225_147;
            }
        }
    } else {
        rsp109 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
        *rsp109 = 0xc212;
        rax110 = fun_2840(r10_16, rbx19);
        rsp15 = reinterpret_cast<void*>(rsp109 + 1);
        r10_16 = r10_16;
        r15_101 = rax110;
        if (!rax110) 
            goto addr_c8c8_16; else 
            goto addr_c225_147;
    }
    addr_ca8c_145:
    rbx19 = tmp64_100;
    goto addr_c1f3_142;
    addr_c980_73:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp15) - 8);
    *rsp111 = 0xc985;
    rax112 = fun_2570();
    rsp15 = reinterpret_cast<void*>(rsp111 + 1);
    v32 = rax112;
    goto addr_c8e2_18;
}

int32_t setlocale_null_r();

int64_t fun_cd23() {
    void* rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax1) - reinterpret_cast<uint64_t>(g28));
    if (rdx7) {
        fun_2690();
    } else {
        return rax3;
    }
}

int64_t fun_cda3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2850(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_2680(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_27e0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_27e0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_ce53() {
    __asm__("cli ");
    goto fun_2850;
}

struct s20 {
    int64_t f0;
    uint32_t* f8;
};

int64_t fun_ce63(int64_t rdi, struct s20* rsi) {
    int64_t rdx3;

    __asm__("cli ");
    if (!rsi->f0) {
        return 0;
    }
    if (*rsi->f8 <= 22) 
        goto addr_ce99_5;
    return 0xffffffff;
    addr_ce99_5:
    *reinterpret_cast<uint32_t*>(&rdx3) = *rsi->f8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x10490 + rdx3 * 4) + 0x10490;
}

struct s21 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
    int64_t f18;
    void** f20;
};

struct s22 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

struct s23 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

struct s24 {
    unsigned char f0;
    signed char[1] pad2;
    void** f2;
};

int64_t fun_d093(void** rdi, struct s21* rsi, struct s22* rdx) {
    void** r10_4;
    void** rax5;
    void** rdi6;
    struct s22* r15_7;
    struct s21* r14_8;
    void** rcx9;
    void** r9_10;
    void** v11;
    void** v12;
    uint32_t edx13;
    void** rbx14;
    int64_t rax15;
    void** r12_16;
    int64_t rbp17;
    int32_t edx18;
    void** rdx19;
    int64_t rcx20;
    int32_t esi21;
    void** rdx22;
    struct s23* rax23;
    uint64_t rdi24;
    int32_t edx25;
    struct s23* rcx26;
    uint64_t rdx27;
    uint64_t rsi28;
    uint64_t rsi29;
    uint64_t tmp64_30;
    int32_t edx31;
    int32_t eax32;
    int64_t rax33;
    int64_t rcx34;
    int32_t eax35;
    int32_t eax36;
    uint32_t eax37;
    void** rdx38;
    uint32_t eax39;
    void* rax40;
    void** rdx41;
    uint32_t eax42;
    void* rax43;
    uint32_t eax44;
    void** rcx45;
    int64_t rsi46;
    int32_t eax47;
    void** rbx48;
    void** rax49;
    int64_t rsi50;
    int32_t edi51;
    void** rbp52;
    void** rdx53;
    void** r8_54;
    void** rdx55;
    void*** rax56;
    void*** rcx57;
    void** r9_58;
    void*** rbp59;
    void** rsi60;
    void** rax61;
    void** rax62;
    void** rdx63;
    void** rax64;
    struct s23* rbx65;
    void* rsi66;
    int32_t eax67;
    struct s23* rdx68;
    void* rax69;
    void* rcx70;
    void* rcx71;
    void* tmp64_72;
    int32_t eax73;
    void** rax74;
    int64_t rdx75;
    int32_t edi76;
    void** rbx77;
    void** r9_78;
    void** rdx79;
    void*** rax80;
    void*** rsi81;
    void** rsi82;
    void** rax83;
    void** rdi84;
    void** r8_85;
    void** rdi86;
    void** rdx87;
    void** rdx88;
    void** rax89;
    void** rax90;
    void** rax91;
    void*** rax92;
    void** rdi93;
    void** rax94;
    struct s24* rbx95;
    void* rdi96;
    int32_t eax97;
    struct s24* rcx98;
    void* rax99;
    void* rdx100;
    void* rdx101;
    void* tmp64_102;
    int32_t eax103;
    int32_t eax104;
    int32_t eax105;
    int64_t rax106;
    int64_t rax107;

    __asm__("cli ");
    r10_4 = reinterpret_cast<void**>(&rsi->f20);
    rax5 = rdi;
    rdi6 = reinterpret_cast<void**>(&rdx->f10);
    r15_7 = rdx;
    r14_8 = rsi;
    rcx9 = r10_4;
    *reinterpret_cast<int32_t*>(&r9_10) = 7;
    *reinterpret_cast<int32_t*>(&r9_10 + 4) = 0;
    rsi->f0 = 0;
    rsi->f8 = r10_4;
    v11 = rdi6;
    rdx->f0 = reinterpret_cast<void**>(0);
    rdx->f8 = rdi6;
    v12 = reinterpret_cast<void**>(0);
    while (edx13 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5)), !!*reinterpret_cast<signed char*>(&edx13)) {
        rbx14 = rax5 + 1;
        if (*reinterpret_cast<signed char*>(&edx13) == 37) 
            goto addr_d148_4;
        rax5 = rbx14;
    }
    *reinterpret_cast<void***>(rcx9) = rax5;
    r14_8->f10 = 0;
    r14_8->f18 = 0;
    *reinterpret_cast<int32_t*>(&rax15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    addr_d135_7:
    return rax15;
    addr_d148_4:
    r12_16 = rcx9;
    *reinterpret_cast<void***>(r12_16) = rax5;
    *reinterpret_cast<uint32_t*>(r12_16 + 16) = 0;
    *reinterpret_cast<void***>(r12_16 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 32) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<void***>(r12_16 + 48) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 56) = reinterpret_cast<void**>(0);
    *reinterpret_cast<void***>(r12_16 + 64) = reinterpret_cast<void**>(0xffffffffffffffff);
    *reinterpret_cast<int64_t*>(r12_16 + 80) = -1;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax5 + 1));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    edx18 = static_cast<int32_t>(rbp17 - 48);
    if (*reinterpret_cast<unsigned char*>(&edx18) <= 9) {
        rdx19 = rbx14;
        do {
            *reinterpret_cast<uint32_t*>(&rcx20) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx19 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx20) + 4) = 0;
            ++rdx19;
            esi21 = static_cast<int32_t>(rcx20 - 48);
        } while (*reinterpret_cast<unsigned char*>(&esi21) <= 9);
        if (*reinterpret_cast<signed char*>(&rcx20) != 36) 
            goto addr_d1b9_11;
    } else {
        addr_d1b9_11:
        rdx22 = rbx14 + 1;
        if (*reinterpret_cast<signed char*>(&rbp17) == 39) {
            do {
                *reinterpret_cast<uint32_t*>(r12_16 + 16) = *reinterpret_cast<uint32_t*>(r12_16 + 16) | 1;
                *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx22));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                rbx14 = rdx22;
                rdx22 = rbx14 + 1;
            } while (*reinterpret_cast<signed char*>(&rbp17) == 39);
            goto addr_d1d0_14;
        } else {
            goto addr_d1d0_14;
        }
    }
    rax23 = reinterpret_cast<struct s23*>(rax5 + 2);
    *reinterpret_cast<int32_t*>(&rdi24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0;
    while (1) {
        edx25 = static_cast<int32_t>(rbp17 - 48);
        rcx26 = reinterpret_cast<struct s23*>(reinterpret_cast<uint64_t>(rax23) + 0xffffffffffffffff);
        rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx25)));
        if (rdi24 > 0x1999999999999999) {
            rsi28 = 0xffffffffffffffff;
        } else {
            rsi29 = rdi24 + rdi24 * 4;
            rsi28 = rsi29 + rsi29;
        }
        while (*reinterpret_cast<uint32_t*>(&rbp17) = rax23->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0, tmp64_30 = rsi28 + rdx27, rdi24 = tmp64_30, edx31 = static_cast<int32_t>(rbp17 - 48), tmp64_30 < rsi28) {
            if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
                goto addr_d698_22;
            rcx26 = rax23;
            rdx27 = reinterpret_cast<uint64_t>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&edx31)));
            rax23 = reinterpret_cast<struct s23*>(&rax23->pad2);
            rsi28 = 0xffffffffffffffff;
        }
        if (*reinterpret_cast<unsigned char*>(&edx31) > 9) 
            break;
        rax23 = reinterpret_cast<struct s23*>(&rax23->pad2);
    }
    if (tmp64_30 - 1 > 0xfffffffffffffffd) 
        goto addr_d698_22;
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rcx26->f2));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    rbx14 = reinterpret_cast<void**>(&rcx26->f2);
    goto addr_d1b9_11;
    addr_d1d0_14:
    eax32 = static_cast<int32_t>(rbp17 - 32);
    if (*reinterpret_cast<unsigned char*>(&eax32) <= 41) {
        *reinterpret_cast<uint32_t*>(&rax33) = *reinterpret_cast<unsigned char*>(&eax32);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x1050c + rax33 * 4) + 0x1050c;
    }
    if (*reinterpret_cast<signed char*>(&rbp17) == 42) {
        *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
        *reinterpret_cast<void***>(r12_16 + 32) = rdx22;
        *reinterpret_cast<uint32_t*>(&rcx34) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0;
        if (0) {
        }
        eax35 = static_cast<int32_t>(rcx34 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax35) > 9) 
            goto addr_d317_33;
    } else {
        eax36 = static_cast<int32_t>(rbp17 - 48);
        if (*reinterpret_cast<unsigned char*>(&eax36) <= 9) {
            *reinterpret_cast<void***>(r12_16 + 24) = rbx14;
            eax37 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14) - 48);
            if (*reinterpret_cast<unsigned char*>(&eax37) > 9) {
                addr_da19_36:
                *reinterpret_cast<void***>(r12_16 + 32) = rbx14;
                goto addr_da1e_37;
            } else {
                rdx38 = rbx14;
                do {
                    ++rdx38;
                    eax39 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx38 + 1) - 48);
                } while (*reinterpret_cast<unsigned char*>(&eax39) <= 9);
                rax40 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx38) - reinterpret_cast<unsigned char>(rbx14));
                rbx14 = rdx38;
                if (0 >= reinterpret_cast<uint64_t>(rax40)) 
                    goto label_41; else 
                    goto addr_da14_42;
            }
        } else {
            addr_d1fd_43:
            if (*reinterpret_cast<signed char*>(&rbp17) == 46) {
                addr_d418_44:
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx14 + 1) == 42)) {
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    rdx41 = rbx14 + 1;
                    eax42 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rbx14 + 1) - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax42) > 9) {
                        rbx14 = rdx41;
                        *reinterpret_cast<int32_t*>(&rax43) = 1;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax43) + 4) = 0;
                    } else {
                        do {
                            ++rdx41;
                            eax44 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx41 + 1) - 48);
                        } while (*reinterpret_cast<unsigned char*>(&eax44) <= 9);
                        rax43 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx41) - reinterpret_cast<unsigned char>(rbx14));
                        rbx14 = rdx41;
                    }
                    *reinterpret_cast<void***>(r12_16 + 56) = rdx41;
                    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                    if (0 >= reinterpret_cast<uint64_t>(rax43)) {
                    }
                    goto addr_d207_52;
                } else {
                    rcx45 = rbx14 + 2;
                    *reinterpret_cast<void***>(r12_16 + 48) = rbx14;
                    *reinterpret_cast<void***>(r12_16 + 56) = rcx45;
                    *reinterpret_cast<uint32_t*>(&rsi46) = *reinterpret_cast<unsigned char*>(rbx14 + 2);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0;
                    if (0) {
                    }
                    eax47 = static_cast<int32_t>(rsi46 - 48);
                    if (*reinterpret_cast<unsigned char*>(&eax47) <= 9) 
                        goto addr_dc04_56; else 
                        goto addr_d455_57;
                }
            } else {
                addr_d207_52:
                rbx48 = rbx14 + 1;
                if (*reinterpret_cast<signed char*>(&rbp17) == 0x68) {
                    do {
                        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx48));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
                        ++rbx48;
                    } while (*reinterpret_cast<signed char*>(&rbp17) == 0x68);
                    goto addr_d228_60;
                } else {
                    goto addr_d228_60;
                }
            }
        }
    }
    rax49 = rdx22;
    do {
        *reinterpret_cast<uint32_t*>(&rsi50) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax49 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi50) + 4) = 0;
        ++rax49;
        edi51 = static_cast<int32_t>(rsi50 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi51) <= 9);
    if (*reinterpret_cast<signed char*>(&rsi50) == 36) 
        goto addr_d79b_65;
    addr_d317_33:
    *reinterpret_cast<void***>(r12_16 + 40) = reinterpret_cast<void**>(0);
    if (0) 
        goto addr_d698_22;
    rbp52 = reinterpret_cast<void**>(0);
    v12 = reinterpret_cast<void**>(1);
    rbx14 = rdx22;
    addr_d33c_67:
    rdx53 = r15_7->f8;
    r8_54 = rdx53;
    if (reinterpret_cast<unsigned char>(7) > reinterpret_cast<unsigned char>(rbp52)) {
        addr_d3ba_68:
        rdx55 = r15_7->f0;
        rax56 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx55) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52)) {
            do {
                ++rdx55;
                *rax56 = reinterpret_cast<void**>(0);
                rcx57 = rax56;
                rax56 = rax56 + 32;
            } while (reinterpret_cast<unsigned char>(rdx55) <= reinterpret_cast<unsigned char>(rbp52));
            r15_7->f0 = rdx55;
            *rcx57 = reinterpret_cast<void**>(0);
        }
    } else {
        r9_58 = reinterpret_cast<void**>(14);
        if (reinterpret_cast<unsigned char>(14) <= reinterpret_cast<unsigned char>(rbp52)) {
            r9_58 = rbp52 + 1;
        }
        if (reinterpret_cast<unsigned char>(r9_58) >> 59) 
            goto addr_dcbb_75; else 
            goto addr_d363_76;
    }
    rbp59 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbp52) << 5) + reinterpret_cast<unsigned char>(r8_54));
    if (*rbp59) {
        if (*rbp59 != 5) {
            goto addr_d69c_80;
        }
    } else {
        *rbp59 = reinterpret_cast<void**>(5);
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        if (*reinterpret_cast<signed char*>(&rbp17) != 46) 
            goto addr_d207_52;
        goto addr_d418_44;
    }
    addr_da1e_37:
    *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx14));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
    goto addr_d1fd_43;
    addr_dcbb_75:
    if (v11 != rdx53) {
        fun_2550(rdx53);
        r10_4 = r10_4;
        goto addr_daca_84;
    } else {
        goto addr_daca_84;
    }
    addr_d363_76:
    rsi60 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_58) << 5);
    if (v11 == rdx53) {
        rax61 = fun_2800(rsi60, rsi60);
        rdx53 = rdx53;
        r9_10 = r9_58;
        r10_4 = r10_4;
        r8_54 = rax61;
    } else {
        rax62 = fun_2840(rdx53, rsi60);
        rdx53 = r15_7->f8;
        r10_4 = r10_4;
        r9_10 = r9_58;
        r8_54 = rax62;
    }
    if (!r8_54) 
        goto addr_dcbb_75;
    if (v11 == rdx53) {
        rdx63 = r15_7->f0;
        rax64 = fun_27e0(r8_54, v11, reinterpret_cast<unsigned char>(rdx63) << 5);
        r9_10 = r9_10;
        r10_4 = r10_4;
        r8_54 = rax64;
    }
    r15_7->f8 = r8_54;
    goto addr_d3ba_68;
    addr_d79b_65:
    rbx65 = reinterpret_cast<struct s23*>(rbx14 + 2);
    *reinterpret_cast<int32_t*>(&rsi66) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi66) + 4) = 0;
    while (1) {
        eax67 = static_cast<int32_t>(rcx34 - 48);
        rdx68 = reinterpret_cast<struct s23*>(reinterpret_cast<uint64_t>(rbx65) + 0xffffffffffffffff);
        rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax67)));
        if (reinterpret_cast<uint64_t>(rsi66) > 0x1999999999999999) {
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rcx71 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsi66) + reinterpret_cast<uint64_t>(rsi66) * 4);
            rcx70 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx71) + reinterpret_cast<uint64_t>(rcx71));
        }
        while (tmp64_72 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx70) + reinterpret_cast<uint64_t>(rax69)), rsi66 = tmp64_72, *reinterpret_cast<uint32_t*>(&rcx34) = rbx65->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx34) + 4) = 0, eax73 = static_cast<int32_t>(rcx34 - 48), reinterpret_cast<uint64_t>(tmp64_72) < reinterpret_cast<uint64_t>(rcx70)) {
            if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
                goto addr_d698_22;
            rdx68 = rbx65;
            rax69 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax73)));
            rbx65 = reinterpret_cast<struct s23*>(&rbx65->pad2);
            rcx70 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax73) > 9) 
            break;
        rbx65 = reinterpret_cast<struct s23*>(&rbx65->pad2);
    }
    rbp52 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_72) - 1);
    if (reinterpret_cast<unsigned char>(rbp52) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_d698_22;
    *reinterpret_cast<void***>(r12_16 + 40) = rbp52;
    rbx14 = reinterpret_cast<void**>(&rdx68->f2);
    goto addr_d33c_67;
    label_41:
    addr_da14_42:
    goto addr_da19_36;
    addr_dc04_56:
    rax74 = rcx45;
    do {
        *reinterpret_cast<uint32_t*>(&rdx75) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74 + 1));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
        ++rax74;
        edi76 = static_cast<int32_t>(rdx75 - 48);
    } while (*reinterpret_cast<unsigned char*>(&edi76) <= 9);
    if (*reinterpret_cast<signed char*>(&rdx75) == 36) 
        goto addr_dc2a_104;
    addr_d455_57:
    rbx77 = *reinterpret_cast<void***>(r12_16 + 64);
    if (rbx77 == 0xffffffffffffffff) {
        *reinterpret_cast<void***>(r12_16 + 64) = v12;
        if (0) {
            addr_d698_22:
            r8_54 = r15_7->f8;
            goto addr_d69c_80;
        } else {
            rbx77 = v12;
        }
    }
    addr_d464_107:
    r8_54 = r15_7->f8;
    if (reinterpret_cast<unsigned char>(r9_10) <= reinterpret_cast<unsigned char>(rbx77)) {
        r9_78 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_10) + reinterpret_cast<unsigned char>(r9_10));
        if (reinterpret_cast<unsigned char>(r9_78) <= reinterpret_cast<unsigned char>(rbx77)) {
            r9_78 = rbx77 + 1;
        }
        if (!(reinterpret_cast<unsigned char>(r9_78) >> 59)) 
            goto addr_db12_111;
    } else {
        addr_d471_112:
        rdx79 = r15_7->f0;
        rax80 = reinterpret_cast<void***>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rdx79) << 5) + reinterpret_cast<unsigned char>(r8_54));
        if (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77)) {
            do {
                ++rdx79;
                *rax80 = reinterpret_cast<void**>(0);
                rsi81 = rax80;
                rax80 = rax80 + 32;
            } while (reinterpret_cast<unsigned char>(rdx79) <= reinterpret_cast<unsigned char>(rbx77));
            r15_7->f0 = rdx79;
            *rsi81 = reinterpret_cast<void**>(0);
            goto addr_d4a7_116;
        }
    }
    rdx53 = r8_54;
    goto addr_dcbb_75;
    addr_db12_111:
    rsi82 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_78) << 5);
    if (v11 == r8_54) {
        rax83 = fun_2800(rsi82, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        rdi84 = rax83;
        r8_85 = r8_54;
        if (!rax83) {
            addr_daca_84:
            rdi86 = r14_8->f8;
            if (r10_4 != rdi86) {
                fun_2550(rdi86);
            }
        } else {
            addr_dd50_120:
            rdx87 = r15_7->f0;
            rdx88 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx87) << 5);
            rax89 = fun_27e0(rdi84, r8_85, rdx88, rdi84, r8_85, rdx88);
            r10_4 = r10_4;
            rcx45 = rcx45;
            r8_54 = rax89;
            goto addr_db6f_121;
        }
    } else {
        rax90 = fun_2840(r8_54, rsi82);
        rcx45 = rcx45;
        r10_4 = r10_4;
        r8_54 = rax90;
        if (!rax90) {
            rdx53 = r15_7->f8;
            goto addr_dcbb_75;
        } else {
            if (v11 == r15_7->f8) {
                rdi84 = r8_54;
                r8_85 = v11;
                goto addr_dd50_120;
            }
        }
    }
    rax91 = fun_2570();
    *reinterpret_cast<void***>(rax91) = reinterpret_cast<void**>(12);
    return 0xffffffff;
    addr_db6f_121:
    r15_7->f8 = r8_54;
    goto addr_d471_112;
    addr_d4a7_116:
    rax92 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r8_54) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rbx77) << 5));
    if (*rax92) {
        if (!reinterpret_cast<int1_t>(*rax92 == 5)) {
            addr_d69c_80:
            if (v11 != r8_54) {
                fun_2550(r8_54, r8_54);
                r10_4 = r10_4;
            }
        } else {
            *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
            rbx14 = rcx45;
            goto addr_d207_52;
        }
    } else {
        *rax92 = reinterpret_cast<void**>(5);
        rbx14 = rcx45;
        *reinterpret_cast<uint32_t*>(&rbp17) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx45));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp17) + 4) = 0;
        goto addr_d207_52;
    }
    rdi93 = r14_8->f8;
    if (r10_4 != rdi93) {
        fun_2550(rdi93, rdi93);
    }
    rax94 = fun_2570();
    *reinterpret_cast<void***>(rax94) = reinterpret_cast<void**>(22);
    *reinterpret_cast<int32_t*>(&rax15) = -1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
    goto addr_d135_7;
    addr_dc2a_104:
    rbx95 = reinterpret_cast<struct s24*>(rbx14 + 3);
    *reinterpret_cast<int32_t*>(&rdi96) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi96) + 4) = 0;
    while (1) {
        eax97 = static_cast<int32_t>(rsi46 - 48);
        rcx98 = reinterpret_cast<struct s24*>(reinterpret_cast<uint64_t>(rbx95) + 0xffffffffffffffff);
        rax99 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax97)));
        if (reinterpret_cast<uint64_t>(rdi96) > 0x1999999999999999) {
            rdx100 = reinterpret_cast<void*>(0xffffffffffffffff);
        } else {
            rdx101 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi96) + reinterpret_cast<uint64_t>(rdi96) * 4);
            rdx100 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx101) + reinterpret_cast<uint64_t>(rdx101));
        }
        while (*reinterpret_cast<uint32_t*>(&rsi46) = rbx95->f0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi46) + 4) = 0, tmp64_102 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx100) + reinterpret_cast<uint64_t>(rax99)), rdi96 = tmp64_102, eax103 = static_cast<int32_t>(rsi46 - 48), reinterpret_cast<uint64_t>(tmp64_102) < reinterpret_cast<uint64_t>(rdx100)) {
            if (*reinterpret_cast<unsigned char*>(&eax103) > 9) 
                goto addr_d698_22;
            rcx98 = rbx95;
            rax99 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<signed char*>(&eax103)));
            rbx95 = reinterpret_cast<struct s24*>(&rbx95->pad2);
            rdx100 = reinterpret_cast<void*>(0xffffffffffffffff);
        }
        if (*reinterpret_cast<unsigned char*>(&eax103) > 9) 
            break;
        rbx95 = reinterpret_cast<struct s24*>(&rbx95->pad2);
    }
    rbx77 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(tmp64_102) + 0xffffffffffffffff);
    if (reinterpret_cast<unsigned char>(rbx77) > reinterpret_cast<unsigned char>(0xfffffffffffffffd)) 
        goto addr_d698_22;
    *reinterpret_cast<void***>(r12_16 + 64) = rbx77;
    rcx45 = reinterpret_cast<void**>(&rcx98->f2);
    goto addr_d464_107;
    addr_d228_60:
    eax104 = static_cast<int32_t>(rbp17 - 76);
    if (*reinterpret_cast<unsigned char*>(&eax104) > 46) {
        eax105 = static_cast<int32_t>(rbp17 - 37);
        if (*reinterpret_cast<unsigned char*>(&eax105) <= 83) {
            *reinterpret_cast<uint32_t*>(&rax106) = *reinterpret_cast<unsigned char*>(&eax105);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax106) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0x10670 + rax106 * 4) + 0x10670;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rax107) = *reinterpret_cast<unsigned char*>(&eax104);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax107) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x105b4 + rax107 * 4) + 0x105b4;
    }
}

void fun_de23() {
    __asm__("cli ");
}

void fun_de37() {
    __asm__("cli ");
    return;
}

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2960(int64_t rdi, void** rsi);

uint32_t fun_2950(void** rdi, void** rsi);

void fun_8255() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2660();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2660();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2680(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_8553_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_8553_22; else 
                            goto addr_894d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_8a0d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_8d60_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_8550_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_8550_30; else 
                                goto addr_8d79_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2680(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_8d60_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2770(rdi39, rsi30, rdx10, rdi39, rsi30, rdx10);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_8d60_28; else 
                            goto addr_83fc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_8ec0_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_8d40_40:
                        if (r11_27 == 1) {
                            addr_88cd_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_8e88_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_8507_44;
                            }
                        } else {
                            goto addr_8d50_46;
                        }
                    } else {
                        addr_8ecf_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_88cd_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_8553_22:
                                if (v47 != 1) {
                                    addr_8aa9_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2680(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_8af4_54;
                                    }
                                } else {
                                    goto addr_8560_56;
                                }
                            } else {
                                addr_8505_57:
                                ebp36 = 0;
                                goto addr_8507_44;
                            }
                        } else {
                            addr_8d34_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_8ecf_47; else 
                                goto addr_8d3e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_88cd_41;
                        if (v47 == 1) 
                            goto addr_8560_56; else 
                            goto addr_8aa9_52;
                    }
                }
                addr_85c1_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_8458_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_847d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_8780_65;
                    } else {
                        addr_85e9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_8e38_67;
                    }
                } else {
                    goto addr_85e0_69;
                }
                addr_8491_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_84dc_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_8e38_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_84dc_81;
                }
                addr_85e0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_847d_64; else 
                    goto addr_85e9_66;
                addr_8507_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_85bf_91;
                if (v22) 
                    goto addr_851f_93;
                addr_85bf_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_85c1_62;
                addr_8af4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_927b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_92eb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_90ef_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2960(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2950(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_8bee_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_85ac_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_8bf8_112;
                    }
                } else {
                    addr_8bf8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_8cc9_114;
                }
                addr_85b8_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_85bf_91;
                while (1) {
                    addr_8cc9_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_91d7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_8c36_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_91e5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_8cb7_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_8cb7_128;
                        }
                    }
                    addr_8c65_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_8cb7_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_8c36_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_8c65_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_84dc_81;
                addr_91e5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8e38_67;
                addr_927b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_8bee_109;
                addr_92eb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_8bee_109;
                addr_8560_56:
                rax93 = fun_29a0(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_85ac_110;
                addr_8d3e_59:
                goto addr_8d40_40;
                addr_8a0d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_8553_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_85b8_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_8505_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_8553_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_8a52_160;
                if (!v22) 
                    goto addr_8e27_162; else 
                    goto addr_9033_163;
                addr_8a52_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_8e27_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_8e38_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_88fb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_8763_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_8491_70; else 
                    goto addr_8777_169;
                addr_88fb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_8458_63;
                goto addr_85e0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_8d34_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_8e6f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_8550_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_8448_178; else 
                        goto addr_8df2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_8d34_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_8553_22;
                }
                addr_8e6f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_8550_30:
                    r8d42 = 0;
                    goto addr_8553_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_85c1_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_8e88_42;
                    }
                }
                addr_8448_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_8458_63;
                addr_8df2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_8d50_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_85c1_62;
                } else {
                    addr_8e02_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_8553_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_95b2_188;
                if (v28) 
                    goto addr_8e27_162;
                addr_95b2_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_8763_168;
                addr_83fc_37:
                if (v22) 
                    goto addr_93f3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_8413_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_8ec0_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_8f4b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_8553_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_8448_178; else 
                        goto addr_8f27_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_8d34_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_8553_22;
                }
                addr_8f4b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_8553_22;
                }
                addr_8f27_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_8d50_46;
                goto addr_8e02_186;
                addr_8413_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_8553_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_8553_22; else 
                    goto addr_8424_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_94fe_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_9384_210;
            if (1) 
                goto addr_9382_212;
            if (!v29) 
                goto addr_8fbe_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2670();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_94f1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_8780_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_853b_219; else 
            goto addr_879a_220;
        addr_851f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_8533_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_879a_220; else 
            goto addr_853b_219;
        addr_90ef_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_879a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2670();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_910d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2670();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_9580_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_8fe6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_91d7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_8533_221;
        addr_9033_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_8533_221;
        addr_8777_169:
        goto addr_8780_65;
        addr_94fe_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_879a_220;
        goto addr_910d_222;
        addr_9384_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = v112 - reinterpret_cast<uint64_t>(g28);
        if (!rax111) 
            goto addr_93de_236;
        fun_2690();
        rsp25 = rsp25 - 8 + 8;
        goto addr_9580_225;
        addr_9382_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_9384_210;
        addr_8fbe_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_9384_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_8fe6_226;
        }
        addr_94f1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_894d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xfcac + rax113 * 4) + 0xfcac;
    addr_8d79_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xfdac + rax114 * 4) + 0xfdac;
    addr_93f3_190:
    addr_853b_219:
    goto 0x8220;
    addr_8424_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xfbac + rax115 * 4) + 0xfbac;
    addr_93de_236:
    goto v116;
}

void fun_8440() {
}

void fun_85f8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x82f2;
}

void fun_8651() {
    goto 0x82f2;
}

void fun_873e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x85c1;
    }
    if (v2) 
        goto 0x9033;
    if (!r10_3) 
        goto addr_919e_5;
    if (!v4) 
        goto addr_906e_7;
    addr_919e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_906e_7:
    goto 0x8474;
}

void fun_875c() {
}

void fun_8807() {
    signed char v1;

    if (v1) {
        goto 0x878f;
    } else {
        goto 0x84ca;
    }
}

void fun_8821() {
    signed char v1;

    if (!v1) 
        goto 0x881a; else 
        goto "???";
}

void fun_8848() {
    goto 0x8763;
}

void fun_88c8() {
}

void fun_88e0() {
}

void fun_890f() {
    goto 0x8763;
}

void fun_8961() {
    goto 0x88f0;
}

void fun_8990() {
    goto 0x88f0;
}

void fun_89c3() {
    goto 0x88f0;
}

void fun_8d90() {
    goto 0x8448;
}

void fun_908e() {
    signed char v1;

    if (v1) 
        goto 0x9033;
    goto 0x8474;
}

void fun_9135() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x8474;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x8458;
        goto 0x8474;
    }
}

void fun_9552() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x87c0;
    } else {
        goto 0x82f2;
    }
}

void fun_a5b8() {
    fun_2660();
}

void fun_b33c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0xb34b;
}

void fun_b40c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0xb419;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0xb34b;
}

void fun_b430() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_b45c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xb34b;
}

void fun_b47d() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xb34b;
}

void fun_b4a1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xb34b;
}

void fun_b4c5() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xb454;
}

void fun_b4e9() {
}

void fun_b509() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xb454;
}

void fun_b525() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xb454;
}

struct s25 {
    signed char[80] pad80;
    int64_t f50;
};

struct s26 {
    signed char[1] pad1;
    signed char f1;
};

struct s27 {
    signed char[72] pad72;
    unsigned char f48;
};

void fun_c298() {
    void** rdi1;
    void* r15_2;
    void* r12_3;
    void** r9_4;
    struct s25* r14_5;
    int64_t rbp6;
    int64_t rbp7;
    void** r9_8;
    int64_t rbp9;
    int64_t r8_10;
    int64_t rbp11;
    int64_t rbp12;
    int64_t rsi13;
    uint64_t rsi14;
    void* rbp15;
    int32_t eax16;
    int64_t rdx17;
    int64_t rbp18;
    void** rsi19;
    int64_t rbp20;
    void** rax21;
    int64_t rbp22;
    void** r9_23;
    int64_t rbp24;
    int64_t r8_25;
    int64_t rbp26;
    int64_t rbp27;
    int64_t rsi28;
    uint64_t rsi29;
    void* rbp30;
    int64_t rbp31;
    int64_t r8_32;
    int64_t rbp33;
    int64_t rbp34;
    int64_t rsi35;
    uint64_t rsi36;
    void* rbp37;
    void** v38;
    int64_t rbp39;
    int64_t rbp40;
    void** rcx41;
    uint64_t r15_42;
    int64_t r12_43;
    void** rax44;
    int64_t rbp45;
    int64_t rbp46;
    int64_t rbp47;
    uint64_t r13_48;
    int64_t rbp49;
    int64_t rbx50;
    int64_t rbx51;
    void** rax52;
    void** rcx53;
    void* rbx54;
    void* rbx55;
    void** tmp64_56;
    void* r12_57;
    void** rax58;
    void** rbx59;
    int64_t r15_60;
    int64_t rbp61;
    void** r15_62;
    void** rax63;
    void** rax64;
    int64_t r12_65;
    void** r15_66;
    void** r12_67;
    int64_t rbp68;
    int64_t rbp69;
    uint32_t eax70;
    struct s27* r14_71;
    int32_t eax72;
    int64_t rbp73;

    rdi1 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(r15_2) + reinterpret_cast<uint64_t>(r12_3));
    r9_4 = *reinterpret_cast<void***>((r14_5->f50 << 5) + reinterpret_cast<int64_t>(*reinterpret_cast<void**>(rbp6 - 0x3a8)) + 16);
    if (*reinterpret_cast<int32_t*>(rbp7 - 0x3d8) == 1) {
        *reinterpret_cast<int32_t*>(&r9_8) = *reinterpret_cast<int32_t*>(rbp9 - 0x3b8);
        *reinterpret_cast<int32_t*>(&r9_8 + 4) = 0;
        r8_10 = *reinterpret_cast<int64_t*>(rbp11 - 0x3e0);
        *reinterpret_cast<int64_t*>(rbp12 - 0x418) = rsi13;
        eax16 = fun_2530(rdi1, rsi14, 1, -1, r8_10, r9_8, r9_4, reinterpret_cast<int64_t>(rbp15) + 0xfffffffffffffc44, __return_address());
        *reinterpret_cast<int32_t*>(&rdx17) = *reinterpret_cast<int32_t*>(rbp18 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
        rsi19 = *reinterpret_cast<void***>(rbp20 - 0x418);
        if (*reinterpret_cast<int32_t*>(&rdx17) < 0) 
            goto addr_c423_5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp7 - 0x3d8) == 2) {
            *reinterpret_cast<int32_t*>(&rax21) = *reinterpret_cast<int32_t*>(rbp22 - 0x3b4);
            *reinterpret_cast<int32_t*>(&rax21 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_23) = *reinterpret_cast<int32_t*>(rbp24 - 0x3b8);
            *reinterpret_cast<int32_t*>(&r9_23 + 4) = 0;
            r8_25 = *reinterpret_cast<int64_t*>(rbp26 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp27 - 0x418) = rsi28;
            eax16 = fun_2530(rdi1, rsi29, 1, -1, r8_25, r9_23, rax21, r9_4, reinterpret_cast<int64_t>(rbp30) + 0xfffffffffffffc44);
            rsi19 = *reinterpret_cast<void***>(rbp31 - 0x418);
        } else {
            r8_32 = *reinterpret_cast<int64_t*>(rbp33 - 0x3e0);
            *reinterpret_cast<int64_t*>(rbp34 - 0x418) = rsi35;
            eax16 = fun_2530(rdi1, rsi36, 1, -1, r8_32, r9_4, reinterpret_cast<int64_t>(rbp37) + 0xfffffffffffffc44, v38, __return_address());
            rsi19 = *reinterpret_cast<void***>(rbp39 - 0x418);
        }
        *reinterpret_cast<int32_t*>(&rdx17) = *reinterpret_cast<int32_t*>(rbp40 - 0x3bc);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rdx17) < 0) 
            goto addr_c423_5;
    }
    rcx41 = reinterpret_cast<void**>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&rdx17)));
    if (reinterpret_cast<unsigned char>(rcx41) < reinterpret_cast<unsigned char>(rsi19)) {
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx41) + r15_42 + r12_43)) 
            goto 0x29fd;
    }
    if (*reinterpret_cast<int32_t*>(&rdx17) >= eax16) {
        addr_c32d_16:
        *reinterpret_cast<int32_t*>(&rax44) = static_cast<int32_t>(rdx17 + 1);
        *reinterpret_cast<int32_t*>(&rax44 + 4) = 0;
        if (reinterpret_cast<unsigned char>(rax44) < reinterpret_cast<unsigned char>(rsi19)) {
            **reinterpret_cast<int32_t**>(rbp45 - 0x3d0) = *reinterpret_cast<int32_t*>(rbp46 - 0x40c);
            goto 0xc807;
        }
    } else {
        addr_c325_18:
        *reinterpret_cast<int32_t*>(rbp47 - 0x3bc) = eax16;
        *reinterpret_cast<int32_t*>(&rdx17) = eax16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx17) + 4) = 0;
        goto addr_c32d_16;
    }
    if (r13_48 > 0x7ffffffe) {
        **reinterpret_cast<int32_t**>(rbp49 - 0x3d0) = 75;
        goto 0xc470;
    }
    if (rbx50 < 0) {
        if (rbx51 == -1) 
            goto 0xc248;
        goto 0xc8e2;
    }
    *reinterpret_cast<int32_t*>(&rax52) = static_cast<int32_t>(rdx17 + 2);
    *reinterpret_cast<int32_t*>(&rax52 + 4) = 0;
    rcx53 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbx54) + reinterpret_cast<uint64_t>(rbx55));
    tmp64_56 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax52) + reinterpret_cast<uint64_t>(r12_57));
    rax58 = tmp64_56;
    if (reinterpret_cast<unsigned char>(tmp64_56) < reinterpret_cast<unsigned char>(rax52)) 
        goto 0xc8e2;
    if (reinterpret_cast<unsigned char>(rax58) >= reinterpret_cast<unsigned char>(rcx53)) 
        goto addr_c366_26;
    rax58 = rcx53;
    addr_c366_26:
    if (reinterpret_cast<unsigned char>(rbx59) >= reinterpret_cast<unsigned char>(rax58)) 
        goto 0xc248;
    if (reinterpret_cast<unsigned char>(rcx53) >= reinterpret_cast<unsigned char>(rax58)) {
        rax58 = rcx53;
    }
    if (rax58 == 0xffffffffffffffff) 
        goto 0xc8e2;
    if (r15_60 != *reinterpret_cast<int64_t*>(rbp61 - 0x3e8)) {
        rax63 = fun_2840(r15_62, rax58);
        if (!rax63) 
            goto 0xc8e2;
        goto 0xc248;
    }
    rax64 = fun_2800(rax58, rsi19);
    if (!rax64) 
        goto 0xc8e2;
    if (r12_65) 
        goto addr_c72a_36;
    goto 0xc248;
    addr_c72a_36:
    fun_27e0(rax64, r15_66, r12_67, rax64, r15_66, r12_67);
    goto 0xc248;
    addr_c423_5:
    if ((*reinterpret_cast<struct s26**>(rbp68 - 0x3f0))->f1) {
        (*reinterpret_cast<struct s26**>(rbp68 - 0x3f0))->f1 = 0;
        goto 0xc248;
    }
    if (eax16 >= 0) 
        goto addr_c325_18;
    if (**reinterpret_cast<int32_t**>(rbp69 - 0x3d0)) 
        goto 0xc470;
    eax70 = static_cast<uint32_t>(r14_71->f48) & 0xffffffef;
    eax72 = 22;
    if (*reinterpret_cast<signed char*>(&eax70) != 99) 
        goto addr_c467_42;
    eax72 = 84;
    addr_c467_42:
    **reinterpret_cast<int32_t**>(rbp73 - 0x3d0) = eax72;
}

void fun_c3b0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
        }
    }
}

void fun_c4a0() {
    int64_t rbp1;
    void** r9_2;
    int64_t rbp3;
    int64_t r8_4;
    int64_t rbp5;
    void** rax6;
    int64_t rbp7;
    int64_t rbp8;
    int64_t rsi9;
    void* r15_10;
    void* r12_11;
    uint64_t rsi12;
    void** v13;
    void** v14;

    __asm__("fld tword [rax+0x10]");
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) {
        __asm__("fstp tword [rsp]");
        goto 0xc5e5;
    } else {
        if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 2) {
            *reinterpret_cast<int32_t*>(&r9_2) = *reinterpret_cast<int32_t*>(rbp3 - 0x3b8);
            *reinterpret_cast<int32_t*>(&r9_2 + 4) = 0;
            r8_4 = *reinterpret_cast<int64_t*>(rbp5 - 0x3e0);
            *reinterpret_cast<int32_t*>(&rax6) = *reinterpret_cast<int32_t*>(rbp7 - 0x3b4);
            *reinterpret_cast<int32_t*>(&rax6 + 4) = 0;
            *reinterpret_cast<int64_t*>(rbp8 - 0x418) = rsi9;
            __asm__("fstp tword [rsp+0x8]");
            fun_2530(reinterpret_cast<int64_t>(r15_10) + reinterpret_cast<uint64_t>(r12_11), rsi12, 1, -1, r8_4, r9_2, rax6, v13, v14);
            goto 0xc2fd;
        } else {
            __asm__("fstp tword [rsp]");
            goto 0xc2d3;
        }
    }
}

struct s28 {
    int32_t f0;
    void** f4;
};

void fun_c4e8() {
    struct s28* rdi1;
    void* r15_2;
    void* r12_3;
    int32_t* rsi4;
    void** rdi5;
    uint64_t rsi6;
    int64_t rsi7;
    int64_t rbp8;
    void** v9;
    void* rbp10;
    void** v11;
    void* rbp12;
    void** rax13;
    int64_t rbp14;
    int64_t r8_15;
    int64_t rbp16;
    int64_t rbp17;
    void* rbp18;
    void** v19;
    void** v20;
    void** r9_21;
    int64_t rbp22;
    int64_t r8_23;
    int64_t rbp24;
    int64_t rbp25;

    rdi1 = reinterpret_cast<struct s28*>(reinterpret_cast<int64_t>(r15_2) + reinterpret_cast<int64_t>(r12_3));
    rdi1->f0 = *rsi4;
    rdi5 = reinterpret_cast<void**>(&rdi1->f4);
    rsi6 = rsi7 + 4;
    if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) == 1) {
        v9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp10) + 0xfffffffffffffc44);
    } else {
        if (*reinterpret_cast<int32_t*>(rbp8 - 0x3d8) == 2) {
            v11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp12) + 0xfffffffffffffc44);
            *reinterpret_cast<int32_t*>(&rax13) = *reinterpret_cast<int32_t*>(rbp14 - 0x3b4);
            *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
            v9 = rax13;
        } else {
            r8_15 = *reinterpret_cast<int64_t*>(rbp16 - 0x3e0);
            *reinterpret_cast<uint64_t*>(rbp17 - 0x418) = rsi6;
            fun_2530(rdi5, rsi6, 1, -1, r8_15, reinterpret_cast<int64_t>(rbp18) + 0xfffffffffffffc44, __return_address(), v19, v20);
            goto 0xc2fd;
        }
    }
    *reinterpret_cast<int32_t*>(&r9_21) = *reinterpret_cast<int32_t*>(rbp22 - 0x3b8);
    *reinterpret_cast<int32_t*>(&r9_21 + 4) = 0;
    r8_23 = *reinterpret_cast<int64_t*>(rbp24 - 0x3e0);
    *reinterpret_cast<uint64_t*>(rbp25 - 0x418) = rsi6;
    fun_2530(rdi5, rsi6, 1, -1, r8_23, r9_21, v9, v11, __return_address());
    goto 0xc2fd;
}

void fun_c550() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xc3d6;
}

void fun_c5a0() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) == 1) 
        goto 0xc580;
    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 2) 
        goto 0xc3df;
}

void fun_c650() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xc3d6;
    goto 0xc580;
}

void fun_c6e3() {
    signed char* r13_1;

    *r13_1 = 76;
    goto 0xc152;
}

void fun_c880() {
    int64_t* rax1;
    int64_t r12_2;

    *rax1 = r12_2;
    goto 0xc807;
}

struct s29 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s30 {
    signed char[8] pad8;
    int64_t f8;
};

struct s31 {
    signed char[16] pad16;
    int64_t f10;
};

struct s32 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_cea8() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s29* rcx3;
    struct s30* rcx4;
    int64_t r11_5;
    struct s31* rcx6;
    uint32_t* rcx7;
    struct s32* rax8;
    int64_t rsi9;
    int64_t r8_10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    if (rsi9 + 1 != r8_10) 
        goto 0xce90; else 
        goto "???";
}

struct s33 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s34 {
    signed char[8] pad8;
    int64_t f8;
};

struct s35 {
    signed char[16] pad16;
    int64_t f10;
};

struct s36 {
    signed char[16] pad16;
    int32_t f10;
};

void fun_cee0() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s33* rcx3;
    struct s34* rcx4;
    int64_t r11_5;
    struct s35* rcx6;
    uint32_t* rcx7;
    struct s36* rax8;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rax8->f10 = *r11_2;
    goto 0xcec6;
}

struct s37 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s38 {
    signed char[8] pad8;
    int64_t f8;
};

struct s39 {
    signed char[16] pad16;
    int64_t f10;
};

struct s40 {
    signed char[16] pad16;
    int16_t f10;
};

void fun_cf00() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s37* rcx3;
    struct s38* rcx4;
    int64_t r11_5;
    struct s39* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s40* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<int16_t*>(&edx8);
    goto 0xcec6;
}

struct s41 {
    signed char[8] pad8;
    int32_t* f8;
};

struct s42 {
    signed char[8] pad8;
    int64_t f8;
};

struct s43 {
    signed char[16] pad16;
    int64_t f10;
};

struct s44 {
    signed char[16] pad16;
    signed char f10;
};

void fun_cf20() {
    uint32_t* rcx1;
    int32_t* r11_2;
    struct s41* rcx3;
    struct s42* rcx4;
    int64_t r11_5;
    struct s43* rcx6;
    uint32_t* rcx7;
    int32_t edx8;
    struct s44* rax9;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 2);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int32_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    edx8 = *r11_2;
    rax9->f10 = *reinterpret_cast<signed char*>(&edx8);
    goto 0xcec6;
}

struct s45 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s46 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_cfa0() {
    struct s45* rcx1;
    struct s46* rcx2;

    rcx1->f8 = (reinterpret_cast<uint64_t>(rcx2->f8 + 15) & 0xfffffffffffffff0) + 16;
    __asm__("fld tword [rdx]");
    __asm__("fstp tword [rax+0x10]");
    goto 0xcec6;
}

struct s47 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s48 {
    signed char[8] pad8;
    int64_t f8;
};

struct s49 {
    signed char[16] pad16;
    int64_t f10;
};

struct s50 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_cff0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s47* rcx3;
    struct s48* rcx4;
    int64_t r11_5;
    struct s49* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r9_9;
    struct s50* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r9_9;
    }
    rax10->f10 = rdx8;
    goto 0xcec6;
}

void fun_d23c() {
}

void fun_d26b() {
    goto 0xd248;
}

void fun_d2c0() {
}

void fun_d4d0() {
    goto 0xd2c3;
}

struct s51 {
    signed char[8] pad8;
    void** f8;
};

struct s52 {
    signed char[8] pad8;
    int64_t f8;
};

struct s53 {
    signed char[8] pad8;
    void** f8;
};

struct s54 {
    signed char[8] pad8;
    void** f8;
};

void fun_d578() {
    int64_t r11_1;
    int64_t r11_2;
    int64_t r11_3;
    void** rbp4;
    struct s51* r14_5;
    void** rsi6;
    int64_t r11_7;
    int64_t r11_8;
    int64_t r11_9;
    void** r10_10;
    void** rax11;
    void** rcx12;
    int64_t v13;
    struct s52* r15_14;
    void** rax15;
    struct s53* r14_16;
    void** r10_17;
    int64_t r13_18;
    void** rax19;
    struct s54* r14_20;
    int64_t* r14_21;

    if (r11_1 < 0) 
        goto 0xdcb7;
    if (reinterpret_cast<uint64_t>(r11_2 + r11_3) > 0x2e8ba2e8ba2e8ba) 
        goto 0xdcb7;
    rbp4 = r14_5->f8;
    rsi6 = reinterpret_cast<void**>(r11_7 + (r11_8 + r11_9 * 4) * 2 << 4);
    if (r10_10 == rbp4) {
        rax11 = fun_2800(rsi6, rsi6);
        rcx12 = rax11;
        if (!rax11) {
            if (v13 == r15_14->f8) 
                goto 0xdad8; else 
                goto "???";
        }
    } else {
        rax15 = fun_2840(rbp4, rsi6);
        rcx12 = rax15;
        if (!rax15) 
            goto 0xdcb7;
        rbp4 = r14_16->f8;
        if (r10_17 == rbp4) 
            goto addr_de0a_9; else 
            goto addr_d5ee_10;
    }
    addr_d744_11:
    rax19 = fun_27e0(rcx12, rbp4, r13_18 + (r13_18 + r13_18 * 4) * 2 << 3);
    rcx12 = rax19;
    addr_d5ee_10:
    r14_20->f8 = rcx12;
    goto 0xd109;
    addr_de0a_9:
    r13_18 = *r14_21;
    goto addr_d744_11;
}

struct s55 {
    signed char[11] pad11;
    void** fb;
};

struct s56 {
    signed char[80] pad80;
    int64_t f50;
};

struct s57 {
    signed char[80] pad80;
    int64_t f50;
};

struct s58 {
    signed char[8] pad8;
    void** f8;
};

struct s59 {
    signed char[8] pad8;
    void** f8;
};

struct s60 {
    signed char[8] pad8;
    void** f8;
};

struct s61 {
    signed char[72] pad72;
    signed char f48;
};

struct s62 {
    signed char[8] pad8;
    int64_t f8;
};

void fun_d801() {
    void** ecx1;
    int32_t edx2;
    struct s55* ecx3;
    uint32_t edx4;
    int64_t r13_5;
    struct s56* r12_6;
    int64_t v7;
    uint64_t r13_8;
    uint64_t v9;
    struct s57* r12_10;
    int64_t r13_11;
    void** r8_12;
    struct s58* r15_13;
    uint64_t r9_14;
    uint64_t r9_15;
    int64_t r9_16;
    uint64_t r9_17;
    void** rsi18;
    void** v19;
    uint64_t rdx20;
    uint64_t* r15_21;
    void*** rax22;
    void*** rsi23;
    uint64_t* r15_24;
    void** rax25;
    uint64_t r11_26;
    uint64_t r11_27;
    void** rdi28;
    void** r8_29;
    uint64_t rdx30;
    uint64_t* r15_31;
    void** rdx32;
    void** rax33;
    struct s59* r15_34;
    void** rax35;
    uint64_t r11_36;
    void** v37;
    struct s60* r15_38;
    void*** r13_39;
    struct s61* r12_40;
    signed char bpl41;
    int64_t rax42;
    int64_t* r14_43;
    struct s62* r12_44;
    int64_t rbx45;
    uint64_t r13_46;
    uint64_t* r14_47;

    ecx1 = reinterpret_cast<void**>(12);
    if (edx2 <= 15) {
        ecx3 = reinterpret_cast<struct s55*>(0);
        *reinterpret_cast<unsigned char*>(&ecx3) = reinterpret_cast<uint1_t>(!!(edx4 & 4));
        ecx1 = reinterpret_cast<void**>(&ecx3->fb);
    }
    if (r13_5 == -1) {
        r12_6->f50 = v7;
        if (v7 == -1) 
            goto 0xd698;
        r13_8 = v9;
    } else {
        r12_10->f50 = r13_11;
    }
    r8_12 = r15_13->f8;
    if (r9_14 <= r13_8) {
        r9_15 = r9_16 + r9_17;
        if (r9_15 <= r13_8) {
            r9_15 = r13_8 + 1;
        }
        if (r9_15 >> 59) 
            goto 0xddec;
        rsi18 = reinterpret_cast<void**>(r9_15 << 5);
        if (v19 != r8_12) 
            goto addr_d95d_12;
    } else {
        addr_d504_13:
        rdx20 = *r15_21;
        rax22 = reinterpret_cast<void***>((rdx20 << 5) + reinterpret_cast<unsigned char>(r8_12));
        if (rdx20 <= r13_8) {
            do {
                ++rdx20;
                *rax22 = reinterpret_cast<void**>(0);
                rsi23 = rax22;
                rax22 = rax22 + 32;
            } while (rdx20 <= r13_8);
            *r15_24 = rdx20;
            *rsi23 = reinterpret_cast<void**>(0);
            goto addr_d53f_17;
        }
    }
    rax25 = fun_2800(rsi18, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_27;
    rdi28 = rax25;
    r8_29 = r8_12;
    if (!rax25) 
        goto 0xdaca;
    addr_da70_19:
    rdx30 = *r15_31;
    rdx32 = reinterpret_cast<void**>(rdx30 << 5);
    rax33 = fun_27e0(rdi28, r8_29, rdx32, rdi28, r8_29, rdx32);
    r11_26 = r11_26;
    ecx1 = ecx1;
    r8_12 = rax33;
    addr_d9a6_20:
    r15_34->f8 = r8_12;
    goto addr_d504_13;
    addr_d95d_12:
    rax35 = fun_2840(r8_12, rsi18);
    ecx1 = ecx1;
    r11_26 = r11_36;
    r8_12 = rax35;
    if (!rax35) 
        goto 0xdcb7;
    if (v37 != r15_38->f8) 
        goto addr_d9a6_20;
    rdi28 = r8_12;
    r8_29 = v37;
    goto addr_da70_19;
    addr_d53f_17:
    r13_39 = reinterpret_cast<void***>((r13_8 << 5) + reinterpret_cast<unsigned char>(r8_12));
    if (*r13_39) {
        if (*r13_39 != ecx1) {
            goto 0xd69c;
        }
    } else {
        *r13_39 = ecx1;
    }
    r12_40->f48 = bpl41;
    rax42 = *r14_43;
    r12_44->f8 = rbx45;
    r13_46 = reinterpret_cast<uint64_t>(rax42 + 1);
    *r14_47 = r13_46;
    if (r11_26 <= r13_46) 
        goto 0xd580;
    goto 0xd109;
}

void fun_d81f() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xd4e8;
    if (dl2 & 4) 
        goto 0xd4e8;
    if (edx3 > 7) 
        goto 0xd4e8;
    if (dl4 & 2) 
        goto 0xd4e8;
    goto 0xd4e8;
}

void fun_d868() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xd4e8;
    if (dl2 & 4) 
        goto 0xd4e8;
    if (edx3 > 7) 
        goto 0xd4e8;
    if (dl4 & 2) 
        goto 0xd4e8;
    goto 0xd4e8;
}

void fun_d8b0() {
    int32_t edx1;
    unsigned char dl2;
    int32_t edx3;
    unsigned char dl4;

    if (edx1 > 15) 
        goto 0xd4e8;
    if (dl2 & 4) 
        goto 0xd4e8;
    if (edx3 > 7) 
        goto 0xd4e8;
    if (dl4 & 2) 
        goto 0xd4e8;
    goto 0xd4e8;
}

void fun_d8f8() {
    goto 0xd4e8;
}

void fun_867e() {
    goto 0x82f2;
}

void fun_8854() {
    goto 0x880c;
}

void fun_891b() {
    goto 0x8448;
}

void fun_896d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x88f0;
    goto 0x851f;
}

void fun_899f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x88fb;
        goto 0x8320;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x879a;
        goto 0x853b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x9138;
    if (r10_8 > r15_9) 
        goto addr_8885_9;
    addr_888a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x9143;
    goto 0x8474;
    addr_8885_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_888a_10;
}

void fun_89d2() {
    goto 0x8507;
}

void fun_8da0() {
    goto 0x8507;
}

void fun_953f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x865c;
    } else {
        goto 0x87c0;
    }
}

void fun_a670() {
}

void fun_b3df() {
    if (__intrinsic()) 
        goto 0xb419; else 
        goto "???";
}

struct s63 {
    signed char[1] pad1;
    signed char f1;
};

void fun_c140() {
    signed char* r13_1;
    struct s63* r13_2;

    *r13_1 = 0x6c;
    r13_2->f1 = 0x6c;
}

void fun_c88e() {
    int32_t* rax1;
    int32_t r12d2;

    *rax1 = r12d2;
    goto 0xc807;
}

struct s64 {
    signed char[8] pad8;
    int64_t* f8;
};

struct s65 {
    signed char[8] pad8;
    int64_t f8;
};

struct s66 {
    signed char[16] pad16;
    int64_t f10;
};

struct s67 {
    signed char[16] pad16;
    int64_t f10;
};

void fun_cfc0() {
    uint32_t* rcx1;
    int64_t* r11_2;
    struct s64* rcx3;
    struct s65* rcx4;
    int64_t r11_5;
    struct s66* rcx6;
    uint32_t* rcx7;
    int64_t rdx8;
    int64_t r10_9;
    struct s67* rax10;

    if (*rcx1 > 47) {
        r11_2 = rcx3->f8;
        rcx4->f8 = reinterpret_cast<int64_t>(r11_2 + 1);
    } else {
        *reinterpret_cast<uint32_t*>(&r11_5) = *rcx1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r11_5) + 4) = 0;
        r11_2 = reinterpret_cast<int64_t*>(r11_5 + rcx6->f10);
        *rcx7 = *rcx1 + 8;
    }
    rdx8 = *r11_2;
    if (!rdx8) {
        rdx8 = r10_9;
    }
    rax10->f10 = rdx8;
    goto 0xcec6;
}

void fun_d275() {
    goto 0xd248;
}

void fun_dbc4() {
    goto 0xd4e8;
}

void fun_d4d8() {
}

void fun_d908() {
    goto 0xd4e8;
}

void fun_89dc() {
    goto 0x8977;
}

void fun_8daa() {
    goto 0x88cd;
}

void fun_a6d0() {
    fun_2660();
    goto fun_2930;
}

void fun_c620() {
    int64_t rbp1;

    if (*reinterpret_cast<int32_t*>(rbp1 - 0x3d8) != 1) 
        goto 0xc3d6;
    goto 0xc580;
}

void fun_c89c() {
    int16_t* rax1;
    int16_t r12w2;

    *rax1 = r12w2;
    goto 0xc807;
}

struct s68 {
    int32_t f0;
    int32_t f4;
};

struct s69 {
    int32_t f0;
    int32_t f4;
};

struct s70 {
    signed char[4] pad4;
    uint32_t f4;
};

struct s71 {
    signed char[8] pad8;
    int64_t f8;
};

struct s72 {
    signed char[8] pad8;
    int64_t f8;
};

struct s73 {
    signed char[4] pad4;
    uint32_t f4;
};

void fun_cf70(struct s68* rdi, struct s69* rsi) {
    struct s70* rcx3;
    struct s71* rcx4;
    struct s72* rcx5;
    struct s73* rcx6;

    if (rcx3->f4 > 0xaf) {
        rcx4->f8 = rcx5->f8 + 8;
    } else {
        rcx6->f4 = rcx3->f4 + 16;
    }
    rdi->f0 = rsi->f0;
    rdi->f4 = rsi->f4;
    goto 0xcec6;
}

void fun_d27f() {
    goto 0xd248;
}

void fun_dbce() {
    goto 0xd4e8;
}

void fun_86ad() {
    goto 0x82f2;
}

void fun_89e8() {
    goto 0x8977;
}

void fun_8db7() {
    goto 0x891e;
}

void fun_a710() {
    fun_2660();
    goto fun_2930;
}

void fun_c8ab() {
    signed char* rax1;
    signed char r12b2;

    *rax1 = r12b2;
    goto 0xc807;
}

void fun_d289() {
    goto 0xd248;
}

void fun_86da() {
    goto 0x82f2;
}

void fun_89f4() {
    goto 0x88f0;
}

void fun_a750() {
    fun_2660();
    goto fun_2930;
}

void fun_d293() {
    goto 0xd248;
}

void fun_86fc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x9090;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x85c1;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x85c1;
    }
    if (v11) 
        goto 0x93f3;
    if (r10_12 > r15_13) 
        goto addr_9443_8;
    addr_9448_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x9181;
    addr_9443_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_9448_9;
}

void fun_a7a0() {
    void** rax1;
    void** rbp2;
    int64_t v3;

    rax1 = fun_2660();
    fun_2930(rbp2, 1, rax1, rbp2, 1, rax1);
    goto v3;
}

void fun_a7f8() {
    fun_2660();
    goto 0xa7c9;
}

void fun_a830() {
    void** rax1;
    void** rbp2;
    int64_t v3;

    rax1 = fun_2660();
    fun_2930(rbp2, 1, rax1, rbp2, 1, rax1);
    goto v3;
}

void fun_a8a8() {
    fun_2660();
    goto 0xa86b;
}
