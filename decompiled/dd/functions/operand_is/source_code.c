operand_is (char const *operand, char const *name)
{
  return operand_matches (operand, name, '=');
}