advance_input_offset (intmax_t offset)
{
  if (0 <= input_offset && INT_ADD_WRAPV (input_offset, offset, &input_offset))
    input_offset = -1;
}