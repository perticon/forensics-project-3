alloc_obuf (void)
{
  if (obuf)
    return;

  if (conversions_mask & C_TWOBUFS)
    {
      obuf = alignalloc (page_size, output_blocksize);
      if (!obuf)
        {
          char hbuf[LONGEST_HUMAN_READABLE + 1];
          die (EXIT_FAILURE, 0,
               _("memory exhausted by output buffer of size %td"
                 " bytes (%s)"),
               output_blocksize,
               human_readable (output_blocksize, hbuf,
                               human_opts | human_base_1024, 1, 1));
        }
    }
  else
    {
      alloc_ibuf ();
      obuf = ibuf;
    }
}