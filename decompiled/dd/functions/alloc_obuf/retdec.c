int64_t alloc_obuf(void) {
    // 0x4ea0
    if (obuf != 0) {
        // 0x4eaa
        int64_t result; // 0x4ea0
        return result;
    }
    // 0x4eab
    if ((g32 & 8) != 0) {
        // 0x4ecd
        return alloc_obuf_part_0();
    }
    // 0x4eb4
    if (ibuf != 0) {
        // 0x4ebe
        obuf = ibuf;
        return ibuf;
    }
    // 0x4ed2
    alloc_ibuf_part_0();
    obuf = ibuf;
    return ibuf;
}