void** alloc_obuf(void** rdi, ...) {
    int1_t zf2;
    int1_t zf3;
    void** rbp4;
    void** rdi5;
    void* rax6;
    void** rdx7;
    void** rcx8;
    void** r8_9;
    void** rax10;
    void** r9_11;
    void** rax12;
    void** r12_13;
    void** rax14;
    void** r9_15;
    void** rax16;
    void** rax17;
    int1_t zf18;
    void** rax19;
    void** rax20;

    while (zf2 = obuf == 0, zf2) {
        zf3 = (*reinterpret_cast<unsigned char*>(&conversions_mask + 1) & 8) == 0;
        if (zf3) 
            goto addr_4eb4_3;
        rbp4 = output_blocksize;
        rdi5 = page_size;
        rax6 = g28;
        rax10 = fun_2940(rdi5, rbp4, rdx7, rcx8, r8_9);
        obuf = rax10;
        if (!rax10) {
            rax12 = human_readable(rbp4, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 0x2a8 - 8 + 8, 0x1f1, 1, 1, r9_11);
            r12_13 = output_blocksize;
            rax14 = fun_2660();
            r8_9 = rax12;
            rcx8 = r12_13;
            rdx7 = rax14;
            nl_error(1, 0, rdx7, rcx8, r8_9, r9_15);
        } else {
            rax16 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax6) - reinterpret_cast<uint64_t>(g28));
            if (!rax16) 
                goto addr_4e32_8;
        }
        rax17 = fun_2690();
    }
    return rax17;
    addr_4eb4_3:
    zf18 = ibuf == 0;
    if (zf18) {
        alloc_ibuf_part_0(rdi, rdi);
        rax19 = ibuf;
        obuf = rax19;
        return rax19;
    } else {
        rax20 = ibuf;
        obuf = rax20;
        return rax20;
    }
    addr_4e32_8:
    return rax16;
}