void alloc_obuf(void)

{
  if (obuf != 0) {
    return;
  }
  if ((conversions_mask._1_1_ & 8) == 0) {
    if (ibuf != 0) {
      obuf = ibuf;
      return;
    }
    alloc_ibuf_part_0();
    obuf = ibuf;
    return;
  }
  alloc_obuf_part_0();
  return;
}