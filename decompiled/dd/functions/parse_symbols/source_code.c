parse_symbols (char const *str, struct symbol_value const *table,
               bool exclusive, char const *error_msgid)
{
  int value = 0;

  while (true)
    {
      char const *strcomma = strchr (str, ',');
      struct symbol_value const *entry;

      for (entry = table;
           ! (operand_matches (str, entry->symbol, ',') && entry->value);
           entry++)
        {
          if (! entry->symbol[0])
            {
              idx_t slen = strcomma ? strcomma - str : strlen (str);
              error (0, 0, "%s: %s", _(error_msgid),
                     quotearg_n_style_mem (0, locale_quoting_style, str, slen));
              usage (EXIT_FAILURE);
            }
        }

      if (exclusive)
        value = entry->value;
      else
        value |= entry->value;
      if (!strcomma)
        break;
      str = strcomma + 1;
    }

  return value;
}