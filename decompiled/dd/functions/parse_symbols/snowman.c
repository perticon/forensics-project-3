uint32_t parse_symbols(void** rdi, struct s0* rsi, int32_t edx, void** rcx) {
    void** r14_5;
    int32_t r12d6;
    struct s0* rbp7;
    uint32_t ebx8;
    struct s1* rax9;
    struct s0* rdi10;
    struct s1* r8_11;
    void* rdx12;
    uint32_t ecx13;
    uint32_t esi14;
    uint32_t eax15;
    void** rax16;
    void** rax17;
    void** r9_18;

    r14_5 = rdi;
    r12d6 = edx;
    rbp7 = rsi;
    ebx8 = 0;
    while (1) {
        rax9 = fun_26d0(r14_5, r14_5);
        rdi10 = rbp7;
        r8_11 = rax9;
        while (1) {
            *reinterpret_cast<int32_t*>(&rdx12) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx12) + 4) = 0;
            do {
                ecx13 = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi10) + reinterpret_cast<uint64_t>(rdx12));
                esi14 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r14_5) + reinterpret_cast<uint64_t>(rdx12));
                if (!*reinterpret_cast<signed char*>(&ecx13)) 
                    break;
                rdx12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx12) + 1);
            } while (*reinterpret_cast<signed char*>(&ecx13) == *reinterpret_cast<signed char*>(&esi14));
            goto addr_68d0_6;
            if (*reinterpret_cast<signed char*>(&esi14) == 44) 
                goto addr_68c9_8;
            if (*reinterpret_cast<signed char*>(&esi14)) 
                goto addr_68d0_6;
            addr_68c9_8:
            eax15 = rdi10->fc;
            if (eax15) 
                break;
            addr_68d0_6:
            if (!rdi10->f0) 
                goto addr_68f9_10;
            ++rdi10;
        }
        if (!*reinterpret_cast<signed char*>(&r12d6)) {
            eax15 = ebx8 | eax15;
        }
        if (!r8_11) 
            goto addr_68f0_15;
        r14_5 = reinterpret_cast<void**>(&r8_11->f1);
        ebx8 = eax15;
    }
    addr_68f9_10:
    if (!r8_11) {
        while (1) {
            fun_2680(r14_5, r14_5);
            addr_6904_18:
            rax16 = quotearg_n_style_mem();
            rax17 = fun_2660();
            nl_error(0, 0, "%s: %s", rax17, rax16, r9_18);
            usage(1);
        }
    } else {
        goto addr_6904_18;
    }
    addr_68f0_15:
    return eax15;
}