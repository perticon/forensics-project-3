int32_t parse_symbols(char * str, int32_t * table, bool exclusive, char * error_msgid) {
    int64_t v1 = (int64_t)str; // 0x6884
    int64_t v2 = 0; // 0x6884
    int64_t v3 = function_26d0(); // 0x688e
    int64_t v4 = (int64_t)table; // 0x6899
    int64_t v5; // 0x6870
    int64_t v6; // 0x6870
    int64_t v7; // 0x6870
    char v8; // 0x68b1
    char v9; // 0x68b5
    uint32_t v10; // 0x68c9
    while (true) {
        // 0x68a0
        v5 = v4;
        v6 = 0;
        v7 = v6;
        v8 = *(char *)(v7 + v5);
        v9 = *(char *)(v7 + v1);
        while (v8 != 0) {
            // 0x68a8
            v6 = v7 + 1;
            if (v8 != v9) {
                goto lab_0x68d0;
            }
            v7 = v6;
            v8 = *(char *)(v7 + v5);
            v9 = *(char *)(v7 + v1);
        }
        if (v9 != 44 != v9 != 0) {
            // 0x68c9
            v10 = *(int32_t *)(v5 + 12);
            if (v10 != 0) {
                // break -> 0x68db
                break;
            }
        }
        // 0x68d0
        v4 = v5 + 16;
        if (*(char *)v5 == 0) {
            // break (via goto) -> 0x68f9
            goto lab_0x68f9;
        }
    }
    // 0x68db
    v2 = (!exclusive ? v2 : 0) | (int64_t)v10;
    while (v3 != 0) {
        // 0x68e8
        v1 = v3 + 1;
        v3 = function_26d0();
        v4 = (int64_t)table;
        while (true) {
            // 0x68a0
            v5 = v4;
            v6 = 0;
            v7 = v6;
            v8 = *(char *)(v7 + v5);
            v9 = *(char *)(v7 + v1);
            while (v8 != 0) {
                // 0x68a8
                v6 = v7 + 1;
                if (v8 != v9) {
                    goto lab_0x68d0;
                }
                v7 = v6;
                v8 = *(char *)(v7 + v5);
                v9 = *(char *)(v7 + v1);
            }
            if (v9 != 44 != v9 != 0) {
                // 0x68c9
                v10 = *(int32_t *)(v5 + 12);
                if (v10 != 0) {
                    // break -> 0x68db
                    break;
                }
            }
            // 0x68d0
            v4 = v5 + 16;
            if (*(char *)v5 == 0) {
                // break (via goto) -> 0x68f9
                goto lab_0x68f9;
            }
        }
        // 0x68db
        v2 = (!exclusive ? v2 : 0) | (int64_t)v10;
    }
    // 0x68f0
    return (!exclusive ? v2 : 0) | (int64_t)v10;
  lab_0x68f9:
    if (v3 == 0) {
        // 0x6947
        function_2680();
        goto lab_0x6904;
    } else {
        goto lab_0x6904;
    }
  lab_0x6904:;
    int64_t v11 = quotearg_n_style_mem(); // 0x690e
    nl_error(0, 0, "%s: %s", (char *)function_2660(), (char *)v11);
    usage(1);
    // 0x6947
    function_2680();
    goto lab_0x6904;
}