signed char invalidate_cache(uint32_t edi, void** rsi, ...) {
    void** rdx3;
    uint32_t r13d4;
    void* r12_5;
    void* r15_6;
    uint32_t r14d7;
    void** rsi8;
    void** rax9;
    void* r12_10;
    void* rcx11;
    void* rax12;
    void* rax13;
    void* rax14;
    uint32_t eax15;
    int64_t rdi16;
    int32_t eax17;
    void** rax18;

    rdx3 = reinterpret_cast<void**>(0x14140);
    r13d4 = o_nocache_eof;
    if (!edi) {
        r13d4 = i_nocache_eof;
        rdx3 = reinterpret_cast<void**>(0x14148);
    }
    r12_5 = *rdx3;
    if (!rsi) {
        if (r12_5 || *reinterpret_cast<signed char*>(&r13d4)) {
            *reinterpret_cast<int32_t*>(&r15_6) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_6) + 4) = 0;
            if (!edi) {
                addr_4c30_6:
                r14d7 = input_seekable;
                rsi8 = input_offset;
                if (!*reinterpret_cast<unsigned char*>(&r14d7)) {
                    rax9 = fun_2570();
                    *reinterpret_cast<void***>(rax9) = reinterpret_cast<void**>(29);
                    goto addr_4b9f_8;
                }
            } else {
                rsi8 = output_offset_2;
                r14d7 = 0;
                if (rsi8 == 0xffffffffffffffff) 
                    goto addr_4b9f_8; else 
                    goto addr_4bd9_10;
            }
        } else {
            addr_4b99_11:
            r14d7 = 1;
            goto addr_4b9f_8;
        }
    } else {
        r12_10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_5) + reinterpret_cast<unsigned char>(rsi));
        if (__intrinsic()) {
            *rdx3 = reinterpret_cast<void*>(0x1ffff);
            r12_5 = reinterpret_cast<void*>(0x7ffffffffffe0000);
        } else {
            rcx11 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_10) >> 63) >> 47);
            rax12 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_10) + reinterpret_cast<uint64_t>(rcx11));
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax12) & 0x1ffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            rax14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax13) - reinterpret_cast<uint64_t>(rcx11));
            *rdx3 = rax14;
            if (reinterpret_cast<int64_t>(r12_10) <= reinterpret_cast<int64_t>(rax14)) 
                goto addr_4b99_11;
            r12_5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_10) - reinterpret_cast<uint64_t>(rax14));
            if (!r12_5) 
                goto addr_4b99_11;
        }
        if (!edi) 
            goto addr_4c30_6;
        rsi8 = output_offset_2;
        r15_6 = o_pending_0;
        r14d7 = 0;
        if (reinterpret_cast<int1_t>(rsi8 == 0xffffffffffffffff)) {
            goto addr_4b9f_8;
        }
    }
    addr_4bf5_19:
    r14d7 = 0;
    if (reinterpret_cast<signed char>(rsi8) < reinterpret_cast<signed char>(0)) {
        addr_4b9f_8:
        eax15 = r14d7;
        return *reinterpret_cast<signed char*>(&eax15);
    } else {
        addr_4bfd_20:
        if (rsi || !r12_5) {
            if (r12_5) {
                addr_4c12_22:
                *reinterpret_cast<uint32_t*>(&rdi16) = edi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_2750(rdi16);
                *reinterpret_cast<unsigned char*>(&r14d7) = reinterpret_cast<uint1_t>(eax17 != -1);
                goto addr_4b9f_8;
            } else {
                addr_4c60_23:
                goto addr_4c12_22;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&r13d4)) 
                goto addr_4c60_23;
            goto addr_4c12_22;
        }
    }
    addr_4bd9_10:
    if (reinterpret_cast<signed char>(rsi8) < reinterpret_cast<signed char>(0)) {
        rax18 = fun_2710();
        output_offset_2 = rax18;
        rsi8 = rax18;
        goto addr_4bf5_19;
    } else {
        if (!rsi) 
            goto addr_4bfd_20;
        rsi8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi8) + (reinterpret_cast<int64_t>(r15_6) + reinterpret_cast<uint64_t>(r12_5)));
        output_offset_2 = rsi8;
        goto addr_4bf5_19;
    }
}