bool invalidate_cache(int32_t fd, int64_t len) {
    int64_t * v1 = fd != 0 ? &g19 : &g20; // 0x4b65
    int64_t v2 = *v1; // 0x4b65
    int64_t v3; // 0x4b30
    int64_t v4; // 0x4b30
    if (len == 0) {
        char v5 = fd != 0 ? *(char *)&o_nocache_eof : *(char *)&i_nocache_eof;
        if (v2 == 0 == v5 == 0) {
            // 0x4b9f
            return true;
        }
        if (fd == 0) {
            goto lab_0x4c30;
        } else {
            // 0x4bc9
            v3 = v2;
            v4 = 0;
            if (g8 == -1) {
                // 0x4b9f
                return false;
            }
            goto lab_0x4bd9;
        }
    } else {
        int64_t v6 = v2 + len; // 0x4b6d
        int64_t v7; // 0x4b30
        if (((v6 ^ v2) & (v6 ^ len)) < 0) {
            // 0x4ca8
            *v1 = 0x1ffff;
            v7 = 0x7ffffffffffe0000;
        } else {
            uint64_t v8 = (v6 >> 63) / 0x800000000000; // 0x4b7d
            uint64_t v9 = (v8 + v6) % 0x20000 - v8; // 0x4b8a
            *v1 = v9;
            if (v6 <= v9) {
                // 0x4b9f
                return true;
            }
            int64_t v10 = v6 - v9; // 0x4c74
            v7 = v10;
            if (v10 == 0) {
                // 0x4b9f
                return true;
            }
        }
        if (fd == 0) {
            goto lab_0x4c30;
        } else {
            // 0x4c88
            v3 = v7;
            v4 = g19;
            if (g8 == -1) {
                // 0x4b9f
                return false;
            }
            goto lab_0x4bd9;
        }
    }
  lab_0x4c30:;
    unsigned char v11 = *(char *)&input_seekable; // 0x4c30
    int64_t v12 = input_offset; // 0x4c42
    if (v11 == 0) {
        // 0x4c44
        *(int32_t *)function_2570() = 29;
        // 0x4b9f
        return (int64_t)v11 % 2 != 0;
    }
    goto lab_0x4bf5;
  lab_0x4bf5:
    // 0x4bf5
    if (v12 < 0) {
        // 0x4b9f
        return false;
    }
    // 0x4b9f
    return (int64_t)((int32_t)function_2750() != -1) % 2 != 0;
  lab_0x4bd9:;
    int64_t v13 = g8;
    if (v13 < 0) {
        int64_t v14 = function_2710(); // 0x4ccc
        g8 = v14;
        v12 = v14;
    } else {
        if (len == 0) {
            // 0x4b9f
            return (int64_t)((int32_t)function_2750() != -1) % 2 != 0;
        }
        int64_t v15 = v3 + v13 + v4; // 0x4beb
        g8 = v15;
        v12 = v15;
    }
    goto lab_0x4bf5;
}