bool invalidate_cache(int param_1,long param_2)

{
  int iVar1;
  long lVar2;
  int *piVar3;
  long *plVar4;
  long lVar5;
  long lVar6;
  char cVar7;
  
  plVar4 = &o_pending_0;
  cVar7 = o_nocache_eof;
  if (param_1 == 0) {
    plVar4 = &i_pending_1;
    cVar7 = i_nocache_eof;
  }
  lVar2 = *plVar4;
  if (param_2 == 0) {
    if ((lVar2 == 0) && (cVar7 == '\0')) {
      return true;
    }
    lVar6 = 0;
    if (param_1 != 0) {
      if (output_offset_2 == -1) {
        return false;
      }
      goto LAB_00104bd9;
    }
LAB_00104c30:
    lVar5 = input_offset;
    if (input_seekable == '\0') {
      piVar3 = __errno_location();
      *piVar3 = 0x1d;
      return false;
    }
LAB_00104bf5:
    if (lVar5 < 0) {
      return false;
    }
  }
  else {
    lVar6 = lVar2 + param_2;
    if (SCARRY8(lVar2,param_2)) {
      *plVar4 = 0x1ffff;
      lVar2 = 0x7ffffffffffe0000;
    }
    else {
      lVar2 = (ulong)((int)lVar6 + ((uint)(lVar6 >> 0x5f) >> 0xf) & 0x1ffff) -
              ((ulong)(lVar6 >> 0x3f) >> 0x2f);
      *plVar4 = lVar2;
      if (lVar6 <= lVar2) {
        return true;
      }
      lVar2 = lVar6 - lVar2;
      if (lVar2 == 0) {
        return true;
      }
    }
    lVar6 = i_pending_1;
    if (param_1 == 0) goto LAB_00104c30;
    lVar6 = o_pending_0;
    if (output_offset_2 == -1) {
      return false;
    }
LAB_00104bd9:
    if (output_offset_2 < 0) {
      output_offset_2 = lseek(1,0,1);
      lVar5 = output_offset_2;
      goto LAB_00104bf5;
    }
    lVar5 = output_offset_2;
    if (param_2 != 0) {
      output_offset_2 = output_offset_2 + lVar6 + lVar2;
      lVar5 = output_offset_2;
      goto LAB_00104bf5;
    }
  }
  lVar5 = lVar5 - lVar2;
  if ((param_2 == 0) && (lVar2 != 0)) {
    if (cVar7 == '\0') {
      lVar5 = lVar5 - lVar6;
      goto LAB_00104c12;
    }
  }
  else {
    lVar5 = lVar5 - lVar6;
    if (lVar2 != 0) goto LAB_00104c12;
  }
  lVar2 = 0;
  lVar5 = lVar5 - lVar5 % page_size;
LAB_00104c12:
  iVar1 = posix_fadvise(param_1,lVar5,lVar2,4);
  return iVar1 != -1;
}