undefined4 synchronize_output(void)

{
  uint uVar1;
  uint uVar2;
  uint uVar3;
  int iVar4;
  int *piVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  undefined4 uVar8;
  
  uVar2 = conversions_mask;
  uVar3 = conversions_mask & 0xffff3fff;
  uVar1 = conversions_mask & 0x4000;
  conversions_mask = uVar3;
  if (uVar1 == 0) {
LAB_001060c3:
    if ((uVar2 & 0x8000) == 0) {
      return 0;
    }
  }
  else {
    do {
      process_signals();
      iVar4 = fdatasync(1);
      if (-1 < iVar4) {
        if (iVar4 == 0) goto LAB_001060c3;
        piVar5 = __errno_location();
        iVar4 = *piVar5;
        break;
      }
      piVar5 = __errno_location();
      iVar4 = *piVar5;
    } while (iVar4 == 4);
    if ((iVar4 - 0x16U & 0xffffffef) != 0) {
      uVar6 = quotearg_style(4,output_file);
      uVar7 = dcgettext(0,"fdatasync failed for %s",5);
      uVar8 = 1;
      nl_error(0,*piVar5,uVar7,uVar6);
      goto LAB_00106175;
    }
  }
  uVar8 = 0;
LAB_00106175:
  do {
    process_signals();
    iVar4 = fsync(1);
    if (-1 < iVar4) {
      if (iVar4 == 0) {
        return uVar8;
      }
      piVar5 = __errno_location();
      break;
    }
    piVar5 = __errno_location();
  } while (*piVar5 == 4);
  uVar6 = quotearg_style(4,output_file);
  uVar7 = dcgettext(0,"fsync failed for %s",5);
  nl_error(0,*piVar5,uVar7,uVar6);
  return 1;
}