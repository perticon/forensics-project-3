synchronize_output (void)
{
  int exit_status = 0;
  int mask = conversions_mask;
  conversions_mask &= ~ (C_FDATASYNC | C_FSYNC);

  if ((mask & C_FDATASYNC) && ifdatasync (STDOUT_FILENO) != 0)
    {
      if (errno != ENOSYS && errno != EINVAL)
        {
          error (0, errno, _("fdatasync failed for %s"), quoteaf (output_file));
          exit_status = EXIT_FAILURE;
        }
      mask |= C_FSYNC;
    }

  if ((mask & C_FSYNC) && ifsync (STDOUT_FILENO) != 0)
    {
      error (0, errno, _("fsync failed for %s"), quoteaf (output_file));
      return EXIT_FAILURE;
    }

  return exit_status;
}