uint32_t synchronize_output(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    void** r12d7;
    void** eax8;
    int32_t eax9;
    void** rax10;
    void** rbx11;
    void** eax12;
    uint32_t r12d13;
    void** rax14;
    void** rsi15;
    void** rax16;
    void** rax17;
    int32_t eax18;
    void** rax19;
    void** rbx20;
    void** rax21;
    void** rsi22;
    void** rax23;
    void** rax24;
    void** esi25;

    r12d7 = conversions_mask;
    eax8 = r12d7;
    *reinterpret_cast<unsigned char*>(&eax8 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax8 + 1) & 63);
    conversions_mask = eax8;
    if (!(reinterpret_cast<unsigned char>(r12d7) & 0x4000)) 
        goto addr_60c3_2;
    do {
        process_signals(rdi, rsi);
        *reinterpret_cast<uint32_t*>(&rdi) = 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        eax9 = fun_2580(1);
        if (eax9 >= 0) 
            break;
        rax10 = fun_2570();
        rbx11 = rax10;
        eax12 = *reinterpret_cast<void***>(rax10);
    } while (reinterpret_cast<int1_t>(eax12 == 4));
    goto addr_6110_5;
    if (!eax9) {
        addr_60c3_2:
        r12d13 = reinterpret_cast<unsigned char>(r12d7) & 0x8000;
        if (r12d13) 
            goto addr_6160_7; else 
            goto addr_60d0_8;
    } else {
        rax14 = fun_2570();
        rbx11 = rax14;
        eax12 = *reinterpret_cast<void***>(rax14);
    }
    addr_6110_5:
    if (!(reinterpret_cast<uint32_t>(eax12 - 22) & 0xffffffef)) {
        addr_6160_7:
        r12d13 = 0;
    } else {
        rsi15 = output_file;
        rax16 = quotearg_style(4, rsi15);
        rax17 = fun_2660();
        rsi = *reinterpret_cast<void***>(rbx11);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rcx = rax16;
        *reinterpret_cast<uint32_t*>(&rdi) = 0;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        rdx = rax17;
        r12d13 = 1;
        nl_error(0, rsi, rdx, rcx, r8, r9);
    }
    do {
        process_signals(rdi, rsi, rdi, rsi);
        *reinterpret_cast<uint32_t*>(&rdi) = 1;
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        eax18 = fun_2890(1, rsi, rdx, rcx);
        if (eax18 >= 0) 
            break;
        rax19 = fun_2570();
        rbx20 = rax19;
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax19) == 4));
    goto addr_61a0_13;
    if (!eax18) {
        addr_60d0_8:
        return r12d13;
    } else {
        rax21 = fun_2570();
        rbx20 = rax21;
    }
    addr_61a0_13:
    rsi22 = output_file;
    rax23 = quotearg_style(4, rsi22, 4, rsi22);
    rax24 = fun_2660();
    esi25 = *reinterpret_cast<void***>(rbx20);
    r12d13 = 1;
    nl_error(0, esi25, rax24, rax23, r8, r9);
    goto addr_60d0_8;
}