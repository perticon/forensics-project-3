int32_t synchronize_output(void) {
    int32_t v1 = conversions_mask; // 0x60a7
    conversions_mask = v1 & -0xc001;
    int32_t result2; // 0x60a0
    if ((v1 & 0x4000) != 0) {
        process_signals();
        uint32_t v2 = (int32_t)function_2580();
        int32_t v3; // 0x60a0
        int64_t v4; // 0x60a0
        while (v2 < 0) {
            int64_t v5 = function_2570(); // 0x60e0
            int32_t v6 = *(int32_t *)v5; // 0x60e8
            v3 = v6;
            v4 = v5;
            if (v6 != 4) {
                goto lab_0x6110;
            }
            process_signals();
            v2 = (int32_t)function_2580();
        }
        if (v2 == 0) {
            goto lab_0x60c3;
        } else {
            int64_t v7 = function_2570(); // 0x6104
            v3 = *(int32_t *)v7;
            v4 = v7;
          lab_0x6110:
            // 0x6110
            result2 = 0;
            if ((v3 - 22 & -17) != 0) {
                // 0x6118
                quotearg_style();
                int64_t v8 = function_2660(); // 0x613a
                nl_error(0, *(int32_t *)v4, (char *)v8);
                result2 = 1;
            }
            goto lab_0x6175;
        }
    } else {
        goto lab_0x60c3;
    }
  lab_0x60c3:;
    int32_t result = v1 & 0x8000; // 0x60c3
    result2 = 0;
    if (result == 0) {
        // 0x60d0
        return result;
    }
    goto lab_0x6175;
  lab_0x6175:;
    // 0x6175
    int32_t * v9; // 0x60a0
    uint32_t v10; // 0x60a0
    while (true) {
        // 0x6175
        process_signals();
        v10 = (int32_t)function_2890();
        if (v10 >= 0) {
            // break -> 0x6188
            break;
        }
        int32_t * v11 = (int32_t *)function_2570();
        v9 = v11;
        if (*v11 != 4) {
            goto lab_0x61a0;
        }
    }
    // 0x6188
    if (v10 == 0) {
        // 0x60d0
        return result2;
    }
    // 0x618e
    v9 = (int32_t *)function_2570();
  lab_0x61a0:
    // 0x61a0
    quotearg_style();
    int64_t v12 = function_2660(); // 0x61c2
    nl_error(0, *v9, (char *)v12);
    // 0x60d0
    return 1;
}