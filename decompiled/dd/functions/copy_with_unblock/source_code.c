copy_with_unblock (char const *buf, idx_t nread)
{
  static idx_t pending_spaces = 0;

  for (idx_t i = 0; i < nread; i++)
    {
      char c = buf[i];

      if (col++ >= conversion_blocksize)
        {
          col = pending_spaces = 0; /* Wipe out any pending spaces.  */
          i--;			/* Push the char back; get it later. */
          output_char (newline_character);
        }
      else if (c == space_character)
        pending_spaces++;
      else
        {
          /* 'c' is the character after a run of spaces that were not
             at the end of the conversion buffer.  Output them.  */
          while (pending_spaces)
            {
              output_char (space_character);
              --pending_spaces;
            }
          output_char (c);
        }
    }
}