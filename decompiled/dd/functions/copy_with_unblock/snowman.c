void copy_with_unblock(void** rdi, void** rsi, void** rdx, ...) {
    void** r12_4;
    void** rbp5;
    void** rbx6;
    int64_t rax7;
    int1_t less8;
    uint32_t r13d9;
    void** rdx10;
    void** rcx11;
    uint32_t esi12;
    void** rax13;
    int1_t less14;
    uint32_t ecx15;
    int64_t rdx16;
    void** rsi17;
    void** rax18;
    void** rdx19;
    int1_t less20;
    int1_t zf21;
    void** rax22;
    int1_t less23;

    if (reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rsi) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rsi == 0)) {
        return;
    } else {
        *reinterpret_cast<int32_t*>(&r12_4) = 0;
        *reinterpret_cast<int32_t*>(&r12_4 + 4) = 0;
        rbp5 = rdi;
        rbx6 = rsi;
        while (1) {
            rax7 = col;
            less8 = rax7 < conversion_blocksize;
            r13d9 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rbp5) + reinterpret_cast<unsigned char>(r12_4));
            col = rax7 + 1;
            if (!less8) {
                rdx10 = oc;
                rcx11 = obuf;
                pending_spaces_3 = 0;
                esi12 = newline_character;
                col = 0;
                rax13 = rdx10 + 1;
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx11) + reinterpret_cast<unsigned char>(rdx10)) = *reinterpret_cast<signed char*>(&esi12);
                less14 = reinterpret_cast<signed char>(rax13) < reinterpret_cast<signed char>(output_blocksize);
                oc = rax13;
                if (!less14) {
                    addr_64a0_6:
                    write_output(rdi);
                    goto addr_6423_7;
                } else {
                    addr_6423_7:
                    if (reinterpret_cast<signed char>(rbx6) <= reinterpret_cast<signed char>(r12_4)) 
                        break;
                }
            } else {
                ecx15 = space_character;
                rdx16 = pending_spaces_3;
                ++r12_4;
                if (*reinterpret_cast<signed char*>(&ecx15) == *reinterpret_cast<signed char*>(&r13d9)) {
                    pending_spaces_3 = rdx16 + 1;
                    if (reinterpret_cast<signed char>(rbx6) > reinterpret_cast<signed char>(r12_4)) 
                        continue; else 
                        break;
                }
                rsi17 = obuf;
                rax18 = oc;
                if (rdx16) {
                    while (1) {
                        rdx19 = rax18 + 1;
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi17) + reinterpret_cast<unsigned char>(rax18)) = *reinterpret_cast<signed char*>(&ecx15);
                        less20 = reinterpret_cast<signed char>(rdx19) < reinterpret_cast<signed char>(output_blocksize);
                        oc = rdx19;
                        if (!less20) {
                            write_output(rdi);
                            rsi17 = obuf;
                            rdx19 = oc;
                        }
                        --pending_spaces_3;
                        zf21 = pending_spaces_3 == 0;
                        if (zf21) 
                            break;
                        ecx15 = space_character;
                        rax18 = rdx19;
                    }
                } else {
                    rdx19 = rax18;
                }
                rax22 = rdx19 + 1;
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi17) + reinterpret_cast<unsigned char>(rdx19)) = *reinterpret_cast<signed char*>(&r13d9);
                less23 = reinterpret_cast<signed char>(rax22) < reinterpret_cast<signed char>(output_blocksize);
                oc = rax22;
                if (less23) 
                    goto addr_6423_7; else 
                    goto addr_64a0_6;
            }
        }
        return;
    }
}