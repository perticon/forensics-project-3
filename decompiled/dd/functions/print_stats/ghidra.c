undefined8 print_stats(void)

{
  char *pcVar1;
  long lVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  undefined8 in_RAX;
  undefined8 uVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  
  if (status_level == 1) {
    return in_RAX;
  }
  if (0 < progress_len) {
    pcVar1 = stderr->_IO_write_ptr;
    if (pcVar1 < stderr->_IO_write_end) {
      stderr->_IO_write_ptr = pcVar1 + 1;
      *pcVar1 = '\n';
    }
    else {
      __overflow(stderr,10);
    }
    progress_len = 0;
  }
  uVar7 = w_partial;
  uVar4 = w_full;
  uVar3 = r_partial;
  uVar6 = r_full;
  uVar5 = dcgettext(0,"%ld+%ld records in\n%ld+%ld records out\n",5);
  __fprintf_chk(stderr,1,uVar5,uVar6,uVar3,uVar4);
  lVar2 = r_truncate;
  if (r_truncate != 0) {
    uVar6 = dcngettext(0,"%ld truncated record\n","%ld truncated records\n",r_truncate,5);
    uVar7 = __fprintf_chk(stderr,1,uVar6,lVar2);
  }
  if (status_level != 2) {
    uVar6 = print_xfer_stats(0);
    return uVar6;
  }
  return uVar7;
}