void** print_stats() {
    int1_t zf1;
    void* rsp2;
    int32_t ecx3;
    void** rdi4;
    void** rax5;
    void** r13_6;
    void** r12_7;
    void** rbp8;
    void** rbx9;
    void** rax10;
    void** rcx11;
    void** r9_12;
    void** rdi13;
    void** r8_14;
    void** r12_15;
    void** rax16;
    void** rdx17;
    void* rsp18;
    void** rax19;
    void** rdi20;
    int1_t zf21;
    void** rbp22;
    void* rsp23;
    void* rax24;
    void** rax25;
    void** rdi26;
    void** rax27;
    void* rsp28;
    void** rdi29;
    void** rax30;
    void* rsp31;
    void** rax32;
    void** rax33;
    void* rsp34;
    void** v35;
    void** v36;
    void** v37;
    void* rsp38;
    void** rdi39;
    void** v40;
    void** rax41;
    void** rax42;
    uint32_t edx43;
    void** v44;
    uint32_t edx45;
    int64_t r8_46;
    void** rdi47;
    void** rax48;
    void** v49;
    void** v50;
    void** v51;
    void** rax52;
    void** rax53;
    void** rdi54;
    void** rax55;
    int64_t rbp56;
    void** rax57;
    void** rax58;
    void** rdx59;
    void** rax60;
    void** rdi61;
    void** rax62;
    void** rdi63;
    void** rax64;
    int32_t ecx65;
    void** rdi66;
    void** rax67;
    void** rax68;
    void** rax69;

    while (zf1 = status_level == 1, !zf1) {
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8);
        ecx3 = progress_len;
        if (!(reinterpret_cast<uint1_t>(ecx3 < 0) | reinterpret_cast<uint1_t>(ecx3 == 0))) {
            rdi4 = stderr;
            rax5 = *reinterpret_cast<void***>(rdi4 + 40);
            if (reinterpret_cast<unsigned char>(rax5) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi4 + 48))) {
                fun_26e0();
                rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
            } else {
                *reinterpret_cast<void***>(rdi4 + 40) = rax5 + 1;
                *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(10);
            }
            progress_len = 0;
        }
        r13_6 = w_partial;
        r12_7 = r_full;
        rbp8 = w_full;
        rbx9 = r_partial;
        rax10 = fun_2660();
        rcx11 = r12_7;
        r9_12 = rbp8;
        rdi13 = stderr;
        r8_14 = rbx9;
        fun_2930(rdi13, 1, rax10, rdi13, 1, rax10);
        r12_15 = r_truncate;
        rax16 = r13_6;
        rdx17 = reinterpret_cast<void**>(0x587a);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
        if (r12_15) {
            *reinterpret_cast<int32_t*>(&r8_14) = 5;
            *reinterpret_cast<int32_t*>(&r8_14 + 4) = 0;
            rax19 = fun_28c0();
            rdi20 = stderr;
            rcx11 = r12_15;
            rdx17 = rax19;
            rax16 = fun_2930(rdi20, 1, rdx17, rdi20, 1, rdx17);
            rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8 - 8 + 8);
        }
        zf21 = status_level == 2;
        if (zf21) 
            goto addr_58d0_10;
        rbp22 = reinterpret_cast<void**>(0);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) + 8 + 8 + 8 + 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x7e8);
        rax24 = g28;
        if (1) {
            rax25 = gethrxtime(0, 1, rdx17, rcx11, r8_14, r9_12);
            rsp23 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
            rbp22 = rax25;
        }
        rdi26 = w_bytes;
        rax27 = human_readable(rdi26, reinterpret_cast<int64_t>(rsp23) + 16, 0x1d1, 1, 1, r9_12);
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp23) - 8 + 8);
        rdi29 = w_bytes;
        rax30 = human_readable(rdi29, reinterpret_cast<int64_t>(rsp28) + 0x29e, 0x1f1, 1, 1, r9_12);
        rsp31 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp28) - 8 + 8);
        rax32 = start_time;
        if (reinterpret_cast<signed char>(rax32) >= reinterpret_cast<signed char>(rbp22)) {
            rax33 = fun_2660();
            rsp34 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8);
            r9_12 = rax33;
            fun_2530(reinterpret_cast<int64_t>(rsp34) + 0x52c, 0x28e, 1, 0x28e, "%s B/s", r9_12, v35, v36, v37);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp34) - 8 + 8);
            __asm__("pxor xmm0, xmm0");
        } else {
            rdi39 = w_bytes;
            __asm__("pxor xmm0, xmm0");
            __asm__("cvtsi2sd xmm0, r8");
            __asm__("divsd xmm0, [rip+0xa4e2]");
            *reinterpret_cast<void***>(rdi39) = v40;
            rax41 = human_readable(rdi39 + 4, reinterpret_cast<int64_t>(rsp31) + 0x52c + 4, 0x1d1, 0x3b9aca00, reinterpret_cast<unsigned char>(rbp22) - reinterpret_cast<unsigned char>(rax32), r9_12);
            rax42 = fun_2680(rax41, rax41);
            rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp31) - 8 + 8 - 8 + 8);
            edx43 = slash_s_5;
            *reinterpret_cast<void***>(rax41) = v44;
            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rax41) + reinterpret_cast<unsigned char>(rax42)) = *reinterpret_cast<int16_t*>(&edx43);
            edx45 = gf490;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax41) + reinterpret_cast<unsigned char>(rax42) + 2) = *reinterpret_cast<signed char*>(&edx45);
        }
        r8_46 = reinterpret_cast<int64_t>("%g s");
        if (!1) {
            rdi47 = stderr;
            rax48 = *reinterpret_cast<void***>(rdi47 + 40);
            if (reinterpret_cast<unsigned char>(rax48) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi47 + 48))) {
                *reinterpret_cast<void***>(rdi47) = gd;
                fun_26e0();
                rsp38 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp38) - 8 + 8);
                *reinterpret_cast<int32_t*>(rdi47 + 4) = g11;
                r8_46 = reinterpret_cast<int64_t>("%.0f s");
            } else {
                r8_46 = reinterpret_cast<int64_t>("%.0f s");
                *reinterpret_cast<void***>(rdi47 + 40) = rax48 + 1;
                *reinterpret_cast<void***>(rax48) = reinterpret_cast<void**>(13);
            }
        }
        fun_2530(reinterpret_cast<int64_t>(rsp38) + 0x7c0, 24, 1, 24, r8_46, r9_12, v49, v50, v51);
        rax52 = fun_2680(rax27, rax27);
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax27) + reinterpret_cast<unsigned char>(rax52) + 0xfffffffffffffffe) == 32) {
            rax53 = fun_28c0();
            rdi54 = stderr;
            rax55 = fun_2930(rdi54, 1, rax53, rdi54, 1, rax53);
            *reinterpret_cast<int32_t*>(&rbp56) = *reinterpret_cast<int32_t*>(&rax55);
        } else {
            rax57 = fun_2680(rax30, rax30);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax30) + reinterpret_cast<unsigned char>(rax57) + 0xfffffffffffffffe) == 32) {
                rax58 = fun_2660();
                rdx59 = rax58;
            } else {
                rax60 = fun_2660();
                rdx59 = rax60;
            }
            rdi61 = stderr;
            rax62 = fun_2930(rdi61, 1, rdx59, rdi61, 1, rdx59);
            *reinterpret_cast<int32_t*>(&rbp56) = *reinterpret_cast<int32_t*>(&rax62);
        }
        if (1) {
            rdi63 = stderr;
            rax64 = *reinterpret_cast<void***>(rdi63 + 40);
            if (reinterpret_cast<unsigned char>(rax64) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi63 + 48))) {
                fun_26e0();
            } else {
                *reinterpret_cast<void***>(rdi63 + 40) = rax64 + 1;
                *reinterpret_cast<void***>(rax64) = reinterpret_cast<void**>(10);
            }
        } else {
            if (*reinterpret_cast<int32_t*>(&rbp56) >= 0 && (ecx65 = progress_len, ecx65 > *reinterpret_cast<int32_t*>(&rbp56))) {
                rdi66 = stderr;
                fun_2930(rdi66, 1, "%*s");
            }
            progress_len = *reinterpret_cast<int32_t*>(&rbp56);
        }
        rax67 = w_bytes;
        reported_w_bytes = rax67;
        rax68 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rax24) - reinterpret_cast<uint64_t>(g28));
        if (!rax68) 
            goto addr_5686_47;
        rax69 = fun_2690();
    }
    return rax69;
    addr_58d0_10:
    return rax16;
    addr_5686_47:
    return rax68;
}