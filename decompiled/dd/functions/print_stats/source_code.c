print_stats (void)
{
  if (status_level == STATUS_NONE)
    return;

  if (0 < progress_len)
    {
      fputc ('\n', stderr);
      progress_len = 0;
    }

  fprintf (stderr,
           _("%"PRIdMAX"+%"PRIdMAX" records in\n"
             "%"PRIdMAX"+%"PRIdMAX" records out\n"),
           r_full, r_partial, w_full, w_partial);

  if (r_truncate != 0)
    fprintf (stderr,
             ngettext ("%"PRIdMAX" truncated record\n",
                       "%"PRIdMAX" truncated records\n",
                       select_plural (r_truncate)),
             r_truncate);

  if (status_level == STATUS_NOXFER)
    return;

  print_xfer_stats (0);
}