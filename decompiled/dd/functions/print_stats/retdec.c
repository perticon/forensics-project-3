void print_stats(void) {
    // 0x5800
    if (status_level == 1) {
        // 0x58d0
        return;
    }
    // 0x580d
    if (progress_len >= 1) {
        int64_t v1 = (int64_t)g17; // 0x5821
        int64_t * v2 = (int64_t *)(v1 + 40); // 0x5828
        uint64_t v3 = *v2; // 0x5828
        if (v3 >= *(int64_t *)(v1 + 48)) {
            // 0x5928
            function_26e0();
        } else {
            // 0x5836
            *v2 = v3 + 1;
            *(char *)v3 = 10;
        }
        // 0x5841
        progress_len = 0;
    }
    // 0x584b
    function_2660();
    function_2930();
    if (r_truncate != 0) {
        // 0x58e0
        function_28c0();
        function_2930();
    }
    // 0x58ad
    if (status_level == 2) {
        // 0x58d0
        return;
    }
    // 0x58b6
    print_xfer_stats(0);
}