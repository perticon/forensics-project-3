main (int argc, char **argv)
{
  int i;
  int exit_status;
  off_t offset;

  install_signal_handlers ();

  initialize_main (&argc, &argv);
  set_program_name (argv[0]);
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  /* Arrange to close stdout if parse_long_options exits.  */
  atexit (maybe_close_stdout);

  page_size = getpagesize ();

  parse_gnu_standard_options_only (argc, argv, PROGRAM_NAME, PACKAGE, Version,
                                   true, usage, AUTHORS, (char const *) NULL);
  close_stdout_required = false;

  /* Initialize translation table to identity translation. */
  for (i = 0; i < 256; i++)
    trans_table[i] = i;

  /* Decode arguments. */
  scanargs (argc, argv);

  apply_translations ();

  if (input_file == NULL)
    {
      input_file = _("standard input");
      set_fd_flags (STDIN_FILENO, input_flags, input_file);
    }
  else
    {
      if (ifd_reopen (STDIN_FILENO, input_file, O_RDONLY | input_flags, 0) < 0)
        die (EXIT_FAILURE, errno, _("failed to open %s"),
             quoteaf (input_file));
    }

  offset = lseek (STDIN_FILENO, 0, SEEK_CUR);
  input_seekable = (0 <= offset);
  input_offset = MAX (0, offset);
  input_seek_errno = errno;

  if (output_file == NULL)
    {
      output_file = _("standard output");
      set_fd_flags (STDOUT_FILENO, output_flags, output_file);
    }
  else
    {
      mode_t perms = MODE_RW_UGO;
      int opts
        = (output_flags
           | (conversions_mask & C_NOCREAT ? 0 : O_CREAT)
           | (conversions_mask & C_EXCL ? O_EXCL : 0)
           | (seek_records || (conversions_mask & C_NOTRUNC) ? 0 : O_TRUNC));

      off_t size;
      if ((INT_MULTIPLY_WRAPV (seek_records, output_blocksize, &size)
           || INT_ADD_WRAPV (seek_bytes, size, &size))
          && !(conversions_mask & C_NOTRUNC))
        die (EXIT_FAILURE, 0,
             _("offset too large: "
               "cannot truncate to a length of seek=%"PRIdMAX""
               " (%td-byte) blocks"),
             seek_records, output_blocksize);

      /* Open the output file with *read* access only if we might
         need to read to satisfy a 'seek=' request.  If we can't read
         the file, go ahead with write-only access; it might work.  */
      if ((! seek_records
           || ifd_reopen (STDOUT_FILENO, output_file, O_RDWR | opts, perms) < 0)
          && (ifd_reopen (STDOUT_FILENO, output_file, O_WRONLY | opts, perms)
              < 0))
        die (EXIT_FAILURE, errno, _("failed to open %s"),
             quoteaf (output_file));

      if (seek_records != 0 && !(conversions_mask & C_NOTRUNC))
        {
          if (iftruncate (STDOUT_FILENO, size) != 0)
            {
              /* Complain only when ftruncate fails on a regular file, a
                 directory, or a shared memory object, as POSIX 1003.1-2004
                 specifies ftruncate's behavior only for these file types.
                 For example, do not complain when Linux kernel 2.4 ftruncate
                 fails on /dev/fd0.  */
              int ftruncate_errno = errno;
              struct stat stdout_stat;
              if (ifstat (STDOUT_FILENO, &stdout_stat) != 0)
                {
                  error (0, errno, _("cannot fstat %s"),
                         quoteaf (output_file));
                  exit_status = EXIT_FAILURE;
                }
              else if (S_ISREG (stdout_stat.st_mode)
                       || S_ISDIR (stdout_stat.st_mode)
                       || S_TYPEISSHM (&stdout_stat))
                {
                  intmax_t isize = size;
                  error (0, ftruncate_errno,
                         _("failed to truncate to %"PRIdMAX" bytes"
                           " in output file %s"),
                         isize, quoteaf (output_file));
                  exit_status = EXIT_FAILURE;
                }
            }
        }
    }

  start_time = gethrxtime ();
  next_time = start_time + XTIME_PRECISION;

  exit_status = dd_copy ();

  int sync_status = synchronize_output ();
  if (sync_status)
    exit_status = sync_status;

  if (max_records == 0 && max_bytes == 0)
    {
      /* Special case to invalidate cache to end of file.  */
      if (i_nocache && !invalidate_cache (STDIN_FILENO, 0))
        {
          error (0, errno, _("failed to discard cache for: %s"),
                 quotef (input_file));
          exit_status = EXIT_FAILURE;
        }
      if (o_nocache && !invalidate_cache (STDOUT_FILENO, 0))
        {
          error (0, errno, _("failed to discard cache for: %s"),
                 quotef (output_file));
          exit_status = EXIT_FAILURE;
        }
    }
  else
    {
      /* Invalidate any pending region or to EOF if appropriate.  */
      if (i_nocache || i_nocache_eof)
        invalidate_cache (STDIN_FILENO, 0);
      if (o_nocache || o_nocache_eof)
        invalidate_cache (STDOUT_FILENO, 0);
    }

  finish_up ();
  main_exit (exit_status);
}