void copy_with_block(char *param_1,long param_2)

{
  long lVar1;
  char *pcVar2;
  long lVar3;
  
  pcVar2 = param_1 + param_2;
  if (param_2 != 0) {
    do {
      while (lVar3 = col, *param_1 == newline_character) {
        if (col < conversion_blocksize) {
          do {
            lVar1 = oc + 1;
            *(undefined *)(obuf + oc) = space_character;
            oc = lVar1;
            if (output_blocksize <= lVar1) {
              write_output();
            }
            lVar3 = lVar3 + 1;
          } while (lVar3 < conversion_blocksize);
        }
        param_1 = param_1 + 1;
        col = 0;
        if (param_1 == pcVar2) {
          col = 0;
          return;
        }
      }
      if (conversion_blocksize == col) {
        r_truncate = r_truncate + 1;
      }
      else if (col < conversion_blocksize) {
        lVar1 = oc + 1;
        *(char *)(obuf + oc) = *param_1;
        oc = lVar1;
        if (output_blocksize <= lVar1) {
          write_output();
          lVar3 = col;
        }
      }
      col = lVar3 + 1;
      param_1 = param_1 + 1;
    } while (param_1 != pcVar2);
  }
  return;
}