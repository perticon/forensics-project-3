copy_with_block (char const *buf, idx_t nread)
{
  for (idx_t i = nread; i; i--, buf++)
    {
      if (*buf == newline_character)
        {
          if (col < conversion_blocksize)
            {
              idx_t j;
              for (j = col; j < conversion_blocksize; j++)
                output_char (space_character);
            }
          col = 0;
        }
      else
        {
          if (col == conversion_blocksize)
            r_truncate++;
          else if (col < conversion_blocksize)
            output_char (*buf);
          col++;
        }
    }
}