void copy_with_block(void** rdi, void** rsi, void** rdx, ...) {
    void** r12_4;
    void** rbp5;
    uint32_t eax6;
    int1_t zf7;
    int64_t rbx8;
    int64_t rdx9;
    void** tmp64_10;
    void** rcx11;
    void** rsi12;
    void** rdx13;
    int1_t less14;
    void** rdx15;
    void** rcx16;
    uint32_t esi17;
    void** rax18;
    int1_t less19;
    int1_t less_or_equal20;

    r12_4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rsi));
    if (rsi) {
        rbp5 = rdi;
        while (1) {
            eax6 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5));
            zf7 = *reinterpret_cast<unsigned char*>(&eax6) == newline_character;
            rbx8 = col;
            rdx9 = conversion_blocksize;
            if (!zf7) {
                if (rdx9 == rbx8) {
                    tmp64_10 = r_truncate + 1;
                    r_truncate = tmp64_10;
                } else {
                    if (rdx9 > rbx8 && (rcx11 = oc, rsi12 = obuf, rdx13 = rcx11 + 1, *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi12) + reinterpret_cast<unsigned char>(rcx11)) = *reinterpret_cast<unsigned char*>(&eax6), less14 = reinterpret_cast<signed char>(rdx13) < reinterpret_cast<signed char>(output_blocksize), oc = rdx13, !less14)) {
                        write_output(rdi);
                        rbx8 = col;
                    }
                }
                ++rbp5;
                col = rbx8 + 1;
                if (rbp5 == r12_4) 
                    break;
            } else {
                if (rdx9 > rbx8) {
                    do {
                        rdx15 = oc;
                        rcx16 = obuf;
                        esi17 = space_character;
                        rax18 = rdx15 + 1;
                        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx16) + reinterpret_cast<unsigned char>(rdx15)) = *reinterpret_cast<signed char*>(&esi17);
                        less19 = reinterpret_cast<signed char>(rax18) < reinterpret_cast<signed char>(output_blocksize);
                        oc = rax18;
                        if (!less19) {
                            write_output(rdi);
                        }
                        ++rbx8;
                        less_or_equal20 = conversion_blocksize <= rbx8;
                    } while (!less_or_equal20);
                }
                ++rbp5;
                col = 0;
                if (rbp5 == r12_4) 
                    break;
            }
        }
    }
    return;
}