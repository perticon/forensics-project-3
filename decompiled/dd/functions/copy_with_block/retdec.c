void copy_with_block(char * buf, int64_t nread) {
    if (nread == 0) {
        // 0x6349
        return;
    }
    int64_t v1 = (int64_t)buf;
    int64_t v2 = v1 + nread; // 0x62b2
    int64_t v3 = col; // 0x62b0
    int64_t v4 = v1; // 0x62b0
    while (true) {
        int64_t v5 = v4;
        int64_t v6 = v3; // 0x6322
        char v7 = *(char *)v5; // 0x6318
        if (v7 != newline_character) {
            int64_t v8; // 0x62b0
            if (conversion_blocksize == v6) {
                // 0x6398
                r_truncate++;
                v8 = v6;
            } else {
                // 0x62d9
                v8 = v6;
                if (conversion_blocksize > v6) {
                    int64_t v9 = oc + 1; // 0x62e9
                    *(char *)(obuf + oc) = v7;
                    oc = v9;
                    v8 = v6;
                    if (v9 >= output_blocksize) {
                        // 0x63a8
                        write_output();
                        v8 = col;
                    }
                }
            }
            int64_t v10 = v8 + 1; // 0x6304
            int64_t v11 = v5 + 1; // 0x6308
            col = v10;
            v3 = v10;
            v4 = v11;
            if (v11 == v2) {
                // break -> 0x6349
                break;
            }
        } else {
            int64_t v12 = v6; // 0x6335
            if (conversion_blocksize > v6) {
                int64_t v13 = oc + 1; // 0x6377
                *(char *)(obuf + oc) = space_character;
                oc = v13;
                if (v13 >= output_blocksize) {
                    // 0x638f
                    write_output();
                }
                int64_t v14 = v12 + 1; // 0x6355
                v12 = v14;
                while (conversion_blocksize > v14) {
                    // 0x6362
                    v13 = oc + 1;
                    *(char *)(obuf + oc) = space_character;
                    oc = v13;
                    if (v13 >= output_blocksize) {
                        // 0x638f
                        write_output();
                    }
                    // 0x6355
                    v14 = v12 + 1;
                    v12 = v14;
                }
            }
            int64_t v15 = v5 + 1; // 0x6339
            col = 0;
            v3 = 0;
            v4 = v15;
            if (v15 == v2) {
                // break -> 0x6349
                break;
            }
        }
    }
}