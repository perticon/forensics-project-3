ulong skip(int param_1,undefined8 param_2,ulong param_3,ulong param_4,ulong *param_5)

{
  int iVar1;
  int *piVar2;
  long lVar3;
  long lVar4;
  __off_t _Var5;
  ulong uVar6;
  undefined8 uVar7;
  undefined8 uVar8;
  char *pcVar9;
  ulong uVar10;
  long in_FS_OFFSET;
  bool bVar11;
  undefined local_d8 [24];
  uint local_c0;
  long local_a8;
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  piVar2 = __errno_location();
  *piVar2 = 0;
  lVar3 = param_3 * param_4;
  if (SEXT816(lVar3) == SEXT816((long)param_3) * SEXT816((long)param_4)) {
    lVar4 = lVar3 + *param_5;
    uVar6 = (ulong)SCARRY8(lVar3,*param_5);
    if (SCARRY8(lVar3,*param_5)) goto LAB_0010656b;
    _Var5 = lseek(param_1,lVar4,1);
    if (-1 < _Var5) {
      if (param_1 != 0) {
LAB_00106620:
        *param_5 = 0;
        uVar6 = 0;
        goto LAB_0010662a;
      }
      iVar1 = ifstat(0,local_d8);
      if (iVar1 == 0) {
        if ((local_c0 & 0xd000) == 0x8000) {
          if (input_offset < 0) goto LAB_0010662a;
          if (local_a8 - input_offset < lVar4) {
            uVar6 = (lVar4 - local_a8) / (long)param_4;
            lVar4 = local_a8 - input_offset;
          }
        }
        else if (input_offset < 0) goto LAB_0010662a;
        bVar11 = SCARRY8(input_offset,lVar4);
        input_offset = input_offset + lVar4;
        if (!bVar11) goto LAB_0010662a;
      }
      else {
        uVar7 = quotearg_style(4,param_2);
        uVar8 = dcgettext(0,"cannot fstat %s",5);
        nl_error(1,*piVar2,uVar8,uVar7);
      }
      input_offset = -1;
LAB_0010662a:
      if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
        __stack_chk_fail();
      }
      return uVar6;
    }
    iVar1 = *piVar2;
    _Var5 = lseek(param_1,0,2);
    if (_Var5 < 0) goto LAB_0010658b;
    if (iVar1 != 0) goto LAB_0010673a;
  }
  else {
LAB_0010656b:
    _Var5 = lseek(param_1,0,2);
    if (_Var5 < 0) {
      iVar1 = 0;
LAB_0010658b:
      if (param_1 == 0) {
        lVar3 = ibuf;
        if (ibuf == 0) {
          alloc_ibuf_part_0();
          lVar3 = ibuf;
        }
      }
      else {
        alloc_obuf();
        lVar3 = obuf;
      }
      do {
        uVar10 = param_4;
        uVar6 = param_3;
        if (param_3 == 0) {
          uVar6 = *param_5;
          goto LAB_00106618;
        }
        while( true ) {
          lVar4 = (*iread_fnc)(param_1,lVar3,uVar10);
          if (lVar4 < 0) {
            if (param_1 != 0) goto LAB_001067da;
            uVar7 = quotearg_style(4,param_2);
            uVar8 = dcgettext(0,"error reading %s",5);
            nl_error(0,*piVar2,uVar8,uVar7);
            if ((conversions_mask._1_1_ & 1) == 0) goto LAB_001067ca;
            print_stats();
            goto LAB_001067ca;
          }
          if (lVar4 == 0) goto LAB_0010662a;
          if (((param_1 == 0) && (-1 < input_offset)) &&
             (bVar11 = SCARRY8(input_offset,lVar4), input_offset = input_offset + lVar4, bVar11)) {
            input_offset = -1;
          }
          if (uVar6 == 0) goto LAB_00106620;
          param_3 = uVar6 - 1;
          if (param_3 != 0) break;
          uVar6 = *param_5;
          if (uVar6 == 0) goto LAB_0010662a;
LAB_00106618:
          uVar10 = uVar6;
          uVar6 = 0;
        }
      } while( true );
    }
  }
  iVar1 = 0x4b;
LAB_0010673a:
  if (param_1 != 0) goto LAB_001067da;
  uVar7 = quotearg_n_style_colon(0,3,param_2);
  pcVar9 = "%s: cannot skip";
  do {
    uVar8 = dcgettext(0,pcVar9,5);
    nl_error(0,iVar1,uVar8,uVar7);
LAB_001067ca:
    quit_constprop_0();
LAB_001067da:
    uVar7 = quotearg_n_style_colon(0,3,param_2);
    pcVar9 = "%s: cannot seek";
  } while( true );
}