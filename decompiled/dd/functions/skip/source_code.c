skip (int fdesc, char const *file, intmax_t records, idx_t blocksize,
      idx_t *bytes)
{
  /* Try lseek and if an error indicates it was an inappropriate operation --
     or if the file offset is not representable as an off_t --
     fall back on using read.  */

  errno = 0;
  off_t offset;
  if (! INT_MULTIPLY_WRAPV (records, blocksize, &offset)
      && ! INT_ADD_WRAPV (offset, *bytes, &offset)
      && 0 <= lseek (fdesc, offset, SEEK_CUR))
    {
      if (fdesc == STDIN_FILENO)
        {
           struct stat st;
           if (ifstat (STDIN_FILENO, &st) != 0)
             die (EXIT_FAILURE, errno, _("cannot fstat %s"), quoteaf (file));
           if (usable_st_size (&st) && 0 <= input_offset
               && st.st_size - input_offset < offset)
             {
               /* When skipping past EOF, return the number of _full_ blocks
                * that are not skipped, and set offset to EOF, so the caller
                * can determine the requested skip was not satisfied.  */
               records = ( offset - st.st_size ) / blocksize;
               offset = st.st_size - input_offset;
             }
           else
             records = 0;
           advance_input_offset (offset);
        }
      else
        {
          records = 0;
          *bytes = 0;
        }
      return records;
    }
  else
    {
      int lseek_errno = errno;

      /* The seek request may have failed above if it was too big
         (> device size, > max file size, etc.)
         Or it may not have been done at all (> OFF_T_MAX).
         Therefore try to seek to the end of the file,
         to avoid redundant reading.  */
      if (lseek (fdesc, 0, SEEK_END) >= 0)
        {
          /* File is seekable, and we're at the end of it, and
             size <= OFF_T_MAX. So there's no point using read to advance.  */

          if (!lseek_errno)
            {
              /* The original seek was not attempted as offset > OFF_T_MAX.
                 We should error for write as can't get to the desired
                 location, even if OFF_T_MAX < max file size.
                 For read we're not going to read any data anyway,
                 so we should error for consistency.
                 It would be nice to not error for /dev/{zero,null}
                 for any offset, but that's not a significant issue.  */
              lseek_errno = EOVERFLOW;
            }

          if (fdesc == STDIN_FILENO)
            error (0, lseek_errno, _("%s: cannot skip"), quotef (file));
          else
            error (0, lseek_errno, _("%s: cannot seek"), quotef (file));
          /* If the file has a specific size and we've asked
             to skip/seek beyond the max allowable, then quit.  */
          quit (EXIT_FAILURE);
        }
      /* else file_size && offset > OFF_T_MAX or file ! seekable */

      char *buf;
      if (fdesc == STDIN_FILENO)
        {
          alloc_ibuf ();
          buf = ibuf;
        }
      else
        {
          alloc_obuf ();
          buf = obuf;
        }

      do
        {
          ssize_t nread = iread_fnc (fdesc, buf, records ? blocksize : *bytes);
          if (nread < 0)
            {
              if (fdesc == STDIN_FILENO)
                {
                  error (0, errno, _("error reading %s"), quoteaf (file));
                  if (conversions_mask & C_NOERROR)
                    print_stats ();
                }
              else
                error (0, lseek_errno, _("%s: cannot seek"), quotef (file));
              quit (EXIT_FAILURE);
            }
          else if (nread == 0)
            break;
          else if (fdesc == STDIN_FILENO)
            advance_input_offset (nread);

          if (records != 0)
            records--;
          else
            *bytes = 0;
        }
      while (records || *bytes);

      return records;
    }
}