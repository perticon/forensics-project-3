int64_t skip(int32_t fdesc, char * file, int64_t records, uint64_t blocksize, int64_t * bytes) {
    int64_t v1 = __readfsqword(40); // 0x6531
    int32_t * v2 = (int32_t *)function_2570(); // 0x6549
    *v2 = 0;
    int128_t v3 = (int128_t)blocksize * (int128_t)records; // 0x6555
    int64_t v4 = function_2710();
    int64_t v5; // 0x6510
    int64_t v6; // 0x6510
    int64_t v7; // 0x6510
    int64_t v8; // 0x6510
    int32_t v9; // 0x6510
    int64_t v10; // 0x66ba
    if (v3 > 0x7fffffffffffffff) {
        // 0x656b
        if (v4 >= 0) {
            // 0x6732
            v9 = 75;
            goto lab_0x673a;
        } else {
            goto lab_0x658b;
        }
    } else {
        if (v4 < 0) {
            int32_t v11 = *v2; // 0x670b
            if (function_2710() < 0) {
                goto lab_0x658b;
            } else {
                // 0x672b
                v9 = v11;
                if (v11 != 0) {
                    goto lab_0x673a;
                } else {
                    // 0x6732
                    v9 = 75;
                    goto lab_0x673a;
                }
            }
        } else {
            if (fdesc != 0) {
                // 0x6620
                *bytes = 0;
                v5 = 0;
                goto lab_0x662a_3;
            } else {
                // 0x669d
                int64_t v12; // bp-216, 0x6510
                int32_t v13 = ifstat(0, (struct stat *)&v12); // 0x66a4
                v7 = 0;
                if (v13 != 0) {
                    goto lab_0x6826;
                } else {
                    int64_t v14 = v3; // 0x6555
                    v10 = input_offset;
                    int32_t v15; // 0x6510
                    if ((v15 & 0xd000) != 0x8000) {
                        // 0x6767
                        v5 = 0;
                        v8 = v14;
                        v6 = 0;
                        if (v10 < 0) {
                            goto lab_0x662a_3;
                        } else {
                            goto lab_0x6770;
                        }
                    } else {
                        // 0x66d1
                        v5 = 0;
                        if (v10 < 0) {
                            goto lab_0x662a_3;
                        } else {
                            // 0x66da
                            int64_t v16; // 0x6510
                            int64_t v17 = v16 - v10; // 0x66e2
                            v8 = v14;
                            v6 = 0;
                            if (v17 < v14) {
                                // 0x66ee
                                v8 = v17;
                                v6 = (v14 - v16) / blocksize;
                            }
                            goto lab_0x6770;
                        }
                    }
                }
            }
        }
    }
  lab_0x65c3:
    if (iread_fnc < NULL) {
        // break -> 0x6785
        goto lab_0x6785;
    }
    // 0x65d7
    int64_t v18; // 0x6510
    int64_t v19 = v18; // 0x65d7
    if (iread_fnc == NULL) {
        // 0x662a
        v5 = v19;
        goto lab_0x662a_3;
    }
    int64_t v20; // 0x6510
    if (fdesc == 0) {
        uint64_t v21 = input_offset; // 0x65e2
        if (v21 >= 0) {
            int64_t v22 = v21 + v20; // 0x65ee
            if (((v22 ^ v21) & (v22 ^ v20)) < 0) {
                // 0x6811
                input_offset = -1;
            } else {
                // 0x65f7
                input_offset = v22;
            }
        }
    }
    if (v18 == 0) {
        // 0x6620
        *bytes = 0;
        v5 = 0;
        goto lab_0x662a_3;
    }
    int64_t v23 = v18 - 1; // 0x6603
    int64_t v24 = v23; // 0x6607
    if (v23 != 0) {
        goto lab_0x65b0;
    } else {
        // 0x6609
        if (blocksize == 0) {
            // 0x662a
            v5 = 0;
            goto lab_0x662a_3;
        }
        // 0x6618
        v18 = 0;
        goto lab_0x65c3;
    }
  lab_0x658b:;
    int32_t v25 = 0;
    if (fdesc != 0) {
        // 0x666a
        alloc_obuf();
    } else {
        // 0x6594
        if (ibuf == 0) {
            // 0x6701
            alloc_ibuf_part_0();
        }
    }
    // 0x65b0
    v20 = (int64_t)iread_fnc;
    v24 = records;
    while (true) {
      lab_0x65b0:
        // 0x65b0
        v18 = v24;
        if (v24 == 0) {
            // 0x6618
            v18 = 0;
            goto lab_0x65c3;
        } else {
            goto lab_0x65c3;
        }
    }
  lab_0x6785:;
    // 0x6785
    int32_t v26; // 0x6510
    int32_t v27; // 0x6510
    if (fdesc != 0) {
        // 0x67cf
        v27 = v25;
        goto lab_0x67da;
    } else {
        // 0x678a
        quotearg_style();
        int64_t v28 = function_2660(); // 0x67a9
        nl_error(0, *v2, (char *)v28);
        v26 = v25;
        if (g32 % 2 != 0) {
            // 0x680a
            print_stats();
            v26 = v25;
        }
        goto lab_0x67ca;
    }
  lab_0x673a:
    // 0x673a
    v27 = v9;
    int32_t v29; // 0x6510
    if (fdesc != 0) {
        goto lab_0x67da;
    } else {
        // 0x674e
        quotearg_n_style_colon();
        v29 = v9;
        goto lab_0x67ee;
    }
  lab_0x67da:
    // 0x67da
    quotearg_n_style_colon();
    v29 = v27;
    goto lab_0x67ee;
  lab_0x67ca:
    // 0x67ca
    quit_constprop_0();
    // 0x67cf
    v27 = v26;
    goto lab_0x67da;
  lab_0x662a_3:;
    int64_t result = v5;
    if (v1 == __readfsqword(40)) {
        // 0x6641
        return result;
    }
    // 0x6821
    function_2690();
    v7 = result;
    goto lab_0x6826;
  lab_0x6826:
    // 0x6826
    quotearg_style();
    int64_t v30 = function_2660(); // 0x6845
    nl_error(1, *v2, (char *)v30);
    int64_t v31 = v7; // 0x685b
    goto lab_0x6860;
  lab_0x67ee:
    // 0x67ee
    nl_error(0, v29, (char *)function_2660());
    v26 = v29;
    goto lab_0x67ca;
  lab_0x6860:
    // 0x6860
    input_offset = -1;
    v5 = v31;
    goto lab_0x662a_3;
  lab_0x6770:;
    int64_t v32 = v8 + v10; // 0x6770
    v31 = v6;
    if (((v32 ^ v10) & (v32 ^ v8)) < 0) {
        goto lab_0x6860;
    } else {
        // 0x6779
        input_offset = v32;
        v5 = v6;
        goto lab_0x662a_3;
    }
}