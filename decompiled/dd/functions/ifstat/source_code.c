ifstat (int fd, struct stat *st)
{
  int ret;

  do
    {
      process_signals ();
      ret = fstat (fd, st);
    }
  while (ret < 0 && errno == EINTR);

  return ret;
}