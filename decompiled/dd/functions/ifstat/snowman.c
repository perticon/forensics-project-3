int32_t ifstat(uint32_t edi, void** rsi, ...) {
    void** rdi1;
    uint32_t ebp3;
    void** rbx4;
    int32_t eax5;
    void** rax6;

    *reinterpret_cast<uint32_t*>(&rdi1) = edi;
    ebp3 = *reinterpret_cast<uint32_t*>(&rdi1);
    rbx4 = rsi;
    do {
        process_signals(rdi1, rsi);
        rsi = rbx4;
        *reinterpret_cast<uint32_t*>(&rdi1) = ebp3;
        *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
        eax5 = fun_2970(rdi1, rsi);
        if (eax5 >= 0) 
            break;
        rax6 = fun_2570();
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 4));
    return eax5;
}