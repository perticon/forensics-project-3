int ifstat(int param_1,stat *param_2)

{
  int iVar1;
  int *piVar2;
  
  do {
    process_signals();
    iVar1 = fstat(param_1,param_2);
    if (-1 < iVar1) {
      return iVar1;
    }
    piVar2 = __errno_location();
  } while (*piVar2 == 4);
  return iVar1;
}