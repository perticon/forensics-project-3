int64_t operand_matches(int64_t a1, char * a2, int64_t a3) {
    int64_t v1 = 0;
    char v2 = *(char *)(v1 + (int64_t)a2); // 0x49f1
    char v3 = *(char *)(v1 + a1); // 0x49f5
    while (v2 != 0) {
        // 0x49e8
        if (v2 != v3) {
            // 0x4a10
            return 0;
        }
        v1++;
        v2 = *(char *)(v1 + (int64_t)a2);
        v3 = *(char *)(v1 + a1);
    }
    // 0x49fe
    return (v1 | a3) & 0xffffff00 | (int64_t)(v3 == (char)a3) | (int64_t)(v3 == 0);
}