uint operand_matches(long param_1,long param_2,uint param_3)

{
  char *pcVar1;
  char cVar2;
  long lVar3;
  
  lVar3 = 0;
  do {
    pcVar1 = (char *)(param_2 + lVar3);
    cVar2 = *(char *)(param_1 + lVar3);
    if (*pcVar1 == '\0') {
      return (uint)lVar3 & 0xffffff00 | (uint)((char)param_3 == cVar2) |
             param_3 & 0xffffff00 | (uint)(cVar2 == '\0');
    }
    lVar3 = lVar3 + 1;
  } while (*pcVar1 == cVar2);
  return 0;
}