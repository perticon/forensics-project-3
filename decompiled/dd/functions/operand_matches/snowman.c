uint32_t operand_matches(void** rdi, void** rsi, signed char dl, void** rcx) {
    void* rax5;
    uint32_t ecx6;
    uint32_t r8d7;
    uint32_t edx8;

    *reinterpret_cast<uint32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    do {
        ecx6 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<uint64_t>(rax5));
        r8d7 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(rax5));
        if (!*reinterpret_cast<signed char*>(&ecx6)) 
            break;
        rax5 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax5) + 1);
    } while (*reinterpret_cast<signed char*>(&ecx6) == *reinterpret_cast<signed char*>(&r8d7));
    goto addr_4a10_4;
    *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(dl == *reinterpret_cast<signed char*>(&r8d7));
    *reinterpret_cast<unsigned char*>(&edx8) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&r8d7) == 0);
    return *reinterpret_cast<uint32_t*>(&rax5) | edx8;
    addr_4a10_4:
    return 0;
}