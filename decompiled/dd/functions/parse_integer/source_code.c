parse_integer (char const *str, strtol_error *invalid)
{
  /* Call xstrtoumax, not xstrtoimax, since we don't want to
     allow strings like " -0".  Initialize N to an interminate value;
     calling code should not rely on this function returning 0
     when *INVALID represents a non-overflow error.  */
  int indeterminate = 0;
  uintmax_t n = indeterminate;
  char *suffix;
  static char const suffixes[] = "bcEGkKMPTwYZ0";
  strtol_error e = xstrtoumax (str, &suffix, 10, &n, suffixes);
  intmax_t result;

  if ((e & ~LONGINT_OVERFLOW) == LONGINT_INVALID_SUFFIX_CHAR
      && suffix[-1] != 'B' && *suffix == 'B')
    {
      suffix++;
      if (!*suffix)
        e &= ~LONGINT_INVALID_SUFFIX_CHAR;
    }

  if ((e & ~LONGINT_OVERFLOW) == LONGINT_INVALID_SUFFIX_CHAR
      && *suffix == 'x' && ! (suffix[-1] == 'B' && strchr (suffix + 1, 'B')))
    {
      uintmax_t o;
      strtol_error f = xstrtoumax (suffix + 1, &suffix, 10, &o, suffixes);
      if ((f & ~LONGINT_OVERFLOW) != LONGINT_OK)
        {
          e = f;
          result = indeterminate;
        }
      else if (INT_MULTIPLY_WRAPV (n, o, &result)
               || (result != 0 && ((e | f) & LONGINT_OVERFLOW)))
        {
          e = LONGINT_OVERFLOW;
          result = INTMAX_MAX;
        }
      else
        {
          if (result == 0 && STRPREFIX (str, "0x"))
            error (0, 0,
                   _("warning: %s is a zero multiplier; "
                     "use %s if that is intended"),
                   quote_n (0, "0x"), quote_n (1, "00x"));
          e = LONGINT_OK;
        }
    }
  else if (n <= INTMAX_MAX)
    result = n;
  else
    {
      e = LONGINT_OVERFLOW;
      result = INTMAX_MAX;
    }

  *invalid = e;
  return result;
}