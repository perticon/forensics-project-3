void translate_charset(char * new_trans) {
    int64_t v1 = (int64_t)&trans_table; // 0x498e
    char * v2 = (char *)v1; // 0x4990
    v1++;
    *v2 = *(char *)((int64_t)*v2 + (int64_t)new_trans);
    while (v1 != (int64_t)&iread_fnc) {
        // 0x4990
        v2 = (char *)v1;
        v1++;
        *v2 = *(char *)((int64_t)*v2 + (int64_t)new_trans);
    }
    // 0x49a3
    *(char *)&translation_needed = 1;
}