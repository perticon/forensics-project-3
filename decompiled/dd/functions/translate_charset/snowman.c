void translate_charset(int64_t rdi) {
    unsigned char* rax2;
    int64_t rdx3;
    uint32_t edx4;

    rax2 = reinterpret_cast<unsigned char*>(0x14160);
    do {
        *reinterpret_cast<uint32_t*>(&rdx3) = *rax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx3) + 4) = 0;
        ++rax2;
        edx4 = *reinterpret_cast<unsigned char*>(rdi + rdx3);
        *reinterpret_cast<signed char*>(rax2 - 1) = *reinterpret_cast<signed char*>(&edx4);
    } while (!reinterpret_cast<int1_t>(rax2 == 0x14260));
    translation_needed = 1;
    return;
}