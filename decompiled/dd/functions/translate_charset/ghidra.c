void translate_charset(long param_1)

{
  undefined8 *puVar1;
  undefined8 *puVar2;
  
  puVar1 = (undefined8 *)trans_table;
  do {
    puVar2 = (undefined8 *)((long)puVar1 + 1);
    *(undefined *)puVar1 = *(undefined *)(param_1 + (ulong)*(byte *)puVar1);
    puVar1 = puVar2;
  } while (puVar2 != &iread_fnc);
  translation_needed = 1;
  return;
}