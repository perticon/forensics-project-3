void nl_error(uint32_t edi, void** esi, void** rdx, void** rcx, void** r8, void** r9) {
    signed char al7;
    void* rax8;
    int32_t eax9;
    void** rdi10;
    void** rax11;
    void* rsi12;
    void* rax13;
    void** rdx14;
    uint32_t r13d15;
    void* r12_16;
    void* r15_17;
    uint32_t r14d18;
    void** rsi19;
    void** rax20;
    void* r12_21;
    void* rcx22;
    void* rax23;
    void* rax24;
    void* rax25;
    int64_t v26;
    int64_t rdi27;
    void** rax28;

    if (al7) {
        __asm__("movaps [rsp+0x60], xmm0");
        __asm__("movaps [rsp+0x70], xmm1");
        __asm__("movaps [rsp+0x80], xmm2");
        __asm__("movaps [rsp+0x90], xmm3");
        __asm__("movaps [rsp+0xa0], xmm4");
        __asm__("movaps [rsp+0xb0], xmm5");
        __asm__("movaps [rsp+0xc0], xmm6");
        __asm__("movaps [rsp+0xd0], xmm7");
    }
    rax8 = g28;
    eax9 = progress_len;
    if (!(reinterpret_cast<uint1_t>(eax9 < 0) | reinterpret_cast<uint1_t>(eax9 == 0))) {
        rdi10 = stderr;
        rax11 = *reinterpret_cast<void***>(rdi10 + 40);
        if (reinterpret_cast<unsigned char>(rax11) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi10 + 48))) {
            fun_26e0();
        } else {
            *reinterpret_cast<void***>(rdi10 + 40) = rax11 + 1;
            *reinterpret_cast<void***>(rax11) = reinterpret_cast<void**>(10);
        }
        progress_len = 0;
    }
    *reinterpret_cast<void***>(&rsi12) = esi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi12) + 4) = 0;
    verror();
    rax13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax8) - reinterpret_cast<uint64_t>(g28));
    if (!rax13) {
        return;
    }
    fun_2690();
    rdx14 = reinterpret_cast<void**>(0x14140);
    r13d15 = o_nocache_eof;
    if (!edi) 
        goto addr_4b56_12;
    addr_4b65_13:
    r12_16 = *rdx14;
    if (!rsi12) {
        if (r12_16 || *reinterpret_cast<signed char*>(&r13d15)) {
            *reinterpret_cast<int32_t*>(&r15_17) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_17) + 4) = 0;
            if (!edi) {
                addr_4c30_16:
                r14d18 = input_seekable;
                rsi19 = input_offset;
                if (!*reinterpret_cast<signed char*>(&r14d18)) {
                    rax20 = fun_2570();
                    *reinterpret_cast<void***>(rax20) = reinterpret_cast<void**>(29);
                    goto addr_4b9f_18;
                }
            } else {
                rsi19 = output_offset_2;
                if (rsi19 == 0xffffffffffffffff) 
                    goto addr_4b9f_18; else 
                    goto addr_4bd9_20;
            }
        } else {
            addr_4b99_21:
            goto addr_4b9f_18;
        }
    } else {
        r12_21 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_16) + reinterpret_cast<uint64_t>(rsi12));
        if (__intrinsic()) {
            *rdx14 = reinterpret_cast<void*>(0x1ffff);
            r12_16 = reinterpret_cast<void*>(0x7ffffffffffe0000);
        } else {
            rcx22 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) >> 63) >> 47);
            rax23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_21) + reinterpret_cast<uint64_t>(rcx22));
            *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<uint32_t*>(&rax23) & 0x1ffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax24) + 4) = 0;
            rax25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rax24) - reinterpret_cast<uint64_t>(rcx22));
            *rdx14 = rax25;
            if (reinterpret_cast<int64_t>(r12_21) <= reinterpret_cast<int64_t>(rax25)) 
                goto addr_4b99_21;
            r12_16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r12_21) - reinterpret_cast<uint64_t>(rax25));
            if (!r12_16) 
                goto addr_4b99_21;
        }
        if (!edi) 
            goto addr_4c30_16;
        rsi19 = output_offset_2;
        r15_17 = o_pending_0;
        if (reinterpret_cast<int1_t>(rsi19 == 0xffffffffffffffff)) {
            goto addr_4b9f_18;
        }
    }
    addr_4bf5_29:
    if (reinterpret_cast<signed char>(rsi19) < reinterpret_cast<signed char>(0)) {
        addr_4b9f_18:
        goto v26;
    } else {
        addr_4bfd_30:
        if (rsi12 || !r12_16) {
            if (r12_16) {
                addr_4c12_32:
                *reinterpret_cast<uint32_t*>(&rdi27) = edi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi27) + 4) = 0;
                fun_2750(rdi27);
                goto addr_4b9f_18;
            } else {
                addr_4c60_33:
                goto addr_4c12_32;
            }
        } else {
            if (*reinterpret_cast<signed char*>(&r13d15)) 
                goto addr_4c60_33;
            goto addr_4c12_32;
        }
    }
    addr_4bd9_20:
    if (reinterpret_cast<signed char>(rsi19) < reinterpret_cast<signed char>(0)) {
        rax28 = fun_2710();
        output_offset_2 = rax28;
        rsi19 = rax28;
        goto addr_4bf5_29;
    } else {
        if (!rsi12) 
            goto addr_4bfd_30;
        rsi19 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi19) + (reinterpret_cast<int64_t>(r15_17) + reinterpret_cast<uint64_t>(r12_16)));
        output_offset_2 = rsi19;
        goto addr_4bf5_29;
    }
    addr_4b56_12:
    r13d15 = i_nocache_eof;
    rdx14 = reinterpret_cast<void**>(0x14148);
    goto addr_4b65_13;
}