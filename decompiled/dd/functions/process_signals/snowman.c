void process_signals(void** rdi, void** rsi, ...) {
    void** rsp3;
    void* rax4;
    void* v5;
    uint32_t eax6;
    int32_t eax7;
    int64_t r12_8;
    int32_t eax9;
    void* rsp10;
    uint32_t eax11;
    int32_t eax12;
    void** rcx13;
    void** r8_14;
    void** r9_15;
    void* rax16;
    uint32_t r14d17;
    void** r13_18;
    int32_t ebx19;
    int32_t ecx20;
    int64_t rcx21;
    int32_t eax22;
    void** rax23;
    int64_t v24;

    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 0x90);
    rax4 = g28;
    v5 = rax4;
    while ((eax6 = interrupt_signal, !!eax6) || (eax7 = info_signal_count, !!eax7)) {
        do {
            fun_2520();
            *reinterpret_cast<uint32_t*>(&r12_8) = interrupt_signal;
            eax9 = info_signal_count;
            if (eax9) {
                info_signal_count = eax9 - 1;
            }
            rsi = rsp3;
            *reinterpret_cast<uint32_t*>(&rdi) = 2;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            fun_2520();
            rsp10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8 - 8 + 8);
            if (*reinterpret_cast<uint32_t*>(&r12_8)) 
                break;
            print_stats();
            rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10) - 8 + 8);
            eax11 = interrupt_signal;
        } while (eax11 || (eax12 = info_signal_count, !!eax12));
        break;
        cleanup(2, rsi, 0, rcx13, r8_14, r9_15);
        print_stats();
        *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&r12_8);
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        fun_2540();
        rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp10) - 8 + 8 - 8 + 8 - 8 + 8);
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v5) - reinterpret_cast<uint64_t>(g28));
    if (rax16) {
        fun_2690();
        r14d17 = *reinterpret_cast<uint32_t*>(&rdi);
        r13_18 = rsi;
        ebx19 = ecx20;
        do {
            process_signals(rdi, rsi, rdi, rsi);
            *reinterpret_cast<int32_t*>(&rcx21) = ebx19;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx21) + 4) = 0;
            rsi = r13_18;
            *reinterpret_cast<uint32_t*>(&rdi) = r14d17;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax22 = fd_reopen(rdi, rsi, 0, rcx21);
            if (eax22 >= 0) 
                break;
            rax23 = fun_2570();
        } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 4));
        goto v24;
    } else {
        return;
    }
}