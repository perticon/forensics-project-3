void process_signals(void) {
    int64_t v1 = __readfsqword(40); // 0x5a72
    if ((info_signal_count || interrupt_signal) != 0) {
        function_2520();
        if (info_signal_count != 0) {
            // 0x5abd
            info_signal_count--;
        }
        // 0x5ac6
        function_2520();
        while (interrupt_signal == 0) {
            // 0x5ada
            print_stats();
            if (interrupt_signal == 0) {
                // 0x5ae9
                if (info_signal_count == 0) {
                    // break (via goto) -> 0x5af3
                    goto lab_0x5af3;
                }
            }
            function_2520();
            if (info_signal_count != 0) {
                // 0x5abd
                info_signal_count--;
            }
            // 0x5ac6
            function_2520();
        }
        // 0x5b18
        cleanup();
        print_stats();
        function_2540();
        while ((info_signal_count || interrupt_signal) != 0) {
            function_2520();
            if (info_signal_count != 0) {
                // 0x5abd
                info_signal_count--;
            }
            // 0x5ac6
            function_2520();
            while (interrupt_signal == 0) {
                // 0x5ada
                print_stats();
                if (interrupt_signal == 0) {
                    // 0x5ae9
                    if (info_signal_count == 0) {
                        // break (via goto) -> 0x5af3
                        goto lab_0x5af3;
                    }
                }
                function_2520();
                if (info_signal_count != 0) {
                    // 0x5abd
                    info_signal_count--;
                }
                // 0x5ac6
                function_2520();
            }
            // 0x5b18
            cleanup();
            print_stats();
            function_2540();
        }
    }
  lab_0x5af3:
    // 0x5af3
    if (v1 == __readfsqword(40)) {
        // 0x5b06
        return;
    }
    // 0x5b2f
    function_2690();
}