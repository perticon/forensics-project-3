void process_signals(void)

{
  int __sig;
  EVP_PKEY_CTX *ctx;
  long in_FS_OFFSET;
  sigset_t sStack168;
  long local_20;
  
  local_20 = *(long *)(in_FS_OFFSET + 0x28);
  while ((interrupt_signal != 0 || (info_signal_count != 0))) {
    while( true ) {
      sigprocmask(0,(sigset_t *)caught_signals,&sStack168);
      __sig = interrupt_signal;
      if (info_signal_count != 0) {
        info_signal_count = info_signal_count + -1;
      }
      ctx = (EVP_PKEY_CTX *)0x2;
      sigprocmask(2,&sStack168,(sigset_t *)0x0);
      if (__sig != 0) break;
      print_stats();
      if ((interrupt_signal == 0) && (info_signal_count == 0)) goto LAB_00105af3;
    }
    cleanup(ctx);
    print_stats();
    raise(__sig);
  }
LAB_00105af3:
  if (local_20 == *(long *)(in_FS_OFFSET + 0x28)) {
    return;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}