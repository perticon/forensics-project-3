swab_buffer (char *buf, idx_t *nread, int *saved_byte)
{
  if (*nread == 0)
    return buf;

  /* Update *SAVED_BYTE, and set PREV_SAVED to its old value.  */
  int prev_saved = *saved_byte;
  if ((prev_saved < 0) == (*nread & 1))
    {
      unsigned char c = buf[--*nread];
      *saved_byte = c;
    }
  else
    *saved_byte = -1;

  /* Do the byte-swapping by moving every other byte two
     positions toward the end, working from the end of the buffer
     toward the beginning.  This way we move only half the data.  */
  for (idx_t i = *nread; 1 < i; i -= 2)
    buf[i] = buf[i - 2];

  if (prev_saved < 0)
    return buf + 1;

  buf[1] = prev_saved;
  ++*nread;
  return buf;
}