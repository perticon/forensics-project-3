void set_fd_flags(int32_t fd, int32_t add_flags, char * name) {
    int64_t v1 = __readfsqword(40); // 0x5bdf
    int32_t v2 = add_flags & -0x20101; // 0x5bf2
    int64_t v3; // 0x5bd0
    int64_t v4; // 0x5bd0
    int64_t v5; // 0x5bd0
    int64_t v6; // 0x5bd0
    if (v2 != 0) {
        int32_t v7 = rpl_fcntl(fd, 3); // 0x5c29
        uint32_t v8 = v7 | v2; // 0x5c31
        int64_t v9 = v8; // 0x5c31
        v5 = v9;
        if (v7 < 0) {
            goto lab_0x5c83;
        } else {
            if (v7 == v8) {
                goto lab_0x5bfa;
            } else {
                // 0x5c3b
                v4 = v9;
                if ((v8 & (int32_t)"ftware: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n") != 0) {
                    // 0x5c5a
                    int64_t v10; // bp-200, 0x5bd0
                    int32_t v11 = ifstat(fd, (struct stat *)&v10); // 0x5c5f
                    v5 = v9;
                    if (v11 != 0) {
                        goto lab_0x5c83;
                    } else {
                        // 0x5c68
                        v6 = v9;
                        v3 = v7;
                        int32_t v12; // 0x5bd0
                        if ((v12 & (int32_t)"yte) blocks") == 0x4000) {
                            goto lab_0x5cc2;
                        } else {
                            // 0x5c78
                            *(int32_t *)function_2570() = 20;
                            v5 = v9;
                            goto lab_0x5c83;
                        }
                    }
                } else {
                    goto lab_0x5c43;
                }
            }
        }
    } else {
        goto lab_0x5bfa;
    }
  lab_0x5bfa:
    // 0x5bfa
    if (v1 != __readfsqword(40)) {
        // 0x5cd6
        function_2690();
        return;
    }
  lab_0x5c83:;
    int64_t v13 = quotearg_style(); // 0x5c8b
    int64_t v14 = function_2660(); // 0x5ca1
    int32_t v15 = *(int32_t *)function_2570(); // 0x5cb9
    nl_error(1, v15, (char *)v14);
    v6 = v5;
    v3 = v13;
    goto lab_0x5cc2;
  lab_0x5cc2:;
    int64_t v16 = v6 & 0xfffeffff; // 0x5cc2
    v4 = v16;
    if ((int32_t)v16 == (int32_t)v3) {
        goto lab_0x5bfa;
    } else {
        goto lab_0x5c43;
    }
  lab_0x5c43:
    // 0x5c43
    v5 = v4;
    if (rpl_fcntl(fd, 4) != -1) {
        goto lab_0x5bfa;
    } else {
        goto lab_0x5c83;
    }
}