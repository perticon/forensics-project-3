void set_fd_flags(ulong param_1,uint param_2,undefined8 param_3)

{
  uint uVar1;
  int iVar2;
  ulong uVar3;
  int *piVar4;
  uint uVar5;
  long in_FS_OFFSET;
  undefined auStack200 [24];
  uint local_b0;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  if ((param_2 & 0xfffdfeff) != 0) {
    uVar1 = rpl_fcntl(param_1,3);
    uVar3 = (ulong)uVar1;
    uVar5 = param_2 & 0xfffdfeff | uVar1;
    if ((int)uVar1 < 0) goto LAB_00105c83;
    if (uVar1 != uVar5) {
      if ((uVar5 & 0x10000) != 0) {
        iVar2 = ifstat(param_1 & 0xffffffff,auStack200);
        if (iVar2 != 0) goto LAB_00105c83;
        if ((local_b0 & 0xf000) == 0x4000) goto LAB_00105cc2;
        piVar4 = __errno_location();
        *piVar4 = 0x14;
        goto LAB_00105c83;
      }
      do {
        iVar2 = rpl_fcntl(param_1 & 0xffffffff,4,uVar5);
        if (iVar2 != -1) break;
LAB_00105c83:
        uVar3 = quotearg_style(4,param_3);
        param_3 = dcgettext(0,"setting flags for %s",5);
        piVar4 = __errno_location();
        nl_error(1,*piVar4,param_3,uVar3);
LAB_00105cc2:
        uVar5 = uVar5 & 0xfffeffff;
      } while (uVar5 != (uint)uVar3);
    }
  }
  if (local_30 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}