void print_xfer_stats(long param_1)

{
  int iVar1;
  char *__s;
  char *pcVar2;
  char *__s_00;
  size_t sVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  long lVar6;
  char *pcVar7;
  long in_FS_OFFSET;
  char *pcVar8;
  undefined local_808 [654];
  undefined local_57a [654];
  char local_2ec [660];
  char local_58 [24];
  long local_40;
  
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  lVar6 = param_1;
  if (param_1 == 0) {
    lVar6 = gethrxtime();
  }
  __s = (char *)human_readable(w_bytes,local_808,0x1d1,1,1);
  pcVar2 = (char *)human_readable(w_bytes,local_57a,0x1f1,1,1);
  if (start_time < lVar6) {
    __s_00 = (char *)human_readable(w_bytes,local_2ec,0x1d1,1000000000);
    sVar3 = strlen(__s_00);
    *(undefined2 *)(__s_00 + sVar3) = (undefined2)slash_s_5;
    __s_00[sVar3 + 2] = slash_s_5._2_1_;
  }
  else {
    uVar5 = dcgettext(0,"Infinity",5);
    __s_00 = local_2ec;
    __snprintf_chk(__s_00,0x28e,1,0x28e,"%s B/s",uVar5);
  }
  pcVar7 = "%g s";
  if (param_1 != 0) {
    pcVar8 = stderr->_IO_write_ptr;
    if (pcVar8 < stderr->_IO_write_end) {
      pcVar7 = "%.0f s";
      stderr->_IO_write_ptr = pcVar8 + 1;
      *pcVar8 = '\r';
    }
    else {
      __overflow(stderr,0xd);
      pcVar7 = "%.0f s";
    }
  }
  __snprintf_chk(local_58,0x18,1,0x18,pcVar7);
  sVar3 = strlen(__s);
  uVar5 = w_bytes;
  if (__s[sVar3 - 2] == ' ') {
    uVar4 = dcngettext(0,"%ld byte copied, %s, %s","%ld bytes copied, %s, %s",w_bytes,5);
    iVar1 = __fprintf_chk(stderr,1,uVar4,uVar5,local_58,__s_00);
  }
  else {
    sVar3 = strlen(pcVar2);
    uVar5 = w_bytes;
    if (pcVar2[sVar3 - 2] == ' ') {
      pcVar8 = (char *)0x1056fe;
      uVar4 = dcgettext(0,"%ld bytes (%s) copied, %s, %s",5);
      pcVar2 = local_58;
      pcVar7 = __s_00;
    }
    else {
      uVar4 = dcgettext(0,"%ld bytes (%s, %s) copied, %s, %s",5);
      pcVar7 = local_58;
      pcVar8 = __s_00;
    }
    iVar1 = __fprintf_chk(stderr,1,uVar4,uVar5,__s,pcVar2,pcVar7,pcVar8);
  }
  if (param_1 == 0) {
    pcVar2 = stderr->_IO_write_ptr;
    if (pcVar2 < stderr->_IO_write_end) {
      stderr->_IO_write_ptr = pcVar2 + 1;
      *pcVar2 = '\n';
      iVar1 = progress_len;
    }
    else {
      __overflow(stderr,10);
      iVar1 = progress_len;
    }
  }
  else if ((-1 < iVar1) && (iVar1 < progress_len)) {
    __fprintf_chk(stderr,1,&DAT_0010f224,progress_len - iVar1,"");
  }
  progress_len = iVar1;
  reported_w_bytes = w_bytes;
  if (local_40 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}