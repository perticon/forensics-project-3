void print_xfer_stats(int64_t progress_time) {
    char hbuf[3][654]; // bp-2064, 0x5490
    char v1[654]; // 0x554e
    char v2[3][654]; // 0x554e
    char v3[654]; // 0x57c5
    char v4[3][654]; // 0x57c5
    // 0x5490
    int128_t v5; // 0x5490
    int128_t v6 = v5;
    int64_t v7 = __readfsqword(40); // 0x54a7
    int64_t v8 = progress_time; // 0x54bd
    if (progress_time == 0) {
        // 0x5738
        v8 = gethrxtime();
    }
    // 0x54c3
    int64_t v9; // bp-2056, 0x5490
    char * v10 = human_readable(w_bytes, (char *)&v9, 465, 1, 1); // 0x54df
    int64_t v11; // bp-1402, 0x5490
    human_readable(w_bytes, (char *)&v11, 497, 1, 1);
    int128_t v12; // 0x5490
    if (v8 > start_time) {
        int64_t v13 = v8 - start_time; // 0x5535
        __asm_pxor(v6, v6);
        int64_t v14 = __asm_movsd(__asm_divsd(__asm_cvtsi2sd(v13), 0x41cdcd6500000000)); // 0x554e
        v1[0] = v14;
        v2[0] = v1;
        hbuf = v2;
        int64_t v15; // bp-748, 0x5490
        char * v16 = human_readable(w_bytes, (char *)&v15, 465, 0x3b9aca00, v13); // 0x5554
        int64_t v17 = function_2680(); // 0x555f
        int128_t v18 = __asm_movsd_1(*(int64_t *)&hbuf); // 0x556b
        int64_t v19 = v17 + (int64_t)v16;
        *(int16_t *)v19 = 0x732f;
        *(char *)(v19 + 2) = 0;
        v12 = v18;
    } else {
        // 0x56a0
        function_2660();
        function_2530();
        v12 = __asm_pxor(v6, v6);
    }
    if (progress_time != 0) {
        int64_t v20 = (int64_t)g17; // 0x558d
        int64_t * v21 = (int64_t *)(v20 + 40); // 0x5594
        uint64_t v22 = *v21; // 0x5594
        if (v22 >= *(int64_t *)(v20 + 48)) {
            int64_t v23 = __asm_movsd(v12); // 0x57c5
            v3[0] = v23;
            v4[0] = v3;
            hbuf = v4;
            function_26e0();
            __asm_movsd_1(*(int64_t *)&hbuf);
        } else {
            // 0x55a2
            *v21 = v22 + 1;
            *(char *)v22 = 13;
        }
    }
    // 0x55b4
    function_2530();
    int64_t v24; // 0x5490
    if (*(char *)((int64_t)v10 - 2 + function_2680()) == 32) {
        // 0x5748
        function_28c0();
        v24 = function_2930();
    } else {
        // 0x55ec
        function_2680();
        function_2660();
        v24 = function_2930();
    }
    if (progress_time == 0) {
        int64_t v25 = (int64_t)g17; // 0x5710
        int64_t * v26 = (int64_t *)(v25 + 40); // 0x5717
        uint64_t v27 = *v26; // 0x5717
        if (v27 >= *(int64_t *)(v25 + 48)) {
            // 0x57e8
            function_26e0();
        } else {
            // 0x5725
            *v26 = v27 + 1;
            *(char *)v27 = 10;
        }
    } else {
        int32_t v28 = v24; // 0x5649
        if (v28 >= 0) {
            // 0x564d
            if ((v24 & 0xffffffff) < (int64_t)progress_len) {
                // 0x5798
                function_2930();
            }
        }
        // 0x565b
        progress_len = v28;
    }
    // 0x5661
    reported_w_bytes = w_bytes;
    if (v7 == __readfsqword(40)) {
        // 0x5686
        return;
    }
    // 0x57f7
    function_2690();
}