void print_xfer_stats() {
    void** rbp1;
    void* rsp2;
    void* rax3;
    void** rdx4;
    void** rcx5;
    void** r8_6;
    void** r9_7;
    void** rax8;
    void** rdi9;
    void** rax10;
    void* rsp11;
    void** rdi12;
    void** rax13;
    void* rsp14;
    void** rax15;
    void** rax16;
    void* rsp17;
    void** v18;
    void** v19;
    void** v20;
    void* rsp21;
    void** rdi22;
    void** v23;
    void** rax24;
    void** rax25;
    uint32_t edx26;
    void** v27;
    uint32_t edx28;
    int64_t r8_29;
    void** rdi30;
    void** rax31;
    void** v32;
    void** v33;
    void** v34;
    void** rax35;
    void** rax36;
    void** rdi37;
    void** rax38;
    int64_t rbp39;
    void** rax40;
    void** rax41;
    void** rdx42;
    void** rax43;
    void** rdi44;
    void** rax45;
    void** rdi46;
    void** rax47;
    int32_t ecx48;
    void** rdi49;
    void** rax50;
    void* rax51;
    int1_t zf52;
    int32_t ecx53;
    void** rdi54;
    void** rax55;
    void** r12_56;
    void** rbp57;
    void** rbx58;
    void** rax59;
    void** rdi60;
    void** r12_61;
    void** rax62;
    void** rdi63;
    int1_t zf64;
    int64_t v65;
    int64_t v66;

    while (1) {
        rbp1 = reinterpret_cast<void**>(0);
        rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x7e8);
        rax3 = g28;
        if (1) {
            rax8 = gethrxtime(0, 1, rdx4, rcx5, r8_6, r9_7);
            rsp2 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
            rbp1 = rax8;
        }
        rdi9 = w_bytes;
        rax10 = human_readable(rdi9, reinterpret_cast<int64_t>(rsp2) + 16, 0x1d1, 1, 1, r9_7);
        rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp2) - 8 + 8);
        rdi12 = w_bytes;
        rax13 = human_readable(rdi12, reinterpret_cast<int64_t>(rsp11) + 0x29e, 0x1f1, 1, 1, r9_7);
        rsp14 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) - 8 + 8);
        rax15 = start_time;
        if (reinterpret_cast<signed char>(rax15) >= reinterpret_cast<signed char>(rbp1)) {
            rax16 = fun_2660();
            rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8);
            r9_7 = rax16;
            fun_2530(reinterpret_cast<int64_t>(rsp17) + 0x52c, 0x28e, 1, 0x28e, "%s B/s", r9_7, v18, v19, v20);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
            __asm__("pxor xmm0, xmm0");
        } else {
            rdi22 = w_bytes;
            __asm__("pxor xmm0, xmm0");
            __asm__("cvtsi2sd xmm0, r8");
            __asm__("divsd xmm0, [rip+0xa4e2]");
            *reinterpret_cast<void***>(rdi22) = v23;
            rax24 = human_readable(rdi22 + 4, reinterpret_cast<int64_t>(rsp14) + 0x52c + 4, 0x1d1, 0x3b9aca00, reinterpret_cast<unsigned char>(rbp1) - reinterpret_cast<unsigned char>(rax15), r9_7);
            rax25 = fun_2680(rax24, rax24);
            rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp14) - 8 + 8 - 8 + 8);
            edx26 = slash_s_5;
            *reinterpret_cast<void***>(rax24) = v27;
            *reinterpret_cast<int16_t*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(rax25)) = *reinterpret_cast<int16_t*>(&edx26);
            edx28 = gf490;
            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(rax25) + 2) = *reinterpret_cast<signed char*>(&edx28);
        }
        r8_29 = reinterpret_cast<int64_t>("%g s");
        if (!1) {
            rdi30 = stderr;
            rax31 = *reinterpret_cast<void***>(rdi30 + 40);
            if (reinterpret_cast<unsigned char>(rax31) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi30 + 48))) {
                *reinterpret_cast<void***>(rdi30) = gd;
                fun_26e0();
                rsp21 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp21) - 8 + 8);
                *reinterpret_cast<int32_t*>(rdi30 + 4) = g11;
                r8_29 = reinterpret_cast<int64_t>("%.0f s");
            } else {
                r8_29 = reinterpret_cast<int64_t>("%.0f s");
                *reinterpret_cast<void***>(rdi30 + 40) = rax31 + 1;
                *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(13);
            }
        }
        fun_2530(reinterpret_cast<int64_t>(rsp21) + 0x7c0, 24, 1, 24, r8_29, r9_7, v32, v33, v34);
        rax35 = fun_2680(rax10, rax10);
        if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax10) + reinterpret_cast<unsigned char>(rax35) + 0xfffffffffffffffe) == 32) {
            rax36 = fun_28c0();
            rdi37 = stderr;
            rax38 = fun_2930(rdi37, 1, rax36, rdi37, 1, rax36);
            *reinterpret_cast<int32_t*>(&rbp39) = *reinterpret_cast<int32_t*>(&rax38);
        } else {
            rax40 = fun_2680(rax13, rax13);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<unsigned char>(rax40) + 0xfffffffffffffffe) == 32) {
                rax41 = fun_2660();
                rdx42 = rax41;
            } else {
                rax43 = fun_2660();
                rdx42 = rax43;
            }
            rdi44 = stderr;
            rax45 = fun_2930(rdi44, 1, rdx42, rdi44, 1, rdx42);
            *reinterpret_cast<int32_t*>(&rbp39) = *reinterpret_cast<int32_t*>(&rax45);
        }
        if (1) {
            rdi46 = stderr;
            rax47 = *reinterpret_cast<void***>(rdi46 + 40);
            if (reinterpret_cast<unsigned char>(rax47) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi46 + 48))) {
                fun_26e0();
            } else {
                *reinterpret_cast<void***>(rdi46 + 40) = rax47 + 1;
                *reinterpret_cast<void***>(rax47) = reinterpret_cast<void**>(10);
            }
        } else {
            if (*reinterpret_cast<int32_t*>(&rbp39) >= 0 && (ecx48 = progress_len, ecx48 > *reinterpret_cast<int32_t*>(&rbp39))) {
                rdi49 = stderr;
                fun_2930(rdi49, 1, "%*s");
            }
            progress_len = *reinterpret_cast<int32_t*>(&rbp39);
        }
        rax50 = w_bytes;
        reported_w_bytes = rax50;
        rax51 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax3) - reinterpret_cast<uint64_t>(g28));
        if (!rax51) 
            break;
        fun_2690();
        zf52 = status_level == 1;
        if (zf52) 
            goto addr_5920_38;
        ecx53 = progress_len;
        if (!(reinterpret_cast<uint1_t>(ecx53 < 0) | reinterpret_cast<uint1_t>(ecx53 == 0))) {
            rdi54 = stderr;
            rax55 = *reinterpret_cast<void***>(rdi54 + 40);
            if (reinterpret_cast<unsigned char>(rax55) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi54 + 48))) {
                fun_26e0();
            } else {
                *reinterpret_cast<void***>(rdi54 + 40) = rax55 + 1;
                *reinterpret_cast<void***>(rax55) = reinterpret_cast<void**>(10);
            }
            progress_len = 0;
        }
        r12_56 = r_full;
        rbp57 = w_full;
        rbx58 = r_partial;
        rax59 = fun_2660();
        rcx5 = r12_56;
        r9_7 = rbp57;
        rdi60 = stderr;
        r8_6 = rbx58;
        fun_2930(rdi60, 1, rax59, rdi60, 1, rax59);
        r12_61 = r_truncate;
        rdx4 = reinterpret_cast<void**>(0x587a);
        if (r12_61) {
            *reinterpret_cast<int32_t*>(&r8_6) = 5;
            *reinterpret_cast<int32_t*>(&r8_6 + 4) = 0;
            rax62 = fun_28c0();
            rdi63 = stderr;
            rcx5 = r12_61;
            rdx4 = rax62;
            fun_2930(rdi63, 1, rdx4, rdi63, 1, rdx4);
        }
        zf64 = status_level == 2;
        if (zf64) 
            goto addr_58d0_47;
    }
    return;
    addr_5920_38:
    goto v65;
    addr_58d0_47:
    goto v66;
}