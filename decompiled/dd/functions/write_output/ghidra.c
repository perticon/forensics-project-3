void write_output(void)

{
  long lVar1;
  char *pcVar2;
  long lVar3;
  undefined8 uVar4;
  undefined8 uVar5;
  uint *puVar6;
  char *pcVar7;
  ulong uVar8;
  
  lVar3 = iwrite_constprop_0(obuf,output_blocksize);
  w_bytes = w_bytes + lVar3;
  if (output_blocksize == lVar3) {
    w_full = w_full + 1;
    oc = 0;
    return;
  }
  uVar4 = quotearg_style(4,output_file);
  uVar5 = dcgettext(0,"writing to %s",5);
  puVar6 = (uint *)__errno_location();
  pcVar7 = (char *)0x0;
  uVar8 = (ulong)*puVar6;
  nl_error(0,uVar8,uVar5,uVar4);
  if (lVar3 != 0) {
    w_partial = w_partial + 1;
  }
  quit_constprop_0();
  pcVar2 = pcVar7 + uVar8;
  if (uVar8 != 0) {
    do {
      while (lVar3 = col, *pcVar7 == newline_character) {
        if (col < conversion_blocksize) {
          do {
            lVar1 = oc + 1;
            *(undefined *)(obuf + oc) = space_character;
            oc = lVar1;
            if (output_blocksize <= lVar1) {
              write_output();
            }
            lVar3 = lVar3 + 1;
          } while (lVar3 < conversion_blocksize);
        }
        pcVar7 = pcVar7 + 1;
        col = 0;
        if (pcVar7 == pcVar2) {
          col = 0;
          return;
        }
      }
      if (conversion_blocksize == col) {
        r_truncate = r_truncate + 1;
      }
      else if (col < conversion_blocksize) {
        lVar1 = oc + 1;
        *(char *)(obuf + oc) = *pcVar7;
        oc = lVar1;
        if (output_blocksize <= lVar1) {
          write_output();
          lVar3 = col;
        }
      }
      col = lVar3 + 1;
      pcVar7 = pcVar7 + 1;
    } while (pcVar7 != pcVar2);
  }
  return;
}