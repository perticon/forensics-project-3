void write_output(void) {
    int64_t v1 = iwrite_constprop_0(obuf, output_blocksize); // 0x6223
    w_bytes += v1;
    if (output_blocksize == v1) {
        // 0x6238
        oc = 0;
        w_full++;
        return;
    }
    // 0x6251
    quotearg_style();
    int64_t v2 = function_2660(); // 0x6276
    int32_t v3 = *(int32_t *)function_2570(); // 0x628b
    nl_error(0, v3, (char *)v2);
    if (v1 != 0) {
        // 0x6299
        w_partial++;
    }
    // 0x62a1
    quit_constprop_0();
}