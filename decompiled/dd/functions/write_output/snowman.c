void write_output(void** rdi, ...) {
    void** rsi2;
    void** rdi3;
    int64_t v4;
    int64_t rbx5;
    void** rdx6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** rax10;
    void** tmp64_11;
    int1_t zf12;
    void** rsi13;
    void** rax14;
    void** rax15;
    void** rax16;
    void** rsi17;
    void** r8_18;
    void** r9_19;
    void** tmp64_20;
    void** r8_21;
    void** r9_22;
    void** r12_23;
    void** rbp24;
    uint32_t eax25;
    int1_t zf26;
    int64_t rbx27;
    int64_t rdx28;
    void** tmp64_29;
    void** rcx30;
    void** rsi31;
    void** rdx32;
    int1_t less33;
    void** rdx34;
    void** rcx35;
    uint32_t esi36;
    void** rax37;
    int1_t less38;
    int1_t less_or_equal39;
    void** tmp64_40;

    rsi2 = output_blocksize;
    rdi3 = obuf;
    v4 = rbx5;
    rax10 = iwrite_constprop_0(rdi3, rsi2, rdx6, rcx7, r8_8, r9_9);
    tmp64_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(w_bytes) + reinterpret_cast<unsigned char>(rax10));
    w_bytes = tmp64_11;
    zf12 = output_blocksize == rax10;
    if (!zf12) {
        rsi13 = output_file;
        rax14 = quotearg_style(4, rsi13);
        rax15 = fun_2660();
        rax16 = fun_2570();
        rsi17 = *reinterpret_cast<void***>(rax16);
        *reinterpret_cast<int32_t*>(&rsi17 + 4) = 0;
        nl_error(0, rsi17, rax15, rax14, r8_18, r9_19);
        if (rax10) {
            tmp64_20 = w_partial + 1;
            w_partial = tmp64_20;
        }
        quit_constprop_0(0, rsi17, rax15, rax14, r8_21, r9_22);
        r12_23 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rsi17)));
        if (rsi17) {
            rbp24 = reinterpret_cast<void**>(0);
            while (1) {
                eax25 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp24));
                zf26 = *reinterpret_cast<unsigned char*>(&eax25) == newline_character;
                rbx27 = col;
                rdx28 = conversion_blocksize;
                if (!zf26) {
                    if (rdx28 == rbx27) {
                        tmp64_29 = r_truncate + 1;
                        r_truncate = tmp64_29;
                    } else {
                        if (rdx28 > rbx27 && (rcx30 = oc, rsi31 = obuf, rdx32 = rcx30 + 1, *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi31) + reinterpret_cast<unsigned char>(rcx30)) = *reinterpret_cast<unsigned char*>(&eax25), less33 = reinterpret_cast<signed char>(rdx32) < reinterpret_cast<signed char>(output_blocksize), oc = rdx32, !less33)) {
                            write_output(0);
                            rbx27 = col;
                        }
                    }
                    ++rbp24;
                    col = rbx27 + 1;
                    if (rbp24 == r12_23) 
                        break;
                } else {
                    if (rdx28 > rbx27) {
                        do {
                            rdx34 = oc;
                            rcx35 = obuf;
                            esi36 = space_character;
                            rax37 = rdx34 + 1;
                            *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rcx35) + reinterpret_cast<unsigned char>(rdx34)) = *reinterpret_cast<signed char*>(&esi36);
                            less38 = reinterpret_cast<signed char>(rax37) < reinterpret_cast<signed char>(output_blocksize);
                            oc = rax37;
                            if (!less38) {
                                write_output(0);
                            }
                            ++rbx27;
                            less_or_equal39 = conversion_blocksize <= rbx27;
                        } while (!less_or_equal39);
                    }
                    ++rbp24;
                    col = 0;
                    if (rbp24 == r12_23) 
                        break;
                }
            }
        }
        goto v4;
    } else {
        oc = reinterpret_cast<void**>(0);
        tmp64_40 = w_full + 1;
        w_full = tmp64_40;
        return;
    }
}