translate_buffer (char *buf, idx_t nread)
{
  idx_t i;
  char *cp;
  for (i = nread, cp = buf; i; i--, cp++)
    *cp = trans_table[to_uchar (*cp)];
}