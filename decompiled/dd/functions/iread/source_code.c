iread (int fd, char *buf, idx_t size)
{
  ssize_t nread;
  static ssize_t prev_nread;

  do
    {
      process_signals ();
      nread = read (fd, buf, size);
      /* Ignore final read error with iflag=direct as that
         returns EINVAL due to the non aligned file offset.  */
      if (nread == -1 && errno == EINVAL
          && 0 < prev_nread && prev_nread < size
          && (input_flags & O_DIRECT))
        {
          errno = 0;
          nread = 0;
        }
    }
  while (nread < 0 && errno == EINTR);

  /* Short read may be due to received signal.  */
  if (0 < nread && nread < size)
    process_signals ();

  if (0 < nread && warn_partial_read)
    {
      if (0 < prev_nread && prev_nread < size)
        {
          idx_t prev = prev_nread;
          if (status_level != STATUS_NONE)
            error (0, 0, ngettext (("warning: partial read (%td byte); "
                                    "suggest iflag=fullblock"),
                                   ("warning: partial read (%td bytes); "
                                    "suggest iflag=fullblock"),
                                   select_plural (prev)),
                   prev);
          warn_partial_read = false;
        }
    }

  prev_nread = nread;
  return nread;
}