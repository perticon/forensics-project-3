ssize_t iread(int param_1,void *param_2,size_t param_3)

{
  int iVar1;
  long lVar2;
  ssize_t sVar3;
  int *piVar4;
  undefined8 uVar5;
  
  do {
    process_signals();
    sVar3 = read(param_1,param_2,param_3);
    if (sVar3 == -1) {
      piVar4 = __errno_location();
      iVar1 = *piVar4;
      if (iVar1 == 0x16) {
        if (prev_nread_6 < 1) {
          prev_nread_6 = sVar3;
          return -1;
        }
        if ((long)param_3 <= prev_nread_6) {
          prev_nread_6 = sVar3;
          return -1;
        }
        if ((input_flags._1_1_ & 0x40) == 0) {
          prev_nread_6 = sVar3;
          return -1;
        }
        *piVar4 = 0;
        prev_nread_6 = 0;
        return 0;
      }
    }
    else {
      if (-1 < sVar3) break;
      piVar4 = __errno_location();
      iVar1 = *piVar4;
    }
  } while (iVar1 == 4);
  if (0 < sVar3) {
    if (sVar3 < (long)param_3) {
      process_signals();
    }
    else if (sVar3 < 1) {
      prev_nread_6 = sVar3;
      return sVar3;
    }
    lVar2 = prev_nread_6;
    if (((warn_partial_read != '\0') && (prev_nread_6 < (long)param_3)) && (0 < prev_nread_6)) {
      if (status_level != 1) {
        uVar5 = dcngettext(0,"warning: partial read (%td byte); suggest iflag=fullblock",
                           "warning: partial read (%td bytes); suggest iflag=fullblock",prev_nread_6
                           ,5);
        nl_error(0,0,uVar5,lVar2);
      }
      warn_partial_read = '\0';
    }
  }
  prev_nread_6 = sVar3;
  return sVar3;
}