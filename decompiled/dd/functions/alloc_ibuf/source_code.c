alloc_ibuf (void)
{
  if (ibuf)
    return;

  bool extra_byte_for_swab = !!(conversions_mask & C_SWAB);
  ibuf = alignalloc (page_size, input_blocksize + extra_byte_for_swab);
  if (!ibuf)
    {
      char hbuf[LONGEST_HUMAN_READABLE + 1];
      die (EXIT_FAILURE, 0,
           _("memory exhausted by input buffer of size %td bytes (%s)"),
           input_blocksize,
           human_readable (input_blocksize, hbuf,
                           human_opts | human_base_1024, 1, 1));
    }
}