abbreviation_lacks_prefix (char const *message)
{
  return message[strlen (message) - 2] == ' ';
}