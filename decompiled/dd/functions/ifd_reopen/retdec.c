int32_t ifd_reopen(int32_t desired_fd, char * file, int32_t flag, int32_t mode) {
    process_signals();
    uint32_t result = fd_reopen(desired_fd, file, flag, mode); // 0x5b71
    while (result < 0) {
        // 0x5b58
        if (*(int32_t *)function_2570() != 4) {
            // break -> 0x5b7d
            break;
        }
        process_signals();
        result = fd_reopen(desired_fd, file, flag, mode);
    }
    // 0x5b7d
    return result;
}