int32_t ifd_reopen(uint32_t edi, void** rsi, void** edx, void** ecx) {
    void** rdi1;
    uint32_t r14d5;
    void** r13_6;
    void** ebp7;
    void** ebx8;
    int64_t rcx9;
    int64_t rdx10;
    int32_t eax11;
    void** rax12;

    *reinterpret_cast<uint32_t*>(&rdi1) = edi;
    r14d5 = *reinterpret_cast<uint32_t*>(&rdi1);
    r13_6 = rsi;
    ebp7 = edx;
    ebx8 = ecx;
    do {
        process_signals(rdi1, rsi, rdi1, rsi);
        *reinterpret_cast<void***>(&rcx9) = ebx8;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx9) + 4) = 0;
        *reinterpret_cast<void***>(&rdx10) = ebp7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx10) + 4) = 0;
        rsi = r13_6;
        *reinterpret_cast<uint32_t*>(&rdi1) = r14d5;
        *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
        eax11 = fd_reopen(rdi1, rsi, rdx10, rcx9);
        if (eax11 >= 0) 
            break;
        rax12 = fun_2570();
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax12) == 4));
    return eax11;
}