int ifd_reopen(undefined4 param_1,undefined8 param_2,undefined4 param_3,undefined4 param_4)

{
  int iVar1;
  int *piVar2;
  
  do {
    process_signals();
    iVar1 = fd_reopen(param_1,param_2,param_3,param_4);
    if (-1 < iVar1) {
      return iVar1;
    }
    piVar2 = __errno_location();
  } while (*piVar2 == 4);
  return iVar1;
}