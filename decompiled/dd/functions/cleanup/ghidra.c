void cleanup(EVP_PKEY_CTX *ctx)

{
  int iVar1;
  int *piVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  
  if ((interrupt_signal == 0) && (iVar1 = synchronize_output(), iVar1 != 0)) {
                    /* WARNING: Subroutine does not return */
    exit(iVar1);
  }
  iVar1 = close(0);
  if (iVar1 != 0) {
    piVar2 = __errno_location();
    iVar1 = *piVar2;
    while (iVar1 == 4) {
      iVar1 = close(0);
      if ((iVar1 == 0) || (iVar1 = *piVar2, iVar1 == 9)) goto LAB_001059d0;
    }
    uVar3 = quotearg_style(4,input_file);
    uVar4 = dcgettext(0,"closing input file %s",5);
    nl_error(1,*piVar2,uVar4,uVar3);
  }
LAB_001059d0:
  iVar1 = close(1);
  if (iVar1 != 0) {
    piVar2 = __errno_location();
    iVar1 = *piVar2;
    while (iVar1 == 4) {
      iVar1 = close(1);
      if (iVar1 == 0) {
        return;
      }
      iVar1 = *piVar2;
      if (iVar1 == 9) {
        return;
      }
    }
    uVar3 = quotearg_style(4,output_file);
    uVar4 = dcgettext(0,"closing output file %s",5);
    nl_error(1,*piVar2,uVar4,uVar3);
  }
  return;
}