void cleanup(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    uint32_t eax7;
    uint32_t eax8;
    int32_t eax9;
    int32_t eax10;
    void** rax11;
    void** rbx12;
    void** eax13;
    int32_t eax14;
    void** rax15;
    void** rbx16;
    void** eax17;
    int32_t eax18;
    void** rdi19;
    void** rsp20;
    void* rax21;
    void* v22;
    uint32_t eax23;
    int32_t eax24;
    int64_t r12_25;
    int32_t eax26;
    void* rsp27;
    uint32_t eax28;
    int32_t eax29;
    void* rax30;
    void** rsi31;
    void** rax32;
    void** rax33;
    void** esi34;
    void** rsi35;
    void** rax36;
    void** rax37;
    void** esi38;
    uint32_t r14d39;
    void** r13_40;
    int32_t ebp41;
    int32_t ebx42;
    int64_t rcx43;
    int64_t rdx44;
    int32_t eax45;
    void** rax46;
    int64_t v47;
    int64_t v48;

    eax7 = interrupt_signal;
    if (eax7 || (eax8 = synchronize_output(rdi, rsi, rdx, rcx, r8, r9), !eax8)) {
        eax9 = fun_2740();
        if (!eax9) {
            addr_59d0_3:
            eax10 = fun_2740();
            if (!eax10) {
                addr_5a48_4:
                return;
            } else {
                rax11 = fun_2570();
                rbx12 = rax11;
                eax13 = *reinterpret_cast<void***>(rax11);
                do {
                    if (eax13 != 4) 
                        break;
                    eax14 = fun_2740();
                } while (eax14 && (eax13 = *reinterpret_cast<void***>(rbx12), eax13 != 9));
                goto addr_5a48_4;
            }
        } else {
            rax15 = fun_2570();
            rbx16 = rax15;
            eax17 = *reinterpret_cast<void***>(rax15);
            do {
                if (eax17 != 4) 
                    goto addr_598f_10;
                eax18 = fun_2740();
            } while (eax18 && (eax17 = *reinterpret_cast<void***>(rbx16), eax17 != 9));
            goto addr_59d0_3;
        }
    } else {
        *reinterpret_cast<uint32_t*>(&rdi19) = eax8;
        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
        fun_2910();
        rsp20 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 0x90);
        rax21 = g28;
        v22 = rax21;
        while ((eax23 = interrupt_signal, !!eax23) || (eax24 = info_signal_count, !!eax24)) {
            do {
                fun_2520();
                *reinterpret_cast<uint32_t*>(&r12_25) = interrupt_signal;
                eax26 = info_signal_count;
                if (eax26) {
                    info_signal_count = eax26 - 1;
                }
                *reinterpret_cast<int32_t*>(&rdx) = 0;
                rsi = rsp20;
                *reinterpret_cast<uint32_t*>(&rdi19) = 2;
                *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
                fun_2520();
                rsp27 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp20 - 8) + 8 - 8 + 8);
                if (*reinterpret_cast<uint32_t*>(&r12_25)) 
                    break;
                print_stats();
                rsp20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp27) - 8 + 8);
                eax28 = interrupt_signal;
            } while (eax28 || (eax29 = info_signal_count, !!eax29));
            break;
            cleanup(2, rsi, 0, rcx, r8, r9);
            print_stats();
            *reinterpret_cast<uint32_t*>(&rdi19) = *reinterpret_cast<uint32_t*>(&r12_25);
            *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
            fun_2540();
            rsp20 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp27) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        rax30 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(v22) - reinterpret_cast<uint64_t>(g28));
        if (rax30) 
            goto addr_5b2f_21; else 
            goto addr_5b06_22;
    }
    rsi31 = output_file;
    rax32 = quotearg_style(4, rsi31, 4, rsi31);
    rax33 = fun_2660();
    esi34 = *reinterpret_cast<void***>(rbx12);
    nl_error(1, esi34, rax33, rax32, r8, r9);
    goto addr_5a48_4;
    addr_598f_10:
    rsi35 = input_file;
    rax36 = quotearg_style(4, rsi35);
    rax37 = fun_2660();
    esi38 = *reinterpret_cast<void***>(rbx16);
    nl_error(1, esi38, rax37, rax36, r8, r9);
    goto addr_59d0_3;
    addr_5b2f_21:
    fun_2690();
    r14d39 = *reinterpret_cast<uint32_t*>(&rdi19);
    r13_40 = rsi;
    ebp41 = *reinterpret_cast<int32_t*>(&rdx);
    ebx42 = *reinterpret_cast<int32_t*>(&rcx);
    do {
        process_signals(rdi19, rsi, rdi19, rsi);
        *reinterpret_cast<int32_t*>(&rcx43) = ebx42;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx43) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdx44) = ebp41;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx44) + 4) = 0;
        rsi = r13_40;
        *reinterpret_cast<uint32_t*>(&rdi19) = r14d39;
        *reinterpret_cast<int32_t*>(&rdi19 + 4) = 0;
        eax45 = fd_reopen(rdi19, rsi, rdx44, rcx43);
        if (eax45 >= 0) 
            break;
        rax46 = fun_2570();
    } while (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax46) == 4));
    goto v47;
    addr_5b06_22:
    goto v48;
}