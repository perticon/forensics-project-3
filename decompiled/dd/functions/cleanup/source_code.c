cleanup (void)
{
  if (!interrupt_signal)
    {
      int sync_status = synchronize_output ();
      if (sync_status)
        exit (sync_status);
    }

  if (iclose (STDIN_FILENO) != 0)
    die (EXIT_FAILURE, errno, _("closing input file %s"), quoteaf (input_file));

  /* Don't remove this call to close, even though close_stdout
     closes standard output.  This close is necessary when cleanup
     is called as a consequence of signal handling.  */
  if (iclose (STDOUT_FILENO) != 0)
    die (EXIT_FAILURE, errno,
         _("closing output file %s"), quoteaf (output_file));
}