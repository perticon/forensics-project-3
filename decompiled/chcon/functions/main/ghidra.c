uint main(int param_1,undefined8 *param_2)

{
  undefined8 *puVar1;
  long lVar2;
  char cVar3;
  int iVar4;
  long lVar5;
  undefined8 uVar6;
  undefined8 uVar7;
  uint uVar8;
  byte bVar9;
  int extraout_EDX;
  long lVar10;
  uint *puVar11;
  int *piVar12;
  char *pcVar13;
  undefined1 *__s1;
  long in_FS_OFFSET;
  undefined8 uVar14;
  uint local_60;
  byte local_55;
  int local_54;
  long local_50;
  long local_48;
  
  lVar5 = 0;
  __s1 = long_options;
  pcVar13 = "HLPRhvu:r:t:l:";
  piVar12 = (int *)(long)param_1;
  lVar2 = *(long *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  puVar11 = &switchD_001029dd::switchdataD_0010cc88;
  textdomain("coreutils");
  atexit(close_stdout);
  local_55 = 0;
  local_50 = 0;
  local_54 = -1;
  local_60 = 0x10;
switchD_001029dd_caseD_66:
  uVar14 = 0x1029ac;
  iVar4 = getopt_long((ulong)piVar12 & 0xffffffff,param_2,"HLPRhvu:r:t:l:",long_options,0);
  if (iVar4 == -1) {
    if (recurse == 0) {
      local_60 = 0x10;
      bVar9 = local_54 != 0;
LAB_00102afe:
      lVar10 = (long)optind;
      affect_symlink_referent = bVar9;
      iVar4 = optind;
      if ((local_50 == 0) && ((char)lVar5 == '\0')) {
        if (1 < param_1 - optind) {
          optind = optind + 1;
          local_50 = param_2[lVar10];
LAB_00102b40:
          specified_context = local_50;
          local_55 = local_55 & recurse;
          puVar11 = (uint *)(ulong)local_55;
          piVar12 = __errno_location();
          if (local_55 == 0) {
            root_dev_ino = (long *)0x0;
            goto LAB_00102b6f;
          }
          goto LAB_0010310f;
        }
      }
      else if (0 < param_1 - optind) {
        if (local_50 != 0) {
          param_2 = (undefined8 *)__errno_location();
          *(undefined4 *)param_2 = 0x5f;
          piVar12 = (int *)quotearg_style(4,local_50);
          uVar14 = dcgettext(0,"failed to get security context of %s",5);
          error(1,*(undefined4 *)param_2,uVar14,piVar12);
          goto LAB_00103054;
        }
        goto LAB_00102b40;
      }
    }
    else {
      if (local_60 != 0x10) {
        bVar9 = recurse;
        if (local_54 == 0) {
          uVar14 = dcgettext(0,"-R -h requires -P",5);
          error(1,0,uVar14);
          goto LAB_00102ed7;
        }
        goto LAB_00102afe;
      }
      bVar9 = 0;
      if (local_54 != 1) goto LAB_00102afe;
      uVar14 = dcgettext(0,"-R --dereference requires either -H or -L",5);
      error(1,0,uVar14);
LAB_0010310f:
      root_dev_ino = (long *)get_root_dev_ino(dev_ino_buf_0);
      if (root_dev_ino != (long *)0x0) {
LAB_00102b6f:
        puVar1 = param_2 + optind;
        param_2 = (undefined8 *)0x1;
        uVar8 = 1;
        puVar11 = (uint *)xfts_open(puVar1,local_60 | 8,0);
        lVar5 = rpl_fts_read(puVar11);
        if (lVar5 != 0) goto LAB_00102ba8;
LAB_00102c7f:
        if (*piVar12 != 0) {
          uVar8 = 0;
          uVar14 = dcgettext(0,"fts_read failed",5);
          error(0,*piVar12,uVar14);
        }
        iVar4 = rpl_fts_close(puVar11);
        if (iVar4 != 0) {
          uVar8 = 0;
          uVar14 = dcgettext(0,"fts_close failed",5);
          error(0,*piVar12,uVar14);
        }
        if (lVar2 == *(long *)(in_FS_OFFSET + 0x28)) {
          return uVar8 ^ 1;
        }
                    /* WARNING: Subroutine does not return */
        __stack_chk_fail();
      }
      uVar14 = quotearg_style(4,"/");
      uVar6 = dcgettext(0,"failed to get attributes of %s",5);
      error(1,*piVar12,uVar6,uVar14);
      iVar4 = extraout_EDX;
    }
    if (iVar4 < (int)piVar12) {
      piVar12 = (int *)quote(param_2[(long)piVar12 + -1]);
      uVar14 = dcgettext(0,"missing operand after %s",5);
      error(0,0,uVar14,piVar12);
    }
    else {
      uVar14 = dcgettext(0,"missing operand",5);
      error(0,0,uVar14);
    }
  }
  else if (iVar4 < 0x84) {
    if (0x47 < iVar4) goto code_r0x001029d5;
    if (iVar4 == -0x83) {
      version_etc(stdout,"chcon","GNU coreutils",Version,"Russell Coker","Jim Meyering",0,uVar14);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar4 == -0x82) {
      usage();
      goto switchD_001029dd_caseD_4c;
    }
  }
  goto switchD_001029dd_caseD_49;
code_r0x001029d5:
  switch(iVar4) {
  case 0x48:
    local_60 = 0x11;
    break;
  default:
switchD_001029dd_caseD_49:
    usage(1);
LAB_0010319c:
    iVar4 = strcmp(__s1,"/");
    if (iVar4 == 0) {
      quotearg_style(4,__s1);
      uVar14 = dcgettext(0,"it is dangerous to operate recursively on %s",5);
      error(0,0,uVar14);
    }
    else {
      uVar14 = quotearg_n_style(1,4,"/");
      uVar6 = quotearg_n_style(0,4,__s1);
      uVar7 = dcgettext(0,"it is dangerous to operate recursively on %s (same as %s)",5);
      error(0,0,uVar7,uVar6,uVar14);
    }
    uVar14 = dcgettext(0,"use --no-preserve-root to override this failsafe",5);
    error(0,0,uVar14);
    uVar8 = 0;
LAB_00102c60:
    do {
      if (recurse == 0) {
        rpl_fts_set(puVar11,lVar5,4);
        param_2 = (undefined8 *)(ulong)((uint)param_2 & uVar8);
        goto LAB_00102c6b;
      }
LAB_00102c69:
      param_2 = (undefined8 *)(ulong)((uint)param_2 & uVar8);
LAB_00102c6b:
      uVar8 = (uint)param_2;
      lVar5 = rpl_fts_read(puVar11);
      if (lVar5 == 0) goto LAB_00102c7f;
LAB_00102ba8:
      __s1 = *(char **)(lVar5 + 0x38);
      pcVar13 = *(char **)(lVar5 + 0x30);
      switch(*(undefined2 *)(lVar5 + 0x68)) {
      case 1:
        uVar8 = (uint)recurse;
        if (recurse != 0) {
          if (((root_dev_ino == (long *)0x0) || (*(long *)(lVar5 + 0x78) != *root_dev_ino)) ||
             (*(long *)(lVar5 + 0x70) != root_dev_ino[1])) goto LAB_00102c69;
          iVar4 = strcmp(__s1,"/");
          if (iVar4 == 0) {
            quotearg_style(4,__s1);
            uVar14 = dcgettext(0,"it is dangerous to operate recursively on %s",5);
            error(0,0,uVar14);
          }
          else {
            uVar14 = quotearg_n_style(1,4,"/");
            uVar6 = quotearg_n_style(0,4,__s1);
            uVar7 = dcgettext(0,"it is dangerous to operate recursively on %s (same as %s)",5);
            error(0,0,uVar7,uVar6,uVar14);
          }
          uVar14 = dcgettext(0,"use --no-preserve-root to override this failsafe",5);
          error(0,0,uVar14);
          rpl_fts_set(puVar11,lVar5,4);
          rpl_fts_read(puVar11);
          param_2 = (undefined8 *)0x0;
          goto LAB_00102c6b;
        }
        break;
      case 2:
        cVar3 = cycle_warning_required(puVar11,lVar5);
        if (cVar3 != '\0') {
          quotearg_n_style_colon(0,3,__s1);
          uVar14 = dcgettext(0,
                             "WARNING: Circular directory structure.\nThis almost certainly means that you have a corrupted file system.\nNOTIFY YOUR SYSTEM MANAGER.\nThe following directory is part of the cycle:\n  %s\n"
                             ,5);
          error(0,0,uVar14);
          param_2 = (undefined8 *)0x0;
          goto LAB_00102c6b;
        }
        break;
      case 4:
        quotearg_style(4,__s1);
        pcVar13 = "cannot read directory %s";
        goto LAB_00102d13;
      case 6:
        if (recurse == 0) goto LAB_00102fed;
        if (((root_dev_ino == (long *)0x0) || (*(long *)(lVar5 + 0x78) != *root_dev_ino)) ||
           (*(long *)(lVar5 + 0x70) != root_dev_ino[1])) break;
        goto LAB_0010319c;
      case 7:
        quotearg_n_style_colon(0,3,__s1);
        error(0,*(undefined4 *)(lVar5 + 0x40),"%s");
        uVar8 = 0;
        goto LAB_00102c60;
      case 10:
        if ((*(long *)(lVar5 + 0x58) == 0) && (*(long *)(lVar5 + 0x20) == 0)) {
          *(undefined8 *)(lVar5 + 0x20) = 1;
          rpl_fts_set(puVar11,lVar5,1);
          goto LAB_00102fed;
        }
        quotearg_style(4,__s1);
        pcVar13 = "cannot access %s";
LAB_00102d13:
        uVar14 = dcgettext(0,pcVar13,5);
        error(0,*(undefined4 *)(lVar5 + 0x40),uVar14);
        uVar8 = 0;
        goto LAB_00102c60;
      }
      if (verbose != '\0') {
LAB_00102ed7:
        uVar14 = quotearg_style(4,__s1);
        uVar6 = dcgettext(0,"changing security context of %s\n",5);
        __printf_chk(1,uVar6,uVar14);
      }
      lVar10 = specified_context;
      local_48 = 0;
      if (specified_context == 0) {
        if (affect_symlink_referent == 0) {
LAB_00103054:
          iVar4 = lgetfileconat();
        }
        else {
          iVar4 = getfileconat(puVar11[0xb],pcVar13);
        }
        if ((iVar4 < 0) && (*piVar12 != 0x3d)) {
          quotearg_style(4,pcVar13);
          pcVar13 = "failed to get security context of %s";
        }
        else {
          if (local_48 == 0) {
            quotearg_style(4,pcVar13);
            uVar14 = dcgettext(0,"can\'t apply partial context to unlabeled file %s",5);
            error(0,0,uVar14);
            uVar8 = 0;
            goto LAB_00102c60;
          }
          *piVar12 = 0x5f;
          quote();
          pcVar13 = "failed to create security context: %s";
        }
        uVar14 = dcgettext(0,pcVar13,5);
        error(0,*piVar12,uVar14);
        uVar8 = 0;
        goto LAB_00102c60;
      }
      if (affect_symlink_referent == 0) {
        iVar4 = lsetfileconat(puVar11[0xb],pcVar13,specified_context);
      }
      else {
        iVar4 = setfileconat();
      }
      if (iVar4 == 0) {
        uVar8 = 1;
      }
      else {
        uVar14 = quote_n(1,lVar10);
        uVar6 = quotearg_n_style(0,4,pcVar13);
        uVar7 = dcgettext(0,"failed to change context of %s to %s",5);
        error(0,*piVar12,uVar7,uVar6,uVar14);
        uVar8 = 0;
      }
    } while( true );
  case 0x4c:
switchD_001029dd_caseD_4c:
    local_60 = 2;
    break;
  case 0x50:
    local_60 = 0x10;
    break;
  case 0x52:
    recurse = 1;
    break;
  case 0x66:
    break;
  case 0x68:
    local_54 = 0;
    break;
  case 0x6c:
  case 0x72:
  case 0x74:
  case 0x75:
    lVar5 = 1;
    break;
  case 0x76:
    verbose = '\x01';
    break;
  case 0x80:
    local_54 = 1;
    break;
  case 0x81:
    local_55 = 0;
    break;
  case 0x82:
    local_55 = 1;
    break;
  case 0x83:
    local_50 = optarg;
  }
  goto switchD_001029dd_caseD_66;
LAB_00102fed:
  param_2 = (undefined8 *)(ulong)((uint)param_2 & 1);
  goto LAB_00102c6b;
}