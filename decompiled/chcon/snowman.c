
int64_t fun_2460(void** rdi, ...);

int32_t i_ring_push(void*** rdi);

int32_t fun_25f0();

int32_t cwd_advance_fd(void** rdi, void** esi, signed char dl) {
    int32_t eax4;
    int32_t eax5;

    if (*reinterpret_cast<void***>(rdi + 44) == esi && !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) {
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
    }
    if (dl) {
        eax4 = i_ring_push(rdi + 96);
        if (eax4 < 0) {
            addr_3eb9_27:
            *reinterpret_cast<void***>(rdi + 44) = esi;
            return eax4;
        } else {
            eax5 = fun_25f0();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_3eb9_27;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_3eb9_27;
        eax5 = fun_25f0();
    }
    *reinterpret_cast<void***>(rdi + 44) = esi;
    return eax5;
}

void** fun_26d0(void** rdi, ...);

void fun_26a0(void** rdi, void** rsi, void** rdx);

void** fts_alloc(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    rax4 = fun_26d0(reinterpret_cast<uint64_t>(rdx + 0x108) & 0xfffffffffffffff8);
    if (rax4) {
        fun_26a0(rax4 + 0x100, rsi, rdx);
        rax5 = *reinterpret_cast<void***>(rdi + 32);
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rdx) + 0x100) = 0;
        *reinterpret_cast<void***>(rax4 + 96) = rdx;
        *reinterpret_cast<void***>(rax4 + 80) = rdi;
        *reinterpret_cast<void***>(rax4 + 56) = rax5;
        *reinterpret_cast<void***>(rax4 + 64) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 24) = reinterpret_cast<void**>(0);
        *reinterpret_cast<unsigned char*>(rax4 + 0x6a) = reinterpret_cast<unsigned char>(0x30000);
        *reinterpret_cast<void***>(rax4 + 32) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rax4 + 40) = reinterpret_cast<void**>(0);
    }
    return rax4;
}

void** g28;

int64_t free = 0;

void** hash_initialize(void** rdi);

struct s0 {
    signed char[8] pad8;
    void** f8;
};

struct s0* hash_lookup();

int32_t fun_27c0();

void** hash_insert(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

uint32_t fun_2570();

void fun_2440(void** rdi, void** rsi, ...);

void** filesystem_type(void** rdi, void** esi, ...) {
    void** rsi2;
    void** rsp3;
    void** r12_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** r13d8;
    int64_t r8_9;
    void** rax10;
    void** v11;
    struct s0* rax12;
    void** rax13;
    int32_t eax14;
    void** r12_15;
    void** v16;
    void** rax17;
    void** rax18;
    void** rax19;
    void* rdx20;
    int32_t eax21;
    void** v22;

    rsi2 = esi;
    rsp3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x98);
    r12_4 = *reinterpret_cast<void***>(rdi + 80);
    rax5 = g28;
    rbp6 = *reinterpret_cast<void***>(r12_4 + 80);
    if (!(*reinterpret_cast<unsigned char*>(r12_4 + 73) & 2)) 
        goto addr_3e08_2;
    rbx7 = rdi;
    r13d8 = rsi2;
    if (!rbp6 && (r8_9 = free, rsi2 = reinterpret_cast<void**>(0), rdi = reinterpret_cast<void**>(13), *reinterpret_cast<int32_t*>(&rdi + 4) = 0, rax10 = hash_initialize(13), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), *reinterpret_cast<void***>(r12_4 + 80) = rax10, rbp6 = rax10, !rax10) || (rsi2 = rsp3, rdi = rbp6, v11 = *reinterpret_cast<void***>(rbx7 + 0x70), rax12 = hash_lookup(), rsp3 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp3 - 8) + 8), rax12 == 0)) {
        if (reinterpret_cast<signed char>(r13d8) < reinterpret_cast<signed char>(0)) {
            addr_3e08_2:
            *reinterpret_cast<int32_t*>(&rax13) = 0;
            *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
        } else {
            rsi2 = rsp3 + 16;
            rdi = r13d8;
            *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
            eax14 = fun_27c0();
            if (!eax14) {
                r12_15 = v16;
                if (!rbp6) {
                    addr_3e76_7:
                    rax13 = r12_15;
                } else {
                    rdi = reinterpret_cast<void**>(16);
                    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
                    rax17 = fun_26d0(16, 16);
                    if (!rax17) 
                        goto addr_3e71_9;
                    rax18 = *reinterpret_cast<void***>(rbx7 + 0x70);
                    *reinterpret_cast<void***>(rax17 + 8) = r12_15;
                    rsi2 = rax17;
                    rdi = rbp6;
                    *reinterpret_cast<void***>(rax17) = rax18;
                    rax19 = hash_insert(rdi, rsi2, 0x39c0, 0x39d0, r8_9);
                    if (!rax19) 
                        goto addr_3e80_11; else 
                        goto addr_3e68_12;
                }
            } else {
                goto addr_3e08_2;
            }
        }
    } else {
        rax13 = rax12->f8;
    }
    rdx20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (!rdx20) {
        return rax13;
    }
    fun_2570();
    if (*reinterpret_cast<void***>(rdi + 44) != rsi2) 
        goto addr_3eab_19;
    if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi + 44) == 0xffffff9c)) 
        goto addr_2875_21;
    addr_3eab_19:
    if (*reinterpret_cast<signed char*>(&rdx20)) {
        eax21 = i_ring_push(rdi + 96);
        if (eax21 < 0) {
            addr_3eb9_23:
            *reinterpret_cast<void***>(rdi + 44) = rsi2;
            goto v11;
        } else {
            fun_25f0();
        }
    } else {
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 4) 
            goto addr_3eb9_23;
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 44)) < reinterpret_cast<signed char>(0)) 
            goto addr_3eb9_23;
        fun_25f0();
    }
    *reinterpret_cast<void***>(rdi + 44) = rsi2;
    goto v11;
    addr_2875_21:
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    fun_2460(rdi);
    addr_3e80_11:
    rdi = rax17;
    fun_2440(rdi, rsi2, rdi, rsi2);
    goto addr_3e71_9;
    addr_3e68_12:
    if (rax17 != rax19) {
        fun_2460(rdi);
        goto addr_2875_21;
    } else {
        addr_3e71_9:
        r12_15 = v22;
        goto addr_3e76_7;
    }
}

void** fun_2720(void** rdi);

void fun_24c0();

void** fts_sort(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r12_5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rsi9;
    void** r8_10;
    void** rax11;
    void** rdx12;
    void** r8_13;
    void** rax14;
    void** rdx15;
    void** rsi16;
    void** rcx17;
    void** rdx18;

    r12_5 = rdi;
    rbp6 = rdx;
    rbx7 = rsi;
    rdi8 = *reinterpret_cast<void***>(rdi + 16);
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_5 + 56)) < reinterpret_cast<unsigned char>(rdx)) {
        rsi9 = rdx + 40;
        r8_10 = rdi8;
        *reinterpret_cast<void***>(r12_5 + 56) = rsi9;
        if (reinterpret_cast<unsigned char>(rsi9) >> 61) {
            addr_3c82_3:
            fun_2440(r8_10, rsi9);
            *reinterpret_cast<void***>(r12_5 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_5 + 56) = reinterpret_cast<void**>(0);
            return rbx7;
        } else {
            rsi9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi9) << 3);
            rax11 = fun_2720(rdi8);
            rdi8 = rax11;
            if (!rax11) {
                r8_10 = *reinterpret_cast<void***>(r12_5 + 16);
                goto addr_3c82_3;
            } else {
                *reinterpret_cast<void***>(r12_5 + 16) = rax11;
            }
        }
    }
    rdx12 = rdi8;
    if (rbx7) {
        do {
            *reinterpret_cast<void***>(rdx12) = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdx12 = rdx12 + 8;
        } while (rbx7);
    }
    fun_24c0();
    r8_13 = *reinterpret_cast<void***>(r12_5 + 16);
    rax14 = *reinterpret_cast<void***>(r8_13);
    rdx15 = r8_13;
    rsi16 = rax14;
    rcx17 = rbp6 - 1;
    if (rcx17) {
        while (rdx15 = rdx15 + 8, *reinterpret_cast<void***>(rsi16 + 16) = *reinterpret_cast<void***>(rdx15 + 8), --rcx17, !!rcx17) {
            rsi16 = *reinterpret_cast<void***>(rdx15);
        }
        rdx18 = *reinterpret_cast<void***>(reinterpret_cast<uint64_t>(r8_13 + reinterpret_cast<unsigned char>(rbp6) * 8) - 8);
    } else {
        rdx18 = rax14;
    }
    *reinterpret_cast<void***>(rdx18 + 16) = reinterpret_cast<void**>(0);
    return rax14;
}

int32_t fun_2710();

unsigned char i_ring_empty(void*** rdi, void** rsi, void** rdx);

void** i_ring_pop(void*** rdi, void** rsi, void** rdx);

uint32_t restore_initial_cwd(void** rdi, void** rsi) {
    void** eax3;
    uint32_t r12d4;
    int32_t eax5;
    void*** rbx6;
    unsigned char al7;
    void** eax8;

    eax3 = *reinterpret_cast<void***>(rdi + 72);
    r12d4 = reinterpret_cast<unsigned char>(eax3) & 4;
    if (r12d4) {
        r12d4 = 0;
    } else {
        if (!(*reinterpret_cast<unsigned char*>(&eax3 + 1) & 2)) {
            r12d4 = 0;
            eax5 = fun_2710();
            *reinterpret_cast<unsigned char*>(&r12d4) = reinterpret_cast<uint1_t>(!!eax5);
        } else {
            rsi = reinterpret_cast<void**>(0xffffff9c);
            *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
            cwd_advance_fd(rdi, 0xffffff9c, 1);
        }
    }
    rbx6 = reinterpret_cast<void***>(rdi + 96);
    while (al7 = i_ring_empty(rbx6, rsi, 1), al7 == 0) {
        eax8 = i_ring_pop(rbx6, rsi, 1);
        if (reinterpret_cast<signed char>(eax8) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_25f0();
    }
    return r12d4;
}

void** fun_2470();

unsigned char fts_palloc(void** rdi, void** rsi, void** rdx) {
    void** rsi4;
    void** rdi5;
    void** tmp64_6;
    void** rax7;
    void** rax8;
    void** rdi9;

    rsi4 = rsi + 0x100;
    rdi5 = *reinterpret_cast<void***>(rdi + 32);
    tmp64_6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 48)));
    if (reinterpret_cast<unsigned char>(tmp64_6) < reinterpret_cast<unsigned char>(rsi4)) {
        fun_2440(rdi5, tmp64_6);
        *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
        rax7 = fun_2470();
        *reinterpret_cast<void***>(rax7) = reinterpret_cast<void**>(36);
        return 0;
    } else {
        *reinterpret_cast<void***>(rdi + 48) = tmp64_6;
        rax8 = fun_2720(rdi5);
        if (!rax8) {
            rdi9 = *reinterpret_cast<void***>(rdi + 32);
            fun_2440(rdi9, tmp64_6);
            *reinterpret_cast<void***>(rdi + 32) = reinterpret_cast<void**>(0);
            return 0;
        } else {
            *reinterpret_cast<void***>(rdi + 32) = rax8;
            return 1;
        }
    }
}

void cycle_check_init(void** rdi);

unsigned char setup_dir(void** rdi, void** rsi, ...) {
    void** rax3;
    void** rax4;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rax3 = fun_26d0(32);
        *reinterpret_cast<void***>(rdi + 88) = rax3;
        if (!rax3) {
            return 0;
        } else {
            cycle_check_init(rax3);
            return 1;
        }
    } else {
        rax4 = hash_initialize(31);
        *reinterpret_cast<void***>(rdi + 88) = rax4;
        return static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!rax4));
    }
}

void** hash_remove(void** rdi, void** rsi);

void leave_dir(void** rdi, void** rsi, ...) {
    void** rax3;
    void** rdi4;
    void** v5;
    void** rax6;
    void** rdx7;
    void** rax8;
    void* rax9;
    void** eax10;
    void*** rbx11;
    unsigned char al12;
    void** eax13;

    rax3 = g28;
    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102) {
        rdi4 = *reinterpret_cast<void***>(rdi + 88);
        v5 = *reinterpret_cast<void***>(rsi + 0x70);
        rsi = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 40);
        rax6 = hash_remove(rdi4, rsi);
        rdi = rax6;
        if (!rax6) {
            addr_287a_3:
            fun_2460(rdi, rdi);
        } else {
            fun_2440(rdi, rsi);
            goto addr_40d8_5;
        }
    } else {
        if (*reinterpret_cast<void***>(rsi + 8) && reinterpret_cast<signed char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 88)) >= reinterpret_cast<signed char>(0)) {
            rdx7 = *reinterpret_cast<void***>(rdi + 88);
            if (!*reinterpret_cast<void***>(rdx7 + 16)) 
                goto addr_287a_3;
            if (*reinterpret_cast<void***>(rdx7) == *reinterpret_cast<void***>(rsi + 0x78)) {
                if (*reinterpret_cast<void***>(rdx7 + 8) == *reinterpret_cast<void***>(rsi + 0x70)) {
                    rax8 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x78);
                    *reinterpret_cast<void***>(rdx7 + 8) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rsi + 8) + 0x70);
                    *reinterpret_cast<void***>(rdx7) = rax8;
                    goto addr_40d8_5;
                }
            } else {
                goto addr_40d8_5;
            }
        }
    }
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    fun_2460(rdi, rdi);
    addr_40d8_5:
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (!rax9) {
        return;
    }
    fun_2570();
    eax10 = *reinterpret_cast<void***>(rdi + 72);
    if (!(reinterpret_cast<unsigned char>(eax10) & 4)) 
        goto addr_4156_36;
    addr_4173_38:
    rbx11 = reinterpret_cast<void***>(rdi + 96);
    while (al12 = i_ring_empty(rbx11, rsi, rdx7), al12 == 0) {
        eax13 = i_ring_pop(rbx11, rsi, rdx7);
        if (reinterpret_cast<signed char>(eax13) < reinterpret_cast<signed char>(0)) 
            continue;
        fun_25f0();
    }
    goto v5;
    addr_4156_36:
    if (!(*reinterpret_cast<unsigned char*>(&eax10 + 1) & 2)) {
        fun_2710();
        goto addr_4173_38;
    } else {
        *reinterpret_cast<int32_t*>(&rdx7) = 1;
        *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
        rsi = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        cwd_advance_fd(rdi, 0xffffff9c, 1);
        goto addr_4173_38;
    }
}

int32_t fun_2830(int64_t rdi, void* rsi);

void** openat_safer(int64_t rdi, void** rsi);

void** open_safer(void** rdi, int64_t rsi);

int32_t fts_safe_changedir(void** rdi, void** rsi, void** rdx, void** rcx) {
    void** r15_5;
    void** r14d6;
    void** r12_7;
    void** rbx8;
    void* rsp9;
    void** rax10;
    void** v11;
    uint32_t eax12;
    void** ebp13;
    unsigned char v14;
    uint32_t eax15;
    void** r13d16;
    void* rax17;
    uint32_t eax18;
    int32_t eax19;
    int64_t rdi20;
    int32_t eax21;
    void** v22;
    void** v23;
    void** rax24;
    void** rax25;
    int64_t rdi26;
    void** eax27;
    int64_t rsi28;
    void** eax29;
    void*** r13_30;
    unsigned char al31;
    void** eax32;

    r15_5 = rdi;
    r14d6 = rdx;
    r12_7 = rsi;
    rbx8 = rcx;
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xb8);
    rax10 = g28;
    v11 = rax10;
    if (!rcx || ((eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rcx)), eax12 != 46) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rcx + 1) == 46) || *reinterpret_cast<signed char*>(rcx + 2)))) {
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 4) {
            addr_4350_3:
            ebp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) & 0x200);
            if (!ebp13 || reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
            } else {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                fun_25f0();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
            }
        } else {
            if (reinterpret_cast<signed char>(r14d6) < reinterpret_cast<signed char>(0)) {
                v14 = 0;
                eax15 = reinterpret_cast<unsigned char>(ebp13) & 0x200;
                goto addr_446c_8;
            } else {
                v14 = 0;
                r13d16 = r14d6;
                if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) 
                    goto addr_4318_10; else 
                    goto addr_423a_11;
            }
        }
    } else {
        ebp13 = *reinterpret_cast<void***>(rdi + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 4) 
            goto addr_4350_3;
        if (reinterpret_cast<signed char>(rdx) >= reinterpret_cast<signed char>(0)) 
            goto addr_44a0_14;
        if (reinterpret_cast<unsigned char>(ebp13) & 0x200) 
            goto addr_43d0_16; else 
            goto addr_42d5_17;
    }
    addr_4270_18:
    while (rax17 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v11) - reinterpret_cast<unsigned char>(g28)), !!rax17) {
        eax12 = fun_2570();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        addr_44a0_14:
        v14 = 1;
        r13d16 = rdx;
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) {
            goto addr_4318_10;
        }
        addr_4242_21:
        if (eax12 != 46 || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx8 + 1) == 46)) {
            addr_424b_22:
            ebp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) & 0x200);
            if (ebp13) {
                *reinterpret_cast<int32_t*>(&r12_7) = 0;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                eax18 = static_cast<uint32_t>(v14) ^ 1;
                rdx = reinterpret_cast<void**>(static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&eax18)));
                cwd_advance_fd(r15_5, r13d16, *reinterpret_cast<signed char*>(&rdx));
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                continue;
            } else {
                eax19 = fun_2710();
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                *reinterpret_cast<int32_t*>(&r12_7) = eax19;
                *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) {
                    continue;
                }
            }
        } else {
            if (!*reinterpret_cast<signed char*>(rbx8 + 2)) {
                addr_4318_10:
                *reinterpret_cast<void***>(&rdi20) = r13d16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi20) + 4) = 0;
                eax21 = fun_2830(rdi20, reinterpret_cast<int64_t>(rsp9) + 16);
                rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                if (eax21) {
                    addr_4373_27:
                    *reinterpret_cast<int32_t*>(&r12_7) = -1;
                    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
                    if (reinterpret_cast<signed char>(r14d6) >= reinterpret_cast<signed char>(0)) 
                        continue;
                } else {
                    if (*reinterpret_cast<void***>(r12_7 + 0x70) != v22 || *reinterpret_cast<void***>(r12_7 + 0x78) != v23) {
                        rax24 = fun_2470();
                        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
                        *reinterpret_cast<void***>(rax24) = reinterpret_cast<void**>(2);
                        goto addr_4373_27;
                    } else {
                        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
                        goto addr_424b_22;
                    }
                }
            } else {
                goto addr_424b_22;
            }
        }
        rax25 = fun_2470();
        ebp13 = *reinterpret_cast<void***>(rax25);
        rbx8 = rax25;
        fun_25f0();
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8 - 8 + 8);
        *reinterpret_cast<void***>(rbx8) = ebp13;
    }
    return *reinterpret_cast<int32_t*>(&r12_7);
    addr_446c_8:
    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r15_5 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
    if (eax15) {
        addr_43f9_34:
        eax27 = openat_safer(rdi26, rbx8);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        r13d16 = eax27;
    } else {
        goto addr_42eb_36;
    }
    addr_42fa_37:
    if (reinterpret_cast<signed char>(r13d16) < reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<int32_t*>(&r12_7) = -1;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        goto addr_4270_18;
    } else {
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        if (*reinterpret_cast<unsigned char*>(&ebp13) & 2) {
            goto addr_4318_10;
        }
    }
    addr_423a_11:
    if (!rbx8) 
        goto addr_424b_22;
    eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx8));
    goto addr_4242_21;
    addr_42eb_36:
    *reinterpret_cast<void***>(&rsi28) = rdx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi28) + 4) = 0;
    eax29 = open_safer(rbx8, rsi28);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    r13d16 = eax29;
    goto addr_42fa_37;
    addr_43d0_16:
    r13_30 = reinterpret_cast<void***>(rdi + 96);
    al31 = i_ring_empty(r13_30, rsi, rdx);
    rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
    v14 = al31;
    if (!al31) {
        eax32 = i_ring_pop(r13_30, rsi, rdx);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        ebp13 = *reinterpret_cast<void***>(r15_5 + 72);
        r13d16 = eax32;
        if (reinterpret_cast<signed char>(eax32) < reinterpret_cast<signed char>(0)) {
            v14 = 1;
            eax15 = reinterpret_cast<unsigned char>(ebp13) & 0x200;
            goto addr_446c_8;
        } else {
            v14 = 1;
            r14d6 = eax32;
            if (!(*reinterpret_cast<unsigned char*>(&ebp13) & 2)) 
                goto addr_424b_22;
            goto addr_4318_10;
        }
    } else {
        *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r15_5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
        rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
        goto addr_43f9_34;
    }
    addr_42d5_17:
    v14 = 1;
    rdx = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(ebp13) << 13 & 0x20000 | 0x90900);
    goto addr_42eb_36;
}

void** opendirat(int64_t rdi, void** rsi, int64_t rdx, void** rcx);

void** fun_2680(void** rdi);

void fun_2610(void** rdi, void** rsi, ...);

void** fts_stat(void** rdi, void** rsi, signed char dl, ...);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void** rpl_fcntl();

struct s1 {
    signed char f0;
    void** f1;
};

struct s2 {
    void** f0;
    signed char[17] pad18;
    unsigned char f12;
    void** f13;
    signed char f14;
};

struct s2* fun_26c0(void** rdi, void** rsi);

void** fun_2540(void** rdi, ...);

void fun_2750(void** rdi, void** rsi, void** rdx, ...);

void** fts_build(void** rdi, void** rsi) {
    void** r14_3;
    void** rbp4;
    void** v5;
    void** rax6;
    void** v7;
    void** rax8;
    void** v9;
    void** eax10;
    int64_t rdx11;
    uint32_t edx12;
    int64_t rdi13;
    void** rcx14;
    void** rax15;
    void** r13_16;
    void** eax17;
    void** v18;
    void** rdi19;
    void** rax20;
    void** rax21;
    void* rax22;
    int64_t r8_23;
    uint64_t rax24;
    void* rax25;
    void** v26;
    void** rax27;
    void** edi28;
    void** v29;
    int32_t r12d30;
    int32_t ebx31;
    void** rax32;
    void** eax33;
    void** rdx34;
    int32_t eax35;
    void** rax36;
    void** rdi37;
    void** edx38;
    unsigned char v39;
    void** rcx40;
    void** v41;
    void** v42;
    void** v43;
    struct s1* rax44;
    void** r15_45;
    void** rax46;
    int1_t zf47;
    void** v48;
    void** v49;
    signed char v50;
    void** r12_51;
    void** rax52;
    void** rdi53;
    void** v54;
    void** rbx55;
    unsigned char v56;
    void** v57;
    void** v58;
    void** v59;
    struct s2* rax60;
    void** rax61;
    void** r14_62;
    void** rax63;
    void** rsi64;
    void** rax65;
    void** r15_66;
    uint32_t eax67;
    void** tmp64_68;
    void** rsi69;
    void** rax70;
    void** edx71;
    void** rdx72;
    void** eax73;
    int64_t rax74;
    int64_t rdx75;
    void** eax76;
    void** rax77;
    void** rax78;
    uint32_t eax79;
    int32_t eax80;
    void** rax81;
    void** rax82;
    uint32_t eax83;
    void** rbp84;
    void** rdi85;
    void** rbp86;
    void** rdi87;
    uint64_t rax88;
    uint32_t eax89;
    void** rdi90;
    void** rax91;
    void** rax92;
    void** rdx93;
    void** r13_94;
    void** r14_95;
    void** rbp96;
    void** ebx97;
    void** r12_98;
    void** rdi99;
    void** rdi100;
    void** r13_101;
    void** rbp102;
    void** r14_103;
    void** r12_104;
    void** rdi105;
    void** rdi106;
    void** v107;
    void** v108;

    r14_3 = rdi;
    rbp4 = *reinterpret_cast<void***>(rdi);
    v5 = rsi;
    rax6 = g28;
    v7 = rax6;
    rax8 = *reinterpret_cast<void***>(rbp4 + 24);
    v9 = rax8;
    if (!rax8) {
        eax10 = *reinterpret_cast<void***>(rdi + 72);
        *reinterpret_cast<uint32_t*>(&rdx11) = reinterpret_cast<unsigned char>(eax10) & 16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&rdx11) && (*reinterpret_cast<uint32_t*>(&rdx11) = 0x20000, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(&eax10) & 1))) {
            edx12 = 0;
            *reinterpret_cast<unsigned char*>(&edx12) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<void***>(rbp4 + 88));
            *reinterpret_cast<uint32_t*>(&rdx11) = edx12 << 17;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx11) + 4) = 0;
        }
        rsi = *reinterpret_cast<void***>(rbp4 + 48);
        *reinterpret_cast<void***>(&rdi13) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        if ((reinterpret_cast<unsigned char>(eax10) & 0x204) == 0x200) {
            *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(r14_3 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        }
        rcx14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x78 + 100);
        rax15 = opendirat(rdi13, rsi, rdx11, rcx14);
        *reinterpret_cast<void***>(rbp4 + 24) = rax15;
        r13_16 = rax15;
        if (rax15) 
            goto addr_49ba_7;
    } else {
        eax17 = fun_2680(rax8);
        v18 = eax17;
        if (reinterpret_cast<signed char>(eax17) < reinterpret_cast<signed char>(0)) {
            rdi19 = *reinterpret_cast<void***>(rbp4 + 24);
            fun_2610(rdi19, rsi);
            *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
            if (reinterpret_cast<int1_t>(v5 == 3)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
                rax20 = fun_2470();
                *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax20);
                goto addr_4d0a_11;
            }
        }
        if (!*reinterpret_cast<void***>(r14_3 + 64)) 
            goto addr_4db0_13; else 
            goto addr_4529_14;
    }
    if (!reinterpret_cast<int1_t>(v5 == 3)) {
        addr_4d0a_11:
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    } else {
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 4;
        rax21 = fun_2470();
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax21);
    }
    addr_4b21_17:
    rax22 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (rax22) {
        fun_2570();
    } else {
        return r13_16;
    }
    addr_49ba_7:
    if (*reinterpret_cast<uint16_t*>(rbp4 + 0x68) == 11) {
        rsi = rbp4;
        rax15 = fts_stat(r14_3, rsi, 0, r14_3, rsi, 0);
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&rax15);
        goto addr_49d0_22;
    }
    if (!(*reinterpret_cast<unsigned char*>(r14_3 + 73) & 1) || (leave_dir(r14_3, rbp4, r14_3, rbp4), fts_stat(r14_3, rbp4, 0, r14_3, rbp4, 0), rsi = rbp4, rax15 = enter_dir(r14_3, rsi, 0, rcx14, r8_23), !!*reinterpret_cast<signed char*>(&rax15))) {
        addr_49d0_22:
        rax24 = reinterpret_cast<unsigned char>(rax15) - (reinterpret_cast<unsigned char>(rax15) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(rax15) < reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(rax15) + static_cast<uint64_t>(reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 64)) < reinterpret_cast<unsigned char>(1)))))));
        *reinterpret_cast<uint32_t*>(&rax25) = *reinterpret_cast<uint32_t*>(&rax24) & 0x186a1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax25) + 4) = 0;
        v26 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rax25) - 1);
        if (v5 == 2) 
            goto addr_4d60_24;
    } else {
        rax27 = fun_2470();
        *reinterpret_cast<int32_t*>(&r13_16) = 0;
        *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
        *reinterpret_cast<void***>(rax27) = reinterpret_cast<void**>(12);
        goto addr_4b21_17;
    }
    edi28 = v29;
    if ((reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 56) != 24 || *reinterpret_cast<int64_t*>(rbp4 + 0x80) != 2) {
        addr_4a05_27:
        r12d30 = 1;
        *reinterpret_cast<unsigned char*>(&ebx31) = reinterpret_cast<uint1_t>(v5 == 3);
    } else {
        rsi = edi28;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rax32 = filesystem_type(rbp4, rsi, rbp4, rsi);
        if (rax32 == 0x9fa0) 
            goto addr_4f1a_29;
        if (reinterpret_cast<signed char>(rax32) > reinterpret_cast<signed char>(0x9fa0)) 
            goto addr_4f04_31; else 
            goto addr_4b8c_32;
    }
    addr_4a13_33:
    if (*reinterpret_cast<unsigned char*>(r14_3 + 73) & 2) {
        rsi = reinterpret_cast<void**>(0x406);
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        eax33 = rpl_fcntl();
        v18 = eax33;
        edi28 = eax33;
    }
    if (reinterpret_cast<signed char>(edi28) < reinterpret_cast<signed char>(0) || (rdx34 = edi28, *reinterpret_cast<int32_t*>(&rdx34 + 4) = 0, rsi = rbp4, eax35 = fts_safe_changedir(r14_3, rsi, rdx34, 0), !!eax35)) {
        if (*reinterpret_cast<unsigned char*>(&ebx31) && *reinterpret_cast<signed char*>(&r12d30)) {
            rax36 = fun_2470();
            *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(rax36);
        }
        *reinterpret_cast<unsigned char*>(rbp4 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(rbp4 + 0x6a) | 1);
        rdi37 = *reinterpret_cast<void***>(rbp4 + 24);
        fun_2610(rdi37, rsi, rdi37, rsi);
        edx38 = *reinterpret_cast<void***>(r14_3 + 72);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        if (*reinterpret_cast<unsigned char*>(&edx38 + 1) & 2 && reinterpret_cast<signed char>(v18) >= reinterpret_cast<signed char>(0)) {
            fun_25f0();
            edx38 = *reinterpret_cast<void***>(r14_3 + 72);
        }
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
        v39 = 0;
    } else {
        goto addr_4532_42;
    }
    addr_453b_43:
    rcx40 = *reinterpret_cast<void***>(rbp4 + 72);
    v41 = rcx40;
    v42 = rcx40 + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 56)) + reinterpret_cast<unsigned char>(rcx40) + 0xffffffffffffffff) != 47) {
        v42 = rcx40;
        v41 = rcx40 + 1;
    }
    v43 = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(edx38) & 4) {
        rax44 = reinterpret_cast<struct s1*>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 32)));
        rcx40 = reinterpret_cast<void**>(&rax44->f1);
        rax44->f0 = 47;
        v43 = rcx40;
    }
    r15_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 48)) - reinterpret_cast<unsigned char>(v41));
    rax46 = *reinterpret_cast<void***>(rbp4 + 88) + 1;
    zf47 = *reinterpret_cast<void***>(rbp4 + 24) == 0;
    v48 = *reinterpret_cast<void***>(rbp4 + 24);
    v49 = rax46;
    if (zf47) {
        if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4)) {
            *reinterpret_cast<int32_t*>(&r13_16) = 0;
            *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
            if (!(v39 & static_cast<unsigned char>(reinterpret_cast<uint1_t>(v9 == 0)))) 
                goto addr_4cd5_50;
            v50 = 0;
            *reinterpret_cast<int32_t*>(&r12_51) = 0;
            *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        } else {
            v50 = 0;
            *reinterpret_cast<int32_t*>(&r13_16) = 0;
            *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
            *reinterpret_cast<int32_t*>(&r12_51) = 0;
            *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
            goto addr_4c2b_53;
        }
    } else {
        rax52 = fun_2470();
        rdi53 = v48;
        *reinterpret_cast<int32_t*>(&r12_51) = 0;
        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
        v54 = rax52;
        rbx55 = r14_3;
        v50 = 0;
        v56 = 0;
        v57 = reinterpret_cast<void**>(0);
        v58 = rbp4;
        v59 = reinterpret_cast<void**>(0);
        while (*reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(0), rax60 = fun_26c0(rdi53, rsi), !!rax60) {
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 72)) & 32) 
                goto addr_4642_57;
            if (*reinterpret_cast<void***>(&rax60->f13) != 46) 
                goto addr_4642_57;
            if (!rax60->f14) {
                addr_4604_60:
                rax61 = v58;
                rdi53 = *reinterpret_cast<void***>(rax61 + 24);
                if (!rdi53) 
                    goto addr_4758_61; else 
                    continue;
            } else {
                if (rax60->f14 != 46) {
                    addr_4642_57:
                    r14_62 = reinterpret_cast<void**>(&rax60->f13);
                    rax63 = fun_2540(r14_62, r14_62);
                    rsi64 = r14_62;
                    rax65 = fts_alloc(rbx55, rsi64, rax63);
                    if (!rax65) 
                        goto addr_4aa8_63;
                } else {
                    goto addr_4604_60;
                }
            }
            if (reinterpret_cast<unsigned char>(rax63) >= reinterpret_cast<unsigned char>(r15_45)) {
                r15_66 = *reinterpret_cast<void***>(rbx55 + 32);
                rsi64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v42) + reinterpret_cast<unsigned char>(rax63) + 2);
                *reinterpret_cast<unsigned char*>(&eax67) = fts_palloc(rbx55, rsi64, rax63);
                if (!*reinterpret_cast<unsigned char*>(&eax67)) 
                    goto addr_4aa8_63;
                rsi64 = *reinterpret_cast<void***>(rbx55 + 32);
                if (rsi64 != r15_66) 
                    goto addr_482b_68;
            } else {
                addr_4674_69:
                tmp64_68 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax63) + reinterpret_cast<unsigned char>(v41));
                if (reinterpret_cast<unsigned char>(tmp64_68) < reinterpret_cast<unsigned char>(rax63)) 
                    goto addr_4f23_70; else 
                    goto addr_4682_71;
            }
            eax67 = v56;
            addr_483f_73:
            r15_45 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 48)) - reinterpret_cast<unsigned char>(v41));
            v56 = *reinterpret_cast<unsigned char*>(&eax67);
            goto addr_4674_69;
            addr_482b_68:
            rsi64 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi64) + reinterpret_cast<unsigned char>(v41));
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx55 + 72)) & 4)) {
                rsi64 = v43;
            }
            v43 = rsi64;
            goto addr_483f_73;
            addr_4682_71:
            rsi69 = rax65 + 0x100;
            *reinterpret_cast<void***>(rax65 + 88) = v49;
            rax70 = *reinterpret_cast<void***>(rbx55);
            *reinterpret_cast<void***>(rax65 + 72) = tmp64_68;
            edx71 = *reinterpret_cast<void***>(rbx55 + 72);
            *reinterpret_cast<void***>(rax65 + 8) = rax70;
            *reinterpret_cast<void***>(rax65 + 0x78) = rax60->f0;
            if (*reinterpret_cast<unsigned char*>(&edx71) & 4) {
                *reinterpret_cast<void***>(rax65 + 48) = *reinterpret_cast<void***>(rax65 + 56);
                rdx72 = *reinterpret_cast<void***>(rax65 + 96) + 1;
                fun_2750(v43, rsi69, rdx72);
                edx71 = *reinterpret_cast<void***>(rbx55 + 72);
            } else {
                *reinterpret_cast<void***>(rax65 + 48) = rsi69;
            }
            if (!*reinterpret_cast<void***>(rbx55 + 64) || *reinterpret_cast<unsigned char*>(&edx71 + 1) & 4) {
                eax73 = reinterpret_cast<void**>(static_cast<uint32_t>(rax60->f12));
                rsi = eax73;
                *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                *reinterpret_cast<void***>(&rax74) = eax73 - 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                if (!(*reinterpret_cast<unsigned char*>(&edx71) & 8) || !(*reinterpret_cast<unsigned char*>(&rsi) & 0xfb)) {
                    rsi = reinterpret_cast<void**>(11);
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax74)) <= reinterpret_cast<unsigned char>(11)) {
                        addr_4868_81:
                        *reinterpret_cast<int32_t*>(&rdx75) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
                    } else {
                        eax76 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rdx75) = 2;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
                        goto addr_46f8_83;
                    }
                } else {
                    if (!(reinterpret_cast<unsigned char>(edx71) & 16) && *reinterpret_cast<unsigned char*>(&rsi) == 10) {
                        *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                        goto addr_4868_81;
                    }
                    *reinterpret_cast<int32_t*>(&rcx40) = 11;
                    *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0;
                    *reinterpret_cast<uint16_t*>(rax65 + 0x68) = 11;
                    if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(&rax74)) <= reinterpret_cast<unsigned char>(11)) 
                        goto addr_4a80_87; else 
                        goto addr_4912_88;
                }
            } else {
                rsi = rax65;
                rax77 = fts_stat(rbx55, rsi, 0);
                *reinterpret_cast<uint16_t*>(rax65 + 0x68) = *reinterpret_cast<uint16_t*>(&rax77);
                goto addr_4706_90;
            }
            addr_486d_91:
            rcx40 = reinterpret_cast<void**>(0xcde0);
            eax76 = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0xcde0) + reinterpret_cast<uint64_t>(rax74 * 4));
            addr_46f8_83:
            *reinterpret_cast<void***>(rax65 + 0x88) = eax76;
            *reinterpret_cast<int64_t*>(rax65 + 0xa0) = rdx75;
            addr_4706_90:
            *reinterpret_cast<void***>(rax65 + 16) = reinterpret_cast<void**>(0);
            if (!v59) {
                v59 = rax65;
            } else {
                *reinterpret_cast<void***>(v57 + 16) = rax65;
            }
            if (!reinterpret_cast<int1_t>(r12_51 == fun_2710)) {
                ++r12_51;
                if (reinterpret_cast<unsigned char>(r12_51) >= reinterpret_cast<unsigned char>(v26)) 
                    goto addr_4d18_96;
                v57 = rax65;
                goto addr_4604_60;
            } else {
                if (!*reinterpret_cast<void***>(rbx55 + 64)) {
                    rsi = v18;
                    *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
                    rax78 = filesystem_type(v58, rsi);
                    if (rax78 == 0x1021994 || ((*reinterpret_cast<int32_t*>(&rcx40) = 0xff534d42, *reinterpret_cast<int32_t*>(&rcx40 + 4) = 0, rax78 == 0xff534d42) || rax78 == 0x6969)) {
                        v57 = rax65;
                        *reinterpret_cast<int32_t*>(&r12_51) = 0x2711;
                        *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
                        v50 = 0;
                        goto addr_4604_60;
                    } else {
                        v50 = 1;
                        goto addr_473b_102;
                    }
                } else {
                    addr_473b_102:
                    rax61 = v58;
                    v57 = rax65;
                    *reinterpret_cast<int32_t*>(&r12_51) = 0x2711;
                    *reinterpret_cast<int32_t*>(&r12_51 + 4) = 0;
                    rdi53 = *reinterpret_cast<void***>(rax61 + 24);
                    if (rdi53) 
                        continue; else 
                        goto addr_4758_61;
                }
            }
            addr_4a80_87:
            *reinterpret_cast<int32_t*>(&rdx75) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            goto addr_486d_91;
            addr_4912_88:
            eax76 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rdx75) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx75) + 4) = 0;
            goto addr_46f8_83;
        }
        goto addr_4bc0_103;
    }
    addr_4cb0_104:
    if (!*reinterpret_cast<void***>(rbp4 + 88)) {
        eax79 = restore_initial_cwd(r14_3, rsi);
        if (eax79) 
            goto addr_4dfd_106;
        goto addr_4cd0_108;
    }
    rsi = *reinterpret_cast<void***>(rbp4 + 8);
    rcx40 = reinterpret_cast<void**>("..");
    eax80 = fts_safe_changedir(r14_3, rsi, 0xffffffff, "..");
    if (!eax80) {
        addr_4cd0_108:
        if (r12_51) {
            addr_4c5c_110:
            if (v50) {
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0x39e0);
                rax81 = fts_sort(r14_3, r13_16, r12_51, rcx40);
                *reinterpret_cast<void***>(r14_3 + 64) = reinterpret_cast<void**>(0);
                r13_16 = rax81;
                goto addr_4b21_17;
            } else {
                if (*reinterpret_cast<void***>(r14_3 + 64) && r12_51 != 1) {
                    rax82 = fts_sort(r14_3, r13_16, r12_51, rcx40);
                    r13_16 = rax82;
                    goto addr_4b21_17;
                }
            }
        } else {
            addr_4cd5_50:
            if (v5 == 3 && ((eax83 = *reinterpret_cast<uint16_t*>(rbp4 + 0x68), *reinterpret_cast<int16_t*>(&eax83) != 7) && *reinterpret_cast<int16_t*>(&eax83) != 4)) {
                *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 6;
            }
        }
    } else {
        addr_4dfd_106:
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = 7;
        *reinterpret_cast<void***>(r14_3 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) | 0x2000);
        if (r13_16) {
            do {
                rbp84 = r13_16;
                r13_16 = *reinterpret_cast<void***>(r13_16 + 16);
                rdi85 = *reinterpret_cast<void***>(rbp84 + 24);
                if (rdi85) {
                    fun_2610(rdi85, rsi, rdi85, rsi);
                }
                fun_2440(rbp84, rsi, rbp84, rsi);
            } while (r13_16);
            goto addr_4d0a_11;
        }
    }
    if (r13_16) {
        do {
            rbp86 = r13_16;
            r13_16 = *reinterpret_cast<void***>(r13_16 + 16);
            rdi87 = *reinterpret_cast<void***>(rbp86 + 24);
            if (rdi87) {
                fun_2610(rdi87, rsi, rdi87, rsi);
            }
            fun_2440(rbp86, rsi, rbp86, rsi);
        } while (r13_16);
        goto addr_4d0a_11;
    }
    addr_4bc0_103:
    rbp4 = v58;
    r14_3 = rbx55;
    r13_16 = v59;
    if (*reinterpret_cast<void***>(v54)) {
        *reinterpret_cast<void***>(rbp4 + 64) = *reinterpret_cast<void***>(v54);
        rax88 = reinterpret_cast<unsigned char>(v9) | reinterpret_cast<unsigned char>(r12_51);
        eax89 = (*reinterpret_cast<uint32_t*>(&rax88) - (*reinterpret_cast<uint32_t*>(&rax88) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax88) < *reinterpret_cast<uint32_t*>(&rax88) + reinterpret_cast<uint1_t>(rax88 < 1))) & 0xfffffffd) + 7;
        *reinterpret_cast<uint16_t*>(rbp4 + 0x68) = *reinterpret_cast<uint16_t*>(&eax89);
    }
    rdi90 = *reinterpret_cast<void***>(rbp4 + 24);
    if (rdi90) {
        fun_2610(rdi90, rsi);
        *reinterpret_cast<void***>(rbp4 + 24) = reinterpret_cast<void**>(0);
    }
    addr_4c09_128:
    if (v56) {
        addr_476e_129:
        rax91 = *reinterpret_cast<void***>(r14_3 + 8);
        rcx40 = *reinterpret_cast<void***>(r14_3 + 32);
        if (rax91) {
            do {
                rsi = rax91 + 0x100;
                if (*reinterpret_cast<void***>(rax91 + 48) != rsi) {
                    *reinterpret_cast<void***>(rax91 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax91 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax91 + 56))) + reinterpret_cast<unsigned char>(rcx40));
                }
                *reinterpret_cast<void***>(rax91 + 56) = rcx40;
                rax91 = *reinterpret_cast<void***>(rax91 + 16);
            } while (rax91);
        }
    } else {
        addr_4c14_134:
        if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_3 + 72)) & 4) {
            if (*reinterpret_cast<void***>(r14_3 + 48) == v41 || !r12_51) {
                addr_4c2b_53:
                --v43;
                goto addr_4c31_136;
            } else {
                addr_4c31_136:
                *reinterpret_cast<void***>(v43) = reinterpret_cast<void**>(0);
                goto addr_4c39_137;
            }
        }
    }
    rax92 = r13_16;
    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r13_16 + 88)) >= reinterpret_cast<signed char>(0)) {
        do {
            rsi = rax92 + 0x100;
            if (*reinterpret_cast<void***>(rax92 + 48) != rsi) {
                *reinterpret_cast<void***>(rax92 + 48) = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax92 + 48)) - reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax92 + 56))) + reinterpret_cast<unsigned char>(rcx40));
            }
            rdx93 = *reinterpret_cast<void***>(rax92 + 16);
            *reinterpret_cast<void***>(rax92 + 56) = rcx40;
            if (rdx93) {
                rax92 = rdx93;
            } else {
                rax92 = *reinterpret_cast<void***>(rax92 + 8);
            }
        } while (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rax92 + 88)) >= reinterpret_cast<signed char>(0));
        goto addr_4c14_134;
    } else {
        goto addr_4c14_134;
    }
    addr_4c39_137:
    if (v9) 
        goto addr_4cd0_108;
    if (!v39) 
        goto addr_4cd0_108;
    if (v5 == 1) 
        goto addr_4cb0_104;
    if (!r12_51) 
        goto addr_4cb0_104; else 
        goto addr_4c5c_110;
    addr_4aa8_63:
    r13_94 = v59;
    r14_95 = rbx55;
    rbp96 = v58;
    ebx97 = *reinterpret_cast<void***>(v54);
    fun_2440(rax65, rsi64, rax65, rsi64);
    if (r13_94) {
        do {
            r12_98 = r13_94;
            r13_94 = *reinterpret_cast<void***>(r13_94 + 16);
            rdi99 = *reinterpret_cast<void***>(r12_98 + 24);
            if (rdi99) {
                fun_2610(rdi99, rsi64, rdi99, rsi64);
            }
            fun_2440(r12_98, rsi64, r12_98, rsi64);
        } while (r13_94);
    }
    rdi100 = *reinterpret_cast<void***>(rbp96 + 24);
    *reinterpret_cast<int32_t*>(&r13_16) = 0;
    *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    fun_2610(rdi100, rsi64, rdi100, rsi64);
    *reinterpret_cast<void***>(rbp96 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp96 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_95 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_95 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v54) = ebx97;
    goto addr_4b21_17;
    addr_4f23_70:
    r13_101 = v59;
    rbp102 = v58;
    r14_103 = rbx55;
    fun_2440(rax65, rsi64);
    if (r13_101) {
        do {
            r12_104 = r13_101;
            r13_101 = *reinterpret_cast<void***>(r13_101 + 16);
            rdi105 = *reinterpret_cast<void***>(r12_104 + 24);
            if (rdi105) {
                fun_2610(rdi105, rsi64);
            }
            fun_2440(r12_104, rsi64);
        } while (r13_101);
    }
    rdi106 = *reinterpret_cast<void***>(rbp102 + 24);
    *reinterpret_cast<int32_t*>(&r13_16) = 0;
    *reinterpret_cast<int32_t*>(&r13_16 + 4) = 0;
    fun_2610(rdi106, rsi64);
    *reinterpret_cast<void***>(rbp102 + 24) = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint16_t*>(rbp102 + 0x68) = 7;
    *reinterpret_cast<void***>(r14_103 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_103 + 72)) | 0x2000);
    *reinterpret_cast<void***>(v54) = reinterpret_cast<void**>(36);
    goto addr_4b21_17;
    addr_4d18_96:
    rbp4 = v58;
    r13_16 = v59;
    r14_3 = rbx55;
    goto addr_4c09_128;
    addr_4758_61:
    r13_16 = v59;
    rbp4 = rax61;
    r14_3 = rbx55;
    if (!v56) 
        goto addr_4c14_134; else 
        goto addr_476e_129;
    addr_4532_42:
    v39 = 1;
    edx38 = *reinterpret_cast<void***>(r14_3 + 72);
    goto addr_453b_43;
    addr_4f04_31:
    if (rax32 == 0x5346414f || reinterpret_cast<int1_t>(rax32 == 0xff534d42)) {
        addr_4f1a_29:
        edi28 = v107;
        goto addr_4a05_27;
    } else {
        addr_4ba1_158:
        if (!reinterpret_cast<int1_t>(v5 == 3)) {
            addr_4d60_24:
            v39 = 0;
            edx38 = *reinterpret_cast<void***>(r14_3 + 72);
            goto addr_453b_43;
        } else {
            edi28 = v108;
            r12d30 = 0;
            ebx31 = 1;
            goto addr_4a13_33;
        }
    }
    addr_4b8c_32:
    if (!rax32) 
        goto addr_4f1a_29;
    if (rax32 == 0x6969) 
        goto addr_4f1a_29; else 
        goto addr_4ba1_158;
    addr_4db0_13:
    v26 = reinterpret_cast<void**>(0x186a0);
    edx38 = *reinterpret_cast<void***>(r14_3 + 72);
    v39 = 1;
    goto addr_453b_43;
    addr_4529_14:
    v26 = reinterpret_cast<void**>(0xffffffffffffffff);
    goto addr_4532_42;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0xce80);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0xce80);
    if (*reinterpret_cast<void***>(rdi + 40) != 0xce80) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x6ef8]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0xce80);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x6d5f]");
        if (1) 
            goto addr_622c_6;
        __asm__("comiss xmm1, [rip+0x6d56]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x6d48]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_620c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_6202_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_620c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_6202_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_6202_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_620c_13:
        return 0;
    } else {
        addr_622c_6:
        return r8_3;
    }
}

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s4 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int32_t transfer_entries(void** rdi, void** rsi, int32_t edx) {
    void** r14_4;
    int32_t r12d5;
    void** rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rdi12;
    void** rax13;
    struct s3* rax14;
    void** rdx15;
    void** rax16;
    void** rsi17;
    void** rax18;
    struct s4* r13_19;
    void** rax20;
    void** rdx21;

    r14_4 = rdi;
    r12d5 = edx;
    rbp6 = rsi;
    rbx7 = *reinterpret_cast<void***>(rsi);
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8))) {
        do {
            addr_6296_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_6288_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_6296_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = *reinterpret_cast<void***>(r14_4 + 16);
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rdi12 = r15_11, rax13 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi10)), rsi10 = *reinterpret_cast<void***>(r14_4 + 16), reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax14 = reinterpret_cast<struct s3*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
                        rdx15 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax14->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14->f8;
                            rax14->f8 = r13_9;
                            if (!rdx15) 
                                goto addr_630e_9;
                        } else {
                            rax14->f0 = r15_11;
                            rax16 = *reinterpret_cast<void***>(r14_4 + 72);
                            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax16;
                            *reinterpret_cast<void***>(r14_4 + 72) = r13_9;
                            if (!rdx15) 
                                goto addr_630e_9;
                        }
                        r13_9 = rdx15;
                    }
                    goto addr_2889_12;
                    addr_630e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_6288_3;
            }
            rsi17 = *reinterpret_cast<void***>(r14_4 + 16);
            rdi12 = r15_8;
            rax18 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi17));
            if (reinterpret_cast<unsigned char>(rax18) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 16))) 
                goto addr_2889_12;
            r13_19 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax18) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
            if (r13_19->f0) 
                goto addr_6348_16;
            r13_19->f0 = r15_8;
            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
            continue;
            addr_6348_16:
            rax20 = *reinterpret_cast<void***>(r14_4 + 72);
            if (!rax20) {
                rax20 = fun_26d0(16, 16);
                if (!rax20) 
                    goto addr_63ba_19;
            } else {
                *reinterpret_cast<void***>(r14_4 + 72) = *reinterpret_cast<void***>(rax20 + 8);
            }
            rdx21 = r13_19->f8;
            *reinterpret_cast<void***>(rax20) = r15_8;
            *reinterpret_cast<void***>(rax20 + 8) = rdx21;
            r13_19->f8 = rax20;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            *reinterpret_cast<void***>(rbp6 + 24) = *reinterpret_cast<void***>(rbp6 + 24) - 1;
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_2889_12:
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    fun_2460(rdi12, rdi12);
    addr_63ba_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void*** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
        fun_2460(r13_5, r13_5);
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *rdx = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_60bf_23:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_6064_26;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_60d0_30;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_60d0_30;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_60bf_23;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_6064_26;
            }
        }
    }
    addr_60c1_34:
    return rax11;
    addr_6064_26:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_60c1_34;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_60d0_30:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_2530();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2530();
    if (r8d > 10) {
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xcf60 + rax11 * 4) + 0xcf60;
    }
}

struct s5 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_25d0();

struct s6 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s5* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s6* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x888f;
    rax8 = fun_2470();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
        fun_2460(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x11070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x8641]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x891b;
            fun_25d0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s6*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x11120) {
                fun_2440(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x89aa);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2570();
        } else {
            return r14_19;
        }
    }
}

void fun_25c0(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

/* cdb_free.part.0 */
void cdb_free_part_0(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx) {
    fun_25c0("! close_fail", "lib/chdir-long.c", 64, "cdb_free");
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x11080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s7 {
    void** f0;
    signed char[135] pad136;
    int64_t f88;
};

int32_t fun_2840(int64_t rdi, ...);

void** fts_stat(void** rdi, void** rsi, signed char dl, ...) {
    struct s7* rbp4;
    void** eax5;
    int64_t rdi6;
    int32_t eax7;
    void** rax8;
    void** eax9;
    int64_t rdi10;
    int32_t eax11;
    uint32_t eax12;
    void** rax13;
    int64_t rax14;
    int64_t rdi15;
    int32_t eax16;
    void** rax17;
    int64_t* rdi18;
    int64_t rcx19;

    rbp4 = reinterpret_cast<struct s7*>(rsi + 0x70);
    eax5 = *reinterpret_cast<void***>(rdi + 72);
    if (*reinterpret_cast<unsigned char*>(&eax5) & 2) 
        goto addr_3a30_2;
    if (*reinterpret_cast<unsigned char*>(&eax5) & 1 && !*reinterpret_cast<void***>(rsi + 88)) {
        goto addr_3a30_2;
    }
    if (dl) {
        addr_3a30_2:
        *reinterpret_cast<void***>(&rdi6) = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
        eax7 = fun_2840(rdi6);
        if (eax7 < 0) {
            rax8 = fun_2470();
            eax9 = *reinterpret_cast<void***>(rax8);
            if (eax9 == 2) {
                *reinterpret_cast<void***>(&rdi10) = *reinterpret_cast<void***>(rdi + 44);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi10) + 4) = 0;
                eax11 = fun_2840(rdi10, rdi10);
                if (eax11 < 0) {
                    eax9 = *reinterpret_cast<void***>(rax8);
                } else {
                    *reinterpret_cast<void***>(rax8) = reinterpret_cast<void**>(0);
                    return 13;
                }
            }
        } else {
            addr_3a47_10:
            eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x88)) & 0xf000;
            if (eax12 == 0x4000) {
                *reinterpret_cast<uint32_t*>(&rax13) = 1;
                *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
                if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi + 0x100) == 46) && (!*reinterpret_cast<signed char*>(rsi + 0x101) || (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 0x100)) & 0xffff00) == 0x2e00)) {
                    *reinterpret_cast<uint32_t*>(&rax13) = (1 - (1 + reinterpret_cast<uint1_t>(1 < 1 + reinterpret_cast<uint1_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 88)) < reinterpret_cast<unsigned char>(1)))) & 0xfffffffc) + 5;
                    *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
                    goto addr_3a77_13;
                }
            } else {
                if (eax12 == 0xa000) {
                    *reinterpret_cast<uint32_t*>(&rax13) = 12;
                    *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
                    goto addr_3a77_13;
                } else {
                    *reinterpret_cast<uint32_t*>(&rax14) = reinterpret_cast<uint1_t>(eax12 == 0x8000);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax14) + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rax13) = static_cast<uint32_t>(rax14 + rax14 * 4 + 3);
                    *reinterpret_cast<int32_t*>(&rax13 + 4) = 0;
                    goto addr_3a77_13;
                }
            }
        }
    } else {
        *reinterpret_cast<void***>(&rdi15) = *reinterpret_cast<void***>(rdi + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        eax16 = fun_2840(rdi15, rdi15);
        if (eax16 >= 0) 
            goto addr_3a47_10;
        rax17 = fun_2470();
        eax9 = *reinterpret_cast<void***>(rax17);
    }
    *reinterpret_cast<void***>(rsi + 64) = eax9;
    rdi18 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbp4) + 8 & 0xfffffffffffffff8);
    *reinterpret_cast<void***>(rsi + 0x70) = reinterpret_cast<void**>(0);
    rbp4->f88 = 0;
    *reinterpret_cast<uint32_t*>(&rcx19) = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rbp4) - reinterpret_cast<uint64_t>(rdi18) + 0x90) >> 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx19) + 4) = 0;
    while (rcx19) {
        --rcx19;
        *rdi18 = 0;
        ++rdi18;
    }
    return 10;
    addr_3a77_13:
    return rax13;
}

void** cycle_check(void** rdi, struct s7* rsi);

void** enter_dir(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8) {
    void** rdi6;
    void** rax7;
    void** rax8;
    void** rax9;
    void** rdi10;
    void** rax11;
    void** rax12;
    void** rax13;

    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 72)) & 0x102)) {
        rdi6 = *reinterpret_cast<void***>(rdi + 88);
        rax7 = cycle_check(rdi6, rsi + 0x70);
        if (*reinterpret_cast<signed char*>(&rax7)) {
            *reinterpret_cast<void***>(rsi) = rsi;
            *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
            return rax7;
        }
    } else {
        rax8 = fun_26d0(24);
        if (!rax8) 
            goto addr_4088_5;
        rax9 = *reinterpret_cast<void***>(rsi + 0x70);
        rdi10 = *reinterpret_cast<void***>(rdi + 88);
        *reinterpret_cast<void***>(rax8 + 16) = rsi;
        *reinterpret_cast<void***>(rax8) = rax9;
        *reinterpret_cast<void***>(rax8 + 8) = *reinterpret_cast<void***>(rsi + 0x78);
        rax11 = hash_insert(rdi10, rax8, rdx, rcx, r8);
        if (rax8 != rax11) 
            goto addr_4033_7;
    }
    addr_4050_8:
    *reinterpret_cast<int32_t*>(&rax12) = 1;
    *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
    addr_4055_9:
    return rax12;
    addr_4033_7:
    fun_2440(rax8, rax8);
    if (!rax11) {
        addr_4088_5:
        *reinterpret_cast<int32_t*>(&rax12) = 0;
        *reinterpret_cast<int32_t*>(&rax12 + 4) = 0;
        goto addr_4055_9;
    } else {
        rax13 = *reinterpret_cast<void***>(rax11 + 16);
        *reinterpret_cast<uint16_t*>(rsi + 0x68) = 2;
        *reinterpret_cast<void***>(rsi) = rax13;
        goto addr_4050_8;
    }
}

struct s8 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s8* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s8* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xcf01);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xcefc);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xcf05);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xcef8);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g10db8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g10db8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2403() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2413() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2423() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2433() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2443() {
    __asm__("cli ");
    goto free;
}

int64_t __cxa_finalize = 0;

void fun_2453() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t abort = 0x2030;

void fun_2463() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2040;

void fun_2473() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2050;

void fun_2483() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2060;

void fun_2493() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x2070;

void fun_24a3() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x2080;

void fun_24b3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t qsort = 0x2090;

void fun_24c3() {
    __asm__("cli ");
    goto qsort;
}

int64_t reallocarray = 0x20a0;

void fun_24d3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t fcntl = 0x20b0;

void fun_24e3() {
    __asm__("cli ");
    goto fcntl;
}

int64_t textdomain = 0x20c0;

void fun_24f3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2503() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2513() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2523() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2533() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2543() {
    __asm__("cli ");
    goto strlen;
}

int64_t openat = 0x2120;

void fun_2553() {
    __asm__("cli ");
    goto openat;
}

int64_t chdir = 0x2130;

void fun_2563() {
    __asm__("cli ");
    goto chdir;
}

int64_t __stack_chk_fail = 0x2140;

void fun_2573() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2150;

void fun_2583() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2160;

void fun_2593() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strrchr = 0x2170;

void fun_25a3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2180;

void fun_25b3() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x2190;

void fun_25c3() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21a0;

void fun_25d3() {
    __asm__("cli ");
    goto memset;
}

int64_t getcwd = 0x21b0;

void fun_25e3() {
    __asm__("cli ");
    goto getcwd;
}

int64_t close = 0x21c0;

void fun_25f3() {
    __asm__("cli ");
    goto close;
}

int64_t strspn = 0x21d0;

void fun_2603() {
    __asm__("cli ");
    goto strspn;
}

int64_t closedir = 0x21e0;

void fun_2613() {
    __asm__("cli ");
    goto closedir;
}

int64_t memchr = 0x21f0;

void fun_2623() {
    __asm__("cli ");
    goto memchr;
}

int64_t lstat = 0x2200;

void fun_2633() {
    __asm__("cli ");
    goto lstat;
}

int64_t memcmp = 0x2210;

void fun_2643() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2220;

void fun_2653() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2230;

void fun_2663() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2240;

void fun_2673() {
    __asm__("cli ");
    goto strcmp;
}

int64_t dirfd = 0x2250;

void fun_2683() {
    __asm__("cli ");
    goto dirfd;
}

int64_t fputc_unlocked = 0x2260;

void fun_2693() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t memcpy = 0x2270;

void fun_26a3() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2280;

void fun_26b3() {
    __asm__("cli ");
    goto fileno;
}

int64_t readdir = 0x2290;

void fun_26c3() {
    __asm__("cli ");
    goto readdir;
}

int64_t malloc = 0x22a0;

void fun_26d3() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x22b0;

void fun_26e3() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22c0;

void fun_26f3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22d0;

void fun_2703() {
    __asm__("cli ");
    goto __freading;
}

int64_t fchdir = 0x22e0;

void fun_2713() {
    __asm__("cli ");
    goto fchdir;
}

int64_t realloc = 0x22f0;

void fun_2723() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2300;

void fun_2733() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x2310;

void fun_2743() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t memmove = 0x2320;

void fun_2753() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2330;

void fun_2763() {
    __asm__("cli ");
    goto error;
}

int64_t memrchr = 0x2340;

void fun_2773() {
    __asm__("cli ");
    goto memrchr;
}

int64_t open = 0x2350;

void fun_2783() {
    __asm__("cli ");
    goto open;
}

int64_t access = 0x2360;

void fun_2793() {
    __asm__("cli ");
    goto access;
}

int64_t fseeko = 0x2370;

void fun_27a3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t fdopendir = 0x2380;

void fun_27b3() {
    __asm__("cli ");
    goto fdopendir;
}

int64_t fstatfs = 0x2390;

void fun_27c3() {
    __asm__("cli ");
    goto fstatfs;
}

int64_t __cxa_atexit = 0x23a0;

void fun_27d3() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x23b0;

void fun_27e3() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x23c0;

void fun_27f3() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x23d0;

void fun_2803() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x23e0;

void fun_2813() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23f0;

void fun_2823() {
    __asm__("cli ");
    goto iswprint;
}

int64_t fstat = 0x2400;

void fun_2833() {
    __asm__("cli ");
    goto fstat;
}

int64_t fstatat = 0x2410;

void fun_2843() {
    __asm__("cli ");
    goto fstatat;
}

int64_t __ctype_b_loc = 0x2420;

void fun_2853() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

int64_t __sprintf_chk = 0x2430;

void fun_2863() {
    __asm__("cli ");
    goto __sprintf_chk;
}

void set_program_name(void** rdi);

void** fun_2730(int64_t rdi, ...);

void fun_2510(int64_t rdi, int64_t rsi);

struct s9 {
    int32_t f0;
    signed char[40] pad44;
    int32_t f2c;
};

void fun_24f0(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2580(int64_t rdi, void** rsi, void** rdx, void** rcx);

int64_t stdout = 0;

int64_t Version = 0xcda4;

void version_etc(int64_t rdi, int64_t rsi, void** rdx, int64_t rcx, void** r8, int64_t r9, int64_t a7, int64_t a8);

int32_t fun_27e0();

void usage();

unsigned char recurse = 0;

void** fun_2520();

void fun_2760();

unsigned char affect_symlink_referent = 0;

int32_t optind = 0;

void** quotearg_style(int64_t rdi, void** rsi, void** rdx, ...);

void** quote(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

int32_t fun_2670(void** rdi, int64_t rsi, void** rdx, ...);

void** quotearg_n_style();

void rpl_fts_set(struct s9* rdi, void** rsi, void** rdx, ...);

void** rpl_fts_read(struct s9* rdi, ...);

signed char verbose = 0;

void fun_2740(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9, int64_t a7, void** a8, int64_t a9, void** a10, int64_t a11, void** a12, int64_t a13, int64_t a14, int64_t a15, int64_t a16, int64_t a17, int64_t a18, int64_t a19, int64_t a20);

void** specified_context = reinterpret_cast<void**>(0);

int32_t lgetfileconat(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

int32_t getfileconat(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

int32_t lsetfileconat(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

int32_t setfileconat(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

void** quote_n(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

int32_t rpl_fts_close(struct s9* rdi, void** rsi, void** rdx, void** rcx, void** r8, int64_t r9);

struct s10 {
    void** f0;
    signed char[7] pad8;
    int64_t f8;
};

struct s10* root_dev_ino = reinterpret_cast<struct s10*>(0);

struct s10* get_root_dev_ino(int64_t rdi, void** rsi, void** rdx, void** rcx);

struct s9* xfts_open(void*** rdi);

int64_t fun_28f3(int32_t edi, void** rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r15_5;
    void** r15_6;
    int64_t v7;
    int64_t r14_8;
    void** r14_9;
    int64_t v10;
    int64_t r13_11;
    void** r13_12;
    int64_t v13;
    int64_t r12_14;
    void** r12_15;
    int64_t v16;
    int64_t rbp17;
    int64_t v18;
    int64_t rbx19;
    void** rbx20;
    void** rdi21;
    void** rax22;
    void** v23;
    struct s9* rbp24;
    void* rsp25;
    int64_t v26;
    void** v27;
    void** v28;
    void** r8_29;
    void** rcx30;
    void** rdx31;
    void** rsi32;
    int64_t rdi33;
    int32_t eax34;
    void* rsp35;
    int64_t rdi36;
    int64_t rcx37;
    uint32_t eax38;
    uint32_t edx39;
    void** rax40;
    void** rdx41;
    void* rsp42;
    void** rdx43;
    void** rax44;
    void** rax45;
    void** rax46;
    void** rax47;
    void** rsi48;
    int64_t rdi49;
    void** rdx50;
    int1_t less_or_equal51;
    void** rax52;
    void* rsp53;
    int64_t rdi54;
    void** rax55;
    void** rax56;
    int32_t eax57;
    void* rsp58;
    void** rax59;
    void* rsp60;
    void** rax61;
    void* rsp62;
    int1_t zf63;
    void** rax64;
    int1_t zf65;
    void** rax66;
    void** rax67;
    int64_t v68;
    int64_t v69;
    uint32_t eax70;
    int32_t eax71;
    void* rsp72;
    void** rax73;
    void* rsp74;
    void** rax75;
    int32_t eax76;
    void** rax77;
    void** rax78;
    void** rax79;
    int32_t eax80;
    void* rsp81;
    int64_t rax82;
    int64_t rax83;
    void** rax84;
    struct s10* rax85;
    int64_t rax86;
    void*** rdi87;
    struct s9* rax88;
    void** rax89;
    void** rax90;
    int64_t rax91;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r15_5;
    *reinterpret_cast<int32_t*>(&r15_6) = 0;
    *reinterpret_cast<int32_t*>(&r15_6 + 4) = 0;
    v7 = r14_8;
    r14_9 = reinterpret_cast<void**>(0x109a0);
    v10 = r13_11;
    r13_12 = reinterpret_cast<void**>("HLPRhvu:r:t:l:");
    v13 = r12_14;
    r12_15 = reinterpret_cast<void**>(static_cast<int64_t>(edi));
    v16 = rbp17;
    v18 = rbx19;
    rbx20 = rsi;
    rdi21 = *reinterpret_cast<void***>(rsi);
    rax22 = g28;
    v23 = rax22;
    set_program_name(rdi21);
    fun_2730(6, 6);
    fun_2510("coreutils", "/usr/local/share/locale");
    rbp24 = reinterpret_cast<struct s9*>(0xcc88);
    fun_24f0("coreutils", "/usr/local/share/locale");
    atexit(0x38c0, "/usr/local/share/locale");
    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(&v26) + 3) = 0;
    v27 = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&v26) + 4) = -1;
    *reinterpret_cast<uint32_t*>(&v28) = 16;
    while (*reinterpret_cast<int32_t*>(&r8_29) = 0, *reinterpret_cast<int32_t*>(&r8_29 + 4) = 0, rcx30 = reinterpret_cast<void**>(0x109a0), rdx31 = reinterpret_cast<void**>("HLPRhvu:r:t:l:"), rsi32 = rbx20, *reinterpret_cast<uint32_t*>(&rdi33) = *reinterpret_cast<uint32_t*>(&r12_15), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi33) + 4) = 0, eax34 = fun_2580(rdi33, rsi32, "HLPRhvu:r:t:l:", 0x109a0), rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8), eax34 != -1) {
        if (eax34 > 0x83) 
            goto addr_3192_4;
        if (eax34 > 71) 
            goto addr_29c9_6;
        if (eax34 == 0xffffff7d) {
            rdi36 = stdout;
            rcx37 = Version;
            r8_29 = reinterpret_cast<void**>("Russell Coker");
            rdx31 = reinterpret_cast<void**>("GNU coreutils");
            version_etc(rdi36, "chcon", "GNU coreutils", rcx37, "Russell Coker", "Jim Meyering", 0, 0x29ac);
            eax34 = fun_27e0();
            rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 - 8 - 8 + 8 - 8 + 8);
        }
        if (eax34 != 0xffffff7e) 
            goto addr_3192_4;
        usage();
        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
        *reinterpret_cast<uint32_t*>(&v28) = 2;
    }
    eax38 = recurse;
    if (!*reinterpret_cast<signed char*>(&eax38)) {
        *reinterpret_cast<uint32_t*>(&v28) = 16;
        *reinterpret_cast<unsigned char*>(&edx39) = 1;
        goto addr_2afe_13;
    }
    if (*reinterpret_cast<uint32_t*>(&v28) != 16) {
        edx39 = eax38;
        if (!1) {
            rax40 = fun_2520();
            rdx41 = rax40;
            fun_2760();
            rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
            goto addr_2ed7_17;
        }
    }
    edx39 = 0;
    if (1) {
        addr_2afe_13:
        affect_symlink_referent = *reinterpret_cast<unsigned char*>(&edx39);
        rdx43 = reinterpret_cast<void**>(static_cast<int64_t>(optind));
        *reinterpret_cast<uint32_t*>(&rcx30) = *reinterpret_cast<uint32_t*>(&r12_15) - *reinterpret_cast<uint32_t*>(&rdx43);
        *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
        if (0) {
            if (reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rcx30) < reinterpret_cast<int32_t>(0)) | reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rcx30) == 0)) {
                addr_3168_20:
                if (*reinterpret_cast<int32_t*>(&rdx43) >= *reinterpret_cast<int32_t*>(&r12_15)) {
                    rax44 = fun_2520();
                    rdx31 = rax44;
                    fun_2760();
                    rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
                    goto addr_3192_4;
                }
            } else {
                if (!1) {
                    rax45 = fun_2470();
                    *reinterpret_cast<void***>(rax45) = reinterpret_cast<void**>(95);
                    rbx20 = rax45;
                    rax46 = quotearg_style(4, 0, rdx43);
                    r12_15 = rax46;
                    rax47 = fun_2520();
                    rsi48 = *reinterpret_cast<void***>(rbx20);
                    *reinterpret_cast<int32_t*>(&rsi48 + 4) = 0;
                    rcx30 = r12_15;
                    *reinterpret_cast<int32_t*>(&rdi49) = 1;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi49) + 4) = 0;
                    rdx50 = rax47;
                    fun_2760();
                    rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    goto addr_3054_24;
                }
            }
        } else {
            less_or_equal51 = *reinterpret_cast<int32_t*>(&rcx30) <= reinterpret_cast<int32_t>(1);
            *reinterpret_cast<uint32_t*>(&rcx30) = *reinterpret_cast<uint32_t*>(&rcx30) - 1;
            *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
            if (less_or_equal51) 
                goto addr_3168_20;
            optind = static_cast<int32_t>(reinterpret_cast<uint64_t>(rdx43 + 1));
            v27 = *reinterpret_cast<void***>(rbx20 + reinterpret_cast<unsigned char>(rdx43) * 8);
            goto addr_2b40_27;
        }
    } else {
        rax52 = fun_2520();
        rsi32 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
        rdx43 = rax52;
        fun_2760();
        rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
        goto addr_310f_29;
    }
    while (1) {
        rdi54 = *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rbx20 + reinterpret_cast<unsigned char>(r12_15) * 8) - 8);
        rax55 = quote(rdi54, rsi32, rdx43, rcx30, r8_29, "Jim Meyering");
        r12_15 = rax55;
        rax56 = fun_2520();
        rdx31 = rax56;
        fun_2760();
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8 - 8 + 8);
        addr_3192_4:
        usage();
        eax57 = fun_2670(r14_9, "/", rdx31, r14_9, "/", rdx31);
        rsp58 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8 - 8 + 8);
        if (eax57) {
            rax59 = quotearg_n_style();
            v28 = rax59;
            quotearg_n_style();
            fun_2520();
            r8_29 = v28;
            fun_2760();
            rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
        } else {
            quotearg_style(4, r14_9, rdx31, 4, r14_9, rdx31);
            fun_2520();
            fun_2760();
            rsp60 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp58) - 8 + 8 - 8 + 8 - 8 + 8);
        }
        rax61 = fun_2520();
        rsi32 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
        rdx41 = rax61;
        fun_2760();
        rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp60) - 8 + 8 - 8 + 8);
        *reinterpret_cast<uint32_t*>(&rcx30) = 0;
        *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
        while (1) {
            zf63 = recurse == 0;
            if (zf63) {
                *reinterpret_cast<int32_t*>(&rdx41) = 4;
                *reinterpret_cast<int32_t*>(&rdx41 + 4) = 0;
                rsi32 = r15_6;
                *reinterpret_cast<unsigned char*>(&v28) = *reinterpret_cast<unsigned char*>(&rcx30);
                rpl_fts_set(rbp24, rsi32, 4);
                rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp62) - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rcx30) = *reinterpret_cast<unsigned char*>(&v28);
                *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx20) = *reinterpret_cast<uint32_t*>(&rbx20) & *reinterpret_cast<uint32_t*>(&rcx30);
            } else {
                *reinterpret_cast<uint32_t*>(&rbx20) = *reinterpret_cast<uint32_t*>(&rbx20) & *reinterpret_cast<uint32_t*>(&rcx30);
            }
            rax64 = rpl_fts_read(rbp24, rbp24);
            rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp62) - 8 + 8);
            r15_6 = rax64;
            if (!rax64) 
                break;
            addr_2ba8_39:
            r14_9 = *reinterpret_cast<void***>(rax64 + 56);
            r13_12 = *reinterpret_cast<void***>(rax64 + 48);
            if (*reinterpret_cast<uint16_t*>(rax64 + 0x68) <= 10) 
                goto addr_2bb7_40;
            zf65 = verbose == 0;
            if (!zf65) {
                addr_2ed7_17:
                rax66 = quotearg_style(4, r14_9, rdx41, 4, r14_9, rdx41);
                rax67 = fun_2520();
                fun_2740(1, rax67, rax66, rcx30, r8_29, "Jim Meyering", v68, v28, v26, v27, 0, v23, v69, v18, v16, v13, v10, v7, v4, v3);
                rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8 - 8 + 8 - 8 + 8);
            }
            r14_9 = specified_context;
            *reinterpret_cast<int32_t*>(&rdi49) = rbp24->f2c;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi49) + 4) = 0;
            eax70 = affect_symlink_referent;
            if (!r14_9) {
                rdx50 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp42) + 32);
                rsi48 = r13_12;
                if (!*reinterpret_cast<signed char*>(&eax70)) {
                    addr_3054_24:
                    eax71 = lgetfileconat(rdi49, rsi48, rdx50, rcx30, r8_29, "Jim Meyering");
                    rsp72 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
                } else {
                    eax71 = getfileconat(rdi49, rsi48, rdx50, rcx30, r8_29, "Jim Meyering");
                    rsp72 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
                }
                if (eax71 >= 0 || *reinterpret_cast<void***>(r12_15) == 61) {
                    if (1) {
                        quotearg_style(4, r13_12, rdx50, 4, r13_12, rdx50);
                        rax73 = fun_2520();
                        rsi32 = reinterpret_cast<void**>(0);
                        *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
                        rdx41 = rax73;
                        fun_2760();
                        rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp72) - 8 + 8 - 8 + 8 - 8 + 8);
                        *reinterpret_cast<uint32_t*>(&rcx30) = 0;
                        *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
                        continue;
                    } else {
                        *reinterpret_cast<void***>(r12_15) = reinterpret_cast<void**>(95);
                        quote(0, rsi48, rdx50, rcx30, r8_29, "Jim Meyering");
                        rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp72) - 8 + 8);
                    }
                } else {
                    quotearg_style(4, r13_12, rdx50, 4, r13_12, rdx50);
                    rsp74 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp72) - 8 + 8);
                }
                rax75 = fun_2520();
                rsi32 = *reinterpret_cast<void***>(r12_15);
                *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
                rdx41 = rax75;
                fun_2760();
                rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp74) - 8 + 8 - 8 + 8);
                *reinterpret_cast<uint32_t*>(&rcx30) = 0;
                *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
            } else {
                rdx41 = r14_9;
                rsi32 = r13_12;
                if (!*reinterpret_cast<signed char*>(&eax70)) {
                    eax76 = lsetfileconat(rdi49, rsi32, rdx41, rcx30, r8_29, "Jim Meyering");
                    rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
                } else {
                    eax76 = setfileconat(rdi49, rsi32, rdx41, rcx30, r8_29, "Jim Meyering");
                    rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
                }
                if (eax76) {
                    rax77 = quote_n(1, r14_9, rdx41, rcx30, r8_29, "Jim Meyering");
                    r14_9 = rax77;
                    quotearg_n_style();
                    rax78 = fun_2520();
                    rsi32 = *reinterpret_cast<void***>(r12_15);
                    *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
                    r8_29 = r14_9;
                    rdx41 = rax78;
                    fun_2760();
                    rsp62 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp62) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
                    *reinterpret_cast<uint32_t*>(&rcx30) = 0;
                    *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx30) = 1;
                    *reinterpret_cast<int32_t*>(&rcx30 + 4) = 0;
                }
            }
        }
        addr_2c7f_57:
        if (*reinterpret_cast<void***>(r12_15)) {
            *reinterpret_cast<uint32_t*>(&rbx20) = 0;
            rax79 = fun_2520();
            rsi32 = *reinterpret_cast<void***>(r12_15);
            *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
            rdx41 = rax79;
            fun_2760();
            rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8 - 8 + 8);
        }
        eax80 = rpl_fts_close(rbp24, rsi32, rdx41, rcx30, r8_29, "Jim Meyering");
        rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp42) - 8 + 8);
        if (eax80) {
            *reinterpret_cast<uint32_t*>(&rbx20) = 0;
            fun_2520();
            rsi32 = *reinterpret_cast<void***>(r12_15);
            *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
            fun_2760();
            rsp81 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8 - 8 + 8);
        }
        *reinterpret_cast<uint32_t*>(&rbx20) = *reinterpret_cast<uint32_t*>(&rbx20) ^ 1;
        *reinterpret_cast<int32_t*>(&rbx20 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rax82) = *reinterpret_cast<unsigned char*>(&rbx20);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax82) + 4) = 0;
        rdx43 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v23) - reinterpret_cast<unsigned char>(g28));
        if (!rdx43) 
            goto addr_2cb4_62;
        fun_2570();
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp81) - 8 + 8);
    }
    addr_2bb7_40:
    *reinterpret_cast<uint32_t*>(&rax83) = *reinterpret_cast<uint16_t*>(rax64 + 0x68);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax83) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xcd78 + rax83 * 4) + 0xcd78;
    addr_2cb4_62:
    return rax82;
    addr_2b40_27:
    rcx30 = v27;
    specified_context = rcx30;
    *reinterpret_cast<uint32_t*>(&rbp24) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp24) + 4) = 0;
    rax84 = fun_2470();
    rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp35) - 8 + 8);
    r12_15 = rax84;
    if (!0) {
        root_dev_ino = reinterpret_cast<struct s10*>(0);
        goto addr_2b6f_65;
    }
    addr_310f_29:
    rax85 = get_root_dev_ino(0x110d0, rsi32, rdx43, rcx30);
    rsp53 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8);
    root_dev_ino = rax85;
    if (rax85) {
        addr_2b6f_65:
        rax86 = optind;
        *reinterpret_cast<int32_t*>(&rdx41) = 0;
        *reinterpret_cast<int32_t*>(&rdx41 + 4) = 0;
        rdi87 = reinterpret_cast<void***>(rbx20 + rax86 * 8);
        rsi32 = reinterpret_cast<void**>(*reinterpret_cast<uint32_t*>(&v28) | 8);
        *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rbx20) = 1;
        rax88 = xfts_open(rdi87);
        rbp24 = rax88;
        rax64 = rpl_fts_read(rbp24, rbp24);
        rsp42 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8 - 8 + 8);
        r15_6 = rax64;
        if (!rax64) 
            goto addr_2c7f_57;
    } else {
        rax89 = quotearg_style(4, "/", rdx43, 4, "/", rdx43);
        rax90 = fun_2520();
        rsi32 = *reinterpret_cast<void***>(r12_15);
        *reinterpret_cast<int32_t*>(&rsi32 + 4) = 0;
        rcx30 = rax89;
        rdx43 = rax90;
        fun_2760();
        rsp35 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp53) - 8 + 8 - 8 + 8 - 8 + 8);
        goto addr_3168_20;
    }
    goto addr_2ba8_39;
    addr_29c9_6:
    *reinterpret_cast<uint32_t*>(&rax91) = eax34 - 72;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax91) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax91) > 59) 
        goto addr_3192_4;
    goto *reinterpret_cast<int32_t*>(0xcc88 + rax91 * 4) + 0xcc88;
}

int64_t __libc_start_main = 0;

void fun_3343() {
    __asm__("cli ");
    __libc_start_main(0x28f0, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x11008;

void fun_2450(int64_t rdi);

int64_t fun_33e3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2450(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3423() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_2650(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8);

int32_t fun_2480(void** rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

int64_t stderr = 0;

void fun_2800(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_3433(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** r8_6;
    int64_t r9_7;
    int64_t r12_8;
    void** rax9;
    int64_t r12_10;
    void** rax11;
    int64_t r12_12;
    void** rax13;
    int64_t r12_14;
    void** rax15;
    int64_t r12_16;
    void** rax17;
    int64_t r12_18;
    void** rax19;
    int64_t r12_20;
    void** rax21;
    int64_t r12_22;
    void** rax23;
    int64_t r12_24;
    void** rax25;
    int64_t r12_26;
    void** rax27;
    int64_t r12_28;
    void** rax29;
    int32_t eax30;
    void** r13_31;
    void** rax32;
    int64_t r9_33;
    void** rax34;
    int32_t eax35;
    void** rax36;
    int64_t r9_37;
    void** rax38;
    int64_t r9_39;
    void** rax40;
    int32_t eax41;
    void** rax42;
    int64_t r9_43;
    int64_t r15_44;
    void** rax45;
    void** rax46;
    int64_t r9_47;
    void** rax48;
    int64_t rdi49;
    void** r8_50;
    void** r9_51;
    int64_t v52;
    int64_t v53;
    int64_t v54;
    int64_t v55;
    int64_t v56;
    int64_t v57;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2520();
            r8_6 = r12_2;
            fun_2740(1, rax5, r12_2, r12_2, r8_6, r9_7, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            r12_8 = stdout;
            rax9 = fun_2520();
            fun_2650(rax9, r12_8, 5, r12_2, r8_6);
            r12_10 = stdout;
            rax11 = fun_2520();
            fun_2650(rax11, r12_10, 5, r12_2, r8_6);
            r12_12 = stdout;
            rax13 = fun_2520();
            fun_2650(rax13, r12_12, 5, r12_2, r8_6);
            r12_14 = stdout;
            rax15 = fun_2520();
            fun_2650(rax15, r12_14, 5, r12_2, r8_6);
            r12_16 = stdout;
            rax17 = fun_2520();
            fun_2650(rax17, r12_16, 5, r12_2, r8_6);
            r12_18 = stdout;
            rax19 = fun_2520();
            fun_2650(rax19, r12_18, 5, r12_2, r8_6);
            r12_20 = stdout;
            rax21 = fun_2520();
            fun_2650(rax21, r12_20, 5, r12_2, r8_6);
            r12_22 = stdout;
            rax23 = fun_2520();
            fun_2650(rax23, r12_22, 5, r12_2, r8_6);
            r12_24 = stdout;
            rax25 = fun_2520();
            fun_2650(rax25, r12_24, 5, r12_2, r8_6);
            r12_26 = stdout;
            rax27 = fun_2520();
            fun_2650(rax27, r12_26, 5, r12_2, r8_6);
            r12_28 = stdout;
            rax29 = fun_2520();
            fun_2650(rax29, r12_28, 5, r12_2, r8_6);
            do {
                if (1) 
                    break;
                eax30 = fun_2670("chcon", 0, 5);
            } while (eax30);
            r13_31 = v4;
            if (!r13_31) {
                rax32 = fun_2520();
                fun_2740(1, rax32, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_6, r9_33, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax34 = fun_2730(5);
                if (!rax34 || (eax35 = fun_2480(rax34, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_6), !eax35)) {
                    rax36 = fun_2520();
                    r13_31 = reinterpret_cast<void**>("chcon");
                    fun_2740(1, rax36, "https://www.gnu.org/software/coreutils/", "chcon", r8_6, r9_37, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_31 = reinterpret_cast<void**>("chcon");
                    goto addr_3870_9;
                }
            } else {
                rax38 = fun_2520();
                fun_2740(1, rax38, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_6, r9_39, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                rax40 = fun_2730(5);
                if (!rax40 || (eax41 = fun_2480(rax40, "en_", 3, "https://www.gnu.org/software/coreutils/", r8_6), !eax41)) {
                    addr_3776_11:
                    rax42 = fun_2520();
                    fun_2740(1, rax42, "https://www.gnu.org/software/coreutils/", "chcon", r8_6, r9_43, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_31 == "chcon")) {
                        r12_2 = reinterpret_cast<void**>(0xd321);
                    }
                } else {
                    addr_3870_9:
                    r15_44 = stdout;
                    rax45 = fun_2520();
                    fun_2650(rax45, r15_44, 5, "https://www.gnu.org/software/coreutils/", r8_6);
                    goto addr_3776_11;
                }
            }
            rax46 = fun_2520();
            fun_2740(1, rax46, r13_31, r12_2, r8_6, r9_47, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0);
            addr_348e_14:
            fun_27e0();
        }
    } else {
        rax48 = fun_2520();
        rdi49 = stderr;
        fun_2800(rdi49, 1, rax48, r12_2, r8_50, r9_51, v52, v53, v54, v55, v56, v57);
        goto addr_348e_14;
    }
}

int64_t file_name = 0;

void fun_38a3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_38b3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(int64_t rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2490(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_38c3() {
    int64_t rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    int64_t rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2470(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2520();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3953_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2760();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2490(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3953_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2760();
    }
}

struct s11 {
    int64_t f0;
    int64_t f8;
};

struct s12 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_3973(struct s11* rdi, struct s12* rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (rdi->f8 == rsi->f8) {
        rax3 = rsi->f0;
        *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(rdi->f0 == rax3);
        return rax3;
    } else {
        return 0;
    }
}

struct s13 {
    signed char[8] pad8;
    int64_t f8;
};

uint64_t fun_39a3(struct s13* rdi, int64_t rsi) {
    __asm__("cli ");
    return rdi->f8 % reinterpret_cast<uint64_t>(rsi);
}

uint64_t fun_39c3(int64_t* rdi, int64_t rsi) {
    __asm__("cli ");
    return *rdi % reinterpret_cast<uint64_t>(rsi);
}

int64_t fun_39d3(int64_t* rdi, int64_t* rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = *rsi;
    *reinterpret_cast<unsigned char*>(&rax3) = reinterpret_cast<uint1_t>(*rdi == rax3);
    return rax3;
}

struct s14 {
    signed char[120] pad120;
    uint64_t f78;
};

struct s15 {
    signed char[120] pad120;
    uint64_t f78;
};

int64_t fun_39e3(struct s14** rdi, struct s15** rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>((*rdi)->f78 > (*rsi)->f78);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>((*rdi)->f78 < (*rsi)->f78)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

struct s16 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_2660(void** rdi, void** rsi);

void i_ring_init(void*** rdi, int64_t rsi);

void** fun_4fc3(struct s16* rdi, void** esi, void** rdx, void** rcx) {
    void** ebp5;
    void** rax6;
    void** r12_7;
    struct s16* rbx8;
    void** r14_9;
    void** rax10;
    void** eax11;
    void** rdi12;
    void** eax13;
    void** rsi14;
    unsigned char al15;
    unsigned char v16;
    void** rdi17;
    void** r15_18;
    void** v19;
    void** rax20;
    void** rdi21;
    uint32_t eax22;
    void** rax23;
    unsigned char al24;
    void** eax25;
    int64_t rdi26;
    int64_t rsi27;
    void** eax28;
    void** v29;
    void** r13_30;
    void** rbp31;
    uint32_t eax32;
    signed char v33;
    void** rax34;
    void** rdx35;
    void** rax36;
    void** rax37;
    void** rax38;
    void** r13_39;
    void** rdi40;
    void** rax41;
    void** rax42;
    unsigned char al43;
    struct s16* r15_44;
    void** r13_45;
    void** rax46;

    __asm__("cli ");
    if (reinterpret_cast<unsigned char>(esi) & 0xfffff000 || ((ebp5 = esi, (reinterpret_cast<unsigned char>(esi) & 0x204) == 0x204) || !(*reinterpret_cast<unsigned char*>(&esi) & 18))) {
        rax6 = fun_2470();
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        *reinterpret_cast<void***>(rax6) = reinterpret_cast<void**>(22);
        goto addr_51de_3;
    }
    rbx8 = rdi;
    r14_9 = rdx;
    rax10 = fun_2660(1, 0x80);
    r12_7 = rax10;
    if (!rax10) {
        addr_51de_3:
        return r12_7;
    } else {
        *reinterpret_cast<void***>(rax10 + 64) = r14_9;
        eax11 = ebp5;
        rdi12 = rbx8->f0;
        *reinterpret_cast<void***>(r12_7 + 44) = reinterpret_cast<void**>(0xffffff9c);
        *reinterpret_cast<unsigned char*>(&eax11 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax11 + 1) & 0xfd);
        eax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(eax11) | 4);
        if (!(*reinterpret_cast<unsigned char*>(&ebp5) & 2)) {
            eax13 = ebp5;
        }
        *reinterpret_cast<void***>(r12_7 + 72) = eax13;
        if (rdi12) 
            goto addr_504a_8;
    }
    *reinterpret_cast<int32_t*>(&rsi14) = 0x1000;
    *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
    addr_5079_10:
    al15 = fts_palloc(r12_7, rsi14, rdx);
    v16 = al15;
    if (!al15) {
        addr_5226_11:
        rdi17 = r12_7;
        *reinterpret_cast<int32_t*>(&r12_7) = 0;
        *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
        fun_2440(rdi17, rsi14);
        goto addr_51de_3;
    } else {
        r15_18 = rbx8->f0;
        if (!r15_18) {
            v19 = reinterpret_cast<void**>(0);
        } else {
            rsi14 = reinterpret_cast<void**>(0xd321);
            rax20 = fts_alloc(r12_7, 0xd321, 0);
            v19 = rax20;
            if (!rax20) {
                addr_521c_15:
                rdi21 = *reinterpret_cast<void***>(r12_7 + 32);
                fun_2440(rdi21, rsi14);
                goto addr_5226_11;
            } else {
                *reinterpret_cast<void***>(rax20 + 88) = reinterpret_cast<void**>(0xffffffffffffffff);
                r15_18 = rbx8->f0;
            }
        }
    }
    if (r14_9) {
        eax22 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) >> 10 & 1;
        v16 = *reinterpret_cast<unsigned char*>(&eax22);
    }
    if (!r15_18) {
        rsi14 = reinterpret_cast<void**>(0xd321);
        rax23 = fts_alloc(r12_7, 0xd321, 0);
        *reinterpret_cast<void***>(r12_7) = rax23;
        if (!rax23) {
            addr_5212_21:
            fun_2440(v19, rsi14);
            goto addr_521c_15;
        } else {
            *reinterpret_cast<void***>(rax23 + 16) = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint16_t*>(rax23 + 0x68) = 9;
            *reinterpret_cast<void***>(rax23 + 88) = reinterpret_cast<void**>(1);
            al24 = setup_dir(r12_7, 0xd321);
            if (al24) {
                addr_52df_23:
                eax25 = *reinterpret_cast<void***>(r12_7 + 72);
                if (!(reinterpret_cast<unsigned char>(eax25) & 0x204)) {
                    *reinterpret_cast<void***>(&rdi26) = *reinterpret_cast<void***>(r12_7 + 44);
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
                    if (!(*reinterpret_cast<unsigned char*>(&eax25 + 1) & 2)) {
                        *reinterpret_cast<uint32_t*>(&rsi27) = reinterpret_cast<unsigned char>(eax25) << 13 & 0x20000 | 0x90900;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi27) + 4) = 0;
                        eax28 = open_safer(".", rsi27);
                    } else {
                        eax28 = openat_safer(rdi26, ".");
                    }
                    *reinterpret_cast<void***>(r12_7 + 40) = eax28;
                    if (reinterpret_cast<signed char>(eax28) < reinterpret_cast<signed char>(0)) {
                        *reinterpret_cast<void***>(r12_7 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_7 + 72)) | 4);
                    }
                }
            } else {
                goto addr_5212_21;
            }
        }
    } else {
        v29 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r13_30) = 0;
        *reinterpret_cast<int32_t*>(&r13_30 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp31) = 0;
        *reinterpret_cast<int32_t*>(&rbp31 + 4) = 0;
        eax32 = (reinterpret_cast<unsigned char>(ebp5) >> 11 ^ 1) & 1;
        v33 = *reinterpret_cast<signed char*>(&eax32);
        do {
            addr_517d_31:
            rax34 = fun_2540(r15_18, r15_18);
            rdx35 = rax34;
            if (reinterpret_cast<unsigned char>(rax34) <= reinterpret_cast<unsigned char>(2) || (!v33 || *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rax34) + 0xffffffffffffffff) != 47)) {
                addr_5100_32:
                rsi14 = r15_18;
                rax36 = fts_alloc(r12_7, rsi14, rdx35);
                if (!rax36) 
                    goto addr_520d_33;
            } else {
                do {
                    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r15_18) + reinterpret_cast<unsigned char>(rdx35) + 0xfffffffffffffffe) != 47) 
                        goto addr_5100_32;
                    --rdx35;
                } while (!reinterpret_cast<int1_t>(rdx35 == 1));
                goto addr_51c6_37;
            }
            *reinterpret_cast<void***>(rax36 + 88) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rax36 + 8) = v19;
            *reinterpret_cast<void***>(rax36 + 48) = rax36 + 0x100;
            if (!rbp31 || !v16) {
                rax37 = fts_stat(r12_7, rax36, 0);
                *reinterpret_cast<uint16_t*>(rax36 + 0x68) = *reinterpret_cast<uint16_t*>(&rax37);
                if (r14_9) {
                    addr_5165_40:
                    *reinterpret_cast<void***>(rax36 + 16) = rbp31;
                    rbp31 = rax36;
                    continue;
                } else {
                    *reinterpret_cast<void***>(rax36 + 16) = reinterpret_cast<void**>(0);
                    if (!rbp31) {
                        ++r13_30;
                        v29 = rax36;
                        rbp31 = rax36;
                        r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_30) * 8);
                        if (r15_18) 
                            goto addr_517d_31; else 
                            goto addr_527d_43;
                    }
                }
            } else {
                *reinterpret_cast<int64_t*>(rax36 + 0xa0) = 2;
                *reinterpret_cast<uint16_t*>(rax36 + 0x68) = 11;
                if (r14_9) 
                    goto addr_5165_40;
                *reinterpret_cast<void***>(rax36 + 16) = reinterpret_cast<void**>(0);
            }
            rax38 = v29;
            v29 = rax36;
            *reinterpret_cast<void***>(rax38 + 16) = rax36;
            continue;
            addr_51c6_37:
            goto addr_5100_32;
            ++r13_30;
            r15_18 = *reinterpret_cast<void***>(reinterpret_cast<int64_t>(rbx8) + reinterpret_cast<unsigned char>(r13_30) * 8);
        } while (r15_18);
        goto addr_5280_48;
    }
    i_ring_init(r12_7 + 96, 0xffffffff);
    goto addr_51de_3;
    addr_520d_33:
    while (rbp31) {
        r13_39 = rbp31;
        rbp31 = *reinterpret_cast<void***>(rbp31 + 16);
        rdi40 = *reinterpret_cast<void***>(r13_39 + 24);
        if (rdi40) {
            fun_2610(rdi40, rsi14);
        }
        fun_2440(r13_39, rsi14);
    }
    goto addr_5212_21;
    addr_5280_48:
    if (r14_9 && reinterpret_cast<unsigned char>(r13_30) > reinterpret_cast<unsigned char>(1)) {
        rax41 = fts_sort(r12_7, rbp31, r13_30, rcx);
        rbp31 = rax41;
    }
    rsi14 = reinterpret_cast<void**>(0xd321);
    rax42 = fts_alloc(r12_7, 0xd321, 0);
    *reinterpret_cast<void***>(r12_7) = rax42;
    if (!rax42) 
        goto addr_520d_33;
    *reinterpret_cast<void***>(rax42 + 16) = rbp31;
    *reinterpret_cast<uint16_t*>(rax42 + 0x68) = 9;
    *reinterpret_cast<void***>(rax42 + 88) = reinterpret_cast<void**>(1);
    al43 = setup_dir(r12_7, 0xd321);
    if (!al43) 
        goto addr_520d_33; else 
        goto addr_52df_23;
    addr_527d_43:
    goto addr_5280_48;
    addr_504a_8:
    r15_44 = rbx8;
    *reinterpret_cast<int32_t*>(&r13_45) = 0;
    *reinterpret_cast<int32_t*>(&r13_45 + 4) = 0;
    do {
        rax46 = fun_2540(rdi12, rdi12);
        if (reinterpret_cast<unsigned char>(r13_45) < reinterpret_cast<unsigned char>(rax46)) {
            r13_45 = rax46;
        }
        rdi12 = r15_44->f8;
        r15_44 = reinterpret_cast<struct s16*>(&r15_44->f8);
    } while (rdi12);
    rsi14 = r13_45 + 1;
    if (reinterpret_cast<unsigned char>(rsi14) >= reinterpret_cast<unsigned char>(0x1000)) 
        goto addr_5079_10;
    rsi14 = reinterpret_cast<void**>(0x1000);
    goto addr_5079_10;
}

void hash_free();

int64_t fun_53e3(void** rdi, void** rsi, void** rdx) {
    void** r12_4;
    void** rdi5;
    void** rbp6;
    void** rbx7;
    void** rbp8;
    void** rdi9;
    void** rdi10;
    void** rdi11;
    void** eax12;
    int32_t eax13;
    void** rax14;
    void** r13d15;
    void** rbx16;
    int32_t eax17;
    void*** rbx18;
    unsigned char al19;
    void** eax20;
    void** rdi21;
    void** rax22;
    int64_t rax23;
    int32_t eax24;
    void** rax25;
    int32_t eax26;
    void** rax27;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *reinterpret_cast<void***>(rdi);
    if (rdi5) {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi5 + 88)) >= reinterpret_cast<signed char>(0)) {
            while (1) {
                rbp6 = *reinterpret_cast<void***>(rdi5 + 16);
                if (rbp6) {
                    fun_2440(rdi5, rsi);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                } else {
                    rbp6 = *reinterpret_cast<void***>(rdi5 + 8);
                    fun_2440(rdi5, rsi);
                    if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbp6 + 88)) < reinterpret_cast<signed char>(0)) 
                        break;
                }
                rdi5 = rbp6;
            }
        } else {
            rbp6 = rdi5;
        }
        fun_2440(rbp6, rsi);
    }
    rbx7 = *reinterpret_cast<void***>(r12_4 + 8);
    if (rbx7) {
        do {
            rbp8 = rbx7;
            rbx7 = *reinterpret_cast<void***>(rbx7 + 16);
            rdi9 = *reinterpret_cast<void***>(rbp8 + 24);
            if (rdi9) {
                fun_2610(rdi9, rsi);
            }
            fun_2440(rbp8, rsi);
        } while (rbx7);
    }
    rdi10 = *reinterpret_cast<void***>(r12_4 + 16);
    fun_2440(rdi10, rsi);
    rdi11 = *reinterpret_cast<void***>(r12_4 + 32);
    fun_2440(rdi11, rsi);
    eax12 = *reinterpret_cast<void***>(r12_4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
        if (!(*reinterpret_cast<unsigned char*>(&eax12) & 4)) {
            eax13 = fun_2710();
            if (eax13) {
                rax14 = fun_2470();
                r13d15 = *reinterpret_cast<void***>(rax14);
                rbx16 = rax14;
                eax17 = fun_25f0();
                if (r13d15 || !eax17) {
                    addr_549c_19:
                    rbx18 = reinterpret_cast<void***>(r12_4 + 96);
                } else {
                    addr_55a8_20:
                    r13d15 = *reinterpret_cast<void***>(rbx16);
                    goto addr_549c_19;
                }
                while (al19 = i_ring_empty(rbx18, rsi, rdx), al19 == 0) {
                    eax20 = i_ring_pop(rbx18, rsi, rdx);
                    if (reinterpret_cast<signed char>(eax20) < reinterpret_cast<signed char>(0)) 
                        continue;
                    fun_25f0();
                }
                if (*reinterpret_cast<void***>(r12_4 + 80)) {
                    hash_free();
                }
                rdi21 = *reinterpret_cast<void***>(r12_4 + 88);
                if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_4 + 72)) & 0x102)) {
                    fun_2440(rdi21, rsi);
                } else {
                    if (rdi21) {
                        hash_free();
                    }
                }
                fun_2440(r12_4, rsi);
                if (r13d15) {
                    rax22 = fun_2470();
                    *reinterpret_cast<void***>(rax22) = r13d15;
                    r13d15 = reinterpret_cast<void**>(0xffffffff);
                }
                *reinterpret_cast<void***>(&rax23) = r13d15;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax23) + 4) = 0;
                return rax23;
            } else {
                eax24 = fun_25f0();
                if (eax24) {
                    rax25 = fun_2470();
                    rbx16 = rax25;
                    goto addr_55a8_20;
                }
            }
        }
    } else {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(r12_4 + 44)) >= reinterpret_cast<signed char>(0) && (eax26 = fun_25f0(), !!eax26)) {
            rax27 = fun_2470();
            r13d15 = *reinterpret_cast<void***>(rax27);
            goto addr_549c_19;
        }
    }
    r13d15 = reinterpret_cast<void**>(0);
    goto addr_549c_19;
}

struct s17 {
    signed char f0;
    void** f1;
};

void** fun_25a0(void** rdi, void** rsi, void** rdx);

void** fun_55d3(void** rdi, void** rsi) {
    void** r12_3;
    void** edx4;
    void** rbp5;
    uint32_t eax6;
    void** rax7;
    void** rcx8;
    int32_t eax9;
    void** rdx10;
    void** rax11;
    void** eax12;
    int64_t rdi13;
    int64_t rsi14;
    void** eax15;
    void** r13_16;
    uint32_t eax17;
    void** r13_18;
    void** rax19;
    void** r14_20;
    void** rdi21;
    int32_t eax22;
    void** rax23;
    void** eax24;
    void** rax25;
    void** rax26;
    void** eax27;
    void** rax28;
    int64_t r8_29;
    void** rax30;
    void** rax31;
    void** r14_32;
    void** rdx33;
    void** rax34;
    void** rax35;
    void** rax36;
    void** rsi37;
    void** rdi38;
    struct s17* rdi39;
    void** rdi40;
    uint32_t eax41;
    void** rax42;
    uint32_t eax43;
    void** eax44;
    int32_t eax45;
    void** rax46;
    void** esi47;
    void** rsi48;
    int32_t eax49;
    uint32_t eax50;
    void** rdi51;
    void** rax52;
    void** rdi53;
    void** r14_54;
    void** rsi55;
    void** rax56;
    void** rax57;
    void** r13_58;
    void** rax59;
    void** rax60;
    void** eax61;
    int64_t rdi62;
    int64_t rsi63;
    void** eax64;
    void** rax65;
    void** eax66;
    void** r13_67;
    void** r14_68;
    void** rdi69;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi);
    if (!r12_3) 
        goto addr_56f8_2;
    edx4 = *reinterpret_cast<void***>(rdi + 72);
    rbp5 = rdi;
    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 32) 
        goto addr_56f8_2;
    eax6 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
    if (*reinterpret_cast<int16_t*>(&eax6) == 1) {
        rax7 = fts_stat(rdi, r12_3, 0);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax7);
        goto addr_56fb_6;
    }
    *reinterpret_cast<uint32_t*>(&rcx8) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    *reinterpret_cast<int32_t*>(&rcx8 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&eax6) != 2) 
        goto addr_5622_8;
    eax9 = static_cast<int32_t>(reinterpret_cast<uint64_t>(rcx8 + 0xfffffffffffffff4));
    if (*reinterpret_cast<uint16_t*>(&eax9) <= 1) {
        *reinterpret_cast<uint32_t*>(&rdx10) = 1;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fts_stat(rdi, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax11);
        if (*reinterpret_cast<uint16_t*>(&rax11) != 1) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            if (*reinterpret_cast<uint16_t*>(&rax11) != 11) 
                goto addr_56fb_6;
            goto addr_59a8_13;
        }
        eax12 = *reinterpret_cast<void***>(rbp5 + 72);
        if (*reinterpret_cast<unsigned char*>(&eax12) & 4) {
            *reinterpret_cast<void***>(rbp5) = r12_3;
            goto addr_593f_16;
        }
        *reinterpret_cast<void***>(&rdi13) = *reinterpret_cast<void***>(rbp5 + 44);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi13) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(eax12) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        if (!(*reinterpret_cast<unsigned char*>(&eax12 + 1) & 2)) {
            *reinterpret_cast<uint32_t*>(&rsi14) = *reinterpret_cast<uint32_t*>(&rdx10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi14) + 4) = 0;
            eax15 = open_safer(".", rsi14);
        } else {
            eax15 = openat_safer(rdi13, ".");
        }
        *reinterpret_cast<void***>(r12_3 + 68) = eax15;
        if (reinterpret_cast<signed char>(eax15) >= reinterpret_cast<signed char>(0)) 
            goto addr_5ca6_21;
    } else {
        if (*reinterpret_cast<int16_t*>(&rcx8) != 1) {
            do {
                addr_5658_23:
                r13_16 = r12_3;
                r12_3 = *reinterpret_cast<void***>(r12_3 + 16);
                if (!r12_3) 
                    goto addr_5665_24;
                *reinterpret_cast<void***>(rbp5) = r12_3;
                fun_2440(r13_16, rsi);
                if (!*reinterpret_cast<void***>(r12_3 + 88)) 
                    goto addr_5830_26;
                eax17 = *reinterpret_cast<uint16_t*>(r12_3 + 0x6c);
            } while (*reinterpret_cast<int16_t*>(&eax17) == 4);
            goto addr_58e0_28;
        } else {
            addr_5727_29:
            if (!(*reinterpret_cast<unsigned char*>(&edx4) & 64) || *reinterpret_cast<void***>(r12_3 + 0x70) == *reinterpret_cast<void***>(rbp5 + 24)) {
                r13_18 = *reinterpret_cast<void***>(rbp5 + 8);
                if (!r13_18) {
                    addr_5a1a_31:
                    rax19 = fts_build(rbp5, 3);
                    *reinterpret_cast<void***>(rbp5 + 8) = rax19;
                    if (!rax19) {
                        if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) {
                            if (*reinterpret_cast<void***>(r12_3 + 64) && *reinterpret_cast<uint16_t*>(r12_3 + 0x68) != 4) {
                                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                            }
                            leave_dir(rbp5, r12_3);
                            goto addr_56fb_6;
                        }
                    } else {
                        r12_3 = rax19;
                    }
                } else {
                    if (*reinterpret_cast<unsigned char*>(&edx4 + 1) & 16) {
                        *reinterpret_cast<unsigned char*>(&edx4 + 1) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx4 + 1) & 0xef);
                        *reinterpret_cast<void***>(rbp5 + 72) = edx4;
                        do {
                            r14_20 = r13_18;
                            r13_18 = *reinterpret_cast<void***>(r13_18 + 16);
                            rdi21 = *reinterpret_cast<void***>(r14_20 + 24);
                            if (rdi21) {
                                fun_2610(rdi21, rsi);
                            }
                            fun_2440(r14_20, rsi);
                        } while (r13_18);
                        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                        goto addr_5a1a_31;
                    } else {
                        rcx8 = *reinterpret_cast<void***>(r12_3 + 48);
                        eax22 = fts_safe_changedir(rbp5, r12_3, 0xffffffff, rcx8);
                        if (!eax22) {
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                        } else {
                            rax23 = fun_2470();
                            eax24 = *reinterpret_cast<void***>(rax23);
                            *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 1);
                            *reinterpret_cast<void***>(r12_3 + 64) = eax24;
                            r12_3 = *reinterpret_cast<void***>(rbp5 + 8);
                            if (r12_3) {
                                rax25 = r12_3;
                                do {
                                    *reinterpret_cast<void***>(rax25 + 48) = *reinterpret_cast<void***>(*reinterpret_cast<void***>(rax25 + 8) + 48);
                                    rax25 = *reinterpret_cast<void***>(rax25 + 16);
                                } while (rax25);
                            }
                        }
                    }
                }
                *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
                goto addr_58ea_50;
            } else {
                addr_57c2_51:
                if (*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) & 2) {
                    fun_25f0();
                    goto addr_57ce_53;
                }
            }
        }
    }
    rax26 = fun_2470();
    eax27 = *reinterpret_cast<void***>(rax26);
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
    *reinterpret_cast<void***>(r12_3 + 64) = eax27;
    *reinterpret_cast<void***>(rbp5) = r12_3;
    goto addr_56fb_6;
    addr_5ca6_21:
    *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    addr_592b_55:
    *reinterpret_cast<void***>(rbp5) = r12_3;
    if (*reinterpret_cast<uint16_t*>(&rax28) == 11) {
        addr_59a8_13:
        if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) == 2) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax28 = fts_stat(rbp5, r12_3, 0, rbp5, r12_3, 0);
            *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax28);
            goto addr_5935_57;
        } else {
            if (*reinterpret_cast<int64_t*>(r12_3 + 0xa0) != 1) {
                goto 0x287f;
            }
        }
    } else {
        addr_5935_57:
        if (*reinterpret_cast<uint16_t*>(&rax28) != 1) {
            addr_56fb_6:
            return r12_3;
        } else {
            addr_593f_16:
            if (!*reinterpret_cast<void***>(r12_3 + 88)) {
                *reinterpret_cast<void***>(rbp5 + 24) = *reinterpret_cast<void***>(r12_3 + 0x70);
            }
        }
    }
    rax30 = enter_dir(rbp5, r12_3, rdx10, rcx8, r8_29);
    if (!*reinterpret_cast<signed char*>(&rax30)) {
        rax31 = fun_2470();
        *reinterpret_cast<int32_t*>(&r12_3) = 0;
        *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
        *reinterpret_cast<void***>(rax31) = reinterpret_cast<void**>(12);
        goto addr_56fb_6;
    }
    addr_5665_24:
    r14_32 = *reinterpret_cast<void***>(r13_16 + 8);
    if (*reinterpret_cast<void***>(r14_32 + 24)) {
        rdx33 = *reinterpret_cast<void***>(rbp5 + 32);
        rax34 = *reinterpret_cast<void***>(r14_32 + 72);
        *reinterpret_cast<void***>(rbp5) = r14_32;
        *reinterpret_cast<int32_t*>(&rsi) = 3;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdx33) + reinterpret_cast<unsigned char>(rax34)) = 0;
        rax35 = fts_build(rbp5, 3);
        if (rax35) {
            r12_3 = rax35;
            fun_2440(r13_16, 3);
        } else {
            if (*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32) {
                addr_56f8_2:
                *reinterpret_cast<int32_t*>(&r12_3) = 0;
                *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
                goto addr_56fb_6;
            } else {
                r14_32 = *reinterpret_cast<void***>(r13_16 + 8);
                goto addr_5674_67;
            }
        }
    } else {
        addr_5674_67:
        *reinterpret_cast<void***>(rbp5) = r14_32;
        fun_2440(r13_16, rsi);
        if (*reinterpret_cast<void***>(r14_32 + 88) == 0xffffffffffffffff) {
            fun_2440(r14_32, rsi);
            rax36 = fun_2470();
            *reinterpret_cast<void***>(rax36) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rbp5) = reinterpret_cast<void**>(0);
            goto addr_56fb_6;
        }
    }
    addr_58ea_50:
    rsi37 = r12_3 + 0x100;
    rdi38 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72) + 0xffffffffffffffff;
    if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 56)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72)) + 0xffffffffffffffff) != 47) {
        rdi38 = *reinterpret_cast<void***>(*reinterpret_cast<void***>(r12_3 + 8) + 72);
    }
    rdi39 = reinterpret_cast<struct s17*>(reinterpret_cast<unsigned char>(rdi38) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)));
    rdi39->f0 = 47;
    rdi40 = reinterpret_cast<void**>(&rdi39->f1);
    rdx10 = *reinterpret_cast<void***>(r12_3 + 96) + 1;
    fun_2750(rdi40, rsi37, rdx10, rdi40, rsi37, rdx10);
    *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
    goto addr_592b_55;
    if (*reinterpret_cast<uint16_t*>(r14_32 + 0x68) == 11) 
        goto 0x287f;
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 32)) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_32 + 72))) = 0;
    if (*reinterpret_cast<void***>(r14_32 + 88)) 
        goto addr_56ae_73;
    eax41 = restore_initial_cwd(rbp5, rsi);
    if (!eax41) {
        addr_56c3_75:
        if (*reinterpret_cast<uint16_t*>(r14_32 + 0x68) != 2) {
            if (*reinterpret_cast<void***>(r14_32 + 64)) {
                *reinterpret_cast<uint16_t*>(r14_32 + 0x68) = 7;
            } else {
                *reinterpret_cast<uint16_t*>(r14_32 + 0x68) = 6;
                leave_dir(rbp5, r14_32);
            }
        }
    } else {
        addr_5b08_79:
        rax42 = fun_2470();
        *reinterpret_cast<void***>(r14_32 + 64) = *reinterpret_cast<void***>(rax42);
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_56c3_75;
    }
    r12_3 = r14_32;
    if (!(*reinterpret_cast<unsigned char*>(rbp5 + 73) & 32)) 
        goto addr_56fb_6;
    goto addr_56f8_2;
    addr_56ae_73:
    eax43 = *reinterpret_cast<unsigned char*>(r14_32 + 0x6a);
    if (*reinterpret_cast<unsigned char*>(&eax43) & 2) {
        eax44 = *reinterpret_cast<void***>(rbp5 + 72);
        if (!(*reinterpret_cast<unsigned char*>(&eax44) & 4)) {
            if (!(*reinterpret_cast<unsigned char*>(&eax44 + 1) & 2)) {
                eax45 = fun_2710();
                if (eax45) {
                    rax46 = fun_2470();
                    *reinterpret_cast<void***>(r14_32 + 64) = *reinterpret_cast<void***>(rax46);
                    *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
                }
            } else {
                esi47 = *reinterpret_cast<void***>(r14_32 + 68);
                cwd_advance_fd(rbp5, esi47, 1);
            }
        }
        fun_25f0();
        goto addr_56c3_75;
    } else {
        if (*reinterpret_cast<unsigned char*>(&eax43) & 1) 
            goto addr_56c3_75;
        rsi48 = *reinterpret_cast<void***>(r14_32 + 8);
        eax49 = fts_safe_changedir(rbp5, rsi48, 0xffffffff, "..");
        if (!eax49) 
            goto addr_56c3_75;
        goto addr_5b08_79;
    }
    addr_5830_26:
    eax50 = restore_initial_cwd(rbp5, rsi);
    if (eax50) {
        *reinterpret_cast<void***>(rbp5 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) | 0x2000);
        goto addr_56f8_2;
    }
    rdi51 = *reinterpret_cast<void***>(rbp5 + 88);
    if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp5 + 72)) & 0x102)) {
        fun_2440(rdi51, rsi);
    } else {
        if (rdi51) {
            hash_free();
        }
    }
    rax52 = *reinterpret_cast<void***>(r12_3 + 96);
    rdi53 = *reinterpret_cast<void***>(rbp5 + 32);
    r14_54 = r12_3 + 0x100;
    *reinterpret_cast<void***>(r12_3 + 72) = rax52;
    rdx10 = rax52 + 1;
    fun_2750(rdi53, r14_54, rdx10);
    *reinterpret_cast<int32_t*>(&rsi55) = 47;
    *reinterpret_cast<int32_t*>(&rsi55 + 4) = 0;
    rax56 = fun_25a0(r14_54, 47, rdx10);
    if (!rax56) 
        goto addr_58bb_98;
    if (r14_54 == rax56) {
        if (!*reinterpret_cast<void***>(r14_54 + 1)) {
            addr_58bb_98:
            rax57 = *reinterpret_cast<void***>(rbp5 + 32);
            *reinterpret_cast<void***>(r12_3 + 56) = rax57;
            *reinterpret_cast<void***>(r12_3 + 48) = rax57;
            setup_dir(rbp5, rsi55, rbp5, rsi55);
            *reinterpret_cast<uint32_t*>(&rax28) = *reinterpret_cast<uint16_t*>(r12_3 + 0x68);
            goto addr_592b_55;
        } else {
            goto addr_5898_102;
        }
    } else {
        addr_5898_102:
        r13_58 = rax56 + 1;
        rax59 = fun_2540(r13_58, r13_58);
        rsi55 = r13_58;
        rdx10 = rax59 + 1;
        fun_2750(r14_54, rsi55, rdx10);
        *reinterpret_cast<void***>(r12_3 + 96) = rax59;
        goto addr_58bb_98;
    }
    addr_58e0_28:
    if (*reinterpret_cast<int16_t*>(&eax17) == 2) {
        rax60 = fts_stat(rbp5, r12_3, 1);
        *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = *reinterpret_cast<uint16_t*>(&rax60);
        if (*reinterpret_cast<uint16_t*>(&rax60) == 1 && (eax61 = *reinterpret_cast<void***>(rbp5 + 72), !(*reinterpret_cast<unsigned char*>(&eax61) & 4))) {
            *reinterpret_cast<void***>(&rdi62) = *reinterpret_cast<void***>(rbp5 + 44);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi62) + 4) = 0;
            if (!(*reinterpret_cast<unsigned char*>(&eax61 + 1) & 2)) {
                *reinterpret_cast<uint32_t*>(&rsi63) = reinterpret_cast<unsigned char>(eax61) << 13 & 0x20000 | 0x90900;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi63) + 4) = 0;
                eax64 = open_safer(".", rsi63);
            } else {
                eax64 = openat_safer(rdi62, ".");
            }
            *reinterpret_cast<void***>(r12_3 + 68) = eax64;
            if (reinterpret_cast<signed char>(eax64) < reinterpret_cast<signed char>(0)) {
                rax65 = fun_2470();
                eax66 = *reinterpret_cast<void***>(rax65);
                *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 7;
                *reinterpret_cast<void***>(r12_3 + 64) = eax66;
            } else {
                *reinterpret_cast<unsigned char*>(r12_3 + 0x6a) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(r12_3 + 0x6a) | 2);
            }
        }
        *reinterpret_cast<uint16_t*>(r12_3 + 0x6c) = 3;
        goto addr_58ea_50;
    }
    addr_57ce_53:
    r13_67 = *reinterpret_cast<void***>(rbp5 + 8);
    if (r13_67) {
        do {
            r14_68 = r13_67;
            r13_67 = *reinterpret_cast<void***>(r13_67 + 16);
            rdi69 = *reinterpret_cast<void***>(r14_68 + 24);
            if (rdi69) {
                fun_2610(rdi69, rsi);
            }
            fun_2440(r14_68, rsi);
        } while (r13_67);
        *reinterpret_cast<void***>(rbp5 + 8) = reinterpret_cast<void**>(0);
    }
    *reinterpret_cast<uint16_t*>(r12_3 + 0x68) = 6;
    leave_dir(rbp5, r12_3);
    goto addr_56fb_6;
    addr_5622_8:
    if (*reinterpret_cast<int16_t*>(&rcx8) != 1) 
        goto addr_5658_23;
    if (*reinterpret_cast<int16_t*>(&eax6) != 4) 
        goto addr_5727_29; else 
        goto addr_57c2_51;
}

struct s18 {
    signed char[108] pad108;
    int16_t f6c;
};

int64_t fun_5d23() {
    uint32_t edx1;
    void** rax2;
    struct s18* rsi3;
    int16_t dx4;

    __asm__("cli ");
    if (edx1 > 4) {
        rax2 = fun_2470();
        *reinterpret_cast<void***>(rax2) = reinterpret_cast<void**>(22);
        return 1;
    } else {
        rsi3->f6c = dx4;
        return 0;
    }
}

void** fun_5d53(void** rdi, void** rsi) {
    uint32_t r13d3;
    void** rbp4;
    void** rax5;
    void** r14_6;
    void** r15_7;
    uint32_t edx8;
    void** rax9;
    void** rbx10;
    void** r12_11;
    void** rdi12;
    int32_t r12d13;
    void** eax14;
    void** rsi15;
    int64_t rdi16;
    int64_t rsi17;
    void** eax18;
    void** r13d19;
    void** eax20;
    void** rsi21;
    void** rax22;
    int32_t eax23;
    void** ebx24;

    __asm__("cli ");
    r13d3 = *reinterpret_cast<uint32_t*>(&rsi);
    rbp4 = rdi;
    rax5 = fun_2470();
    r14_6 = rax5;
    if (r13d3 & 0xffffefff) {
        *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(22);
        return 0;
    }
    r15_7 = *reinterpret_cast<void***>(rbp4);
    *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 32) {
        return 0;
    }
    edx8 = *reinterpret_cast<uint16_t*>(r15_7 + 0x68);
    if (*reinterpret_cast<int16_t*>(&edx8) == 9) {
        return *reinterpret_cast<void***>(r15_7 + 16);
    }
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    if (*reinterpret_cast<int16_t*>(&edx8) == 1) 
        goto addr_5da8_8;
    addr_5e1d_9:
    return rax9;
    addr_5da8_8:
    rbx10 = *reinterpret_cast<void***>(rbp4 + 8);
    if (rbx10) {
        do {
            r12_11 = rbx10;
            rbx10 = *reinterpret_cast<void***>(rbx10 + 16);
            rdi12 = *reinterpret_cast<void***>(r12_11 + 24);
            if (rdi12) {
                fun_2610(rdi12, rsi);
            }
            fun_2440(r12_11, rsi);
        } while (rbx10);
    }
    r12d13 = 1;
    if (r13d3 == 0x1000) {
        *reinterpret_cast<void***>(rbp4 + 72) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4 + 72)) | 0x1000);
        r12d13 = 2;
    }
    if (*reinterpret_cast<void***>(r15_7 + 88)) 
        goto addr_5e0e_17;
    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(r15_7 + 48)) == 47) 
        goto addr_5e0e_17;
    eax14 = *reinterpret_cast<void***>(rbp4 + 72);
    if (!(*reinterpret_cast<unsigned char*>(&eax14) & 4)) 
        goto addr_5e30_20;
    addr_5e0e_17:
    *reinterpret_cast<int32_t*>(&rsi15) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi15 + 4) = 0;
    rax9 = fts_build(rbp4, rsi15);
    *reinterpret_cast<void***>(rbp4 + 8) = rax9;
    goto addr_5e1d_9;
    addr_5e30_20:
    *reinterpret_cast<void***>(&rdi16) = *reinterpret_cast<void***>(rbp4 + 44);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
    if (!(*reinterpret_cast<unsigned char*>(&eax14 + 1) & 2)) {
        *reinterpret_cast<uint32_t*>(&rsi17) = reinterpret_cast<unsigned char>(eax14) << 13 & 0x20000 | 0x90900;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi17) + 4) = 0;
        eax18 = open_safer(".", rsi17);
        r13d19 = eax18;
    } else {
        eax20 = openat_safer(rdi16, ".");
        r13d19 = eax20;
    }
    if (reinterpret_cast<signed char>(r13d19) >= reinterpret_cast<signed char>(0)) 
        goto addr_5e67_24;
    *reinterpret_cast<void***>(rbp4 + 8) = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rax9) = 0;
    *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
    goto addr_5e1d_9;
    addr_5e67_24:
    *reinterpret_cast<int32_t*>(&rsi21) = r12d13;
    *reinterpret_cast<int32_t*>(&rsi21 + 4) = 0;
    rax22 = fts_build(rbp4, rsi21);
    *reinterpret_cast<void***>(rbp4 + 8) = rax22;
    if (*reinterpret_cast<unsigned char*>(rbp4 + 73) & 2) {
        cwd_advance_fd(rbp4, r13d19, 1);
    } else {
        eax23 = fun_2710();
        if (eax23) {
            ebx24 = *reinterpret_cast<void***>(r14_6);
            fun_25f0();
            *reinterpret_cast<int32_t*>(&rax9) = 0;
            *reinterpret_cast<int32_t*>(&rax9 + 4) = 0;
            *reinterpret_cast<void***>(r14_6) = ebx24;
            goto addr_5e1d_9;
        } else {
            fun_25f0();
        }
    }
    rax9 = *reinterpret_cast<void***>(rbp4 + 8);
    goto addr_5e1d_9;
}

uint64_t fun_5f53(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_5f73(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s19 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_63d3(struct s19* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s20 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_63e3(struct s20* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s21 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_63f3(struct s21* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s24 {
    signed char[8] pad8;
    struct s24* f8;
};

struct s23 {
    int64_t f0;
    struct s24* f8;
};

struct s22 {
    struct s23* f0;
    struct s23* f8;
};

uint64_t fun_6403(struct s22* rdi) {
    struct s23* rcx2;
    struct s23* rsi3;
    uint64_t r8_4;
    struct s24* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s27 {
    signed char[8] pad8;
    struct s27* f8;
};

struct s26 {
    int64_t f0;
    struct s27* f8;
};

struct s25 {
    struct s26* f0;
    struct s26* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_6463(struct s25* rdi) {
    struct s26* rcx2;
    struct s26* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s27* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s30 {
    signed char[8] pad8;
    struct s30* f8;
};

struct s29 {
    int64_t f0;
    struct s30* f8;
};

struct s28 {
    struct s29* f0;
    struct s29* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_64d3(struct s28* rdi, int64_t rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r13_5;
    int64_t v6;
    int64_t r12_7;
    uint64_t r12_8;
    int64_t v9;
    int64_t rbp10;
    int64_t rbp11;
    int64_t v12;
    int64_t rbx13;
    struct s29* rcx14;
    struct s29* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s30* rax19;
    uint64_t rdx20;
    void** r9_21;
    int64_t v22;
    void** r9_23;
    int64_t v24;
    void** r9_25;
    int64_t v26;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_2800(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_2800(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x68ac]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_658a_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_6609_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_2800(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_2800;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x692b]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_6609_14; else 
            goto addr_658a_13;
    }
}

struct s31 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s32 {
    int64_t f0;
    struct s32* f8;
};

int64_t fun_6633(struct s31* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s31* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s32* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x288e;
    rbx7 = reinterpret_cast<struct s32*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_6698_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_668b_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_668b_7;
    }
    addr_669b_10:
    return r12_3;
    addr_6698_5:
    r12_3 = rbx7->f0;
    goto addr_669b_10;
    addr_668b_7:
    return 0;
}

struct s33 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_66b3(struct s33* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x2893;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_66ef_7;
    return *rax2;
    addr_66ef_7:
    goto 0x2893;
}

struct s35 {
    int64_t f0;
    struct s35* f8;
};

struct s34 {
    int64_t* f0;
    struct s35* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_6703(struct s34* rdi, int64_t rsi) {
    struct s34* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s35* rax7;
    struct s35* rdx8;
    struct s35* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x2899;
    rax7 = reinterpret_cast<struct s35*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_674e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_674e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_676c_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_676c_10:
    return r8_10;
}

struct s37 {
    int64_t f0;
    struct s37* f8;
};

struct s36 {
    struct s37* f0;
    struct s37* f8;
};

void fun_6793(struct s36* rdi, int64_t rsi, uint64_t rdx) {
    struct s37* r9_4;
    uint64_t rax5;
    struct s37* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_67d2_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_67d2_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s39 {
    int64_t f0;
    struct s39* f8;
};

struct s38 {
    struct s39* f0;
    struct s39* f8;
};

int64_t fun_67e3(struct s38* rdi, int64_t rsi, int64_t rdx) {
    struct s39* r14_4;
    int64_t r12_5;
    struct s38* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s39* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_680f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_6851_10;
            }
            addr_680f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_6819_11:
    return r12_5;
    addr_6851_10:
    goto addr_6819_11;
}

uint64_t fun_6863(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s40 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_68a3(struct s40* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_68d3(uint64_t rdi, void** rsi, void** rdx, void** rcx, void** r8) {
    void** r15_6;
    void** rbp7;
    void** rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    void** rax12;
    void** rax13;
    void** rdi14;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x5f50);
    }
    if (!rcx) {
        rbx8 = reinterpret_cast<void**>(0x5f70);
    }
    rax9 = fun_26d0(80);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0xce80);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((*reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax12 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&rsi)), *reinterpret_cast<void***>(r12_10 + 16) = rax12, rax12 == 0) || (*reinterpret_cast<uint32_t*>(&rsi) = 16, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax13 = fun_2660(rax12, 16), *reinterpret_cast<void***>(r12_10) = rax13, rax13 == 0))) {
            rdi14 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_2440(rdi14, rsi);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<void***>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 32) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s43 {
    int64_t f0;
    struct s43* f8;
};

struct s42 {
    int64_t f0;
    struct s43* f8;
};

struct s41 {
    struct s42* f0;
    struct s42* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s43* f48;
};

void fun_69d3(struct s41* rdi) {
    struct s41* rbp2;
    struct s42* r12_3;
    struct s43* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s43* rax7;
    struct s43* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s44 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_6a83(struct s44* rdi, void** rsi) {
    struct s44* r12_3;
    void** r13_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rdi9;
    void** rbx10;
    void** rbx11;
    void** rdi12;
    void** rdi13;

    __asm__("cli ");
    r12_3 = rdi;
    r13_4 = rdi->f0;
    rax5 = rdi->f8;
    rbp6 = r13_4;
    if (!rdi->f40 || !rdi->f20) {
        addr_6af3_2:
        if (reinterpret_cast<unsigned char>(rax5) > reinterpret_cast<unsigned char>(rbp6)) {
            do {
                rbx7 = *reinterpret_cast<void***>(rbp6 + 8);
                if (rbx7) {
                    do {
                        rdi8 = rbx7;
                        rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
                        fun_2440(rdi8, rsi);
                    } while (rbx7);
                }
                rbp6 = rbp6 + 16;
            } while (reinterpret_cast<unsigned char>(r12_3->f8) > reinterpret_cast<unsigned char>(rbp6));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) < reinterpret_cast<unsigned char>(rax5)) {
            while (1) {
                rdi9 = *reinterpret_cast<void***>(r13_4);
                if (!rdi9) {
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                } else {
                    rbx10 = r13_4;
                    while (r12_3->f40(rdi9), rbx10 = *reinterpret_cast<void***>(rbx10 + 8), !!rbx10) {
                        rdi9 = *reinterpret_cast<void***>(rbx10);
                    }
                    rax5 = r12_3->f8;
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                }
            }
            rbp6 = r12_3->f0;
            goto addr_6af3_2;
        }
    }
    rbx11 = r12_3->f48;
    if (rbx11) {
        do {
            rdi12 = rbx11;
            rbx11 = *reinterpret_cast<void***>(rbx11 + 8);
            fun_2440(rdi12, rsi);
        } while (rbx11);
    }
    rdi13 = r12_3->f0;
    fun_2440(rdi13, rsi);
    goto fun_2440;
}

int64_t fun_6b73(void** rdi, uint64_t rsi) {
    void** r12_3;
    void** rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    void** r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi + 40);
    rax4 = g28;
    esi5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3 + 16));
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_6cb0_2;
    if (*reinterpret_cast<void***>(rdi + 16) == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_2660(rax6, 16);
        if (!rax8) {
            addr_6cb0_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8) - 8 + 8);
            v10 = *reinterpret_cast<void***>(rdi + 72);
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = *reinterpret_cast<void***>(rdi);
                fun_2440(rdi12, rdi);
                *reinterpret_cast<void***>(rdi) = rax8;
                *reinterpret_cast<void***>(rdi + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                *reinterpret_cast<void***>(rdi + 16) = rax6;
                *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rdi + 72) = v10;
            } else {
                *reinterpret_cast<void***>(rdi + 72) = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x289e;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x289e;
                fun_2440(rax8, r13_9);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        fun_2570();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s45 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_6d03(void** rdi, void** rsi, void*** rdx) {
    void** rax4;
    void*** r12_5;
    void** rax6;
    void** rax7;
    uint1_t below_or_equal8;
    uint64_t rax9;
    int1_t cf10;
    signed char al11;
    void** rax12;
    struct s45* v13;
    int32_t r8d14;
    void** rax15;
    void* rax16;
    int64_t rax17;
    void** rdx18;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x28a3;
    r12_5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rax6 = hash_find_entry(rdi, rsi, r12_5, 0);
    if (!rax6) {
        if (reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdi + 24)) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax7 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal8 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax7 == 0)));
            if (reinterpret_cast<signed char>(rax7) < reinterpret_cast<signed char>(0)) 
                goto addr_6e1e_5; else 
                goto addr_6d8f_6;
        }
        __asm__("pxor xmm5, xmm5");
        rax7 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal8 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax7 == 0)));
        if (reinterpret_cast<signed char>(rax7) >= reinterpret_cast<signed char>(0)) {
            addr_6d8f_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_6e1e_5:
            *reinterpret_cast<uint32_t*>(&rax9) = *reinterpret_cast<uint32_t*>(&rax7) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            below_or_equal8 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax7) >> 1) | rax9) == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal8 && (check_tuning(rdi), !below_or_equal8)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf10 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x6031]");
            if (!cf10) 
                goto addr_6e75_12;
            __asm__("comiss xmm4, [rip+0x5fe1]");
            if (!cf10) {
                __asm__("subss xmm4, [rip+0x5fa0]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al11 = hash_rehash(rdi);
            if (!al11) 
                goto addr_6e75_12;
            rax12 = hash_find_entry(rdi, rsi, r12_5, 0);
            if (rax12) {
                goto 0x28a3;
            }
        }
        if (!v13->f0) {
            v13->f0 = rsi;
            r8d14 = 1;
            *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax15 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax15) {
                rax15 = fun_26d0(16, 16);
                if (!rax15) {
                    addr_6e75_12:
                    r8d14 = -1;
                } else {
                    goto addr_6dd2_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax15 + 8);
                goto addr_6dd2_24;
            }
        }
    } else {
        r8d14 = 0;
        if (rdx) {
            *rdx = rax6;
        }
    }
    addr_6d4e_28:
    rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax16) {
        fun_2570();
    } else {
        *reinterpret_cast<int32_t*>(&rax17) = r8d14;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
        return rax17;
    }
    addr_6dd2_24:
    rdx18 = v13->f8;
    *reinterpret_cast<void***>(rax15) = rsi;
    r8d14 = 1;
    *reinterpret_cast<void***>(rax15 + 8) = rdx18;
    v13->f8 = rax15;
    *reinterpret_cast<void***>(rdi + 32) = *reinterpret_cast<void***>(rdi + 32) + 1;
    goto addr_6d4e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_6f23() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2570();
    } else {
        return rax3;
    }
}

void** fun_6f83(void** rdi, void** rsi) {
    void** rbx3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<void***>(rbx3 + 32) = *reinterpret_cast<void***>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_7010_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_70c6_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x5e4e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x5db8]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_2440(rdi15, rsi, rdi15, rsi);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_70c6_5; else 
                goto addr_7010_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (rax16) {
        fun_2570();
    } else {
        return r12_7;
    }
}

void fun_7113() {
    __asm__("cli ");
    goto hash_remove;
}

struct s46 {
    int32_t f0;
    int32_t f4;
    int32_t f8;
    int32_t fc;
    int32_t f10;
    int64_t f14;
    signed char f1c;
};

void fun_7123(struct s46* rdi, int32_t esi) {
    __asm__("cli ");
    rdi->f14 = 0;
    rdi->f1c = 1;
    rdi->f0 = esi;
    rdi->f4 = esi;
    rdi->f8 = esi;
    rdi->fc = esi;
    rdi->f10 = esi;
    return;
}

struct s47 {
    signed char[28] pad28;
    unsigned char f1c;
};

int64_t fun_7143(struct s47* rdi) {
    int64_t rax2;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rax2) = rdi->f1c;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

struct s48 {
    int32_t f0;
    signed char[16] pad20;
    uint32_t f14;
    uint32_t f18;
    unsigned char f1c;
};

int64_t fun_7153(struct s48* rdi, int32_t esi) {
    uint32_t eax3;
    uint32_t eax4;
    uint32_t edx5;
    int64_t rcx6;
    int32_t r8d7;
    uint32_t ecx8;
    int64_t rax9;

    __asm__("cli ");
    eax3 = static_cast<uint32_t>(rdi->f1c) ^ 1;
    eax4 = *reinterpret_cast<unsigned char*>(&eax3);
    edx5 = rdi->f14 + eax4 & 3;
    *reinterpret_cast<uint32_t*>(&rcx6) = edx5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    r8d7 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rcx6 * 4) = esi;
    ecx8 = rdi->f18;
    rdi->f14 = edx5;
    if (ecx8 == edx5) {
        rdi->f18 = eax4 + ecx8 & 3;
    }
    rdi->f1c = 0;
    *reinterpret_cast<int32_t*>(&rax9) = r8d7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    return rax9;
}

struct s49 {
    int32_t f0;
    signed char[12] pad16;
    int32_t f10;
    uint32_t f14;
    uint32_t f18;
    signed char f1c;
};

int64_t fun_7193(struct s49* rdi) {
    int64_t rdx2;
    int32_t r8d3;
    int64_t rax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f1c) 
        goto 0x28a8;
    *reinterpret_cast<uint32_t*>(&rdx2) = rdi->f14;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx2) + 4) = 0;
    r8d3 = *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4);
    rax4 = rdx2;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(rdi) + rdx2 * 4) = rdi->f10;
    if (*reinterpret_cast<uint32_t*>(&rdx2) == rdi->f18) {
        rdi->f1c = 1;
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        rdi->f14 = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax4) + 3) & 3;
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

int32_t fun_2550(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx);

void fd_safer(int64_t rdi);

void fun_71d3(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx) {
    int64_t v5;
    int64_t rcx6;
    void** rax7;
    int32_t eax8;
    int64_t rdi9;
    void* rdx10;

    __asm__("cli ");
    v5 = rcx;
    *reinterpret_cast<int32_t*>(&rcx6) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    rax7 = g28;
    if (*reinterpret_cast<unsigned char*>(&rdx) & 64) {
        *reinterpret_cast<int32_t*>(&rcx6) = *reinterpret_cast<int32_t*>(&v5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx6) + 4) = 0;
    }
    eax8 = fun_2550(rdi, rsi, rdx, rcx6);
    *reinterpret_cast<int32_t*>(&rdi9) = eax8;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0;
    fd_safer(rdi9);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax7) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2570();
    } else {
        return;
    }
}

int64_t fun_27b0(int64_t rdi);

int64_t fun_7253(int64_t rdi, void** rsi, int32_t edx, void*** rcx) {
    int64_t r12_5;
    void** eax6;
    int64_t rdi7;
    int64_t rax8;
    void** rax9;
    void** r13d10;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r12_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    eax6 = openat_safer(rdi, rsi);
    if (reinterpret_cast<signed char>(eax6) >= reinterpret_cast<signed char>(0)) {
        *reinterpret_cast<void***>(&rdi7) = eax6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi7) + 4) = 0;
        rax8 = fun_27b0(rdi7);
        r12_5 = rax8;
        if (!rax8) {
            rax9 = fun_2470();
            r13d10 = *reinterpret_cast<void***>(rax9);
            fun_25f0();
            *reinterpret_cast<void***>(rax9) = r13d10;
        } else {
            *rcx = eax6;
        }
    }
    return r12_5;
}

void fun_27f0(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_72b3(void** rdi) {
    int64_t rcx2;
    void** rbx3;
    void** rdx4;
    void** rax5;
    void** r12_6;
    void** rcx7;
    void** r8_8;
    int32_t eax9;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_27f0("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2460("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax5 = fun_25a0(rdi, 47, rdx4);
        if (rax5 && ((r12_6 = rax5 + 1, reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_6) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax9 = fun_2480(rax5 + 0xfffffffffffffffa, "/.libs/", 7, rcx7, r8_8), !eax9))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax5 + 1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_6 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_6 + 2) != 45)) {
                rbx3 = r12_6;
            } else {
                rbx3 = rax5 + 4;
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_8a53(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2470();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x11220;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_8a93(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x11220);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_8ab3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x11220);
    }
    *rdi = esi;
    return 0x11220;
}

int64_t fun_8ad3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x11220);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s50 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_8b13(struct s50* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s50*>(0x11220);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s51 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s51* fun_8b33(struct s51* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s51*>(0x11220);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x28b8;
    if (!rdx) 
        goto 0x28b8;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x11220;
}

struct s52 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8b73(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s52* r8) {
    struct s52* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s52*>(0x11220);
    }
    rax7 = fun_2470();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x8ba6);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s53 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8bf3(int64_t rdi, int64_t rsi, void*** rdx, struct s53* rcx) {
    struct s53* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s53*>(0x11220);
    }
    rax6 = fun_2470();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x8c21);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x8c7c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_8ce3() {
    __asm__("cli ");
}

void** g11078 = reinterpret_cast<void**>(32);

int64_t slotvec0 = 0x100;

void fun_8cf3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2440(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0x11120) {
        fun_2440(rdi8, rsi9);
        g11078 = reinterpret_cast<void**>(0x11120);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x11070) {
        fun_2440(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0x11070);
    }
    nslots = 1;
    return;
}

void fun_8d93() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8db3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8dc3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8de3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_8e03(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x28be;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax6;
    }
}

void** fun_8e93(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x28c3;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2570();
    } else {
        return rax7;
    }
}

void** fun_8f23(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s5* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x28c8;
    rcx4 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2570();
    } else {
        return rax5;
    }
}

void** fun_8fb3(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x28cd;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax6;
    }
}

void** fun_9043(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s5* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x81d0]");
    __asm__("movdqa xmm1, [rip+0x81d8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x81c1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2570();
    } else {
        return rax10;
    }
}

void** fun_90e3(int64_t rdi, uint32_t esi) {
    struct s5* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8130]");
    __asm__("movdqa xmm1, [rip+0x8138]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8121]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2570();
    } else {
        return rax9;
    }
}

void** fun_9183(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8090]");
    __asm__("movdqa xmm1, [rip+0x8098]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x8079]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2570();
    } else {
        return rax3;
    }
}

void** fun_9213(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8000]");
    __asm__("movdqa xmm1, [rip+0x8008]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x7ff6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2570();
    } else {
        return rax4;
    }
}

void** fun_92a3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x28d2;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax6;
    }
}

void** fun_9343(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7eca]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x7ec2]");
    __asm__("movdqa xmm2, [rip+0x7eca]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x28d7;
    if (!rdx) 
        goto 0x28d7;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2570();
    } else {
        return rax7;
    }
}

void** fun_93e3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s5* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7e2a]");
    __asm__("movdqa xmm1, [rip+0x7e32]");
    __asm__("movdqa xmm2, [rip+0x7e3a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x28dc;
    if (!rdx) 
        goto 0x28dc;
    rcx7 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2570();
    } else {
        return rax9;
    }
}

void** fun_9493(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s5* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7d7a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x7d72]");
    __asm__("movdqa xmm2, [rip+0x7d7a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x28e1;
    if (!rsi) 
        goto 0x28e1;
    rcx5 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax6;
    }
}

void** fun_9533(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s5* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x7cda]");
    __asm__("movdqa xmm1, [rip+0x7ce2]");
    __asm__("movdqa xmm2, [rip+0x7cea]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x28e6;
    if (!rsi) 
        goto 0x28e6;
    rcx6 = reinterpret_cast<struct s5*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2570();
    } else {
        return rax7;
    }
}

void fun_95d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_95e3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9603() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_9623(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s54 {
    int64_t f0;
    int64_t f8;
};

int32_t fun_2630(int64_t rdi, void* rsi);

struct s54* fun_9643(struct s54* rdi) {
    void** rax2;
    int32_t eax3;
    struct s54* rax4;
    int64_t v5;
    int64_t v6;
    void* rdx7;

    __asm__("cli ");
    rax2 = g28;
    eax3 = fun_2630("/", reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xa0);
    if (eax3) {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    } else {
        rdi->f0 = v5;
        rdi->f8 = v6;
        rax4 = rdi;
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax4;
    }
}

void** openat_proc_name(void** rdi, void** rsi, void** rdx);

int32_t save_cwd(void* rdi, void** rsi, void** rdx);

void openat_save_fail(int64_t rdi, void** rsi, void** rdx);

void free_cwd(void* rdi, void** rsi, void** rdx);

int32_t restore_cwd(void* rdi, void** rsi, void** rdx);

void openat_restore_fail(int64_t rdi, void** rsi, void** rdx);

int64_t fun_96c3(int32_t edi, void** rsi) {
    void** rax3;
    void** rax4;
    void* rsp5;
    void** r13_6;
    void** rdx7;
    void** rax8;
    void* rsp9;
    int32_t eax10;
    void* rax11;
    int64_t rdi12;
    int32_t v13;
    int32_t eax14;
    void** ebp15;
    int32_t eax16;
    int64_t rdi17;

    __asm__("cli ");
    rax3 = g28;
    rax4 = fun_2470();
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0xfe8 - 8 + 8);
    if (edi == -100 || *reinterpret_cast<void***>(rsi) == 47) {
        *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(95);
    } else {
        r13_6 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 16);
        rdx7 = rsi;
        *reinterpret_cast<int32_t*>(&rsi) = edi;
        *reinterpret_cast<int32_t*>(&rsi + 4) = 0;
        rax8 = openat_proc_name(r13_6, rsi, rdx7);
        rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        if (rax8 && (*reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(95), rax8 != r13_6)) {
            fun_2440(rax8, rsi, rax8, rsi);
            rsp9 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp9) - 8 + 8);
        }
        eax10 = save_cwd(rsp9, rsi, rdx7);
        if (eax10) 
            goto addr_97d4_6; else 
            goto addr_9742_7;
    }
    addr_9777_8:
    rax11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rax11) {
        fun_2570();
    } else {
        return 0xffffffff;
    }
    addr_97d4_6:
    *reinterpret_cast<void***>(&rdi12) = *reinterpret_cast<void***>(rax4);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi12) + 4) = 0;
    openat_save_fail(rdi12, rsi, rdx7);
    goto addr_97db_11;
    addr_9742_7:
    if (edi < 0 || v13 != edi) {
        eax14 = fun_2710();
        if (eax14) {
            ebp15 = *reinterpret_cast<void***>(rax4);
            free_cwd(rsp9, rsi, rdx7);
        } else {
            *reinterpret_cast<void***>(rax4) = reinterpret_cast<void**>(95);
            eax16 = restore_cwd(rsp9, rsi, rdx7);
            if (eax16) {
                addr_97db_11:
                *reinterpret_cast<void***>(&rdi17) = *reinterpret_cast<void***>(rax4);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi17) + 4) = 0;
                openat_restore_fail(rdi17, rsi, rdx7);
            } else {
                ebp15 = reinterpret_cast<void**>(95);
                free_cwd(rsp9, rsi, rdx7);
            }
        }
    } else {
        ebp15 = reinterpret_cast<void**>(9);
        free_cwd(rsp9, rsi, rdx7);
    }
    *reinterpret_cast<void***>(rax4) = ebp15;
    goto addr_9777_8;
}

void fun_97f3() {
    __asm__("cli ");
    goto getfileconat;
}

void fun_9803() {
    __asm__("cli ");
    goto getfileconat;
}

void fun_9813() {
    __asm__("cli ");
    goto getfileconat;
}

int32_t dup_safer();

int64_t fun_9823(uint32_t edi) {
    int32_t eax2;
    void** rax3;
    void** r13d4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    if (edi <= 2) {
        eax2 = dup_safer();
        rax3 = fun_2470();
        r13d4 = *reinterpret_cast<void***>(rax3);
        fun_25f0();
        *reinterpret_cast<int32_t*>(&rax5) = eax2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        *reinterpret_cast<void***>(rax3) = r13d4;
        return rax5;
    } else {
        *reinterpret_cast<uint32_t*>(&rax6) = edi;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

struct s55 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2690(int64_t rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_9883(int64_t rdi, void** rsi, void** rdx, void** rcx, struct s55* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2800(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2800(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2520();
    fun_2800(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2690(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2520();
    fun_2800(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2690(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2520();
        fun_2800(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xd5c8 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0xd5c8;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_9cf3() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s56 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_9d13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s56* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s56* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2570();
    } else {
        return;
    }
}

void fun_9db3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_9e56_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_9e60_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2570();
    } else {
        return;
    }
    addr_9e56_5:
    goto addr_9e60_7;
}

void fun_9e93() {
    int64_t rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** r8_8;
    int64_t r9_9;
    int64_t v10;
    int64_t v11;
    void** v12;
    int64_t v13;
    void** v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    int64_t v20;
    int64_t v21;
    int64_t v22;
    void** rax23;
    void** r8_24;
    int64_t r9_25;
    int64_t v26;
    int64_t v27;
    void** v28;
    int64_t v29;
    void** v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t v34;
    int64_t v35;
    int64_t v36;
    int64_t v37;
    int64_t v38;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2690(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2520();
    fun_2740(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22);
    rax23 = fun_2520();
    fun_2740(1, rax23, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_24, r9_25, v26, __return_address(), v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
    fun_2520();
    goto fun_2740;
}

int64_t fun_24d0();

void xalloc_die();

void fun_9f33(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_9f73(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_26d0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9f93(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_26d0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9fb3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_26d0(rdi);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9fd3(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2720(rdi);
    if (rax3 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a003(void** rdi, int64_t rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2720(rdi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a033(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_24d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_a073() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_24d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a0b3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_24d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a0e3(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_24d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a133(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_24d0();
            if (rax5) 
                break;
            addr_a17d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_a17d_5;
        rax8 = fun_24d0();
        if (rax8) 
            goto addr_a166_9;
        if (rbx4) 
            goto addr_a17d_5;
        addr_a166_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_a1c3(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_24d0();
            if (rax8) 
                break;
            addr_a20a_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_a20a_5;
        rax11 = fun_24d0();
        if (rax11) 
            goto addr_a1f2_9;
        if (!rbx6) 
            goto addr_a1f2_9;
        if (r12_4) 
            goto addr_a20a_5;
        addr_a1f2_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_a253(void** rdi, void** rsi, void** rdx, void* rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void** r12_8;
    void* rsi9;
    void* rcx10;
    void* rbx11;
    void* rax12;
    void* rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void*>((reinterpret_cast<int64_t>(rcx10) >> 1) + reinterpret_cast<uint64_t>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void*>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<int64_t>(rbx11) <= reinterpret_cast<int64_t>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_a2fd_9:
            rbx11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8)));
            if (!r13_6) {
                addr_a310_10:
                *r12_8 = reinterpret_cast<void*>(0);
            }
            addr_a2b0_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<uint64_t>(rbx11) - reinterpret_cast<uint64_t>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_a2d6_12;
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_a324_14;
            if (reinterpret_cast<int64_t>(rcx10) <= reinterpret_cast<int64_t>(rsi9)) 
                goto addr_a2cd_16;
            if (reinterpret_cast<int64_t>(rsi9) >= reinterpret_cast<int64_t>(0)) 
                goto addr_a324_14;
            addr_a2cd_16:
            rcx10 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_a324_14;
            addr_a2d6_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_2720(rdi7);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_a324_14;
            if (!rbp13) 
                break;
            addr_a324_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<int64_t>(rbp13) <= reinterpret_cast<int64_t>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_a2fd_9;
        } else {
            if (!r13_6) 
                goto addr_a310_10;
            goto addr_a2b0_11;
        }
    }
}

void fun_a353(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2660(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a383(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2660(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a3b3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2660(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a3d3(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2660(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_a3f3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26d0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

void fun_a433(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26d0(rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

struct s57 {
    signed char[1] pad1;
    void** f1;
};

void fun_a473(int64_t rdi, struct s57* rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_26d0(&rsi->f1);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<uint64_t>(rsi)) = 0;
        goto fun_26a0;
    }
}

void fun_a4b3(void** rdi) {
    void** rax2;
    void** rax3;

    __asm__("cli ");
    rax2 = fun_2540(rdi);
    rax3 = fun_26d0(rax2 + 1);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_26a0;
    }
}

void fun_a4f3() {
    void** rdi1;

    __asm__("cli ");
    fun_2520();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2760();
    fun_2460(rdi1);
}

int64_t rpl_fts_open();

void fun_a533() {
    int64_t rax1;
    void** rax2;

    __asm__("cli ");
    rax1 = rpl_fts_open();
    if (!rax1) {
        rax2 = fun_2470();
        if (*reinterpret_cast<void***>(rax2) != 22) {
            xalloc_die();
        }
        fun_25c0("errno != EINVAL", "lib/xfts.c", 41, "xfts_open");
    } else {
        return;
    }
}

struct s58 {
    signed char[72] pad72;
    uint32_t f48;
};

struct s59 {
    signed char[88] pad88;
    int64_t f58;
};

int64_t fun_a583(struct s58* rdi, struct s59* rsi) {
    int32_t r8d3;
    uint32_t eax4;
    int64_t rax5;
    int64_t rax6;

    __asm__("cli ");
    r8d3 = 1;
    eax4 = rdi->f48 & 17;
    if (eax4 == 16 || (r8d3 = 0, eax4 != 17)) {
        *reinterpret_cast<int32_t*>(&rax5) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
        return rax5;
    } else {
        *reinterpret_cast<unsigned char*>(&r8d3) = reinterpret_cast<uint1_t>(!!rsi->f58);
        *reinterpret_cast<int32_t*>(&rax6) = r8d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
        return rax6;
    }
}

/* proc_status.0 */
uint32_t proc_status_0 = 0;

int32_t fun_2780(int64_t rdi, int64_t rsi, void** rdx);

int32_t fun_2860(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

uint32_t fun_2790(void** rdi);

void fun_24a0();

void** fun_a5c3(void** rdi, int32_t esi, void** rdx) {
    void** rbx4;
    void** rax5;
    void** rax6;
    uint32_t eax7;
    int32_t eax8;
    void** r14_9;
    int64_t r8_10;
    uint32_t eax11;
    void** rax12;
    void** rdi13;
    void** rax14;
    int64_t r8_15;
    void* rdx16;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!*reinterpret_cast<void***>(rdx)) {
        *reinterpret_cast<void***>(rdi) = reinterpret_cast<void**>(0);
        rax6 = rdi;
    } else {
        eax7 = proc_status_0;
        if (!eax7) {
            eax8 = fun_2780("/proc/self/fd", 0x90900, rdx);
            if (eax8 >= 0) {
                r14_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 48) - 8 + 8);
                *reinterpret_cast<int32_t*>(&r8_10) = eax8;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
                fun_2860(r14_9, 1, 32, "/proc/self/fd/%d/../fd", r8_10);
                eax11 = fun_2790(r14_9);
                proc_status_0 = (eax11 - (eax11 + reinterpret_cast<uint1_t>(eax11 < eax11 + reinterpret_cast<uint1_t>(eax11 < 1))) & 2) - 1;
                fun_25f0();
                eax7 = proc_status_0;
                goto addr_a600_6;
            } else {
                proc_status_0 = 0xffffffff;
                *reinterpret_cast<int32_t*>(&rax6) = 0;
                *reinterpret_cast<int32_t*>(&rax6 + 4) = 0;
            }
        } else {
            addr_a600_6:
            if (reinterpret_cast<int32_t>(eax7) < reinterpret_cast<int32_t>(0) || (rax12 = fun_2540(rdx, rdx), rdi13 = rax12 + 27, reinterpret_cast<unsigned char>(rdi13) > reinterpret_cast<unsigned char>(0xfc0)) && (rax14 = fun_26d0(rdi13, rdi13), rbx4 = rax14, !rax14)) {
                *reinterpret_cast<int32_t*>(&rax6) = 0;
                *reinterpret_cast<int32_t*>(&rax6 + 4) = 0;
            } else {
                *reinterpret_cast<int32_t*>(&r8_15) = esi;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_15) + 4) = 0;
                fun_2860(rbx4, 1, -1, "/proc/self/fd/%d/", r8_15);
                fun_24a0();
                rax6 = rbx4;
            }
        }
    }
    rdx16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx16) {
        fun_2570();
    } else {
        return rax6;
    }
}

int64_t fun_24b0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_a723(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_24b0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_a77e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2470();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_a77e_3;
            rax6 = fun_2470();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s60 {
    signed char[16] pad16;
    int64_t f10;
    int32_t f18;
};

void fun_a793(struct s60* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f18 = 0x95f616;
    return;
}

struct s61 {
    int64_t f0;
    int64_t f8;
    uint64_t f10;
    int32_t f18;
};

struct s62 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_a7b3(struct s61* rdi, struct s62* rsi) {
    uint64_t rax3;
    int64_t rdx4;
    uint64_t rcx5;
    int64_t rax6;

    __asm__("cli ");
    if (rdi->f18 != 0x95f616) {
        fun_25c0("state->magic == 9827862", "lib/cycle-check.c", 60, "cycle_check");
    } else {
        rax3 = rdi->f10;
        rdx4 = rsi->f8;
        if (!rax3) {
            rdi->f10 = 1;
        } else {
            if (rdi->f0 != rdx4 || rsi->f0 != rdi->f8) {
                rcx5 = rax3 + 1;
                rdi->f10 = rcx5;
                if (!(rax3 & rcx5)) {
                    if (!rcx5) {
                        return 1;
                    }
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        }
        rax6 = rsi->f0;
        rdi->f0 = rdx4;
        rdi->f8 = rax6;
        return 0;
    }
}

struct s63 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_26b0(struct s63* rdi);

int32_t fun_2700(struct s63* rdi);

int64_t fun_25b0(int64_t rdi, ...);

int32_t rpl_fflush(struct s63* rdi);

int64_t fun_2500(struct s63* rdi);

int64_t fun_a843(struct s63* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_26b0(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2700(rdi);
        if (!(eax3 && (eax4 = fun_26b0(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_25b0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2470();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_2500(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2500;
}

uint32_t fun_24e0(int64_t rdi, ...);

/* have_dupfd_cloexec.0 */
int32_t have_dupfd_cloexec_0 = 0;

int64_t fun_a8d3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9, int64_t a7) {
    void** rax8;
    uint32_t eax9;
    uint32_t r12d10;
    int32_t eax11;
    uint32_t eax12;
    int1_t zf13;
    void* rax14;
    int64_t rax15;
    int64_t rdi16;
    uint32_t eax17;
    int64_t rdi18;
    uint32_t eax19;
    void** rax20;
    void** r13d21;
    uint32_t eax22;
    void** rax23;
    int64_t rdi24;
    uint32_t eax25;
    uint32_t ecx26;
    int64_t rax27;
    uint32_t eax28;
    uint32_t eax29;
    uint32_t eax30;
    int32_t ecx31;
    int64_t rax32;

    __asm__("cli ");
    rax8 = g28;
    if (!*reinterpret_cast<int32_t*>(&rsi)) {
        eax9 = fun_24e0(rdi);
        r12d10 = eax9;
        goto addr_a9d4_3;
    }
    if (*reinterpret_cast<int32_t*>(&rsi) == 0x406) {
        eax11 = have_dupfd_cloexec_0;
        if (eax11 < 0) {
            eax12 = fun_24e0(rdi);
            r12d10 = eax12;
            if (reinterpret_cast<int32_t>(eax12) < reinterpret_cast<int32_t>(0) || (zf13 = have_dupfd_cloexec_0 == -1, !zf13)) {
                addr_a9d4_3:
                rax14 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax8) - reinterpret_cast<unsigned char>(g28));
                if (rax14) {
                    fun_2570();
                } else {
                    *reinterpret_cast<uint32_t*>(&rax15) = r12d10;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
                    return rax15;
                }
            } else {
                addr_aa89_9:
                *reinterpret_cast<uint32_t*>(&rdi16) = r12d10;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi16) + 4) = 0;
                eax17 = fun_24e0(rdi16, rdi16);
                if (reinterpret_cast<int32_t>(eax17) < reinterpret_cast<int32_t>(0) || (*reinterpret_cast<uint32_t*>(&rdi18) = r12d10, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi18) + 4) = 0, eax19 = fun_24e0(rdi18, rdi18), eax19 == 0xffffffff)) {
                    rax20 = fun_2470();
                    r12d10 = 0xffffffff;
                    r13d21 = *reinterpret_cast<void***>(rax20);
                    fun_25f0();
                    *reinterpret_cast<void***>(rax20) = r13d21;
                    goto addr_a9d4_3;
                }
            }
        } else {
            eax22 = fun_24e0(rdi, rdi);
            r12d10 = eax22;
            if (reinterpret_cast<int32_t>(eax22) >= reinterpret_cast<int32_t>(0) || (rax23 = fun_2470(), *reinterpret_cast<int32_t*>(&rdi24) = *reinterpret_cast<int32_t*>(&rdi), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi24) + 4) = 0, !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax23) == 22))) {
                have_dupfd_cloexec_0 = 1;
                goto addr_a9d4_3;
            } else {
                eax25 = fun_24e0(rdi24);
                r12d10 = eax25;
                if (reinterpret_cast<int32_t>(eax25) < reinterpret_cast<int32_t>(0)) 
                    goto addr_a9d4_3;
                have_dupfd_cloexec_0 = -1;
                goto addr_aa89_9;
            }
        }
    }
    if (*reinterpret_cast<int32_t*>(&rsi) <= 11) 
        goto addr_a939_16;
    ecx26 = static_cast<uint32_t>(rsi - 0x400);
    if (ecx26 > 10) 
        goto addr_a93d_18;
    rax27 = 1 << *reinterpret_cast<unsigned char*>(&ecx26);
    if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x2c5)) {
        if (!(*reinterpret_cast<uint32_t*>(&rax27) & 0x502)) {
            addr_a93d_18:
            if (0) {
            }
        } else {
            addr_a985_23:
            eax28 = fun_24e0(rdi);
            r12d10 = eax28;
            goto addr_a9d4_3;
        }
        eax29 = fun_24e0(rdi);
        r12d10 = eax29;
        goto addr_a9d4_3;
    }
    if (0) {
    }
    eax30 = fun_24e0(rdi);
    r12d10 = eax30;
    goto addr_a9d4_3;
    addr_a939_16:
    if (reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) < 0) | reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rsi) == 0))) 
        goto addr_a93d_18;
    ecx31 = *reinterpret_cast<int32_t*>(&rsi);
    rax32 = 1 << *reinterpret_cast<unsigned char*>(&ecx31);
    if (!(*reinterpret_cast<uint32_t*>(&rax32) & 0x514)) {
        if (*reinterpret_cast<uint32_t*>(&rax32) & 0xa0a) 
            goto addr_a985_23;
        goto addr_a93d_18;
    }
}

void fun_ab43(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t v4;
    void** rdx5;
    void** rax6;
    int32_t eax7;
    int64_t rdi8;
    void* rdx9;

    __asm__("cli ");
    v4 = rdx;
    *reinterpret_cast<int32_t*>(&rdx5) = 0;
    *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
    rax6 = g28;
    if (*reinterpret_cast<unsigned char*>(&rsi) & 64) {
        *reinterpret_cast<int32_t*>(&rdx5) = *reinterpret_cast<int32_t*>(&v4);
        *reinterpret_cast<int32_t*>(&rdx5 + 4) = 0;
    }
    eax7 = fun_2780(rdi, rsi, rdx5);
    *reinterpret_cast<int32_t*>(&rdi8) = eax7;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi8) + 4) = 0;
    fd_safer(rdi8);
    rdx9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
    if (rdx9) {
        fun_2570();
    } else {
        return;
    }
}

void rpl_fseeko(struct s63* rdi);

void fun_abc3(struct s63* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2700(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_ac13(struct s63* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_26b0(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_25b0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

signed char* fun_26f0(int64_t rdi);

signed char* fun_ac93() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_26f0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2590(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_acd3(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2590(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2570();
    } else {
        return r12_7;
    }
}

void fun_ad63(int32_t edi) {
    void** rdi2;

    __asm__("cli ");
    fun_2520();
    *reinterpret_cast<int32_t*>(&rdi2) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi2 + 4) = 0;
    fun_2760();
    fun_2460(rdi2, rdi2);
}

void fun_ada3(int32_t edi) {
    void** rdi2;

    __asm__("cli ");
    fun_2520();
    *reinterpret_cast<int32_t*>(&rdi2) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi2 + 4) = 0;
    fun_2760();
    fun_2460(rdi2, rdi2);
}

struct s64 {
    void** f0;
    signed char[7] pad8;
    uint64_t f8;
};

uint64_t fun_25e0();

int64_t fun_ade3(struct s64* rdi) {
    void** eax2;
    uint64_t rax3;
    int64_t rax4;

    __asm__("cli ");
    rdi->f8 = 0;
    eax2 = open_safer(".", 0x80000);
    rdi->f0 = eax2;
    if (reinterpret_cast<signed char>(eax2) < reinterpret_cast<signed char>(0)) {
        rax3 = fun_25e0();
        rdi->f8 = rax3;
        *reinterpret_cast<uint32_t*>(&rax4) = -static_cast<uint32_t>(reinterpret_cast<uint1_t>(0 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax3 < 1))));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        return rax4;
    } else {
        return 0;
    }
}

void fun_ae43(int32_t* rdi) {
    __asm__("cli ");
    if (*rdi < 0) {
        goto 0xaee0;
    } else {
        goto fun_2710;
    }
}

void fun_ae73(int32_t* rdi) {
    __asm__("cli ");
    if (*rdi >= 0) {
        fun_25f0();
    }
    goto fun_2440;
}

void fun_ae93() {
    __asm__("cli ");
    goto rpl_fcntl;
}

int32_t fun_2560();

void* fun_2600(void** rdi, void** rsi, int64_t rdx);

struct s65 {
    signed char f0;
    void** f1;
};

struct s65* fun_2620(uint64_t rdi, int64_t rsi, uint64_t rdx);

void** fun_2770(void** rdi, int64_t rsi, int64_t rdx);

int64_t fun_aee3(void** rdi) {
    int32_t eax2;
    int32_t r12d3;
    void** rax4;
    int1_t zf5;
    void** v6;
    int64_t rax7;
    void** rax8;
    void** rbx9;
    void** rsi10;
    int64_t rdx11;
    void* rax12;
    void* r15_13;
    struct s65* rax14;
    int64_t rdx15;
    int64_t rcx16;
    int32_t eax17;
    int32_t r13d18;
    void** r14_19;
    void* rax20;
    void** r14_21;
    int64_t rcx22;
    int32_t eax23;
    void** rbp24;
    void** rax25;
    int64_t rdi26;
    int64_t rcx27;
    int32_t eax28;
    int64_t rdi29;
    int32_t eax30;
    void* rax31;
    int32_t eax32;
    int32_t eax33;
    int64_t rdi34;
    int64_t rcx35;
    int32_t eax36;
    int32_t eax37;
    int32_t eax38;
    int32_t eax39;

    __asm__("cli ");
    eax2 = fun_2560();
    r12d3 = eax2;
    if (!eax2 || (rax4 = fun_2470(), zf5 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax4) == 36), v6 = rax4, !zf5)) {
        addr_b050_2:
        *reinterpret_cast<int32_t*>(&rax7) = r12d3;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        return rax7;
    } else {
        rax8 = fun_2540(rdi);
        rbx9 = rax8;
        if (!rax8) {
            fun_25c0("0 < len", "lib/chdir-long.c", 0x7e, "chdir_long");
            goto addr_b222_5;
        }
        if (reinterpret_cast<unsigned char>(rax8) <= reinterpret_cast<unsigned char>(0xfff)) {
            addr_b222_5:
            fun_25c0("4096 <= len", "lib/chdir-long.c", 0x7f, "chdir_long");
            goto addr_b241_7;
        } else {
            rsi10 = reinterpret_cast<void**>("/");
            rax12 = fun_2600(rdi, "/", rdx11);
            r15_13 = rax12;
            if (rax12 == 2) {
                rax14 = fun_2620(rdi + 3, 47, rbx9 + 0xfffffffffffffffd);
                if (!rax14) {
                    r12d3 = -1;
                    goto addr_b050_2;
                } else {
                    rax14->f0 = 0;
                    *reinterpret_cast<int32_t*>(&rdx15) = 0x10900;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx15) + 4) = 0;
                    eax17 = fun_2550(0xffffff9c, rdi, 0x10900, rcx16);
                    rax14->f0 = 47;
                    r13d18 = eax17;
                    if (eax17 < 0) {
                        addr_b1d0_12:
                        rbx9 = *reinterpret_cast<void***>(v6);
                    } else {
                        r14_19 = reinterpret_cast<void**>(&rax14->f1);
                        rsi10 = reinterpret_cast<void**>("/");
                        rax20 = fun_2600(r14_19, "/", 0x10900);
                        r14_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r14_19) + reinterpret_cast<uint64_t>(rax20));
                        goto addr_af69_14;
                    }
                }
            } else {
                r14_21 = rdi;
                r13d18 = -100;
                if (rax12) {
                    *reinterpret_cast<int32_t*>(&rdx15) = 0x10900;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx15) + 4) = 0;
                    rsi10 = reinterpret_cast<void**>("/");
                    eax23 = fun_2550(0xffffff9c, "/", 0x10900, rcx22);
                    r13d18 = eax23;
                    if (eax23 < 0) 
                        goto addr_b1d0_12;
                    r14_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<uint64_t>(r15_13));
                    goto addr_af69_14;
                }
            }
        }
    }
    addr_b043_18:
    r12d3 = -1;
    *reinterpret_cast<void***>(v6) = rbx9;
    goto addr_b050_2;
    addr_af69_14:
    if (*reinterpret_cast<void***>(r14_21) == 47) {
        addr_b241_7:
        fun_25c0("*dir != '/'", "lib/chdir-long.c", 0xa2, "chdir_long");
        goto addr_b260_19;
    } else {
        rbp24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rbx9));
        if (reinterpret_cast<unsigned char>(r14_21) > reinterpret_cast<unsigned char>(rbp24)) {
            addr_b260_19:
            fun_25c0("dir <= dir_end", "lib/chdir-long.c", 0xa3, "chdir_long");
        } else {
            if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rbp24) - reinterpret_cast<unsigned char>(r14_21)) > reinterpret_cast<int64_t>(0xfff)) {
                while (rax25 = fun_2770(r14_21, 47, 0x1000), rbx9 = rax25, !!rax25) {
                    *reinterpret_cast<void***>(rax25) = reinterpret_cast<void**>(0);
                    if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rax25) - reinterpret_cast<unsigned char>(r14_21)) > reinterpret_cast<int64_t>(0xfff)) 
                        goto addr_b1dc_24;
                    *reinterpret_cast<int32_t*>(&rdx15) = 0x10900;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx15) + 4) = 0;
                    rsi10 = r14_21;
                    *reinterpret_cast<int32_t*>(&rdi26) = r13d18;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi26) + 4) = 0;
                    eax28 = fun_2550(rdi26, rsi10, 0x10900, rcx27);
                    *reinterpret_cast<int32_t*>(&r15_13) = eax28;
                    if (eax28 < 0) 
                        goto addr_b030_26;
                    if (r13d18 < 0) 
                        goto addr_af98_28;
                    *reinterpret_cast<int32_t*>(&rdi29) = r13d18;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
                    eax30 = fun_25f0();
                    if (eax30) 
                        goto addr_b021_30;
                    addr_af98_28:
                    *reinterpret_cast<void***>(rbx9) = reinterpret_cast<void**>(47);
                    ++rbx9;
                    rsi10 = reinterpret_cast<void**>("/");
                    rax31 = fun_2600(rbx9, "/", 0x10900);
                    r14_21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx9) + reinterpret_cast<uint64_t>(rax31));
                    if (reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(rbp24) - reinterpret_cast<unsigned char>(r14_21)) <= reinterpret_cast<int64_t>(0xfff)) 
                        goto addr_b100_31;
                    r13d18 = *reinterpret_cast<int32_t*>(&r15_13);
                }
            } else {
                *reinterpret_cast<int32_t*>(&r15_13) = r13d18;
                goto addr_b100_31;
            }
        }
    }
    r12d3 = -1;
    *reinterpret_cast<void***>(v6) = reinterpret_cast<void**>(36);
    goto addr_b050_2;
    addr_b1dc_24:
    *reinterpret_cast<int32_t*>(&rdx15) = 0xb3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx15) + 4) = 0;
    rsi10 = reinterpret_cast<void**>("lib/chdir-long.c");
    fun_25c0("slash - dir < 4096", "lib/chdir-long.c", 0xb3, "chdir_long");
    goto addr_b1fb_36;
    addr_b100_31:
    if (reinterpret_cast<unsigned char>(rbp24) <= reinterpret_cast<unsigned char>(r14_21)) {
        r13d18 = *reinterpret_cast<int32_t*>(&r15_13);
        eax32 = fun_2710();
        if (eax32) {
            addr_b033_38:
            while (rbx9 = *reinterpret_cast<void***>(v6), *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0, r13d18 >= 0) {
                addr_b197_39:
                *reinterpret_cast<int32_t*>(&rdi29) = r13d18;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
                eax33 = fun_25f0();
                if (!eax33) 
                    break;
                addr_b021_30:
                cdb_free_part_0(rdi29, rsi10, rdx15, "chdir_long");
                addr_b030_26:
                *reinterpret_cast<void***>(rbx9) = reinterpret_cast<void**>(47);
            }
            goto addr_b043_18;
        } else {
            if (*reinterpret_cast<int32_t*>(&r15_13) < 0) {
                addr_b147_42:
                r12d3 = 0;
                goto addr_b050_2;
            }
        }
    } else {
        *reinterpret_cast<int32_t*>(&rdx15) = 0x10900;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx15) + 4) = 0;
        rsi10 = r14_21;
        *reinterpret_cast<int32_t*>(&rdi34) = *reinterpret_cast<int32_t*>(&r15_13);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi34) + 4) = 0;
        eax36 = fun_2550(rdi34, rsi10, 0x10900, rcx35);
        r13d18 = eax36;
        if (eax36 < 0) {
            addr_b1fb_36:
            r13d18 = *reinterpret_cast<int32_t*>(&r15_13);
            goto addr_b033_38;
        } else {
            if (*reinterpret_cast<int32_t*>(&r15_13) < 0 || (*reinterpret_cast<int32_t*>(&rdi29) = *reinterpret_cast<int32_t*>(&r15_13), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0, eax37 = fun_25f0(), eax37 == 0)) {
                eax38 = fun_2710();
                if (eax38) {
                    rbx9 = *reinterpret_cast<void***>(v6);
                    *reinterpret_cast<int32_t*>(&rbx9 + 4) = 0;
                    goto addr_b197_39;
                }
            } else {
                goto addr_b021_30;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rdi29) = r13d18;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi29) + 4) = 0;
    eax39 = fun_25f0();
    if (eax39) 
        goto addr_b021_30; else 
        goto addr_b147_42;
}

int32_t setlocale_null_r();

int64_t fun_b283() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2570();
    } else {
        return rax3;
    }
}

int64_t fun_b303(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2730(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax6 = fun_2540(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_26a0(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_26a0(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_b3b3() {
    __asm__("cli ");
    goto fun_2730;
}

void fun_b3c3() {
    __asm__("cli ");
}

void fun_b3d7() {
    __asm__("cli ");
    return;
}

void fun_29e0() {
    goto 0x2998;
}

void fun_2bcc() {
    goto 0x2998;
}

int64_t quotearg_n_style_colon();

void fun_2d34() {
    quotearg_n_style_colon();
    fun_2760();
    goto 0x2c60;
}

struct s66 {
    signed char[120] pad120;
    void** f78;
};

struct s67 {
    signed char[112] pad112;
    int64_t f70;
};

void fun_2dd1() {
    uint32_t ecx1;
    struct s10* rax2;
    void** rdx3;
    struct s66* r15_4;
    struct s67* r15_5;
    void** r14_6;
    int32_t eax7;
    void** r14_8;
    struct s9* rbp9;
    void** r15_10;
    struct s9* rbp11;

    ecx1 = recurse;
    if (!*reinterpret_cast<signed char*>(&ecx1)) 
        goto 0x2c10;
    rax2 = root_dev_ino;
    if (!rax2) 
        goto 0x2c69;
    rdx3 = rax2->f0;
    if (r15_4->f78 != rdx3) 
        goto 0x2c69;
    if (r15_5->f70 != rax2->f8) 
        goto 0x2c69;
    eax7 = fun_2670(r14_6, "/", rdx3);
    if (eax7) {
        quotearg_n_style();
        quotearg_n_style();
        fun_2520();
        fun_2760();
    } else {
        quotearg_style(4, r14_8, rdx3);
        fun_2520();
        fun_2760();
    }
    fun_2520();
    fun_2760();
    rpl_fts_set(rbp9, r15_10, 4, rbp9, r15_10, 4);
    rpl_fts_read(rbp11, rbp11);
    goto 0x2c6b;
}

uint32_t fun_2640(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2820(int64_t rdi, void** rsi);

uint32_t fun_2810(void** rdi, void** rsi);

void** fun_2850(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_74e5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2520();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2520();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2540(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_77e3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_77e3_22; else 
                            goto addr_7bdd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_7c9d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_7ff0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_77e0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_77e0_30; else 
                                goto addr_8009_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2540(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_7ff0_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_2640(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_7ff0_28; else 
                            goto addr_768c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_8150_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_7fd0_40:
                        if (r11_27 == 1) {
                            addr_7b5d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_8118_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_7797_44;
                            }
                        } else {
                            goto addr_7fe0_46;
                        }
                    } else {
                        addr_815f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_7b5d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_77e3_22:
                                if (v47 != 1) {
                                    addr_7d39_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2540(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_7d84_54;
                                    }
                                } else {
                                    goto addr_77f0_56;
                                }
                            } else {
                                addr_7795_57:
                                ebp36 = 0;
                                goto addr_7797_44;
                            }
                        } else {
                            addr_7fc4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_815f_47; else 
                                goto addr_7fce_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_7b5d_41;
                        if (v47 == 1) 
                            goto addr_77f0_56; else 
                            goto addr_7d39_52;
                    }
                }
                addr_7851_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_76e8_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_770d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_7a10_65;
                    } else {
                        addr_7879_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_80c8_67;
                    }
                } else {
                    goto addr_7870_69;
                }
                addr_7721_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_776c_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_80c8_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_776c_81;
                }
                addr_7870_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_770d_64; else 
                    goto addr_7879_66;
                addr_7797_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_784f_91;
                if (v22) 
                    goto addr_77af_93;
                addr_784f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_7851_62;
                addr_7d84_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_850b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_857b_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_837f_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2820(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2810(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_7e7e_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_783c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_7e88_112;
                    }
                } else {
                    addr_7e88_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_7f59_114;
                }
                addr_7848_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_784f_91;
                while (1) {
                    addr_7f59_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_8467_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_7ec6_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_8475_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_7f47_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_7f47_128;
                        }
                    }
                    addr_7ef5_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_7f47_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_7ec6_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_7ef5_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_776c_81;
                addr_8475_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_80c8_67;
                addr_850b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_7e7e_109;
                addr_857b_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_7e7e_109;
                addr_77f0_56:
                rax93 = fun_2850(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_783c_110;
                addr_7fce_59:
                goto addr_7fd0_40;
                addr_7c9d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_77e3_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_7848_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_7795_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_77e3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_7ce2_160;
                if (!v22) 
                    goto addr_80b7_162; else 
                    goto addr_82c3_163;
                addr_7ce2_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_80b7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_80c8_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_7b8b_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_79f3_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_7721_70; else 
                    goto addr_7a07_169;
                addr_7b8b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_76e8_63;
                goto addr_7870_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_7fc4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_80ff_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_77e0_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_76d8_178; else 
                        goto addr_8082_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_7fc4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_77e3_22;
                }
                addr_80ff_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_77e0_30:
                    r8d42 = 0;
                    goto addr_77e3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_7851_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_8118_42;
                    }
                }
                addr_76d8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_76e8_63;
                addr_8082_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_7fe0_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_7851_62;
                } else {
                    addr_8092_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_77e3_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_8842_188;
                if (v28) 
                    goto addr_80b7_162;
                addr_8842_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_79f3_168;
                addr_768c_37:
                if (v22) 
                    goto addr_8683_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_76a3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_8150_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_81db_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_77e3_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_76d8_178; else 
                        goto addr_81b7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_7fc4_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_77e3_22;
                }
                addr_81db_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_77e3_22;
                }
                addr_81b7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_7fe0_46;
                goto addr_8092_186;
                addr_76a3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_77e3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_77e3_22; else 
                    goto addr_76b4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_878e_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_8614_210;
            if (1) 
                goto addr_8612_212;
            if (!v29) 
                goto addr_824e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2530();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_8781_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_7a10_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_77cb_219; else 
            goto addr_7a2a_220;
        addr_77af_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_77c3_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_7a2a_220; else 
            goto addr_77cb_219;
        addr_837f_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_7a2a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2530();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_839d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2530();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_8810_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_8276_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_8467_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_77c3_221;
        addr_82c3_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_77c3_221;
        addr_7a07_169:
        goto addr_7a10_65;
        addr_878e_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_7a2a_220;
        goto addr_839d_222;
        addr_8614_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_866e_236;
        fun_2570();
        rsp25 = rsp25 - 8 + 8;
        goto addr_8810_225;
        addr_8612_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_8614_210;
        addr_824e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_8614_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_8276_226;
        }
        addr_8781_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_7bdd_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd08c + rax113 * 4) + 0xd08c;
    addr_8009_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd18c + rax114 * 4) + 0xd18c;
    addr_8683_190:
    addr_77cb_219:
    goto 0x74b0;
    addr_76b4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xcf8c + rax115 * 4) + 0xcf8c;
    addr_866e_236:
    goto v116;
}

void fun_76d0() {
}

void fun_7888() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x7582;
}

void fun_78e1() {
    goto 0x7582;
}

void fun_79ce() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x7851;
    }
    if (v2) 
        goto 0x82c3;
    if (!r10_3) 
        goto addr_842e_5;
    if (!v4) 
        goto addr_82fe_7;
    addr_842e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_82fe_7:
    goto 0x7704;
}

void fun_79ec() {
}

void fun_7a97() {
    signed char v1;

    if (v1) {
        goto 0x7a1f;
    } else {
        goto 0x775a;
    }
}

void fun_7ab1() {
    signed char v1;

    if (!v1) 
        goto 0x7aaa; else 
        goto "???";
}

void fun_7ad8() {
    goto 0x79f3;
}

void fun_7b58() {
}

void fun_7b70() {
}

void fun_7b9f() {
    goto 0x79f3;
}

void fun_7bf1() {
    goto 0x7b80;
}

void fun_7c20() {
    goto 0x7b80;
}

void fun_7c53() {
    goto 0x7b80;
}

void fun_8020() {
    goto 0x76d8;
}

void fun_831e() {
    signed char v1;

    if (v1) 
        goto 0x82c3;
    goto 0x7704;
}

void fun_83c5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x7704;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x76e8;
        goto 0x7704;
    }
}

void fun_87e2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x7a50;
    } else {
        goto 0x7582;
    }
}

void fun_9958() {
    fun_2520();
}

void fun_29f0() {
    goto 0x2998;
}

struct s68 {
    signed char[120] pad120;
    void** f78;
};

struct s69 {
    signed char[112] pad112;
    int64_t f70;
};

void fun_2bd9() {
    int1_t zf1;
    struct s10* rax2;
    struct s68* r15_3;
    struct s69* r15_4;

    zf1 = recurse == 0;
    if (zf1) {
        goto 0x2c6b;
    } else {
        rax2 = root_dev_ino;
        if (!rax2) 
            goto 0x2c10;
        if (r15_3->f78 != rax2->f0) 
            goto 0x2c10;
        if (r15_4->f70 == rax2->f8) 
            goto 0x319c;
    }
}

struct s70 {
    signed char[88] pad88;
    int64_t f58;
};

struct s71 {
    signed char[32] pad32;
    int64_t f20;
};

struct s72 {
    signed char[32] pad32;
    int64_t f20;
};

void fun_2ce5() {
    struct s70* r15_1;
    struct s71* r15_2;
    void** r14_3;
    void** rdx4;
    struct s72* r15_5;
    struct s9* rbp6;
    void** r15_7;

    if (r15_1->f58 || r15_2->f20) {
        quotearg_style(4, r14_3, rdx4);
        fun_2520();
        fun_2760();
        goto 0x2c60;
    } else {
        r15_5->f20 = 1;
        rpl_fts_set(rbp6, r15_7, 1);
    }
}

void fun_2d61() {
    void** r14_1;
    void** rdx2;

    quotearg_style(4, r14_1, rdx2);
    goto 0x2d13;
}

void fun_790e() {
    goto 0x7582;
}

void fun_7ae4() {
    goto 0x7a9c;
}

void fun_7bab() {
    goto 0x76d8;
}

void fun_7bfd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x7b80;
    goto 0x77af;
}

void fun_7c2f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x7b8b;
        goto 0x75b0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x7a2a;
        goto 0x77cb;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x83c8;
    if (r10_8 > r15_9) 
        goto addr_7b15_9;
    addr_7b1a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x83d3;
    goto 0x7704;
    addr_7b15_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_7b1a_10;
}

void fun_7c62() {
    goto 0x7797;
}

void fun_8030() {
    goto 0x7797;
}

void fun_87cf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x78ec;
    } else {
        goto 0x7a50;
    }
}

void fun_9a10() {
}

void fun_2a00() {
    recurse = 1;
    goto 0x2998;
}

signed char cycle_warning_required(int64_t rdi, int64_t rsi);

void fun_2d7f() {
    int64_t rbp1;
    int64_t r15_2;
    signed char al3;

    al3 = cycle_warning_required(rbp1, r15_2);
    if (!al3) 
        goto 0x2c10;
    quotearg_n_style_colon();
    fun_2520();
    fun_2760();
    goto 0x2c6b;
}

void fun_7c6c() {
    goto 0x7c07;
}

void fun_803a() {
    goto 0x7b5d;
}

void fun_9a70() {
    fun_2520();
    goto fun_2800;
}

void fun_2a10() {
    goto 0x2998;
}

void fun_793d() {
    goto 0x7582;
}

void fun_7c78() {
    goto 0x7c07;
}

void fun_8047() {
    goto 0x7bae;
}

void fun_9ab0() {
    fun_2520();
    goto fun_2800;
}

void fun_2a20() {
    goto 0x2998;
}

void fun_796a() {
    goto 0x7582;
}

void fun_7c84() {
    goto 0x7b80;
}

void fun_9af0() {
    fun_2520();
    goto fun_2800;
}

void fun_2a30() {
    verbose = 1;
    goto 0x2998;
}

void fun_798c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x8320;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x7851;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x7851;
    }
    if (v11) 
        goto 0x8683;
    if (r10_12 > r15_13) 
        goto addr_86d3_8;
    addr_86d8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x8411;
    addr_86d3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_86d8_9;
}

struct s73 {
    signed char[24] pad24;
    int64_t f18;
};

struct s74 {
    signed char[16] pad16;
    void** f10;
};

struct s75 {
    signed char[8] pad8;
    void** f8;
};

void fun_9b40() {
    int64_t r15_1;
    struct s73* rbx2;
    void** r14_3;
    struct s74* rbx4;
    void** r13_5;
    struct s75* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    int64_t rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2520();
    fun_2800(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x9b62, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_2a40() {
    goto 0x2998;
}

void fun_9b98() {
    fun_2520();
    goto 0x9b69;
}

void fun_2a50() {
    goto 0x2998;
}

struct s76 {
    signed char[32] pad32;
    int64_t f20;
};

struct s77 {
    signed char[24] pad24;
    int64_t f18;
};

struct s78 {
    signed char[16] pad16;
    void** f10;
};

struct s79 {
    signed char[8] pad8;
    void** f8;
};

struct s80 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_9bd0() {
    int64_t rcx1;
    struct s76* rbx2;
    int64_t r15_3;
    struct s77* rbx4;
    void** r14_5;
    struct s78* rbx6;
    void** r13_7;
    struct s79* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s80* rbx12;
    void** rax13;
    int64_t rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2520();
    fun_2800(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x9c04, __return_address(), rcx1);
    goto v15;
}

void fun_2a60() {
    goto 0x2998;
}

void fun_9c48() {
    fun_2520();
    goto 0x9c0b;
}
