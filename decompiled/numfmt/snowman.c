
int32_t delimiter = 0x80;

void** fun_2790(void** rdi, ...);

signed char process_field(void** rdi, void** rsi);

void** stdout = reinterpret_cast<void**>(0);

void fun_2550();

unsigned char line_delim = 10;

uint32_t process_line(void** rdi, uint32_t esi, ...) {
    void** r15_3;
    uint32_t r13d4;
    int32_t ebp5;
    int32_t edx6;
    uint32_t r14d7;
    void** rax8;
    void** rdx9;
    void** rbx10;
    int64_t rax11;
    uint32_t eax12;
    int64_t rcx13;
    signed char al14;
    int32_t esi15;
    void** rdi16;
    signed char* rax17;
    int32_t eax18;
    signed char al19;
    void** rdi20;
    uint32_t edx21;
    signed char* rax22;

    r15_3 = reinterpret_cast<void**>(1);
    r13d4 = esi;
    ebp5 = 1;
    edx6 = delimiter;
    r14d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (edx6 != 0x80) {
        goto addr_5040_3;
    }
    addr_50d1_4:
    while (*reinterpret_cast<unsigned char*>(&r14d7)) {
        rax8 = fun_2790(rdi, rdi);
        rdi = rdi;
        rdx9 = *reinterpret_cast<void***>(rax8);
        rbx10 = rdi;
        do {
            *reinterpret_cast<uint32_t*>(&rax11) = *reinterpret_cast<unsigned char*>(&r14d7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx9) + reinterpret_cast<uint64_t>(rax11 * 2))) & 1) 
                continue;
            if (*reinterpret_cast<unsigned char*>(&r14d7) != 10) 
                break;
            r14d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10 + 1));
            ++rbx10;
        } while (*reinterpret_cast<unsigned char*>(&r14d7));
        goto addr_5192_9;
        eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10));
        if (!*reinterpret_cast<unsigned char*>(&eax12)) 
            break;
        do {
            *reinterpret_cast<uint32_t*>(&rcx13) = *reinterpret_cast<unsigned char*>(&eax12);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx13) + 4) = 0;
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx9) + reinterpret_cast<uint64_t>(rcx13 * 2))) & 1) 
                break;
            if (*reinterpret_cast<unsigned char*>(&eax12) == 10) 
                break;
            eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10 + 1));
            ++rbx10;
        } while (*reinterpret_cast<unsigned char*>(&eax12));
        goto addr_51a8_14;
        r14d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10));
        while (*reinterpret_cast<unsigned char*>(&r14d7)) {
            while (1) {
                *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
                al14 = process_field(rdi, r15_3);
                edx6 = delimiter;
                esi15 = 32;
                rdi16 = stdout;
                if (!al14) {
                    ebp5 = 0;
                }
                rax17 = *reinterpret_cast<signed char**>(rdi16 + 40);
                if (edx6 != 0x80) {
                    esi15 = edx6;
                }
                if (reinterpret_cast<uint64_t>(rax17) >= reinterpret_cast<uint64_t>(*reinterpret_cast<signed char**>(rdi16 + 48))) {
                    fun_2550();
                    edx6 = delimiter;
                } else {
                    *reinterpret_cast<signed char**>(rdi16 + 40) = rax17 + 1;
                    *rax17 = *reinterpret_cast<signed char*>(&esi15);
                }
                rdi = rbx10 + 1;
                ++r15_3;
                r14d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
                if (edx6 == 0x80) 
                    goto addr_50d1_4;
                addr_5040_3:
                if (static_cast<int32_t>(*reinterpret_cast<signed char*>(&r14d7)) == edx6) 
                    break;
                if (!*reinterpret_cast<unsigned char*>(&r14d7)) 
                    goto addr_5130_26;
                rbx10 = rdi;
                do {
                    eax18 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx10 + 1));
                    ++rbx10;
                    if (!*reinterpret_cast<signed char*>(&eax18)) 
                        goto addr_5130_26;
                } while (eax18 != edx6);
            }
            rbx10 = rdi;
        }
        goto addr_5129_31;
    }
    addr_5130_26:
    al19 = process_field(rdi, r15_3);
    if (!al19) {
        ebp5 = 0;
    }
    if (*reinterpret_cast<signed char*>(&r13d4)) {
        rdi20 = stdout;
        edx21 = line_delim;
        rax22 = *reinterpret_cast<signed char**>(rdi20 + 40);
        if (reinterpret_cast<uint64_t>(rax22) >= reinterpret_cast<uint64_t>(*reinterpret_cast<signed char**>(rdi20 + 48))) {
            fun_2550();
        } else {
            *reinterpret_cast<signed char**>(rdi20 + 40) = rax22 + 1;
            *rax22 = *reinterpret_cast<signed char*>(&edx21);
        }
    }
    return static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ebp5));
    addr_5192_9:
    goto addr_5130_26;
    addr_51a8_14:
    goto addr_5130_26;
    addr_5129_31:
    goto addr_5130_26;
}

int32_t decimal_point_length = 0;

void** decimal_point = reinterpret_cast<void**>(0);

int32_t fun_2430(void** rdi, void** rsi, void** rdx, ...);

uint32_t simple_strtod_int(void** rdi, void** rsi, void* rdx, void** rcx, void** r8, int64_t r9) {
    int32_t ebp7;
    uint32_t eax8;
    void** rdx9;
    void** rsi10;
    int32_t eax11;
    uint32_t r8d12;
    void** rdi13;
    uint32_t ecx14;
    uint1_t pf15;
    int1_t zf16;
    int32_t edx17;

    ebp7 = 0;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45)) {
        ++rdi;
        ebp7 = 1;
    }
    *reinterpret_cast<void***>(rcx) = *reinterpret_cast<void***>(&ebp7);
    *reinterpret_cast<void***>(rsi) = rdi;
    eax8 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 48);
    if (eax8 > 9) {
        rdx9 = reinterpret_cast<void**>(static_cast<int64_t>(decimal_point_length));
        rsi10 = decimal_point;
        eax11 = fun_2430(rdi, rsi10, rdx9);
        r8d12 = 3;
        if (eax11) {
            addr_3715_5:
            return r8d12;
        } else {
            r8d12 = 0;
            __asm__("fldz ");
        }
    } else {
        __asm__("fldz ");
        rdi13 = rdi + 1;
        ecx14 = 0;
        r8d12 = 0;
        pf15 = __intrinsic();
        zf16 = 1;
        while (1) {
            __asm__("fldz ");
            __asm__("fucomip st0, st1");
            *reinterpret_cast<unsigned char*>(&edx17) = pf15;
            if (!zf16) {
                edx17 = 1;
            }
            if (*reinterpret_cast<unsigned char*>(&edx17) || eax8) {
                ++ecx14;
                if (ecx14 <= 18) {
                    addr_36e9_12:
                    __asm__("fmul dword [rip+0x96c1]");
                    *reinterpret_cast<void***>(rsi) = rdi13;
                    __asm__("fild dword [rsp+0xc]");
                    ++rdi13;
                    eax8 = *reinterpret_cast<signed char*>(rdi13 + 0xffffffffffffffff) - 48;
                    __asm__("faddp st1, st0");
                    pf15 = __intrinsic();
                    zf16 = eax8 == 9;
                    if (eax8 <= 9) 
                        continue; else 
                        break;
                } else {
                    if (ecx14 == 28) 
                        goto addr_3755_14;
                }
            } else {
                if (ecx14 <= 18) 
                    goto addr_36e9_12;
            }
            r8d12 = 1;
            goto addr_36e9_12;
        }
    }
    if (*reinterpret_cast<void***>(&ebp7)) {
        __asm__("fchs ");
    }
    __asm__("fstp tword [rbx]");
    goto addr_3715_5;
    addr_3755_14:
    __asm__("fstp st0");
    r8d12 = 2;
    goto addr_3715_5;
}

void** g28;

void** frp = reinterpret_cast<void**>(0);

void fun_25c0(void** rdi, void** rsi, void** rdx, ...);

void** suffix = reinterpret_cast<void**>(0);

void** fun_2500(void** rdi, ...);

int32_t fun_25e0(void** rdi, signed char* rsi, void** rdx);

unsigned char dev_debug = 0;

void** stderr = reinterpret_cast<void**>(0);

void fun_2730(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8);

void** quote(void** rdi, ...);

void fun_2740(void** rdi, void** rsi, void** rdx, ...);

int32_t auto_padding = 0;

uint32_t scale_from = 0;

void** quote_n();

void** padding_buffer_size = reinterpret_cast<void**>(0);

void** padding_width = reinterpret_cast<void**>(0);

void** padding_buffer = reinterpret_cast<void**>(0);

void** xrealloc(void** rdi, ...);

void** inval_style = reinterpret_cast<void**>(0);

void** fun_24e0();

uint32_t conv_exit_code = 2;

void fun_26d0();

void** from_unit_size = reinterpret_cast<void**>(1);

void** to_unit_size = reinterpret_cast<void**>(1);

void** user_precision = reinterpret_cast<void**>(0xff);

uint32_t scale_to = 0;

uint32_t grouping = 0;

uint32_t round_style = 2;

void** zero_padding_width = reinterpret_cast<void**>(0);

uint32_t fun_23f0(void** rdi, void** rsi, int64_t rdx, ...);

signed char debug = 0;

void** g1;

void** g5;

int64_t fun_2540(void** rdi, ...);

void fun_2450(void** rdi, void** rsi);

int32_t padding_alignment = 1;

void** mbsalign(void** rdi, void** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9);

void** format_str_prefix = reinterpret_cast<void**>(0);

void** format_str_suffix = reinterpret_cast<void**>(0);

uint64_t fun_2510();

void fun_2750(void** rdi, void** rsi);

signed char process_field(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void* rsp5;
    void** rax6;
    void** v7;
    void** rax8;
    void** rdx9;
    void** rsi10;
    void** rdi11;
    void* rsp12;
    void** r13_13;
    void** rax14;
    void** rax15;
    signed char* rbx16;
    int32_t eax17;
    uint32_t edx18;
    void** rcx19;
    void** r8_20;
    int64_t rbx21;
    void** r13_22;
    int32_t eax23;
    void** rax24;
    void** rax25;
    int32_t edx26;
    int64_t r15_27;
    uint32_t eax28;
    void** r14_29;
    int32_t r14d30;
    int32_t r14d31;
    uint32_t r14d32;
    int32_t r14d33;
    void** rax34;
    void** rdi35;
    void** r8_36;
    void** rax37;
    int1_t cf38;
    void** rdi39;
    void** rax40;
    int1_t zf41;
    void** rcx42;
    void** rdi43;
    void** rsi44;
    void** v45;
    uint32_t eax46;
    void* rsp47;
    void** rbx48;
    void** v49;
    int64_t rax50;
    void** rdx51;
    int32_t eax52;
    uint32_t eax53;
    signed char v54;
    int1_t zf55;
    int1_t zf56;
    void** rax57;
    void** rax58;
    void** rdx59;
    void** rax60;
    int1_t cf61;
    void** v62;
    uint32_t eax63;
    int1_t cf64;
    uint32_t tmp32_65;
    void* rdx66;
    int64_t r13_67;
    void** r9_68;
    uint32_t eax69;
    void** rcx70;
    void** rax71;
    int32_t eax72;
    int32_t edx73;
    void** rdx74;
    void** rdi75;
    void** v76;
    void* rsp77;
    uint32_t eax78;
    void* rsp79;
    int1_t zf80;
    int1_t zf81;
    void** rax82;
    int1_t zf83;
    void** rax84;
    void** rax85;
    void** rdi86;
    int1_t zf87;
    void** rax88;
    void** rax89;
    uint32_t eax90;
    int1_t cf91;
    void*** rdi92;
    int1_t cf93;
    uint32_t tmp32_94;
    void** rdi95;
    void** rdi96;
    void** rsi97;
    uint1_t cf98;
    int1_t pf99;
    uint1_t zf100;
    uint1_t below_or_equal101;
    int64_t rax102;
    void** rax103;
    int64_t rax104;
    uint64_t rax105;
    uint32_t edx106;
    uint32_t ebx107;
    int1_t zf108;
    void** v109;
    void** rax110;
    int32_t eax111;
    signed char v112;
    void** rax113;
    void* rsp114;
    int32_t edi115;
    void** rsi116;
    int64_t rax117;
    void** rdx118;
    int64_t rax119;
    int32_t eax120;
    void** rdi121;
    int32_t r14d122;
    int32_t r14d123;
    uint32_t r14d124;
    int32_t r14d125;
    int64_t rax126;
    int32_t eax127;
    void** rdi128;
    void** rax129;
    void* rsp130;
    void** rdx131;
    void** rdi132;
    void** rax133;
    void* rsp134;
    uint64_t r8_135;
    int1_t zf136;
    void** rdi137;
    void** rax138;
    void** rdi139;
    void** rdi140;
    void** rdi141;
    void** rdi142;
    void* rax143;
    void** rdi144;
    uint32_t eax145;
    uint32_t tmp32_146;
    uint1_t cf147;
    void** rdi148;

    r12_3 = rdi;
    rbp4 = rsi;
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x168);
    rax6 = g28;
    v7 = rax6;
    rax8 = frp;
    if (rax8) {
        while (rdx9 = *reinterpret_cast<void***>(rax8), !reinterpret_cast<int1_t>(rdx9 == 0xffffffffffffffff)) {
            if (reinterpret_cast<unsigned char>(rbp4) < reinterpret_cast<unsigned char>(rdx9)) 
                goto addr_392b_4;
            if (reinterpret_cast<unsigned char>(rbp4) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax8 + 8))) 
                goto addr_3986_6;
            addr_392b_4:
            rax8 = rax8 + 16;
        }
    } else {
        if (reinterpret_cast<int1_t>(rsi == 1)) 
            goto addr_3986_6;
    }
    rsi10 = stdout;
    rdi11 = r12_3;
    *reinterpret_cast<int32_t*>(&r12_3) = 1;
    *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
    fun_25c0(rdi11, rsi10, rdx9);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    goto addr_394d_10;
    addr_3986_6:
    r13_13 = suffix;
    if (r13_13 && (rax14 = fun_2500(r12_3), rdi = r13_13, rax15 = fun_2500(rdi), rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8), reinterpret_cast<unsigned char>(rax14) > reinterpret_cast<unsigned char>(rax15))) {
        rdi = r13_13;
        rbx16 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax14) - reinterpret_cast<unsigned char>(rax15)) + reinterpret_cast<unsigned char>(r12_3));
        eax17 = fun_25e0(rdi, rbx16, rdx9);
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        edx18 = dev_debug;
        if (eax17) {
            if (*reinterpret_cast<signed char*>(&edx18)) {
                rcx19 = stderr;
                rdi = reinterpret_cast<void**>("no valid suffix found\n");
                fun_2730("no valid suffix found\n", 1, 22, rcx19, r8_20);
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
            }
        } else {
            *rbx16 = 0;
            if (*reinterpret_cast<signed char*>(&edx18)) {
                quote(r13_13, r13_13);
                rdi = stderr;
                fun_2740(rdi, 1, "trimming suffix %s\n", rdi, 1, "trimming suffix %s\n");
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8);
            }
        }
    }
    *reinterpret_cast<uint32_t*>(&rbx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx21) + 4) = 0;
    if (!*reinterpret_cast<signed char*>(&rbx21)) {
        r13_22 = r12_3;
        eax23 = 0;
    } else {
        rax24 = fun_2790(rdi, rdi);
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        r13_22 = r12_3;
        rax25 = *reinterpret_cast<void***>(rax24);
        do {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax25) + reinterpret_cast<uint64_t>(rbx21 * 2))) & 1)) 
                break;
            *reinterpret_cast<uint32_t*>(&rbx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_22 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx21) + 4) = 0;
            ++r13_22;
        } while (*reinterpret_cast<signed char*>(&rbx21));
        eax23 = *reinterpret_cast<int32_t*>(&r12_3) - *reinterpret_cast<int32_t*>(&r13_22);
    }
    edx26 = auto_padding;
    if (!edx26) {
        addr_3c20_24:
        *reinterpret_cast<uint32_t*>(&r15_27) = scale_from;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_27) + 4) = 0;
        __asm__("pxor xmm1, xmm1");
        eax28 = dev_debug;
        __asm__("movss [rsp], xmm1");
        __asm__("fld dword [rsp]");
        __asm__("fstp tword [rsp+0x70]");
        *reinterpret_cast<uint32_t*>(&r14_29) = (r14d30 - (r14d31 + reinterpret_cast<uint1_t>(r14d32 < r14d33 + reinterpret_cast<uint1_t>(static_cast<uint32_t>(r15_27 - 3) < 2))) & 24) + 0x3e8;
        if (*reinterpret_cast<signed char*>(&eax28)) {
            rax34 = quote_n();
            quote_n();
            rdi35 = stderr;
            r8_36 = rax34;
            fun_2740(rdi35, 1, "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n", rdi35, 1, "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n");
            rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8 - 8 + 8);
        }
    } else {
        if (eax23 || reinterpret_cast<signed char>(rbp4) > reinterpret_cast<signed char>(1)) {
            rax37 = fun_2500(r12_3, r12_3);
            rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
            cf38 = reinterpret_cast<unsigned char>(rax37) < reinterpret_cast<unsigned char>(padding_buffer_size);
            padding_width = rax37;
            if (!cf38) {
                rdi39 = padding_buffer;
                padding_buffer_size = rax37 + 1;
                rax40 = xrealloc(rdi39);
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
                padding_buffer = rax40;
            }
        } else {
            padding_width = reinterpret_cast<void**>(0);
        }
        zf41 = dev_debug == 0;
        if (!zf41) 
            goto addr_3bff_31; else 
            goto addr_3a49_32;
    }
    addr_3a7e_33:
    rcx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 94);
    rdi43 = r13_22;
    rsi44 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 96);
    v45 = reinterpret_cast<void**>(0x3a95);
    eax46 = simple_strtod_int(rdi43, rsi44, reinterpret_cast<int64_t>(rsp5) + 0x70, rcx42, r8_36, 18);
    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    *reinterpret_cast<uint32_t*>(&rbx48) = eax46;
    if (eax46 > 1) {
        addr_3f38_34:
        __asm__("fld tword [rsp+0x70]");
        __asm__("fld st0");
        if (*reinterpret_cast<uint32_t*>(&rbx48) > 6) {
            __asm__("fstp st1");
            v49 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&rbp4) = 0;
        } else {
            *reinterpret_cast<uint32_t*>(&rax50) = *reinterpret_cast<uint32_t*>(&rbx48);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax50) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xcba0 + rax50 * 4) + 0xcba0;
        }
    } else {
        rbp4 = reinterpret_cast<void**>(0);
        rdx51 = reinterpret_cast<void**>(static_cast<int64_t>(decimal_point_length));
        rsi44 = decimal_point;
        rdi43 = reinterpret_cast<void**>(0);
        v45 = reinterpret_cast<void**>(0x3ac0);
        eax52 = fun_2430(0, rsi44, rdx51);
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
        __asm__("fld tword [rsp+0x70]");
        v49 = reinterpret_cast<void**>(0);
        if (!eax52) {
            __asm__("fstp tword [rsp+0x20]");
            __asm__("fld dword [rsp]");
            rdi43 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rdx51)));
            rcx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 95);
            rsi44 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0x68);
            __asm__("fstp tword [rsp+0x80]");
            v45 = reinterpret_cast<void**>(0x3e22);
            eax53 = simple_strtod_int(rdi43, rsi44, reinterpret_cast<int64_t>(rsp47) + 0x80, rcx42, r8_36, 18);
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            if (eax53 > 1) {
                *reinterpret_cast<uint32_t*>(&rbx48) = eax53;
                goto addr_3f38_34;
            } else {
                __asm__("fld tword [rsp+0x20]");
                rdi43 = rdi43;
                if (eax53 == 1) {
                    *reinterpret_cast<uint32_t*>(&rbx48) = 1;
                }
                if (v54) 
                    goto addr_4328_44; else 
                    goto addr_3e47_45;
            }
        } else {
            addr_3ada_46:
            zf55 = dev_debug == 0;
            if (!zf55) {
                rdi43 = stderr;
                __asm__("fld st0");
                __asm__("fstp tword [rsp]");
                *reinterpret_cast<int32_t*>(&rsi44) = 1;
                *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
                __asm__("fstp tword [rsp+0x30]");
                fun_2740(rdi43, 1, "  parsed numeric value: %Lf\n  input precision = %d\n", rdi43, 1, "  parsed numeric value: %Lf\n  input precision = %d\n");
                rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
                __asm__("fld tword [rsp+0x20]");
                goto addr_3ae7_48;
            }
        }
    }
    addr_3f70_49:
    zf56 = reinterpret_cast<int1_t>(inval_style == 3);
    if (!zf56) {
        __asm__("fstp tword [rsp+0x20]");
        rax57 = quote(r13_22, r13_22);
        fun_24e0();
        *reinterpret_cast<uint32_t*>(&rdi43) = conv_exit_code;
        *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
        rcx42 = rax57;
        *reinterpret_cast<int32_t*>(&rsi44) = 0;
        *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
        v45 = reinterpret_cast<void**>(0x3fb0);
        fun_26d0();
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 8 + 8 - 8 + 8);
        __asm__("fld tword [rsp+0x20]");
    }
    while (1) {
        rax58 = from_unit_size;
        rdx59 = to_unit_size;
        if (rax58 != 1 || !reinterpret_cast<int1_t>(rdx59 == 1)) {
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<signed char>(rax58) < reinterpret_cast<signed char>(0)) {
                __asm__("fadd dword [rip+0x9200]");
            }
            __asm__("fmulp st1, st0");
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<signed char>(rdx59) < reinterpret_cast<signed char>(0)) {
                __asm__("fadd dword [rip+0x9106]");
            }
            __asm__("fdivp st1, st0");
        }
        if (*reinterpret_cast<uint32_t*>(&rbx48) > 1) 
            break;
        rax60 = user_precision;
        __asm__("fld tword [rip+0x8e03]");
        __asm__("fxch st0, st1");
        cf61 = reinterpret_cast<unsigned char>(rax60) < reinterpret_cast<unsigned char>(0xffffffffffffffff);
        if (rax60 == 0xffffffffffffffff) {
            rax60 = v49;
        }
        __asm__("fcomi st0, st1");
        __asm__("fstp st1");
        v62 = rax60;
        if (cf61 || cf61) {
            *reinterpret_cast<uint32_t*>(&rbp4) = scale_to;
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rbp4)) 
                goto addr_40b6_63;
            if (reinterpret_cast<unsigned char>(v62) > reinterpret_cast<unsigned char>(18)) 
                goto addr_447e_65;
        } else {
            __asm__("fld st0");
            eax63 = 0;
            cf64 = 0;
            __asm__("fld dword [rip+0x8da4]");
            while (!cf64) {
                __asm__("fdivp st2, st0");
                tmp32_65 = eax63 + 1;
                cf64 = tmp32_65 < eax63;
                eax63 = tmp32_65;
            }
            __asm__("fstp st0");
            __asm__("fstp st0");
            __asm__("fstp st0");
            *reinterpret_cast<uint32_t*>(&rbp4) = scale_to;
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rbp4)) 
                goto addr_4052_70;
            *reinterpret_cast<uint32_t*>(&rdx66) = eax63;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx66) + 4) = 0;
            rdx59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx66) + reinterpret_cast<unsigned char>(v62));
            if (reinterpret_cast<unsigned char>(rdx59) > reinterpret_cast<unsigned char>(18)) 
                goto addr_447e_65;
            addr_4052_70:
            if (eax63 > 26) 
                goto addr_4057_72;
        }
        addr_40b6_63:
        *reinterpret_cast<uint32_t*>(&r14_29) = grouping;
        r12_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0x91);
        *reinterpret_cast<uint32_t*>(&r13_67) = round_style;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_67) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r14_29)) {
            r12_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0x92);
        }
        r9_68 = zero_padding_width;
        if (r9_68) {
            rdi43 = r12_3;
            r8_36 = reinterpret_cast<void**>("0%ld");
            __asm__("fstp tword [rsp+0x20]");
            *reinterpret_cast<int32_t*>(&rsi44) = 62;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            v45 = reinterpret_cast<void**>(0x4470);
            eax69 = fun_23f0(rdi43, 62, 1, rdi43, 62, 1);
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
            r12_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_3) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax69))));
        }
        *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
        if (*reinterpret_cast<unsigned char*>(&rcx42)) {
            rcx70 = stderr;
            *reinterpret_cast<int32_t*>(&rsi44) = 1;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            rdi43 = reinterpret_cast<void**>("double_to_human:\n");
            __asm__("fstp tword [rsp+0x20]");
            v45 = reinterpret_cast<void**>(0x443c);
            fun_2730("double_to_human:\n", 1, 17, rcx70, r8_36);
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
            __asm__("fld tword [rsp+0x20]");
        }
        if (*reinterpret_cast<uint32_t*>(&rbp4)) 
            goto addr_4110_79;
        rax71 = v62;
        if (!*reinterpret_cast<int32_t*>(&rax71)) {
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fld tword [rip+0x80f8]");
            __asm__("fld st1");
            __asm__("fdiv st0, st1");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fild qword [rsp+0x20]");
            __asm__("fmulp st1, st0");
            __asm__("fld st1");
            __asm__("fsub st0, st1");
            if (*reinterpret_cast<uint32_t*>(&r13_67) <= 4) 
                goto addr_4d39_82;
            __asm__("fstp st0");
            __asm__("fstp st0");
        } else {
            eax72 = *reinterpret_cast<int32_t*>(&rax71) - 1;
            if (!eax72) {
                __asm__("fnstcw word [rsp+0x4e]");
                __asm__("fld tword [rip+0x7f0a]");
                __asm__("fld st1");
                __asm__("fmul dword [rip+0x7eb1]");
                __asm__("fld st0");
                __asm__("fdiv st0, st2");
                __asm__("fldcw word [rsp+0x4c]");
                __asm__("fistp qword [rsp+0x20]");
                __asm__("fldcw word [rsp+0x4e]");
                __asm__("fild qword [rsp+0x20]");
                __asm__("fmulp st2, st0");
                __asm__("fsub st0, st1");
                if (*reinterpret_cast<uint32_t*>(&r13_67) <= 4) 
                    goto addr_4f2d_86;
                __asm__("fstp st0");
                __asm__("fstp st0");
            } else {
                __asm__("fld dword [rip+0x8a2e]");
                edx73 = eax72;
                __asm__("fld st0");
                do {
                    __asm__("fmul st0, st1");
                    --edx73;
                } while (edx73);
                __asm__("fnstcw word [rsp+0x4e]");
                __asm__("fmul st0, st2");
                __asm__("fld tword [rip+0x8a5d]");
                __asm__("fld st1");
                __asm__("fdiv st0, st1");
                __asm__("fldcw word [rsp+0x4c]");
                __asm__("fistp qword [rsp+0x20]");
                __asm__("fldcw word [rsp+0x4e]");
                __asm__("fild qword [rsp+0x20]");
                __asm__("fmulp st1, st0");
                __asm__("fsub st1, st0");
                if (*reinterpret_cast<uint32_t*>(&r13_67) <= 4) 
                    goto addr_43d2_91;
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fldz ");
                __asm__("fxch st0, st1");
                __asm__("fld st0");
                do {
                    __asm__("fmul st0, st1");
                    --eax72;
                } while (eax72);
                __asm__("fstp st1");
                __asm__("fdivp st1, st0");
                goto addr_461b_96;
            }
        }
        __asm__("fldz ");
        addr_461b_96:
        *reinterpret_cast<uint32_t*>(&rbx48) = *reinterpret_cast<uint32_t*>(&v62);
        *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
        if (!*reinterpret_cast<unsigned char*>(&rcx42)) {
            __asm__("fxch st0, st1");
        } else {
            __asm__("fxch st0, st1");
            __asm__("fstp tword [rsp+0x10]");
            rdx74 = reinterpret_cast<void**>("  no scaling, returning (grouped) value: %'.*Lf\n");
            rdi75 = stderr;
            if (!*reinterpret_cast<uint32_t*>(&r14_29)) {
                rdx74 = reinterpret_cast<void**>("  no scaling, returning value: %.*Lf\n");
            }
            __asm__("fld st0");
            __asm__("fstp tword [rsp]");
            __asm__("fstp tword [rsp+0x10]");
            fun_2740(rdi75, 1, rdx74, rdi75, 1, rdx74);
            r14_29 = v76;
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
            __asm__("fld tword [rsp]");
            __asm__("fld tword [rsp+0x10]");
        }
        __asm__("fstp tword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rcx42) = 0x80;
        *reinterpret_cast<void***>(r12_3) = reinterpret_cast<void**>(0x664c2a2e);
        *reinterpret_cast<void***>(r12_3 + 4) = reinterpret_cast<void**>(0);
        r12_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0xd0);
        rsp77 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16);
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        r8_36 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp77) + 0xa0);
        __asm__("fstp tword [rsp+0x10]");
        eax78 = fun_23f0(r12_3, 0x80, 1, r12_3, 0x80, 1);
        rbp4 = v76;
        r13_22 = v45;
        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8 + 8 + 8);
        __asm__("fld tword [rsp]");
        if (eax78 <= 0x7f) 
            goto addr_46c9_103;
        __asm__("fstp tword [rsp]");
        fun_24e0();
        __asm__("fld tword [rsp]");
        *reinterpret_cast<int32_t*>(&rsi44) = 0;
        *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdi43) = 1;
        *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
        v45 = rbx48;
        v76 = rbx48;
        __asm__("fstp tword [rsp]");
        fun_26d0();
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 - 8 - 8 + 8);
        addr_4f87_105:
        __asm__("fstp st0");
        v49 = reinterpret_cast<void**>(0);
        __asm__("fld1 ");
        addr_3b0d_106:
        if (*reinterpret_cast<unsigned char*>(&rcx42)) {
            __asm__("fxch st0, st1");
            __asm__("fstp tword [rsp+0x30]");
            *reinterpret_cast<uint32_t*>(&rcx42) = *reinterpret_cast<uint32_t*>(&r14_29);
            rdi43 = stderr;
            *reinterpret_cast<int32_t*>(&rsi44) = 1;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            __asm__("fld st0");
            __asm__("fstp tword [rsp]");
            __asm__("fstp tword [rsp+0x30]");
            fun_2740(rdi43, 1, "  suffix power=%d^%d = %Lf\n", rdi43, 1, "  suffix power=%d^%d = %Lf\n");
            __asm__("fld tword [rsp+0x30]");
            zf80 = dev_debug == 0;
            __asm__("fld tword [rsp+0x40]");
            __asm__("fmulp st1, st0");
            __asm__("fld st0");
            __asm__("fstp tword [rsp+0x80]");
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
            if (!zf80) {
                rdi43 = stderr;
                __asm__("fld st0");
                __asm__("fstp tword [rsp+0x10]");
                *reinterpret_cast<int32_t*>(&rsi44) = 1;
                *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
                __asm__("fld st0");
                __asm__("fstp tword [rsp]");
                __asm__("fstp tword [rsp+0x40]");
                fun_2740(rdi43, 1, "  returning value: %Lf (%LG)\n", rdi43, 1, "  returning value: %Lf (%LG)\n");
                __asm__("fld tword [rsp+0x40]");
                rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 32 - 8 + 8 + 32);
            }
        } else {
            __asm__("fmulp st1, st0");
            __asm__("fld st0");
            __asm__("fstp tword [rsp+0x70]");
        }
        if (!*reinterpret_cast<void***>(rbp4)) {
            if (*reinterpret_cast<uint32_t*>(&rbx48) != 1) 
                continue;
            zf81 = debug == 0;
            if (zf81) 
                continue;
            __asm__("fstp tword [rsp+0x20]");
            rax82 = quote(r13_22, r13_22);
            fun_24e0();
            rcx42 = rax82;
            *reinterpret_cast<int32_t*>(&rsi44) = 0;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi43) = 0;
            *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
            v45 = reinterpret_cast<void**>(0x3f29);
            fun_26d0();
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 8 + 8 - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
        } else {
            zf83 = reinterpret_cast<int1_t>(inval_style == 3);
            *reinterpret_cast<uint32_t*>(&rbx48) = 5;
            if (zf83) 
                continue;
            __asm__("fstp tword [rsp+0x20]");
            *reinterpret_cast<uint32_t*>(&rbx48) = 5;
            rax84 = quote_n();
            rbp4 = rax84;
            rax85 = quote_n();
            fun_24e0();
            *reinterpret_cast<uint32_t*>(&rdi43) = conv_exit_code;
            *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
            r8_36 = rbp4;
            *reinterpret_cast<int32_t*>(&rsi44) = 0;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            rcx42 = rax85;
            v45 = reinterpret_cast<void**>(0x3b86);
            fun_26d0();
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
        }
    }
    __asm__("fstp st0");
    addr_3be8_117:
    rsi10 = stdout;
    rdi86 = r12_3;
    *reinterpret_cast<int32_t*>(&r12_3) = 0;
    *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
    fun_25c0(rdi86, rsi10, rdx59, rdi86, rsi10, rdx59);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
    goto addr_394d_10;
    addr_447e_65:
    zf87 = reinterpret_cast<int1_t>(inval_style == 3);
    if (zf87) {
        __asm__("fstp st0");
        goto addr_3be8_117;
    } else {
        rbx48 = v62;
        __asm__("fstp tword [rsp]");
        *reinterpret_cast<uint32_t*>(&rbp4) = conv_exit_code;
        if (!rbx48) {
            rax88 = fun_24e0();
            __asm__("fld tword [rsp]");
            rdx59 = rax88;
            __asm__("fstp tword [rsp]");
            fun_26d0();
            rcx42 = v76;
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
            goto addr_3be8_117;
        } else {
            rax89 = fun_24e0();
            __asm__("fld tword [rsp]");
            rdx59 = rax89;
            rcx42 = rbx48;
            __asm__("fstp tword [rsp]");
            fun_26d0();
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
            goto addr_3be8_117;
        }
    }
    addr_4110_79:
    eax90 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rbp4 + 0xfffffffffffffffd));
    cf91 = eax90 < 1;
    if (eax90 <= 1) {
        *reinterpret_cast<void***>(rdi43) = *reinterpret_cast<void***>(rsi44);
        rdi92 = reinterpret_cast<void***>(rdi43 + 4);
    } else {
        *reinterpret_cast<void***>(rdi43) = *reinterpret_cast<void***>(rsi44);
        rdi92 = reinterpret_cast<void***>(rdi43 + 4);
    }
    __asm__("fld tword [rip+0x8cae]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    __asm__("fstp st1");
    if (cf91 || cf91) {
        __asm__("fld st0");
        *reinterpret_cast<uint32_t*>(&rbx48) = 0;
        *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
    } else {
        __asm__("fild dword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rbx48) = 0;
        *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
        cf93 = 0;
        __asm__("fld st1");
        while (!cf93) {
            __asm__("fdiv st0, st1");
            tmp32_94 = *reinterpret_cast<uint32_t*>(&rbx48) + 1;
            cf93 = tmp32_94 < *reinterpret_cast<uint32_t*>(&rbx48);
            *reinterpret_cast<uint32_t*>(&rbx48) = tmp32_94;
            *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
        }
        __asm__("fstp st1");
    }
    if (*reinterpret_cast<unsigned char*>(&rcx42)) {
        __asm__("fxch st0, st1");
        __asm__("fstp tword [rsp+0x30]");
        rdi95 = stderr;
        *reinterpret_cast<void***>(rdi95) = g1;
        rdi96 = rdi95 + 4;
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fstp tword [rsp+0x30]");
        fun_2740(rdi96, 5, "  scaled value to %Lf * %0.f ^ %u\n", rdi96, 5, "  scaled value to %Lf * %0.f ^ %u\n");
        *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
        *reinterpret_cast<void***>(rdi96) = g5;
        rdi92 = reinterpret_cast<void***>(rdi96 + 4);
        __asm__("fld tword [rsp+0x30]");
        __asm__("fld tword [rsp+0x20]");
    }
    rsi97 = user_precision;
    if (rsi97 == 0xffffffffffffffff) {
        __asm__("fldz ");
        __asm__("fld st1");
        __asm__("fchs ");
        __asm__("fxch st0, st1");
        __asm__("fcomip st0, st2");
        __asm__("fcmovbe st0, st1");
        __asm__("fld dword [rip+0x81dc]");
        __asm__("fld st0");
        __asm__("fcomip st0, st2");
        __asm__("fstp st1");
        if (reinterpret_cast<unsigned char>(rsi97) <= reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
            __asm__("fstp st0");
        } else {
            addr_4be4_146:
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fmul st1, st0");
            __asm__("fld tword [rip+0x8210]");
            __asm__("fld st2");
            __asm__("fdiv st0, st1");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x10]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fild qword [rsp+0x10]");
            __asm__("fmulp st1, st0");
            __asm__("fsub st2, st0");
            cf98 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) < 4);
            pf99 = __intrinsic();
            zf100 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) == 4);
            below_or_equal101 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) <= 4);
            if (!below_or_equal101) {
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fldz ");
                goto addr_454d_148;
            } else {
                *reinterpret_cast<uint32_t*>(&rax102) = *reinterpret_cast<uint32_t*>(&r13_67);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax102) + 4) = 0;
                goto *reinterpret_cast<int32_t*>(0xcc1c + rax102 * 4) + 0xcc1c;
            }
        }
    } else {
        *reinterpret_cast<int32_t*>(&rax103) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx48 + reinterpret_cast<unsigned char>(rbx48) * 2));
        *reinterpret_cast<int32_t*>(&rax103 + 4) = 0;
        if (reinterpret_cast<signed char>(rax103) > reinterpret_cast<signed char>(rsi97)) {
            rax103 = rsi97;
        }
        *reinterpret_cast<int32_t*>(&rdi92) = *reinterpret_cast<int32_t*>(&rax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi92) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax103)) 
            goto addr_419a_153;
    }
    __asm__("fnstcw word [rsp+0x4e]");
    __asm__("fld tword [rip+0x82a0]");
    __asm__("fld st1");
    __asm__("fdiv st0, st1");
    __asm__("fldcw word [rsp+0x4c]");
    __asm__("fistp qword [rsp+0x10]");
    __asm__("fldcw word [rsp+0x4e]");
    __asm__("fild qword [rsp+0x10]");
    __asm__("fmulp st1, st0");
    __asm__("fsub st1, st0");
    cf98 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) < 4);
    pf99 = __intrinsic();
    zf100 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) == 4);
    below_or_equal101 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) <= 4);
    if (!below_or_equal101) {
        __asm__("fstp st0");
        __asm__("fstp st0");
        __asm__("fldz ");
        goto addr_454d_148;
    } else {
        *reinterpret_cast<uint32_t*>(&rax104) = *reinterpret_cast<uint32_t*>(&r13_67);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xcc08 + rax104 * 4) + 0xcc08;
    }
    addr_419a_153:
    *reinterpret_cast<uint32_t*>(&rax105) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax103) - 1);
    if (!*reinterpret_cast<uint32_t*>(&rax105)) {
        __asm__("fld dword [rip+0x7e07]");
        goto addr_4be4_146;
    }
    __asm__("fld dword [rip+0x8c0b]");
    edx106 = *reinterpret_cast<uint32_t*>(&rax105);
    __asm__("fld st0");
    do {
        __asm__("fmul st0, st1");
        --edx106;
    } while (edx106);
    __asm__("fnstcw word [rsp+0x4e]");
    __asm__("fmulp st2, st0");
    __asm__("fld tword [rip+0x8c3d]");
    __asm__("fld st2");
    __asm__("fdiv st0, st1");
    __asm__("fldcw word [rsp+0x4c]");
    __asm__("fistp qword [rsp+0x10]");
    __asm__("fldcw word [rsp+0x4e]");
    __asm__("fild qword [rsp+0x10]");
    __asm__("fmulp st1, st0");
    __asm__("fsub st2, st0");
    if (*reinterpret_cast<uint32_t*>(&r13_67) <= 4) 
        goto addr_41f2_161;
    __asm__("fstp st0");
    __asm__("fstp st1");
    __asm__("fldz ");
    __asm__("fxch st0, st1");
    __asm__("fld st0");
    do {
        __asm__("fmul st0, st1");
        cf98 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax105) < 1);
        below_or_equal101 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax105) <= 1);
        *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<uint32_t*>(&rax105) - 1;
        pf99 = __intrinsic();
        zf100 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax105) == 0);
    } while (!zf100);
    __asm__("fstp st1");
    __asm__("fdivp st1, st0");
    goto addr_4543_166;
    addr_41f2_161:
    goto *reinterpret_cast<int32_t*>(0xcbbc + r13_67 * 4) + 0xcbbc;
    addr_4d39_82:
    goto *reinterpret_cast<int32_t*>(0xcc30 + r13_67 * 4) + 0xcc30;
    addr_46c9_103:
    __asm__("fstp st0");
    __asm__("fld tword [rsp+0x10]");
    ebx107 = dev_debug;
    goto addr_46d6_167;
    addr_4f2d_86:
    goto *reinterpret_cast<int32_t*>(0xcc44 + r13_67 * 4) + 0xcc44;
    addr_43d2_91:
    __asm__("fstp st2");
    __asm__("fxch st0, st1");
    goto *reinterpret_cast<int32_t*>(0xcbd0 + r13_67 * 4) + 0xcbd0;
    addr_4057_72:
    zf108 = reinterpret_cast<int1_t>(inval_style == 3);
    __asm__("fstp tword [rsp]");
    if (!zf108) {
        fun_24e0();
        __asm__("fld tword [rsp]");
        __asm__("fstp tword [rsp]");
        fun_26d0();
        rdx59 = reinterpret_cast<void**>(0x407a);
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
        goto addr_3be8_117;
    }
    addr_4328_44:
    __asm__("fstp st0");
    __asm__("fld tword [rsp+0x70]");
    v49 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rbx48) = 3;
    rbp4 = reinterpret_cast<void**>("invalid number: %s");
    goto addr_3f70_49;
    addr_3e47_45:
    rbp4 = v109;
    __asm__("fld tword [rsp+0x80]");
    rax110 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp4) - reinterpret_cast<unsigned char>(rdi43));
    v49 = rax110;
    if (*reinterpret_cast<uint32_t*>(&rbp4) != *reinterpret_cast<uint32_t*>(&rdi43)) {
        eax111 = *reinterpret_cast<int32_t*>(&rax110) - 1;
        if (!eax111) {
            __asm__("fdiv dword [rip+0x7f0c]");
        } else {
            __asm__("fld dword [rip+0x8f43]");
            __asm__("fld st0");
            do {
                __asm__("fmul st0, st1");
                --eax111;
            } while (eax111);
            __asm__("fstp st1");
            __asm__("fdivp st1, st0");
        }
    }
    if (!v112) {
        __asm__("faddp st1, st0");
    } else {
        __asm__("fsubp st1, st0");
    }
    __asm__("fld st0");
    __asm__("fstp tword [rsp+0x70]");
    goto addr_3ada_46;
    addr_3ae7_48:
    *reinterpret_cast<uint32_t*>(&rcx42) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4));
    if (*reinterpret_cast<unsigned char*>(&rcx42)) {
        __asm__("fstp tword [rsp+0x30]");
        rax113 = fun_2790(rdi43, rdi43);
        rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
        *reinterpret_cast<uint32_t*>(&rcx42) = *reinterpret_cast<unsigned char*>(&rcx42);
        __asm__("fld tword [rsp+0x30]");
        edi115 = 0;
        rsi116 = *reinterpret_cast<void***>(rax113);
        while (*reinterpret_cast<uint32_t*>(&rax117) = *reinterpret_cast<unsigned char*>(&rcx42), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax117) + 4) = 0, rdx118 = rbp4, ++rbp4, !!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rsi116) + reinterpret_cast<uint64_t>(rax117 * 2))) & 1)) {
            *reinterpret_cast<uint32_t*>(&rcx42) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx118 + 1));
            edi115 = 1;
        }
        if (*reinterpret_cast<signed char*>(&edi115)) 
            goto addr_3cfe_183;
    } else {
        if (*reinterpret_cast<uint32_t*>(&r15_27) == 4) {
            *reinterpret_cast<uint32_t*>(&rbx48) = 6;
            rbp4 = reinterpret_cast<void**>("missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)");
            goto addr_3f70_49;
        } else {
            *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
            *reinterpret_cast<int32_t*>(&r8_36) = 0;
            *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
            __asm__("fld1 ");
            goto addr_3b0d_106;
        }
    }
    addr_3d03_187:
    *reinterpret_cast<int32_t*>(&rsi44) = *reinterpret_cast<signed char*>(&rcx42);
    *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
    rdi43 = reinterpret_cast<void**>("KMGTPEZY");
    __asm__("fstp tword [rsp+0x20]");
    v45 = reinterpret_cast<void**>(0x3d1f);
    rax119 = fun_2540("KMGTPEZY", "KMGTPEZY");
    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
    __asm__("fld tword [rsp+0x20]");
    if (!rax119) {
        *reinterpret_cast<uint32_t*>(&rbx48) = 5;
        rbp4 = reinterpret_cast<void**>("invalid suffix in input: %s");
        goto addr_3f70_49;
    } else {
        if (!*reinterpret_cast<uint32_t*>(&r15_27)) {
            *reinterpret_cast<uint32_t*>(&rbx48) = 4;
            rbp4 = reinterpret_cast<void**>("rejecting suffix in input: %s (consider using --from)");
            goto addr_3f70_49;
        } else {
            *reinterpret_cast<int32_t*>(&r8_36) = 0;
            *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx42) = *reinterpret_cast<unsigned char*>(&rcx42) - 69;
            if (*reinterpret_cast<unsigned char*>(&rcx42) <= 21) {
                *reinterpret_cast<uint32_t*>(&rcx42) = *reinterpret_cast<unsigned char*>(&rcx42);
                *reinterpret_cast<int32_t*>(&rcx42 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r8_36) = *reinterpret_cast<int32_t*>(0xcd00 + reinterpret_cast<unsigned char>(rcx42) * 4);
                *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
            }
            rbp4 = rdx118 + 1;
            if (*reinterpret_cast<uint32_t*>(&r15_27) != 1) 
                goto addr_3d6b_194;
        }
    }
    *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
    if (*reinterpret_cast<void***>(rdx118 + 1) == 0x69) {
        rbp4 = rdx118 + 2;
        if (*reinterpret_cast<unsigned char*>(&rcx42)) {
            *reinterpret_cast<int32_t*>(&rsi44) = 1;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            __asm__("fstp tword [rsp+0x20]");
            rdi43 = stderr;
            *reinterpret_cast<uint32_t*>(&r14_29) = 0x400;
            v45 = reinterpret_cast<void**>(0x4e50);
            fun_2740(rdi43, 1, "  Auto-scaling, found 'i', switching to base %d\n", rdi43, 1, "  Auto-scaling, found 'i', switching to base %d\n");
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            *reinterpret_cast<int32_t*>(&r8_36) = *reinterpret_cast<int32_t*>(&r8_36);
            *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
            __asm__("fld tword [rsp+0x20]");
            __asm__("fld dword [rip+0x7f59]");
            *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
        } else {
            __asm__("fld dword [rip+0x8118]");
            *reinterpret_cast<uint32_t*>(&r14_29) = 0x400;
        }
    } else {
        __asm__("fild dword [rsp+0x10]");
    }
    addr_3d85_200:
    if (!*reinterpret_cast<int32_t*>(&r8_36)) 
        goto addr_4f87_105;
    __asm__("fld st0");
    eax120 = *reinterpret_cast<int32_t*>(&r8_36) - 1;
    if (!eax120) {
        __asm__("fstp st1");
        v49 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r8_36) = 1;
        *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
        goto addr_3b0d_106;
    } else {
        do {
            __asm__("fmul st0, st1");
            --eax120;
        } while (eax120);
        __asm__("fstp st1");
        v49 = reinterpret_cast<void**>(0);
        goto addr_3b0d_106;
    }
    addr_3d6b_194:
    if (*reinterpret_cast<uint32_t*>(&r15_27) == 4) {
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx118 + 1) == 0x69)) {
            v49 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&rbx48) = 6;
            rbp4 = reinterpret_cast<void**>("missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)");
            goto addr_3f70_49;
        } else {
            rbp4 = rdx118 + 2;
            goto addr_3d75_209;
        }
    } else {
        addr_3d75_209:
        __asm__("fild dword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
        goto addr_3d85_200;
    }
    addr_3cfe_183:
    goto addr_3d03_187;
    addr_3bff_31:
    rdi121 = stderr;
    fun_2740(rdi121, 1, "setting Auto-Padding to %ld characters\n", rdi121, 1, "setting Auto-Padding to %ld characters\n");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    goto addr_3c20_24;
    addr_3a49_32:
    *reinterpret_cast<uint32_t*>(&r15_27) = scale_from;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_27) + 4) = 0;
    __asm__("pxor xmm2, xmm2");
    __asm__("movss [rsp], xmm2");
    __asm__("fld dword [rsp]");
    __asm__("fstp tword [rsp+0x70]");
    *reinterpret_cast<uint32_t*>(&r14_29) = (r14d122 - (r14d123 + reinterpret_cast<uint1_t>(r14d124 < r14d125 + reinterpret_cast<uint1_t>(static_cast<uint32_t>(r15_27 - 3) < 2))) & 24) + 0x3e8;
    goto addr_3a7e_33;
    addr_45b8_210:
    *reinterpret_cast<uint32_t*>(&rax126) = *reinterpret_cast<uint32_t*>(&rbx48);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax126) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xcbe4 + rax126 * 4) + 0xcbe4;
    addr_3964_211:
    eax127 = *reinterpret_cast<int32_t*>(&r12_3);
    return *reinterpret_cast<signed char*>(&eax127);
    while (1) {
        if (!*reinterpret_cast<signed char*>(&ebx107)) {
            __asm__("fstp st0");
        } else {
            __asm__("fstp tword [rsp]");
            quote(r12_3, r12_3);
            __asm__("fld tword [rsp]");
            rdi128 = stderr;
            __asm__("fstp tword [rsp]");
            fun_2740(rdi128, 1, "formatting output:\n  value: %Lf\n  humanized: %s\n", rdi128, 1, "formatting output:\n  value: %Lf\n  humanized: %s\n");
            rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
        }
        while (1) {
            rbx48 = padding_width;
            rax129 = fun_2500(r12_3, r12_3);
            rsp130 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
            rdx131 = padding_buffer_size;
            rdi132 = padding_buffer;
            if (!rbx48 || reinterpret_cast<unsigned char>(rbx48) <= reinterpret_cast<unsigned char>(rax129)) {
                rcx42 = rax129 + 1;
                if (reinterpret_cast<unsigned char>(rcx42) >= reinterpret_cast<unsigned char>(rdx131)) {
                    padding_buffer_size = rax129 + 2;
                    rax133 = xrealloc(rdi132);
                    rsp130 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp130) - 8 + 8);
                    padding_buffer = rax133;
                    rdi132 = rax133;
                }
                fun_2450(rdi132, r12_3);
                rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp130) - 8 + 8);
            } else {
                *reinterpret_cast<int32_t*>(&r8_135) = padding_alignment;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_135) + 4) = 0;
                rcx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp130) + 0x80);
                mbsalign(r12_3, rdi132, rdx131, rcx42, r8_135, 2);
                rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp130) - 8 + 8);
                zf136 = dev_debug == 0;
                if (!zf136) {
                    rdi137 = padding_buffer;
                    rax138 = quote(rdi137, rdi137);
                    rdi139 = stderr;
                    rdx131 = reinterpret_cast<void**>("  After padding: %s\n");
                    rcx42 = rax138;
                    fun_2740(rdi139, 1, "  After padding: %s\n", rdi139, 1, "  After padding: %s\n");
                    rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp134) - 8 + 8 - 8 + 8);
                }
            }
            rdi140 = format_str_prefix;
            rsi10 = stdout;
            if (rdi140) {
                fun_25c0(rdi140, rsi10, rdx131, rdi140, rsi10, rdx131);
                rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp134) - 8 + 8);
                rsi10 = stdout;
            }
            rdi141 = padding_buffer;
            *reinterpret_cast<int32_t*>(&r12_3) = 1;
            *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
            fun_25c0(rdi141, rsi10, rdx131, rdi141, rsi10, rdx131);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp134) - 8 + 8);
            rdi142 = format_str_suffix;
            if (rdi142) {
                rsi10 = stdout;
                fun_25c0(rdi142, rsi10, rdx131, rdi142, rsi10, rdx131);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            }
            addr_394d_10:
            rax143 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
            if (!rax143) 
                goto addr_3964_211;
            fun_2510();
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            while (1) {
                addr_4f9e_226:
                __asm__("fld st0");
                __asm__("fchs ");
                while (1) {
                    __asm__("fxch st0, st1");
                    while (1) {
                        __asm__("fld dword [rip+0x8278]");
                        __asm__("fcomip st0, st2");
                        __asm__("fstp st1");
                        do {
                            if (*reinterpret_cast<unsigned char*>(&rcx42)) {
                                __asm__("fxch st0, st1");
                                __asm__("fstp tword [rsp+0x20]");
                                rdi144 = stderr;
                                __asm__("fld st0");
                                __asm__("fstp tword [rsp]");
                                __asm__("fstp tword [rsp+0x10]");
                                fun_2740(rdi144, 1, "  after rounding, value=%Lf * %0.f ^ %u\n", rdi144, 1, "  after rounding, value=%Lf * %0.f ^ %u\n");
                                rsi10 = user_precision;
                                rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
                                __asm__("fld tword [rsp+0x20]");
                                __asm__("fld tword [rsp]");
                            }
                            *reinterpret_cast<void***>(r12_3) = reinterpret_cast<void**>(0x664c2a2e);
                            *reinterpret_cast<void***>(r12_3 + 4) = reinterpret_cast<void**>(0x7325);
                            if (!reinterpret_cast<int1_t>(rsi10 == 0xffffffffffffffff)) {
                            }
                            *reinterpret_cast<signed char*>(r12_3 + 6) = 0;
                            if (*reinterpret_cast<uint32_t*>(&rbx48) <= 8) 
                                goto addr_45b8_210;
                            __asm__("fxch st0, st1");
                            __asm__("fstp tword [rsp+0x10]");
                            *reinterpret_cast<uint32_t*>(&rcx42) = 0x80;
                            r12_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0xd0);
                            __asm__("fld st0");
                            __asm__("fstp tword [rsp]");
                            __asm__("fstp tword [rsp+0x20]");
                            eax145 = fun_23f0(r12_3, 0x7f, 1, r12_3, 0x7f, 1);
                            __asm__("fld tword [rsp+0x20]");
                            rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 - 8 - 16 - 8 + 8 + 32);
                            __asm__("fld tword [rsp+0x10]");
                            if (eax145 <= 0x7e) 
                                goto addr_4937_236;
                            __asm__("fstp st0");
                            __asm__("fstp tword [rsp]");
                            fun_24e0();
                            __asm__("fld tword [rsp]");
                            *reinterpret_cast<int32_t*>(&rsi97) = 0;
                            *reinterpret_cast<int32_t*>(&rsi97 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rdi92) = 1;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi92) + 4) = 0;
                            cf98 = 0;
                            pf99 = __intrinsic();
                            zf100 = 1;
                            below_or_equal101 = 1;
                            __asm__("fstp tword [rsp]");
                            fun_26d0();
                            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 - 8 - 8 + 8);
                            __asm__("fxch st0, st2");
                            __asm__("fldcw word [rsp+0x4c]");
                            __asm__("fistp qword [rsp+0x10]");
                            __asm__("fldcw word [rsp+0x4e]");
                            __asm__("fild qword [rsp+0x10]");
                            __asm__("faddp st2, st0");
                            __asm__("fdivp st1, st0");
                            addr_4543_166:
                            __asm__("fldz ");
                            __asm__("fcomip st0, st1");
                            if (!below_or_equal101) {
                                *rdi92 = *reinterpret_cast<void***>(rsi97);
                                rsi10 = rsi97 + 4;
                                __asm__("fld qword [rsp+0x10]");
                                __asm__("fld st1");
                                __asm__("fchs ");
                                __asm__("fcomi st0, st1");
                                if (cf98) 
                                    goto addr_4b32_244;
                                __asm__("fstp st0");
                                __asm__("fxch st0, st1");
                            } else {
                                addr_454d_148:
                                *rdi92 = *reinterpret_cast<void***>(rsi97);
                                rsi10 = rsi97 + 4;
                                __asm__("fld qword [rsp+0x10]");
                                __asm__("fxch st0, st1");
                                __asm__("fcomi st0, st1");
                                if (cf98) 
                                    goto addr_456a_249; else 
                                    goto addr_455d_250;
                            }
                            addr_4563_251:
                            tmp32_146 = *reinterpret_cast<uint32_t*>(&rbx48) + 1;
                            cf147 = reinterpret_cast<uint1_t>(tmp32_146 < *reinterpret_cast<uint32_t*>(&rbx48));
                            *reinterpret_cast<uint32_t*>(&rbx48) = tmp32_146;
                            pf99 = __intrinsic();
                            zf100 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rbx48) == 0);
                            below_or_equal101 = reinterpret_cast<uint1_t>(cf147 | zf100);
                            __asm__("fdivrp st1, st0");
                            continue;
                            addr_456a_249:
                            __asm__("fstp st1");
                            continue;
                            addr_455d_250:
                            goto addr_4563_251;
                            __asm__("fldz ");
                            __asm__("fxch st0, st1");
                            __asm__("fucomi st0, st1");
                            __asm__("fstp st1");
                        } while (!pf99 && zf100);
                        __asm__("fldz ");
                        __asm__("fcomip st0, st1");
                        if (!below_or_equal101) 
                            goto addr_4f9e_226;
                        __asm__("fld st0");
                    }
                    addr_4b32_244:
                    __asm__("fstp st1");
                }
            }
            addr_4937_236:
            __asm__("fstp st1");
            if (*reinterpret_cast<uint32_t*>(&rbx48)) {
                if (*reinterpret_cast<uint32_t*>(&rbp4) == 4) {
                    __asm__("fstp tword [rsp]");
                    fun_2750(r12_3, "i");
                    rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
                    __asm__("fld tword [rsp]");
                }
            }
            ebx107 = dev_debug;
            if (*reinterpret_cast<signed char*>(&ebx107)) 
                break;
            rbp4 = suffix;
            if (rbp4) 
                goto addr_46e2_260;
            __asm__("fstp st0");
        }
        __asm__("fstp tword [rsp]");
        quote(r12_3, r12_3);
        rdi148 = stderr;
        fun_2740(rdi148, 1, "  returning value: %s\n", rdi148, 1, "  returning value: %s\n");
        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8);
        ebx107 = dev_debug;
        __asm__("fld tword [rsp]");
        addr_46d6_167:
        rbp4 = suffix;
        if (!rbp4) 
            continue;
        addr_46e2_260:
        __asm__("fstp tword [rsp]");
        fun_2500(r12_3, r12_3);
        fun_2750(r12_3, rbp4);
        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8);
        __asm__("fld tword [rsp]");
    }
}

uint64_t n_frp = 0;

uint64_t n_frp_allocated = 0;

void** x2nrealloc();

struct s0 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void add_range_pair(void** rdi, void** rsi, ...) {
    uint64_t rdx3;
    int1_t zf4;
    void** rdi5;
    void** rax6;
    struct s0* rdi7;

    rdx3 = n_frp;
    zf4 = rdx3 == n_frp_allocated;
    rdi5 = frp;
    if (zf4) {
        rax6 = x2nrealloc();
        rdx3 = n_frp;
        frp = rax6;
        rdi5 = rax6;
    }
    rdi7 = reinterpret_cast<struct s0*>(reinterpret_cast<unsigned char>(rdi5) + (rdx3 << 4));
    rdi7->f0 = rdi;
    rdi7->f8 = rsi;
    n_frp = rdx3 + 1;
    return;
}

uint64_t fun_24f0();

int64_t fun_2410(void** rdi, ...);

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_24f0();
    if (r8d > 10) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xd0e0 + rax11 * 4) + 0xd0e0;
    }
}

struct s1 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_2420();

void** slotvec = reinterpret_cast<void**>(0xb0);

uint32_t nslots = 1;

void** xpalloc();

void fun_2590();

struct s2 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void fun_2400(void** rdi, ...);

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s1* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    void** rax8;
    void** r15_9;
    void** v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s2* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x7f6f;
    rax8 = fun_2420();
    r15_9 = slotvec;
    v10 = *reinterpret_cast<void***>(rax8);
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
        fun_2410(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0x110b0) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x8fa1]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x7ffb;
            fun_2590();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s2*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0x111e0) {
                fun_2400(r14_19);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x808a);
        }
        *reinterpret_cast<void***>(rax8) = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2510();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0x110c0;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s3 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s3* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, int64_t rdx) {
    struct s3* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0xd06b);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0xd064);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0xd06f);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0xd060);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

struct s4 {
    signed char f0;
    int16_t f1;
};

signed char* xmalloc(signed char* rdi);

void** fun_2610(signed char* rdi, void** rsi, void** rdx);

int32_t xstrtoumax(void** rdi);

void** unit_to_umax(void** rdi) {
    void** rax2;
    void** rax3;
    void* rsp4;
    struct s4* v5;
    void** rbx6;
    void** rdi7;
    void** r8_8;
    void** r13_9;
    signed char* rax10;
    struct s4* r12_11;
    void** rax12;
    void** rcx13;
    void** rsi14;
    void** rdx15;
    int32_t eax16;
    void* rsp17;
    void** v18;
    void** rax19;
    void** rax20;
    void** rdi21;
    void* rsp22;
    void* rax23;
    void** r12_24;
    void** rbp25;
    void* rsp26;
    void** rax27;
    void** v28;
    void** rax29;
    void** rsi30;
    void** rdi31;
    void* rsp32;
    void** r13_33;
    void** rax34;
    void** rax35;
    signed char* rbx36;
    int32_t eax37;
    uint32_t edx38;
    void** rcx39;
    int64_t rbx40;
    void** r13_41;
    int32_t eax42;
    void** rax43;
    void** rax44;
    int32_t edx45;
    int64_t r15_46;
    uint32_t eax47;
    void** r14_48;
    int32_t r14d49;
    int32_t r14d50;
    uint32_t r14d51;
    int32_t r14d52;
    void** rax53;
    void** rdi54;
    void** rax55;
    int1_t cf56;
    void** rdi57;
    void** rax58;
    int1_t zf59;
    void** rdi60;
    void** rsi61;
    void** v62;
    uint32_t eax63;
    void* rsp64;
    void** v65;
    int64_t rax66;
    void** rdx67;
    int32_t eax68;
    uint32_t eax69;
    signed char v70;
    int1_t zf71;
    int1_t zf72;
    void** rax73;
    void** rax74;
    void** rdx75;
    void** rax76;
    int1_t cf77;
    void** v78;
    uint32_t eax79;
    int1_t cf80;
    uint32_t tmp32_81;
    void* rdx82;
    int64_t r13_83;
    void** r9_84;
    uint32_t eax85;
    void** rcx86;
    void** rax87;
    int32_t eax88;
    int32_t edx89;
    void** rdx90;
    void** rdi91;
    void** v92;
    void* rsp93;
    uint32_t eax94;
    void* rsp95;
    int1_t zf96;
    int1_t zf97;
    void** rax98;
    int1_t zf99;
    void** rax100;
    void** rax101;
    void** rdi102;
    int1_t zf103;
    void** rax104;
    void** rax105;
    uint32_t eax106;
    int1_t cf107;
    void*** rdi108;
    int1_t cf109;
    uint32_t tmp32_110;
    void** rdi111;
    void** rdi112;
    void** rsi113;
    uint1_t cf114;
    int1_t pf115;
    uint1_t zf116;
    uint1_t below_or_equal117;
    int64_t rax118;
    void** rax119;
    int64_t rax120;
    uint64_t rax121;
    uint32_t edx122;
    uint32_t ebx123;
    int1_t zf124;
    void** v125;
    void** rax126;
    int32_t eax127;
    signed char v128;
    void** rax129;
    void* rsp130;
    int32_t edi131;
    void** rsi132;
    int64_t rax133;
    void** rdx134;
    int64_t rax135;
    int32_t eax136;
    void** rdi137;
    int32_t r14d138;
    int32_t r14d139;
    uint32_t r14d140;
    int32_t r14d141;
    int64_t rax142;
    int64_t v143;
    void** rdi144;
    void** rax145;
    void* rsp146;
    void** rdx147;
    void** rdi148;
    void** rax149;
    void* rsp150;
    uint64_t r8_151;
    int1_t zf152;
    void** rdi153;
    void** rax154;
    void** rdi155;
    void** rdi156;
    void** rdi157;
    void** rdi158;
    void* rax159;
    void** rdi160;
    uint32_t eax161;
    uint32_t tmp32_162;
    uint1_t cf163;
    void** rdi164;

    rax2 = g28;
    rax3 = fun_2500(rdi);
    rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 40 - 8 + 8);
    v5 = reinterpret_cast<struct s4*>(0);
    if (!rax3 || (rbx6 = rax3, *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rdi) + reinterpret_cast<unsigned char>(rax3) + 0xffffffffffffffff) - 48 <= 9)) {
        rdi7 = rdi;
        r8_8 = reinterpret_cast<void**>("KMGTPEZY");
        *reinterpret_cast<int32_t*>(&r13_9) = 0;
        *reinterpret_cast<int32_t*>(&r13_9 + 4) = 0;
    } else {
        rax10 = xmalloc(rbx6 + 2);
        r12_11 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(rax3 + 0xffffffffffffffff) + reinterpret_cast<int64_t>(rax10));
        v5 = r12_11;
        rax12 = fun_2610(rax10, rdi, rbx6);
        rsp4 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8 - 8 + 8);
        rdi7 = rax12;
        if (r12_11->f0 != 0x69 || (rbx6 == 1 || *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(r12_11) - 1) - 48 <= 9)) {
            r13_9 = rdi7;
            r8_8 = reinterpret_cast<void**>("KMGTPEZY0");
            v5 = reinterpret_cast<struct s4*>(reinterpret_cast<uint64_t>(r12_11) + 2);
            r12_11->f1 = 66;
        } else {
            r12_11->f0 = 0;
            r13_9 = rdi7;
            r8_8 = reinterpret_cast<void**>("KMGTPEZY");
        }
    }
    rcx13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp4) + 16);
    rsi14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp4) + 8);
    *reinterpret_cast<int32_t*>(&rdx15) = 10;
    *reinterpret_cast<int32_t*>(&rdx15 + 4) = 0;
    eax16 = xstrtoumax(rdi7);
    rsp17 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp4) - 8 + 8);
    if (eax16 || (v5->f0 || !v18)) {
        fun_2400(r13_9);
        rax19 = quote(rdi, rdi);
        rax20 = fun_24e0();
        rcx13 = rax19;
        *reinterpret_cast<int32_t*>(&rsi14) = 0;
        *reinterpret_cast<int32_t*>(&rsi14 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rdi21) = 1;
        *reinterpret_cast<int32_t*>(&rdi21 + 4) = 0;
        rdx15 = rax20;
        fun_26d0();
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    } else {
        rdi21 = r13_9;
        fun_2400(rdi21);
        rsp22 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp17) - 8 + 8);
        rax23 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
        if (!rax23) {
            return v18;
        }
    }
    fun_2510();
    r12_24 = rdi21;
    rbp25 = rsi14;
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp22) - 8 + 8 - 8 - 8 - 8 - 8 - 8 - 8 - 0x168);
    rax27 = g28;
    v28 = rax27;
    rax29 = frp;
    if (rax29) {
        while (rdx15 = *reinterpret_cast<void***>(rax29), !reinterpret_cast<int1_t>(rdx15 == 0xffffffffffffffff)) {
            if (reinterpret_cast<unsigned char>(rbp25) < reinterpret_cast<unsigned char>(rdx15)) 
                goto addr_392b_14;
            if (reinterpret_cast<unsigned char>(rbp25) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax29 + 8))) 
                goto addr_3986_16;
            addr_392b_14:
            rax29 = rax29 + 16;
        }
    } else {
        if (reinterpret_cast<int1_t>(rsi14 == 1)) 
            goto addr_3986_16;
    }
    rsi30 = stdout;
    rdi31 = r12_24;
    *reinterpret_cast<int32_t*>(&r12_24) = 1;
    *reinterpret_cast<int32_t*>(&r12_24 + 4) = 0;
    fun_25c0(rdi31, rsi30, rdx15);
    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
    goto addr_394d_20;
    addr_3986_16:
    r13_33 = suffix;
    if (r13_33 && (rax34 = fun_2500(r12_24, r12_24), rdi21 = r13_33, rax35 = fun_2500(rdi21, rdi21), rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8), reinterpret_cast<unsigned char>(rax34) > reinterpret_cast<unsigned char>(rax35))) {
        rdi21 = r13_33;
        rbx36 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax34) - reinterpret_cast<unsigned char>(rax35)) + reinterpret_cast<unsigned char>(r12_24));
        eax37 = fun_25e0(rdi21, rbx36, rdx15);
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
        edx38 = dev_debug;
        if (eax37) {
            if (*reinterpret_cast<signed char*>(&edx38)) {
                rcx39 = stderr;
                rdi21 = reinterpret_cast<void**>("no valid suffix found\n");
                fun_2730("no valid suffix found\n", 1, 22, rcx39, r8_8);
                rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
            }
        } else {
            *rbx36 = 0;
            if (*reinterpret_cast<signed char*>(&edx38)) {
                quote(r13_33, r13_33);
                rdi21 = stderr;
                fun_2740(rdi21, 1, "trimming suffix %s\n", rdi21, 1, "trimming suffix %s\n");
                rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8);
            }
        }
    }
    *reinterpret_cast<uint32_t*>(&rbx40) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_24));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx40) + 4) = 0;
    if (!*reinterpret_cast<signed char*>(&rbx40)) {
        r13_41 = r12_24;
        eax42 = 0;
    } else {
        rax43 = fun_2790(rdi21, rdi21);
        rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
        r13_41 = r12_24;
        rax44 = *reinterpret_cast<void***>(rax43);
        do {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax44) + reinterpret_cast<uint64_t>(rbx40 * 2))) & 1)) 
                break;
            *reinterpret_cast<uint32_t*>(&rbx40) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_41 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx40) + 4) = 0;
            ++r13_41;
        } while (*reinterpret_cast<signed char*>(&rbx40));
        eax42 = *reinterpret_cast<int32_t*>(&r12_24) - *reinterpret_cast<int32_t*>(&r13_41);
    }
    edx45 = auto_padding;
    if (!edx45) {
        addr_3c20_34:
        *reinterpret_cast<uint32_t*>(&r15_46) = scale_from;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_46) + 4) = 0;
        __asm__("pxor xmm1, xmm1");
        eax47 = dev_debug;
        __asm__("movss [rsp], xmm1");
        __asm__("fld dword [rsp]");
        __asm__("fstp tword [rsp+0x70]");
        *reinterpret_cast<uint32_t*>(&r14_48) = (r14d49 - (r14d50 + reinterpret_cast<uint1_t>(r14d51 < r14d52 + reinterpret_cast<uint1_t>(static_cast<uint32_t>(r15_46 - 3) < 2))) & 24) + 0x3e8;
        if (*reinterpret_cast<signed char*>(&eax47)) {
            rax53 = quote_n();
            quote_n();
            rdi54 = stderr;
            r8_8 = rax53;
            fun_2740(rdi54, 1, "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n", rdi54, 1, "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n");
            rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8 - 8 + 8 - 8 + 8);
        }
    } else {
        if (eax42 || reinterpret_cast<signed char>(rbp25) > reinterpret_cast<signed char>(1)) {
            rax55 = fun_2500(r12_24, r12_24);
            rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
            cf56 = reinterpret_cast<unsigned char>(rax55) < reinterpret_cast<unsigned char>(padding_buffer_size);
            padding_width = rax55;
            if (!cf56) {
                rdi57 = padding_buffer;
                padding_buffer_size = rax55 + 1;
                rax58 = xrealloc(rdi57);
                rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
                padding_buffer = rax58;
            }
        } else {
            padding_width = reinterpret_cast<void**>(0);
        }
        zf59 = dev_debug == 0;
        if (!zf59) 
            goto addr_3bff_41; else 
            goto addr_3a49_42;
    }
    addr_3a7e_43:
    rcx13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp26) + 94);
    rdi60 = r13_41;
    rsi61 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp26) + 96);
    v62 = reinterpret_cast<void**>(0x3a95);
    eax63 = simple_strtod_int(rdi60, rsi61, reinterpret_cast<int64_t>(rsp26) + 0x70, rcx13, r8_8, 18);
    rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
    *reinterpret_cast<uint32_t*>(&rbx6) = eax63;
    if (eax63 > 1) {
        addr_3f38_44:
        __asm__("fld tword [rsp+0x70]");
        __asm__("fld st0");
        if (*reinterpret_cast<uint32_t*>(&rbx6) > 6) {
            __asm__("fstp st1");
            v65 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&rbp25) = 0;
        } else {
            *reinterpret_cast<uint32_t*>(&rax66) = *reinterpret_cast<uint32_t*>(&rbx6);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax66) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xcba0 + rax66 * 4) + 0xcba0;
        }
    } else {
        rbp25 = reinterpret_cast<void**>(0);
        rdx67 = reinterpret_cast<void**>(static_cast<int64_t>(decimal_point_length));
        rsi61 = decimal_point;
        rdi60 = reinterpret_cast<void**>(0);
        v62 = reinterpret_cast<void**>(0x3ac0);
        eax68 = fun_2430(0, rsi61, rdx67);
        rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
        __asm__("fld tword [rsp+0x70]");
        v65 = reinterpret_cast<void**>(0);
        if (!eax68) {
            __asm__("fstp tword [rsp+0x20]");
            __asm__("fld dword [rsp]");
            rdi60 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rdx67)));
            rcx13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp64) + 95);
            rsi61 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp64) + 0x68);
            __asm__("fstp tword [rsp+0x80]");
            v62 = reinterpret_cast<void**>(0x3e22);
            eax69 = simple_strtod_int(rdi60, rsi61, reinterpret_cast<int64_t>(rsp64) + 0x80, rcx13, r8_8, 18);
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
            if (eax69 > 1) {
                *reinterpret_cast<uint32_t*>(&rbx6) = eax69;
                goto addr_3f38_44;
            } else {
                __asm__("fld tword [rsp+0x20]");
                rdi60 = rdi60;
                if (eax69 == 1) {
                    *reinterpret_cast<uint32_t*>(&rbx6) = 1;
                }
                if (v70) 
                    goto addr_4328_54; else 
                    goto addr_3e47_55;
            }
        } else {
            addr_3ada_56:
            zf71 = dev_debug == 0;
            if (!zf71) {
                rdi60 = stderr;
                __asm__("fld st0");
                __asm__("fstp tword [rsp]");
                *reinterpret_cast<int32_t*>(&rsi61) = 1;
                *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
                __asm__("fstp tword [rsp+0x30]");
                fun_2740(rdi60, 1, "  parsed numeric value: %Lf\n  input precision = %d\n", rdi60, 1, "  parsed numeric value: %Lf\n  input precision = %d\n");
                rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 16 - 8 + 8 + 8 + 8);
                __asm__("fld tword [rsp+0x20]");
                goto addr_3ae7_58;
            }
        }
    }
    addr_3f70_59:
    zf72 = reinterpret_cast<int1_t>(inval_style == 3);
    if (!zf72) {
        __asm__("fstp tword [rsp+0x20]");
        rax73 = quote(r13_41, r13_41);
        fun_24e0();
        *reinterpret_cast<uint32_t*>(&rdi60) = conv_exit_code;
        *reinterpret_cast<int32_t*>(&rdi60 + 4) = 0;
        rcx13 = rax73;
        *reinterpret_cast<int32_t*>(&rsi61) = 0;
        *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
        v62 = reinterpret_cast<void**>(0x3fb0);
        fun_26d0();
        rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 8 + 8 - 8 + 8);
        __asm__("fld tword [rsp+0x20]");
    }
    while (1) {
        rax74 = from_unit_size;
        rdx75 = to_unit_size;
        if (rax74 != 1 || !reinterpret_cast<int1_t>(rdx75 == 1)) {
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<signed char>(rax74) < reinterpret_cast<signed char>(0)) {
                __asm__("fadd dword [rip+0x9200]");
            }
            __asm__("fmulp st1, st0");
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<signed char>(rdx75) < reinterpret_cast<signed char>(0)) {
                __asm__("fadd dword [rip+0x9106]");
            }
            __asm__("fdivp st1, st0");
        }
        if (*reinterpret_cast<uint32_t*>(&rbx6) > 1) 
            break;
        rax76 = user_precision;
        __asm__("fld tword [rip+0x8e03]");
        __asm__("fxch st0, st1");
        cf77 = reinterpret_cast<unsigned char>(rax76) < reinterpret_cast<unsigned char>(0xffffffffffffffff);
        if (rax76 == 0xffffffffffffffff) {
            rax76 = v65;
        }
        __asm__("fcomi st0, st1");
        __asm__("fstp st1");
        v78 = rax76;
        if (cf77 || cf77) {
            *reinterpret_cast<uint32_t*>(&rbp25) = scale_to;
            *reinterpret_cast<int32_t*>(&rbp25 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rbp25)) 
                goto addr_40b6_73;
            if (reinterpret_cast<unsigned char>(v78) > reinterpret_cast<unsigned char>(18)) 
                goto addr_447e_75;
        } else {
            __asm__("fld st0");
            eax79 = 0;
            cf80 = 0;
            __asm__("fld dword [rip+0x8da4]");
            while (!cf80) {
                __asm__("fdivp st2, st0");
                tmp32_81 = eax79 + 1;
                cf80 = tmp32_81 < eax79;
                eax79 = tmp32_81;
            }
            __asm__("fstp st0");
            __asm__("fstp st0");
            __asm__("fstp st0");
            *reinterpret_cast<uint32_t*>(&rbp25) = scale_to;
            *reinterpret_cast<int32_t*>(&rbp25 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rbp25)) 
                goto addr_4052_80;
            *reinterpret_cast<uint32_t*>(&rdx82) = eax79;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx82) + 4) = 0;
            rdx75 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx82) + reinterpret_cast<unsigned char>(v78));
            if (reinterpret_cast<unsigned char>(rdx75) > reinterpret_cast<unsigned char>(18)) 
                goto addr_447e_75;
            addr_4052_80:
            if (eax79 > 26) 
                goto addr_4057_82;
        }
        addr_40b6_73:
        *reinterpret_cast<uint32_t*>(&r14_48) = grouping;
        r12_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp64) + 0x91);
        *reinterpret_cast<uint32_t*>(&r13_83) = round_style;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_83) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r14_48)) {
            r12_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp64) + 0x92);
        }
        r9_84 = zero_padding_width;
        if (r9_84) {
            rdi60 = r12_24;
            r8_8 = reinterpret_cast<void**>("0%ld");
            __asm__("fstp tword [rsp+0x20]");
            *reinterpret_cast<int32_t*>(&rsi61) = 62;
            *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
            v62 = reinterpret_cast<void**>(0x4470);
            eax85 = fun_23f0(rdi60, 62, 1, rdi60, 62, 1);
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
            r12_24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_24) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax85))));
        }
        *reinterpret_cast<uint32_t*>(&rcx13) = dev_debug;
        if (*reinterpret_cast<unsigned char*>(&rcx13)) {
            rcx86 = stderr;
            *reinterpret_cast<int32_t*>(&rsi61) = 1;
            *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
            rdi60 = reinterpret_cast<void**>("double_to_human:\n");
            __asm__("fstp tword [rsp+0x20]");
            v62 = reinterpret_cast<void**>(0x443c);
            fun_2730("double_to_human:\n", 1, 17, rcx86, r8_8);
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rcx13) = dev_debug;
            __asm__("fld tword [rsp+0x20]");
        }
        if (*reinterpret_cast<uint32_t*>(&rbp25)) 
            goto addr_4110_89;
        rax87 = v78;
        if (!*reinterpret_cast<int32_t*>(&rax87)) {
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fld tword [rip+0x80f8]");
            __asm__("fld st1");
            __asm__("fdiv st0, st1");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fild qword [rsp+0x20]");
            __asm__("fmulp st1, st0");
            __asm__("fld st1");
            __asm__("fsub st0, st1");
            if (*reinterpret_cast<uint32_t*>(&r13_83) <= 4) 
                goto addr_4d39_92;
            __asm__("fstp st0");
            __asm__("fstp st0");
        } else {
            eax88 = *reinterpret_cast<int32_t*>(&rax87) - 1;
            if (!eax88) {
                __asm__("fnstcw word [rsp+0x4e]");
                __asm__("fld tword [rip+0x7f0a]");
                __asm__("fld st1");
                __asm__("fmul dword [rip+0x7eb1]");
                __asm__("fld st0");
                __asm__("fdiv st0, st2");
                __asm__("fldcw word [rsp+0x4c]");
                __asm__("fistp qword [rsp+0x20]");
                __asm__("fldcw word [rsp+0x4e]");
                __asm__("fild qword [rsp+0x20]");
                __asm__("fmulp st2, st0");
                __asm__("fsub st0, st1");
                if (*reinterpret_cast<uint32_t*>(&r13_83) <= 4) 
                    goto addr_4f2d_96;
                __asm__("fstp st0");
                __asm__("fstp st0");
            } else {
                __asm__("fld dword [rip+0x8a2e]");
                edx89 = eax88;
                __asm__("fld st0");
                do {
                    __asm__("fmul st0, st1");
                    --edx89;
                } while (edx89);
                __asm__("fnstcw word [rsp+0x4e]");
                __asm__("fmul st0, st2");
                __asm__("fld tword [rip+0x8a5d]");
                __asm__("fld st1");
                __asm__("fdiv st0, st1");
                __asm__("fldcw word [rsp+0x4c]");
                __asm__("fistp qword [rsp+0x20]");
                __asm__("fldcw word [rsp+0x4e]");
                __asm__("fild qword [rsp+0x20]");
                __asm__("fmulp st1, st0");
                __asm__("fsub st1, st0");
                if (*reinterpret_cast<uint32_t*>(&r13_83) <= 4) 
                    goto addr_43d2_101;
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fldz ");
                __asm__("fxch st0, st1");
                __asm__("fld st0");
                do {
                    __asm__("fmul st0, st1");
                    --eax88;
                } while (eax88);
                __asm__("fstp st1");
                __asm__("fdivp st1, st0");
                goto addr_461b_106;
            }
        }
        __asm__("fldz ");
        addr_461b_106:
        *reinterpret_cast<uint32_t*>(&rbx6) = *reinterpret_cast<uint32_t*>(&v78);
        *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
        if (!*reinterpret_cast<unsigned char*>(&rcx13)) {
            __asm__("fxch st0, st1");
        } else {
            __asm__("fxch st0, st1");
            __asm__("fstp tword [rsp+0x10]");
            rdx90 = reinterpret_cast<void**>("  no scaling, returning (grouped) value: %'.*Lf\n");
            rdi91 = stderr;
            if (!*reinterpret_cast<uint32_t*>(&r14_48)) {
                rdx90 = reinterpret_cast<void**>("  no scaling, returning value: %.*Lf\n");
            }
            __asm__("fld st0");
            __asm__("fstp tword [rsp]");
            __asm__("fstp tword [rsp+0x10]");
            fun_2740(rdi91, 1, rdx90, rdi91, 1, rdx90);
            r14_48 = v92;
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 16 - 8 + 8 + 8 + 8);
            __asm__("fld tword [rsp]");
            __asm__("fld tword [rsp+0x10]");
        }
        __asm__("fstp tword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rcx13) = 0x80;
        *reinterpret_cast<void***>(r12_24) = reinterpret_cast<void**>(0x664c2a2e);
        *reinterpret_cast<void***>(r12_24 + 4) = reinterpret_cast<void**>(0);
        r12_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp64) + 0xd0);
        rsp93 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 16);
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        r8_8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp93) + 0xa0);
        __asm__("fstp tword [rsp+0x10]");
        eax94 = fun_23f0(r12_24, 0x80, 1, r12_24, 0x80, 1);
        rbp25 = v92;
        r13_41 = v62;
        rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp93) - 8 + 8 + 8 + 8);
        __asm__("fld tword [rsp]");
        if (eax94 <= 0x7f) 
            goto addr_46c9_113;
        __asm__("fstp tword [rsp]");
        fun_24e0();
        __asm__("fld tword [rsp]");
        *reinterpret_cast<int32_t*>(&rsi61) = 0;
        *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdi60) = 1;
        *reinterpret_cast<int32_t*>(&rdi60 + 4) = 0;
        v62 = rbx6;
        v92 = rbx6;
        __asm__("fstp tword [rsp]");
        fun_26d0();
        rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 - 8 - 8 + 8);
        addr_4f87_115:
        __asm__("fstp st0");
        v65 = reinterpret_cast<void**>(0);
        __asm__("fld1 ");
        addr_3b0d_116:
        if (*reinterpret_cast<unsigned char*>(&rcx13)) {
            __asm__("fxch st0, st1");
            __asm__("fstp tword [rsp+0x30]");
            *reinterpret_cast<uint32_t*>(&rcx13) = *reinterpret_cast<uint32_t*>(&r14_48);
            rdi60 = stderr;
            *reinterpret_cast<int32_t*>(&rsi61) = 1;
            *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
            __asm__("fld st0");
            __asm__("fstp tword [rsp]");
            __asm__("fstp tword [rsp+0x30]");
            fun_2740(rdi60, 1, "  suffix power=%d^%d = %Lf\n", rdi60, 1, "  suffix power=%d^%d = %Lf\n");
            __asm__("fld tword [rsp+0x30]");
            zf96 = dev_debug == 0;
            __asm__("fld tword [rsp+0x40]");
            __asm__("fmulp st1, st0");
            __asm__("fld st0");
            __asm__("fstp tword [rsp+0x80]");
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 16 - 8 + 8 + 8 + 8);
            if (!zf96) {
                rdi60 = stderr;
                __asm__("fld st0");
                __asm__("fstp tword [rsp+0x10]");
                *reinterpret_cast<int32_t*>(&rsi61) = 1;
                *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
                __asm__("fld st0");
                __asm__("fstp tword [rsp]");
                __asm__("fstp tword [rsp+0x40]");
                fun_2740(rdi60, 1, "  returning value: %Lf (%LG)\n", rdi60, 1, "  returning value: %Lf (%LG)\n");
                __asm__("fld tword [rsp+0x40]");
                rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 32 - 8 + 8 + 32);
            }
        } else {
            __asm__("fmulp st1, st0");
            __asm__("fld st0");
            __asm__("fstp tword [rsp+0x70]");
        }
        if (!*reinterpret_cast<void***>(rbp25)) {
            if (*reinterpret_cast<uint32_t*>(&rbx6) != 1) 
                continue;
            zf97 = debug == 0;
            if (zf97) 
                continue;
            __asm__("fstp tword [rsp+0x20]");
            rax98 = quote(r13_41, r13_41);
            fun_24e0();
            rcx13 = rax98;
            *reinterpret_cast<int32_t*>(&rsi61) = 0;
            *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi60) = 0;
            *reinterpret_cast<int32_t*>(&rdi60 + 4) = 0;
            v62 = reinterpret_cast<void**>(0x3f29);
            fun_26d0();
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 8 + 8 - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
        } else {
            zf99 = reinterpret_cast<int1_t>(inval_style == 3);
            *reinterpret_cast<uint32_t*>(&rbx6) = 5;
            if (zf99) 
                continue;
            __asm__("fstp tword [rsp+0x20]");
            *reinterpret_cast<uint32_t*>(&rbx6) = 5;
            rax100 = quote_n();
            rbp25 = rax100;
            rax101 = quote_n();
            fun_24e0();
            *reinterpret_cast<uint32_t*>(&rdi60) = conv_exit_code;
            *reinterpret_cast<int32_t*>(&rdi60 + 4) = 0;
            r8_8 = rbp25;
            *reinterpret_cast<int32_t*>(&rsi61) = 0;
            *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
            rcx13 = rax101;
            v62 = reinterpret_cast<void**>(0x3b86);
            fun_26d0();
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
        }
    }
    __asm__("fstp st0");
    addr_3be8_127:
    rsi30 = stdout;
    rdi102 = r12_24;
    *reinterpret_cast<int32_t*>(&r12_24) = 0;
    *reinterpret_cast<int32_t*>(&r12_24 + 4) = 0;
    fun_25c0(rdi102, rsi30, rdx75, rdi102, rsi30, rdx75);
    rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
    goto addr_394d_20;
    addr_447e_75:
    zf103 = reinterpret_cast<int1_t>(inval_style == 3);
    if (zf103) {
        __asm__("fstp st0");
        goto addr_3be8_127;
    } else {
        rbx6 = v78;
        __asm__("fstp tword [rsp]");
        *reinterpret_cast<uint32_t*>(&rbp25) = conv_exit_code;
        if (!rbx6) {
            rax104 = fun_24e0();
            __asm__("fld tword [rsp]");
            rdx75 = rax104;
            __asm__("fstp tword [rsp]");
            fun_26d0();
            rcx13 = v92;
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
            goto addr_3be8_127;
        } else {
            rax105 = fun_24e0();
            __asm__("fld tword [rsp]");
            rdx75 = rax105;
            rcx13 = rbx6;
            __asm__("fstp tword [rsp]");
            fun_26d0();
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
            goto addr_3be8_127;
        }
    }
    addr_4110_89:
    eax106 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rbp25 + 0xfffffffffffffffd));
    cf107 = eax106 < 1;
    if (eax106 <= 1) {
        *reinterpret_cast<void***>(rdi60) = *reinterpret_cast<void***>(rsi61);
        rdi108 = reinterpret_cast<void***>(rdi60 + 4);
    } else {
        *reinterpret_cast<void***>(rdi60) = *reinterpret_cast<void***>(rsi61);
        rdi108 = reinterpret_cast<void***>(rdi60 + 4);
    }
    __asm__("fld tword [rip+0x8cae]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    __asm__("fstp st1");
    if (cf107 || cf107) {
        __asm__("fld st0");
        *reinterpret_cast<uint32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
    } else {
        __asm__("fild dword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rbx6) = 0;
        *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
        cf109 = 0;
        __asm__("fld st1");
        while (!cf109) {
            __asm__("fdiv st0, st1");
            tmp32_110 = *reinterpret_cast<uint32_t*>(&rbx6) + 1;
            cf109 = tmp32_110 < *reinterpret_cast<uint32_t*>(&rbx6);
            *reinterpret_cast<uint32_t*>(&rbx6) = tmp32_110;
            *reinterpret_cast<int32_t*>(&rbx6 + 4) = 0;
        }
        __asm__("fstp st1");
    }
    if (*reinterpret_cast<unsigned char*>(&rcx13)) {
        __asm__("fxch st0, st1");
        __asm__("fstp tword [rsp+0x30]");
        rdi111 = stderr;
        *reinterpret_cast<void***>(rdi111) = g1;
        rdi112 = rdi111 + 4;
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fstp tword [rsp+0x30]");
        fun_2740(rdi112, 5, "  scaled value to %Lf * %0.f ^ %u\n", rdi112, 5, "  scaled value to %Lf * %0.f ^ %u\n");
        *reinterpret_cast<uint32_t*>(&rcx13) = dev_debug;
        rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 16 - 8 + 8 + 8 + 8);
        *reinterpret_cast<void***>(rdi112) = g5;
        rdi108 = reinterpret_cast<void***>(rdi112 + 4);
        __asm__("fld tword [rsp+0x30]");
        __asm__("fld tword [rsp+0x20]");
    }
    rsi113 = user_precision;
    if (rsi113 == 0xffffffffffffffff) {
        __asm__("fldz ");
        __asm__("fld st1");
        __asm__("fchs ");
        __asm__("fxch st0, st1");
        __asm__("fcomip st0, st2");
        __asm__("fcmovbe st0, st1");
        __asm__("fld dword [rip+0x81dc]");
        __asm__("fld st0");
        __asm__("fcomip st0, st2");
        __asm__("fstp st1");
        if (reinterpret_cast<unsigned char>(rsi113) <= reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
            __asm__("fstp st0");
        } else {
            addr_4be4_156:
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fmul st1, st0");
            __asm__("fld tword [rip+0x8210]");
            __asm__("fld st2");
            __asm__("fdiv st0, st1");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x10]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fild qword [rsp+0x10]");
            __asm__("fmulp st1, st0");
            __asm__("fsub st2, st0");
            cf114 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_83) < 4);
            pf115 = __intrinsic();
            zf116 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_83) == 4);
            below_or_equal117 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_83) <= 4);
            if (!below_or_equal117) {
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fldz ");
                goto addr_454d_158;
            } else {
                *reinterpret_cast<uint32_t*>(&rax118) = *reinterpret_cast<uint32_t*>(&r13_83);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax118) + 4) = 0;
                goto *reinterpret_cast<int32_t*>(0xcc1c + rax118 * 4) + 0xcc1c;
            }
        }
    } else {
        *reinterpret_cast<int32_t*>(&rax119) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx6 + reinterpret_cast<unsigned char>(rbx6) * 2));
        *reinterpret_cast<int32_t*>(&rax119 + 4) = 0;
        if (reinterpret_cast<signed char>(rax119) > reinterpret_cast<signed char>(rsi113)) {
            rax119 = rsi113;
        }
        *reinterpret_cast<int32_t*>(&rdi108) = *reinterpret_cast<int32_t*>(&rax119);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi108) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax119)) 
            goto addr_419a_163;
    }
    __asm__("fnstcw word [rsp+0x4e]");
    __asm__("fld tword [rip+0x82a0]");
    __asm__("fld st1");
    __asm__("fdiv st0, st1");
    __asm__("fldcw word [rsp+0x4c]");
    __asm__("fistp qword [rsp+0x10]");
    __asm__("fldcw word [rsp+0x4e]");
    __asm__("fild qword [rsp+0x10]");
    __asm__("fmulp st1, st0");
    __asm__("fsub st1, st0");
    cf114 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_83) < 4);
    pf115 = __intrinsic();
    zf116 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_83) == 4);
    below_or_equal117 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_83) <= 4);
    if (!below_or_equal117) {
        __asm__("fstp st0");
        __asm__("fstp st0");
        __asm__("fldz ");
        goto addr_454d_158;
    } else {
        *reinterpret_cast<uint32_t*>(&rax120) = *reinterpret_cast<uint32_t*>(&r13_83);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax120) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xcc08 + rax120 * 4) + 0xcc08;
    }
    addr_419a_163:
    *reinterpret_cast<uint32_t*>(&rax121) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax119) - 1);
    if (!*reinterpret_cast<uint32_t*>(&rax121)) {
        __asm__("fld dword [rip+0x7e07]");
        goto addr_4be4_156;
    }
    __asm__("fld dword [rip+0x8c0b]");
    edx122 = *reinterpret_cast<uint32_t*>(&rax121);
    __asm__("fld st0");
    do {
        __asm__("fmul st0, st1");
        --edx122;
    } while (edx122);
    __asm__("fnstcw word [rsp+0x4e]");
    __asm__("fmulp st2, st0");
    __asm__("fld tword [rip+0x8c3d]");
    __asm__("fld st2");
    __asm__("fdiv st0, st1");
    __asm__("fldcw word [rsp+0x4c]");
    __asm__("fistp qword [rsp+0x10]");
    __asm__("fldcw word [rsp+0x4e]");
    __asm__("fild qword [rsp+0x10]");
    __asm__("fmulp st1, st0");
    __asm__("fsub st2, st0");
    if (*reinterpret_cast<uint32_t*>(&r13_83) <= 4) 
        goto addr_41f2_171;
    __asm__("fstp st0");
    __asm__("fstp st1");
    __asm__("fldz ");
    __asm__("fxch st0, st1");
    __asm__("fld st0");
    do {
        __asm__("fmul st0, st1");
        cf114 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax121) < 1);
        below_or_equal117 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax121) <= 1);
        *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<uint32_t*>(&rax121) - 1;
        pf115 = __intrinsic();
        zf116 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax121) == 0);
    } while (!zf116);
    __asm__("fstp st1");
    __asm__("fdivp st1, st0");
    goto addr_4543_176;
    addr_41f2_171:
    goto *reinterpret_cast<int32_t*>(0xcbbc + r13_83 * 4) + 0xcbbc;
    addr_4d39_92:
    goto *reinterpret_cast<int32_t*>(0xcc30 + r13_83 * 4) + 0xcc30;
    addr_46c9_113:
    __asm__("fstp st0");
    __asm__("fld tword [rsp+0x10]");
    ebx123 = dev_debug;
    goto addr_46d6_177;
    addr_4f2d_96:
    goto *reinterpret_cast<int32_t*>(0xcc44 + r13_83 * 4) + 0xcc44;
    addr_43d2_101:
    __asm__("fstp st2");
    __asm__("fxch st0, st1");
    goto *reinterpret_cast<int32_t*>(0xcbd0 + r13_83 * 4) + 0xcbd0;
    addr_4057_82:
    zf124 = reinterpret_cast<int1_t>(inval_style == 3);
    __asm__("fstp tword [rsp]");
    if (!zf124) {
        fun_24e0();
        __asm__("fld tword [rsp]");
        __asm__("fstp tword [rsp]");
        fun_26d0();
        rdx75 = reinterpret_cast<void**>(0x407a);
        rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
        goto addr_3be8_127;
    }
    addr_4328_54:
    __asm__("fstp st0");
    __asm__("fld tword [rsp+0x70]");
    v65 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rbx6) = 3;
    rbp25 = reinterpret_cast<void**>("invalid number: %s");
    goto addr_3f70_59;
    addr_3e47_55:
    rbp25 = v125;
    __asm__("fld tword [rsp+0x80]");
    rax126 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp25) - reinterpret_cast<unsigned char>(rdi60));
    v65 = rax126;
    if (*reinterpret_cast<uint32_t*>(&rbp25) != *reinterpret_cast<uint32_t*>(&rdi60)) {
        eax127 = *reinterpret_cast<int32_t*>(&rax126) - 1;
        if (!eax127) {
            __asm__("fdiv dword [rip+0x7f0c]");
        } else {
            __asm__("fld dword [rip+0x8f43]");
            __asm__("fld st0");
            do {
                __asm__("fmul st0, st1");
                --eax127;
            } while (eax127);
            __asm__("fstp st1");
            __asm__("fdivp st1, st0");
        }
    }
    if (!v128) {
        __asm__("faddp st1, st0");
    } else {
        __asm__("fsubp st1, st0");
    }
    __asm__("fld st0");
    __asm__("fstp tword [rsp+0x70]");
    goto addr_3ada_56;
    addr_3ae7_58:
    *reinterpret_cast<uint32_t*>(&rcx13) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp25));
    if (*reinterpret_cast<unsigned char*>(&rcx13)) {
        __asm__("fstp tword [rsp+0x30]");
        rax129 = fun_2790(rdi60, rdi60);
        rsp130 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
        *reinterpret_cast<uint32_t*>(&rcx13) = *reinterpret_cast<unsigned char*>(&rcx13);
        __asm__("fld tword [rsp+0x30]");
        edi131 = 0;
        rsi132 = *reinterpret_cast<void***>(rax129);
        while (*reinterpret_cast<uint32_t*>(&rax133) = *reinterpret_cast<unsigned char*>(&rcx13), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax133) + 4) = 0, rdx134 = rbp25, ++rbp25, !!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rsi132) + reinterpret_cast<uint64_t>(rax133 * 2))) & 1)) {
            *reinterpret_cast<uint32_t*>(&rcx13) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx134 + 1));
            edi131 = 1;
        }
        if (*reinterpret_cast<signed char*>(&edi131)) 
            goto addr_3cfe_193;
    } else {
        if (*reinterpret_cast<uint32_t*>(&r15_46) == 4) {
            *reinterpret_cast<uint32_t*>(&rbx6) = 6;
            rbp25 = reinterpret_cast<void**>("missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)");
            goto addr_3f70_59;
        } else {
            *reinterpret_cast<uint32_t*>(&rcx13) = dev_debug;
            *reinterpret_cast<int32_t*>(&r8_8) = 0;
            *reinterpret_cast<int32_t*>(&r8_8 + 4) = 0;
            __asm__("fld1 ");
            goto addr_3b0d_116;
        }
    }
    addr_3d03_197:
    *reinterpret_cast<int32_t*>(&rsi61) = *reinterpret_cast<signed char*>(&rcx13);
    *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
    rdi60 = reinterpret_cast<void**>("KMGTPEZY");
    __asm__("fstp tword [rsp+0x20]");
    v62 = reinterpret_cast<void**>(0x3d1f);
    rax135 = fun_2540("KMGTPEZY", "KMGTPEZY");
    rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp130) - 8 + 8);
    __asm__("fld tword [rsp+0x20]");
    if (!rax135) {
        *reinterpret_cast<uint32_t*>(&rbx6) = 5;
        rbp25 = reinterpret_cast<void**>("invalid suffix in input: %s");
        goto addr_3f70_59;
    } else {
        if (!*reinterpret_cast<uint32_t*>(&r15_46)) {
            *reinterpret_cast<uint32_t*>(&rbx6) = 4;
            rbp25 = reinterpret_cast<void**>("rejecting suffix in input: %s (consider using --from)");
            goto addr_3f70_59;
        } else {
            *reinterpret_cast<int32_t*>(&r8_8) = 0;
            *reinterpret_cast<int32_t*>(&r8_8 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx13) = *reinterpret_cast<unsigned char*>(&rcx13) - 69;
            if (*reinterpret_cast<unsigned char*>(&rcx13) <= 21) {
                *reinterpret_cast<uint32_t*>(&rcx13) = *reinterpret_cast<unsigned char*>(&rcx13);
                *reinterpret_cast<int32_t*>(&rcx13 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r8_8) = *reinterpret_cast<int32_t*>(0xcd00 + reinterpret_cast<unsigned char>(rcx13) * 4);
                *reinterpret_cast<int32_t*>(&r8_8 + 4) = 0;
            }
            rbp25 = rdx134 + 1;
            if (*reinterpret_cast<uint32_t*>(&r15_46) != 1) 
                goto addr_3d6b_204;
        }
    }
    *reinterpret_cast<uint32_t*>(&rcx13) = dev_debug;
    if (*reinterpret_cast<void***>(rdx134 + 1) == 0x69) {
        rbp25 = rdx134 + 2;
        if (*reinterpret_cast<unsigned char*>(&rcx13)) {
            *reinterpret_cast<int32_t*>(&rsi61) = 1;
            *reinterpret_cast<int32_t*>(&rsi61 + 4) = 0;
            __asm__("fstp tword [rsp+0x20]");
            rdi60 = stderr;
            *reinterpret_cast<uint32_t*>(&r14_48) = 0x400;
            v62 = reinterpret_cast<void**>(0x4e50);
            fun_2740(rdi60, 1, "  Auto-scaling, found 'i', switching to base %d\n", rdi60, 1, "  Auto-scaling, found 'i', switching to base %d\n");
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 + 8);
            *reinterpret_cast<int32_t*>(&r8_8) = *reinterpret_cast<int32_t*>(&r8_8);
            *reinterpret_cast<int32_t*>(&r8_8 + 4) = 0;
            __asm__("fld tword [rsp+0x20]");
            __asm__("fld dword [rip+0x7f59]");
            *reinterpret_cast<uint32_t*>(&rcx13) = dev_debug;
        } else {
            __asm__("fld dword [rip+0x8118]");
            *reinterpret_cast<uint32_t*>(&r14_48) = 0x400;
        }
    } else {
        __asm__("fild dword [rsp+0x10]");
    }
    addr_3d85_210:
    if (!*reinterpret_cast<int32_t*>(&r8_8)) 
        goto addr_4f87_115;
    __asm__("fld st0");
    eax136 = *reinterpret_cast<int32_t*>(&r8_8) - 1;
    if (!eax136) {
        __asm__("fstp st1");
        v65 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r8_8) = 1;
        *reinterpret_cast<int32_t*>(&r8_8 + 4) = 0;
        goto addr_3b0d_116;
    } else {
        do {
            __asm__("fmul st0, st1");
            --eax136;
        } while (eax136);
        __asm__("fstp st1");
        v65 = reinterpret_cast<void**>(0);
        goto addr_3b0d_116;
    }
    addr_3d6b_204:
    if (*reinterpret_cast<uint32_t*>(&r15_46) == 4) {
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx134 + 1) == 0x69)) {
            v65 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&rbx6) = 6;
            rbp25 = reinterpret_cast<void**>("missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)");
            goto addr_3f70_59;
        } else {
            rbp25 = rdx134 + 2;
            goto addr_3d75_219;
        }
    } else {
        addr_3d75_219:
        __asm__("fild dword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rcx13) = dev_debug;
        goto addr_3d85_210;
    }
    addr_3cfe_193:
    goto addr_3d03_197;
    addr_3bff_41:
    rdi137 = stderr;
    fun_2740(rdi137, 1, "setting Auto-Padding to %ld characters\n", rdi137, 1, "setting Auto-Padding to %ld characters\n");
    rsp26 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp26) - 8 + 8);
    goto addr_3c20_34;
    addr_3a49_42:
    *reinterpret_cast<uint32_t*>(&r15_46) = scale_from;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_46) + 4) = 0;
    __asm__("pxor xmm2, xmm2");
    __asm__("movss [rsp], xmm2");
    __asm__("fld dword [rsp]");
    __asm__("fstp tword [rsp+0x70]");
    *reinterpret_cast<uint32_t*>(&r14_48) = (r14d138 - (r14d139 + reinterpret_cast<uint1_t>(r14d140 < r14d141 + reinterpret_cast<uint1_t>(static_cast<uint32_t>(r15_46 - 3) < 2))) & 24) + 0x3e8;
    goto addr_3a7e_43;
    addr_45b8_220:
    *reinterpret_cast<uint32_t*>(&rax142) = *reinterpret_cast<uint32_t*>(&rbx6);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax142) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xcbe4 + rax142 * 4) + 0xcbe4;
    addr_3964_221:
    goto v143;
    while (1) {
        if (!*reinterpret_cast<signed char*>(&ebx123)) {
            __asm__("fstp st0");
        } else {
            __asm__("fstp tword [rsp]");
            quote(r12_24, r12_24);
            __asm__("fld tword [rsp]");
            rdi144 = stderr;
            __asm__("fstp tword [rsp]");
            fun_2740(rdi144, 1, "formatting output:\n  value: %Lf\n  humanized: %s\n", rdi144, 1, "formatting output:\n  value: %Lf\n  humanized: %s\n");
            rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
        }
        while (1) {
            rbx6 = padding_width;
            rax145 = fun_2500(r12_24, r12_24);
            rsp146 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
            rdx147 = padding_buffer_size;
            rdi148 = padding_buffer;
            if (!rbx6 || reinterpret_cast<unsigned char>(rbx6) <= reinterpret_cast<unsigned char>(rax145)) {
                rcx13 = rax145 + 1;
                if (reinterpret_cast<unsigned char>(rcx13) >= reinterpret_cast<unsigned char>(rdx147)) {
                    padding_buffer_size = rax145 + 2;
                    rax149 = xrealloc(rdi148);
                    rsp146 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp146) - 8 + 8);
                    padding_buffer = rax149;
                    rdi148 = rax149;
                }
                fun_2450(rdi148, r12_24);
                rsp150 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp146) - 8 + 8);
            } else {
                *reinterpret_cast<int32_t*>(&r8_151) = padding_alignment;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_151) + 4) = 0;
                rcx13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp146) + 0x80);
                mbsalign(r12_24, rdi148, rdx147, rcx13, r8_151, 2);
                rsp150 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp146) - 8 + 8);
                zf152 = dev_debug == 0;
                if (!zf152) {
                    rdi153 = padding_buffer;
                    rax154 = quote(rdi153, rdi153);
                    rdi155 = stderr;
                    rdx147 = reinterpret_cast<void**>("  After padding: %s\n");
                    rcx13 = rax154;
                    fun_2740(rdi155, 1, "  After padding: %s\n", rdi155, 1, "  After padding: %s\n");
                    rsp150 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp150) - 8 + 8 - 8 + 8);
                }
            }
            rdi156 = format_str_prefix;
            rsi30 = stdout;
            if (rdi156) {
                fun_25c0(rdi156, rsi30, rdx147, rdi156, rsi30, rdx147);
                rsp150 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp150) - 8 + 8);
                rsi30 = stdout;
            }
            rdi157 = padding_buffer;
            *reinterpret_cast<int32_t*>(&r12_24) = 1;
            *reinterpret_cast<int32_t*>(&r12_24 + 4) = 0;
            fun_25c0(rdi157, rsi30, rdx147, rdi157, rsi30, rdx147);
            rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp150) - 8 + 8);
            rdi158 = format_str_suffix;
            if (rdi158) {
                rsi30 = stdout;
                fun_25c0(rdi158, rsi30, rdx147, rdi158, rsi30, rdx147);
                rsp32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
            }
            addr_394d_20:
            rax159 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v28) - reinterpret_cast<unsigned char>(g28));
            if (!rax159) 
                goto addr_3964_221;
            fun_2510();
            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp32) - 8 + 8);
            while (1) {
                addr_4f9e_236:
                __asm__("fld st0");
                __asm__("fchs ");
                while (1) {
                    __asm__("fxch st0, st1");
                    while (1) {
                        __asm__("fld dword [rip+0x8278]");
                        __asm__("fcomip st0, st2");
                        __asm__("fstp st1");
                        do {
                            if (*reinterpret_cast<unsigned char*>(&rcx13)) {
                                __asm__("fxch st0, st1");
                                __asm__("fstp tword [rsp+0x20]");
                                rdi160 = stderr;
                                __asm__("fld st0");
                                __asm__("fstp tword [rsp]");
                                __asm__("fstp tword [rsp+0x10]");
                                fun_2740(rdi160, 1, "  after rounding, value=%Lf * %0.f ^ %u\n", rdi160, 1, "  after rounding, value=%Lf * %0.f ^ %u\n");
                                rsi30 = user_precision;
                                rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 16 - 8 + 8 + 8 + 8);
                                __asm__("fld tword [rsp+0x20]");
                                __asm__("fld tword [rsp]");
                            }
                            *reinterpret_cast<void***>(r12_24) = reinterpret_cast<void**>(0x664c2a2e);
                            *reinterpret_cast<void***>(r12_24 + 4) = reinterpret_cast<void**>(0x7325);
                            if (!reinterpret_cast<int1_t>(rsi30 == 0xffffffffffffffff)) {
                            }
                            *reinterpret_cast<signed char*>(r12_24 + 6) = 0;
                            if (*reinterpret_cast<uint32_t*>(&rbx6) <= 8) 
                                goto addr_45b8_220;
                            __asm__("fxch st0, st1");
                            __asm__("fstp tword [rsp+0x10]");
                            *reinterpret_cast<uint32_t*>(&rcx13) = 0x80;
                            r12_24 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp64) + 0xd0);
                            __asm__("fld st0");
                            __asm__("fstp tword [rsp]");
                            __asm__("fstp tword [rsp+0x20]");
                            eax161 = fun_23f0(r12_24, 0x7f, 1, r12_24, 0x7f, 1);
                            __asm__("fld tword [rsp+0x20]");
                            rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp64) - 8 - 8 - 16 - 8 + 8 + 32);
                            __asm__("fld tword [rsp+0x10]");
                            if (eax161 <= 0x7e) 
                                goto addr_4937_246;
                            __asm__("fstp st0");
                            __asm__("fstp tword [rsp]");
                            fun_24e0();
                            __asm__("fld tword [rsp]");
                            *reinterpret_cast<int32_t*>(&rsi113) = 0;
                            *reinterpret_cast<int32_t*>(&rsi113 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rdi108) = 1;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi108) + 4) = 0;
                            cf114 = 0;
                            pf115 = __intrinsic();
                            zf116 = 1;
                            below_or_equal117 = 1;
                            __asm__("fstp tword [rsp]");
                            fun_26d0();
                            rsp64 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 - 8 - 8 + 8);
                            __asm__("fxch st0, st2");
                            __asm__("fldcw word [rsp+0x4c]");
                            __asm__("fistp qword [rsp+0x10]");
                            __asm__("fldcw word [rsp+0x4e]");
                            __asm__("fild qword [rsp+0x10]");
                            __asm__("faddp st2, st0");
                            __asm__("fdivp st1, st0");
                            addr_4543_176:
                            __asm__("fldz ");
                            __asm__("fcomip st0, st1");
                            if (!below_or_equal117) {
                                *rdi108 = *reinterpret_cast<void***>(rsi113);
                                rsi30 = rsi113 + 4;
                                __asm__("fld qword [rsp+0x10]");
                                __asm__("fld st1");
                                __asm__("fchs ");
                                __asm__("fcomi st0, st1");
                                if (cf114) 
                                    goto addr_4b32_254;
                                __asm__("fstp st0");
                                __asm__("fxch st0, st1");
                            } else {
                                addr_454d_158:
                                *rdi108 = *reinterpret_cast<void***>(rsi113);
                                rsi30 = rsi113 + 4;
                                __asm__("fld qword [rsp+0x10]");
                                __asm__("fxch st0, st1");
                                __asm__("fcomi st0, st1");
                                if (cf114) 
                                    goto addr_456a_259; else 
                                    goto addr_455d_260;
                            }
                            addr_4563_261:
                            tmp32_162 = *reinterpret_cast<uint32_t*>(&rbx6) + 1;
                            cf163 = reinterpret_cast<uint1_t>(tmp32_162 < *reinterpret_cast<uint32_t*>(&rbx6));
                            *reinterpret_cast<uint32_t*>(&rbx6) = tmp32_162;
                            pf115 = __intrinsic();
                            zf116 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rbx6) == 0);
                            below_or_equal117 = reinterpret_cast<uint1_t>(cf163 | zf116);
                            __asm__("fdivrp st1, st0");
                            continue;
                            addr_456a_259:
                            __asm__("fstp st1");
                            continue;
                            addr_455d_260:
                            goto addr_4563_261;
                            __asm__("fldz ");
                            __asm__("fxch st0, st1");
                            __asm__("fucomi st0, st1");
                            __asm__("fstp st1");
                        } while (!pf115 && zf116);
                        __asm__("fldz ");
                        __asm__("fcomip st0, st1");
                        if (!below_or_equal117) 
                            goto addr_4f9e_236;
                        __asm__("fld st0");
                    }
                    addr_4b32_254:
                    __asm__("fstp st1");
                }
            }
            addr_4937_246:
            __asm__("fstp st1");
            if (*reinterpret_cast<uint32_t*>(&rbx6)) {
                if (*reinterpret_cast<uint32_t*>(&rbp25) == 4) {
                    __asm__("fstp tword [rsp]");
                    fun_2750(r12_24, "i");
                    rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8);
                    __asm__("fld tword [rsp]");
                }
            }
            ebx123 = dev_debug;
            if (*reinterpret_cast<signed char*>(&ebx123)) 
                break;
            rbp25 = suffix;
            if (rbp25) 
                goto addr_46e2_270;
            __asm__("fstp st0");
        }
        __asm__("fstp tword [rsp]");
        quote(r12_24, r12_24);
        rdi164 = stderr;
        fun_2740(rdi164, 1, "  returning value: %s\n", rdi164, 1, "  returning value: %s\n");
        rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 + 8);
        ebx123 = dev_debug;
        __asm__("fld tword [rsp]");
        addr_46d6_177:
        rbp25 = suffix;
        if (!rbp25) 
            continue;
        addr_46e2_270:
        __asm__("fstp tword [rsp]");
        fun_2500(r12_24, r12_24);
        fun_2750(r12_24, rbp25);
        rsp95 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp95) - 8 + 8 - 8 + 8);
        __asm__("fld tword [rsp]");
    }
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t g10df8 = 0;

void fun_2033() {
    __asm__("cli ");
    goto g10df8;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2363() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2373() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2383() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2393() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_23d3() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_23e3() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t __snprintf_chk = 0x2030;

void fun_23f3() {
    __asm__("cli ");
    goto __snprintf_chk;
}

int64_t free = 0x2040;

void fun_2403() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2050;

void fun_2413() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2060;

void fun_2423() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2070;

void fun_2433() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2080;

void fun_2443() {
    __asm__("cli ");
    goto _exit;
}

int64_t strcpy = 0x2090;

void fun_2453() {
    __asm__("cli ");
    goto strcpy;
}

int64_t __fpending = 0x20a0;

void fun_2463() {
    __asm__("cli ");
    goto __fpending;
}

int64_t qsort = 0x20b0;

void fun_2473() {
    __asm__("cli ");
    goto qsort;
}

int64_t reallocarray = 0x20c0;

void fun_2483() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t wcswidth = 0x20d0;

void fun_2493() {
    __asm__("cli ");
    goto wcswidth;
}

int64_t mbstowcs = 0x20e0;

void fun_24a3() {
    __asm__("cli ");
    goto mbstowcs;
}

int64_t textdomain = 0x20f0;

void fun_24b3() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x2100;

void fun_24c3() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x2110;

void fun_24d3() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x2120;

void fun_24e3() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2130;

void fun_24f3() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2140;

void fun_2503() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2150;

void fun_2513() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2160;

void fun_2523() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2170;

void fun_2533() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t strchr = 0x2180;

void fun_2543() {
    __asm__("cli ");
    goto strchr;
}

int64_t __overflow = 0x2190;

void fun_2553() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x21a0;

void fun_2563() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x21b0;

void fun_2573() {
    __asm__("cli ");
    goto lseek;
}

int64_t __assert_fail = 0x21c0;

void fun_2583() {
    __asm__("cli ");
    goto __assert_fail;
}

int64_t memset = 0x21d0;

void fun_2593() {
    __asm__("cli ");
    goto memset;
}

int64_t strspn = 0x21e0;

void fun_25a3() {
    __asm__("cli ");
    goto strspn;
}

int64_t memcmp = 0x21f0;

void fun_25b3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x2200;

void fun_25c3() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t calloc = 0x2210;

void fun_25d3() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x2220;

void fun_25e3() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2230;

void fun_25f3() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t strtol = 0x2240;

void fun_2603() {
    __asm__("cli ");
    goto strtol;
}

int64_t memcpy = 0x2250;

void fun_2613() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2260;

void fun_2623() {
    __asm__("cli ");
    goto fileno;
}

int64_t wcwidth = 0x2270;

void fun_2633() {
    __asm__("cli ");
    goto wcwidth;
}

int64_t malloc = 0x2280;

void fun_2643() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2290;

void fun_2653() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x22a0;

void fun_2663() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x22b0;

void fun_2673() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x22c0;

void fun_2683() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x22d0;

void fun_2693() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22e0;

void fun_26a3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x22f0;

void fun_26b3() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t memmove = 0x2300;

void fun_26c3() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x2310;

void fun_26d3() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x2320;

void fun_26e3() {
    __asm__("cli ");
    goto fseeko;
}

int64_t strtoumax = 0x2330;

void fun_26f3() {
    __asm__("cli ");
    goto strtoumax;
}

int64_t __cxa_atexit = 0x2340;

void fun_2703() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t wcstombs = 0x2350;

void fun_2713() {
    __asm__("cli ");
    goto wcstombs;
}

int64_t exit = 0x2360;

void fun_2723() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2370;

void fun_2733() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2380;

void fun_2743() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t __strncat_chk = 0x2390;

void fun_2753() {
    __asm__("cli ");
    goto __strncat_chk;
}

int64_t mbsinit = 0x23a0;

void fun_2763() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x23b0;

void fun_2773() {
    __asm__("cli ");
    goto iswprint;
}

int64_t getdelim = 0x23c0;

void fun_2783() {
    __asm__("cli ");
    goto getdelim;
}

int64_t __ctype_b_loc = 0x23d0;

void fun_2793() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(void** rdi);

void** fun_2690(int64_t rdi, ...);

void fun_24d0(int64_t rdi, void** rsi);

void fun_24b0(int64_t rdi, void** rsi);

void** fun_2660(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void atexit(int64_t rdi, void** rsi);

int32_t fun_2520(int64_t rdi, void** rsi);

int64_t Version = 0xcf9f;

void version_etc(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8);

void fun_2720();

int64_t header = 0;

void usage();

void** optarg = reinterpret_cast<void**>(0);

void** argmatch_die = reinterpret_cast<void**>(0x90);

int64_t __xargmatch_internal(int64_t rdi, void** rsi, int64_t rdx, int64_t rcx, int64_t r8, void** r9);

void** format_str = reinterpret_cast<void**>(0);

int32_t optind = 0;

void** stdin = reinterpret_cast<void**>(0);

void* fun_2780(void** rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_25a0(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9);

void** fun_2600(void** rdi, void** rsi, int64_t rdx, void** rcx, void** r8, void** r9);

unsigned char __cxa_finalize;

void** ximemdup0(void** rdi, void** rsi, ...);

void** xstrdup(void** rdi, void** rsi);

struct s5 {
    signed char[1] pad1;
    void** f1;
};

void fun_2823(int32_t edi, void** rsi, void** rdx, void** rcx, void** r8, void** r9) {
    int32_t ebp7;
    void** rbx8;
    void** rdi9;
    void** rax10;
    void** v11;
    void** rax12;
    void** rdi13;
    void** r13_14;
    void** r12_15;
    void** r15_16;
    void** rax17;
    void* rsp18;
    void** r8_19;
    void** rcx20;
    void** rdx21;
    void** rsi22;
    int64_t rdi23;
    int32_t eax24;
    void* rsp25;
    void** rdi26;
    int64_t rcx27;
    int64_t rax28;
    void** rsi29;
    int64_t rax30;
    int1_t zf31;
    int1_t zf32;
    int1_t zf33;
    void** rax34;
    int1_t zf35;
    uint32_t eax36;
    uint32_t eax37;
    int1_t zf38;
    void** rax39;
    int1_t cf40;
    void** rdi41;
    void** rax42;
    int1_t zf43;
    int1_t zf44;
    void** rax45;
    int32_t eax46;
    int1_t zf47;
    uint1_t zf48;
    int1_t zf49;
    int1_t less50;
    int64_t rax51;
    void* rax52;
    void** rsi53;
    int1_t zf54;
    int1_t zf55;
    void** rax56;
    int64_t rax57;
    void** rdi58;
    uint32_t eax59;
    int32_t tmp32_60;
    void* rax61;
    uint32_t edx62;
    signed char* rax63;
    uint1_t zf64;
    uint32_t eax65;
    void** rax66;
    void** rax67;
    int1_t zf68;
    void** rax69;
    void** eax70;
    signed char v71;
    void** rdi72;
    void** rax73;
    void** r13_74;
    void** rax75;
    void** rax76;
    int1_t zf77;
    int1_t zf78;
    uint32_t edx79;
    void* rax80;
    void** r14_81;
    void** rax82;
    void** rax83;
    void* rax84;
    void** rax85;
    int1_t zf86;
    void** rax87;
    int1_t zf88;
    int1_t zf89;
    void** rsi90;
    void** rax91;
    int1_t zf92;
    void** r9_93;
    int1_t zf94;
    void** rax95;
    void** rdi96;
    void** rsi97;
    void** rax98;
    struct s5* rax99;
    void** rax100;
    void** rax101;
    void** rax102;
    void** rax103;
    void* rcx104;
    void** rdi105;
    void** rax106;
    int1_t zf107;
    int1_t zf108;
    void** rax109;

    __asm__("cli ");
    ebp7 = edi;
    rbx8 = rsi;
    rdi9 = *reinterpret_cast<void***>(rsi);
    set_program_name(rdi9);
    rax10 = fun_2690(6, 6);
    v11 = rax10;
    fun_24d0("coreutils", "/usr/local/share/locale");
    fun_24b0("coreutils", "/usr/local/share/locale");
    rax12 = fun_2660(0x10000, "/usr/local/share/locale", rdx, rcx, r8, r9);
    decimal_point = rax12;
    if (!rax12 || !*reinterpret_cast<void***>(rax12)) {
        decimal_point = reinterpret_cast<void**>(".");
    }
    rdi13 = decimal_point;
    r13_14 = reinterpret_cast<void**>("d:z");
    r12_15 = reinterpret_cast<void**>(0xcc58);
    r15_16 = reinterpret_cast<void**>(0xcd60);
    rax17 = fun_2500(rdi13, rdi13);
    decimal_point_length = *reinterpret_cast<int32_t*>(&rax17);
    atexit(0x6420, "/usr/local/share/locale");
    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 72 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
    while (*reinterpret_cast<int32_t*>(&r8_19) = 0, *reinterpret_cast<int32_t*>(&r8_19 + 4) = 0, rcx20 = reinterpret_cast<void**>(0x10840), rdx21 = reinterpret_cast<void**>("d:z"), rsi22 = rbx8, *reinterpret_cast<int32_t*>(&rdi23) = ebp7, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi23) + 4) = 0, eax24 = fun_2520(rdi23, rsi22), rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp18) - 8 + 8), eax24 != -1) {
        if (eax24 <= 0x8d) {
            if (eax24 <= 99) {
                if (eax24 == 0xffffff7d) {
                    rdi26 = stdout;
                    rcx27 = Version;
                    *reinterpret_cast<int32_t*>(&r9) = 0;
                    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
                    version_etc(rdi26, "numfmt", "GNU coreutils", rcx27, "Assaf Gordon");
                    fun_2720();
                    rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8);
                    header = 1;
                    continue;
                } else {
                    if (eax24 == 0xffffff7e) {
                        usage();
                        rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                    }
                }
            } else {
                *reinterpret_cast<uint32_t*>(&rax28) = eax24 - 100;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax28) + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax28) <= 41) 
                    goto addr_2910_13;
            }
        }
        usage();
        rsi29 = optarg;
        r9 = argmatch_die;
        rax30 = __xargmatch_internal("--invalid", rsi29, 0x10aa0, 0xcd60, 4, r9);
        rsp18 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 - 8 - 8 + 8 + 8 + 8);
        inval_style = *reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0xcd60) + reinterpret_cast<uint64_t>(rax30 * 4));
    }
    zf31 = format_str == 0;
    if (zf31 || (zf32 = grouping == 0, zf32)) {
        zf33 = debug == 0;
        if (!zf33 && !v11) {
            rax34 = fun_24e0();
            *reinterpret_cast<uint32_t*>(&rsi22) = 0;
            *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
            rdx21 = rax34;
            fun_26d0();
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8);
        }
        zf35 = debug == 0;
        r12_15 = format_str;
        if (zf35) 
            goto addr_2c9c_19;
        eax36 = scale_from;
        eax37 = eax36 | scale_to;
        if (!eax37) 
            goto addr_315b_21;
    } else {
        addr_3407_22:
        fun_24e0();
        fun_26d0();
        goto addr_342b_23;
    }
    addr_2c9c_19:
    if (!r12_15) {
        while (1) {
            zf38 = grouping == 0;
            if (zf38) {
                addr_2e0d_25:
                rax39 = padding_width;
                cf40 = reinterpret_cast<unsigned char>(rax39) < reinterpret_cast<unsigned char>(padding_buffer_size);
                if (!cf40) {
                    rdi41 = padding_buffer;
                    padding_buffer_size = rax39 + 1;
                    rax42 = xrealloc(rdi41, rdi41);
                    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                    padding_buffer = rax42;
                }
            } else {
                zf43 = scale_to == 0;
                if (!zf43) 
                    goto addr_344f_28;
                addr_2e00_29:
                zf44 = debug == 0;
                if (zf44) 
                    goto addr_2e0d_25;
                rax45 = fun_2660(0x10001, rsi22, rdx21, rcx20, r8_19, r9);
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                if (*reinterpret_cast<void***>(rax45)) 
                    goto addr_2e0d_25; else 
                    goto addr_323d_31;
            }
            eax46 = 0;
            zf47 = padding_width == 0;
            if (zf47) {
                eax46 = 0;
                zf48 = reinterpret_cast<uint1_t>(delimiter == 0x80);
                *reinterpret_cast<unsigned char*>(&eax46) = zf48;
            }
            zf49 = inval_style == 0;
            auto_padding = eax46;
            if (!zf49) {
                conv_exit_code = 0;
            }
            less50 = optind < ebp7;
            if (!less50) {
                r13_14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp25) + 48);
                rbx8 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp25) + 40);
                while (rax51 = header, rcx20 = stdin, header = rax51 - 1, *reinterpret_cast<uint32_t*>(&rdx21) = line_delim, *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0, !!rax51) {
                    rax52 = fun_2780(rbx8, r13_14, rdx21, rcx20, r8_19, r9);
                    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                    if (reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rax52) < reinterpret_cast<int64_t>(0)) | reinterpret_cast<uint1_t>(rax52 == 0)) 
                        goto addr_32d0_40;
                    rsi53 = stdout;
                    fun_25c0(0, rsi53, rdx21, 0, rsi53, rdx21);
                    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                }
            } else {
                zf54 = debug == 0;
                if (!zf54 && (zf55 = header == 0, !zf55)) {
                    rax56 = fun_24e0();
                    rdx21 = rax56;
                    fun_26d0();
                    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8);
                }
                *reinterpret_cast<uint32_t*>(&r12_15) = 1;
                *reinterpret_cast<int32_t*>(&r12_15 + 4) = 0;
                while (rax57 = optind, *reinterpret_cast<int32_t*>(&rax57) < ebp7) {
                    rdi58 = *reinterpret_cast<void***>(rbx8 + rax57 * 8);
                    eax59 = process_line(rdi58, 1, rdi58, 1);
                    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                    tmp32_60 = optind + 1;
                    optind = tmp32_60;
                    *reinterpret_cast<uint32_t*>(&r12_15) = *reinterpret_cast<uint32_t*>(&r12_15) & eax59;
                    *reinterpret_cast<int32_t*>(&r12_15 + 4) = 0;
                }
                goto addr_2eb3_47;
            }
            addr_30c7_48:
            *reinterpret_cast<uint32_t*>(&r12_15) = 1;
            *reinterpret_cast<int32_t*>(&r12_15 + 4) = 0;
            while (rax61 = fun_2780(rbx8, r13_14, rdx21, rcx20, r8_19, r9), rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8), !reinterpret_cast<uint1_t>(reinterpret_cast<uint1_t>(reinterpret_cast<int64_t>(rax61) < reinterpret_cast<int64_t>(0)) | reinterpret_cast<uint1_t>(rax61 == 0))) {
                edx62 = line_delim;
                rax63 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rax61) + 0xffffffffffffffff);
                zf64 = reinterpret_cast<uint1_t>(static_cast<int32_t>(*rax63) == edx62);
                if (zf64) {
                    *rax63 = 0;
                }
                eax65 = process_line(0, static_cast<uint32_t>(static_cast<unsigned char>(zf64)));
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                rcx20 = stdin;
                *reinterpret_cast<uint32_t*>(&rdx21) = line_delim;
                *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&r12_15) = *reinterpret_cast<uint32_t*>(&r12_15) & eax65;
                *reinterpret_cast<int32_t*>(&r12_15 + 4) = 0;
            }
            rax66 = stdin;
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax66)) & 32) {
                rax67 = fun_24e0();
                r13_14 = rax67;
                fun_2420();
                rdx21 = r13_14;
                fun_26d0();
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8 - 8 + 8);
            }
            addr_2eb3_47:
            zf68 = debug == 0;
            if (zf68) {
                if (*reinterpret_cast<uint32_t*>(&r12_15)) 
                    goto addr_2ec9_56;
            } else {
                if (*reinterpret_cast<uint32_t*>(&r12_15)) 
                    goto addr_2ec9_56;
                rax69 = fun_24e0();
                rdx21 = rax69;
                fun_26d0();
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8);
            }
            eax70 = inval_style;
            if (reinterpret_cast<uint32_t>(eax70 - 2) <= 1) {
                addr_2ec9_56:
            }
            fun_2720();
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
            addr_2ed0_62:
            v71 = 0;
            rdi72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<unsigned char>(r13_14));
            while (1) {
                rax73 = fun_25a0(rdi72, " ", rdx21, rcx20, r8_19, r9);
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                r13_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_14) + reinterpret_cast<unsigned char>(rax73));
                rdi72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<unsigned char>(r13_14));
                *reinterpret_cast<uint32_t*>(&rdx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi72));
                *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rdx21) == 39) {
                    grouping = 1;
                    ++r13_14;
                    rdi72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<unsigned char>(r13_14));
                } else {
                    if (*reinterpret_cast<signed char*>(&rdx21) == 48) {
                        ++r13_14;
                        v71 = 1;
                        rdi72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<unsigned char>(r13_14));
                    } else {
                        if (!rax73) 
                            break;
                    }
                }
            }
            r13_74 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp25) + 48);
            rax75 = fun_2420();
            rsi22 = r13_74;
            *reinterpret_cast<void***>(rax75) = reinterpret_cast<void**>(0);
            rax76 = fun_2600(rdi72, rsi22, 10, rcx20, r8_19, r9);
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8);
            rcx20 = rax75;
            if (*reinterpret_cast<void***>(rcx20) == 34) 
                break;
            if (rax76 == 0x8000000000000000) 
                break;
            if (rdi72 && rax76) {
                zf77 = debug == 0;
                if (!zf77 && (zf78 = padding_width == 0, !zf78)) {
                    if (!v71) 
                        goto addr_32e3_73;
                    if (!(reinterpret_cast<uint1_t>(reinterpret_cast<signed char>(rax76) < reinterpret_cast<signed char>(0)) | reinterpret_cast<uint1_t>(rax76 == 0))) 
                        goto addr_2f98_75;
                    addr_32e3_73:
                    fun_24e0();
                    *reinterpret_cast<uint32_t*>(&rsi22) = 0;
                    *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                    fun_26d0();
                    rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8);
                    rcx20 = rcx20;
                }
                if (reinterpret_cast<signed char>(rax76) < reinterpret_cast<signed char>(0)) {
                    padding_alignment = 0;
                    padding_width = reinterpret_cast<void**>(-reinterpret_cast<unsigned char>(rax76));
                } else {
                    if (v71) {
                        addr_2f98_75:
                        zero_padding_width = rax76;
                    } else {
                        padding_width = rax76;
                    }
                }
            }
            edx79 = __cxa_finalize;
            rax80 = reinterpret_cast<void*>(-static_cast<int64_t>(reinterpret_cast<unsigned char>(r12_15)));
            if (!*reinterpret_cast<signed char*>(&edx79)) 
                goto addr_34fb_81;
            if (*reinterpret_cast<signed char*>(&edx79) == 46) {
                *reinterpret_cast<void***>(rcx20) = reinterpret_cast<void**>(0);
                r14_81 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<uint64_t>(rax80) + 1);
                rsi22 = r13_74;
                rax82 = fun_2600(r14_81, rsi22, 10, rcx20, r8_19, r9);
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                user_precision = rax82;
                if (*reinterpret_cast<void***>(rcx20) == 34) 
                    goto addr_355f_84;
                if (reinterpret_cast<signed char>(rax82) < reinterpret_cast<signed char>(0)) 
                    goto addr_355f_84;
                rax83 = fun_2790(r14_81, r14_81);
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                rcx20 = reinterpret_cast<void**>(static_cast<int64_t>(reinterpret_cast<signed char>(*reinterpret_cast<void***>(r14_81))));
                r9 = rax83;
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(r9) + reinterpret_cast<unsigned char>(rcx20) * 2)) & 1) 
                    goto addr_355f_84;
                if (*reinterpret_cast<signed char*>(&rcx20) == 43) 
                    goto addr_355f_84;
                rax80 = reinterpret_cast<void*>(-static_cast<int64_t>(reinterpret_cast<unsigned char>(r12_15)));
            }
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<uint64_t>(rax80)) != 0x66) 
                goto addr_34c9_90;
            rax84 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax80) + 1);
            r13_14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<uint64_t>(rax84));
            *reinterpret_cast<uint32_t*>(&rdx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_14));
            *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
            if (*reinterpret_cast<signed char*>(&rdx21)) {
                do {
                    *reinterpret_cast<uint32_t*>(&rsi22) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<uint64_t>(rax84) + 1);
                    *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
                    *reinterpret_cast<uint32_t*>(&rcx20) = 1;
                    *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rdx21) == 37) {
                        if (*reinterpret_cast<signed char*>(&rsi22) != 37) 
                            goto addr_3473_94;
                        *reinterpret_cast<uint32_t*>(&rcx20) = 2;
                        *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0;
                    }
                    rax84 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rax84) + reinterpret_cast<unsigned char>(rcx20));
                    *reinterpret_cast<uint32_t*>(&rdx21) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<uint64_t>(rax84));
                    *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
                } while (*reinterpret_cast<signed char*>(&rdx21));
                if (!r15_16) 
                    goto addr_31e0_98;
            } else {
                if (!r15_16) 
                    goto addr_2d1f_101;
            }
            rsi22 = r15_16;
            rax85 = ximemdup0(r12_15, rsi22);
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
            zf86 = *reinterpret_cast<void***>(r13_14) == 0;
            format_str_prefix = rax85;
            if (!zf86) {
                addr_31e0_98:
                rax87 = xstrdup(r13_14, rsi22);
                rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8);
                zf88 = dev_debug == 0;
                format_str_suffix = rax87;
                rsi22 = rax87;
                if (zf88) {
                    continue;
                }
            } else {
                addr_2d1f_101:
                zf89 = dev_debug == 0;
                if (zf89) 
                    continue; else 
                    goto addr_2d2c_105;
            }
            addr_2d3f_106:
            quote_n();
            rsi90 = format_str_prefix;
            if (!rsi90) {
            }
            r13_14 = reinterpret_cast<void**>("yes");
            rax91 = quote_n();
            zf92 = padding_alignment == 0;
            r9_93 = padding_width;
            r15_16 = rax91;
            if (!zf92) {
            }
            zf94 = grouping == 0;
            if (zf94) {
                r13_14 = reinterpret_cast<void**>("no");
            }
            rax95 = quote_n();
            rdi96 = stderr;
            *reinterpret_cast<uint32_t*>(&rsi22) = 1;
            *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
            rcx20 = rax95;
            r8_19 = r13_14;
            rdx21 = reinterpret_cast<void**>("format String:\n  input: %s\n  grouping: %s\n  padding width: %ld\n  alignment: %s\n  prefix: %s\n  suffix: %s\n");
            r9 = r9_93;
            fun_2740(rdi96, 1, "format String:\n  input: %s\n  grouping: %s\n  padding width: %ld\n  alignment: %s\n  prefix: %s\n  suffix: %s\n", rdi96, 1, "format String:\n  input: %s\n  grouping: %s\n  padding width: %ld\n  alignment: %s\n  prefix: %s\n  suffix: %s\n");
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8 - 8 + 8 - 8 - 8 - 8 - 8 - 8 + 8 + 32);
            continue;
            addr_2d2c_105:
            rsi97 = format_str_suffix;
            if (!rsi97) {
                goto addr_2d3f_106;
            }
            addr_32d0_40:
            rcx20 = stdin;
            *reinterpret_cast<uint32_t*>(&rdx21) = line_delim;
            *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
            goto addr_30c7_48;
            addr_323d_31:
            rax98 = fun_24e0();
            rdx21 = rax98;
            fun_26d0();
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8);
            goto addr_2e0d_25;
        }
    } else {
        addr_2ca5_114:
        *reinterpret_cast<uint32_t*>(&rdx21) = 1;
        *reinterpret_cast<int32_t*>(&rdx21 + 4) = 0;
        *reinterpret_cast<int32_t*>(&rax99) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax99) + 4) = 0;
        goto addr_2cd4_115;
    }
    addr_352d_116:
    rax100 = quote(r12_15, r12_15);
    r12_15 = rax100;
    fun_24e0();
    fun_26d0();
    addr_355f_84:
    quote(r12_15, r12_15);
    fun_24e0();
    fun_26d0();
    addr_34fb_81:
    rax101 = quote(r12_15, r12_15);
    r12_15 = rax101;
    fun_24e0();
    fun_26d0();
    goto addr_352d_116;
    addr_34c9_90:
    rax102 = quote(r12_15, r12_15);
    r12_15 = rax102;
    fun_24e0();
    fun_26d0();
    goto addr_34fb_81;
    addr_3473_94:
    rax103 = quote(r12_15, r12_15);
    r12_15 = rax103;
    fun_24e0();
    fun_26d0();
    fun_24e0();
    fun_26d0();
    goto addr_34c9_90;
    addr_344f_28:
    fun_24e0();
    fun_26d0();
    goto addr_3473_94;
    while (1) {
        addr_2cd4_115:
        *reinterpret_cast<uint32_t*>(&rcx20) = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<uint64_t>(rax99));
        *reinterpret_cast<int32_t*>(&rcx20 + 4) = 0;
        r15_16 = rdx21 + 0xffffffffffffffff;
        if (*reinterpret_cast<signed char*>(&rcx20) != 37) {
            if (!*reinterpret_cast<signed char*>(&rcx20)) 
                break;
            *reinterpret_cast<int32_t*>(&rcx104) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx104) + 4) = 0;
        } else {
            r13_14 = reinterpret_cast<void**>(&rax99->f1);
            if (*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_15) + reinterpret_cast<uint64_t>(rax99) + 1) != 37) 
                goto addr_2ed0_62;
            *reinterpret_cast<int32_t*>(&rcx104) = 2;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx104) + 4) = 0;
        }
        rax99 = reinterpret_cast<struct s5*>(reinterpret_cast<uint64_t>(rax99) + reinterpret_cast<uint64_t>(rcx104));
        ++rdx21;
    }
    quote(r12_15, r12_15);
    fun_24e0();
    fun_26d0();
    rdi105 = optarg;
    rax106 = quote(rdi105, rdi105);
    r12_15 = rax106;
    fun_24e0();
    fun_26d0();
    goto addr_3407_22;
    addr_315b_21:
    zf107 = grouping == 0;
    if (!zf107) {
        if (!r12_15) {
            goto addr_2e00_29;
        }
    } else {
        zf108 = padding_width == 0;
        if (!zf108) {
            if (!r12_15) {
                goto addr_2e0d_25;
            }
        } else {
            if (r12_15) 
                goto addr_2ca5_114;
            rax109 = fun_24e0();
            *reinterpret_cast<uint32_t*>(&rsi22) = 0;
            *reinterpret_cast<int32_t*>(&rsi22 + 4) = 0;
            rdx21 = rax109;
            fun_26d0();
            rsp25 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp25) - 8 + 8 - 8 + 8);
            r12_15 = format_str;
            goto addr_2c9c_19;
        }
    }
    addr_342b_23:
    fun_24e0();
    fun_26d0();
    goto addr_344f_28;
    addr_2910_13:
    goto static_cast<int64_t>(reinterpret_cast<int32_t>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(0xcc58) + reinterpret_cast<uint64_t>(rax28 * 4)))) + reinterpret_cast<unsigned char>(0xcc58);
}

int64_t __libc_start_main = 0;

void fun_35a3() {
    __asm__("cli ");
    __libc_start_main(0x2820, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0x11008;

void fun_23e0(int64_t rdi);

int64_t fun_3643() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_23e0(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_3683() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_26a0(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9, void** a7, void** a8, void** a9, void** a10, void** a11, void** a12, void** a13, void** a14, void** a15, void** a16, void** a17, void** a18, void** a19, void** a20, void** a21, void** a22, void** a23, void** a24, void** a25, void** a26, int64_t a27, void** a28, int64_t a29, void** a30, void** a31, void** a32, int64_t a33, void** a34, int64_t a35, void** a36);

void fun_51e3(int32_t edi) {
    void** v2;
    int64_t v3;
    int64_t r15_4;
    void** v5;
    void** r14_6;
    int64_t v7;
    int64_t r13_8;
    void** v9;
    void** r12_10;
    void** v11;
    void** rbp12;
    void** v13;
    void** rbx14;
    void** r12_15;
    void** rax16;
    void** v17;
    void** rax18;
    void** rcx19;
    void** r8_20;
    void** r9_21;
    void** v22;
    void** v23;
    void** v24;
    void** v25;
    int64_t v26;
    void** v27;
    int64_t v28;
    void** v29;
    void** r12_30;
    void** rax31;
    void** r12_32;
    void** rax33;
    void** r12_34;
    void** rax35;
    void** r12_36;
    void** rax37;
    void** r12_38;
    void** rax39;
    void** r12_40;
    void** rax41;
    void** r12_42;
    void** rax43;
    void** r12_44;
    void** rax45;
    void** r12_46;
    void** rax47;
    void** r12_48;
    void** rax49;
    void** r12_50;
    void** rax51;
    void** r12_52;
    void** rax53;
    void** r12_54;
    void** rax55;
    void** r12_56;
    void** rax57;
    void** r12_58;
    void** rax59;
    void** r12_60;
    void** rax61;
    void** r12_62;
    void** rax63;
    void** r12_64;
    void** rax65;
    void** r12_66;
    void** rax67;
    void** r12_68;
    void** rax69;
    void** r12_70;
    void** rax71;
    void** r12_72;
    void** rax73;
    void** r12_74;
    void** rax75;
    void** r12_76;
    void** rax77;
    void** r12_78;
    void** rax79;
    void** r12_80;
    void** rax81;
    void** r12_82;
    void** rax83;
    void** r12_84;
    void** rax85;
    void** v86;
    void** v87;
    void** v88;
    void** v89;
    int64_t v90;
    void** v91;
    int64_t v92;
    void** v93;
    void** r12_94;
    void** rax95;
    void** rdx96;
    int64_t v97;
    int64_t v98;
    int32_t eax99;
    void** r13_100;
    void** rax101;
    void** v102;
    void** v103;
    void** v104;
    void** v105;
    int64_t v106;
    void** v107;
    int64_t v108;
    void** v109;
    void** rax110;
    int32_t eax111;
    void** rax112;
    void** v113;
    void** v114;
    void** v115;
    void** v116;
    int64_t v117;
    void** v118;
    int64_t v119;
    void** v120;
    void** rax121;
    void** v122;
    void** v123;
    void** v124;
    void** v125;
    int64_t v126;
    void** v127;
    int64_t v128;
    void** v129;
    void** rax130;
    int32_t eax131;
    void** rax132;
    void** v133;
    void** v134;
    void** v135;
    void** v136;
    int64_t v137;
    void** v138;
    int64_t v139;
    void** v140;
    void** r15_141;
    void** rax142;
    void** rax143;
    void** v144;
    void** v145;
    void** v146;
    void** v147;
    int64_t v148;
    void** v149;
    int64_t v150;
    void** v151;
    void** rax152;
    void** rdi153;

    v2 = reinterpret_cast<void**>(__return_address());
    __asm__("cli ");
    v3 = r15_4;
    v5 = r14_6;
    v7 = r13_8;
    v9 = r12_10;
    v11 = rbp12;
    v13 = rbx14;
    r12_15 = program_name;
    rax16 = g28;
    v17 = rax16;
    if (!edi) {
        while (1) {
            rax18 = fun_24e0();
            fun_26a0(1, rax18, r12_15, rcx19, r8_20, r9_21, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0, v22, v17, v23, v13, v11, v9, v7, v5, v3, v2, v24, v25, v26, v27, v28, v29);
            r12_30 = stdout;
            rax31 = fun_24e0();
            fun_25c0(rax31, r12_30, 5, rax31, r12_30, 5);
            r12_32 = stdout;
            rax33 = fun_24e0();
            fun_25c0(rax33, r12_32, 5, rax33, r12_32, 5);
            r12_34 = stdout;
            rax35 = fun_24e0();
            fun_25c0(rax35, r12_34, 5, rax35, r12_34, 5);
            r12_36 = stdout;
            rax37 = fun_24e0();
            fun_25c0(rax37, r12_36, 5, rax37, r12_36, 5);
            r12_38 = stdout;
            rax39 = fun_24e0();
            fun_25c0(rax39, r12_38, 5, rax39, r12_38, 5);
            r12_40 = stdout;
            rax41 = fun_24e0();
            fun_25c0(rax41, r12_40, 5, rax41, r12_40, 5);
            r12_42 = stdout;
            rax43 = fun_24e0();
            fun_25c0(rax43, r12_42, 5, rax43, r12_42, 5);
            r12_44 = stdout;
            rax45 = fun_24e0();
            fun_25c0(rax45, r12_44, 5, rax45, r12_44, 5);
            r12_46 = stdout;
            rax47 = fun_24e0();
            fun_25c0(rax47, r12_46, 5, rax47, r12_46, 5);
            r12_48 = stdout;
            rax49 = fun_24e0();
            fun_25c0(rax49, r12_48, 5, rax49, r12_48, 5);
            r12_50 = stdout;
            rax51 = fun_24e0();
            fun_25c0(rax51, r12_50, 5, rax51, r12_50, 5);
            r12_52 = stdout;
            rax53 = fun_24e0();
            fun_25c0(rax53, r12_52, 5, rax53, r12_52, 5);
            r12_54 = stdout;
            rax55 = fun_24e0();
            fun_25c0(rax55, r12_54, 5, rax55, r12_54, 5);
            r12_56 = stdout;
            rax57 = fun_24e0();
            fun_25c0(rax57, r12_56, 5, rax57, r12_56, 5);
            r12_58 = stdout;
            rax59 = fun_24e0();
            fun_25c0(rax59, r12_58, 5, rax59, r12_58, 5);
            r12_60 = stdout;
            rax61 = fun_24e0();
            fun_25c0(rax61, r12_60, 5, rax61, r12_60, 5);
            r12_62 = stdout;
            rax63 = fun_24e0();
            fun_25c0(rax63, r12_62, 5, rax63, r12_62, 5);
            r12_64 = stdout;
            rax65 = fun_24e0();
            fun_25c0(rax65, r12_64, 5, rax65, r12_64, 5);
            r12_66 = stdout;
            rax67 = fun_24e0();
            fun_25c0(rax67, r12_66, 5, rax67, r12_66, 5);
            r12_68 = stdout;
            rax69 = fun_24e0();
            fun_25c0(rax69, r12_68, 5, rax69, r12_68, 5);
            r12_70 = stdout;
            rax71 = fun_24e0();
            fun_25c0(rax71, r12_70, 5, rax71, r12_70, 5);
            r12_72 = stdout;
            rax73 = fun_24e0();
            fun_25c0(rax73, r12_72, 5, rax73, r12_72, 5);
            r12_74 = stdout;
            rax75 = fun_24e0();
            fun_25c0(rax75, r12_74, 5, rax75, r12_74, 5);
            r12_76 = stdout;
            rax77 = fun_24e0();
            fun_25c0(rax77, r12_76, 5, rax77, r12_76, 5);
            r12_78 = stdout;
            rax79 = fun_24e0();
            fun_25c0(rax79, r12_78, 5, rax79, r12_78, 5);
            r12_80 = stdout;
            rax81 = fun_24e0();
            fun_25c0(rax81, r12_80, 5, rax81, r12_80, 5);
            r12_82 = stdout;
            rax83 = fun_24e0();
            fun_25c0(rax83, r12_82, 5, rax83, r12_82, 5);
            r12_84 = program_name;
            rax85 = fun_24e0();
            fun_26a0(1, rax85, r12_84, rcx19, r8_20, r9_21, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0, v86, v17, v87, v13, v11, v9, v7, v5, v3, v2, v88, v89, v90, v91, v92, v93);
            r12_94 = program_name;
            rax95 = fun_24e0();
            r9_21 = r12_94;
            r8_20 = r12_94;
            rdx96 = r12_94;
            fun_26a0(1, rax95, rdx96, r12_94, r8_20, r9_21, r12_94, r12_94, r12_94, r12_94, r12_94, rax95, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0, v97, v17, v98, v13, v11, v9, v7, v5, v3, v2);
            do {
                if (1) 
                    break;
                eax99 = fun_25e0("numfmt", 0, rdx96);
            } while (eax99);
            r13_100 = v17;
            if (!r13_100) {
                rax101 = fun_24e0();
                fun_26a0(1, rax101, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_20, r9_21, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0, v102, v17, v103, v13, v11, v9, v7, v5, v3, v2, v104, v105, v106, v107, v108, v109);
                rax110 = fun_2690(5);
                if (!rax110 || (eax111 = fun_2430(rax110, "en_", 3, rax110, "en_", 3), !eax111)) {
                    rax112 = fun_24e0();
                    r13_100 = reinterpret_cast<void**>("numfmt");
                    fun_26a0(1, rax112, "https://www.gnu.org/software/coreutils/", "numfmt", r8_20, r9_21, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0, v113, v17, v114, v13, v11, v9, v7, v5, v3, v2, v115, v116, v117, v118, v119, v120);
                    r12_15 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_100 = reinterpret_cast<void**>("numfmt");
                    goto addr_58e0_9;
                }
            } else {
                rax121 = fun_24e0();
                fun_26a0(1, rax121, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_20, r9_21, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0, v122, v17, v123, v13, v11, v9, v7, v5, v3, v2, v124, v125, v126, v127, v128, v129);
                rax130 = fun_2690(5);
                if (!rax130 || (eax131 = fun_2430(rax130, "en_", 3, rax130, "en_", 3), !eax131)) {
                    addr_57e6_11:
                    rax132 = fun_24e0();
                    fun_26a0(1, rax132, "https://www.gnu.org/software/coreutils/", "numfmt", r8_20, r9_21, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0, v133, v17, v134, v13, v11, v9, v7, v5, v3, v2, v135, v136, v137, v138, v139, v140);
                    r12_15 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_100 == "numfmt")) {
                        r12_15 = reinterpret_cast<void**>(0xb0d6);
                    }
                } else {
                    addr_58e0_9:
                    r15_141 = stdout;
                    rax142 = fun_24e0();
                    fun_25c0(rax142, r15_141, 5, rax142, r15_141, 5);
                    goto addr_57e6_11;
                }
            }
            rax143 = fun_24e0();
            rcx19 = r12_15;
            fun_26a0(1, rax143, r13_100, rcx19, r8_20, r9_21, "[", "test invocation", "coreutils", "Multi-call invocation", "sha224sum", "sha2 utilities", "sha256sum", "sha2 utilities", "sha384sum", "sha2 utilities", "sha512sum", "sha2 utilities", 0, 0, v144, v17, v145, v13, v11, v9, v7, v5, v3, v2, v146, v147, v148, v149, v150, v151);
            addr_523e_14:
            fun_2720();
        }
    } else {
        rax152 = fun_24e0();
        rdi153 = stderr;
        rcx19 = r12_15;
        fun_2740(rdi153, 1, rax152, rdi153, 1, rax152);
        goto addr_523e_14;
    }
}

int64_t fun_5913(uint64_t* rdi, uint64_t* rsi) {
    uint32_t eax3;
    int64_t rax4;

    __asm__("cli ");
    eax3 = reinterpret_cast<uint1_t>(*rdi > *rsi);
    *reinterpret_cast<uint32_t*>(&rax4) = eax3 - reinterpret_cast<uint1_t>(eax3 < static_cast<uint32_t>(reinterpret_cast<uint1_t>(*rdi < *rsi)));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    return rax4;
}

struct s6 {
    signed char f0;
    signed char f1;
};

/* num_start.0 */
void** num_start_0 = reinterpret_cast<void**>(0);

void fun_2470(void** rdi);

struct s7 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s8 {
    signed char[1] pad1;
    void** f1;
};

struct s9 {
    signed char[8] pad8;
    uint64_t f8;
};

struct s10 {
    uint64_t f0;
    uint64_t f8;
};

void fun_26c0();

struct s11 {
    int64_t f0;
    int64_t f8;
};

void fun_59b3(struct s6* rdi, uint32_t esi) {
    struct s6* rbx3;
    uint32_t eax4;
    uint32_t v5;
    uint32_t v6;
    void** rdx7;
    void** rdi8;
    signed char* rbx9;
    uint32_t esi10;
    int32_t r13d11;
    uint32_t r15d12;
    void** r12_13;
    uint32_t ebp14;
    void** r14_15;
    void** rax16;
    int64_t rax17;
    int64_t rbp18;
    int1_t zf19;
    void** rcx20;
    int1_t zf21;
    void** rax22;
    void** rax23;
    void** rax24;
    void** rbp25;
    void** r8_26;
    void** r9_27;
    void** rax28;
    void** rax29;
    void** rax30;
    void** rax31;
    uint64_t rsi32;
    void** rax33;
    void** rdi34;
    uint64_t r14_35;
    void* rbp36;
    uint64_t rbx37;
    void** r15_38;
    void** rax39;
    void** rsi40;
    struct s7* rbp41;
    uint64_t r12_42;
    void** rdi43;
    void** rsi44;
    void** rdi45;
    void* r13_46;
    void* r12_47;
    struct s9* rsi48;
    struct s10* rdi49;
    uint64_t rax50;
    uint64_t rax51;
    uint64_t rax52;
    void** rax53;
    uint64_t rdx54;
    struct s11* rax55;
    void** rax56;
    void** rax57;
    void** rax58;
    void** rax59;

    __asm__("cli ");
    rbx3 = rdi;
    eax4 = esi & 1;
    v5 = esi;
    v6 = eax4;
    if (!eax4 || (rdi->f0 != 45 || rdi->f1)) {
        *reinterpret_cast<uint32_t*>(&rdx7) = 0;
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    } else {
        rbx3 = reinterpret_cast<struct s6*>(&rbx3->f1);
        *reinterpret_cast<uint32_t*>(&rdx7) = 1;
        *reinterpret_cast<int32_t*>(&rdi8) = 1;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    }
    rbx9 = &rbx3->f1;
    esi10 = *reinterpret_cast<uint32_t*>(&rdx7);
    r13d11 = 0;
    r15d12 = 0;
    *reinterpret_cast<int32_t*>(&r12_13) = 1;
    *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
    while (1) {
        ebp14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx9 + 0xffffffffffffffff));
        r14_15 = reinterpret_cast<void**>(rbx9 + 0xffffffffffffffff);
        if (*reinterpret_cast<unsigned char*>(&ebp14) != 45) {
            if (*reinterpret_cast<unsigned char*>(&ebp14) == 44 || ((rax16 = fun_2790(rdi8), esi10 = *reinterpret_cast<unsigned char*>(&esi10), *reinterpret_cast<uint32_t*>(&rdx7) = *reinterpret_cast<unsigned char*>(&rdx7), *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0, *reinterpret_cast<uint32_t*>(&rax17) = *reinterpret_cast<unsigned char*>(&ebp14), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0, rdi8 = rdi8, !!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax16)) + reinterpret_cast<uint64_t>(rax17 * 2))) & 1)) || !*reinterpret_cast<unsigned char*>(&ebp14))) {
                if (!*reinterpret_cast<unsigned char*>(&esi10)) {
                    if (!rdi8) 
                        break;
                    add_range_pair(rdi8, rdi8);
                    if (!*reinterpret_cast<void***>(rbx9 + 0xffffffffffffffff)) 
                        goto addr_5ae7_10;
                } else {
                    if (!*reinterpret_cast<unsigned char*>(&rdx7)) {
                        if (*reinterpret_cast<signed char*>(&r15d12)) 
                            goto addr_5c19_13;
                        if (!v6) 
                            goto addr_5db1_15;
                        *reinterpret_cast<int32_t*>(&r12_13) = 1;
                        *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
                        goto addr_5a6e_17;
                    }
                    if (!*reinterpret_cast<signed char*>(&r15d12)) {
                        addr_5a6e_17:
                        add_range_pair(r12_13, 0xffffffffffffffff);
                        goto addr_5a7d_19;
                    } else {
                        addr_5c19_13:
                        if (reinterpret_cast<unsigned char>(r12_13) > reinterpret_cast<unsigned char>(rdi8)) 
                            goto addr_5e4c_20; else 
                            goto addr_5c22_21;
                    }
                }
            } else {
                *reinterpret_cast<int32_t*>(&rbp18) = *reinterpret_cast<signed char*>(&ebp14);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp18) + 4) = 0;
                if (static_cast<uint32_t>(rbp18 - 48) > 9) 
                    goto addr_5f28_23;
                if (!*reinterpret_cast<signed char*>(&r13d11)) 
                    goto addr_5cc8_25;
                zf19 = num_start_0 == 0;
                if (zf19) 
                    goto addr_5cc8_25; else 
                    goto addr_5c6f_27;
            }
        } else {
            if (*reinterpret_cast<unsigned char*>(&esi10)) 
                goto addr_5e7f_29;
            *reinterpret_cast<unsigned char*>(&rcx20) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(rdi8 == 0)) & *reinterpret_cast<unsigned char*>(&rdx7));
            r13d11 = *reinterpret_cast<int32_t*>(&rcx20);
            if (*reinterpret_cast<unsigned char*>(&rcx20)) 
                goto addr_5e5d_31;
            if (!*reinterpret_cast<unsigned char*>(&rdx7)) 
                goto addr_5c38_33; else 
                goto addr_5ac7_34;
        }
        addr_5a83_35:
        r13d11 = 0;
        esi10 = 0;
        r15d12 = 0;
        *reinterpret_cast<uint32_t*>(&rdx7) = 0;
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
        addr_5a8f_36:
        ++rbx9;
        continue;
        addr_5a7d_19:
        if (!*reinterpret_cast<void***>(rbx9 + 0xffffffffffffffff)) 
            goto addr_5ae7_10; else 
            goto addr_5a83_35;
        addr_5c22_21:
        add_range_pair(r12_13, rdi8);
        goto addr_5a7d_19;
        addr_5cc8_25:
        num_start_0 = r14_15;
        addr_5c6f_27:
        zf21 = *reinterpret_cast<unsigned char*>(&esi10) == 0;
        if (zf21) {
            *reinterpret_cast<uint32_t*>(&rdx7) = 1;
            *reinterpret_cast<int32_t*>(&rdx7 + 4) = 0;
        }
        if (!zf21) {
            r15d12 = esi10;
        }
        if (reinterpret_cast<unsigned char>(rdi8) > reinterpret_cast<unsigned char>(0x1999999999999999)) 
            goto addr_5ddc_41;
        rax22 = reinterpret_cast<void**>(*reinterpret_cast<int32_t*>(&rbp18) - 48 + reinterpret_cast<uint64_t>(rdi8 + reinterpret_cast<unsigned char>(rdi8) * 4) * 2);
        if (reinterpret_cast<unsigned char>(rax22) < reinterpret_cast<unsigned char>(rdi8)) 
            goto addr_5ddc_41;
        if (rax22 == 0xffffffffffffffff) 
            goto addr_5ddc_41;
        rdi8 = rax22;
        r13d11 = 1;
        goto addr_5a8f_36;
        addr_5c38_33:
        esi10 = 1;
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r12_13) = 1;
        *reinterpret_cast<int32_t*>(&r12_13 + 4) = 0;
        goto addr_5a8f_36;
        addr_5ac7_34:
        r12_13 = rdi8;
        esi10 = *reinterpret_cast<uint32_t*>(&rdx7);
        *reinterpret_cast<int32_t*>(&rdi8) = 0;
        *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
        goto addr_5a8f_36;
    }
    if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
        addr_5ea1_46:
        rax23 = fun_24e0();
        rdx7 = rax23;
    } else {
        rax24 = fun_24e0();
        rdx7 = rax24;
    }
    while (1) {
        addr_5dc7_48:
        fun_26d0();
        usage();
        addr_5ddc_41:
        rbp25 = num_start_0;
        rax28 = fun_25a0(rbp25, "0123456789", rdx7, rcx20, r8_26, r9_27);
        rax29 = ximemdup0(rbp25, rax28, rbp25, rax28);
        rax30 = quote(rax29, rax29);
        if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
            fun_24e0();
        } else {
            fun_24e0();
        }
        rcx20 = rax30;
        fun_26d0();
        fun_2400(rax29);
        usage();
        addr_5e4c_20:
        addr_5dbd_52:
        rax31 = fun_24e0();
        rdx7 = rax31;
    }
    addr_5ae7_10:
    rsi32 = n_frp;
    if (!rsi32) {
        if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
            addr_5f68_54:
            goto addr_5dbd_52;
        } else {
            rax33 = fun_24e0();
            rdx7 = rax33;
            goto addr_5dc7_48;
        }
    } else {
        rdi34 = frp;
        *reinterpret_cast<int32_t*>(&r14_35) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        *reinterpret_cast<int32_t*>(&rbp36) = 16;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp36) + 4) = 0;
        fun_2470(rdi34);
        rbx37 = n_frp;
        r15_38 = frp;
        if (!rbx37) {
            addr_5bb0_57:
            if (*reinterpret_cast<unsigned char*>(&v5) & 2) {
                frp = reinterpret_cast<void**>(0);
                rax39 = *reinterpret_cast<void***>(r15_38);
                n_frp = 0;
                n_frp_allocated = 0;
                if (reinterpret_cast<unsigned char>(rax39) > reinterpret_cast<unsigned char>(1)) {
                    rsi40 = rax39 + 0xffffffffffffffff;
                    add_range_pair(1, rsi40, 1, rsi40);
                }
                rbp41 = reinterpret_cast<struct s7*>(r15_38 + 8);
                *reinterpret_cast<int32_t*>(&r12_42) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_42) + 4) = 0;
                if (rbx37 > 1) {
                    do {
                        rdi43 = rbp41->f0 + 1;
                        if (rdi43 != rbp41->f8) {
                            rsi44 = rbp41->f8 - 1;
                            add_range_pair(rdi43, rsi44, rdi43, rsi44);
                        }
                        ++r12_42;
                        rbp41 = reinterpret_cast<struct s7*>(reinterpret_cast<uint64_t>(rbp41) + 16);
                    } while (r12_42 != rbx37);
                }
                if (!reinterpret_cast<int1_t>(*reinterpret_cast<struct s8**>(reinterpret_cast<unsigned char>(r15_38) + (rbx37 << 4) - 8) == -1)) {
                    rdi45 = reinterpret_cast<void**>(&(*reinterpret_cast<struct s8**>(reinterpret_cast<unsigned char>(r15_38) + (rbx37 << 4) - 8))->f1);
                    add_range_pair(rdi45, 0xffffffffffffffff, rdi45, 0xffffffffffffffff);
                    fun_2400(r15_38, r15_38);
                    r15_38 = frp;
                } else {
                    fun_2400(r15_38, r15_38);
                    r15_38 = frp;
                }
            }
        } else {
            do {
                ++r14_35;
                if (r14_35 >= rbx37) 
                    goto addr_5bb0_57;
                r13_46 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp36) - 16);
                r12_47 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbp36) + 16);
                do {
                    rsi48 = reinterpret_cast<struct s9*>(reinterpret_cast<unsigned char>(r15_38) + reinterpret_cast<uint64_t>(r13_46));
                    rdi49 = reinterpret_cast<struct s10*>(reinterpret_cast<unsigned char>(r15_38) + reinterpret_cast<uint64_t>(rbp36));
                    if (rdi49->f0 > rsi48->f8) 
                        break;
                    rax50 = rdi49->f8;
                    if (rax50 < rsi48->f8) {
                        rax50 = rsi48->f8;
                    }
                    rsi48->f8 = rax50;
                    fun_26c0();
                    rax51 = n_frp;
                    r15_38 = frp;
                    rbx37 = rax51 - 1;
                    n_frp = rbx37;
                } while (rbx37 > r14_35);
                goto addr_5bb0_57;
                rbx37 = n_frp;
                rbp36 = r12_47;
            } while (r14_35 < rbx37);
            goto addr_5ba9_76;
        }
    }
    rax52 = n_frp;
    n_frp = rax52 + 1;
    rax53 = xrealloc(r15_38, r15_38);
    rdx54 = n_frp;
    frp = rax53;
    rax55 = reinterpret_cast<struct s11*>(reinterpret_cast<unsigned char>(rax53) + (rdx54 << 4) - 16);
    rax55->f8 = -1;
    rax55->f0 = -1;
    return;
    addr_5ba9_76:
    goto addr_5bb0_57;
    addr_5db1_15:
    goto addr_5dbd_52;
    addr_5f28_23:
    rax56 = quote(r14_15);
    if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
        fun_24e0();
    } else {
        fun_24e0();
    }
    rcx20 = rax56;
    fun_26d0();
    usage();
    goto addr_5f68_54;
    addr_5e7f_29:
    if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) {
        rax57 = fun_24e0();
        rdx7 = rax57;
        goto addr_5dc7_48;
    } else {
        rax58 = fun_24e0();
        rdx7 = rax58;
        goto addr_5dc7_48;
    }
    addr_5e5d_31:
    if (!(*reinterpret_cast<unsigned char*>(&v5) & 4)) 
        goto addr_5ea1_46;
    rax59 = fun_24e0();
    rdx7 = rax59;
    goto addr_5dc7_48;
}

void fun_5f93() {
    __asm__("cli ");
    goto usage;
}

uint32_t fun_25b0(void** rdi, void** rsi, void** rdx, ...);

int64_t fun_5fa3(void** rdi, void*** rsi, void** rdx, void** rcx) {
    void** r14_5;
    void** r13_6;
    void** rbp7;
    void*** v8;
    void** v9;
    void** rax10;
    void** r15_11;
    int64_t v12;
    unsigned char v13;
    void** r12_14;
    int64_t rbx15;
    int64_t v16;
    int32_t eax17;
    void** rax18;
    uint32_t eax19;
    uint32_t eax20;
    int64_t rax21;

    __asm__("cli ");
    r14_5 = rdi;
    r13_6 = rcx;
    rbp7 = rdx;
    v8 = rsi;
    v9 = rdx;
    rax10 = fun_2500(rdi);
    r15_11 = *rsi;
    if (!r15_11) {
        v12 = -1;
    } else {
        v13 = 0;
        r12_14 = rax10;
        *reinterpret_cast<int32_t*>(&rbx15) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx15) + 4) = 0;
        v16 = -1;
        while (1) {
            eax17 = fun_2430(r15_11, r14_5, r12_14);
            if (eax17) {
                addr_6023_5:
                ++rbx15;
                rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp7) + reinterpret_cast<unsigned char>(r13_6));
                r15_11 = v8[rbx15 * 8];
                if (!r15_11) 
                    goto addr_6070_6; else 
                    continue;
            } else {
                rax18 = fun_2500(r15_11, r15_11);
                if (rax18 == r12_14) 
                    goto addr_60a0_8;
                if (v16 == -1) 
                    goto addr_605e_10;
            }
            if (!v9) {
                v13 = 1;
                goto addr_6023_5;
            } else {
                eax19 = fun_25b0(reinterpret_cast<uint64_t>(v16 * reinterpret_cast<unsigned char>(r13_6)) + reinterpret_cast<unsigned char>(v9), rbp7, r13_6);
                eax20 = v13;
                if (eax19) {
                    eax20 = 1;
                }
                v13 = *reinterpret_cast<unsigned char*>(&eax20);
                goto addr_6023_5;
            }
            addr_605e_10:
            v16 = rbx15;
            goto addr_6023_5;
        }
    }
    addr_6085_16:
    return v12;
    addr_6070_6:
    rax21 = -2;
    if (!v13) {
        rax21 = v16;
    }
    v12 = rax21;
    goto addr_6085_16;
    addr_60a0_8:
    v12 = rbx15;
    goto addr_6085_16;
}

int64_t fun_60b3(signed char* rdi, void*** rsi, void** rdx) {
    signed char* r12_4;
    void** rdi5;
    void*** rbp6;
    int64_t rbx7;
    int32_t eax8;

    __asm__("cli ");
    r12_4 = rdi;
    rdi5 = *rsi;
    if (!rdi5) {
        addr_60f8_2:
        return -1;
    } else {
        rbp6 = rsi;
        *reinterpret_cast<int32_t*>(&rbx7) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx7) + 4) = 0;
        do {
            eax8 = fun_25e0(rdi5, r12_4, rdx);
            if (!eax8) 
                break;
            ++rbx7;
            rdi5 = rbp6[rbx7 * 8];
        } while (rdi5);
        goto addr_60f8_2;
    }
    return rbx7;
}

int64_t quotearg_n_style();

void fun_6113(int64_t rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (rdx == -1) {
        fun_24e0();
    } else {
        fun_24e0();
    }
    quote_n();
    quotearg_n_style();
    goto fun_26d0;
}

void fun_61a3(void*** rdi, void** rsi, void** rdx) {
    void** r13_4;
    void** r12_5;
    void** rbp6;
    void** r14_7;
    void*** v8;
    void** rax9;
    void** r15_10;
    int64_t rbx11;
    uint32_t eax12;
    void** rdi13;
    void** rdi14;
    void** rdi15;
    signed char* rax16;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r13_4) = 0;
    *reinterpret_cast<int32_t*>(&r13_4 + 4) = 0;
    r12_5 = rdx;
    rbp6 = rsi;
    r14_7 = stderr;
    v8 = rdi;
    rax9 = fun_24e0();
    fun_25c0(rax9, r14_7, 5);
    r15_10 = *rdi;
    *reinterpret_cast<int32_t*>(&rbx11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx11) + 4) = 0;
    if (r15_10) {
        do {
            if (!rbx11 || (eax12 = fun_25b0(r13_4, rbp6, r12_5, r13_4, rbp6, r12_5), !!eax12)) {
                r13_4 = rbp6;
                quote(r15_10, r15_10);
                rdi13 = stderr;
                fun_2740(rdi13, 1, "\n  - %s", rdi13, 1, "\n  - %s");
            } else {
                quote(r15_10, r15_10);
                rdi14 = stderr;
                fun_2740(rdi14, 1, ", %s", rdi14, 1, ", %s");
            }
            ++rbx11;
            rbp6 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp6) + reinterpret_cast<unsigned char>(r12_5));
            r15_10 = v8[rbx11 * 8];
        } while (r15_10);
    }
    rdi15 = stderr;
    rax16 = *reinterpret_cast<signed char**>(rdi15 + 40);
    if (reinterpret_cast<uint64_t>(rax16) >= reinterpret_cast<uint64_t>(*reinterpret_cast<signed char**>(rdi15 + 48))) {
        goto fun_2550;
    } else {
        *reinterpret_cast<signed char**>(rdi15 + 40) = rax16 + 1;
        *rax16 = 10;
        return;
    }
}

int64_t argmatch(signed char* rdi, void** rsi, int64_t rdx, int64_t rcx);

void argmatch_invalid(int64_t rdi, signed char* rsi, int64_t rdx, int64_t rcx);

void argmatch_valid(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx);

int64_t fun_62d3(int64_t rdi, signed char* rsi, void** rdx, int64_t rcx, int64_t r8, int64_t r9, signed char a7) {
    int64_t r15_8;
    signed char* r14_9;
    int64_t r13_10;
    int64_t r12_11;
    void** rbp12;
    int64_t v13;
    int64_t rax14;
    void** rdi15;
    int64_t rbx16;
    int32_t eax17;

    __asm__("cli ");
    r15_8 = rdi;
    r14_9 = rsi;
    r13_10 = r8;
    r12_11 = rcx;
    rbp12 = rdx;
    v13 = r9;
    if (a7) {
        rcx = r8;
        rax14 = argmatch(r14_9, rbp12, r12_11, rcx);
        if (rax14 < 0) {
            addr_6317_3:
            argmatch_invalid(r15_8, r14_9, rax14, rcx);
            argmatch_valid(rbp12, r12_11, r13_10, rcx);
            v13(rbp12, r12_11, r13_10, rcx);
            rax14 = -1;
            goto addr_638e_4;
        } else {
            addr_638e_4:
            return rax14;
        }
    }
    rdi15 = *reinterpret_cast<void***>(rdx);
    *reinterpret_cast<int32_t*>(&rbx16) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx16) + 4) = 0;
    if (rdi15) {
        do {
            eax17 = fun_25e0(rdi15, r14_9, rdx);
            if (!eax17) 
                break;
            ++rbx16;
            rdi15 = *reinterpret_cast<void***>(rbp12 + rbx16 * 8);
        } while (rdi15);
        goto addr_6310_8;
    } else {
        goto addr_6310_8;
    }
    return rbx16;
    addr_6310_8:
    rax14 = -1;
    goto addr_6317_3;
}

struct s12 {
    int64_t f0;
    int64_t f8;
};

int64_t fun_63a3(void** rdi, struct s12* rsi, void** rdx, void** rcx) {
    int64_t r14_5;
    void** r12_6;
    void** r13_7;
    int64_t* rbx8;
    void** rbp9;
    uint32_t eax10;

    __asm__("cli ");
    r14_5 = rsi->f0;
    if (r14_5) {
        r12_6 = rdi;
        r13_7 = rcx;
        rbx8 = &rsi->f8;
        rbp9 = rdx;
        do {
            eax10 = fun_25b0(r12_6, rbp9, r13_7);
            if (!eax10) 
                break;
            r14_5 = *rbx8;
            rbp9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp9) + reinterpret_cast<unsigned char>(r13_7));
            ++rbx8;
        } while (r14_5);
    }
    return r14_5;
}

int64_t file_name = 0;

void fun_6403(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_6413(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_2440(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_6423() {
    void** rdi1;
    int32_t eax2;
    void** rax3;
    int1_t zf4;
    void** rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2420(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax3) == 32)) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_24e0();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_64b3_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_26d0();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_2440(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_64b3_5:
        *reinterpret_cast<void***>(&rsi11) = *reinterpret_cast<void***>(rbx5);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_26d0();
    }
}

struct s13 {
    signed char[1] pad1;
    void** f1;
};

struct s13* fun_24a0();

void** fun_2640(void** rdi, void** rsi, ...);

int32_t fun_2490(void** rdi, void** rsi, void** rdx);

int32_t fun_2630();

signed char* fun_26b0();

void** fun_2710();

int32_t fun_2770();

void** fun_64d3(void** rdi, signed char* rsi, void* rdx, void*** rcx, int32_t r8d, uint32_t r9d) {
    uint32_t ebp7;
    signed char* rbx8;
    void* v9;
    void*** v10;
    int32_t v11;
    void** rax12;
    void** v13;
    void** r9_14;
    uint64_t rax15;
    void** r12_16;
    void** r15_17;
    void** r14_18;
    struct s13* rax19;
    void** r8_20;
    void** rax21;
    void** rax22;
    void** r8_23;
    void** rdx24;
    struct s13* rax25;
    void** r8_26;
    void** rsi27;
    int32_t eax28;
    void** rax29;
    void** v30;
    void** rax31;
    void* rdx32;
    int32_t eax33;
    void** edi34;
    void** r13_35;
    void** v36;
    int32_t eax37;
    void** rax38;
    void* r12_39;
    void* rdx40;
    signed char* rbp41;
    signed char* rdi42;
    signed char* rax43;
    signed char* rdx44;
    void** rax45;
    void** r12_46;
    int32_t eax47;
    int32_t eax48;
    void** rax49;

    __asm__("cli ");
    ebp7 = r9d;
    rbx8 = rsi;
    v9 = rdx;
    v10 = rcx;
    v11 = r8d;
    rax12 = fun_2500(rdi);
    v13 = rax12;
    r9_14 = rax12;
    if (*reinterpret_cast<unsigned char*>(&ebp7) & 2 || (rax15 = fun_24f0(), r9_14 = rax12, rax15 <= 1)) {
        addr_6511_2:
        r12_16 = r9_14;
        *reinterpret_cast<int32_t*>(&r15_17) = 0;
        *reinterpret_cast<int32_t*>(&r15_17 + 4) = 0;
        *reinterpret_cast<int32_t*>(&r14_18) = 0;
        *reinterpret_cast<int32_t*>(&r14_18 + 4) = 0;
        goto addr_651a_3;
    } else {
        rax19 = fun_24a0();
        r9_14 = rax12;
        if (!reinterpret_cast<int1_t>(rax19 == -1)) {
            r8_20 = reinterpret_cast<void**>(&rax19->f1);
            rax21 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_20) * 4);
            rax22 = fun_2640(rax21, rdi);
            r9_14 = r9_14;
            r15_17 = rax22;
            if (!rax22) {
                if (!(*reinterpret_cast<unsigned char*>(&ebp7) & 1)) {
                    addr_66aa_7:
                    *reinterpret_cast<int32_t*>(&r15_17) = 0;
                    *reinterpret_cast<int32_t*>(&r15_17 + 4) = 0;
                    *reinterpret_cast<int32_t*>(&r14_18) = 0;
                    *reinterpret_cast<int32_t*>(&r14_18 + 4) = 0;
                    r8_23 = reinterpret_cast<void**>(0xffffffffffffffff);
                } else {
                    r12_16 = r9_14;
                    *reinterpret_cast<int32_t*>(&r14_18) = 0;
                    *reinterpret_cast<int32_t*>(&r14_18 + 4) = 0;
                    goto addr_651a_3;
                }
            } else {
                rdx24 = r8_20;
                *reinterpret_cast<int32_t*>(&r14_18) = 0;
                *reinterpret_cast<int32_t*>(&r14_18 + 4) = 0;
                rax25 = fun_24a0();
                r9_14 = r9_14;
                r8_26 = r8_20;
                r12_16 = r9_14;
                if (!rax25) 
                    goto addr_651a_3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<unsigned char>(r15_17) + reinterpret_cast<unsigned char>(rax21) + 0xfffffffffffffffc) = 0;
                if (!*reinterpret_cast<void***>(r15_17)) 
                    goto addr_686b_11; else 
                    goto addr_6743_12;
            }
        } else {
            if (*reinterpret_cast<unsigned char*>(&ebp7) & 1) 
                goto addr_6511_2; else 
                goto addr_66aa_7;
        }
    }
    addr_65fb_14:
    fun_2400(r15_17, r15_17);
    fun_2400(r14_18, r14_18);
    return r8_23;
    addr_686b_11:
    rsi27 = r8_26;
    eax28 = fun_2490(r15_17, rsi27, rdx24);
    r9_14 = r9_14;
    r12_16 = reinterpret_cast<void**>(static_cast<int64_t>(eax28));
    addr_6883_15:
    rax29 = *v10;
    if (reinterpret_cast<unsigned char>(rax29) >= reinterpret_cast<unsigned char>(r12_16)) {
        *reinterpret_cast<int32_t*>(&r14_18) = 0;
        *reinterpret_cast<int32_t*>(&r14_18 + 4) = 0;
        goto addr_662b_17;
    } else {
        v30 = v13 + 1;
    }
    addr_67c8_19:
    rax31 = fun_2640(v30, rsi27, v30, rsi27);
    r9_14 = r9_14;
    r14_18 = rax31;
    if (!rax31) {
        r8_23 = reinterpret_cast<void**>(0xffffffffffffffff);
        if (*reinterpret_cast<unsigned char*>(&ebp7) & 1) {
            addr_651a_3:
            rax29 = *v10;
            if (reinterpret_cast<unsigned char>(rax29) >= reinterpret_cast<unsigned char>(r12_16)) {
                addr_662b_17:
                if (reinterpret_cast<unsigned char>(r12_16) >= reinterpret_cast<unsigned char>(rax29)) {
                    rax29 = r12_16;
                    *reinterpret_cast<uint32_t*>(&rdx32) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
                } else {
                    rdx32 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax29) - reinterpret_cast<unsigned char>(r12_16));
                    *v10 = r12_16;
                    eax33 = v11;
                    if (eax33) 
                        goto addr_6544_23; else 
                        goto addr_6651_24;
                }
            } else {
                r9_14 = rax29;
                *reinterpret_cast<uint32_t*>(&rdx32) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
            }
        } else {
            goto addr_65fb_14;
        }
    } else {
        edi34 = *reinterpret_cast<void***>(r15_17);
        r13_35 = r15_17;
        *reinterpret_cast<int32_t*>(&r12_16) = 0;
        *reinterpret_cast<int32_t*>(&r12_16 + 4) = 0;
        v36 = *v10;
        if (edi34) {
            do {
                eax37 = fun_2630();
                if (eax37 != -1) {
                    rax38 = reinterpret_cast<void**>(static_cast<int64_t>(eax37) + reinterpret_cast<unsigned char>(r12_16));
                    if (reinterpret_cast<unsigned char>(v36) < reinterpret_cast<unsigned char>(rax38)) 
                        goto addr_6848_30;
                } else {
                    *reinterpret_cast<void***>(r13_35) = reinterpret_cast<void**>(0xfffd);
                    rax38 = r12_16 + 1;
                    if (reinterpret_cast<unsigned char>(v36) < reinterpret_cast<unsigned char>(rax38)) 
                        goto addr_6844_32;
                }
                r13_35 = r13_35 + 4;
                r12_16 = rax38;
            } while (*reinterpret_cast<void***>(r13_35 + 4));
            goto addr_6848_30;
        } else {
            goto addr_6848_30;
        }
    }
    *v10 = rax29;
    eax33 = v11;
    if (!eax33) {
        addr_6651_24:
        r12_39 = rdx32;
        *reinterpret_cast<uint32_t*>(&rdx32) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
    } else {
        addr_6544_23:
        *reinterpret_cast<int32_t*>(&r12_39) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_39) + 4) = 0;
        if (eax33 != 1) {
            *reinterpret_cast<uint32_t*>(&rdx40) = *reinterpret_cast<uint32_t*>(&rdx32) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx40) + 4) = 0;
            r12_39 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx32) >> 1);
            rdx32 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rdx40) + reinterpret_cast<uint64_t>(r12_39));
        }
    }
    r8_23 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdx32) + reinterpret_cast<unsigned char>(r9_14));
    if (*reinterpret_cast<unsigned char*>(&ebp7) & 4) {
        r8_23 = r9_14;
        *reinterpret_cast<uint32_t*>(&rdx32) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx32) + 4) = 0;
    }
    if (ebp7 & 8) {
        *reinterpret_cast<int32_t*>(&r12_39) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_39) + 4) = 0;
    } else {
        r8_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8_23) + reinterpret_cast<uint64_t>(r12_39));
    }
    if (v9) {
        rbp41 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(rbx8) + reinterpret_cast<uint64_t>(v9) + 0xffffffffffffffff);
        rdi42 = rbx8;
        if (reinterpret_cast<uint64_t>(rbx8) < reinterpret_cast<uint64_t>(rbp41)) {
            if (rdx32) {
                do {
                    ++rdi42;
                    rdi42[0xffffffffffffffff] = 32;
                    if (!(reinterpret_cast<uint64_t>(rbx8) - reinterpret_cast<uint64_t>(rdi42) + reinterpret_cast<uint64_t>(rdx32))) 
                        break;
                } while (reinterpret_cast<uint64_t>(rbp41) > reinterpret_cast<uint64_t>(rdi42));
            }
        }
        if (reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(rbp41) - reinterpret_cast<uint64_t>(rdi42)) > reinterpret_cast<unsigned char>(r9_14)) {
        }
        rax43 = fun_26b0();
        r8_23 = r8_23;
        rdx44 = rax43;
        if (reinterpret_cast<uint64_t>(rbp41) > reinterpret_cast<uint64_t>(rax43)) {
            if (r12_39) {
                do {
                    ++rdx44;
                    rdx44[0xffffffffffffffff] = 32;
                    if (!(reinterpret_cast<uint64_t>(r12_39) - reinterpret_cast<uint64_t>(rdx44) + reinterpret_cast<uint64_t>(rax43))) 
                        break;
                } while (reinterpret_cast<uint64_t>(rbp41) > reinterpret_cast<uint64_t>(rdx44));
            }
        }
        *rdx44 = 0;
        goto addr_65fb_14;
    }
    addr_6848_30:
    *reinterpret_cast<void***>(r13_35) = reinterpret_cast<void**>(0);
    rax45 = fun_2710();
    r9_14 = rax45;
    goto addr_651a_3;
    addr_6844_32:
    goto addr_6848_30;
    addr_6743_12:
    r12_46 = r15_17;
    do {
        eax47 = fun_2770();
        r9_14 = r9_14;
        r8_26 = r8_26;
        if (!eax47) {
            *reinterpret_cast<void***>(r12_46) = reinterpret_cast<void**>(0xfffd);
            *reinterpret_cast<int32_t*>(&r14_18) = 1;
        }
        r12_46 = r12_46 + 4;
    } while (*reinterpret_cast<void***>(r12_46 + 4));
    rsi27 = r8_26;
    eax48 = fun_2490(r15_17, rsi27, rdx24);
    r9_14 = r9_14;
    r12_16 = reinterpret_cast<void**>(static_cast<int64_t>(eax48));
    if (!*reinterpret_cast<signed char*>(&r14_18)) 
        goto addr_6883_15;
    rsi27 = r15_17;
    rax49 = fun_2710();
    r9_14 = r9_14;
    v30 = rax49 + 1;
    goto addr_67c8_19;
}

void** fun_2680(void** rdi, void** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9);

void** fun_68e3(void** rdi, void** rsi, int32_t edx, int32_t ecx) {
    void** rdx3;
    void** rcx4;
    void** r14_5;
    int32_t r13d6;
    void** r12_7;
    void** rbx8;
    void** rax9;
    int32_t v10;
    void** v11;
    void** rbp12;
    void** r15_13;
    uint64_t r8_14;
    int64_t r9_15;
    void** rax16;
    void** rdi17;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    *reinterpret_cast<int32_t*>(&rcx4) = ecx;
    __asm__("cli ");
    r14_5 = rdi;
    r13d6 = *reinterpret_cast<int32_t*>(&rdx3);
    *reinterpret_cast<int32_t*>(&r12_7) = 0;
    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
    rbx8 = rsi;
    rax9 = *reinterpret_cast<void***>(rsi);
    v10 = *reinterpret_cast<int32_t*>(&rcx4);
    v11 = rax9;
    do {
        rbp12 = rax9 + 1;
        r15_13 = r12_7;
        rax16 = fun_2680(r12_7, rbp12, rdx3, rcx4, r8_14, r9_15);
        r12_7 = rax16;
        if (!rax16) 
            break;
        *reinterpret_cast<int32_t*>(&r9_15) = v10;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_15) + 4) = 0;
        *reinterpret_cast<int32_t*>(&r8_14) = r13d6;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_14) + 4) = 0;
        rcx4 = rbx8;
        rdx3 = rbp12;
        *reinterpret_cast<void***>(rbx8) = v11;
        rax9 = mbsalign(r14_5, r12_7, rdx3, rcx4, r8_14, r9_15);
        if (rax9 == 0xffffffffffffffff) 
            goto addr_6970_4;
    } while (reinterpret_cast<unsigned char>(rbp12) <= reinterpret_cast<unsigned char>(rax9));
    goto addr_6955_6;
    fun_2400(r15_13, r15_13);
    addr_6955_6:
    return r12_7;
    addr_6970_4:
    rdi17 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_7) = 0;
    *reinterpret_cast<int32_t*>(&r12_7 + 4) = 0;
    fun_2400(rdi17, rdi17);
    goto addr_6955_6;
}

struct s14 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s14* fun_2560();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_6993(void** rdi) {
    void** rcx2;
    void** r8_3;
    void** rbx4;
    struct s14* rax5;
    void** r12_6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2730("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2, r8_3);
        fun_2410("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx4 = rdi;
        rax5 = fun_2560();
        if (rax5 && ((r12_6 = reinterpret_cast<void**>(&rax5->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_6) - reinterpret_cast<unsigned char>(rbx4)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_2430(reinterpret_cast<int64_t>(rax5) + 0xfffffffffffffffa, "/.libs/", 7), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax5->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_6 + 1) == 0x74) || *reinterpret_cast<signed char*>(r12_6 + 2) != 45)) {
                rbx4 = r12_6;
            } else {
                rbx4 = reinterpret_cast<void**>(&rax5->f4);
                __progname = rbx4;
            }
        }
        program_name = rbx4;
        __progname_full = rbx4;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_8133(int64_t rdi) {
    int64_t rbp2;
    void** rax3;
    void** r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2420();
    r12d4 = *reinterpret_cast<void***>(rax3);
    if (!rbp2) {
        rbp2 = 0x112e0;
    }
    xmemdup(rbp2, 56);
    *reinterpret_cast<void***>(rax3) = r12d4;
    return;
}

int64_t fun_8173(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x112e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_8193(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0x112e0);
    }
    *rdi = esi;
    return 0x112e0;
}

int64_t fun_81b3(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0x112e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s15 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_81f3(struct s15* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s15*>(0x112e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s16 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s16* fun_8213(struct s16* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s16*>(0x112e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x27e3;
    if (!rdx) 
        goto 0x27e3;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0x112e0;
}

struct s17 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_8253(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s17* r8) {
    struct s17* rbx6;
    void** rax7;
    void** r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s17*>(0x112e0);
    }
    rax7 = fun_2420();
    r15d8 = *reinterpret_cast<void***>(rax7);
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x8286);
    *reinterpret_cast<void***>(rax7) = r15d8;
    return rax13;
}

struct s18 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_82d3(int64_t rdi, int64_t rsi, void*** rdx, struct s18* rcx) {
    struct s18* rbx5;
    void** rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    void** v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s18*>(0x112e0);
    }
    rax6 = fun_2420();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *reinterpret_cast<void***>(rax6);
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x8301);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x835c);
    *reinterpret_cast<void***>(rax6) = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_83c3() {
    __asm__("cli ");
}

void** g110b8 = reinterpret_cast<void**>(0xe0);

int64_t slotvec0 = 0x100;

void fun_83d3() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rdi7;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2400(rdi6);
        } while (rbx4 != rbp5);
    }
    rdi7 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi7 != 0x111e0) {
        fun_2400(rdi7);
        g110b8 = reinterpret_cast<void**>(0x111e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0x110b0) {
        fun_2400(r12_2);
        slotvec = reinterpret_cast<void**>(0x110b0);
    }
    nslots = 1;
    return;
}

void fun_8473() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8493() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_84a3(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_84c3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_84e3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x27e9;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax6;
    }
}

void** fun_8573(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x27ee;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2510();
    } else {
        return rax7;
    }
}

void** fun_8603(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s1* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x27f3;
    rcx4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2510();
    } else {
        return rax5;
    }
}

void** fun_8693(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x27f8;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax6;
    }
}

void** fun_8723(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s1* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8bb0]");
    __asm__("movdqa xmm1, [rip+0x8bb8]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x8ba1]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2510();
    } else {
        return rax10;
    }
}

void** fun_87c3(int64_t rdi, uint32_t esi) {
    struct s1* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x8b10]");
    __asm__("movdqa xmm1, [rip+0x8b18]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x8b01]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2510();
    } else {
        return rax9;
    }
}

void** fun_8863(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x8a70]");
    __asm__("movdqa xmm1, [rip+0x8a78]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x8a59]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2510();
    } else {
        return rax3;
    }
}

void** fun_88f3(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x89e0]");
    __asm__("movdqa xmm1, [rip+0x89e8]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x89d6]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2510();
    } else {
        return rax4;
    }
}

void** fun_8983(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x27fd;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax6;
    }
}

void** fun_8a23(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x88aa]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x88a2]");
    __asm__("movdqa xmm2, [rip+0x88aa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2802;
    if (!rdx) 
        goto 0x2802;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2510();
    } else {
        return rax7;
    }
}

void** fun_8ac3(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s1* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x880a]");
    __asm__("movdqa xmm1, [rip+0x8812]");
    __asm__("movdqa xmm2, [rip+0x881a]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x2807;
    if (!rdx) 
        goto 0x2807;
    rcx7 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2510();
    } else {
        return rax9;
    }
}

void** fun_8b73(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s1* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x875a]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x8752]");
    __asm__("movdqa xmm2, [rip+0x875a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x280c;
    if (!rsi) 
        goto 0x280c;
    rcx5 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax6;
    }
}

void** fun_8c13(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s1* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x86ba]");
    __asm__("movdqa xmm1, [rip+0x86c2]");
    __asm__("movdqa xmm2, [rip+0x86ca]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x2811;
    if (!rsi) 
        goto 0x2811;
    rcx6 = reinterpret_cast<struct s1*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2510();
    } else {
        return rax7;
    }
}

void fun_8cb3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8cc3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8ce3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_8d03(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_25f0(int64_t rdi, void** rsi, void** rdx, void** rcx, int64_t r8, uint64_t r9);

void fun_8d23(void** rdi, int64_t rsi, int64_t rdx, uint64_t rcx, int64_t r8, uint64_t r9) {
    uint64_t r12_7;
    void** rax8;
    void** rax9;
    void** rax10;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2740(rdi, 1, "%s %s\n", rdi, 1, "%s %s\n");
    } else {
        r9 = rcx;
        fun_2740(rdi, 1, "%s (%s) %s\n", rdi, 1, "%s (%s) %s\n");
    }
    rax8 = fun_24e0();
    fun_2740(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rdi, 1, "Copyright %s %d Free Software Foundation, Inc.");
    fun_25f0(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax8, 0x7e6, r9);
    rax9 = fun_24e0();
    fun_2740(rdi, 1, rax9, rdi, 1, rax9);
    fun_25f0(10, rdi, rax9, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (r12_7 > 9) {
        rax10 = fun_24e0();
        fun_2740(rdi, 1, rax10, rdi, 1, rax10);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0xd748 + r12_7 * 4) + 0xd748;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_9193() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s19 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_91b3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s19* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s19* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2510();
    } else {
        return;
    }
}

void fun_9253(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_92f6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_9300_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2510();
    } else {
        return;
    }
    addr_92f6_5:
    goto addr_9300_7;
}

void fun_9333() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    int64_t r8_4;
    uint64_t r9_5;
    void** rax6;
    void** rcx7;
    void** r8_8;
    void** r9_9;
    void** v10;
    void** v11;
    void** v12;
    void** v13;
    void** v14;
    void** v15;
    void** v16;
    void** v17;
    void** v18;
    void** v19;
    void** v20;
    void** v21;
    void** v22;
    void** v23;
    void** v24;
    void** v25;
    void** v26;
    void** v27;
    void** v28;
    int64_t v29;
    void** v30;
    int64_t v31;
    void** v32;
    void** v33;
    void** v34;
    int64_t v35;
    void** v36;
    int64_t v37;
    void** v38;
    void** rax39;
    void** r8_40;
    void** r9_41;
    void** v42;
    void** v43;
    void** v44;
    void** v45;
    void** v46;
    void** v47;
    void** v48;
    void** v49;
    void** v50;
    void** v51;
    void** v52;
    void** v53;
    void** v54;
    void** v55;
    void** v56;
    void** v57;
    void** v58;
    void** v59;
    void** v60;
    int64_t v61;
    void** v62;
    int64_t v63;
    void** v64;
    void** v65;
    void** v66;
    int64_t v67;
    void** v68;
    int64_t v69;
    void** v70;

    __asm__("cli ");
    rsi1 = stdout;
    fun_25f0(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_24e0();
    fun_26a0(1, rax6, "bug-coreutils@gnu.org", rcx7, r8_8, r9_9, v10, __return_address(), v11, v12, v13, v14, v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29, v30, v31, v32, v33, v34, v35, v36, v37, v38);
    rax39 = fun_24e0();
    fun_26a0(1, rax39, "GNU coreutils", "https://www.gnu.org/software/coreutils/", r8_40, r9_41, v42, __return_address(), v43, v44, v45, v46, v47, v48, v49, v50, v51, v52, v53, v54, v55, v56, v57, v58, v59, v60, v61, v62, v63, v64, v65, v66, v67, v68, v69, v70);
    fun_24e0();
    goto fun_26a0;
}

int64_t fun_2480();

void xalloc_die();

void fun_93d3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2480();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_9413(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9433(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9453(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9473(void** rdi, void** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9) {
    void** rax7;

    __asm__("cli ");
    rax7 = fun_2680(rdi, rsi, rdx, rcx, r8, r9);
    if (rax7 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_94a3(void** rdi, uint64_t rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9) {
    uint64_t rax7;
    void** rax8;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rsi == 0);
    rax8 = fun_2680(rdi, rsi | rax7, rdx, rcx, r8, r9);
    if (!rax8) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_94d3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_2480();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_9513() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_2480();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9553(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_2480();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9583(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_2480();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_95d3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_2480();
            if (rax5) 
                break;
            addr_961d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_961d_5;
        rax8 = fun_2480();
        if (rax8) 
            goto addr_9606_9;
        if (rbx4) 
            goto addr_961d_5;
        addr_9606_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_9663(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_2480();
            if (rax8) 
                break;
            addr_96aa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_96aa_5;
        rax11 = fun_2480();
        if (rax11) 
            goto addr_9692_9;
        if (!rbx6) 
            goto addr_9692_9;
        if (r12_4) 
            goto addr_96aa_5;
        addr_9692_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_96f3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8, int64_t r9) {
    void** r13_7;
    void** rdi8;
    void*** r12_9;
    void** rsi10;
    void** rcx11;
    void** rbx12;
    void** rax13;
    void** rbp14;
    void* rbp15;
    void** rax16;

    __asm__("cli ");
    r13_7 = rdi;
    rdi8 = rdx;
    r12_9 = rsi;
    rsi10 = rcx;
    rcx11 = *r12_9;
    rbx12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx11) >> 1) + reinterpret_cast<unsigned char>(rcx11));
    if (__intrinsic()) {
        rbx12 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax13 = rsi10;
    if (reinterpret_cast<signed char>(rbx12) <= reinterpret_cast<signed char>(rsi10)) {
        rax13 = rbx12;
    }
    if (reinterpret_cast<signed char>(rsi10) >= reinterpret_cast<signed char>(0)) {
        rbx12 = rax13;
    }
    rbp14 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx12) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp15 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_979d_9:
            rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp15) % reinterpret_cast<int64_t>(r8));
            rbx12 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp15) / reinterpret_cast<int64_t>(r8));
            rbp14 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp15) - reinterpret_cast<unsigned char>(rdx));
            if (!r13_7) {
                addr_97b0_10:
                *r12_9 = reinterpret_cast<void**>(0);
            }
            addr_9750_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx12) - reinterpret_cast<unsigned char>(rcx11)) >= reinterpret_cast<signed char>(rdi8)) 
                goto addr_9776_12;
            rcx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx11) + reinterpret_cast<unsigned char>(rdi8));
            rbx12 = rcx11;
            if (__intrinsic()) 
                goto addr_97c4_14;
            if (reinterpret_cast<signed char>(rcx11) <= reinterpret_cast<signed char>(rsi10)) 
                goto addr_976d_16;
            if (reinterpret_cast<signed char>(rsi10) >= reinterpret_cast<signed char>(0)) 
                goto addr_97c4_14;
            addr_976d_16:
            rcx11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx11) * r8);
            rbp14 = rcx11;
            if (__intrinsic()) 
                goto addr_97c4_14;
            addr_9776_12:
            rsi10 = rbp14;
            rdi8 = r13_7;
            rax16 = fun_2680(rdi8, rsi10, rdx, rcx11, r8, r9);
            if (rax16) 
                break;
            if (!r13_7) 
                goto addr_97c4_14;
            if (!rbp14) 
                break;
            addr_97c4_14:
            xalloc_die();
        }
        *r12_9 = rbx12;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp14) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp15) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp15) + 4) = 0;
            goto addr_979d_9;
        } else {
            if (!r13_7) 
                goto addr_97b0_10;
            goto addr_9750_11;
        }
    }
}

int64_t fun_25d0();

void fun_97f3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9823() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9853() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9873() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = fun_25d0();
    if (!rax1) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_9893(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2610;
    }
}

void fun_98d3(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rsi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        goto fun_2610;
    }
}

void fun_9913(int64_t rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2640(rsi + 1, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax3) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2610;
    }
}

void fun_9953(void** rdi, void** rsi) {
    void** rax3;
    void** rax4;

    __asm__("cli ");
    rax3 = fun_2500(rdi);
    rax4 = fun_2640(rax3 + 1, rsi);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2610;
    }
}

void fun_9993() {
    void** rdi1;

    __asm__("cli ");
    fun_24e0();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_26d0();
    fun_2410(rdi1);
}

void fun_2580(int64_t rdi);

int64_t fun_99d3(void** rdi, void** rsi, uint32_t edx, void** rcx, void** r8, void** r9) {
    void** v7;
    void** rax8;
    void** v9;
    void** rdx10;
    void** rsi11;
    uint64_t rax12;
    void** rbx13;
    void** r10_14;
    uint32_t r12d15;
    void** rax16;
    void** rbp17;
    void* rax18;
    int64_t rax19;
    void** r14_20;
    void** rax21;
    int64_t rdx22;
    void** rax23;
    int64_t r13_24;
    int64_t rax25;
    int64_t rax26;
    uint32_t eax27;
    int64_t r9_28;
    uint32_t r13d29;
    int64_t r13_30;
    int64_t rax31;

    __asm__("cli ");
    v7 = rcx;
    rax8 = g28;
    v9 = rax8;
    if (edx > 36) {
        *reinterpret_cast<uint32_t*>(&rdx10) = 85;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rsi11 = reinterpret_cast<void**>("lib/xstrtol.c");
        fun_2580("0 <= strtol_base && strtol_base <= 36");
        do {
            rax12 = fun_2510();
            while (1) {
                addr_9f09_4:
                if (reinterpret_cast<signed char>(rbx13) < reinterpret_cast<signed char>(0)) {
                    rbx13 = r10_14;
                } else {
                    rbx13 = r8;
                }
                while (*reinterpret_cast<int32_t*>(&rsi11) = *reinterpret_cast<int32_t*>(&rsi11) - 1, !!*reinterpret_cast<int32_t*>(&rsi11)) {
                    if (__intrinsic()) 
                        goto addr_9f09_4;
                    rbx13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx13) * rax12);
                }
                break;
            }
            r12d15 = r12d15 | 1;
            rax16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r9));
            *reinterpret_cast<uint32_t*>(&rdx10) = r12d15 | 2;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            *reinterpret_cast<void***>(rbp17) = rax16;
            if (*reinterpret_cast<void***>(rax16)) {
                r12d15 = *reinterpret_cast<uint32_t*>(&rdx10);
            }
            addr_9a5b_15:
            *reinterpret_cast<void***>(v7) = rbx13;
            addr_9a62_16:
            rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v9) - reinterpret_cast<unsigned char>(g28));
        } while (rax18);
        *reinterpret_cast<uint32_t*>(&rax19) = r12d15;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
        return rax19;
    }
    rbp17 = rsi;
    if (!rsi) {
        rbp17 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r14_20 = r8;
    rax21 = fun_2420();
    *reinterpret_cast<uint32_t*>(&rdx22) = edx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx22) + 4) = 0;
    *reinterpret_cast<void***>(rax21) = reinterpret_cast<void**>(0);
    rsi11 = rbp17;
    rax23 = fun_2600(rdi, rsi11, rdx22, rcx, r8, r9);
    rdx10 = *reinterpret_cast<void***>(rbp17);
    rbx13 = rax23;
    if (rdx10 != rdi) 
        goto addr_9a3f_21;
    if (!r14_20) {
        addr_9bd0_23:
        r12d15 = 4;
        goto addr_9a62_16;
    } else {
        *reinterpret_cast<uint32_t*>(&r13_24) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_24) + 4) = 0;
        r12d15 = 4;
        if (!*reinterpret_cast<signed char*>(&r13_24)) 
            goto addr_9a62_16;
        *reinterpret_cast<int32_t*>(&rsi11) = *reinterpret_cast<signed char*>(&r13_24);
        r12d15 = 0;
        *reinterpret_cast<int32_t*>(&rbx13) = 1;
        *reinterpret_cast<int32_t*>(&rbx13 + 4) = 0;
        rax25 = fun_2540(r14_20);
        rdx10 = rdx10;
        if (!rax25) 
            goto addr_9bd0_23;
    }
    addr_9ae7_26:
    *reinterpret_cast<uint32_t*>(&r8) = static_cast<uint32_t>(r13_24 - 69);
    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
    *reinterpret_cast<int32_t*>(&r9) = 1;
    *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
    if (*reinterpret_cast<unsigned char*>(&r8) <= 47 && (static_cast<int1_t>(0x814400308945 >> reinterpret_cast<unsigned char>(r8)) && (*reinterpret_cast<int32_t*>(&rsi11) = 48, rax26 = fun_2540(r14_20, r14_20), rdx10 = rdx10, *reinterpret_cast<int32_t*>(&r9) = 1, *reinterpret_cast<int32_t*>(&r9 + 4) = 0, !!rax26))) {
        eax27 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10 + 1));
        if (*reinterpret_cast<signed char*>(&eax27) == 68) {
            *reinterpret_cast<int32_t*>(&r9) = 2;
            *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
        } else {
            if (*reinterpret_cast<signed char*>(&eax27) == 0x69) {
                *reinterpret_cast<int32_t*>(&r9_28) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_28) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&r9_28) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(rdx10 + 2) == 66);
                *reinterpret_cast<int32_t*>(&r9) = static_cast<int32_t>(r9_28 + r9_28 + 1);
                *reinterpret_cast<int32_t*>(&r9 + 4) = 0;
            } else {
                *reinterpret_cast<uint32_t*>(&r8) = *reinterpret_cast<unsigned char*>(&r8);
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                if (*reinterpret_cast<signed char*>(&eax27) != 66) {
                    goto *reinterpret_cast<int32_t*>(0xd8d0 + reinterpret_cast<unsigned char>(r8) * 4) + 0xd8d0;
                }
            }
        }
    }
    r13d29 = *reinterpret_cast<uint32_t*>(&r13_24) - 66;
    if (*reinterpret_cast<unsigned char*>(&r13d29) <= 53) {
        *reinterpret_cast<uint32_t*>(&r13_30) = *reinterpret_cast<unsigned char*>(&r13d29);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_30) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xd7f8 + r13_30 * 4) + 0xd7f8;
    }
    addr_9b4b_35:
    r12d15 = r12d15 | 2;
    *reinterpret_cast<void***>(v7) = rbx13;
    goto addr_9a62_16;
    addr_9a3f_21:
    if (*reinterpret_cast<void***>(rax21)) {
        r12d15 = 4;
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax21) == 34)) 
            goto addr_9a62_16;
        r12d15 = 1;
    } else {
        r12d15 = 0;
    }
    if (!r14_20) 
        goto addr_9a5b_15;
    *reinterpret_cast<uint32_t*>(&r13_24) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_24) + 4) = 0;
    if (!*reinterpret_cast<signed char*>(&r13_24)) 
        goto addr_9a5b_15;
    *reinterpret_cast<int32_t*>(&rsi11) = *reinterpret_cast<signed char*>(&r13_24);
    rax31 = fun_2540(r14_20);
    rdx10 = rdx10;
    if (rax31) 
        goto addr_9ae7_26; else 
        goto addr_9b4b_35;
}

uint64_t fun_26f0(void** rdi);

int64_t fun_9fd3(void** rdi, void*** rsi, uint32_t edx, uint64_t* rcx, void** r8) {
    uint64_t* v6;
    void** rax7;
    void** v8;
    void** rcx9;
    uint64_t rbx10;
    uint32_t r12d11;
    void* r9_12;
    void** rax13;
    void*** r15_14;
    void* rax15;
    int64_t rax16;
    void** rbp17;
    void** r13_18;
    void** rax19;
    void** r12_20;
    void** rax21;
    void** rax22;
    int64_t rdx23;
    uint64_t rax24;
    int64_t rbp25;
    int64_t rax26;
    int64_t rax27;
    int64_t rax28;
    uint32_t eax29;
    int64_t r9_30;
    int32_t r9d31;
    uint32_t ebp32;
    int64_t rbp33;
    int64_t rax34;

    __asm__("cli ");
    v6 = rcx;
    rax7 = g28;
    v8 = rax7;
    if (edx > 36) {
        rcx9 = reinterpret_cast<void**>("xstrtoumax");
        rsi = reinterpret_cast<void***>("lib/xstrtol.c");
        fun_2580("0 <= strtol_base && strtol_base <= 36");
        do {
            fun_2510();
            while (1) {
                rbx10 = 0xffffffffffffffff;
                do {
                    *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<int32_t*>(&rsi) - 1;
                    if (!*reinterpret_cast<int32_t*>(&rsi)) 
                        goto addr_a344_6;
                    rbx10 = rbx10 * reinterpret_cast<unsigned char>(rcx9);
                } while (!__intrinsic());
            }
            addr_a344_6:
            r12d11 = r12d11 | 1;
            r9_12 = reinterpret_cast<void*>(static_cast<int64_t>(*reinterpret_cast<int32_t*>(&r9_12)));
            rax13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r8) + reinterpret_cast<uint64_t>(r9_12));
            *r15_14 = rax13;
            if (*reinterpret_cast<void***>(rax13)) {
                r12d11 = r12d11 | 2;
            }
            addr_a08d_12:
            *v6 = rbx10;
            addr_a095_13:
            rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v8) - reinterpret_cast<unsigned char>(g28));
        } while (rax15);
        *reinterpret_cast<uint32_t*>(&rax16) = r12d11;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
    r15_14 = rsi;
    rbp17 = rdi;
    if (!rsi) {
        r15_14 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 56 + 32);
    }
    r13_18 = r8;
    rax19 = fun_2420();
    *reinterpret_cast<void***>(rax19) = reinterpret_cast<void**>(0);
    r12_20 = rax19;
    *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17));
    rax21 = fun_2790(rdi);
    rcx9 = *reinterpret_cast<void***>(rax21);
    rax22 = rbp17;
    while (*reinterpret_cast<uint32_t*>(&rdx23) = *reinterpret_cast<unsigned char*>(&rbx10), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx23) + 4) = 0, !!(*reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rcx9) + reinterpret_cast<uint64_t>(rdx23 * 2) + 1) & 32)) {
        *reinterpret_cast<uint32_t*>(&rbx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax22 + 1));
        ++rax22;
    }
    if (*reinterpret_cast<unsigned char*>(&rbx10) == 45) 
        goto addr_a0cb_21;
    rsi = r15_14;
    rax24 = fun_26f0(rbp17);
    r8 = *r15_14;
    rbx10 = rax24;
    if (r8 == rbp17) {
        if (!r13_18 || ((*reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp17)), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0, *reinterpret_cast<signed char*>(&rbp25) == 0) || (*reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25), r12d11 = 0, *reinterpret_cast<uint32_t*>(&rbx10) = 1, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx10) + 4) = 0, rax26 = fun_2540(r13_18), r8 = r8, rax26 == 0))) {
            addr_a0cb_21:
            r12d11 = 4;
            goto addr_a095_13;
        } else {
            addr_a109_24:
            *reinterpret_cast<int32_t*>(&rax27) = static_cast<int32_t>(rbp25 - 69);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax27) + 4) = 0;
            *reinterpret_cast<int32_t*>(&r9_12) = 1;
            *reinterpret_cast<int32_t*>(&rcx9) = 0x400;
            *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rax27) <= 47 && (static_cast<int1_t>(0x814400308945 >> rax27) && (*reinterpret_cast<int32_t*>(&rsi) = 48, rax28 = fun_2540(r13_18, r13_18), r8 = r8, *reinterpret_cast<int32_t*>(&rcx9) = 0x400, *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0, *reinterpret_cast<int32_t*>(&r9_12) = 1, !!rax28))) {
                eax29 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8 + 1));
                if (*reinterpret_cast<signed char*>(&eax29) == 68) {
                    *reinterpret_cast<int32_t*>(&r9_12) = 2;
                    *reinterpret_cast<int32_t*>(&rcx9) = 0x3e8;
                    *reinterpret_cast<int32_t*>(&rcx9 + 4) = 0;
                } else {
                    if (*reinterpret_cast<signed char*>(&eax29) == 0x69) {
                        *reinterpret_cast<int32_t*>(&r9_30) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_30) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&r9_30) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(r8 + 2) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = static_cast<int32_t>(r9_30 + r9_30 + 1);
                    } else {
                        r9d31 = 0;
                        *reinterpret_cast<unsigned char*>(&r9d31) = reinterpret_cast<uint1_t>(*reinterpret_cast<signed char*>(&eax29) == 66);
                        *reinterpret_cast<int32_t*>(&r9_12) = r9d31 + 1;
                        if (*reinterpret_cast<signed char*>(&eax29) == 66) {
                            rcx9 = reinterpret_cast<void**>(0x3e8);
                        }
                    }
                }
            }
        }
        ebp32 = *reinterpret_cast<uint32_t*>(&rbp25) - 66;
        if (*reinterpret_cast<unsigned char*>(&ebp32) <= 53) {
            *reinterpret_cast<uint32_t*>(&rbp33) = *reinterpret_cast<unsigned char*>(&ebp32);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp33) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xd998 + rbp33 * 4) + 0xd998;
        }
    } else {
        if (*reinterpret_cast<void***>(r12_20)) {
            r12d11 = 1;
            if (*reinterpret_cast<void***>(r12_20) != 34) 
                goto addr_a0cb_21;
        } else {
            r12d11 = 0;
        }
        if (!r13_18) 
            goto addr_a08d_12;
        *reinterpret_cast<uint32_t*>(&rbp25) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r8));
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp25) + 4) = 0;
        if (!*reinterpret_cast<signed char*>(&rbp25)) 
            goto addr_a08d_12;
        *reinterpret_cast<int32_t*>(&rsi) = *reinterpret_cast<signed char*>(&rbp25);
        rax34 = fun_2540(r13_18);
        r8 = r8;
        if (rax34) 
            goto addr_a109_24;
    }
    r12d11 = r12d11 | 2;
    *v6 = rbx10;
    goto addr_a095_13;
}

int64_t fun_2460();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_a403(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    void** rax5;
    void** rax6;

    __asm__("cli ");
    rax2 = fun_2460();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_a45e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2420();
            *reinterpret_cast<void***>(rax5) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_a45e_3;
            rax6 = fun_2420();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rax6) == 9)))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s20 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2620(struct s20* rdi);

int32_t fun_2670(struct s20* rdi);

int64_t fun_2570(int64_t rdi, ...);

int32_t rpl_fflush(struct s20* rdi);

int64_t fun_24c0(struct s20* rdi);

int64_t fun_a473(struct s20* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    void** rax8;
    void** r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2620(rdi);
    if (eax2 >= 0) {
        eax3 = fun_2670(rdi);
        if (!(eax3 && (eax4 = fun_2620(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_2570(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2420();
            r12d9 = *reinterpret_cast<void***>(rax8);
            rax10 = fun_24c0(rdi);
            if (r12d9) {
                *reinterpret_cast<void***>(rax8) = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_24c0;
}

void rpl_fseeko(struct s20* rdi);

void fun_a503(struct s20* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_2670(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_a553(struct s20* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2620(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_2570(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

void** fun_a5d3() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;

    __asm__("cli ");
    rax6 = fun_2660(14, rsi1, rdx2, rcx3, r8_4, r9_5);
    if (!rax6) {
        return "ASCII";
    } else {
        if (!*reinterpret_cast<void***>(rax6)) {
            rax6 = reinterpret_cast<void**>("ASCII");
        }
        return rax6;
    }
}

uint64_t fun_2530(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_a613(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2530(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2510();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_a6a3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2510();
    } else {
        return rax3;
    }
}

int64_t fun_a723(int64_t rdi, signed char* rsi, void** rdx) {
    void** rax4;
    int32_t r13d5;
    void** rax6;
    int64_t rax7;

    __asm__("cli ");
    rax4 = fun_2690(rdi);
    if (!rax4) {
        r13d5 = 22;
        if (rdx) {
            *rsi = 0;
        }
    } else {
        rax6 = fun_2500(rax4);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax6)) {
            fun_2610(rsi, rax4, rax6 + 1);
            return 0;
        } else {
            r13d5 = 34;
            if (rdx) {
                fun_2610(rsi, rax4, rdx + 0xffffffffffffffff);
                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(rsi) + reinterpret_cast<unsigned char>(rdx)) - 1) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = r13d5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    return rax7;
}

void fun_a7d3() {
    __asm__("cli ");
    goto fun_2690;
}

void fun_a7e3() {
    __asm__("cli ");
}

void fun_a7f7() {
    __asm__("cli ");
    return;
}

int32_t xstrtol(void** rdi);

void fun_2a4a() {
    void** rdi1;
    int32_t eax2;
    void** rax3;

    rdi1 = optarg;
    eax2 = xstrtol(rdi1);
    if (eax2) 
        goto 0x33d1;
    rax3 = padding_width;
    __asm__("btr rdx, 0x3f");
    if (!rax3) 
        goto 0x33d1;
    if (reinterpret_cast<signed char>(rax3) >= reinterpret_cast<signed char>(0)) 
        goto 0x28e0;
    padding_alignment = 0;
    padding_width = reinterpret_cast<void**>(-reinterpret_cast<unsigned char>(rax3));
    goto 0x28e0;
}

void fun_2aad() {
    grouping = 1;
    goto 0x28e0;
}

void fun_3f5a() {
    __asm__("fstp st1");
}

void fun_44d8() {
    __asm__("fldz ");
    __asm__("fcomip st0, st2");
    if (0) {
        __asm__("fxch st0, st1");
        __asm__("fchs ");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x10]");
        __asm__("fldcw word [rsp+0x4e]");
        __asm__("fild qword [rsp+0x10]");
        __asm__("fxch st0, st1");
        __asm__("fcomip st0, st1");
        __asm__("fstp st0");
        __asm__("fild qword [rsp+0x10]");
    } else {
        __asm__("fxch st0, st1");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x10]");
        __asm__("fldcw word [rsp+0x4e]");
        __asm__("fild qword [rsp+0x10]");
        __asm__("fxch st0, st1");
        __asm__("fcomip st0, st1");
        if (!1) 
            goto addr_4508_7;
    }
    addr_451c_8:
    __asm__("faddp st1, st0");
    if (1) 
        goto 0x4543;
    __asm__("fld dword [rip+0x888a]");
    if (0) 
        goto 0x4a91; else 
        goto "???";
    addr_4508_7:
    __asm__("fstp st0");
    __asm__("fild qword [rsp+0x10]");
    goto addr_451c_8;
}

void fun_44ec() {
    __asm__("fstp st1");
    __asm__("fxch st0, st1");
}

void fun_45cb() {
    int1_t below_or_equal1;
    int32_t esi2;
    int32_t esi3;

    __asm__("fxch st0, st1");
    __asm__("fldz ");
    __asm__("fcomip st0, st2");
    if (!below_or_equal1) {
        __asm__("fxch st0, st1");
        __asm__("fsub dword [rip+0x7ee7]");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp]");
        __asm__("fldcw word [rsp+0x4e]");
        __asm__("fild qword [rsp]");
    } else {
        __asm__("fxch st0, st1");
        __asm__("fadd dword [rip+0x87dd]");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp]");
        __asm__("fldcw word [rsp+0x4e]");
        __asm__("fild qword [rsp]");
    }
    __asm__("faddp st1, st0");
    if (!esi2) 
        goto 0x461b;
    __asm__("fld dword [rip+0x87b1]");
    if (esi3 - 1) 
        goto "???";
    __asm__("fdivp st1, st0");
    goto 0x461b;
}

void fun_45e5() {
    __asm__("fxch st0, st1");
}

void fun_47ec() {
    int1_t below_or_equal1;
    int1_t below_or_equal2;

    __asm__("fxch st0, st1");
    __asm__("fldz ");
    __asm__("fcomip st0, st2");
    if (!below_or_equal1) {
        __asm__("fxch st0, st1");
        __asm__("fchs ");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp]");
        __asm__("fldcw word [rsp+0x4e]");
        __asm__("fild qword [rsp]");
        __asm__("fxch st0, st1");
        __asm__("fcomip st0, st1");
        __asm__("fstp st0");
        __asm__("fild qword [rsp]");
        goto 0x45f5;
    } else {
        __asm__("fxch st0, st1");
        __asm__("fld st0");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp]");
        __asm__("fldcw word [rsp+0x4e]");
        __asm__("fild qword [rsp]");
        __asm__("fxch st0, st1");
        __asm__("fcomip st0, st1");
        if (below_or_equal2) 
            goto 0x45f5;
        __asm__("fstp st0");
        __asm__("fild qword [rsp]");
        goto 0x45f5;
    }
}

void fun_47fc() {
    __asm__("fxch st0, st1");
}

void fun_482e() {
    __asm__("fxch st0, st1");
    goto 0x4834;
}

void fun_48da() {
    __asm__("fxch st0, st1");
}

void fun_4970() {
    __asm__("fxch st0, st1");
    goto 0x48e3;
}

void fun_4ba3() {
    __asm__("fxch st0, st1");
    goto 0x44f0;
}

void** rpl_mbrtowc(void* rdi, unsigned char* rsi);

uint32_t fun_2760(void* rdi, unsigned char* rsi);

void fun_6bc5() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rax10;
    void** r11_11;
    void** v12;
    int32_t ebp13;
    void** rax14;
    void* r15_15;
    int32_t ebx16;
    void** rdx17;
    uint32_t eax18;
    void* r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void* rsi30;
    void* v31;
    void** v32;
    void* r10_33;
    void* r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rdx44;
    uint32_t ecx45;
    uint32_t edx46;
    unsigned char al47;
    void** v48;
    uint64_t v49;
    void* v50;
    void** v51;
    void** rax52;
    uint64_t rdx53;
    uint32_t edx54;
    int64_t rdx55;
    uint32_t eax56;
    uint32_t eax57;
    uint32_t eax58;
    uint1_t zf59;
    unsigned char v60;
    void** v61;
    unsigned char v62;
    void* v63;
    void* v64;
    void** v65;
    signed char* v66;
    void* r12_67;
    unsigned char v68;
    void* rbx69;
    uint32_t v70;
    void* r14_71;
    void** r13_72;
    unsigned char* rsi73;
    void* v74;
    void** r15_75;
    unsigned char* rdx76;
    void* v77;
    int64_t rax78;
    int32_t eax79;
    void* rdi80;
    uint32_t edx81;
    unsigned char v82;
    void* rdi83;
    void* v84;
    uint32_t esi85;
    uint32_t ebp86;
    void** rcx87;
    uint32_t eax88;
    uint32_t eax89;
    uint32_t eax90;
    uint32_t eax91;
    uint32_t eax92;
    uint32_t eax93;
    void* rdx94;
    void* rcx95;
    void* v96;
    void** rax97;
    uint1_t zf98;
    int32_t ecx99;
    int32_t ecx100;
    uint32_t ecx101;
    uint32_t edi102;
    int32_t ecx103;
    uint32_t edi104;
    uint32_t edx105;
    uint32_t edi106;
    uint32_t edx107;
    uint64_t rax108;
    uint32_t eax109;
    uint32_t r12d110;
    uint64_t rax111;
    uint64_t rax112;
    uint32_t r12d113;
    uint32_t ecx114;
    void* v115;
    void* rdx116;
    void* rax117;
    void* v118;
    uint64_t rax119;
    int64_t v120;
    int64_t rax121;
    int64_t rax122;
    int64_t rax123;
    int64_t v124;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_24e0();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        rax10 = fun_24e0();
        rsp1 = rsp4 - 8 + 8;
        r11_11 = r11_5;
        v12 = rax10;
        if (rax10 == "'") {
            rax14 = gettext_quote_part_0(rax10, ebp13, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_11 = r11_5;
            v12 = rax14;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_15) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
    if (!ebx16 && (rdx17 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx17)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<uint64_t>(r13_19) > reinterpret_cast<uint64_t>(r15_15)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&eax18);
            }
            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx17) + reinterpret_cast<uint64_t>(r15_15));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2500(v12, v12);
    rsp25 = rsp1 - 8 + 8;
    v26 = v12;
    r11_27 = r11_11;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    v31 = reinterpret_cast<void*>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_6ec3_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_6ec3_22; else 
                            goto addr_72bd_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_737d_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_76d0_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_6ec0_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_6ec0_30; else 
                                goto addr_76e9_32;
                        }
                    } else {
                        rdx44 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2500(rdi39, rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx44 = rdx44;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx44) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_76d0_28;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_25b0(rdi39, v26, v28, rdi39, v26, v28);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_76d0_28; else 
                            goto addr_6d6c_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_7830_39:
                    ecx45 = 0x7d;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_76b0_40:
                        if (r11_27 == 1) {
                            addr_723d_41:
                            edx46 = r8d42;
                            if (r9_37) {
                                addr_77f8_42:
                                r8d42 = edx46;
                                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = ecx45;
                                goto addr_6e77_44;
                            }
                        } else {
                            goto addr_76c0_46;
                        }
                    } else {
                        addr_783f_47:
                        rax24 = v48;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_723d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        ecx45 = 0x7b;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_6ec3_22:
                                if (v49 != 1) {
                                    addr_7419_52:
                                    v50 = reinterpret_cast<void*>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax52 = fun_2500(v51, v51);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax52;
                                        goto addr_7464_54;
                                    }
                                } else {
                                    goto addr_6ed0_56;
                                }
                            } else {
                                addr_6e75_57:
                                ebp36 = 0;
                                goto addr_6e77_44;
                            }
                        } else {
                            addr_76a4_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_783f_47; else 
                                goto addr_76ae_59;
                        }
                    } else {
                        ecx45 = 0x7e;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_723d_41;
                        if (v49 == 1) 
                            goto addr_6ed0_56; else 
                            goto addr_7419_52;
                    }
                }
                addr_6f31_62:
                *reinterpret_cast<uint32_t*>(&rdx53) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                rax24 = reinterpret_cast<void**>(al47 | *reinterpret_cast<unsigned char*>(&rdx53));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_6dc8_63:
                    if (!1 && (edx54 = ecx45, *reinterpret_cast<uint32_t*>(&rdx55) = *reinterpret_cast<unsigned char*>(&edx54) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx55) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx53) = *reinterpret_cast<uint32_t*>(rdx55 * 4) >> *reinterpret_cast<signed char*>(&ecx45) & 1, !!*reinterpret_cast<uint32_t*>(&rdx53)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_6ded_64:
                        *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                        if (v22) 
                            goto addr_70f0_65;
                    } else {
                        addr_6f59_66:
                        ++r9_37;
                        eax57 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_77a8_67;
                    }
                } else {
                    goto addr_6f50_69;
                }
                addr_6e01_70:
                eax58 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax58) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax58) & *reinterpret_cast<unsigned char*>(&rdx53));
                if (*reinterpret_cast<unsigned char*>(&eax58)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax58;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                }
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                }
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                ++r9_37;
                addr_6e4c_81:
                if (reinterpret_cast<uint64_t>(r15_15) < reinterpret_cast<uint64_t>(r10_33)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<signed char*>(&ecx45);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_77a8_67:
                if (*reinterpret_cast<signed char*>(&eax57)) {
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_6e4c_81;
                }
                addr_6f50_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_6ded_64; else 
                    goto addr_6f59_66;
                addr_6e77_44:
                zf59 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al47 = zf59;
                if (!zf59) 
                    goto addr_6f2f_91;
                if (v22) 
                    goto addr_6e8f_93;
                addr_6f2f_91:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_6f31_62;
                addr_7464_54:
                v60 = *reinterpret_cast<unsigned char*>(&r8d42);
                v61 = r9_37;
                v62 = *reinterpret_cast<unsigned char*>(&r13_34);
                v63 = r15_15;
                v64 = r10_33;
                v65 = r11_27;
                v66 = r12_21;
                r12_67 = v50;
                v68 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx69 = reinterpret_cast<void*>(0);
                v70 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_71 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    r13_72 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v61) + reinterpret_cast<uint64_t>(rbx69));
                    rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(v74) + reinterpret_cast<unsigned char>(r13_72));
                    rax24 = rpl_mbrtowc(r14_71, rsi73);
                    rsp25 = rsp25 - 8 + 8;
                    r15_75 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_7beb_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_7c5b_98;
                    if (v70 == 2 && (v22 && rax24 != 1)) {
                        rdx76 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r13_72)) + 1);
                        rsi73 = reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v77) + reinterpret_cast<unsigned char>(r15_75)) + reinterpret_cast<unsigned char>(r13_72));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax78) = *rdx76 - 91;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax78) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax78) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax78)) 
                                goto addr_7a5f_103;
                            ++rdx76;
                        } while (rsi73 != rdx76);
                    }
                    eax79 = fun_2770();
                    if (!eax79) {
                        ebp36 = 0;
                    }
                    rbx69 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx69) + reinterpret_cast<unsigned char>(r15_75));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2760(r12_67, rsi73);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi80 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                edx81 = ebp36 ^ 1;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&edx81) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&edx81) & reinterpret_cast<unsigned char>(v32));
                addr_755e_109:
                if (reinterpret_cast<uint64_t>(rdi80) <= 1) {
                    addr_6f1c_110:
                    if (*reinterpret_cast<unsigned char*>(&edx81)) {
                        edx81 = reinterpret_cast<unsigned char>(v32);
                        ebp36 = 0;
                        goto addr_7568_112;
                    }
                } else {
                    addr_7568_112:
                    v82 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi83 = v84;
                    esi85 = 0;
                    ebp86 = reinterpret_cast<unsigned char>(v22);
                    rcx87 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_7639_114;
                }
                addr_6f28_115:
                al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_6f2f_91;
                while (1) {
                    addr_7639_114:
                    if (*reinterpret_cast<unsigned char*>(&edx81)) {
                        *reinterpret_cast<unsigned char*>(&esi85) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax88 = esi85;
                        if (*reinterpret_cast<signed char*>(&ebp86)) 
                            goto addr_7b47_117;
                        eax89 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax89) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax89) & *reinterpret_cast<unsigned char*>(&esi85));
                        if (*reinterpret_cast<unsigned char*>(&eax89)) 
                            goto addr_75a6_119;
                    } else {
                        eax57 = (esi85 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx87)) 
                            goto addr_7b55_125;
                        if (!*reinterpret_cast<signed char*>(&eax57)) {
                            r8d42 = 0;
                            goto addr_7627_128;
                        } else {
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                            }
                            if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 39;
                            }
                            r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 2);
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                            goto addr_7627_128;
                        }
                    }
                    addr_75d5_134:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 92;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        eax90 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax90) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax90) >> 6);
                        eax91 = eax90 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = *reinterpret_cast<signed char*>(&eax91);
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        eax92 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax92) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax92) >> 3);
                        eax93 = (eax92 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = *reinterpret_cast<signed char*>(&eax93);
                    }
                    ++r9_37;
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx87)) 
                        break;
                    esi85 = edx81;
                    addr_7627_128:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi83) + reinterpret_cast<unsigned char>(r9_37));
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 1);
                    continue;
                    addr_75a6_119:
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15)) = 39;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 1) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 1) = 36;
                    }
                    if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15) + 2) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<uint64_t>(r15_15) + 2) = 39;
                    }
                    r15_15 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(r15_15) + 3);
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax89;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                    goto addr_75d5_134;
                }
                ebp36 = v82;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_6e4c_81;
                addr_7b55_125:
                ebp36 = v82;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_77a8_67;
                addr_7beb_96:
                rdi80 = rbx69;
                r8d42 = v60;
                r9_37 = v61;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                r11_27 = v65;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                edx81 = reinterpret_cast<unsigned char>(v32);
                goto addr_755e_109;
                addr_7c5b_98:
                r11_27 = v65;
                rdi80 = rbx69;
                rax24 = r13_72;
                r9_37 = v61;
                r8d42 = v60;
                *reinterpret_cast<uint32_t*>(&rbx43) = v68;
                rdx94 = rdi80;
                *reinterpret_cast<uint32_t*>(&r13_34) = v62;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
                r15_15 = v63;
                r12_21 = v66;
                r10_33 = v64;
                *reinterpret_cast<uint32_t*>(&r14_35) = v70;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx95 = v96;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx95) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx94 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx94) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx94));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi80 = rdx94;
                }
                edx81 = reinterpret_cast<unsigned char>(v32);
                ebp36 = 0;
                goto addr_755e_109;
                addr_6ed0_56:
                rax97 = fun_2790(rdi39, rdi39);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi80) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi80) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf98 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(*reinterpret_cast<void***>(rax97) + reinterpret_cast<unsigned char>(rax24) * 2) + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf98);
                *reinterpret_cast<unsigned char*>(&edx81) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf98) & reinterpret_cast<unsigned char>(v32));
                goto addr_6f1c_110;
                addr_76ae_59:
                goto addr_76b0_40;
                addr_737d_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_6ec3_22;
                ecx99 = static_cast<int32_t>(rbx43 - 65);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx99));
                if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                    goto addr_6f28_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_6e75_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_6ec3_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_73c2_160;
                if (!v22) 
                    goto addr_7797_162; else 
                    goto addr_79a3_163;
                addr_73c2_160:
                *reinterpret_cast<uint32_t*>(&rdx53) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx53) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (*reinterpret_cast<unsigned char*>(&rdx53)) {
                    addr_7797_162:
                    ++r9_37;
                    eax57 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    ecx45 = 92;
                    goto addr_77a8_67;
                } else {
                    ecx45 = 92;
                    if (!v32) 
                        goto addr_726b_166;
                }
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                ebp36 = 0;
                addr_70d3_168:
                *reinterpret_cast<unsigned char*>(&rdx53) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax56 = *reinterpret_cast<uint32_t*>(&rdx53);
                if (!v22) 
                    goto addr_6e01_70; else 
                    goto addr_70e7_169;
                addr_726b_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_6dc8_63;
                goto addr_6f50_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        ecx45 = 0x7d;
                        r8d42 = 0;
                        goto addr_76a4_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_77df_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_6ec0_30;
                    ecx100 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx100));
                    ecx101 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_6db8_178; else 
                        goto addr_7762_179;
                }
                ecx45 = 0x7b;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_76a4_58;
                ecx45 = 0x7c;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_6ec3_22;
                }
                addr_77df_175:
                edx46 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_6ec0_30:
                    r8d42 = 0;
                    goto addr_6ec3_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        ecx45 = 0x7e;
                        r8d42 = edx46;
                        al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_6f31_62;
                    } else {
                        ecx45 = 0x7e;
                        goto addr_77f8_42;
                    }
                }
                addr_6db8_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx101;
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                goto addr_6dc8_63;
                addr_7762_179:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_76c0_46:
                    al47 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_6f31_62;
                } else {
                    addr_7772_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_6ec3_22;
                }
                edi102 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi102))) 
                    goto addr_7f22_188;
                if (v28) 
                    goto addr_7797_162;
                addr_7f22_188:
                ecx45 = 92;
                ebp36 = 0;
                goto addr_70d3_168;
                addr_6d6c_37:
                if (v22) 
                    goto addr_7d63_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_6d83_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_7830_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_78bb_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_6ec3_22;
                    ecx103 = static_cast<int32_t>(rbx43 - 65);
                    rdx53 = 0x3ffffff53ffffff;
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx103));
                    ecx101 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & 0x3ffffff53ffffff) 
                        goto addr_6db8_178; else 
                        goto addr_7897_199;
                }
                ecx45 = 0x7b;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_76a4_58;
                ecx45 = 0x7c;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_6ec3_22;
                }
                addr_78bb_196:
                edx46 = r8d42;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_6ec3_22;
                }
                addr_7897_199:
                ecx45 = *reinterpret_cast<uint32_t*>(&rbx43);
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_76c0_46;
                goto addr_7772_186;
                addr_6d83_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_6ec3_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_6ec3_22; else 
                    goto addr_6d94_206;
            }
            edi104 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            edx105 = edi104 & *reinterpret_cast<uint32_t*>(&rax24);
            if (static_cast<unsigned char>(reinterpret_cast<uint1_t>(r15_15 == 0)) & *reinterpret_cast<unsigned char*>(&edx105)) 
                goto addr_7e6e_208;
            edi106 = edi104 ^ 1;
            edx107 = edi106;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi106));
            if (!rax24) 
                goto addr_7cf4_210;
            if (1) 
                goto addr_7cf2_212;
            if (!v29) 
                goto addr_792e_214;
            *reinterpret_cast<int32_t*>(&r15_15) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax108 = fun_24f0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v49 = rax108;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_7e61_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void*>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_34) + 4) = 0;
            v31 = reinterpret_cast<void*>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_70f0_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax109 = eax56 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax109)) 
            goto addr_6eab_219; else 
            goto addr_710a_220;
        addr_6e8f_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax88 = reinterpret_cast<unsigned char>(v32);
        addr_6ea3_221:
        if (*reinterpret_cast<signed char*>(&eax88)) 
            goto addr_710a_220; else 
            goto addr_6eab_219;
        addr_7a5f_103:
        r12d110 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v66;
        r13_34 = v64;
        r11_27 = v65;
        if (*reinterpret_cast<signed char*>(&r12d110)) {
            addr_710a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax111 = fun_24f0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax111;
        } else {
            addr_7a7d_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax112 = fun_24f0();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v49 = rax112;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void*>(0);
            continue;
        }
        addr_7ef0_225:
        v31 = r13_34;
        addr_7956_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void*>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_7b47_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_6ea3_221;
        addr_79a3_163:
        eax88 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_6ea3_221;
        addr_70e7_169:
        goto addr_70f0_65;
        addr_7e6e_208:
        r14_35 = r12_21;
        r12d113 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d113)) 
            goto addr_710a_220;
        goto addr_7a7d_222;
        addr_7cf4_210:
        if (v26 && (*reinterpret_cast<signed char*>(&edx107) && (ecx114 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), !!*reinterpret_cast<signed char*>(&ecx114)))) {
            rsi30 = v115;
            rdx116 = r15_15;
            rax117 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<uint64_t>(r15_15));
            do {
                if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(rdx116)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rsi30) + reinterpret_cast<uint64_t>(rdx116)) = *reinterpret_cast<signed char*>(&ecx114);
                }
                rdx116 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx116) + 1);
                ecx114 = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax117) + reinterpret_cast<uint64_t>(rdx116));
            } while (*reinterpret_cast<signed char*>(&ecx114));
            r15_15 = rdx116;
        }
        if (reinterpret_cast<uint64_t>(r10_33) > reinterpret_cast<uint64_t>(r15_15)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v118) + reinterpret_cast<uint64_t>(r15_15)) = 0;
        }
        rax119 = reinterpret_cast<uint64_t>(v120 - reinterpret_cast<unsigned char>(g28));
        if (!rax119) 
            goto addr_7d4e_236;
        fun_2510();
        rsp25 = rsp25 - 8 + 8;
        goto addr_7ef0_225;
        addr_7cf2_212:
        edx107 = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_7cf4_210;
        addr_792e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            edx107 = 0;
            goto addr_7cf4_210;
        } else {
            goto addr_7956_226;
        }
        addr_7e61_216:
        r13_34 = reinterpret_cast<void*>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_15) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_15) + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void*>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_72bd_24:
    *reinterpret_cast<uint32_t*>(&rax121) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax121) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd20c + rax121 * 4) + 0xd20c;
    addr_76e9_32:
    *reinterpret_cast<uint32_t*>(&rax122) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax122) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd30c + rax122 * 4) + 0xd30c;
    addr_7d63_190:
    addr_6eab_219:
    goto 0x6b90;
    addr_6d94_206:
    *reinterpret_cast<uint32_t*>(&rax123) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax123) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xd10c + rax123 * 4) + 0xd10c;
    addr_7d4e_236:
    goto v124;
}

void fun_6db0() {
}

void fun_6f68() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x6c62;
}

void fun_6fc1() {
    goto 0x6c62;
}

void fun_70ae() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x6f31;
    }
    if (v2) 
        goto 0x79a3;
    if (!r10_3) 
        goto addr_7b0e_5;
    if (!v4) 
        goto addr_79de_7;
    addr_7b0e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_79de_7:
    goto 0x6de4;
}

void fun_70cc() {
}

void fun_7177() {
    signed char v1;

    if (v1) {
        goto 0x70ff;
    } else {
        goto 0x6e3a;
    }
}

void fun_7191() {
    signed char v1;

    if (!v1) 
        goto 0x718a; else 
        goto "???";
}

void fun_71b8() {
    goto 0x70d3;
}

void fun_7238() {
}

void fun_7250() {
}

void fun_727f() {
    goto 0x70d3;
}

void fun_72d1() {
    goto 0x7260;
}

void fun_7300() {
    goto 0x7260;
}

void fun_7333() {
    goto 0x7260;
}

void fun_7700() {
    goto 0x6db8;
}

void fun_79fe() {
    signed char v1;

    if (v1) 
        goto 0x79a3;
    goto 0x6de4;
}

void fun_7aa5() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x6de4;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x6dc8;
        goto 0x6de4;
    }
}

void fun_7ec2() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x7130;
    } else {
        goto 0x6c62;
    }
}

void fun_8df8() {
    fun_24e0();
}

void fun_9c35() {
    int64_t rbx1;
    int64_t rbx2;
    int32_t ecx3;

    if (__intrinsic()) {
        if (rbx1 < 0) {
            goto 0x9c1b;
        } else {
            goto 0x9c1b;
        }
    } else {
        if (__intrinsic()) {
            if (rbx2 * ecx3 >= 0) {
            }
            goto 0x9c1b;
        } else {
            goto 0x9c1b;
        }
    }
}

void fun_9c55() {
    if (!__intrinsic()) 
        goto 0x9c50; else 
        goto "???";
}

void fun_9c78() {
    int32_t esi1;
    int64_t rax2;
    int32_t ecx3;
    int64_t rbx4;

    esi1 = 3;
    rax2 = ecx3;
    do {
        if (__intrinsic()) {
            if (rbx4 < 0) {
                rbx4 = 0x8000000000000000;
            } else {
                rbx4 = 0x7fffffffffffffff;
            }
        } else {
            rbx4 = rbx4 * rax2;
        }
        --esi1;
    } while (esi1);
    goto 0x9c18;
}

void fun_9cba() {
    int64_t rbx1;

    if (!__intrinsic()) 
        goto 0x9c50;
    if (rbx1 >= 0) 
        goto 0x9c6c;
}

void fun_9cf8() {
    int32_t esi1;
    int64_t rax2;
    int32_t ecx3;
    int64_t rbx4;

    esi1 = 5;
    rax2 = ecx3;
    do {
        if (__intrinsic()) {
            if (rbx4 < 0) {
                rbx4 = 0x8000000000000000;
            } else {
                rbx4 = 0x7fffffffffffffff;
            }
        } else {
            rbx4 = rbx4 * rax2;
        }
        --esi1;
    } while (esi1);
    goto 0x9c18;
}

void fun_9d3a() {
    int32_t esi1;
    int64_t rax2;
    int32_t ecx3;
    int64_t rbx4;

    esi1 = 6;
    rax2 = ecx3;
    do {
        if (__intrinsic()) {
            if (rbx4 < 0) {
                rbx4 = 0x8000000000000000;
            } else {
                rbx4 = 0x7fffffffffffffff;
            }
        } else {
            rbx4 = rbx4 * rax2;
        }
        --esi1;
    } while (esi1);
    goto 0x9c18;
}

void fun_9d7a() {
    goto 0x9c1b;
}

void fun_9dc2() {
    int32_t esi1;
    int64_t rax2;
    int32_t ecx3;
    int64_t rbx4;

    esi1 = 8;
    rax2 = ecx3;
    do {
        if (__intrinsic()) {
            if (rbx4 < 0) {
                rbx4 = 0x8000000000000000;
            } else {
                rbx4 = 0x7fffffffffffffff;
            }
        } else {
            rbx4 = rbx4 * rax2;
        }
        --esi1;
    } while (esi1);
    goto 0x9c18;
}

void fun_9e02() {
    if (!__intrinsic()) 
        goto 0x9c50;
    goto 0x9c61;
}

void fun_9e20() {
    int32_t esi1;
    int64_t rbx2;

    esi1 = 4;
    do {
        if (__intrinsic()) {
            if (rbx2 < 0) {
                rbx2 = 0x8000000000000000;
            } else {
                rbx2 = 0x7fffffffffffffff;
            }
        } else {
            rbx2 = rbx2 * 0x400;
        }
        --esi1;
    } while (esi1);
}

void fun_a17c() {
    if (!__intrinsic()) 
        goto "???";
    goto 0xa18b;
}

void fun_a24c() {
    int64_t rdx1;
    int1_t zf2;

    if (__intrinsic()) 
        goto 0xa259;
    if (__intrinsic()) 
        goto "???";
    *reinterpret_cast<uint32_t*>(&rdx1) = __intrinsic();
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx1) + 4) = 0;
    zf2 = rdx1 == 0;
    if (!zf2) {
    }
    if (zf2) {
    }
    goto 0xa18b;
}

void fun_a270() {
    int32_t esi1;

    esi1 = 4;
    while (1) {
        if (__intrinsic()) {
        }
        --esi1;
        if (!esi1) 
            goto "???";
    }
}

void fun_a29c() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 63 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xa18b;
}

void fun_a2bd() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 55 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xa18b;
}

void fun_a2e1() {
    int1_t zf1;
    uint64_t rbx2;

    zf1 = rbx2 >> 54 == 0;
    if (!zf1) {
    }
    if (zf1) {
    }
    goto 0xa18b;
}

void fun_a305() {
    int32_t esi1;

    esi1 = 6;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xa294;
}

void fun_a329() {
}

void fun_a349() {
    int32_t esi1;

    esi1 = 7;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xa294;
}

void fun_a365() {
    int32_t esi1;

    esi1 = 8;
    do {
        if (__intrinsic()) {
        }
        --esi1;
    } while (esi1);
    goto 0xa294;
}

void fun_292d() {
    dev_debug = 1;
    debug = 1;
    goto 0x28e0;
}

void fun_29a1() {
    void** rax1;

    rax1 = optarg;
    format_str = rax1;
    goto 0x28e0;
}

void fun_2abc() {
    void** rax1;

    rax1 = optarg;
    suffix = rax1;
    goto 0x28e0;
}

void fun_4330() {
    __asm__("fstp st0");
}

void fun_4fb2() {
    __asm__("fstp st1");
    goto 0x44de;
}

void fun_42e8() {
    __asm__("fstp st1");
}

void fun_44dc() {
    __asm__("fstp st1");
}

void fun_45cf() {
    __asm__("fxch st0, st1");
}

void fun_47f0() {
    __asm__("fxch st0, st1");
}

void fun_497e() {
    __asm__("fxch st0, st2");
    __asm__("fldcw word [rsp+0x4c]");
    __asm__("fistp qword [rsp+0x10]");
    __asm__("fldcw word [rsp+0x4e]");
    __asm__("fild qword [rsp+0x10]");
    __asm__("faddp st2, st0");
    goto 0x4533;
}

void fun_4bad() {
    __asm__("fxch st0, st1");
    __asm__("fldcw word [rsp+0x4c]");
    __asm__("fistp qword [rsp+0x10]");
    __asm__("fldcw word [rsp+0x4e]");
    __asm__("fild qword [rsp+0x10]");
    __asm__("faddp st1, st0");
    goto 0x4543;
}

void fun_6fee() {
    goto 0x6c62;
}

void fun_71c4() {
    goto 0x717c;
}

void fun_728b() {
    goto 0x6db8;
}

void fun_72dd() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x7260;
    goto 0x6e8f;
}

void fun_730f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x726b;
        goto 0x6c90;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x710a;
        goto 0x6eab;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x7aa8;
    if (r10_8 > r15_9) 
        goto addr_71f5_9;
    addr_71fa_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x7ab3;
    goto 0x6de4;
    addr_71f5_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_71fa_10;
}

void fun_7342() {
    goto 0x6e77;
}

void fun_7710() {
    goto 0x6e77;
}

void fun_7eaf() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x6fcc;
    } else {
        goto 0x7130;
    }
}

void fun_8eb0() {
}

void fun_9bdb() {
}

void fun_9ce6() {
    if (!__intrinsic()) 
        goto 0x9c50;
    goto 0x9cc6;
}

void fun_9d82() {
}

void fun_9e30() {
    goto 0x9c38;
}

void fun_a21f() {
    if (__intrinsic()) 
        goto 0xa259; else 
        goto "???";
}

void set_fields(void** rdi, int64_t rsi, void** rdx, void** rcx, int64_t r8);

void fun_29b4() {
    void** rdi1;
    int32_t eax2;
    int1_t zf3;
    void** rdi4;
    void** rax5;
    void** rax6;
    int1_t zf7;
    void** rdi8;

    rdi1 = optarg;
    if (!rdi1) 
        goto 0x2c3e;
    eax2 = xstrtoumax(rdi1);
    if (!eax2) {
        zf3 = header == 0;
        if (!zf3) 
            goto 0x28e0;
    }
    rdi4 = optarg;
    rax5 = quote(rdi4, rdi4);
    rax6 = fun_24e0();
    fun_26d0();
    zf7 = n_frp == 0;
    if (!zf7) 
        goto 0x34a5;
    rdi8 = optarg;
    set_fields(rdi8, 1, rax6, rax5, 0xb0d6);
    goto 0x28e0;
}

void fun_2acf(int64_t rdi) {
    void** r9_2;
    void** rsi3;
    int64_t rax4;

    r9_2 = argmatch_die;
    rsi3 = optarg;
    rax4 = __xargmatch_internal("--round", rsi3, 0x10ae0, 0xcd70, 4, r9_2);
    round_style = *reinterpret_cast<uint32_t*>(0xcd70 + rax4 * 4);
    goto 0x28e0;
}

void fun_434c() {
    __asm__("fstp st1");
    goto 0x3f70;
}

void fun_4fbe() {
    __asm__("fstp st1");
    __asm__("fxch st0, st1");
    goto 0x4875;
}

void fun_42fa() {
    __asm__("fstp st1");
    goto 0x3f70;
}

void fun_4867() {
    __asm__("fxch st0, st1");
    goto 0x4875;
}

void fun_4997() {
    __asm__("fxch st0, st1");
    goto 0x48e3;
}

void fun_734c() {
    goto 0x72e7;
}

void fun_771a() {
    goto 0x723d;
}

void fun_8f10() {
    fun_24e0();
    goto fun_2740;
}

void fun_9e40() {
    goto 0x9c58;
}

void fun_2b19() {
    void** rdi1;
    void** rax2;

    rdi1 = optarg;
    rax2 = unit_to_umax(rdi1);
    to_unit_size = rax2;
    goto 0x28e0;
}

void fun_4fca() {
    __asm__("fstp st1");
    __asm__("fxch st0, st1");
    goto 0x44f0;
}

void fun_4311() {
    __asm__("fstp st1");
    goto 0x3f70;
}

void fun_486d() {
    __asm__("fstp st1");
    __asm__("fxch st0, st1");
    goto 0x4875;
}

void fun_49a5() {
    __asm__("fxch st0, st1");
    goto 0x48e3;
}

void fun_701d() {
    goto 0x6c62;
}

void fun_7358() {
    goto 0x72e7;
}

void fun_7727() {
    goto 0x728e;
}

void fun_8f50() {
    fun_24e0();
    goto fun_2740;
}

void fun_9e50() {
    goto 0x9c7b;
}

void fun_2b31() {
    void** r9_1;
    void** rsi2;
    int64_t rax3;

    r9_1 = argmatch_die;
    rsi2 = optarg;
    rax3 = __xargmatch_internal("--to", rsi2, 0x10b20, 0xcd90, 4, r9_1);
    scale_to = *reinterpret_cast<uint32_t*>(0xcd90 + rax3 * 4);
    goto 0x28e0;
}

void fun_48ad() {
    __asm__("fldz ");
    __asm__("fcomip st0, st2");
    if (0) {
        __asm__("fxch st0, st1");
        __asm__("fsub dword [rip+0x7f04]");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x10]");
        __asm__("fldcw word [rsp+0x4e]");
        __asm__("fild qword [rsp+0x10]");
        goto 0x451c;
    } else {
        __asm__("fxch st0, st1");
        __asm__("fadd dword [rip+0x84fb]");
        __asm__("fldcw word [rsp+0x4c]");
        __asm__("fistp qword [rsp+0x10]");
        __asm__("fldcw word [rsp+0x4e]");
        __asm__("fild qword [rsp+0x10]");
        goto 0x451c;
    }
}

void fun_4fd8() {
    __asm__("fstp st1");
    goto 0x48b3;
}

void fun_49b3() {
    __asm__("fxch st0, st1");
    goto 0x48e3;
}

void fun_704a() {
    goto 0x6c62;
}

void fun_7364() {
    goto 0x7260;
}

void fun_8f90() {
    fun_24e0();
    goto fun_2740;
}

void fun_9e60() {
    goto 0x9d3d;
}

void fun_2b7b() {
    void** rdi1;
    void** rax2;

    rdi1 = optarg;
    rax2 = unit_to_umax(rdi1);
    from_unit_size = rax2;
    goto 0x28e0;
}

void fun_48b1() {
    __asm__("fstp st1");
}

void fun_49c1() {
    __asm__("fxch st0, st1");
    goto 0x48e3;
}

void fun_706c() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x7a00;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x6f31;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x6f31;
    }
    if (v11) 
        goto 0x7d63;
    if (r10_12 > r15_13) 
        goto addr_7db3_8;
    addr_7db8_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x7af1;
    addr_7db3_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_7db8_9;
}

void fun_8fe0() {
    void** rax1;
    void** rbp2;
    int64_t v3;

    rax1 = fun_24e0();
    fun_2740(rbp2, 1, rax1, rbp2, 1, rax1);
    goto v3;
}

void fun_9e70() {
    goto 0x9e05;
}

void fun_2b93() {
    void** r9_1;
    void** rsi2;
    int64_t rax3;

    r9_1 = argmatch_die;
    rsi2 = optarg;
    rax3 = __xargmatch_internal("--from", rsi2, 0x10b60, 0xcda0, 4, r9_1);
    scale_from = *reinterpret_cast<uint32_t*>(0xcda0 + rax3 * 4);
    goto 0x28e0;
}

void fun_49cf() {
    __asm__("fxch st0, st1");
    goto 0x48e3;
}

void fun_9038() {
    fun_24e0();
    goto 0x9009;
}

void fun_9e78() {
    goto 0x9cfb;
}

void fun_2bdb() {
    line_delim = 0;
    goto 0x28e0;
}

void fun_49dd() {
    __asm__("fxch st0, st1");
    goto 0x48e3;
}

void fun_9070() {
    void** rax1;
    void** rbp2;
    int64_t v3;

    rax1 = fun_24e0();
    fun_2740(rbp2, 1, rax1, rbp2, 1, rax1);
    goto v3;
}

void fun_9e88() {
    goto 0x9d85;
}

void fun_2be7() {
    void** rdx1;
    int32_t eax2;

    rdx1 = optarg;
    eax2 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rdx1));
    if (*reinterpret_cast<signed char*>(&eax2)) {
        if (*reinterpret_cast<void***>(rdx1 + 1)) 
            goto 0x342b;
    }
    delimiter = eax2;
    goto 0x28e0;
}

void fun_49eb() {
    void** rdi1;
    uint32_t eax2;

    __asm__("fxch st0, st1");
    __asm__("fstp tword [rsp+0x10]");
    rdi1 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) + 0xd0);
    __asm__("fld st0");
    __asm__("fstp tword [rsp]");
    __asm__("fstp tword [rsp+0x20]");
    eax2 = fun_23f0(rdi1, 0x7f, 1, rdi1, 0x7f, 1);
    __asm__("fld tword [rsp+0x20]");
    __asm__("fld tword [rsp+0x10]");
    if (eax2 <= 0x7e) {
        __asm__("fstp st1");
    } else {
        __asm__("fstp st0");
        goto 0x4a4e;
    }
}

void fun_90e8() {
    fun_24e0();
    goto 0x90ab;
}

void fun_9e98() {
    goto 0x9dc5;
}

void fun_9ea8() {
    goto 0x9c1b;
}
