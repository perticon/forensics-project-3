#include <stdint.h>

/* /tmp/tmp2diozptm @ 0x35a0 */
 
void entry0 (int64_t arg3) {
    rdx = arg3;
    ebp = 0;
    libc_start_main (dbg.main, rsi, rsp, 0, 0, rdx);
    return _hlt ();
}

/* /tmp/tmp2diozptm @ 0x3690 */
 
int32_t dbg_simple_strtod_int (uint32_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* simple_strtod_error simple_strtod_int(char const * input_str,char ** endptr,long double * value,_Bool * negative); */
    ebp = 0;
    rbx = rdx;
    if (*(rdi) == 0x2d) {
        rdi++;
    }
    *(rcx) = bpl;
    *(rsi) = rdi;
    eax = *(rdi);
    eax -= 0x30;
    if (eax > 9) {
        goto label_4;
    }
    *(fp_stack--) = 0.0;
    rdi++;
    ecx = 0;
    r8d = 0;
    r9d = 1;
    do {
        *(fp_stack--) = 0.0;
        __asm ("fucompi st(1)");
        __asm ("setp dl");
        if (rdi != 0) {
            edx = r9d;
        }
        if (dl != 0) {
            goto label_5;
        }
        if (eax != 0) {
            goto label_5;
        }
        if (ecx > 0x12) {
label_2:
            r8d = 1;
        }
label_1:
        *((rsp + 0xc)) = eax;
        fp_stack[0] *= *(0x0000cdb4);
        *(rsi) = rdi;
        *(fp_stack--) = *((rsp + 0xc));
        rdi++;
        eax = *((rdi - 1));
        eax -= 0x30;
        fp_stack[0] += fp_stack[1];
        fp_stack++;
    } while (eax <= 9);
label_0:
    if (bpl != 0) {
        fp_stack[0] = -fp_stack[0];
    }
    ? = fp_stack[0];
    fp_stack--;
    do {
label_3:
        eax = r8d;
        return eax;
label_4:
        rdx = *(obj.decimal_point_length);
        eax = strncmp (rdi, *(obj.decimal_point), rdx);
        r8d = 3;
    } while (eax != 0);
    r8d = 0;
    *(fp_stack--) = 0.0;
    goto label_0;
label_5:
    ecx++;
    if (ecx <= 0x12) {
        goto label_1;
    }
    if (ecx != 0x1c) {
        goto label_2;
    }
    fp_stack++;
    r8d = 2;
    goto label_3;
}

/* /tmp/tmp2diozptm @ 0x3760 */
 
int64_t dbg_unit_to_umax (int64_t arg1) {
    char * end;
    uintmax_t n;
    int64_t var_1h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    rdi = arg1;
    /* uintmax_t unit_to_umax(char const * n_string); */
    rax = *(fs:0x28);
    *((rsp + 0x18)) = rax;
    eax = 0;
    rax = strlen (rdi);
    *((rsp + 8)) = 0;
    if (rax != 0) {
        rbx = rax;
        r12 = rax - 1;
        eax = *((rbp + rax - 1));
        eax -= 0x30;
        if (eax > 9) {
            goto label_1;
        }
    }
    r13d = 0;
    do {
label_0:
        eax = xstrtoumax (rbp, rsp + 8, 0xa, rsp + 0x10, "KMGTPEZY");
        if (eax != 0) {
            goto label_2;
        }
        rax = *((rsp + 8));
        if (*(rax) != 0) {
            goto label_2;
        }
        r12 = *((rsp + 0x10));
        if (r12 == 0) {
            goto label_2;
        }
        free (r13);
        rax = *((rsp + 0x18));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_3;
        }
        rax = r12;
        return rax;
label_1:
        rax = xmalloc (rbx + 2);
        r12 += rax;
        *((rsp + 8)) = r12;
        rax = memcpy (rax, rbp, rbx);
        rdi = rax;
        if (*(r12) != 0x69) {
            goto label_4;
        }
        if (rbx == 1) {
            goto label_4;
        }
        eax = *((r12 - 1));
        eax -= 0x30;
        if (eax <= 9) {
            goto label_4;
        }
        *(r12) = 0;
        r13 = rdi;
        r8 = "KMGTPEZY";
    } while (1);
label_4:
    rax = r12 + 2;
    r13 = rdi;
    r8 = "KMGTPEZY0";
    *((rsp + 8)) = rax;
    eax = 0x42;
    *((r12 + 1)) = ax;
    goto label_0;
label_2:
    free (r13);
    rax = quote (rbp, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid unit size: %s");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_3:
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x38e0 */
 
void process_field (int64_t arg_10h_2, int64_t arg_10h, int64_t arg_80h, int64_t arg_b0h, int64_t arg_d0h, int64_t arg_158h, int64_t arg1, uint32_t arg2) {
    int64_t var_1h;
    int64_t var_3h;
    int64_t var_10h_2;
    uint32_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_4ch;
    int64_t var_4eh;
    uint32_t var_5eh;
    uint32_t var_5fh;
    char ** s1;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_91h;
    int64_t var_92h;
    int64_t var_d0h;
    int64_t var_158h;
    rdi = arg1;
    rsi = arg2;
label_37:
    *(fp_stack--) = 0.0;
    goto label_32;
label_15:
    fp_stack++;
    fp_stack++;
    *(fp_stack--) = 0.0;
    fp_tmp_0 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_0;
    goto label_38;
label_10:
    fp_stack++;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = 0.0;
    fp_tmp_1 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_1;
    goto label_22;
label_27:
    fp_stack++;
    fp_stack++;
    *(fp_stack--) = 0.0;
    void (*0x454d)() ();
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x158)) = rax;
    rax = frp;
    if (rax != 0) {
        goto label_39;
    }
    goto label_40;
    do {
        if (rbp >= rdx) {
            if (rbp <= *((rax + 8))) {
                goto label_41;
            }
        }
        rax += 0x10;
label_39:
        rdx = *(rax);
    } while (rdx != -1);
    do {
        rsi = stdout;
        rdi = r12;
        r12d = 1;
        fputs_unlocked ();
label_40:
    } while (rsi != 1);
label_41:
    r13 = suffix;
    if (r13 != 0) {
        rax = strlen (r12);
        rbx = rax;
        rax = strlen (r13);
        if (rbx <= rax) {
            goto label_11;
        }
        rbx -= rax;
        rbx += r12;
        eax = strcmp (r13, rbx);
        edx = *(obj.dev_debug);
        if (eax != 0) {
            goto label_42;
        }
        *(rbx) = 0;
        if (dl != 0) {
            goto label_43;
        }
    }
label_11:
    ebx = *(r12);
    if (bl == 0) {
        goto label_44;
    }
    rax = ctype_b_loc ();
    r13 = r12;
    rax = *(rax);
    while ((*((rax + rbx*2)) & 1) != 0) {
        ebx = *((r13 + 1));
        r13++;
        if (bl == 0) {
            goto label_45;
        }
    }
label_45:
    eax = r12d;
    eax -= r13d;
label_16:
    edx = auto_padding;
    if (edx == 0) {
        goto label_46;
    }
    if (eax != 0) {
        goto label_47;
    }
    if (rbp > 1) {
        goto label_47;
    }
    *(obj.padding_width) = 0;
label_5:
    if (*(obj.dev_debug) != 0) {
        goto label_48;
    }
    r15d = scale_from;
    xmm2 = 0;
    *((rsp + 0x60)) = 0;
    *(rsp) = xmm2;
    *(fp_stack--) = *(rsp);
    eax = r15 - 3;
    r14d -= r14d;
    ? = fp_stack[0];
    fp_stack--;
    r14d &= 0x18;
    r14d += 0x3e8;
    do {
label_0:
        eax = simple_strtod_int (r13, rsp + 0x60, rsp + 0x70, rsp + 0x5e);
        ebx = eax;
        if (eax > 1) {
            goto label_49;
        }
        rbp = *((rsp + 0x60));
        rdx = *(obj.decimal_point_length);
        *((rsp + 0x20)) = rdx;
        eax = strncmp (rbp, *(obj.decimal_point), rdx);
        rdx = *((rsp + 0x20));
        *(fp_stack--) = fp_stack[?];
        *((rsp + 0x10)) = 0;
        if (eax == 0) {
            goto label_50;
        }
label_4:
        if (*(obj.dev_debug) != 0) {
            goto label_51;
        }
label_3:
        ecx = *(rbp);
        *((rsp + 0x20)) = cl;
        if (cl != 0) {
            goto label_52;
        }
        if (r15d == 4) {
            goto label_53;
        }
        ecx = *(obj.dev_debug);
        r8d = 0;
        *(fp_stack--) = 1.0;
label_2:
        if (cl != 0) {
            goto label_54;
        }
        fp_stack[0] *= fp_stack[1];
        fp_stack++;
        *(fp_stack--) = fp_stack[0];
        ? = fp_stack[0];
        fp_stack--;
label_12:
        if (*(rbp) == 0) {
            goto label_55;
        }
        ebx = 5;
        if (*(obj.inval_style) != 3) {
            rsi = rbp;
            edi = 1;
            ? = fp_stack[0];
            fp_stack--;
            ebx = 5;
            rax = quote_n ();
            rsi = r13;
            edi = 0;
            rax = quote_n ();
            edx = 5;
            r13 = rax;
            rax = dcgettext (0, "invalid suffix in input %s: %s");
            r8 = rbp;
            rcx = r13;
            eax = 0;
            error (*(obj.conv_exit_code), 0, rax);
            *(fp_stack--) = fp_stack[?];
        }
label_6:
        rax = from_unit_size;
        rdx = to_unit_size;
        if (rax == 1) {
            goto label_56;
        }
label_7:
        *((rsp + 0x20)) = rax;
        *(fp_stack--) = *((rsp + 0x20));
        if (rax < 0) {
            fp_stack[0] += *(0x0000cdbc);
        }
        fp_stack[0] *= fp_stack[1];
        fp_stack++;
        *((rsp + 0x20)) = rdx;
        *(fp_stack--) = *((rsp + 0x20));
        if (rdx < 0) {
            goto label_57;
        }
label_1:
        fp_stack[0] /= fp_stack[1];
        fp_stack++;
label_8:
        if (ebx <= 1) {
            goto label_58;
        }
        fp_stack++;
        goto label_9;
label_19:
        fp_stack++;
label_9:
        rsi = stdout;
        rdi = r12;
        r12d = 0;
        eax = fputs_unlocked ();
        void (*0x394d)() ();
label_48:
        rcx = padding_width;
        rdi = stderr;
        rdx = "setting Auto-Padding to %ld characters\n";
        eax = 0;
        esi = 1;
        fprintf_chk ();
label_46:
        r15d = scale_from;
        xmm1 = 0;
        eax = *(obj.dev_debug);
        *((rsp + 0x60)) = 0;
        *(rsp) = xmm1;
        *(fp_stack--) = *(rsp);
        edx = r15 - 3;
        r14d -= r14d;
        ? = fp_stack[0];
        fp_stack--;
        r14d &= 0x18;
        r14d += 0x3e8;
    } while (al == 0);
    rsi = decimal_point;
    edi = 1;
    rax = quote_n ();
    rsi = r13;
    edi = 0;
    rbx = rax;
    rax = quote_n ();
    rdi = stderr;
    r8 = rbx;
    r9d = 0x12;
    rcx = rax;
    rdx = "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n";
    esi = 1;
    eax = 0;
    fprintf_chk ();
    goto label_0;
label_57:
    fp_stack[0] += *(0x0000cdbc);
    goto label_1;
label_52:
    ? = fp_stack[0];
    fp_stack--;
    rax = ctype_b_loc ();
    ecx = *((rsp + 0x20));
    *(fp_stack--) = fp_stack[?];
    edi = 0;
    rsi = *(rax);
    while ((*((rsi + rax*2)) & 1) != 0) {
        ecx = *((rdx + 1));
        edi = 1;
        eax = (int32_t) cl;
        rdx = rbp;
        rbp = rbp + 1;
    }
    if (dil != 0) {
        *((rsp + 0x60)) = rdx;
    }
    esi = (int32_t) cl;
    ? = fp_stack[0];
    fp_stack--;
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x30)) = cl;
    rax = strchr ("KMGTPEZY", rsi);
    *(fp_stack--) = fp_stack[?];
    if (rax == 0) {
        goto label_59;
    }
    if (r15d == 0) {
        goto label_60;
    }
    ecx = *((rsp + 0x30));
    r8d = 0;
    rdx = *((rsp + 0x40));
    ecx -= 0x45;
    if (cl <= 0x15) {
        ecx = (int32_t) cl;
        rax = obj_CSWTCH_313;
        r8d = *((rax + rcx*4));
    }
    rbp = rdx + 1;
    *((rsp + 0x60)) = rbp;
    if (r15d == 1) {
        goto label_61;
    }
    if (r15d == 4) {
        goto label_62;
    }
label_14:
    *((rsp + 0x10)) = r14d;
    *(fp_stack--) = *((rsp + 0x10));
    ecx = *(obj.dev_debug);
label_23:
    if (r8d == 0) {
        goto label_63;
    }
    eax = r8d;
    *(fp_stack--) = fp_stack[0];
    eax--;
    if (eax == 0) {
        goto label_64;
    }
    do {
        fp_stack[0] *= fp_stack[1];
        eax--;
    } while (eax != 0);
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    goto label_2;
label_51:
    rdi = stderr;
    rdx = "  parsed numeric value: %Lf\n  input precision = %d\n";
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ecx = *((rsp + 0x20));
    esi = 1;
    ? = fp_stack[0];
    fp_stack--;
    fprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    goto label_3;
label_50:
    ? = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
    *((rsp + 0x60)) = rdi;
    ? = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = rdi;
    eax = simple_strtod_int (rbp + rdx, rsp + 0x68, rsp + 0x80, rsp + 0x5f);
    if (eax > 1) {
        goto label_65;
    }
    *(fp_stack--) = fp_stack[?];
    eax = 1;
    rdi = *((rsp + 0x10));
    if (eax == 1) {
        ebx = eax;
    }
    if (*((rsp + 0x5f)) != 0) {
        goto label_66;
    }
    rbp = *((rsp + 0x68));
    *(fp_stack--) = fp_stack[?];
    rax = rbp;
    rax -= rdi;
    *((rsp + 0x10)) = rax;
    if (ebp == edi) {
        goto label_31;
    }
    eax--;
    if (eax == 0) {
        goto label_67;
    }
    *(fp_stack--) = *(0x0000cdb4);
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        eax--;
    } while (eax != 0);
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
label_31:
    if (*((rsp + 0x5e)) == 0) {
        goto label_68;
    }
    fp_stack[0] -= fp_stack[1];
    fp_stack++;
label_25:
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    *((rsp + 0x60)) = rbp;
    goto label_4;
label_47:
    rax = strlen (r12);
    *(obj.padding_width) = rax;
    if (rax < *(obj.padding_buffer_size)) {
        goto label_5;
    }
    rdi = padding_buffer;
    rsi = rax + 1;
    *(obj.padding_buffer_size) = rsi;
    rax = xrealloc ();
    *(obj.padding_buffer) = rax;
    goto label_5;
label_55:
    if (ebx != 1) {
        goto label_6;
    }
    if (*(obj.debug) == 0) {
        goto label_6;
    }
    ? = fp_stack[0];
    fp_stack--;
    rax = quote (r13, rsi, rdx, rcx, r8);
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "large input value %s: possible precision loss");
    rcx = r13;
    eax = 0;
    eax = error (0, 0, rax);
    *(fp_stack--) = fp_stack[?];
    goto label_6;
label_65:
    ebx = eax;
label_49:
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[0];
    if (ebx > 6) {
        goto label_69;
    }
    rdx = 0x0000cba0;
    eax = ebx;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (7 cases) at 0xcba0 */
    rax = void (*rax)() ();
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    rbp = "value too large to be converted: %s";
label_13:
    if (*(obj.inval_style) == 3) {
        goto label_6;
    }
    ? = fp_stack[0];
    fp_stack--;
    rax = quote (r13, rsi, rdx, rcx, r8);
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, rbp);
    rcx = r13;
    eax = 0;
    error (*(obj.conv_exit_code), 0, rax);
    *(fp_stack--) = fp_stack[?];
    goto label_6;
label_56:
    if (rdx != 1) {
        goto label_7;
    }
    goto label_8;
label_58:
    rax = user_precision;
    *(fp_stack--) = fp_stack[?];
    fp_tmp_2 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_2;
    if (rax == -1) {
        rax = *((rsp + 0x10));
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = rax;
    if (fp_stack[0] < fp_stack[1]) {
        goto label_70;
    }
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    if (fp_stack[0] < fp_stack[1]) {
        goto label_70;
    }
    *(fp_stack--) = fp_stack[0];
    eax = 0;
    *(fp_stack--) = *(0x0000cdb4);
    while (eax >= 0) {
        fp_stack[0] /= fp_stack[2];
        fp_stack++;
        eax++;
        *(fp_stack--) = 0.0;
        *(fp_stack--) = fp_stack[2];
        fp_stack[0] = -fp_stack[0];
        fp_tmp_3 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_3;
        __asm ("fcompi st(3)");
        __asm ("fcmovbe st(0), st(2)");
        *(fp_stack--) = fp_stack[1];
        fp_tmp_4 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_4;
        __asm ("fcompi st(1)");
    }
    fp_stack++;
    fp_stack++;
    fp_stack++;
    ebp = scale_to;
    if (ebp == 0) {
        edx = eax;
        rdx += *((rsp + 0x10));
        if (rdx > 0x12) {
            goto label_71;
        }
    }
    if (eax <= 0x1a) {
        goto label_72;
    }
    ? = fp_stack[0];
    fp_stack--;
    if (*(obj.inval_style) == 3) {
        goto label_9;
    }
    edx = 5;
    rax = dcgettext (0, "value too large to be printed: '%Lg' (cannot handle values > 999Y)");
    *(fp_stack--) = fp_stack[?];
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    error (*(obj.conv_exit_code), 0, rax);
    goto label_9;
label_70:
    ebp = scale_to;
    if (ebp == 0) {
        if (*((rsp + 0x10)) > 0x12) {
            goto label_71;
        }
    }
label_72:
    r14d = grouping;
    *((rsp + 0x90)) = 0x25;
    r12 = rsp + 0x91;
    r13d = round_style;
    if (r14d != 0) {
        *((rsp + 0x91)) = 0x27;
        r12 = rsp + 0x92;
    }
    r9 = zero_padding_width;
    if (r9 != 0) {
        goto label_73;
    }
label_18:
    ecx = *(obj.dev_debug);
    if (cl != 0) {
        goto label_74;
    }
label_17:
    if (ebp == 0) {
        goto label_75;
    }
    eax = rbp - 3;
    if (eax <= 1) {
        goto label_76;
    }
    *((rsp + 0x10)) = 0x3e8;
    xmm0 = *(0x0000cdd0);
label_26:
    *(fp_stack--) = fp_stack[?];
    fp_tmp_5 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_5;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    if (fp_stack[0] < fp_stack[1]) {
        goto label_77;
    }
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    if (fp_stack[0] < fp_stack[1]) {
        goto label_77;
    }
    *(fp_stack--) = *((rsp + 0x10));
    ebx = 0;
    *(fp_stack--) = fp_stack[1];
    while (ebx >= 0) {
        fp_stack[0] /= fp_stack[1];
        ebx++;
        *(fp_stack--) = 0.0;
        *(fp_stack--) = fp_stack[1];
        fp_stack[0] = -fp_stack[0];
        fp_tmp_6 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_6;
        __asm ("fcompi st(2)");
        __asm ("fcmovbe st(0), st(1)");
        __asm ("fcompi st(2)");
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_24:
    if (cl != 0) {
        goto label_78;
    }
label_30:
    rsi = user_precision;
    if (rsi == -1) {
        goto label_79;
    }
    eax = rbx * 3;
    if (rax > rsi) {
        rax = rsi;
    }
    edi = eax;
    if (eax == 0) {
        goto label_80;
    }
    eax--;
    if (eax == 0) {
        goto label_81;
    }
    *(fp_stack--) = *(0x0000cdb4);
    edx = eax;
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        edx--;
    } while (edx != 0);
    fp_stack[0] *= fp_stack[2];
    fp_stack++;
    *(fp_stack--) = fp_stack[?];
    edx = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[2];
    dh |= 0xc;
    *((rsp + 0x4c)) = dx;
    fp_stack[0] /= fp_stack[1];
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    fp_stack[2] -= fp_stack[0];
    if (r13d > 4) {
        goto label_10;
    }
    r8 = 0x0000cbbc;
    rdx = *((r8 + r13*4));
    rdx += r8;
    /* switch table (5 cases) at 0xcbbc */
    void (*rdx)() ();
label_42:
    if (dl == 0) {
        goto label_11;
    }
    eax = fwrite (0x0000b086, 1, 0x16, *(obj.stderr));
    goto label_11;
label_54:
    fp_tmp_7 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_7;
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    ecx = r14d;
    rdi = stderr;
    rdx = "  suffix power=%d^%d = %Lf\n";
    esi = 1;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ? = fp_stack[0];
    fp_stack--;
    eax = fprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    if (*(obj.dev_debug) == 0) {
        goto label_12;
    }
    rdi = stderr;
    rdx = "  returning value: %Lf (%LG)\n";
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    esi = 1;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ? = fp_stack[0];
    fp_stack--;
    fprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    goto label_12;
label_53:
    ebx = 6;
    rbp = "missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)";
    goto label_13;
label_62:
    if (*((rdx + 1)) != 0x69) {
        goto label_82;
    }
    rbp = rdx + 2;
    *((rsp + 0x60)) = rbp;
    goto label_14;
label_69:
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    goto label_83;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_83:
    *((rsp + 0x10)) = 0;
    ebp = 0;
    goto label_13;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    rbp = "missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)";
    goto label_13;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    rbp = "invalid suffix in input: %s";
    goto label_13;
label_66:
    fp_stack++;
    *(fp_stack--) = fp_stack[?];
    goto label_84;
    fp_stack++;
label_84:
    *((rsp + 0x10)) = 0;
    ebx = 3;
    rbp = "invalid number: %s";
    goto label_13;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    rbp = "rejecting suffix in input: %s (consider using --from)";
    goto label_13;
label_75:
    rax = *((rsp + 0x10));
    esi = eax;
    if (eax == 0) {
        goto label_85;
    }
    eax--;
    if (eax == 0) {
        goto label_86;
    }
    *(fp_stack--) = *(0x0000cdb4);
    edx = eax;
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        edx--;
    } while (edx != 0);
    fp_stack[0] *= fp_stack[2];
    *(fp_stack--) = fp_stack[?];
    edx = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[1];
    dh |= 0xc;
    fp_stack[0] /= fp_stack[1];
    *((rsp + 0x4c)) = dx;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x20));
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    fp_stack[1] -= fp_stack[0];
    if (r13d > 4) {
        goto label_15;
    }
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_tmp_8 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_8;
    rax = *((rdx + r13*4));
    rax += rdx;
    /* switch table (5 cases) at 0xcbd0 */
    void (*rax)() ();
label_43:
    rax = quote (r13, rsi, 0x0000cbd0, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = 0x0000b072;
    rcx = rax;
    eax = 0;
    eax = fprintf_chk ();
    goto label_11;
label_44:
    r13 = r12;
    eax = 0;
    goto label_16;
label_74:
    ? = fp_stack[0];
    fp_stack--;
    eax = fwrite ("double_to_human:\n", 1, 0x11, *(obj.stderr));
    ecx = *(obj.dev_debug);
    *(fp_stack--) = fp_stack[?];
    goto label_17;
label_73:
    rdi = r12;
    r8 = "0%ld";
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    ecx = 0x3f;
    edx = 1;
    esi = 0x3e;
    rax = snprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    rax = (int64_t) eax;
    r12 += rax;
    goto label_18;
label_71:
    if (*(obj.inval_style) == 3) {
        goto label_19;
    }
    rbx = *((rsp + 0x10));
    ? = fp_stack[0];
    fp_stack--;
    ebp = conv_exit_code;
    edx = 5;
    if (rbx == 0) {
        goto label_87;
    }
    rax = dcgettext (0, "value/precision too large to be printed: '%Lg/%lu' (consider using --to)");
    *(fp_stack--) = fp_stack[?];
    rcx = rbx;
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    error (ebp, 0, rax);
    goto label_9;
    edi = 0;
    goto label_34;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_34:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(2)");
    if (rbx > 0) {
        goto label_88;
    }
    fp_tmp_9 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_9;
    goto label_28;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_tmp_10 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_10;
label_28:
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_tmp_11 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_11;
    __asm ("fcompi st(1)");
    if (rbx > 0) {
        fp_stack++;
        rax = *((rsp + 0x10));
        rax++;
        *((rsp + 0x10)) = rax;
        *(fp_stack--) = *((rsp + 0x10));
    }
label_21:
    fp_stack[0] += fp_stack[1];
    fp_stack++;
    if (edi == 0) {
        void (*0x4543)() ();
    }
    eax = edi;
    *(fp_stack--) = *(0x0000cdb4);
    eax--;
    if (eax == 0) {
        void (*0x4a91)() ();
    }
label_22:
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        eax--;
    } while (eax != 0);
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    fp_tmp_12 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_12;
    goto label_89;
    fp_tmp_13 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_13;
label_89:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(2)");
    if (eax > 0) {
        goto label_90;
    }
    fp_tmp_14 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_14;
    fp_stack[0] += *(0x0000cdc0);
    goto label_91;
    fp_tmp_15 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_15;
label_91:
    *(rsp) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
label_20:
    fp_stack[0] += fp_stack[1];
    fp_stack++;
    if (esi == 0) {
        goto label_32;
    }
    eax = esi;
    *(fp_stack--) = *(0x0000cdb4);
    eax--;
    if (eax == 0) {
        goto label_92;
    }
label_38:
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        eax--;
    } while (eax != 0);
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
label_32:
    ebx = *((rsp + 0x10));
    if (cl != 0) {
        fp_tmp_16 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_16;
        ? = fp_stack[0];
        fp_stack--;
        rax = "  no scaling, returning value: %.*Lf\n";
        ecx = ebx;
        rdx = "  no scaling, returning (grouped) value: %'.*Lf\n";
        rdi = stderr;
        esi = 1;
        if (r14d == 0) {
            rdx = rax;
        }
        eax = 0;
        *(fp_stack--) = fp_stack[0];
        ? = fp_stack[0];
        fp_stack--;
        ? = fp_stack[0];
        fp_stack--;
        eax = fprintf_chk ();
        *(fp_stack--) = fp_stack[?];
        *(fp_stack--) = fp_stack[?];
    } else {
        fp_tmp_17 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_17;
    }
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    r9d = ebx;
    ecx = 0x80;
    edx = 1;
    esi = 0x80;
    *(r12) = 0x664c2a2e;
    *((r12 + 4)) = 0;
    r12 = rsp + 0xd0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    r8 = rsp + 0xa0;
    rdi = r12;
    ? = fp_stack[0];
    fp_stack--;
    eax = snprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    if (eax > 0x7f) {
        goto label_93;
    }
    fp_stack++;
    *(fp_stack--) = fp_stack[?];
    ebx = *(obj.dev_debug);
    fp_tmp_18 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_18;
    goto label_94;
    fp_tmp_19 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_19;
label_94:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(2)");
    if (eax > 0x7f) {
        goto label_95;
    }
    fp_tmp_20 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_20;
    goto label_96;
    fp_tmp_21 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_21;
label_96:
    *(fp_stack--) = fp_stack[0];
    *(rsp) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
    fp_tmp_22 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_22;
    __asm ("fcompi st(1)");
    if (eax <= 0x7f) {
        goto label_20;
    }
    fp_stack++;
    rax = *(rsp);
    rax++;
    *(rsp) = rax;
    *(fp_stack--) = *(rsp);
    goto label_20;
    fp_tmp_23 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_23;
    goto label_97;
label_95:
    fp_tmp_24 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_24;
label_97:
    fp_stack[0] = -fp_stack[0];
    edx = 0;
    *(fp_stack--) = fp_stack[0];
    *(rsp) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
    fp_tmp_25 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_25;
    rax = *(rsp);
    __asm ("fcompi st(1)");
    fp_stack++;
    dl = (rax > 0) ? 1 : 0;
    rax += rdx;
    rax = -rax;
    *(rsp) = rax;
    *(fp_stack--) = *(rsp);
    goto label_20;
    fp_tmp_26 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_26;
    edi = 0;
    goto label_35;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_tmp_27 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_27;
    goto label_35;
label_88:
    fp_tmp_28 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_28;
label_35:
    fp_stack[0] = -fp_stack[0];
    edx = 0;
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_tmp_29 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_29;
    rax = *((rsp + 0x10));
    __asm ("fcompi st(1)");
    fp_stack++;
    dl = (rax > 0) ? 1 : 0;
    rax += rdx;
    rax = -rax;
    *((rsp + 0x10)) = rax;
    *(fp_stack--) = *((rsp + 0x10));
    goto label_21;
    edi = 0;
    goto label_36;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_36:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(2)");
    if (rax > 0) {
        goto label_98;
    }
    fp_tmp_30 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_30;
    fp_stack[0] += *(0x0000cdc0);
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    goto label_21;
    fp_tmp_31 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_31;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] += fp_stack[2];
    fp_stack++;
    goto label_22;
label_61:
    ecx = *(obj.dev_debug);
    if (*((rdx + 1)) == 0x69) {
        goto label_99;
    }
    *((rsp + 0x10)) = r14d;
    *(fp_stack--) = *((rsp + 0x10));
    goto label_23;
label_60:
    ebx = 4;
    rbp = "rejecting suffix in input: %s (consider using --from)";
    goto label_13;
label_77:
    *(fp_stack--) = fp_stack[0];
    ebx = 0;
    goto label_24;
label_68:
    fp_stack[0] += fp_stack[1];
    fp_stack++;
    goto label_25;
label_59:
    ebx = 5;
    rbp = "invalid suffix in input: %s";
    goto label_13;
label_64:
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    r8d = 1;
    goto label_2;
label_76:
    *((rsp + 0x10)) = section..dynsym;
    xmm0 = *(0x0000cdc8);
    goto label_26;
label_29:
    fp_stack++;
label_80:
    *(fp_stack--) = fp_stack[?];
    eax = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[1];
    fp_stack[0] /= fp_stack[1];
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    fp_stack[1] -= fp_stack[0];
    if (r13d > 4) {
        goto label_27;
    }
    rdx = 0x0000cc08;
    eax = r13d;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (5 cases) at 0xcc08 */
    void (*rax)() ();
    fp_tmp_32 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_32;
    edi = r13d;
    goto label_28;
    fp_tmp_33 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_33;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] += fp_stack[1];
    fp_stack++;
    void (*0x4543)() ();
label_79:
    *(fp_stack--) = 0.0;
    *(fp_stack--) = fp_stack[1];
    fp_stack[0] = -fp_stack[0];
    fp_tmp_34 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_34;
    __asm ("fcompi st(2)");
    __asm ("fcmovbe st(0), st(1)");
    *(fp_stack--) = *(0x0000cdb4);
    *(fp_stack--) = fp_stack[0];
    __asm ("fcompi st(2)");
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    if (rax <= 0) {
        goto label_29;
    }
label_33:
    fp_stack[1] *= fp_stack[0];
    *(fp_stack--) = fp_stack[?];
    eax = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[2];
    ah |= 0xc;
    fp_stack[0] /= fp_stack[1];
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    fp_stack[2] -= fp_stack[0];
    if (r13d > 4) {
        void (*0x27a0)() ();
    }
    rdx = 0x0000cc1c;
    eax = r13d;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (5 cases) at 0xcc1c */
    void (*rax)() ();
label_78:
    fp_tmp_35 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_35;
    ? = fp_stack[0];
    fp_stack--;
    ecx = ebx;
    esi = 1;
    rdi = stderr;
    rdx = "  scaled value to %Lf * %0.f ^ %u\n";
    eax = 1;
    *((rsp + 0x20)) = xmm0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ? = fp_stack[0];
    fp_stack--;
    fprintf_chk ();
    ecx = *(obj.dev_debug);
    xmm0 = *((rsp + 0x10));
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    goto label_30;
label_99:
    rbp = rdx + 2;
    *((rsp + 0x60)) = rbp;
    if (cl == 0) {
        *(fp_stack--) = *(0x0000cdb8);
        r14d = section..dynsym;
        goto label_23;
label_85:
        *(fp_stack--) = fp_stack[?];
        eax = *((rsp + 0x4e));
        *(fp_stack--) = fp_stack[1];
        fp_stack[0] /= fp_stack[1];
        ah |= 0xc;
        *((rsp + 0x4c)) = ax;
        *((rsp + 0x20)) = fp_stack[0];
        fp_stack--;
        *(fp_stack--) = *((rsp + 0x20));
        fp_stack[0] *= fp_stack[1];
        fp_stack++;
        *(fp_stack--) = fp_stack[1];
        fp_stack[0] -= fp_stack[1];
        if (r13d > 4) {
            goto label_100;
        }
        rdx = 0x0000cc30;
        rax = *((rdx + r13*4));
        rax += rdx;
        /* switch table (5 cases) at 0xcc30 */
        void (*rax)() ();
label_87:
        rax = dcgettext (0, "value too large to be printed: '%Lg' (consider using --to)");
        *(fp_stack--) = fp_stack[?];
        eax = 0;
        ? = fp_stack[0];
        fp_stack--;
        eax = error (ebp, 0, rax);
        goto label_9;
    }
    ecx = section..dynsym;
    esi = 1;
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    rdi = stderr;
    rdx = "  Auto-scaling, found 'i', switching to base %d\n";
    r14d = section..dynsym;
    *((rsp + 0x10)) = r8d;
    fprintf_chk ();
    r8d = *((rsp + 0x10));
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = *(0x0000cdb8);
    ecx = *(obj.dev_debug);
    goto label_23;
label_67:
    fp_stack[0] /= *(0x0000cdb4);
    goto label_31;
label_92:
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    goto label_32;
label_98:
    fp_tmp_36 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_36;
    fp_stack[0] -= *(0x0000cdc0);
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    goto label_21;
label_90:
    fp_tmp_37 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_37;
    fp_stack[0] -= *(0x0000cdc0);
    *(rsp) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
    goto label_20;
label_86:
    *(fp_stack--) = fp_stack[?];
    eax = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[1];
    fp_stack[0] *= *(0x0000cdb4);
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *(fp_stack--) = fp_stack[0];
    fp_stack[0] /= fp_stack[2];
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x20));
    fp_stack[0] *= fp_stack[2];
    fp_stack++;
    fp_stack[0] -= fp_stack[1];
    if (r13d <= 4) {
        rdx = 0x0000cc44;
        rax = *((rdx + r13*4));
        rax += rdx;
        /* switch table (5 cases) at 0xcc44 */
        void (*rax)() ();
label_82:
        *((rsp + 0x10)) = 0;
        ebx = 6;
        rbp = "missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)";
        goto label_13;
label_93:
        edx = 5;
        ? = fp_stack[0];
        fp_stack--;
        rax = dcgettext (0, "failed to prepare value '%Lf' for printing");
        *(fp_stack--) = fp_stack[?];
        eax = 0;
        ? = fp_stack[0];
        fp_stack--;
        error (1, 0, rax);
label_63:
        fp_stack++;
        *((rsp + 0x10)) = 0;
        *(fp_stack--) = 1.0;
        goto label_2;
label_81:
        *(fp_stack--) = *(0x0000cdb4);
        goto label_33;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        edi = 1;
        goto label_34;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        fp_tmp_38 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_38;
        edi = r13d;
        goto label_35;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        fp_tmp_39 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_39;
        edi = 1;
        goto label_28;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        edi = 1;
        goto label_36;
label_100:
        fp_stack++;
        fp_stack++;
        goto label_37;
    }
    fp_stack++;
    fp_stack++;
    goto label_37;
}

/* /tmp/tmp2diozptm @ 0x27a0 */
 
void process_field_cold (int64_t arg_10h_2, int64_t arg_10h, int64_t arg_80h, int64_t arg_b0h, int64_t arg_d0h, int64_t arg_158h, int64_t arg7) {
    xmm0 = arg7;
    /* [16] -r-x section size 32850 named .text */
    fp_stack++;
    fp_stack++;
    fp_stack++;
    *(fp_stack--) = 0.0;
    while (rdi == 0) {
label_0:
        rax = *((rsp + 0x158));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_14;
        }
        eax = r12d;
        return rax;
label_5:
        *(fp_stack--) = 0.0;
        __asm ("fcompi st(1)");
        if (rax > 0) {
            goto label_15;
        }
        *((rsp + 0x10)) = xmm0;
        *(fp_stack--) = *((rsp + 0x10));
        fp_tmp_0 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_0;
        if (fp_stack[0] >= fp_stack[1]) {
            goto label_16;
label_6:
            fp_stack++;
            fp_tmp_1 = fp_stack[1];
            fp_stack[1] = fp_stack[0];
            fp_stack[0] = fp_tmp_1;
label_16:
            ebx++;
            fp_stack[1] /= fp_stack[0];
            fp_stack++;
        } else {
            fp_stack[1] = fp_stack[0];
            fp_stack--;
        }
        *(fp_stack--) = 0.0;
        fp_tmp_2 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_2;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        if (fp_stack[0] == fp_stack[1]) {
            goto label_17;
        }
        r9d = 0;
        if (fp_stack[0] != fp_stack[1]) {
            goto label_17;
        }
label_7:
        if (cl != 0) {
            goto label_18;
        }
label_8:
        edi = 0x7325;
        *(r12) = 0x664c2a2e;
        *((r12 + 4)) = di;
        if (rsi != -1) {
            r9d = esi;
        }
        *((r12 + 6)) = 0;
        if (ebx > 8) {
            goto label_19;
        }
        rdx = 0x0000cbe4;
        eax = ebx;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (9 cases) at 0xcbe4 */
        void (*rax)() ();
label_10:
        rbp = suffix;
        if (rbp != 0) {
label_1:
            ? = fp_stack[0];
            fp_stack--;
            rax = strlen (r12);
            edx = 0x7f;
            rsi = rbp;
            rdi = r12;
            rdx -= rax;
            ecx = 0x80;
            strncat_chk ();
            *(fp_stack--) = fp_stack[?];
        }
        if (bl != 0) {
            ? = fp_stack[0];
            fp_stack--;
            rax = quote (r12, rsi, rdx, rcx, r8);
            *(fp_stack--) = fp_stack[?];
            rdi = stderr;
            rcx = rax;
            rdx = "formatting output:\n  value: %Lf\n  humanized: %s\n";
            esi = 1;
            eax = 0;
            ? = fp_stack[0];
            fp_stack--;
            fprintf_chk ();
        } else {
            fp_stack++;
        }
label_2:
        rbx = padding_width;
        rax = strlen (r12);
        rdx = padding_buffer_size;
        rdi = padding_buffer;
        if (rbx != 0) {
            if (rbx > rax) {
                goto label_20;
            }
        }
        rcx = rax + 1;
        if (rcx >= rdx) {
            rsi = rax + 2;
            *(obj.padding_buffer_size) = rsi;
            rax = xrealloc ();
            *(obj.padding_buffer) = rax;
        }
        strcpy (rax, r12);
label_9:
        rdi = format_str_prefix;
        rsi = stdout;
        if (rdi != 0) {
            fputs_unlocked ();
            rsi = stdout;
        }
        rdi = padding_buffer;
        r12d = 1;
        fputs_unlocked ();
        rdi = format_str_suffix;
    }
    rsi = stdout;
    fputs_unlocked ();
    goto label_0;
    fp_tmp_3 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_3;
    rax = 0x0000b066;
label_3:
    ? = fp_stack[0];
    fp_stack--;
    ecx = 0x80;
    edx = 1;
    r12 = rsp + 0xd0;
    esi = 0x7f;
    rdi = r12;
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    r8 = rsp + 0xb0;
    ? = fp_stack[0];
    fp_stack--;
    eax = snprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    if (eax > 0x7e) {
        goto label_21;
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    if (ebx != 0) {
        if (ebp == 4) {
            goto label_22;
        }
        goto label_12;
label_4:
        fp_stack[1] = fp_stack[0];
        fp_stack--;
    }
label_12:
    ebx = *(obj.dev_debug);
    if (bl != 0) {
        goto label_23;
    }
    rbp = suffix;
    if (rbp != 0) {
        goto label_1;
    }
    fp_stack++;
    goto label_2;
    fp_tmp_4 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_4;
    rax = "KMGTPEZ\xe7\x89\xb4\xe6\xb5\xa9\xe6\xa5\xad\xe6\x9d\xae\xe7\x8c\xa0\xe6\x99\xb5\xe6\xa5\xa6\xe2\x81\xb8\xe7\x8c\xa5\n\xe6\xbd\xae\xe7\x98\xa0\xe6\xb1\xa1\xe6\x91\xa9\xe7\x8c\xa0\xe6\x99\xb5\xe6\xa5\xa6\xe2\x81\xb8\xe6\xbd\xa6\xe6\xb9\xb5";
    goto label_3;
    fp_tmp_5 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_5;
    rax = 0x0000b070;
    goto label_3;
    fp_tmp_6 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_6;
    rax = 0x0000b06e;
    goto label_3;
    fp_tmp_7 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_7;
    rax = 0x0000b06c;
    goto label_3;
    fp_tmp_8 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_8;
    rax = 0x0000b06a;
    goto label_3;
    fp_tmp_9 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_9;
    rax = 0x0000b00b;
    goto label_3;
    fp_tmp_10 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_10;
    rax = 0x0000b068;
    goto label_3;
    fp_tmp_11 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_11;
    ? = fp_stack[0];
    fp_stack--;
    ecx = 0x80;
    edx = 1;
    r12 = rsp + 0xd0;
    rax = 0x0000b0d6;
    esi = 0x7f;
    rdi = r12;
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    r8 = rsp + 0xb0;
    ? = fp_stack[0];
    fp_stack--;
    eax = snprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    if (eax <= 0x7e) {
        goto label_4;
    }
    fp_stack++;
    goto label_24;
label_21:
    fp_stack++;
label_24:
    edx = 5;
    ? = fp_stack[0];
    fp_stack--;
    rax = dcgettext (0, "failed to prepare value '%Lf' for printing");
    *(fp_stack--) = fp_stack[?];
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    error (1, 0, rax);
    fp_tmp_12 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_12;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] += fp_stack[2];
    fp_stack++;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    goto label_5;
label_15:
    *((rsp + 0x10)) = xmm0;
    *(fp_stack--) = *((rsp + 0x10));
    *(fp_stack--) = fp_stack[1];
    fp_stack[0] = -fp_stack[0];
    if (fp_stack[0] >= fp_stack[1]) {
        goto label_6;
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_13:
    fp_tmp_13 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_13;
label_11:
    *(fp_stack--) = *(0x0000cdb4);
    al = (ebx != 0) ? 1 : 0;
    r9d = 0;
    __asm ("fcompi st(2)");
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    r9b = (ebx > 0) ? 1 : 0;
    r9d &= eax;
    goto label_7;
label_18:
    fp_tmp_14 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_14;
    ? = fp_stack[0];
    fp_stack--;
    esi = 1;
    ecx = ebx;
    rdi = stderr;
    rdx = "  after rounding, value=%Lf * %0.f ^ %u\n";
    eax = 1;
    *((rsp + 0x20)) = r9d;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ? = fp_stack[0];
    fp_stack--;
    fprintf_chk ();
    rsi = user_precision;
    r9d = *((rsp + 0x10));
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    goto label_8;
label_20:
    *((rsp + 0x80)) = rbx;
    mbsalign (r12, rdi, rdx, rsp + 0x80, *(obj.padding_alignment), 2);
    if (*(obj.dev_debug) == 0) {
        goto label_9;
    }
    rax = quote (*(obj.padding_buffer), rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = "  After padding: %s\n";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_9;
label_23:
    ? = fp_stack[0];
    fp_stack--;
    rax = quote (r12, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = "  returning value: %s\n";
    rcx = rax;
    eax = 0;
    rax = fprintf_chk ();
    ebx = *(obj.dev_debug);
    *(fp_stack--) = fp_stack[?];
    goto label_10;
label_17:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(1)");
    if (*(obj.dev_debug) <= 0) {
        *(fp_stack--) = fp_stack[0];
        goto label_11;
label_22:
        rax = (int64_t) eax;
        edx = 0x7f;
        ecx = 0x80;
        rdi = r12;
        rdx -= rax;
        rsi = 0x0000b396;
        ? = fp_stack[0];
        fp_stack--;
        strncat_chk ();
        *(fp_stack--) = fp_stack[?];
        goto label_12;
label_19:
        fp_tmp_15 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_15;
        rax = "(error)";
        goto label_3;
label_14:
        stack_chk_fail ();
    }
    *(fp_stack--) = fp_stack[0];
    fp_stack[0] = -fp_stack[0];
    goto label_13;
}

/* /tmp/tmp2diozptm @ 0x5000 */
 
int64_t dbg_process_line (int64_t arg1, int64_t arg2) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    /* int process_line(char * line,_Bool newline); */
    r15d = 0;
    r15++;
    r13d = esi;
    r12d = 0;
    edx = delimiter;
    r14d = *(rdi);
    if (edx == 0x80) {
        goto label_8;
    }
label_0:
    eax = (int32_t) r14b;
    if (eax == edx) {
        goto label_9;
    }
    if (r14b == 0) {
        goto label_3;
    }
    rbx = rdi;
    do {
        eax = *((rbx + 1));
        rbx++;
        if (al == 0) {
            goto label_3;
        }
    } while (eax != edx);
label_1:
    *(rbx) = 0;
    rsi = r15;
    al = process_field ();
    edx = delimiter;
    esi = 0x20;
    rdi = stdout;
    if (al == 0) {
    }
    rax = *((rdi + 0x28));
    if (edx != 0x80) {
        esi = edx;
    }
    if (rax >= *((rdi + 0x30))) {
        goto label_10;
    }
    rcx = rax + 1;
    *((rdi + 0x28)) = rcx;
    *(rax) = sil;
label_6:
    rdi = rbx + 1;
    r15++;
    r14d = *(rdi);
    if (edx != 0x80) {
        goto label_0;
    }
label_8:
    if (r14b == 0) {
        goto label_3;
    }
    *((rsp + 8)) = rdi;
    rax = ctype_b_loc ();
    rdi = *((rsp + 8));
    rdx = *(rax);
    rbx = rdi;
label_2:
    eax = (int32_t) r14b;
    if ((*((rdx + rax*2)) & 1) != 0) {
        goto label_11;
    }
    if (r14b == 0xa) {
        goto label_11;
    }
    eax = *(rbx);
    if (al == 0) {
        goto label_3;
    }
label_4:
    ecx = (int32_t) al;
    if ((*((rdx + rcx*2)) & 1) == 0) {
        if (al != 0xa) {
            goto label_12;
        }
    }
    r14d = *(rbx);
label_5:
    if (r14b != 0) {
        goto label_1;
    }
label_3:
    rsi = r15;
    al = process_field ();
    eax = 0;
    if (al == 0) {
    }
    r12d = (int32_t) bpl;
    if (r13b != 0) {
        rdi = stdout;
        edx = *(obj.line_delim);
        rax = *((rdi + 0x28));
        if (rax >= *((rdi + 0x30))) {
            goto label_13;
        }
        rcx = rax + 1;
        *((rdi + 0x28)) = rcx;
        *(rax) = dl;
    }
label_7:
    eax = r12d;
    return rax;
label_11:
    r14d = *((rbx + 1));
    rbx++;
    if (r14b != 0) {
        goto label_2;
    }
    goto label_3;
label_12:
    eax = *((rbx + 1));
    rbx++;
    if (al != 0) {
        goto label_4;
    }
    goto label_3;
label_9:
    rbx = rdi;
    goto label_5;
label_10:
    esi = (int32_t) sil;
    overflow ();
    edx = delimiter;
    goto label_6;
label_13:
    esi = (int32_t) dl;
    overflow ();
    goto label_7;
}

/* /tmp/tmp2diozptm @ 0x6a40 */
 
uint64_t gettext_quote_part_0 (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    ebx = esi;
    rax = locale_charset ();
    edx = *(rax);
    edx &= 0xffffffdf;
    if (dl == 0x55) {
        edx = *((rax + 1));
        edx &= 0xffffffdf;
        if (dl != 0x54) {
            goto label_0;
        }
        edx = *((rax + 2));
        edx &= 0xffffffdf;
        if (dl != 0x46) {
            goto label_0;
        }
        if (*((rax + 3)) != 0x2d) {
            goto label_0;
        }
        if (*((rax + 4)) != 0x38) {
            goto label_0;
        }
        if (*((rax + 5)) != 0) {
            goto label_0;
        }
        rax = 0x0000d06f;
        rdx = 0x0000d060;
        if (*(rbp) != 0x60) {
            rax = rdx;
        }
        return rax;
    }
    if (dl != 0x47) {
        goto label_0;
    }
    edx = *((rax + 1));
    edx &= 0xffffffdf;
    while (*((rax + 2)) != 0x31) {
label_0:
        rax = 0x0000d067;
        rdx = 0x0000d069;
        if (ebx != 9) {
            rax = rdx;
        }
        return rax;
    }
    if (*((rax + 3)) != 0x38) {
        goto label_0;
    }
    if (*((rax + 4)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 5)) != 0x33) {
        goto label_0;
    }
    if (*((rax + 6)) != 0x30) {
        goto label_0;
    }
    if (*((rax + 7)) != 0) {
        goto label_0;
    }
    rax = 0x0000d06b;
    rdx = 0x0000d064;
    if (*(rbp) != 0x60) {
        rax = rdx;
    }
    return rax;
}

/* /tmp/tmp2diozptm @ 0xa5d0 */
 
uint64_t dbg_locale_charset (void) {
    /* char const * locale_charset(); */
    rax = nl_langinfo (0xe);
    if (rax != 0) {
        rdx = "ASCII";
        if (*(rax) == 0) {
            rax = rdx;
        }
        return rax;
    }
    rax = "ASCII";
    return rax;
}

/* /tmp/tmp2diozptm @ 0x2660 */
 
void nl_langinfo (void) {
    __asm ("bnd jmp qword [reloc.nl_langinfo]");
}

/* /tmp/tmp2diozptm @ 0x6b20 */
 
int64_t quotearg_buffer_restyled (int64_t arg_100h, int64_t arg_108h, int64_t arg_110h, int64_t arg1, int64_t arg2, char * arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t * var_8h;
    int64_t var_10h;
    char * var_18h;
    uint32_t var_27h;
    size_t canary;
    size_t * var_30h;
    size_t * var_38h;
    size_t * var_40h;
    size_t var_48h;
    size_t s2;
    uint32_t var_58h;
    uint32_t var_60h;
    size_t * var_68h;
    size_t * var_70h;
    int64_t var_78h;
    uint32_t var_7ch;
    size_t * var_7dh;
    size_t * var_7eh;
    size_t * var_7fh;
    size_t * var_80h;
    char * s;
    int64_t var_90h;
    int64_t var_98h;
    wint_t wc;
    int64_t var_b0h;
    int64_t var_b8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    r11 = rcx;
    r14 = rdi;
    r13 = rsi;
    rax = *((rsp + 0x100));
    *((rsp + 0x98)) = rdi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rax;
    rax = *((rsp + 0x108));
    *((rsp + 0x78)) = r9d;
    *((rsp + 0x90)) = rax;
    rax = *((rsp + 0x110));
    *((rsp + 0x88)) = rax;
    rax = *(fs:0x28);
    *((rsp + 0xb8)) = rax;
    eax = 0;
label_0:
    *(rsp) = r11;
    rax = ctype_get_mb_cur_max ();
    ebx = *((rsp + 0x78));
    *((rsp + 0x60)) = rax;
    ebx &= 2;
    if (ebp > 0xa) {
        void (*0x27d9)() ();
    }
    rdx = 0x0000d0e0;
    eax = ebp;
    r11 = *(rsp);
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (11 cases) at 0xd0e0 */
    void (*rax)() ();
    if (ebp != 0xa) {
        r12 = 0x0000d073;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        if (rax == r12) {
            goto label_62;
        }
label_54:
        r12 = 0x0000d069;
        edx = 5;
        *(rsp) = r11;
        rax = dcgettext (0, r12);
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        if (rax == r12) {
            goto label_63;
        }
    }
label_53:
    r15d = 0;
    if (ebx == 0) {
        goto label_64;
    }
label_50:
    rbx = *((rsp + 0x88));
    *(rsp) = r11;
    r12d = 1;
    rsp + 0x27 = (ebx != 0) ? 1 : 0;
    rax = strlen (rbx);
    *((rsp + 0x50)) = rbx;
    r11 = *(rsp);
    *((rsp + 0x28)) = rax;
label_4:
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
label_3:
    *((rsp + 8)) = r12b;
    r10 = r13;
    r12 = r14;
    r13d = esi;
    r14d = ebp;
label_59:
    r9d = 0;
    do {
label_21:
        bpl = (r11 != r9) ? 1 : 0;
        if (r11 == -1) {
            rax = *((rsp + 0x18));
            bpl = (*((rax + r9)) != 0) ? 1 : 0;
        }
        if (bpl == 0) {
            goto label_65;
        }
        rdi = *((rsp + 0x18));
        al = (r14d != 2) ? 1 : 0;
        al &= *((rsp + 8));
        rbx = rdi + r9;
        r8d = eax;
        if (al == 0) {
            goto label_66;
        }
        rax = *((rsp + 0x28));
        if (rax == 0) {
            goto label_67;
        }
        rdx = r9 + rax;
        if (r11 == -1) {
            if (rax <= 1) {
                goto label_68;
            }
            *((rsp + 0x48)) = r10;
            *((rsp + 0x40)) = r9;
            *((rsp + 0x38)) = rdx;
            *((rsp + 0x30)) = r8b;
            rax = strlen (rdi);
            r10 = *((rsp + 0x48));
            r9 = *((rsp + 0x40));
            rdx = *((rsp + 0x38));
            r8d = *((rsp + 0x30));
            r11 = rax;
        }
label_68:
        if (rdx > r11) {
            goto label_67;
        }
        *((rsp + 0x48)) = r11;
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        eax = memcmp (rbx, *((rsp + 0x50)), *((rsp + 0x28)));
        r8d = *((rsp + 0x30));
        r9 = *((rsp + 0x38));
        r10 = *((rsp + 0x40));
        r11 = *((rsp + 0x48));
        if (eax != 0) {
            goto label_67;
        }
        if (*((rsp + 0x27)) != 0) {
            goto label_69;
        }
        ebx = *(rbx);
        if (bl > 0x3f) {
            goto label_70;
        }
        if (bl < 0) {
            goto label_16;
        }
        if (bl > 0x3f) {
            goto label_16;
        }
        rdx = 0x0000d10c;
        eax = (int32_t) bl;
        rax = *((rdx + rax*4));
        rax += rdx;
        /* switch table (64 cases) at 0xd10c */
        eax = void (*rax)() ();
        ecx = r8d;
label_15:
        eax = 0;
        r8d = ecx;
        ecx = ebx;
label_1:
        rsi = *((rsp + 0x10));
        if (rsi != 0) {
            edx = ecx;
            dl >>= 5;
            edx = (int32_t) dl;
            edx = *((rsi + rdx*4));
            edx >>= cl;
            edx &= 1;
            if (edx != 0) {
                goto label_2;
            }
        }
label_12:
        if (r8b == 0) {
            goto label_71;
        }
label_2:
        dl = (r14d == 2) ? 1 : 0;
        eax = edx;
        if (*((rsp + 0x27)) != 0) {
            goto label_72;
        }
label_6:
        eax = r13d;
        eax ^= 1;
        al &= dl;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rdx = r15 + 1;
            if (r10 > rdx) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rdx = r15 + 2;
            if (r10 > rdx) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
label_8:
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
        r9++;
label_33:
        if (r15 < r10) {
            *((r12 + r15)) = cl;
        }
        eax = *(rsp);
        r15++;
        esi = 0;
        if (bpl == 0) {
            eax = esi;
        }
        *(rsp) = al;
    } while (1);
label_29:
    if (bl == 0x7c) {
label_24:
        ebp = 0;
label_13:
        al = (r14d == 2) ? 1 : 0;
        if (r14d != 2) {
            goto label_73;
        }
        if (*((rsp + 0x27)) == 0) {
            goto label_73;
        }
label_18:
        r14 = r12;
        r12d = *((rsp + 8));
        r13 = r10;
        eax = r12d;
label_40:
        if (al != 0) {
            goto label_44;
        }
label_7:
        *((rsp + 0x10)) = 0;
        goto label_0;
label_30:
        r8d = 0;
    }
label_16:
    if (*((rsp + 0x60)) != 1) {
        goto label_74;
    }
label_26:
    *((rsp + 0x48)) = r11;
    *((rsp + 0x40)) = r10;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x30)) = r8b;
    rax = ctype_b_loc ();
    r8d = *((rsp + 0x30));
    r9 = *((rsp + 0x38));
    edi = 1;
    rdx = rax;
    eax = (int32_t) bl;
    r10 = *((rsp + 0x40));
    r11 = *((rsp + 0x48));
    rdx = *(rdx);
    bpl = ((*((rdx + rax*2 + 1)) & 0x40) != 0) ? 1 : 0;
    dl = ((*((rdx + rax*2 + 1)) & 0x40) == 0) ? 1 : 0;
    dl &= *((rsp + 8));
label_27:
    if (dl != 0) {
        goto label_75;
    }
label_23:
    al = (r14d == 2) ? 1 : 0;
label_73:
    ecx = ebx;
label_5:
    edx = *((rsp + 8));
    edx ^= 1;
    al |= dl;
    if (al == 0) {
        goto label_1;
    }
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
label_14:
    if (r8b != 0) {
        goto label_2;
    }
label_71:
    eax ^= 1;
    r9++;
    eax &= r13d;
    goto label_47;
    if (ebx != 0) {
        goto label_76;
    }
label_57:
    rax = 0x0000d067;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    *((rsp + 0x58)) = 0;
    if (r13 == 0) {
        goto label_3;
    }
    *(r14) = 0x22;
    goto label_3;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    do {
        rax = 0x0000d069;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
        *((rsp + 0x27)) = 0;
        r12d = 1;
        r15d = 0;
        *((rsp + 0x28)) = 0;
        *((rsp + 0x50)) = 0;
        goto label_4;
label_60:
        *((rsp + 0x27)) = 1;
        r12d = 0;
    } while (1);
    rax = 0x0000d067;
    *((rsp + 0x27)) = 1;
    r15d = 0;
    r12d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
    *((rsp + 0x27)) = 0;
    r12d = 0;
    r15d = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x50)) = 0;
    goto label_4;
    r8d = 0;
    if (r14d == 2) {
        goto label_77;
    }
    if (r14d == 5) {
        if ((*((rsp + 0x78)) & 4) == 0) {
            goto label_52;
        }
        rdx = r9 + 2;
        if (rdx >= r11) {
            goto label_52;
        }
        rax = *((rsp + 0x18));
        if (*((rax + r9 + 1)) == 0x3f) {
            goto label_78;
        }
    }
label_52:
    eax = 0;
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
    r8d = 0;
    if (r14d == 2) {
        goto label_79;
    }
    *((rsp + 0x7c)) = bpl;
    eax = 0;
    ecx = 0x27;
    goto label_5;
    ecx = 0x72;
    ebp = 0;
label_10:
    dl = (r14d == 2) ? 1 : 0;
    eax = edx;
    if (*((rsp + 0x27)) == 0) {
        goto label_6;
    }
label_72:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
label_9:
    eax &= r12d;
    if (al == 0) {
        goto label_7;
    }
label_44:
    *(rsp) = r11;
    r12d = 1;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
label_45:
    rax = 0x0000d069;
    *(rsp) = 1;
    esi = 0;
    *((rsp + 0x50)) = rax;
    r15d = 1;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x7c)) = 0;
    if (r13 != 0) {
        goto label_80;
    }
    *((rsp + 0x58)) = 0;
    goto label_3;
    ecx = 0x66;
label_11:
    al = (r14d == 2) ? 1 : 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_81;
    }
    do {
        ebp = 0;
        goto label_8;
        ecx = 0x62;
        al = (r14d == 2) ? 1 : 0;
    } while (*((rsp + 0x27)) == 0);
label_81:
    r13 = r10;
    r14 = r12;
    r12d = *((rsp + 0x27));
    goto label_9;
    ecx = 0x6e;
    ebp = 0;
    goto label_10;
    ecx = 0x61;
    goto label_11;
label_19:
    if (*((rsp + 0x27)) != 0) {
        goto label_82;
    }
    r8d = 0;
    eax = r13d;
    sil = (r14d == 2) ? 1 : 0;
    eax ^= 1;
    al &= sil;
    if (al == 0) {
        goto label_83;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rdx = r15 + 1;
    if (r10 > rdx) {
        *((r12 + r15 + 1)) = 0x24;
    }
    rdx = r15 + 2;
    if (r10 > rdx) {
        *((r12 + r15 + 2)) = 0x27;
    }
    rdx = r15 + 3;
    if (r10 > rdx) {
        goto label_84;
    }
    r15 += 4;
    r13d = eax;
    ebp = 0;
    ecx = 0x30;
    goto label_12;
    ecx = 0x23;
label_25:
    edx = r8d;
    if (r9 != 0) {
        goto label_85;
    }
    ebx = ecx;
    goto label_13;
    r8d = 0;
    ecx = 9;
    ebx = 0x74;
label_17:
    if (*((rsp + 8)) != 0) {
        goto label_86;
    }
label_20:
    ebp = 0;
    eax = 0;
    if (*((rsp + 0x27)) != 0) {
        goto label_1;
    }
    goto label_14;
    ecx = 0x76;
    ebp = 0;
    goto label_10;
    ecx = r8d;
label_31:
    ebx = 0x20;
    goto label_15;
label_66:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_87;
    }
    if (bl < 0) {
        goto label_16;
    }
    if (bl > 0x3f) {
        goto label_16;
    }
    rdx = 0x0000d20c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xd20c */
    void (*rax)() ();
    ecx = 0xc;
    ebx = 0x66;
    goto label_17;
    ecx = 9;
    ebx = 0x74;
label_22:
    al = (r14d == 2) ? 1 : 0;
    al &= *((rsp + 0x27));
    r8d = eax;
    if (al == 0) {
        goto label_17;
    }
    goto label_18;
    ecx = 8;
    ebx = 0x62;
    goto label_17;
    if (*((rsp + 8)) != 0) {
        goto label_19;
    }
    r8d = 0;
    ecx = 0;
    if ((*((rsp + 0x78)) & 1) == 0) {
        goto label_20;
    }
    r9++;
    goto label_21;
    ecx = 0xb;
    ebx = 0x76;
    goto label_17;
    ebx = 0x20;
    goto label_13;
    ecx = 0xd;
    ebx = 0x72;
    goto label_22;
    ecx = 0xa;
    ebx = 0x6e;
    goto label_22;
    ecx = 7;
    ebx = 0x61;
    goto label_17;
label_87:
    if (bl > 0x7a) {
        goto label_88;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    if ((rax & rdx) != 0) {
        goto label_23;
    }
    if ((eax & 0xa4000000) != 0) {
        goto label_24;
    }
    if (bl != 0x5c) {
        goto label_16;
    }
    if (r14d == 2) {
        goto label_89;
    }
    edx = *((rsp + 8));
    dl &= *((rsp + 0x27));
    al = (*((rsp + 0x28)) != 0) ? 1 : 0;
    dl &= al;
    r8d = edx;
    if (dl != 0) {
        goto label_39;
    }
    ecx = 0x5c;
    goto label_17;
label_88:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_90;
    }
    ecx = 0x7e;
    if (bl == 0x7e) {
        goto label_25;
    }
    if (*((rsp + 0x60)) == 1) {
        goto label_26;
    }
label_74:
    rax = rsp + 0xb0;
    *((rsp + 0xb0)) = 0;
    *((rsp + 0x48)) = rax;
    if (r11 == -1) {
        *((rsp + 0x40)) = r10;
        *((rsp + 0x38)) = r9;
        *((rsp + 0x30)) = r8b;
        rax = strlen (*((rsp + 0x18)));
        r10 = *((rsp + 0x40));
        r9 = *((rsp + 0x38));
        r8d = *((rsp + 0x30));
        r11 = rax;
    }
    *((rsp + 0x7d)) = r8b;
    edi = 0;
    rax = rsp + 0xac;
    *((rsp + 0x38)) = r9;
    *((rsp + 0x7e)) = r13b;
    *((rsp + 0x80)) = r15;
    *((rsp + 0x70)) = r10;
    *((rsp + 0x30)) = r11;
    *((rsp + 0x68)) = r12;
    r12 = *((rsp + 0x48));
    *((rsp + 0x7f)) = bl;
    rbx = rdi;
    *((rsp + 0x40)) = r14d;
    r14 = rax;
    do {
        rax = *((rsp + 0x38));
        r13 = rax + rbx;
        rax = *((rsp + 0x18));
        rdx -= r13;
        rax = rpl_mbrtowc (r14, rax + r13, *((rsp + 0x30)), r12);
        r15 = rax;
        if (rax == 0) {
            goto label_91;
        }
        if (rax == -1) {
            goto label_92;
        }
        if (rax == 0xfffffffffffffffe) {
            goto label_93;
        }
        if (*((rsp + 0x40)) == 2) {
            if (*((rsp + 0x27)) != 0) {
                goto label_94;
            }
        }
label_42:
        eax = iswprint (*((rsp + 0xac)));
        rdi = r12;
        eax = 0;
        if (eax == 0) {
        }
        rbx += r15;
        eax = mbsinit (rdi);
    } while (eax == 0);
label_91:
    rdi = rbx;
    edx = ebp;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    edx ^= 1;
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    dl &= *((rsp + 8));
label_51:
    if (rdi <= 1) {
        goto label_27;
    }
label_48:
    rcx = rdi;
    *((rsp + 0x30)) = bpl;
    rdi = *((rsp + 0x18));
    esi = 0;
    ebp = *((rsp + 0x27));
    rcx += r9;
    while (dl != 0) {
        sil = (r14d == 2) ? 1 : 0;
        eax = esi;
        if (bpl != 0) {
            goto label_95;
        }
        eax = r13d;
        eax ^= 1;
        al &= sil;
        if (al != 0) {
            if (r10 > r15) {
                *((r12 + r15)) = 0x27;
            }
            rsi = r15 + 1;
            if (r10 > rsi) {
                *((r12 + r15 + 1)) = 0x24;
            }
            rsi = r15 + 2;
            if (r10 > rsi) {
                *((r12 + r15 + 2)) = 0x27;
            }
            r15 += 3;
            r13d = eax;
        }
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        rax = r15 + 1;
        if (r10 > rax) {
            eax = ebx;
            al >>= 6;
            eax += 0x30;
            *((r12 + r15 + 1)) = al;
        }
        rax = r15 + 2;
        if (r10 > rax) {
            eax = ebx;
            al >>= 3;
            eax &= 7;
            eax += 0x30;
            *((r12 + r15 + 2)) = al;
        }
        ebx &= 7;
        r9++;
        r15 += 3;
        ebx += 0x30;
        if (r9 >= rcx) {
            goto label_96;
        }
        esi = edx;
label_28:
        if (r10 > r15) {
            *((r12 + r15)) = bl;
        }
        ebx = *((rdi + r9));
        r15++;
    }
    eax = esi;
    eax ^= 1;
    eax &= r13d;
    if (r8b != 0) {
        if (r10 > r15) {
            *((r12 + r15)) = 0x5c;
        }
        r15++;
    }
    r9++;
    if (r9 >= rcx) {
        goto label_97;
    }
    if (al == 0) {
        goto label_98;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r8d = 0;
    r13d = 0;
    goto label_28;
label_90:
    ecx = 0x7b;
    if (bl != 0x7b) {
        goto label_29;
    }
label_34:
    if (r11 == -1) {
        goto label_99;
    }
label_35:
    if (r11 == 1) {
        goto label_25;
    }
label_32:
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_67:
    ebx = *(rbx);
    if (bl > 0x3f) {
        goto label_100;
    }
    if (bl < 0) {
        goto label_30;
    }
    if (bl > 0x3f) {
        goto label_30;
    }
    rdx = 0x0000d30c;
    eax = (int32_t) bl;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (64 cases) at 0xd30c */
    void (*rax)() ();
    ecx = 0;
    goto label_15;
    r8d = 0;
    ebp = 0;
    goto label_13;
    ecx = 0x23;
    r8d = 0;
    goto label_25;
    ecx = 0;
    goto label_31;
label_100:
    if (bl > 0x7a) {
        goto label_101;
    }
    if (bl == 0x40) {
        goto label_30;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = 0;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    r8d = 0;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
label_36:
    if (bl != 0x5c) {
        goto label_16;
    }
    edi = *((rsp + 0x27));
    if ((*((rsp + 8)) & dil) == 0) {
        goto label_102;
    }
    if (*((rsp + 0x28)) == 0) {
        goto label_102;
    }
label_39:
    r9++;
    eax = r13d;
    ebp = 0;
    ecx = 0x5c;
label_47:
    if (al == 0) {
        goto label_33;
    }
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x27;
    }
    r15 += 2;
    r13d = 0;
    goto label_33;
label_101:
    if (bl == 0x7d) {
        goto label_103;
    }
    if (bl <= 0x7d) {
        goto label_104;
    }
    edx = 0;
    if (bl != 0x7e) {
        goto label_30;
    }
label_38:
    if (r9 == 0) {
        goto label_105;
    }
    ecx = 0x7e;
label_85:
    r8d = edx;
    al = (r14d == 2) ? 1 : 0;
    ebp = 0;
    goto label_5;
label_104:
    ecx = 0x7b;
    r8d = 0;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_37:
    ecx = 0x7d;
    if (r11 != -1) {
        goto label_35;
    }
label_99:
    rax = *((rsp + 0x18));
    if (*((rax + 1)) != 0) {
        goto label_32;
    }
    goto label_25;
label_103:
    ecx = 0x7d;
    r8d = 0;
    goto label_34;
label_70:
    if (bl > 0x7a) {
        goto label_106;
    }
    if (bl == 0x40) {
        goto label_16;
    }
    ecx = rbx - 0x41;
    eax = 1;
    rdx = 0x3ffffff53ffffff;
    rax <<= cl;
    ecx = r8d;
    if ((rax & rdx) != 0) {
        goto label_15;
    }
    ecx = ebx;
    if ((eax & 0xa4000000) != 0) {
        goto label_32;
    }
    goto label_36;
label_106:
    if (bl == 0x7d) {
        goto label_37;
    }
    if (bl <= 0x7d) {
        goto label_107;
    }
    edx = r8d;
    if (bl == 0x7e) {
        goto label_38;
    }
    goto label_16;
label_107:
    ecx = 0x7b;
    if (bl == 0x7b) {
        goto label_34;
    }
    ecx = 0x7c;
    if (bl == 0x7c) {
        goto label_32;
    }
    goto label_16;
label_65:
    edi = *((rsp + 0x27));
    al = (r14d == 2) ? 1 : 0;
    edx = edi;
    cl = (r15 == 0) ? 1 : 0;
    edx &= eax;
    if ((cl & dl) != 0) {
        goto label_108;
    }
    edi ^= 1;
    edx = edi;
    al &= dil;
    if (al == 0) {
        goto label_56;
    }
    if (*((rsp + 0x7c)) == 0) {
        goto label_109;
    }
    if (*(rsp) != 0) {
        goto label_110;
    }
    r14 = r12;
    esi = r13d;
    r12d = *((rsp + 8));
    al = (r10 == 0) ? 1 : 0;
    dl = (*((rsp + 0x58)) != 0) ? 1 : 0;
    al &= dl;
    if (al == 0) {
        goto label_111;
    }
    rdx = *((rsp + 0x58));
label_61:
    *((rsp + 0x7c)) = al;
    r13 = *((rsp + 0x58));
    r15d = 1;
    rax = 0x0000d069;
    *(r14) = 0x27;
    *((rsp + 0x58)) = rdx;
    *((rsp + 0x27)) = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_3;
label_89:
    if (*((rsp + 0x27)) == 0) {
        goto label_39;
    }
    do {
label_41:
        eax = *((rsp + 8));
        r13 = r10;
        r14 = r12;
        goto label_40;
label_79:
    } while (*((rsp + 0x27)) != 0);
    if (r10 == 0) {
        goto label_112;
    }
    edx = 0;
    if (*((rsp + 0x58)) != 0) {
        goto label_112;
    }
label_46:
    *((rsp + 0x58)) = r10;
    r15 += 3;
    eax = 0;
    r13d = 0;
    *((rsp + 0x7c)) = bpl;
    r10 = rdx;
    ecx = 0x27;
    goto label_12;
label_77:
    if (*((rsp + 0x27)) != 0) {
        goto label_41;
    }
    ebp = 0;
    eax = 0;
    ecx = 0x3f;
    goto label_12;
label_94:
    if (rax == 1) {
        goto label_42;
    }
    rax = *((rsp + 0x18));
    rsi = rax + r15;
    rdx = rax + r13 + 1;
    rsi += r13;
    goto label_113;
label_43:
    rdx++;
    if (rsi == rdx) {
        goto label_42;
    }
label_113:
    eax = *(rdx);
    eax -= 0x5b;
    if (al > 0x21) {
        goto label_43;
    }
    rdi = 0x20000002b;
    if (((rdi >> rax) & 1) >= 0) {
        goto label_43;
    }
    r12d = *((rsp + 8));
    r14 = *((rsp + 0x68));
    r13 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    if (r12b != 0) {
        goto label_44;
    }
label_58:
    *(rsp) = r11;
    r12d = 0;
    rax = ctype_get_mb_cur_max ();
    r11 = *(rsp);
    *((rsp + 0x10)) = 0;
    *((rsp + 0x60)) = rax;
    goto label_45;
label_83:
    rdx = r15;
    if (r10 > r15) {
        eax = r13d;
label_84:
        *((r12 + rdx)) = 0x5c;
        r13d = eax;
    }
    r15 = rdx + 1;
    if (r14d == 2) {
        goto label_114;
    }
    rax = r9 + 1;
    ecx = 0x30;
    if (rax < r11) {
        rax = *((rsp + 0x18));
        eax = *((rax + r9 + 1));
        *((rsp + 0x30)) = al;
        eax -= 0x30;
        if (al <= 9) {
            goto label_115;
        }
    }
label_49:
    eax = *((rsp + 8));
    eax ^= 1;
    al |= sil;
    eax = ebp;
    if (al == 0) {
        goto label_1;
    }
    goto label_12;
label_112:
    if (r10 > r15) {
        *((r12 + r15)) = 0x27;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x5c;
    }
    rax = r15 + 2;
    if (r10 <= rax) {
        goto label_116;
    }
    rdx = r10;
    *((r12 + r15 + 2)) = 0x27;
    r10 = *((rsp + 0x58));
    goto label_46;
label_95:
    r13 = r10;
    r14 = r12;
    goto label_40;
label_97:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_47;
label_75:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_48;
label_96:
    ebp = *((rsp + 0x30));
    ecx = ebx;
    goto label_33;
label_98:
    r8d = 0;
    goto label_28;
label_114:
    eax = ebp;
    ecx = 0x30;
    ebp = 0;
    goto label_12;
label_115:
    if (r10 > r15) {
        *((r12 + r15)) = 0x30;
    }
    rax = rdx + 2;
    if (r10 > rax) {
        *((r12 + rdx + 2)) = 0x30;
    }
    r15 = rdx + 3;
    ecx = 0x30;
    goto label_49;
label_64:
    rdx = *((rsp + 0x90));
    eax = *(rdx);
    if (al == 0) {
        goto label_50;
    }
    do {
        if (r13 > r15) {
            *((r14 + r15)) = al;
        }
        r15++;
        eax = *((rdx + r15));
    } while (al != 0);
    goto label_50;
label_92:
    rdi = rbx;
    r8d = *((rsp + 0x7d));
    r9 = *((rsp + 0x38));
    ebp = 0;
    r13d = *((rsp + 0x7e));
    ebx = *((rsp + 0x7f));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r11 = *((rsp + 0x30));
    r14d = *((rsp + 0x40));
    edx = *((rsp + 8));
    goto label_51;
label_78:
    ecx = *((rax + rdx));
    if (cl > 0x3e) {
        goto label_52;
    }
    rax = 0x7000a38200000000;
    rax >>= cl;
    eax &= 1;
    if (eax != 0) {
        goto label_117;
    }
    ebp = 0;
    ecx = 0x3f;
    goto label_5;
label_93:
    r11 = *((rsp + 0x30));
    rdi = rbx;
    rax = r13;
    r9 = *((rsp + 0x38));
    r8d = *((rsp + 0x7d));
    ebx = *((rsp + 0x7f));
    rdx = rdi;
    r13d = *((rsp + 0x7e));
    r15 = *((rsp + 0x80));
    r12 = *((rsp + 0x68));
    r10 = *((rsp + 0x70));
    r14d = *((rsp + 0x40));
    rcx = *((rsp + 0x18));
    if (rax < r11) {
        goto label_118;
    }
    goto label_119;
    do {
        rdx++;
        rax = r9 + rdx;
        if (rax >= r11) {
            goto label_120;
        }
label_118:
    } while (*((rcx + rax)) != 0);
label_120:
    rdi = rdx;
label_119:
    edx = *((rsp + 8));
    ebp = 0;
    goto label_51;
label_76:
    rax = 0x0000d067;
    *((rsp + 0x27)) = 1;
    r12d = 1;
    r15d = 0;
    *((rsp + 0x28)) = 1;
    *((rsp + 0x50)) = rax;
    goto label_4;
label_109:
    edx = eax;
label_56:
    rax = *((rsp + 0x50));
    if (rax == 0) {
        goto label_121;
    }
    if (dl == 0) {
        goto label_121;
    }
    ecx = *(rax);
    if (cl == 0) {
        goto label_121;
    }
    rsi = *((rsp + 0x98));
    rdx = r15;
    rax -= r15;
    do {
        if (r10 > rdx) {
            *((rsi + rdx)) = cl;
        }
        rdx++;
        ecx = *((rax + rdx));
    } while (cl != 0);
    r15 = rdx;
label_121:
    if (r10 > r15) {
        goto label_122;
    }
label_55:
    rax = *((rsp + 0xb8));
    rax -= *(fs:0x28);
    if (rax != 0) {
        goto label_123;
    }
    rax = r15;
    return rax;
    do {
label_69:
        r13 = r10;
        r14 = r12;
        goto label_7;
label_63:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x88)) = rax;
        goto label_53;
label_62:
        esi = ebp;
        rdi = rax;
        rax = gettext_quote_part_0 ();
        r11 = *(rsp);
        *((rsp + 0x90)) = rax;
        goto label_54;
label_117:
    } while (*((rsp + 0x27)) != 0);
    if (r10 > r15) {
        *((r12 + r15)) = 0x3f;
    }
    rax = r15 + 1;
    if (r10 > rax) {
        *((r12 + r15 + 1)) = 0x22;
    }
    rax = r15 + 2;
    if (r10 > rax) {
        *((r12 + r15 + 2)) = 0x22;
    }
    rax = r15 + 3;
    if (r10 > rax) {
        *((r12 + r15 + 3)) = 0x3f;
    }
    r15 += 4;
    esi = 0;
    ebp = 0;
    r9 = rdx;
    goto label_49;
label_122:
    rax = *((rsp + 0x98));
    *((rax + r15)) = 0;
    goto label_55;
label_82:
    r13 = r10;
    r14 = r12;
    if (ebp == 2) {
        goto label_44;
    }
    goto label_7;
label_111:
    edx = *((rsp + 0x7c));
    goto label_56;
label_110:
    *((rsp + 8)) = r11;
    r15d = 0;
    r14d = 5;
    rax = ctype_get_mb_cur_max ();
    r11 = *((rsp + 8));
    *((rsp + 0x28)) = 1;
    *((rsp + 0x60)) = rax;
    rax = 0x0000d067;
    *((rsp + 0x50)) = rax;
    if ((*((rsp + 0x78)) & 2) != 0) {
        goto label_124;
    }
    r13 = *((rsp + 0x58));
    r14 = r12;
    goto label_57;
label_108:
    r14 = r12;
    r12d = *((rsp + 8));
    r13 = r10;
    if (r12b != 0) {
        goto label_44;
    }
    goto label_58;
label_124:
    eax = *(rsp);
    r10 = *((rsp + 0x58));
    *((rsp + 0x7c)) = 0;
    r13d = 0;
    *((rsp + 0x58)) = 0;
    *((rsp + 0x27)) = al;
    *((rsp + 8)) = al;
    goto label_59;
    if (ebx != 0) {
        goto label_60;
    }
    r12d = 1;
    goto label_45;
    if (ebx != 0) {
        rax = 0x0000d069;
        *((rsp + 0x27)) = 1;
        r12d = 0;
        r15d = 0;
        *((rsp + 0x28)) = 1;
        *((rsp + 0x50)) = rax;
        goto label_4;
label_123:
        eax = stack_chk_fail ();
label_80:
        *((rsp + 0x58)) = r13;
        eax = 0;
        edx = 0;
        goto label_61;
label_105:
        ecx = 0x7e;
        r8d = edx;
        al = (r14d == 2) ? 1 : 0;
        goto label_5;
label_116:
        rdx = r10;
        r10 = *((rsp + 0x58));
        goto label_46;
label_102:
        ecx = 0x5c;
        ebp = 0;
        goto label_10;
label_86:
        ecx = ebx;
        ebp = 0;
        goto label_10;
    }
    r12d = 0;
    goto label_45;
}

/* /tmp/tmp2diozptm @ 0x27d9 */
 
void quotearg_buffer_restyled_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x7f40 */
 
int64_t quotearg_n_options (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    size_t n;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r13 = rdx;
    r12 = rsi;
    rbx = (int64_t) edi;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    rax = errno_location ();
    r15 = slotvec;
    *(rsp) = rax;
    eax = *(rax);
    *((rsp + 0xc)) = eax;
    if (ebx > 0x7ffffffe) {
        void (*0x27de)() ();
    }
    eax = nslots;
    if (eax > ebx) {
        goto label_0;
    }
    rdx = (int64_t) eax;
    *((rsp + 0x20)) = rdx;
    edx = ebx;
    edx -= eax;
    rax = obj_slotvec0;
    edx++;
    rdx = (int64_t) edx;
    if (r15 == rax) {
        goto label_1;
    }
    r8d = 0x10;
    rax = xpalloc (r15, rsp + 0x20, rdx, 0x7fffffff);
    *(obj.slotvec) = rax;
    r15 = rax;
    do {
        rdi = *(obj.nslots);
        rdx -= rdi;
        rdi <<= 4;
        rdx <<= 4;
        rdi += r15;
        memset (rdi, 0, *((rsp + 0x20)));
        rax = *((rsp + 0x20));
        *(obj.nslots) = eax;
label_0:
        rax = rbp + 8;
        rbx <<= 4;
        r8d = *(rbp);
        rbx += r15;
        r15d = *((rbp + 4));
        *((rsp + 0x20)) = rax;
        rcx = r13;
        rsi = *(rbx);
        r14 = *((rbx + 8));
        rdx = r12;
        r15d |= 1;
        r9d = r15d;
        rdi = r14;
        *((rsp + 0x30)) = rsi;
        rax = quotearg_buffer_restyled ();
        rsi = *((rsp + 0x30));
        if (rsi <= rax) {
            rsi = rax + 1;
            rax = obj_slot0;
            *(rbx) = rsi;
            if (r14 != rax) {
                *((rsp + 0x10)) = rsi;
                free (r14);
                rsi = *((rsp + 0x10));
            }
            *((rsp + 0x10)) = rsi;
            rax = xcharalloc (*((rsp + 0x10)));
            r8d = *(rbp);
            r9d = r15d;
            *((rbx + 8)) = rax;
            rcx = r13;
            rdx = r12;
            rdi = rax;
            r14 = rax;
            rsi = *((rsp + 0x30));
            quotearg_buffer_restyled ();
        }
        rax = *(rsp);
        ecx = *((rsp + 0xc));
        *(rax) = ecx;
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_2;
        }
        rax = r14;
        return rax;
label_1:
        r8d = 0x10;
        rax = xpalloc (0, rsp + 0x20, rdx, 0x7fffffff);
        __asm ("movdqa xmm0, xmmword [obj.slotvec0]");
        *(obj.slotvec) = rax;
        r15 = rax;
        __asm ("movups xmmword [rax], xmm0");
    } while (1);
label_2:
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x27de */
 
void quotearg_n_options_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x27e3 */
 
void set_custom_quoting_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x2410 */
 
void abort (void) {
    __asm ("bnd jmp qword [reloc.abort]");
}

/* /tmp/tmp2diozptm @ 0x27e9 */
 
void quotearg_n_style_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x27ee */
 
void quotearg_n_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x27f3 */
 
void quotearg_style_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x27f8 */
 
void quotearg_style_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x27fd */
 
void quotearg_n_style_colon_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x2802 */
 
void quotearg_n_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x2807 */
 
void quotearg_n_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x280c */
 
void quotearg_custom_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x2811 */
 
void quotearg_custom_mem_cold (void) {
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x35d0 */
 
uint64_t deregister_tm_clones (void) {
    rdi = obj___progname;
    rax = obj___progname;
    if (rax != rdi) {
        rax = *(reloc._ITM_deregisterTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp2diozptm @ 0x3600 */
 
int64_t register_tm_clones (void) {
    rdi = obj___progname;
    rsi = obj___progname;
    rsi -= rdi;
    rax = rsi;
    rsi >>= 0x3f;
    rax >>= 3;
    rsi += rax;
    rsi >>= 1;
    if (rsi != 0) {
        rax = *(reloc._ITM_registerTMCloneTable);
        if (rax == 0) {
            goto label_0;
        }
        void (*rax)() ();
    }
label_0:
    return rax;
}

/* /tmp/tmp2diozptm @ 0x3640 */
 
void do_global_dtors_aux (void) {
    if (*(obj.completed.0) == 0) {
        if (*(reloc.__cxa_finalize) != 0) {
            rdi = *(obj.__dso_handle);
            fcn_000023e0 ();
        }
        deregister_tm_clones ();
        *(obj.completed.0) = 1;
        return;
    }
}

/* /tmp/tmp2diozptm @ 0x23e0 */
 
void fcn_000023e0 (void) {
    /* [14] -r-x section size 16 named .plt.got */
    __asm ("bnd jmp qword [reloc.__cxa_finalize]");
}

/* /tmp/tmp2diozptm @ 0x3680 */
 
void entry_init0 (void) {
    return register_tm_clones ();
}

/* /tmp/tmp2diozptm @ 0x5910 */
 
int64_t dbg_compare_ranges ( const * a,  const * b) {
    rdi = a;
    rsi = b;
    /* int compare_ranges( const * a, const * b); */
    rax = *(rsi);
    al = (*(rdi) > rax) ? 1 : 0;
    eax = (int32_t) al;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x5930 */
 
int64_t dbg_add_range_pair (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void add_range_pair(uintmax_t lo,uintmax_t hi); */
    rbx = rsi;
    rdx = n_frp;
    rdi = frp;
    while (1) {
        rax = rdx;
        rdx++;
        rax <<= 4;
        rdi += rax;
        *(rdi) = rbp;
        *((rdi + 8)) = rbx;
        *(obj.n_frp) = rdx;
        return rax;
        edx = 0x10;
        rsi = obj_n_frp_allocated;
        rax = x2nrealloc ();
        rdx = n_frp;
        *(obj.frp) = rax;
        rdi = rax;
    }
}

/* /tmp/tmp2diozptm @ 0x5f90 */
 
void dbg_argmatch_die (void) {
    int64_t var_30h;
    int64_t var_38h;
    int64_t var_40h;
    int64_t var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h_2;
    int64_t var_80h;
    int64_t var_88h;
    int64_t var_90h;
    int64_t var_98h;
    int64_t var_78h;
    /* void __argmatch_die(); */
    edi = 1;
    return void (*0x51e0)() ();
}

/* /tmp/tmp2diozptm @ 0x24e0 */
 
void dcgettext (void) {
    __asm ("bnd jmp qword [reloc.dcgettext]");
}

/* /tmp/tmp2diozptm @ 0x2740 */
 
void fprintf_chk (void) {
    __asm ("bnd jmp qword [reloc.__fprintf_chk]");
}

/* /tmp/tmp2diozptm @ 0x2720 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc.exit]");
}

/* /tmp/tmp2diozptm @ 0x26a0 */
 
void printf_chk (void) {
    __asm ("bnd jmp qword [reloc.__printf_chk]");
}

/* /tmp/tmp2diozptm @ 0x25c0 */
 
void fputs_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputs_unlocked]");
}

/* /tmp/tmp2diozptm @ 0x25e0 */
 
void strcmp (void) {
    __asm ("bnd jmp qword [reloc.strcmp]");
}

/* /tmp/tmp2diozptm @ 0x2690 */
 
void setlocale (void) {
    __asm ("bnd jmp qword [reloc.setlocale]");
}

/* /tmp/tmp2diozptm @ 0x2430 */
 
void strncmp (void) {
    __asm ("bnd jmp qword [reloc.strncmp]");
}

/* /tmp/tmp2diozptm @ 0xa7e0 */
 
void atexit (void) {
    rdx = *(obj.__dso_handle);
    esi = 0;
    return cxa_atexit ();
}

/* /tmp/tmp2diozptm @ 0x8490 */
 
void quotearg_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_default_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp2diozptm @ 0x87c0 */
 
int64_t quotearg_char (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000112f0]");
    ecx = esi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    ecx &= 0x1f;
    r9 = rsp;
    __asm ("movdqa xmm2, xmmword [0x00011300]");
    *(rsp) = xmm0;
    *((rsp + 0x30)) = rax;
    eax = esi;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r9;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = 0xffffffffffffffff;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x2510 */
 
void stack_chk_fail (void) {
    __asm ("bnd jmp qword [reloc.__stack_chk_fail]");
}

/* /tmp/tmp2diozptm @ 0x9250 */
 
int64_t dbg_version_etc (int64_t arg_c0h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    va_list authors;
    char const *[10] authtab;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_78h;
    int64_t var_80h;
    int64_t var_a0h;
    int64_t var_a8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc(FILE * stream,char const * command_name,char const * package,char const * version,va_args ...); */
    r10 = rdi;
    r11 = rsi;
    r12 = rdx;
    edx = 0x20;
    *((rsp + 0xa0)) = r8;
    rdi = rsp + 0x80;
    rsi = rsp + 0xc0;
    *((rsp + 0xa8)) = r9;
    r8 = rsp + 0x20;
    r9d = 0;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    rax = rsp + 0xc0;
    *((rsp + 8)) = 0x20;
    *((rsp + 0x10)) = rax;
    *((rsp + 0x18)) = rdi;
    while (edx <= 0x2f) {
        eax = edx;
        edx += 8;
        rax += rdi;
        rax = *(rax);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
    }
    rax = rsi;
    rsi += 8;
    rax = *(rax);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (r10, r11, r12, rcx, r8, r9);
    rax = *((rsp + 0x78));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x82d0 */
 
uint64_t dbg_quotearg_alloc_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * quotearg_alloc_mem(char const * arg,size_t argsize,size_t * size,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rsi;
    r13 = rdi;
    rbx = rcx;
    if (rcx == 0) {
        rbx = rax;
    }
    rax = errno_location ();
    r9d = 0;
    rcx = r14;
    r12 = rax;
    eax = *(rax);
    r9b = (rbp == 0) ? 1 : 0;
    r10 = rbx + 8;
    r9d |= *((rbx + 4));
    r8d = *(rbx);
    rdx = r13;
    *((rsp + 0x18)) = eax;
    esi = 0;
    edi = 0;
    *((rsp + 0x38)) = r10;
    *((rsp + 0x34)) = r9d;
    rax = quotearg_buffer_restyled ();
    rsi = rax + 1;
    r15 = rax;
    rdi = rsi;
    *((rsp + 8)) = rsi;
    rax = xcharalloc (rdi);
    r8d = *(rbx);
    rcx = r14;
    rdx = r13;
    rdi = rax;
    r10 = *((rsp + 0x30));
    rsi = *((rsp + 0x28));
    r9d = *((rsp + 0x34));
    *((rsp + 0x28)) = rax;
    quotearg_buffer_restyled ();
    eax = *((rsp + 0x30));
    r11 = *((rsp + 8));
    *(r12) = eax;
    if (rbp != 0) {
        *(rbp) = r15;
    }
    rax = r11;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x2420 */
 
void errno_location (void) {
    __asm ("bnd jmp qword [reloc.__errno_location]");
}

/* /tmp/tmp2diozptm @ 0x9450 */
 
uint64_t dbg_xcharalloc (size_t size) {
    rdi = size;
    /* char * xcharalloc(size_t n); */
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x2640 */
 
void malloc (void) {
    __asm ("bnd jmp qword [reloc.malloc]");
}

/* /tmp/tmp2diozptm @ 0x9990 */
 
uint64_t dbg_xalloc_die (void) {
    /* void xalloc_die(); */
    edx = 5;
    rax = dcgettext (0, "memory exhausted");
    rcx = rax;
    eax = 0;
    error (*(obj.exit_failure), 0, 0x0000d003);
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x26d0 */
 
void error (void) {
    __asm ("bnd jmp qword [reloc.error]");
}

/* /tmp/tmp2diozptm @ 0x2680 */
 
void realloc (void) {
    __asm ("bnd jmp qword [reloc.realloc]");
}

/* /tmp/tmp2diozptm @ 0x2480 */
 
void reallocarray (void) {
    __asm ("bnd jmp qword [reloc.reallocarray]");
}

/* /tmp/tmp2diozptm @ 0x81f0 */
 
uint64_t dbg_set_quoting_flags (int64_t arg1, int32_t i) {
    rdi = arg1;
    rsi = i;
    /* int set_quoting_flags(quoting_options * o,int i); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *((rdi + 4));
    *((rdi + 4)) = esi;
    return rax;
}

/* /tmp/tmp2diozptm @ 0xa610 */
 
int64_t dbg_rpl_mbrtowc (int64_t arg2, size_t * arg3, mbstate_t * ps, wchar_t ** pwc) {
    wchar_t wc;
    int64_t var_4h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    rcx = ps;
    rdi = pwc;
    /* size_t rpl_mbrtowc(wchar_t * pwc,char const * s,size_t n,mbstate_t * ps); */
    r13 = rsi;
    rbx = rdi;
    rax = *(fs:0x28);
    *((rsp + 8)) = rax;
    eax = 0;
    rax = rsp + 4;
    if (rdi == 0) {
        rbx = rax;
    }
    rax = mbrtowc (rbx, rsi, rdx, rcx);
    r12 = rax;
    if (rax <= 0xfffffffffffffffd) {
        goto label_0;
    }
    while (al != 0) {
label_0:
        rax = *((rsp + 8));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_1;
        }
        rax = r12;
        return rax;
        edi = 0;
        al = hard_locale ();
    }
    eax = *(r13);
    r12d = 1;
    *(rbx) = eax;
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x84e0 */
 
int32_t quotearg_n_style (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x27e9)() ();
    }
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x68e0 */
 
int64_t dbg_ambsalign (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char * ambsalign(char const * src,size_t * width,mbs_align_t align,int flags); */
    r14 = rdi;
    r13d = edx;
    r12d = 0;
    rbx = rsi;
    rax = *(rsi);
    *((rsp + 0xc)) = ecx;
    *(rsp) = rax;
    do {
        rbp = rax + 1;
        rdi = r12;
        r15 = r12;
        rax = realloc (rdi, rbp);
        r12 = rax;
        if (rax == 0) {
            goto label_1;
        }
        rax = *(rsp);
        *(rbx) = rax;
        rax = mbsalign (r14, r12, rbp, rbx, r13d, *((rsp + 0xc)));
        if (rax == -1) {
            goto label_2;
        }
    } while (rbp <= rax);
    do {
label_0:
        rax = r12;
        return rax;
label_2:
        r12d = 0;
        free (r12);
    } while (1);
label_1:
    free (r15);
    goto label_0;
}

/* /tmp/tmp2diozptm @ 0x8250 */
 
uint64_t dbg_quotearg_buffer (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* size_t quotearg_buffer(char * buffer,size_t buffersize,char const * arg,size_t argsize,quoting_options const * o); */
    rax = obj_default_quoting_options;
    r14 = rdx;
    r13 = rsi;
    r12 = rdi;
    rbx = r8;
    if (r8 == 0) {
        rbx = rax;
    }
    *((rsp + 8)) = rcx;
    rax = errno_location ();
    rdx = r14;
    rsi = r13;
    r15d = *(rax);
    rax = rbx + 8;
    r9d = *((rbx + 4));
    r8d = *(rbx);
    rdi = r12;
    rcx = *((rsp + 0x28));
    quotearg_buffer_restyled ();
    *(rbp) = r15d;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x6990 */
 
uint64_t dbg_set_program_name (uint32_t arg_1h, int64_t arg_4h, char ** arg1) {
    char * s1;
    rdi = arg1;
    /* void set_program_name(char const * argv0); */
    if (rdi == 0) {
        goto label_0;
    }
    rbx = rdi;
    rax = strrchr (rdi, 0x2f);
    if (rax == 0) {
        goto label_1;
    }
    r12 = rax + 1;
    rax = r12;
    rax -= rbx;
    if (rax <= 6) {
        goto label_1;
    }
    eax = strncmp (rbp - 6, "/.libs/", 7);
    if (eax != 0) {
        goto label_1;
    }
    if (*((rbp + 1)) != 0x6c) {
        goto label_2;
    }
    if (*((r12 + 1)) != 0x74) {
        goto label_2;
    }
    if (*((r12 + 2)) != 0x2d) {
        goto label_2;
    }
    rbx = rbp + 4;
    *(obj.__progname) = rbx;
    do {
label_1:
        *(obj.program_name) = rbx;
        *(obj.program_invocation_name) = rbx;
        return rax;
label_2:
        rbx = r12;
    } while (1);
label_0:
    fwrite ("A NULL argv[0] was passed through an exec system call.\n", 1, 0x37, *(obj.stderr));
    return abort ();
}

/* /tmp/tmp2diozptm @ 0x2560 */
 
void strrchr (void) {
    __asm ("bnd jmp qword [reloc.strrchr]");
}

/* /tmp/tmp2diozptm @ 0x2730 */
 
void fwrite (void) {
    __asm ("bnd jmp qword [reloc.fwrite]");
}

/* /tmp/tmp2diozptm @ 0x6110 */
 
uint64_t dbg_argmatch_invalid (int64_t arg1, int64_t arg2, uint32_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_invalid(char const * context,char const * value,ptrdiff_t problem); */
    r13 = rsi;
    edx = 5;
    if (rdx == -1) {
        goto label_0;
    }
    rax = dcgettext (0, "ambiguous argument %s for %s");
    r12 = rax;
    do {
        rsi = rbp;
        edi = 1;
        rax = quote_n ();
        rdx = r13;
        esi = 8;
        edi = 0;
        rbx = rax;
        rax = quotearg_n_style ();
        r8 = rbx;
        rdx = r12;
        rcx = rax;
        esi = 0;
        edi = 0;
        eax = 0;
        void (*0x26d0)() ();
label_0:
        rax = dcgettext (0, "invalid argument %s for %s");
        r12 = rax;
    } while (1);
}

/* /tmp/tmp2diozptm @ 0x8ce0 */
 
void quote_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp2diozptm @ 0x8a20 */
 
int64_t quotearg_n_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rcx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    __asm ("movdqa xmm1, xmmword [0x000112f0]");
    __asm ("movdqa xmm2, xmmword [0x00011300]");
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rax == 0) {
        void (*0x2802)() ();
    }
    if (rdx == 0) {
        void (*0x2802)() ();
    }
    *((rsp + 0x30)) = rdx;
    rcx = rsp;
    rdx = 0xffffffffffffffff;
    *((rsp + 0x28)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x8ac0 */
 
int64_t quotearg_n_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    r9d = edi;
    rdi = rsi;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000112f0]");
    __asm ("movdqa xmm2, xmmword [0x00011300]");
    rsi = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2807)() ();
    }
    rax = rdx;
    if (rdx == 0) {
        void (*0x2807)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = r8;
    rcx = rsp;
    edi = r9d;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x8600 */
 
int64_t quotearg_style (uint32_t arg1, int64_t arg2) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x27f3)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    rdx = 0xffffffffffffffff;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x8980 */
 
int64_t quotearg_n_style_colon (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    eax = esi;
    rsi = rdx;
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    if (eax == 0xa) {
        void (*0x27fd)() ();
    }
    *(rsp) = eax;
    rdx = 0xffffffffffffffff;
    rcx = rsp;
    rax = 0x400000000000000;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = rax;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0xa7f4 */
 
void fini (void) {
    /* [17] -r-x section size 13 named .fini */
}

/* /tmp/tmp2diozptm @ 0x94d0 */
 
uint64_t xreallocarray (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x95d0 */
 
int64_t dbg_x2realloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * x2realloc(void * p,size_t * ps); */
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_1;
    }
    edx = 1;
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
        *(rbp) = rbx;
        return rax;
    }
    do {
label_1:
        xalloc_die ();
label_0:
        eax = 0x80;
        edx = 1;
        if (rbx == 0) {
            rbx = rax;
        }
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x9470 */
 
uint64_t xrealloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    rbx = rsi;
    rax = realloc (rdi, rsi);
    while (rbx == 0) {
        return rax;
        if (rbp == 0) {
            goto label_0;
        }
    }
label_0:
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0xa500 */
 
uint32_t dbg_rpl_fflush (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fflush(FILE * stream); */
    if (rdi == 0) {
        goto label_0;
    }
    eax = freading ();
    while ((*(rbp) & 0x100) == 0) {
label_0:
        rdi = rbp;
        void (*0x2650)() ();
    }
    rpl_fseeko (rbp, 0, 1, rcx);
    rdi = rbp;
    return fflush ();
}

/* /tmp/tmp2diozptm @ 0x9410 */
 
uint64_t xmalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x84c0 */
 
void dbg_quotearg_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char * quotearg_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_default_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp2diozptm @ 0x9950 */
 
uint64_t dbg_xstrdup (int64_t arg1) {
    rdi = arg1;
    /* char * xstrdup(char const * string); */
    strlen (rdi);
    r12 = rax + 1;
    rax = malloc (r12);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2610)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x2500 */
 
void strlen (void) {
    __asm ("bnd jmp qword [reloc.strlen]");
}

/* /tmp/tmp2diozptm @ 0x9430 */
 
uint64_t ximalloc (size_t size) {
    rdi = size;
    rax = malloc (rdi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x5fa0 */
 
uint64_t dbg_argmatch (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_8h;
    uint32_t var_17h;
    int64_t var_18h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* ptrdiff_t argmatch(char const * arg,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = rdi;
    r13 = rcx;
    rbx = rsi;
    *((rsp + 8)) = rsi;
    *((rsp + 0x18)) = rdx;
    rax = strlen (rdi);
    r15 = *(rbx);
    if (r15 == 0) {
        goto label_3;
    }
    *((rsp + 0x17)) = 0;
    r12 = rax;
    ebx = 0;
    *(rsp) = 0xffffffffffffffff;
    goto label_4;
label_0:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_5;
    }
    rdi *= r13;
    rdi += rax;
    eax = memcmp (*(rsp), rbp, r13);
    ecx = 1;
    eax = *((rsp + 0x17));
    if (eax != 0) {
        eax = ecx;
    }
    *((rsp + 0x17)) = al;
    do {
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r13;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_6;
        }
label_4:
        eax = strncmp (r15, r14, r12);
    } while (eax != 0);
    rax = strlen (r15);
    if (rax == r12) {
        goto label_7;
    }
    if (*(rsp) != -1) {
        goto label_0;
    }
    *(rsp) = rbx;
    goto label_1;
label_5:
    *((rsp + 0x17)) = 1;
    goto label_1;
label_6:
    rax = 0xfffffffffffffffe;
    if (*((rsp + 0x17)) == 0) {
        rax = *(rsp);
    }
    *(rsp) = rax;
    do {
label_2:
        rax = *(rsp);
        return rax;
label_7:
        *(rsp) = rbx;
    } while (1);
label_3:
    *(rsp) = 0xffffffffffffffff;
    goto label_2;
}

/* /tmp/tmp2diozptm @ 0x9190 */
 
void dbg_version_etc_ar (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, uint32_t arg5) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* void version_etc_ar(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors); */
    r9d = 0;
    if (*(r8) == 0) {
        goto label_0;
    }
    do {
        r9++;
    } while (*((r8 + r9*8)) != 0);
label_0:
    return void (*0x8d20)() ();
}

/* /tmp/tmp2diozptm @ 0x6420 */
 
uint64_t dbg_close_stdout (void) {
    /* void close_stdout(); */
    eax = close_stream (*(obj.stdout));
    if (eax != 0) {
        rax = errno_location ();
        rbx = rax;
        if (*(obj.ignore_EPIPE) == 0) {
            goto label_0;
        }
        if (*(rax) != 0x20) {
            goto label_0;
        }
    }
    eax = close_stream (*(obj.stderr));
    if (eax != 0) {
        goto label_1;
    }
    return rax;
label_0:
    edx = 5;
    rax = dcgettext (0, "write error");
    rdi = file_name;
    r12 = rax;
    if (rdi == 0) {
        goto label_2;
    }
    rax = quotearg_colon (rdi, rsi, rdx, rcx);
    r8 = r12;
    rcx = rax;
    eax = 0;
    error (0, *(rbx), "%s: %s");
    do {
label_1:
        rax = exit (*(obj.exit_failure));
label_2:
        rcx = rax;
        eax = 0;
        error (0, *(rbx), 0x0000d003);
    } while (1);
}

/* /tmp/tmp2diozptm @ 0xa720 */
 
uint64_t dbg_setlocale_null_r (int64_t arg2, int64_t arg3, int32_t category) {
    rsi = arg2;
    rdx = arg3;
    rdi = category;
    /* int setlocale_null_r(int category,char * buf,size_t bufsize); */
    r12 = rsi;
    rbx = rdx;
    rax = setlocale (rdi, 0);
    if (rax == 0) {
        goto label_1;
    }
    rdi = rax;
    rax = strlen (rdi);
    if (rbx > rax) {
        goto label_2;
    }
    r13d = 0x22;
    while (rbx == 0) {
label_0:
        eax = r13d;
        return rax;
label_2:
        r13d = 0;
        memcpy (r12, rbp, rax + 1);
        eax = r13d;
        return rax;
        memcpy (r12, rbp, rbx - 1);
        *((r12 + rbx - 1)) = 0;
        eax = r13d;
        return rax;
label_1:
        r13d = 0x16;
    }
    *(r12) = 0;
    goto label_0;
}

/* /tmp/tmp2diozptm @ 0x93d0 */
 
uint64_t dbg_xnrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void * xnrealloc(void * p,size_t n,size_t s); */
    r12 = rdx;
    rbx = rdi;
    rax = reallocarray ();
    while (rbp == 0) {
label_0:
        return rax;
        if (rbx == 0) {
            goto label_1;
        }
    }
    if (r12 == 0) {
        goto label_0;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0xa6a0 */
 
int64_t dbg_hard_locale (void) {
    char[257] locale;
    uint32_t var_4h;
    int64_t var_108h;
    /* _Bool hard_locale(int category); */
    rax = *(fs:0x28);
    *((rsp + 0x108)) = rax;
    eax = 0;
    eax = setlocale_null_r (rdi, rsp, 0x101);
    r8d = eax;
    eax = 0;
    if (r8d != 0) {
        goto label_0;
    }
    if (*(rsp) == 0x43) {
        goto label_0;
    }
    while (*((rsp + 4)) != 0x58) {
        eax = 1;
label_0:
        rdx = *((rsp + 0x108));
        rdx -= *(fs:0x28);
        if (rdx != 0) {
            goto label_1;
        }
        return rax;
        eax = 0;
    }
    goto label_0;
label_1:
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x62d0 */
 
int64_t dbg_xargmatch_internal (uint32_t arg_50h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    _Bool allow_abbreviation;
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* ptrdiff_t __xargmatch_internal(char const * context,char const * arg,char const * const * arglist, const * vallist,size_t valsize,argmatch_exit_fn exit_fn,_Bool allow_abbreviation); */
    r15 = rdi;
    r14 = rsi;
    r13 = r8;
    r12 = rcx;
    *((rsp + 8)) = r9;
    if (*((rsp + 0x50)) != 0) {
        goto label_2;
    }
    rdi = *(rdx);
    ebx = 0;
    if (rdi != 0) {
        goto label_3;
    }
    do {
        rax = 0xffffffffffffffff;
label_1:
        argmatch_invalid (r15, r14, 0xffffffffffffffff);
        argmatch_valid (rbp, r12, r13);
        rax = *((rsp + 8));
        void (*rax)() ();
        rax = 0xffffffffffffffff;
        goto label_4;
label_0:
        rbx++;
        rdi = *((rbp + rbx*8));
    } while (rdi == 0);
label_3:
    eax = strcmp (rdi, r14);
    if (eax != 0) {
        goto label_0;
    }
    rax = rbx;
    return rax;
label_2:
    rax = argmatch (r14, rbp, r12, r8);
    if (rax < 0) {
        goto label_1;
    }
label_4:
    return rax;
}

/* /tmp/tmp2diozptm @ 0x8b70 */
 
int64_t quotearg_custom (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    rdx = *(fs:0x28);
    *((rsp + 0x38)) = rdx;
    edx = 0;
    __asm ("movdqa xmm1, xmmword [0x000112f0]");
    __asm ("movdqa xmm2, xmmword [0x00011300]");
    rdx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rdx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x280c)() ();
    }
    if (rax == 0) {
        void (*0x280c)() ();
    }
    *((rsp + 0x28)) = rdi;
    rdx = 0xffffffffffffffff;
    edi = 0;
    rcx = rsp;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x88f0 */
 
int64_t dbg_quotearg_colon_mem (int64_t arg1, int64_t arg2, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon_mem(char const * arg,size_t argsize); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000112f0]");
    rdx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    __asm ("movdqa xmm2, xmmword [0x00011300]");
    rsi = rdi;
    *(rsp) = xmm0;
    ecx = *((rsp + 0xc));
    edi = 0;
    *((rsp + 0x30)) = rax;
    eax = ecx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= ecx;
    rcx = rsp;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x9820 */
 
uint64_t dbg_xizalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xizalloc(idx_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x25d0 */
 
void calloc (void) {
    __asm ("bnd jmp qword [reloc.calloc]");
}

/* /tmp/tmp2diozptm @ 0x8190 */
 
uint64_t dbg_set_quoting_style (int64_t arg1, quoting_style s) {
    rdi = arg1;
    rsi = s;
    /* void set_quoting_style(quoting_options * o,quoting_style s); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = esi;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x8cb0 */
 
void quote_n_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = obj_quote_quoting_options;
    return quotearg_n_options ();
}

/* /tmp/tmp2diozptm @ 0x8130 */
 
uint64_t dbg_clone_quoting_options (int64_t arg1) {
    rdi = arg1;
    /* quoting_options * clone_quoting_options(quoting_options * o); */
    rax = errno_location ();
    esi = 0x38;
    r12d = *(rax);
    rbx = rax;
    rax = obj_default_quoting_options;
    if (rbp == 0) {
    }
    rdi = rbp;
    xmemdup ();
    *(rbx) = r12d;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x9890 */
 
uint64_t xmemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2610)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x83d0 */
 
int64_t dbg_quotearg_free (void) {
    /* void quotearg_free(); */
    eax = nslots;
    r12 = slotvec;
    if (eax <= 1) {
        goto label_0;
    }
    eax -= 2;
    rbx = r12 + 0x18;
    rax <<= 4;
    rbp = r12 + rax + 0x28;
    do {
        rbx += 0x10;
        free (*(rbx));
    } while (rbx != rbp);
label_0:
    rdi = *((r12 + 8));
    rbx = obj_slot0;
    if (rdi != rbx) {
        free (rdi);
        *(obj.slot0) = rbx;
        *(obj.slotvec0) = 0x100;
    }
    rbx = obj_slotvec0;
    if (r12 != rbx) {
        free (r12);
        *(obj.slotvec) = rbx;
    }
    *(obj.nslots) = 1;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x6410 */
 
void dbg_close_stdout_set_ignore_EPIPE (_Bool ignore) {
    rdi = ignore;
    /* void close_stdout_set_ignore_EPIPE(_Bool ignore); */
    *(obj.ignore_EPIPE) = dil;
}

/* /tmp/tmp2diozptm @ 0x94a0 */
 
uint64_t dbg_xirealloc (void * ptr, size_t size) {
    rdi = ptr;
    rsi = size;
    /* void * xirealloc(void * p,idx_t s); */
    eax = 0;
    al = (rsi == 0) ? 1 : 0;
    rsi |= rax;
    rax = realloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x9660 */
 
int64_t x2nrealloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    r12 = rdx;
    rbx = *(rsi);
    if (rdi == 0) {
        goto label_1;
    }
    rax = rbx;
    rax >>= 1;
    rax++;
    rbx += rax;
    if (rbx < 0) {
        goto label_2;
    }
    rsi = rbx;
    rax = reallocarray ();
    while (rbx == 0) {
label_0:
        *(rbp) = rbx;
        return rax;
    }
    if (r12 == 0) {
        goto label_0;
    }
    do {
label_2:
        xalloc_die ();
label_1:
        if (rbx == 0) {
            edx = 0;
            eax = 0x80;
            rax = rdx:rax / r12;
            rdx = rdx:rax % r12;
            edx = 0;
            dl = (r12 > 0x80) ? 1 : 0;
            rbx = rax + rdx;
        }
        edi = 0;
        rdx = r12;
        rsi = rbx;
        rax = reallocarray ();
    } while (rax == 0);
    *(rbp) = rbx;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x60b0 */
 
uint64_t argmatch_exact (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    r12 = rdi;
    rdi = *(rsi);
    if (rdi == 0) {
        goto label_0;
    }
    ebx = 0;
    while (eax != 0) {
        rbx++;
        rdi = *((rbp + rbx*8));
        if (rdi == 0) {
            goto label_0;
        }
        eax = strcmp (rdi, r12);
    }
    rax = rbx;
    return rax;
label_0:
    rax = 0xffffffffffffffff;
    return rax;
}

/* /tmp/tmp2diozptm @ 0xa470 */
 
uint64_t dbg_rpl_fclose (int64_t arg1) {
    rdi = arg1;
    /* int rpl_fclose(FILE * fp); */
    eax = fileno (rdi);
    rdi = rbp;
    if (eax < 0) {
        goto label_1;
    }
    eax = freading ();
    while (rax != -1) {
        eax = rpl_fflush (rbp);
        if (eax == 0) {
            goto label_2;
        }
        rax = errno_location ();
        r12d = *(rax);
        rbx = rax;
        fclose (rbp);
        if (r12d != 0) {
            goto label_3;
        }
label_0:
        return rax;
        eax = fileno (rbp);
        esi = 0;
        edx = 1;
        edi = eax;
        rax = lseek ();
    }
label_2:
    rdi = rbp;
label_1:
    void (*0x24c0)() ();
label_3:
    *(rbx) = r12d;
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp2diozptm @ 0x9580 */
 
uint64_t dbg_xinmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xinmalloc(idx_t n,idx_t s); */
    if (rdi == 0) {
        goto label_0;
    }
    if (rsi == 0) {
        goto label_0;
    }
    rdx = rsi;
    rsi = rdi;
    do {
        edi = 0;
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    } while (1);
label_1:
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x8cc0 */
 
void dbg_quote_mem (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    /* char const * quote_mem(char const * arg,size_t argsize); */
    rdx = rsi;
    rcx = obj_quote_quoting_options;
    rsi = rdi;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp2diozptm @ 0x8c10 */
 
int64_t quotearg_custom_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    rax = rsi;
    rsi = rdx;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000112f0]");
    __asm ("movdqa xmm2, xmmword [0x00011300]");
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    rcx = Scrt1.o;
    *(rsp) = xmm0;
    *((rsp + 0x10)) = xmm1;
    *((rsp + 0x30)) = rcx;
    *(rsp) = 0xa;
    *((rsp + 0x20)) = xmm2;
    if (rdi == 0) {
        void (*0x2811)() ();
    }
    if (rax == 0) {
        void (*0x2811)() ();
    }
    *((rsp + 0x28)) = rdi;
    rcx = rsp;
    edi = 0;
    *((rsp + 0x30)) = rax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x61a0 */
 
uint64_t dbg_argmatch_valid (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* void argmatch_valid(char const * const * arglist, const * vallist,size_t valsize); */
    r13d = 0;
    r12 = rdx;
    edx = 5;
    rbx = rdi;
    r14 = stderr;
    *((rsp + 8)) = rdi;
    rax = dcgettext (0, "Valid arguments are:");
    rsi = r14;
    r14 = "\n  - %s";
    rdi = rax;
    fputs_unlocked ();
    r15 = *(rbx);
    ebx = 0;
    if (r15 != 0) {
        goto label_2;
    }
    goto label_3;
    do {
label_0:
        r13 = rbp;
        rax = quote (r15, rsi, rdx, rcx, r8);
        rdi = stderr;
        rdx = r14;
        esi = 1;
        rcx = rax;
        eax = 0;
        fprintf_chk ();
label_1:
        rax = *((rsp + 8));
        rbx++;
        rbp += r12;
        r15 = *((rax + rbx*8));
        if (r15 == 0) {
            goto label_3;
        }
label_2:
    } while (rbx == 0);
    eax = memcmp (r13, rbp, r12);
    if (eax != 0) {
        goto label_0;
    }
    rax = quote (r15, rsi, rdx, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = ", %s";
    rcx = rax;
    eax = 0;
    fprintf_chk ();
    goto label_1;
label_3:
    rdi = stderr;
    rax = *((rdi + 0x28));
    if (rax < *((rdi + 0x30))) {
        rdx = rax + 1;
        *((rdi + 0x28)) = rdx;
        *(rax) = 0xa;
        return rax;
    }
    esi = 0xa;
    return overflow ();
}

/* /tmp/tmp2diozptm @ 0x98d0 */
 
uint64_t dbg_ximemdup (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* void * ximemdup( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi);
    if (rax != 0) {
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2610)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x83c0 */
 
void dbg_quotearg_alloc (int64_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_8h;
    int64_t var_18h;
    int64_t var_34h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* char * quotearg_alloc(char const * arg,size_t argsize,quoting_options const * o); */
    rcx = rdx;
    edx = 0;
    return void (*0x82d0)() ();
}

/* /tmp/tmp2diozptm @ 0x64d0 */
 
uint64_t dbg_mbsalign (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    size_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_24h;
    size_t size;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* size_t mbsalign(char const * src,char * dest,size_t dest_size,size_t * width,mbs_align_t align,int flags); */
    r13 = rdi;
    rbx = rsi;
    *((rsp + 0x18)) = rdx;
    *((rsp + 0x10)) = rcx;
    *((rsp + 0x24)) = r8d;
    rax = strlen (rdi);
    *((rsp + 8)) = rax;
    r9 = rax;
    if ((bpl & 2) == 0) {
        goto label_11;
    }
label_3:
    r12 = r9;
    r15d = 0;
    r14d = 0;
label_5:
    rax = *((rsp + 0x10));
    rax = *(rax);
    if (rax >= r12) {
        goto label_12;
    }
    r9 = rax;
    edx = 0;
label_9:
    rsi = *((rsp + 0x10));
    *(rsi) = rax;
    eax = *((rsp + 0x24));
    if (eax == 0) {
        goto label_13;
    }
label_0:
    r12d = 0;
    if (eax != 1) {
        r12 = rdx;
        edx &= 1;
        r12 >>= 1;
        rdx += r12;
    }
label_1:
    r8 = rdx + r9;
    if ((bpl & 4) != 0) {
        r8 = r9;
        edx = 0;
    }
    ebp &= 8;
    if (ebp != 0) {
        goto label_14;
    }
    r8 += r12;
label_2:
    rax = *((rsp + 0x18));
    if (rax == 0) {
        goto label_4;
    }
    rbp = rbx + rax - 1;
    rdi = rbx;
    if (rbx >= rbp) {
        goto label_15;
    }
    if (rdx != 0) {
        goto label_16;
    }
    goto label_15;
    do {
        if (rbp <= rdi) {
            goto label_15;
        }
label_16:
        rdi++;
        rax = rbx;
        *((rdi - 1)) = 0x20;
        rax -= rdi;
        rax += rdx;
    } while (rax != 0);
label_15:
    rdx = rbp;
    rsi = r13;
    *((rsp + 8)) = r8;
    rdx -= rdi;
    if (rdx > r9) {
        rdx = r9;
    }
    rax = mempcpy ();
    r8 = *((rsp + 8));
    rdx = rax;
    if (rbp <= rax) {
        goto label_17;
    }
    if (r12 != 0) {
        goto label_18;
    }
    goto label_17;
    do {
        if (rbp <= rdx) {
            goto label_17;
        }
label_18:
        rdx++;
        rcx = r12;
        *((rdx - 1)) = 0x20;
        rcx -= rdx;
        rcx += rax;
    } while (rcx != 0);
label_17:
    *(rdx) = 0;
label_4:
    *((rsp + 8)) = r8;
    free (r15);
    free (r14);
    rax = *((rsp + 8));
    return rax;
label_7:
    r14d = 0;
label_12:
    if (r12 >= rax) {
        goto label_19;
    }
    rax -= r12;
    rsi = *((rsp + 0x10));
    rdx = rax;
    rax = r12;
    *(rsi) = rax;
    eax = *((rsp + 0x24));
    if (eax != 0) {
        goto label_0;
    }
label_13:
    r12 = rdx;
    edx = 0;
    goto label_1;
label_14:
    r12d = 0;
    goto label_2;
label_11:
    *((rsp + 0x28)) = rax;
    rax = ctype_get_mb_cur_max ();
    r9 = *((rsp + 0x28));
    if (rax <= 1) {
        goto label_3;
    }
    rax = mbstowcs (0, r13, 0);
    r9 = *((rsp + 0x28));
    if (rax != -1) {
        goto label_20;
    }
    if ((bpl & 1) != 0) {
        goto label_3;
    }
label_10:
    r15d = 0;
    r14d = 0;
    r8 = 0xffffffffffffffff;
    goto label_4;
label_20:
    r8 = rax + 1;
    *((rsp + 0x30)) = r9;
    rax = r8*4;
    *((rsp + 0x28)) = r8;
    rdi = rax;
    *((rsp + 0x38)) = rax;
    rax = malloc (rdi);
    r8 = *((rsp + 0x28));
    r9 = *((rsp + 0x30));
    r15 = rax;
    if (rax == 0) {
        goto label_21;
    }
    rdx = r8;
    *((rsp + 0x30)) = r9;
    *((rsp + 0x28)) = r8;
    r14d = 0;
    rax = mbstowcs (rax, r13, rdx);
    r9 = *((rsp + 0x30));
    r8 = *((rsp + 0x28));
    r12 = r9;
    if (rax == 0) {
        goto label_5;
    }
    rax = *((rsp + 0x38));
    *((r15 + rax - 4)) = 0;
    edi = *(r15);
    if (edi == 0) {
        goto label_22;
    }
    r12 = r15;
    do {
        *((rsp + 0x30)) = r8;
        *((rsp + 0x28)) = r9;
        eax = iswprint (rdi);
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x30));
        if (eax == 0) {
            *(r12) = 0xfffd;
            r14d = 1;
        }
        edi = *((r12 + 4));
        r12 += 4;
    } while (edi != 0);
    rsi = r8;
    rdi = r15;
    *((rsp + 0x28)) = r9;
    eax = wcswidth ();
    r9 = *((rsp + 0x28));
    r12 = (int64_t) eax;
    if (r14b == 0) {
        goto label_23;
    }
    *((rsp + 8)) = r9;
    rax = wcstombs (0, r15, 0);
    r9 = *((rsp + 8));
    rax++;
    *((rsp + 0x28)) = rax;
label_8:
    *((rsp + 8)) = r9;
    rax = malloc (*((rsp + 0x28)));
    r9 = *((rsp + 8));
    r14 = rax;
    if (rax == 0) {
        goto label_24;
    }
    rax = *((rsp + 0x10));
    edi = *(r15);
    r13 = r15;
    r12d = 0;
    rax = *(rax);
    *((rsp + 8)) = rax;
    if (edi != 0) {
        goto label_25;
    }
    goto label_26;
    do {
        rax = (int64_t) eax;
        rax += r12;
        if (*((rsp + 8)) < rax) {
            goto label_26;
        }
label_6:
        edi = *((r13 + 4));
        r13 += 4;
        r12 = rax;
        if (edi == 0) {
            goto label_26;
        }
label_25:
        eax = wcwidth ();
    } while (eax != 0xffffffff);
    eax = 1;
    *(r13) = 0xfffd;
    rax += r12;
    if (*((rsp + 8)) >= rax) {
        goto label_6;
    }
label_26:
    *(r13) = 0;
    rdi = r14;
    r13 = r14;
    rax = wcstombs (rdi, r15, *((rsp + 0x28)));
    r9 = rax;
    goto label_5;
label_22:
    rsi = r8;
    rdi = r15;
    *((rsp + 0x28)) = r9;
    eax = wcswidth ();
    r9 = *((rsp + 0x28));
    r12 = (int64_t) eax;
label_23:
    rax = *((rsp + 0x10));
    rax = *(rax);
    if (rax >= r12) {
        goto label_7;
    }
    rax = *((rsp + 8));
    rax++;
    *((rsp + 0x28)) = rax;
    goto label_8;
label_24:
    r8 |= 0xffffffffffffffff;
    if ((bpl & 1) != 0) {
        goto label_5;
    }
    goto label_4;
label_19:
    rax = r12;
    edx = 0;
    goto label_9;
label_21:
    if ((bpl & 1) == 0) {
        goto label_10;
    }
    r12 = r9;
    r14d = 0;
    goto label_5;
}

/* /tmp/tmp2diozptm @ 0x99d0 */
 
int64_t dbg_xstrtol (int64_t arg1, int64_t arg2, uint32_t arg3, int64_t arg4, int64_t arg5) {
    char * t_ptr;
    int64_t var_8h;
    int64_t var_14h;
    int64_t var_18h;
    int64_t var_1fh;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtol(char const * s,char ** ptr,int strtol_base,long int * val,char const * valid_suffixes); */
    *(rsp) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_25;
    }
    rax = rsp + 0x20;
    r13 = rdi;
    if (rsi == 0) {
    }
    r12d = edx;
    r14 = r8;
    errno_location ();
    *(rax) = 0;
    r15 = rax;
    rax = strtol (r13, rbp, r12d);
    rdx = *(rbp);
    rbx = rax;
    if (rdx == r13) {
        goto label_26;
    }
    eax = *(r15);
    if (eax != 0) {
        goto label_27;
    }
    r12d = 0;
label_0:
    if (r14 != 0) {
        r13d = *(rdx);
        if (r13b != 0) {
            goto label_28;
        }
    }
label_3:
    rax = *(rsp);
    *(rax) = rbx;
    do {
label_1:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_29;
        }
        eax = r12d;
        return rax;
label_27:
        r12d = 4;
    } while (eax != 0x22);
    r12d = 1;
    goto label_0;
label_26:
    if (r14 == 0) {
        goto label_30;
    }
    r13d = *(rdx);
    *((rsp + 8)) = rdx;
    r12d = 4;
    if (r13b == 0) {
        goto label_1;
    }
    esi = (int32_t) r13b;
    r12d = 0;
    ebx = 1;
    rax = strchr (r14, rsi);
    rdx = *((rsp + 8));
    if (rax == 0) {
        goto label_30;
    }
    do {
        r8d = r13 - 0x45;
        r9d = 1;
        ecx = section..dynsym;
        if (r8b <= 0x2f) {
            rax = 0x814400308945;
            if (((rax >> r8) & 1) < 0) {
                goto label_31;
            }
        }
label_2:
        r13d -= 0x42;
        if (r13b > 0x35) {
            goto label_32;
        }
        r13d = (int32_t) r13b;
        rax = *((rsi + r13*4));
        rax += rsi;
        /* switch table (54 cases) at 0xd7f8 */
        void (*rax)() ();
label_28:
        esi = (int32_t) r13b;
        *((rsp + 8)) = rdx;
        rax = strchr (r14, 0x0000d7f8);
        rdx = *((rsp + 8));
    } while (rax != 0);
label_32:
    rax = *(rsp);
    r12d |= 2;
    *(rax) = rbx;
    goto label_1;
label_31:
    *((rsp + 0x18)) = r9d;
    *((rsp + 0x14)) = ecx;
    *((rsp + 8)) = rdx;
    *((rsp + 0x1f)) = r8b;
    rax = strchr (r14, 0x30);
    rdx = *((rsp + 8));
    ecx = section..dynsym;
    r9d = 1;
    if (rax == 0) {
        goto label_2;
    }
    eax = *((rdx + 1));
    if (al == 0x44) {
        goto label_33;
    }
    if (al == 0x69) {
        goto label_34;
    }
    r8d = *((rsp + 0x1f));
    if (al == 0x42) {
        goto label_33;
    }
    rcx = 0x0000d8d0;
    rax = *((rcx + r8*4));
    rax += rcx;
    /* switch table (48 cases) at 0xd8d0 */
    void (*rax)() ();
label_30:
    r12d = 4;
    goto label_1;
    r9 = (int64_t) r9d;
label_10:
    esi = 4;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rax overflow 0) {
            goto label_35;
        }
        rbx = rcx;
label_21:
        esi--;
    } while (esi != 0);
label_6:
    r12d |= edi;
    do {
label_5:
        rax = rdx + r9;
        edx = r12d;
        edx |= 2;
        *(rbp) = rax;
        if (*(rax) != 0) {
            r12d = edx;
        }
        goto label_3;
        r9 = (int64_t) r9d;
label_11:
        rax = (int64_t) ecx;
        rcx = rbx;
        rcx *= rax;
        if (*(rax) overflow 0) {
            goto label_9;
        }
        rax *= rcx;
        if (*(rax) overflow 0) {
            goto label_36;
        }
label_4:
        rbx = rax;
    } while (1);
    r9 = (int64_t) r9d;
label_12:
    rax = (int64_t) ecx;
    rax *= rbx;
    if (*(rax) !overflow 0) {
        goto label_4;
    }
label_9:
    r12d = 1;
    if (rbx < 0) {
        goto label_37;
    }
label_7:
    rbx = 0x7fffffffffffffff;
    goto label_5;
    r9 = (int64_t) r9d;
label_13:
    esi = 3;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rbx overflow 0) {
            goto label_38;
        }
        rbx = rcx;
label_20:
        esi--;
    } while (esi != 0);
    goto label_6;
    rax = rbx * section..dynsym;
    r9 = (int64_t) r9d;
    if (esi !overflow 0) {
        goto label_4;
    }
label_8:
    r12d = 1;
    if (rbx >= 0) {
        goto label_7;
    }
    r12d = 1;
label_37:
    rbx = 0x8000000000000000;
    goto label_5;
    rax = rbx * 2;
    r9 = (int64_t) r9d;
    if (rbx !overflow 0) {
        goto label_4;
    }
    goto label_8;
    r9 = (int64_t) r9d;
label_16:
    esi = 5;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (rbx overflow 0) {
            goto label_39;
        }
        rbx = rcx;
label_22:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_14:
    esi = 6;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_40;
        }
        rbx = rcx;
label_23:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
    goto label_5;
    r9 = (int64_t) r9d;
label_17:
    esi = 7;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_41;
        }
        rbx = rcx;
label_19:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_18:
    esi = 8;
    edi = 0;
    rax = (int64_t) ecx;
    r10 = 0x8000000000000000;
    r8 = 0x7fffffffffffffff;
    do {
        rcx = rbx;
        rcx *= rax;
        if (esi overflow 0) {
            goto label_42;
        }
        rbx = rcx;
label_24:
        esi--;
    } while (esi != 0);
    goto label_6;
    r9 = (int64_t) r9d;
label_15:
    rax = rbx * 0x200;
    if (esi !overflow 0) {
        goto label_4;
    }
    goto label_9;
    r9d = 1;
    ecx = section..dynsym;
    goto label_10;
    r9d = 1;
    ecx = section..dynsym;
    goto label_11;
    r9d = 1;
    ecx = section..dynsym;
    goto label_12;
    r9d = 1;
    ecx = section..dynsym;
    goto label_13;
    r9d = 1;
    ecx = section..dynsym;
    goto label_14;
    r9d = 1;
    goto label_15;
    r9d = 1;
    ecx = section..dynsym;
    goto label_16;
    r9d = 1;
    ecx = section..dynsym;
    goto label_17;
    r9d = 1;
    ecx = section..dynsym;
    goto label_18;
    r9d = 1;
    goto label_5;
label_33:
    r9d = 2;
    ecx = 0x3e8;
    goto label_2;
label_34:
    r9d = 0;
    r9b = (*((rdx + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_2;
label_25:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtol");
label_29:
    stack_chk_fail ();
label_41:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_19;
    }
    rbx = r10;
    goto label_19;
label_38:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_20;
    }
    rbx = r10;
    goto label_20;
label_35:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_21;
    }
    rbx = r10;
    goto label_21;
label_39:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_22;
    }
    rbx = r10;
    goto label_22;
label_40:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_23;
    }
    rbx = r10;
    goto label_23;
label_42:
    edi = 1;
    if (rbx >= 0) {
        rbx = r8;
        goto label_24;
    }
    rbx = r10;
    goto label_24;
label_36:
    r12d = 1;
    rbx = 0x8000000000000000;
    rax = 0x7fffffffffffffff;
    __asm ("cmovns rbx, rax");
    goto label_5;
}

/* /tmp/tmp2diozptm @ 0x59b0 */
 
uint64_t dbg_set_fields (int64_t arg_8h, int64_t arg_10h, uint32_t arg1, int64_t arg2, int64_t arg4) {
    int64_t var_30h;
    int64_t var_10h;
    int64_t var_eh;
    int64_t var_fh;
    int64_t var_sp_10h;
    int64_t var_18h;
    int64_t var_1ch;
    rdi = arg1;
    rsi = arg2;
    rcx = arg4;
    /* void set_fields(char const * fieldstr,unsigned int options); */
    eax = esi;
    rbx = rdi;
    eax &= 1;
    *((rsp + 0x1c)) = esi;
    *((rsp + 0x18)) = eax;
    if (eax != 0) {
        if (*(rdi) == 0x2d) {
            goto label_12;
        }
    }
label_5:
    edx = 0;
    edi = 0;
label_6:
    rbx++;
    esi = edx;
    r13d = 0;
    r15d = 0;
    r12d = 1;
    while (bpl != 0x2d) {
        if (bpl != 0x2c) {
            *((rsp + 0x10)) = rdi;
            *((rsp + 0xf)) = dl;
            *((rsp + 0xe)) = sil;
            rax = ctype_b_loc ();
            esi = *((rsp + 0xe));
            edx = *((rsp + 0xf));
            r10 = rax;
            eax = (int32_t) bpl;
            rdi = *((rsp + 0x10));
            r10 = *(r10);
            if ((*((r10 + rax*2)) & 1) != 0) {
                goto label_13;
            }
            if (bpl != 0) {
                goto label_14;
            }
        }
label_13:
        if (sil == 0) {
            goto label_15;
        }
        if (dl != 0) {
            goto label_16;
        }
        if (r15b != 0) {
            goto label_17;
        }
        eax = *((rsp + 0x18));
        if (eax == 0) {
            goto label_18;
        }
        r12d = 1;
label_3:
        add_range_pair (r12, 0xffffffffffffffff);
label_4:
        if (*((rbx - 1)) == 0) {
            goto label_19;
        }
label_1:
        r13d = 0;
        esi = 0;
        r15d = 0;
        edx = 0;
        edi = 0;
label_0:
        rbx++;
        ebp = *((rbx - 1));
        r14 = rbx - 1;
    }
    if (sil != 0) {
        goto label_20;
    }
    cl = (rdi == 0) ? 1 : 0;
    cl &= dl;
    r13d = ecx;
    if (cl != 0) {
        goto label_21;
    }
    if (dl == 0) {
        goto label_22;
    }
    r12 = rdi;
    esi = edx;
    edi = 0;
    goto label_0;
label_15:
    if (rdi == 0) {
        goto label_23;
    }
    add_range_pair (rdi, rdi);
    if (*((rbx - 1)) != 0) {
        goto label_1;
    }
label_19:
    rsi = n_frp;
    if (rsi == 0) {
        goto label_24;
    }
    rdi = frp;
    rcx = dbg_compare_ranges;
    edx = 0x10;
    r14d = 0;
    qsort ();
    rbx = n_frp;
    r15 = frp;
    if (rbx == 0) {
        goto label_25;
    }
label_2:
    r14++;
    if (r14 >= rbx) {
        goto label_25;
    }
    r13 = rbp - 0x10;
    r12 = rbp + 0x10;
    while (*(rdi) <= rdx) {
        rax = *((rdi + 8));
        if (rax < rdx) {
            rax = rdx;
        }
        rbx -= r14;
        *((rsi + 8)) = rax;
        rdx <<= 4;
        memmove (rdi, r15 + r12, rbx - 1);
        rax = n_frp;
        r15 = frp;
        rbx = rax - 1;
        *(obj.n_frp) = rbx;
        if (rbx <= r14) {
            goto label_25;
        }
        rsi = r15 + r13;
        rdi = r15 + rbp;
        rdx = *((rsi + 8));
    }
    rbx = n_frp;
    if (r14 < rbx) {
        goto label_2;
    }
label_25:
    if ((*((rsp + 0x1c)) & 2) != 0) {
        goto label_26;
    }
label_7:
    rax = n_frp;
    rdi = r15;
    rsi = rax + 1;
    *(obj.n_frp) = rsi;
    rsi <<= 4;
    rax = xrealloc ();
    rdx = n_frp;
    *(obj.frp) = rax;
    rdx <<= 4;
    rax = rax + rdx - 0x10;
    *((rax + 8)) = 0xffffffffffffffff;
    *(rax) = 0xffffffffffffffff;
    return rax;
label_16:
    if (r15b == 0) {
        goto label_3;
    }
label_17:
    if (r12 > rdi) {
        goto label_27;
    }
    add_range_pair (r12, rdi);
    goto label_4;
label_22:
    esi = 1;
    edi = 0;
    r12d = 1;
    goto label_0;
label_14:
    ebp = (int32_t) bpl;
    eax = rbp - 0x30;
    if (eax > 9) {
        goto label_28;
    }
    if (r13b == 0) {
        goto label_29;
    }
    while (1) {
        eax = 1;
        if (sil == 0) {
            edx = eax;
        }
        if (sil != 0) {
            r15d = esi;
        }
        rax = 0x1999999999999999;
        if (rdi > rax) {
            goto label_30;
        }
        ebp -= 0x30;
        rax = rdi * 5;
        rbp = (int64_t) ebp;
        rax = rbp + rax*2;
        if (rax < rdi) {
            goto label_30;
        }
        if (rax == -1) {
            goto label_30;
        }
        rdi = rax;
        r13d = 1;
        goto label_0;
label_29:
        *(0x000111a0) = r14;
    }
label_12:
    if (*((rdi + 1)) != 0) {
        goto label_5;
    }
    rbx++;
    edx = 1;
    edi = 1;
    goto label_6;
label_26:
    *(obj.frp) = 0;
    rax = *(r15);
    *(obj.n_frp) = 0;
    *(obj.n_frp_allocated) = 0;
    if (rax > 1) {
        goto label_31;
    }
label_8:
    rbp = r15 + 8;
    r12d = 1;
    if (rbx <= 1) {
        goto label_32;
    }
    do {
        rax = *(rbp);
        rsi = *((rbp + 8));
        rdi = rax + 1;
        if (rdi != rsi) {
            rsi--;
            add_range_pair (rdi, rsi);
        }
        r12++;
        rbp += 0x10;
    } while (r12 != rbx);
label_32:
    rbx <<= 4;
    rax = *((r15 + rbx - 8));
    if (rax == -1) {
        free (r15);
        r15 = frp;
        goto label_7;
    }
    add_range_pair (rax + 1, 0xffffffffffffffff);
    free (r15);
    r15 = frp;
    goto label_7;
label_31:
    add_range_pair (1, rax - 1);
    goto label_8;
label_18:
    edx = 5;
    do {
label_11:
        rax = dcgettext (0, "invalid range with no endpoint: -");
label_9:
        eax = 0;
        error (0, 0, rax);
        usage (1);
label_30:
        rbp = Scrt1.o;
        rax = strspn (rbp, "0123456789");
        rax = ximemdup0 (rbp, rax);
        rdi = rax;
        rax = quote (rdi, rsi, rdx, rcx, r8);
        edx = 5;
        r12 = rax;
        if ((*((rsp + 0x1c)) & 4) == 0) {
            goto label_33;
        }
        rax = dcgettext (0, "byte/character offset %s is too large");
label_10:
        rcx = r12;
        eax = 0;
        error (0, 0, rax);
        free (rbp);
        usage (1);
label_27:
        edx = 5;
        rsi = "invalid decreasing range";
    } while (1);
label_21:
    edx = 5;
    if ((*((rsp + 0x1c)) & 4) == 0) {
        goto label_34;
    }
    rax = dcgettext (0, "byte/character positions are numbered from 1");
    rdx = rax;
    goto label_9;
label_20:
    edx = 5;
    if ((*((rsp + 0x1c)) & 4) == 0) {
        goto label_35;
    }
    rax = dcgettext (0, "invalid byte or character range");
    rdx = rax;
    goto label_9;
    do {
label_34:
        rax = dcgettext (0, "fields are numbered from 1");
        rdx = rax;
        goto label_9;
label_35:
        rax = dcgettext (0, "invalid field range");
        rdx = rax;
        goto label_9;
label_23:
        edx = 5;
    } while ((*((rsp + 0x1c)) & 4) == 0);
    rax = dcgettext (rdi, "byte/character positions are numbered from 1");
    rdx = rax;
    goto label_9;
label_33:
    rax = dcgettext (0, "field number %s is too large");
    rdx = rax;
    goto label_10;
label_24:
    edx = 5;
    if ((*((rsp + 0x1c)) & 4) == 0) {
        goto label_36;
    }
    rax = dcgettext (0, "missing list of byte/character positions");
    rdx = rax;
    goto label_9;
label_28:
    rax = quote (r14, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    if ((*((rsp + 0x1c)) & 4) == 0) {
        goto label_37;
    }
    rax = dcgettext (0, "invalid byte/character position %s");
    do {
        rcx = r12;
        eax = 0;
        error (0, 0, rax);
        usage (1);
label_36:
        rsi = "missing list of fields";
        goto label_11;
label_37:
        rax = dcgettext (0, "invalid field value %s");
        rdx = rax;
    } while (1);
}

/* /tmp/tmp2diozptm @ 0x84a0 */
 
void dbg_quotearg (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char * quotearg(char const * arg); */
    rsi = rdi;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp2diozptm @ 0x2820 */
 
void dbg_main (int32_t argc, char ** argv) {
    char * line;
    char * endptr;
    char * var_8h;
    char * str;
    long * var_18h;
    int64_t var_28h;
    uint32_t var_30h;
    int64_t var_38h;
    rdi = argc;
    rsi = argv;
    /* int main(int argc,char ** argv); */
    r12 = 0x0000b1ab;
    rbx = rsi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    set_program_name (*(rsi), rsi, rdx);
    rax = setlocale (6, 0x0000b0d6);
    *((rsp + 8)) = rax;
    bindtextdomain (r12, "/usr/local/share/locale");
    textdomain (r12, rsi);
    rax = nl_langinfo (0x10000);
    *(obj.decimal_point) = rax;
    if (rax == 0) {
        goto label_24;
    }
    while (1) {
        r14 = obj_longopts;
        r13 = 0x0000b200;
        r12 = 0x0000cc58;
        r15 = obj_inval_types;
        eax = strlen (*(obj.decimal_point));
        rdi = dbg_close_stdout;
        *(obj.decimal_point_length) = eax;
        rax = atexit ();
label_0:
        r8d = 0;
        rcx = r14;
        rdx = r13;
        rsi = rbx;
        edi = ebp;
        eax = getopt_long ();
        if (eax == 0xffffffff) {
            goto label_25;
        }
        if (eax > 0x8d) {
            goto label_26;
        }
        if (eax <= 0x63) {
            goto label_27;
        }
        eax -= 0x64;
        if (eax > 0x29) {
            goto label_26;
        }
        rax = *((r12 + rax*4));
        rax += r12;
        /* switch table (42 cases) at 0xcc58 */
        void (*rax)() ();
label_24:
        rax = 0x0000b1fe;
        *(obj.decimal_point) = rax;
    }
    *(obj.dev_debug) = 1;
    *(obj.debug) = 1;
    goto label_0;
label_27:
    if (eax == 0xffffff7d) {
        goto label_28;
    }
    if (eax == 0xffffff7e) {
        usage (0);
    }
label_26:
    usage (1);
    _xargmatch_internal ("--invalid", *(obj.optarg), obj.inval_args, r15, 4, *(obj.argmatch_die));
    eax = *((r15 + rax*4));
    *(obj.inval_style) = eax;
    goto label_0;
    rax = optarg;
    *(obj.format_str) = rax;
    goto label_0;
    rdi = optarg;
    if (rdi == 0) {
        goto label_29;
    }
    eax = xstrtoumax (rdi, 0, 0xa, obj.header, 0x0000b0d6);
    if (eax != 0) {
        goto label_30;
    }
    if (*(obj.header) != 0) {
        goto label_0;
    }
label_30:
    rax = quote (*(obj.optarg), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid header value %s");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
    if (*(obj.n_frp) != 0) {
        goto label_31;
    }
    set_fields (*(obj.optarg), 1, rdx, rcx, r8);
    goto label_0;
    eax = xstrtol (*(obj.optarg), 0, 0xa, obj.padding_width, 0x0000b0d6);
    if (eax != 0) {
        goto label_32;
    }
    rax = padding_width;
    rdx = padding_width;
    __asm ("btr rdx, 0x3f");
    if (rdx == 0) {
        goto label_32;
    }
    if (rax >= 0) {
        goto label_0;
    }
    *(obj.padding_alignment) = 0;
    rax = -rax;
    *(obj.padding_width) = rax;
    goto label_0;
    *(obj.grouping) = 1;
    goto label_0;
    rax = optarg;
    *(obj.suffix) = rax;
    goto label_0;
    _xargmatch_internal ("--round", *(obj.optarg), obj.round_args, obj.round_types, 4, *(obj.argmatch_die));
    rcx = obj_round_types;
    eax = *((rcx + rax*4));
    *(obj.round_style) = eax;
    goto label_0;
    rax = unit_to_umax (*(obj.optarg));
    *(obj.to_unit_size) = rax;
    goto label_0;
    _xargmatch_internal ("--to", *(obj.optarg), obj.scale_to_args, obj.scale_to_types, 4, *(obj.argmatch_die));
    rcx = obj_scale_to_types;
    eax = *((rcx + rax*4));
    *(obj.scale_to) = eax;
    goto label_0;
    rax = unit_to_umax (*(obj.optarg));
    *(obj.from_unit_size) = rax;
    goto label_0;
    _xargmatch_internal ("--from", *(obj.optarg), obj.scale_from_args, obj.scale_from_types, 4, *(obj.argmatch_die));
    rcx = obj_scale_from_types;
    eax = *((rcx + rax*4));
    *(obj.scale_from) = eax;
    goto label_0;
    *(obj.line_delim) = 0;
    goto label_0;
    rdx = optarg;
    eax = *(rdx);
    if (al != 0) {
        if (*((rdx + 1)) != 0) {
            goto label_33;
        }
    }
    *(obj.delimiter) = eax;
    goto label_0;
label_28:
    eax = 0;
    version_etc (*(obj.stdout), "numfmt", "GNU coreutils", *(obj.Version), "Assaf Gordon", 0);
    exit (0);
label_29:
    *(obj.header) = 1;
    goto label_0;
label_25:
    if (*(obj.format_str) != 0) {
        if (*(obj.grouping) != 0) {
            goto label_34;
        }
    }
    if (*(obj.debug) != 0) {
        if (*((rsp + 8)) == 0) {
            goto label_35;
        }
    }
label_19:
    r12 = format_str;
    if (*(obj.debug) != 0) {
        eax = scale_from;
        eax |= *(obj.scale_to);
        if (eax == 0) {
            goto label_36;
        }
    }
label_9:
    if (r12 == 0) {
        goto label_12;
    }
label_8:
    *((rsp + 0x30)) = 0;
    edx = 1;
    eax = 0;
    while (cl != 0x25) {
        if (cl == 0) {
            goto label_37;
        }
        ecx = 1;
label_1:
        rax += rcx;
        rdx++;
        ecx = *((r12 + rax));
        r15 = rdx - 1;
    }
    r13 = rax + 1;
    if (*((r12 + rax + 1)) != 0x25) {
        goto label_38;
    }
    ecx = 2;
    goto label_1;
label_2:
    if (r15 == 0) {
        goto label_39;
    }
label_23:
    rax = ximemdup0 (r12, r15);
    *(obj.format_str_prefix) = rax;
    if (*(r13) != 0) {
        goto label_39;
    }
label_22:
    if (*(obj.dev_debug) != 0) {
        rsi = format_str_suffix;
        if (rsi == 0) {
            rsi = 0x0000b0d6;
        }
label_11:
        edi = 2;
        rax = quote_n ();
        rsi = format_str_prefix;
        rdx = rax;
        if (rsi == 0) {
            goto label_40;
        }
label_20:
        edi = 1;
        *((rsp + 0x10)) = rdx;
        r14 = "Left";
        r13 = 0x0000b1df;
        rax = quote_n ();
        r9 = padding_width;
        rsi = r12;
        r15 = rax;
        rax = "Right";
        if (*(obj.padding_alignment) != 0) {
            r14 = rax;
        }
        rax = 0x0000b1e3;
        *((rsp + 8)) = r9;
        if (*(obj.grouping) == 0) {
            r13 = rax;
        }
        edi = 0;
        rax = quote_n ();
        rdi = stderr;
        esi = 1;
        rcx = rax;
        rdx = *((rsp + 0x18));
        r8 = r13;
        eax = 0;
        rdx = "format String:\n  input: %s\n  grouping: %s\n  padding width: %ld\n  alignment: %s\n  prefix: %s\n  suffix: %s\n";
        r9 = *((rsp + 0x28));
        fprintf_chk ();
    }
label_12:
    if (*(obj.grouping) != 0) {
        if (*(obj.scale_to) != 0) {
            goto label_41;
        }
label_21:
        if (*(obj.debug) != 0) {
            goto label_42;
        }
    }
label_14:
    rax = padding_width;
    if (rax >= *(obj.padding_buffer_size)) {
        rdi = padding_buffer;
        rsi = rax + 1;
        *(obj.padding_buffer_size) = rsi;
        rax = xrealloc ();
        *(obj.padding_buffer) = rax;
    }
    eax = 0;
    if (*(obj.padding_width) == 0) {
        eax = 0;
        al = (*(obj.delimiter) == 0x80) ? 1 : 0;
    }
    *(obj.auto_padding) = eax;
    if (*(obj.inval_style) != 0) {
        *(obj.conv_exit_code) = 0;
    }
    if (*(obj.optind) >= ebp) {
        goto label_43;
    }
    if (*(obj.debug) != 0) {
        goto label_44;
    }
label_10:
    r12d = 1;
    while (eax < ebp) {
        eax = process_line (*((rbx + rax*8)), 1);
        *(obj.optind)++;
        r12d &= eax;
        rax = *(obj.optind);
    }
label_7:
    if (*(obj.debug) == 0) {
        goto label_45;
    }
    if (r12d == 0) {
        goto label_46;
    }
label_5:
label_6:
    exit (0);
label_38:
    *((rsp + 8)) = 0;
    r14 = 0x0000b2b1;
    do {
label_4:
        rax = strspn (r12 + r13, r14);
        r13 += rax;
        rdi = r12 + r13;
        edx = *(rdi);
        if (dl == 0x27) {
            goto label_47;
        }
        if (dl == 0x30) {
            goto label_48;
        }
    } while (rax != 0);
    *((rsp + 0x10)) = rdi;
    r13 = rsp + 0x30;
    errno_location ();
    *(rax) = 0;
    *((rsp + 0x18)) = rax;
    rax = strtol (*((rsp + 0x10)), r13, 0xa);
    rcx = *((rsp + 0x18));
    r14 = rax;
    if (*(rcx) == 0x22) {
        goto label_49;
    }
    eax = 1;
    rdi = *((rsp + 0x10));
    rax <<= 0x3f;
    if (r14 == rax) {
        goto label_49;
    }
    if (*((rsp + 0x30)) != rdi) {
        if (r14 == 0) {
            goto label_18;
        }
        if (*(obj.debug) == 0) {
            goto label_50;
        }
        if (*(obj.padding_width) == 0) {
            goto label_50;
        }
        if (*((rsp + 8)) == 0) {
            goto label_51;
        }
        if (r14 <= 0) {
            goto label_51;
        }
label_17:
        *(obj.zero_padding_width) = r14;
    }
label_18:
    rdx = *((rsp + 0x30));
    rax = *((rsp + 0x30));
    edx = *(rdx);
    rax -= r12;
    if (dl == 0) {
        goto label_52;
    }
    if (dl == 0x2e) {
        goto label_53;
    }
label_15:
    if (*((r12 + rax)) != 0x66) {
        goto label_54;
    }
    rax++;
    r13 = r12 + rax;
    edx = *(r13);
    if (dl != 0) {
        goto label_55;
    }
    goto label_56;
label_3:
    rax += rcx;
    edx = *((r12 + rax));
    if (dl == 0) {
        goto label_2;
    }
label_55:
    esi = *((r12 + rax + 1));
    ecx = 1;
    if (dl != 0x25) {
        goto label_3;
    }
    if (sil != 0x25) {
        goto label_57;
    }
    ecx = 2;
    goto label_3;
label_47:
    *(obj.grouping) = 1;
    r13++;
    rdi = r12 + r13;
    goto label_4;
label_48:
    r13++;
    *((rsp + 8)) = 1;
    rdi = r12 + r13;
    goto label_4;
label_45:
    if (r12d != 0) {
        goto label_5;
    }
label_13:
    eax = inval_style;
    edi = 2;
    eax -= 2;
    if (eax <= 1) {
        goto label_5;
    }
    goto label_6;
label_43:
    *((rsp + 0x28)) = 0;
    r13 = rsp + 0x30;
    rbx = rsp + 0x28;
    *((rsp + 0x30)) = 0;
    while (rax != 0) {
        rsi = r13;
        rdi = rbx;
        rax = getdelim ();
        if (rax <= 0) {
            goto label_58;
        }
        rsi = stdout;
        rdi = *((rsp + 0x28));
        fputs_unlocked ();
        rax = header;
        rcx = stdin;
        rdx = rax - 1;
        *(obj.header) = rdx;
        edx = *(obj.line_delim);
    }
label_16:
    r12d = 1;
    while (rax > 0) {
        rdi = *((rsp + 0x28));
        edx = *(obj.line_delim);
        rax = rdi + rax - 1;
        ecx = *(rax);
        sil = (ecx == edx) ? 1 : 0;
        if (ecx == edx) {
            *(rax) = 0;
        }
        esi = (int32_t) sil;
        eax = process_line (*((rsp + 0x28)), rsi);
        rcx = stdin;
        edx = *(obj.line_delim);
        r12d &= eax;
        rsi = r13;
        rdi = rbx;
        rax = getdelim ();
    }
    rax = stdin;
    if ((*(rax) & 0x20) == 0) {
        goto label_7;
    }
    edx = 5;
    rax = dcgettext (0, "error reading input");
    r13 = rax;
    rax = errno_location ();
    eax = 0;
    error (0, *(rax), r13);
    goto label_7;
label_36:
    if (*(obj.grouping) != 0) {
        goto label_59;
    }
    if (*(obj.padding_width) != 0) {
        goto label_60;
    }
    if (r12 != 0) {
        goto label_8;
    }
    edx = 5;
    rax = dcgettext (0, "no conversion option specified");
    eax = 0;
    error (0, 0, rax);
    r12 = format_str;
    goto label_9;
label_44:
    if (*(obj.header) == 0) {
        goto label_10;
    }
    edx = 5;
    rax = dcgettext (0, "--header ignored with command-line input");
    eax = 0;
    error (0, 0, rax);
    goto label_10;
label_39:
    rax = xstrdup (r13);
    *(obj.format_str_suffix) = rax;
    rsi = rax;
    if (*(obj.dev_debug) != 0) {
        goto label_11;
    }
    goto label_12;
label_46:
    edx = 5;
    rax = dcgettext (0, "failed to convert some of the input numbers");
    eax = 0;
    error (0, 0, rax);
    goto label_13;
label_42:
    rax = nl_langinfo (0x10001);
    if (*(rax) != 0) {
        goto label_14;
    }
    edx = 5;
    rax = dcgettext (0, "grouping has no effect in this locale");
    eax = 0;
    error (0, 0, rax);
    goto label_14;
label_53:
    *(rcx) = 0;
    r14 = r12 + rax + 1;
    *((rsp + 8)) = rcx;
    rax = strtol (r14, r13, 0xa);
    rcx = *((rsp + 8));
    *(obj.user_precision) = rax;
    if (*(rcx) == 0x22) {
        goto label_61;
    }
    if (rax < 0) {
        goto label_61;
    }
    rax = ctype_b_loc ();
    rcx = *(r14);
    r9 = rax;
    rdx = *(r9);
    if ((*((rdx + rcx*2)) & 1) != 0) {
        goto label_61;
    }
    if (cl == 0x2b) {
        goto label_61;
    }
    rax = *((rsp + 0x30));
    rax -= r12;
    goto label_15;
label_58:
    rcx = stdin;
    edx = *(obj.line_delim);
    goto label_16;
label_51:
    edx = 5;
    *((rsp + 0x10)) = rcx;
    rax = dcgettext (0, "--format padding overriding --padding");
    eax = 0;
    error (0, 0, rax);
    rcx = *((rsp + 0x10));
label_50:
    if (r14 < 0) {
        goto label_62;
    }
    if (*((rsp + 8)) != 0) {
        goto label_17;
    }
    *(obj.padding_width) = r14;
    goto label_18;
label_35:
    edx = 5;
    rax = dcgettext (0, "failed to set locale");
    eax = 0;
    error (0, 0, rax);
    goto label_19;
label_40:
    rsi = 0x0000b0d6;
    goto label_20;
label_62:
    *(obj.padding_alignment) = 0;
    r14 = -r14;
    *(obj.padding_width) = r14;
    goto label_18;
label_59:
    if (r12 != 0) {
        goto label_8;
    }
    goto label_21;
label_60:
    if (r12 != 0) {
        goto label_8;
    }
    goto label_14;
label_56:
    if (r15 == 0) {
        goto label_22;
    }
    goto label_23;
label_37:
    rax = quote (r12, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "format %s has no %% directive");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_32:
    rax = quote (*(obj.optarg), rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid padding value %s");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_34:
    edx = 5;
    rax = dcgettext (0, "--grouping cannot be combined with --format");
    eax = 0;
    error (1, 0, rax);
label_33:
    edx = 5;
    rax = dcgettext (0, "the delimiter must be a single character");
    eax = 0;
    error (1, 0, rax);
label_41:
    edx = 5;
    rax = dcgettext (0, "grouping cannot be combined with --to");
    eax = 0;
    error (1, 0, rax);
label_57:
    rax = quote (r12, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "format %s has too many %% directives");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_31:
    edx = 5;
    rax = dcgettext (0, "multiple field specifications");
    eax = 0;
    error (1, 0, rax);
label_54:
    rax = quote (r12, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid format %s, directive must be %%[0]['][-][N][.][N]f");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_52:
    rax = quote (r12, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "format %s ends in %%");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_49:
    rax = quote (r12, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid format %s (width overflow)");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
label_61:
    rax = quote (r12, rsi, rdx, rcx, r8);
    edx = 5;
    r12 = rax;
    rax = dcgettext (0, "invalid precision in format %s");
    rcx = r12;
    eax = 0;
    error (1, 0, rax);
}

/* /tmp/tmp2diozptm @ 0x51e0 */
 
int64_t dbg_usage (int64_t arg1) {
    infomap const[7] const infomap;
    int64_t var_30h;
    char * var_38h;
    int64_t var_40h;
    char * var_48h;
    int64_t var_50h;
    int64_t var_58h;
    int64_t var_60h;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_78h_2;
    int64_t var_80h;
    int64_t var_88h;
    char * var_90h;
    int64_t var_98h;
    int64_t var_78h;
    rdi = arg1;
    /* void usage(int status); */
    edx = 5;
    r12 = program_name;
    rax = *(fs:0x28);
    *((rsp + 0x78)) = rax;
    eax = 0;
    if (edi != 0) {
        rax = dcgettext (0, "Try '%s --help' for more information.\n");
        rdi = stderr;
        rcx = r12;
        esi = 1;
        rdx = rax;
        eax = 0;
        fprintf_chk ();
label_0:
        exit (ebp);
    }
    rax = dcgettext (0, "Usage: %s [OPTION]... [NUMBER]...\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "Reformat NUMBER(s), or the numbers from standard input if none are specified.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nMandatory arguments to long options are mandatory for short options too.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --debug          print warnings about invalid input\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -d, --delimiter=X    use X instead of whitespace for field delimiter\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --field=FIELDS   replace the numbers in these input fields (default=1);\n                         see FIELDS below\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --format=FORMAT  use printf style floating-point FORMAT;\n                         see FORMAT below for details\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --from=UNIT      auto-scale input numbers to UNITs; default is 'none';\n                         see UNIT below\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --from-unit=N    specify the input unit size (instead of the default 1)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --grouping       use locale-defined grouping of digits, e.g. 1,000,000\n                         (which means it has no effect in the C/POSIX locale)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --header[=N]     print (without converting) the first N header lines;\n                         N defaults to 1 if not specified\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --invalid=MODE   failure mode for invalid numbers: MODE can be:\n                         abort (default), fail, warn, ignore\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --padding=N      pad the output to N characters; positive N will\n                         right-align; negative N will left-align;\n                         padding is ignored if the output is wider than N;\n                         the default is to automatically pad if a whitespace\n                         is found\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --round=METHOD   use METHOD for rounding when scaling; METHOD can be:\n                         up, down, from-zero (default), towards-zero, nearest\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --suffix=SUFFIX  add SUFFIX to output numbers, and accept optional\n                         SUFFIX in input numbers\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --to=UNIT        auto-scale output numbers to UNITs; see UNIT below\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --to-unit=N      the output unit size (instead of the default 1)\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  -z, --zero-terminated    line delimiter is NUL, not newline\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --help        display this help and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "      --version     output version information and exit\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nUNIT options:\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  none       no auto-scaling is done; suffixes will trigger an error\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  auto       accept optional single/two letter suffix:\n               1K = 1000,\n               1Ki = 1024,\n               1M = 1000000,\n               1Mi = 1048576,\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  si         accept optional single letter suffix:\n               1K = 1000,\n               1M = 1000000,\n               ...\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  iec        accept optional single letter suffix:\n               1K = 1024,\n               1M = 1048576,\n               ...\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "  iec-i      accept optional two-letter suffix:\n               1Ki = 1024,\n               1Mi = 1048576,\n               ...\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nFIELDS supports cut(1) style field ranges:\n  N    N'th field, counted from 1\n  N-   from N'th field, to end of line\n  N-M  from N'th to M'th field (inclusive)\n  -M   from first to M'th field (inclusive)\n  -    all fields\nMultiple fields/ranges can be separated with commas\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = stdout;
    edx = 5;
    rax = dcgettext (0, "\nFORMAT must be suitable for printing one floating-point argument '%f'.\nOptional quote (%'f) will enable --grouping (if supported by current locale).\nOptional width value (%10f) will pad output. Optional zero (%010f) width\nwill zero pad the number. Optional negative values (%-10f) will left align.\nOptional precision (%.1f) will override the input determined precision.\n");
    rsi = r12;
    rdi = rax;
    fputs_unlocked ();
    r12 = program_name;
    edx = 5;
    rax = dcgettext (0, "\nExit status is 0 if all input numbers were successfully converted.\nBy default, %s will stop at the first conversion error with exit status 2.\nWith --invalid='fail' a warning is printed for each conversion error\nand the exit status is 2.  With --invalid='warn' each conversion error is\ndiagnosed, but the exit status is 0.  With --invalid='ignore' conversion\nerrors are not diagnosed and the exit status is 0.\n");
    rdx = r12;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    r12 = program_name;
    edx = 5;
    rax = dcgettext (0, "\nExamples:\n  $ %s --to=si 1000\n            -> \"1.0K\"\n  $ %s --to=iec 2048\n           -> \"2.0K\"\n  $ %s --to=iec-i 4096\n           -> \"4.0Ki\"\n  $ echo 1K | %s --from=si\n           -> \"1000\"\n  $ echo 1K | %s --from=iec\n           -> \"1024\"\n  $ df -B1 | %s --header --field 2-4 --to=si\n  $ ls -l  | %s --header --field 5 --to=iec\n  $ ls -lh | %s --header --field 5 --from=iec --padding=10\n  $ ls -lh | %s --header --field 5 --from=iec --format %%10f\n");
    rcx = r12;
    r9 = r12;
    r8 = r12;
    rsi = rax;
    rdx = r12;
    edi = 1;
    eax = 0;
    r12 = "numfmt";
    printf_chk ();
    rax = 0x0000b131;
    rcx = "sha256sum";
    *((rsp + 0x90)) = 0;
    *((rsp + 0x30)) = rax;
    rax = "test invocation";
    *((rsp + 0x38)) = rax;
    rax = 0x0000b1ab;
    *((rsp + 0x40)) = rax;
    rax = "Multi-call invocation";
    *((rsp + 0x48)) = rax;
    rax = "sha224sum";
    *((rsp + 0x60)) = rcx;
    rcx = "sha384sum";
    *((rsp + 0x50)) = rax;
    rax = "sha2 utilities";
    *((rsp + 0x70)) = rcx;
    rcx = "sha512sum";
    *((rsp + 0x58)) = rax;
    *((rsp + 0x68)) = rax;
    *((rsp + 0x78)) = rax;
    *((rsp + 0x80)) = rcx;
    *((rsp + 0x88)) = rax;
    *((rsp + 0x98)) = 0;
    rbx = rsp;
    do {
        rsi = *((rbx + 0x10));
        rbx += 0x10;
        if (rsi == 0) {
            goto label_2;
        }
        eax = strcmp (r12, rsi);
    } while (eax != 0);
label_2:
    r13 = *((rbx + 8));
    edx = 5;
    rsi = "\n%s online help: <%s>\n";
    edi = 0;
    if (r13 == 0) {
        goto label_3;
    }
    rax = dcgettext (rdi, rsi);
    r14 = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    rcx = r14;
    eax = 0;
    printf_chk ();
    rax = setlocale (5, 0);
    rdi = rax;
    if (rax != 0) {
        eax = strncmp (rdi, 0x0000b1b5, 3);
        if (eax != 0) {
            goto label_4;
        }
    }
label_1:
    edx = 5;
    rax = dcgettext (0, "Full documentation <%s%s>\n");
    rcx = r12;
    rdx = r14;
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    rax = 0x0000b0d6;
    r12 = 0x0000b14d;
    r12 = rax;
    while (1) {
        edx = 5;
        rax = dcgettext (0, "or available locally via: info '(coreutils) %s%s'\n");
        rcx = r12;
        rdx = r13;
        edi = 1;
        rsi = rax;
        eax = 0;
        printf_chk ();
        goto label_0;
label_3:
        rax = dcgettext (rdi, rsi);
        r14 = "https://www.gnu.org/software/coreutils/";
        edi = 1;
        rdx = "GNU coreutils";
        rsi = rax;
        rcx = r14;
        eax = 0;
        printf_chk ();
        rax = setlocale (5, 0);
        rdi = rax;
        if (rax != 0) {
            eax = strncmp (rdi, 0x0000b1b5, 3);
            if (eax != 0) {
                goto label_5;
            }
        }
        edx = 5;
        rax = dcgettext (0, "Full documentation <%s%s>\n");
        rcx = r12;
        rdx = r14;
        edi = 1;
        rsi = rax;
        eax = 0;
        r13 = "numfmt";
        printf_chk ();
        r12 = 0x0000b14d;
    }
label_5:
    r13 = "numfmt";
label_4:
    r15 = stdout;
    edx = 5;
    rax = dcgettext (0, "Report any translation bugs to <https://translationproject.org/team/>\n");
    rdi = rax;
    rsi = r15;
    fputs_unlocked ();
    goto label_1;
}

/* /tmp/tmp2diozptm @ 0x8170 */
 
uint64_t dbg_get_quoting_style (int64_t arg1) {
    rdi = arg1;
    /* quoting_style get_quoting_style(quoting_options const * o); */
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = *(rdi);
    return rax;
}

/* /tmp/tmp2diozptm @ 0x8860 */
 
int64_t dbg_quotearg_colon (int64_t arg1, int64_t arg7, int64_t arg8, int64_t arg9) {
    quoting_options options;
    int64_t var_ch;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    /* char * quotearg_colon(char const * arg); */
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000112f0]");
    rsi = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    edi = 0;
    rcx = rsp;
    *(rsp) = xmm0;
    edx = *((rsp + 0xc));
    __asm ("movdqa xmm2, xmmword [0x00011300]");
    *((rsp + 0x30)) = rax;
    eax = edx;
    *((rsp + 0x10)) = xmm1;
    eax = ~eax;
    *((rsp + 0x20)) = xmm2;
    eax &= 0x4000000;
    eax ^= edx;
    rdx = 0xffffffffffffffff;
    *((rsp + 0xc)) = eax;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x8690 */
 
int64_t quotearg_style_mem (uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    if (edi == 0xa) {
        void (*0x27f8)() ();
    }
    *(rsp) = edi;
    rcx = rsp;
    edi = 0;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x8570 */
 
int32_t quotearg_n_style_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    int64_t var_4h;
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    eax = esi;
    rsi = rdx;
    rdx = rcx;
    rcx = *(fs:0x28);
    *((rsp + 0x38)) = rcx;
    ecx = 0;
    if (eax == 0xa) {
        void (*0x27ee)() ();
    }
    rcx = rsp;
    *(rsp) = eax;
    *((rsp + 4)) = 0;
    *((rsp + 8)) = 0;
    *((rsp + 0x10)) = 0;
    *((rsp + 0x18)) = 0;
    *((rsp + 0x20)) = 0;
    *((rsp + 0x28)) = 0;
    *((rsp + 0x30)) = 0;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return eax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x8d00 */
 
void dbg_quote (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    /* char const * quote(char const * arg); */
    rsi = rdi;
    rcx = obj_quote_quoting_options;
    rdx = 0xffffffffffffffff;
    edi = 0;
    return quotearg_n_options ();
}

/* /tmp/tmp2diozptm @ 0x9fd0 */
 
int64_t dbg_xstrtoumax (int64_t arg1, int64_t arg2, uint32_t arg3, uintmax_t * arg4, int64_t arg5) {
    int64_t var_45h;
    char * t_ptr;
    uintmax_t * var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_1ch;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    /* strtol_error xstrtoumax(char const * s,char ** ptr,int strtol_base,uintmax_t * val,char const * valid_suffixes); */
    *((rsp + 8)) = rcx;
    rax = *(fs:0x28);
    *((rsp + 0x28)) = rax;
    eax = 0;
    if (edx > 0x24) {
        goto label_11;
    }
    r15 = rsi;
    rax = rsp + 0x20;
    if (rsi == 0) {
        r15 = rax;
    }
    r14d = edx;
    r13 = r8;
    errno_location ();
    *(rax) = 0;
    r12 = rax;
    ebx = *(rbp);
    rax = ctype_b_loc ();
    rcx = *(rax);
    rax = rbp;
    while ((*((rcx + rdx*2 + 1)) & 0x20) != 0) {
        ebx = *((rax + 1));
        rax++;
        edx = (int32_t) bl;
    }
    if (bl == 0x2d) {
        goto label_1;
    }
    rax = strtoumax (rbp, r15, r14d);
    r8 = *(r15);
    rbx = rax;
    if (r8 == rbp) {
        goto label_12;
    }
    eax = *(r12);
    if (eax != 0) {
        goto label_13;
    }
    r12d = 0;
    do {
        if (r13 != 0) {
            ebp = *(r8);
            if (bpl != 0) {
                goto label_14;
            }
        }
label_2:
        rax = *((rsp + 8));
        *(rax) = rbx;
label_0:
        rax = *((rsp + 0x28));
        rax -= *(fs:0x28);
        if (rax != 0) {
            goto label_15;
        }
        eax = r12d;
        return rax;
label_13:
        r12d = 1;
    } while (eax == 0x22);
    do {
label_1:
        r12d = 4;
        goto label_0;
label_12:
        *((rsp + 0x10)) = r8;
    } while (r13 == 0);
    ebp = *(rbp);
    if (bpl == 0) {
        goto label_1;
    }
    esi = (int32_t) bpl;
    r12d = 0;
    ebx = 1;
    rax = strchr (r13, rsi);
    r8 = *((rsp + 0x10));
    if (rax == 0) {
        goto label_1;
    }
    do {
        eax = rbp - 0x45;
        r9d = 1;
        ecx = section..dynsym;
        if (al <= 0x2f) {
            rdx = 0x814400308945;
            if (((rdx >> rax) & 1) < 0) {
                goto label_16;
            }
        }
label_3:
        ebp -= 0x42;
        if (bpl > 0x35) {
            goto label_17;
        }
        rdx = 0x0000d998;
        ebp = (int32_t) bpl;
        rax = *((rdx + rbp*4));
        rax += rdx;
        /* switch table (54 cases) at 0xd998 */
        void (*rax)() ();
label_14:
        esi = (int32_t) bpl;
        *((rsp + 0x10)) = r8;
        rax = strchr (r13, rsi);
        r8 = *((rsp + 0x10));
    } while (rax != 0);
label_17:
    rax = *((rsp + 8));
    r12d |= 2;
    *(rax) = rbx;
    goto label_0;
    rax = rbx;
    rdx:rax = rax * rcx;
    rbx = rax;
    if (r12d overflow 0) {
        goto label_18;
    }
label_4:
    r9 = (int64_t) r9d;
    edx = r12d;
    rax = r8 + r9;
    edx |= 2;
    *(r15) = rax;
    if (*(rax) != 0) {
        r12d = edx;
    }
    goto label_2;
label_16:
    *((rsp + 0x1c)) = r9d;
    *((rsp + 0x18)) = ecx;
    *((rsp + 0x10)) = r8;
    rax = strchr (r13, 0x30);
    r8 = *((rsp + 0x10));
    ecx = section..dynsym;
    r9d = 1;
    if (rax == 0) {
        goto label_3;
    }
    eax = *((r8 + 1));
    if (al == 0x44) {
        goto label_19;
    }
    if (al != 0x69) {
        r9d = 0;
        r9b = (al == 0x42) ? 1 : 0;
        r9d++;
        eax = 0x3e8;
        if (al == 0x42) {
            rcx = rax;
        }
        goto label_3;
    }
    r9d = 0;
    r9b = (*((r8 + 2)) == 0x42) ? 1 : 0;
    r9d = r9 + r9 + 1;
    goto label_3;
    rax = rbx;
    rdx:rax = rax * rcx;
    if (*((r8 + 2)) overflow 0x42) {
        goto label_18;
    }
    do {
        rdx:rax = rax * rcx;
        rbx = 0xffffffffffffffff;
        __asm ("seto dl");
        edx = (int32_t) dl;
        edx = 1;
        if (rdx != 0) {
            r12d = edx;
        }
        if (rdx == 0) {
            rbx = rax;
        }
        goto label_4;
        rax = rbx;
        rdx:rax = rax * rcx;
        if (rdx overflow 0) {
            goto label_18;
        }
        rdx:rax = rax * rcx;
    } while (rdx !overflow 0);
label_18:
    r12d = 1;
    rbx |= 0xffffffffffffffff;
    goto label_4;
    esi = 4;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_20;
        }
label_6:
        esi--;
    } while (esi != 0);
label_5:
    r12d |= edi;
    goto label_4;
    rax = rbx + rbx;
    edx = 1;
    rbx >>= 0x3f;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 9;
    rbx >>= 0x37;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    rax = rbx;
    edx = 1;
    rax <<= 0xa;
    rbx >>= 0x36;
    rbx = 0xffffffffffffffff;
    if (rbx != 0) {
        r12d = edx;
    }
    if (rbx == 0) {
        rbx = rax;
    }
    goto label_4;
    esi = 6;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (rbx overflow 0) {
            goto label_21;
        }
label_9:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 5;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_22;
        }
label_7:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 7;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_23;
        }
label_8:
        esi--;
    } while (esi != 0);
    goto label_5;
    esi = 8;
    edi = 0;
    do {
        rax = rbx;
        rdx:rax = rax * rcx;
        rbx = rax;
        if (esi overflow 0) {
            goto label_24;
        }
label_10:
        esi--;
    } while (esi != 0);
    goto label_5;
label_19:
    r9d = 2;
    ecx = 0x3e8;
    goto label_3;
label_20:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_6;
label_11:
    assert_fail ("0 <= strtol_base && strtol_base <= 36", "lib/xstrtol.c", 0x55, "xstrtoumax");
label_15:
    stack_chk_fail ();
label_22:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_7;
label_23:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_8;
label_21:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_9;
label_24:
    edi = 1;
    rbx |= 0xffffffffffffffff;
    goto label_10;
}

/* /tmp/tmp2diozptm @ 0x8210 */
 
uint64_t set_custom_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    if (rdi == 0) {
        rdi = rax;
    }
    *(rdi) = 0xa;
    if (rsi == 0) {
        void (*0x27e3)() ();
    }
    if (rdx == 0) {
        void (*0x27e3)() ();
    }
    *((rdi + 0x28)) = rsi;
    *((rdi + 0x30)) = rdx;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x8720 */
 
int64_t quotearg_char_mem (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg7, int64_t arg8, int64_t arg9) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_38h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    xmm0 = arg7;
    xmm1 = arg8;
    xmm2 = arg9;
    __asm ("movdqa xmm0, xmmword [obj.default_quoting_options]");
    __asm ("movdqa xmm1, xmmword [0x000112f0]");
    ecx = edx;
    rax = *(fs:0x28);
    *((rsp + 0x38)) = rax;
    eax = 0;
    rax = Scrt1.o;
    r9 = rsi;
    ecx &= 0x1f;
    __asm ("movdqa xmm2, xmmword [0x00011300]");
    *(rsp) = xmm0;
    r10 = rsp;
    *((rsp + 0x30)) = rax;
    eax = edx;
    al >>= 5;
    *((rsp + 0x10)) = xmm1;
    eax = (int32_t) al;
    *((rsp + 0x20)) = xmm2;
    rdx = rsp + rax*4 + 8;
    esi = *(rdx);
    eax = *(rdx);
    eax >>= cl;
    eax = ~eax;
    eax &= 1;
    eax <<= cl;
    rcx = r10;
    eax ^= esi;
    rsi = rdi;
    edi = 0;
    *(rdx) = eax;
    rdx = r9;
    quotearg_n_options ();
    rdx = *((rsp + 0x38));
    rdx -= *(fs:0x28);
    if (rdx == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x97f0 */
 
uint64_t dbg_xzalloc (size_t nmeb) {
    rdi = nmeb;
    /* void * xzalloc(size_t s); */
    rax = calloc (rdi, 1);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x9870 */
 
uint64_t xicalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x8470 */
 
void quotearg_n (int64_t arg_4h, int64_t arg_8h, int64_t arg_28h, int64_t arg_30h, int64_t arg1, int64_t arg2) {
    int64_t var_ch;
    int64_t var_30h;
    int64_t var_20h;
    int64_t var_20h_2;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rcx = obj_default_quoting_options;
    rdx = 0xffffffffffffffff;
    return quotearg_n_options ();
}

/* /tmp/tmp2diozptm @ 0x9510 */
 
uint64_t xireallocarray (int64_t arg2, int64_t arg3) {
    rsi = arg2;
    rdx = arg3;
    if (rsi == 0) {
        goto label_0;
    }
    while (1) {
        rax = reallocarray ();
        if (rax == 0) {
            goto label_1;
        }
        return rax;
label_0:
        esi = 1;
        edx = 1;
    }
label_1:
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0xa550 */
 
int64_t dbg_rpl_fseeko (int64_t arg_90h, uint32_t arg1, int64_t arg2, int64_t arg3) {
    int64_t var_ch;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    /* int rpl_fseeko(FILE * fp,off_t offset,int whence); */
    rax = *((rdi + 8));
    while (*((rdi + 0x28)) != rax) {
label_0:
        rdi = rbp;
        void (*0x26e0)() ();
        rax = *((rdi + 0x20));
    }
    if (*((rdi + 0x48)) != 0) {
        goto label_0;
    }
    *((rsp + 0xc)) = edx;
    *(rsp) = rsi;
    eax = fileno (rdi);
    edx = *((rsp + 0xc));
    rsi = *(rsp);
    edi = eax;
    rax = lseek ();
    if (rax == -1) {
        goto label_1;
    }
    *(rbp) &= 0xffffffef;
    *((rbp + 0x90)) = rax;
    eax = 0;
    do {
        return rax;
label_1:
        eax |= 0xffffffff;
    } while (1);
}

/* /tmp/tmp2diozptm @ 0x81b0 */
 
uint64_t set_char_quoting (int64_t arg1, int64_t arg2, int64_t arg3) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rax = obj_default_quoting_options;
    ecx = esi;
    if (rdi == 0) {
        rdi = rax;
    }
    eax = esi;
    ecx &= 0x1f;
    al >>= 5;
    eax = (int32_t) al;
    rsi = rdi + rax*4 + 8;
    edi = *(rsi);
    eax = *(rsi);
    eax >>= cl;
    edx ^= eax;
    eax &= 1;
    edx &= 1;
    edx <<= cl;
    edx ^= edi;
    *(rsi) = edx;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x96f0 */
 
int64_t dbg_xpalloc (int64_t arg1, size_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* void * xpalloc(void * pa,idx_t * pn,idx_t n_incr_min,ptrdiff_t n_max,idx_t s); */
    r13 = rdi;
    rdi = rdx;
    r12 = rsi;
    rsi = rcx;
    rcx = *(r12);
    rbx = *(r12);
    rbx >>= 1;
    rbx += rcx;
    if (rbx overflow 0) {
        goto label_4;
    }
label_3:
    rax = rsi;
    if (rbx <= rsi) {
        rax = rbx;
    }
    __asm ("cmovns rbx, rax");
    rbp *= r8;
    if (rsi overflow 0) {
        goto label_5;
    }
    if (rbp <= 0x7f) {
        goto label_6;
    }
    if (r13 == 0) {
        goto label_7;
    }
    do {
label_0:
        rax = rbx;
        rax -= rcx;
        if (rax < rdi) {
            rcx += rdi;
            rbx = rcx;
            if (rcx overflow 0) {
                goto label_8;
            }
            if (rcx > rsi) {
                if (rsi >= 0) {
                    goto label_8;
                }
            }
            rcx *= r8;
            if (rsi overflow 0) {
                goto label_8;
            }
        }
        rax = realloc (r13, rbp);
        if (rax == 0) {
            goto label_9;
        }
label_1:
        *(r12) = rbx;
        return rax;
label_6:
label_2:
        rax = rbp;
        __asm ("cqo");
        rax = rdx:rax / r8;
        rdx = rdx:rax % r8;
        rbx = rax;
        rbp -= rdx;
    } while (r13 != 0);
label_7:
    *(r12) = 0;
    goto label_0;
label_9:
    if (r13 == 0) {
        goto label_8;
    }
    if (rbp == 0) {
        goto label_1;
    }
label_8:
    xalloc_die ();
label_5:
    goto label_2;
label_4:
    rbx = 0x7fffffffffffffff;
    goto label_3;
}

/* /tmp/tmp2diozptm @ 0x9330 */
 
uint64_t dbg_emit_bug_reporting_address (void) {
    /* void emit_bug_reporting_address(); */
    rsi = stdout;
    edi = 0xa;
    fputc_unlocked ();
    edx = 5;
    rax = dcgettext (0, "Report bugs to: %s\n");
    rdx = "bug-coreutils@gnu.org";
    edi = 1;
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "%s home page: <%s>\n");
    rcx = "https://www.gnu.org/software/coreutils/";
    edi = 1;
    rdx = "GNU coreutils";
    rsi = rax;
    eax = 0;
    printf_chk ();
    edx = 5;
    rax = dcgettext (0, "General help using GNU software: <%s>\n");
    rdx = "https://www.gnu.org/gethelp/";
    edi = 1;
    rsi = rax;
    eax = 0;
    return printf_chk ();
}

/* /tmp/tmp2diozptm @ 0x25f0 */
 
void fputc_unlocked (void) {
    __asm ("bnd jmp qword [reloc.fputc_unlocked]");
}

/* /tmp/tmp2diozptm @ 0x6400 */
 
void dbg_close_stdout_set_file_name (char const * file) {
    rdi = file;
    /* void close_stdout_set_file_name(char const * file); */
    *(obj.file_name) = rdi;
}

/* /tmp/tmp2diozptm @ 0xa400 */
 
uint64_t dbg_close_stream (int64_t arg1) {
    rdi = arg1;
    /* int close_stream(FILE * stream); */
    rax = fpending ();
    ebx = *(rbp);
    r12 = rax;
    ebx &= 0x20;
    eax = rpl_fclose (rbp);
    if (ebx != 0) {
        goto label_1;
    }
    if (eax == 0) {
        goto label_0;
    }
    if (r12 != 0) {
        goto label_2;
    }
    rax = errno_location ();
    al = (*(rax) != 9) ? 1 : 0;
    eax = (int32_t) al;
    eax = -eax;
    do {
label_0:
        return rax;
label_1:
        if (eax != 0) {
            goto label_2;
        }
        errno_location ();
        *(rax) = 0;
        eax = 0xffffffff;
    } while (1);
label_2:
    eax = 0xffffffff;
    goto label_0;
}

/* /tmp/tmp2diozptm @ 0x2460 */
 
void fpending (void) {
    __asm ("bnd jmp qword [reloc.__fpending]");
}

/* /tmp/tmp2diozptm @ 0x8d20 */
 
void dbg_version_etc_arn (int64_t arg_8h_2, int64_t arg_8h, int64_t arg_8h_4, int64_t arg_8h_3, int64_t arg_18h_2, int64_t arg_18h, int64_t arg_8h_5, int64_t arg_10h, int64_t arg_18h_3, int64_t arg_20h, int64_t arg_28h, int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5, int64_t arg6) {
    int64_t var_8h;
    int64_t var_10h;
    int64_t var_18h;
    int64_t var_20h;
    int64_t var_28h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r9 = arg6;
    /* void version_etc_arn(FILE * stream,char const * command_name,char const * package,char const * version,char const * const * authors,size_t n_authors); */
    r12 = r9;
    rbx = r8;
    if (rsi == 0) {
        goto label_2;
    }
    r9 = rcx;
    r8 = rdx;
    rcx = rsi;
    eax = 0;
    rdx = 0x0000d448;
    esi = 1;
    fprintf_chk ();
    do {
        edx = 5;
        rax = dcgettext (0, 0x0000d45b);
        r8d = 0x7e6;
        esi = 1;
        rdi = rbp;
        rcx = rax;
        rdx = "Copyright %s %d Free Software Foundation, Inc.";
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        edx = 5;
        rax = dcgettext (0, "License GPLv3+: GNU GPL version 3 or later <%s>.\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n");
        esi = 1;
        rdi = rbp;
        rcx = "https://gnu.org/licenses/gpl.html";
        rdx = rax;
        eax = 0;
        fprintf_chk ();
        rsi = rbp;
        edi = 0xa;
        fputc_unlocked ();
        if (r12 > 9) {
            goto label_3;
        }
        rdx = 0x0000d748;
        rax = *((rdx + r12*4));
        rax += rdx;
        /* switch table (10 cases) at 0xd748 */
        void (*rax)() ();
        r10 = *((rbx + 0x38));
        r9 = *((rbx + 0x30));
        edx = 5;
        r8 = *((rbx + 0x28));
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        *((rsp + 0x20)) = r10;
        r13 = *((rbx + 8));
        r12 = *(rbx);
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\nand %s.\n");
        rdx = rax;
label_0:
        r10 = *((rsp + 0x28));
        esi = 1;
        rdi = rbp;
        eax = 0;
        r9 = *((rsp + 0x28));
        r8 = *((rsp + 0x28));
        r9 = r14;
        rcx = *((rsp + 0x28));
        r8 = r13;
        rcx = r12;
        eax = fprintf_chk ();
        return rax;
label_2:
        r8 = rcx;
        esi = 1;
        rcx = rdx;
        eax = 0;
        rdx = "%s %s\n";
        fprintf_chk ();
    } while (1);
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
label_1:
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, and %s.\n");
    r11 = *((rsp + 0x28));
    rdx = rax;
    goto label_0;
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s.\n");
    rdi = rbp;
    esi = 1;
    rdx = rax;
    rcx = r12;
    eax = 0;
    void (*0x2740)() ();
    r13 = *((rbx + 8));
    r12 = *(rbx);
    edx = 5;
    rax = dcgettext (0, "Written by %s and %s.\n");
    r8 = r13;
    rcx = r12;
    rdx = rax;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2740)() ();
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    edx = 5;
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, and %s.\n");
    r9 = r14;
    r8 = r13;
    rdx = rax;
    rcx = r12;
    rdi = rbp;
    esi = 1;
    eax = 0;
    void (*0x2740)() ();
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    rax = dcgettext (0, "Written by %s, %s, %s,\nand %s.\n");
    rdx = rax;
    do {
        r9 = r14;
        r8 = r13;
        rcx = r12;
        rdi = rbp;
        esi = 1;
        eax = 0;
        fprintf_chk ();
        return;
        rcx = *((rbx + 0x20));
        edx = 5;
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 8)) = rcx;
        r12 = *(rbx);
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, and %s.\n");
        rcx = *((rsp + 8));
        rdx = rax;
    } while (1);
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    edx = 5;
    r15 = *((rbx + 0x18));
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, and %s.\n");
    rdx = rax;
    do {
        r8 = *((rsp + 0x18));
        r9 = r14;
        rdi = rbp;
        esi = 1;
        eax = 0;
        rcx = *((rsp + 0x18));
        r8 = r13;
        rcx = r12;
        fprintf_chk ();
        return;
        r9 = *((rbx + 0x30));
        r8 = *((rbx + 0x28));
        edx = 5;
        rcx = *((rbx + 0x20));
        r15 = *((rbx + 0x18));
        r14 = *((rbx + 0x10));
        r13 = *((rbx + 8));
        *((rsp + 0x18)) = r9;
        *((rsp + 0x10)) = r8;
        r12 = *(rbx);
        *((rsp + 8)) = rcx;
        rax = dcgettext (0, "Written by %s, %s, %s,\n%s, %s, %s, and %s.\n");
        r9 = *((rsp + 0x18));
        rdx = rax;
    } while (1);
label_3:
    r11 = *((rbx + 0x40));
    r10 = *((rbx + 0x38));
    edx = 5;
    rsi = "Written by %s, %s, %s,\n%s, %s, %s, %s,\n%s, %s, and others.\n";
    r9 = *((rbx + 0x30));
    r8 = *((rbx + 0x28));
    rcx = *((rbx + 0x20));
    r15 = *((rbx + 0x18));
    *((rsp + 0x28)) = r11;
    r14 = *((rbx + 0x10));
    r13 = *((rbx + 8));
    *((rsp + 0x20)) = r10;
    *((rsp + 0x18)) = r9;
    r12 = *(rbx);
    *((rsp + 0x10)) = r8;
    *((rsp + 8)) = rcx;
    goto label_1;
}

/* /tmp/tmp2diozptm @ 0x91b0 */
 
int64_t version_etc_va (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4, int64_t arg5) {
    int64_t var_58h;
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    r8 = arg5;
    r11 = rcx;
    r10 = rdx;
    rcx = r8;
    rax = *(fs:0x28);
    *((rsp + 0x58)) = rax;
    eax = 0;
    r9d = 0;
    r8 = rsp;
    while (eax <= 0x2f) {
        edx = eax;
        eax += 8;
        rdx += *((rcx + 0x10));
        *(rcx) = eax;
        rax = *(rdx);
        *((r8 + r9*8)) = rax;
        if (rax == 0) {
            goto label_1;
        }
label_0:
        r9++;
        if (r9 == 0xa) {
            goto label_1;
        }
        eax = *(rcx);
    }
    rdx = *((rcx + 8));
    rax = rdx + 8;
    *((rcx + 8)) = rax;
    rax = *(rdx);
    *((r8 + r9*8)) = rax;
    if (rax != 0) {
        goto label_0;
    }
label_1:
    version_etc_arn (rdi, rsi, r10, r11, r8, r9);
    rax = *((rsp + 0x58));
    rax -= *(fs:0x28);
    if (rax == 0) {
        return rax;
    }
    return stack_chk_fail ();
}

/* /tmp/tmp2diozptm @ 0x2000 */
 
int64_t init (void) {
    /* [12] -r-x section size 27 named .init */
    rax = *(reloc.__gmon_start__);
    if (rax != 0) {
        void (*rax)() ();
    }
    return rax;
}

/* /tmp/tmp2diozptm @ 0x9850 */
 
uint64_t xcalloc (size_t nmeb, size_t size) {
    rdi = nmeb;
    rsi = size;
    rax = calloc (rdi, rsi);
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0xa7d0 */
 
void dbg_setlocale_null (void) {
    /* char const * setlocale_null(int category); */
    esi = 0;
    return setlocale ();
}

/* /tmp/tmp2diozptm @ 0x9550 */
 
uint64_t dbg_xnmalloc (int64_t arg1, int64_t arg2) {
    rdi = arg1;
    rsi = arg2;
    /* void * xnmalloc(size_t n,size_t s); */
    rdx = rsi;
    rsi = rdi;
    edi = 0;
    rax = reallocarray ();
    if (rax != 0) {
        return rax;
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x9910 */
 
uint64_t dbg_ximemdup0 (int64_t arg1, size_t size) {
    rdi = arg1;
    rsi = size;
    /* char * ximemdup0( const * p,idx_t s); */
    r12 = rsi;
    rax = malloc (rsi + 1);
    if (rax != 0) {
        *((rax + r12)) = 0;
        rdx = r12;
        rsi = rbp;
        rdi = rax;
        void (*0x2610)() ();
    }
    return xalloc_die ();
}

/* /tmp/tmp2diozptm @ 0x63a0 */
 
uint64_t dbg_argmatch_to_argument (int64_t arg1, int64_t arg2, int64_t arg3, int64_t arg4) {
    rdi = arg1;
    rsi = arg2;
    rdx = arg3;
    rcx = arg4;
    /* char const * argmatch_to_argument( const * value,char const * const * arglist, const * vallist,size_t valsize); */
    r14 = *(rsi);
    if (r14 == 0) {
        goto label_0;
    }
    r12 = rdi;
    r13 = rcx;
    rbx = rsi + 8;
    while (eax != 0) {
        r14 = *(rbx);
        rbp += r13;
        rbx += 8;
        if (r14 == 0) {
            goto label_0;
        }
        eax = memcmp (r12, rbp, r13);
    }
label_0:
    rax = r14;
    return rax;
}

/* /tmp/tmp2diozptm @ 0x23f0 */
 
void snprintf_chk (void) {
    /* [15] -r-x section size 944 named .plt.sec */
    __asm ("bnd jmp qword [loc._end]");
}

/* /tmp/tmp2diozptm @ 0x2400 */
 
void free (void) {
    __asm ("bnd jmp qword [reloc.free]");
}

/* /tmp/tmp2diozptm @ 0x0 */
 
int64_t libc_start_main (int32_t argc, func init, func main, char ** ubp_av) {
    rsi = argc;
    rcx = init;
    rdi = main;
    rdx = ubp_av;
    /* [39] ---- section size 407 named .shstrtab */
    al += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *((rcx + rsi)) ^= esi;
    *(rsi) += bh;
    *(rcx) += al;
    *(rax) += al;
    *((rax + 0x35)) += ah;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    __asm ("retf");
    return _hlt ();
    eax += *(rax);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dil;
    *(0x2800403d) += cl;
    *(rdi) += ah;
    *(rsi) += ch;
    cl -= *(cs:);
    if (cl < 0) {
    }
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    fp_stack[0] += *(rdx);
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) |= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    eax += *(rax);
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) -= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    al += 0;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rbx) += bl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
    eax += 0;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) &= al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) = al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) = al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rcx) += al;
    *(rax) += al;
    *((rax + rax)) += al;
    *(rax) += al;
    *(rax) += dh;
    *(rax) += al;
    *(rax) += dh;
    *(rax) += al;
    *(rax) += dh;
    *(rax) += al;
    al = 0x41;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    al = 0x41;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += dl;
    *(rax) += al;
    *(rax) += al;
    *(rax) += al;
    *(rax) += eax;
    *(rax) += al;
}

/* /tmp/tmp2diozptm @ 0x2440 */
 
void exit (void) {
    __asm ("bnd jmp qword [reloc._exit]");
}

/* /tmp/tmp2diozptm @ 0x2450 */
 
void strcpy (void) {
    __asm ("bnd jmp qword [reloc.strcpy]");
}

/* /tmp/tmp2diozptm @ 0x2470 */
 
void qsort (void) {
    __asm ("bnd jmp qword [reloc.qsort]");
}

/* /tmp/tmp2diozptm @ 0x2490 */
 
void wcswidth (void) {
    __asm ("bnd jmp qword [reloc.wcswidth]");
}

/* /tmp/tmp2diozptm @ 0x24a0 */
 
void mbstowcs (void) {
    __asm ("bnd jmp qword [reloc.mbstowcs]");
}

/* /tmp/tmp2diozptm @ 0x24b0 */
 
void textdomain (void) {
    __asm ("bnd jmp qword [reloc.textdomain]");
}

/* /tmp/tmp2diozptm @ 0x24c0 */
 
void fclose (void) {
    __asm ("bnd jmp qword [reloc.fclose]");
}

/* /tmp/tmp2diozptm @ 0x24d0 */
 
void bindtextdomain (void) {
    __asm ("bnd jmp qword [reloc.bindtextdomain]");
}

/* /tmp/tmp2diozptm @ 0x24f0 */
 
void ctype_get_mb_cur_max (void) {
    __asm ("bnd jmp qword [reloc.__ctype_get_mb_cur_max]");
}

/* /tmp/tmp2diozptm @ 0x2520 */
 
void getopt_long (void) {
    __asm ("bnd jmp qword [reloc.getopt_long]");
}

/* /tmp/tmp2diozptm @ 0x2530 */
 
void mbrtowc (void) {
    __asm ("bnd jmp qword [reloc.mbrtowc]");
}

/* /tmp/tmp2diozptm @ 0x2540 */
 
void strchr (void) {
    __asm ("bnd jmp qword [reloc.strchr]");
}

/* /tmp/tmp2diozptm @ 0x2550 */
 
void overflow (void) {
    __asm ("bnd jmp qword [reloc.__overflow]");
}

/* /tmp/tmp2diozptm @ 0x2570 */
 
void lseek (void) {
    __asm ("bnd jmp qword [reloc.lseek]");
}

/* /tmp/tmp2diozptm @ 0x2580 */
 
void assert_fail (void) {
    __asm ("bnd jmp qword [reloc.__assert_fail]");
}

/* /tmp/tmp2diozptm @ 0x2590 */
 
void memset (void) {
    __asm ("bnd jmp qword [reloc.memset]");
}

/* /tmp/tmp2diozptm @ 0x25a0 */
 
void strspn (void) {
    __asm ("bnd jmp qword [reloc.strspn]");
}

/* /tmp/tmp2diozptm @ 0x25b0 */
 
void memcmp (void) {
    __asm ("bnd jmp qword [reloc.memcmp]");
}

/* /tmp/tmp2diozptm @ 0x2600 */
 
void strtol (void) {
    __asm ("bnd jmp qword [reloc.strtol]");
}

/* /tmp/tmp2diozptm @ 0x2610 */
 
void memcpy (void) {
    __asm ("bnd jmp qword [reloc.memcpy]");
}

/* /tmp/tmp2diozptm @ 0x2620 */
 
void fileno (void) {
    __asm ("bnd jmp qword [reloc.fileno]");
}

/* /tmp/tmp2diozptm @ 0x2630 */
 
void wcwidth (void) {
    __asm ("bnd jmp qword [reloc.wcwidth]");
}

/* /tmp/tmp2diozptm @ 0x2650 */
 
void fflush (void) {
    __asm ("bnd jmp qword [reloc.fflush]");
}

/* /tmp/tmp2diozptm @ 0x2670 */
 
void freading (void) {
    __asm ("bnd jmp qword [reloc.__freading]");
}

/* /tmp/tmp2diozptm @ 0x26b0 */
 
void mempcpy (void) {
    __asm ("bnd jmp qword [reloc.mempcpy]");
}

/* /tmp/tmp2diozptm @ 0x26c0 */
 
void memmove (void) {
    __asm ("bnd jmp qword [reloc.memmove]");
}

/* /tmp/tmp2diozptm @ 0x26e0 */
 
void fseeko (void) {
    __asm ("bnd jmp qword [reloc.fseeko]");
}

/* /tmp/tmp2diozptm @ 0x26f0 */
 
void strtoumax (void) {
    __asm ("bnd jmp qword [reloc.strtoumax]");
}

/* /tmp/tmp2diozptm @ 0x2700 */
 
void cxa_atexit (void) {
    __asm ("bnd jmp qword [reloc.__cxa_atexit]");
}

/* /tmp/tmp2diozptm @ 0x2710 */
 
void wcstombs (void) {
    __asm ("bnd jmp qword [reloc.wcstombs]");
}

/* /tmp/tmp2diozptm @ 0x2750 */
 
void strncat_chk (void) {
    __asm ("bnd jmp qword [reloc.__strncat_chk]");
}

/* /tmp/tmp2diozptm @ 0x2760 */
 
void mbsinit (void) {
    __asm ("bnd jmp qword [reloc.mbsinit]");
}

/* /tmp/tmp2diozptm @ 0x2770 */
 
void iswprint (void) {
    __asm ("bnd jmp qword [reloc.iswprint]");
}

/* /tmp/tmp2diozptm @ 0x2780 */
 
void getdelim (void) {
    __asm ("bnd jmp qword [reloc.getdelim]");
}

/* /tmp/tmp2diozptm @ 0x2790 */
 
void ctype_b_loc (void) {
    __asm ("bnd jmp qword [reloc.__ctype_b_loc]");
}

/* /tmp/tmp2diozptm @ 0x2030 */
 
void fcn_00002030 (void) {
    __asm ("bnd jmp section..plt");
    /* [13] -r-x section size 960 named .plt */
    __asm ("bnd jmp qword [0x00010df8]");
}

/* /tmp/tmp2diozptm @ 0x2040 */
 
void fcn_00002040 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2050 */
 
void fcn_00002050 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2060 */
 
void fcn_00002060 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2070 */
 
void fcn_00002070 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2080 */
 
void fcn_00002080 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2090 */
 
void fcn_00002090 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x20a0 */
 
void fcn_000020a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x20b0 */
 
void fcn_000020b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x20c0 */
 
void fcn_000020c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x20d0 */
 
void fcn_000020d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x20e0 */
 
void fcn_000020e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x20f0 */
 
void fcn_000020f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2100 */
 
void fcn_00002100 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2110 */
 
void fcn_00002110 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2120 */
 
void fcn_00002120 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2130 */
 
void fcn_00002130 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2140 */
 
void fcn_00002140 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2150 */
 
void fcn_00002150 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2160 */
 
void fcn_00002160 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2170 */
 
void fcn_00002170 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2180 */
 
void fcn_00002180 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2190 */
 
void fcn_00002190 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x21a0 */
 
void fcn_000021a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x21b0 */
 
void fcn_000021b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x21c0 */
 
void fcn_000021c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x21d0 */
 
void fcn_000021d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x21e0 */
 
void fcn_000021e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x21f0 */
 
void fcn_000021f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2200 */
 
void fcn_00002200 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2210 */
 
void fcn_00002210 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2220 */
 
void fcn_00002220 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2230 */
 
void fcn_00002230 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2240 */
 
void fcn_00002240 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2250 */
 
void fcn_00002250 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2260 */
 
void fcn_00002260 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2270 */
 
void fcn_00002270 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2280 */
 
void fcn_00002280 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2290 */
 
void fcn_00002290 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x22a0 */
 
void fcn_000022a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x22b0 */
 
void fcn_000022b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x22c0 */
 
void fcn_000022c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x22d0 */
 
void fcn_000022d0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x22e0 */
 
void fcn_000022e0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x22f0 */
 
void fcn_000022f0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2300 */
 
void fcn_00002300 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2310 */
 
void fcn_00002310 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2320 */
 
void fcn_00002320 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2330 */
 
void fcn_00002330 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2340 */
 
void fcn_00002340 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2350 */
 
void fcn_00002350 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2360 */
 
void fcn_00002360 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2370 */
 
void fcn_00002370 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2380 */
 
void fcn_00002380 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x2390 */
 
void fcn_00002390 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x23a0 */
 
void fcn_000023a0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x23b0 */
 
void fcn_000023b0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x23c0 */
 
void fcn_000023c0 (void) {
    return __asm ("bnd jmp section..plt");
}

/* /tmp/tmp2diozptm @ 0x23d0 */
 
void fcn_000023d0 (void) {
    return __asm ("bnd jmp section..plt");
}
