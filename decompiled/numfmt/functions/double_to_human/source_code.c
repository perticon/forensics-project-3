double_to_human (long double val, int precision,
                 char *buf, size_t buf_size,
                 enum scale_type scale, int group, enum round_type round)
{
  int num_size;
  char fmt[64];
  verify (sizeof (fmt) > (INT_BUFSIZE_BOUND (zero_padding_width)
                          + INT_BUFSIZE_BOUND (precision)
                          + 10 /* for %.Lf  etc.  */));

  char *pfmt = fmt;
  *pfmt++ = '%';

  if (group)
    *pfmt++ = '\'';

  if (zero_padding_width)
    pfmt += snprintf (pfmt, sizeof (fmt) - 2, "0%ld", zero_padding_width);

  devmsg ("double_to_human:\n");

  if (scale == scale_none)
    {
      val *= powerld (10, precision);
      val = simple_round (val, round);
      val /= powerld (10, precision);

      devmsg ((group) ?
              "  no scaling, returning (grouped) value: %'.*Lf\n" :
              "  no scaling, returning value: %.*Lf\n", precision, val);

      stpcpy (pfmt, ".*Lf");

      num_size = snprintf (buf, buf_size, fmt, precision, val);
      if (num_size < 0 || num_size >= (int) buf_size)
        die (EXIT_FAILURE, 0,
             _("failed to prepare value '%Lf' for printing"), val);
      return;
    }

  /* Scaling requested by user. */
  double scale_base = default_scale_base (scale);

  /* Normalize val to scale. */
  unsigned int power = 0;
  val = expld (val, scale_base, &power);
  devmsg ("  scaled value to %Lf * %0.f ^ %u\n", val, scale_base, power);

  /* Perform rounding. */
  unsigned int power_adjust = 0;
  if (user_precision != -1)
    power_adjust = MIN (power * 3, user_precision);
  else if (absld (val) < 10)
    {
      /* for values less than 10, we allow one decimal-point digit,
         so adjust before rounding. */
      power_adjust = 1;
    }

  val *= powerld (10, power_adjust);
  val = simple_round (val, round);
  val /= powerld (10, power_adjust);

  /* two special cases after rounding:
     1. a "999.99" can turn into 1000 - so scale down
     2. a "9.99" can turn into 10 - so don't display decimal-point.  */
  if (absld (val) >= scale_base)
    {
      val /= scale_base;
      power++;
    }

  /* should "7.0" be printed as "7" ?
     if removing the ".0" is preferred, enable the fourth condition.  */
  int show_decimal_point = (val != 0) && (absld (val) < 10) && (power > 0);
  /* && (absld (val) > simple_round_floor (val))) */

  devmsg ("  after rounding, value=%Lf * %0.f ^ %u\n", val, scale_base, power);

  stpcpy (pfmt, ".*Lf%s");

  int prec = user_precision == -1 ? show_decimal_point : user_precision;

  /* buf_size - 1 used here to ensure place for possible scale_IEC_I suffix.  */
  num_size = snprintf (buf, buf_size - 1, fmt, prec, val,
                       suffix_power_char (power));
  if (num_size < 0 || num_size >= (int) buf_size - 1)
    die (EXIT_FAILURE, 0,
         _("failed to prepare value '%Lf' for printing"), val);

  if (scale == scale_IEC_I && power > 0)
    strncat (buf, "i", buf_size - num_size - 1);

  devmsg ("  returning value: %s\n", quote (buf));

  return;
}