absld (long double val)
{
  return val < 0 ? -val : val;
}