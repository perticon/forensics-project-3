void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002740(fn00000000000024E0(0x05, "Try '%s --help' for more information.\n", 0x00), 0x01, stderr);
		goto l000000000000523E;
	}
	fn00000000000026A0(fn00000000000024E0(0x05, "Usage: %s [OPTION]... [NUMBER]...\n", 0x00), 0x01);
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "Reformat NUMBER(s), or the numbers from standard input if none are specified.\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "\nMandatory arguments to long options are mandatory for short options too.\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --debug          print warnings about invalid input\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "  -d, --delimiter=X    use X instead of whitespace for field delimiter\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --field=FIELDS   replace the numbers in these input fields (default=1);\n                         see FIELDS below\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --format=FORMAT  use printf style floating-point FORMAT;\n                         see FORMAT below for details\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --from=UNIT      auto-scale input numbers to UNITs; default is 'none';\n                         see UNIT below\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --from-unit=N    specify the input unit size (instead of the default 1)\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --grouping       use locale-defined grouping of digits, e.g. 1,000,000\n                         (which means it has no effect in the C/POSIX locale)\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --header[=N]     print (without converting) the first N header lines;\n                         N defaults to 1 if not specified\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --invalid=MODE   failure mode for invalid numbers: MODE can be:\n                         abort (default), fail, warn, ignore\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --padding=N      pad the output to N characters; positive N will\n                         right-align; negative N will left-align;\n                         padding is ignored if the output is wider than N;\n                         the default is to automatically pad if a whitespace\n                         is found\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --round=METHOD   use METHOD for rounding when scaling; METHOD can be:\n                         up, down, from-zero (default), towards-zero, nearest\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --suffix=SUFFIX  add SUFFIX to output numbers, and accept optional\n                         SUFFIX in input numbers\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --to=UNIT        auto-scale output numbers to UNITs; see UNIT below\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --to-unit=N      the output unit size (instead of the default 1)\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "  -z, --zero-terminated    line delimiter is NUL, not newline\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --help        display this help and exit\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "      --version     output version information and exit\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "\nUNIT options:\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "  none       no auto-scaling is done; suffixes will trigger an error\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "  auto       accept optional single/two letter suffix:\n               1K = 1000,\n               1Ki = 1024,\n               1M = 1000000,\n               1Mi = 1048576,\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "  si         accept optional single letter suffix:\n               1K = 1000,\n               1M = 1000000,\n               ...\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "  iec        accept optional single letter suffix:\n               1K = 1024,\n               1M = 1048576,\n               ...\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "  iec-i      accept optional two-letter suffix:\n               1Ki = 1024,\n               1Mi = 1048576,\n               ...\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "\nFIELDS supports cut(1) style field ranges:\n  N    N'th field, counted from 1\n  N-   from N'th field, to end of line\n  N-M  from N'th to M'th field (inclusive)\n  -M   from first to M'th field (inclusive)\n  -    all fields\nMultiple fields/ranges can be separated with commas\n", 0x00));
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "\nFORMAT must be suitable for printing one floating-point argument '%f'.\nOptional quote (%'f) will enable --grouping (if supported by current locale).\nOptional width value (%10f) will pad output. Optional zero (%010f) width\nwill zero pad the number. Optional negative values (%-10f) will left align.\nOptional precision (%.1f) will override the input determined precision.\n", 0x00));
	fn00000000000026A0(fn00000000000024E0(0x05, "\nExit status is 0 if all input numbers were successfully converted.\nBy default, %s will stop at the first conversion error with exit status 2.\nWith --invalid='fail' a warning is printed for each conversion error\nand the exit status is 2.  With --invalid='warn' each conversion error is\ndiagnosed, but the exit status is 0.  With --invalid='ignore' conversion\nerrors are not diagnosed and the exit status is 0.\n", 0x00), 0x01);
	fn00000000000026A0(fn00000000000024E0(0x05, "\nExamples:\n  $ %s --to=si 1000\n            -> \"1.0K\"\n  $ %s --to=iec 2048\n           -> \"2.0K\"\n  $ %s --to=iec-i 4096\n           -> \"4.0Ki\"\n  $ echo 1K | %s --from=si\n           -> \"1000\"\n  $ echo 1K | %s --from=iec\n           -> \"1024\"\n  $ df -B1 | %s --header --field 2-4 --to=si\n  $ ls -l  | %s --header --field 5 --to=iec\n  $ ls -lh | %s --header --field 5 --from=iec --padding=10\n  $ ls -lh | %s --header --field 5 --from=iec --format %%10f\n", 0x00), 0x01);
	struct Eq_6384 * rbx_507 = fp - 0xB8 + 16;
	do
	{
		Eq_12 rsi_510 = rbx_507->qw0000;
		++rbx_507;
	} while (rsi_510 != 0x00 && fn00000000000025E0(rsi_510, 0xB11A) != 0x00);
	ptr64 r13_523 = rbx_507->qw0008;
	if (r13_523 != 0x00)
	{
		fn00000000000026A0(fn00000000000024E0(0x05, "\n%s online help: <%s>\n", 0x00), 0x01);
		Eq_12 rax_610 = fn0000000000002690(null, 0x05);
		if (rax_610 == 0x00 || fn0000000000002430(0x03, 0xB1B5, rax_610) == 0x00)
			goto l00000000000057E6;
	}
	else
	{
		fn00000000000026A0(fn00000000000024E0(0x05, "\n%s online help: <%s>\n", 0x00), 0x01);
		Eq_12 rax_552 = fn0000000000002690(null, 0x05);
		if (rax_552 == 0x00 || fn0000000000002430(0x03, 0xB1B5, rax_552) == 0x00)
		{
			fn00000000000026A0(fn00000000000024E0(0x05, "Full documentation <%s%s>\n", 0x00), 0x01);
l0000000000005823:
			fn00000000000026A0(fn00000000000024E0(0x05, "or available locally via: info '(coreutils) %s%s'\n", 0x00), 0x01);
l000000000000523E:
			fn0000000000002720(edi);
		}
		r13_523 = 0xB11A;
	}
	fn00000000000025C0(stdout, fn00000000000024E0(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", 0x00));
l00000000000057E6:
	fn00000000000026A0(fn00000000000024E0(0x05, "Full documentation <%s%s>\n", 0x00), 0x01);
	goto l0000000000005823;
}