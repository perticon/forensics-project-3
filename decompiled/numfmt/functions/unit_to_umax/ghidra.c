long unit_to_umax(char *param_1)

{
  int iVar1;
  size_t __n;
  void *__dest;
  char *pcVar2;
  undefined8 uVar3;
  undefined8 uVar4;
  char *pcVar5;
  char *pcVar6;
  char *pcVar7;
  long in_FS_OFFSET;
  char *local_40;
  long local_38;
  long local_30;
  
  local_30 = *(long *)(in_FS_OFFSET + 0x28);
  __n = strlen(param_1);
  local_40 = (char *)0x0;
  if ((__n == 0) || ((int)param_1[__n - 1] - 0x30U < 10)) {
    pcVar5 = "KMGTPEZY";
    pcVar2 = param_1;
    pcVar7 = (char *)0x0;
  }
  else {
    __dest = (void *)xmalloc(__n + 2);
    pcVar6 = (char *)((__n - 1) + (long)__dest);
    local_40 = pcVar6;
    pcVar2 = (char *)memcpy(__dest,param_1,__n);
    pcVar7 = pcVar2;
    if ((*pcVar6 == 'i') && ((__n != 1 && (9 < (int)pcVar6[-1] - 0x30U)))) {
      *pcVar6 = '\0';
      pcVar5 = "KMGTPEZY";
    }
    else {
      local_40 = pcVar6 + 2;
      pcVar5 = "KMGTPEZY0";
      *(undefined2 *)(pcVar6 + 1) = 0x42;
    }
  }
  iVar1 = xstrtoumax(pcVar2,&local_40,10,&local_38,pcVar5);
  if (((iVar1 == 0) && (*local_40 == '\0')) && (local_38 != 0)) {
    free(pcVar7);
    if (local_30 == *(long *)(in_FS_OFFSET + 0x28)) {
      return local_38;
    }
  }
  else {
    free(pcVar7);
    uVar3 = quote(param_1);
    uVar4 = dcgettext(0,"invalid unit size: %s",5);
    error(1,0,uVar4,uVar3);
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}