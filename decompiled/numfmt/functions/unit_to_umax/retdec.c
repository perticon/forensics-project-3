int64_t unit_to_umax(char * n_string) {
    int64_t v1 = (int64_t)n_string;
    int64_t v2 = __readfsqword(40); // 0x376d
    int64_t v3 = function_2500(); // 0x377d
    int64_t n = 0; // bp-64, 0x3782
    int64_t v4 = v1; // 0x378e
    int64_t v5 = (int64_t)"KMGTPEZY"; // 0x378e
    int64_t v6; // 0x3827
    int64_t v7; // 0x3832
    int64_t v8; // 0x381c
    if (v3 == 0) {
        goto lab_0x37b1;
    } else {
        int64_t v9 = v3 - 1;
        char v10 = *(char *)(v9 + v1); // 0x3797
        v4 = v1;
        v5 = (int64_t)"KMGTPEZY";
        if (v10 == 57 || (int32_t)v10 < 57) {
            goto lab_0x37b1;
        } else {
            // 0x3818
            v8 = xmalloc();
            v6 = v8 + v9;
            n = v6;
            v7 = function_2610();
            char * v11 = (char *)v6; // 0x3837
            if (v3 == 1 | *v11 != 105) {
                goto lab_0x3870;
            } else {
                char v12 = *(char *)(v6 - 1); // 0x3847
                if (v12 == 57 || (int32_t)v12 < 57) {
                    goto lab_0x3870;
                } else {
                    // 0x3855
                    *v11 = 0;
                    v4 = v7;
                    v5 = (int64_t)"KMGTPEZY";
                    goto lab_0x37b1;
                }
            }
        }
    }
  lab_0x37b1:;
    // 0x37b1
    int64_t result; // bp-56, 0x3760
    int32_t v13 = xstrtoumax((char *)v4, (char **)&n, 10, &result, (char *)v5); // 0x37c0
    if (v13 == 0) {
        // 0x37cd
        if (*(char *)n == 0) {
            // 0x37db
            if (result != 0) {
                // 0x37e9
                function_2400();
                if (v2 != __readfsqword(40)) {
                    // 0x38ce
                    return function_2510();
                }
                // 0x3805
                return result;
            }
        }
    }
    // 0x3894
    function_2400();
    quote(n_string);
    function_24e0();
    function_26d0();
    // 0x38ce
    return function_2510();
  lab_0x3870:
    // 0x3870
    n = v6 + 2;
    *(int16_t *)(v8 + v3) = 66;
    v4 = v7;
    v5 = (int64_t)"KMGTPEZY0";
    goto lab_0x37b1;
}