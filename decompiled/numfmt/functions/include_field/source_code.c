include_field (uintmax_t field)
{
  struct field_range_pair *p = frp;
  if (!p)
    return field == 1;

  while (p->lo != UINTMAX_MAX)
    {
      if (p->lo <= field && p->hi >= field)
        return true;
      ++p;
    }
  return false;
}