uint64 process_line(cu8 cl, Eq_12 rbx, Eq_12 rdi, struct Eq_460 * fs, Eq_461 xmm0, union Eq_462 & clOut, union Eq_1923 & xmm0Out)
{
	ptr64 fp;
	Eq_2479 r15_12 = 0x01;
	struct Eq_1937 * rsp_155 = fp - 0x48;
	int32 edx_108 = g_dw11014;
	byte r14b_148 = *rdi;
	while (edx_108 != 0x80)
	{
		if ((int32) r14b_148 != edx_108)
		{
			if (r14b_148 == 0x00)
				goto l0000000000005130;
			rbx = rdi;
			do
			{
				int32 eax_54 = (int32) *((word64) rbx + 1);
				rbx = (word64) rbx + 1;
				if ((byte) eax_54 == 0x00)
					goto l0000000000005130;
			} while (eax_54 != edx_108);
		}
		else
		{
			rbx = rdi;
l0000000000005120:
			if (r14b_148 == 0x00)
				goto l0000000000005130;
		}
		*rbx = 0x00;
		struct Eq_1937 * rcx_83 = (struct Eq_1937 *) <invalid>;
		word64 r15_77;
		byte r13b_504;
		byte cl_502;
		byte bpl_503;
		byte al_101 = process_field(cl, (word32) rbx, r15_12, rdi, r15_12, fs, xmm0, rsp_155->t0008, out cl_502, out rbx, out bpl_503, out r13b_504, out r15_77, out xmm0);
		rsp_155 = (struct Eq_1937 *) <invalid>;
		cl = (byte) rcx_83;
		edx_108 = g_dw11014;
		byte sil_120 = 0x20;
		Eq_12 rdi_100 = stdout;
		byte * rax_112 = *((word64) rdi_100 + 40);
		if (edx_108 != 0x80)
			sil_120 = (byte) edx_108;
		if (rax_112 < *((word64) rdi_100 + 48))
		{
			*((word64) rdi_100 + 40) = rax_112 + 1;
			*rax_112 = sil_120;
			cl = (byte) rax_112 + 0x01;
		}
		else
		{
			fn0000000000002550((word32) sil_120, rdi_100);
			edx_108 = g_dw11014;
		}
		rdi = rbx + 1;
		r15_12 = r15_77 + 0x01;
		r14b_148 = Mem140[rbx + 1:byte];
	}
	if (r14b_148 != 0x00)
	{
		rsp_155->t0008 = rdi;
		Eq_11882 (** rax_160)[] = fn0000000000002790();
		rdi = rsp_155->t0008;
		Eq_11882 rdx_162[] = *rax_160;
		rbx = rdi;
		do
		{
			if ((rdx_162[(uint64) r14b_148].b0000 & 0x01) == 0x00 && r14b_148 != 0x0A)
			{
				byte al_186 = *rbx;
				while (al_186 != 0x00)
				{
					uint64 rcx_192 = (uint64) al_186;
					cl = (byte) rcx_192;
					if ((rdx_162[rcx_192].b0000 & 0x01) != 0x00 || al_186 == 0x0A)
					{
						r14b_148 = (byte) *rbx;
						goto l0000000000005120;
					}
					al_186 = (byte) *((word64) rbx + 1);
					++rbx;
				}
				break;
			}
			r14b_148 = (byte) *((word64) rbx + 1);
			rbx = (word64) rbx + 1;
		} while (r14b_148 != 0x00);
	}
l0000000000005130:
	struct Eq_1937 * rbp_300 = (struct Eq_1937 *) <invalid>;
	struct Eq_1937 * r13_294 = (struct Eq_1937 *) <invalid>;
	byte bpl_323 = (byte) rbp_300;
	byte r13b_329 = (byte) r13_294;
	Eq_1923 xmm0_309;
	byte cl_497;
	word64 rbx_498;
	byte bpl_499;
	byte r13b_500;
	word64 r15_501;
	if (process_field(cl, (word32) rbx, r15_12, rdi, r15_12, fs, xmm0, rsp_155->t0008, out cl_497, out rbx_498, out bpl_499, out r13b_500, out r15_501, out xmm0_309) == 0x00)
		bpl_323 = 0x00;
	word32 r12d_352 = (word32) bpl_323;
	if (r13b_329 != 0x00)
	{
		Eq_12 rdi_334 = stdout;
		byte dl_340 = g_b11010;
		byte * rax_337 = *((word64) rdi_334 + 40);
		if (rax_337 < *((word64) rdi_334 + 48))
		{
			*((word64) rdi_334 + 40) = rax_337 + 1;
			*rax_337 = dl_340;
		}
		else
			fn0000000000002550((word32) dl_340, rdi_334);
	}
	clOut.u1 = <invalid>;
	xmm0Out = xmm0_309;
	return (uint64) r12d_352;
}