uint32_t process_line(void** rdi, uint32_t esi, ...) {
    void** r15_3;
    uint32_t r13d4;
    int32_t ebp5;
    int32_t edx6;
    uint32_t r14d7;
    void** rax8;
    void** rdx9;
    void** rbx10;
    int64_t rax11;
    uint32_t eax12;
    int64_t rcx13;
    signed char al14;
    int32_t esi15;
    void** rdi16;
    signed char* rax17;
    int32_t eax18;
    signed char al19;
    void** rdi20;
    uint32_t edx21;
    signed char* rax22;

    r15_3 = reinterpret_cast<void**>(1);
    r13d4 = esi;
    ebp5 = 1;
    edx6 = delimiter;
    r14d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
    if (edx6 != 0x80) {
        goto addr_5040_3;
    }
    addr_50d1_4:
    while (*reinterpret_cast<unsigned char*>(&r14d7)) {
        rax8 = fun_2790(rdi, rdi);
        rdi = rdi;
        rdx9 = *reinterpret_cast<void***>(rax8);
        rbx10 = rdi;
        do {
            *reinterpret_cast<uint32_t*>(&rax11) = *reinterpret_cast<unsigned char*>(&r14d7);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx9) + reinterpret_cast<uint64_t>(rax11 * 2))) & 1) 
                continue;
            if (*reinterpret_cast<unsigned char*>(&r14d7) != 10) 
                break;
            r14d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10 + 1));
            ++rbx10;
        } while (*reinterpret_cast<unsigned char*>(&r14d7));
        goto addr_5192_9;
        eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10));
        if (!*reinterpret_cast<unsigned char*>(&eax12)) 
            break;
        do {
            *reinterpret_cast<uint32_t*>(&rcx13) = *reinterpret_cast<unsigned char*>(&eax12);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx13) + 4) = 0;
            if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rdx9) + reinterpret_cast<uint64_t>(rcx13 * 2))) & 1) 
                break;
            if (*reinterpret_cast<unsigned char*>(&eax12) == 10) 
                break;
            eax12 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10 + 1));
            ++rbx10;
        } while (*reinterpret_cast<unsigned char*>(&eax12));
        goto addr_51a8_14;
        r14d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx10));
        while (*reinterpret_cast<unsigned char*>(&r14d7)) {
            while (1) {
                *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
                al14 = process_field(rdi, r15_3);
                edx6 = delimiter;
                esi15 = 32;
                rdi16 = stdout;
                if (!al14) {
                    ebp5 = 0;
                }
                rax17 = *reinterpret_cast<signed char**>(rdi16 + 40);
                if (edx6 != 0x80) {
                    esi15 = edx6;
                }
                if (reinterpret_cast<uint64_t>(rax17) >= reinterpret_cast<uint64_t>(*reinterpret_cast<signed char**>(rdi16 + 48))) {
                    fun_2550();
                    edx6 = delimiter;
                } else {
                    *reinterpret_cast<signed char**>(rdi16 + 40) = rax17 + 1;
                    *rax17 = *reinterpret_cast<signed char*>(&esi15);
                }
                rdi = rbx10 + 1;
                ++r15_3;
                r14d7 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi));
                if (edx6 == 0x80) 
                    goto addr_50d1_4;
                addr_5040_3:
                if (static_cast<int32_t>(*reinterpret_cast<signed char*>(&r14d7)) == edx6) 
                    break;
                if (!*reinterpret_cast<unsigned char*>(&r14d7)) 
                    goto addr_5130_26;
                rbx10 = rdi;
                do {
                    eax18 = reinterpret_cast<signed char>(*reinterpret_cast<void***>(rbx10 + 1));
                    ++rbx10;
                    if (!*reinterpret_cast<signed char*>(&eax18)) 
                        goto addr_5130_26;
                } while (eax18 != edx6);
            }
            rbx10 = rdi;
        }
        goto addr_5129_31;
    }
    addr_5130_26:
    al19 = process_field(rdi, r15_3);
    if (!al19) {
        ebp5 = 0;
    }
    if (*reinterpret_cast<signed char*>(&r13d4)) {
        rdi20 = stdout;
        edx21 = line_delim;
        rax22 = *reinterpret_cast<signed char**>(rdi20 + 40);
        if (reinterpret_cast<uint64_t>(rax22) >= reinterpret_cast<uint64_t>(*reinterpret_cast<signed char**>(rdi20 + 48))) {
            fun_2550();
        } else {
            *reinterpret_cast<signed char**>(rdi20 + 40) = rax22 + 1;
            *rax22 = *reinterpret_cast<signed char*>(&edx21);
        }
    }
    return static_cast<uint32_t>(*reinterpret_cast<unsigned char*>(&ebp5));
    addr_5192_9:
    goto addr_5130_26;
    addr_51a8_14:
    goto addr_5130_26;
    addr_5129_31:
    goto addr_5130_26;
}