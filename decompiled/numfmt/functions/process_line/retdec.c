int32_t process_line(char * line, bool newline) {
    int64_t v1 = (int64_t)line;
    int64_t v2 = v1 % 256; // 0x5026
    int64_t v3 = v1; // 0x5030
    int64_t v4 = delimiter; // 0x5030
    int64_t v5 = 1; // 0x5030
    int64_t v6 = v2; // 0x5030
    int64_t v7 = 1; // 0x5030
    int64_t v8 = v1; // 0x5030
    int64_t v9 = 128; // 0x5030
    int64_t v10 = 1; // 0x5030
    int64_t v11 = v2; // 0x5030
    int64_t v12 = 1; // 0x5030
    if (delimiter == 128) {
        goto lab_0x50d1;
    } else {
        goto lab_0x5040;
    }
  lab_0x5130_2:;
    // 0x5130
    int64_t v13; // 0x5000
    int64_t v14; // 0x5000
    int64_t v15; // 0x5000
    int64_t v16; // 0x5000
    int64_t v17 = process_field(v13, v14, v16, v15, (int64_t)&g48); // 0x5133
    int64_t v18; // 0x5000
    if (!newline) {
        // 0x516d
        return (char)v17 == 0 ? 0 : (int32_t)v18 % 256;
    }
    int64_t v19 = (int64_t)g6; // 0x514b
    int64_t * v20 = (int64_t *)(v19 + 40); // 0x5159
    uint64_t v21 = *v20; // 0x5159
    if (v21 >= *(int64_t *)(v19 + 48)) {
        // 0x51d4
        function_2550();
    } else {
        // 0x5163
        *v20 = v21 + 1;
        *(char *)v21 = line_delim;
    }
    // 0x516d
    return (char)v17 == 0 ? 0 : (int32_t)v18 % 256;
  lab_0x5074:;
    // 0x5074
    int64_t v22; // 0x5000
    int64_t v23 = v22;
    int64_t v24; // 0x5000
    *(char *)v24 = 0;
    int64_t v25; // 0x5000
    int64_t v26; // 0x5000
    int64_t v27; // 0x5000
    int64_t v28 = process_field(v26, v25, v27, v23, (int64_t)&g48); // 0x507a
    int64_t v29 = (int64_t)g6; // 0x508a
    int64_t * v30 = (int64_t *)(v29 + 40); // 0x509d
    uint64_t v31 = *v30; // 0x509d
    int64_t v32; // 0x5000
    int32_t v33; // 0x5000
    if (v31 >= *(int64_t *)(v29 + 48)) {
        // 0x51c0
        function_2550();
        v32 = v23;
        v33 = delimiter;
    } else {
        int32_t v34 = delimiter; // 0x507f
        int64_t v35 = v31 + 1; // 0x50ae
        *v30 = v35;
        *(char *)v31 = v34 != 128 ? (char)v34 : 32;
        v32 = v35;
        v33 = v34;
    }
    // 0x50b9
    int64_t v36; // 0x5000
    int64_t v37 = (char)v28 == 0 ? 0 : v36 & 0xffffffff; // 0x5093
    int64_t v38 = v33;
    int64_t v39 = v24 + 1; // 0x50b9
    int64_t v40 = v25 + 1; // 0x50bd
    int64_t v41 = (int64_t)*(char *)v39; // 0x50c1
    v3 = v39;
    int64_t v42 = v32; // 0x50cb
    v4 = v38;
    v5 = v37;
    v6 = v41;
    v7 = v40;
    v8 = v39;
    int64_t v43 = v32; // 0x50cb
    v9 = v38;
    v10 = v37;
    v11 = v41;
    v12 = v40;
    if (v33 != 128) {
        goto lab_0x5040;
    } else {
        goto lab_0x50d1;
    }
  lab_0x50d1:;
    int64_t v44 = v12;
    int64_t v45 = v10;
    int64_t v46 = v43;
    int64_t v47 = v8; // 0x50d6
    v13 = v47;
    v15 = v46;
    v16 = v9;
    v18 = v45;
    v14 = v44;
    int64_t v48; // 0x5000
    int64_t v49; // 0x5000
    int64_t v50; // 0x5000
    int64_t v51; // 0x5000
    int64_t v52; // 0x5000
    int64_t v53; // 0x5000
    int64_t v54; // 0x5000
    if (v11 == 0) {
        goto lab_0x5130_2;
    } else {
        int64_t v55 = *(int64_t *)function_2790(); // 0x50e5
        int64_t v56 = v11;
        int64_t v57 = v47;
        while (v56 == 10 | *(char *)((2 * v56 & 510) + v55) % 2 != 0) {
            int64_t v58 = v57 + 1; // 0x5180
            unsigned char v59 = *(char *)v58; // 0x5180
            v13 = v47;
            v15 = v46;
            v16 = v55;
            v18 = v45;
            v14 = v44;
            if (v59 == 0) {
                goto lab_0x5130_2;
            }
            v56 = v59;
            v57 = v58;
        }
        char v60 = *(char *)v57; // 0x5104
        char v61 = v60; // 0x5109
        v13 = v47;
        v15 = v46;
        v16 = v55;
        v18 = v45;
        v14 = v44;
        if (v60 == 0) {
            goto lab_0x5130_2;
        } else {
            int64_t v62 = v57;
            int64_t v63 = v61;
            v53 = v47;
            v52 = v63;
            v54 = v55;
            v51 = v62;
            v50 = v45;
            v48 = v63;
            v49 = v44;
            while (v61 != 10 == *(char *)(2 * v63 + v55) % 2 == 0) {
                int64_t v64 = v62 + 1; // 0x5198
                char v65 = *(char *)v64; // 0x5198
                v61 = v65;
                v13 = v47;
                v15 = v63;
                v16 = v55;
                v18 = v45;
                v14 = v44;
                if (v65 == 0) {
                    goto lab_0x5130_2;
                }
                v62 = v64;
                v63 = v61;
                v53 = v47;
                v52 = v63;
                v54 = v55;
                v51 = v62;
                v50 = v45;
                v48 = v63;
                v49 = v44;
            }
            goto lab_0x5120;
        }
    }
  lab_0x5040:;
    int64_t v66 = v7;
    int64_t v67 = v6;
    int64_t v68 = v5;
    int64_t v69 = v4;
    int64_t v70 = v42;
    int64_t v71 = v3;
    int32_t v72 = v69; // 0x5044
    v53 = v71;
    v52 = v70;
    v54 = v69;
    v51 = v71;
    v50 = v68;
    v48 = v67;
    v49 = v66;
    if (0x1000000 * (int32_t)v67 >> 24 == v72) {
        goto lab_0x5120;
    } else {
        // 0x504c
        v13 = v71;
        v15 = v70;
        v16 = v69;
        v18 = v68;
        v14 = v66;
        if (v67 != 0) {
            int64_t v73 = v71 + 1; // 0x5060
            char v74 = *(char *)v73; // 0x5060
            v13 = v71;
            v15 = v70;
            v16 = v69;
            v18 = v68;
            v14 = v66;
            while (v74 != 0) {
                int64_t v75 = v73; // 0x5072
                v26 = v71;
                v22 = v70;
                v27 = v69;
                v24 = v73;
                v36 = v68;
                v25 = v66;
                if ((int32_t)v74 == v72) {
                    goto lab_0x5074;
                }
                v73 = v75 + 1;
                v74 = *(char *)v73;
                v13 = v71;
                v15 = v70;
                v16 = v69;
                v18 = v68;
                v14 = v66;
            }
        }
        goto lab_0x5130_2;
    }
  lab_0x5120:
    // 0x5120
    v26 = v53;
    v22 = v52;
    v27 = v54;
    v24 = v51;
    v36 = v50;
    v25 = v49;
    v13 = v53;
    v15 = v52;
    v16 = v54;
    v18 = v50;
    v14 = v49;
    if ((char)v48 != 0) {
        goto lab_0x5074;
    } else {
        goto lab_0x5130_2;
    }
}