int32_t simple_strtod_int(char * input_str, char ** endptr, float128_t * value, bool * negative) {
    int64_t v1 = (int64_t)input_str;
    int64_t v2 = (int64_t)((char)v1 == 45) + v1;
    *(char *)negative = (char)((char)v1 == 45);
    *(int64_t *)endptr = v2;
    int64_t v3 = (int64_t)*(char *)v2 + 0xffffffd0; // 0x36b2
    int64_t v4; // 0x3690
    int3_t v5; // 0x3690
    int64_t v6; // 0x3690
    int64_t v7; // 0x3690
    int64_t v8; // 0x3690
    int64_t v9; // 0x3690
    int64_t v10; // 0x3690
    int64_t v11; // 0x3690
    int64_t v12; // 0x3690
    int64_t v13; // 0x3690
    int64_t v14; // 0x3690
    int64_t v15; // 0x3690
    int3_t v16; // 0x3690
    int3_t v17; // 0x36ba
    int3_t v18; // 0x36cb
    if ((int32_t)v3 < 10) {
        // 0x36ba
        v17 = v16 - 1;
        __frontend_reg_store_fpr(v17, 0.0L);
        v18 = v16 - 2;
        v4 = v2;
        v10 = v3;
        v12 = 0;
        v6 = 0;
        while (true) {
          lab_0x36cb:
            // 0x36cb
            v7 = v6;
            v13 = v12;
            v11 = v10;
            __frontend_reg_store_fpr(v18, 0.0L);
            if (__frontend_reg_load_fpr(v18) != __frontend_reg_load_fpr(v17)) {
                goto lab_0x3748;
            } else {
                if ((int32_t)v11 != 0) {
                    goto lab_0x3748;
                } else {
                    // 0x36de
                    v14 = v13;
                    v15 = v13;
                    v8 = v7;
                    if ((int32_t)v13 < 19) {
                        goto lab_0x36e9;
                    } else {
                        goto lab_0x36e3;
                    }
                }
            }
        }
    } else {
        // 0x3720
        if ((int32_t)function_2430() != 0) {
            // 0x3715
            return 3;
        }
        int3_t v19 = v16 - 1; // 0x3740
        __frontend_reg_store_fpr(v19, 0.0L);
        v5 = v19;
        v9 = 0;
    }
  lab_0x370c:;
    int3_t v20 = v5;
    if ((char)v1 == 45) {
        // 0x3711
        __frontend_reg_store_fpr(v20, -__frontend_reg_load_fpr(v20));
    }
    // 0x3713
    *(float80_t *)value = __frontend_reg_load_fpr(v20);
    int64_t result = v9; // 0x3713
    // 0x3715
    return result;
  lab_0x3748:;
    int64_t v21 = v13 + 1; // 0x3748
    int64_t v22 = v21 & 0xffffffff; // 0x3748
    uint32_t v23 = (int32_t)v21; // 0x374b
    v15 = v22;
    v8 = v7;
    if (v23 < 19) {
        goto lab_0x36e9;
    } else {
        // 0x3750
        v14 = v22;
        if (v23 == 28) {
            // 0x3755
            __frontend_reg_store_fpr(v17, __frontend_reg_load_fpr(v17));
            result = 2;
            return result;
        }
        goto lab_0x36e3;
    }
  lab_0x36e9:;
    int64_t v24 = v4 + 1;
    __frontend_reg_store_fpr(v17, 10.0L * __frontend_reg_load_fpr(v17));
    *(int64_t *)endptr = v24;
    __frontend_reg_store_fpr(v18, (float80_t)(int32_t)v11);
    int64_t v25 = (int64_t)*(char *)v24 + 0xffffffd0; // 0x3702
    __frontend_reg_store_fpr(v17, __frontend_reg_load_fpr(v18) + __frontend_reg_load_fpr(v17));
    v4 = v24;
    v10 = v25;
    v12 = v15;
    v6 = v8;
    v5 = v17;
    v9 = v8;
    if ((int32_t)v25 >= 10) {
        // break -> 0x370c
        goto lab_0x370c;
    }
    goto lab_0x36cb;
  lab_0x36e3:
    // 0x36e3
    v15 = v14;
    v8 = 1;
    goto lab_0x36e9;
}