word32 simple_strtod_int(byte * rcx, real80 * rdx, union Eq_12 * rsi, Eq_12 rdi, union Eq_462 & clOut)
{
	word32 rdx_32_32_295 = SLICE(rdx, word32, 32);
	word24 edx_24_8_301 = SLICE(rdx, word24, 8);
	byte bpl_22 = 0x00;
	if (*rdi == 0x2D)
	{
		rdi = (word64) rdi + 1;
		bpl_22 = 0x01;
	}
	word64 r8_328;
	word64 r8_335;
	real64 rLoc1_206;
	*rcx = bpl_22;
	*rsi = (union Eq_12 *) rdi;
	Eq_2250 eax_143 = (int32) *rdi - 0x30;
	if (eax_143 <= 0x09)
	{
		rLoc1_206 = 0.0;
		Eq_12 rdi_126 = (word64) rdi + 1;
		uint64 rcx_332 = 0x00;
		word64 r8_341 = 0x00;
		do
		{
			up32 ecx_90 = (word32) rcx_332;
			word64 rdx_325 = SEQ(rdx_32_32_295, edx_24_8_301, (int8) PARITY_EVEN(0.0 - rLoc1_206));
			if (rLoc1_206 != 0.0)
				rdx_325 = SEQ(rdx_32_32_295, 0x00, 0x01);
			word56 rdx_56_8_294 = SLICE(rdx_325, word56, 8);
			if ((byte) rdx_325 == 0x00 && eax_143 == 0x00)
			{
				if (ecx_90 > 0x12)
					goto l00000000000036E3;
			}
			else
			{
				rcx_332 = (uint64) (ecx_90 + 0x01);
				up32 ecx_93 = (word32) rcx_332;
				if (ecx_93 > 0x12)
				{
					if (ecx_93 == 0x1C)
					{
						r8_328 = 0x02;
						goto l0000000000003715;
					}
l00000000000036E3:
					r8_341 = 0x01;
				}
			}
			real64 rLoc1_216 = rLoc1_206 * (real64) 10.0F;
			*rsi = (union Eq_12 *) rdi_126;
			rdi_126 = (word64) rdi_126 + 1;
			rdx_32_32_295 = SLICE(rdx_56_8_294, word32, 24);
			edx_24_8_301 = (word24) rdx_56_8_294;
			eax_143 = (int32) *((word64) rdi_126 - 1) - 0x30;
			rLoc1_206 = rLoc1_216 + (real64) eax_143;
			r8_335 = r8_341;
		} while (eax_143 <= 0x09);
	}
	else
	{
		r8_328 = 0x03;
		if (fn0000000000002430(g_dw11114, decimal_point, rdi) != 0x00)
			goto l0000000000003715;
		rLoc1_206 = 0.0;
		r8_335 = 0x00;
	}
	if (bpl_22 != 0x00)
		rLoc1_206 = -rLoc1_206;
	*rdx = (real80) rLoc1_206;
	r8_328 = r8_335;
l0000000000003715:
	clOut.u1 = <invalid>;
	return (word32) r8_328;
}