uint32_t simple_strtod_int(void** rdi, void** rsi, void* rdx, void** rcx, void** r8, int64_t r9) {
    int32_t ebp7;
    uint32_t eax8;
    void** rdx9;
    void** rsi10;
    int32_t eax11;
    uint32_t r8d12;
    void** rdi13;
    uint32_t ecx14;
    uint1_t pf15;
    int1_t zf16;
    int32_t edx17;

    ebp7 = 0;
    if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 45)) {
        ++rdi;
        ebp7 = 1;
    }
    *reinterpret_cast<void***>(rcx) = *reinterpret_cast<void***>(&ebp7);
    *reinterpret_cast<void***>(rsi) = rdi;
    eax8 = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdi) - 48);
    if (eax8 > 9) {
        rdx9 = reinterpret_cast<void**>(static_cast<int64_t>(decimal_point_length));
        rsi10 = decimal_point;
        eax11 = fun_2430(rdi, rsi10, rdx9);
        r8d12 = 3;
        if (eax11) {
            addr_3715_5:
            return r8d12;
        } else {
            r8d12 = 0;
            __asm__("fldz ");
        }
    } else {
        __asm__("fldz ");
        rdi13 = rdi + 1;
        ecx14 = 0;
        r8d12 = 0;
        pf15 = __intrinsic();
        zf16 = 1;
        while (1) {
            __asm__("fldz ");
            __asm__("fucomip st0, st1");
            *reinterpret_cast<unsigned char*>(&edx17) = pf15;
            if (!zf16) {
                edx17 = 1;
            }
            if (*reinterpret_cast<unsigned char*>(&edx17) || eax8) {
                ++ecx14;
                if (ecx14 <= 18) {
                    addr_36e9_12:
                    __asm__("fmul dword [rip+0x96c1]");
                    *reinterpret_cast<void***>(rsi) = rdi13;
                    __asm__("fild dword [rsp+0xc]");
                    ++rdi13;
                    eax8 = *reinterpret_cast<signed char*>(rdi13 + 0xffffffffffffffff) - 48;
                    __asm__("faddp st1, st0");
                    pf15 = __intrinsic();
                    zf16 = eax8 == 9;
                    if (eax8 <= 9) 
                        continue; else 
                        break;
                } else {
                    if (ecx14 == 28) 
                        goto addr_3755_14;
                }
            } else {
                if (ecx14 <= 18) 
                    goto addr_36e9_12;
            }
            r8d12 = 1;
            goto addr_36e9_12;
        }
    }
    if (*reinterpret_cast<void***>(&ebp7)) {
        __asm__("fchs ");
    }
    __asm__("fstp tword [rbx]");
    goto addr_3715_5;
    addr_3755_14:
    __asm__("fstp st0");
    r8d12 = 2;
    goto addr_3715_5;
}