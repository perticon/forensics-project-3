simple_round_nearest (long double val)
{
  return val < 0 ? val - 0.5 : val + 0.5;
}