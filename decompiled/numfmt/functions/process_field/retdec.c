int64_t process_field(int64_t a1, int64_t a2, int64_t a3, int64_t a4, int64_t a5) {
    // 0x38e0
    int128_t v1; // 0x38e0
    int128_t v2 = v1;
    int128_t v3 = v1;
    float32_t v4; // bp-408, 0x38e0
    int64_t v5 = &v4; // 0x38f0
    __readfsqword(40);
    if (g15 != 0) {
        // 0x392f
        if (*(int64_t *)g15 != -1) {
            while (true) {
                // continue -> 0x3920
            }
        }
        goto lab_0x3938;
    } else {
        if (a2 != 1) {
            goto lab_0x3938;
        } else {
            goto lab_0x3986;
        }
    }
  lab_0x3986:
    // 0x3986
    if (suffix != 0) {
        uint64_t v6 = function_2500(); // 0x3995
        uint64_t v7 = function_2500(); // 0x39a0
        if (v6 > v7) {
            int64_t v8 = function_25e0(); // 0x39b6
            char v9 = *(char *)&dev_debug; // 0x39bb
            if ((int32_t)v8 != 0) {
                if (v9 != 0) {
                    // 0x420b
                    function_2730();
                }
            } else {
                // 0x39ca
                *(char *)(v6 + a1 - v7) = 0;
                if (v9 != 0) {
                    // 0x43e7
                    quote((char *)suffix);
                    function_2740();
                }
            }
        }
    }
    // 0x39d8
    int64_t v10; // 0x38e0
    char v11 = *(char *)&v10; // 0x39d8
    int64_t v12 = a1; // 0x39df
    int64_t v13 = 0; // 0x39df
    if (v11 != 0) {
        int64_t v14 = *(int64_t *)function_2790(); // 0x39ed
        char v15 = v11; // 0x39f0
        int64_t v16 = a1; // 0x39f0
        int64_t v17 = v16; // 0x3a09
        while (*(char *)(2 * (int64_t)v15 + v14) % 2 != 0) {
            // 0x39f8
            v16++;
            v15 = *(char *)v16;
            v17 = v16;
            if (v15 == 0) {
                // break -> 0x3a0b
                break;
            }
            v17 = v16;
        }
        // 0x3a0b
        v12 = v17;
        v13 = a1 - v17 & 0xffffffff;
    }
    int64_t v18 = v12;
    int64_t v19; // 0x38e0
    int64_t v20; // 0x38e0
    float80_t v21; // bp-296, 0x38e0
    char * v22; // bp-312, 0x38e0
    if (auto_padding == 0) {
        goto lab_0x3c20;
    } else {
        // 0x3a1f
        if (a2 > 1 || v13 != 0) {
            int64_t v23 = function_2500(); // 0x3ea3
            padding_width = v23;
            if (v23 >= padding_buffer_size) {
                // 0x3ebc
                padding_buffer_size = v23 + 1;
                padding_buffer = xrealloc();
            }
        } else {
            // 0x3a31
            padding_width = 0;
        }
        // 0x3a3c
        if (*(char *)&dev_debug != 0) {
            // 0x3bff
            function_2740();
            goto lab_0x3c20;
        } else {
            // 0x3a49
            v22 = NULL;
            int64_t v24 = __asm_movss(__asm_pxor(v2, v2)); // 0x3a5d
            v4 = (int32_t)v24;
            v21 = (int32_t)v24;
            int64_t v25 = scale_from < 5 ? (int64_t)(int32_t)&g20 + 24 & 0xfffffff8 : (int64_t)(int32_t)&g20; // 0x3a77
            v19 = v25;
            v20 = scale_from;
            goto lab_0x3a7e;
        }
    }
  lab_0x456c:;
    float80_t v26 = 0.0L; // 0x4574
    float80_t v27; // 0x4a8f
    float80_t v28 = v27; // 0x4574
    int64_t v29 = 128; // 0x4574
    int64_t v30; // 0x38e0
    int64_t v31 = v30; // 0x4574
    int64_t v32; // 0x4905
    int64_t v33 = v32; // 0x4574
    int32_t v34 = 0; // 0x4574
    int64_t v35; // 0x48f1
    int64_t v36 = v35; // 0x4574
    float80_t v37 = 0.0L; // 0x4574
    float80_t v38 = 0.0L; // 0x4574
    float80_t v39 = v27; // 0x4574
    int64_t v40 = 128; // 0x4574
    int64_t v41 = v30; // 0x4574
    int64_t v42 = v32; // 0x4574
    int64_t v43 = v35; // 0x4574
    float80_t v44; // 0x38e0
    if (v44 != 0.0L) {
        goto lab_0x4b36;
    } else {
        goto lab_0x4586;
    }
  lab_0x4563:;
    // 0x4563
    float80_t v45; // 0x38e0
    v44 = v45;
    int64_t v46; // 0x38e0
    v30 = v46 + 1 & 0xffffffff;
    goto lab_0x456c;
  lab_0x4708:;
    // 0x4708
    int64_t v47; // 0x38e0
    int64_t v48 = v47;
    float80_t v49; // 0x38e0
    float80_t v50 = v49;
    float80_t v51 = v50; // 0x470a
    int64_t v52 = v48; // 0x470a
    char v53; // 0x38e0
    float80_t * v54; // 0x38e0
    if (v53 != 0) {
        // 0x470c
        *v54 = v50;
        quote((char *)v35);
        float80_t v55 = *v54; // 0x4717
        *(float80_t *)v32 = v55;
        function_2740();
        v51 = v55;
        v52 = v48;
    }
    goto lab_0x4744;
  lab_0x46e2:;
    // 0x46e2
    float80_t v106; // 0x38e0
    *v54 = v106;
    function_2500();
    function_2750();
    v49 = *v54;
    char v107; // 0x38e0
    v53 = v107;
    int64_t v108; // 0x38e0
    v47 = v108;
    goto lab_0x4708;
  lab_0x4744:;
    uint64_t v56 = function_2500(); // 0x474e
    uint64_t v57 = padding_buffer_size; // 0x4753
    int64_t v58 = padding_buffer; // 0x475a
    v10 = v58;
    int64_t v59; // 0x38e0
    int64_t v60; // 0x38e0
    if (padding_width > v56) {
        int64_t v61 = v60 + 128; // 0x4d54
        int64_t * v62 = (int64_t *)v61; // 0x4d65
        *v62 = padding_width;
        mbsalign((char *)v35, (char *)v58, v57, v62, padding_alignment, 2);
        v59 = v61;
        if (*(char *)&dev_debug != 0) {
            char * v63 = quote((char *)padding_buffer); // 0x4d86
            function_2740();
            v59 = (int64_t)v63;
        }
    } else {
        int64_t v64 = v56 + 1; // 0x476f
        if (v64 >= v57) {
            // 0x4778
            padding_buffer_size = v56 + 2;
            padding_buffer = xrealloc();
        }
        // 0x4792
        function_2450();
        v59 = v64;
    }
    // 0x479a
    if (format_str_prefix != 0) {
        // 0x47ad
        function_25c0();
    }
    // 0x47b9
    function_25c0();
    if (format_str_suffix != 0) {
        // 0x47db
        function_25c0();
    }
    int64_t v65 = 1; // 0x395e
    int64_t v66 = v52; // 0x395e
    int64_t v67 = v60; // 0x395e
    int64_t v68 = padding_width; // 0x395e
    int64_t v69 = v59; // 0x395e
    float80_t v70; // 0x38e0
    float80_t v71 = v70; // 0x395e
    float80_t v72; // 0x492d
    float80_t v73 = v72; // 0x395e
    float80_t v74 = v51; // 0x395e
    if (*(int64_t *)(v60 + 344) == __readfsqword(40)) {
        // 0x3964
        return 1;
    }
    goto lab_0x4f9e;
  lab_0x3938:
    // 0x3938
    function_25c0();
    int64_t v109 = a4; // 0x3948
    int64_t v110 = a2; // 0x3948
    int64_t v111 = 1; // 0x3948
    goto lab_0x394d;
  lab_0x394d:;
    int64_t result = v111; // 0x395e
    v65 = v111;
    v66 = v110;
    v67 = v5;
    int64_t v112; // 0x38e0
    v68 = v112;
    v69 = v109;
    float80_t v113; // 0x38e0
    v73 = v113;
    if (*(int64_t *)(v5 + 344) == __readfsqword(40)) {
        // 0x3964
        return result;
    }
    float80_t v114; // 0x38e0
    v74 = v114;
    while (true) {
      lab_0x4f9e:;
        int64_t v75 = v66;
        function_2510();
        float80_t v76 = v74; // 0x4fa2
        float80_t v77 = v73; // 0x4fa2
        float80_t v78 = v71; // 0x4fa2
        int64_t v79 = v69; // 0x4fa2
        int64_t v80 = v68; // 0x4fa2
        int64_t v81 = v67; // 0x4fa2
        int64_t v82 = v65; // 0x4fa2
        while (true) {
          lab_0x4b34:
            // 0x4b34
            v37 = v77;
            v38 = v76;
            v39 = v78;
            v40 = v79;
            v41 = v80;
            v42 = v81;
            v43 = v82;
            while (true) {
              lab_0x4b36:;
                float80_t v83 = v38;
                bool v84 = false; // 0x4b44
                bool v85 = false; // 0x4b44
                if (v83 >= 10.0L) {
                    v84 = true;
                    v85 = false;
                    if (v83 <= 10.0L) {
                        v84 = v83 != 10.0L;
                        v85 = true;
                    }
                }
                v26 = v37;
                v28 = v39;
                v29 = v40;
                v31 = v41;
                v33 = v42;
                v34 = !(((int32_t)v41 == 0 | v84 | v85));
                v36 = v43;
                while (true) {
                  lab_0x4586:
                    // 0x4586
                    v60 = v33;
                    v46 = v31;
                    v70 = v28;
                    float80_t v86 = v26;
                    float80_t v87 = v86; // 0x4588
                    float80_t v88 = v86; // 0x4588
                    if ((char)v29 != 0) {
                        float80_t * v89 = (float80_t *)(v60 + 32); // 0x4cad
                        *v89 = v86;
                        *(int32_t *)(v60 + 16) = v34;
                        *(float80_t *)(v60 - 16) = v86;
                        float80_t * v90 = (float80_t *)v60; // 0x4cd9
                        *v90 = v86;
                        function_2740();
                        v88 = *v89;
                        v87 = *v90;
                    }
                    float80_t v91 = v88;
                    float80_t v92 = v87;
                    *(int32_t *)v36 = 0x664c2a2e;
                    *(int16_t *)(v36 + 4) = 0x7325;
                    *(char *)(v36 + 6) = 0;
                    if ((int32_t)v46 < 9) {
                        // break (via goto) -> 0x45b8
                        goto lab_0x45b8;
                    }
                    float80_t * v93 = (float80_t *)(v60 + 16); // 0x48e3
                    *v93 = v91;
                    v35 = v60 + 208;
                    v10 = v35;
                    v32 = v60 - 16;
                    int64_t * v94 = (int64_t *)v32; // 0x4905
                    *v94 = (int64_t)"(error)";
                    *(float80_t *)(v60 - 32) = v92;
                    v54 = (float80_t *)v60;
                    *v54 = v92;
                    int64_t v95 = function_23f0(); // 0x491d
                    v72 = *v93;
                    if ((int32_t)v95 < 127) {
                        // break (via goto) -> 0x4937
                        goto lab_0x4937;
                    }
                    // 0x4a4c
                    *v54 = v72;
                    function_24e0();
                    float80_t v96 = *v54; // 0x4a64
                    *(int64_t *)(v60 - 8) = 128;
                    *v94 = 128;
                    *(float80_t *)v32 = v96;
                    function_26d0();
                    int64_t * v97 = (int64_t *)v60; // 0x4a83
                    *v97 = (int64_t)v70;
                    bool v98 = false; // 0x4545
                    bool v99 = false; // 0x4545
                    if (v96 >= 0.0L) {
                        v98 = true;
                        v99 = false;
                        if (v96 <= 0.0L) {
                            v98 = v96 != 0.0L;
                            v99 = true;
                        }
                    }
                    v27 = *v54 + v70;
                    *v97 = __asm_movsd_1(v1);
                    float80_t v100 = (float80_t)*(float64_t *)v60;
                    if (v98 || v99) {
                        // 0x454d
                        v45 = v100;
                        v44 = v100;
                        v30 = v46;
                        if (v96 < v100) {
                            goto lab_0x456c;
                        } else {
                            goto lab_0x4563;
                        }
                    } else {
                        // 0x4b1c
                        v45 = v96;
                        v76 = v96;
                        v77 = v96;
                        v78 = v27;
                        v79 = 128;
                        v80 = v46;
                        v81 = v32;
                        v82 = v35;
                        if (-v96 >= v100) {
                            goto lab_0x4563;
                        } else {
                            goto lab_0x4b34;
                        }
                    }
                }
            }
        }
      lab_0x4937:;
        float80_t v101 = v72; // 0x493b
        if ((int32_t)v75 == 4) {
            // 0x4e6b
            *v54 = v72;
            function_2750();
            v101 = *v54;
        }
        float80_t v102 = v101;
        char v103 = *(char *)&dev_debug; // 0x494a
        if (v103 != 0) {
            // 0x4dad
            *v54 = v102;
            quote((char *)v35);
            function_2740();
            char v104 = *(char *)&dev_debug; // 0x4dd5
            float80_t v105 = *v54; // 0x4ddc
            v106 = v105;
            v107 = v104;
            v108 = suffix;
            v49 = v105;
            v53 = v104;
            v47 = 0;
            if (suffix == 0) {
                goto lab_0x4708;
            } else {
                goto lab_0x46e2;
            }
        } else {
            // 0x4959
            v106 = v102;
            v107 = v103;
            v108 = suffix;
            v51 = v102;
            v52 = suffix;
            if (suffix != 0) {
                goto lab_0x46e2;
            } else {
                goto lab_0x4744;
            }
        }
    }
  lab_0x45b8:;
    int32_t v115 = *(int32_t *)((4 * v46 & 0x3fffffffc) + (int64_t)&g26); // 0x45c1
    return (int64_t)v115 + (int64_t)&g26;
  lab_0x3c20:;
    int64_t v116 = scale_from; // 0x3c20
    v22 = NULL;
    int64_t v117 = __asm_movss(__asm_pxor(v3, v3)); // 0x3c3b
    v4 = (int32_t)v117;
    v21 = (int32_t)v117;
    int64_t v118 = scale_from < 5 ? (int64_t)(int32_t)&g20 + 24 & 0xfffffff8 : (int64_t)(int32_t)&g20; // 0x3c55
    v19 = v118;
    v20 = v116;
    if (*(char *)&dev_debug != 0) {
        // 0x3c64
        quote_n();
        quote_n();
        function_2740();
        v19 = v118;
        v20 = v116;
    }
    goto lab_0x3a7e;
  lab_0x3a7e:
    // 0x3a7e
    v10 = v18;
    char * v119 = (char *)v18; // 0x3a90
    char v120; // bp-314, 0x38e0
    uint32_t v121 = simple_strtod_int(v119, &v22, (float128_t *)&v21, (bool *)&v120); // 0x3a90
    int64_t v122 = v121; // 0x3a95
    int64_t v123 = &v120; // 0x3a9a
    int64_t v124 = v122; // 0x3a9a
    int64_t v125; // 0x38e0
    int64_t v126; // 0x38e0
    int64_t v127; // 0x38e0
    int64_t v128; // 0x38e0
    int64_t v129; // 0x38e0
    int64_t v130; // 0x38e0
    int64_t v131; // 0x38e0
    float80_t v132; // 0x38e0
    float80_t v133; // 0x38e0
    float80_t v134; // 0x38e0
    float80_t v135; // 0x38e0
    float80_t v136; // 0x38e0
    if (v121 < 2) {
        // 0x3aa0
        v126 = v20;
        v125 = v19;
        int64_t v137 = (int64_t)v22; // 0x3aa0
        v10 = v137;
        v129 = v122;
        v127 = v137;
        if ((int32_t)function_2430() == 0) {
            float80_t v138 = v4; // 0x3df2
            int64_t v139 = (int64_t)decimal_point_length + v137; // 0x3df5
            v10 = v139;
            char * v140 = (char *)v139; // 0x3e0c
            v22 = v140;
            int64_t v141 = (float64_t)v4; // bp-280, 0x3e11
            char * v142; // bp-304, 0x38e0
            char v143; // bp-313, 0x38e0
            uint32_t v144 = simple_strtod_int(v140, &v142, (float128_t *)&v141, (bool *)&v143); // 0x3e1d
            if (v144 < 2) {
                int64_t v145 = 0x100000000 * v139 >> 32; // 0x3e34
                v10 = v145;
                if (v143 != 0) {
                    float80_t v146 = v21; // 0x432a
                    v133 = v146;
                    int64_t v147; // 0x3dfa
                    v131 = v147;
                    v130 = 3;
                    v128 = (int64_t)"invalid number: %s";
                    goto lab_0x3f70;
                } else {
                    int64_t v148 = (int64_t)v142; // 0x3e47
                    float80_t v149 = v138; // 0x3e60
                    float80_t v150; // 0x38e0
                    float80_t v151; // 0x38e0
                    if ((int32_t)v148 != (int32_t)v145) {
                        int32_t v152 = (int32_t)(v148 - v145) - 1; // 0x3e62
                        float80_t v153 = v138; // 0x3e65
                        int32_t v154 = v152; // 0x3e65
                        if (v152 == 0) {
                            // 0x4ea2
                            v149 = v138 / 10.0L;
                        } else {
                            float80_t v155; // 0x38e0
                            float80_t v156 = v155 * v153; // 0x3e78
                            int32_t v157 = v154 - 1; // 0x3e7a
                            v153 = v156;
                            v154 = v157;
                            while (v157 != 0) {
                                // 0x3e78
                                v156 = v155 * v153;
                                v157 = v154 - 1;
                                v153 = v156;
                                v154 = v157;
                            }
                            // 0x3e7f
                            v149 = v156;
                            v150 = v156;
                            v151 = v155 / v156;
                        }
                    }
                    float80_t v158 = v149;
                    v21 = v158;
                    v22 = v142;
                    v134 = v158;
                    v135 = v150 + (v120 == 0 ? v158 : -v158);
                    v136 = v151;
                    v129 = (int64_t)(v144 == 1 ? 1 : v121);
                    v127 = v148;
                    goto lab_0x3ada;
                }
            } else {
                // 0x3f32
                v132 = v138;
                v123 = &v143;
                v124 = v144;
                goto lab_0x3f38;
            }
        } else {
            goto lab_0x3ada;
        }
    } else {
        goto lab_0x3f38;
    }
  lab_0x3f38:;
    int64_t v159 = v124;
    v133 = v132;
    float80_t v160 = v132; // 0x3f41
    v131 = v123;
    v130 = v159;
    v128 = 0;
    if ((int32_t)v159 < 7) {
        int32_t v161 = *(int32_t *)(4 * v159 + (int64_t)&g24); // 0x3f50
        return (int64_t)v161 + (int64_t)&g24;
    }
    goto lab_0x3f70;
  lab_0x3ada:;
    int64_t v274 = v127;
    int64_t v253 = v129;
    float80_t v252 = v136;
    float80_t v266 = v135;
    float80_t v269 = v134;
    if (*(char *)&dev_debug != 0) {
        // 0x3db7
        v10 = (int64_t)g12;
        function_2740();
    }
    unsigned char v275 = *(char *)v274; // 0x3ae7
    int64_t v246; // 0x38e0
    int64_t v262; // 0x38e0
    int32_t v267; // 0x38e0
    int64_t v236; // 0x38e0
    int64_t v261; // 0x38e0
    int64_t v260; // 0x38e0
    int64_t v238; // 0x38e0
    int64_t v259; // 0x38e0
    float80_t v242; // 0x38e0
    float80_t v240; // 0x38e0
    float80_t v165; // 0x38e0
    if (v275 != 0) {
        int64_t v276 = function_2790(); // 0x3cc4
        int64_t v277 = (int64_t)(float64_t)(float80_t)(int80_t)v275 % 256; // 0x3cc9
        v10 = 0;
        int64_t v278 = *(int64_t *)v276; // 0x3cd4
        int64_t v279 = v274; // 0x3cf7
        int64_t v280 = v274; // 0x3cf7
        int64_t v281 = v277; // 0x3cf7
        if (*(char *)(v278 + 2 * v277) % 2 != 0) {
            int64_t v282 = v279 + 1; // 0x3cef
            char * v283 = (char *)v282;
            int64_t v284 = (int64_t)*v283; // 0x3ce0
            v10 = 1;
            v279 = v282;
            while (*(char *)(2 * v284 + v278) % 2 != 0) {
                // 0x3ce0
                v282 = v279 + 1;
                v283 = (char *)v282;
                v284 = (int64_t)*v283;
                v10 = 1;
                v279 = v282;
            }
            // 0x3cfe
            v22 = v283;
            v280 = v282;
            v281 = v284;
        }
        int64_t v285 = v281;
        v10 = (int64_t)"KMGTPEZY";
        v133 = v269;
        v160 = v266;
        v165 = v252;
        v131 = v285;
        v130 = 5;
        v128 = (int64_t)"invalid suffix in input: %s";
        if (function_2540() == 0) {
            goto lab_0x3f70;
        } else {
            // 0x3d2c
            v133 = v269;
            v160 = v266;
            v165 = v252;
            v131 = v285;
            v130 = 4;
            v128 = (int64_t)"rejecting suffix in input: %s (consider using --from)";
            if (v126 == 0) {
                goto lab_0x3f70;
            } else {
                int64_t v286 = v280;
                uint32_t v287 = (int32_t)(float32_t)(float80_t)(int80_t)(char)v285 % 256 - 69; // 0x3d42
                int64_t v288 = v287; // 0x3d42
                int64_t v289 = v288; // 0x3d48
                int32_t v290 = 0; // 0x3d48
                if ((char)v287 < 22) {
                    int64_t v291 = v288 % 256; // 0x3d4a
                    v290 = *(int32_t *)(4 * v291 + (int64_t)&g32);
                    v289 = v291;
                }
                // 0x3d58
                v267 = v290;
                int64_t v292 = v286 + 1; // 0x3d58
                char * v293 = (char *)v292; // 0x3d5c
                v22 = v293;
                v261 = v292;
                switch ((int32_t)v126) {
                    case 1: {
                        unsigned char v294 = *(char *)&dev_debug; // 0x4aa4
                        int64_t v295 = v294; // 0x4aa4
                        v259 = v295;
                        v260 = v292;
                        v262 = v125;
                        if (*v293 == 105) {
                            int64_t v296 = v286 + 2; // 0x4c89
                            v22 = (char *)v296;
                            v259 = v295;
                            v260 = v296;
                            v262 = &g21;
                            if (v294 != 0) {
                                // 0x4e22
                                v10 = (int64_t)g12;
                                function_2740();
                                v259 = (int64_t)*(char *)&dev_debug;
                                v260 = v296;
                                v262 = &g21;
                            }
                        }
                        goto lab_0x3d85;
                    }
                    case 4: {
                        // 0x42cc
                        v133 = v269;
                        v160 = v266;
                        v165 = v252;
                        v131 = v289;
                        v130 = 6;
                        v128 = (int64_t)"missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)";
                        if (*v293 != 105) {
                            goto lab_0x3f70;
                        } else {
                            int64_t v297 = v286 + 2; // 0x42d6
                            v22 = (char *)v297;
                            v261 = v297;
                            goto lab_0x3d75;
                        }
                    }
                    default: {
                        goto lab_0x3d75;
                    }
                }
            }
        }
    } else {
        // 0x3af7
        v133 = v269;
        v160 = v266;
        v165 = v252;
        v131 = v275;
        v130 = 6;
        v128 = (int64_t)"missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)";
        if (v126 == 4) {
            goto lab_0x3f70;
        } else {
            // 0x3b01
            v242 = v269;
            v240 = v266;
            v238 = (int64_t)*(char *)&dev_debug;
            v236 = v274;
            v246 = v125;
            goto lab_0x3b0d;
        }
    }
  lab_0x3f70:;
    int64_t v162 = v128;
    int64_t v163 = v130;
    float80_t v164 = v165;
    float80_t v166 = v160;
    float80_t v167 = v133;
    float80_t v168 = v167; // 0x3f77
    float80_t v169 = v166; // 0x3f77
    float80_t v170 = v164; // 0x3f77
    int64_t v171 = v131; // 0x3f77
    int64_t v172 = v163; // 0x3f77
    int64_t v173 = v162; // 0x3f77
    if (inval_style != 3) {
        char * v174 = quote(v119); // 0x3f84
        function_24e0();
        v10 = conv_exit_code;
        function_26d0();
        v168 = v167;
        v169 = v166;
        v170 = v164;
        v171 = (int64_t)v174;
        v172 = v163;
        v173 = v162;
    }
    goto lab_0x3b90;
  lab_0x3b90:;
    int64_t v175 = v172;
    int64_t v176 = v171;
    float80_t v177 = v170;
    float80_t v178 = v169;
    float80_t v179 = v168;
    float80_t v180 = v179; // 0x3ba2
    float80_t v181 = v178; // 0x3ba2
    if (from_unit_size != 1 || to_unit_size != 1) {
        float80_t v182 = from_unit_size >= 0 ? v179 : v179 + 18446744073709551616.0L;
        *(int64_t *)(v5 + 32) = to_unit_size;
        float80_t v183 = to_unit_size; // 0x3bc3
        float80_t v184 = to_unit_size < 0 ? v183 + 18446744073709551616.0L : v183;
        v180 = v184;
        v181 = v178 * v182 / v184;
    }
    float80_t v185 = v180;
    float80_t v186 = v185; // 0x3bd5
    float80_t v187 = v181; // 0x3bd5
    int64_t v188 = v176; // 0x3bd5
    int64_t v189 = v175; // 0x3bd5
    int64_t v190 = v173; // 0x3bd5
    if ((int32_t)v175 < 2) {
        int64_t v191 = user_precision; // 0x3fd0
        int64_t v192 = v5 + 16; // 0x3fe3
        int64_t * v193 = (int64_t *)v192; // 0x3fe3
        int64_t v194 = v191 == -1 ? *v193 : v191; // 0x3fe3
        *v193 = v194;
        if (v185 >= -1.18973e+4932L) {
            while (true) {
                // continue -> 0x4018
            }
        }
        // 0x40a0
        if (v194 < 19 || scale_to != 0) {
            // 0x40b6
            *(char *)(v5 + 144) = 37;
            if (grouping != 0) {
                // 0x40d9
                *(char *)(v5 + 145) = 39;
            }
            float80_t v195 = -1.18973e+4932L; // 0x40f3
            if (zero_padding_width != 0) {
                float80_t * v196 = (float80_t *)(v5 + 32); // 0x4458
                *v196 = -1.18973e+4932L;
                function_23f0();
                v195 = *v196;
            }
            char v197 = *(char *)&dev_debug; // 0x40f9
            float80_t v198 = v195; // 0x4102
            char v199 = v197; // 0x4102
            if (v197 != 0) {
                float80_t * v200 = (float80_t *)(v5 + 32); // 0x4433
                *v200 = v195;
                function_2730();
                v199 = *(char *)&dev_debug;
                v198 = *v200;
            }
            int64_t v201 = round_style; // 0x40cd
            if (scale_to == 0) {
                int32_t v202 = (int32_t)*v193; // 0x436f
                if (v202 == 0) {
                    uint16_t result2 = *(int16_t *)(v5 + 78) | 3072;
                    *(int16_t *)(v5 + 76) = result2;
                    *(int64_t *)(v5 + 32) = (int64_t)(v198 / 9.22337e+18L);
                    if (round_style >= 5) {
                        // 0x4fe4
                        return result2;
                    }
                    int32_t v203 = *(int32_t *)(4 * v201 + (int64_t)&g29); // 0x4d40
                    return (int64_t)v203 + (int64_t)&g29;
                }
                int32_t v204 = v202 - 1; // 0x4377
                int32_t v205 = v204; // 0x437a
                if (v204 == 0) {
                    uint16_t result3 = *(int16_t *)(v5 + 78) | 3072;
                    *(int16_t *)(v5 + 76) = result3;
                    *(int64_t *)(v5 + 32) = (int64_t)(10.0L * v198 / 9.22337e+18L);
                    if (round_style >= 5) {
                        // 0x4fed
                        return result3;
                    }
                    int32_t v206 = *(int32_t *)(4 * v201 + (int64_t)&g30); // 0x4f34
                    return (int64_t)v206 + (int64_t)&g30;
                }
                float80_t v207 = -1.18973e+4932L * v198; // 0x4390
                v205--;
                float80_t v208 = v207; // 0x4395
                while (v205 != 0) {
                    // 0x4390
                    v207 = -1.18973e+4932L * v208;
                    v205--;
                    v208 = v207;
                }
                // 0x4397
                *(int16_t *)(v5 + 76) = *(int16_t *)(v5 + 78) | 3072;
                *(int64_t *)(v5 + 32) = (int64_t)(v177 * v207 / 9.22337e+18L);
                int32_t v209 = *(int32_t *)(4 * v201 + (int64_t)&g25); // 0x43dd
                return (int64_t)v209 + (int64_t)&g25;
            }
            int32_t * v210 = (int32_t *)v192;
            int128_t v211; // 0x38e0
            if (scale_to < 5) {
                // 0x4b07
                *v210 = (int32_t)&g21;
                v211 = __asm_movsd(0x4090000000000000);
            } else {
                // 0x411c
                *v210 = (int32_t)&g20;
                v211 = __asm_movsd(0x408f400000000000);
            }
            int64_t v212 = 0; // 0x4138
            float80_t v213 = -1.18973e+4932L; // 0x4138
            int64_t v214 = 0; // 0x4138
            if (v198 >= -1.18973e+4932L) {
                float80_t v215 = -1.18973e+4932L;
                bool v216 = false; // 0x4163
                bool v217 = false; // 0x4163
                if (v215 >= 0.0L) {
                    v216 = true;
                    v217 = false;
                    if (v215 <= 0.0L) {
                        v216 = v215 != 0.0L;
                        v217 = true;
                    }
                }
                v213 = v215;
                v214 = v212;
                while ((v216 | v217 ? v215 : -v215) >= -1.18973e+4932L) {
                    // 0x4156
                    v212 = v212 + 1 & 0xffffffff;
                    v215 /= -1.18973e+4932L;
                    v216 = false;
                    v217 = false;
                    if (v215 >= 0.0L) {
                        v216 = true;
                        v217 = false;
                        if (v215 <= 0.0L) {
                            v216 = v215 != 0.0L;
                            v217 = true;
                        }
                    }
                    v213 = v215;
                    v214 = v212;
                }
            }
            float80_t v218 = v213; // 0x416f
            float80_t v219 = v213; // 0x416f
            if (v199 != 0) {
                float80_t * v220 = (float80_t *)(v5 + 48); // 0x4c35
                *v220 = v213;
                *v193 = __asm_movsd_1(v211);
                *(float80_t *)(v5 - 16) = v213;
                float80_t * v221 = (float80_t *)(v5 + 32); // 0x4c62
                *v221 = v213;
                function_2740();
                __asm_movsd(*v193);
                v219 = *v220;
                v218 = *v221;
            }
            float80_t v222 = v218;
            int64_t v223 = user_precision; // 0x4175
            uint32_t v224; // 0x4b99
            if (v223 == -1) {
                bool v225 = false; // 0x4bce
                bool v226 = false; // 0x4bce
                if (v222 >= 0.0L) {
                    v225 = true;
                    v226 = false;
                    if (v222 <= 0.0L) {
                        v225 = v222 != 0.0L;
                        v226 = true;
                    }
                }
                if ((v225 || v226 ? v222 : -v222) >= 10.0L) {
                    // 0x4b56
                    *(int16_t *)(v5 + 76) = *(int16_t *)(v5 + 78) | 3072;
                    *v193 = (int64_t)(v222 / 9.22337e+18L);
                    v224 = *(int32_t *)(4 * v201 + (int64_t)&g27);
                    return (int64_t)v224 + (int64_t)&g27;
                }
            } else {
                int64_t v227 = 3 * v214; // 0x4186
                int64_t v228 = (v227 & 0xffffffff) - v223; // 0x4189
                int32_t v229 = v228 < 0 == (v228 & v223) < 0 == (v228 != 0) ? v223 : v227; // 0x4192
                if (v229 == 0) {
                    // 0x4b56
                    *(int16_t *)(v5 + 76) = *(int16_t *)(v5 + 78) | 3072;
                    *v193 = (int64_t)(v222 / 9.22337e+18L);
                    v224 = *(int32_t *)(4 * v201 + (int64_t)&g27);
                    return (int64_t)v224 + (int64_t)&g27;
                }
                int32_t result4 = v229 - 1; // 0x419a
                if (result4 != 0) {
                    float80_t v230 = v219 * v222; // 0x41b0
                    int32_t v231 = result4 - 1; // 0x41b2
                    float80_t v232 = v230; // 0x41b5
                    int32_t v233 = v231; // 0x41b5
                    while (v231 != 0) {
                        // 0x41b0
                        v230 = v219 * v232;
                        v231 = v233 - 1;
                        v232 = v230;
                        v233 = v231;
                    }
                    // 0x41b7
                    *(int16_t *)(v5 + 76) = *(int16_t *)(v5 + 78) | 3072;
                    *v193 = (int64_t)(v177 * v230 / 9.22337e+18L);
                    return result4;
                }
            }
            // 0x4be4
            *(int16_t *)(v5 + 76) = *(int16_t *)(v5 + 78) | 3072;
            *v193 = (int64_t)(v222 * v219 / 9.22337e+18L);
            if (round_style >= 5) {
                process_field_cold();
            }
            int32_t v234 = *(int32_t *)(4 * v201 + (int64_t)&g28); // 0x4c29
            return (int64_t)v234 + (int64_t)&g28;
        }
        // 0x447e
        v186 = -1.18973e+4932L;
        v187 = -1.18973e+4932L;
        v188 = v176;
        v189 = v175;
        v190 = scale_to;
        if (inval_style != 3) {
            // 0x448b
            *(float80_t *)&v4 = -1.18973e+4932L;
            function_24e0();
            *(float80_t *)(v5 - 16) = *(float80_t *)&v4;
            function_26d0();
            v186 = -1.18973e+4932L;
            v187 = -1.18973e+4932L;
            v188 = v194;
            v189 = v194;
            v190 = conv_exit_code;
        }
    }
    // 0x3be8
    function_25c0();
    v114 = v186;
    v113 = v187;
    v109 = v188;
    v112 = v189;
    v110 = v190;
    v111 = 0;
    goto lab_0x394d;
  lab_0x3b0d:;
    int64_t v235 = v236;
    int64_t v237 = v238;
    float80_t v239 = v240;
    float80_t v241 = v242;
    int64_t v243; // 0x38e0
    float80_t v244; // 0x38e0
    if ((char)v237 != 0) {
        // 0x422d
        v10 = (int64_t)g12;
        function_2740();
        float80_t v245 = v241 * v239; // 0x426c
        v21 = v245;
        v244 = v245;
        v243 = v246 & 0xffffffff;
        if (*(char *)&dev_debug != 0) {
            struct _IO_FILE * v247 = g12; // 0x4285
            v10 = (int64_t)v247;
            function_2740();
            v244 = v245;
            int64_t v248; // 0x4239
            v243 = v248;
        }
    } else {
        float80_t v249 = v241 * v239; // 0x3b15
        v21 = v249;
        v244 = v249;
        v243 = v237;
    }
    int64_t v250 = v243;
    float80_t v251 = v244;
    if (*(char *)v235 == 0) {
        // 0x3ee0
        v168 = v251;
        v169 = v251;
        v170 = v252;
        v171 = v250;
        v172 = v253;
        v173 = v235;
        if (!((v253 != 1 | *(char *)&debug == 0))) {
            float80_t * v254 = (float80_t *)(v5 + 32); // 0x3ef9
            *v254 = v251;
            char * v255 = quote(v119); // 0x3efd
            function_24e0();
            v10 = 0;
            function_26d0();
            v168 = *v254;
            v169 = v251;
            v170 = v252;
            v171 = (int64_t)v255;
            v172 = v253;
            v173 = v235;
        }
    } else {
        // 0x3b27
        v168 = v251;
        v169 = v251;
        v170 = v252;
        v171 = v250;
        v172 = 5;
        v173 = v235;
        if (inval_style != 3) {
            float80_t * v256 = (float80_t *)(v5 + 32); // 0x3b3d
            *v256 = v251;
            int64_t v257 = quote_n(); // 0x3b46
            int64_t v258 = quote_n(); // 0x3b53
            function_24e0();
            v10 = conv_exit_code;
            function_26d0();
            v168 = *v256;
            v169 = v251;
            v170 = v252;
            v171 = v258;
            v172 = 5;
            v173 = v257;
        }
    }
    goto lab_0x3b90;
  lab_0x3d75:
    // 0x3d75
    v259 = (int64_t)*(char *)&dev_debug;
    v260 = v261;
    v262 = v125;
    goto lab_0x3d85;
  lab_0x3d85:;
    int64_t v263 = v262;
    int64_t v264 = v260;
    int64_t v265 = v259;
    v242 = 1.0L;
    v240 = v266;
    v238 = v265;
    v236 = v264;
    v246 = v263;
    if (v267 != 0) {
        int32_t v268 = v267 - 1; // 0x3d93
        v242 = v269;
        v240 = v269;
        v238 = v265;
        v236 = v264;
        v246 = v263;
        float80_t v270 = v269; // 0x3d96
        int32_t v271 = v268; // 0x3d96
        if (v268 != 0) {
            float80_t v272 = v266 * v270; // 0x3da0
            int32_t v273 = v271 - 1; // 0x3da2
            v242 = v272;
            v240 = v272;
            v238 = v265;
            v236 = v264;
            v246 = v263;
            v270 = v272;
            v271 = v273;
            while (v273 != 0) {
                // 0x3da0
                v272 = v266 * v270;
                v273 = v271 - 1;
                v242 = v272;
                v240 = v272;
                v238 = v265;
                v236 = v264;
                v246 = v263;
                v270 = v272;
                v271 = v273;
            }
        }
    }
    goto lab_0x3b0d;
}