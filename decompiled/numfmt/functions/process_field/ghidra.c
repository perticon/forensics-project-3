undefined8 process_field(long *param_1,ulong param_2)

{
  bool bVar1;
  undefined4 uVar2;
  int iVar3;
  uint uVar4;
  int iVar5;
  uint uVar6;
  int iVar7;
  int iVar8;
  int iVar9;
  ulong *puVar10;
  size_t sVar11;
  size_t sVar12;
  ushort **ppuVar13;
  undefined8 uVar14;
  undefined8 uVar15;
  undefined8 uVar16;
  ulong uVar17;
  char *pcVar18;
  byte bVar19;
  ulong uVar20;
  int iVar21;
  char *__s2;
  float *pfVar22;
  float10 *pfVar23;
  long *plVar24;
  undefined4 *puVar25;
  long *plVar26;
  uint uVar27;
  long in_FS_OFFSET;
  undefined2 in_FPUControlWord;
  float10 in_ST0;
  float10 fVar28;
  float10 fVar29;
  float10 fVar30;
  float10 fVar31;
  unkbyte10 Var32;
  unkbyte10 Var33;
  float10 in_ST1;
  float10 fVar34;
  float10 fVar35;
  float10 fVar36;
  float10 in_ST2;
  float10 in_ST3;
  float10 in_ST4;
  long lVar37;
  long alStack448 [3];
  char *local_1a8;
  long *plStack416;
  undefined8 local_198 [2];
  char *local_188 [2];
  undefined local_178 [8];
  undefined2 uStack368;
  undefined local_168 [16];
  char *local_158;
  ushort local_14c [9];
  char local_13a;
  char local_139;
  char *local_138;
  char *local_130;
  float10 local_128;
  float10 local_118;
  undefined local_108 [200];
  undefined8 local_40;
  
  pcVar18 = suffix;
  pfVar23 = (float10 *)local_198;
  plVar24 = local_198;
  pfVar22 = (float *)local_198;
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  puVar10 = frp;
  if (frp == (ulong *)0x0) {
    if (param_2 == 1) {
LAB_00103986:
      if (suffix != (char *)0x0) {
        sVar11 = strlen((char *)param_1);
        sVar12 = strlen(pcVar18);
        if (sVar12 < sVar11) {
          __s2 = (char *)((sVar11 - sVar12) + (long)param_1);
          iVar3 = strcmp(pcVar18,__s2);
          bVar19 = dev_debug;
          if (iVar3 == 0) {
            *__s2 = '\0';
            if (bVar19 != 0) {
              uVar14 = quote(pcVar18);
              __fprintf_chk(stderr,1,"trimming suffix %s\n",uVar14);
            }
          }
          else if (dev_debug != 0) {
            fwrite("no valid suffix found\n",1,0x16,stderr);
          }
        }
      }
      bVar19 = *(byte *)param_1;
      plVar26 = param_1;
      if (bVar19 == 0) {
        iVar3 = 0;
      }
      else {
        ppuVar13 = __ctype_b_loc();
        do {
          if ((*(byte *)(*ppuVar13 + bVar19) & 1) == 0) break;
          bVar19 = *(byte *)((long)plVar26 + 1);
          plVar26 = (long *)((long)plVar26 + 1);
        } while (bVar19 != 0);
        iVar3 = (int)param_1 - (int)plVar26;
      }
      if (auto_padding == 0) {
LAB_00103c20:
        iVar3 = scale_from;
        local_138 = (char *)0x0;
        local_198[0]._0_4_ = 0.0;
        local_128 = (float10)0.0;
        uVar27 = (-(uint)(scale_from - 3U < 2) & 0x18) + 1000;
        if (dev_debug != 0) {
          uVar14 = quote_n(1,decimal_point);
          uVar15 = quote_n(0,plVar26);
          __fprintf_chk(stderr,1,
                        "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n"
                        ,uVar15,uVar14,0x12);
        }
      }
      else {
        if ((iVar3 == 0) && ((long)param_2 < 2)) {
          padding_width = 0;
        }
        else {
          padding_width = strlen((char *)param_1);
          if (padding_buffer_size <= padding_width) {
            padding_buffer_size = padding_width + 1;
            padding_buffer = (char *)xrealloc(padding_buffer);
          }
        }
        if (dev_debug != 0) {
          __fprintf_chk(stderr,1,"setting Auto-Padding to %ld characters\n",padding_width);
          goto LAB_00103c20;
        }
        local_138 = (char *)0x0;
        local_198[0]._0_4_ = 0.0;
        local_128 = (float10)0.0;
        uVar27 = (-(uint)(scale_from - 3U < 2) & 0x18) + 1000;
        iVar3 = scale_from;
      }
      uVar4 = simple_strtod_int(plVar26,&local_138,&local_128);
      pcVar18 = local_138;
      uVar6 = uVar4;
      if (uVar4 < 2) {
        _local_178 = (float10)CONCAT28(uStack368,(long)decimal_point_length);
        iVar5 = strncmp(local_138,decimal_point,(long)decimal_point_length);
        local_188[0] = (char *)0x0;
        if (iVar5 == 0) {
          _local_178 = local_128;
          local_118 = (float10)(float)local_198[0];
          local_188[0] = pcVar18 + local_178;
          local_138 = local_188[0];
          uVar6 = simple_strtod_int(local_188[0],&local_130,&local_118);
          pcVar18 = local_188[0];
          if (1 < uVar6) goto LAB_00103f38;
          if (uVar6 == 1) {
            uVar4 = 1;
          }
          if (local_139 == '\0') {
            local_188[0] = (char *)((long)local_130 - (long)local_188[0]);
            local_128 = local_118;
            if ((int)local_130 != (int)pcVar18) {
              iVar5 = (int)local_188[0] + -1;
              if (iVar5 == 0) {
                local_128 = local_118 / (float10)_DAT_0010cdb4;
              }
              else {
                local_128 = (float10)_DAT_0010cdb4;
                do {
                  local_128 = local_128 * (float10)_DAT_0010cdb4;
                  iVar5 = iVar5 + -1;
                } while (iVar5 != 0);
                local_128 = local_118 / local_128;
                in_ST4 = in_ST3;
              }
            }
            if (local_13a == '\0') {
              local_128 = local_128 + _local_178;
            }
            else {
              local_128 = _local_178 - local_128;
            }
            local_138 = local_130;
            pcVar18 = local_130;
            goto LAB_00103ada;
          }
          goto LAB_00104332;
        }
LAB_00103ada:
        fVar34 = local_128;
        if (dev_debug != 0) {
          _local_178 = local_128;
          __fprintf_chk(stderr,1,"  parsed numeric value: %Lf\n  input precision = %d\n");
          fVar34 = _local_178;
        }
        _local_178 = (float10)CONCAT91(stack0xfffffffffffffe89,*pcVar18);
        if (*pcVar18 == '\0') {
          if (iVar3 != 4) {
            pfVar22 = (float *)local_198;
            fVar28 = (float10)1;
            bVar19 = dev_debug;
            goto LAB_00103b0d;
          }
          uVar4 = 6;
          pcVar18 = "missing \'i\' suffix in input: %s (e.g Ki/Mi/Gi)";
        }
        else {
          local_168._0_10_ = fVar34;
          ppuVar13 = __ctype_b_loc();
          bVar1 = false;
          local_158 = pcVar18;
          while ((*(byte *)(*ppuVar13 + local_178[0]) & 1) != 0) {
            local_178[0] = local_158[1];
            bVar1 = true;
            local_158 = local_158 + 1;
          }
          if (bVar1) {
            local_138 = local_158;
          }
          _local_178 = local_168._0_10_;
          local_168._0_10_ = (float10)CONCAT91(local_168._1_9_,local_178[0]);
          pcVar18 = strchr("KMGTPEZY",(int)(char)local_178[0]);
          fVar34 = _local_178;
          if (pcVar18 == (char *)0x0) {
            uVar4 = 5;
            pcVar18 = "invalid suffix in input: %s";
          }
          else {
            if (iVar3 != 0) {
              local_188[0]._0_4_ = 0;
              if ((byte)(local_168[0] + 0xbbU) < 0x16) {
                local_188[0]._0_4_ = *(uint *)(CSWTCH_313 + (ulong)(byte)(local_168[0] + 0xbbU) * 4)
                ;
              }
              pcVar18 = local_158 + 1;
              local_138 = pcVar18;
              if (iVar3 == 1) {
                if (local_158[1] == 'i') {
                  pcVar18 = local_158 + 2;
                  local_138 = pcVar18;
                  if (dev_debug == 0) {
                    fVar28 = (float10)_DAT_0010cdb8;
                  }
                  else {
                    local_188[0] = (char *)((ulong)local_188[0] & 0xffffffff00000000 |
                                           (ulong)(uint)local_188[0]);
                    __fprintf_chk(stderr,1,"  Auto-scaling, found \'i\', switching to base %d\n");
                    fVar28 = (float10)_DAT_0010cdb8;
                  }
                }
                else {
                  local_188[0] = (char *)((ulong)local_188[0] & 0xffffffff00000000 | (ulong)uVar27);
                  fVar28 = (float10)uVar27;
                }
                goto LAB_00103d85;
              }
              if (iVar3 == 4) {
                if (local_158[1] != 'i') {
                  local_188[0] = (char *)0x0;
                  uVar4 = 6;
                  pcVar18 = "missing \'i\' suffix in input: %s (e.g Ki/Mi/Gi)";
                  goto LAB_00103f70;
                }
                pcVar18 = local_158 + 2;
                local_138 = pcVar18;
              }
              local_188[0] = (char *)((ulong)local_188[0] & 0xffffffff00000000 | (ulong)uVar27);
              fVar28 = (float10)uVar27;
              local_138 = pcVar18;
LAB_00103d85:
              fVar29 = _local_178;
              bVar19 = dev_debug;
              if ((uint)local_188[0] == 0) goto LAB_00104f87;
              iVar3 = (uint)local_188[0] - 1;
              fVar29 = fVar28;
              fVar34 = _local_178;
              if (iVar3 == 0) {
                local_188[0] = (char *)0x0;
                goto LAB_00103b0d;
              }
              do {
                fVar29 = fVar29 * fVar28;
                iVar3 = iVar3 + -1;
              } while (iVar3 != 0);
              local_188[0] = (char *)0x0;
              pfVar22 = (float *)local_198;
              fVar28 = fVar29;
              goto LAB_00103b0d;
            }
            uVar4 = 4;
            pcVar18 = "rejecting suffix in input: %s (consider using --from)";
          }
        }
      }
      else {
LAB_00103f38:
        uVar4 = uVar6;
        fVar34 = local_128;
        switch(uVar4) {
        case 0:
        case 1:
        default:
          local_188[0] = (char *)0x0;
          pcVar18 = (char *)0x0;
          break;
        case 2:
          local_188[0] = (char *)0x0;
          pcVar18 = "value too large to be converted: %s";
          break;
        case 3:
LAB_00104332:
          local_188[0] = (char *)0x0;
          uVar4 = 3;
          pcVar18 = "invalid number: %s";
          fVar34 = local_128;
          break;
        case 4:
          local_188[0] = (char *)0x0;
          pcVar18 = "rejecting suffix in input: %s (consider using --from)";
          break;
        case 5:
          local_188[0] = (char *)0x0;
          pcVar18 = "invalid suffix in input: %s";
          break;
        case 6:
          local_188[0] = (char *)0x0;
          pcVar18 = "missing \'i\' suffix in input: %s (e.g Ki/Mi/Gi)";
        }
      }
LAB_00103f70:
      pfVar23 = (float10 *)local_198;
      if (inval_style != 3) {
        _local_178 = fVar34;
        quote();
        uVar14 = dcgettext(0,pcVar18,5);
        error(conv_exit_code,0,uVar14);
        pfVar23 = (float10 *)local_198;
        fVar34 = _local_178;
      }
LAB_00103b90:
      iVar9 = scale_to;
      iVar5 = grouping;
      iVar3 = round_style;
      uVar2 = conv_exit_code;
      if ((from_unit_size != 1) || (to_unit_size != 1)) {
        *(long *)((long)pfVar23 + 0x20) = from_unit_size;
        fVar28 = (float10)*(long *)((long)pfVar23 + 0x20);
        if (from_unit_size < 0) {
          fVar28 = fVar28 + (float10)_DAT_0010cdbc;
        }
        *(long *)((long)pfVar23 + 0x20) = to_unit_size;
        fVar29 = (float10)*(long *)((long)pfVar23 + 0x20);
        if (to_unit_size < 0) {
          fVar29 = fVar29 + (float10)_DAT_0010cdbc;
        }
        fVar34 = (fVar28 * fVar34) / fVar29;
      }
      if (1 < uVar4) {
LAB_00103be8:
        uVar14 = 0;
        *(undefined8 *)((long)pfVar23 + -8) = 0x103bfa;
        fputs_unlocked((char *)param_1,stdout);
        goto LAB_0010394d;
      }
      uVar20 = user_precision;
      if (user_precision == 0xffffffffffffffff) {
        uVar20 = *(ulong *)((long)pfVar23 + 0x10);
      }
      *(ulong *)((long)pfVar23 + 0x10) = uVar20;
      if ((_DAT_0010cde0 <= fVar34) && (fVar34 <= _DAT_0010cdf0)) {
        uVar27 = 0;
        fVar28 = fVar34;
        while( true ) {
          fVar29 = -fVar28;
          if ((float10)0 <= fVar28) {
            fVar29 = fVar28;
          }
          if (fVar29 < (float10)_DAT_0010cdb4) break;
          fVar28 = (float10)_DAT_0010cdb4 / fVar28;
          uVar27 = uVar27 + 1;
        }
        if ((scale_to == 0) && (0x12 < (ulong)uVar27 + *(long *)((long)pfVar23 + 0x10)))
        goto LAB_0010447e;
        in_ST3 = in_ST2;
        in_ST4 = in_ST2;
        if (uVar27 < 0x1b) goto LAB_001040b6;
        *pfVar23 = fVar34;
        if (inval_style != 3) {
          *(undefined8 *)((long)pfVar23 + -8) = 0x10407a;
          uVar14 = dcgettext(0,
                             "value too large to be printed: \'%Lg\' (cannot handle values > 999Y)",
                             5);
          *(float10 *)((long)pfVar23 + -0x10) = *pfVar23;
          *(undefined8 *)((long)pfVar23 + -0x18) = 0x104096;
          error(conv_exit_code,0,uVar14);
        }
        goto LAB_00103be8;
      }
      if ((scale_to == 0) && (0x12 < *(ulong *)((long)pfVar23 + 0x10))) {
LAB_0010447e:
        if (inval_style != 3) {
          lVar37 = *(long *)((long)pfVar23 + 0x10);
          *pfVar23 = fVar34;
          if (lVar37 == 0) {
            *(undefined8 *)((long)pfVar23 + -8) = 0x104e03;
            uVar14 = dcgettext(0,"value too large to be printed: \'%Lg\' (consider using --to)",5);
            *(float10 *)((long)pfVar23 + -0x10) = *pfVar23;
            *(undefined8 *)((long)pfVar23 + -0x18) = 0x104e1b;
            error(uVar2,0,uVar14);
          }
          else {
            *(undefined8 *)((long)pfVar23 + -8) = 0x1044b5;
            uVar14 = dcgettext(0,
                               "value/precision too large to be printed: \'%Lg/%lu\' (consider using --to)"
                               ,5);
            *(float10 *)((long)pfVar23 + -0x10) = *pfVar23;
            *(undefined8 *)((long)pfVar23 + -0x18) = 0x1044d0;
            error(uVar2,0,uVar14,lVar37);
          }
        }
        goto LAB_00103be8;
      }
LAB_001040b6:
      *(undefined *)((long)pfVar23 + 0x90) = 0x25;
      puVar25 = (undefined4 *)((long)pfVar23 + 0x91);
      if (grouping != 0) {
        *(undefined *)((long)pfVar23 + 0x91) = 0x27;
        puVar25 = (undefined4 *)((long)pfVar23 + 0x92);
      }
      if (zero_padding_width != 0) {
        *(float10 *)((long)pfVar23 + 0x20) = fVar34;
        *(undefined8 *)((long)pfVar23 + -8) = 0x104470;
        iVar8 = __snprintf_chk(puVar25,0x3e,1,0x3f,&DAT_0010b0d7);
        fVar34 = *(float10 *)((long)pfVar23 + 0x20);
        puVar25 = (undefined4 *)((long)puVar25 + (long)iVar8);
      }
      if (dev_debug != 0) {
        *(float10 *)((long)pfVar23 + 0x20) = fVar34;
        *(undefined8 *)((long)pfVar23 + -8) = 0x10443c;
        fwrite("double_to_human:\n",1,0x11,stderr);
        fVar34 = *(float10 *)((long)pfVar23 + 0x20);
      }
      if (iVar9 != 0) {
        if (iVar9 - 3U < 2) {
          *(undefined4 *)((long)pfVar23 + 0x10) = 0x400;
          lVar37 = DAT_0010cdc8;
        }
        else {
          *(undefined4 *)((long)pfVar23 + 0x10) = 1000;
          lVar37 = DAT_0010cdd0;
        }
        fVar28 = fVar34;
        if ((fVar34 < _DAT_0010cde0) || (_DAT_0010cdf0 < fVar34)) {
          iVar5 = 0;
        }
        else {
          iVar5 = 0;
          while( true ) {
            fVar29 = -fVar28;
            if ((float10)0 <= fVar28) {
              fVar29 = fVar28;
            }
            in_ST3 = in_ST2;
            if (fVar29 < (float10)*(int *)((long)pfVar23 + 0x10)) break;
            fVar28 = fVar28 / (float10)*(int *)((long)pfVar23 + 0x10);
            iVar5 = iVar5 + 1;
          }
        }
        if (dev_debug != 0) {
          *(float10 *)((long)pfVar23 + 0x30) = fVar34;
          *(long *)((long)pfVar23 + 0x10) = lVar37;
          *(float10 *)((long)pfVar23 + -0x10) = fVar28;
          *(float10 *)((long)pfVar23 + 0x20) = fVar28;
          *(undefined8 *)((long)pfVar23 + -0x18) = 0x104c6b;
          __fprintf_chk(stderr,1,"  scaled value to %Lf * %0.f ^ %u\n");
          lVar37 = *(long *)((long)pfVar23 + 0x10);
          fVar34 = *(float10 *)((long)pfVar23 + 0x30);
          fVar28 = *(float10 *)((long)pfVar23 + 0x20);
        }
        uVar20 = (ulong)dev_debug;
        bVar19 = dev_debug;
        if (user_precision != 0xffffffffffffffff) {
          uVar17 = (ulong)(uint)(iVar5 * 3);
          if ((long)user_precision < (long)(ulong)(uint)(iVar5 * 3)) {
            uVar17 = user_precision;
          }
          iVar8 = (int)uVar17;
          if (iVar8 == 0) goto LAB_00104b56;
          iVar7 = iVar8 + -1;
          if (iVar7 == 0) goto LAB_00104be4;
          fVar29 = (float10)_DAT_0010cdb4;
          iVar21 = iVar7;
          do {
            fVar29 = fVar29 * (float10)_DAT_0010cdb4;
            iVar21 = iVar21 + -1;
          } while (iVar21 != 0);
          *(undefined2 *)((long)pfVar23 + 0x4e) = in_FPUControlWord;
          *(ushort *)((long)pfVar23 + 0x4c) =
               *(ushort *)((long)pfVar23 + 0x4e) & 0xff |
               (ushort)(byte)((ulong)*(ushort *)((long)pfVar23 + 0x4e) >> 8) << 8 | 0xc00;
          *(long *)((long)pfVar23 + 0x10) = (long)ROUND((fVar29 * fVar28) / _DAT_0010ce00);
          fVar35 = (float10)*(long *)((long)pfVar23 + 0x10) * _DAT_0010ce00;
          fVar28 = fVar29 * fVar28 - fVar35;
          switch(iVar3) {
          case 0:
            in_ST3 = in_ST2;
            goto LAB_001044f0;
          case 1:
            in_ST3 = in_ST2;
            goto LAB_00104875;
          case 2:
            in_ST3 = in_ST2;
            goto LAB_001044de;
          case 3:
            *(long *)((long)pfVar23 + 0x10) = (long)ROUND(fVar28);
            fVar29 = (float10)*(long *)((long)pfVar23 + 0x10) + fVar35;
            in_ST3 = in_ST2;
            break;
          case 4:
            in_ST3 = in_ST2;
            goto LAB_001048b3;
          default:
            fVar29 = (float10)0;
            in_ST3 = in_ST2;
          }
          goto LAB_00104533;
        }
        fVar29 = -fVar28;
        if ((float10)0 <= fVar28) {
          fVar29 = fVar28;
        }
        in_ST3 = in_ST2;
        if ((float10)_DAT_0010cdb4 <= fVar29) {
LAB_00104b56:
          *(undefined2 *)((long)pfVar23 + 0x4e) = in_FPUControlWord;
          *(ushort *)((long)pfVar23 + 0x4c) =
               *(ushort *)((long)pfVar23 + 0x4e) & 0xff |
               (ushort)(byte)((ulong)*(ushort *)((long)pfVar23 + 0x4e) >> 8) << 8 | 0xc00;
          *(long *)((long)pfVar23 + 0x10) = (long)ROUND(fVar28 / _DAT_0010ce00);
          fVar35 = (float10)*(long *)((long)pfVar23 + 0x10) * _DAT_0010ce00;
          fVar28 = fVar28 - fVar35;
          switch(iVar3) {
          case 0:
            iVar8 = iVar3;
            goto LAB_001044f0;
          case 1:
            iVar8 = 0;
            goto LAB_00104875;
          case 2:
            iVar8 = 0;
            goto LAB_001044de;
          case 3:
            *(long *)((long)pfVar23 + 0x10) = (long)ROUND(fVar28);
            fVar29 = (float10)*(long *)((long)pfVar23 + 0x10) + fVar35;
            break;
          case 4:
            iVar8 = 0;
            goto LAB_001048b3;
          default:
            fVar29 = (float10)0;
            goto LAB_0010454d;
          }
          goto LAB_00104543;
        }
LAB_00104be4:
        fVar30 = (float10)_DAT_0010cdb4;
        *(undefined2 *)((long)pfVar23 + 0x4e) = in_FPUControlWord;
        *(ushort *)((long)pfVar23 + 0x4c) =
             *(ushort *)((long)pfVar23 + 0x4e) & 0xff |
             (ushort)(byte)((ulong)*(ushort *)((long)pfVar23 + 0x4e) >> 8) << 8 | 0xc00;
        *(long *)((long)pfVar23 + 0x10) = (long)ROUND((fVar28 * fVar30) / _DAT_0010ce00);
        fVar35 = (float10)*(long *)((long)pfVar23 + 0x10) * _DAT_0010ce00;
        fVar28 = fVar28 * fVar30 - fVar35;
        in_ST3 = in_ST2;
        switch(iVar3) {
        case 0:
          iVar8 = 1;
          goto LAB_001044f0;
        case 1:
          iVar8 = iVar3;
LAB_00104875:
          *(long *)((long)pfVar23 + 0x10) = (long)ROUND(-fVar28);
          *(ulong *)((long)pfVar23 + 0x10) =
               -(*(long *)((long)pfVar23 + 0x10) +
                (ulong)((float10)*(long *)((long)pfVar23 + 0x10) < -fVar28));
          fVar29 = (float10)*(long *)((long)pfVar23 + 0x10);
          break;
        case 2:
          iVar8 = 1;
LAB_001044de:
          if (fVar28 < (float10)0) goto LAB_00104875;
LAB_001044f0:
          *(long *)((long)pfVar23 + 0x10) = (long)ROUND(fVar28);
          fVar29 = (float10)*(long *)((long)pfVar23 + 0x10);
          if (fVar29 < fVar28) {
            *(long *)((long)pfVar23 + 0x10) = *(long *)((long)pfVar23 + 0x10) + 1;
            fVar29 = (float10)*(long *)((long)pfVar23 + 0x10);
          }
          break;
        case 3:
          goto switchD_00104c30_caseD_3;
        case 4:
          iVar8 = 1;
LAB_001048b3:
          if (fVar28 < (float10)0) {
            *(long *)((long)pfVar23 + 0x10) = (long)ROUND(fVar28 - (float10)_DAT_0010cdc0);
            fVar29 = (float10)*(long *)((long)pfVar23 + 0x10);
          }
          else {
            *(long *)((long)pfVar23 + 0x10) = (long)ROUND(fVar28 + (float10)_DAT_0010cdc0);
            fVar29 = (float10)*(long *)((long)pfVar23 + 0x10);
          }
          break;
        default:
          fVar29 = (float10)0;
          goto LAB_0010454d;
        }
        fVar29 = fVar29 + fVar35;
        if (iVar8 != 0) {
          fVar30 = (float10)_DAT_0010cdb4;
          iVar7 = iVar8 + -1;
          if (iVar7 == 0) goto LAB_00104a91;
LAB_00104533:
          fVar28 = (float10)_DAT_0010cdb4;
          do {
            fVar28 = fVar28 * (float10)_DAT_0010cdb4;
            iVar7 = iVar7 + -1;
          } while (iVar7 != 0);
          fVar29 = fVar29 / fVar28;
        }
LAB_00104543:
        do {
          bVar19 = (byte)uVar20;
          if (fVar29 < (float10)0) {
            *(long *)((long)pfVar23 + 0x10) = lVar37;
            fVar31 = (float10)*(double *)((long)pfVar23 + 0x10);
            fVar36 = -fVar29;
            fVar35 = in_ST0;
            fVar30 = in_ST1;
            fVar28 = in_ST2;
            if (fVar31 <= -fVar29) goto LAB_00104563;
LAB_00104b36:
            uVar27 = (uint)(iVar5 != 0 && fVar36 < (float10)_DAT_0010cdb4);
          }
          else {
LAB_0010454d:
            *(long *)((long)pfVar23 + 0x10) = lVar37;
            fVar31 = (float10)*(double *)((long)pfVar23 + 0x10);
            if (fVar31 <= fVar29) {
LAB_00104563:
              iVar5 = iVar5 + 1;
              fVar29 = fVar29 / fVar31;
            }
            uVar27 = 0;
            fVar35 = in_ST0;
            fVar30 = in_ST1;
            fVar28 = in_ST2;
            if (fVar29 != (float10)0) {
              fVar36 = fVar29;
              if (fVar29 < (float10)0) {
                fVar36 = -fVar29;
              }
              goto LAB_00104b36;
            }
          }
          in_ST0 = in_ST3;
          in_ST1 = in_ST3;
          if (bVar19 != 0) {
            *(float10 *)((long)pfVar23 + 0x20) = fVar34;
            *(uint *)((long)pfVar23 + 0x10) = uVar27;
            *(float10 *)((long)pfVar23 + -0x10) = fVar29;
            *pfVar23 = fVar29;
            *(undefined8 *)((long)pfVar23 + -0x18) = 0x104ce2;
            __fprintf_chk(stderr,1,"  after rounding, value=%Lf * %0.f ^ %u\n",iVar5);
            fVar34 = *(float10 *)((long)pfVar23 + 0x20);
            fVar29 = *pfVar23;
          }
          *puVar25 = 0x664c2a2e;
          *(undefined2 *)(puVar25 + 1) = 0x7325;
          *(undefined *)((long)puVar25 + 6) = 0;
          switch(iVar5) {
          case 0:
            *(float10 *)((long)pfVar23 + 0x10) = fVar34;
            uVar20 = 0x80;
            *(char **)((long)pfVar23 + -0x10) = "";
            *(float10 *)((long)pfVar23 + -0x20) = fVar29;
            *pfVar23 = fVar29;
            *(undefined8 *)(pfVar23 + -4) = 0x104a33;
            uVar27 = __snprintf_chk((undefined *)((long)pfVar23 + 0xd0),0x7f,1,0x80,
                                    (undefined *)((long)pfVar23 + 0x90));
            Var33 = *pfVar23;
            Var32 = *(unkbyte10 *)((long)pfVar23 + 0x10);
            fVar34 = in_ST3;
            if (uVar27 < 0x7f) goto LAB_0010494a;
            goto LAB_00104a4e;
          case 1:
            pcVar18 = "K";
            break;
          case 2:
            pcVar18 = "M";
            break;
          case 3:
            pcVar18 = "G";
            break;
          case 4:
            pcVar18 = "T";
            break;
          case 5:
            pcVar18 = "P";
            break;
          case 6:
            pcVar18 = "E";
            break;
          case 7:
            pcVar18 = "Z";
            break;
          case 8:
            pcVar18 = "Y";
            break;
          default:
            pcVar18 = "(error)";
          }
          *(float10 *)((long)pfVar23 + 0x10) = fVar34;
          uVar20 = 0x80;
          *(char **)((long)pfVar23 + -0x10) = pcVar18;
          *(float10 *)((long)pfVar23 + -0x20) = fVar29;
          *pfVar23 = fVar29;
          *(undefined8 *)(pfVar23 + -4) = 0x104922;
          uVar27 = __snprintf_chk((undefined *)((long)pfVar23 + 0xd0),0x7f,1,0x80,
                                  (undefined *)((long)pfVar23 + 0x90));
          Var33 = *pfVar23;
          Var32 = *(unkbyte10 *)((long)pfVar23 + 0x10);
          fVar34 = in_ST3;
          if (uVar27 < 0x7f) {
            if ((iVar5 != 0) && (iVar9 == 4)) {
              *pfVar23 = (float10)Var32;
              *(undefined8 *)((long)pfVar23 + -8) = 0x104e8c;
              __strncat_chk((undefined *)((long)pfVar23 + 0xd0),&DAT_0010b396,
                            0x7f - (long)(int)uVar27,0x80);
              Var32 = *pfVar23;
            }
LAB_0010494a:
            param_1 = (long *)((long)pfVar23 + 0xd0);
            if (dev_debug != 0) {
              *pfVar23 = (float10)Var32;
              *(undefined8 *)((long)pfVar23 + -8) = 0x104db8;
              uVar14 = quote(param_1);
              *(undefined8 *)((long)pfVar23 + -8) = 0x104dd5;
              __fprintf_chk(stderr,1,"  returning value: %s\n",uVar14);
              Var32 = *pfVar23;
              goto LAB_001046d6;
            }
            if (suffix != (char *)0x0) goto LAB_001046e2;
            goto LAB_00104744;
          }
LAB_00104a4e:
          puVar25 = (undefined4 *)((long)pfVar23 + 0xd0);
          *pfVar23 = (float10)Var33;
          *(undefined8 *)((long)pfVar23 + -8) = 0x104a64;
          in_ST2 = in_ST1;
          uVar14 = dcgettext(0,"failed to prepare value \'%Lf\' for printing",5);
          *(ulong *)((long)pfVar23 + -8) = uVar20;
          *(ulong *)((long)pfVar23 + -0x10) = uVar20;
          *(float10 *)((long)pfVar23 + -0x10) = *pfVar23;
          *(undefined8 *)((long)pfVar23 + -0x18) = 0x104a7d;
          in_ST3 = in_ST2;
          lVar37 = error(1,0,uVar14);
          pfVar23 = (float10 *)((long)pfVar23 + -0x10);
switchD_00104c30_caseD_3:
          *(long *)((long)pfVar23 + 0x10) = (long)ROUND(fVar28);
          fVar29 = (float10)*(long *)((long)pfVar23 + 0x10) + fVar35;
LAB_00104a91:
          fVar29 = fVar29 / fVar30;
        } while( true );
      }
      iVar9 = (int)*(long *)((long)pfVar23 + 0x10);
      if (iVar9 == 0) {
        *(undefined2 *)((long)pfVar23 + 0x4e) = in_FPUControlWord;
        *(ushort *)((long)pfVar23 + 0x4c) =
             *(ushort *)((long)pfVar23 + 0x4e) & 0xff |
             (ushort)(byte)((ulong)*(ushort *)((long)pfVar23 + 0x4e) >> 8) << 8 | 0xc00;
        *(long *)((long)pfVar23 + 0x20) = (long)ROUND(fVar34 / _DAT_0010ce00);
        in_FPUControlWord = *(undefined2 *)((long)pfVar23 + 0x4e);
        fVar29 = (float10)*(long *)((long)pfVar23 + 0x20) * _DAT_0010ce00;
        fVar28 = fVar34 - fVar29;
        switch(iVar3) {
        case 0:
          goto switchD_00104d47_caseD_0;
        case 1:
          goto switchD_00104d47_caseD_1;
        case 2:
          goto switchD_001043e4_caseD_2;
        case 3:
          goto switchD_00104d47_caseD_3;
        case 4:
          goto switchD_001043e4_caseD_4;
        default:
switchD_00104d47_caseD_5:
          fVar35 = (float10)0;
        }
        goto LAB_0010461b;
      }
      iVar8 = iVar9 + -1;
      if (iVar8 == 0) {
        *(undefined2 *)((long)pfVar23 + 0x4e) = in_FPUControlWord;
        *(ushort *)((long)pfVar23 + 0x4c) =
             *(ushort *)((long)pfVar23 + 0x4e) & 0xff |
             (ushort)(byte)((ulong)*(ushort *)((long)pfVar23 + 0x4e) >> 8) << 8 | 0xc00;
        *(long *)((long)pfVar23 + 0x20) =
             (long)ROUND((fVar34 * (float10)_DAT_0010cdb4) / _DAT_0010ce00);
        in_FPUControlWord = *(undefined2 *)((long)pfVar23 + 0x4e);
        fVar29 = (float10)*(long *)((long)pfVar23 + 0x20) * _DAT_0010ce00;
        fVar28 = fVar34 * (float10)_DAT_0010cdb4 - fVar29;
        in_ST4 = in_ST3;
        switch(iVar3) {
        case 0:
          goto switchD_00104d47_caseD_0;
        case 1:
          goto switchD_00104d47_caseD_1;
        case 2:
          goto switchD_001043e4_caseD_2;
        case 3:
          goto switchD_00104d47_caseD_3;
        case 4:
          goto switchD_001043e4_caseD_4;
        default:
          goto switchD_00104d47_caseD_5;
        }
      }
      fVar28 = (float10)_DAT_0010cdb4;
      do {
        fVar28 = fVar28 * (float10)_DAT_0010cdb4;
        iVar8 = iVar8 + -1;
      } while (iVar8 != 0);
      *(undefined2 *)((long)pfVar23 + 0x4e) = in_FPUControlWord;
      *(ushort *)((long)pfVar23 + 0x4c) =
           *(ushort *)((long)pfVar23 + 0x4e) & 0xff |
           (ushort)(byte)((ulong)*(ushort *)((long)pfVar23 + 0x4e) >> 8) << 8 | 0xc00;
      *(long *)((long)pfVar23 + 0x20) = (long)ROUND((fVar28 * fVar34) / _DAT_0010ce00);
      in_FPUControlWord = *(undefined2 *)((long)pfVar23 + 0x4e);
      fVar29 = (float10)*(long *)((long)pfVar23 + 0x20) * _DAT_0010ce00;
      fVar28 = fVar28 * fVar34 - fVar29;
      in_ST3 = in_ST2;
      switch(iVar3) {
      case 0:
        in_ST3 = in_ST2;
        goto switchD_00104d47_caseD_0;
      case 1:
        in_ST3 = in_ST2;
switchD_00104d47_caseD_1:
        *(long *)pfVar23 = (long)ROUND(-fVar28);
        in_FPUControlWord = *(undefined2 *)((long)pfVar23 + 0x4e);
        *(ulong *)pfVar23 = -(*(long *)pfVar23 + (ulong)((float10)*(long *)pfVar23 < -fVar28));
        fVar35 = (float10)*(long *)pfVar23;
        in_ST4 = in_ST3;
        break;
      case 2:
switchD_001043e4_caseD_2:
        if (fVar28 < (float10)0) goto switchD_00104d47_caseD_1;
switchD_00104d47_caseD_0:
        *(long *)pfVar23 = (long)ROUND(fVar28);
        in_FPUControlWord = *(undefined2 *)((long)pfVar23 + 0x4e);
        fVar35 = (float10)*(long *)pfVar23;
        in_ST4 = in_ST3;
        if (fVar35 < fVar28) {
          *(long *)pfVar23 = *(long *)pfVar23 + 1;
          fVar35 = (float10)*(long *)pfVar23;
        }
        break;
      case 3:
        in_ST3 = in_ST2;
        in_ST4 = in_ST2;
switchD_00104d47_caseD_3:
        *(long *)pfVar23 = (long)ROUND(fVar28);
        in_FPUControlWord = *(undefined2 *)((long)pfVar23 + 0x4e);
        fVar35 = (float10)*(long *)pfVar23;
        break;
      case 4:
switchD_001043e4_caseD_4:
        in_ST4 = in_ST3;
        if ((float10)0 <= fVar28) {
          fVar28 = fVar28 + (float10)_DAT_0010cdc0;
          goto switchD_00104d47_caseD_3;
        }
        *(long *)pfVar23 = (long)ROUND(fVar28 - (float10)_DAT_0010cdc0);
        in_FPUControlWord = *(undefined2 *)((long)pfVar23 + 0x4e);
        fVar35 = (float10)*(long *)pfVar23;
        break;
      default:
        fVar35 = (float10)0;
        in_ST3 = in_ST2;
        goto LAB_0010460c;
      }
      fVar35 = fVar35 + fVar29;
      if (iVar9 != 0) {
        if (iVar9 == 1) {
          fVar35 = fVar35 / (float10)_DAT_0010cdb4;
        }
        else {
LAB_0010460c:
          iVar9 = iVar9 + -1;
          fVar28 = (float10)_DAT_0010cdb4;
          do {
            fVar28 = fVar28 * (float10)_DAT_0010cdb4;
            iVar9 = iVar9 + -1;
          } while (iVar9 != 0);
          fVar35 = fVar35 / fVar28;
          in_ST4 = in_ST3;
        }
      }
LAB_0010461b:
      uVar4 = *(uint *)((long)pfVar23 + 0x10);
      uVar20 = (ulong)uVar4;
      fVar29 = in_ST1;
      in_ST0 = in_ST2;
      fVar28 = in_ST3;
      fVar30 = in_ST4;
      if (dev_debug != 0) {
        *(float10 *)((long)pfVar23 + 0x10) = fVar34;
        pcVar18 = "  no scaling, returning (grouped) value: %\'.*Lf\n";
        if (iVar5 == 0) {
          pcVar18 = "  no scaling, returning value: %.*Lf\n";
        }
        *(float10 *)((long)pfVar23 + -0x10) = fVar35;
        *pfVar23 = fVar35;
        *(undefined8 *)((long)pfVar23 + -0x18) = 0x104660;
        __fprintf_chk(stderr,1,pcVar18,uVar20);
        fVar35 = *pfVar23;
        fVar34 = *(float10 *)((long)pfVar23 + 0x10);
        fVar29 = in_ST1;
        in_ST0 = in_ST2;
        fVar28 = in_ST3;
      }
      in_ST3 = fVar30;
      in_ST2 = in_ST4;
      in_ST1 = fVar28;
      *(float10 *)((long)pfVar23 + 0x10) = fVar34;
      bVar19 = 0x80;
      *puVar25 = 0x664c2a2e;
      *(undefined *)(puVar25 + 1) = 0;
      param_1 = (long *)((long)pfVar23 + 0xd0);
      *(float10 *)((long)pfVar23 + -0x10) = fVar35;
      *pfVar23 = fVar35;
      *(undefined8 *)((long)pfVar23 + -0x18) = 0x1046ba;
      in_ST4 = in_ST3;
      uVar27 = __snprintf_chk(param_1,0x80,1,0x80,(long *)((long)pfVar23 + 0x90),uVar20);
      pcVar18 = *(char **)((long)pfVar23 + -0x10);
      plVar26 = *(long **)((long)pfVar23 + -8);
      if (0x7f < uVar27) {
        *pfVar23 = *pfVar23;
        *(undefined8 *)((long)pfVar23 + -8) = 0x104f6e;
        uVar14 = dcgettext(0,"failed to prepare value \'%Lf\' for printing",5);
        *(ulong *)((long)pfVar23 + -8) = uVar20;
        plVar24 = (long *)((long)pfVar23 + -0x10);
        *(ulong *)((long)pfVar23 + -0x10) = uVar20;
        *(float10 *)((long)pfVar23 + -0x10) = *pfVar23;
        *(undefined8 *)((long)pfVar23 + -0x18) = 0x104f87;
        error(1,0,uVar14);
LAB_00104f87:
        *(undefined8 *)((long)plVar24 + 0x10) = 0;
        pfVar22 = (float *)plVar24;
        fVar28 = (float10)1;
        fVar34 = fVar29;
LAB_00103b0d:
        if (bVar19 == 0) {
          fVar34 = fVar28 * fVar34;
          *(float10 *)((long)pfVar22 + 0x70) = fVar34;
        }
        else {
          *(float10 *)((long)pfVar22 + 0x30) = fVar34;
          *(float10 *)((long)pfVar22 + -0x10) = fVar28;
          *(float10 *)((long)pfVar22 + 0x20) = fVar28;
          *(undefined8 *)((long)pfVar22 + -0x18) = 0x10425d;
          __fprintf_chk(stderr,1,"  suffix power=%d^%d = %Lf\n");
          fVar34 = *(float10 *)((long)pfVar22 + 0x30) * *(float10 *)((long)pfVar22 + 0x20);
          *(float10 *)((long)pfVar22 + 0x70) = fVar34;
          if (dev_debug != 0) {
            *(float10 *)((long)pfVar22 + -0x10) = fVar34;
            *(float10 *)((long)pfVar22 + -0x20) = fVar34;
            *(float10 *)((long)pfVar22 + 0x20) = fVar34;
            *(undefined8 *)((long)pfVar22 + -0x28) = 0x1042ae;
            __fprintf_chk(stderr,1,"  returning value: %Lf (%LG)\n");
            fVar34 = *(float10 *)((long)pfVar22 + 0x20);
          }
        }
        pfVar23 = (float10 *)pfVar22;
        if (*pcVar18 == '\0') {
          if ((uVar4 == 1) && (debug != '\0')) {
            *(float10 *)((long)pfVar22 + 0x20) = fVar34;
            *(undefined8 *)((long)pfVar22 + -8) = 0x103f02;
            quote();
            *(undefined8 *)((long)pfVar22 + -8) = 0x103f18;
            uVar14 = dcgettext(0,"large input value %s: possible precision loss",5);
            *(undefined8 *)((long)pfVar22 + -8) = 0x103f29;
            error(0,0,uVar14);
            fVar34 = *(float10 *)((long)pfVar22 + 0x20);
          }
        }
        else {
          uVar4 = 5;
          if (inval_style != 3) {
            *(float10 *)((long)pfVar22 + 0x20) = fVar34;
            uVar4 = 5;
            *(undefined8 *)((long)pfVar22 + -8) = 0x103b4b;
            uVar14 = quote_n(1,pcVar18);
            *(undefined8 *)((long)pfVar22 + -8) = 0x103b58;
            uVar15 = quote_n(0,plVar26);
            *(undefined8 *)((long)pfVar22 + -8) = 0x103b6e;
            uVar16 = dcgettext(0,"invalid suffix in input %s: %s",5);
            *(undefined8 *)((long)pfVar22 + -8) = 0x103b86;
            error(conv_exit_code,0,uVar16,uVar15,uVar14);
            fVar34 = *(float10 *)((long)pfVar22 + 0x20);
          }
        }
        goto LAB_00103b90;
      }
      Var32 = *(unkbyte10 *)((long)pfVar23 + 0x10);
LAB_001046d6:
      bVar19 = dev_debug;
      if (suffix != (char *)0x0) {
LAB_001046e2:
        pcVar18 = suffix;
        bVar19 = dev_debug;
        *pfVar23 = (float10)Var32;
        *(undefined8 *)((long)pfVar23 + -8) = 0x1046ed;
        sVar11 = strlen((char *)param_1);
        *(undefined8 *)((long)pfVar23 + -8) = 0x104705;
        __strncat_chk(param_1,pcVar18,0x7f - sVar11,0x80);
        Var32 = *pfVar23;
      }
      if (bVar19 != 0) {
        *pfVar23 = (float10)Var32;
        *(undefined8 *)((long)pfVar23 + -8) = 0x104717;
        uVar14 = quote(param_1);
        *(float10 *)((long)pfVar23 + -0x10) = *pfVar23;
        *(undefined8 *)((long)pfVar23 + -0x18) = 0x10473e;
        __fprintf_chk(stderr,1,"formatting output:\n  value: %Lf\n  humanized: %s\n",uVar14);
      }
LAB_00104744:
      uVar20 = padding_width;
      *(undefined8 *)((long)pfVar23 + -8) = 0x104753;
      sVar11 = strlen((char *)param_1);
      if ((uVar20 == 0) || (uVar20 <= sVar11)) {
        if (padding_buffer_size <= sVar11 + 1) {
          padding_buffer_size = sVar11 + 2;
          *(undefined8 *)((long)pfVar23 + -8) = 0x104788;
          padding_buffer = (char *)xrealloc();
        }
        *(undefined8 *)((long)pfVar23 + -8) = 0x10479a;
        strcpy(padding_buffer,(char *)param_1);
      }
      else {
        *(ulong *)((long)pfVar23 + 0x80) = uVar20;
        *(undefined8 *)((long)pfVar23 + -8) = 0x104d72;
        mbsalign(param_1,padding_buffer,padding_buffer_size,(undefined *)((long)pfVar23 + 0x80),
                 padding_alignment,2);
        if (dev_debug != 0) {
          *(undefined8 *)((long)pfVar23 + -8) = 0x104d8b;
          uVar14 = quote(padding_buffer);
          *(undefined8 *)((long)pfVar23 + -8) = 0x104da8;
          __fprintf_chk(stderr,1,"  After padding: %s\n",uVar14);
        }
      }
      if (format_str_prefix != (char *)0x0) {
        *(undefined8 *)((long)pfVar23 + -8) = 0x1047b2;
        fputs_unlocked(format_str_prefix,stdout);
      }
      uVar14 = 1;
      *(undefined8 *)((long)pfVar23 + -8) = 0x1047cb;
      fputs_unlocked(padding_buffer,stdout);
      if (format_str_suffix != (char *)0x0) {
        *(undefined8 *)((long)pfVar23 + -8) = 0x1047e7;
        fputs_unlocked(format_str_suffix,stdout);
      }
      goto LAB_0010394d;
    }
  }
  else {
    for (; *puVar10 != 0xffffffffffffffff; puVar10 = puVar10 + 2) {
      if ((*puVar10 <= param_2) && (param_2 < puVar10[1] || param_2 == puVar10[1]))
      goto LAB_00103986;
    }
  }
  uVar14 = 1;
  fputs_unlocked((char *)param_1,stdout);
LAB_0010394d:
  if (*(long *)((long)pfVar23 + 0x158) == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar14;
  }
                    /* WARNING: Subroutine does not return */
  *(undefined8 *)((long)pfVar23 + -8) = 0x104f9e;
  __stack_chk_fail();
}