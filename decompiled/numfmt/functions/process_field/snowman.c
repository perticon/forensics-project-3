signed char process_field(void** rdi, void** rsi) {
    void** r12_3;
    void** rbp4;
    void* rsp5;
    void** rax6;
    void** v7;
    void** rax8;
    void** rdx9;
    void** rsi10;
    void** rdi11;
    void* rsp12;
    void** r13_13;
    void** rax14;
    void** rax15;
    signed char* rbx16;
    int32_t eax17;
    uint32_t edx18;
    void** rcx19;
    void** r8_20;
    int64_t rbx21;
    void** r13_22;
    int32_t eax23;
    void** rax24;
    void** rax25;
    int32_t edx26;
    int64_t r15_27;
    uint32_t eax28;
    void** r14_29;
    int32_t r14d30;
    int32_t r14d31;
    uint32_t r14d32;
    int32_t r14d33;
    void** rax34;
    void** rdi35;
    void** r8_36;
    void** rax37;
    int1_t cf38;
    void** rdi39;
    void** rax40;
    int1_t zf41;
    void** rcx42;
    void** rdi43;
    void** rsi44;
    void** v45;
    uint32_t eax46;
    void* rsp47;
    void** rbx48;
    void** v49;
    int64_t rax50;
    void** rdx51;
    int32_t eax52;
    uint32_t eax53;
    signed char v54;
    int1_t zf55;
    int1_t zf56;
    void** rax57;
    void** rax58;
    void** rdx59;
    void** rax60;
    int1_t cf61;
    void** v62;
    uint32_t eax63;
    int1_t cf64;
    uint32_t tmp32_65;
    void* rdx66;
    int64_t r13_67;
    void** r9_68;
    uint32_t eax69;
    void** rcx70;
    void** rax71;
    int32_t eax72;
    int32_t edx73;
    void** rdx74;
    void** rdi75;
    void** v76;
    void* rsp77;
    uint32_t eax78;
    void* rsp79;
    int1_t zf80;
    int1_t zf81;
    void** rax82;
    int1_t zf83;
    void** rax84;
    void** rax85;
    void** rdi86;
    int1_t zf87;
    void** rax88;
    void** rax89;
    uint32_t eax90;
    int1_t cf91;
    void*** rdi92;
    int1_t cf93;
    uint32_t tmp32_94;
    void** rdi95;
    void** rdi96;
    void** rsi97;
    uint1_t cf98;
    int1_t pf99;
    uint1_t zf100;
    uint1_t below_or_equal101;
    int64_t rax102;
    void** rax103;
    int64_t rax104;
    uint64_t rax105;
    uint32_t edx106;
    uint32_t ebx107;
    int1_t zf108;
    void** v109;
    void** rax110;
    int32_t eax111;
    signed char v112;
    void** rax113;
    void* rsp114;
    int32_t edi115;
    void** rsi116;
    int64_t rax117;
    void** rdx118;
    int64_t rax119;
    int32_t eax120;
    void** rdi121;
    int32_t r14d122;
    int32_t r14d123;
    uint32_t r14d124;
    int32_t r14d125;
    int64_t rax126;
    int32_t eax127;
    void** rdi128;
    void** rax129;
    void* rsp130;
    void** rdx131;
    void** rdi132;
    void** rax133;
    void* rsp134;
    uint64_t r8_135;
    int1_t zf136;
    void** rdi137;
    void** rax138;
    void** rdi139;
    void** rdi140;
    void** rdi141;
    void** rdi142;
    void* rax143;
    void** rdi144;
    uint32_t eax145;
    uint32_t tmp32_146;
    uint1_t cf147;
    void** rdi148;

    r12_3 = rdi;
    rbp4 = rsi;
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x168);
    rax6 = g28;
    v7 = rax6;
    rax8 = frp;
    if (rax8) {
        while (rdx9 = *reinterpret_cast<void***>(rax8), !reinterpret_cast<int1_t>(rdx9 == 0xffffffffffffffff)) {
            if (reinterpret_cast<unsigned char>(rbp4) < reinterpret_cast<unsigned char>(rdx9)) 
                goto addr_392b_4;
            if (reinterpret_cast<unsigned char>(rbp4) <= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax8 + 8))) 
                goto addr_3986_6;
            addr_392b_4:
            rax8 = rax8 + 16;
        }
    } else {
        if (reinterpret_cast<int1_t>(rsi == 1)) 
            goto addr_3986_6;
    }
    rsi10 = stdout;
    rdi11 = r12_3;
    *reinterpret_cast<int32_t*>(&r12_3) = 1;
    *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
    fun_25c0(rdi11, rsi10, rdx9);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    goto addr_394d_10;
    addr_3986_6:
    r13_13 = suffix;
    if (r13_13 && (rax14 = fun_2500(r12_3), rdi = r13_13, rax15 = fun_2500(rdi), rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8), reinterpret_cast<unsigned char>(rax14) > reinterpret_cast<unsigned char>(rax15))) {
        rdi = r13_13;
        rbx16 = reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax14) - reinterpret_cast<unsigned char>(rax15)) + reinterpret_cast<unsigned char>(r12_3));
        eax17 = fun_25e0(rdi, rbx16, rdx9);
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        edx18 = dev_debug;
        if (eax17) {
            if (*reinterpret_cast<signed char*>(&edx18)) {
                rcx19 = stderr;
                rdi = reinterpret_cast<void**>("no valid suffix found\n");
                fun_2730("no valid suffix found\n", 1, 22, rcx19, r8_20);
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
            }
        } else {
            *rbx16 = 0;
            if (*reinterpret_cast<signed char*>(&edx18)) {
                quote(r13_13, r13_13);
                rdi = stderr;
                fun_2740(rdi, 1, "trimming suffix %s\n", rdi, 1, "trimming suffix %s\n");
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8);
            }
        }
    }
    *reinterpret_cast<uint32_t*>(&rbx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx21) + 4) = 0;
    if (!*reinterpret_cast<signed char*>(&rbx21)) {
        r13_22 = r12_3;
        eax23 = 0;
    } else {
        rax24 = fun_2790(rdi, rdi);
        rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
        r13_22 = r12_3;
        rax25 = *reinterpret_cast<void***>(rax24);
        do {
            if (!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rax25) + reinterpret_cast<uint64_t>(rbx21 * 2))) & 1)) 
                break;
            *reinterpret_cast<uint32_t*>(&rbx21) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_22 + 1));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx21) + 4) = 0;
            ++r13_22;
        } while (*reinterpret_cast<signed char*>(&rbx21));
        eax23 = *reinterpret_cast<int32_t*>(&r12_3) - *reinterpret_cast<int32_t*>(&r13_22);
    }
    edx26 = auto_padding;
    if (!edx26) {
        addr_3c20_24:
        *reinterpret_cast<uint32_t*>(&r15_27) = scale_from;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_27) + 4) = 0;
        __asm__("pxor xmm1, xmm1");
        eax28 = dev_debug;
        __asm__("movss [rsp], xmm1");
        __asm__("fld dword [rsp]");
        __asm__("fstp tword [rsp+0x70]");
        *reinterpret_cast<uint32_t*>(&r14_29) = (r14d30 - (r14d31 + reinterpret_cast<uint1_t>(r14d32 < r14d33 + reinterpret_cast<uint1_t>(static_cast<uint32_t>(r15_27 - 3) < 2))) & 24) + 0x3e8;
        if (*reinterpret_cast<signed char*>(&eax28)) {
            rax34 = quote_n();
            quote_n();
            rdi35 = stderr;
            r8_36 = rax34;
            fun_2740(rdi35, 1, "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n", rdi35, 1, "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n");
            rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8 - 8 + 8 - 8 + 8);
        }
    } else {
        if (eax23 || reinterpret_cast<signed char>(rbp4) > reinterpret_cast<signed char>(1)) {
            rax37 = fun_2500(r12_3, r12_3);
            rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
            cf38 = reinterpret_cast<unsigned char>(rax37) < reinterpret_cast<unsigned char>(padding_buffer_size);
            padding_width = rax37;
            if (!cf38) {
                rdi39 = padding_buffer;
                padding_buffer_size = rax37 + 1;
                rax40 = xrealloc(rdi39);
                rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
                padding_buffer = rax40;
            }
        } else {
            padding_width = reinterpret_cast<void**>(0);
        }
        zf41 = dev_debug == 0;
        if (!zf41) 
            goto addr_3bff_31; else 
            goto addr_3a49_32;
    }
    addr_3a7e_33:
    rcx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 94);
    rdi43 = r13_22;
    rsi44 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp5) + 96);
    v45 = reinterpret_cast<void**>(0x3a95);
    eax46 = simple_strtod_int(rdi43, rsi44, reinterpret_cast<int64_t>(rsp5) + 0x70, rcx42, r8_36, 18);
    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    *reinterpret_cast<uint32_t*>(&rbx48) = eax46;
    if (eax46 > 1) {
        addr_3f38_34:
        __asm__("fld tword [rsp+0x70]");
        __asm__("fld st0");
        if (*reinterpret_cast<uint32_t*>(&rbx48) > 6) {
            __asm__("fstp st1");
            v49 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&rbp4) = 0;
        } else {
            *reinterpret_cast<uint32_t*>(&rax50) = *reinterpret_cast<uint32_t*>(&rbx48);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax50) + 4) = 0;
            goto *reinterpret_cast<int32_t*>(0xcba0 + rax50 * 4) + 0xcba0;
        }
    } else {
        rbp4 = reinterpret_cast<void**>(0);
        rdx51 = reinterpret_cast<void**>(static_cast<int64_t>(decimal_point_length));
        rsi44 = decimal_point;
        rdi43 = reinterpret_cast<void**>(0);
        v45 = reinterpret_cast<void**>(0x3ac0);
        eax52 = fun_2430(0, rsi44, rdx51);
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
        __asm__("fld tword [rsp+0x70]");
        v49 = reinterpret_cast<void**>(0);
        if (!eax52) {
            __asm__("fstp tword [rsp+0x20]");
            __asm__("fld dword [rsp]");
            rdi43 = reinterpret_cast<void**>(static_cast<uint32_t>(reinterpret_cast<unsigned char>(rdx51)));
            rcx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 95);
            rsi44 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0x68);
            __asm__("fstp tword [rsp+0x80]");
            v45 = reinterpret_cast<void**>(0x3e22);
            eax53 = simple_strtod_int(rdi43, rsi44, reinterpret_cast<int64_t>(rsp47) + 0x80, rcx42, r8_36, 18);
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            if (eax53 > 1) {
                *reinterpret_cast<uint32_t*>(&rbx48) = eax53;
                goto addr_3f38_34;
            } else {
                __asm__("fld tword [rsp+0x20]");
                rdi43 = rdi43;
                if (eax53 == 1) {
                    *reinterpret_cast<uint32_t*>(&rbx48) = 1;
                }
                if (v54) 
                    goto addr_4328_44; else 
                    goto addr_3e47_45;
            }
        } else {
            addr_3ada_46:
            zf55 = dev_debug == 0;
            if (!zf55) {
                rdi43 = stderr;
                __asm__("fld st0");
                __asm__("fstp tword [rsp]");
                *reinterpret_cast<int32_t*>(&rsi44) = 1;
                *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
                __asm__("fstp tword [rsp+0x30]");
                fun_2740(rdi43, 1, "  parsed numeric value: %Lf\n  input precision = %d\n", rdi43, 1, "  parsed numeric value: %Lf\n  input precision = %d\n");
                rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
                __asm__("fld tword [rsp+0x20]");
                goto addr_3ae7_48;
            }
        }
    }
    addr_3f70_49:
    zf56 = reinterpret_cast<int1_t>(inval_style == 3);
    if (!zf56) {
        __asm__("fstp tword [rsp+0x20]");
        rax57 = quote(r13_22, r13_22);
        fun_24e0();
        *reinterpret_cast<uint32_t*>(&rdi43) = conv_exit_code;
        *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
        rcx42 = rax57;
        *reinterpret_cast<int32_t*>(&rsi44) = 0;
        *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
        v45 = reinterpret_cast<void**>(0x3fb0);
        fun_26d0();
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 8 + 8 - 8 + 8);
        __asm__("fld tword [rsp+0x20]");
    }
    while (1) {
        rax58 = from_unit_size;
        rdx59 = to_unit_size;
        if (rax58 != 1 || !reinterpret_cast<int1_t>(rdx59 == 1)) {
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<signed char>(rax58) < reinterpret_cast<signed char>(0)) {
                __asm__("fadd dword [rip+0x9200]");
            }
            __asm__("fmulp st1, st0");
            __asm__("fild qword [rsp+0x20]");
            if (reinterpret_cast<signed char>(rdx59) < reinterpret_cast<signed char>(0)) {
                __asm__("fadd dword [rip+0x9106]");
            }
            __asm__("fdivp st1, st0");
        }
        if (*reinterpret_cast<uint32_t*>(&rbx48) > 1) 
            break;
        rax60 = user_precision;
        __asm__("fld tword [rip+0x8e03]");
        __asm__("fxch st0, st1");
        cf61 = reinterpret_cast<unsigned char>(rax60) < reinterpret_cast<unsigned char>(0xffffffffffffffff);
        if (rax60 == 0xffffffffffffffff) {
            rax60 = v49;
        }
        __asm__("fcomi st0, st1");
        __asm__("fstp st1");
        v62 = rax60;
        if (cf61 || cf61) {
            *reinterpret_cast<uint32_t*>(&rbp4) = scale_to;
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rbp4)) 
                goto addr_40b6_63;
            if (reinterpret_cast<unsigned char>(v62) > reinterpret_cast<unsigned char>(18)) 
                goto addr_447e_65;
        } else {
            __asm__("fld st0");
            eax63 = 0;
            cf64 = 0;
            __asm__("fld dword [rip+0x8da4]");
            while (!cf64) {
                __asm__("fdivp st2, st0");
                tmp32_65 = eax63 + 1;
                cf64 = tmp32_65 < eax63;
                eax63 = tmp32_65;
            }
            __asm__("fstp st0");
            __asm__("fstp st0");
            __asm__("fstp st0");
            *reinterpret_cast<uint32_t*>(&rbp4) = scale_to;
            *reinterpret_cast<int32_t*>(&rbp4 + 4) = 0;
            if (*reinterpret_cast<uint32_t*>(&rbp4)) 
                goto addr_4052_70;
            *reinterpret_cast<uint32_t*>(&rdx66) = eax63;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx66) + 4) = 0;
            rdx59 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rdx66) + reinterpret_cast<unsigned char>(v62));
            if (reinterpret_cast<unsigned char>(rdx59) > reinterpret_cast<unsigned char>(18)) 
                goto addr_447e_65;
            addr_4052_70:
            if (eax63 > 26) 
                goto addr_4057_72;
        }
        addr_40b6_63:
        *reinterpret_cast<uint32_t*>(&r14_29) = grouping;
        r12_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0x91);
        *reinterpret_cast<uint32_t*>(&r13_67) = round_style;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r13_67) + 4) = 0;
        if (*reinterpret_cast<uint32_t*>(&r14_29)) {
            r12_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0x92);
        }
        r9_68 = zero_padding_width;
        if (r9_68) {
            rdi43 = r12_3;
            r8_36 = reinterpret_cast<void**>("0%ld");
            __asm__("fstp tword [rsp+0x20]");
            *reinterpret_cast<int32_t*>(&rsi44) = 62;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            v45 = reinterpret_cast<void**>(0x4470);
            eax69 = fun_23f0(rdi43, 62, 1, rdi43, 62, 1);
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
            r12_3 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_3) + reinterpret_cast<uint64_t>(static_cast<int64_t>(reinterpret_cast<int32_t>(eax69))));
        }
        *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
        if (*reinterpret_cast<unsigned char*>(&rcx42)) {
            rcx70 = stderr;
            *reinterpret_cast<int32_t*>(&rsi44) = 1;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            rdi43 = reinterpret_cast<void**>("double_to_human:\n");
            __asm__("fstp tword [rsp+0x20]");
            v45 = reinterpret_cast<void**>(0x443c);
            fun_2730("double_to_human:\n", 1, 17, rcx70, r8_36);
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
            __asm__("fld tword [rsp+0x20]");
        }
        if (*reinterpret_cast<uint32_t*>(&rbp4)) 
            goto addr_4110_79;
        rax71 = v62;
        if (!*reinterpret_cast<int32_t*>(&rax71)) {
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fld tword [rip+0x80f8]");
            __asm__("fld st1");
            __asm__("fdiv st0, st1");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x20]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fild qword [rsp+0x20]");
            __asm__("fmulp st1, st0");
            __asm__("fld st1");
            __asm__("fsub st0, st1");
            if (*reinterpret_cast<uint32_t*>(&r13_67) <= 4) 
                goto addr_4d39_82;
            __asm__("fstp st0");
            __asm__("fstp st0");
        } else {
            eax72 = *reinterpret_cast<int32_t*>(&rax71) - 1;
            if (!eax72) {
                __asm__("fnstcw word [rsp+0x4e]");
                __asm__("fld tword [rip+0x7f0a]");
                __asm__("fld st1");
                __asm__("fmul dword [rip+0x7eb1]");
                __asm__("fld st0");
                __asm__("fdiv st0, st2");
                __asm__("fldcw word [rsp+0x4c]");
                __asm__("fistp qword [rsp+0x20]");
                __asm__("fldcw word [rsp+0x4e]");
                __asm__("fild qword [rsp+0x20]");
                __asm__("fmulp st2, st0");
                __asm__("fsub st0, st1");
                if (*reinterpret_cast<uint32_t*>(&r13_67) <= 4) 
                    goto addr_4f2d_86;
                __asm__("fstp st0");
                __asm__("fstp st0");
            } else {
                __asm__("fld dword [rip+0x8a2e]");
                edx73 = eax72;
                __asm__("fld st0");
                do {
                    __asm__("fmul st0, st1");
                    --edx73;
                } while (edx73);
                __asm__("fnstcw word [rsp+0x4e]");
                __asm__("fmul st0, st2");
                __asm__("fld tword [rip+0x8a5d]");
                __asm__("fld st1");
                __asm__("fdiv st0, st1");
                __asm__("fldcw word [rsp+0x4c]");
                __asm__("fistp qword [rsp+0x20]");
                __asm__("fldcw word [rsp+0x4e]");
                __asm__("fild qword [rsp+0x20]");
                __asm__("fmulp st1, st0");
                __asm__("fsub st1, st0");
                if (*reinterpret_cast<uint32_t*>(&r13_67) <= 4) 
                    goto addr_43d2_91;
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fldz ");
                __asm__("fxch st0, st1");
                __asm__("fld st0");
                do {
                    __asm__("fmul st0, st1");
                    --eax72;
                } while (eax72);
                __asm__("fstp st1");
                __asm__("fdivp st1, st0");
                goto addr_461b_96;
            }
        }
        __asm__("fldz ");
        addr_461b_96:
        *reinterpret_cast<uint32_t*>(&rbx48) = *reinterpret_cast<uint32_t*>(&v62);
        *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
        if (!*reinterpret_cast<unsigned char*>(&rcx42)) {
            __asm__("fxch st0, st1");
        } else {
            __asm__("fxch st0, st1");
            __asm__("fstp tword [rsp+0x10]");
            rdx74 = reinterpret_cast<void**>("  no scaling, returning (grouped) value: %'.*Lf\n");
            rdi75 = stderr;
            if (!*reinterpret_cast<uint32_t*>(&r14_29)) {
                rdx74 = reinterpret_cast<void**>("  no scaling, returning value: %.*Lf\n");
            }
            __asm__("fld st0");
            __asm__("fstp tword [rsp]");
            __asm__("fstp tword [rsp+0x10]");
            fun_2740(rdi75, 1, rdx74, rdi75, 1, rdx74);
            r14_29 = v76;
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
            __asm__("fld tword [rsp]");
            __asm__("fld tword [rsp+0x10]");
        }
        __asm__("fstp tword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rcx42) = 0x80;
        *reinterpret_cast<void***>(r12_3) = reinterpret_cast<void**>(0x664c2a2e);
        *reinterpret_cast<void***>(r12_3 + 4) = reinterpret_cast<void**>(0);
        r12_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0xd0);
        rsp77 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16);
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        r8_36 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp77) + 0xa0);
        __asm__("fstp tword [rsp+0x10]");
        eax78 = fun_23f0(r12_3, 0x80, 1, r12_3, 0x80, 1);
        rbp4 = v76;
        r13_22 = v45;
        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp77) - 8 + 8 + 8 + 8);
        __asm__("fld tword [rsp]");
        if (eax78 <= 0x7f) 
            goto addr_46c9_103;
        __asm__("fstp tword [rsp]");
        fun_24e0();
        __asm__("fld tword [rsp]");
        *reinterpret_cast<int32_t*>(&rsi44) = 0;
        *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&rdi43) = 1;
        *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
        v45 = rbx48;
        v76 = rbx48;
        __asm__("fstp tword [rsp]");
        fun_26d0();
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 - 8 - 8 + 8);
        addr_4f87_105:
        __asm__("fstp st0");
        v49 = reinterpret_cast<void**>(0);
        __asm__("fld1 ");
        addr_3b0d_106:
        if (*reinterpret_cast<unsigned char*>(&rcx42)) {
            __asm__("fxch st0, st1");
            __asm__("fstp tword [rsp+0x30]");
            *reinterpret_cast<uint32_t*>(&rcx42) = *reinterpret_cast<uint32_t*>(&r14_29);
            rdi43 = stderr;
            *reinterpret_cast<int32_t*>(&rsi44) = 1;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            __asm__("fld st0");
            __asm__("fstp tword [rsp]");
            __asm__("fstp tword [rsp+0x30]");
            fun_2740(rdi43, 1, "  suffix power=%d^%d = %Lf\n", rdi43, 1, "  suffix power=%d^%d = %Lf\n");
            __asm__("fld tword [rsp+0x30]");
            zf80 = dev_debug == 0;
            __asm__("fld tword [rsp+0x40]");
            __asm__("fmulp st1, st0");
            __asm__("fld st0");
            __asm__("fstp tword [rsp+0x80]");
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
            if (!zf80) {
                rdi43 = stderr;
                __asm__("fld st0");
                __asm__("fstp tword [rsp+0x10]");
                *reinterpret_cast<int32_t*>(&rsi44) = 1;
                *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
                __asm__("fld st0");
                __asm__("fstp tword [rsp]");
                __asm__("fstp tword [rsp+0x40]");
                fun_2740(rdi43, 1, "  returning value: %Lf (%LG)\n", rdi43, 1, "  returning value: %Lf (%LG)\n");
                __asm__("fld tword [rsp+0x40]");
                rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 32 - 8 + 8 + 32);
            }
        } else {
            __asm__("fmulp st1, st0");
            __asm__("fld st0");
            __asm__("fstp tword [rsp+0x70]");
        }
        if (!*reinterpret_cast<void***>(rbp4)) {
            if (*reinterpret_cast<uint32_t*>(&rbx48) != 1) 
                continue;
            zf81 = debug == 0;
            if (zf81) 
                continue;
            __asm__("fstp tword [rsp+0x20]");
            rax82 = quote(r13_22, r13_22);
            fun_24e0();
            rcx42 = rax82;
            *reinterpret_cast<int32_t*>(&rsi44) = 0;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rdi43) = 0;
            *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
            v45 = reinterpret_cast<void**>(0x3f29);
            fun_26d0();
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 8 + 8 - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
        } else {
            zf83 = reinterpret_cast<int1_t>(inval_style == 3);
            *reinterpret_cast<uint32_t*>(&rbx48) = 5;
            if (zf83) 
                continue;
            __asm__("fstp tword [rsp+0x20]");
            *reinterpret_cast<uint32_t*>(&rbx48) = 5;
            rax84 = quote_n();
            rbp4 = rax84;
            rax85 = quote_n();
            fun_24e0();
            *reinterpret_cast<uint32_t*>(&rdi43) = conv_exit_code;
            *reinterpret_cast<int32_t*>(&rdi43 + 4) = 0;
            r8_36 = rbp4;
            *reinterpret_cast<int32_t*>(&rsi44) = 0;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            rcx42 = rax85;
            v45 = reinterpret_cast<void**>(0x3b86);
            fun_26d0();
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 8 + 8 - 8 + 8 - 8 + 8);
            __asm__("fld tword [rsp+0x20]");
        }
    }
    __asm__("fstp st0");
    addr_3be8_117:
    rsi10 = stdout;
    rdi86 = r12_3;
    *reinterpret_cast<int32_t*>(&r12_3) = 0;
    *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
    fun_25c0(rdi86, rsi10, rdx59, rdi86, rsi10, rdx59);
    rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
    goto addr_394d_10;
    addr_447e_65:
    zf87 = reinterpret_cast<int1_t>(inval_style == 3);
    if (zf87) {
        __asm__("fstp st0");
        goto addr_3be8_117;
    } else {
        rbx48 = v62;
        __asm__("fstp tword [rsp]");
        *reinterpret_cast<uint32_t*>(&rbp4) = conv_exit_code;
        if (!rbx48) {
            rax88 = fun_24e0();
            __asm__("fld tword [rsp]");
            rdx59 = rax88;
            __asm__("fstp tword [rsp]");
            fun_26d0();
            rcx42 = v76;
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
            goto addr_3be8_117;
        } else {
            rax89 = fun_24e0();
            __asm__("fld tword [rsp]");
            rdx59 = rax89;
            rcx42 = rbx48;
            __asm__("fstp tword [rsp]");
            fun_26d0();
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
            goto addr_3be8_117;
        }
    }
    addr_4110_79:
    eax90 = static_cast<uint32_t>(reinterpret_cast<uint64_t>(rbp4 + 0xfffffffffffffffd));
    cf91 = eax90 < 1;
    if (eax90 <= 1) {
        *reinterpret_cast<void***>(rdi43) = *reinterpret_cast<void***>(rsi44);
        rdi92 = reinterpret_cast<void***>(rdi43 + 4);
    } else {
        *reinterpret_cast<void***>(rdi43) = *reinterpret_cast<void***>(rsi44);
        rdi92 = reinterpret_cast<void***>(rdi43 + 4);
    }
    __asm__("fld tword [rip+0x8cae]");
    __asm__("fxch st0, st1");
    __asm__("fcomi st0, st1");
    __asm__("fstp st1");
    if (cf91 || cf91) {
        __asm__("fld st0");
        *reinterpret_cast<uint32_t*>(&rbx48) = 0;
        *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
    } else {
        __asm__("fild dword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rbx48) = 0;
        *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
        cf93 = 0;
        __asm__("fld st1");
        while (!cf93) {
            __asm__("fdiv st0, st1");
            tmp32_94 = *reinterpret_cast<uint32_t*>(&rbx48) + 1;
            cf93 = tmp32_94 < *reinterpret_cast<uint32_t*>(&rbx48);
            *reinterpret_cast<uint32_t*>(&rbx48) = tmp32_94;
            *reinterpret_cast<int32_t*>(&rbx48 + 4) = 0;
        }
        __asm__("fstp st1");
    }
    if (*reinterpret_cast<unsigned char*>(&rcx42)) {
        __asm__("fxch st0, st1");
        __asm__("fstp tword [rsp+0x30]");
        rdi95 = stderr;
        *reinterpret_cast<void***>(rdi95) = g1;
        rdi96 = rdi95 + 4;
        __asm__("fld st0");
        __asm__("fstp tword [rsp]");
        __asm__("fstp tword [rsp+0x30]");
        fun_2740(rdi96, 5, "  scaled value to %Lf * %0.f ^ %u\n", rdi96, 5, "  scaled value to %Lf * %0.f ^ %u\n");
        *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
        *reinterpret_cast<void***>(rdi96) = g5;
        rdi92 = reinterpret_cast<void***>(rdi96 + 4);
        __asm__("fld tword [rsp+0x30]");
        __asm__("fld tword [rsp+0x20]");
    }
    rsi97 = user_precision;
    if (rsi97 == 0xffffffffffffffff) {
        __asm__("fldz ");
        __asm__("fld st1");
        __asm__("fchs ");
        __asm__("fxch st0, st1");
        __asm__("fcomip st0, st2");
        __asm__("fcmovbe st0, st1");
        __asm__("fld dword [rip+0x81dc]");
        __asm__("fld st0");
        __asm__("fcomip st0, st2");
        __asm__("fstp st1");
        if (reinterpret_cast<unsigned char>(rsi97) <= reinterpret_cast<unsigned char>(0xffffffffffffffff)) {
            __asm__("fstp st0");
        } else {
            addr_4be4_146:
            __asm__("fnstcw word [rsp+0x4e]");
            __asm__("fmul st1, st0");
            __asm__("fld tword [rip+0x8210]");
            __asm__("fld st2");
            __asm__("fdiv st0, st1");
            __asm__("fldcw word [rsp+0x4c]");
            __asm__("fistp qword [rsp+0x10]");
            __asm__("fldcw word [rsp+0x4e]");
            __asm__("fild qword [rsp+0x10]");
            __asm__("fmulp st1, st0");
            __asm__("fsub st2, st0");
            cf98 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) < 4);
            pf99 = __intrinsic();
            zf100 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) == 4);
            below_or_equal101 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) <= 4);
            if (!below_or_equal101) {
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fstp st0");
                __asm__("fldz ");
                goto addr_454d_148;
            } else {
                *reinterpret_cast<uint32_t*>(&rax102) = *reinterpret_cast<uint32_t*>(&r13_67);
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax102) + 4) = 0;
                goto *reinterpret_cast<int32_t*>(0xcc1c + rax102 * 4) + 0xcc1c;
            }
        }
    } else {
        *reinterpret_cast<int32_t*>(&rax103) = static_cast<int32_t>(reinterpret_cast<uint64_t>(rbx48 + reinterpret_cast<unsigned char>(rbx48) * 2));
        *reinterpret_cast<int32_t*>(&rax103 + 4) = 0;
        if (reinterpret_cast<signed char>(rax103) > reinterpret_cast<signed char>(rsi97)) {
            rax103 = rsi97;
        }
        *reinterpret_cast<int32_t*>(&rdi92) = *reinterpret_cast<int32_t*>(&rax103);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi92) + 4) = 0;
        if (*reinterpret_cast<int32_t*>(&rax103)) 
            goto addr_419a_153;
    }
    __asm__("fnstcw word [rsp+0x4e]");
    __asm__("fld tword [rip+0x82a0]");
    __asm__("fld st1");
    __asm__("fdiv st0, st1");
    __asm__("fldcw word [rsp+0x4c]");
    __asm__("fistp qword [rsp+0x10]");
    __asm__("fldcw word [rsp+0x4e]");
    __asm__("fild qword [rsp+0x10]");
    __asm__("fmulp st1, st0");
    __asm__("fsub st1, st0");
    cf98 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) < 4);
    pf99 = __intrinsic();
    zf100 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) == 4);
    below_or_equal101 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r13_67) <= 4);
    if (!below_or_equal101) {
        __asm__("fstp st0");
        __asm__("fstp st0");
        __asm__("fldz ");
        goto addr_454d_148;
    } else {
        *reinterpret_cast<uint32_t*>(&rax104) = *reinterpret_cast<uint32_t*>(&r13_67);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax104) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0xcc08 + rax104 * 4) + 0xcc08;
    }
    addr_419a_153:
    *reinterpret_cast<uint32_t*>(&rax105) = reinterpret_cast<uint32_t>(*reinterpret_cast<int32_t*>(&rax103) - 1);
    if (!*reinterpret_cast<uint32_t*>(&rax105)) {
        __asm__("fld dword [rip+0x7e07]");
        goto addr_4be4_146;
    }
    __asm__("fld dword [rip+0x8c0b]");
    edx106 = *reinterpret_cast<uint32_t*>(&rax105);
    __asm__("fld st0");
    do {
        __asm__("fmul st0, st1");
        --edx106;
    } while (edx106);
    __asm__("fnstcw word [rsp+0x4e]");
    __asm__("fmulp st2, st0");
    __asm__("fld tword [rip+0x8c3d]");
    __asm__("fld st2");
    __asm__("fdiv st0, st1");
    __asm__("fldcw word [rsp+0x4c]");
    __asm__("fistp qword [rsp+0x10]");
    __asm__("fldcw word [rsp+0x4e]");
    __asm__("fild qword [rsp+0x10]");
    __asm__("fmulp st1, st0");
    __asm__("fsub st2, st0");
    if (*reinterpret_cast<uint32_t*>(&r13_67) <= 4) 
        goto addr_41f2_161;
    __asm__("fstp st0");
    __asm__("fstp st1");
    __asm__("fldz ");
    __asm__("fxch st0, st1");
    __asm__("fld st0");
    do {
        __asm__("fmul st0, st1");
        cf98 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax105) < 1);
        below_or_equal101 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax105) <= 1);
        *reinterpret_cast<uint32_t*>(&rax105) = *reinterpret_cast<uint32_t*>(&rax105) - 1;
        pf99 = __intrinsic();
        zf100 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rax105) == 0);
    } while (!zf100);
    __asm__("fstp st1");
    __asm__("fdivp st1, st0");
    goto addr_4543_166;
    addr_41f2_161:
    goto *reinterpret_cast<int32_t*>(0xcbbc + r13_67 * 4) + 0xcbbc;
    addr_4d39_82:
    goto *reinterpret_cast<int32_t*>(0xcc30 + r13_67 * 4) + 0xcc30;
    addr_46c9_103:
    __asm__("fstp st0");
    __asm__("fld tword [rsp+0x10]");
    ebx107 = dev_debug;
    goto addr_46d6_167;
    addr_4f2d_86:
    goto *reinterpret_cast<int32_t*>(0xcc44 + r13_67 * 4) + 0xcc44;
    addr_43d2_91:
    __asm__("fstp st2");
    __asm__("fxch st0, st1");
    goto *reinterpret_cast<int32_t*>(0xcbd0 + r13_67 * 4) + 0xcbd0;
    addr_4057_72:
    zf108 = reinterpret_cast<int1_t>(inval_style == 3);
    __asm__("fstp tword [rsp]");
    if (!zf108) {
        fun_24e0();
        __asm__("fld tword [rsp]");
        __asm__("fstp tword [rsp]");
        fun_26d0();
        rdx59 = reinterpret_cast<void**>(0x407a);
        rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
        goto addr_3be8_117;
    }
    addr_4328_44:
    __asm__("fstp st0");
    __asm__("fld tword [rsp+0x70]");
    v49 = reinterpret_cast<void**>(0);
    *reinterpret_cast<uint32_t*>(&rbx48) = 3;
    rbp4 = reinterpret_cast<void**>("invalid number: %s");
    goto addr_3f70_49;
    addr_3e47_45:
    rbp4 = v109;
    __asm__("fld tword [rsp+0x80]");
    rax110 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp4) - reinterpret_cast<unsigned char>(rdi43));
    v49 = rax110;
    if (*reinterpret_cast<uint32_t*>(&rbp4) != *reinterpret_cast<uint32_t*>(&rdi43)) {
        eax111 = *reinterpret_cast<int32_t*>(&rax110) - 1;
        if (!eax111) {
            __asm__("fdiv dword [rip+0x7f0c]");
        } else {
            __asm__("fld dword [rip+0x8f43]");
            __asm__("fld st0");
            do {
                __asm__("fmul st0, st1");
                --eax111;
            } while (eax111);
            __asm__("fstp st1");
            __asm__("fdivp st1, st0");
        }
    }
    if (!v112) {
        __asm__("faddp st1, st0");
    } else {
        __asm__("fsubp st1, st0");
    }
    __asm__("fld st0");
    __asm__("fstp tword [rsp+0x70]");
    goto addr_3ada_46;
    addr_3ae7_48:
    *reinterpret_cast<uint32_t*>(&rcx42) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp4));
    if (*reinterpret_cast<unsigned char*>(&rcx42)) {
        __asm__("fstp tword [rsp+0x30]");
        rax113 = fun_2790(rdi43, rdi43);
        rsp114 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
        *reinterpret_cast<uint32_t*>(&rcx42) = *reinterpret_cast<unsigned char*>(&rcx42);
        __asm__("fld tword [rsp+0x30]");
        edi115 = 0;
        rsi116 = *reinterpret_cast<void***>(rax113);
        while (*reinterpret_cast<uint32_t*>(&rax117) = *reinterpret_cast<unsigned char*>(&rcx42), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax117) + 4) = 0, rdx118 = rbp4, ++rbp4, !!(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(reinterpret_cast<unsigned char>(rsi116) + reinterpret_cast<uint64_t>(rax117 * 2))) & 1)) {
            *reinterpret_cast<uint32_t*>(&rcx42) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx118 + 1));
            edi115 = 1;
        }
        if (*reinterpret_cast<signed char*>(&edi115)) 
            goto addr_3cfe_183;
    } else {
        if (*reinterpret_cast<uint32_t*>(&r15_27) == 4) {
            *reinterpret_cast<uint32_t*>(&rbx48) = 6;
            rbp4 = reinterpret_cast<void**>("missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)");
            goto addr_3f70_49;
        } else {
            *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
            *reinterpret_cast<int32_t*>(&r8_36) = 0;
            *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
            __asm__("fld1 ");
            goto addr_3b0d_106;
        }
    }
    addr_3d03_187:
    *reinterpret_cast<int32_t*>(&rsi44) = *reinterpret_cast<signed char*>(&rcx42);
    *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
    rdi43 = reinterpret_cast<void**>("KMGTPEZY");
    __asm__("fstp tword [rsp+0x20]");
    v45 = reinterpret_cast<void**>(0x3d1f);
    rax119 = fun_2540("KMGTPEZY", "KMGTPEZY");
    rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp114) - 8 + 8);
    __asm__("fld tword [rsp+0x20]");
    if (!rax119) {
        *reinterpret_cast<uint32_t*>(&rbx48) = 5;
        rbp4 = reinterpret_cast<void**>("invalid suffix in input: %s");
        goto addr_3f70_49;
    } else {
        if (!*reinterpret_cast<uint32_t*>(&r15_27)) {
            *reinterpret_cast<uint32_t*>(&rbx48) = 4;
            rbp4 = reinterpret_cast<void**>("rejecting suffix in input: %s (consider using --from)");
            goto addr_3f70_49;
        } else {
            *reinterpret_cast<int32_t*>(&r8_36) = 0;
            *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&rcx42) = *reinterpret_cast<unsigned char*>(&rcx42) - 69;
            if (*reinterpret_cast<unsigned char*>(&rcx42) <= 21) {
                *reinterpret_cast<uint32_t*>(&rcx42) = *reinterpret_cast<unsigned char*>(&rcx42);
                *reinterpret_cast<int32_t*>(&rcx42 + 4) = 0;
                *reinterpret_cast<int32_t*>(&r8_36) = *reinterpret_cast<int32_t*>(0xcd00 + reinterpret_cast<unsigned char>(rcx42) * 4);
                *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
            }
            rbp4 = rdx118 + 1;
            if (*reinterpret_cast<uint32_t*>(&r15_27) != 1) 
                goto addr_3d6b_194;
        }
    }
    *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
    if (*reinterpret_cast<void***>(rdx118 + 1) == 0x69) {
        rbp4 = rdx118 + 2;
        if (*reinterpret_cast<unsigned char*>(&rcx42)) {
            *reinterpret_cast<int32_t*>(&rsi44) = 1;
            *reinterpret_cast<int32_t*>(&rsi44 + 4) = 0;
            __asm__("fstp tword [rsp+0x20]");
            rdi43 = stderr;
            *reinterpret_cast<uint32_t*>(&r14_29) = 0x400;
            v45 = reinterpret_cast<void**>(0x4e50);
            fun_2740(rdi43, 1, "  Auto-scaling, found 'i', switching to base %d\n", rdi43, 1, "  Auto-scaling, found 'i', switching to base %d\n");
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 + 8);
            *reinterpret_cast<int32_t*>(&r8_36) = *reinterpret_cast<int32_t*>(&r8_36);
            *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
            __asm__("fld tword [rsp+0x20]");
            __asm__("fld dword [rip+0x7f59]");
            *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
        } else {
            __asm__("fld dword [rip+0x8118]");
            *reinterpret_cast<uint32_t*>(&r14_29) = 0x400;
        }
    } else {
        __asm__("fild dword [rsp+0x10]");
    }
    addr_3d85_200:
    if (!*reinterpret_cast<int32_t*>(&r8_36)) 
        goto addr_4f87_105;
    __asm__("fld st0");
    eax120 = *reinterpret_cast<int32_t*>(&r8_36) - 1;
    if (!eax120) {
        __asm__("fstp st1");
        v49 = reinterpret_cast<void**>(0);
        *reinterpret_cast<int32_t*>(&r8_36) = 1;
        *reinterpret_cast<int32_t*>(&r8_36 + 4) = 0;
        goto addr_3b0d_106;
    } else {
        do {
            __asm__("fmul st0, st1");
            --eax120;
        } while (eax120);
        __asm__("fstp st1");
        v49 = reinterpret_cast<void**>(0);
        goto addr_3b0d_106;
    }
    addr_3d6b_194:
    if (*reinterpret_cast<uint32_t*>(&r15_27) == 4) {
        if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdx118 + 1) == 0x69)) {
            v49 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&rbx48) = 6;
            rbp4 = reinterpret_cast<void**>("missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)");
            goto addr_3f70_49;
        } else {
            rbp4 = rdx118 + 2;
            goto addr_3d75_209;
        }
    } else {
        addr_3d75_209:
        __asm__("fild dword [rsp+0x10]");
        *reinterpret_cast<uint32_t*>(&rcx42) = dev_debug;
        goto addr_3d85_200;
    }
    addr_3cfe_183:
    goto addr_3d03_187;
    addr_3bff_31:
    rdi121 = stderr;
    fun_2740(rdi121, 1, "setting Auto-Padding to %ld characters\n", rdi121, 1, "setting Auto-Padding to %ld characters\n");
    rsp5 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp5) - 8 + 8);
    goto addr_3c20_24;
    addr_3a49_32:
    *reinterpret_cast<uint32_t*>(&r15_27) = scale_from;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_27) + 4) = 0;
    __asm__("pxor xmm2, xmm2");
    __asm__("movss [rsp], xmm2");
    __asm__("fld dword [rsp]");
    __asm__("fstp tword [rsp+0x70]");
    *reinterpret_cast<uint32_t*>(&r14_29) = (r14d122 - (r14d123 + reinterpret_cast<uint1_t>(r14d124 < r14d125 + reinterpret_cast<uint1_t>(static_cast<uint32_t>(r15_27 - 3) < 2))) & 24) + 0x3e8;
    goto addr_3a7e_33;
    addr_45b8_210:
    *reinterpret_cast<uint32_t*>(&rax126) = *reinterpret_cast<uint32_t*>(&rbx48);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax126) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0xcbe4 + rax126 * 4) + 0xcbe4;
    addr_3964_211:
    eax127 = *reinterpret_cast<int32_t*>(&r12_3);
    return *reinterpret_cast<signed char*>(&eax127);
    while (1) {
        if (!*reinterpret_cast<signed char*>(&ebx107)) {
            __asm__("fstp st0");
        } else {
            __asm__("fstp tword [rsp]");
            quote(r12_3, r12_3);
            __asm__("fld tword [rsp]");
            rdi128 = stderr;
            __asm__("fstp tword [rsp]");
            fun_2740(rdi128, 1, "formatting output:\n  value: %Lf\n  humanized: %s\n", rdi128, 1, "formatting output:\n  value: %Lf\n  humanized: %s\n");
            rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 16 - 8 + 8 + 8 + 8);
        }
        while (1) {
            rbx48 = padding_width;
            rax129 = fun_2500(r12_3, r12_3);
            rsp130 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
            rdx131 = padding_buffer_size;
            rdi132 = padding_buffer;
            if (!rbx48 || reinterpret_cast<unsigned char>(rbx48) <= reinterpret_cast<unsigned char>(rax129)) {
                rcx42 = rax129 + 1;
                if (reinterpret_cast<unsigned char>(rcx42) >= reinterpret_cast<unsigned char>(rdx131)) {
                    padding_buffer_size = rax129 + 2;
                    rax133 = xrealloc(rdi132);
                    rsp130 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp130) - 8 + 8);
                    padding_buffer = rax133;
                    rdi132 = rax133;
                }
                fun_2450(rdi132, r12_3);
                rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp130) - 8 + 8);
            } else {
                *reinterpret_cast<int32_t*>(&r8_135) = padding_alignment;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_135) + 4) = 0;
                rcx42 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp130) + 0x80);
                mbsalign(r12_3, rdi132, rdx131, rcx42, r8_135, 2);
                rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp130) - 8 + 8);
                zf136 = dev_debug == 0;
                if (!zf136) {
                    rdi137 = padding_buffer;
                    rax138 = quote(rdi137, rdi137);
                    rdi139 = stderr;
                    rdx131 = reinterpret_cast<void**>("  After padding: %s\n");
                    rcx42 = rax138;
                    fun_2740(rdi139, 1, "  After padding: %s\n", rdi139, 1, "  After padding: %s\n");
                    rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp134) - 8 + 8 - 8 + 8);
                }
            }
            rdi140 = format_str_prefix;
            rsi10 = stdout;
            if (rdi140) {
                fun_25c0(rdi140, rsi10, rdx131, rdi140, rsi10, rdx131);
                rsp134 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp134) - 8 + 8);
                rsi10 = stdout;
            }
            rdi141 = padding_buffer;
            *reinterpret_cast<int32_t*>(&r12_3) = 1;
            *reinterpret_cast<int32_t*>(&r12_3 + 4) = 0;
            fun_25c0(rdi141, rsi10, rdx131, rdi141, rsi10, rdx131);
            rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp134) - 8 + 8);
            rdi142 = format_str_suffix;
            if (rdi142) {
                rsi10 = stdout;
                fun_25c0(rdi142, rsi10, rdx131, rdi142, rsi10, rdx131);
                rsp12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            }
            addr_394d_10:
            rax143 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
            if (!rax143) 
                goto addr_3964_211;
            fun_2510();
            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp12) - 8 + 8);
            while (1) {
                addr_4f9e_226:
                __asm__("fld st0");
                __asm__("fchs ");
                while (1) {
                    __asm__("fxch st0, st1");
                    while (1) {
                        __asm__("fld dword [rip+0x8278]");
                        __asm__("fcomip st0, st2");
                        __asm__("fstp st1");
                        do {
                            if (*reinterpret_cast<unsigned char*>(&rcx42)) {
                                __asm__("fxch st0, st1");
                                __asm__("fstp tword [rsp+0x20]");
                                rdi144 = stderr;
                                __asm__("fld st0");
                                __asm__("fstp tword [rsp]");
                                __asm__("fstp tword [rsp+0x10]");
                                fun_2740(rdi144, 1, "  after rounding, value=%Lf * %0.f ^ %u\n", rdi144, 1, "  after rounding, value=%Lf * %0.f ^ %u\n");
                                rsi10 = user_precision;
                                rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 16 - 8 + 8 + 8 + 8);
                                __asm__("fld tword [rsp+0x20]");
                                __asm__("fld tword [rsp]");
                            }
                            *reinterpret_cast<void***>(r12_3) = reinterpret_cast<void**>(0x664c2a2e);
                            *reinterpret_cast<void***>(r12_3 + 4) = reinterpret_cast<void**>(0x7325);
                            if (!reinterpret_cast<int1_t>(rsi10 == 0xffffffffffffffff)) {
                            }
                            *reinterpret_cast<signed char*>(r12_3 + 6) = 0;
                            if (*reinterpret_cast<uint32_t*>(&rbx48) <= 8) 
                                goto addr_45b8_210;
                            __asm__("fxch st0, st1");
                            __asm__("fstp tword [rsp+0x10]");
                            *reinterpret_cast<uint32_t*>(&rcx42) = 0x80;
                            r12_3 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rsp47) + 0xd0);
                            __asm__("fld st0");
                            __asm__("fstp tword [rsp]");
                            __asm__("fstp tword [rsp+0x20]");
                            eax145 = fun_23f0(r12_3, 0x7f, 1, r12_3, 0x7f, 1);
                            __asm__("fld tword [rsp+0x20]");
                            rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp47) - 8 - 8 - 16 - 8 + 8 + 32);
                            __asm__("fld tword [rsp+0x10]");
                            if (eax145 <= 0x7e) 
                                goto addr_4937_236;
                            __asm__("fstp st0");
                            __asm__("fstp tword [rsp]");
                            fun_24e0();
                            __asm__("fld tword [rsp]");
                            *reinterpret_cast<int32_t*>(&rsi97) = 0;
                            *reinterpret_cast<int32_t*>(&rsi97 + 4) = 0;
                            *reinterpret_cast<int32_t*>(&rdi92) = 1;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi92) + 4) = 0;
                            cf98 = 0;
                            pf99 = __intrinsic();
                            zf100 = 1;
                            below_or_equal101 = 1;
                            __asm__("fstp tword [rsp]");
                            fun_26d0();
                            rsp47 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 - 8 - 8 + 8);
                            __asm__("fxch st0, st2");
                            __asm__("fldcw word [rsp+0x4c]");
                            __asm__("fistp qword [rsp+0x10]");
                            __asm__("fldcw word [rsp+0x4e]");
                            __asm__("fild qword [rsp+0x10]");
                            __asm__("faddp st2, st0");
                            __asm__("fdivp st1, st0");
                            addr_4543_166:
                            __asm__("fldz ");
                            __asm__("fcomip st0, st1");
                            if (!below_or_equal101) {
                                *rdi92 = *reinterpret_cast<void***>(rsi97);
                                rsi10 = rsi97 + 4;
                                __asm__("fld qword [rsp+0x10]");
                                __asm__("fld st1");
                                __asm__("fchs ");
                                __asm__("fcomi st0, st1");
                                if (cf98) 
                                    goto addr_4b32_244;
                                __asm__("fstp st0");
                                __asm__("fxch st0, st1");
                            } else {
                                addr_454d_148:
                                *rdi92 = *reinterpret_cast<void***>(rsi97);
                                rsi10 = rsi97 + 4;
                                __asm__("fld qword [rsp+0x10]");
                                __asm__("fxch st0, st1");
                                __asm__("fcomi st0, st1");
                                if (cf98) 
                                    goto addr_456a_249; else 
                                    goto addr_455d_250;
                            }
                            addr_4563_251:
                            tmp32_146 = *reinterpret_cast<uint32_t*>(&rbx48) + 1;
                            cf147 = reinterpret_cast<uint1_t>(tmp32_146 < *reinterpret_cast<uint32_t*>(&rbx48));
                            *reinterpret_cast<uint32_t*>(&rbx48) = tmp32_146;
                            pf99 = __intrinsic();
                            zf100 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rbx48) == 0);
                            below_or_equal101 = reinterpret_cast<uint1_t>(cf147 | zf100);
                            __asm__("fdivrp st1, st0");
                            continue;
                            addr_456a_249:
                            __asm__("fstp st1");
                            continue;
                            addr_455d_250:
                            goto addr_4563_251;
                            __asm__("fldz ");
                            __asm__("fxch st0, st1");
                            __asm__("fucomi st0, st1");
                            __asm__("fstp st1");
                        } while (!pf99 && zf100);
                        __asm__("fldz ");
                        __asm__("fcomip st0, st1");
                        if (!below_or_equal101) 
                            goto addr_4f9e_226;
                        __asm__("fld st0");
                    }
                    addr_4b32_244:
                    __asm__("fstp st1");
                }
            }
            addr_4937_236:
            __asm__("fstp st1");
            if (*reinterpret_cast<uint32_t*>(&rbx48)) {
                if (*reinterpret_cast<uint32_t*>(&rbp4) == 4) {
                    __asm__("fstp tword [rsp]");
                    fun_2750(r12_3, "i");
                    rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8);
                    __asm__("fld tword [rsp]");
                }
            }
            ebx107 = dev_debug;
            if (*reinterpret_cast<signed char*>(&ebx107)) 
                break;
            rbp4 = suffix;
            if (rbp4) 
                goto addr_46e2_260;
            __asm__("fstp st0");
        }
        __asm__("fstp tword [rsp]");
        quote(r12_3, r12_3);
        rdi148 = stderr;
        fun_2740(rdi148, 1, "  returning value: %s\n", rdi148, 1, "  returning value: %s\n");
        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8);
        ebx107 = dev_debug;
        __asm__("fld tword [rsp]");
        addr_46d6_167:
        rbp4 = suffix;
        if (!rbp4) 
            continue;
        addr_46e2_260:
        __asm__("fstp tword [rsp]");
        fun_2500(r12_3, r12_3);
        fun_2750(r12_3, rbp4);
        rsp79 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp79) - 8 + 8 - 8 + 8);
        __asm__("fld tword [rsp]");
    }
}