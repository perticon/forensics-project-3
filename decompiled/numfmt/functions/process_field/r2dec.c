void process_field (int64_t arg_10h_2, int64_t arg_10h, int64_t arg_80h, int64_t arg_b0h, int64_t arg_d0h, int64_t arg_158h, int64_t arg1, uint32_t arg2) {
    int64_t var_1h;
    int64_t var_3h;
    int64_t var_10h_2;
    uint32_t var_10h;
    int64_t var_20h;
    int64_t var_30h;
    int64_t var_40h;
    int64_t var_4ch;
    int64_t var_4eh;
    uint32_t var_5eh;
    uint32_t var_5fh;
    char ** s1;
    int64_t var_68h;
    int64_t var_70h;
    int64_t var_80h;
    int64_t var_90h;
    int64_t var_91h;
    int64_t var_92h;
    int64_t var_d0h;
    int64_t var_158h;
    rdi = arg1;
    rsi = arg2;
label_37:
    *(fp_stack--) = 0.0;
    goto label_32;
label_15:
    fp_stack++;
    fp_stack++;
    *(fp_stack--) = 0.0;
    fp_tmp_0 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_0;
    goto label_38;
label_10:
    fp_stack++;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = 0.0;
    fp_tmp_1 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_1;
    goto label_22;
label_27:
    fp_stack++;
    fp_stack++;
    *(fp_stack--) = 0.0;
    void (*0x454d)() ();
    r12 = rdi;
    rax = *(fs:0x28);
    *((rsp + 0x158)) = rax;
    rax = frp;
    if (rax != 0) {
        goto label_39;
    }
    goto label_40;
    do {
        if (rbp >= rdx) {
            if (rbp <= *((rax + 8))) {
                goto label_41;
            }
        }
        rax += 0x10;
label_39:
        rdx = *(rax);
    } while (rdx != -1);
    do {
        rsi = stdout;
        rdi = r12;
        r12d = 1;
        fputs_unlocked ();
label_40:
    } while (rsi != 1);
label_41:
    r13 = suffix;
    if (r13 != 0) {
        rax = strlen (r12);
        rbx = rax;
        rax = strlen (r13);
        if (rbx <= rax) {
            goto label_11;
        }
        rbx -= rax;
        rbx += r12;
        eax = strcmp (r13, rbx);
        edx = *(obj.dev_debug);
        if (eax != 0) {
            goto label_42;
        }
        *(rbx) = 0;
        if (dl != 0) {
            goto label_43;
        }
    }
label_11:
    ebx = *(r12);
    if (bl == 0) {
        goto label_44;
    }
    rax = ctype_b_loc ();
    r13 = r12;
    rax = *(rax);
    while ((*((rax + rbx*2)) & 1) != 0) {
        ebx = *((r13 + 1));
        r13++;
        if (bl == 0) {
            goto label_45;
        }
    }
label_45:
    eax = r12d;
    eax -= r13d;
label_16:
    edx = auto_padding;
    if (edx == 0) {
        goto label_46;
    }
    if (eax != 0) {
        goto label_47;
    }
    if (rbp > 1) {
        goto label_47;
    }
    *(obj.padding_width) = 0;
label_5:
    if (*(obj.dev_debug) != 0) {
        goto label_48;
    }
    r15d = scale_from;
    xmm2 = 0;
    *((rsp + 0x60)) = 0;
    *(rsp) = xmm2;
    *(fp_stack--) = *(rsp);
    eax = r15 - 3;
    r14d -= r14d;
    ? = fp_stack[0];
    fp_stack--;
    r14d &= 0x18;
    r14d += 0x3e8;
    do {
label_0:
        eax = simple_strtod_int (r13, rsp + 0x60, rsp + 0x70, rsp + 0x5e);
        ebx = eax;
        if (eax > 1) {
            goto label_49;
        }
        rbp = *((rsp + 0x60));
        rdx = *(obj.decimal_point_length);
        *((rsp + 0x20)) = rdx;
        eax = strncmp (rbp, *(obj.decimal_point), rdx);
        rdx = *((rsp + 0x20));
        *(fp_stack--) = fp_stack[?];
        *((rsp + 0x10)) = 0;
        if (eax == 0) {
            goto label_50;
        }
label_4:
        if (*(obj.dev_debug) != 0) {
            goto label_51;
        }
label_3:
        ecx = *(rbp);
        *((rsp + 0x20)) = cl;
        if (cl != 0) {
            goto label_52;
        }
        if (r15d == 4) {
            goto label_53;
        }
        ecx = *(obj.dev_debug);
        r8d = 0;
        *(fp_stack--) = 1.0;
label_2:
        if (cl != 0) {
            goto label_54;
        }
        fp_stack[0] *= fp_stack[1];
        fp_stack++;
        *(fp_stack--) = fp_stack[0];
        ? = fp_stack[0];
        fp_stack--;
label_12:
        if (*(rbp) == 0) {
            goto label_55;
        }
        ebx = 5;
        if (*(obj.inval_style) != 3) {
            rsi = rbp;
            edi = 1;
            ? = fp_stack[0];
            fp_stack--;
            ebx = 5;
            rax = quote_n ();
            rsi = r13;
            edi = 0;
            rax = quote_n ();
            edx = 5;
            r13 = rax;
            rax = dcgettext (0, "invalid suffix in input %s: %s");
            r8 = rbp;
            rcx = r13;
            eax = 0;
            error (*(obj.conv_exit_code), 0, rax);
            *(fp_stack--) = fp_stack[?];
        }
label_6:
        rax = from_unit_size;
        rdx = to_unit_size;
        if (rax == 1) {
            goto label_56;
        }
label_7:
        *((rsp + 0x20)) = rax;
        *(fp_stack--) = *((rsp + 0x20));
        if (rax < 0) {
            fp_stack[0] += *(0x0000cdbc);
        }
        fp_stack[0] *= fp_stack[1];
        fp_stack++;
        *((rsp + 0x20)) = rdx;
        *(fp_stack--) = *((rsp + 0x20));
        if (rdx < 0) {
            goto label_57;
        }
label_1:
        fp_stack[0] /= fp_stack[1];
        fp_stack++;
label_8:
        if (ebx <= 1) {
            goto label_58;
        }
        fp_stack++;
        goto label_9;
label_19:
        fp_stack++;
label_9:
        rsi = stdout;
        rdi = r12;
        r12d = 0;
        eax = fputs_unlocked ();
        void (*0x394d)() ();
label_48:
        rcx = padding_width;
        rdi = stderr;
        rdx = "setting Auto-Padding to %ld characters\n";
        eax = 0;
        esi = 1;
        fprintf_chk ();
label_46:
        r15d = scale_from;
        xmm1 = 0;
        eax = *(obj.dev_debug);
        *((rsp + 0x60)) = 0;
        *(rsp) = xmm1;
        *(fp_stack--) = *(rsp);
        edx = r15 - 3;
        r14d -= r14d;
        ? = fp_stack[0];
        fp_stack--;
        r14d &= 0x18;
        r14d += 0x3e8;
    } while (al == 0);
    rsi = decimal_point;
    edi = 1;
    rax = quote_n ();
    rsi = r13;
    edi = 0;
    rbx = rax;
    rax = quote_n ();
    rdi = stderr;
    r8 = rbx;
    r9d = 0x12;
    rcx = rax;
    rdx = "simple_strtod_human:\n  input string: %s\n  locale decimal-point: %s\n  MAX_UNSCALED_DIGITS: %d\n";
    esi = 1;
    eax = 0;
    fprintf_chk ();
    goto label_0;
label_57:
    fp_stack[0] += *(0x0000cdbc);
    goto label_1;
label_52:
    ? = fp_stack[0];
    fp_stack--;
    rax = ctype_b_loc ();
    ecx = *((rsp + 0x20));
    *(fp_stack--) = fp_stack[?];
    edi = 0;
    rsi = *(rax);
    while ((*((rsi + rax*2)) & 1) != 0) {
        ecx = *((rdx + 1));
        edi = 1;
        eax = (int32_t) cl;
        rdx = rbp;
        rbp = rbp + 1;
    }
    if (dil != 0) {
        *((rsp + 0x60)) = rdx;
    }
    esi = (int32_t) cl;
    ? = fp_stack[0];
    fp_stack--;
    *((rsp + 0x40)) = rdx;
    *((rsp + 0x30)) = cl;
    rax = strchr ("KMGTPEZY", rsi);
    *(fp_stack--) = fp_stack[?];
    if (rax == 0) {
        goto label_59;
    }
    if (r15d == 0) {
        goto label_60;
    }
    ecx = *((rsp + 0x30));
    r8d = 0;
    rdx = *((rsp + 0x40));
    ecx -= 0x45;
    if (cl <= 0x15) {
        ecx = (int32_t) cl;
        rax = obj_CSWTCH_313;
        r8d = *((rax + rcx*4));
    }
    rbp = rdx + 1;
    *((rsp + 0x60)) = rbp;
    if (r15d == 1) {
        goto label_61;
    }
    if (r15d == 4) {
        goto label_62;
    }
label_14:
    *((rsp + 0x10)) = r14d;
    *(fp_stack--) = *((rsp + 0x10));
    ecx = *(obj.dev_debug);
label_23:
    if (r8d == 0) {
        goto label_63;
    }
    eax = r8d;
    *(fp_stack--) = fp_stack[0];
    eax--;
    if (eax == 0) {
        goto label_64;
    }
    do {
        fp_stack[0] *= fp_stack[1];
        eax--;
    } while (eax != 0);
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    goto label_2;
label_51:
    rdi = stderr;
    rdx = "  parsed numeric value: %Lf\n  input precision = %d\n";
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ecx = *((rsp + 0x20));
    esi = 1;
    ? = fp_stack[0];
    fp_stack--;
    fprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    goto label_3;
label_50:
    ? = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
    *((rsp + 0x60)) = rdi;
    ? = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = rdi;
    eax = simple_strtod_int (rbp + rdx, rsp + 0x68, rsp + 0x80, rsp + 0x5f);
    if (eax > 1) {
        goto label_65;
    }
    *(fp_stack--) = fp_stack[?];
    eax = 1;
    rdi = *((rsp + 0x10));
    if (eax == 1) {
        ebx = eax;
    }
    if (*((rsp + 0x5f)) != 0) {
        goto label_66;
    }
    rbp = *((rsp + 0x68));
    *(fp_stack--) = fp_stack[?];
    rax = rbp;
    rax -= rdi;
    *((rsp + 0x10)) = rax;
    if (ebp == edi) {
        goto label_31;
    }
    eax--;
    if (eax == 0) {
        goto label_67;
    }
    *(fp_stack--) = *(0x0000cdb4);
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        eax--;
    } while (eax != 0);
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
label_31:
    if (*((rsp + 0x5e)) == 0) {
        goto label_68;
    }
    fp_stack[0] -= fp_stack[1];
    fp_stack++;
label_25:
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    *((rsp + 0x60)) = rbp;
    goto label_4;
label_47:
    rax = strlen (r12);
    *(obj.padding_width) = rax;
    if (rax < *(obj.padding_buffer_size)) {
        goto label_5;
    }
    rdi = padding_buffer;
    rsi = rax + 1;
    *(obj.padding_buffer_size) = rsi;
    rax = xrealloc ();
    *(obj.padding_buffer) = rax;
    goto label_5;
label_55:
    if (ebx != 1) {
        goto label_6;
    }
    if (*(obj.debug) == 0) {
        goto label_6;
    }
    ? = fp_stack[0];
    fp_stack--;
    rax = quote (r13, rsi, rdx, rcx, r8);
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, "large input value %s: possible precision loss");
    rcx = r13;
    eax = 0;
    eax = error (0, 0, rax);
    *(fp_stack--) = fp_stack[?];
    goto label_6;
label_65:
    ebx = eax;
label_49:
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[0];
    if (ebx > 6) {
        goto label_69;
    }
    rdx = 0x0000cba0;
    eax = ebx;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (7 cases) at 0xcba0 */
    rax = void (*rax)() ();
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    rbp = "value too large to be converted: %s";
label_13:
    if (*(obj.inval_style) == 3) {
        goto label_6;
    }
    ? = fp_stack[0];
    fp_stack--;
    rax = quote (r13, rsi, rdx, rcx, r8);
    edx = 5;
    r13 = rax;
    rax = dcgettext (0, rbp);
    rcx = r13;
    eax = 0;
    error (*(obj.conv_exit_code), 0, rax);
    *(fp_stack--) = fp_stack[?];
    goto label_6;
label_56:
    if (rdx != 1) {
        goto label_7;
    }
    goto label_8;
label_58:
    rax = user_precision;
    *(fp_stack--) = fp_stack[?];
    fp_tmp_2 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_2;
    if (rax == -1) {
        rax = *((rsp + 0x10));
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = rax;
    if (fp_stack[0] < fp_stack[1]) {
        goto label_70;
    }
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    if (fp_stack[0] < fp_stack[1]) {
        goto label_70;
    }
    *(fp_stack--) = fp_stack[0];
    eax = 0;
    *(fp_stack--) = *(0x0000cdb4);
    while (eax >= 0) {
        fp_stack[0] /= fp_stack[2];
        fp_stack++;
        eax++;
        *(fp_stack--) = 0.0;
        *(fp_stack--) = fp_stack[2];
        fp_stack[0] = -fp_stack[0];
        fp_tmp_3 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_3;
        __asm ("fcompi st(3)");
        __asm ("fcmovbe st(0), st(2)");
        *(fp_stack--) = fp_stack[1];
        fp_tmp_4 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_4;
        __asm ("fcompi st(1)");
    }
    fp_stack++;
    fp_stack++;
    fp_stack++;
    ebp = scale_to;
    if (ebp == 0) {
        edx = eax;
        rdx += *((rsp + 0x10));
        if (rdx > 0x12) {
            goto label_71;
        }
    }
    if (eax <= 0x1a) {
        goto label_72;
    }
    ? = fp_stack[0];
    fp_stack--;
    if (*(obj.inval_style) == 3) {
        goto label_9;
    }
    edx = 5;
    rax = dcgettext (0, "value too large to be printed: '%Lg' (cannot handle values > 999Y)");
    *(fp_stack--) = fp_stack[?];
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    error (*(obj.conv_exit_code), 0, rax);
    goto label_9;
label_70:
    ebp = scale_to;
    if (ebp == 0) {
        if (*((rsp + 0x10)) > 0x12) {
            goto label_71;
        }
    }
label_72:
    r14d = grouping;
    *((rsp + 0x90)) = 0x25;
    r12 = rsp + 0x91;
    r13d = round_style;
    if (r14d != 0) {
        *((rsp + 0x91)) = 0x27;
        r12 = rsp + 0x92;
    }
    r9 = zero_padding_width;
    if (r9 != 0) {
        goto label_73;
    }
label_18:
    ecx = *(obj.dev_debug);
    if (cl != 0) {
        goto label_74;
    }
label_17:
    if (ebp == 0) {
        goto label_75;
    }
    eax = rbp - 3;
    if (eax <= 1) {
        goto label_76;
    }
    *((rsp + 0x10)) = 0x3e8;
    xmm0 = *(0x0000cdd0);
label_26:
    *(fp_stack--) = fp_stack[?];
    fp_tmp_5 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_5;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    if (fp_stack[0] < fp_stack[1]) {
        goto label_77;
    }
    *(fp_stack--) = fp_stack[?];
    __asm ("fcompi st(1)");
    if (fp_stack[0] < fp_stack[1]) {
        goto label_77;
    }
    *(fp_stack--) = *((rsp + 0x10));
    ebx = 0;
    *(fp_stack--) = fp_stack[1];
    while (ebx >= 0) {
        fp_stack[0] /= fp_stack[1];
        ebx++;
        *(fp_stack--) = 0.0;
        *(fp_stack--) = fp_stack[1];
        fp_stack[0] = -fp_stack[0];
        fp_tmp_6 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_6;
        __asm ("fcompi st(2)");
        __asm ("fcmovbe st(0), st(1)");
        __asm ("fcompi st(2)");
    }
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_24:
    if (cl != 0) {
        goto label_78;
    }
label_30:
    rsi = user_precision;
    if (rsi == -1) {
        goto label_79;
    }
    eax = rbx * 3;
    if (rax > rsi) {
        rax = rsi;
    }
    edi = eax;
    if (eax == 0) {
        goto label_80;
    }
    eax--;
    if (eax == 0) {
        goto label_81;
    }
    *(fp_stack--) = *(0x0000cdb4);
    edx = eax;
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        edx--;
    } while (edx != 0);
    fp_stack[0] *= fp_stack[2];
    fp_stack++;
    *(fp_stack--) = fp_stack[?];
    edx = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[2];
    dh |= 0xc;
    *((rsp + 0x4c)) = dx;
    fp_stack[0] /= fp_stack[1];
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    fp_stack[2] -= fp_stack[0];
    if (r13d > 4) {
        goto label_10;
    }
    r8 = 0x0000cbbc;
    rdx = *((r8 + r13*4));
    rdx += r8;
    /* switch table (5 cases) at 0xcbbc */
    void (*rdx)() ();
label_42:
    if (dl == 0) {
        goto label_11;
    }
    eax = fwrite (0x0000b086, 1, 0x16, *(obj.stderr));
    goto label_11;
label_54:
    fp_tmp_7 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_7;
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    ecx = r14d;
    rdi = stderr;
    rdx = "  suffix power=%d^%d = %Lf\n";
    esi = 1;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ? = fp_stack[0];
    fp_stack--;
    eax = fprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    if (*(obj.dev_debug) == 0) {
        goto label_12;
    }
    rdi = stderr;
    rdx = "  returning value: %Lf (%LG)\n";
    eax = 0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    esi = 1;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ? = fp_stack[0];
    fp_stack--;
    fprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    goto label_12;
label_53:
    ebx = 6;
    rbp = "missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)";
    goto label_13;
label_62:
    if (*((rdx + 1)) != 0x69) {
        goto label_82;
    }
    rbp = rdx + 2;
    *((rsp + 0x60)) = rbp;
    goto label_14;
label_69:
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    goto label_83;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_83:
    *((rsp + 0x10)) = 0;
    ebp = 0;
    goto label_13;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    rbp = "missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)";
    goto label_13;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    rbp = "invalid suffix in input: %s";
    goto label_13;
label_66:
    fp_stack++;
    *(fp_stack--) = fp_stack[?];
    goto label_84;
    fp_stack++;
label_84:
    *((rsp + 0x10)) = 0;
    ebx = 3;
    rbp = "invalid number: %s";
    goto label_13;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    rbp = "rejecting suffix in input: %s (consider using --from)";
    goto label_13;
label_75:
    rax = *((rsp + 0x10));
    esi = eax;
    if (eax == 0) {
        goto label_85;
    }
    eax--;
    if (eax == 0) {
        goto label_86;
    }
    *(fp_stack--) = *(0x0000cdb4);
    edx = eax;
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        edx--;
    } while (edx != 0);
    fp_stack[0] *= fp_stack[2];
    *(fp_stack--) = fp_stack[?];
    edx = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[1];
    dh |= 0xc;
    fp_stack[0] /= fp_stack[1];
    *((rsp + 0x4c)) = dx;
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x20));
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    fp_stack[1] -= fp_stack[0];
    if (r13d > 4) {
        goto label_15;
    }
    fp_stack[2] = fp_stack[0];
    fp_stack--;
    fp_tmp_8 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_8;
    rax = *((rdx + r13*4));
    rax += rdx;
    /* switch table (5 cases) at 0xcbd0 */
    void (*rax)() ();
label_43:
    rax = quote (r13, rsi, 0x0000cbd0, rcx, r8);
    rdi = stderr;
    esi = 1;
    rdx = 0x0000b072;
    rcx = rax;
    eax = 0;
    eax = fprintf_chk ();
    goto label_11;
label_44:
    r13 = r12;
    eax = 0;
    goto label_16;
label_74:
    ? = fp_stack[0];
    fp_stack--;
    eax = fwrite ("double_to_human:\n", 1, 0x11, *(obj.stderr));
    ecx = *(obj.dev_debug);
    *(fp_stack--) = fp_stack[?];
    goto label_17;
label_73:
    rdi = r12;
    r8 = "0%ld";
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    ecx = 0x3f;
    edx = 1;
    esi = 0x3e;
    rax = snprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    rax = (int64_t) eax;
    r12 += rax;
    goto label_18;
label_71:
    if (*(obj.inval_style) == 3) {
        goto label_19;
    }
    rbx = *((rsp + 0x10));
    ? = fp_stack[0];
    fp_stack--;
    ebp = conv_exit_code;
    edx = 5;
    if (rbx == 0) {
        goto label_87;
    }
    rax = dcgettext (0, "value/precision too large to be printed: '%Lg/%lu' (consider using --to)");
    *(fp_stack--) = fp_stack[?];
    rcx = rbx;
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    error (ebp, 0, rax);
    goto label_9;
    edi = 0;
    goto label_34;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_34:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(2)");
    if (rbx > 0) {
        goto label_88;
    }
    fp_tmp_9 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_9;
    goto label_28;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_tmp_10 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_10;
label_28:
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_tmp_11 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_11;
    __asm ("fcompi st(1)");
    if (rbx > 0) {
        fp_stack++;
        rax = *((rsp + 0x10));
        rax++;
        *((rsp + 0x10)) = rax;
        *(fp_stack--) = *((rsp + 0x10));
    }
label_21:
    fp_stack[0] += fp_stack[1];
    fp_stack++;
    if (edi == 0) {
        void (*0x4543)() ();
    }
    eax = edi;
    *(fp_stack--) = *(0x0000cdb4);
    eax--;
    if (eax == 0) {
        void (*0x4a91)() ();
    }
label_22:
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        eax--;
    } while (eax != 0);
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    fp_tmp_12 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_12;
    goto label_89;
    fp_tmp_13 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_13;
label_89:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(2)");
    if (eax > 0) {
        goto label_90;
    }
    fp_tmp_14 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_14;
    fp_stack[0] += *(0x0000cdc0);
    goto label_91;
    fp_tmp_15 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_15;
label_91:
    *(rsp) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
label_20:
    fp_stack[0] += fp_stack[1];
    fp_stack++;
    if (esi == 0) {
        goto label_32;
    }
    eax = esi;
    *(fp_stack--) = *(0x0000cdb4);
    eax--;
    if (eax == 0) {
        goto label_92;
    }
label_38:
    *(fp_stack--) = fp_stack[0];
    do {
        fp_stack[0] *= fp_stack[1];
        eax--;
    } while (eax != 0);
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
label_32:
    ebx = *((rsp + 0x10));
    if (cl != 0) {
        fp_tmp_16 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_16;
        ? = fp_stack[0];
        fp_stack--;
        rax = "  no scaling, returning value: %.*Lf\n";
        ecx = ebx;
        rdx = "  no scaling, returning (grouped) value: %'.*Lf\n";
        rdi = stderr;
        esi = 1;
        if (r14d == 0) {
            rdx = rax;
        }
        eax = 0;
        *(fp_stack--) = fp_stack[0];
        ? = fp_stack[0];
        fp_stack--;
        ? = fp_stack[0];
        fp_stack--;
        eax = fprintf_chk ();
        *(fp_stack--) = fp_stack[?];
        *(fp_stack--) = fp_stack[?];
    } else {
        fp_tmp_17 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_17;
    }
    ? = fp_stack[0];
    fp_stack--;
    eax = 0;
    r9d = ebx;
    ecx = 0x80;
    edx = 1;
    esi = 0x80;
    *(r12) = 0x664c2a2e;
    *((r12 + 4)) = 0;
    r12 = rsp + 0xd0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    r8 = rsp + 0xa0;
    rdi = r12;
    ? = fp_stack[0];
    fp_stack--;
    eax = snprintf_chk ();
    *(fp_stack--) = fp_stack[?];
    if (eax > 0x7f) {
        goto label_93;
    }
    fp_stack++;
    *(fp_stack--) = fp_stack[?];
    ebx = *(obj.dev_debug);
    fp_tmp_18 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_18;
    goto label_94;
    fp_tmp_19 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_19;
label_94:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(2)");
    if (eax > 0x7f) {
        goto label_95;
    }
    fp_tmp_20 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_20;
    goto label_96;
    fp_tmp_21 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_21;
label_96:
    *(fp_stack--) = fp_stack[0];
    *(rsp) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
    fp_tmp_22 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_22;
    __asm ("fcompi st(1)");
    if (eax <= 0x7f) {
        goto label_20;
    }
    fp_stack++;
    rax = *(rsp);
    rax++;
    *(rsp) = rax;
    *(fp_stack--) = *(rsp);
    goto label_20;
    fp_tmp_23 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_23;
    goto label_97;
label_95:
    fp_tmp_24 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_24;
label_97:
    fp_stack[0] = -fp_stack[0];
    edx = 0;
    *(fp_stack--) = fp_stack[0];
    *(rsp) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
    fp_tmp_25 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_25;
    rax = *(rsp);
    __asm ("fcompi st(1)");
    fp_stack++;
    dl = (rax > 0) ? 1 : 0;
    rax += rdx;
    rax = -rax;
    *(rsp) = rax;
    *(fp_stack--) = *(rsp);
    goto label_20;
    fp_tmp_26 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_26;
    edi = 0;
    goto label_35;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    fp_tmp_27 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_27;
    goto label_35;
label_88:
    fp_tmp_28 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_28;
label_35:
    fp_stack[0] = -fp_stack[0];
    edx = 0;
    *(fp_stack--) = fp_stack[0];
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_tmp_29 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_29;
    rax = *((rsp + 0x10));
    __asm ("fcompi st(1)");
    fp_stack++;
    dl = (rax > 0) ? 1 : 0;
    rax += rdx;
    rax = -rax;
    *((rsp + 0x10)) = rax;
    *(fp_stack--) = *((rsp + 0x10));
    goto label_21;
    edi = 0;
    goto label_36;
    fp_stack[1] = fp_stack[0];
    fp_stack--;
label_36:
    *(fp_stack--) = 0.0;
    __asm ("fcompi st(2)");
    if (rax > 0) {
        goto label_98;
    }
    fp_tmp_30 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_30;
    fp_stack[0] += *(0x0000cdc0);
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    goto label_21;
    fp_tmp_31 = fp_stack[2];
    fp_stack[2] = fp_stack[0];
    fp_stack[0] = fp_tmp_31;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] += fp_stack[2];
    fp_stack++;
    goto label_22;
label_61:
    ecx = *(obj.dev_debug);
    if (*((rdx + 1)) == 0x69) {
        goto label_99;
    }
    *((rsp + 0x10)) = r14d;
    *(fp_stack--) = *((rsp + 0x10));
    goto label_23;
label_60:
    ebx = 4;
    rbp = "rejecting suffix in input: %s (consider using --from)";
    goto label_13;
label_77:
    *(fp_stack--) = fp_stack[0];
    ebx = 0;
    goto label_24;
label_68:
    fp_stack[0] += fp_stack[1];
    fp_stack++;
    goto label_25;
label_59:
    ebx = 5;
    rbp = "invalid suffix in input: %s";
    goto label_13;
label_64:
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    *((rsp + 0x10)) = 0;
    r8d = 1;
    goto label_2;
label_76:
    *((rsp + 0x10)) = section..dynsym;
    xmm0 = *(0x0000cdc8);
    goto label_26;
label_29:
    fp_stack++;
label_80:
    *(fp_stack--) = fp_stack[?];
    eax = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[1];
    fp_stack[0] /= fp_stack[1];
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    fp_stack[1] -= fp_stack[0];
    if (r13d > 4) {
        goto label_27;
    }
    rdx = 0x0000cc08;
    eax = r13d;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (5 cases) at 0xcc08 */
    void (*rax)() ();
    fp_tmp_32 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_32;
    edi = r13d;
    goto label_28;
    fp_tmp_33 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_33;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] += fp_stack[1];
    fp_stack++;
    void (*0x4543)() ();
label_79:
    *(fp_stack--) = 0.0;
    *(fp_stack--) = fp_stack[1];
    fp_stack[0] = -fp_stack[0];
    fp_tmp_34 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_34;
    __asm ("fcompi st(2)");
    __asm ("fcmovbe st(0), st(1)");
    *(fp_stack--) = *(0x0000cdb4);
    *(fp_stack--) = fp_stack[0];
    __asm ("fcompi st(2)");
    fp_stack[1] = fp_stack[0];
    fp_stack--;
    if (rax <= 0) {
        goto label_29;
    }
label_33:
    fp_stack[1] *= fp_stack[0];
    *(fp_stack--) = fp_stack[?];
    eax = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[2];
    ah |= 0xc;
    fp_stack[0] /= fp_stack[1];
    *((rsp + 0x4c)) = ax;
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    fp_stack[0] *= fp_stack[1];
    fp_stack++;
    fp_stack[2] -= fp_stack[0];
    if (r13d > 4) {
        void (*0x27a0)() ();
    }
    rdx = 0x0000cc1c;
    eax = r13d;
    rax = *((rdx + rax*4));
    rax += rdx;
    /* switch table (5 cases) at 0xcc1c */
    void (*rax)() ();
label_78:
    fp_tmp_35 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_35;
    ? = fp_stack[0];
    fp_stack--;
    ecx = ebx;
    esi = 1;
    rdi = stderr;
    rdx = "  scaled value to %Lf * %0.f ^ %u\n";
    eax = 1;
    *((rsp + 0x20)) = xmm0;
    *(fp_stack--) = fp_stack[0];
    ? = fp_stack[0];
    fp_stack--;
    ? = fp_stack[0];
    fp_stack--;
    fprintf_chk ();
    ecx = *(obj.dev_debug);
    xmm0 = *((rsp + 0x10));
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = fp_stack[?];
    goto label_30;
label_99:
    rbp = rdx + 2;
    *((rsp + 0x60)) = rbp;
    if (cl == 0) {
        *(fp_stack--) = *(0x0000cdb8);
        r14d = section..dynsym;
        goto label_23;
label_85:
        *(fp_stack--) = fp_stack[?];
        eax = *((rsp + 0x4e));
        *(fp_stack--) = fp_stack[1];
        fp_stack[0] /= fp_stack[1];
        ah |= 0xc;
        *((rsp + 0x4c)) = ax;
        *((rsp + 0x20)) = fp_stack[0];
        fp_stack--;
        *(fp_stack--) = *((rsp + 0x20));
        fp_stack[0] *= fp_stack[1];
        fp_stack++;
        *(fp_stack--) = fp_stack[1];
        fp_stack[0] -= fp_stack[1];
        if (r13d > 4) {
            goto label_100;
        }
        rdx = 0x0000cc30;
        rax = *((rdx + r13*4));
        rax += rdx;
        /* switch table (5 cases) at 0xcc30 */
        void (*rax)() ();
label_87:
        rax = dcgettext (0, "value too large to be printed: '%Lg' (consider using --to)");
        *(fp_stack--) = fp_stack[?];
        eax = 0;
        ? = fp_stack[0];
        fp_stack--;
        eax = error (ebp, 0, rax);
        goto label_9;
    }
    ecx = section..dynsym;
    esi = 1;
    eax = 0;
    ? = fp_stack[0];
    fp_stack--;
    rdi = stderr;
    rdx = "  Auto-scaling, found 'i', switching to base %d\n";
    r14d = section..dynsym;
    *((rsp + 0x10)) = r8d;
    fprintf_chk ();
    r8d = *((rsp + 0x10));
    *(fp_stack--) = fp_stack[?];
    *(fp_stack--) = *(0x0000cdb8);
    ecx = *(obj.dev_debug);
    goto label_23;
label_67:
    fp_stack[0] /= *(0x0000cdb4);
    goto label_31;
label_92:
    fp_stack[0] /= fp_stack[1];
    fp_stack++;
    goto label_32;
label_98:
    fp_tmp_36 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_36;
    fp_stack[0] -= *(0x0000cdc0);
    *((rsp + 0x10)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x10));
    goto label_21;
label_90:
    fp_tmp_37 = fp_stack[1];
    fp_stack[1] = fp_stack[0];
    fp_stack[0] = fp_tmp_37;
    fp_stack[0] -= *(0x0000cdc0);
    *(rsp) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *(rsp);
    goto label_20;
label_86:
    *(fp_stack--) = fp_stack[?];
    eax = *((rsp + 0x4e));
    *(fp_stack--) = fp_stack[1];
    fp_stack[0] *= *(0x0000cdb4);
    ah |= 0xc;
    *((rsp + 0x4c)) = ax;
    *(fp_stack--) = fp_stack[0];
    fp_stack[0] /= fp_stack[2];
    *((rsp + 0x20)) = fp_stack[0];
    fp_stack--;
    *(fp_stack--) = *((rsp + 0x20));
    fp_stack[0] *= fp_stack[2];
    fp_stack++;
    fp_stack[0] -= fp_stack[1];
    if (r13d <= 4) {
        rdx = 0x0000cc44;
        rax = *((rdx + r13*4));
        rax += rdx;
        /* switch table (5 cases) at 0xcc44 */
        void (*rax)() ();
label_82:
        *((rsp + 0x10)) = 0;
        ebx = 6;
        rbp = "missing 'i' suffix in input: %s (e.g Ki/Mi/Gi)";
        goto label_13;
label_93:
        edx = 5;
        ? = fp_stack[0];
        fp_stack--;
        rax = dcgettext (0, "failed to prepare value '%Lf' for printing");
        *(fp_stack--) = fp_stack[?];
        eax = 0;
        ? = fp_stack[0];
        fp_stack--;
        error (1, 0, rax);
label_63:
        fp_stack++;
        *((rsp + 0x10)) = 0;
        *(fp_stack--) = 1.0;
        goto label_2;
label_81:
        *(fp_stack--) = *(0x0000cdb4);
        goto label_33;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        edi = 1;
        goto label_34;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        fp_tmp_38 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_38;
        edi = r13d;
        goto label_35;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        fp_tmp_39 = fp_stack[1];
        fp_stack[1] = fp_stack[0];
        fp_stack[0] = fp_tmp_39;
        edi = 1;
        goto label_28;
        fp_stack[1] = fp_stack[0];
        fp_stack--;
        edi = 1;
        goto label_36;
label_100:
        fp_stack++;
        fp_stack++;
        goto label_37;
    }
    fp_stack++;
    fp_stack++;
    goto label_37;
}