process_field (char *text, uintmax_t field)
{
  long double val = 0;
  size_t precision = 0;
  bool valid_number = true;

  if (include_field (field))
    {
      valid_number =
        process_suffixed_number (text, &val, &precision, field);

      if (valid_number)
        valid_number = prepare_padded_number (val, precision);

      if (valid_number)
        print_padded_number ();
      else
        fputs (text, stdout);
    }
  else
    fputs (text, stdout);

  return valid_number;
}