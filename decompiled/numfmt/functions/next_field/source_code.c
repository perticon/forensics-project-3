next_field (char **line)
{
  char *field_start = *line;
  char *field_end   = field_start;

  if (delimiter != DELIMITER_DEFAULT)
    {
      if (*field_start != delimiter)
        {
          while (*field_end && *field_end != delimiter)
            ++field_end;
        }
      /* else empty field */
    }
  else
    {
      /* keep any space prefix in the returned field */
      while (*field_end && field_sep (*field_end))
        ++field_end;

      while (*field_end && ! field_sep (*field_end))
        ++field_end;
    }

  *line = field_end;
  return field_start;
}