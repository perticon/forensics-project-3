prepare_padded_number (const long double val, size_t precision)
{
  /* Generate Output. */
  char buf[128];

  size_t precision_used = user_precision == -1 ? precision : user_precision;

  /* Can't reliably print too-large values without auto-scaling. */
  unsigned int x;
  expld (val, 10, &x);

  if (scale_to == scale_none
      && x + precision_used > MAX_UNSCALED_DIGITS)
    {
      if (inval_style != inval_ignore)
        {
          if (precision_used)
            error (conv_exit_code, 0,
                   _("value/precision too large to be printed: '%Lg/%"PRIuMAX"'"
                     " (consider using --to)"), val, (uintmax_t)precision_used);
          else
            error (conv_exit_code, 0,
                   _("value too large to be printed: '%Lg'"
                     " (consider using --to)"), val);
        }
      return 0;
    }

  if (x > MAX_ACCEPTABLE_DIGITS - 1)
    {
      if (inval_style != inval_ignore)
        error (conv_exit_code, 0, _("value too large to be printed: '%Lg'"
                                    " (cannot handle values > 999Y)"), val);
      return 0;
    }

  double_to_human (val, precision_used, buf, sizeof (buf),
                   scale_to, grouping, round_style);
  if (suffix)
    strncat (buf, suffix, sizeof (buf) - strlen (buf) -1);

  devmsg ("formatting output:\n  value: %Lf\n  humanized: %s\n",
          val, quote (buf));

  if (padding_width && strlen (buf) < padding_width)
    {
      size_t w = padding_width;
      mbsalign (buf, padding_buffer, padding_buffer_size, &w,
                padding_alignment, MBA_UNIBYTE_ONLY);

      devmsg ("  After padding: %s\n", quote (padding_buffer));
    }
  else
    {
      setup_padding_buffer (strlen (buf) + 1);
      strcpy (padding_buffer, buf);
    }

  return 1;
}