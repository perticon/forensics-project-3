void main(uint param_1,undefined8 *param_2)

{
  int *piVar1;
  int iVar2;
  int iVar3;
  uint uVar4;
  uint uVar5;
  size_t sVar6;
  long lVar7;
  undefined8 uVar8;
  undefined8 uVar9;
  int *piVar10;
  __ssize_t _Var11;
  ushort **ppuVar12;
  long lVar13;
  int *piVar14;
  char cVar15;
  undefined8 extraout_RDX;
  undefined8 extraout_RDX_00;
  long lVar16;
  char *pcVar17;
  ulong uVar18;
  undefined8 in_R10;
  uint uVar19;
  uint *puVar20;
  undefined *puVar21;
  long lVar22;
  char *pcVar23;
  long in_FS_OFFSET;
  bool bVar24;
  undefined auVar25 [16];
  undefined8 uStack120;
  undefined8 local_70;
  int *local_68;
  int *local_60;
  char *local_50;
  int *local_48;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  local_70 = (int *)setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  decimal_point = nl_langinfo(0x10000);
  if ((decimal_point == (char *)0x0) || (*decimal_point == '\0')) {
    decimal_point = ".";
  }
  puVar20 = &switchD_00102917::switchdataD_0010cc58;
  sVar6 = strlen(decimal_point);
  decimal_point_length = (undefined4)sVar6;
  atexit(close_stdout);
  iVar3 = delimiter;
LAB_001028e0:
  delimiter = iVar3;
  uVar18 = (ulong)param_1;
  iVar2 = getopt_long(uVar18,param_2,&DAT_0010b200,longopts,0);
  if (iVar2 == -1) {
    if ((format_str != (uint *)0x0) && (grouping != 0)) goto LAB_00103407;
    if ((debug != '\0') && (local_70 == (int *)0x0)) {
      uVar8 = dcgettext(0,"failed to set locale",5);
      error(0,0,uVar8);
    }
    if ((debug == '\0') || ((scale_from | scale_to) != 0)) {
LAB_00102c9c:
      puVar20 = format_str;
      if (format_str == (uint *)0x0) goto LAB_00102dea;
    }
    else if (grouping == 0) {
      if (padding_width == (int *)0x0) {
        if (format_str == (uint *)0x0) {
          uVar8 = dcgettext(0,"no conversion option specified",5);
          error(0,0,uVar8);
          goto LAB_00102c9c;
        }
      }
      else if (format_str == (uint *)0x0) goto LAB_00102e0d;
    }
    else if (format_str == (uint *)0x0) goto LAB_00102e00;
    puVar20 = format_str;
    local_48 = (int *)0x0;
    lVar16 = 1;
    lVar7 = 0;
    goto LAB_00102cd4;
  }
  if (0x8d < iVar2) {
switchD_00102917_caseD_102959:
    usage(1);
    uVar8 = extraout_RDX_00;
switchD_00102917_caseD_102963:
    lVar7 = __xargmatch_internal("--invalid",optarg,inval_args,inval_types,4,argmatch_die,1,uVar8);
    inval_style = *(int *)(inval_types + lVar7 * 4);
    iVar3 = delimiter;
    goto LAB_001028e0;
  }
  if (iVar2 < 100) {
    if (iVar2 == -0x83) {
      version_etc(stdout,"numfmt","GNU coreutils",Version,"Assaf Gordon",0);
                    /* WARNING: Subroutine does not return */
      exit(0);
    }
    if (iVar2 == -0x82) {
      usage(0);
    }
    goto switchD_00102917_caseD_102959;
  }
  if (0x29 < iVar2 - 100U) goto switchD_00102917_caseD_102959;
  uVar8 = extraout_RDX;
  iVar3 = delimiter;
  switch((char *)((long)puVar20 + (long)(int)puVar20[iVar2 - 100U])) {
  case (char *)0x10292d:
    dev_debug = '\x01';
switchD_00102917_caseD_102934:
    debug = '\x01';
    goto LAB_001028e0;
  case (char *)0x102934:
    goto switchD_00102917_caseD_102934;
  case (char *)0x102959:
    goto switchD_00102917_caseD_102959;
  case (char *)0x102963:
    goto switchD_00102917_caseD_102963;
  case (char *)0x1029a1:
    format_str = optarg;
    goto LAB_001028e0;
  case (char *)0x1029b4:
    if (optarg == (uint *)0x0) {
      header = 1;
      goto LAB_001028e0;
    }
    iVar3 = xstrtoumax(optarg,0,10,&header);
    if ((iVar3 == 0) && (iVar3 = delimiter, header != 0)) goto LAB_001028e0;
    puVar20 = (uint *)quote(optarg);
    uVar8 = dcgettext(0,"invalid header value %s",5);
    error(1,0,uVar8,puVar20);
    break;
  case (char *)0x102a4a:
    iVar3 = xstrtol(optarg,0,10,&padding_width);
    if ((iVar3 != 0) || (((ulong)padding_width & 0x7fffffffffffffff) == 0)) goto LAB_001033d1;
    iVar3 = delimiter;
    if ((long)padding_width < 0) {
      padding_alignment = 0;
      padding_width = (int *)-(long)padding_width;
    }
    goto LAB_001028e0;
  case (char *)0x102aad:
    grouping = 1;
    goto LAB_001028e0;
  case (char *)0x102abc:
    suffix = optarg;
    goto LAB_001028e0;
  case (char *)0x102acf:
    lVar7 = __xargmatch_internal("--round",optarg,round_args,round_types,4,argmatch_die,1,uVar18);
    round_style = *(undefined4 *)(round_types + lVar7 * 4);
    iVar3 = delimiter;
    goto LAB_001028e0;
  case (char *)0x102b19:
    to_unit_size = unit_to_umax();
    iVar3 = delimiter;
    goto LAB_001028e0;
  case (char *)0x102b31:
    lVar7 = __xargmatch_internal
                      (&DAT_0010b20b,optarg,scale_to_args,scale_to_types,4,argmatch_die,1,in_R10);
    scale_to = *(uint *)(scale_to_types + lVar7 * 4);
    iVar3 = delimiter;
    goto LAB_001028e0;
  case (char *)0x102b7b:
    from_unit_size = unit_to_umax();
    iVar3 = delimiter;
    goto LAB_001028e0;
  case (char *)0x102b93:
    lVar7 = __xargmatch_internal
                      ("--from",optarg,scale_from_args,scale_from_types,4,argmatch_die,1,
                       (char *)((long)puVar20 + (long)(int)puVar20[iVar2 - 100U]));
    scale_from = *(uint *)(scale_from_types + lVar7 * 4);
    iVar3 = delimiter;
    goto LAB_001028e0;
  case (char *)0x102bdb:
    line_delim = 0;
    goto LAB_001028e0;
  case (char *)0x102be7:
    goto switchD_00102917_caseD_102be7;
  }
  if (n_frp != 0) goto LAB_001034a5;
  set_fields(optarg,1);
  iVar3 = delimiter;
  goto LAB_001028e0;
switchD_00102917_caseD_102be7:
  cVar15 = *(char *)optarg;
  iVar3 = (int)cVar15;
  if ((cVar15 == '\0') || (*(char *)((long)optarg + 1) == '\0')) goto LAB_001028e0;
  goto LAB_0010342b;
LAB_00102cd4:
  lVar13 = lVar16 + -1;
  if (*(char *)((long)format_str + lVar7) != '%') {
    if (*(char *)((long)format_str + lVar7) != '\0') {
      lVar13 = 1;
      goto LAB_00102ccd;
    }
    uVar8 = quote(format_str);
    uVar9 = dcgettext(0,"format %s has no %% directive",5);
    error(1,0,uVar9,uVar8);
LAB_001033d1:
    puVar20 = (uint *)quote(optarg);
    uVar8 = dcgettext(0,"invalid padding value %s",5);
    error(1,0,uVar8,puVar20);
LAB_00103407:
    uVar8 = dcgettext(0,"--grouping cannot be combined with --format",5);
    error(1,0,uVar8);
LAB_0010342b:
    uVar8 = dcgettext(0,"the delimiter must be a single character",5);
    error(1,0,uVar8);
    goto LAB_0010344f;
  }
  lVar22 = lVar7 + 1;
  if (*(char *)((long)format_str + lVar7 + 1) != '%') goto LAB_00102ed0;
  lVar13 = 2;
LAB_00102ccd:
  lVar7 = lVar7 + lVar13;
  lVar16 = lVar16 + 1;
  goto LAB_00102cd4;
LAB_00102ed0:
  local_70 = (int *)((ulong)local_70 & 0xffffffffffffff00);
  local_68 = (int *)((long)format_str + lVar22);
  do {
    while( true ) {
      while( true ) {
        sVar6 = strspn((char *)local_68," ");
        lVar22 = lVar22 + sVar6;
        local_68 = (int *)((long)puVar20 + lVar22);
        if (*(char *)local_68 != '\'') break;
        grouping = 1;
        lVar22 = lVar22 + 1;
        local_68 = (int *)((long)puVar20 + lVar22);
      }
      if (*(char *)local_68 != '0') break;
      lVar22 = lVar22 + 1;
      local_70 = (int *)CONCAT71(local_70._1_7_,1);
      local_68 = (int *)((long)puVar20 + lVar22);
    }
  } while (sVar6 != 0);
  local_60 = __errno_location();
  *local_60 = 0;
  piVar10 = (int *)strtol((char *)local_68,(char **)&local_48,10);
  if ((*local_60 != 0x22) && (piVar10 != (int *)0x8000000000000000)) {
    piVar14 = local_60;
    piVar1 = padding_width;
    if ((local_48 != local_68) && (piVar10 != (int *)0x0)) {
      if ((debug == '\0') || (padding_width == (int *)0x0)) {
LAB_0010330e:
        if ((long)piVar10 < 0) {
          padding_alignment = 0;
          piVar1 = (int *)-(long)piVar10;
        }
        else {
          piVar1 = piVar10;
          if ((char)local_70 != '\0') goto LAB_00102f98;
        }
      }
      else {
        if (((char)local_70 == '\0') || ((long)piVar10 < 1)) {
          local_68 = local_60;
          uVar8 = dcgettext(0,"--format padding overriding --padding",5);
          error(0,0,uVar8);
          piVar14 = local_68;
          goto LAB_0010330e;
        }
LAB_00102f98:
        zero_padding_width = piVar10;
        piVar1 = padding_width;
      }
    }
    padding_width = piVar1;
    lVar7 = (long)local_48 - (long)puVar20;
    if (*(char *)local_48 != '\0') {
      if (*(char *)local_48 == '.') {
        *piVar14 = 0;
        pcVar17 = (char *)((long)puVar20 + lVar7 + 1);
        local_70 = piVar14;
        user_precision = strtol(pcVar17,(char **)&local_48,10);
        if ((*local_70 == 0x22) || (user_precision < 0)) goto LAB_0010355f;
        ppuVar12 = __ctype_b_loc();
        cVar15 = *pcVar17;
        if (((*(byte *)(*ppuVar12 + cVar15) & 1) != 0) || (cVar15 == '+')) goto LAB_0010355f;
        lVar7 = (long)local_48 - (long)puVar20;
      }
      if (*(char *)((long)puVar20 + lVar7) == 'f') {
        lVar7 = lVar7 + 1;
        pcVar17 = (char *)((long)puVar20 + lVar7);
        cVar15 = *pcVar17;
        if (cVar15 == '\0') {
          if (lVar13 != 0) {
LAB_00102d02:
            format_str_prefix = (char *)ximemdup0(puVar20,lVar13);
            if (*pcVar17 != '\0') goto LAB_001031e0;
          }
          if (dev_debug != '\0') {
            pcVar17 = format_str_suffix;
            if (format_str_suffix == (char *)0x0) {
              pcVar17 = "";
            }
LAB_00102d3f:
            local_68 = (int *)quote_n(2,pcVar17);
            pcVar17 = format_str_prefix;
            if (format_str_prefix == (char *)0x0) {
              pcVar17 = "";
            }
            pcVar23 = "Left";
            puVar21 = &DAT_0010b1df;
            uVar8 = quote_n(1,pcVar17);
            if (padding_alignment != 0) {
              pcVar23 = "Right";
            }
            local_70 = padding_width;
            if (grouping == 0) {
              puVar21 = &DAT_0010b1e3;
            }
            uVar9 = quote_n(0,puVar20);
            __fprintf_chk(stderr,1,
                          "format String:\n  input: %s\n  grouping: %s\n  padding width: %ld\n  alignment: %s\n  prefix: %s\n  suffix: %s\n"
                          ,uVar9,puVar21,local_70,pcVar23,uVar8,local_68,uVar9);
          }
        }
        else {
          do {
            lVar16 = 1;
            if (cVar15 == '%') {
              if (*(char *)((long)puVar20 + lVar7 + 1) != '%') goto LAB_00103473;
              lVar16 = 2;
            }
            lVar7 = lVar7 + lVar16;
            cVar15 = *(char *)((long)puVar20 + lVar7);
          } while (cVar15 != '\0');
          if (lVar13 != 0) goto LAB_00102d02;
LAB_001031e0:
          pcVar17 = (char *)xstrdup();
          format_str_suffix = pcVar17;
          if (dev_debug != '\0') goto LAB_00102d3f;
        }
LAB_00102dea:
        if (grouping != 0) {
          if (scale_to != 0) {
LAB_0010344f:
            uVar8 = dcgettext(0,"grouping cannot be combined with --to",5);
            error(1,0,uVar8);
LAB_00103473:
            puVar20 = (uint *)quote(puVar20);
            uVar8 = dcgettext(0,"format %s has too many %% directives",5);
            error(1,0,uVar8,puVar20);
LAB_001034a5:
            uVar8 = dcgettext(0,"multiple field specifications",5);
            error(1,0,uVar8);
            goto LAB_001034c9;
          }
LAB_00102e00:
          if ((debug != '\0') && (pcVar17 = nl_langinfo(0x10001), *pcVar17 == '\0')) {
            uVar8 = dcgettext(0,"grouping has no effect in this locale",5);
            error(0,0,uVar8);
          }
        }
LAB_00102e0d:
        if (padding_buffer_size <= padding_width) {
          padding_buffer_size = (int *)((long)padding_width + 1);
          padding_buffer = xrealloc();
        }
        auto_padding = 0;
        if (padding_width == (int *)0x0) {
          auto_padding = (uint)(delimiter == 0x80);
        }
        if (inval_style != 0) {
          conv_exit_code = 0;
        }
        if (optind < (int)param_1) {
          if ((debug != '\0') && (header != 0)) {
            uVar8 = dcgettext(0,"--header ignored with command-line input",5);
            error(0,0,uVar8);
          }
          uVar19 = 1;
          for (; optind < (int)param_1; optind = optind + 1) {
            uVar4 = process_line(param_2[optind],1);
            uVar19 = uVar19 & uVar4;
          }
        }
        else {
          local_50 = (char *)0x0;
          local_48 = (int *)0x0;
          while( true ) {
            lVar7 = header + -1;
            uVar4 = (uint)line_delim;
            bVar24 = header == 0;
            header = lVar7;
            if (bVar24) break;
            _Var11 = getdelim(&local_50,(size_t *)&local_48,uVar4,stdin);
            if (_Var11 < 1) {
              uVar4 = (uint)line_delim;
              break;
            }
            fputs_unlocked(local_50,stdout);
          }
          uVar19 = 1;
          while (_Var11 = getdelim(&local_50,(size_t *)&local_48,uVar4,stdin), 0 < _Var11) {
            bVar24 = (int)local_50[_Var11 + -1] == (uint)line_delim;
            if (bVar24) {
              local_50[_Var11 + -1] = '\0';
            }
            uVar5 = process_line(local_50,bVar24);
            uVar4 = (uint)line_delim;
            uVar19 = uVar19 & uVar5;
          }
          if ((*(byte *)&stdin->_flags & 0x20) != 0) {
            uVar8 = dcgettext(0,"error reading input",5);
            piVar10 = __errno_location();
            error(0,*piVar10,uVar8);
          }
        }
        if (debug == '\0') {
          if (uVar19 == 0) goto LAB_00103043;
        }
        else if (uVar19 == 0) {
          uVar8 = dcgettext(0,"failed to convert some of the input numbers",5);
          error(0,0,uVar8);
LAB_00103043:
          iVar3 = 2;
          if (1 < inval_style - 2U) goto LAB_00102ecb;
        }
        iVar3 = 0;
LAB_00102ecb:
                    /* WARNING: Subroutine does not return */
        exit(iVar3);
      }
LAB_001034c9:
      puVar20 = (uint *)quote(puVar20);
      uVar8 = dcgettext(0,"invalid format %s, directive must be %%[0][\'][-][N][.][N]f",5);
      error(1,0,uVar8,puVar20);
    }
    puVar20 = (uint *)quote(puVar20);
    uVar8 = dcgettext(0,"format %s ends in %%",5);
    error(1,0,uVar8,puVar20);
  }
  puVar20 = (uint *)quote(puVar20);
  uVar8 = dcgettext(0,"invalid format %s (width overflow)",5);
  error(1,0,uVar8,puVar20);
LAB_0010355f:
  uVar8 = quote(puVar20);
  uVar9 = dcgettext(0,"invalid precision in format %s",5);
  auVar25 = error(1,0,uVar9,uVar8);
  uVar8 = uStack120;
  uStack120 = SUB168(auVar25,0);
  (*(code *)PTR___libc_start_main_00110fd8)
            (main,uVar8,&local_70,0,0,SUB168(auVar25 >> 0x40,0),&uStack120);
  do {
                    /* WARNING: Do nothing block with infinite loop */
  } while( true );
}