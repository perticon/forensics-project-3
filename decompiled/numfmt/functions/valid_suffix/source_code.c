valid_suffix (const char suf)
{
  static char const *valid_suffixes = "KMGTPEZY";
  return (strchr (valid_suffixes, suf) != NULL);
}