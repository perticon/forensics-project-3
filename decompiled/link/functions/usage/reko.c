void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002560(fn0000000000002390(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l00000000000028FE;
	}
	fn00000000000024F0(fn0000000000002390(0x05, "Usage: %s FILE1 FILE2\n  or:  %s OPTION\n", null), 0x01);
	fn0000000000002430(stdout, fn0000000000002390(0x05, "Call the link function to create a link named FILE2 to an existing FILE1.\n\n", null));
	fn0000000000002430(stdout, fn0000000000002390(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002430(stdout, fn0000000000002390(0x05, "      --version     output version information and exit\n", null));
	struct Eq_627 * rbx_140 = fp - 0xB8 + 16;
	do
	{
		char * rsi_142 = rbx_140->qw0000;
		++rbx_140;
	} while (rsi_142 != null && fn0000000000002450(rsi_142, "link") != 0x00);
	ptr64 r13_155 = rbx_140->qw0008;
	if (r13_155 != 0x00)
	{
		fn00000000000024F0(fn0000000000002390(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_242 = fn00000000000024E0(null, 0x05);
		if (rax_242 == 0x00 || fn0000000000002320(0x03, "en_", rax_242) == 0x00)
			goto l0000000000002AB6;
	}
	else
	{
		fn00000000000024F0(fn0000000000002390(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_26 rax_184 = fn00000000000024E0(null, 0x05);
		if (rax_184 == 0x00 || fn0000000000002320(0x03, "en_", rax_184) == 0x00)
		{
			fn00000000000024F0(fn0000000000002390(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002AF3:
			fn00000000000024F0(fn0000000000002390(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l00000000000028FE:
			fn0000000000002540(edi);
		}
		r13_155 = 0x7004;
	}
	fn0000000000002430(stdout, fn0000000000002390(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002AB6:
	fn00000000000024F0(fn0000000000002390(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002AF3;
}