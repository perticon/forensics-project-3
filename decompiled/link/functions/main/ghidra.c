undefined8 main(int param_1,undefined8 *param_2)

{
  int iVar1;
  undefined8 uVar2;
  undefined8 uVar3;
  int *piVar4;
  long lVar5;
  long extraout_RDX;
  char *pcVar6;
  undefined8 uVar7;
  
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  uVar7 = 0x102636;
  atexit(close_stdout);
  parse_gnu_standard_options_only
            (param_1,param_2,&DAT_00107004,"GNU coreutils",Version,1,usage,"Michael Stone",0,uVar7);
  if (param_1 <= optind + 1) {
    if (param_1 <= optind) {
      uVar7 = dcgettext(0,"missing operand",5);
      error(0,0,uVar7);
      goto LAB_001026df;
    }
    uVar7 = quote(param_2[optind]);
    pcVar6 = "missing operand after %s";
    goto LAB_0010276c;
  }
  lVar5 = (long)optind;
  if (optind + 2 < param_1) goto LAB_00102753;
  iVar1 = link((char *)param_2[lVar5],(char *)param_2[lVar5 + 1]);
  if (iVar1 == 0) {
    return 0;
  }
  do {
    uVar7 = quotearg_n_style(1,4,param_2[optind]);
    uVar2 = quotearg_n_style(0,4,param_2[(long)optind + 1]);
    uVar3 = dcgettext(0,"cannot create link %s to %s",5);
    piVar4 = __errno_location();
    error(1,*piVar4,uVar3,uVar2,uVar7);
    lVar5 = extraout_RDX;
LAB_00102753:
    uVar7 = quote(param_2[lVar5 + 2]);
    pcVar6 = "extra operand %s";
LAB_0010276c:
    uVar2 = dcgettext(0,pcVar6,5);
    error(0,0,uVar2,uVar7);
LAB_001026df:
    usage(1);
  } while( true );
}