void usage(word32 edi)
{
	ptr64 fp;
	if (edi != 0x00)
	{
		fn0000000000002660(fn0000000000002430(0x05, "Try '%s --help' for more information.\n", null), 0x01, stderr);
		goto l0000000000002B5E;
	}
	fn00000000000025E0(fn0000000000002430(0x05, "Usage: %s [OPTION]... FILE...\n", null), 0x01);
	fn0000000000002500(stdout, fn0000000000002430(0x05, "Print value of a symbolic link or canonical file name\n\n", null));
	fn0000000000002500(stdout, fn0000000000002430(0x05, "  -f, --canonicalize            canonicalize by following every symlink in\n                                every component of the given name recursively;\n                                all but the last component must exist\n  -e, --canonicalize-existing   canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                all components must exist\n", null));
	fn0000000000002500(stdout, fn0000000000002430(0x05, "  -m, --canonicalize-missing    canonicalize by following every symlink in\n                                every component of the given name recursively,\n                                without requirements on components existence\n  -n, --no-newline              do not output the trailing delimiter\n  -q, --quiet\n  -s, --silent                  suppress most error messages (on by default)\n  -v, --verbose                 report error messages\n  -z, --zero                    end each output line with NUL, not newline\n", null));
	fn0000000000002500(stdout, fn0000000000002430(0x05, "      --help        display this help and exit\n", null));
	fn0000000000002500(stdout, fn0000000000002430(0x05, "      --version     output version information and exit\n", null));
	struct Eq_779 * rbx_165 = fp - 0xB8 + 16;
	do
	{
		char * rsi_167 = rbx_165->qw0000;
		++rbx_165;
	} while (rsi_167 != null && fn0000000000002530(rsi_167, "readlink") != 0x00);
	ptr64 r13_180 = rbx_165->qw0008;
	if (r13_180 != 0x00)
	{
		fn00000000000025E0(fn0000000000002430(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_11 rax_267 = fn00000000000025D0(null, 0x05);
		if (rax_267 == 0x00 || fn00000000000023A0(0x03, "en_", rax_267) == 0x00)
			goto l0000000000002D5E;
	}
	else
	{
		fn00000000000025E0(fn0000000000002430(0x05, "\n%s online help: <%s>\n", null), 0x01);
		Eq_11 rax_209 = fn00000000000025D0(null, 0x05);
		if (rax_209 == 0x00 || fn00000000000023A0(0x03, "en_", rax_209) == 0x00)
		{
			fn00000000000025E0(fn0000000000002430(0x05, "Full documentation <%s%s>\n", null), 0x01);
l0000000000002D9B:
			fn00000000000025E0(fn0000000000002430(0x05, "or available locally via: info '(coreutils) %s%s'\n", null), 0x01);
l0000000000002B5E:
			fn0000000000002640(edi);
		}
		r13_180 = 0x9004;
	}
	fn0000000000002500(stdout, fn0000000000002430(0x05, "Report any translation bugs to <https://translationproject.org/team/>\n", null));
l0000000000002D5E:
	fn00000000000025E0(fn0000000000002430(0x05, "Full documentation <%s%s>\n", null), 0x01);
	goto l0000000000002D9B;
}