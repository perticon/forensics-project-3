undefined8 main(int param_1,undefined8 *param_2)

{
  char *pcVar1;
  undefined8 uVar2;
  bool bVar3;
  int iVar4;
  char *__s;
  int *piVar5;
  undefined8 uVar6;
  int iVar7;
  
  iVar7 = -1;
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdout);
  bVar3 = false;
  while (iVar4 = getopt_long(param_1,param_2,"efmnqsvz",longopts,0), iVar4 != -1) {
    if (0x7a < iVar4) goto switchD_001027cd_caseD_67;
    if (iVar4 < 0x65) {
      if (iVar4 == -0x83) {
        version_etc(stdout,"readlink","GNU coreutils",Version,"Dmitry V. Levin",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar4 != -0x82) goto switchD_001027cd_caseD_67;
      usage();
switchD_001027cd_caseD_6d:
      iVar7 = 2;
    }
    else {
      switch(iVar4) {
      case 0x65:
        iVar7 = 0;
        break;
      case 0x66:
        iVar7 = 1;
        break;
      default:
        goto switchD_001027cd_caseD_67;
      case 0x6d:
        goto switchD_001027cd_caseD_6d;
      case 0x6e:
        no_newline = '\x01';
        break;
      case 0x71:
      case 0x73:
        verbose = '\0';
        break;
      case 0x76:
        verbose = '\x01';
        break;
      case 0x7a:
        bVar3 = true;
      }
    }
  }
  if (optind < param_1) {
    if (param_1 - optind == 1) goto LAB_001028aa;
    if (no_newline != '\0') goto LAB_001029cf;
  }
  else {
    uVar6 = dcgettext(0,"missing operand",5);
    error(0,0,uVar6);
switchD_001027cd_caseD_67:
    usage(1);
LAB_001029cf:
    uVar6 = dcgettext(0,"ignoring --no-newline with multiple arguments",5);
    error(0,0,uVar6);
  }
  no_newline = '\0';
  if (param_1 <= optind) {
    no_newline = 0;
    return 0;
  }
LAB_001028aa:
  uVar6 = 0;
  do {
    uVar2 = param_2[optind];
    if (iVar7 == -1) {
      __s = (char *)areadlink_with_size(uVar2,0x3f);
    }
    else {
      __s = (char *)canonicalize_filename_mode(uVar2,iVar7);
    }
    if (__s == (char *)0x0) {
      uVar6 = 1;
      if (verbose != '\0') {
        quotearg_n_style_colon(0,3,uVar2);
        piVar5 = __errno_location();
        error(0,*piVar5,"%s");
      }
    }
    else {
      fputs_unlocked(__s,stdout);
      if (no_newline == '\0') {
        pcVar1 = stdout->_IO_write_ptr;
        if (stdout->_IO_write_end < pcVar1 || stdout->_IO_write_end == pcVar1) {
          __overflow(stdout,-(uint)!bVar3 & 10);
        }
        else {
          stdout->_IO_write_ptr = pcVar1 + 1;
          *pcVar1 = (char)(-(uint)!bVar3 & 10);
        }
      }
      free(__s);
    }
    optind = optind + 1;
  } while (optind < param_1);
  return uVar6;
}