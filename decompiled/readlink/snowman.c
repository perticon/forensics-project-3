
void** g28;

int32_t* fun_2390();

signed char gl_scratch_buffer_grow_preserve(void** rdi, void** rsi);

void** fun_25f0(void** rdi, void** rsi);

void** fun_23f0(void** rdi, void** rsi, void** rdx);

signed char gl_scratch_buffer_grow(void** rdi, void** rsi);

void** fun_2450(void** rdi, ...);

int32_t fun_2550(void** rdi, void** rsi);

int64_t hash_initialize(int64_t rdi);

signed char seen_file(int64_t rdi, void** rsi);

void fun_2600(void* rdi, void** rsi);

void** fun_2560(void** rdi, void** rsi, void** rdx, int64_t rcx);

void record_file(int64_t rdi, void** rsi);

int32_t fun_23e0(int64_t rdi, void** rsi);

void* fun_24e0(void** rdi, void** rsi);

uint16_t dir_suffix = 47;

void fun_2370(void** rdi, void** rsi, ...);

void** gl_scratch_buffer_dupfree(void** rdi);

void xalloc_die();

void hash_free();

int64_t fun_24d0(void** rdi, void** rsi);

void fun_2460();

void** fun_2510(void** rdi);

void** canonicalize_filename_mode_stk(void** rdi, uint32_t esi, void** rdx) {
    int64_t r14_4;
    void*** rsp5;
    void** rax6;
    void** v7;
    void** rbp8;
    int32_t* rax9;
    void** rax10;
    int32_t* rax11;
    void** rax12;
    void** rbx13;
    void** r12_14;
    void** v15;
    void** rsi16;
    void** v17;
    void** rax18;
    void** v19;
    void** v20;
    void** rax21;
    int1_t zf22;
    void** r13_23;
    void** v24;
    void** r15_25;
    uint32_t eax26;
    int64_t v27;
    uint32_t v28;
    signed char v29;
    int32_t v30;
    void** v31;
    void** v32;
    void** rcx33;
    void** v34;
    void** v35;
    void** rcx36;
    void** r13_37;
    signed char al38;
    void** rax39;
    int64_t rcx40;
    void** v41;
    void** r13_42;
    void** v43;
    void** v44;
    void** rax45;
    signed char al46;
    void** r10_47;
    void** r9_48;
    int32_t* rax49;
    void** rdx50;
    void** rsi51;
    uint32_t ecx52;
    uint32_t ecx53;
    void** rax54;
    void** r9_55;
    void** r8_56;
    void* v57;
    void** r10_58;
    void** rax59;
    void** rax60;
    void** r11_61;
    void** rdi62;
    int32_t eax63;
    void** r11_64;
    uint32_t eax65;
    void** r10_66;
    void** r9_67;
    int64_t rax68;
    signed char al69;
    void** v70;
    void** v71;
    void** v72;
    signed char al73;
    void** rax74;
    signed char v75;
    int32_t eax76;
    int32_t* rax77;
    void* rax78;
    uint32_t edx79;
    uint32_t eax80;
    uint64_t rax81;
    int32_t* rax82;
    int64_t rax83;
    void* rsp84;
    int32_t* rax85;
    signed char al86;
    void* rdx87;
    void** rdi88;
    void** rax89;

    *reinterpret_cast<uint32_t*>(&r14_4) = esi & 3;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_4) + 4) = 0;
    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0x928);
    rax6 = g28;
    v7 = rax6;
    if (static_cast<uint32_t>(r14_4 - 1) & *reinterpret_cast<uint32_t*>(&r14_4) || (rbp8 = rdi, rdi == 0)) {
        rax9 = fun_2390();
        *rax9 = 22;
        *reinterpret_cast<int32_t*>(&rax10) = 0;
        *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
    } else {
        if (!*reinterpret_cast<void***>(rdi)) {
            rax11 = fun_2390();
            *rax11 = 2;
            *reinterpret_cast<int32_t*>(&rax10) = 0;
            *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
        } else {
            rax12 = reinterpret_cast<void**>(rsp5 + 0x100);
            *reinterpret_cast<uint32_t*>(&rbx13) = esi;
            *reinterpret_cast<void***>(rdx + 8) = reinterpret_cast<void**>(0x400);
            r12_14 = rdx;
            v15 = rax12;
            *reinterpret_cast<int32_t*>(&rsi16) = 0x400;
            *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
            v17 = rax12;
            rax18 = reinterpret_cast<void**>(rsp5 + 0x510);
            v19 = rax18;
            v20 = rax18;
            rax21 = rdx + 16;
            *reinterpret_cast<void***>(rdx) = rax21;
            zf22 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 47);
            r13_23 = rax21;
            v24 = rax21;
            if (zf22) {
                while (1) {
                    *reinterpret_cast<void***>(rdx + 16) = reinterpret_cast<void**>(47);
                    r15_25 = v24;
                    r13_23 = rdx + 17;
                    addr_323d_7:
                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
                    if (!*reinterpret_cast<signed char*>(&eax26)) {
                        if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r15_25 + 1) || !reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47)) {
                            *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                        } else {
                            --r13_23;
                            *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                        }
                    } else {
                        v27 = 0;
                        v28 = *reinterpret_cast<uint32_t*>(&rbx13) & 4;
                        v29 = 0;
                        v30 = 0;
                        v31 = reinterpret_cast<void**>(rsp5 + 0x500);
                        while (1) {
                            if (*reinterpret_cast<signed char*>(&eax26) != 47) {
                                v32 = rbp8;
                                *reinterpret_cast<uint32_t*>(&rdx) = eax26;
                                *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            } else {
                                do {
                                    *reinterpret_cast<uint32_t*>(&rdx) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8 + 1));
                                    *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                    ++rbp8;
                                } while (*reinterpret_cast<signed char*>(&rdx) == 47);
                                if (!*reinterpret_cast<signed char*>(&rdx)) 
                                    goto addr_3320_17;
                                v32 = rbp8;
                            }
                            do {
                                rbx13 = rbp8;
                                eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8 + 1));
                                ++rbp8;
                                if (!*reinterpret_cast<signed char*>(&eax26)) 
                                    break;
                            } while (*reinterpret_cast<signed char*>(&eax26) != 47);
                            rcx33 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbp8) - reinterpret_cast<unsigned char>(v32));
                            v34 = rcx33;
                            if (!rcx33) 
                                goto addr_3320_17;
                            if (rcx33 != 1) {
                                if (!reinterpret_cast<int1_t>(v34 == 2)) 
                                    goto addr_31b9_24;
                                if (*reinterpret_cast<signed char*>(&rdx) != 46) 
                                    goto addr_31b9_24;
                                if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(v32 + 1) == 46)) 
                                    goto addr_31b9_24;
                                rdx = r15_25 + 1;
                                if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(rdx)) 
                                    goto addr_3318_28;
                                --r13_23;
                                if (reinterpret_cast<unsigned char>(r13_23) <= reinterpret_cast<unsigned char>(r15_25)) 
                                    goto addr_3318_28;
                                do {
                                    if (*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47) 
                                        goto addr_3318_28;
                                    --r13_23;
                                } while (r13_23 != r15_25);
                                goto addr_3318_28;
                            }
                            if (*reinterpret_cast<signed char*>(&rdx) == 46) {
                                addr_3318_28:
                                if (*reinterpret_cast<signed char*>(&eax26)) 
                                    continue; else 
                                    goto addr_3320_17;
                            } else {
                                addr_31b9_24:
                                if (*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) != 47) {
                                    *reinterpret_cast<void***>(r13_23) = reinterpret_cast<void**>(47);
                                    ++r13_23;
                                }
                            }
                            rsi16 = v34 + 2;
                            if (reinterpret_cast<unsigned char>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_14 + 8)) + reinterpret_cast<unsigned char>(r15_25)) - reinterpret_cast<unsigned char>(r13_23)) < reinterpret_cast<unsigned char>(rsi16)) {
                                v35 = rbx13;
                                rcx36 = r13_23;
                                rbx13 = rsi16;
                                r13_37 = r12_14;
                                r12_14 = rbp8;
                                do {
                                    rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx36) - reinterpret_cast<unsigned char>(r15_25));
                                    al38 = gl_scratch_buffer_grow_preserve(r13_37, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                    if (!al38) 
                                        goto addr_322a_38;
                                    r15_25 = *reinterpret_cast<void***>(r13_37);
                                    rcx36 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r15_25) + reinterpret_cast<unsigned char>(rbp8));
                                } while (reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r13_37 + 8)) - reinterpret_cast<unsigned char>(rbp8)) < reinterpret_cast<unsigned char>(rbx13));
                                rbx13 = v35;
                                rbp8 = r12_14;
                                r12_14 = r13_37;
                                r13_23 = rcx36;
                            }
                            rdx = v34;
                            rsi16 = v32;
                            rax39 = fun_25f0(r13_23, rsi16);
                            rsp5 = rsp5 - 8 + 8;
                            *reinterpret_cast<uint32_t*>(&rcx40) = v28;
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx40) + 4) = 0;
                            *reinterpret_cast<void***>(rax39) = reinterpret_cast<void**>(0);
                            r13_23 = rax39;
                            if (!*reinterpret_cast<uint32_t*>(&rcx40)) {
                                v41 = rax39;
                                r13_42 = v31;
                                v43 = rbx13;
                                v44 = rbp8;
                                do {
                                    rbx13 = v20;
                                    rbp8 = reinterpret_cast<void**>(0x3ff);
                                    rsi16 = rbx13;
                                    rdx = reinterpret_cast<void**>(0x3ff);
                                    rax45 = fun_23f0(r15_25, rsi16, 0x3ff);
                                    rsp5 = rsp5 - 8 + 8;
                                    if (reinterpret_cast<signed char>(0x3ff) > reinterpret_cast<signed char>(rax45)) 
                                        break;
                                    al46 = gl_scratch_buffer_grow(r13_42, rsi16);
                                    rsp5 = rsp5 - 8 + 8;
                                } while (al46);
                                goto addr_3523_45;
                                r10_47 = rbx13;
                                r13_23 = v41;
                                rbx13 = v43;
                                r9_48 = rax45;
                                rbp8 = v44;
                                if (reinterpret_cast<signed char>(rax45) < reinterpret_cast<signed char>(0)) 
                                    goto addr_33c2_47;
                            } else {
                                addr_33c2_47:
                                if (*reinterpret_cast<uint32_t*>(&r14_4) == 2) {
                                    addr_3440_48:
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx13 + 1));
                                    if (*reinterpret_cast<signed char*>(&eax26)) 
                                        continue; else 
                                        goto addr_344c_49;
                                } else {
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp8));
                                    if (*reinterpret_cast<signed char*>(&eax26) != 47) {
                                        addr_3480_51:
                                        *reinterpret_cast<uint32_t*>(&rdx) = v28;
                                        *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                                        if (*reinterpret_cast<uint32_t*>(&rdx)) {
                                            if (*reinterpret_cast<signed char*>(&eax26)) 
                                                continue; else 
                                                goto addr_3424_53;
                                        } else {
                                            rax49 = fun_2390();
                                            rsp5 = rsp5 - 8 + 8;
                                            if (*rax49 == 22) 
                                                goto addr_3440_48; else 
                                                goto addr_3492_55;
                                        }
                                    } else {
                                        rdx50 = rbp8;
                                        while (1) {
                                            rsi51 = rdx50;
                                            ecx52 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx50 + 1));
                                            ++rdx50;
                                            if (*reinterpret_cast<signed char*>(&ecx52) == 47) 
                                                continue;
                                            rsi16 = rsi51 + 2;
                                            if (!*reinterpret_cast<signed char*>(&ecx52)) 
                                                goto addr_3528_59;
                                            if (*reinterpret_cast<signed char*>(&ecx52) != 46) 
                                                goto addr_3480_51;
                                            ecx53 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx50 + 1));
                                            if (!*reinterpret_cast<signed char*>(&ecx53)) 
                                                goto addr_3528_59;
                                            if (*reinterpret_cast<signed char*>(&ecx53) == 46) 
                                                goto addr_3464_63;
                                            if (*reinterpret_cast<signed char*>(&ecx53) != 47) 
                                                goto addr_3480_51;
                                            rdx50 = rsi16;
                                        }
                                    }
                                }
                            }
                            if (v30 <= 19) {
                                ++v30;
                                goto addr_363c_68;
                            }
                            if (!*reinterpret_cast<void***>(v32)) {
                                addr_363c_68:
                                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r10_47) + reinterpret_cast<unsigned char>(r9_48)) = 0;
                                if (!v29) {
                                    rax54 = fun_2450(rbp8, rbp8);
                                    rsp5 = rsp5 - 8 + 8;
                                    r9_55 = r9_48;
                                    r8_56 = v17;
                                    v57 = reinterpret_cast<void*>(0);
                                    r10_58 = r10_47;
                                    rax59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax54) + reinterpret_cast<unsigned char>(r9_55));
                                    if (reinterpret_cast<unsigned char>(rax59) < reinterpret_cast<unsigned char>(0x400)) 
                                        goto addr_3762_71;
                                } else {
                                    v57 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbp8) - reinterpret_cast<unsigned char>(v17));
                                    rax60 = fun_2450(rbp8, rbp8);
                                    rsp5 = rsp5 - 8 + 8;
                                    r9_55 = r9_48;
                                    r8_56 = v17;
                                    r10_58 = r10_47;
                                    rax59 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax60) + reinterpret_cast<unsigned char>(r9_55));
                                    if (reinterpret_cast<unsigned char>(0x400) > reinterpret_cast<unsigned char>(rax59)) 
                                        goto addr_3807_73;
                                }
                            } else {
                                r11_61 = reinterpret_cast<void**>(rsp5 + 96);
                                rdi62 = reinterpret_cast<void**>(".");
                                rsi16 = r11_61;
                                rdx = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(v32) - reinterpret_cast<unsigned char>(rbp8)) + reinterpret_cast<unsigned char>(r13_23));
                                *reinterpret_cast<void***>(rdx) = reinterpret_cast<void**>(0);
                                if (*reinterpret_cast<void***>(r15_25)) {
                                    rdi62 = r15_25;
                                }
                                eax63 = fun_2550(rdi62, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (eax63) 
                                    goto addr_34b8_77;
                                r11_64 = r11_61;
                                eax65 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v32));
                                r10_66 = r10_47;
                                r9_67 = r9_48;
                                *reinterpret_cast<void***>(rdx) = *reinterpret_cast<void***>(&eax65);
                                if (v27) 
                                    goto addr_35de_79;
                                rcx40 = 0x4c90;
                                *reinterpret_cast<int32_t*>(&rsi16) = 0;
                                *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
                                rdx = reinterpret_cast<void**>(0x4c60);
                                rax68 = hash_initialize(7);
                                rsp5 = rsp5 - 8 + 8;
                                r10_66 = r10_66;
                                r9_67 = r9_67;
                                v27 = rax68;
                                r11_64 = r11_64;
                                if (!rax68) 
                                    goto addr_3861_81;
                                addr_35de_79:
                                rsi16 = v32;
                                rdx = r11_64;
                                al69 = seen_file(v27, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (al69) 
                                    goto addr_36fc_82; else 
                                    goto addr_3616_83;
                            }
                            v70 = rbp8;
                            v71 = r10_58;
                            rbp8 = reinterpret_cast<void**>(rsp5 + 0xf0);
                            rbx13 = rax59;
                            v72 = r9_55;
                            do {
                                al73 = gl_scratch_buffer_grow_preserve(rbp8, rsi16);
                                rsp5 = rsp5 - 8 + 8;
                                if (!al73) 
                                    goto addr_36dc_86;
                                r8_56 = v17;
                            } while (reinterpret_cast<unsigned char>(0x400) <= reinterpret_cast<unsigned char>(rbx13));
                            r10_58 = v71;
                            r9_55 = v72;
                            rbp8 = v70;
                            if (!v29) {
                                addr_3762_71:
                                fun_2600(reinterpret_cast<unsigned char>(r8_56) + reinterpret_cast<unsigned char>(r9_55), rbp8);
                                rsi16 = r10_58;
                                rax74 = fun_2560(r8_56, rsi16, r9_55, rcx40);
                                rsp5 = rsp5 - 8 + 8 - 8 + 8;
                                rdx = r15_25 + 1;
                                rbp8 = rax74;
                                if (v75 == 47) {
                                    *reinterpret_cast<void***>(r15_25) = reinterpret_cast<void**>(47);
                                    r13_23 = rdx;
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74));
                                    v29 = 1;
                                    goto addr_3318_28;
                                } else {
                                    v29 = 1;
                                    eax26 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rax74));
                                    if (reinterpret_cast<unsigned char>(r13_23) > reinterpret_cast<unsigned char>(rdx)) {
                                        do {
                                            --r13_23;
                                            if (r13_23 == r15_25) 
                                                break;
                                        } while (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47));
                                        v29 = 1;
                                        goto addr_3318_28;
                                    }
                                }
                            } else {
                                addr_3807_73:
                                rbp8 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(v57) + reinterpret_cast<unsigned char>(r8_56));
                                goto addr_3762_71;
                            }
                            addr_36fc_82:
                            if (*reinterpret_cast<uint32_t*>(&r14_4) == 2) 
                                goto addr_3440_48; else 
                                goto addr_3706_94;
                            addr_3616_83:
                            rsi16 = v32;
                            rdx = r11_64;
                            record_file(v27, rsi16);
                            rsp5 = rsp5 - 8 + 8;
                            r10_47 = r10_66;
                            r9_48 = r9_67;
                            goto addr_363c_68;
                            addr_3424_53:
                            *reinterpret_cast<uint32_t*>(&rdx) = 0;
                            *reinterpret_cast<int32_t*>(&rdx + 4) = 0;
                            rsi16 = r15_25;
                            eax76 = fun_23e0(0xffffff9c, rsi16);
                            rsp5 = rsp5 - 8 + 8;
                            if (eax76) {
                                addr_3492_55:
                                if (*reinterpret_cast<uint32_t*>(&r14_4) != 1) 
                                    goto addr_34b8_77;
                                rax77 = fun_2390();
                                rsp5 = rsp5 - 8 + 8;
                                if (*rax77 != 2) 
                                    goto addr_34b8_77;
                                rsi16 = reinterpret_cast<void**>("/");
                                rax78 = fun_24e0(rbp8, "/");
                                rsp5 = rsp5 - 8 + 8;
                                if (!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rbp8) + reinterpret_cast<uint64_t>(rax78))) 
                                    goto addr_3440_48; else 
                                    goto addr_34b8_77;
                            } else {
                                goto addr_3440_48;
                            }
                            addr_3464_63:
                            edx79 = *reinterpret_cast<unsigned char*>(rdx50 + 2);
                            if (!*reinterpret_cast<signed char*>(&edx79) || *reinterpret_cast<signed char*>(&edx79) == 47) {
                                addr_3528_59:
                                eax80 = dir_suffix;
                                *reinterpret_cast<void***>(r13_23) = *reinterpret_cast<void***>(&eax80);
                                goto addr_3424_53;
                            } else {
                                goto addr_3480_51;
                            }
                        }
                    }
                    addr_310a_99:
                    if (v17 != v15) {
                        fun_2370(v17, rsi16);
                        rsp5 = rsp5 - 8 + 8;
                    }
                    if (v20 != v19) {
                        fun_2370(v20, rsi16);
                        rsp5 = rsp5 - 8 + 8;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx13)) 
                        goto addr_3350_104;
                    *reinterpret_cast<void***>(r13_23) = reinterpret_cast<void**>(0);
                    rsi16 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23 + 1) - reinterpret_cast<unsigned char>(r15_25));
                    rax10 = gl_scratch_buffer_dupfree(r12_14);
                    rsp5 = rsp5 - 8 + 8;
                    if (rax10) 
                        break;
                    addr_322a_38:
                    xalloc_die();
                    rsp5 = rsp5 - 8 + 8;
                    continue;
                    addr_3320_17:
                    *reinterpret_cast<uint32_t*>(&rbx13) = 0;
                    if (reinterpret_cast<unsigned char>(r13_23) > reinterpret_cast<unsigned char>(r15_25 + 1)) {
                        *reinterpret_cast<int32_t*>(&rax81) = 0;
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax81) + 4) = 0;
                        *reinterpret_cast<unsigned char*>(&rax81) = reinterpret_cast<uint1_t>(*reinterpret_cast<void***>(r13_23 + 0xffffffffffffffff) == 47);
                        r13_23 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_23) - rax81);
                    }
                    addr_3338_107:
                    if (v27) {
                        hash_free();
                        rsp5 = rsp5 - 8 + 8;
                        goto addr_310a_99;
                    }
                    addr_3523_45:
                    goto addr_322a_38;
                    addr_344c_49:
                    goto addr_3320_17;
                    addr_34b8_77:
                    *reinterpret_cast<uint32_t*>(&rbx13) = 1;
                    goto addr_3338_107;
                    addr_36dc_86:
                    goto addr_322a_38;
                    addr_3861_81:
                    goto addr_322a_38;
                    addr_3706_94:
                    rax82 = fun_2390();
                    rsp5 = rsp5 - 8 + 8;
                    *rax82 = 40;
                    goto addr_34b8_77;
                }
            } else {
                while (rax83 = fun_24d0(r13_23, rsi16), rsp84 = reinterpret_cast<void*>(rsp5 - 8 + 8), !rax83) {
                    rax85 = fun_2390();
                    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
                    if (*rax85 == 12) 
                        goto addr_322a_38;
                    if (*rax85 != 34) 
                        goto addr_3102_112;
                    al86 = gl_scratch_buffer_grow(r12_14, rsi16);
                    rsp5 = rsp5 - 8 + 8;
                    if (!al86) 
                        goto addr_322a_38;
                    r13_23 = *reinterpret_cast<void***>(r12_14);
                    rsi16 = *reinterpret_cast<void***>(r12_14 + 8);
                }
                goto addr_34c2_115;
            }
        }
    }
    addr_3156_116:
    rdx87 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v7) - reinterpret_cast<unsigned char>(g28));
    if (rdx87) {
        fun_2460();
    } else {
        return rax10;
    }
    addr_3350_104:
    rdi88 = *reinterpret_cast<void***>(r12_14);
    *reinterpret_cast<int32_t*>(&rax10) = 0;
    *reinterpret_cast<int32_t*>(&rax10 + 4) = 0;
    if (v24 != rdi88) {
        fun_2370(rdi88, rsi16);
        rax10 = reinterpret_cast<void**>(0);
        goto addr_3156_116;
    }
    addr_34c2_115:
    *reinterpret_cast<int32_t*>(&rsi16) = 0;
    *reinterpret_cast<int32_t*>(&rsi16 + 4) = 0;
    r15_25 = r13_23;
    rax89 = fun_2510(r13_23);
    rsp5 = reinterpret_cast<void***>(reinterpret_cast<int64_t>(rsp84) - 8 + 8);
    r13_23 = rax89;
    goto addr_323d_7;
    addr_3102_112:
    r15_25 = r13_23;
    *reinterpret_cast<uint32_t*>(&rbx13) = 1;
    goto addr_310a_99;
}

signed char check_tuning(void** rdi, ...) {
    int1_t cf2;
    int1_t below_or_equal3;

    cf2 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) < reinterpret_cast<unsigned char>(0x97e0);
    below_or_equal3 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi + 40)) <= reinterpret_cast<unsigned char>(0x97e0);
    if (*reinterpret_cast<void***>(rdi + 40) != 0x97e0) {
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("comiss xmm0, [rip+0x5d18]");
        if (below_or_equal3 || (below_or_equal3 || (below_or_equal3 || (cf2 || (below_or_equal3 || (cf2 || below_or_equal3)))))) {
            *reinterpret_cast<void***>(rdi + 40) = reinterpret_cast<void**>(0x97e0);
            return 0;
        }
    }
    return 1;
}

/* compute_bucket_size.isra.0 */
void** compute_bucket_size_isra_0(uint64_t rdi, signed char sil) {
    void** r8_3;
    uint64_t rax4;
    uint64_t rcx5;
    void* rdi6;
    void** rsi7;
    int64_t rax8;

    if (!sil) {
        if (reinterpret_cast<int64_t>(rdi) < reinterpret_cast<int64_t>(0)) {
            *reinterpret_cast<uint32_t*>(&rdi) = *reinterpret_cast<uint32_t*>(&rdi) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi) + 4) = 0;
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rax");
            __asm__("addss xmm1, xmm1");
        } else {
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2ss xmm1, rdi");
        }
        __asm__("divss xmm1, xmm0");
        *reinterpret_cast<int32_t*>(&r8_3) = 0;
        *reinterpret_cast<int32_t*>(&r8_3 + 4) = 0;
        __asm__("comiss xmm1, [rip+0x5b7f]");
        if (1) 
            goto addr_3d6c_6;
        __asm__("comiss xmm1, [rip+0x5b76]");
        if (0) {
            __asm__("cvttss2si rdi, xmm1");
        } else {
            __asm__("subss xmm1, [rip+0x5b68]");
            __asm__("cvttss2si rdi, xmm1");
            __asm__("btc rdi, 0x3f");
        }
    }
    *reinterpret_cast<int32_t*>(&rax4) = 10;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    if (rdi >= 10) {
        rax4 = rdi;
    }
    r8_3 = reinterpret_cast<void**>(rax4 | 1);
    if (r8_3 == 0xffffffffffffffff) 
        goto addr_3d4c_13;
    while (1) {
        if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(9)) {
            *reinterpret_cast<int32_t*>(&rcx5) = 3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
        } else {
            if (!(reinterpret_cast<unsigned char>(r8_3) - ((__intrinsic() & 0xfffffffffffffffe) + (__intrinsic() >> 1)))) {
                addr_3d42_18:
                r8_3 = r8_3 + 2;
                if (!reinterpret_cast<int1_t>(r8_3 == 0xffffffffffffffff)) 
                    continue; else 
                    goto addr_3d4c_13;
            } else {
                *reinterpret_cast<int32_t*>(&rdi6) = 16;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi6) + 4) = 0;
                *reinterpret_cast<int32_t*>(&rsi7) = 9;
                *reinterpret_cast<int32_t*>(&rsi7 + 4) = 0;
                *reinterpret_cast<int32_t*>(&rcx5) = 3;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx5) + 4) = 0;
                do {
                    rcx5 = rcx5 + 2;
                    rsi7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rsi7) + reinterpret_cast<uint64_t>(rdi6));
                    if (reinterpret_cast<unsigned char>(r8_3) <= reinterpret_cast<unsigned char>(rsi7)) 
                        break;
                    rdi6 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdi6) + 8);
                } while (reinterpret_cast<unsigned char>(r8_3) % rcx5);
                goto addr_3d42_18;
            }
        }
        if (reinterpret_cast<unsigned char>(r8_3) % rcx5) 
            break; else 
            goto addr_3d42_18;
    }
    *reinterpret_cast<uint32_t*>(&rax8) = reinterpret_cast<uint1_t>(!!(reinterpret_cast<unsigned char>(r8_3) >> 61));
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    if (static_cast<int1_t>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(r8_3) >> 60)) || rax8) {
        addr_3d4c_13:
        return 0;
    } else {
        addr_3d6c_6:
        return r8_3;
    }
}

struct s0 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

struct s1 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** fun_2580(void** rdi, void** rsi, void** rdx);

int64_t fun_2380(void** rdi, ...);

int32_t transfer_entries(void** rdi, void** rsi, int32_t edx) {
    void** rdx3;
    void** r14_4;
    int32_t r12d5;
    void** rbp6;
    void** rbx7;
    void** r15_8;
    void** r13_9;
    void** rsi10;
    void** r15_11;
    void** rdi12;
    void** rax13;
    struct s0* rax14;
    void** rax15;
    void** rsi16;
    void** rax17;
    struct s1* r13_18;
    void** rax19;

    *reinterpret_cast<int32_t*>(&rdx3) = edx;
    r14_4 = rdi;
    r12d5 = *reinterpret_cast<int32_t*>(&rdx3);
    rbp6 = rsi;
    rbx7 = *reinterpret_cast<void***>(rsi);
    if (reinterpret_cast<unsigned char>(rbx7) < reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rsi + 8))) {
        do {
            addr_3dd6_2:
            r15_8 = *reinterpret_cast<void***>(rbx7);
            if (!r15_8) {
                addr_3dc8_3:
                rbx7 = rbx7 + 16;
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) <= reinterpret_cast<unsigned char>(rbx7)) 
                    break; else 
                    goto addr_3dd6_2;
            } else {
                r13_9 = *reinterpret_cast<void***>(rbx7 + 8);
                if (r13_9) {
                    rsi10 = *reinterpret_cast<void***>(r14_4 + 16);
                    while (r15_11 = *reinterpret_cast<void***>(r13_9), rdi12 = r15_11, rax13 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi10)), rsi10 = *reinterpret_cast<void***>(r14_4 + 16), reinterpret_cast<unsigned char>(rax13) < reinterpret_cast<unsigned char>(rsi10)) {
                        rax14 = reinterpret_cast<struct s0*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax13) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
                        rdx3 = *reinterpret_cast<void***>(r13_9 + 8);
                        if (rax14->f0) {
                            *reinterpret_cast<void***>(r13_9 + 8) = rax14->f8;
                            rax14->f8 = r13_9;
                            if (!rdx3) 
                                goto addr_3e4e_9;
                        } else {
                            rax14->f0 = r15_11;
                            rax15 = *reinterpret_cast<void***>(r14_4 + 72);
                            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
                            *reinterpret_cast<void***>(r13_9) = reinterpret_cast<void**>(0);
                            *reinterpret_cast<void***>(r13_9 + 8) = rax15;
                            *reinterpret_cast<void***>(r14_4 + 72) = r13_9;
                            if (!rdx3) 
                                goto addr_3e4e_9;
                        }
                        r13_9 = rdx3;
                    }
                    goto addr_26a5_12;
                    addr_3e4e_9:
                    r15_8 = *reinterpret_cast<void***>(rbx7);
                }
                *reinterpret_cast<void***>(rbx7 + 8) = reinterpret_cast<void**>(0);
                if (*reinterpret_cast<signed char*>(&r12d5)) 
                    goto addr_3dc8_3;
            }
            rsi16 = *reinterpret_cast<void***>(r14_4 + 16);
            rdi12 = r15_8;
            rax17 = reinterpret_cast<void**>(*reinterpret_cast<void***>(r14_4 + 48)(rdi12, rsi16));
            if (reinterpret_cast<unsigned char>(rax17) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4 + 16))) 
                goto addr_26a5_12;
            r13_18 = reinterpret_cast<struct s1*>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax17) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r14_4)));
            if (r13_18->f0) 
                goto addr_3e88_16;
            r13_18->f0 = r15_8;
            *reinterpret_cast<void***>(r14_4 + 24) = *reinterpret_cast<void***>(r14_4 + 24) + 1;
            continue;
            addr_3e88_16:
            rax19 = *reinterpret_cast<void***>(r14_4 + 72);
            if (!rax19) {
                rax19 = fun_2580(16, rsi16, rdx3);
                if (!rax19) 
                    goto addr_3efa_19;
            } else {
                *reinterpret_cast<void***>(r14_4 + 72) = *reinterpret_cast<void***>(rax19 + 8);
            }
            rdx3 = r13_18->f8;
            *reinterpret_cast<void***>(rax19) = r15_8;
            *reinterpret_cast<void***>(rax19 + 8) = rdx3;
            r13_18->f8 = rax19;
            *reinterpret_cast<void***>(rbx7) = reinterpret_cast<void**>(0);
            rbx7 = rbx7 + 16;
            *reinterpret_cast<void***>(rbp6 + 24) = *reinterpret_cast<void***>(rbp6 + 24) - 1;
        } while (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp6 + 8)) > reinterpret_cast<unsigned char>(rbx7));
    }
    return 1;
    addr_26a5_12:
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    fun_2380(rdi12, rdi12);
    addr_3efa_19:
    return 0;
}

void** hash_find_entry(void** rdi, void** rsi, void** rdx, int32_t ecx) {
    void** r13_5;
    int32_t r12d6;
    void** rbp7;
    void** rsi8;
    void** rax9;
    void** rbx10;
    void** rax11;
    signed char al12;
    signed char al13;
    void** rdx14;
    void** rdx15;

    r13_5 = rsi;
    r12d6 = ecx;
    rbp7 = rdi;
    rsi8 = *reinterpret_cast<void***>(rdi + 16);
    rax9 = reinterpret_cast<void**>(*reinterpret_cast<void***>(rbp7 + 48)(r13_5, rsi8));
    if (reinterpret_cast<unsigned char>(rax9) >= reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7 + 16))) {
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
        fun_2380(r13_5, r13_5);
    }
    rbx10 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) << 4) + reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbp7)));
    *reinterpret_cast<void***>(rdx) = rbx10;
    if (!*reinterpret_cast<void***>(rbx10)) {
        addr_3bff_22:
        *reinterpret_cast<int32_t*>(&rax11) = 0;
        *reinterpret_cast<int32_t*>(&rax11 + 4) = 0;
    } else {
        if (*reinterpret_cast<void***>(rbx10) == r13_5) {
            rax11 = *reinterpret_cast<void***>(rbx10);
            goto addr_3ba4_25;
        } else {
            al12 = reinterpret_cast<signed char>(*reinterpret_cast<int64_t*>(rbp7 + 56)(r13_5));
            if (!al12) {
                while (*reinterpret_cast<void***>(rbx10 + 8)) {
                    if (*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx10 + 8)) == r13_5) 
                        goto addr_3c10_29;
                    al13 = reinterpret_cast<signed char>(*reinterpret_cast<int64_t*>(rbp7 + 56)(r13_5));
                    if (al13) 
                        goto addr_3c10_29;
                    rbx10 = *reinterpret_cast<void***>(rbx10 + 8);
                }
                goto addr_3bff_22;
            } else {
                rax11 = *reinterpret_cast<void***>(rbx10);
                goto addr_3ba4_25;
            }
        }
    }
    addr_3c01_33:
    return rax11;
    addr_3ba4_25:
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        rdx14 = *reinterpret_cast<void***>(rbx10 + 8);
        if (!rdx14) {
            *reinterpret_cast<void***>(rbx10) = reinterpret_cast<void**>(0);
            goto addr_3c01_33;
        } else {
            __asm__("movdqu xmm0, [rdx]");
            __asm__("movups [rbx], xmm0");
            *reinterpret_cast<void***>(rdx14) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(rdx14 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
            *reinterpret_cast<void***>(rbp7 + 72) = rdx14;
            return rax11;
        }
    }
    addr_3c10_29:
    rdx15 = *reinterpret_cast<void***>(rbx10 + 8);
    rax11 = *reinterpret_cast<void***>(rdx15);
    if (*reinterpret_cast<signed char*>(&r12d6)) {
        *reinterpret_cast<void***>(rbx10 + 8) = *reinterpret_cast<void***>(rdx15 + 8);
        *reinterpret_cast<void***>(rdx15) = reinterpret_cast<void**>(0);
        *reinterpret_cast<void***>(rdx15 + 8) = *reinterpret_cast<void***>(rbp7 + 72);
        *reinterpret_cast<void***>(rbp7 + 72) = rdx15;
        return rax11;
    }
}

int64_t fun_2440();

void** quotearg_buffer_restyled(void** rdi, void** rsi, int64_t rdx, int64_t rcx, uint32_t r8d, uint32_t r9d, void** a7, int64_t a8, int64_t a9, int64_t a10) {
    int64_t rax11;

    fun_2440();
    if (r8d > 10) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        *reinterpret_cast<uint32_t*>(&rax11) = r8d;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax11) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x98c0 + rax11 * 4) + 0x98c0;
    }
}

struct s2 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** slotvec = reinterpret_cast<void**>(0x70);

uint32_t nslots = 1;

void** xpalloc();

void fun_24c0();

struct s3 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

void** xcharalloc(void** rdi, ...);

void** quotearg_n_options(void** rdi, int64_t rsi, int64_t rdx, struct s2* rcx, ...) {
    int64_t rbx5;
    void** rax6;
    int64_t v7;
    int32_t* rax8;
    void** r15_9;
    int32_t v10;
    uint32_t eax11;
    void** rax12;
    void** rax13;
    int64_t rax14;
    uint32_t r8d15;
    struct s3* rbx16;
    uint32_t r15d17;
    void** rsi18;
    void** r14_19;
    int64_t v20;
    int64_t v21;
    uint32_t r15d22;
    void** rax23;
    void** rsi24;
    void** rax25;
    uint32_t r8d26;
    int64_t v27;
    int64_t v28;
    void* rax29;

    rbx5 = *reinterpret_cast<int32_t*>(&rdi);
    rax6 = g28;
    v7 = 0x62cf;
    rax8 = fun_2390();
    r15_9 = slotvec;
    v10 = *rax8;
    if (*reinterpret_cast<uint32_t*>(&rbx5) > 0x7ffffffe) {
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
        fun_2380(rdi);
    } else {
        eax11 = nslots;
        if (reinterpret_cast<int32_t>(eax11) <= *reinterpret_cast<int32_t*>(&rbx5)) {
            if (r15_9 == 0xd070) {
                rax12 = xpalloc();
                __asm__("movdqa xmm0, [rip+0x6c01]");
                slotvec = rax12;
                r15_9 = rax12;
                __asm__("movups [rax], xmm0");
            } else {
                rax13 = xpalloc();
                slotvec = rax13;
                r15_9 = rax13;
            }
            v7 = 0x635b;
            fun_24c0();
            rax14 = reinterpret_cast<int32_t>(eax11);
            nslots = *reinterpret_cast<uint32_t*>(&rax14);
        }
        r8d15 = rcx->f0;
        rbx16 = reinterpret_cast<struct s3*>((rbx5 << 4) + reinterpret_cast<unsigned char>(r15_9));
        r15d17 = rcx->f4;
        rsi18 = rbx16->f0;
        r14_19 = rbx16->f8;
        v20 = rcx->f30;
        v21 = rcx->f28;
        r15d22 = r15d17 | 1;
        rax23 = quotearg_buffer_restyled(r14_19, rsi18, rsi, rdx, r8d15, r15d22, &rcx->f8, v21, v20, v7);
        if (reinterpret_cast<unsigned char>(rsi18) <= reinterpret_cast<unsigned char>(rax23)) {
            rsi24 = rax23 + 1;
            rbx16->f0 = rsi24;
            if (r14_19 != 0xd0e0) {
                fun_2370(r14_19, rsi24, r14_19, rsi24);
                rsi24 = rsi24;
            }
            rax25 = xcharalloc(rsi24, rsi24);
            r8d26 = rcx->f0;
            rbx16->f8 = rax25;
            v27 = rcx->f30;
            r14_19 = rax25;
            v28 = rcx->f28;
            quotearg_buffer_restyled(rax25, rsi24, rsi, rdx, r8d26, r15d22, rsi24, v28, v27, 0x63ea);
        }
        *rax8 = v10;
        rax29 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax6) - reinterpret_cast<unsigned char>(g28));
        if (rax29) {
            fun_2460();
        } else {
            return r14_19;
        }
    }
}

int64_t _ITM_deregisterTMCloneTable = 0;

int64_t deregister_tm_clones(int64_t rdi) {
    int64_t rax2;

    rax2 = 0xd080;
    if (1 || (rax2 = _ITM_deregisterTMCloneTable, rax2 == 0)) {
        return rax2;
    } else {
        goto rax2;
    }
}

struct s4 {
    unsigned char f0;
    unsigned char f1;
    unsigned char f2;
    signed char f3;
    signed char f4;
    signed char f5;
    signed char f6;
    signed char f7;
};

struct s4* locale_charset();

/* gettext_quote.part.0 */
void** gettext_quote_part_0(void** rdi, int32_t esi, void** rdx) {
    struct s4* rax4;
    uint32_t edx5;
    uint32_t edx6;
    void** rax7;
    uint32_t edx8;
    uint32_t edx9;
    void** rax10;
    void** rax11;

    rax4 = locale_charset();
    edx5 = static_cast<uint32_t>(rax4->f0) & 0xffffffdf;
    if (*reinterpret_cast<signed char*>(&edx5) != 85) {
        if (*reinterpret_cast<signed char*>(&edx5) == 71 && ((edx6 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx6) == 66) && (rax4->f2 == 49 && (rax4->f3 == 56 && (rax4->f4 == 48 && (rax4->f5 == 51 && (rax4->f6 == 48 && !rax4->f7))))))) {
            rax7 = reinterpret_cast<void**>(0x9863);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax7 = reinterpret_cast<void**>(0x985c);
            }
            return rax7;
        }
    } else {
        edx8 = static_cast<uint32_t>(rax4->f1) & 0xffffffdf;
        if (*reinterpret_cast<signed char*>(&edx8) == 84 && ((edx9 = static_cast<uint32_t>(rax4->f2) & 0xffffffdf, *reinterpret_cast<signed char*>(&edx9) == 70) && (rax4->f3 == 45 && (rax4->f4 == 56 && !rax4->f5)))) {
            rax10 = reinterpret_cast<void**>(0x9867);
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rdi) == 96)) {
                rax10 = reinterpret_cast<void**>(0x9858);
            }
            return rax10;
        }
    }
    rax11 = reinterpret_cast<void**>("\"");
    if (esi != 9) {
        rax11 = reinterpret_cast<void**>("'");
    }
    return rax11;
}

int64_t __gmon_start__ = 0;

void fun_2003() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = __gmon_start__;
    if (rax1) {
        rax1();
    }
    return;
}

int64_t gce38 = 0;

void fun_2033() {
    __asm__("cli ");
    goto gce38;
}

void fun_2043() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2053() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2063() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2073() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2083() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2093() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_20f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2103() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2113() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2123() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2133() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2143() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2153() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2163() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2173() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2183() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2193() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_21f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2203() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2213() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2223() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2233() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2243() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2253() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2263() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2273() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2283() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2293() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22a3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22b3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22c3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22d3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22e3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_22f3() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2303() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2313() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2323() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2333() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2343() {
    __asm__("cli ");
    goto 0x2020;
}

void fun_2353() {
    __asm__("cli ");
    goto 0x2020;
}

int64_t __cxa_finalize = 0;

void fun_2363() {
    __asm__("cli ");
    goto __cxa_finalize;
}

int64_t free = 0x2030;

void fun_2373() {
    __asm__("cli ");
    goto free;
}

int64_t abort = 0x2040;

void fun_2383() {
    __asm__("cli ");
    goto abort;
}

int64_t __errno_location = 0x2050;

void fun_2393() {
    __asm__("cli ");
    goto __errno_location;
}

int64_t strncmp = 0x2060;

void fun_23a3() {
    __asm__("cli ");
    goto strncmp;
}

int64_t _exit = 0x2070;

void fun_23b3() {
    __asm__("cli ");
    goto _exit;
}

int64_t __fpending = 0x2080;

void fun_23c3() {
    __asm__("cli ");
    goto __fpending;
}

int64_t reallocarray = 0x2090;

void fun_23d3() {
    __asm__("cli ");
    goto reallocarray;
}

int64_t faccessat = 0x20a0;

void fun_23e3() {
    __asm__("cli ");
    goto faccessat;
}

int64_t readlink = 0x20b0;

void fun_23f3() {
    __asm__("cli ");
    goto readlink;
}

int64_t textdomain = 0x20c0;

void fun_2403() {
    __asm__("cli ");
    goto textdomain;
}

int64_t fclose = 0x20d0;

void fun_2413() {
    __asm__("cli ");
    goto fclose;
}

int64_t bindtextdomain = 0x20e0;

void fun_2423() {
    __asm__("cli ");
    goto bindtextdomain;
}

int64_t dcgettext = 0x20f0;

void fun_2433() {
    __asm__("cli ");
    goto dcgettext;
}

int64_t __ctype_get_mb_cur_max = 0x2100;

void fun_2443() {
    __asm__("cli ");
    goto __ctype_get_mb_cur_max;
}

int64_t strlen = 0x2110;

void fun_2453() {
    __asm__("cli ");
    goto strlen;
}

int64_t __stack_chk_fail = 0x2120;

void fun_2463() {
    __asm__("cli ");
    goto __stack_chk_fail;
}

int64_t getopt_long = 0x2130;

void fun_2473() {
    __asm__("cli ");
    goto getopt_long;
}

int64_t mbrtowc = 0x2140;

void fun_2483() {
    __asm__("cli ");
    goto mbrtowc;
}

int64_t __overflow = 0x2150;

void fun_2493() {
    __asm__("cli ");
    goto __overflow;
}

int64_t strrchr = 0x2160;

void fun_24a3() {
    __asm__("cli ");
    goto strrchr;
}

int64_t lseek = 0x2170;

void fun_24b3() {
    __asm__("cli ");
    goto lseek;
}

int64_t memset = 0x2180;

void fun_24c3() {
    __asm__("cli ");
    goto memset;
}

int64_t getcwd = 0x2190;

void fun_24d3() {
    __asm__("cli ");
    goto getcwd;
}

int64_t strspn = 0x21a0;

void fun_24e3() {
    __asm__("cli ");
    goto strspn;
}

int64_t memcmp = 0x21b0;

void fun_24f3() {
    __asm__("cli ");
    goto memcmp;
}

int64_t fputs_unlocked = 0x21c0;

void fun_2503() {
    __asm__("cli ");
    goto fputs_unlocked;
}

int64_t rawmemchr = 0x21d0;

void fun_2513() {
    __asm__("cli ");
    goto rawmemchr;
}

int64_t calloc = 0x21e0;

void fun_2523() {
    __asm__("cli ");
    goto calloc;
}

int64_t strcmp = 0x21f0;

void fun_2533() {
    __asm__("cli ");
    goto strcmp;
}

int64_t fputc_unlocked = 0x2200;

void fun_2543() {
    __asm__("cli ");
    goto fputc_unlocked;
}

int64_t stat = 0x2210;

void fun_2553() {
    __asm__("cli ");
    goto stat;
}

int64_t memcpy = 0x2220;

void fun_2563() {
    __asm__("cli ");
    goto memcpy;
}

int64_t fileno = 0x2230;

void fun_2573() {
    __asm__("cli ");
    goto fileno;
}

int64_t malloc = 0x2240;

void fun_2583() {
    __asm__("cli ");
    goto malloc;
}

int64_t fflush = 0x2250;

void fun_2593() {
    __asm__("cli ");
    goto fflush;
}

int64_t nl_langinfo = 0x2260;

void fun_25a3() {
    __asm__("cli ");
    goto nl_langinfo;
}

int64_t __freading = 0x2270;

void fun_25b3() {
    __asm__("cli ");
    goto __freading;
}

int64_t realloc = 0x2280;

void fun_25c3() {
    __asm__("cli ");
    goto realloc;
}

int64_t setlocale = 0x2290;

void fun_25d3() {
    __asm__("cli ");
    goto setlocale;
}

int64_t __printf_chk = 0x22a0;

void fun_25e3() {
    __asm__("cli ");
    goto __printf_chk;
}

int64_t mempcpy = 0x22b0;

void fun_25f3() {
    __asm__("cli ");
    goto mempcpy;
}

int64_t memmove = 0x22c0;

void fun_2603() {
    __asm__("cli ");
    goto memmove;
}

int64_t error = 0x22d0;

void fun_2613() {
    __asm__("cli ");
    goto error;
}

int64_t fseeko = 0x22e0;

void fun_2623() {
    __asm__("cli ");
    goto fseeko;
}

int64_t __cxa_atexit = 0x22f0;

void fun_2633() {
    __asm__("cli ");
    goto __cxa_atexit;
}

int64_t exit = 0x2300;

void fun_2643() {
    __asm__("cli ");
    goto exit;
}

int64_t fwrite = 0x2310;

void fun_2653() {
    __asm__("cli ");
    goto fwrite;
}

int64_t __fprintf_chk = 0x2320;

void fun_2663() {
    __asm__("cli ");
    goto __fprintf_chk;
}

int64_t mbsinit = 0x2330;

void fun_2673() {
    __asm__("cli ");
    goto mbsinit;
}

int64_t iswprint = 0x2340;

void fun_2683() {
    __asm__("cli ");
    goto iswprint;
}

int64_t __ctype_b_loc = 0x2350;

void fun_2693() {
    __asm__("cli ");
    goto __ctype_b_loc;
}

void set_program_name(int64_t rdi);

void** fun_25d0(int64_t rdi, ...);

void fun_2420(int64_t rdi, int64_t rsi);

void fun_2400(int64_t rdi, int64_t rsi);

void atexit(int64_t rdi, int64_t rsi);

int32_t fun_2470(int64_t rdi, int64_t* rsi, int64_t rdx);

void** stdout = reinterpret_cast<void**>(0);

int64_t Version = 0x9748;

void version_etc(void** rdi, int64_t rsi, int64_t rdx);

int32_t fun_2640();

void usage();

int32_t optind = 0;

void** fun_2430();

void fun_2610();

signed char no_newline = 0;

void** areadlink_with_size(int64_t rdi, int64_t rsi, void** rdx, void** rcx, int64_t r8);

void** canonicalize_filename_mode(int64_t rdi, int64_t rsi, void** rdx, void** rcx, int64_t r8);

void fun_2500(void** rdi, void** rsi, void** rdx, void** rcx, int64_t r8);

void fun_2490();

signed char verbose = 0;

void** quotearg_n_style_colon();

int64_t fun_2713(int32_t edi, int64_t* rsi) {
    int32_t r15d3;
    int32_t ebp4;
    int64_t* rbx5;
    int64_t rdi6;
    int64_t r8_7;
    int64_t rcx8;
    int64_t rdi9;
    int32_t eax10;
    void** rdi11;
    void** rdx12;
    int32_t eax13;
    int1_t zf14;
    int32_t r12d15;
    void** rcx16;
    uint32_t v17;
    void** v18;
    int64_t rax19;
    int64_t rdi20;
    void** rax21;
    void** r13_22;
    int64_t rsi23;
    int64_t rdi24;
    void** rax25;
    void** rsi26;
    int1_t zf27;
    void** rdi28;
    void** rax29;
    int1_t zf30;
    void** rax31;
    int32_t eax32;
    int64_t rax33;

    __asm__("cli ");
    r15d3 = -1;
    ebp4 = edi;
    rbx5 = rsi;
    rdi6 = *rsi;
    set_program_name(rdi6);
    fun_25d0(6, 6);
    fun_2420("coreutils", "/usr/local/share/locale");
    fun_2400("coreutils", "/usr/local/share/locale");
    atexit(0x38f0, "/usr/local/share/locale");
    while (*reinterpret_cast<int32_t*>(&r8_7) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_7) + 4) = 0, rcx8 = 0xca80, *reinterpret_cast<int32_t*>(&rdi9) = ebp4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi9) + 4) = 0, eax10 = fun_2470(rdi9, rbx5, "efmnqsvz"), eax10 != -1) {
        if (eax10 > 0x7a) 
            goto addr_29c5_4;
        if (eax10 > 100) 
            goto addr_27ba_6;
        if (eax10 == 0xffffff7d) {
            rdi11 = stdout;
            rcx8 = Version;
            r8_7 = reinterpret_cast<int64_t>("Dmitry V. Levin");
            version_etc(rdi11, "readlink", "GNU coreutils");
            eax10 = fun_2640();
        }
        if (eax10 != 0xffffff7e) 
            goto addr_29c5_4;
        usage();
        r15d3 = 2;
    }
    *reinterpret_cast<int32_t*>(&rdx12) = optind;
    *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
    eax13 = *reinterpret_cast<int32_t*>(&rdx12);
    if (*reinterpret_cast<int32_t*>(&rdx12) >= ebp4) {
        fun_2430();
        fun_2610();
    } else {
        *reinterpret_cast<uint32_t*>(&rcx8) = ebp4 - *reinterpret_cast<int32_t*>(&rdx12) - 1;
        if (!*reinterpret_cast<uint32_t*>(&rcx8)) 
            goto addr_28aa_14;
        zf14 = no_newline == 0;
        if (!zf14) 
            goto addr_29cf_16; else 
            goto addr_2899_17;
    }
    addr_29c5_4:
    usage();
    addr_29cf_16:
    fun_2430();
    fun_2610();
    *reinterpret_cast<int32_t*>(&rdx12) = optind;
    *reinterpret_cast<int32_t*>(&rdx12 + 4) = 0;
    addr_2899_17:
    no_newline = 0;
    eax13 = *reinterpret_cast<int32_t*>(&rdx12);
    if (ebp4 <= *reinterpret_cast<int32_t*>(&rdx12)) {
        r12d15 = 0;
    } else {
        addr_28aa_14:
        r12d15 = 0;
        *reinterpret_cast<uint32_t*>(&rcx16) = *reinterpret_cast<uint32_t*>(&rcx8) - (*reinterpret_cast<uint32_t*>(&rcx8) + reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&rcx8) < *reinterpret_cast<uint32_t*>(&rcx8) + 1)) & 10;
        *reinterpret_cast<int32_t*>(&rcx16 + 4) = 0;
        v17 = *reinterpret_cast<uint32_t*>(&rcx16);
        v18 = rcx16;
        goto addr_291f_19;
    }
    addr_2992_20:
    *reinterpret_cast<int32_t*>(&rax19) = r12d15;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax19) + 4) = 0;
    return rax19;
    do {
        addr_291f_19:
        if (r15d3 == -1) {
            rdi20 = rbx5[eax13];
            rax21 = areadlink_with_size(rdi20, 63, rdx12, rcx16, r8_7);
            r13_22 = rax21;
        } else {
            *reinterpret_cast<int32_t*>(&rsi23) = r15d3;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi23) + 4) = 0;
            rdi24 = rbx5[eax13];
            rax25 = canonicalize_filename_mode(rdi24, rsi23, rdx12, rcx16, r8_7);
            r13_22 = rax25;
        }
        if (r13_22) {
            rsi26 = stdout;
            fun_2500(r13_22, rsi26, rdx12, rcx16, r8_7);
            zf27 = no_newline == 0;
            if (zf27) {
                rdi28 = stdout;
                rax29 = *reinterpret_cast<void***>(rdi28 + 40);
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdi28 + 48)) <= reinterpret_cast<unsigned char>(rax29)) {
                    *reinterpret_cast<uint32_t*>(&rsi26) = v17;
                    *reinterpret_cast<int32_t*>(&rsi26 + 4) = 0;
                    fun_2490();
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx16) = reinterpret_cast<unsigned char>(v18);
                    *reinterpret_cast<int32_t*>(&rcx16 + 4) = 0;
                    rdx12 = rax29 + 1;
                    *reinterpret_cast<void***>(rdi28 + 40) = rdx12;
                    *reinterpret_cast<void***>(rax29) = rcx16;
                }
            }
            fun_2370(r13_22, rsi26, r13_22, rsi26);
        } else {
            zf30 = verbose == 0;
            r12d15 = 1;
            if (!zf30) {
                rax31 = quotearg_n_style_colon();
                fun_2390();
                rcx16 = rax31;
                rdx12 = reinterpret_cast<void**>("%s");
                fun_2610();
            }
        }
        eax32 = optind;
        eax13 = eax32 + 1;
        optind = eax13;
    } while (eax13 < ebp4);
    goto addr_2992_20;
    addr_27ba_6:
    *reinterpret_cast<uint32_t*>(&rax33) = eax10 - 0x65;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax33) + 4) = 0;
    if (*reinterpret_cast<uint32_t*>(&rax33) <= 21) {
        goto *reinterpret_cast<int32_t*>(0x96f0 + rax33 * 4) + 0x96f0;
    }
}

int64_t __libc_start_main = 0;

void fun_2a13() {
    __asm__("cli ");
    __libc_start_main(0x2710, __return_address(), reinterpret_cast<int64_t>(__zero_stack_offset()) + 8);
    __asm__("hlt ");
}

/* completed.0 */
signed char completed_0 = 0;

int64_t __dso_handle = 0xd008;

void fun_2360(int64_t rdi);

int64_t fun_2ab3() {
    int1_t zf1;
    int64_t rax2;
    int1_t zf3;
    int64_t rdi4;
    int64_t rax5;

    __asm__("cli ");
    zf1 = completed_0 == 0;
    if (!zf1) {
        return rax2;
    } else {
        zf3 = __cxa_finalize == 0;
        if (!zf3) {
            rdi4 = __dso_handle;
            fun_2360(rdi4);
        }
        rax5 = deregister_tm_clones(rdi4);
        completed_0 = 1;
        return rax5;
    }
}

int64_t _ITM_registerTMCloneTable = 0;

int64_t fun_2af3() {
    int64_t rax1;

    __asm__("cli ");
    rax1 = 0;
    if (1 || (rax1 = _ITM_registerTMCloneTable, rax1 == 0)) {
        return rax1;
    } else {
        goto rax1;
    }
}

void** program_name = reinterpret_cast<void**>(0);

void fun_25e0(int64_t rdi, void** rsi, void** rdx, void** rcx);

int64_t fun_2530(int64_t rdi, int64_t rsi, void** rdx);

int32_t fun_23a0(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

void** stderr = reinterpret_cast<void**>(0);

void fun_2660(void** rdi, int64_t rsi, void** rdx, void** rcx, void** r8, void** r9, int64_t a7, int64_t a8, int64_t a9, int64_t a10, int64_t a11, int64_t a12);

void fun_2b03(int32_t edi) {
    void** r12_2;
    void** rax3;
    void** v4;
    void** rax5;
    void** rcx6;
    void** r12_7;
    void** rax8;
    int64_t r8_9;
    void** r12_10;
    void** rax11;
    int64_t r8_12;
    void** r12_13;
    void** rax14;
    int64_t r8_15;
    void** r12_16;
    void** rax17;
    int64_t r8_18;
    void** r12_19;
    void** rax20;
    int64_t r8_21;
    int64_t rax22;
    void** r13_23;
    void** rax24;
    void** rax25;
    int32_t eax26;
    void** rax27;
    void** rax28;
    void** rax29;
    int32_t eax30;
    void** rax31;
    void** r15_32;
    void** rax33;
    int64_t r8_34;
    void** rax35;
    void** rax36;
    void** rdi37;
    void** r8_38;
    void** r9_39;
    int64_t v40;
    int64_t v41;
    int64_t v42;
    int64_t v43;
    int64_t v44;
    int64_t v45;

    __asm__("cli ");
    r12_2 = program_name;
    rax3 = g28;
    v4 = rax3;
    if (!edi) {
        while (1) {
            rax5 = fun_2430();
            fun_25e0(1, rax5, r12_2, rcx6);
            r12_7 = stdout;
            rax8 = fun_2430();
            fun_2500(rax8, r12_7, 5, rcx6, r8_9);
            r12_10 = stdout;
            rax11 = fun_2430();
            fun_2500(rax11, r12_10, 5, rcx6, r8_12);
            r12_13 = stdout;
            rax14 = fun_2430();
            fun_2500(rax14, r12_13, 5, rcx6, r8_15);
            r12_16 = stdout;
            rax17 = fun_2430();
            fun_2500(rax17, r12_16, 5, rcx6, r8_18);
            r12_19 = stdout;
            rax20 = fun_2430();
            fun_2500(rax20, r12_19, 5, rcx6, r8_21);
            do {
                if (1) 
                    break;
                rax22 = fun_2530("readlink", 0, 5);
            } while (*reinterpret_cast<int32_t*>(&rax22));
            r13_23 = v4;
            if (!r13_23) {
                rax24 = fun_2430();
                fun_25e0(1, rax24, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax25 = fun_25d0(5);
                if (!rax25 || (eax26 = fun_23a0(rax25, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax26)) {
                    rax27 = fun_2430();
                    r13_23 = reinterpret_cast<void**>("readlink");
                    fun_25e0(1, rax27, "https://www.gnu.org/software/coreutils/", "readlink");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                } else {
                    r13_23 = reinterpret_cast<void**>("readlink");
                    goto addr_2e58_9;
                }
            } else {
                rax28 = fun_2430();
                fun_25e0(1, rax28, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
                rax29 = fun_25d0(5);
                if (!rax29 || (eax30 = fun_23a0(rax29, "en_", 3, "https://www.gnu.org/software/coreutils/"), !eax30)) {
                    addr_2d5e_11:
                    rax31 = fun_2430();
                    fun_25e0(1, rax31, "https://www.gnu.org/software/coreutils/", "readlink");
                    r12_2 = reinterpret_cast<void**>(" invocation");
                    if (!reinterpret_cast<int1_t>(r13_23 == "readlink")) {
                        r12_2 = reinterpret_cast<void**>(0x9c81);
                    }
                } else {
                    addr_2e58_9:
                    r15_32 = stdout;
                    rax33 = fun_2430();
                    fun_2500(rax33, r15_32, 5, "https://www.gnu.org/software/coreutils/", r8_34);
                    goto addr_2d5e_11;
                }
            }
            rax35 = fun_2430();
            rcx6 = r12_2;
            fun_25e0(1, rax35, r13_23, rcx6);
            addr_2b5e_14:
            fun_2640();
        }
    } else {
        rax36 = fun_2430();
        rdi37 = stderr;
        rcx6 = r12_2;
        fun_2660(rdi37, 1, rax36, rcx6, r8_38, r9_39, v40, v41, v42, v43, v44, v45);
        goto addr_2b5e_14;
    }
}

void** fun_25c0(void** rdi, void** rsi, void** rdx);

void** fun_2e93(void** rdi, void** rsi) {
    void** rbp3;
    void** rbx4;
    void** rax5;
    void** v6;
    int1_t zf7;
    unsigned char r14b8;
    void** v9;
    int32_t* rax10;
    void** r15_11;
    void* rax12;
    void** rdi13;
    void** rdx14;
    void** rax15;
    void** r13_16;
    void** rax17;
    void** r12_18;
    void** rax19;
    int64_t rcx20;
    void** rax21;

    __asm__("cli ");
    rbp3 = rdi;
    *reinterpret_cast<int32_t*>(&rbx4) = 0x80;
    *reinterpret_cast<int32_t*>(&rbx4 + 4) = 0;
    rax5 = g28;
    v6 = rax5;
    zf7 = rsi == 0;
    r14b8 = reinterpret_cast<uint1_t>(!zf7);
    if (!zf7 && (rbx4 = rsi + 1, reinterpret_cast<unsigned char>(rsi) >= reinterpret_cast<unsigned char>(0x401))) {
        rbx4 = reinterpret_cast<void**>(0x401);
    }
    v9 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 8 - 8 - 0xa8 + 16);
    goto addr_2ef8_4;
    addr_2fe0_5:
    rax10 = fun_2390();
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    *rax10 = 12;
    addr_2f8b_6:
    rax12 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v6) - reinterpret_cast<unsigned char>(g28));
    if (rax12) {
        fun_2460();
    } else {
        return r15_11;
    }
    addr_2f80_9:
    rdi13 = r15_11;
    *reinterpret_cast<int32_t*>(&r15_11) = 0;
    *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
    fun_2370(rdi13, rsi, rdi13, rsi);
    goto addr_2f8b_6;
    while (rax15 = fun_2580(rbx4, rsi, rdx14), r13_16 = rax15, !!rax15) {
        r15_11 = rax15;
        do {
            rdx14 = rbx4;
            rsi = r13_16;
            rax17 = fun_23f0(rbp3, rsi, rdx14);
            if (reinterpret_cast<signed char>(rax17) < reinterpret_cast<signed char>(0)) 
                goto addr_2f80_9;
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(rax17)) 
                goto addr_2fb8_14;
            fun_2370(r15_11, rsi, r15_11, rsi);
            if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(0x3fffffffffffffff)) {
                if (rbx4 == 0x7fffffffffffffff) 
                    goto addr_2fe0_5;
                rbx4 = reinterpret_cast<void**>(0x7fffffffffffffff);
                addr_2ef8_4:
                if (!reinterpret_cast<int1_t>(rbx4 == 0x80)) 
                    break;
            } else {
                rbx4 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx4) + reinterpret_cast<unsigned char>(rbx4));
                if (rbx4 != 0x80) 
                    break;
            }
            r13_16 = v9;
            *reinterpret_cast<int32_t*>(&r15_11) = 0;
            *reinterpret_cast<int32_t*>(&r15_11 + 4) = 0;
        } while (!r14b8);
    }
    goto addr_2fe0_5;
    addr_2fb8_14:
    *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_16) + reinterpret_cast<unsigned char>(rax17)) = 0;
    r12_18 = rax17 + 1;
    if (!r15_11) {
        rax19 = fun_2580(r12_18, rsi, rdx14);
        r15_11 = rax19;
        if (rax19) {
            fun_2560(rax19, r13_16, r12_18, rcx20);
            goto addr_2f8b_6;
        }
    } else {
        if (reinterpret_cast<unsigned char>(rbx4) > reinterpret_cast<unsigned char>(r12_18)) {
            rax21 = fun_25c0(r15_11, r12_18, rdx14);
            if (rax21) {
                r15_11 = rax21;
            }
            goto addr_2f8b_6;
        }
    }
}

void** fun_3883(void** rdi, uint32_t esi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    rax3 = g28;
    rax4 = canonicalize_filename_mode_stk(rdi, esi, reinterpret_cast<int64_t>(__zero_stack_offset()) - reinterpret_cast<uint64_t>("l"));
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2460();
    } else {
        return rax4;
    }
}

int64_t file_name = 0;

void fun_38d3(int64_t rdi) {
    __asm__("cli ");
    file_name = rdi;
    return;
}

signed char ignore_EPIPE = 0;

void fun_38e3(signed char dil) {
    __asm__("cli ");
    ignore_EPIPE = dil;
    return;
}

int32_t close_stream(void** rdi);

void** quotearg_colon();

int32_t exit_failure = 1;

void** fun_23b0(int64_t rdi, int64_t rsi, int64_t rdx, void** rcx, void** r8);

void fun_38f3() {
    void** rdi1;
    int32_t eax2;
    int32_t* rax3;
    int1_t zf4;
    int32_t* rbx5;
    void** rdi6;
    int32_t eax7;
    void** rax8;
    int64_t rdi9;
    void** rax10;
    int64_t rsi11;
    void** r8_12;
    void** rcx13;
    int64_t rdx14;
    int64_t rdi15;

    __asm__("cli ");
    rdi1 = stdout;
    eax2 = close_stream(rdi1);
    if (!eax2 || (rax3 = fun_2390(), zf4 = ignore_EPIPE == 0, rbx5 = rax3, !zf4) && *rax3 == 32) {
        rdi6 = stderr;
        eax7 = close_stream(rdi6);
        if (!eax7) {
            return;
        }
    } else {
        rax8 = fun_2430();
        rdi9 = file_name;
        if (!rdi9) 
            goto addr_3983_5;
        rax10 = quotearg_colon();
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        r8_12 = rax8;
        rcx13 = rax10;
        rdx14 = reinterpret_cast<int64_t>("%s: %s");
        fun_2610();
    }
    while (1) {
        *reinterpret_cast<int32_t*>(&rdi15) = exit_failure;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi15) + 4) = 0;
        rax8 = fun_23b0(rdi15, rsi11, rdx14, rcx13, r8_12);
        addr_3983_5:
        *reinterpret_cast<int32_t*>(&rsi11) = *rbx5;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rsi11) + 4) = 0;
        rcx13 = rax8;
        rdx14 = reinterpret_cast<int64_t>("%s");
        fun_2610();
    }
}

struct s5 {
    int64_t f0;
    int64_t f8;
};

struct s6 {
    int64_t f0;
    int64_t f8;
    int64_t f10;
};

struct s6* xmalloc(int64_t rdi);

int64_t xstrdup(int64_t rdi);

struct s6* hash_insert(int64_t rdi, struct s6* rsi);

void fun_39a3(int64_t rdi, int64_t rsi, struct s5* rdx) {
    struct s6* rax4;
    int64_t rax5;
    struct s6* rax6;

    __asm__("cli ");
    if (!rdi) {
        return;
    } else {
        rax4 = xmalloc(24);
        rax5 = xstrdup(rsi);
        rax4->f0 = rax5;
        rax4->f8 = rdx->f8;
        rax4->f10 = rdx->f0;
        rax6 = hash_insert(rdi, rax4);
        if (!rax6) {
            xalloc_die();
        } else {
            if (rax4 == rax6) {
                return;
            }
        }
    }
}

int64_t hash_lookup();

int64_t fun_3a33(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rax4;
    int64_t rax5;
    void* rdx6;

    __asm__("cli ");
    rax4 = g28;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (rdi) {
        rax5 = hash_lookup();
        *reinterpret_cast<unsigned char*>(&rax5) = reinterpret_cast<uint1_t>(!!rax5);
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2460();
    } else {
        return rax5;
    }
}

uint64_t fun_3a93(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    __asm__("ror rax, 0x3");
    return rdi % reinterpret_cast<uint64_t>(rsi);
}

unsigned char fun_3ab3(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    return static_cast<unsigned char>(reinterpret_cast<uint1_t>(rsi == rdi));
}

struct s7 {
    signed char[16] pad16;
    int64_t f10;
};

int64_t fun_3f13(struct s7* rdi) {
    __asm__("cli ");
    return rdi->f10;
}

struct s8 {
    signed char[24] pad24;
    int64_t f18;
};

int64_t fun_3f23(struct s8* rdi) {
    __asm__("cli ");
    return rdi->f18;
}

struct s9 {
    signed char[32] pad32;
    int64_t f20;
};

int64_t fun_3f33(struct s9* rdi) {
    __asm__("cli ");
    return rdi->f20;
}

struct s12 {
    signed char[8] pad8;
    struct s12* f8;
};

struct s11 {
    int64_t f0;
    struct s12* f8;
};

struct s10 {
    struct s11* f0;
    struct s11* f8;
};

uint64_t fun_3f43(struct s10* rdi) {
    struct s11* rcx2;
    struct s11* rsi3;
    uint64_t r8_4;
    struct s12* rax5;
    uint64_t rdx6;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&r8_4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_4) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                rax5 = rcx2->f8;
                *reinterpret_cast<int32_t*>(&rdx6) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx6) + 4) = 0;
                if (rax5) {
                    do {
                        rax5 = rax5->f8;
                        ++rdx6;
                    } while (rax5);
                }
                if (r8_4 < rdx6) {
                    r8_4 = rdx6;
                }
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    return r8_4;
}

struct s15 {
    signed char[8] pad8;
    struct s15* f8;
};

struct s14 {
    int64_t f0;
    struct s15* f8;
};

struct s13 {
    struct s14* f0;
    struct s14* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
};

int64_t fun_3fa3(struct s13* rdi) {
    struct s14* rcx2;
    struct s14* rsi3;
    int64_t rdx4;
    int64_t r8_5;
    struct s15* rax6;
    int64_t rax7;

    __asm__("cli ");
    rcx2 = rdi->f0;
    rsi3 = rdi->f8;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    *reinterpret_cast<int32_t*>(&r8_5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(rcx2) < reinterpret_cast<uint64_t>(rsi3)) {
        while (1) {
            if (!rcx2->f0 || (rax6 = rcx2->f8, ++r8_5, ++rdx4, rax6 == 0)) {
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            } else {
                do {
                    rax6 = rax6->f8;
                    ++rdx4;
                } while (rax6);
                ++rcx2;
                if (reinterpret_cast<uint64_t>(rcx2) >= reinterpret_cast<uint64_t>(rsi3)) 
                    break;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax7) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    if (rdi->f18 == r8_5) {
        *reinterpret_cast<unsigned char*>(&rax7) = reinterpret_cast<uint1_t>(rdi->f20 == rdx4);
        return rax7;
    } else {
        return 0;
    }
}

struct s18 {
    signed char[8] pad8;
    struct s18* f8;
};

struct s17 {
    int64_t f0;
    struct s18* f8;
};

struct s16 {
    struct s17* f0;
    struct s17* f8;
    void** f10;
    signed char[7] pad24;
    void** f18;
    signed char[7] pad32;
    void** f20;
};

void fun_4013(struct s16* rdi, void** rsi) {
    int64_t v3;
    int64_t v4;
    int64_t r13_5;
    int64_t v6;
    int64_t r12_7;
    uint64_t r12_8;
    int64_t v9;
    int64_t rbp10;
    void** rbp11;
    int64_t v12;
    int64_t rbx13;
    struct s17* rcx14;
    struct s17* rsi15;
    void** r8_16;
    void** rbx17;
    void** r13_18;
    struct s18* rax19;
    uint64_t rdx20;
    void** r9_21;
    int64_t v22;
    void** r9_23;
    int64_t v24;
    void** r9_25;
    int64_t v26;

    v3 = reinterpret_cast<int64_t>(__return_address());
    __asm__("cli ");
    v4 = r13_5;
    v6 = r12_7;
    *reinterpret_cast<int32_t*>(&r12_8) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_8) + 4) = 0;
    v9 = rbp10;
    rbp11 = rsi;
    v12 = rbx13;
    rcx14 = rdi->f0;
    rsi15 = rdi->f8;
    r8_16 = rdi->f20;
    rbx17 = rdi->f10;
    r13_18 = rdi->f18;
    if (reinterpret_cast<uint64_t>(rcx14) < reinterpret_cast<uint64_t>(rsi15)) {
        while (1) {
            if (!rcx14->f0) {
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            } else {
                rax19 = rcx14->f8;
                *reinterpret_cast<int32_t*>(&rdx20) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx20) + 4) = 0;
                if (rax19) {
                    do {
                        rax19 = rax19->f8;
                        ++rdx20;
                    } while (rax19);
                }
                if (r12_8 < rdx20) {
                    r12_8 = rdx20;
                }
                ++rcx14;
                if (reinterpret_cast<uint64_t>(rsi15) <= reinterpret_cast<uint64_t>(rcx14)) 
                    break;
            }
        }
    }
    fun_2660(rbp11, 1, "# entries:         %lu\n", r8_16, r8_16, r9_21, v22, v12, v9, v6, v4, v3);
    fun_2660(rbp11, 1, "# buckets:         %lu\n", rbx17, r8_16, r9_23, v24, v12, v9, v6, v4, v3);
    if (reinterpret_cast<signed char>(r13_18) < reinterpret_cast<signed char>(0)) {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, rax");
        __asm__("addsd xmm0, xmm0");
        __asm__("mulsd xmm0, [rip+0x56cc]");
        if (reinterpret_cast<signed char>(rbx17) >= reinterpret_cast<signed char>(0)) {
            addr_40ca_13:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rbx");
        } else {
            addr_4149_14:
            __asm__("pxor xmm1, xmm1");
            __asm__("cvtsi2sd xmm1, rax");
            __asm__("addsd xmm1, xmm1");
        }
        __asm__("divsd xmm0, xmm1");
        fun_2660(rbp11, 1, "# buckets used:    %lu (%.2f%%)\n", r13_18, r8_16, r9_25, v26, v12, v9, v6, v4, v3);
        goto fun_2660;
    } else {
        __asm__("pxor xmm0, xmm0");
        __asm__("cvtsi2sd xmm0, r13");
        __asm__("mulsd xmm0, [rip+0x574b]");
        if (reinterpret_cast<signed char>(rbx17) < reinterpret_cast<signed char>(0)) 
            goto addr_4149_14; else 
            goto addr_40ca_13;
    }
}

struct s19 {
    int64_t* f0;
    signed char[8] pad16;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
    int64_t f38;
};

struct s20 {
    int64_t f0;
    struct s20* f8;
};

int64_t fun_4173(struct s19* rdi, int64_t rsi) {
    int64_t r12_3;
    struct s19* rbp4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s20* rbx7;
    int64_t rsi8;
    signed char al9;

    __asm__("cli ");
    r12_3 = rsi;
    rbp4 = rdi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp4->f30(r12_3, rsi5));
    if (rax6 >= rbp4->f10) 
        goto 0x26aa;
    rbx7 = reinterpret_cast<struct s20*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp4->f0));
    rsi8 = rbx7->f0;
    if (rsi8) {
        while (rsi8 != r12_3) {
            al9 = reinterpret_cast<signed char>(rbp4->f38(r12_3));
            if (al9) 
                goto addr_41d8_5;
            rbx7 = rbx7->f8;
            if (!rbx7) 
                goto addr_41cb_7;
            rsi8 = rbx7->f0;
        }
    } else {
        goto addr_41cb_7;
    }
    addr_41db_10:
    return r12_3;
    addr_41d8_5:
    r12_3 = rbx7->f0;
    goto addr_41db_10;
    addr_41cb_7:
    return 0;
}

struct s21 {
    int64_t* f0;
    int64_t* f8;
    signed char[16] pad32;
    int64_t f20;
};

int64_t fun_41f3(struct s21* rdi) {
    int64_t* rax2;
    int64_t* rdx3;

    __asm__("cli ");
    if (!rdi->f20) {
        return 0;
    }
    rax2 = rdi->f0;
    rdx3 = rdi->f8;
    if (reinterpret_cast<uint64_t>(rax2) >= reinterpret_cast<uint64_t>(rdx3)) {
        goto 0x26af;
    }
    do {
        if (*rax2) 
            break;
        rax2 = rax2 + 2;
    } while (reinterpret_cast<uint64_t>(rax2) < reinterpret_cast<uint64_t>(rdx3));
    goto addr_422f_7;
    return *rax2;
    addr_422f_7:
    goto 0x26af;
}

struct s23 {
    int64_t f0;
    struct s23* f8;
};

struct s22 {
    int64_t* f0;
    struct s23* f8;
    uint64_t f10;
    signed char[24] pad48;
    int64_t f30;
};

int64_t fun_4243(struct s22* rdi, int64_t rsi) {
    struct s22* rbp3;
    int64_t rbx4;
    uint64_t rsi5;
    uint64_t rax6;
    struct s23* rax7;
    struct s23* rdx8;
    struct s23* rdx9;
    int64_t r8_10;

    __asm__("cli ");
    rbp3 = rdi;
    rbx4 = rsi;
    rsi5 = rdi->f10;
    rax6 = reinterpret_cast<uint64_t>(rbp3->f30(rbx4, rsi5));
    if (rax6 >= rbp3->f10) 
        goto 0x26b5;
    rax7 = reinterpret_cast<struct s23*>((rax6 << 4) + reinterpret_cast<int64_t>(rbp3->f0));
    rdx8 = rax7;
    do {
        rdx8 = rdx8->f8;
        if (rdx8->f0 == rbx4) 
            break;
    } while (rdx8);
    goto addr_428e_5;
    if (rdx8) {
        return rdx8->f0;
    }
    addr_428e_5:
    rdx9 = rbp3->f8;
    do {
        ++rax7;
        if (reinterpret_cast<uint64_t>(rdx9) <= reinterpret_cast<uint64_t>(rax7)) 
            break;
        r8_10 = rax7->f0;
    } while (!r8_10);
    goto addr_42ac_10;
    *reinterpret_cast<int32_t*>(&r8_10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r8_10) + 4) = 0;
    addr_42ac_10:
    return r8_10;
}

struct s25 {
    int64_t f0;
    struct s25* f8;
};

struct s24 {
    struct s25* f0;
    struct s25* f8;
};

void fun_42d3(struct s24* rdi, int64_t rsi, uint64_t rdx) {
    struct s25* r9_4;
    uint64_t rax5;
    struct s25* rcx6;

    __asm__("cli ");
    r9_4 = rdi->f0;
    *reinterpret_cast<int32_t*>(&rax5) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax5) + 4) = 0;
    if (reinterpret_cast<uint64_t>(r9_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        addr_4312_2:
        return;
    } else {
        do {
            if (r9_4->f0) {
                rcx6 = r9_4;
                do {
                    if (rdx <= rax5) 
                        goto addr_4312_2;
                    ++rax5;
                    *reinterpret_cast<int64_t*>(rsi + rax5 * 8 - 8) = rcx6->f0;
                    rcx6 = rcx6->f8;
                } while (rcx6);
            }
            ++r9_4;
        } while (reinterpret_cast<uint64_t>(rdi->f8) > reinterpret_cast<uint64_t>(r9_4));
    }
    return;
}

struct s27 {
    int64_t f0;
    struct s27* f8;
};

struct s26 {
    struct s27* f0;
    struct s27* f8;
};

int64_t fun_4323(struct s26* rdi, int64_t rsi, int64_t rdx) {
    struct s27* r14_4;
    int64_t r12_5;
    struct s26* r15_6;
    int64_t rbp7;
    int64_t r13_8;
    int64_t rdi9;
    struct s27* rbx10;
    signed char al11;

    __asm__("cli ");
    r14_4 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r14_4) >= reinterpret_cast<uint64_t>(rdi->f8)) {
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
    } else {
        r15_6 = rdi;
        rbp7 = rsi;
        r13_8 = rdx;
        *reinterpret_cast<int32_t*>(&r12_5) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_5) + 4) = 0;
        do {
            rdi9 = r14_4->f0;
            if (rdi9) {
                rbx10 = r14_4;
                while (al11 = reinterpret_cast<signed char>(rbp7(rdi9, r13_8)), !!al11) {
                    rbx10 = rbx10->f8;
                    ++r12_5;
                    if (!rbx10) 
                        goto addr_434f_8;
                    rdi9 = rbx10->f0;
                }
                goto addr_4391_10;
            }
            addr_434f_8:
            ++r14_4;
        } while (reinterpret_cast<uint64_t>(r15_6->f8) > reinterpret_cast<uint64_t>(r14_4));
    }
    addr_4359_11:
    return r12_5;
    addr_4391_10:
    goto addr_4359_11;
}

uint64_t fun_43a3(unsigned char* rdi, int64_t rsi) {
    int64_t rcx3;
    uint64_t rdx4;
    uint64_t rax5;

    __asm__("cli ");
    *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
    *reinterpret_cast<int32_t*>(&rdx4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx4) + 4) = 0;
    if (*reinterpret_cast<signed char*>(&rcx3)) {
        do {
            ++rdi;
            rax5 = (rdx4 << 5) - rdx4 + rcx3;
            *reinterpret_cast<uint32_t*>(&rcx3) = *rdi;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx3) + 4) = 0;
            rdx4 = rax5 % rsi;
        } while (*reinterpret_cast<signed char*>(&rcx3));
    }
    return rdx4;
}

struct s28 {
    int64_t f0;
    int64_t f8;
    signed char f10;
};

void fun_43e3(struct s28* rdi) {
    __asm__("cli ");
    rdi->f10 = 0;
    rdi->f0 = 0x3f80000000000000;
    rdi->f8 = 0x3fb4fdf43f4ccccd;
    return;
}

void** fun_2520(void** rdi, void** rsi);

void** fun_4413(uint64_t rdi, void** rsi, void** rdx, int64_t rcx, int64_t r8) {
    void** r15_6;
    void** rbp7;
    int64_t rbx8;
    void** rax9;
    void** r12_10;
    signed char al11;
    void** rax12;
    void** rax13;
    void** rdi14;

    __asm__("cli ");
    r15_6 = rsi;
    rbp7 = rdx;
    rbx8 = rcx;
    if (!rdx) {
        rbp7 = reinterpret_cast<void**>(0x3a90);
    }
    if (!rcx) {
        rbx8 = 0x3ab0;
    }
    rax9 = fun_2580(80, rsi, rdx);
    r12_10 = rax9;
    if (rax9) {
        if (!r15_6) {
            r15_6 = reinterpret_cast<void**>(0x97e0);
        }
        *reinterpret_cast<void***>(r12_10 + 40) = r15_6;
        al11 = check_tuning(r12_10);
        if (!al11 || ((*reinterpret_cast<uint32_t*>(&rsi) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r15_6 + 16)), *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax12 = compute_bucket_size_isra_0(rdi, *reinterpret_cast<signed char*>(&rsi)), *reinterpret_cast<void***>(r12_10 + 16) = rax12, rax12 == 0) || (*reinterpret_cast<uint32_t*>(&rsi) = 16, *reinterpret_cast<int32_t*>(&rsi + 4) = 0, rax13 = fun_2520(rax12, 16), *reinterpret_cast<void***>(r12_10) = rax13, rax13 == 0))) {
            rdi14 = r12_10;
            *reinterpret_cast<int32_t*>(&r12_10) = 0;
            *reinterpret_cast<int32_t*>(&r12_10 + 4) = 0;
            fun_2370(rdi14, rsi);
        } else {
            *reinterpret_cast<void***>(r12_10 + 48) = rbp7;
            *reinterpret_cast<int64_t*>(r12_10 + 56) = rbx8;
            *reinterpret_cast<void***>(r12_10 + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax13) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax12) << 4));
            *reinterpret_cast<void***>(r12_10 + 24) = reinterpret_cast<void**>(0);
            *reinterpret_cast<int64_t*>(r12_10 + 32) = 0;
            *reinterpret_cast<int64_t*>(r12_10 + 64) = r8;
            *reinterpret_cast<void***>(r12_10 + 72) = reinterpret_cast<void**>(0);
        }
    }
    return r12_10;
}

struct s31 {
    int64_t f0;
    struct s31* f8;
};

struct s30 {
    int64_t f0;
    struct s31* f8;
};

struct s29 {
    struct s30* f0;
    struct s30* f8;
    signed char[8] pad24;
    int64_t f18;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    struct s31* f48;
};

void fun_4513(struct s29* rdi) {
    struct s29* rbp2;
    struct s30* r12_3;
    struct s31* rbx4;
    int64_t rdx5;
    int64_t rdi6;
    struct s31* rax7;
    struct s31* rcx8;
    int64_t rdi9;

    __asm__("cli ");
    rbp2 = rdi;
    r12_3 = rdi->f0;
    if (reinterpret_cast<uint64_t>(r12_3) < reinterpret_cast<uint64_t>(rdi->f8)) {
        while (1) {
            if (!r12_3->f0) {
                ++r12_3;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            } else {
                rbx4 = r12_3->f8;
                rdx5 = rbp2->f40;
                if (rbx4) {
                    while (1) {
                        if (rdx5) {
                            rdi6 = rbx4->f0;
                            rdx5(rdi6);
                            rdx5 = rbp2->f40;
                        }
                        rax7 = rbx4->f8;
                        rcx8 = rbp2->f48;
                        rbx4->f0 = 0;
                        rbx4->f8 = rcx8;
                        rbp2->f48 = rbx4;
                        if (!rax7) 
                            break;
                        rbx4 = rax7;
                    }
                }
                if (rdx5) {
                    rdi9 = r12_3->f0;
                    rdx5(rdi9);
                }
                r12_3->f0 = 0;
                ++r12_3;
                *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(r12_3) - 8) = 0;
                if (reinterpret_cast<uint64_t>(rbp2->f8) <= reinterpret_cast<uint64_t>(r12_3)) 
                    break;
            }
        }
    }
    rbp2->f18 = 0;
    rbp2->f20 = 0;
    return;
}

struct s32 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[23] pad32;
    int64_t f20;
    signed char[24] pad64;
    int64_t f40;
    void** f48;
};

void fun_45c3(struct s32* rdi, void** rsi) {
    struct s32* r12_3;
    void** r13_4;
    void** rax5;
    void** rbp6;
    void** rbx7;
    void** rdi8;
    void** rdi9;
    void** rbx10;
    void** rbx11;
    void** rdi12;
    void** rdi13;

    __asm__("cli ");
    r12_3 = rdi;
    r13_4 = rdi->f0;
    rax5 = rdi->f8;
    rbp6 = r13_4;
    if (!rdi->f40 || !rdi->f20) {
        addr_4633_2:
        if (reinterpret_cast<unsigned char>(rax5) > reinterpret_cast<unsigned char>(rbp6)) {
            do {
                rbx7 = *reinterpret_cast<void***>(rbp6 + 8);
                if (rbx7) {
                    do {
                        rdi8 = rbx7;
                        rbx7 = *reinterpret_cast<void***>(rbx7 + 8);
                        fun_2370(rdi8, rsi);
                    } while (rbx7);
                }
                rbp6 = rbp6 + 16;
            } while (reinterpret_cast<unsigned char>(r12_3->f8) > reinterpret_cast<unsigned char>(rbp6));
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_4) < reinterpret_cast<unsigned char>(rax5)) {
            while (1) {
                rdi9 = *reinterpret_cast<void***>(r13_4);
                if (!rdi9) {
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                } else {
                    rbx10 = r13_4;
                    while (r12_3->f40(rdi9), rbx10 = *reinterpret_cast<void***>(rbx10 + 8), !!rbx10) {
                        rdi9 = *reinterpret_cast<void***>(rbx10);
                    }
                    rax5 = r12_3->f8;
                    r13_4 = r13_4 + 16;
                    if (reinterpret_cast<unsigned char>(rax5) <= reinterpret_cast<unsigned char>(r13_4)) 
                        break;
                }
            }
            rbp6 = r12_3->f0;
            goto addr_4633_2;
        }
    }
    rbx11 = r12_3->f48;
    if (rbx11) {
        do {
            rdi12 = rbx11;
            rbx11 = *reinterpret_cast<void***>(rbx11 + 8);
            fun_2370(rdi12, rsi);
        } while (rbx11);
    }
    rdi13 = r12_3->f0;
    fun_2370(rdi13, rsi);
    goto fun_2370;
}

int64_t fun_46b3(void** rdi, uint64_t rsi) {
    void** r12_3;
    void** rax4;
    uint32_t esi5;
    void** rax6;
    int32_t r12d7;
    void** rax8;
    void** r13_9;
    void** v10;
    int32_t eax11;
    void** rdi12;
    int32_t eax13;
    int32_t eax14;
    void* rax15;
    int64_t rax16;

    __asm__("cli ");
    r12_3 = *reinterpret_cast<void***>(rdi + 40);
    rax4 = g28;
    esi5 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r12_3 + 16));
    __asm__("movss xmm0, [r12+0x8]");
    rax6 = compute_bucket_size_isra_0(rsi, *reinterpret_cast<signed char*>(&esi5));
    if (!rax6) 
        goto addr_47f0_2;
    if (*reinterpret_cast<void***>(rdi + 16) == rax6) {
        r12d7 = 1;
    } else {
        rax8 = fun_2520(rax6, 16);
        if (!rax8) {
            addr_47f0_2:
            r12d7 = 0;
        } else {
            r13_9 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 0x68 - 8 + 8) - 8 + 8);
            v10 = *reinterpret_cast<void***>(rdi + 72);
            eax11 = transfer_entries(r13_9, rdi, 0);
            r12d7 = eax11;
            if (*reinterpret_cast<signed char*>(&eax11)) {
                rdi12 = *reinterpret_cast<void***>(rdi);
                fun_2370(rdi12, rdi);
                *reinterpret_cast<void***>(rdi) = rax8;
                *reinterpret_cast<void***>(rdi + 8) = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax8) + reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax6) << 4));
                *reinterpret_cast<void***>(rdi + 16) = rax6;
                *reinterpret_cast<void***>(rdi + 24) = reinterpret_cast<void**>(0);
                *reinterpret_cast<void***>(rdi + 72) = v10;
            } else {
                *reinterpret_cast<void***>(rdi + 72) = v10;
                eax13 = transfer_entries(rdi, r13_9, 1);
                if (!*reinterpret_cast<signed char*>(&eax13)) 
                    goto 0x26ba;
                eax14 = transfer_entries(rdi, r13_9, 0);
                if (!*reinterpret_cast<signed char*>(&eax14)) 
                    goto 0x26ba;
                fun_2370(rax8, r13_9);
            }
        }
    }
    rax15 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax15) {
        fun_2460();
    } else {
        *reinterpret_cast<int32_t*>(&rax16) = r12d7;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax16) + 4) = 0;
        return rax16;
    }
}

signed char hash_rehash(void** rdi, ...);

struct s33 {
    void** f0;
    signed char[7] pad8;
    void** f8;
};

int64_t fun_4843(void** rdi, void** rsi, void*** rdx) {
    void** rax4;
    void** r12_5;
    void** rdx6;
    void** rbp7;
    void** rax8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    uint64_t rax13;
    int1_t cf14;
    signed char al15;
    void** rax16;
    struct s33* v17;
    int32_t r8d18;
    void** rax19;
    void* rax20;
    int64_t rax21;
    void** rdx22;

    __asm__("cli ");
    rax4 = g28;
    if (!rsi) 
        goto 0x26bf;
    r12_5 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24);
    rdx6 = r12_5;
    rbp7 = rsi;
    rax8 = hash_find_entry(rdi, rsi, rdx6, 0);
    if (!rax8) {
        rax9 = *reinterpret_cast<void***>(rdi + 24);
        if (reinterpret_cast<signed char>(rax9) >= reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rdi + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_495e_5; else 
                goto addr_48cf_6;
        }
        *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax9) & 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
        __asm__("pxor xmm5, xmm5");
        rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax9) >> 1) | rax12);
        rax10 = *reinterpret_cast<void***>(rdi + 16);
        __asm__("cvtsi2ss xmm5, rdx");
        __asm__("addss xmm5, xmm5");
        below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
        if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
            addr_48cf_6:
            __asm__("pxor xmm4, xmm4");
            __asm__("cvtsi2ss xmm4, rax");
        } else {
            addr_495e_5:
            *reinterpret_cast<uint32_t*>(&rax13) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax13) + 4) = 0;
            __asm__("pxor xmm4, xmm4");
            rdx6 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax13);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rdx6 == 0)));
            __asm__("cvtsi2ss xmm4, rdx");
            __asm__("addss xmm4, xmm4");
        }
        __asm__("movss xmm0, [rax+0x8]");
        __asm__("mulss xmm0, xmm4");
        __asm__("comiss xmm5, xmm0");
        if (!below_or_equal11 && (check_tuning(rdi), !below_or_equal11)) {
            __asm__("mulss xmm4, [rax+0xc]");
            cf14 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) < 0;
            if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rdi + 40) + 16)) {
                __asm__("mulss xmm4, xmm0");
            }
            __asm__("comiss xmm4, [rip+0x4e51]");
            if (!cf14) 
                goto addr_49b5_12;
            __asm__("comiss xmm4, [rip+0x4e01]");
            if (!cf14) {
                __asm__("subss xmm4, [rip+0x4dc0]");
                __asm__("cvttss2si rsi, xmm4");
                __asm__("btc rsi, 0x3f");
            } else {
                __asm__("cvttss2si rsi, xmm4");
            }
            al15 = hash_rehash(rdi);
            if (!al15) 
                goto addr_49b5_12;
            rdx6 = r12_5;
            rsi = rbp7;
            rax16 = hash_find_entry(rdi, rsi, rdx6, 0);
            if (rax16) {
                goto 0x26bf;
            }
        }
        if (!v17->f0) {
            v17->f0 = rbp7;
            r8d18 = 1;
            *reinterpret_cast<int64_t*>(rdi + 32) = *reinterpret_cast<int64_t*>(rdi + 32) + 1;
            *reinterpret_cast<void***>(rdi + 24) = *reinterpret_cast<void***>(rdi + 24) + 1;
        } else {
            rax19 = *reinterpret_cast<void***>(rdi + 72);
            if (!rax19) {
                rax19 = fun_2580(16, rsi, rdx6);
                if (!rax19) {
                    addr_49b5_12:
                    r8d18 = -1;
                } else {
                    goto addr_4912_24;
                }
            } else {
                *reinterpret_cast<void***>(rdi + 72) = *reinterpret_cast<void***>(rax19 + 8);
                goto addr_4912_24;
            }
        }
    } else {
        r8d18 = 0;
        if (rdx) {
            *rdx = rax8;
        }
    }
    addr_488e_28:
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2460();
    } else {
        *reinterpret_cast<int32_t*>(&rax21) = r8d18;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax21) + 4) = 0;
        return rax21;
    }
    addr_4912_24:
    rdx22 = v17->f8;
    *reinterpret_cast<void***>(rax19) = rbp7;
    r8d18 = 1;
    *reinterpret_cast<void***>(rax19 + 8) = rdx22;
    v17->f8 = rax19;
    *reinterpret_cast<int64_t*>(rdi + 32) = *reinterpret_cast<int64_t*>(rdi + 32) + 1;
    goto addr_488e_28;
}

int32_t hash_insert_if_absent();

int64_t fun_4a63() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int64_t rsi4;
    int64_t v5;
    void* rdx6;

    __asm__("cli ");
    rax1 = g28;
    eax2 = hash_insert_if_absent();
    if (eax2 == -1) {
        *reinterpret_cast<int32_t*>(&rax3) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    } else {
        rax3 = rsi4;
        if (!eax2) {
            rax3 = v5;
        }
    }
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2460();
    } else {
        return rax3;
    }
}

void** fun_4ac3(void** rdi, void** rsi) {
    void** rbx3;
    void** rax4;
    void** v5;
    void** rax6;
    void** r12_7;
    int64_t* v8;
    void** rax9;
    void** rax10;
    uint1_t below_or_equal11;
    uint64_t rax12;
    signed char al13;
    void** rbp14;
    void** rdi15;
    void* rax16;

    __asm__("cli ");
    rbx3 = rdi;
    rax4 = g28;
    v5 = rax4;
    rax6 = hash_find_entry(rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 16, 1);
    r12_7 = rax6;
    if (rax6 && (*reinterpret_cast<int64_t*>(rbx3 + 32) = *reinterpret_cast<int64_t*>(rbx3 + 32) - 1, *v8 == 0)) {
        rax9 = *reinterpret_cast<void***>(rbx3 + 24) - 1;
        *reinterpret_cast<void***>(rbx3 + 24) = rax9;
        if (reinterpret_cast<signed char>(rax9) < reinterpret_cast<signed char>(0)) {
            __asm__("pxor xmm5, xmm5");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            __asm__("cvtsi2ss xmm5, rdx");
            __asm__("addss xmm5, xmm5");
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) >= reinterpret_cast<signed char>(0)) {
                addr_4b50_4:
                __asm__("pxor xmm4, xmm4");
                __asm__("cvtsi2ss xmm4, rax");
            } else {
                addr_4c06_5:
                *reinterpret_cast<uint32_t*>(&rax12) = *reinterpret_cast<uint32_t*>(&rax10) & 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax12) + 4) = 0;
                __asm__("pxor xmm4, xmm4");
                below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>((reinterpret_cast<uint64_t>(reinterpret_cast<unsigned char>(rax10) >> 1) | rax12) == 0)));
                __asm__("cvtsi2ss xmm4, rdx");
                __asm__("addss xmm4, xmm4");
            }
            __asm__("movss xmm0, [rax]");
            __asm__("mulss xmm0, xmm4");
            __asm__("comiss xmm0, xmm5");
            if (!below_or_equal11 && (check_tuning(rbx3, rbx3), !below_or_equal11)) {
                __asm__("mulss xmm4, [rax+0x4]");
                if (!*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) {
                    __asm__("mulss xmm4, [rax+0x8]");
                }
                __asm__("comiss xmm4, [rip+0x4c6e]");
                if (reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(*reinterpret_cast<void***>(rbx3 + 40) + 16)) >= 0) {
                    __asm__("subss xmm4, [rip+0x4bd8]");
                    __asm__("cvttss2si rsi, xmm4");
                    __asm__("btc rsi, 0x3f");
                } else {
                    __asm__("cvttss2si rsi, xmm4");
                }
                al13 = hash_rehash(rbx3, rbx3);
                if (!al13) {
                    rbp14 = *reinterpret_cast<void***>(rbx3 + 72);
                    if (rbp14) {
                        do {
                            rdi15 = rbp14;
                            rbp14 = *reinterpret_cast<void***>(rbp14 + 8);
                            fun_2370(rdi15, rsi, rdi15, rsi);
                        } while (rbp14);
                    }
                    *reinterpret_cast<void***>(rbx3 + 72) = reinterpret_cast<void**>(0);
                }
            }
        } else {
            __asm__("pxor xmm5, xmm5");
            __asm__("cvtsi2ss xmm5, rax");
            rax10 = *reinterpret_cast<void***>(rbx3 + 16);
            below_or_equal11 = reinterpret_cast<uint1_t>(static_cast<uint32_t>(reinterpret_cast<uint1_t>(rax10 == 0)));
            if (reinterpret_cast<signed char>(rax10) < reinterpret_cast<signed char>(0)) 
                goto addr_4c06_5; else 
                goto addr_4b50_4;
        }
    }
    rax16 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v5) - reinterpret_cast<unsigned char>(g28));
    if (rax16) {
        fun_2460();
    } else {
        return r12_7;
    }
}

void fun_4c53() {
    __asm__("cli ");
    goto 0x4ac0;
}

struct s34 {
    int64_t f0;
    uint64_t f8;
};

uint64_t hash_pjw(int64_t rdi);

uint64_t fun_4c63(struct s34* rdi, int64_t rsi) {
    int64_t rdi3;
    uint64_t rax4;

    __asm__("cli ");
    rdi3 = rdi->f0;
    rax4 = hash_pjw(rdi3);
    return (rax4 ^ rdi->f8) % rsi;
}

struct s35 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
};

struct s36 {
    int64_t f0;
    void** f8;
    signed char[7] pad16;
    int64_t f10;
};

int64_t fun_4c93(struct s35* rdi, struct s36* rsi) {
    void** rdx3;
    int64_t rsi4;
    int64_t rdi5;
    int64_t rax6;

    __asm__("cli ");
    rdx3 = rsi->f8;
    if (rdi->f8 != rdx3 || rdi->f10 != rsi->f10) {
        return 0;
    } else {
        rsi4 = rsi->f0;
        rdi5 = rdi->f0;
        rax6 = fun_2530(rdi5, rsi4, rdx3);
        *reinterpret_cast<unsigned char*>(&rax6) = reinterpret_cast<uint1_t>(*reinterpret_cast<int32_t*>(&rax6) == 0);
        return rax6;
    }
}

void fun_4cd3(void*** rdi, void** rsi) {
    void** rdi3;

    __asm__("cli ");
    rdi3 = *rdi;
    fun_2370(rdi3, rsi);
    goto fun_2370;
}

void fun_2650(void** rdi, int64_t rsi, int64_t rdx, void** rcx);

struct s37 {
    signed char[1] pad1;
    void** f1;
    signed char[2] pad4;
    void** f4;
};

struct s37* fun_24a0();

void** __progname = reinterpret_cast<void**>(0);

void** __progname_full = reinterpret_cast<void**>(0);

void fun_4cf3(void** rdi) {
    void** rcx2;
    void** rbx3;
    struct s37* rax4;
    void** r12_5;
    void** rcx6;
    int32_t eax7;

    __asm__("cli ");
    if (!rdi) {
        rcx2 = stderr;
        fun_2650("A NULL argv[0] was passed through an exec system call.\n", 1, 55, rcx2);
        fun_2380("A NULL argv[0] was passed through an exec system call.\n", "A NULL argv[0] was passed through an exec system call.\n");
    } else {
        rbx3 = rdi;
        rax4 = fun_24a0();
        if (rax4 && ((r12_5 = reinterpret_cast<void**>(&rax4->f1), reinterpret_cast<int64_t>(reinterpret_cast<unsigned char>(r12_5) - reinterpret_cast<unsigned char>(rbx3)) > reinterpret_cast<int64_t>(6)) && (eax7 = fun_23a0(reinterpret_cast<int64_t>(rax4) + 0xfffffffffffffffa, "/.libs/", 7, rcx6), !eax7))) {
            if (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(&rax4->f1) == 0x6c) || (!reinterpret_cast<int1_t>(*reinterpret_cast<void***>(r12_5 + 1) == 0x74) || *reinterpret_cast<unsigned char*>(r12_5 + 2) != 45)) {
                rbx3 = r12_5;
            } else {
                rbx3 = reinterpret_cast<void**>(&rax4->f4);
                __progname = rbx3;
            }
        }
        program_name = rbx3;
        __progname_full = rbx3;
        return;
    }
}

void xmemdup(int64_t rdi, int64_t rsi);

void fun_6493(int64_t rdi) {
    int64_t rbp2;
    int32_t* rax3;
    int32_t r12d4;

    __asm__("cli ");
    rbp2 = rdi;
    rax3 = fun_2390();
    r12d4 = *rax3;
    if (!rbp2) {
        rbp2 = 0xd1e0;
    }
    xmemdup(rbp2, 56);
    *rax3 = r12d4;
    return;
}

int64_t fun_64d3(int32_t* rdi) {
    int64_t rax2;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd1e0);
    }
    *reinterpret_cast<int32_t*>(&rax2) = *rdi;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax2) + 4) = 0;
    return rax2;
}

int32_t* fun_64f3(int32_t* rdi, int32_t esi) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<int32_t*>(0xd1e0);
    }
    *rdi = esi;
    return 0xd1e0;
}

int64_t fun_6513(void* rdi, uint32_t esi, uint32_t edx) {
    uint32_t eax4;
    uint32_t ecx5;
    int64_t rax6;
    uint32_t* rsi7;
    uint32_t eax8;
    int64_t rax9;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<void*>(0xd1e0);
    }
    eax4 = esi;
    ecx5 = esi & 31;
    *reinterpret_cast<uint32_t*>(&rax6) = *reinterpret_cast<unsigned char*>(&eax4) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax6) + 4) = 0;
    rsi7 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rdi) + rax6 * 4 + 8);
    eax8 = *rsi7 >> *reinterpret_cast<unsigned char*>(&ecx5);
    *reinterpret_cast<uint32_t*>(&rax9) = eax8 & 1;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax9) + 4) = 0;
    *rsi7 = ((edx ^ eax8) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rsi7;
    return rax9;
}

struct s38 {
    signed char[4] pad4;
    int32_t f4;
};

int64_t fun_6553(struct s38* rdi, int32_t esi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s38*>(0xd1e0);
    }
    *reinterpret_cast<int32_t*>(&rax3) = rdi->f4;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    rdi->f4 = esi;
    return rax3;
}

struct s39 {
    int32_t f0;
    signed char[36] pad40;
    int64_t f28;
    int64_t f30;
};

struct s39* fun_6573(struct s39* rdi, int64_t rsi, int64_t rdx) {
    __asm__("cli ");
    if (!rdi) {
        rdi = reinterpret_cast<struct s39*>(0xd1e0);
    }
    rdi->f0 = 10;
    if (!rsi) 
        goto 0x26ce;
    if (!rdx) 
        goto 0x26ce;
    rdi->f28 = rsi;
    rdi->f30 = rdx;
    return 0xd1e0;
}

struct s40 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_65b3(void** rdi, void** rsi, int64_t rdx, int64_t rcx, struct s40* r8) {
    struct s40* rbx6;
    int32_t* rax7;
    int32_t r15d8;
    uint32_t r9d9;
    int64_t v10;
    uint32_t r8d11;
    int64_t v12;
    void** rax13;

    __asm__("cli ");
    rbx6 = r8;
    if (!r8) {
        rbx6 = reinterpret_cast<struct s40*>(0xd1e0);
    }
    rax7 = fun_2390();
    r15d8 = *rax7;
    r9d9 = rbx6->f4;
    v10 = rbx6->f30;
    r8d11 = rbx6->f0;
    v12 = rbx6->f28;
    rax13 = quotearg_buffer_restyled(rdi, rsi, rdx, rcx, r8d11, r9d9, &rbx6->f8, v12, v10, 0x65e6);
    *rax7 = r15d8;
    return rax13;
}

struct s41 {
    uint32_t f0;
    uint32_t f4;
    void** f8;
    signed char[31] pad40;
    int64_t f28;
    int64_t f30;
};

void** fun_6633(int64_t rdi, int64_t rsi, void*** rdx, struct s41* rcx) {
    struct s41* rbx5;
    int32_t* rax6;
    uint32_t r9d7;
    void** r10_8;
    uint32_t r9d9;
    uint32_t r8d10;
    int32_t v11;
    int64_t v12;
    int64_t v13;
    void** rax14;
    void** rsi15;
    void** rax16;
    int64_t v17;
    uint32_t r8d18;
    int64_t v19;

    __asm__("cli ");
    rbx5 = rcx;
    if (!rcx) {
        rbx5 = reinterpret_cast<struct s41*>(0xd1e0);
    }
    rax6 = fun_2390();
    r9d7 = 0;
    *reinterpret_cast<unsigned char*>(&r9d7) = reinterpret_cast<uint1_t>(rdx == 0);
    r10_8 = reinterpret_cast<void**>(&rbx5->f8);
    r9d9 = r9d7 | rbx5->f4;
    r8d10 = rbx5->f0;
    v11 = *rax6;
    v12 = rbx5->f30;
    v13 = rbx5->f28;
    rax14 = quotearg_buffer_restyled(0, 0, rdi, rsi, r8d10, r9d9, r10_8, v13, v12, 0x6661);
    rsi15 = rax14 + 1;
    rax16 = xcharalloc(rsi15);
    v17 = rbx5->f30;
    r8d18 = rbx5->f0;
    v19 = rbx5->f28;
    quotearg_buffer_restyled(rax16, rsi15, rdi, rsi, r8d18, r9d9, r10_8, v19, v17, 0x66bc);
    *rax6 = v11;
    if (rdx) {
        *rdx = rax14;
    }
    return rax16;
}

void fun_6723() {
    __asm__("cli ");
}

void** gd078 = reinterpret_cast<void**>(0xe0);

int64_t slotvec0 = 0x100;

void fun_6733() {
    uint32_t eax1;
    void** r12_2;
    uint64_t rax3;
    void*** rbx4;
    void*** rbp5;
    void** rdi6;
    void** rsi7;
    void** rdi8;
    void** rsi9;
    void** rsi10;

    __asm__("cli ");
    eax1 = nslots;
    r12_2 = slotvec;
    if (reinterpret_cast<int32_t>(eax1) > reinterpret_cast<int32_t>(1)) {
        *reinterpret_cast<uint32_t*>(&rax3) = eax1 - 2;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        rbx4 = reinterpret_cast<void***>(r12_2 + 24);
        rbp5 = reinterpret_cast<void***>(reinterpret_cast<unsigned char>(r12_2) + (rax3 << 4) + 40);
        do {
            rdi6 = *rbx4;
            rbx4 = rbx4 + 16;
            fun_2370(rdi6, rsi7);
        } while (rbx4 != rbp5);
    }
    rdi8 = *reinterpret_cast<void***>(r12_2 + 8);
    if (rdi8 != 0xd0e0) {
        fun_2370(rdi8, rsi9);
        gd078 = reinterpret_cast<void**>(0xd0e0);
        slotvec0 = 0x100;
    }
    if (r12_2 != 0xd070) {
        fun_2370(r12_2, rsi10);
        slotvec = reinterpret_cast<void**>(0xd070);
    }
    nslots = 1;
    return;
}

void fun_67d3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_67f3() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6803(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_6823(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void** fun_6843(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26d4;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2460();
    } else {
        return rax6;
    }
}

void** fun_68d3(void** rdi, int32_t esi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    rcx5 = g28;
    if (esi == 10) 
        goto 0x26d9;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rdx, rcx, rcx6, rdi, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2460();
    } else {
        return rax7;
    }
}

void** fun_6963(int32_t edi, int64_t rsi) {
    void** rax3;
    struct s2* rcx4;
    void** rax5;
    void* rdx6;

    __asm__("cli ");
    rax3 = g28;
    if (edi == 10) 
        goto 0x26de;
    rcx4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax5 = quotearg_n_options(0, rsi, -1, rcx4, 0, rsi, -1, rcx4);
    rdx6 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx6) {
        fun_2460();
    } else {
        return rax5;
    }
}

void** fun_69f3(int32_t edi, int64_t rsi, int64_t rdx) {
    void** rax4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rax4 = g28;
    if (edi == 10) 
        goto 0x26e3;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rsi, rdx, rcx5, 0, rsi, rdx, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2460();
    } else {
        return rax6;
    }
}

void** fun_6a83(int64_t rdi, int64_t rsi, uint32_t edx) {
    struct s2* rsp4;
    void** rax5;
    uint32_t ecx6;
    uint32_t eax7;
    int64_t rax8;
    uint32_t* rdx9;
    void** rax10;
    void* rdx11;

    __asm__("cli ");
    rsp4 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x6750]");
    __asm__("movdqa xmm1, [rip+0x6758]");
    rax5 = g28;
    ecx6 = edx & 31;
    __asm__("movdqa xmm2, [rip+0x6741]");
    __asm__("movaps [rsp], xmm0");
    eax7 = edx;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax8) = *reinterpret_cast<unsigned char*>(&eax7) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx9 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp4) + rax8 * 4 + 8);
    *rdx9 = (~(*rdx9 >> *reinterpret_cast<unsigned char*>(&ecx6)) & 1) << *reinterpret_cast<unsigned char*>(&ecx6) ^ *rdx9;
    rax10 = quotearg_n_options(0, rdi, rsi, rsp4);
    rdx11 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rdx11) {
        fun_2460();
    } else {
        return rax10;
    }
}

void** fun_6b23(int64_t rdi, uint32_t esi) {
    struct s2* rsp3;
    void** rax4;
    uint32_t ecx5;
    uint32_t eax6;
    int64_t rax7;
    uint32_t* rdx8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    rsp3 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    __asm__("movdqa xmm0, [rip+0x66b0]");
    __asm__("movdqa xmm1, [rip+0x66b8]");
    rax4 = g28;
    ecx5 = esi & 31;
    __asm__("movdqa xmm2, [rip+0x66a1]");
    __asm__("movaps [rsp], xmm0");
    eax6 = esi;
    __asm__("movaps [rsp+0x10], xmm1");
    *reinterpret_cast<uint32_t*>(&rax7) = *reinterpret_cast<unsigned char*>(&eax6) >> 5;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
    __asm__("movaps [rsp+0x20], xmm2");
    rdx8 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(rsp3) + rax7 * 4 + 8);
    *rdx8 = (~(*rdx8 >> *reinterpret_cast<unsigned char*>(&ecx5)) & 1) << *reinterpret_cast<unsigned char*>(&ecx5) ^ *rdx8;
    rax9 = quotearg_n_options(0, rdi, -1, rsp3);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax4) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2460();
    } else {
        return rax9;
    }
}

void** fun_6bc3(int64_t rdi) {
    void** rax2;
    void** rax3;
    void* rdx4;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6610]");
    __asm__("movdqa xmm1, [rip+0x6618]");
    rax2 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movdqa xmm2, [rip+0x65f9]");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax3 = quotearg_n_options(0, rdi, -1, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx4 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax2) - reinterpret_cast<unsigned char>(g28));
    if (rdx4) {
        fun_2460();
    } else {
        return rax3;
    }
}

void** fun_6c53(int64_t rdi, int64_t rsi) {
    void** rax3;
    void** rax4;
    void* rdx5;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x6580]");
    __asm__("movdqa xmm1, [rip+0x6588]");
    rax3 = g28;
    __asm__("movdqa xmm2, [rip+0x6576]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    rax4 = quotearg_n_options(0, rdi, rsi, reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rdx5 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax3) - reinterpret_cast<unsigned char>(g28));
    if (rdx5) {
        fun_2460();
    } else {
        return rax4;
    }
}

void** fun_6ce3(void** rdi, int32_t esi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    rdx4 = g28;
    if (esi == 10) 
        goto 0x26e8;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(rdi, rdx, -1, rcx5, rdi, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2460();
    } else {
        return rax6;
    }
}

void** fun_6d83(void** rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x644a]");
    rcx5 = g28;
    __asm__("movdqa xmm1, [rip+0x6442]");
    __asm__("movdqa xmm2, [rip+0x644a]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26ed;
    if (!rdx) 
        goto 0x26ed;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(rdi, rcx, -1, rcx6, rdi, rcx, -1, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2460();
    } else {
        return rax7;
    }
}

void** fun_6e23(int32_t edi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8) {
    void** rcx6;
    struct s2* rcx7;
    void** rdi8;
    void** rax9;
    void* rdx10;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x63aa]");
    __asm__("movdqa xmm1, [rip+0x63b2]");
    __asm__("movdqa xmm2, [rip+0x63ba]");
    rcx6 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rsi) 
        goto 0x26f2;
    if (!rdx) 
        goto 0x26f2;
    rcx7 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    *reinterpret_cast<int32_t*>(&rdi8) = edi;
    *reinterpret_cast<int32_t*>(&rdi8 + 4) = 0;
    rax9 = quotearg_n_options(rdi8, rcx, r8, rcx7, rdi8, rcx, r8, rcx7);
    rdx10 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx6) - reinterpret_cast<unsigned char>(g28));
    if (rdx10) {
        fun_2460();
    } else {
        return rax9;
    }
}

void** fun_6ed3(int64_t rdi, int64_t rsi, int64_t rdx) {
    void** rdx4;
    struct s2* rcx5;
    void** rax6;
    void* rdx7;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x62fa]");
    rdx4 = g28;
    __asm__("movdqa xmm1, [rip+0x62f2]");
    __asm__("movdqa xmm2, [rip+0x62fa]");
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26f7;
    if (!rsi) 
        goto 0x26f7;
    rcx5 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax6 = quotearg_n_options(0, rdx, -1, rcx5, 0, rdx, -1, rcx5);
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rdx4) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2460();
    } else {
        return rax6;
    }
}

void** fun_6f73(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx) {
    void** rcx5;
    struct s2* rcx6;
    void** rax7;
    void* rdx8;

    __asm__("cli ");
    __asm__("movdqa xmm0, [rip+0x625a]");
    __asm__("movdqa xmm1, [rip+0x6262]");
    __asm__("movdqa xmm2, [rip+0x626a]");
    rcx5 = g28;
    __asm__("movaps [rsp], xmm0");
    __asm__("movaps [rsp+0x10], xmm1");
    __asm__("movaps [rsp+0x20], xmm2");
    if (!rdi) 
        goto 0x26fc;
    if (!rsi) 
        goto 0x26fc;
    rcx6 = reinterpret_cast<struct s2*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 72);
    rax7 = quotearg_n_options(0, rdx, rcx, rcx6, 0, rdx, rcx, rcx6);
    rdx8 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rcx5) - reinterpret_cast<unsigned char>(g28));
    if (rdx8) {
        fun_2460();
    } else {
        return rax7;
    }
}

void fun_7013() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7023(int64_t rdi, int64_t rsi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7043() {
    __asm__("cli ");
    goto quotearg_n_options;
}

void fun_7063(int64_t rdi) {
    __asm__("cli ");
    goto quotearg_n_options;
}

struct s42 {
    signed char[16] pad16;
    void** f10;
};

void** fun_7083(struct s42* rdi, void** rsi, void** rdx) {
    void** rdi4;
    void** r12_5;
    void** rax6;

    __asm__("cli ");
    rdi4 = reinterpret_cast<void**>(&rdi->f10);
    r12_5 = *reinterpret_cast<void***>(rdi4 + 0xfffffffffffffff0);
    if (r12_5 == rdi4) {
        rax6 = fun_2580(rsi, rsi, rdx);
        if (rax6) {
            goto fun_2560;
        }
    } else {
        rax6 = fun_25c0(r12_5, rsi, rdx);
        if (!rax6) {
            rax6 = r12_5;
        }
    }
    return rax6;
}

struct s43 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_70e3(struct s43* rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rdi5;
    void** r12_6;
    void** rbp7;
    int32_t* rax8;
    void** rax9;

    __asm__("cli ");
    rax4 = rdi->f8;
    rdi5 = rdi->f0;
    r12_6 = reinterpret_cast<void**>(&rdi->f10);
    rbp7 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rax4));
    if (rdi5 != r12_6) {
        fun_2370(rdi5, rsi);
        rax4 = rdi->f8;
    }
    if (reinterpret_cast<unsigned char>(rax4) > reinterpret_cast<unsigned char>(rbp7)) {
        rax8 = fun_2390();
        *rax8 = 12;
    } else {
        rax9 = fun_2580(rbp7, rsi, rdx);
        if (rax9) {
            rdi->f0 = rax9;
            rdi->f8 = rbp7;
            return 1;
        }
    }
    rdi->f0 = r12_6;
    rdi->f8 = reinterpret_cast<void**>(0x400);
    return 0;
}

struct s44 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
};

int64_t fun_7163(struct s44* rdi, void** rsi, void** rdx, int64_t rcx) {
    void** r14_5;
    void** r13_6;
    void** r12_7;
    void** rbp8;
    void** rax9;
    int64_t rax10;
    void** rax11;
    void** rcx12;
    int32_t* rax13;
    void** rax14;

    __asm__("cli ");
    r14_5 = reinterpret_cast<void**>(&rdi->f10);
    r13_6 = rdi->f8;
    r12_7 = rdi->f0;
    rbp8 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_6) + reinterpret_cast<unsigned char>(r13_6));
    if (r12_7 == r14_5) {
        rax9 = fun_2580(rbp8, rsi, rdx);
        if (!rax9) {
            *reinterpret_cast<int32_t*>(&rax10) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
        } else {
            rax11 = fun_2560(rax9, r12_7, r13_6, rcx);
            rcx12 = rax11;
            goto addr_719c_5;
        }
    } else {
        if (reinterpret_cast<unsigned char>(r13_6) > reinterpret_cast<unsigned char>(rbp8)) {
            rax13 = fun_2390();
            *rax13 = 12;
            goto addr_71eb_8;
        } else {
            rsi = rbp8;
            rax14 = fun_25c0(r12_7, rsi, rdx);
            rcx12 = rax14;
            if (!rax14) {
                r12_7 = rdi->f0;
                goto addr_71eb_8;
            } else {
                addr_719c_5:
                rdi->f0 = rcx12;
                *reinterpret_cast<int32_t*>(&rax10) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
                rdi->f8 = rbp8;
            }
        }
    }
    addr_71a8_11:
    return rax10;
    addr_71eb_8:
    fun_2370(r12_7, rsi);
    rdi->f0 = r14_5;
    *reinterpret_cast<int32_t*>(&rax10) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
    rdi->f8 = reinterpret_cast<void**>(0x400);
    goto addr_71a8_11;
}

struct s45 {
    void** f0;
    signed char[7] pad8;
    void** f8;
    signed char[7] pad16;
    void** f10;
    signed char[7] pad24;
    int64_t f18;
    int64_t f20;
    int64_t f28;
    int64_t f30;
    int64_t f38;
    int64_t f40;
};

void fun_2540(int64_t rdi, void** rsi, void** rdx, void** rcx, void** r8, void** r9);

void fun_7223(void** rdi, void** rsi, void** rdx, void** rcx, struct s45* r8, void** r9) {
    void** r12_7;
    int64_t v8;
    int64_t v9;
    int64_t v10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;
    int64_t v15;
    int64_t v16;
    int64_t v17;
    int64_t v18;
    int64_t v19;
    void** rax20;
    int64_t v21;
    int64_t v22;
    int64_t v23;
    int64_t v24;
    int64_t v25;
    int64_t v26;
    void** rax27;
    int64_t v28;
    int64_t v29;
    int64_t v30;
    int64_t v31;
    int64_t v32;
    int64_t v33;
    int64_t r10_34;
    int64_t r9_35;
    int64_t r8_36;
    int64_t rcx37;
    int64_t r15_38;
    int64_t v39;
    void** r14_40;
    void** r13_41;
    void** r12_42;
    void** rax43;

    __asm__("cli ");
    r12_7 = r9;
    if (!rsi) {
        fun_2660(rdi, 1, "%s %s\n", rdx, rcx, r9, v8, v9, v10, v11, v12, v13);
    } else {
        r9 = rcx;
        fun_2660(rdi, 1, "%s (%s) %s\n", rsi, rdx, r9, v14, v15, v16, v17, v18, v19);
    }
    rax20 = fun_2430();
    fun_2660(rdi, 1, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9, v21, v22, v23, v24, v25, v26);
    fun_2540(10, rdi, "Copyright %s %d Free Software Foundation, Inc.", rax20, 0x7e6, r9);
    rax27 = fun_2430();
    fun_2660(rdi, 1, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9, v28, v29, v30, v31, v32, v33);
    fun_2540(10, rdi, rax27, "https://gnu.org/licenses/gpl.html", 0x7e6, r9);
    if (reinterpret_cast<unsigned char>(r12_7) > reinterpret_cast<unsigned char>(9)) {
        r10_34 = r8->f38;
        r9_35 = r8->f30;
        r8_36 = r8->f28;
        rcx37 = r8->f20;
        r15_38 = r8->f18;
        v39 = r8->f40;
        r14_40 = r8->f10;
        r13_41 = r8->f8;
        r12_42 = r8->f0;
        rax43 = fun_2430();
        fun_2660(rdi, 1, rax43, r12_42, r13_41, r14_40, r15_38, rcx37, r8_36, r9_35, r10_34, v39);
        return;
    } else {
        goto *reinterpret_cast<int32_t*>(0x9f28 + reinterpret_cast<unsigned char>(r12_7) * 4) + 0x9f28;
    }
}

void version_etc_arn(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx);

void fun_7693() {
    int64_t r9_1;
    int64_t* r8_2;
    int64_t* r8_3;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&r9_1) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_1) + 4) = 0;
    if (*r8_2) {
        do {
            ++r9_1;
        } while (r8_3[r9_1]);
    }
    goto version_etc_arn;
}

struct s46 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t* f8;
    int64_t f10;
};

void fun_76b3(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, struct s46* r8) {
    int64_t r11_6;
    int64_t r10_7;
    struct s46* rcx8;
    void** rax9;
    void** v10;
    int64_t r9_11;
    int64_t* r8_12;
    int64_t rdx13;
    int64_t* rdx14;
    int64_t rax15;
    int64_t* rdx16;
    int64_t rax17;
    void* rax18;

    __asm__("cli ");
    r11_6 = rcx;
    r10_7 = rdx;
    rcx8 = r8;
    rax9 = g28;
    v10 = rax9;
    *reinterpret_cast<int32_t*>(&r9_11) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_11) + 4) = 0;
    r8_12 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 0x68);
    do {
        if (rcx8->f0 <= 47) {
            *reinterpret_cast<uint32_t*>(&rdx13) = rcx8->f0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx13) + 4) = 0;
            rdx14 = reinterpret_cast<int64_t*>(rdx13 + rcx8->f10);
            rcx8->f0 = rcx8->f0 + 8;
            rax15 = *rdx14;
            r8_12[r9_11] = rax15;
            if (!rax15) 
                break;
        } else {
            rdx16 = rcx8->f8;
            rcx8->f8 = rdx16 + 1;
            rax17 = *rdx16;
            r8_12[r9_11] = rax17;
            if (!rax17) 
                break;
        }
        ++r9_11;
    } while (r9_11 != 10);
    version_etc_arn(rdi, rsi, r10_7, r11_6);
    rax18 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v10) - reinterpret_cast<unsigned char>(g28));
    if (rax18) {
        fun_2460();
    } else {
        return;
    }
}

void fun_7753(int64_t rdi, int64_t rsi, int64_t rdx, int64_t rcx, int64_t r8, int64_t r9) {
    int64_t r10_7;
    int64_t r11_8;
    int64_t r12_9;
    uint32_t edx10;
    void* rsp11;
    void* rdi12;
    int64_t* r8_13;
    int64_t r9_14;
    void** rax15;
    void** v16;
    int64_t rax17;
    int64_t rax18;
    int64_t v19;
    void* rax20;

    __asm__("cli ");
    r10_7 = rdi;
    r11_8 = rsi;
    r12_9 = rdx;
    edx10 = 32;
    rsp11 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 0xb0);
    rdi12 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp11) + 0x80);
    r8_13 = reinterpret_cast<int64_t*>(reinterpret_cast<int64_t>(rsp11) + 32);
    *reinterpret_cast<int32_t*>(&r9_14) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r9_14) + 4) = 0;
    rax15 = g28;
    v16 = rax15;
    do {
        if (edx10 <= 47) {
            *reinterpret_cast<uint32_t*>(&rax17) = edx10;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax17) + 4) = 0;
            edx10 = edx10 + 8;
            rax18 = *reinterpret_cast<int64_t*>(rax17 + reinterpret_cast<int64_t>(rdi12));
            r8_13[r9_14] = rax18;
            if (!rax18) 
                break;
        } else {
            r8_13[r9_14] = v19;
            if (!v19) 
                goto addr_77f6_5;
        }
        ++r9_14;
    } while (r9_14 != 10);
    addr_7800_7:
    version_etc_arn(r10_7, r11_8, r12_9, rcx);
    rax20 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v16) - reinterpret_cast<unsigned char>(g28));
    if (rax20) {
        fun_2460();
    } else {
        return;
    }
    addr_77f6_5:
    goto addr_7800_7;
}

void fun_7833() {
    void** rsi1;
    void** rdx2;
    void** rcx3;
    void** r8_4;
    void** r9_5;
    void** rax6;
    void** rcx7;
    void** rax8;

    __asm__("cli ");
    rsi1 = stdout;
    fun_2540(10, rsi1, rdx2, rcx3, r8_4, r9_5);
    rax6 = fun_2430();
    fun_25e0(1, rax6, "bug-coreutils@gnu.org", rcx7);
    rax8 = fun_2430();
    fun_25e0(1, rax8, "GNU coreutils", "https://www.gnu.org/software/coreutils/");
    fun_2430();
    goto fun_25e0;
}

int64_t fun_23d0();

void fun_78d3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7913(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2580(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7933(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2580(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7953(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2580(rdi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7973(void** rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_25c0(rdi, rsi, rdx);
    if (rax4 || rdi && !rsi) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_79a3(void** rdi, uint64_t rsi, void** rdx) {
    uint64_t rax4;
    void** rax5;

    __asm__("cli ");
    *reinterpret_cast<int32_t*>(&rax4) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
    *reinterpret_cast<unsigned char*>(&rax4) = reinterpret_cast<uint1_t>(rsi == 0);
    rax5 = fun_25c0(rdi, rsi | rax4, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_79d3(int64_t rdi, int64_t rsi, int64_t rdx) {
    int64_t rax4;

    __asm__("cli ");
    rax4 = fun_23d0();
    if (rax4 || rdi && (!rsi || !rdx)) {
        return;
    } else {
        xalloc_die();
    }
}

void fun_7a13() {
    int64_t rsi1;
    int64_t rdx2;
    int64_t rax3;

    __asm__("cli ");
    if (!rsi1 || !rdx2) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7a53(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7a83(int64_t rdi, int64_t rsi) {
    int64_t rax3;

    __asm__("cli ");
    if (!rdi || !rsi) {
    }
    rax3 = fun_23d0();
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7ad3(int64_t rdi, uint64_t* rsi) {
    uint64_t* rbp3;
    uint64_t rbx4;
    int64_t rax5;
    uint64_t tmp64_6;
    int1_t cf7;
    int64_t rax8;

    __asm__("cli ");
    rbp3 = rsi;
    rbx4 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx4) {
                rbx4 = 0x80;
            }
            rax5 = fun_23d0();
            if (rax5) 
                break;
            addr_7b1d_5:
            xalloc_die();
        }
        *rbp3 = rbx4;
        return;
    } else {
        tmp64_6 = rbx4 + ((rbx4 >> 1) + 1);
        cf7 = tmp64_6 < rbx4;
        rbx4 = tmp64_6;
        if (cf7) 
            goto addr_7b1d_5;
        rax8 = fun_23d0();
        if (rax8) 
            goto addr_7b06_9;
        if (rbx4) 
            goto addr_7b1d_5;
        addr_7b06_9:
        *rbp3 = rbx4;
        return;
    }
}

void fun_7b63(int64_t rdi, uint64_t* rsi, uint64_t rdx) {
    uint64_t r12_4;
    uint64_t* rbp5;
    uint64_t rbx6;
    int64_t rdx7;
    int64_t rax8;
    uint64_t tmp64_9;
    int1_t cf10;
    int64_t rax11;

    __asm__("cli ");
    r12_4 = rdx;
    rbp5 = rsi;
    rbx6 = *rsi;
    if (!rdi) {
        while (1) {
            if (!rbx6) {
                *reinterpret_cast<int32_t*>(&rdx7) = 0;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx7) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx7) = reinterpret_cast<uint1_t>(r12_4 > 0x80);
                rbx6 = 0x80 / r12_4 + rdx7;
            }
            rax8 = fun_23d0();
            if (rax8) 
                break;
            addr_7baa_5:
            xalloc_die();
        }
        *rbp5 = rbx6;
        return;
    } else {
        tmp64_9 = rbx6 + ((rbx6 >> 1) + 1);
        cf10 = tmp64_9 < rbx6;
        rbx6 = tmp64_9;
        if (cf10) 
            goto addr_7baa_5;
        rax11 = fun_23d0();
        if (rax11) 
            goto addr_7b92_9;
        if (!rbx6) 
            goto addr_7b92_9;
        if (r12_4) 
            goto addr_7baa_5;
        addr_7b92_9:
        *rbp5 = rbx6;
        return;
    }
}

void fun_7bf3(void** rdi, void*** rsi, void** rdx, void** rcx, uint64_t r8) {
    void** r13_6;
    void** rdi7;
    void*** r12_8;
    void** rsi9;
    void** rcx10;
    void** rbx11;
    void** rax12;
    void** rbp13;
    void* rbp14;
    void** rax15;

    __asm__("cli ");
    r13_6 = rdi;
    rdi7 = rdx;
    r12_8 = rsi;
    rsi9 = rcx;
    rcx10 = *r12_8;
    rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(reinterpret_cast<signed char>(rcx10) >> 1) + reinterpret_cast<unsigned char>(rcx10));
    if (__intrinsic()) {
        rbx11 = reinterpret_cast<void**>(0x7fffffffffffffff);
    }
    rax12 = rsi9;
    if (reinterpret_cast<signed char>(rbx11) <= reinterpret_cast<signed char>(rsi9)) {
        rax12 = rbx11;
    }
    if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) {
        rbx11 = rax12;
    }
    rbp13 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx11) * r8);
    if (__intrinsic()) {
        while (1) {
            rbp14 = reinterpret_cast<void*>(0x7fffffffffffffff);
            addr_7c9d_9:
            rdx = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) % reinterpret_cast<int64_t>(r8));
            rbx11 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) / reinterpret_cast<int64_t>(r8));
            rbp13 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp14) - reinterpret_cast<unsigned char>(rdx));
            if (!r13_6) {
                addr_7cb0_10:
                *r12_8 = reinterpret_cast<void**>(0);
            }
            addr_7c50_11:
            if (reinterpret_cast<signed char>(reinterpret_cast<unsigned char>(rbx11) - reinterpret_cast<unsigned char>(rcx10)) >= reinterpret_cast<signed char>(rdi7)) 
                goto addr_7c76_12;
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) + reinterpret_cast<unsigned char>(rdi7));
            rbx11 = rcx10;
            if (__intrinsic()) 
                goto addr_7cc4_14;
            if (reinterpret_cast<signed char>(rcx10) <= reinterpret_cast<signed char>(rsi9)) 
                goto addr_7c6d_16;
            if (reinterpret_cast<signed char>(rsi9) >= reinterpret_cast<signed char>(0)) 
                goto addr_7cc4_14;
            addr_7c6d_16:
            rcx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rcx10) * r8);
            rbp13 = rcx10;
            if (__intrinsic()) 
                goto addr_7cc4_14;
            addr_7c76_12:
            rsi9 = rbp13;
            rdi7 = r13_6;
            rax15 = fun_25c0(rdi7, rsi9, rdx);
            if (rax15) 
                break;
            if (!r13_6) 
                goto addr_7cc4_14;
            if (!rbp13) 
                break;
            addr_7cc4_14:
            xalloc_die();
        }
        *r12_8 = rbx11;
        return;
    } else {
        if (reinterpret_cast<signed char>(rbp13) <= reinterpret_cast<signed char>(0x7f)) {
            *reinterpret_cast<int32_t*>(&rbp14) = 0x80;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbp14) + 4) = 0;
            goto addr_7c9d_9;
        } else {
            if (!r13_6) 
                goto addr_7cb0_10;
            goto addr_7c50_11;
        }
    }
}

void fun_7cf3(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2520(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7d23(void** rdi) {
    void** rax2;

    __asm__("cli ");
    rax2 = fun_2520(rdi, 1);
    if (!rax2) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7d53(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2520(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7d73(void** rdi, void** rsi) {
    void** rax3;

    __asm__("cli ");
    rax3 = fun_2520(rdi, rsi);
    if (!rax3) {
        xalloc_die();
    } else {
        return;
    }
}

void fun_7d93(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2580(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2560;
    }
}

void fun_7dd3(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2580(rsi, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        goto fun_2560;
    }
}

void fun_7e13(int64_t rdi, void** rsi, void** rdx) {
    void** rax4;

    __asm__("cli ");
    rax4 = fun_2580(rsi + 1, rsi, rdx);
    if (!rax4) {
        xalloc_die();
    } else {
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax4) + reinterpret_cast<unsigned char>(rsi)) = 0;
        goto fun_2560;
    }
}

void fun_7e53(void** rdi, void** rsi, void** rdx) {
    void** rax4;
    void** rax5;

    __asm__("cli ");
    rax4 = fun_2450(rdi);
    rax5 = fun_2580(rax4 + 1, rsi, rdx);
    if (!rax5) {
        xalloc_die();
    } else {
        goto fun_2560;
    }
}

void fun_7e93() {
    void** rdi1;

    __asm__("cli ");
    fun_2430();
    *reinterpret_cast<int32_t*>(&rdi1) = exit_failure;
    *reinterpret_cast<int32_t*>(&rdi1 + 4) = 0;
    fun_2610();
    fun_2380(rdi1);
}

int64_t fun_23c0();

int64_t rpl_fclose(uint32_t* rdi);

int64_t fun_7ed3(uint32_t* rdi) {
    int64_t rax2;
    uint32_t ebx3;
    int64_t rax4;
    int32_t* rax5;
    int32_t* rax6;

    __asm__("cli ");
    rax2 = fun_23c0();
    ebx3 = *rdi & 32;
    rax4 = rpl_fclose(rdi);
    if (ebx3) {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            addr_7f2e_3:
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        } else {
            rax5 = fun_2390();
            *rax5 = 0;
            *reinterpret_cast<int32_t*>(&rax4) = -1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    } else {
        if (*reinterpret_cast<int32_t*>(&rax4)) {
            if (rax2) 
                goto addr_7f2e_3;
            rax6 = fun_2390();
            *reinterpret_cast<int32_t*>(&rax4) = reinterpret_cast<int32_t>(-static_cast<uint32_t>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*rax6 != 9))));
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        }
    }
    return rax4;
}

struct s47 {
    uint32_t f0;
    signed char[4] pad8;
    int64_t f8;
    int64_t f10;
    signed char[8] pad32;
    int64_t f20;
    int64_t f28;
    signed char[24] pad72;
    int64_t f48;
    signed char[64] pad144;
    int64_t f90;
};

int32_t fun_2570(struct s47* rdi);

int32_t fun_25b0(struct s47* rdi);

int64_t fun_24b0(int64_t rdi, ...);

int32_t rpl_fflush(struct s47* rdi);

int64_t fun_2410(struct s47* rdi);

int64_t fun_7f43(struct s47* rdi) {
    int32_t eax2;
    int32_t eax3;
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int32_t eax7;
    int32_t* rax8;
    int32_t r12d9;
    int64_t rax10;

    __asm__("cli ");
    eax2 = fun_2570(rdi);
    if (eax2 >= 0) {
        eax3 = fun_25b0(rdi);
        if (!(eax3 && (eax4 = fun_2570(rdi), *reinterpret_cast<int32_t*>(&rdi5) = eax4, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0, rax6 = fun_24b0(rdi5), rax6 == -1) || (eax7 = rpl_fflush(rdi), eax7 == 0))) {
            rax8 = fun_2390();
            r12d9 = *rax8;
            rax10 = fun_2410(rdi);
            if (r12d9) {
                *rax8 = r12d9;
                *reinterpret_cast<int32_t*>(&rax10) = -1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax10) + 4) = 0;
            }
            return rax10;
        }
    }
    goto fun_2410;
}

void rpl_fseeko(struct s47* rdi);

void fun_7fd3(struct s47* rdi) {
    int32_t eax2;

    __asm__("cli ");
    if (!(!rdi || ((eax2 = fun_25b0(rdi), !eax2) || !(rdi->f0 & 0x100)))) {
        rpl_fseeko(rdi);
    }
}

int64_t fun_8023(struct s47* rdi, int64_t rsi, int32_t edx) {
    int32_t eax4;
    int64_t rdi5;
    int64_t rax6;
    int64_t rax7;

    __asm__("cli ");
    if (!(rdi->f10 != rdi->f8 || (rdi->f28 != rdi->f20 || rdi->f48))) {
        eax4 = fun_2570(rdi);
        *reinterpret_cast<int32_t*>(&rdi5) = eax4;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi5) + 4) = 0;
        rax6 = fun_24b0(rdi5, rdi5);
        if (rax6 == -1) {
            *reinterpret_cast<uint32_t*>(&rax7) = 0xffffffff;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        } else {
            rdi->f0 = rdi->f0 & 0xffffffef;
            rdi->f90 = rax6;
            *reinterpret_cast<uint32_t*>(&rax7) = 0;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax7) + 4) = 0;
        }
        return rax7;
    }
}

uint64_t fun_80a3(signed char* rdi, int64_t rsi) {
    int64_t rdx3;
    int64_t rax4;

    __asm__("cli ");
    rdx3 = *rdi;
    if (!*reinterpret_cast<signed char*>(&rdx3)) {
        return 0;
    } else {
        *reinterpret_cast<int32_t*>(&rax4) = 0;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax4) + 4) = 0;
        do {
            __asm__("rol rax, 0x9");
            ++rdi;
            rax4 = rax4 + rdx3;
            rdx3 = *rdi;
        } while (*reinterpret_cast<signed char*>(&rdx3));
        return rax4 % reinterpret_cast<uint64_t>(rsi);
    }
}

signed char* fun_25a0(int64_t rdi);

signed char* fun_80e3() {
    signed char* rax1;

    __asm__("cli ");
    rax1 = fun_25a0(14);
    if (!rax1) {
        return "ASCII";
    } else {
        if (!*rax1) {
            rax1 = "ASCII";
        }
        return rax1;
    }
}

uint64_t fun_2480(uint32_t* rdi);

signed char hard_locale();

uint64_t fun_8123(uint32_t* rdi, unsigned char* rsi, int64_t rdx) {
    uint32_t* rbx4;
    void** rax5;
    uint64_t rax6;
    uint64_t r12_7;
    signed char al8;
    void* rax9;

    __asm__("cli ");
    rbx4 = rdi;
    rax5 = g28;
    if (!rdi) {
        rbx4 = reinterpret_cast<uint32_t*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8 - 8 - 8 - 8 - 24 + 4);
    }
    rax6 = fun_2480(rbx4);
    r12_7 = rax6;
    if (rax6 > 0xfffffffffffffffd && (rdx && (al8 = hard_locale(), !al8))) {
        *reinterpret_cast<int32_t*>(&r12_7) = 1;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r12_7) + 4) = 0;
        *rbx4 = *rsi;
    }
    rax9 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax5) - reinterpret_cast<unsigned char>(g28));
    if (rax9) {
        fun_2460();
    } else {
        return r12_7;
    }
}

int32_t setlocale_null_r();

int64_t fun_81b3() {
    void** rax1;
    int32_t eax2;
    int64_t rax3;
    int16_t v4;
    int16_t v5;
    int16_t v6;
    void* rdx7;

    __asm__("cli ");
    rax1 = g28;
    eax2 = setlocale_null_r();
    *reinterpret_cast<int32_t*>(&rax3) = 0;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
    if (!eax2 && v4 != 67) {
        if (v5 != 0x49534f50 || (*reinterpret_cast<int32_t*>(&rax3) = 0, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0, v6 != 88)) {
            *reinterpret_cast<int32_t*>(&rax3) = 1;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax3) + 4) = 0;
        }
    }
    rdx7 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rax1) - reinterpret_cast<unsigned char>(g28));
    if (rdx7) {
        fun_2460();
    } else {
        return rax3;
    }
}

int64_t fun_8233(int64_t rdi, void** rsi, void** rdx, int64_t rcx) {
    void** rax5;
    int32_t r13d6;
    void** rax7;
    int64_t rax8;

    __asm__("cli ");
    rax5 = fun_25d0(rdi);
    if (!rax5) {
        r13d6 = 22;
        if (rdx) {
            *reinterpret_cast<void***>(rsi) = reinterpret_cast<void**>(0);
        }
    } else {
        rax7 = fun_2450(rax5);
        if (reinterpret_cast<unsigned char>(rdx) > reinterpret_cast<unsigned char>(rax7)) {
            fun_2560(rsi, rax5, rax7 + 1, rcx);
            return 0;
        } else {
            r13d6 = 34;
            if (rdx) {
                fun_2560(rsi, rax5, rdx + 0xffffffffffffffff, rcx);
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rsi) + reinterpret_cast<unsigned char>(rdx) + 0xffffffffffffffff) = 0;
                return 34;
            }
        }
    }
    *reinterpret_cast<int32_t*>(&rax8) = r13d6;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax8) + 4) = 0;
    return rax8;
}

void fun_82e3() {
    __asm__("cli ");
    goto fun_25d0;
}

void fun_82f3() {
    __asm__("cli ");
}

void fun_8307() {
    __asm__("cli ");
    return;
}

void fun_27d0() {
    verbose = 0;
    goto 0x2790;
}

void fun_2868() {
    goto 0x2790;
}

uint32_t fun_24f0(void** rdi, void** rsi, void** rdx, void** rcx);

void** rpl_mbrtowc(void* rdi, void** rsi);

int32_t fun_2680(int64_t rdi, void** rsi);

uint32_t fun_2670(void** rdi, void** rsi);

void** fun_2690(void** rdi, void** rsi, void** rdx, void** rcx);

void fun_4f25() {
    void*** rsp1;
    int32_t ebp2;
    void** rax3;
    void*** rsp4;
    void** r11_5;
    void** r11_6;
    void** v7;
    int32_t ebp8;
    void** rax9;
    void** rdx10;
    void** rax11;
    void** r11_12;
    void** v13;
    int32_t ebp14;
    void** rax15;
    void** r15_16;
    int32_t ebx17;
    uint32_t eax18;
    void** r13_19;
    void* r14_20;
    signed char* r12_21;
    void** v22;
    int32_t ebx23;
    void** rax24;
    void*** rsp25;
    void** v26;
    void** r11_27;
    void** v28;
    void** v29;
    void** rsi30;
    void** v31;
    void** v32;
    void** r10_33;
    void** r13_34;
    signed char* r14_35;
    uint32_t ebp36;
    void** r9_37;
    void** v38;
    void** rdi39;
    void** v40;
    void** rbx41;
    uint32_t r8d42;
    int64_t rbx43;
    void** rcx44;
    unsigned char al45;
    void** v46;
    int64_t v47;
    void** v48;
    void** v49;
    void** rax50;
    uint32_t edx51;
    int64_t rdx52;
    uint32_t eax53;
    uint32_t eax54;
    uint32_t eax55;
    uint1_t zf56;
    unsigned char v57;
    void** v58;
    unsigned char v59;
    void** v60;
    void** v61;
    void** v62;
    signed char* v63;
    void** r12_64;
    unsigned char v65;
    void* rbx66;
    uint32_t v67;
    void* r14_68;
    void** r13_69;
    void** rsi70;
    void* v71;
    void** r15_72;
    void* v73;
    int64_t rax74;
    int64_t rdi75;
    int32_t v76;
    int32_t eax77;
    void* rdi78;
    unsigned char v79;
    void* rdi80;
    void* v81;
    uint32_t esi82;
    uint32_t ebp83;
    uint32_t eax84;
    uint32_t eax85;
    uint32_t eax86;
    uint32_t eax87;
    uint32_t eax88;
    uint32_t eax89;
    void* rdx90;
    void* rcx91;
    void* v92;
    void** rax93;
    uint1_t zf94;
    int32_t ecx95;
    uint32_t ecx96;
    uint32_t edi97;
    int32_t ecx98;
    uint32_t edi99;
    uint32_t edi100;
    int64_t rax101;
    uint32_t eax102;
    uint32_t r12d103;
    int64_t rax104;
    int64_t rax105;
    uint32_t r12d106;
    void** v107;
    void** rdx108;
    void* rax109;
    void* v110;
    uint64_t rax111;
    int64_t v112;
    int64_t rax113;
    int64_t rax114;
    int64_t rax115;
    int64_t v116;

    rsp1 = reinterpret_cast<void***>(__zero_stack_offset());
    if (ebp2 != 10) {
        rax3 = fun_2430();
        rsp4 = rsp1 - 8 + 8;
        r11_5 = r11_6;
        v7 = rax3;
        if (rax3 == "`") {
            rax9 = gettext_quote_part_0(rax3, ebp8, 5);
            rsp4 = rsp4 - 8 + 8;
            r11_5 = r11_6;
            v7 = rax9;
        }
        *reinterpret_cast<uint32_t*>(&rdx10) = 5;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        rax11 = fun_2430();
        rsp1 = rsp4 - 8 + 8;
        r11_12 = r11_5;
        v13 = rax11;
        if (rax11 == "'") {
            rax15 = gettext_quote_part_0(rax11, ebp14, 5);
            rsp1 = rsp1 - 8 + 8;
            r11_12 = r11_5;
            v13 = rax15;
        }
    }
    *reinterpret_cast<int32_t*>(&r15_16) = 0;
    *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
    if (!ebx17 && (rdx10 = v7, eax18 = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rdx10)), !!*reinterpret_cast<signed char*>(&eax18))) {
        do {
            if (reinterpret_cast<unsigned char>(r13_19) > reinterpret_cast<unsigned char>(r15_16)) {
                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r14_20) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<signed char*>(&eax18);
            }
            ++r15_16;
            eax18 = *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rdx10) + reinterpret_cast<unsigned char>(r15_16));
        } while (*reinterpret_cast<signed char*>(&eax18));
    }
    *reinterpret_cast<uint32_t*>(&r12_21) = 1;
    v22 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!ebx23)));
    rax24 = fun_2450(v13, v13);
    rsp25 = rsp1 - 8 + 8;
    v26 = v13;
    r11_27 = r11_12;
    v28 = rax24;
    v29 = reinterpret_cast<void**>(1);
    *reinterpret_cast<uint32_t*>(&rsi30) = 0;
    *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
    v31 = reinterpret_cast<void**>(0);
    while (1) {
        v32 = *reinterpret_cast<void***>(&r12_21);
        r10_33 = r13_34;
        r12_21 = r14_35;
        *reinterpret_cast<uint32_t*>(&r13_34) = *reinterpret_cast<uint32_t*>(&rsi30);
        *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r14_35) = ebp36;
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
        while (1) {
            *reinterpret_cast<int32_t*>(&r9_37) = 0;
            *reinterpret_cast<int32_t*>(&r9_37 + 4) = 0;
            while (1) {
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(r11_27 != r9_37);
                if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                    rax24 = v38;
                    *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!!*reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(rax24) + reinterpret_cast<unsigned char>(r9_37)));
                }
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) 
                    break;
                rdi39 = v40;
                rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) != 2)) & reinterpret_cast<unsigned char>(v32));
                rbx41 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rdi39) + reinterpret_cast<unsigned char>(r9_37));
                r8d42 = *reinterpret_cast<uint32_t*>(&rax24);
                if (!rax24) {
                    *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                        if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                            goto addr_5223_22;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                            goto addr_5223_22; else 
                            goto addr_561d_24;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7a)) 
                        goto addr_56dd_26;
                } else {
                    rax24 = v28;
                    if (!rax24) {
                        addr_5a30_28:
                        *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                        if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) {
                            if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                                goto addr_5220_30;
                            if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                                goto addr_5220_30; else 
                                goto addr_5a49_32;
                        }
                    } else {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<unsigned char>(rax24));
                        if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff) && reinterpret_cast<unsigned char>(rax24) > reinterpret_cast<unsigned char>(1)) {
                            rax24 = fun_2450(rdi39);
                            rsp25 = rsp25 - 8 + 8;
                            r10_33 = r10_33;
                            r9_37 = r9_37;
                            rdx10 = rdx10;
                            r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                            r11_27 = rax24;
                        }
                        if (reinterpret_cast<unsigned char>(rdx10) > reinterpret_cast<unsigned char>(r11_27)) 
                            goto addr_5a30_28;
                        rdx10 = v28;
                        rsi30 = v26;
                        rdi39 = rbx41;
                        *reinterpret_cast<uint32_t*>(&rax24) = fun_24f0(rdi39, rsi30, rdx10, rcx44);
                        rsp25 = rsp25 - 8 + 8;
                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                        r9_37 = r9_37;
                        r10_33 = r10_33;
                        r11_27 = r11_27;
                        if (*reinterpret_cast<uint32_t*>(&rax24)) 
                            goto addr_5a30_28; else 
                            goto addr_50cc_37;
                    }
                }
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                    addr_5b90_39:
                    *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                        addr_5a10_40:
                        if (r11_27 == 1) {
                            addr_559d_41:
                            *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                            if (r9_37) {
                                addr_5b58_42:
                                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                                ebp36 = 0;
                            } else {
                                *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<uint32_t*>(&rcx44);
                                goto addr_51d7_44;
                            }
                        } else {
                            goto addr_5a20_46;
                        }
                    } else {
                        addr_5b9f_47:
                        rax24 = v46;
                        if (!*reinterpret_cast<void***>(rax24 + 1)) {
                            goto addr_559d_41;
                        }
                    }
                } else {
                    if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(0x7d)) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7b) {
                            if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                                addr_5223_22:
                                if (v47 != 1) {
                                    addr_5779_52:
                                    v48 = reinterpret_cast<void**>(rsp25 + 0xb0);
                                    if (reinterpret_cast<int1_t>(r11_27 == 0xffffffffffffffff)) {
                                        rax50 = fun_2450(v49, v49);
                                        rsp25 = rsp25 - 8 + 8;
                                        r10_33 = r10_33;
                                        r9_37 = r9_37;
                                        r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                                        r11_27 = rax50;
                                        goto addr_57c4_54;
                                    }
                                } else {
                                    goto addr_5230_56;
                                }
                            } else {
                                addr_51d5_57:
                                ebp36 = 0;
                                goto addr_51d7_44;
                            }
                        } else {
                            addr_5a04_58:
                            if (r11_27 == 0xffffffffffffffff) 
                                goto addr_5b9f_47; else 
                                goto addr_5a0e_59;
                        }
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7e) 
                            goto addr_559d_41;
                        if (v47 == 1) 
                            goto addr_5230_56; else 
                            goto addr_5779_52;
                    }
                }
                addr_5291_62:
                *reinterpret_cast<uint32_t*>(&rdx10) = static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32)) ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                rax24 = reinterpret_cast<void**>(al45 | *reinterpret_cast<unsigned char*>(&rdx10));
                if (!rax24 || (*reinterpret_cast<uint32_t*>(&rax24) = 0, !!v22)) {
                    addr_5128_63:
                    if (!1 && (edx51 = *reinterpret_cast<uint32_t*>(&rcx44), *reinterpret_cast<uint32_t*>(&rdx52) = *reinterpret_cast<unsigned char*>(&edx51) >> 5, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx52) + 4) = 0, *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(rdx52 * 4) >> *reinterpret_cast<unsigned char*>(&rcx44) & 1, *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0, !!*reinterpret_cast<uint32_t*>(&rdx10)) || *reinterpret_cast<unsigned char*>(&r8d42)) {
                        addr_514d_64:
                        *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                        if (v22) 
                            goto addr_5450_65;
                    } else {
                        addr_52b9_66:
                        ++r9_37;
                        eax54 = (*reinterpret_cast<uint32_t*>(&rax24) ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        goto addr_5b08_67;
                    }
                } else {
                    goto addr_52b0_69;
                }
                addr_5161_70:
                eax55 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                *reinterpret_cast<unsigned char*>(&eax55) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax55) & *reinterpret_cast<unsigned char*>(&rdx10));
                if (*reinterpret_cast<unsigned char*>(&eax55)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    rdx10 = r15_16 + 2;
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx10)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax55;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                }
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                    *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                }
                ++r15_16;
                ++r9_37;
                addr_51ac_81:
                if (reinterpret_cast<unsigned char>(r15_16) < reinterpret_cast<unsigned char>(r10_33)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
                ++r15_16;
                *reinterpret_cast<uint32_t*>(&rsi30) = 0;
                *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
                if (!*reinterpret_cast<unsigned char*>(&ebp36)) {
                    *reinterpret_cast<uint32_t*>(&rax24) = 0;
                }
                v29 = rax24;
                continue;
                addr_5b08_67:
                if (*reinterpret_cast<signed char*>(&eax54)) {
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                    }
                    r15_16 = r15_16 + 2;
                    *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_51ac_81;
                }
                addr_52b0_69:
                if (*reinterpret_cast<unsigned char*>(&r8d42)) 
                    goto addr_514d_64; else 
                    goto addr_52b9_66;
                addr_51d7_44:
                zf56 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                al45 = zf56;
                if (!zf56) 
                    goto addr_528f_91;
                if (v22) 
                    goto addr_51ef_93;
                addr_528f_91:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5291_62;
                addr_57c4_54:
                v57 = *reinterpret_cast<unsigned char*>(&r8d42);
                v58 = r9_37;
                v59 = *reinterpret_cast<unsigned char*>(&r13_34);
                v60 = r15_16;
                v61 = r10_33;
                v62 = r11_27;
                v63 = r12_21;
                r12_64 = v48;
                v65 = *reinterpret_cast<unsigned char*>(&rbx43);
                rbx66 = reinterpret_cast<void*>(0);
                v67 = *reinterpret_cast<uint32_t*>(&r14_35);
                r14_68 = reinterpret_cast<void*>(rsp25 + 0xac);
                do {
                    rcx44 = r12_64;
                    r13_69 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(v58) + reinterpret_cast<uint64_t>(rbx66));
                    rsi70 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v71) + reinterpret_cast<unsigned char>(r13_69));
                    rax24 = rpl_mbrtowc(r14_68, rsi70);
                    rsp25 = rsp25 - 8 + 8;
                    r15_72 = rax24;
                    if (!rax24) 
                        break;
                    if (rax24 == 0xffffffffffffffff) 
                        goto addr_5f4b_96;
                    if (rax24 == 0xfffffffffffffffe) 
                        goto addr_5fbb_98;
                    if (v67 == 2 && (v22 && rax24 != 1)) {
                        rdx10 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r13_69) + 1);
                        rsi70 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(v73) + reinterpret_cast<unsigned char>(r15_72)) + reinterpret_cast<unsigned char>(r13_69));
                        do {
                            *reinterpret_cast<uint32_t*>(&rax74) = reinterpret_cast<uint32_t>(*reinterpret_cast<void***>(rdx10) - 91);
                            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax74) + 4) = 0;
                            if (*reinterpret_cast<unsigned char*>(&rax74) > 33) 
                                continue;
                            if (static_cast<int1_t>(0x20000002b >> rax74)) 
                                goto addr_5dbf_103;
                            ++rdx10;
                        } while (rsi70 != rdx10);
                    }
                    *reinterpret_cast<int32_t*>(&rdi75) = v76;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi75) + 4) = 0;
                    eax77 = fun_2680(rdi75, rsi70);
                    if (!eax77) {
                        ebp36 = 0;
                    }
                    rbx66 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rbx66) + reinterpret_cast<unsigned char>(r15_72));
                    *reinterpret_cast<uint32_t*>(&rax24) = fun_2670(r12_64, rsi70);
                    rsp25 = rsp25 - 8 + 8 - 8 + 8;
                } while (!*reinterpret_cast<uint32_t*>(&rax24));
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                *reinterpret_cast<uint32_t*>(&rdx10) = ebp36 ^ 1;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v32));
                addr_58be_109:
                if (reinterpret_cast<uint64_t>(rdi78) <= 1) {
                    addr_527c_110:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                        ebp36 = 0;
                        goto addr_58c8_112;
                    }
                } else {
                    addr_58c8_112:
                    v79 = *reinterpret_cast<unsigned char*>(&ebp36);
                    rdi80 = v81;
                    esi82 = 0;
                    ebp83 = reinterpret_cast<unsigned char>(v22);
                    rcx44 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rdi78) + reinterpret_cast<unsigned char>(r9_37));
                    goto addr_5999_114;
                }
                addr_5288_115:
                al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                goto addr_528f_91;
                while (1) {
                    addr_5999_114:
                    if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                        *reinterpret_cast<unsigned char*>(&esi82) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        eax84 = esi82;
                        if (*reinterpret_cast<signed char*>(&ebp83)) 
                            goto addr_5ea7_117;
                        eax85 = *reinterpret_cast<uint32_t*>(&r13_34) ^ 1;
                        *reinterpret_cast<unsigned char*>(&eax85) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax85) & *reinterpret_cast<unsigned char*>(&esi82));
                        if (*reinterpret_cast<unsigned char*>(&eax85)) 
                            goto addr_5906_119;
                    } else {
                        eax54 = (esi82 ^ 1) & *reinterpret_cast<uint32_t*>(&r13_34);
                        if (*reinterpret_cast<unsigned char*>(&r8d42)) {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                            }
                            ++r15_16;
                        }
                        ++r9_37;
                        if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                            goto addr_5eb5_125;
                        if (!*reinterpret_cast<signed char*>(&eax54)) {
                            r8d42 = 0;
                            goto addr_5987_128;
                        } else {
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                            }
                            if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                                *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 39;
                            }
                            r15_16 = r15_16 + 2;
                            r8d42 = 0;
                            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
                            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                            goto addr_5987_128;
                        }
                    }
                    addr_5935_134:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 92;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        eax86 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax86) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax86) >> 6);
                        eax87 = eax86 + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = *reinterpret_cast<signed char*>(&eax87);
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        eax88 = *reinterpret_cast<uint32_t*>(&rbx43);
                        *reinterpret_cast<unsigned char*>(&eax88) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax88) >> 3);
                        eax89 = (eax88 & 7) + 48;
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = *reinterpret_cast<signed char*>(&eax89);
                    }
                    ++r9_37;
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&rbx43) = (*reinterpret_cast<uint32_t*>(&rbx43) & 7) + 48;
                    if (reinterpret_cast<unsigned char>(r9_37) >= reinterpret_cast<unsigned char>(rcx44)) 
                        break;
                    esi82 = *reinterpret_cast<uint32_t*>(&rdx10);
                    addr_5987_128:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = *reinterpret_cast<unsigned char*>(&rbx43);
                    }
                    *reinterpret_cast<uint32_t*>(&rbx43) = *reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(rdi80) + reinterpret_cast<unsigned char>(r9_37));
                    ++r15_16;
                    continue;
                    addr_5906_119:
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) = 39;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 1)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 1) = 36;
                    }
                    if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16 + 2)) {
                        *reinterpret_cast<signed char*>(reinterpret_cast<uint64_t>(reinterpret_cast<int64_t>(r12_21) + reinterpret_cast<unsigned char>(r15_16)) + 2) = 39;
                    }
                    r15_16 = r15_16 + 3;
                    *reinterpret_cast<uint32_t*>(&r13_34) = eax85;
                    *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                    goto addr_5935_134;
                }
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_51ac_81;
                addr_5eb5_125:
                ebp36 = v79;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5b08_67;
                addr_5f4b_96:
                rdi78 = rbx66;
                r8d42 = v57;
                r9_37 = v58;
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                r11_27 = v62;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                goto addr_58be_109;
                addr_5fbb_98:
                r11_27 = v62;
                rdi78 = rbx66;
                rax24 = r13_69;
                r9_37 = v58;
                r8d42 = v57;
                *reinterpret_cast<uint32_t*>(&rbx43) = v65;
                rdx90 = rdi78;
                *reinterpret_cast<uint32_t*>(&r13_34) = v59;
                *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
                r15_16 = v60;
                r12_21 = v63;
                r10_33 = v61;
                *reinterpret_cast<uint32_t*>(&r14_35) = v67;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
                rcx91 = v92;
                if (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27)) {
                    do {
                        if (!*reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(rcx91) + reinterpret_cast<unsigned char>(rax24))) 
                            break;
                        rdx90 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rdx90) + 1);
                        rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r9_37) + reinterpret_cast<uint64_t>(rdx90));
                    } while (reinterpret_cast<unsigned char>(rax24) < reinterpret_cast<unsigned char>(r11_27));
                    rdi78 = rdx90;
                }
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                ebp36 = 0;
                goto addr_58be_109;
                addr_5230_56:
                rax93 = fun_2690(rdi39, rsi30, rdx10, rcx44);
                rsp25 = rsp25 - 8 + 8;
                r8d42 = *reinterpret_cast<unsigned char*>(&r8d42);
                r9_37 = r9_37;
                *reinterpret_cast<int32_t*>(&rdi78) = 1;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi78) + 4) = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = *reinterpret_cast<unsigned char*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rax24 + 4) = 0;
                r10_33 = r10_33;
                r11_27 = r11_27;
                zf94 = reinterpret_cast<uint1_t>((*reinterpret_cast<unsigned char*>(reinterpret_cast<int64_t>(*rax93) + reinterpret_cast<unsigned char>(rax24) * 2 + 1) & 64) == 0);
                *reinterpret_cast<unsigned char*>(&ebp36) = reinterpret_cast<uint1_t>(!zf94);
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(static_cast<unsigned char>(zf94) & reinterpret_cast<unsigned char>(v32));
                goto addr_527c_110;
                addr_5a0e_59:
                goto addr_5a10_40;
                addr_56dd_26:
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                    goto addr_5223_22;
                *reinterpret_cast<uint32_t*>(&rcx44) = static_cast<uint32_t>(rbx43 - 65);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&rcx44));
                if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                    goto addr_5288_115;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_51d5_57;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                    goto addr_5223_22;
                if (*reinterpret_cast<uint32_t*>(&r14_35) != 2) 
                    goto addr_5722_160;
                if (!v22) 
                    goto addr_5af7_162; else 
                    goto addr_5d03_163;
                addr_5722_160:
                *reinterpret_cast<uint32_t*>(&rdx10) = reinterpret_cast<unsigned char>(v32);
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<unsigned char>(reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&rdx10) & reinterpret_cast<unsigned char>(v22)) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(!!v28)));
                r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (*reinterpret_cast<unsigned char*>(&rdx10)) {
                    addr_5af7_162:
                    ++r9_37;
                    eax54 = *reinterpret_cast<uint32_t*>(&r13_34);
                    ebp36 = 0;
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    goto addr_5b08_67;
                } else {
                    *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                    *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                    if (!v32) 
                        goto addr_55cb_166;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                addr_5433_168:
                *reinterpret_cast<unsigned char*>(&rdx10) = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                eax53 = *reinterpret_cast<uint32_t*>(&rdx10);
                if (!v22) 
                    goto addr_5161_70; else 
                    goto addr_5447_169;
                addr_55cb_166:
                ebp36 = 0;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                if (v22) 
                    goto addr_5128_63;
                goto addr_52b0_69;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7d;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = 0;
                        goto addr_5a04_58;
                    }
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_5b3f_175;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_5220_30;
                    ecx95 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx95));
                    ecx96 = 0;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_5118_178; else 
                        goto addr_5ac2_179;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5a04_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_5223_22;
                }
                addr_5b3f_175:
                *reinterpret_cast<uint32_t*>(&rdx10) = 0;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    addr_5220_30:
                    r8d42 = 0;
                    goto addr_5223_22;
                } else {
                    if (!r9_37) {
                        ebp36 = r8d42;
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        r8d42 = *reinterpret_cast<uint32_t*>(&rdx10);
                        al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                        goto addr_5291_62;
                    } else {
                        *reinterpret_cast<uint32_t*>(&rcx44) = 0x7e;
                        *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                        goto addr_5b58_42;
                    }
                }
                addr_5118_178:
                ebp36 = r8d42;
                *reinterpret_cast<uint32_t*>(&rax24) = 0;
                r8d42 = ecx96;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                goto addr_5128_63;
                addr_5ac2_179:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                r8d42 = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) {
                    addr_5a20_46:
                    al45 = reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2);
                    ebp36 = 0;
                    goto addr_5291_62;
                } else {
                    addr_5ad2_186:
                    if (*reinterpret_cast<unsigned char*>(&rbx43) != 92) 
                        goto addr_5223_22;
                }
                edi97 = reinterpret_cast<unsigned char>(v22);
                if (!(reinterpret_cast<unsigned char>(v32) & *reinterpret_cast<unsigned char*>(&edi97))) 
                    goto addr_6282_188;
                if (v28) 
                    goto addr_5af7_162;
                addr_6282_188:
                *reinterpret_cast<uint32_t*>(&rcx44) = 92;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                ebp36 = 0;
                goto addr_5433_168;
                addr_50cc_37:
                if (v22) 
                    goto addr_60c3_190;
                *reinterpret_cast<uint32_t*>(&rbx43) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(rbx41));
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rbx43) + 4) = 0;
                if (*reinterpret_cast<signed char*>(&rbx43) <= reinterpret_cast<signed char>(63)) 
                    goto addr_50e3_192;
                if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7a)) {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7d) 
                        goto addr_5b90_39;
                    if (*reinterpret_cast<signed char*>(&rbx43) > reinterpret_cast<signed char>(0x7d)) 
                        goto addr_5c1b_196;
                } else {
                    if (*reinterpret_cast<unsigned char*>(&rbx43) == 64) 
                        goto addr_5223_22;
                    ecx98 = static_cast<int32_t>(rbx43 - 65);
                    rdx10 = reinterpret_cast<void**>(0x3ffffff53ffffff);
                    rax24 = reinterpret_cast<void**>(1 << *reinterpret_cast<unsigned char*>(&ecx98));
                    ecx96 = r8d42;
                    if (reinterpret_cast<unsigned char>(rax24) & reinterpret_cast<unsigned char>(0x3ffffff53ffffff)) 
                        goto addr_5118_178; else 
                        goto addr_5bf7_199;
                }
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7b;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) == 0x7b) 
                    goto addr_5a04_58;
                *reinterpret_cast<uint32_t*>(&rcx44) = 0x7c;
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7c) {
                    goto addr_5223_22;
                }
                addr_5c1b_196:
                *reinterpret_cast<uint32_t*>(&rdx10) = r8d42;
                *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
                if (*reinterpret_cast<unsigned char*>(&rbx43) != 0x7e) {
                    goto addr_5223_22;
                }
                addr_5bf7_199:
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<uint32_t*>(&rbx43);
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
                if (*reinterpret_cast<uint32_t*>(&rax24) & 0xa4000000) 
                    goto addr_5a20_46;
                goto addr_5ad2_186;
                addr_50e3_192:
                if (*reinterpret_cast<signed char*>(&rbx43) < reinterpret_cast<signed char>(0)) 
                    goto addr_5223_22;
                if (*reinterpret_cast<unsigned char*>(&rbx43) > 63) 
                    goto addr_5223_22; else 
                    goto addr_50f4_206;
            }
            edi99 = reinterpret_cast<unsigned char>(v22);
            rax24 = reinterpret_cast<void**>(static_cast<unsigned char>(reinterpret_cast<uint1_t>(*reinterpret_cast<uint32_t*>(&r14_35) == 2)));
            *reinterpret_cast<unsigned char*>(&rcx44) = reinterpret_cast<uint1_t>(r15_16 == 0);
            *reinterpret_cast<uint32_t*>(&rdx10) = edi99 & *reinterpret_cast<uint32_t*>(&rax24);
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            if (*reinterpret_cast<unsigned char*>(&rcx44) & *reinterpret_cast<unsigned char*>(&rdx10)) 
                goto addr_61ce_208;
            edi100 = edi99 ^ 1;
            *reinterpret_cast<uint32_t*>(&rdx10) = edi100;
            *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
            rax24 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rax24) & *reinterpret_cast<unsigned char*>(&edi100));
            if (!rax24) 
                goto addr_6054_210;
            if (1) 
                goto addr_6052_212;
            if (!v29) 
                goto addr_5c8e_214;
            *reinterpret_cast<int32_t*>(&r15_16) = 0;
            *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
            *reinterpret_cast<uint32_t*>(&r14_35) = 5;
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r14_35) + 4) = 0;
            rax101 = fun_2440();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v28 = reinterpret_cast<void**>(1);
            v47 = rax101;
            v26 = reinterpret_cast<void**>("\"");
            if (!0) 
                goto addr_61c1_216;
            *reinterpret_cast<uint32_t*>(&rax24) = reinterpret_cast<unsigned char>(v29);
            r10_33 = reinterpret_cast<void**>(0);
            *reinterpret_cast<uint32_t*>(&r13_34) = 0;
            *reinterpret_cast<int32_t*>(&r13_34 + 4) = 0;
            v31 = reinterpret_cast<void**>(0);
            v22 = rax24;
            v32 = rax24;
        }
        addr_5450_65:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax102 = eax53 & static_cast<uint32_t>(reinterpret_cast<unsigned char>(v32));
        if (!*reinterpret_cast<signed char*>(&eax102)) 
            goto addr_520b_219; else 
            goto addr_546a_220;
        addr_51ef_93:
        r14_35 = r12_21;
        r13_34 = r10_33;
        eax84 = reinterpret_cast<unsigned char>(v32);
        addr_5203_221:
        if (*reinterpret_cast<signed char*>(&eax84)) 
            goto addr_546a_220; else 
            goto addr_520b_219;
        addr_5dbf_103:
        r12d103 = reinterpret_cast<unsigned char>(v32);
        r14_35 = v63;
        r13_34 = v61;
        r11_27 = v62;
        if (*reinterpret_cast<signed char*>(&r12d103)) {
            addr_546a_220:
            *reinterpret_cast<uint32_t*>(&r12_21) = 1;
            rax104 = fun_2440();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax104;
        } else {
            addr_5ddd_222:
            *reinterpret_cast<uint32_t*>(&r12_21) = 0;
            rax105 = fun_2440();
            rsp25 = rsp25 - 8 + 8;
            r11_27 = r11_27;
            v47 = rax105;
        }
        rax24 = reinterpret_cast<void**>("'");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 2;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("'");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        if (!r13_34) {
            v31 = reinterpret_cast<void**>(0);
            continue;
        }
        addr_6250_225:
        v31 = r13_34;
        *reinterpret_cast<uint32_t*>(&rdx10) = 0;
        *reinterpret_cast<int32_t*>(&rdx10 + 4) = 0;
        addr_5cb6_226:
        r13_34 = v31;
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        rax24 = reinterpret_cast<void**>("'");
        *r14_35 = 39;
        ebp36 = 2;
        v31 = reinterpret_cast<void**>(0);
        v22 = reinterpret_cast<void**>(0);
        v28 = reinterpret_cast<void**>(1);
        v26 = reinterpret_cast<void**>("'");
        continue;
        addr_5ea7_117:
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_5203_221;
        addr_5d03_163:
        eax84 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        r14_35 = r12_21;
        goto addr_5203_221;
        addr_5447_169:
        goto addr_5450_65;
        addr_61ce_208:
        r14_35 = r12_21;
        r12d106 = reinterpret_cast<unsigned char>(v32);
        r13_34 = r10_33;
        if (*reinterpret_cast<signed char*>(&r12d106)) 
            goto addr_546a_220;
        goto addr_5ddd_222;
        addr_6054_210:
        if (v26 && (*reinterpret_cast<unsigned char*>(&rdx10) && (*reinterpret_cast<uint32_t*>(&rcx44) = reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(v26)), *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0, !!*reinterpret_cast<unsigned char*>(&rcx44)))) {
            rsi30 = v107;
            rdx108 = r15_16;
            rax109 = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(v26) - reinterpret_cast<unsigned char>(r15_16));
            do {
                if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(rdx108)) {
                    *reinterpret_cast<unsigned char*>(reinterpret_cast<unsigned char>(rsi30) + reinterpret_cast<unsigned char>(rdx108)) = *reinterpret_cast<unsigned char*>(&rcx44);
                }
                ++rdx108;
                *reinterpret_cast<uint32_t*>(&rcx44) = *reinterpret_cast<unsigned char*>(reinterpret_cast<uint64_t>(rax109) + reinterpret_cast<unsigned char>(rdx108));
                *reinterpret_cast<int32_t*>(&rcx44 + 4) = 0;
            } while (*reinterpret_cast<unsigned char*>(&rcx44));
            r15_16 = rdx108;
        }
        if (reinterpret_cast<unsigned char>(r10_33) > reinterpret_cast<unsigned char>(r15_16)) {
            *reinterpret_cast<signed char*>(reinterpret_cast<int64_t>(v110) + reinterpret_cast<unsigned char>(r15_16)) = 0;
        }
        rax111 = reinterpret_cast<uint64_t>(v112 - reinterpret_cast<unsigned char>(g28));
        if (!rax111) 
            goto addr_60ae_236;
        fun_2460();
        rsp25 = rsp25 - 8 + 8;
        goto addr_6250_225;
        addr_6052_212:
        *reinterpret_cast<uint32_t*>(&rdx10) = *reinterpret_cast<uint32_t*>(&rax24);
        goto addr_6054_210;
        addr_5c8e_214:
        r14_35 = r12_21;
        *reinterpret_cast<uint32_t*>(&rsi30) = *reinterpret_cast<uint32_t*>(&r13_34);
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = reinterpret_cast<unsigned char>(v32);
        if (1) {
            *reinterpret_cast<uint32_t*>(&rdx10) = 0;
            goto addr_6054_210;
        } else {
            rdx10 = reinterpret_cast<void**>(0);
            goto addr_5cb6_226;
        }
        addr_61c1_216:
        r13_34 = reinterpret_cast<void**>(0);
        r14_35 = r12_21;
        rax24 = reinterpret_cast<void**>("\"");
        v29 = reinterpret_cast<void**>(1);
        ebp36 = 5;
        *reinterpret_cast<uint32_t*>(&rsi30) = 0;
        *reinterpret_cast<int32_t*>(&rsi30 + 4) = 0;
        v26 = reinterpret_cast<void**>("\"");
        *reinterpret_cast<int32_t*>(&r15_16) = 1;
        *reinterpret_cast<int32_t*>(&r15_16 + 4) = 0;
        *reinterpret_cast<uint32_t*>(&r12_21) = 1;
        v28 = reinterpret_cast<void**>(1);
        v22 = reinterpret_cast<void**>(0);
        v31 = reinterpret_cast<void**>(0);
        if (1) 
            continue;
        *r14_35 = 34;
    }
    addr_561d_24:
    *reinterpret_cast<uint32_t*>(&rax113) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax113) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x99ec + rax113 * 4) + 0x99ec;
    addr_5a49_32:
    *reinterpret_cast<uint32_t*>(&rax114) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax114) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x9aec + rax114 * 4) + 0x9aec;
    addr_60c3_190:
    addr_520b_219:
    goto 0x4ef0;
    addr_50f4_206:
    *reinterpret_cast<uint32_t*>(&rax115) = *reinterpret_cast<unsigned char*>(&rbx43);
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax115) + 4) = 0;
    goto *reinterpret_cast<int32_t*>(0x98ec + rax115 * 4) + 0x98ec;
    addr_60ae_236:
    goto v116;
}

void fun_5110() {
}

void fun_52c8() {
    int32_t ebx1;

    if (!ebx1) 
        goto "???";
    goto 0x4fc2;
}

void fun_5321() {
    goto 0x4fc2;
}

void fun_540e() {
    int32_t r14d1;
    signed char v2;
    int64_t r10_3;
    int64_t v4;
    uint64_t r10_5;
    uint64_t r15_6;
    int64_t r12_7;
    int64_t r15_8;
    uint64_t r10_9;
    int64_t r15_10;
    int64_t r12_11;
    int64_t r15_12;
    uint64_t r10_13;
    int64_t r15_14;
    int64_t r12_15;
    int64_t r15_16;

    if (r14d1 != 2) {
        goto 0x5291;
    }
    if (v2) 
        goto 0x5d03;
    if (!r10_3) 
        goto addr_5e6e_5;
    if (!v4) 
        goto addr_5d3e_7;
    addr_5e6e_5:
    if (r10_5 > r15_6) {
        *reinterpret_cast<signed char*>(r12_7 + r15_8) = 39;
    }
    if (r10_9 > reinterpret_cast<uint64_t>(r15_10 + 1)) {
        *reinterpret_cast<signed char*>(r12_11 + r15_12 + 1) = 92;
    }
    if (r10_13 > reinterpret_cast<uint64_t>(r15_14 + 2)) {
        *reinterpret_cast<signed char*>(r12_15 + r15_16 + 2) = 39;
    }
    addr_5d3e_7:
    goto 0x5144;
}

void fun_542c() {
}

void fun_54d7() {
    signed char v1;

    if (v1) {
        goto 0x545f;
    } else {
        goto 0x519a;
    }
}

void fun_54f1() {
    signed char v1;

    if (!v1) 
        goto 0x54ea; else 
        goto "???";
}

void fun_5518() {
    goto 0x5433;
}

void fun_5598() {
}

void fun_55b0() {
}

void fun_55df() {
    goto 0x5433;
}

void fun_5631() {
    goto 0x55c0;
}

void fun_5660() {
    goto 0x55c0;
}

void fun_5693() {
    goto 0x55c0;
}

void fun_5a60() {
    goto 0x5118;
}

void fun_5d5e() {
    signed char v1;

    if (v1) 
        goto 0x5d03;
    goto 0x5144;
}

void fun_5e05() {
    uint64_t r10_1;
    uint64_t r15_2;
    int64_t r12_3;
    int64_t r15_4;
    uint64_t r15_5;
    int32_t r14d6;
    int64_t r9_7;
    uint64_t r11_8;
    uint32_t eax9;
    int64_t v10;
    int64_t r9_11;
    uint32_t eax12;
    uint64_t r10_13;
    int64_t r12_14;
    uint64_t r10_15;
    int64_t r12_16;
    uint32_t eax17;
    unsigned char v18;
    unsigned char sil19;

    if (r10_1 > r15_2) {
        *reinterpret_cast<signed char*>(r12_3 + r15_4) = 92;
    }
    r15_5 = reinterpret_cast<uint64_t>(r15_4 + 1);
    if (r14d6 == 2) {
        goto 0x5144;
    } else {
        if (reinterpret_cast<uint64_t>(r9_7 + 1) < r11_8 && (eax9 = *reinterpret_cast<unsigned char*>(v10 + r9_11 + 1), eax12 = eax9 - 48, *reinterpret_cast<unsigned char*>(&eax12) <= 9)) {
            if (r10_13 > r15_5) {
                *reinterpret_cast<signed char*>(r12_14 + r15_5) = 48;
            }
            if (r10_15 > reinterpret_cast<uint64_t>(r15_4 + 2)) {
                *reinterpret_cast<signed char*>(r12_16 + r15_4 + 2) = 48;
            }
        }
        eax17 = static_cast<uint32_t>(v18) ^ 1;
        if (!(*reinterpret_cast<unsigned char*>(&eax17) | sil19)) 
            goto 0x5128;
        goto 0x5144;
    }
}

void fun_6222() {
    int32_t ebx1;

    if (!ebx1) {
        goto 0x5490;
    } else {
        goto 0x4fc2;
    }
}

void fun_72f8() {
    fun_2430();
}

void fun_27e0() {
    no_newline = 1;
    goto 0x2790;
}

void fun_534e() {
    goto 0x4fc2;
}

void fun_5524() {
    goto 0x54dc;
}

void fun_55eb() {
    goto 0x5118;
}

void fun_563d() {
    int32_t r14d1;
    unsigned char v2;

    if (!(static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d1 == 2)) & v2)) 
        goto 0x55c0;
    goto 0x51ef;
}

void fun_566f() {
    signed char v1;
    unsigned char v2;
    signed char v3;
    int32_t r14d4;
    uint32_t eax5;
    uint32_t r13d6;
    int32_t r14d7;
    uint64_t r10_8;
    uint64_t r15_9;
    uint64_t r10_10;
    int64_t r15_11;
    int64_t r12_12;
    int64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;

    if (!v1) {
        if (!(v2 & 1)) 
            goto 0x55cb;
        goto 0x4ff0;
    }
    if (v3) {
        if (r14d4 == 2) 
            goto 0x546a;
        goto 0x520b;
    }
    eax5 = r13d6 ^ 1;
    *reinterpret_cast<unsigned char*>(&eax5) = reinterpret_cast<unsigned char>(*reinterpret_cast<unsigned char*>(&eax5) & static_cast<unsigned char>(reinterpret_cast<uint1_t>(r14d7 == 2)));
    if (!*reinterpret_cast<unsigned char*>(&eax5)) 
        goto 0x5e08;
    if (r10_8 > r15_9) 
        goto addr_5555_9;
    addr_555a_10:
    if (r10_10 > reinterpret_cast<uint64_t>(r15_11 + 1)) {
        *reinterpret_cast<signed char*>(r12_12 + r15_13 + 1) = 36;
    }
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 2)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 2) = 39;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 3)) 
        goto 0x5e13;
    goto 0x5144;
    addr_5555_9:
    *reinterpret_cast<signed char*>(r12_20 + r15_21) = 39;
    goto addr_555a_10;
}

void fun_56a2() {
    goto 0x51d7;
}

void fun_5a70() {
    goto 0x51d7;
}

void fun_620f() {
    int32_t ebx1;

    if (ebx1) {
        goto 0x532c;
    } else {
        goto 0x5490;
    }
}

void fun_73b0() {
}

void fun_27f0() {
    goto 0x2790;
}

void fun_56ac() {
    goto 0x5647;
}

void fun_5a7a() {
    goto 0x559d;
}

void fun_7410() {
    fun_2430();
    goto fun_2660;
}

void fun_27f8() {
    goto 0x2790;
}

void fun_537d() {
    goto 0x4fc2;
}

void fun_56b8() {
    goto 0x5647;
}

void fun_5a87() {
    goto 0x55ee;
}

void fun_7450() {
    fun_2430();
    goto fun_2660;
}

void fun_2800() {
    verbose = 1;
    goto 0x2790;
}

void fun_53aa() {
    goto 0x4fc2;
}

void fun_56c4() {
    goto 0x55c0;
}

void fun_7490() {
    fun_2430();
    goto fun_2660;
}

void fun_53cc() {
    int32_t r14d1;
    int32_t r14d2;
    unsigned char v3;
    uint64_t rdx4;
    int64_t r9_5;
    uint64_t r11_6;
    int64_t v7;
    int64_t r9_8;
    uint32_t ecx9;
    uint64_t rax10;
    signed char v11;
    uint64_t r10_12;
    uint64_t r15_13;
    uint64_t r10_14;
    int64_t r15_15;
    int64_t r12_16;
    int64_t r15_17;
    uint64_t r10_18;
    int64_t r15_19;
    int64_t r12_20;
    int64_t r15_21;
    uint64_t r10_22;
    int64_t r15_23;
    int64_t r12_24;
    int64_t r15_25;
    int64_t r12_26;
    int64_t r15_27;

    if (r14d1 == 2) 
        goto 0x5d60;
    if (r14d2 != 5 || (!(v3 & 4) || ((rdx4 = reinterpret_cast<uint64_t>(r9_5 + 2), rdx4 >= r11_6) || (*reinterpret_cast<signed char*>(v7 + r9_8 + 1) != 63 || (ecx9 = *reinterpret_cast<unsigned char*>(v7 + rdx4), *reinterpret_cast<unsigned char*>(&ecx9) > 62))))) {
        goto 0x5291;
    }
    rax10 = 0x7000a38200000000 >> *reinterpret_cast<unsigned char*>(&ecx9);
    if (!(*reinterpret_cast<uint32_t*>(&rax10) & 1)) {
        goto 0x5291;
    }
    if (v11) 
        goto 0x60c3;
    if (r10_12 > r15_13) 
        goto addr_6113_8;
    addr_6118_9:
    if (r10_14 > reinterpret_cast<uint64_t>(r15_15 + 1)) {
        *reinterpret_cast<signed char*>(r12_16 + r15_17 + 1) = 34;
    }
    if (r10_18 > reinterpret_cast<uint64_t>(r15_19 + 2)) {
        *reinterpret_cast<signed char*>(r12_20 + r15_21 + 2) = 34;
    }
    if (r10_22 > reinterpret_cast<uint64_t>(r15_23 + 3)) {
        *reinterpret_cast<signed char*>(r12_24 + r15_25 + 3) = 63;
    }
    goto 0x5e51;
    addr_6113_8:
    *reinterpret_cast<signed char*>(r12_26 + r15_27) = 63;
    goto addr_6118_9;
}

struct s48 {
    signed char[24] pad24;
    int64_t f18;
};

struct s49 {
    signed char[16] pad16;
    void** f10;
};

struct s50 {
    signed char[8] pad8;
    void** f8;
};

void fun_74e0() {
    int64_t r15_1;
    struct s48* rbx2;
    void** r14_3;
    struct s49* rbx4;
    void** r13_5;
    struct s50* rbx6;
    void** r12_7;
    void*** rbx8;
    void** rax9;
    void** rbp10;
    int64_t v11;
    int64_t v12;
    int64_t v13;
    int64_t v14;

    r15_1 = rbx2->f18;
    r14_3 = rbx4->f10;
    r13_5 = rbx6->f8;
    r12_7 = *rbx8;
    rax9 = fun_2430();
    fun_2660(rbp10, 1, rax9, r12_7, r13_5, r14_3, r15_1, 0x7502, __return_address(), v11, v12, v13);
    goto v14;
}

void fun_7538() {
    fun_2430();
    goto 0x7509;
}

struct s51 {
    signed char[32] pad32;
    int64_t f20;
};

struct s52 {
    signed char[24] pad24;
    int64_t f18;
};

struct s53 {
    signed char[16] pad16;
    void** f10;
};

struct s54 {
    signed char[8] pad8;
    void** f8;
};

struct s55 {
    signed char[40] pad40;
    int64_t f28;
};

void fun_7570() {
    int64_t rcx1;
    struct s51* rbx2;
    int64_t r15_3;
    struct s52* rbx4;
    void** r14_5;
    struct s53* rbx6;
    void** r13_7;
    struct s54* rbx8;
    void** r12_9;
    void*** rbx10;
    int64_t v11;
    struct s55* rbx12;
    void** rax13;
    void** rbp14;
    int64_t v15;

    rcx1 = rbx2->f20;
    r15_3 = rbx4->f18;
    r14_5 = rbx6->f10;
    r13_7 = rbx8->f8;
    r12_9 = *rbx10;
    v11 = rbx12->f28;
    rax13 = fun_2430();
    fun_2660(rbp14, 1, rax13, r12_9, r13_7, r14_5, r15_3, rcx1, v11, 0x75a4, __return_address(), rcx1);
    goto v15;
}

void fun_75e8() {
    fun_2430();
    goto 0x75ab;
}
