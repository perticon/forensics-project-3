bool make_dir_parents_private(char * const_dir, uint64_t src_offset, int32_t dst_dirfd, char * verbose_fmt_string, int32_t ** attr_list, bool * new_dst, int32_t * x) {
    // 0x4720
    int32_t v1; // 0x4720
    int32_t v2 = v1;
    int64_t v3 = __readfsqword(40); // 0x4756
    uint64_t v4 = dir_len(); // 0x4765
    *(int64_t *)attr_list = 0;
    int64_t v5 = 1; // 0x477e
    int64_t v6; // 0x4720
    int64_t v7; // 0x4720
    int64_t v8; // 0x4720
    int64_t v9; // 0x4720
    int64_t v10; // 0x4810
    int64_t v11; // 0x4720
    char * v12; // 0x4720
    int32_t v13; // 0x4720
    char * v14; // 0x4916
    int64_t v15; // 0x4928
    if (v4 > src_offset) {
        // 0x47b0
        int64_t v16; // bp-424, 0x4720
        int64_t v17 = &v16; // 0x4736
        int64_t v18 = function_3820(); // 0x47b3
        int64_t v19 = v18 + 24; // 0x47bf
        int64_t v20 = v19 & -0x1000; // 0x47c6
        int64_t v21 = v17; // 0x47d6
        if (v20 != 0) {
            int64_t v22 = v17 - (int64_t)"_ctype_get_mb_cur_max"; // 0x47d8
            int64_t v23 = v22; // 0x47eb
            v21 = v22;
            while (v22 != v17 - v20) {
                // 0x47d8
                v22 = v23 - (int64_t)"_ctype_get_mb_cur_max";
                v23 = v22;
                v21 = v22;
            }
        }
        int64_t v24 = v21 - (v19 & (int64_t)(int32_t)"__ctype_get_mb_cur_max"); // 0x47f3
        v10 = function_3a80();
        int64_t v25 = v4 + 24; // 0x481f
        int64_t v26 = v25 & -0x1000; // 0x4826
        int64_t v27 = v24; // 0x4836
        if (v26 != 0) {
            int64_t v28 = v24 - (int64_t)"_ctype_get_mb_cur_max"; // 0x4838
            int64_t v29 = v28; // 0x484b
            v27 = v28;
            while (v28 != v24 - v26) {
                // 0x4838
                v28 = v29 - (int64_t)"_ctype_get_mb_cur_max";
                v29 = v28;
                v27 = v28;
            }
        }
        int64_t v30 = 15 - (v25 & (int64_t)(int32_t)"__ctype_get_mb_cur_max") + v27 & -16; // 0x4870
        function_3a80();
        int64_t v31 = v30 + src_offset; // 0x487f
        *(char *)(v30 + v4) = 0;
        if (*(char *)v31 == 47) {
            int64_t v32 = v31 + 1; // 0x4890
            int64_t v33 = v32; // 0x4897
            while (*(char *)v32 == 47) {
                // 0x4890
                v32 = v33 + 1;
                v33 = v32;
            }
        }
        // 0x4899
        if ((int32_t)function_3cb0() == 0) {
            if ((v2 & 0xf000) != 0x4000) {
              lab_0x4d3c:
                // 0x4d3c
                quotearg_style();
                function_3800();
                function_3b80();
                v5 = 0;
            } else {
                // 0x4d2a
                *(char *)new_dst = 0;
                v5 = 1;
            }
        } else {
            int64_t v34 = v10 + src_offset; // 0x48bc
            if (*(char *)v34 == 47) {
                int64_t v35 = v34 + 1; // 0x48e0
                int64_t v36 = v35; // 0x48e8
                while (*(char *)v35 == 47) {
                    // 0x48e0
                    v35 = v36 + 1;
                    v36 = v35;
                }
            }
            // 0x48ea
            int128_t v37; // bp-216, 0x4720
            v16 = &v37;
            int64_t v38 = function_3870(); // 0x4905
            v5 = 1;
            if (v38 != 0) {
                // 0x4916
                v11 = (int64_t)x;
                v12 = (char *)(v11 + 30);
                v13 = v2 ^ 63;
                v6 = v38;
                v8 = v18 + 1;
                while (true) {
                  lab_0x4916:
                    // 0x4916
                    v9 = v8;
                    v7 = v6;
                    v14 = (char *)v7;
                    *v14 = 0;
                    v15 = function_3cb0();
                    if ((int32_t)v15 != 0) {
                        goto lab_0x49f0;
                    } else {
                        // 0x4938
                        if (*(int32_t *)(v11 + 28) >= 256) {
                            goto lab_0x49f0;
                        } else {
                            goto lab_0x4947;
                        }
                    }
                }
              lab_0x4c4c_2:
                // 0x4c4c
                quotearg_style();
                function_3800();
                function_3b80();
                v5 = 0;
            }
        }
    }
    goto lab_0x4785_4;
  lab_0x4785_4:
    // 0x4785
    if (v3 != __readfsqword(40)) {
        // 0x4e17
        return function_3840() % 2 != 0;
    }
    // 0x4798
    return v5 != 0;
  lab_0x49f0:;
    // 0x49f0
    int64_t v39; // 0x4720
    if ((int32_t)function_3a70() == 0) {
        // 0x4c30
        v39 = v9;
        if ((v1 & 0xf000) != 0x4000) {
            // break -> 0x4c4c
            goto lab_0x4c4c_2;
        }
    } else {
        uint32_t v40 = *(int32_t *)function_36d0(); // 0x4a10
        v39 = v40;
        if (v40 != 0) {
            // break -> 0x4c4c
            goto lab_0x4c4c_2;
        }
    }
    int64_t v41 = xmalloc(); // 0x4a21
    int128_t v42 = __asm_movdqa(0); // 0x4a26
    int128_t v43; // 0x4720
    int128_t v44 = __asm_movdqa(v43); // 0x4a2e
    int128_t v45 = __asm_movdqa(v43); // 0x4a36
    int128_t v46 = __asm_movdqa(v43); // 0x4a3e
    int64_t v47 = v41; // 0x4a46
    __asm_movups(*(int128_t *)v41, v42);
    int128_t v48 = __asm_movdqa(v43); // 0x4a4c
    int128_t v49 = __asm_movdqa(v43); // 0x4a51
    int128_t v50 = __asm_movdqa(v43); // 0x4a56
    int128_t v51 = __asm_movdqa(v43); // 0x4a5b
    __asm_movups(*(int128_t *)(v41 + 16), v44);
    int128_t v52 = __asm_movdqa(v43); // 0x4a64
    __asm_movups(*(int128_t *)(v41 + 32), v45);
    __asm_movups(*(int128_t *)(v41 + 48), v46);
    __asm_movups(*(int128_t *)(v41 + 64), v52);
    __asm_movups(*(int128_t *)(v41 + 80), v48);
    __asm_movups(*(int128_t *)(v41 + 96), v49);
    __asm_movups(*(int128_t *)(v41 + 112), v50);
    __asm_movups(*(int128_t *)(v41 + 128), v51);
    *(int64_t *)(v41 + 152) = v7 - v10;
    *(char *)(v41 + 144) = 0;
    *(int64_t *)(v41 + 160) = v39;
    *(int64_t *)attr_list = v41;
    int32_t v53; // 0x4720
    int64_t v54; // 0x4720
    int64_t v55; // 0x4adb
    if ((int32_t)v15 == 0) {
        goto lab_0x4947;
    } else {
        // 0x4ac1
        v5 = 0;
        if ((char)set_process_security_ctx() == 0) {
            goto lab_0x4785_4;
        }
        // 0x4aef
        v55 = v47;
        *(char *)new_dst = 1;
        uint64_t v56 = (int64_t)*(int32_t *)(v55 + 24); // 0x4b06
        int64_t v57; // 0x4720
        if (*(char *)(v11 + 29) == 0) {
            // 0x4ca0
            v57 = *v12 == 0 ? 0 : v56 & 18;
        } else {
            // 0x4b10
            v57 = v56 % 64;
        }
        // 0x4b1c
        if ((int32_t)function_3a50() != 0) {
            // 0x4dd3
            quotearg_style();
            goto lab_0x4da0;
        }
        int64_t v58 = v57;
        if (verbose_fmt_string != NULL) {
            // 0x4b5f
            function_3b40();
        }
        // 0x4b87
        if ((int32_t)function_3cb0() != 0) {
            // 0x4d80
            quotearg_style();
            goto lab_0x4da0;
        }
        // 0x4bb4
        if (*v12 != 0) {
            goto lab_0x4bee;
        } else {
            // 0x4bc2
            v54 = v58;
            if ((v13 & (int32_t)v58) != 0) {
                int64_t v59 = (cached_umask() ^ 0xffffffff) & v58; // 0x4cfa
                int32_t v60 = v59;
                v54 = v59;
                v53 = v60;
                if ((v13 & v60) != 0) {
                    goto lab_0x4bdf;
                } else {
                    goto lab_0x4bcf;
                }
            } else {
                goto lab_0x4bcf;
            }
        }
    }
  lab_0x4947:
    // 0x4947
    v5 = 0;
    if ((char)set_process_security_ctx() == 0) {
        goto lab_0x4785_4;
    }
    if ((v2 & 0xf000) != 0x4000) {
        goto lab_0x4d3c;
    }
    // 0x497f
    *(char *)new_dst = 0;
    goto lab_0x4989;
  lab_0x4989:
    // 0x4989
    if (*(int64_t *)(v11 + 40) == 0) {
        // 0x4cc0
        if (*(char *)(v11 + 51) == 0) {
            goto lab_0x49c0;
        } else {
            goto lab_0x4995;
        }
    } else {
        goto lab_0x4995;
    }
  lab_0x4bee:
    if ((v2 || 448) != v2) {
        // 0x4bfa
        if ((int32_t)function_3ad0() != 0) {
            // 0x4df5
            quotearg_style();
            goto lab_0x4da0;
        }
    }
    // 0x4c12
    if (*(char *)&v47 != 0) {
        goto lab_0x49c0;
    } else {
        goto lab_0x4989;
    }
  lab_0x4995:
    // 0x4995
    if (!set_file_security_ctx((char *)v10, false, x)) {
        // 0x49aa
        v5 = 0;
        if (*(char *)(v11 + 52) != 0) {
            goto lab_0x4785_4;
        }
    }
    goto lab_0x49c0;
  lab_0x49c0:
    // 0x49c0
    *v14 = 47;
    if (*(char *)(v7 + 1) == 47) {
        int64_t v61; // 0x4720
        int64_t v62 = v61 + 1; // 0x49d8
        v61 = v62;
        while (*(char *)v62 == 47) {
            int64_t v63 = v61;
            v62 = v63 + 1;
            char v64 = *(char *)v62; // 0x49dc
            v61 = v62;
        }
    }
    int64_t v65 = function_3870(); // 0x4905
    v5 = 1;
    v6 = v65;
    v8 = v11;
    if (v65 == 0) {
        goto lab_0x4785_4;
    }
    goto lab_0x4916;
  lab_0x4bcf:
    if ((v2 & 448) == 448) {
        goto lab_0x4bee;
    } else {
        // 0x4bcf
        v53 = v54;
        goto lab_0x4bdf;
    }
  lab_0x4bdf:
    // 0x4bdf
    *(char *)(v55 + 144) = 1;
    *(int32_t *)(v47 + 24) = v53 | v2;
    goto lab_0x4bee;
  lab_0x4da0:
    // 0x4da0
    function_3800();
    function_36d0();
    function_3b80();
    v5 = 0;
    goto lab_0x4785_4;
}