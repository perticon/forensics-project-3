signed char make_dir_parents_private(void** rdi, void** rsi, void** edx, void** rcx, void** r8, void** r9, void** a7) {
    void* rsp8;
    void* rbp9;
    int64_t r15_10;
    void** r13_11;
    void** rbx12;
    void** v13;
    void** r12_14;
    void** v15;
    void** v16;
    void* rax17;
    void* v18;
    void** rax19;
    void* rsp20;
    void** r14_21;
    void** rax22;
    void* rsp23;
    void*** rax24;
    void* rcx25;
    uint64_t rdx26;
    void* rdx27;
    void* rsp28;
    int64_t* rsp29;
    void** rax30;
    struct s8* rsp31;
    void** v32;
    void*** rax33;
    struct s8* rdx34;
    uint64_t rcx35;
    void* rcx36;
    void* rsp37;
    void** rax38;
    int64_t* rsp39;
    void* rsp40;
    void** rax41;
    void** v42;
    int64_t* rsp43;
    void** eax44;
    int32_t eax45;
    uint32_t v46;
    void** rsi47;
    void** rsp48;
    void** rax49;
    void** rsp50;
    void** rsp51;
    void* rdx52;
    void** rsp53;
    void** rsp54;
    void** rbp55;
    int64_t* rsp56;
    void** r15_57;
    void*** rsp58;
    void** r14d59;
    void*** rsp60;
    void** r13_61;
    void*** rsp62;
    void*** rsp63;
    void** rbx64;
    void* rax65;
    int64_t* rsp66;
    void** rax67;
    struct s9* rsp68;
    void** r8_69;
    void*** rax70;
    struct s9* rcx71;
    uint64_t rdx72;
    void* rdx73;
    void* rsp74;
    int64_t* rsp75;
    void** rax76;
    void* rsp77;
    void** r12_78;
    void** r13_79;
    int1_t zf80;
    void** rax81;
    int64_t rdi82;
    uint64_t rdx83;
    int64_t rax84;
    void*** rdx85;
    int64_t* rsp86;
    int64_t rax87;
    void** rcx88;
    int64_t rdx89;
    int64_t rdi90;
    int64_t* rsp91;
    int32_t eax92;
    uint32_t r8d93;
    void** r8_94;
    int64_t* rsp95;
    int32_t eax96;
    int64_t rdi97;
    int64_t* rsp98;
    int32_t eax99;
    int64_t* rsp100;
    void** eax101;
    void* rsp102;
    void** rcx103;
    int64_t rdi104;
    int64_t* rsp105;
    void* rax106;
    int64_t* rsp107;
    int64_t* rsp108;
    int64_t* rsp109;
    int64_t* rsp110;
    int64_t* rsp111;
    void* rsp112;
    int64_t* rsp113;
    int64_t* rsp114;
    int64_t* rsp115;
    int64_t* rsp116;
    void** rbx117;
    int1_t zf118;
    void** v119;
    void** v120;
    void** rsp121;
    void** rax122;
    void** rdi123;
    void** rsp124;
    void** eax125;
    void* rsp126;
    void** rsp127;
    int32_t eax128;
    void* rsp129;
    uint32_t v130;
    void** rsp131;
    void** rax132;
    void** rsp133;
    void** rax134;
    void** rax135;
    void** rsp136;
    void** eax137;
    void** rsp138;
    void** eax139;
    int64_t rdi140;
    void** rsp141;
    int32_t eax142;
    void* rsp143;
    void** rsp144;
    void** rdi145;
    void** rsp146;
    void** eax147;
    uint32_t eax148;
    uint32_t v149;
    void** rsp150;
    int32_t eax151;
    uint32_t v152;
    int64_t rdi153;
    void** rsp154;
    int32_t eax155;
    int1_t zf156;
    void** rsp157;
    void** eax158;
    uint32_t v159;
    void** rsp160;
    void** rax161;
    void** rsp162;
    void** rsp163;
    void** rsp164;
    void** rax165;
    void* rsp166;
    void** rsp167;
    void** rax168;
    void** rsp169;
    void** rax170;
    void** rsp171;
    void** rsp172;
    void** rax173;
    void** rsp174;
    void** rax175;

    rsp8 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(__zero_stack_offset()) - 8);
    rbp9 = rsp8;
    *reinterpret_cast<void***>(&r15_10) = edx;
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&r15_10) + 4) = 0;
    r13_11 = rdi;
    rbx12 = rsi;
    v13 = rcx;
    r12_14 = a7;
    v15 = r8;
    v16 = r9;
    rax17 = g28;
    v18 = rax17;
    rax19 = dir_len();
    rsp20 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(rsp8) - 8 - 8 - 8 - 8 - 8 - 0x178 - 8 + 8);
    r14_21 = rax19;
    *reinterpret_cast<void***>(v15) = reinterpret_cast<void**>(0);
    if (reinterpret_cast<unsigned char>(r14_21) > reinterpret_cast<unsigned char>(rbx12)) {
        rax22 = fun_3820(r13_11);
        rsp23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp20) - 8 + 8);
        r8 = rax22 + 1;
        rax24 = reinterpret_cast<void***>(rax22 + 24);
        rcx25 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp23) - (reinterpret_cast<uint64_t>(rax24) & 0xfffffffffffff000));
        rdx26 = reinterpret_cast<uint64_t>(rax24) & 0xfffffffffffffff0;
        if (rsp23 != rcx25) {
            do {
                rsp23 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp23) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
            } while (rsp23 != rcx25);
        }
        *reinterpret_cast<uint32_t*>(&rdx27) = *reinterpret_cast<uint32_t*>(&rdx26) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx27) + 4) = 0;
        rsp28 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp23) - reinterpret_cast<int64_t>(rdx27));
        if (rdx27) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp28) + reinterpret_cast<int64_t>(rdx27) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp28) + reinterpret_cast<int64_t>(rdx27) - 8);
        }
        rsp29 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp28) - 8);
        *rsp29 = 0x4815;
        rax30 = fun_3a80(reinterpret_cast<uint64_t>(rsp28) + 15 & 0xfffffffffffffff0, r13_11, r8);
        rsp31 = reinterpret_cast<struct s8*>(rsp29 + 1);
        v32 = rax30;
        rax33 = reinterpret_cast<void***>(r14_21 + 24);
        rdx34 = reinterpret_cast<struct s8*>(reinterpret_cast<uint64_t>(rsp31) - (reinterpret_cast<uint64_t>(rax33) & 0xfffffffffffff000));
        rcx35 = reinterpret_cast<uint64_t>(rax33) & 0xfffffffffffffff0;
        if (rsp31 != rdx34) {
            do {
                rsp31 = reinterpret_cast<struct s8*>(reinterpret_cast<uint64_t>(rsp31) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
                *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp31) + reinterpret_cast<int64_t>("strcmp")) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp31) + reinterpret_cast<int64_t>("strcmp"));
            } while (rsp31 != rdx34);
        }
        *reinterpret_cast<uint32_t*>(&rcx36) = *reinterpret_cast<uint32_t*>(&rcx35) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rcx36) + 4) = 0;
        rsp37 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp31) - reinterpret_cast<int64_t>(rcx36));
        if (rcx36) {
            *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp37) + reinterpret_cast<int64_t>(rcx36) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp37) + reinterpret_cast<int64_t>(rcx36) - 8);
        }
        rax38 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp37) + 15 & 0xfffffffffffffff0);
        r13_11 = rax38;
        rsp39 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp37) - 8);
        *rsp39 = 0x487f;
        fun_3a80(rax38, v32, r14_21);
        rsp40 = reinterpret_cast<void*>(rsp39 + 1);
        rsi = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_11) + reinterpret_cast<unsigned char>(rbx12));
        *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r13_11) + reinterpret_cast<unsigned char>(r14_21)) = 0;
        if (reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rsi) == 47)) {
            do {
                ++rsi;
            } while (*reinterpret_cast<void***>(rsi) == 47);
        }
        rax41 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp9) + 0xfffffffffffffea0);
        *reinterpret_cast<int32_t*>(&rcx) = 0;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rdi = *reinterpret_cast<void***>(&r15_10);
        *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
        v42 = rax41;
        rsp43 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp40) - 8);
        *rsp43 = 0x48b4;
        eax44 = fun_3cb0(rdi, rsi, rax41);
        rsp20 = reinterpret_cast<void*>(rsp43 + 1);
        if (eax44) 
            goto addr_48bc_14;
    } else {
        addr_4780_15:
        eax45 = 1;
        goto addr_4785_16;
    }
    if ((v46 & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) {
        rsi47 = r13_11;
    } else {
        *reinterpret_cast<void***>(v16) = reinterpret_cast<void**>(0);
        goto addr_4780_15;
    }
    addr_4d3c_20:
    rsp48 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
    *rsp48 = reinterpret_cast<void*>(0x4d46);
    rax49 = quotearg_style(4, rsi47, 4, rsi47);
    r12_14 = rax49;
    rsp50 = rsp48 + 1 - 1;
    *rsp50 = reinterpret_cast<void*>(0x4d5c);
    fun_3800();
    rcx = r12_14;
    rsi = reinterpret_cast<void**>(0);
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsp51 = rsp50 + 1 - 1;
    *rsp51 = reinterpret_cast<void*>(0x4d6d);
    fun_3b80();
    rsp20 = reinterpret_cast<void*>(rsp51 + 1);
    addr_4d6d_21:
    eax45 = 0;
    addr_4785_16:
    rdx52 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(v18) - reinterpret_cast<int64_t>(g28));
    if (!rdx52) {
        return *reinterpret_cast<signed char*>(&eax45);
    }
    rsp53 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
    *rsp53 = reinterpret_cast<void*>(0x4e1c);
    fun_3840();
    rsp54 = rsp53 + 1 - 1;
    *rsp54 = rbp9;
    rbp55 = rsp54;
    rsp56 = reinterpret_cast<int64_t*>(rsp54 - 1);
    *rsp56 = r15_10;
    r15_57 = r8;
    rsp58 = reinterpret_cast<void***>(rsp56 - 1);
    *rsp58 = r14_21;
    r14d59 = rsi;
    rsp60 = rsp58 - 8;
    *rsp60 = r13_11;
    r13_61 = rdi;
    rsp62 = rsp60 - 8;
    *rsp62 = r12_14;
    rsp63 = rsp62 - 8;
    *rsp63 = rbx12;
    rbx64 = rcx;
    *reinterpret_cast<void**>(rbp55 - 13) = rdx52;
    rax65 = g28;
    *(rbp55 - 7) = rax65;
    rsp66 = reinterpret_cast<int64_t*>(rsp63 - 72 - 8);
    *rsp66 = 0x4e55;
    rax67 = fun_3820(rdi);
    rsp68 = reinterpret_cast<struct s9*>(rsp66 + 1);
    r8_69 = rax67 + 1;
    rax70 = reinterpret_cast<void***>(rax67 + 24);
    rcx71 = reinterpret_cast<struct s9*>(reinterpret_cast<uint64_t>(rsp68) - (reinterpret_cast<uint64_t>(rax70) & 0xfffffffffffff000));
    rdx72 = reinterpret_cast<uint64_t>(rax70) & 0xfffffffffffffff0;
    if (rsp68 != rcx71) 
        goto addr_4e75_25;
    addr_4e8a_26:
    *reinterpret_cast<uint32_t*>(&rdx73) = *reinterpret_cast<uint32_t*>(&rdx72) & reinterpret_cast<uint32_t>("__ctype_get_mb_cur_max");
    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx73) + 4) = 0;
    rsp74 = reinterpret_cast<void*>(reinterpret_cast<uint64_t>(rsp68) - reinterpret_cast<int64_t>(rdx73));
    if (rdx73) {
        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp74) + reinterpret_cast<int64_t>(rdx73) - 8) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp74) + reinterpret_cast<int64_t>(rdx73) - 8);
    }
    rsp75 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp74) - 8);
    *rsp75 = 0x4eb0;
    rax76 = fun_3a80(reinterpret_cast<uint64_t>(rsp74) + 15 & 0xfffffffffffffff0, r13_61, r8_69);
    rsp77 = reinterpret_cast<void*>(rsp75 + 1);
    r12_78 = rax76;
    r13_79 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r12_78) + reinterpret_cast<uint64_t>(reinterpret_cast<signed char>(*reinterpret_cast<void**>(rbp55 - 13)) - reinterpret_cast<unsigned char>(r13_61)));
    if (!rbx64) {
        addr_50a8_29:
    } else {
        *reinterpret_cast<void****>(rbp55 - 14) = reinterpret_cast<void***>(rbp55 - 12);
        while ((zf80 = *reinterpret_cast<signed char*>(r15_57 + 31) == 0, *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_78) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbx64 + 0x98))) = 0, zf80) || (rax81 = *reinterpret_cast<void***>(rbx64 + 80), *reinterpret_cast<void***>(&rdi82) = r14d59, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi82) + 4) = 0, *reinterpret_cast<void***>(rbp55 - 12) = *reinterpret_cast<void***>(rbx64 + 72), rdx83 = *reinterpret_cast<uint64_t*>(rbx64 + 88), *reinterpret_cast<void***>(rbp55 - 11) = rax81, rax84 = *reinterpret_cast<int64_t*>(rbx64 + 96), *reinterpret_cast<uint64_t*>(rbp55 - 10) = rdx83, rdx85 = *reinterpret_cast<void****>(rbp55 - 14), *reinterpret_cast<int64_t*>(rbp55 - 9) = rax84, rsp86 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8), *rsp86 = 0x4f5e, rax87 = fun_36b0(rdi82, r13_79, rdx85, rdi82, r13_79, rdx85), rsp77 = reinterpret_cast<void*>(rsp86 + 1), *reinterpret_cast<int32_t*>(&rax87) == 0)) {
            if (!*reinterpret_cast<unsigned char*>(r15_57 + 29) || (rcx88 = *reinterpret_cast<void***>(rbx64 + 32), *reinterpret_cast<int32_t*>(&rcx88 + 4) = 0, *reinterpret_cast<unsigned char*>(&rdx89) = *reinterpret_cast<unsigned char*>(rbx64 + 28), *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdx89) + 4) = 0, *reinterpret_cast<void***>(&rdi90) = r14d59, *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi90) + 4) = 0, rsp91 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8), *rsp91 = 0x4fc7, eax92 = fun_3bf0(rdi90, r13_79, rdx89, rcx88, 0x100), rsp77 = reinterpret_cast<void*>(rsp91 + 1), eax92 == 0)) {
                r8d93 = *reinterpret_cast<unsigned char*>(r15_57 + 30);
                if (*reinterpret_cast<void**>(&r8d93)) {
                    addr_5010_34:
                    r8_94 = *reinterpret_cast<void***>(rbx64 + 24);
                    *reinterpret_cast<int32_t*>(&r8_94 + 4) = 0;
                    rsp95 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
                    *rsp95 = 0x5029;
                    eax96 = copy_acl(r13_79, 0xffffffff, r12_78, 0xffffffff, r8_94, r9);
                    rsp77 = reinterpret_cast<void*>(rsp95 + 1);
                    if (eax96) 
                        goto addr_5031_35;
                } else {
                    addr_4ef1_36:
                    if (!*reinterpret_cast<void***>(rbx64 + 0x90)) 
                        goto addr_4efe_37;
                    *reinterpret_cast<void***>(&rdi97) = r14d59;
                    *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi97) + 4) = 0;
                    *reinterpret_cast<void**>(rbp55 - 13) = *reinterpret_cast<void**>(&r8d93);
                    rsp98 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
                    *rsp98 = 0x5057;
                    eax99 = fun_3ad0(rdi97, r13_79, rdi97, r13_79);
                    rsp77 = reinterpret_cast<void*>(rsp98 + 1);
                    if (eax99) 
                        goto addr_505f_39;
                }
                addr_4efe_37:
                *reinterpret_cast<signed char*>(reinterpret_cast<unsigned char>(r12_78) + reinterpret_cast<uint64_t>(*reinterpret_cast<void**>(rbx64 + 0x98))) = 47;
                rbx64 = *reinterpret_cast<void***>(rbx64 + 0xa0);
                if (!rbx64) 
                    goto addr_50a8_29;
            } else {
                rsp100 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
                *rsp100 = 0x4fd7;
                eax101 = chown_failure_ok(r15_57, r13_79, rdx89, rcx88, 0x100);
                rsp102 = reinterpret_cast<void*>(rsp100 + 1);
                if (!*reinterpret_cast<void**>(&eax101)) 
                    goto addr_50da_41;
                rcx103 = *reinterpret_cast<void***>(rbx64 + 32);
                *reinterpret_cast<int32_t*>(&rcx103 + 4) = 0;
                *reinterpret_cast<void***>(&rdi104) = r14d59;
                *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi104) + 4) = 0;
                rsp105 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp102) - 8);
                *rsp105 = 0x4ff8;
                fun_3bf0(rdi104, r13_79, 0xffffffff, rcx103, 0x100);
                rsp77 = reinterpret_cast<void*>(rsp105 + 1);
                r8d93 = *reinterpret_cast<unsigned char*>(r15_57 + 30);
                if (!*reinterpret_cast<void**>(&r8d93)) 
                    goto addr_4ef1_36;
                goto addr_5010_34;
            }
        }
        goto addr_4f66_44;
    }
    addr_50ae_45:
    rax106 = reinterpret_cast<void*>(reinterpret_cast<int64_t>(*(rbp55 - 7)) - reinterpret_cast<int64_t>(g28));
    if (rax106) {
        *reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8) = reinterpret_cast<int64_t>(usage);
        fun_3840();
    } else {
        goto *reinterpret_cast<int64_t*>(rbp55 - 5 + 1 + 1 + 1 + 1 + 1 + 1);
    }
    addr_4f66_44:
    rsp107 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
    *rsp107 = 0x4f73;
    quotearg_style(4, r12_78, 4, r12_78);
    rsp108 = rsp107 + 1 - 1;
    *rsp108 = 0x4f89;
    fun_3800();
    rsp109 = rsp108 + 1 - 1;
    *rsp109 = 0x4f91;
    fun_36d0();
    rsp110 = rsp109 + 1 - 1;
    *rsp110 = 0x4fa2;
    fun_3b80();
    rsp77 = reinterpret_cast<void*>(rsp110 + 1);
    goto addr_50ae_45;
    addr_5031_35:
    goto addr_50ae_45;
    addr_505f_39:
    rsp111 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp77) - 8);
    *rsp111 = 0x506c;
    quotearg_style(4, r12_78, 4, r12_78);
    rsp112 = reinterpret_cast<void*>(rsp111 + 1);
    addr_507b_49:
    rsp113 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp112) - 8);
    *rsp113 = 0x5082;
    fun_3800();
    rsp114 = rsp113 + 1 - 1;
    *rsp114 = 0x508a;
    fun_36d0();
    rsp115 = rsp114 + 1 - 1;
    *rsp115 = 0x509b;
    fun_3b80();
    rsp77 = reinterpret_cast<void*>(rsp115 + 1);
    goto addr_50ae_45;
    addr_50da_41:
    *reinterpret_cast<void**>(rbp55 - 13) = *reinterpret_cast<void**>(&eax101);
    rsp116 = reinterpret_cast<int64_t*>(reinterpret_cast<uint64_t>(rsp102) - 8);
    *rsp116 = 0x50ea;
    quotearg_style(4, r12_78, 4, r12_78);
    rsp112 = reinterpret_cast<void*>(rsp116 + 1);
    goto addr_507b_49;
    do {
        addr_4e75_25:
        rsp68 = reinterpret_cast<struct s9*>(reinterpret_cast<uint64_t>(rsp68) - reinterpret_cast<int64_t>("_ctype_get_mb_cur_max"));
        *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp68) + reinterpret_cast<int64_t>("strcmp")) = *reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(rsp68) + reinterpret_cast<int64_t>("strcmp"));
    } while (rsp68 != rcx71);
    goto addr_4e8a_26;
    addr_48bc_14:
    rbx117 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(rbx12) + reinterpret_cast<unsigned char>(v32));
    zf118 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx117) == 47);
    v119 = rbx117;
    r14_21 = rbx117;
    if (!zf118) {
        r14_21 = v119;
    } else {
        do {
            ++r14_21;
        } while (*reinterpret_cast<void***>(r14_21) == 47);
    }
    rdi = r14_21;
    v120 = reinterpret_cast<void**>(reinterpret_cast<int64_t>(rbp9) + 0xffffffffffffff30);
    while (rsi = reinterpret_cast<void**>(47), rsp121 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8), *rsp121 = reinterpret_cast<void*>(0x490a), rax122 = fun_3870(rdi), rsp20 = reinterpret_cast<void*>(rsp121 + 1), rbx12 = rax122, !!rax122) {
        *reinterpret_cast<void***>(rbx12) = reinterpret_cast<void**>(0);
        rdi123 = *reinterpret_cast<void***>(&r15_10);
        *reinterpret_cast<int32_t*>(&rdi123 + 4) = 0;
        rsp124 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
        *rsp124 = reinterpret_cast<void*>(0x492d);
        eax125 = fun_3cb0(rdi123, r14_21, v42);
        rsp126 = reinterpret_cast<void*>(rsp124 + 1);
        r13_11 = eax125;
        *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
        if (eax125 || *reinterpret_cast<unsigned char*>(r12_14 + 28) & 0xffffff00) {
            rsp127 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp126) - 8);
            *rsp127 = reinterpret_cast<void*>(0x4a03);
            eax128 = fun_3a70(v119, v120, v119, v120);
            rsp129 = reinterpret_cast<void*>(rsp127 + 1);
            if (!eax128) {
                if ((v130 & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) 
                    goto addr_4c46_58;
            } else {
                rsp131 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp129) - 8);
                *rsp131 = reinterpret_cast<void*>(0x4a10);
                rax132 = fun_36d0();
                rsp129 = reinterpret_cast<void*>(rsp131 + 1);
                r8 = *reinterpret_cast<void***>(rax132);
                *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
                if (r8) 
                    goto addr_4c4c_60;
            }
            rsp133 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp129) - 8);
            *rsp133 = reinterpret_cast<void*>(0x4a26);
            rax134 = xmalloc(0xa8, v120, v42);
            rsp126 = reinterpret_cast<void*>(rsp133 + 1);
            __asm__("movdqa xmm0, [rbp-0xd0]");
            __asm__("movdqa xmm1, [rbp-0xc0]");
            __asm__("movdqa xmm2, [rbp-0xb0]");
            __asm__("movdqa xmm3, [rbp-0xa0]");
            r9 = rax134;
            __asm__("movups [rax], xmm0");
            __asm__("movdqa xmm5, [rbp-0x80]");
            __asm__("movdqa xmm6, [rbp-0x70]");
            __asm__("movdqa xmm7, [rbp-0x60]");
            __asm__("movdqa xmm0, [rbp-0x50]");
            __asm__("movups [rax+0x10], xmm1");
            __asm__("movdqa xmm4, [rbp-0x90]");
            __asm__("movups [rax+0x20], xmm2");
            __asm__("movups [rax+0x30], xmm3");
            __asm__("movups [rax+0x40], xmm4");
            __asm__("movups [rax+0x50], xmm5");
            __asm__("movups [rax+0x60], xmm6");
            __asm__("movups [rax+0x70], xmm7");
            __asm__("movups [rax+0x80], xmm0");
            *reinterpret_cast<void**>(r9 + 0x98) = reinterpret_cast<void*>(reinterpret_cast<unsigned char>(rbx12) - reinterpret_cast<unsigned char>(v32));
            rax135 = *reinterpret_cast<void***>(v15);
            *reinterpret_cast<void***>(r9 + 0x90) = reinterpret_cast<void**>(0);
            *reinterpret_cast<void***>(r9 + 0xa0) = rax135;
            *reinterpret_cast<void***>(v15) = r9;
            if (!r13_11) 
                goto addr_4947_62;
        } else {
            addr_4947_62:
            rsi = v32;
            *reinterpret_cast<int32_t*>(&rcx) = 0;
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            r8 = r12_14;
            rdi = v119;
            rsp136 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp126) - 8);
            *rsp136 = reinterpret_cast<void*>(0x4961);
            eax137 = set_process_security_ctx(rdi, rsi);
            rsp20 = reinterpret_cast<void*>(rsp136 + 1);
            if (!*reinterpret_cast<signed char*>(&eax137)) 
                goto addr_4d6d_21; else 
                goto addr_4969_63;
        }
        rsi = v32;
        r8 = r12_14;
        *reinterpret_cast<int32_t*>(&rcx) = 1;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rdi = v119;
        rsp138 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp126) - 8);
        *rsp138 = reinterpret_cast<void*>(0x4ae7);
        eax139 = set_process_security_ctx(rdi, rsi, rdi, rsi);
        rsp20 = reinterpret_cast<void*>(rsp138 + 1);
        if (!*reinterpret_cast<signed char*>(&eax139)) 
            goto addr_4d6d_21;
        *reinterpret_cast<void***>(v16) = reinterpret_cast<void**>(1);
        if (*reinterpret_cast<unsigned char*>(r12_14 + 29)) 
            goto addr_4b10_66;
        if (!*reinterpret_cast<unsigned char*>(r12_14 + 30)) {
            r13_11 = reinterpret_cast<void**>(0);
            *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
        } else {
            r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 24)) & 18);
            *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
        }
        addr_4b1c_70:
        *reinterpret_cast<void***>(&rdi140) = *reinterpret_cast<void***>(&r15_10);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi140) + 4) = 0;
        if (*reinterpret_cast<void***>(r12_14 + 32)) {
        }
        rsp141 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
        *rsp141 = reinterpret_cast<void*>(0x4b44);
        eax142 = fun_3a50(rdi140, r14_21);
        rsp143 = reinterpret_cast<void*>(rsp141 + 1);
        r9 = r9;
        if (eax142) 
            goto addr_4dd3_73;
        if (v13) {
            rsp144 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp143) - 8);
            *rsp144 = reinterpret_cast<void*>(0x4b80);
            fun_3b40(1, v13, v119, v32);
            rsp143 = reinterpret_cast<void*>(rsp144 + 1);
            r9 = r9;
        }
        *reinterpret_cast<int32_t*>(&rcx) = 0x100;
        *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
        rdi145 = *reinterpret_cast<void***>(&r15_10);
        *reinterpret_cast<int32_t*>(&rdi145 + 4) = 0;
        rsp146 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp143) - 8);
        *rsp146 = reinterpret_cast<void*>(0x4ba5);
        eax147 = fun_3cb0(rdi145, r14_21, v42);
        rsp20 = reinterpret_cast<void*>(rsp146 + 1);
        r9 = r9;
        if (eax147) 
            goto addr_4d80_77;
        eax148 = v149;
        if (*reinterpret_cast<unsigned char*>(r12_14 + 30)) 
            goto addr_4bee_79;
        if (~eax148 & reinterpret_cast<unsigned char>(r13_11)) {
            rsp150 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
            *rsp150 = reinterpret_cast<void*>(0x4cf1);
            eax151 = cached_umask(rdi145, r14_21);
            rsp20 = reinterpret_cast<void*>(rsp150 + 1);
            r9 = r9;
            r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_11) & reinterpret_cast<uint32_t>(~eax151));
            *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
            eax148 = v152;
            if (~eax148 & reinterpret_cast<unsigned char>(r13_11)) 
                goto addr_4bdf_82;
        }
        if ((eax148 & 0x1c0) == 0x1c0) {
            addr_4bee_79:
            if ((eax148 | 0x1c0) == eax148) 
                goto addr_4c12_85;
            *reinterpret_cast<int32_t*>(&rcx) = 0x100;
            *reinterpret_cast<int32_t*>(&rcx + 4) = 0;
            *reinterpret_cast<void***>(&rdi153) = *reinterpret_cast<void***>(&r15_10);
            *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rdi153) + 4) = 0;
            rsp154 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
            *rsp154 = reinterpret_cast<void*>(0x4c0a);
            eax155 = fun_3ad0(rdi153, r14_21);
            rsp20 = reinterpret_cast<void*>(rsp154 + 1);
            if (eax155) 
                goto addr_4df5_87;
        } else {
            addr_4bdf_82:
            r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(r13_11) | eax148);
            *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
            *reinterpret_cast<void***>(r9 + 0x90) = reinterpret_cast<void**>(1);
            *reinterpret_cast<void***>(r9 + 24) = r13_11;
            goto addr_4bee_79;
        }
        addr_4c12_85:
        if (*reinterpret_cast<void***>(v16)) 
            goto addr_49c0_88;
        addr_4989_90:
        if (!*reinterpret_cast<void***>(r12_14 + 40)) {
            if (!*reinterpret_cast<signed char*>(r12_14 + 51)) {
                addr_49c0_88:
                zf156 = reinterpret_cast<int1_t>(*reinterpret_cast<void***>(rbx12 + 1) == 47);
                *reinterpret_cast<void***>(rbx12) = reinterpret_cast<void**>(47);
                rdi = rbx12 + 1;
                if (!zf156) 
                    continue;
            } else {
                goto addr_4995_93;
            }
        } else {
            addr_4995_93:
            rdi = v32;
            rsi = reinterpret_cast<void**>(0);
            rsp157 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
            *rsp157 = reinterpret_cast<void*>(0x49a6);
            eax158 = set_file_security_ctx(rdi);
            rsp20 = reinterpret_cast<void*>(rsp157 + 1);
            if (!*reinterpret_cast<signed char*>(&eax158)) {
                if (*reinterpret_cast<unsigned char*>(r12_14 + 52)) 
                    goto addr_4d6d_21;
                goto addr_49c0_88;
            }
        }
        do {
            ++rdi;
        } while (*reinterpret_cast<void***>(rdi) == 47);
        continue;
        addr_4b10_66:
        r13_11 = reinterpret_cast<void**>(reinterpret_cast<unsigned char>(*reinterpret_cast<void***>(r9 + 24)) & 63);
        *reinterpret_cast<int32_t*>(&r13_11 + 4) = 0;
        goto addr_4b1c_70;
        addr_4969_63:
        if ((v159 & reinterpret_cast<uint32_t>(fun_f000)) != 0x4000) 
            goto addr_4dc7_99;
        *reinterpret_cast<void***>(v16) = reinterpret_cast<void**>(0);
        goto addr_4989_90;
    }
    goto addr_4780_15;
    addr_4c46_58:
    r8 = reinterpret_cast<void**>(20);
    *reinterpret_cast<int32_t*>(&r8 + 4) = 0;
    addr_4c4c_60:
    rsp160 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp129) - 8);
    *rsp160 = reinterpret_cast<void*>(0x4c64);
    rax161 = quotearg_style(4, v119, 4, v119);
    r12_14 = rax161;
    rsp162 = rsp160 + 1 - 1;
    *rsp162 = reinterpret_cast<void*>(0x4c7a);
    fun_3800();
    rsi = r8;
    rcx = r12_14;
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsp163 = rsp162 + 1 - 1;
    *rsp163 = reinterpret_cast<void*>(0x4c8f);
    fun_3b80();
    rsp20 = reinterpret_cast<void*>(rsp163 + 1);
    eax45 = 0;
    goto addr_4785_16;
    addr_4dc7_99:
    rsi47 = v32;
    goto addr_4d3c_20;
    addr_4dd3_73:
    rsp164 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp143) - 8);
    *rsp164 = reinterpret_cast<void*>(0x4de4);
    rax165 = quotearg_style(4, v32);
    rsp166 = reinterpret_cast<void*>(rsp164 + 1);
    r13_11 = rax165;
    addr_4da0_101:
    rsp167 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp166) - 8);
    *rsp167 = reinterpret_cast<void*>(0x4da7);
    rax168 = fun_3800();
    r12_14 = rax168;
    rsp169 = rsp167 + 1 - 1;
    *rsp169 = reinterpret_cast<void*>(0x4daf);
    rax170 = fun_36d0();
    rcx = r13_11;
    rdi = reinterpret_cast<void**>(0);
    *reinterpret_cast<int32_t*>(&rdi + 4) = 0;
    rsi = *reinterpret_cast<void***>(rax170);
    rsp171 = rsp169 + 1 - 1;
    *rsp171 = reinterpret_cast<void*>(0x4dc0);
    fun_3b80();
    rsp20 = reinterpret_cast<void*>(rsp171 + 1);
    eax45 = 0;
    goto addr_4785_16;
    addr_4d80_77:
    rsp172 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
    *rsp172 = reinterpret_cast<void*>(0x4d91);
    rax173 = quotearg_style(4, v32, 4, v32);
    rsp166 = reinterpret_cast<void*>(rsp172 + 1);
    r13_11 = rax173;
    goto addr_4da0_101;
    addr_4df5_87:
    rsp174 = reinterpret_cast<void**>(reinterpret_cast<uint64_t>(rsp20) - 8);
    *rsp174 = reinterpret_cast<void*>(0x4e06);
    rax175 = quotearg_style(4, v32);
    rsp166 = reinterpret_cast<void*>(rsp174 + 1);
    r13_11 = rax175;
    goto addr_4da0_101;
}