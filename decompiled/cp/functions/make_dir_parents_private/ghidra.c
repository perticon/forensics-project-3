make_dir_parents_private
          (char *param_1,ulong param_2,int param_3,long param_4,long *param_5,char *param_6,
          long param_7)

{
  undefined4 uVar1;
  long lVar2;
  long lVar3;
  char cVar4;
  int iVar5;
  int iVar6;
  uint uVar7;
  size_t __n;
  undefined8 uVar8;
  size_t sVar9;
  void *__dest;
  char *pcVar10;
  uint *puVar11;
  undefined4 *puVar12;
  undefined8 uVar13;
  int *piVar14;
  uint uVar15;
  ulong uVar16;
  stat **ppsVar17;
  undefined *puVar18;
  stat **ppsVar19;
  undefined *puVar21;
  char *pcVar23;
  uint uVar24;
  long in_FS_OFFSET;
  stat *local_1a8;
  long local_1a0;
  undefined4 *local_198;
  long *local_190;
  stat *local_188;
  char *local_180;
  char *local_178;
  void *local_170;
  stat local_168;
  undefined4 local_d8;
  undefined4 uStack212;
  undefined4 uStack208;
  undefined4 uStack204;
  __nlink_t local_c8;
  uint uStack192;
  __uid_t _Stack188;
  undefined8 local_b8;
  __dev_t _Stack176;
  __off_t local_a8;
  __blksize_t _Stack160;
  __blkcnt_t local_98;
  __time_t _Stack144;
  long local_88;
  __time_t _Stack128;
  long local_78;
  __time_t _Stack112;
  long local_68;
  long lStack96;
  long local_58;
  long lStack80;
  long local_40;
  stat **ppsVar20;
  undefined *puVar22;
  
  ppsVar20 = &local_1a8;
  ppsVar19 = &local_1a8;
  local_40 = *(long *)(in_FS_OFFSET + 0x28);
  local_1a0 = param_4;
  local_190 = param_5;
  local_180 = param_6;
  __n = dir_len();
  *local_190 = 0;
  ppsVar17 = &local_1a8;
  if (param_2 < __n) {
    sVar9 = strlen(param_1);
    ppsVar17 = &local_1a8;
    while (ppsVar20 != (stat **)((long)&local_1a8 - (sVar9 + 0x18 & 0xfffffffffffff000))) {
      ppsVar19 = (stat **)((long)ppsVar17 + -0x1000);
      *(undefined8 *)((long)ppsVar17 + -8) = *(undefined8 *)((long)ppsVar17 + -8);
      ppsVar20 = (stat **)((long)ppsVar17 + -0x1000);
      ppsVar17 = (stat **)((long)ppsVar17 + -0x1000);
    }
    uVar16 = (ulong)((uint)(sVar9 + 0x18) & 0xff0);
    lVar3 = -uVar16;
    puVar21 = (undefined *)((long)ppsVar19 + lVar3);
    puVar22 = (undefined *)((long)ppsVar19 + lVar3);
    if (uVar16 != 0) {
      *(undefined8 *)((long)ppsVar19 + -8) = *(undefined8 *)((long)ppsVar19 + -8);
    }
    *(undefined8 *)((long)ppsVar19 + lVar3 + -8) = 0x104815;
    local_170 = memcpy((void *)((ulong)((long)ppsVar19 + lVar3 + 0xf) & 0xfffffffffffffff0),param_1,
                       sVar9 + 1);
    puVar18 = (undefined *)((long)ppsVar19 + lVar3);
    while (puVar21 != (undefined *)((long)ppsVar19 + (lVar3 - (__n + 0x18 & 0xfffffffffffff000)))) {
      puVar22 = puVar18 + -0x1000;
      *(undefined8 *)(puVar18 + -8) = *(undefined8 *)(puVar18 + -8);
      puVar21 = puVar18 + -0x1000;
      puVar18 = puVar18 + -0x1000;
    }
    uVar16 = (ulong)((uint)(__n + 0x18) & 0xff0);
    lVar3 = -uVar16;
    ppsVar17 = (stat **)(puVar22 + lVar3);
    puVar18 = puVar22 + lVar3;
    if (uVar16 != 0) {
      *(undefined8 *)(puVar22 + -8) = *(undefined8 *)(puVar22 + -8);
    }
    __dest = (void *)((ulong)(puVar22 + lVar3 + 0xf) & 0xfffffffffffffff0);
    *(undefined8 *)(puVar22 + lVar3 + -8) = 0x10487f;
    memcpy(__dest,local_170,__n);
    pcVar23 = (char *)((long)__dest + param_2);
    *(undefined *)((long)__dest + __n) = 0;
    cVar4 = *pcVar23;
    while (cVar4 == '/') {
      pcVar23 = pcVar23 + 1;
      cVar4 = *pcVar23;
    }
    local_188 = &local_168;
    *(undefined8 *)(puVar22 + lVar3 + -8) = 0x1048b4;
    iVar5 = fstatat(param_3,pcVar23,&local_168,0);
    if (iVar5 == 0) {
      if ((local_168.st_mode & 0xf000) != 0x4000) {
LAB_00104d3c:
        *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104d46;
        uVar8 = quotearg_style(4,__dest);
        *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104d5c;
        uVar13 = dcgettext(0,"%s exists but is not a directory",5);
        *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104d6d;
        error(0,0,uVar13,uVar8);
LAB_00104d6d:
        uVar8 = 0;
        puVar18 = puVar22 + lVar3;
        goto LAB_00104785;
      }
      *local_180 = '\0';
    }
    else {
      pcVar23 = (char *)(param_2 + (long)local_170);
      cVar4 = *pcVar23;
      local_178 = pcVar23;
      while (cVar4 == '/') {
        pcVar23 = pcVar23 + 1;
        cVar4 = *pcVar23;
      }
      local_1a8 = (stat *)&local_d8;
      pcVar10 = pcVar23;
      while( true ) {
        *(undefined8 *)(puVar22 + lVar3 + -8) = 0x10490a;
        pcVar10 = strchr(pcVar10,0x2f);
        ppsVar17 = (stat **)(puVar22 + lVar3);
        if (pcVar10 == (char *)0x0) break;
        *pcVar10 = '\0';
        *(undefined8 *)(puVar22 + lVar3 + -8) = 0x10492d;
        iVar5 = fstatat(param_3,pcVar23,local_188,0);
        if ((iVar5 == 0) && ((*(uint *)(param_7 + 0x1c) & 0xffffff00) == 0)) {
LAB_00104947:
          *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104961;
          cVar4 = set_process_security_ctx(local_178,local_170,0);
          if (cVar4 == '\0') goto LAB_00104d6d;
          __dest = local_170;
          if ((local_168.st_mode & 0xf000) != 0x4000) goto LAB_00104d3c;
          *local_180 = '\0';
LAB_00104989:
          if ((*(long *)(param_7 + 0x28) != 0) || (*(char *)(param_7 + 0x33) != '\0')) {
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x1049a6;
            cVar4 = set_file_security_ctx(local_170,0,param_7);
            if ((cVar4 == '\0') && (*(char *)(param_7 + 0x34) != '\0')) goto LAB_00104d6d;
          }
        }
        else {
          *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104a03;
          iVar6 = stat(local_178,local_1a8);
          if (iVar6 == 0) {
            if ((uStack192 & 0xf000) != 0x4000) {
              uVar7 = 0x14;
              goto LAB_00104c4c;
            }
          }
          else {
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104a10;
            puVar11 = (uint *)__errno_location();
            uVar7 = *puVar11;
            if (uVar7 != 0) {
LAB_00104c4c:
              local_170 = (void *)((ulong)local_170 & 0xffffffff00000000 | (ulong)uVar7);
              *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104c64;
              uVar8 = quotearg_style(4,local_178);
              *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104c7a;
              uVar13 = dcgettext(0,"failed to get attributes of %s",5);
              *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104c8f;
              error(0,(ulong)local_170 & 0xffffffff,uVar13,uVar8);
              uVar8 = 0;
              goto LAB_00104785;
            }
          }
          *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104a26;
          puVar12 = (undefined4 *)xmalloc(0xa8);
          *puVar12 = local_d8;
          puVar12[1] = uStack212;
          puVar12[2] = uStack208;
          puVar12[3] = uStack204;
          *(__nlink_t *)(puVar12 + 4) = local_c8;
          *(ulong *)(puVar12 + 6) = CONCAT44(_Stack188,uStack192);
          *(undefined8 *)(puVar12 + 8) = local_b8;
          *(__dev_t *)(puVar12 + 10) = _Stack176;
          *(__off_t *)(puVar12 + 0xc) = local_a8;
          *(__blksize_t *)(puVar12 + 0xe) = _Stack160;
          *(__blkcnt_t *)(puVar12 + 0x10) = local_98;
          *(__time_t *)(puVar12 + 0x12) = _Stack144;
          *(long *)(puVar12 + 0x14) = local_88;
          *(__time_t *)(puVar12 + 0x16) = _Stack128;
          *(long *)(puVar12 + 0x18) = local_78;
          *(__time_t *)(puVar12 + 0x1a) = _Stack112;
          *(long *)(puVar12 + 0x1c) = local_68;
          *(long *)(puVar12 + 0x1e) = lStack96;
          *(long *)(puVar12 + 0x20) = local_58;
          *(long *)(puVar12 + 0x22) = lStack80;
          *(long *)(puVar12 + 0x26) = (long)pcVar10 - (long)local_170;
          lVar2 = *local_190;
          *(undefined *)(puVar12 + 0x24) = 0;
          *(long *)(puVar12 + 0x28) = lVar2;
          *local_190 = (long)puVar12;
          if (iVar5 == 0) goto LAB_00104947;
          uVar1 = puVar12[6];
          local_198 = puVar12;
          *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104ae7;
          cVar4 = set_process_security_ctx
                            (local_58,local_c8,local_b8,local_a8,local_98,local_88,local_78,local_68
                             ,local_178,local_170,uVar1);
          if (cVar4 == '\0') goto LAB_00104d6d;
          *local_180 = '\x01';
          uVar7 = local_198[6];
          if (*(char *)(param_7 + 0x1d) == '\0') {
            if (*(char *)(param_7 + 0x1e) == '\0') {
              uVar15 = 0xffffffff;
              uVar24 = 0;
            }
            else {
              uVar24 = uVar7 & 0x12;
              uVar15 = ~uVar24;
            }
          }
          else {
            uVar24 = uVar7 & 0x3f;
            uVar15 = ~uVar24;
          }
          if (*(char *)(param_7 + 0x20) != '\0') {
            uVar7 = 0x1ff;
          }
          *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104b44;
          iVar5 = mkdirat(param_3,pcVar23,uVar7 & uVar15 & 0xfff);
          if (iVar5 != 0) {
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104de4;
            uVar8 = quotearg_style(4,local_170);
            pcVar23 = "cannot make directory %s";
LAB_00104da0:
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104da7;
            uVar13 = dcgettext(0,pcVar23,5);
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104daf;
            piVar14 = __errno_location();
            iVar5 = *piVar14;
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104dc0;
            error(0,iVar5,uVar13,uVar8);
            uVar8 = 0;
            puVar18 = puVar22 + lVar3;
            goto LAB_00104785;
          }
          if (local_1a0 != 0) {
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104b80;
            __printf_chk(1,local_1a0,local_178,local_170);
          }
          *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104ba5;
          iVar5 = fstatat(param_3,pcVar23,local_188,0x100);
          if (iVar5 != 0) {
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104d91;
            uVar8 = quotearg_style(4,local_170);
            pcVar23 = "failed to get attributes of %s";
            goto LAB_00104da0;
          }
          if (*(char *)(param_7 + 0x1e) == '\0') {
            if ((~local_168.st_mode & uVar24) == 0) {
LAB_00104bcf:
              if ((local_168.st_mode & 0x1c0) == 0x1c0) goto LAB_00104bee;
            }
            else {
              *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104cf1;
              uVar7 = cached_umask();
              uVar24 = uVar24 & ~uVar7;
              if ((~local_168.st_mode & uVar24) == 0) goto LAB_00104bcf;
            }
            *(undefined *)(local_198 + 0x24) = 1;
            local_198[6] = uVar24 | local_168.st_mode;
          }
LAB_00104bee:
          if ((local_168.st_mode | 0x1c0) != local_168.st_mode) {
            *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104c0a;
            iVar5 = fchmodat(param_3,pcVar23,local_168.st_mode | 0x1c0,0x100);
            if (iVar5 != 0) {
              *(undefined8 *)(puVar22 + lVar3 + -8) = 0x104e06;
              uVar8 = quotearg_style(4,local_170);
              pcVar23 = "setting permissions for %s";
              goto LAB_00104da0;
            }
          }
          if (*local_180 == '\0') goto LAB_00104989;
        }
        cVar4 = pcVar10[1];
        *pcVar10 = '/';
        pcVar10 = pcVar10 + 1;
        while (cVar4 == '/') {
          pcVar10 = pcVar10 + 1;
          cVar4 = *pcVar10;
        }
      }
    }
  }
  uVar8 = 1;
  puVar18 = (undefined *)ppsVar17;
LAB_00104785:
  if (local_40 == *(long *)(in_FS_OFFSET + 0x28)) {
    return uVar8;
  }
                    /* WARNING: Subroutine does not return */
  *(undefined8 *)(puVar18 + -8) = 0x104e1c;
  __stack_chk_fail();
}