void main(int param_1,undefined8 *param_2)

{
  ulong uVar1;
  ulong uVar2;
  int iVar3;
  uint uVar4;
  undefined4 uVar5;
  char *pcVar6;
  undefined8 uVar7;
  long lVar8;
  int *piVar9;
  undefined8 uVar10;
  undefined8 *puVar11;
  long in_FS_OFFSET;
  undefined auVar12 [16];
  ulong local_c8;
  ulong local_c0;
  ulong local_b8;
  ulong local_b0;
  char local_a3;
  undefined local_a2;
  undefined local_a1;
  int *local_a0;
  undefined4 local_98;
  int local_94;
  int local_90;
  int iStack140;
  undefined8 local_88;
  undefined2 local_80;
  undefined4 local_7c;
  undefined local_78;
  ulong local_70;
  undefined local_68;
  undefined uStack103;
  undefined uStack102;
  char cStack101;
  char cStack100;
  undefined uStack99;
  char cStack98;
  undefined uStack97;
  char cStack96;
  undefined uStack95;
  char cStack94;
  undefined uStack93;
  undefined local_5c;
  undefined uStack91;
  undefined local_5a;
  int local_54;
  undefined8 local_50;
  undefined8 local_48;
  undefined8 local_40;
  
  local_40 = *(undefined8 *)(in_FS_OFFSET + 0x28);
  set_program_name(*param_2);
  setlocale(6,"");
  bindtextdomain("coreutils","/usr/local/share/locale");
  textdomain("coreutils");
  atexit(close_stdin);
  selinux_enabled = '\0';
  cp_options_default(&local_98);
  local_78 = 0;
  local_54 = 1;
  local_90 = 4;
  iStack140 = 2;
  local_7c = 0;
  local_94 = 1;
  local_88 = 0x100000000;
  local_80 = 0;
  local_70 = 0;
  local_68 = 0;
  uStack103 = 1;
  uStack102 = 0;
  cStack101 = '\0';
  cStack100 = '\0';
  uStack99 = 0;
  cStack98 = '\0';
  uStack97 = 0;
  cStack96 = '\0';
  uStack95 = 0;
  cStack94 = '\0';
  uStack93 = 0;
  local_5c = 0;
  uStack91 = 0;
  pcVar6 = getenv("POSIXLY_CORRECT");
  local_a2 = 0;
  local_50 = 0;
  local_5a = pcVar6 != (char *)0x0;
  local_48 = 0;
  local_b0 = 0;
  local_c8 = 0;
  local_a1 = 0;
  local_c0 = 0;
  local_b8 = 0;
  local_a3 = '\0';
  uVar1 = local_c0;
  uVar2 = local_b0;
LAB_00103ea0:
  do {
    while (local_b0 = uVar2, local_c0 = uVar1, puVar11 = param_2,
          iVar3 = getopt_long(param_1,param_2,"abdfHilLnprst:uvxPRS:TZ",long_opts,0), iVar3 == -1) {
LAB_00103ef9:
      if ((local_88._7_1_ == '\0') || (cStack94 == '\0')) {
        if (local_90 == 2) {
          uStack93 = 0;
          if (local_a3 == '\0') goto LAB_0010426b;
          pcVar6 = "options --backup and --no-clobber are mutually exclusive";
        }
        else {
          if (local_a3 == '\0') {
LAB_0010426b:
            uVar5 = 0;
            if (local_54 != 2) goto LAB_00104278;
            if (iStack140 == 2) goto LAB_00104278;
          }
          else if ((local_54 != 2) || (iStack140 == 2)) {
LAB_0010438b:
            uVar7 = dcgettext(0,"backup type",5);
            uVar5 = xget_version(uVar7,local_c0);
LAB_00104278:
            local_98 = uVar5;
            set_simple_backup_suffix();
            if (local_94 == 1) goto LAB_00104334;
LAB_00104291:
            if (cStack96 != '\0') goto LAB_00104352;
            do {
              if ((local_b0 | local_70) != 0) {
                if (cStack100 == '\0') {
                  cStack101 = '\0';
                }
                else {
                  if (cStack101 != '\0') {
                    uVar7 = dcgettext(0,"cannot set target context and preserve it",5);
                    error(1,0,uVar7);
                    goto LAB_0010438b;
                  }
                  if (selinux_enabled == '\0') goto LAB_00104310;
                }
                if (local_b0 != 0) goto LAB_0010448d;
LAB_001042c6:
                if (cStack98 == '\0') {
                  hash_init();
                  uVar4 = do_copy(param_1 - optind,param_2 + optind,local_c8,local_a2,&local_98);
                    /* WARNING: Subroutine does not return */
                  exit((uVar4 ^ 1) & 0xff);
                }
                goto LAB_00104469;
              }
              if ((cStack100 == '\0') || (selinux_enabled != '\0')) goto LAB_001042c6;
LAB_00104310:
              uVar7 = dcgettext(0,
                                "cannot preserve security context without an SELinux-enabled kernel"
                                ,5);
              error(1,0,uVar7);
LAB_00104334:
              if ((cStack96 == '\0') || (local_88._7_1_ != '\0')) goto LAB_001043d2;
              local_94 = 2;
LAB_00104352:
              local_88._0_5_ = CONCAT14(local_a1,(undefined4)local_88);
              local_88 = local_88 & 0xffffff0000000000 | (ulong)(uint5)local_88;
            } while( true );
          }
          pcVar6 = "--reflink can be used only with --sparse=auto";
        }
      }
      else {
        pcVar6 = "cannot make both hard and symbolic links";
      }
      uVar7 = dcgettext(0,pcVar6,5);
      error(0,0,uVar7);
switchD_00103edc_caseD_49:
      usage();
switchD_00103edc_caseD_52:
      cStack96 = '\x01';
      uVar1 = local_c0;
      uVar2 = local_b0;
    }
    if (0x88 < iVar3) goto switchD_00103edc_caseD_49;
    if (iVar3 < 0x48) {
      if (iVar3 == -0x83) {
        version_etc(stdout,&DAT_00114051,"GNU coreutils",Version,"Torbjorn Granlund",
                    "David MacKenzie","Jim Meyering",0);
                    /* WARNING: Subroutine does not return */
        exit(0);
      }
      if (iVar3 == -0x82) {
        usage(0);
        goto LAB_00103ef9;
      }
      goto switchD_00103edc_caseD_49;
    }
    uVar1 = local_c0;
    uVar2 = local_b0;
    switch(iVar3) {
    case 0x48:
      local_94 = 3;
      break;
    default:
      goto switchD_00103edc_caseD_49;
    case 0x4c:
      local_94 = 4;
      break;
    case 0x50:
      local_94 = 2;
      break;
    case 0x52:
    case 0x72:
      goto switchD_00103edc_caseD_52;
    case 0x53:
      local_b8 = optarg;
      local_a3 = '\x01';
      break;
    case 0x54:
      local_a2 = 1;
      break;
    case 0x5a:
      if (selinux_enabled == '\0') {
        if (optarg != 0) {
          uVar7 = dcgettext(0,"warning: ignoring --context; it requires an SELinux-enabled kernel",5
                           );
          error(0,0,uVar7);
          uVar1 = local_c0;
          uVar2 = local_b0;
        }
      }
      else {
        uVar2 = optarg;
        if (optarg == 0) {
          local_a0 = __errno_location();
          *local_a0 = 0x5f;
          local_70 = 0;
          uVar7 = dcgettext(0,"warning: ignoring --context",5);
          error(0,*local_a0,uVar7);
          uVar1 = local_c0;
          uVar2 = local_b0;
        }
      }
      break;
    case 0x61:
      local_94 = 2;
      local_68 = 1;
      local_7c._0_2_ = CONCAT11(1,(undefined)local_7c);
      local_7c = CONCAT22(0x101,(undefined2)local_7c);
      uStack102 = 1;
      if (selinux_enabled != '\0') {
        cStack101 = '\x01';
      }
      uStack99 = 1;
      uStack97 = 1;
      cStack96 = '\x01';
      break;
    case 0x62:
      local_a3 = '\x01';
      uVar1 = optarg;
      if (optarg == 0) {
        uVar1 = local_c0;
      }
      break;
    case 100:
      local_68 = 1;
      local_94 = 2;
      break;
    case 0x66:
      local_88._0_7_ = CONCAT16(1,(uint6)local_88);
      local_88 = local_88 & 0xff00000000000000 | (ulong)(uint7)local_88;
      break;
    case 0x69:
      local_90 = 3;
      break;
    case 0x6c:
      local_88 = CONCAT17(1,(uint7)local_88);
      break;
    case 0x6e:
      local_90 = 2;
      break;
    case 0x73:
      cStack94 = '\x01';
      break;
    case 0x74:
      if (local_c8 != 0) {
        uVar7 = dcgettext(0,"multiple target directories specified",5);
        error(1,0,uVar7);
LAB_00104469:
        uVar7 = dcgettext(0,"cannot preserve extended attributes, cp is built without xattr support"
                          ,5);
        error(1,0,uVar7);
LAB_0010448d:
        piVar9 = __errno_location();
        *piVar9 = 0x5f;
        uVar7 = quote(local_b0);
        uVar10 = dcgettext(0,"failed to set default file creation context to %s",5);
        auVar12 = error(1,*piVar9,uVar10,uVar7);
        uVar1 = local_c8;
        local_c8 = SUB168(auVar12,0);
        (*(code *)PTR___libc_start_main_0011bfc8)
                  (main,uVar1,&local_c0,0,0,SUB168(auVar12 >> 0x40,0),&local_c8);
        do {
                    /* WARNING: Do nothing block with infinite loop */
        } while( true );
      }
      local_c8 = optarg;
      break;
    case 0x75:
      uStack93 = 1;
      break;
    case 0x76:
      local_5c = 1;
      break;
    case 0x78:
      local_7c = CONCAT31(local_7c._1_3_,1);
      break;
    case 0x80:
      uStack103 = 0;
      break;
    case 0x81:
      local_a1 = 1;
      break;
    case 0x82:
      decode_preserve_arg(optarg,&local_98,0);
      uVar1 = local_c0;
      uVar2 = local_b0;
      break;
    case 0x83:
      parents_option = 1;
      break;
    case 0x84:
      if (optarg != 0) {
        decode_preserve_arg(optarg,&local_98,1);
        uStack102 = 1;
        uVar1 = local_c0;
        uVar2 = local_b0;
        break;
      }
    case 0x70:
      local_7c._0_2_ = CONCAT11(1,(undefined)local_7c);
      local_7c = CONCAT22(0x101,(undefined2)local_7c);
      uStack102 = 1;
      break;
    case 0x85:
      if (optarg == 0) {
        local_54 = 2;
      }
      else {
        lVar8 = __xargmatch_internal
                          ("--reflink",optarg,reflink_type_string,reflink_type,4,argmatch_die,1,
                           (long)&switchD_00103edc::switchdataD_0011433c +
                           (long)(int)(&switchD_00103edc::switchdataD_0011433c)[iVar3 - 0x48]);
        local_54 = *(int *)(reflink_type + lVar8 * 4);
        uVar1 = local_c0;
        uVar2 = local_b0;
      }
      break;
    case 0x86:
      lVar8 = __xargmatch_internal
                        ("--sparse",optarg,sparse_type_string,sparse_type,4,argmatch_die,1,puVar11);
      iStack140 = *(int *)(sparse_type + lVar8 * 4);
      uVar1 = local_c0;
      uVar2 = local_b0;
      break;
    case 0x87:
      remove_trailing_slashes = 1;
      break;
    case 0x88:
      goto switchD_00103edc_caseD_88;
    }
  } while( true );
LAB_001043d2:
  local_94 = 4;
  goto LAB_00104291;
switchD_00103edc_caseD_88:
  local_88._0_6_ = CONCAT15(1,(uint5)local_88);
  local_88 = local_88 & 0xffff000000000000 | (ulong)(uint6)local_88;
  goto LAB_00103ea0;
}