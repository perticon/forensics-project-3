uint64_t decode_preserve_arg (int64_t arg_1dh, int64_t arg_1eh, int64_t arg_1fh, int64_t arg_20h, int64_t arg_30h, int64_t arg_33h, int64_t arg_34h, int64_t arg_35h, int64_t arg_36h, int64_t arg2, int64_t arg3) {
    int64_t var_10h;
    int64_t var_8h;
    rsi = arg2;
    rdx = arg3;
    r14 = "--preserve";
    r13 = 0x00014320;
    r12d = edx;
    ebx = edx;
    rax = xstrdup (rdi);
    rbx = obj_preserve_vals_1;
    *((rsp + 8)) = rax;
    r15 = rax;
    rax = "--no-preserve";
    if (bl != 0) {
        rax = r14;
    }
    *(rsp) = rax;
label_0:
    r14d = 0;
    rax = strchr (r15, 0x2c);
    if (rax != 0) {
        *(rax) = 0;
        r14 = rax + 1;
    }
    _xargmatch_internal (*((rsp + 0x10)), r15, obj.preserve_args.2, rbx, 4, *(obj.argmatch_die));
    if (*((rbx + rax*4)) > 6) {
        void (*0x3cd0)() ();
    }
    eax = *((rbx + rax*4));
    rax = *((r13 + rax*4));
    rax += r13;
    /* switch table (7 cases) at 0x14320 */
    void (*rax)() ();
    *((rbp + 0x35)) = r12b;
    *((rbp + 0x36)) = r12b;
label_1:
    if (r14 == 0) {
        goto label_2;
    }
    do {
        r15 = r14;
        goto label_0;
        *((rbp + 0x34)) = r12b;
        *((rbp + 0x33)) = r12b;
    } while (r14 != 0);
label_2:
    rdi = *((rsp + 8));
    void (*0x3670)() ();
    *((rbp + 0x30)) = r12b;
    goto label_1;
    *((rbp + 0x1d)) = r12b;
    goto label_1;
    *((rbp + 0x1f)) = r12b;
    goto label_1;
    eax = r12d;
    *((rbp + 0x1e)) = r12b;
    eax ^= 1;
    *((rbp + 0x20)) = al;
    goto label_1;
    eax = r12d;
    *((rbp + 0x1e)) = r12b;
    eax ^= 1;
    *((rbp + 0x1f)) = r12b;
    *((rbp + 0x1d)) = r12b;
    *((rbp + 0x30)) = r12b;
    *((rbp + 0x20)) = al;
    if (*(obj.selinux_enabled) != 0) {
        *((rbp + 0x33)) = r12b;
    }
    *((rbp + 0x35)) = r12b;
    goto label_1;
}