void decode_preserve_arg(undefined8 param_1,undefined8 param_2,char param_3)

{
  char *__s;
  char *pcVar1;
  char *pcVar2;
  long lVar3;
  
  __s = (char *)xstrdup();
  pcVar1 = "--no-preserve";
  if (param_3 != '\0') {
    pcVar1 = "--preserve";
  }
  pcVar2 = strchr(__s,0x2c);
  if (pcVar2 != (char *)0x0) {
    *pcVar2 = '\0';
  }
  lVar3 = __xargmatch_internal(pcVar1,__s,preserve_args_2,preserve_vals_1,4,argmatch_die);
  if (*(uint *)(preserve_vals_1 + lVar3 * 4) < 7) {
                    /* WARNING: Could not recover jumptable at 0x0010466d. Too many branches */
                    /* WARNING: Treating indirect jump as call */
    (*(code *)(&DAT_00114320 +
              *(int *)(&DAT_00114320 + (ulong)*(uint *)(preserve_vals_1 + lVar3 * 4) * 4)))();
    return;
  }
                    /* WARNING: Subroutine does not return */
  abort();
}