void decode_preserve_arg(void** rdi, void** rsi, int32_t edx, int64_t rcx, int64_t r8, void** r9, int64_t a7, int64_t a8) {
    int32_t ebx9;
    void** rax10;
    void** rax11;
    void** rax12;
    int64_t r9_13;
    int64_t rax14;
    int64_t rax15;

    ebx9 = edx;
    rax10 = xstrdup(rdi, rsi);
    rax11 = reinterpret_cast<void**>("--no-preserve");
    if (*reinterpret_cast<signed char*>(&ebx9)) {
        rax11 = reinterpret_cast<void**>("--preserve");
    }
    rax12 = fun_3870(rax10, rax10);
    if (rax12) {
        *reinterpret_cast<void***>(rax12) = reinterpret_cast<void**>(0);
    }
    r9_13 = argmatch_die;
    rax14 = __xargmatch_internal(rax11, rax10, 0x1b560, 0x14440, 4, r9_13);
    if (*reinterpret_cast<uint32_t*>(0x14440 + rax14 * 4) > 6) {
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
        fun_36c0(rax11, rax11);
    } else {
        *reinterpret_cast<uint32_t*>(&rax15) = *reinterpret_cast<uint32_t*>(0x14440 + rax14 * 4);
        *reinterpret_cast<int32_t*>(reinterpret_cast<int64_t>(&rax15) + 4) = 0;
        goto *reinterpret_cast<int32_t*>(0x14320 + rax15 * 4) + 0x14320;
    }
}